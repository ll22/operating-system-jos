
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start+0xeffffff4>:
.globl _start
_start = RELOC(entry)

.globl entry
entry:
	movw	$0x1234, 0x472			# warm boot
f0100000:	02 b0 ad 1b 02 00    	add    0x21bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fc                   	cld    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <entry>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 
	# sufficient until we set up our real page table in mem_init
	# in lab 2.

	# Load the physical address of entry_pgdir into cr3.  entry_pgdir
	# is defined in entrypgdir.c.
	movl	$(RELOC(entry_pgdir)), %ecx
f0100015:	b9 00 20 12 00       	mov    $0x122000,%ecx
	movl	%ecx, %cr3
f010001a:	0f 22 d9             	mov    %ecx,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %ecx
f010001d:	0f 20 e1             	mov    %cr4,%ecx
	orl	$(CR4_PSE), %ecx
f0100020:	83 c9 10             	or     $0x10,%ecx
	movl	%ecx, %cr4
f0100023:	0f 22 e1             	mov    %ecx,%cr4
	# Turn on paging.
	movl	%cr0, %ecx
f0100026:	0f 20 c1             	mov    %cr0,%ecx
	orl	$(CR0_PE|CR0_PG|CR0_WP), %ecx
f0100029:	81 c9 01 00 01 80    	or     $0x80010001,%ecx
	movl	%ecx, %cr0
f010002f:	0f 22 c1             	mov    %ecx,%cr0

	# Now paging is enabled, but we're still running at a low EIP
	# (why is this okay?).  Jump up above KERNBASE before entering
	# C code.
	mov	$relocated, %ecx
f0100032:	b9 39 00 10 f0       	mov    $0xf0100039,%ecx
	jmp	*%ecx
f0100037:	ff e1                	jmp    *%ecx

f0100039 <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0, %ebp			# nuke frame pointer
f0100039:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f010003e:	bc 00 20 12 f0       	mov    $0xf0122000,%esp

	# pointer to struct multiboot_info
	pushl	%ebx
f0100043:	53                   	push   %ebx
	# saved magic value
	pushl	%eax
f0100044:	50                   	push   %eax

	# now to C code
	call	i386_init
f0100045:	e8 5c 00 00 00       	call   f01000a6 <i386_init>

f010004a <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010004a:	eb fe                	jmp    f010004a <spin>

f010004c <_panic>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
f010004c:	55                   	push   %ebp
f010004d:	89 e5                	mov    %esp,%ebp
f010004f:	56                   	push   %esi
f0100050:	53                   	push   %ebx
f0100051:	8b 75 10             	mov    0x10(%ebp),%esi
	va_list ap;

	if (panicstr)
f0100054:	83 3d 10 2f 27 f0 00 	cmpl   $0x0,0xf0272f10
f010005b:	75 3a                	jne    f0100097 <_panic+0x4b>
		goto dead;
	panicstr = fmt;
f010005d:	89 35 10 2f 27 f0    	mov    %esi,0xf0272f10

	// Be extra sure that the machine is in as reasonable state
	asm volatile("cli; cld");
f0100063:	fa                   	cli    
f0100064:	fc                   	cld    

	va_start(ap, fmt);
f0100065:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel panic on CPU %d at %s:%d: ", cpunum(), file, line);
f0100068:	e8 9d 66 00 00       	call   f010670a <cpunum>
f010006d:	ff 75 0c             	pushl  0xc(%ebp)
f0100070:	ff 75 08             	pushl  0x8(%ebp)
f0100073:	50                   	push   %eax
f0100074:	68 20 6f 10 f0       	push   $0xf0106f20
f0100079:	e8 7a 3b 00 00       	call   f0103bf8 <cprintf>
	vcprintf(fmt, ap);
f010007e:	83 c4 08             	add    $0x8,%esp
f0100081:	53                   	push   %ebx
f0100082:	56                   	push   %esi
f0100083:	e8 4a 3b 00 00       	call   f0103bd2 <vcprintf>
	cprintf("\n");
f0100088:	c7 04 24 32 81 10 f0 	movl   $0xf0108132,(%esp)
f010008f:	e8 64 3b 00 00       	call   f0103bf8 <cprintf>
	va_end(ap);
f0100094:	83 c4 10             	add    $0x10,%esp

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f0100097:	83 ec 0c             	sub    $0xc,%esp
f010009a:	6a 00                	push   $0x0
f010009c:	e8 b7 09 00 00       	call   f0100a58 <monitor>
f01000a1:	83 c4 10             	add    $0x10,%esp
f01000a4:	eb f1                	jmp    f0100097 <_panic+0x4b>

f01000a6 <i386_init>:
static void boot_aps(void);


void
i386_init(uint32_t magic, uint32_t addr)
{
f01000a6:	55                   	push   %ebp
f01000a7:	89 e5                	mov    %esp,%ebp
f01000a9:	56                   	push   %esi
f01000aa:	53                   	push   %ebx
	extern char edata[], end[];

	// Before doing anything else, complete the ELF loading process.
	// Clear the uninitialized global data (BSS) section of our program.
	// This ensures that all static/global variables start out zero.
	memset(edata, 0, end - edata);
f01000ab:	83 ec 04             	sub    $0x4,%esp
f01000ae:	b8 08 50 2b f0       	mov    $0xf02b5008,%eax
f01000b3:	2d 7c 11 27 f0       	sub    $0xf027117c,%eax
f01000b8:	50                   	push   %eax
f01000b9:	6a 00                	push   $0x0
f01000bb:	68 7c 11 27 f0       	push   $0xf027117c
f01000c0:	e8 3f 5f 00 00       	call   f0106004 <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f01000c5:	e8 23 06 00 00       	call   f01006ed <cons_init>

	// Must boot from Multiboot.
	assert(magic == MULTIBOOT_BOOTLOADER_MAGIC);
f01000ca:	83 c4 10             	add    $0x10,%esp
f01000cd:	81 7d 08 02 b0 ad 2b 	cmpl   $0x2badb002,0x8(%ebp)
f01000d4:	74 16                	je     f01000ec <i386_init+0x46>
f01000d6:	68 44 6f 10 f0       	push   $0xf0106f44
f01000db:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01000e0:	6a 26                	push   $0x26
f01000e2:	68 e6 6f 10 f0       	push   $0xf0106fe6
f01000e7:	e8 60 ff ff ff       	call   f010004c <_panic>

	cprintf("451 decimal is %o octal!\n", 451);
f01000ec:	83 ec 08             	sub    $0x8,%esp
f01000ef:	68 c3 01 00 00       	push   $0x1c3
f01000f4:	68 f2 6f 10 f0       	push   $0xf0106ff2
f01000f9:	e8 fa 3a 00 00       	call   f0103bf8 <cprintf>

	// Print CPU information.
	cpuid_print();
f01000fe:	e8 32 56 00 00       	call   f0105735 <cpuid_print>

	// Initialize e820 memory map.
	e820_init(addr);
f0100103:	83 c4 04             	add    $0x4,%esp
f0100106:	ff 75 0c             	pushl  0xc(%ebp)
f0100109:	e8 8e 0a 00 00       	call   f0100b9c <e820_init>

	// Lab 2 memory management initialization functions
	mem_init();
f010010e:	e8 43 16 00 00       	call   f0101756 <mem_init>

	// Lab 3 user environment initialization functions
	env_init();
f0100113:	e8 d8 32 00 00       	call   f01033f0 <env_init>
	cprintf("After env_init\n");
f0100118:	c7 04 24 0c 70 10 f0 	movl   $0xf010700c,(%esp)
f010011f:	e8 d4 3a 00 00       	call   f0103bf8 <cprintf>
	trap_init();
f0100124:	e8 a3 3b 00 00       	call   f0103ccc <trap_init>
	//Advanced Configuration and Power Interface (ACPI) specification provides an open standard
	//that the operating systems can use for computer hardware discovery, configuration, power 
	//management, and monitoring. Internally, ACPI exports the available functionalities by providing 
	//certain instruction lists as part of the system firmware, which the operating system kernel 
	//interprets and executes to perform desired operations, using a form of embedded virtual machine.
	acpi_init();
f0100129:	e8 84 62 00 00       	call   f01063b2 <acpi_init>
	mp_init(); 
f010012e:	e8 07 65 00 00       	call   f010663a <mp_init>
	lapic_init(); //The local APIC manages internal (non-I/O) interrupts
f0100133:	e8 2d 66 00 00       	call   f0106765 <lapic_init>

	// Lab 4 multitasking initialization functions
	pic_init();
f0100138:	e8 1d 3a 00 00       	call   f0103b5a <pic_init>
	ioapic_init(); //The I/O APIC manages hardware interrupts for an SMP system.
f010013d:	e8 9d 68 00 00       	call   f01069df <ioapic_init>

	// Enable keyboard & serial interrupts
	ioapic_enable(IRQ_KBD, bootcpu->cpu_apicid);
f0100142:	83 c4 08             	add    $0x8,%esp
f0100145:	0f b6 05 00 40 27 f0 	movzbl 0xf0274000,%eax
f010014c:	50                   	push   %eax
f010014d:	6a 01                	push   $0x1
f010014f:	e8 34 69 00 00       	call   f0106a88 <ioapic_enable>
	ioapic_enable(IRQ_SERIAL, bootcpu->cpu_apicid);
f0100154:	83 c4 08             	add    $0x8,%esp
f0100157:	0f b6 05 00 40 27 f0 	movzbl 0xf0274000,%eax
f010015e:	50                   	push   %eax
f010015f:	6a 04                	push   $0x4
f0100161:	e8 22 69 00 00       	call   f0106a88 <ioapic_enable>
extern struct spinlock kernel_lock;

static inline void
lock_kernel(void)
{
	spin_lock(&kernel_lock);
f0100166:	c7 04 24 c0 33 12 f0 	movl   $0xf01233c0,(%esp)
f010016d:	e8 56 69 00 00       	call   f0106ac8 <spin_lock>
{
	extern unsigned char mpentry_start[], mpentry_end[];
	void *code;
	struct CpuInfo *c;

	if (ncpu <= 1)
f0100172:	83 c4 10             	add    $0x10,%esp
f0100175:	83 3d a0 43 27 f0 01 	cmpl   $0x1,0xf02743a0
f010017c:	0f 8e e1 00 00 00    	jle    f0100263 <i386_init+0x1bd>
		return;
	cprintf("SMP: BSP #%d [apicid %02x]\n", cpunum(), thiscpu->cpu_apicid);
f0100182:	e8 83 65 00 00       	call   f010670a <cpunum>
f0100187:	6b c0 74             	imul   $0x74,%eax,%eax
f010018a:	0f b6 98 00 40 27 f0 	movzbl -0xfd8c000(%eax),%ebx
f0100191:	e8 74 65 00 00       	call   f010670a <cpunum>
f0100196:	83 ec 04             	sub    $0x4,%esp
f0100199:	53                   	push   %ebx
f010019a:	50                   	push   %eax
f010019b:	68 1c 70 10 f0       	push   $0xf010701c
f01001a0:	e8 53 3a 00 00       	call   f0103bf8 <cprintf>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01001a5:	83 c4 10             	add    $0x10,%esp
f01001a8:	83 3d 24 34 27 f0 07 	cmpl   $0x7,0xf0273424
f01001af:	77 16                	ja     f01001c7 <i386_init+0x121>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01001b1:	68 00 70 00 00       	push   $0x7000
f01001b6:	68 68 6f 10 f0       	push   $0xf0106f68
f01001bb:	6a 7f                	push   $0x7f
f01001bd:	68 e6 6f 10 f0       	push   $0xf0106fe6
f01001c2:	e8 85 fe ff ff       	call   f010004c <_panic>

	// Write entry code to unused memory at MPENTRY_PADDR
	code = KADDR(MPENTRY_PADDR);
	memmove(code, mpentry_start, mpentry_end - mpentry_start);
f01001c7:	83 ec 04             	sub    $0x4,%esp
f01001ca:	b8 86 62 10 f0       	mov    $0xf0106286,%eax
f01001cf:	2d 04 62 10 f0       	sub    $0xf0106204,%eax
f01001d4:	50                   	push   %eax
f01001d5:	68 04 62 10 f0       	push   $0xf0106204
f01001da:	68 00 70 00 f0       	push   $0xf0007000
f01001df:	e8 6d 5e 00 00       	call   f0106051 <memmove>
f01001e4:	83 c4 10             	add    $0x10,%esp
f01001e7:	be 74 00 00 00       	mov    $0x74,%esi
f01001ec:	eb 52                	jmp    f0100240 <i386_init+0x19a>

	// Boot each AP one at a time
	for (c = cpus + 1; c < cpus + ncpu; c++) {
		// Tell mpentry.S what stack to use 
		mpentry_kstack = percpu_kstacks[c - cpus] + KSTKSIZE;
f01001ee:	89 f0                	mov    %esi,%eax
f01001f0:	c1 f8 02             	sar    $0x2,%eax
f01001f3:	8d 14 80             	lea    (%eax,%eax,4),%edx
f01001f6:	8d 0c d0             	lea    (%eax,%edx,8),%ecx
f01001f9:	89 ca                	mov    %ecx,%edx
f01001fb:	c1 e2 05             	shl    $0x5,%edx
f01001fe:	29 ca                	sub    %ecx,%edx
f0100200:	8d 0c 90             	lea    (%eax,%edx,4),%ecx
f0100203:	89 ca                	mov    %ecx,%edx
f0100205:	c1 e2 0e             	shl    $0xe,%edx
f0100208:	29 ca                	sub    %ecx,%edx
f010020a:	8d 14 90             	lea    (%eax,%edx,4),%edx
f010020d:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0100210:	c1 e0 0f             	shl    $0xf,%eax
f0100213:	05 00 d0 27 f0       	add    $0xf027d000,%eax
f0100218:	a3 14 2f 27 f0       	mov    %eax,0xf0272f14
		// Start the CPU at mpentry_start
		lapic_startap(c->cpu_apicid, PADDR(code));
f010021d:	83 ec 08             	sub    $0x8,%esp
f0100220:	68 00 70 00 00       	push   $0x7000
f0100225:	0f b6 86 00 40 27 f0 	movzbl -0xfd8c000(%esi),%eax
f010022c:	50                   	push   %eax
f010022d:	e8 c9 66 00 00       	call   f01068fb <lapic_startap>
f0100232:	83 c4 10             	add    $0x10,%esp
		// Wait for the CPU to finish some basic setup in mp_main()
		while(c->cpu_status != CPU_STARTED)
f0100235:	8b 43 04             	mov    0x4(%ebx),%eax
f0100238:	83 f8 01             	cmp    $0x1,%eax
f010023b:	75 f8                	jne    f0100235 <i386_init+0x18f>
f010023d:	83 c6 74             	add    $0x74,%esi
f0100240:	8d 9e 00 40 27 f0    	lea    -0xfd8c000(%esi),%ebx
	// Write entry code to unused memory at MPENTRY_PADDR
	code = KADDR(MPENTRY_PADDR);
	memmove(code, mpentry_start, mpentry_end - mpentry_start);

	// Boot each AP one at a time
	for (c = cpus + 1; c < cpus + ncpu; c++) {
f0100246:	8b 15 a0 43 27 f0    	mov    0xf02743a0,%edx
f010024c:	8d 04 12             	lea    (%edx,%edx,1),%eax
f010024f:	01 d0                	add    %edx,%eax
f0100251:	01 c0                	add    %eax,%eax
f0100253:	01 d0                	add    %edx,%eax
f0100255:	8d 04 82             	lea    (%edx,%eax,4),%eax
f0100258:	8d 04 85 00 40 27 f0 	lea    -0xfd8c000(,%eax,4),%eax
f010025f:	39 c3                	cmp    %eax,%ebx
f0100261:	72 8b                	jb     f01001ee <i386_init+0x148>
#if defined(TEST)
	// Don't touch -- used by grading script!
	//cprintf("H3\n");
	//cprintf("Before ENV_CREATE(user_dumbfork)\n");
	//ENV_CREATE(user_dumbfork ,ENV_TYPE_USER);
	ENV_CREATE(TEST, ENV_TYPE_USER);
f0100263:	83 ec 08             	sub    $0x8,%esp
f0100266:	6a 00                	push   $0x0
f0100268:	68 a0 5b 26 f0       	push   $0xf0265ba0
f010026d:	e8 82 33 00 00       	call   f01035f4 <env_create>
	//ENV_CREATE(user_dumbfork ,ENV_TYPE_USER);
	//ENV_CREATE(user_primes, ENV_TYPE_USER);
	//ENV_CREATE(user_dumbfork ,ENV_TYPE_USER);
#endif // TEST*
	// Schedule and run the first user environment!
	sched_yield();
f0100272:	e8 74 48 00 00       	call   f0104aeb <sched_yield>

f0100277 <mp_main>:
}

// Setup code for APs
void
mp_main(void)
{
f0100277:	55                   	push   %ebp
f0100278:	89 e5                	mov    %esp,%ebp
f010027a:	53                   	push   %ebx
f010027b:	83 ec 04             	sub    $0x4,%esp
	// We are in high EIP now, safe to switch to kern_pgdir 
	lcr3(PADDR(kern_pgdir));
f010027e:	a1 28 34 27 f0       	mov    0xf0273428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0100283:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0100288:	77 15                	ja     f010029f <mp_main+0x28>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010028a:	50                   	push   %eax
f010028b:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0100290:	68 93 00 00 00       	push   $0x93
f0100295:	68 e6 6f 10 f0       	push   $0xf0106fe6
f010029a:	e8 ad fd ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f010029f:	05 00 00 00 10       	add    $0x10000000,%eax
f01002a4:	0f 22 d8             	mov    %eax,%cr3
	cprintf("  AP #%d [apicid %02x] starting\n", cpunum(), thiscpu->cpu_apicid);
f01002a7:	e8 5e 64 00 00       	call   f010670a <cpunum>
f01002ac:	6b c0 74             	imul   $0x74,%eax,%eax
f01002af:	0f b6 98 00 40 27 f0 	movzbl -0xfd8c000(%eax),%ebx
f01002b6:	e8 4f 64 00 00       	call   f010670a <cpunum>
f01002bb:	83 ec 04             	sub    $0x4,%esp
f01002be:	53                   	push   %ebx
f01002bf:	50                   	push   %eax
f01002c0:	68 b0 6f 10 f0       	push   $0xf0106fb0
f01002c5:	e8 2e 39 00 00       	call   f0103bf8 <cprintf>

	lapic_init();
f01002ca:	e8 96 64 00 00       	call   f0106765 <lapic_init>
	env_init_percpu();
f01002cf:	e8 ec 30 00 00       	call   f01033c0 <env_init_percpu>
	trap_init_percpu();
f01002d4:	e8 33 39 00 00       	call   f0103c0c <trap_init_percpu>
	xchg(&thiscpu->cpu_status, CPU_STARTED); // tell boot_aps() we're up
f01002d9:	e8 2c 64 00 00       	call   f010670a <cpunum>
f01002de:	6b d0 74             	imul   $0x74,%eax,%edx
f01002e1:	81 c2 00 40 27 f0    	add    $0xf0274000,%edx
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f01002e7:	b8 01 00 00 00       	mov    $0x1,%eax
f01002ec:	f0 87 42 04          	lock xchg %eax,0x4(%edx)
f01002f0:	c7 04 24 c0 33 12 f0 	movl   $0xf01233c0,(%esp)
f01002f7:	e8 cc 67 00 00       	call   f0106ac8 <spin_lock>
	// to start running processes on this CPU.  But make sure that
	// only one CPU can enter the scheduler at a time!
	//
	// Your code here:
	lock_kernel();
    sched_yield();
f01002fc:	e8 ea 47 00 00       	call   f0104aeb <sched_yield>

f0100301 <_warn>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
f0100301:	55                   	push   %ebp
f0100302:	89 e5                	mov    %esp,%ebp
f0100304:	53                   	push   %ebx
f0100305:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f0100308:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f010030b:	ff 75 0c             	pushl  0xc(%ebp)
f010030e:	ff 75 08             	pushl  0x8(%ebp)
f0100311:	68 38 70 10 f0       	push   $0xf0107038
f0100316:	e8 dd 38 00 00       	call   f0103bf8 <cprintf>
	vcprintf(fmt, ap);
f010031b:	83 c4 08             	add    $0x8,%esp
f010031e:	53                   	push   %ebx
f010031f:	ff 75 10             	pushl  0x10(%ebp)
f0100322:	e8 ab 38 00 00       	call   f0103bd2 <vcprintf>
	cprintf("\n");
f0100327:	c7 04 24 32 81 10 f0 	movl   $0xf0108132,(%esp)
f010032e:	e8 c5 38 00 00       	call   f0103bf8 <cprintf>
	va_end(ap);
}
f0100333:	83 c4 10             	add    $0x10,%esp
f0100336:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100339:	c9                   	leave  
f010033a:	c3                   	ret    

f010033b <serial_proc_data>:

static bool serial_exists;

static int
serial_proc_data(void)
{
f010033b:	55                   	push   %ebp
f010033c:	89 e5                	mov    %esp,%ebp

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010033e:	ba fd 03 00 00       	mov    $0x3fd,%edx
f0100343:	ec                   	in     (%dx),%al
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f0100344:	a8 01                	test   $0x1,%al
f0100346:	74 0b                	je     f0100353 <serial_proc_data+0x18>
f0100348:	ba f8 03 00 00       	mov    $0x3f8,%edx
f010034d:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f010034e:	0f b6 c0             	movzbl %al,%eax
f0100351:	eb 05                	jmp    f0100358 <serial_proc_data+0x1d>

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f0100353:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return inb(COM1+COM_RX);
}
f0100358:	5d                   	pop    %ebp
f0100359:	c3                   	ret    

f010035a <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f010035a:	55                   	push   %ebp
f010035b:	89 e5                	mov    %esp,%ebp
f010035d:	53                   	push   %ebx
f010035e:	83 ec 04             	sub    $0x4,%esp
f0100361:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f0100363:	eb 2b                	jmp    f0100390 <cons_intr+0x36>
		if (c == 0)
f0100365:	85 c0                	test   %eax,%eax
f0100367:	74 27                	je     f0100390 <cons_intr+0x36>
			continue;
		cons.buf[cons.wpos++] = c;
f0100369:	8b 0d 24 22 27 f0    	mov    0xf0272224,%ecx
f010036f:	8d 51 01             	lea    0x1(%ecx),%edx
f0100372:	89 15 24 22 27 f0    	mov    %edx,0xf0272224
f0100378:	88 81 20 20 27 f0    	mov    %al,-0xfd8dfe0(%ecx)
		if (cons.wpos == CONSBUFSIZE)
f010037e:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f0100384:	75 0a                	jne    f0100390 <cons_intr+0x36>
			cons.wpos = 0;
f0100386:	c7 05 24 22 27 f0 00 	movl   $0x0,0xf0272224
f010038d:	00 00 00 
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f0100390:	ff d3                	call   *%ebx
f0100392:	83 f8 ff             	cmp    $0xffffffff,%eax
f0100395:	75 ce                	jne    f0100365 <cons_intr+0xb>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f0100397:	83 c4 04             	add    $0x4,%esp
f010039a:	5b                   	pop    %ebx
f010039b:	5d                   	pop    %ebp
f010039c:	c3                   	ret    

f010039d <kbd_proc_data>:
f010039d:	ba 64 00 00 00       	mov    $0x64,%edx
f01003a2:	ec                   	in     (%dx),%al
	int c;
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
f01003a3:	a8 01                	test   $0x1,%al
f01003a5:	0f 84 ee 00 00 00    	je     f0100499 <kbd_proc_data+0xfc>
		return -1;
	// Ignore data from mouse.
	if (stat & KBS_TERR)
f01003ab:	a8 20                	test   $0x20,%al
f01003ad:	0f 85 ec 00 00 00    	jne    f010049f <kbd_proc_data+0x102>
f01003b3:	ba 60 00 00 00       	mov    $0x60,%edx
f01003b8:	ec                   	in     (%dx),%al
f01003b9:	88 c2                	mov    %al,%dl
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f01003bb:	3c e0                	cmp    $0xe0,%al
f01003bd:	75 0d                	jne    f01003cc <kbd_proc_data+0x2f>
		// E0 escape character
		shift |= E0ESC;
f01003bf:	83 0d 00 20 27 f0 40 	orl    $0x40,0xf0272000
		return 0;
f01003c6:	b8 00 00 00 00       	mov    $0x0,%eax
f01003cb:	c3                   	ret    
	} else if (data & 0x80) {
f01003cc:	84 c0                	test   %al,%al
f01003ce:	79 2e                	jns    f01003fe <kbd_proc_data+0x61>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f01003d0:	8b 0d 00 20 27 f0    	mov    0xf0272000,%ecx
f01003d6:	f6 c1 40             	test   $0x40,%cl
f01003d9:	75 05                	jne    f01003e0 <kbd_proc_data+0x43>
f01003db:	83 e0 7f             	and    $0x7f,%eax
f01003de:	88 c2                	mov    %al,%dl
		shift &= ~(shiftcode[data] | E0ESC);
f01003e0:	0f b6 c2             	movzbl %dl,%eax
f01003e3:	8a 80 a0 71 10 f0    	mov    -0xfef8e60(%eax),%al
f01003e9:	83 c8 40             	or     $0x40,%eax
f01003ec:	0f b6 c0             	movzbl %al,%eax
f01003ef:	f7 d0                	not    %eax
f01003f1:	21 c8                	and    %ecx,%eax
f01003f3:	a3 00 20 27 f0       	mov    %eax,0xf0272000
		return 0;
f01003f8:	b8 00 00 00 00       	mov    $0x0,%eax
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f01003fd:	c3                   	ret    
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f01003fe:	55                   	push   %ebp
f01003ff:	89 e5                	mov    %esp,%ebp
f0100401:	53                   	push   %ebx
f0100402:	83 ec 04             	sub    $0x4,%esp
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
f0100405:	8b 0d 00 20 27 f0    	mov    0xf0272000,%ecx
f010040b:	f6 c1 40             	test   $0x40,%cl
f010040e:	74 0e                	je     f010041e <kbd_proc_data+0x81>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f0100410:	83 c8 80             	or     $0xffffff80,%eax
f0100413:	88 c2                	mov    %al,%dl
		shift &= ~E0ESC;
f0100415:	83 e1 bf             	and    $0xffffffbf,%ecx
f0100418:	89 0d 00 20 27 f0    	mov    %ecx,0xf0272000
	}

	shift |= shiftcode[data];
f010041e:	0f b6 c2             	movzbl %dl,%eax
	shift ^= togglecode[data];
f0100421:	0f b6 90 a0 71 10 f0 	movzbl -0xfef8e60(%eax),%edx
f0100428:	0b 15 00 20 27 f0    	or     0xf0272000,%edx
f010042e:	0f b6 88 a0 70 10 f0 	movzbl -0xfef8f60(%eax),%ecx
f0100435:	31 ca                	xor    %ecx,%edx
f0100437:	89 15 00 20 27 f0    	mov    %edx,0xf0272000

	c = charcode[shift & (CTL | SHIFT)][data];
f010043d:	89 d1                	mov    %edx,%ecx
f010043f:	83 e1 03             	and    $0x3,%ecx
f0100442:	8b 0c 8d 80 70 10 f0 	mov    -0xfef8f80(,%ecx,4),%ecx
f0100449:	8a 04 01             	mov    (%ecx,%eax,1),%al
f010044c:	0f b6 d8             	movzbl %al,%ebx
	if (shift & CAPSLOCK) {
f010044f:	f6 c2 08             	test   $0x8,%dl
f0100452:	74 1a                	je     f010046e <kbd_proc_data+0xd1>
		if ('a' <= c && c <= 'z')
f0100454:	89 d8                	mov    %ebx,%eax
f0100456:	8d 4b 9f             	lea    -0x61(%ebx),%ecx
f0100459:	83 f9 19             	cmp    $0x19,%ecx
f010045c:	77 05                	ja     f0100463 <kbd_proc_data+0xc6>
			c += 'A' - 'a';
f010045e:	83 eb 20             	sub    $0x20,%ebx
f0100461:	eb 0b                	jmp    f010046e <kbd_proc_data+0xd1>
		else if ('A' <= c && c <= 'Z')
f0100463:	83 e8 41             	sub    $0x41,%eax
f0100466:	83 f8 19             	cmp    $0x19,%eax
f0100469:	77 03                	ja     f010046e <kbd_proc_data+0xd1>
			c += 'a' - 'A';
f010046b:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f010046e:	f7 d2                	not    %edx
f0100470:	f6 c2 06             	test   $0x6,%dl
f0100473:	75 30                	jne    f01004a5 <kbd_proc_data+0x108>
f0100475:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f010047b:	75 2c                	jne    f01004a9 <kbd_proc_data+0x10c>
		cprintf("Rebooting!\n");
f010047d:	83 ec 0c             	sub    $0xc,%esp
f0100480:	68 52 70 10 f0       	push   $0xf0107052
f0100485:	e8 6e 37 00 00       	call   f0103bf8 <cprintf>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010048a:	ba 92 00 00 00       	mov    $0x92,%edx
f010048f:	b0 03                	mov    $0x3,%al
f0100491:	ee                   	out    %al,(%dx)
f0100492:	83 c4 10             	add    $0x10,%esp
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f0100495:	89 d8                	mov    %ebx,%eax
f0100497:	eb 12                	jmp    f01004ab <kbd_proc_data+0x10e>
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
		return -1;
f0100499:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f010049e:	c3                   	ret    
	// Ignore data from mouse.
	if (stat & KBS_TERR)
		return -1;
f010049f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01004a4:	c3                   	ret    
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01004a5:	89 d8                	mov    %ebx,%eax
f01004a7:	eb 02                	jmp    f01004ab <kbd_proc_data+0x10e>
f01004a9:	89 d8                	mov    %ebx,%eax
}
f01004ab:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01004ae:	c9                   	leave  
f01004af:	c3                   	ret    

f01004b0 <cons_putc>:
}

// output a character to the console
static void
cons_putc(int c)
{
f01004b0:	55                   	push   %ebp
f01004b1:	89 e5                	mov    %esp,%ebp
f01004b3:	57                   	push   %edi
f01004b4:	56                   	push   %esi
f01004b5:	53                   	push   %ebx
f01004b6:	83 ec 1c             	sub    $0x1c,%esp
f01004b9:	89 c7                	mov    %eax,%edi
f01004bb:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01004c0:	be fd 03 00 00       	mov    $0x3fd,%esi
f01004c5:	b9 84 00 00 00       	mov    $0x84,%ecx
f01004ca:	eb 06                	jmp    f01004d2 <cons_putc+0x22>
f01004cc:	89 ca                	mov    %ecx,%edx
f01004ce:	ec                   	in     (%dx),%al
f01004cf:	ec                   	in     (%dx),%al
f01004d0:	ec                   	in     (%dx),%al
f01004d1:	ec                   	in     (%dx),%al
f01004d2:	89 f2                	mov    %esi,%edx
f01004d4:	ec                   	in     (%dx),%al
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f01004d5:	a8 20                	test   $0x20,%al
f01004d7:	75 03                	jne    f01004dc <cons_putc+0x2c>
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
f01004d9:	4b                   	dec    %ebx
f01004da:	75 f0                	jne    f01004cc <cons_putc+0x1c>
f01004dc:	89 f8                	mov    %edi,%eax
f01004de:	88 45 e7             	mov    %al,-0x19(%ebp)
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01004e1:	ba f8 03 00 00       	mov    $0x3f8,%edx
f01004e6:	ee                   	out    %al,(%dx)
f01004e7:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01004ec:	be 79 03 00 00       	mov    $0x379,%esi
f01004f1:	b9 84 00 00 00       	mov    $0x84,%ecx
f01004f6:	eb 06                	jmp    f01004fe <cons_putc+0x4e>
f01004f8:	89 ca                	mov    %ecx,%edx
f01004fa:	ec                   	in     (%dx),%al
f01004fb:	ec                   	in     (%dx),%al
f01004fc:	ec                   	in     (%dx),%al
f01004fd:	ec                   	in     (%dx),%al
f01004fe:	89 f2                	mov    %esi,%edx
f0100500:	ec                   	in     (%dx),%al
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f0100501:	84 c0                	test   %al,%al
f0100503:	78 03                	js     f0100508 <cons_putc+0x58>
f0100505:	4b                   	dec    %ebx
f0100506:	75 f0                	jne    f01004f8 <cons_putc+0x48>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100508:	ba 78 03 00 00       	mov    $0x378,%edx
f010050d:	8a 45 e7             	mov    -0x19(%ebp),%al
f0100510:	ee                   	out    %al,(%dx)
f0100511:	ba 7a 03 00 00       	mov    $0x37a,%edx
f0100516:	b0 0d                	mov    $0xd,%al
f0100518:	ee                   	out    %al,(%dx)
f0100519:	b0 08                	mov    $0x8,%al
f010051b:	ee                   	out    %al,(%dx)

static void
cga_putc(int c)
{
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f010051c:	f7 c7 00 ff ff ff    	test   $0xffffff00,%edi
f0100522:	75 06                	jne    f010052a <cons_putc+0x7a>
		c |= 0x0500;
f0100524:	81 cf 00 05 00 00    	or     $0x500,%edi

	switch (c & 0xff) {
f010052a:	89 f8                	mov    %edi,%eax
f010052c:	0f b6 c0             	movzbl %al,%eax
f010052f:	83 f8 09             	cmp    $0x9,%eax
f0100532:	74 75                	je     f01005a9 <cons_putc+0xf9>
f0100534:	83 f8 09             	cmp    $0x9,%eax
f0100537:	7f 0a                	jg     f0100543 <cons_putc+0x93>
f0100539:	83 f8 08             	cmp    $0x8,%eax
f010053c:	74 14                	je     f0100552 <cons_putc+0xa2>
f010053e:	e9 9a 00 00 00       	jmp    f01005dd <cons_putc+0x12d>
f0100543:	83 f8 0a             	cmp    $0xa,%eax
f0100546:	74 38                	je     f0100580 <cons_putc+0xd0>
f0100548:	83 f8 0d             	cmp    $0xd,%eax
f010054b:	74 3b                	je     f0100588 <cons_putc+0xd8>
f010054d:	e9 8b 00 00 00       	jmp    f01005dd <cons_putc+0x12d>
	case '\b':
		if (crt_pos > 0) {
f0100552:	66 a1 28 22 27 f0    	mov    0xf0272228,%ax
f0100558:	66 85 c0             	test   %ax,%ax
f010055b:	0f 84 e7 00 00 00    	je     f0100648 <cons_putc+0x198>
			crt_pos--;
f0100561:	48                   	dec    %eax
f0100562:	66 a3 28 22 27 f0    	mov    %ax,0xf0272228
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f0100568:	0f b7 c0             	movzwl %ax,%eax
f010056b:	81 e7 00 ff ff ff    	and    $0xffffff00,%edi
f0100571:	83 cf 20             	or     $0x20,%edi
f0100574:	8b 15 2c 22 27 f0    	mov    0xf027222c,%edx
f010057a:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
f010057e:	eb 7a                	jmp    f01005fa <cons_putc+0x14a>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f0100580:	66 83 05 28 22 27 f0 	addw   $0x50,0xf0272228
f0100587:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f0100588:	66 8b 0d 28 22 27 f0 	mov    0xf0272228,%cx
f010058f:	bb 50 00 00 00       	mov    $0x50,%ebx
f0100594:	89 c8                	mov    %ecx,%eax
f0100596:	ba 00 00 00 00       	mov    $0x0,%edx
f010059b:	66 f7 f3             	div    %bx
f010059e:	29 d1                	sub    %edx,%ecx
f01005a0:	66 89 0d 28 22 27 f0 	mov    %cx,0xf0272228
f01005a7:	eb 51                	jmp    f01005fa <cons_putc+0x14a>
		break;
	case '\t':
		cons_putc(' ');
f01005a9:	b8 20 00 00 00       	mov    $0x20,%eax
f01005ae:	e8 fd fe ff ff       	call   f01004b0 <cons_putc>
		cons_putc(' ');
f01005b3:	b8 20 00 00 00       	mov    $0x20,%eax
f01005b8:	e8 f3 fe ff ff       	call   f01004b0 <cons_putc>
		cons_putc(' ');
f01005bd:	b8 20 00 00 00       	mov    $0x20,%eax
f01005c2:	e8 e9 fe ff ff       	call   f01004b0 <cons_putc>
		cons_putc(' ');
f01005c7:	b8 20 00 00 00       	mov    $0x20,%eax
f01005cc:	e8 df fe ff ff       	call   f01004b0 <cons_putc>
		cons_putc(' ');
f01005d1:	b8 20 00 00 00       	mov    $0x20,%eax
f01005d6:	e8 d5 fe ff ff       	call   f01004b0 <cons_putc>
f01005db:	eb 1d                	jmp    f01005fa <cons_putc+0x14a>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f01005dd:	66 a1 28 22 27 f0    	mov    0xf0272228,%ax
f01005e3:	8d 50 01             	lea    0x1(%eax),%edx
f01005e6:	66 89 15 28 22 27 f0 	mov    %dx,0xf0272228
f01005ed:	0f b7 c0             	movzwl %ax,%eax
f01005f0:	8b 15 2c 22 27 f0    	mov    0xf027222c,%edx
f01005f6:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f01005fa:	66 81 3d 28 22 27 f0 	cmpw   $0x7cf,0xf0272228
f0100601:	cf 07 
f0100603:	76 43                	jbe    f0100648 <cons_putc+0x198>
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f0100605:	a1 2c 22 27 f0       	mov    0xf027222c,%eax
f010060a:	83 ec 04             	sub    $0x4,%esp
f010060d:	68 00 0f 00 00       	push   $0xf00
f0100612:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f0100618:	52                   	push   %edx
f0100619:	50                   	push   %eax
f010061a:	e8 32 5a 00 00       	call   f0106051 <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f010061f:	8b 15 2c 22 27 f0    	mov    0xf027222c,%edx
f0100625:	8d 82 00 0f 00 00    	lea    0xf00(%edx),%eax
f010062b:	81 c2 a0 0f 00 00    	add    $0xfa0,%edx
f0100631:	83 c4 10             	add    $0x10,%esp
f0100634:	66 c7 00 20 07       	movw   $0x720,(%eax)
f0100639:	83 c0 02             	add    $0x2,%eax
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f010063c:	39 d0                	cmp    %edx,%eax
f010063e:	75 f4                	jne    f0100634 <cons_putc+0x184>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f0100640:	66 83 2d 28 22 27 f0 	subw   $0x50,0xf0272228
f0100647:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f0100648:	8b 0d 30 22 27 f0    	mov    0xf0272230,%ecx
f010064e:	b0 0e                	mov    $0xe,%al
f0100650:	89 ca                	mov    %ecx,%edx
f0100652:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f0100653:	8d 59 01             	lea    0x1(%ecx),%ebx
f0100656:	66 a1 28 22 27 f0    	mov    0xf0272228,%ax
f010065c:	66 c1 e8 08          	shr    $0x8,%ax
f0100660:	89 da                	mov    %ebx,%edx
f0100662:	ee                   	out    %al,(%dx)
f0100663:	b0 0f                	mov    $0xf,%al
f0100665:	89 ca                	mov    %ecx,%edx
f0100667:	ee                   	out    %al,(%dx)
f0100668:	a0 28 22 27 f0       	mov    0xf0272228,%al
f010066d:	89 da                	mov    %ebx,%edx
f010066f:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f0100670:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100673:	5b                   	pop    %ebx
f0100674:	5e                   	pop    %esi
f0100675:	5f                   	pop    %edi
f0100676:	5d                   	pop    %ebp
f0100677:	c3                   	ret    

f0100678 <serial_intr>:
}

void
serial_intr(void)
{
	if (serial_exists)
f0100678:	80 3d 34 22 27 f0 00 	cmpb   $0x0,0xf0272234
f010067f:	74 11                	je     f0100692 <serial_intr+0x1a>
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f0100681:	55                   	push   %ebp
f0100682:	89 e5                	mov    %esp,%ebp
f0100684:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
		cons_intr(serial_proc_data);
f0100687:	b8 3b 03 10 f0       	mov    $0xf010033b,%eax
f010068c:	e8 c9 fc ff ff       	call   f010035a <cons_intr>
}
f0100691:	c9                   	leave  
f0100692:	c3                   	ret    

f0100693 <kbd_intr>:
	return c;
}

void
kbd_intr(void)
{
f0100693:	55                   	push   %ebp
f0100694:	89 e5                	mov    %esp,%ebp
f0100696:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f0100699:	b8 9d 03 10 f0       	mov    $0xf010039d,%eax
f010069e:	e8 b7 fc ff ff       	call   f010035a <cons_intr>
}
f01006a3:	c9                   	leave  
f01006a4:	c3                   	ret    

f01006a5 <cons_getc>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f01006a5:	55                   	push   %ebp
f01006a6:	89 e5                	mov    %esp,%ebp
f01006a8:	83 ec 08             	sub    $0x8,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f01006ab:	e8 c8 ff ff ff       	call   f0100678 <serial_intr>
	kbd_intr();
f01006b0:	e8 de ff ff ff       	call   f0100693 <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f01006b5:	a1 20 22 27 f0       	mov    0xf0272220,%eax
f01006ba:	3b 05 24 22 27 f0    	cmp    0xf0272224,%eax
f01006c0:	74 24                	je     f01006e6 <cons_getc+0x41>
		c = cons.buf[cons.rpos++];
f01006c2:	8d 50 01             	lea    0x1(%eax),%edx
f01006c5:	89 15 20 22 27 f0    	mov    %edx,0xf0272220
f01006cb:	0f b6 80 20 20 27 f0 	movzbl -0xfd8dfe0(%eax),%eax
		if (cons.rpos == CONSBUFSIZE)
f01006d2:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f01006d8:	75 11                	jne    f01006eb <cons_getc+0x46>
			cons.rpos = 0;
f01006da:	c7 05 20 22 27 f0 00 	movl   $0x0,0xf0272220
f01006e1:	00 00 00 
f01006e4:	eb 05                	jmp    f01006eb <cons_getc+0x46>
		return c;
	}
	return 0;
f01006e6:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01006eb:	c9                   	leave  
f01006ec:	c3                   	ret    

f01006ed <cons_init>:
}

// initialize the console devices
void
cons_init(void)
{
f01006ed:	55                   	push   %ebp
f01006ee:	89 e5                	mov    %esp,%ebp
f01006f0:	56                   	push   %esi
f01006f1:	53                   	push   %ebx
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f01006f2:	66 8b 15 00 80 0b f0 	mov    0xf00b8000,%dx
	*cp = (uint16_t) 0xA55A;
f01006f9:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f0100700:	5a a5 
	if (*cp != 0xA55A) {
f0100702:	66 a1 00 80 0b f0    	mov    0xf00b8000,%ax
f0100708:	66 3d 5a a5          	cmp    $0xa55a,%ax
f010070c:	74 11                	je     f010071f <cons_init+0x32>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f010070e:	c7 05 30 22 27 f0 b4 	movl   $0x3b4,0xf0272230
f0100715:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f0100718:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f010071d:	eb 16                	jmp    f0100735 <cons_init+0x48>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f010071f:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f0100726:	c7 05 30 22 27 f0 d4 	movl   $0x3d4,0xf0272230
f010072d:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f0100730:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
f0100735:	b0 0e                	mov    $0xe,%al
f0100737:	8b 15 30 22 27 f0    	mov    0xf0272230,%edx
f010073d:	ee                   	out    %al,(%dx)
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
f010073e:	8d 5a 01             	lea    0x1(%edx),%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100741:	89 da                	mov    %ebx,%edx
f0100743:	ec                   	in     (%dx),%al
f0100744:	0f b6 c8             	movzbl %al,%ecx
f0100747:	c1 e1 08             	shl    $0x8,%ecx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010074a:	b0 0f                	mov    $0xf,%al
f010074c:	8b 15 30 22 27 f0    	mov    0xf0272230,%edx
f0100752:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100753:	89 da                	mov    %ebx,%edx
f0100755:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f0100756:	89 35 2c 22 27 f0    	mov    %esi,0xf027222c
	crt_pos = pos;
f010075c:	0f b6 c0             	movzbl %al,%eax
f010075f:	09 c8                	or     %ecx,%eax
f0100761:	66 a3 28 22 27 f0    	mov    %ax,0xf0272228

static void
kbd_init(void)
{
	// Drain the kbd buffer so that QEMU generates interrupts.
	kbd_intr();
f0100767:	e8 27 ff ff ff       	call   f0100693 <kbd_intr>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010076c:	be fa 03 00 00       	mov    $0x3fa,%esi
f0100771:	b0 00                	mov    $0x0,%al
f0100773:	89 f2                	mov    %esi,%edx
f0100775:	ee                   	out    %al,(%dx)
f0100776:	ba fb 03 00 00       	mov    $0x3fb,%edx
f010077b:	b0 80                	mov    $0x80,%al
f010077d:	ee                   	out    %al,(%dx)
f010077e:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f0100783:	b0 0c                	mov    $0xc,%al
f0100785:	89 da                	mov    %ebx,%edx
f0100787:	ee                   	out    %al,(%dx)
f0100788:	ba f9 03 00 00       	mov    $0x3f9,%edx
f010078d:	b0 00                	mov    $0x0,%al
f010078f:	ee                   	out    %al,(%dx)
f0100790:	ba fb 03 00 00       	mov    $0x3fb,%edx
f0100795:	b0 03                	mov    $0x3,%al
f0100797:	ee                   	out    %al,(%dx)
f0100798:	ba fc 03 00 00       	mov    $0x3fc,%edx
f010079d:	b0 00                	mov    $0x0,%al
f010079f:	ee                   	out    %al,(%dx)
f01007a0:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01007a5:	b0 01                	mov    $0x1,%al
f01007a7:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01007a8:	ba fd 03 00 00       	mov    $0x3fd,%edx
f01007ad:	ec                   	in     (%dx),%al
f01007ae:	88 c1                	mov    %al,%cl
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f01007b0:	3c ff                	cmp    $0xff,%al
f01007b2:	0f 95 05 34 22 27 f0 	setne  0xf0272234
f01007b9:	89 f2                	mov    %esi,%edx
f01007bb:	ec                   	in     (%dx),%al
f01007bc:	89 da                	mov    %ebx,%edx
f01007be:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f01007bf:	80 f9 ff             	cmp    $0xff,%cl
f01007c2:	75 10                	jne    f01007d4 <cons_init+0xe7>
		cprintf("Serial port does not exist!\n");
f01007c4:	83 ec 0c             	sub    $0xc,%esp
f01007c7:	68 5e 70 10 f0       	push   $0xf010705e
f01007cc:	e8 27 34 00 00       	call   f0103bf8 <cprintf>
f01007d1:	83 c4 10             	add    $0x10,%esp
}
f01007d4:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01007d7:	5b                   	pop    %ebx
f01007d8:	5e                   	pop    %esi
f01007d9:	5d                   	pop    %ebp
f01007da:	c3                   	ret    

f01007db <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f01007db:	55                   	push   %ebp
f01007dc:	89 e5                	mov    %esp,%ebp
f01007de:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f01007e1:	8b 45 08             	mov    0x8(%ebp),%eax
f01007e4:	e8 c7 fc ff ff       	call   f01004b0 <cons_putc>
}
f01007e9:	c9                   	leave  
f01007ea:	c3                   	ret    

f01007eb <getchar>:

int
getchar(void)
{
f01007eb:	55                   	push   %ebp
f01007ec:	89 e5                	mov    %esp,%ebp
f01007ee:	83 ec 08             	sub    $0x8,%esp
	int c;

	while ((c = cons_getc()) == 0)
f01007f1:	e8 af fe ff ff       	call   f01006a5 <cons_getc>
f01007f6:	85 c0                	test   %eax,%eax
f01007f8:	74 f7                	je     f01007f1 <getchar+0x6>
		/* do nothing */;
	return c;
}
f01007fa:	c9                   	leave  
f01007fb:	c3                   	ret    

f01007fc <iscons>:

int
iscons(int fdnum)
{
f01007fc:	55                   	push   %ebp
f01007fd:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
}
f01007ff:	b8 01 00 00 00       	mov    $0x1,%eax
f0100804:	5d                   	pop    %ebp
f0100805:	c3                   	ret    

f0100806 <mon_help>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f0100806:	55                   	push   %ebp
f0100807:	89 e5                	mov    %esp,%ebp
f0100809:	83 ec 0c             	sub    $0xc,%esp
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f010080c:	68 a0 72 10 f0       	push   $0xf01072a0
f0100811:	68 be 72 10 f0       	push   $0xf01072be
f0100816:	68 c3 72 10 f0       	push   $0xf01072c3
f010081b:	e8 d8 33 00 00       	call   f0103bf8 <cprintf>
f0100820:	83 c4 0c             	add    $0xc,%esp
f0100823:	68 84 73 10 f0       	push   $0xf0107384
f0100828:	68 cc 72 10 f0       	push   $0xf01072cc
f010082d:	68 c3 72 10 f0       	push   $0xf01072c3
f0100832:	e8 c1 33 00 00       	call   f0103bf8 <cprintf>
f0100837:	83 c4 0c             	add    $0xc,%esp
f010083a:	68 33 81 10 f0       	push   $0xf0108133
f010083f:	68 d5 72 10 f0       	push   $0xf01072d5
f0100844:	68 c3 72 10 f0       	push   $0xf01072c3
f0100849:	e8 aa 33 00 00       	call   f0103bf8 <cprintf>
	return 0;
}
f010084e:	b8 00 00 00 00       	mov    $0x0,%eax
f0100853:	c9                   	leave  
f0100854:	c3                   	ret    

f0100855 <mon_kerninfo>:

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f0100855:	55                   	push   %ebp
f0100856:	89 e5                	mov    %esp,%ebp
f0100858:	83 ec 14             	sub    $0x14,%esp
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f010085b:	68 df 72 10 f0       	push   $0xf01072df
f0100860:	e8 93 33 00 00       	call   f0103bf8 <cprintf>
	cprintf("  _start                  %08x (phys)\n", _start);
f0100865:	83 c4 08             	add    $0x8,%esp
f0100868:	68 0c 00 10 00       	push   $0x10000c
f010086d:	68 ac 73 10 f0       	push   $0xf01073ac
f0100872:	e8 81 33 00 00       	call   f0103bf8 <cprintf>
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
f0100877:	83 c4 0c             	add    $0xc,%esp
f010087a:	68 0c 00 10 00       	push   $0x10000c
f010087f:	68 0c 00 10 f0       	push   $0xf010000c
f0100884:	68 d4 73 10 f0       	push   $0xf01073d4
f0100889:	e8 6a 33 00 00       	call   f0103bf8 <cprintf>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f010088e:	83 c4 0c             	add    $0xc,%esp
f0100891:	68 15 6f 10 00       	push   $0x106f15
f0100896:	68 15 6f 10 f0       	push   $0xf0106f15
f010089b:	68 f8 73 10 f0       	push   $0xf01073f8
f01008a0:	e8 53 33 00 00       	call   f0103bf8 <cprintf>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f01008a5:	83 c4 0c             	add    $0xc,%esp
f01008a8:	68 7c 11 27 00       	push   $0x27117c
f01008ad:	68 7c 11 27 f0       	push   $0xf027117c
f01008b2:	68 1c 74 10 f0       	push   $0xf010741c
f01008b7:	e8 3c 33 00 00       	call   f0103bf8 <cprintf>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f01008bc:	83 c4 0c             	add    $0xc,%esp
f01008bf:	68 08 50 2b 00       	push   $0x2b5008
f01008c4:	68 08 50 2b f0       	push   $0xf02b5008
f01008c9:	68 40 74 10 f0       	push   $0xf0107440
f01008ce:	e8 25 33 00 00       	call   f0103bf8 <cprintf>
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
f01008d3:	b8 07 54 2b f0       	mov    $0xf02b5407,%eax
f01008d8:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
f01008dd:	83 c4 08             	add    $0x8,%esp
f01008e0:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f01008e5:	89 c2                	mov    %eax,%edx
f01008e7:	85 c0                	test   %eax,%eax
f01008e9:	79 06                	jns    f01008f1 <mon_kerninfo+0x9c>
f01008eb:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f01008f1:	c1 fa 0a             	sar    $0xa,%edx
f01008f4:	52                   	push   %edx
f01008f5:	68 64 74 10 f0       	push   $0xf0107464
f01008fa:	e8 f9 32 00 00       	call   f0103bf8 <cprintf>
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
f01008ff:	b8 00 00 00 00       	mov    $0x0,%eax
f0100904:	c9                   	leave  
f0100905:	c3                   	ret    

f0100906 <print_stack_Info>:
ebp f010ff78  eip f01008ae  args 00000001 f010ff8c 00000000 f0110580 00000000
       kern/monitor.c:143: monitor+106
       (filename, line#, function name+offset)
*/
void print_stack_Info(uint32_t esp, uint32_t* return_Addr)
{
f0100906:	55                   	push   %ebp
f0100907:	89 e5                	mov    %esp,%ebp
f0100909:	57                   	push   %edi
f010090a:	56                   	push   %esi
f010090b:	53                   	push   %ebx
f010090c:	83 ec 30             	sub    $0x30,%esp
f010090f:	8b 75 0c             	mov    0xc(%ebp),%esi

	//if(0==debuginfo_eip((uintptr_t)(*return_Addr), info)){

	//}

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
f0100912:	ff 36                	pushl  (%esi)
f0100914:	ff 75 08             	pushl  0x8(%ebp)
f0100917:	68 f8 72 10 f0       	push   $0xf01072f8
f010091c:	e8 d7 32 00 00       	call   f0103bf8 <cprintf>
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
f0100921:	c7 04 24 0b 73 10 f0 	movl   $0xf010730b,(%esp)
f0100928:	e8 cb 32 00 00       	call   f0103bf8 <cprintf>
f010092d:	8d 5e 04             	lea    0x4(%esi),%ebx
f0100930:	8d 7e 18             	lea    0x18(%esi),%edi
f0100933:	83 c4 10             	add    $0x10,%esp
	for(int i=1; i<6; i++){
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
f0100936:	83 ec 08             	sub    $0x8,%esp
f0100939:	ff 33                	pushl  (%ebx)
f010093b:	68 05 73 10 f0       	push   $0xf0107305
f0100940:	e8 b3 32 00 00       	call   f0103bf8 <cprintf>
f0100945:	83 c3 04             	add    $0x4,%ebx

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
	for(int i=1; i<6; i++){
f0100948:	83 c4 10             	add    $0x10,%esp
f010094b:	39 fb                	cmp    %edi,%ebx
f010094d:	75 e7                	jne    f0100936 <print_stack_Info+0x30>
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
	}
	cprintf("\n");
f010094f:	83 ec 0c             	sub    $0xc,%esp
f0100952:	68 32 81 10 f0       	push   $0xf0108132
f0100957:	e8 9c 32 00 00       	call   f0103bf8 <cprintf>

	//print more readable info
	//struct Eipdebuginfo* info_Ptr;
	debuginfo_eip((uintptr_t)(*return_Addr), info_Ptr); //get the info anyway
f010095c:	83 c4 08             	add    $0x8,%esp
f010095f:	8d 45 d0             	lea    -0x30(%ebp),%eax
f0100962:	50                   	push   %eax
f0100963:	ff 36                	pushl  (%esi)
f0100965:	e8 f3 4a 00 00       	call   f010545d <debuginfo_eip>
	//print file name
	cprintf("%s", (info_Ptr->eip_file) );
f010096a:	83 c4 08             	add    $0x8,%esp
f010096d:	ff 75 d0             	pushl  -0x30(%ebp)
f0100970:	68 e3 6f 10 f0       	push   $0xf0106fe3
f0100975:	e8 7e 32 00 00       	call   f0103bf8 <cprintf>
	cprintf(":");
f010097a:	c7 04 24 12 73 10 f0 	movl   $0xf0107312,(%esp)
f0100981:	e8 72 32 00 00       	call   f0103bf8 <cprintf>

	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
f0100986:	83 c4 08             	add    $0x8,%esp
f0100989:	ff 75 d4             	pushl  -0x2c(%ebp)
f010098c:	68 de 90 10 f0       	push   $0xf01090de
f0100991:	e8 62 32 00 00       	call   f0103bf8 <cprintf>
	cprintf(": ");
f0100996:	c7 04 24 4f 70 10 f0 	movl   $0xf010704f,(%esp)
f010099d:	e8 56 32 00 00       	call   f0103bf8 <cprintf>
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01009a2:	83 c4 10             	add    $0x10,%esp
f01009a5:	bb 00 00 00 00       	mov    $0x0,%ebx
f01009aa:	eb 19                	jmp    f01009c5 <print_stack_Info+0xbf>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
f01009ac:	83 ec 08             	sub    $0x8,%esp
f01009af:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01009b2:	0f be 04 18          	movsbl (%eax,%ebx,1),%eax
f01009b6:	50                   	push   %eax
f01009b7:	68 14 73 10 f0       	push   $0xf0107314
f01009bc:	e8 37 32 00 00       	call   f0103bf8 <cprintf>
	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
	cprintf(": ");
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01009c1:	43                   	inc    %ebx
f01009c2:	83 c4 10             	add    $0x10,%esp
f01009c5:	3b 5d dc             	cmp    -0x24(%ebp),%ebx
f01009c8:	7c e2                	jl     f01009ac <print_stack_Info+0xa6>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
	}
	//print offset number
	cprintf("+");
f01009ca:	83 ec 0c             	sub    $0xc,%esp
f01009cd:	68 17 73 10 f0       	push   $0xf0107317
f01009d2:	e8 21 32 00 00       	call   f0103bf8 <cprintf>
	cprintf("%d", ( (int)(*return_Addr) - (int)(info_Ptr->eip_fn_addr) ) );
f01009d7:	83 c4 08             	add    $0x8,%esp
f01009da:	8b 06                	mov    (%esi),%eax
f01009dc:	2b 45 e0             	sub    -0x20(%ebp),%eax
f01009df:	50                   	push   %eax
f01009e0:	68 de 90 10 f0       	push   $0xf01090de
f01009e5:	e8 0e 32 00 00       	call   f0103bf8 <cprintf>
	cprintf("\n");
f01009ea:	c7 04 24 32 81 10 f0 	movl   $0xf0108132,(%esp)
f01009f1:	e8 02 32 00 00       	call   f0103bf8 <cprintf>
}
f01009f6:	83 c4 10             	add    $0x10,%esp
f01009f9:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01009fc:	5b                   	pop    %ebx
f01009fd:	5e                   	pop    %esi
f01009fe:	5f                   	pop    %edi
f01009ff:	5d                   	pop    %ebp
f0100a00:	c3                   	ret    

f0100a01 <mon_backtrace>:


*/
int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
f0100a01:	55                   	push   %ebp
f0100a02:	89 e5                	mov    %esp,%ebp
f0100a04:	56                   	push   %esi
f0100a05:	53                   	push   %ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f0100a06:	89 ee                	mov    %ebp,%esi
	//function prologue
	//f0100751:	55                   	push   %ebp
	//f0100752:	89 e5                	mov    %esp,%ebp
	// Your code here. -.-
	uint32_t esp = read_ebp(); //since mov %esp,%ebp
	uint32_t ebp = *( (uint32_t*)esp );
f0100a08:	8b 1e                	mov    (%esi),%ebx
	uint32_t* return_Addr = (uint32_t*)(esp+4);
	//start print information
	cprintf("Stack backtrace:\n");
f0100a0a:	83 ec 0c             	sub    $0xc,%esp
f0100a0d:	68 19 73 10 f0       	push   $0xf0107319
f0100a12:	e8 e1 31 00 00       	call   f0103bf8 <cprintf>
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
f0100a17:	83 c4 08             	add    $0x8,%esp
f0100a1a:	8d 46 04             	lea    0x4(%esi),%eax
f0100a1d:	50                   	push   %eax
f0100a1e:	56                   	push   %esi
f0100a1f:	e8 e2 fe ff ff       	call   f0100906 <print_stack_Info>
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100a24:	83 c4 10             	add    $0x10,%esp
f0100a27:	eb 12                	jmp    f0100a3b <mon_backtrace+0x3a>
		return_Addr = ((uint32_t*)ebp+1);
		//ebp = *((uint32_t*)ebp); //trace back to the last ebp
		//cprintf("ebp %08x eip %08x\n", ebp, return_Addr);  //the current ebp == esp
		print_stack_Info(ebp, return_Addr);
f0100a29:	83 ec 08             	sub    $0x8,%esp
f0100a2c:	8d 43 04             	lea    0x4(%ebx),%eax
f0100a2f:	50                   	push   %eax
f0100a30:	53                   	push   %ebx
f0100a31:	e8 d0 fe ff ff       	call   f0100906 <print_stack_Info>
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
f0100a36:	8b 1b                	mov    (%ebx),%ebx
f0100a38:	83 c4 10             	add    $0x10,%esp
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100a3b:	85 db                	test   %ebx,%ebx
f0100a3d:	75 ea                	jne    f0100a29 <mon_backtrace+0x28>
		print_stack_Info(ebp, return_Addr);
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
	}

	//Not print the 1st stack address since it is the kernel allocated the beginning of the stack
	cprintf("Stoppp Stack backtrace:\n ");
f0100a3f:	83 ec 0c             	sub    $0xc,%esp
f0100a42:	68 2b 73 10 f0       	push   $0xf010732b
f0100a47:	e8 ac 31 00 00       	call   f0103bf8 <cprintf>

	//cprintf("argc is %08x\n", argc);
	return 0;
}
f0100a4c:	b8 00 00 00 00       	mov    $0x0,%eax
f0100a51:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0100a54:	5b                   	pop    %ebx
f0100a55:	5e                   	pop    %esi
f0100a56:	5d                   	pop    %ebp
f0100a57:	c3                   	ret    

f0100a58 <monitor>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f0100a58:	55                   	push   %ebp
f0100a59:	89 e5                	mov    %esp,%ebp
f0100a5b:	57                   	push   %edi
f0100a5c:	56                   	push   %esi
f0100a5d:	53                   	push   %ebx
f0100a5e:	83 ec 58             	sub    $0x58,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f0100a61:	68 90 74 10 f0       	push   $0xf0107490
f0100a66:	e8 8d 31 00 00       	call   f0103bf8 <cprintf>
	cprintf("Type 'help' for a list of commands.\n");
f0100a6b:	c7 04 24 b4 74 10 f0 	movl   $0xf01074b4,(%esp)
f0100a72:	e8 81 31 00 00       	call   f0103bf8 <cprintf>

	if (tf != NULL)
f0100a77:	83 c4 10             	add    $0x10,%esp
f0100a7a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0100a7e:	74 0e                	je     f0100a8e <monitor+0x36>
		print_trapframe(tf);
f0100a80:	83 ec 0c             	sub    $0xc,%esp
f0100a83:	ff 75 08             	pushl  0x8(%ebp)
f0100a86:	e8 bd 38 00 00       	call   f0104348 <print_trapframe>
f0100a8b:	83 c4 10             	add    $0x10,%esp

	while (1) {
		buf = readline("K> ");
f0100a8e:	83 ec 0c             	sub    $0xc,%esp
f0100a91:	68 45 73 10 f0       	push   $0xf0107345
f0100a96:	e8 1c 53 00 00       	call   f0105db7 <readline>
f0100a9b:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f0100a9d:	83 c4 10             	add    $0x10,%esp
f0100aa0:	85 c0                	test   %eax,%eax
f0100aa2:	74 ea                	je     f0100a8e <monitor+0x36>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f0100aa4:	c7 45 a8 00 00 00 00 	movl   $0x0,-0x58(%ebp)
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f0100aab:	be 00 00 00 00       	mov    $0x0,%esi
f0100ab0:	eb 0a                	jmp    f0100abc <monitor+0x64>
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f0100ab2:	c6 03 00             	movb   $0x0,(%ebx)
f0100ab5:	89 f7                	mov    %esi,%edi
f0100ab7:	8d 5b 01             	lea    0x1(%ebx),%ebx
f0100aba:	89 fe                	mov    %edi,%esi
	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f0100abc:	8a 03                	mov    (%ebx),%al
f0100abe:	84 c0                	test   %al,%al
f0100ac0:	74 60                	je     f0100b22 <monitor+0xca>
f0100ac2:	83 ec 08             	sub    $0x8,%esp
f0100ac5:	0f be c0             	movsbl %al,%eax
f0100ac8:	50                   	push   %eax
f0100ac9:	68 49 73 10 f0       	push   $0xf0107349
f0100ace:	e8 fc 54 00 00       	call   f0105fcf <strchr>
f0100ad3:	83 c4 10             	add    $0x10,%esp
f0100ad6:	85 c0                	test   %eax,%eax
f0100ad8:	75 d8                	jne    f0100ab2 <monitor+0x5a>
			*buf++ = 0;
		if (*buf == 0)
f0100ada:	80 3b 00             	cmpb   $0x0,(%ebx)
f0100add:	74 43                	je     f0100b22 <monitor+0xca>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f0100adf:	83 fe 0f             	cmp    $0xf,%esi
f0100ae2:	75 14                	jne    f0100af8 <monitor+0xa0>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f0100ae4:	83 ec 08             	sub    $0x8,%esp
f0100ae7:	6a 10                	push   $0x10
f0100ae9:	68 4e 73 10 f0       	push   $0xf010734e
f0100aee:	e8 05 31 00 00       	call   f0103bf8 <cprintf>
f0100af3:	83 c4 10             	add    $0x10,%esp
f0100af6:	eb 96                	jmp    f0100a8e <monitor+0x36>
			return 0;
		}
		argv[argc++] = buf;
f0100af8:	8d 7e 01             	lea    0x1(%esi),%edi
f0100afb:	89 5c b5 a8          	mov    %ebx,-0x58(%ebp,%esi,4)
f0100aff:	eb 01                	jmp    f0100b02 <monitor+0xaa>
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f0100b01:	43                   	inc    %ebx
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
f0100b02:	8a 03                	mov    (%ebx),%al
f0100b04:	84 c0                	test   %al,%al
f0100b06:	74 b2                	je     f0100aba <monitor+0x62>
f0100b08:	83 ec 08             	sub    $0x8,%esp
f0100b0b:	0f be c0             	movsbl %al,%eax
f0100b0e:	50                   	push   %eax
f0100b0f:	68 49 73 10 f0       	push   $0xf0107349
f0100b14:	e8 b6 54 00 00       	call   f0105fcf <strchr>
f0100b19:	83 c4 10             	add    $0x10,%esp
f0100b1c:	85 c0                	test   %eax,%eax
f0100b1e:	74 e1                	je     f0100b01 <monitor+0xa9>
f0100b20:	eb 98                	jmp    f0100aba <monitor+0x62>
			buf++;
	}
	argv[argc] = 0;
f0100b22:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
f0100b29:	00 

	// Lookup and invoke the command
	if (argc == 0)
f0100b2a:	85 f6                	test   %esi,%esi
f0100b2c:	0f 84 5c ff ff ff    	je     f0100a8e <monitor+0x36>
f0100b32:	bf e0 74 10 f0       	mov    $0xf01074e0,%edi
f0100b37:	bb 00 00 00 00       	mov    $0x0,%ebx
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f0100b3c:	83 ec 08             	sub    $0x8,%esp
f0100b3f:	ff 37                	pushl  (%edi)
f0100b41:	ff 75 a8             	pushl  -0x58(%ebp)
f0100b44:	e8 32 54 00 00       	call   f0105f7b <strcmp>
f0100b49:	83 c4 10             	add    $0x10,%esp
f0100b4c:	85 c0                	test   %eax,%eax
f0100b4e:	75 23                	jne    f0100b73 <monitor+0x11b>
			return commands[i].func(argc, argv, tf);
f0100b50:	83 ec 04             	sub    $0x4,%esp
f0100b53:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
f0100b56:	01 c3                	add    %eax,%ebx
f0100b58:	ff 75 08             	pushl  0x8(%ebp)
f0100b5b:	8d 45 a8             	lea    -0x58(%ebp),%eax
f0100b5e:	50                   	push   %eax
f0100b5f:	56                   	push   %esi
f0100b60:	ff 14 9d e8 74 10 f0 	call   *-0xfef8b18(,%ebx,4)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f0100b67:	83 c4 10             	add    $0x10,%esp
f0100b6a:	85 c0                	test   %eax,%eax
f0100b6c:	78 26                	js     f0100b94 <monitor+0x13c>
f0100b6e:	e9 1b ff ff ff       	jmp    f0100a8e <monitor+0x36>
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
f0100b73:	43                   	inc    %ebx
f0100b74:	83 c7 0c             	add    $0xc,%edi
f0100b77:	83 fb 03             	cmp    $0x3,%ebx
f0100b7a:	75 c0                	jne    f0100b3c <monitor+0xe4>
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f0100b7c:	83 ec 08             	sub    $0x8,%esp
f0100b7f:	ff 75 a8             	pushl  -0x58(%ebp)
f0100b82:	68 6b 73 10 f0       	push   $0xf010736b
f0100b87:	e8 6c 30 00 00       	call   f0103bf8 <cprintf>
f0100b8c:	83 c4 10             	add    $0x10,%esp
f0100b8f:	e9 fa fe ff ff       	jmp    f0100a8e <monitor+0x36>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f0100b94:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100b97:	5b                   	pop    %ebx
f0100b98:	5e                   	pop    %esi
f0100b99:	5f                   	pop    %edi
f0100b9a:	5d                   	pop    %ebp
f0100b9b:	c3                   	ret    

f0100b9c <e820_init>:

// This function may ONLY be used during initialization,
// before page_init().
void
e820_init(physaddr_t mbi_pa)
{
f0100b9c:	55                   	push   %ebp
f0100b9d:	89 e5                	mov    %esp,%ebp
f0100b9f:	57                   	push   %edi
f0100ba0:	56                   	push   %esi
f0100ba1:	53                   	push   %ebx
f0100ba2:	83 ec 1c             	sub    $0x1c,%esp
f0100ba5:	8b 75 08             	mov    0x8(%ebp),%esi
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
f0100ba8:	f6 06 40             	testb  $0x40,(%esi)
f0100bab:	75 16                	jne    f0100bc3 <e820_init+0x27>
f0100bad:	68 04 75 10 f0       	push   $0xf0107504
f0100bb2:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100bb7:	6a 26                	push   $0x26
f0100bb9:	68 57 75 10 f0       	push   $0xf0107557
f0100bbe:	e8 89 f4 ff ff       	call   f010004c <_panic>
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);
f0100bc3:	8b 56 30             	mov    0x30(%esi),%edx
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
f0100bc6:	83 ec 04             	sub    $0x4,%esp
f0100bc9:	89 d0                	mov    %edx,%eax
f0100bcb:	03 46 2c             	add    0x2c(%esi),%eax
f0100bce:	48                   	dec    %eax
f0100bcf:	50                   	push   %eax
f0100bd0:	52                   	push   %edx
f0100bd1:	68 28 75 10 f0       	push   $0xf0107528
f0100bd6:	e8 1d 30 00 00       	call   f0103bf8 <cprintf>
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
f0100bdb:	8b 5e 30             	mov    0x30(%esi),%ebx
	addr_end = mbi->mmap_addr + mbi->mmap_length;
f0100bde:	89 d8                	mov    %ebx,%eax
f0100be0:	03 46 2c             	add    0x2c(%esi),%eax
f0100be3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0100be6:	c7 45 dc 24 2f 27 f0 	movl   $0xf0272f24,-0x24(%ebp)
	for (i = 0; addr < addr_end; ++i) {
f0100bed:	83 c4 10             	add    $0x10,%esp
f0100bf0:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f0100bf7:	e9 b5 00 00 00       	jmp    f0100cb1 <e820_init+0x115>
		struct multiboot_mmap_entry *e;

		// Print memory mapping.
		assert(addr_end - addr >= sizeof(*e));
f0100bfc:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100bff:	29 d8                	sub    %ebx,%eax
f0100c01:	83 f8 17             	cmp    $0x17,%eax
f0100c04:	77 16                	ja     f0100c1c <e820_init+0x80>
f0100c06:	68 63 75 10 f0       	push   $0xf0107563
f0100c0b:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100c10:	6a 30                	push   $0x30
f0100c12:	68 57 75 10 f0       	push   $0xf0107557
f0100c17:	e8 30 f4 ff ff       	call   f010004c <_panic>
		e = (struct multiboot_mmap_entry *)addr;
f0100c1c:	89 de                	mov    %ebx,%esi
		cprintf("  [mem %08p-%08p] ",
f0100c1e:	8b 53 04             	mov    0x4(%ebx),%edx
f0100c21:	83 ec 04             	sub    $0x4,%esp
f0100c24:	89 d0                	mov    %edx,%eax
f0100c26:	03 43 0c             	add    0xc(%ebx),%eax
f0100c29:	48                   	dec    %eax
f0100c2a:	50                   	push   %eax
f0100c2b:	52                   	push   %edx
f0100c2c:	68 81 75 10 f0       	push   $0xf0107581
f0100c31:	e8 c2 2f 00 00       	call   f0103bf8 <cprintf>
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
f0100c36:	8b 43 14             	mov    0x14(%ebx),%eax
	"unusable",
};

static void
print_e820_map_type(uint32_t type) {
	switch (type) {
f0100c39:	83 c4 10             	add    $0x10,%esp
f0100c3c:	8d 50 ff             	lea    -0x1(%eax),%edx
f0100c3f:	83 fa 04             	cmp    $0x4,%edx
f0100c42:	77 14                	ja     f0100c58 <e820_init+0xbc>
	case 1 ... 5:
		cprintf(e820_map_types[type - 1]);
f0100c44:	83 ec 0c             	sub    $0xc,%esp
f0100c47:	ff 34 85 e0 75 10 f0 	pushl  -0xfef8a20(,%eax,4)
f0100c4e:	e8 a5 2f 00 00       	call   f0103bf8 <cprintf>
f0100c53:	83 c4 10             	add    $0x10,%esp
f0100c56:	eb 11                	jmp    f0100c69 <e820_init+0xcd>
		break;
	default:
		cprintf("type %u", type);
f0100c58:	83 ec 08             	sub    $0x8,%esp
f0100c5b:	50                   	push   %eax
f0100c5c:	68 94 75 10 f0       	push   $0xf0107594
f0100c61:	e8 92 2f 00 00       	call   f0103bf8 <cprintf>
f0100c66:	83 c4 10             	add    $0x10,%esp
		e = (struct multiboot_mmap_entry *)addr;
		cprintf("  [mem %08p-%08p] ",
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
		cprintf("\n");
f0100c69:	83 ec 0c             	sub    $0xc,%esp
f0100c6c:	68 32 81 10 f0       	push   $0xf0108132
f0100c71:	e8 82 2f 00 00       	call   f0103bf8 <cprintf>

		// Save a copy.
		assert(i < E820_NR_MAX);
f0100c76:	83 c4 10             	add    $0x10,%esp
f0100c79:	83 7d e0 40          	cmpl   $0x40,-0x20(%ebp)
f0100c7d:	75 16                	jne    f0100c95 <e820_init+0xf9>
f0100c7f:	68 9c 75 10 f0       	push   $0xf010759c
f0100c84:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100c89:	6a 39                	push   $0x39
f0100c8b:	68 57 75 10 f0       	push   $0xf0107557
f0100c90:	e8 b7 f3 ff ff       	call   f010004c <_panic>
		e820_map.entries[i] = e->e820;
f0100c95:	89 f0                	mov    %esi,%eax
f0100c97:	8d 76 04             	lea    0x4(%esi),%esi
f0100c9a:	b9 05 00 00 00       	mov    $0x5,%ecx
f0100c9f:	8b 7d dc             	mov    -0x24(%ebp),%edi
f0100ca2:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		addr += (e->size + 4);
f0100ca4:	8b 00                	mov    (%eax),%eax
f0100ca6:	8d 5c 03 04          	lea    0x4(%ebx,%eax,1),%ebx
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
	addr_end = mbi->mmap_addr + mbi->mmap_length;
	for (i = 0; addr < addr_end; ++i) {
f0100caa:	ff 45 e0             	incl   -0x20(%ebp)
f0100cad:	83 45 dc 14          	addl   $0x14,-0x24(%ebp)
f0100cb1:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f0100cb4:	0f 82 42 ff ff ff    	jb     f0100bfc <e820_init+0x60>
		// Save a copy.
		assert(i < E820_NR_MAX);
		e820_map.entries[i] = e->e820;
		addr += (e->size + 4);
	}
	e820_map.nr = i;
f0100cba:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100cbd:	a3 20 2f 27 f0       	mov    %eax,0xf0272f20
}
f0100cc2:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100cc5:	5b                   	pop    %ebx
f0100cc6:	5e                   	pop    %esi
f0100cc7:	5f                   	pop    %edi
f0100cc8:	5d                   	pop    %ebp
f0100cc9:	c3                   	ret    

f0100cca <boot_alloc>:
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100cca:	89 c1                	mov    %eax,%ecx
	// Initialize nextfree if this is the first time.
	// 'end' is a magic symbol automatically generated by the linker,
	// which points to the end of the kernel's bss segment:
	// the first virtual address that the linker did *not* assign
	// to any kernel code or global variables.
	if (!nextfree) {
f0100ccc:	83 3d 38 22 27 f0 00 	cmpl   $0x0,0xf0272238
f0100cd3:	75 0f                	jne    f0100ce4 <boot_alloc+0x1a>
		extern char end[]; //points to the end of the kernel
		nextfree = ROUNDUP((char *) end, PGSIZE); //set the nextfree pointer
f0100cd5:	b8 07 60 2b f0       	mov    $0xf02b6007,%eax
f0100cda:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100cdf:	a3 38 22 27 f0       	mov    %eax,0xf0272238
	//
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
f0100ce4:	a1 38 22 27 f0       	mov    0xf0272238,%eax
f0100ce9:	8b 15 24 34 27 f0    	mov    0xf0273424,%edx
f0100cef:	c1 e2 0c             	shl    $0xc,%edx
f0100cf2:	29 c2                	sub    %eax,%edx
f0100cf4:	39 ca                	cmp    %ecx,%edx
f0100cf6:	73 17                	jae    f0100d0f <boot_alloc+0x45>
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100cf8:	55                   	push   %ebp
f0100cf9:	89 e5                	mov    %esp,%ebp
f0100cfb:	83 ec 0c             	sub    $0xc,%esp
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
		panic("boot_alloc: not enough physical address to allocate\n");
f0100cfe:	68 f8 75 10 f0       	push   $0xf01075f8
f0100d03:	6a 64                	push   $0x64
f0100d05:	68 61 7f 10 f0       	push   $0xf0107f61
f0100d0a:	e8 3d f3 ff ff       	call   f010004c <_panic>
	}
	//nextfree = ROUNDUP( ((char *)n + nextfree), PGSIZE); //update the nextfree pointer
	result = nextfree;
	nextfree = n + nextfree; //update the nextfree pointer
f0100d0f:	01 c1                	add    %eax,%ecx
f0100d11:	89 0d 38 22 27 f0    	mov    %ecx,0xf0272238
	return result;
	//return NULL;
}
f0100d17:	c3                   	ret    

f0100d18 <check_va2pa>:
check_va2pa(pde_t *pgdir, uintptr_t va)
{
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
f0100d18:	89 d1                	mov    %edx,%ecx
f0100d1a:	c1 e9 16             	shr    $0x16,%ecx
f0100d1d:	8b 04 88             	mov    (%eax,%ecx,4),%eax
f0100d20:	a8 01                	test   $0x1,%al
f0100d22:	74 47                	je     f0100d6b <check_va2pa+0x53>
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
f0100d24:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100d29:	89 c1                	mov    %eax,%ecx
f0100d2b:	c1 e9 0c             	shr    $0xc,%ecx
f0100d2e:	3b 0d 24 34 27 f0    	cmp    0xf0273424,%ecx
f0100d34:	72 1b                	jb     f0100d51 <check_va2pa+0x39>
// this functionality for us!  We define our own version to help check
// the check_kern_pgdir() function; it shouldn't be used elsewhere.

static physaddr_t
check_va2pa(pde_t *pgdir, uintptr_t va)
{
f0100d36:	55                   	push   %ebp
f0100d37:	89 e5                	mov    %esp,%ebp
f0100d39:	83 ec 08             	sub    $0x8,%esp
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100d3c:	50                   	push   %eax
f0100d3d:	68 68 6f 10 f0       	push   $0xf0106f68
f0100d42:	68 8d 04 00 00       	push   $0x48d
f0100d47:	68 61 7f 10 f0       	push   $0xf0107f61
f0100d4c:	e8 fb f2 ff ff       	call   f010004c <_panic>
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
f0100d51:	c1 ea 0c             	shr    $0xc,%edx
f0100d54:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f0100d5a:	8b 84 90 00 00 00 f0 	mov    -0x10000000(%eax,%edx,4),%eax
f0100d61:	a8 01                	test   $0x1,%al
f0100d63:	74 0c                	je     f0100d71 <check_va2pa+0x59>
	}

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
f0100d65:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100d6a:	c3                   	ret    
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
f0100d6b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100d70:	c3                   	ret    
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
		//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
		//cprintf("check_va2pa no pte, return fff...\n");
		//cprintf("shit happens\n");
		return ~0;
f0100d71:	b8 ff ff ff ff       	mov    $0xffffffff,%eax

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
}
f0100d76:	c3                   	ret    

f0100d77 <check_page_free_list>:
//
// Check that the pages on the page_free_list are reasonable.
//
static void
check_page_free_list(bool only_low_memory)
{
f0100d77:	55                   	push   %ebp
f0100d78:	89 e5                	mov    %esp,%ebp
f0100d7a:	57                   	push   %edi
f0100d7b:	56                   	push   %esi
f0100d7c:	53                   	push   %ebx
f0100d7d:	83 ec 2c             	sub    $0x2c,%esp
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100d80:	84 c0                	test   %al,%al
f0100d82:	0f 85 8b 02 00 00    	jne    f0101013 <check_page_free_list+0x29c>
f0100d88:	e9 98 02 00 00       	jmp    f0101025 <check_page_free_list+0x2ae>
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
		panic("'page_free_list' is a null pointer!");
f0100d8d:	83 ec 04             	sub    $0x4,%esp
f0100d90:	68 30 76 10 f0       	push   $0xf0107630
f0100d95:	68 bb 03 00 00       	push   $0x3bb
f0100d9a:	68 61 7f 10 f0       	push   $0xf0107f61
f0100d9f:	e8 a8 f2 ff ff       	call   f010004c <_panic>

	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
f0100da4:	8d 55 d8             	lea    -0x28(%ebp),%edx
f0100da7:	89 55 e0             	mov    %edx,-0x20(%ebp)
f0100daa:	8d 55 dc             	lea    -0x24(%ebp),%edx
f0100dad:	89 55 e4             	mov    %edx,-0x1c(%ebp)
		for (pp = page_free_list; pp; pp = pp->pp_link) {
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
f0100db0:	89 c2                	mov    %eax,%edx
f0100db2:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f0100db8:	f7 c2 00 e0 7f 00    	test   $0x7fe000,%edx
f0100dbe:	0f 95 c2             	setne  %dl
f0100dc1:	0f b6 d2             	movzbl %dl,%edx
			*tp[pagetype] = pp;
f0100dc4:	8b 4c 95 e0          	mov    -0x20(%ebp,%edx,4),%ecx
f0100dc8:	89 01                	mov    %eax,(%ecx)
			tp[pagetype] = &pp->pp_link;
f0100dca:	89 44 95 e0          	mov    %eax,-0x20(%ebp,%edx,4)
	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
		for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100dce:	8b 00                	mov    (%eax),%eax
f0100dd0:	85 c0                	test   %eax,%eax
f0100dd2:	75 dc                	jne    f0100db0 <check_page_free_list+0x39>
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
			*tp[pagetype] = pp;
			tp[pagetype] = &pp->pp_link;
		}
		*tp[1] = 0;
f0100dd4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100dd7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		*tp[0] = pp2;
f0100ddd:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100de0:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100de3:	89 10                	mov    %edx,(%eax)
		page_free_list = pp1;
f0100de5:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100de8:	a3 40 22 27 f0       	mov    %eax,0xf0272240
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100ded:	be 01 00 00 00       	mov    $0x1,%esi
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100df2:	8b 1d 40 22 27 f0    	mov    0xf0272240,%ebx
f0100df8:	eb 53                	jmp    f0100e4d <check_page_free_list+0xd6>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100dfa:	89 d8                	mov    %ebx,%eax
f0100dfc:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0100e02:	c1 f8 03             	sar    $0x3,%eax
f0100e05:	c1 e0 0c             	shl    $0xc,%eax
		if (PDX(page2pa(pp)) < pdx_limit)
f0100e08:	89 c2                	mov    %eax,%edx
f0100e0a:	c1 ea 16             	shr    $0x16,%edx
f0100e0d:	39 f2                	cmp    %esi,%edx
f0100e0f:	73 3a                	jae    f0100e4b <check_page_free_list+0xd4>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100e11:	89 c2                	mov    %eax,%edx
f0100e13:	c1 ea 0c             	shr    $0xc,%edx
f0100e16:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f0100e1c:	72 12                	jb     f0100e30 <check_page_free_list+0xb9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100e1e:	50                   	push   %eax
f0100e1f:	68 68 6f 10 f0       	push   $0xf0106f68
f0100e24:	6a 58                	push   $0x58
f0100e26:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0100e2b:	e8 1c f2 ff ff       	call   f010004c <_panic>
			memset(page2kva(pp), 0x97, 128);
f0100e30:	83 ec 04             	sub    $0x4,%esp
f0100e33:	68 80 00 00 00       	push   $0x80
f0100e38:	68 97 00 00 00       	push   $0x97
f0100e3d:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100e42:	50                   	push   %eax
f0100e43:	e8 bc 51 00 00       	call   f0106004 <memset>
f0100e48:	83 c4 10             	add    $0x10,%esp
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100e4b:	8b 1b                	mov    (%ebx),%ebx
f0100e4d:	85 db                	test   %ebx,%ebx
f0100e4f:	75 a9                	jne    f0100dfa <check_page_free_list+0x83>
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
f0100e51:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e56:	e8 6f fe ff ff       	call   f0100cca <boot_alloc>
f0100e5b:	89 45 cc             	mov    %eax,-0x34(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100e5e:	8b 15 40 22 27 f0    	mov    0xf0272240,%edx
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100e64:	8b 0d 2c 34 27 f0    	mov    0xf027342c,%ecx
		assert(pp < pages + npages);
f0100e6a:	a1 24 34 27 f0       	mov    0xf0273424,%eax
f0100e6f:	89 45 c8             	mov    %eax,-0x38(%ebp)
f0100e72:	8d 04 c1             	lea    (%ecx,%eax,8),%eax
f0100e75:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100e78:	89 4d d0             	mov    %ecx,-0x30(%ebp)
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
f0100e7b:	be 00 00 00 00       	mov    $0x0,%esi
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100e80:	e9 4c 01 00 00       	jmp    f0100fd1 <check_page_free_list+0x25a>
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100e85:	39 ca                	cmp    %ecx,%edx
f0100e87:	73 19                	jae    f0100ea2 <check_page_free_list+0x12b>
f0100e89:	68 7b 7f 10 f0       	push   $0xf0107f7b
f0100e8e:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100e93:	68 d5 03 00 00       	push   $0x3d5
f0100e98:	68 61 7f 10 f0       	push   $0xf0107f61
f0100e9d:	e8 aa f1 ff ff       	call   f010004c <_panic>
		assert(pp < pages + npages);
f0100ea2:	3b 55 d4             	cmp    -0x2c(%ebp),%edx
f0100ea5:	72 19                	jb     f0100ec0 <check_page_free_list+0x149>
f0100ea7:	68 87 7f 10 f0       	push   $0xf0107f87
f0100eac:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100eb1:	68 d6 03 00 00       	push   $0x3d6
f0100eb6:	68 61 7f 10 f0       	push   $0xf0107f61
f0100ebb:	e8 8c f1 ff ff       	call   f010004c <_panic>
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100ec0:	89 d0                	mov    %edx,%eax
f0100ec2:	2b 45 d0             	sub    -0x30(%ebp),%eax
f0100ec5:	a8 07                	test   $0x7,%al
f0100ec7:	74 19                	je     f0100ee2 <check_page_free_list+0x16b>
f0100ec9:	68 54 76 10 f0       	push   $0xf0107654
f0100ece:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100ed3:	68 d7 03 00 00       	push   $0x3d7
f0100ed8:	68 61 7f 10 f0       	push   $0xf0107f61
f0100edd:	e8 6a f1 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100ee2:	c1 f8 03             	sar    $0x3,%eax

		// check a few pages that shouldn't be on the free list
		assert(page2pa(pp) != 0);
f0100ee5:	c1 e0 0c             	shl    $0xc,%eax
f0100ee8:	75 19                	jne    f0100f03 <check_page_free_list+0x18c>
f0100eea:	68 9b 7f 10 f0       	push   $0xf0107f9b
f0100eef:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100ef4:	68 da 03 00 00       	push   $0x3da
f0100ef9:	68 61 7f 10 f0       	push   $0xf0107f61
f0100efe:	e8 49 f1 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != IOPHYSMEM);
f0100f03:	3d 00 00 0a 00       	cmp    $0xa0000,%eax
f0100f08:	75 19                	jne    f0100f23 <check_page_free_list+0x1ac>
f0100f0a:	68 ac 7f 10 f0       	push   $0xf0107fac
f0100f0f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100f14:	68 db 03 00 00       	push   $0x3db
f0100f19:	68 61 7f 10 f0       	push   $0xf0107f61
f0100f1e:	e8 29 f1 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM - PGSIZE);
f0100f23:	3d 00 f0 0f 00       	cmp    $0xff000,%eax
f0100f28:	75 19                	jne    f0100f43 <check_page_free_list+0x1cc>
f0100f2a:	68 88 76 10 f0       	push   $0xf0107688
f0100f2f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100f34:	68 dc 03 00 00       	push   $0x3dc
f0100f39:	68 61 7f 10 f0       	push   $0xf0107f61
f0100f3e:	e8 09 f1 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM);
f0100f43:	3d 00 00 10 00       	cmp    $0x100000,%eax
f0100f48:	75 19                	jne    f0100f63 <check_page_free_list+0x1ec>
f0100f4a:	68 c5 7f 10 f0       	push   $0xf0107fc5
f0100f4f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100f54:	68 dd 03 00 00       	push   $0x3dd
f0100f59:	68 61 7f 10 f0       	push   $0xf0107f61
f0100f5e:	e8 e9 f0 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) < EXTPHYSMEM || (char *) page2kva(pp) >= first_free_page);
f0100f63:	3d ff ff 0f 00       	cmp    $0xfffff,%eax
f0100f68:	0f 86 da 00 00 00    	jbe    f0101048 <check_page_free_list+0x2d1>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100f6e:	89 c7                	mov    %eax,%edi
f0100f70:	c1 ef 0c             	shr    $0xc,%edi
f0100f73:	39 7d c8             	cmp    %edi,-0x38(%ebp)
f0100f76:	77 12                	ja     f0100f8a <check_page_free_list+0x213>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100f78:	50                   	push   %eax
f0100f79:	68 68 6f 10 f0       	push   $0xf0106f68
f0100f7e:	6a 58                	push   $0x58
f0100f80:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0100f85:	e8 c2 f0 ff ff       	call   f010004c <_panic>
f0100f8a:	8d b8 00 00 00 f0    	lea    -0x10000000(%eax),%edi
f0100f90:	39 7d cc             	cmp    %edi,-0x34(%ebp)
f0100f93:	0f 86 a3 00 00 00    	jbe    f010103c <check_page_free_list+0x2c5>
f0100f99:	68 ac 76 10 f0       	push   $0xf01076ac
f0100f9e:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100fa3:	68 de 03 00 00       	push   $0x3de
f0100fa8:	68 61 7f 10 f0       	push   $0xf0107f61
f0100fad:	e8 9a f0 ff ff       	call   f010004c <_panic>
		// (new test for lab 4)
		assert(page2pa(pp) != MPENTRY_PADDR);
f0100fb2:	68 df 7f 10 f0       	push   $0xf0107fdf
f0100fb7:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100fbc:	68 e0 03 00 00       	push   $0x3e0
f0100fc1:	68 61 7f 10 f0       	push   $0xf0107f61
f0100fc6:	e8 81 f0 ff ff       	call   f010004c <_panic>

		if (page2pa(pp) < EXTPHYSMEM)
			++nfree_basemem;
f0100fcb:	46                   	inc    %esi
f0100fcc:	eb 01                	jmp    f0100fcf <check_page_free_list+0x258>
		else
			++nfree_extmem;
f0100fce:	43                   	inc    %ebx
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100fcf:	8b 12                	mov    (%edx),%edx
f0100fd1:	85 d2                	test   %edx,%edx
f0100fd3:	0f 85 ac fe ff ff    	jne    f0100e85 <check_page_free_list+0x10e>
			++nfree_basemem;
		else
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
f0100fd9:	85 f6                	test   %esi,%esi
f0100fdb:	7f 19                	jg     f0100ff6 <check_page_free_list+0x27f>
f0100fdd:	68 fc 7f 10 f0       	push   $0xf0107ffc
f0100fe2:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0100fe7:	68 e8 03 00 00       	push   $0x3e8
f0100fec:	68 61 7f 10 f0       	push   $0xf0107f61
f0100ff1:	e8 56 f0 ff ff       	call   f010004c <_panic>
	assert(nfree_extmem > 0);
f0100ff6:	85 db                	test   %ebx,%ebx
f0100ff8:	7f 5e                	jg     f0101058 <check_page_free_list+0x2e1>
f0100ffa:	68 0e 80 10 f0       	push   $0xf010800e
f0100fff:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101004:	68 e9 03 00 00       	push   $0x3e9
f0101009:	68 61 7f 10 f0       	push   $0xf0107f61
f010100e:	e8 39 f0 ff ff       	call   f010004c <_panic>
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
f0101013:	a1 40 22 27 f0       	mov    0xf0272240,%eax
f0101018:	85 c0                	test   %eax,%eax
f010101a:	0f 85 84 fd ff ff    	jne    f0100da4 <check_page_free_list+0x2d>
f0101020:	e9 68 fd ff ff       	jmp    f0100d8d <check_page_free_list+0x16>
f0101025:	83 3d 40 22 27 f0 00 	cmpl   $0x0,0xf0272240
f010102c:	0f 84 5b fd ff ff    	je     f0100d8d <check_page_free_list+0x16>
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0101032:	be 00 04 00 00       	mov    $0x400,%esi
f0101037:	e9 b6 fd ff ff       	jmp    f0100df2 <check_page_free_list+0x7b>
		assert(page2pa(pp) != IOPHYSMEM);
		assert(page2pa(pp) != EXTPHYSMEM - PGSIZE);
		assert(page2pa(pp) != EXTPHYSMEM);
		assert(page2pa(pp) < EXTPHYSMEM || (char *) page2kva(pp) >= first_free_page);
		// (new test for lab 4)
		assert(page2pa(pp) != MPENTRY_PADDR);
f010103c:	3d 00 70 00 00       	cmp    $0x7000,%eax
f0101041:	75 8b                	jne    f0100fce <check_page_free_list+0x257>
f0101043:	e9 6a ff ff ff       	jmp    f0100fb2 <check_page_free_list+0x23b>
f0101048:	3d 00 70 00 00       	cmp    $0x7000,%eax
f010104d:	0f 85 78 ff ff ff    	jne    f0100fcb <check_page_free_list+0x254>
f0101053:	e9 5a ff ff ff       	jmp    f0100fb2 <check_page_free_list+0x23b>
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
	assert(nfree_extmem > 0);
}
f0101058:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010105b:	5b                   	pop    %ebx
f010105c:	5e                   	pop    %esi
f010105d:	5f                   	pop    %edi
f010105e:	5d                   	pop    %ebp
f010105f:	c3                   	ret    

f0101060 <page_init>:
// allocator functions below to allocate and deallocate physical
// memory via the page_free_list.
//
void
page_init(void)
{
f0101060:	55                   	push   %ebp
f0101061:	89 e5                	mov    %esp,%ebp
f0101063:	56                   	push   %esi
f0101064:	53                   	push   %ebx

	//page_free_list -> Pn -> Pn-1 -> ... -> P2 -> P1
	//physical page 0 is in use.
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
f0101065:	83 ec 08             	sub    $0x8,%esp
f0101068:	ff 35 24 34 27 f0    	pushl  0xf0273424
f010106e:	68 1f 80 10 f0       	push   $0xf010801f
f0101073:	e8 80 2b 00 00       	call   f0103bf8 <cprintf>
	assert(PGNUM(0x0009fbff) <= npages);
f0101078:	83 c4 10             	add    $0x10,%esp
f010107b:	81 3d 24 34 27 f0 9e 	cmpl   $0x9e,0xf0273424
f0101082:	00 00 00 
f0101085:	76 0f                	jbe    f0101096 <page_init+0x36>
f0101087:	8b 1d 40 22 27 f0    	mov    0xf0272240,%ebx
f010108d:	b1 00                	mov    $0x0,%cl
f010108f:	b8 01 00 00 00       	mov    $0x1,%eax
f0101094:	eb 49                	jmp    f01010df <page_init+0x7f>
f0101096:	68 2b 80 10 f0       	push   $0xf010802b
f010109b:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01010a0:	68 7b 01 00 00       	push   $0x17b
f01010a5:	68 61 7f 10 f0       	push   $0xf0107f61
f01010aa:	e8 9d ef ff ff       	call   f010004c <_panic>
	size_t mpentry_index = PGNUM(MPENTRY_PADDR);
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
		//skip the 
		if (i == mpentry_index){
f01010af:	83 f8 07             	cmp    $0x7,%eax
f01010b2:	75 0f                	jne    f01010c3 <page_init+0x63>
            pages[i].pp_ref = 1;
f01010b4:	66 c7 42 3c 01 00    	movw   $0x1,0x3c(%edx)
            pages[i].pp_link = NULL;
f01010ba:	c7 42 38 00 00 00 00 	movl   $0x0,0x38(%edx)
            continue;
f01010c1:	eb 1b                	jmp    f01010de <page_init+0x7e>
f01010c3:	8d 0c c5 00 00 00 00 	lea    0x0(,%eax,8),%ecx
        }

		pages[i].pp_ref = 0;
f01010ca:	01 ca                	add    %ecx,%edx
f01010cc:	66 c7 42 04 00 00    	movw   $0x0,0x4(%edx)
		pages[i].pp_link = page_free_list;
f01010d2:	89 1a                	mov    %ebx,(%edx)
		page_free_list = &pages[i];
f01010d4:	89 cb                	mov    %ecx,%ebx
f01010d6:	03 1d 2c 34 27 f0    	add    0xf027342c,%ebx
f01010dc:	b1 01                	mov    $0x1,%cl
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
	assert(PGNUM(0x0009fbff) <= npages);
	size_t mpentry_index = PGNUM(MPENTRY_PADDR);
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
f01010de:	40                   	inc    %eax
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01010df:	81 3d 24 34 27 f0 9f 	cmpl   $0x9f,0xf0273424
f01010e6:	00 00 00 
f01010e9:	77 1e                	ja     f0101109 <page_init+0xa9>
f01010eb:	84 c9                	test   %cl,%cl
f01010ed:	74 06                	je     f01010f5 <page_init+0x95>
f01010ef:	89 1d 40 22 27 f0    	mov    %ebx,0xf0272240
		panic("pa2page called with invalid pa");
f01010f5:	83 ec 04             	sub    $0x4,%esp
f01010f8:	68 f4 76 10 f0       	push   $0xf01076f4
f01010fd:	6a 51                	push   $0x51
f01010ff:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0101104:	e8 43 ef ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0101109:	8b 15 2c 34 27 f0    	mov    0xf027342c,%edx
f010110f:	3d 9f 00 00 00       	cmp    $0x9f,%eax
f0101114:	75 99                	jne    f01010af <page_init+0x4f>
f0101116:	84 c9                	test   %cl,%cl
f0101118:	74 06                	je     f0101120 <page_init+0xc0>
f010111a:	89 1d 40 22 27 f0    	mov    %ebx,0xf0272240
	//     0x00100000-0x????????  in use for kernel
    //[mem 0x00100000-0x07fdffff] available
	//physaddr_t start_PA = PADDR(boot_alloc(0)); //physical address of the end of the kernel plus previously allocated stuff
	//physaddr_t end_PA = PADDR(0x07fdffff);

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
f0101120:	b8 00 00 00 00       	mov    $0x0,%eax
f0101125:	e8 a0 fb ff ff       	call   f0100cca <boot_alloc>
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
f010112a:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101130:	c1 ea 0c             	shr    $0xc,%edx
f0101133:	a1 24 34 27 f0       	mov    0xf0273424,%eax
f0101138:	39 c2                	cmp    %eax,%edx
f010113a:	76 19                	jbe    f0101155 <page_init+0xf5>
f010113c:	68 47 80 10 f0       	push   $0xf0108047
f0101141:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101146:	68 91 01 00 00       	push   $0x191
f010114b:	68 61 7f 10 f0       	push   $0xf0107f61
f0101150:	e8 f7 ee ff ff       	call   f010004c <_panic>
	assert(PGNUM(end_PA) <= npages);
f0101155:	3d de 7f 00 00       	cmp    $0x7fde,%eax
f010115a:	77 19                	ja     f0101175 <page_init+0x115>
f010115c:	68 61 80 10 f0       	push   $0xf0108061
f0101161:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101166:	68 92 01 00 00       	push   $0x192
f010116b:	68 61 7f 10 f0       	push   $0xf0107f61
f0101170:	e8 d7 ee ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101175:	39 c2                	cmp    %eax,%edx
f0101177:	72 14                	jb     f010118d <page_init+0x12d>
		panic("pa2page called with invalid pa");
f0101179:	83 ec 04             	sub    $0x4,%esp
f010117c:	68 f4 76 10 f0       	push   $0xf01076f4
f0101181:	6a 51                	push   $0x51
f0101183:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0101188:	e8 bf ee ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f010118d:	c1 e2 03             	shl    $0x3,%edx
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f0101190:	89 d1                	mov    %edx,%ecx
f0101192:	c1 f9 03             	sar    $0x3,%ecx
f0101195:	8b 1d 40 22 27 f0    	mov    0xf0272240,%ebx
f010119b:	be 00 00 00 00       	mov    $0x0,%esi
f01011a0:	eb 1b                	jmp    f01011bd <page_init+0x15d>
		pages[i].pp_ref = 0;
f01011a2:	01 d0                	add    %edx,%eax
f01011a4:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
		pages[i].pp_link = page_free_list;
f01011aa:	89 18                	mov    %ebx,(%eax)
		page_free_list = &pages[i];
f01011ac:	89 d3                	mov    %edx,%ebx
f01011ae:	03 1d 2c 34 27 f0    	add    0xf027342c,%ebx

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
	assert(PGNUM(end_PA) <= npages);
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f01011b4:	41                   	inc    %ecx
f01011b5:	83 c2 08             	add    $0x8,%edx
f01011b8:	be 01 00 00 00       	mov    $0x1,%esi
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01011bd:	81 3d 24 34 27 f0 df 	cmpl   $0x7fdf,0xf0273424
f01011c4:	7f 00 00 
f01011c7:	77 20                	ja     f01011e9 <page_init+0x189>
f01011c9:	89 f0                	mov    %esi,%eax
f01011cb:	84 c0                	test   %al,%al
f01011cd:	74 06                	je     f01011d5 <page_init+0x175>
f01011cf:	89 1d 40 22 27 f0    	mov    %ebx,0xf0272240
		panic("pa2page called with invalid pa");
f01011d5:	83 ec 04             	sub    $0x4,%esp
f01011d8:	68 f4 76 10 f0       	push   $0xf01076f4
f01011dd:	6a 51                	push   $0x51
f01011df:	68 6d 7f 10 f0       	push   $0xf0107f6d
f01011e4:	e8 63 ee ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f01011e9:	a1 2c 34 27 f0       	mov    0xf027342c,%eax
f01011ee:	81 f9 de 7f 00 00    	cmp    $0x7fde,%ecx
f01011f4:	7e ac                	jle    f01011a2 <page_init+0x142>
f01011f6:	89 f0                	mov    %esi,%eax
f01011f8:	84 c0                	test   %al,%al
f01011fa:	74 06                	je     f0101202 <page_init+0x1a2>
f01011fc:	89 1d 40 22 27 f0    	mov    %ebx,0xf0272240
		page_free_list = &pages[i];
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//cprintf("Got here1\n");
}
f0101202:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0101205:	5b                   	pop    %ebx
f0101206:	5e                   	pop    %esi
f0101207:	5d                   	pop    %ebp
f0101208:	c3                   	ret    

f0101209 <page_alloc>:
// Returns NULL if out of free memory.
//
// Hint: use page2kva and memset
struct PageInfo *
page_alloc(int alloc_flags)
{
f0101209:	55                   	push   %ebp
f010120a:	89 e5                	mov    %esp,%ebp
f010120c:	53                   	push   %ebx
f010120d:	83 ec 04             	sub    $0x4,%esp
	
	// Fill this function in
	if(page_free_list == NULL){ //check if out of memory
f0101210:	8b 1d 40 22 27 f0    	mov    0xf0272240,%ebx
f0101216:	85 db                	test   %ebx,%ebx
f0101218:	74 5e                	je     f0101278 <page_alloc+0x6f>
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//page_free_list->pp_link = NULL;
	struct PageInfo *page = page_free_list;
	page_free_list = page_free_list->pp_link;
f010121a:	8b 03                	mov    (%ebx),%eax
f010121c:	a3 40 22 27 f0       	mov    %eax,0xf0272240
	page->pp_link = NULL;
f0101221:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
	page->pp_ref = 0;
f0101227:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)
	if(alloc_flags & ALLOC_ZERO){  //fills the entire returned physical page with '\0' bytes
f010122d:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
f0101231:	74 45                	je     f0101278 <page_alloc+0x6f>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101233:	89 d8                	mov    %ebx,%eax
f0101235:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f010123b:	c1 f8 03             	sar    $0x3,%eax
f010123e:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101241:	89 c2                	mov    %eax,%edx
f0101243:	c1 ea 0c             	shr    $0xc,%edx
f0101246:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f010124c:	72 12                	jb     f0101260 <page_alloc+0x57>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010124e:	50                   	push   %eax
f010124f:	68 68 6f 10 f0       	push   $0xf0106f68
f0101254:	6a 58                	push   $0x58
f0101256:	68 6d 7f 10 f0       	push   $0xf0107f6d
f010125b:	e8 ec ed ff ff       	call   f010004c <_panic>
		memset(page2kva(page), '\0', PGSIZE);
f0101260:	83 ec 04             	sub    $0x4,%esp
f0101263:	68 00 10 00 00       	push   $0x1000
f0101268:	6a 00                	push   $0x0
f010126a:	2d 00 00 00 10       	sub    $0x10000000,%eax
f010126f:	50                   	push   %eax
f0101270:	e8 8f 4d 00 00       	call   f0106004 <memset>
f0101275:	83 c4 10             	add    $0x10,%esp
		char *kva = page2kva(result);
		memset(kva, '\0', PGSIZE);
	}
	
	return result;*/
}
f0101278:	89 d8                	mov    %ebx,%eax
f010127a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010127d:	c9                   	leave  
f010127e:	c3                   	ret    

f010127f <page_free>:
// Return a page to the free list.
// (This function should only be called when pp->pp_ref reaches 0.)
//
void
page_free(struct PageInfo *pp)
{
f010127f:	55                   	push   %ebp
f0101280:	89 e5                	mov    %esp,%ebp
f0101282:	83 ec 08             	sub    $0x8,%esp
f0101285:	8b 45 08             	mov    0x8(%ebp),%eax
	// Fill this function in
	// Hint: You may want to panic if pp->pp_ref is nonzero or
	// pp->pp_link is not NULL.
	if(pp->pp_ref != 0){
f0101288:	66 83 78 04 00       	cmpw   $0x0,0x4(%eax)
f010128d:	74 17                	je     f01012a6 <page_free+0x27>
		panic("pp->pp_ref is nonzero");
f010128f:	83 ec 04             	sub    $0x4,%esp
f0101292:	68 79 80 10 f0       	push   $0xf0108079
f0101297:	68 de 01 00 00       	push   $0x1de
f010129c:	68 61 7f 10 f0       	push   $0xf0107f61
f01012a1:	e8 a6 ed ff ff       	call   f010004c <_panic>
	}

	if(pp->pp_link != NULL){
f01012a6:	83 38 00             	cmpl   $0x0,(%eax)
f01012a9:	74 17                	je     f01012c2 <page_free+0x43>
		panic("pp->pp_link is not NULL");
f01012ab:	83 ec 04             	sub    $0x4,%esp
f01012ae:	68 8f 80 10 f0       	push   $0xf010808f
f01012b3:	68 e2 01 00 00       	push   $0x1e2
f01012b8:	68 61 7f 10 f0       	push   $0xf0107f61
f01012bd:	e8 8a ed ff ff       	call   f010004c <_panic>
	}

	if(page_free_list == NULL){
f01012c2:	8b 15 40 22 27 f0    	mov    0xf0272240,%edx
f01012c8:	85 d2                	test   %edx,%edx
f01012ca:	75 07                	jne    f01012d3 <page_free+0x54>
		page_free_list = pp;
f01012cc:	a3 40 22 27 f0       	mov    %eax,0xf0272240
f01012d1:	eb 07                	jmp    f01012da <page_free+0x5b>
	}else{
		pp->pp_link = page_free_list;
f01012d3:	89 10                	mov    %edx,(%eax)
		page_free_list = pp;
f01012d5:	a3 40 22 27 f0       	mov    %eax,0xf0272240
	}
	//cprintf("Free-ed %x\n", pp);
}
f01012da:	c9                   	leave  
f01012db:	c3                   	ret    

f01012dc <page_decref>:
// Decrement the reference count on a page,
// freeing it if there are no more refs.
//
void
page_decref(struct PageInfo* pp)
{
f01012dc:	55                   	push   %ebp
f01012dd:	89 e5                	mov    %esp,%ebp
f01012df:	83 ec 08             	sub    $0x8,%esp
f01012e2:	8b 55 08             	mov    0x8(%ebp),%edx
	if (--pp->pp_ref == 0)
f01012e5:	8b 42 04             	mov    0x4(%edx),%eax
f01012e8:	48                   	dec    %eax
f01012e9:	66 89 42 04          	mov    %ax,0x4(%edx)
f01012ed:	66 85 c0             	test   %ax,%ax
f01012f0:	75 0c                	jne    f01012fe <page_decref+0x22>
		page_free(pp);
f01012f2:	83 ec 0c             	sub    $0xc,%esp
f01012f5:	52                   	push   %edx
f01012f6:	e8 84 ff ff ff       	call   f010127f <page_free>
f01012fb:	83 c4 10             	add    $0x10,%esp
}
f01012fe:	c9                   	leave  
f01012ff:	c3                   	ret    

f0101300 <pgdir_walk>:
// Hint 3: look at inc/mmu.h for useful macros that mainipulate page
// table and page directory entries.
//
pte_t *
pgdir_walk(pde_t *pgdir, const void *va, int create)
{
f0101300:	55                   	push   %ebp
f0101301:	89 e5                	mov    %esp,%ebp
f0101303:	53                   	push   %ebx
f0101304:	83 ec 04             	sub    $0x4,%esp
f0101307:	8b 45 08             	mov    0x8(%ebp),%eax
	//pte_t* pte_Ptr = (pte_t*)(KADDR(pde) + PTX(va));
	return pte_Ptr;
	*/

	//reference code:
	assert(pgdir);
f010130a:	85 c0                	test   %eax,%eax
f010130c:	75 19                	jne    f0101327 <pgdir_walk+0x27>
f010130e:	68 a7 80 10 f0       	push   $0xf01080a7
f0101313:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101318:	68 37 02 00 00       	push   $0x237
f010131d:	68 61 7f 10 f0       	push   $0xf0107f61
f0101322:	e8 25 ed ff ff       	call   f010004c <_panic>

	// Page directory index.
	size_t pdx = PDX(va);

	// Page directory entry.
	pde_t pde = pgdir[pdx];
f0101327:	8b 55 0c             	mov    0xc(%ebp),%edx
f010132a:	c1 ea 16             	shr    $0x16,%edx
f010132d:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
f0101330:	8b 03                	mov    (%ebx),%eax

	// Page table present?
	// pt is a (kernel) virtual address.
	if (pde & PTE_P) {
f0101332:	a8 01                	test   $0x1,%al
f0101334:	74 2f                	je     f0101365 <pgdir_walk+0x65>
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
f0101336:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010133b:	89 c2                	mov    %eax,%edx
f010133d:	c1 ea 0c             	shr    $0xc,%edx
f0101340:	39 15 24 34 27 f0    	cmp    %edx,0xf0273424
f0101346:	77 15                	ja     f010135d <pgdir_walk+0x5d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101348:	50                   	push   %eax
f0101349:	68 68 6f 10 f0       	push   $0xf0106f68
f010134e:	68 49 02 00 00       	push   $0x249
f0101353:	68 61 7f 10 f0       	push   $0xf0107f61
f0101358:	e8 ef ec ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f010135d:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx
f0101363:	eb 5f                	jmp    f01013c4 <pgdir_walk+0xc4>
	} else {
		if (!create)
f0101365:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0101369:	74 68                	je     f01013d3 <pgdir_walk+0xd3>
			return NULL;
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
f010136b:	83 ec 0c             	sub    $0xc,%esp
f010136e:	6a 01                	push   $0x1
f0101370:	e8 94 fe ff ff       	call   f0101209 <page_alloc>
		if (!page)
f0101375:	83 c4 10             	add    $0x10,%esp
f0101378:	85 c0                	test   %eax,%eax
f010137a:	74 5e                	je     f01013da <pgdir_walk+0xda>
			return NULL;

		// The new page has these permissions for convenience.
		pgdir[pdx] = page2pa(page) | PTE_U | PTE_W | PTE_P;
f010137c:	89 c2                	mov    %eax,%edx
f010137e:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f0101384:	c1 fa 03             	sar    $0x3,%edx
f0101387:	c1 e2 0c             	shl    $0xc,%edx
f010138a:	83 ca 07             	or     $0x7,%edx
f010138d:	89 13                	mov    %edx,(%ebx)

		// It appeared in page directory, so increment refcount.
		page->pp_ref++;
f010138f:	66 ff 40 04          	incw   0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101393:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0101399:	c1 f8 03             	sar    $0x3,%eax
f010139c:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010139f:	89 c2                	mov    %eax,%edx
f01013a1:	c1 ea 0c             	shr    $0xc,%edx
f01013a4:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f01013aa:	72 12                	jb     f01013be <pgdir_walk+0xbe>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01013ac:	50                   	push   %eax
f01013ad:	68 68 6f 10 f0       	push   $0xf0106f68
f01013b2:	6a 58                	push   $0x58
f01013b4:	68 6d 7f 10 f0       	push   $0xf0107f6d
f01013b9:	e8 8e ec ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01013be:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx

	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
f01013c4:	8b 45 0c             	mov    0xc(%ebp),%eax
f01013c7:	c1 e8 0a             	shr    $0xa,%eax
f01013ca:	25 fc 0f 00 00       	and    $0xffc,%eax
f01013cf:	01 d0                	add    %edx,%eax
f01013d1:	eb 0c                	jmp    f01013df <pgdir_walk+0xdf>
	if (pde & PTE_P) {
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
	} else {
		if (!create)
			return NULL;
f01013d3:	b8 00 00 00 00       	mov    $0x0,%eax
f01013d8:	eb 05                	jmp    f01013df <pgdir_walk+0xdf>
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
		if (!page)
			return NULL;
f01013da:	b8 00 00 00 00       	mov    $0x0,%eax
	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
}
f01013df:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01013e2:	c9                   	leave  
f01013e3:	c3                   	ret    

f01013e4 <boot_map_region>:
// mapped pages.
//
// Hint: the TA solution uses pgdir_walk
static void
boot_map_region(pde_t *pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm)
{
f01013e4:	55                   	push   %ebp
f01013e5:	89 e5                	mov    %esp,%ebp
f01013e7:	57                   	push   %edi
f01013e8:	56                   	push   %esi
f01013e9:	53                   	push   %ebx
f01013ea:	83 ec 1c             	sub    $0x1c,%esp
f01013ed:	89 c3                	mov    %eax,%ebx
f01013ef:	89 45 e0             	mov    %eax,-0x20(%ebp)
f01013f2:	89 cf                	mov    %ecx,%edi
f01013f4:	8b 45 08             	mov    0x8(%ebp),%eax
		//cprintf("pte boot_map_region get is: %x\n", (*pte) );
		//page_insert(pgdir, pa2page(pa+i*PGSIZE), (void*)(va+i*PGSIZE), perm);
//	}
	
	//reference code:
	assert(pgdir);
f01013f7:	85 db                	test   %ebx,%ebx
f01013f9:	75 19                	jne    f0101414 <boot_map_region+0x30>
f01013fb:	68 a7 80 10 f0       	push   $0xf01080a7
f0101400:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101405:	68 88 02 00 00       	push   $0x288
f010140a:	68 61 7f 10 f0       	push   $0xf0107f61
f010140f:	e8 38 ec ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(va, PGSIZE) == va);
f0101414:	f7 c2 ff 0f 00 00    	test   $0xfff,%edx
f010141a:	74 19                	je     f0101435 <boot_map_region+0x51>
f010141c:	68 ad 80 10 f0       	push   $0xf01080ad
f0101421:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101426:	68 89 02 00 00       	push   $0x289
f010142b:	68 61 7f 10 f0       	push   $0xf0107f61
f0101430:	e8 17 ec ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(size, PGSIZE) == size);
f0101435:	f7 c7 ff 0f 00 00    	test   $0xfff,%edi
f010143b:	74 19                	je     f0101456 <boot_map_region+0x72>
f010143d:	68 14 77 10 f0       	push   $0xf0107714
f0101442:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101447:	68 8a 02 00 00       	push   $0x28a
f010144c:	68 61 7f 10 f0       	push   $0xf0107f61
f0101451:	e8 f6 eb ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(pa, PGSIZE) == pa);
f0101456:	a9 ff 0f 00 00       	test   $0xfff,%eax
f010145b:	74 3d                	je     f010149a <boot_map_region+0xb6>
f010145d:	68 c9 80 10 f0       	push   $0xf01080c9
f0101462:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101467:	68 8b 02 00 00       	push   $0x28b
f010146c:	68 61 7f 10 f0       	push   $0xf0107f61
f0101471:	e8 d6 eb ff ff       	call   f010004c <_panic>

	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);
f0101476:	83 ec 04             	sub    $0x4,%esp
f0101479:	6a 01                	push   $0x1
f010147b:	53                   	push   %ebx
f010147c:	ff 75 e0             	pushl  -0x20(%ebp)
f010147f:	e8 7c fe ff ff       	call   f0101300 <pgdir_walk>

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P | PTE_W;
f0101484:	0b 75 dc             	or     -0x24(%ebp),%esi
f0101487:	89 30                	mov    %esi,(%eax)

		// Next page.
		va += PGSIZE;
f0101489:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		pa += PGSIZE;
		size -= PGSIZE;
f010148f:	81 ef 00 10 00 00    	sub    $0x1000,%edi
f0101495:	83 c4 10             	add    $0x10,%esp
f0101498:	eb 10                	jmp    f01014aa <boot_map_region+0xc6>
f010149a:	89 d3                	mov    %edx,%ebx
f010149c:	29 d0                	sub    %edx,%eax
f010149e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P | PTE_W;
f01014a1:	8b 45 0c             	mov    0xc(%ebp),%eax
f01014a4:	83 c8 03             	or     $0x3,%eax
f01014a7:	89 45 dc             	mov    %eax,-0x24(%ebp)
f01014aa:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01014ad:	8d 34 18             	lea    (%eax,%ebx,1),%esi
	assert(pgdir);
	assert(ROUNDDOWN(va, PGSIZE) == va);
	assert(ROUNDDOWN(size, PGSIZE) == size);
	assert(ROUNDDOWN(pa, PGSIZE) == pa);

	while (size >= PGSIZE) {
f01014b0:	81 ff ff 0f 00 00    	cmp    $0xfff,%edi
f01014b6:	77 be                	ja     f0101476 <boot_map_region+0x92>
		// Next page.
		va += PGSIZE;
		pa += PGSIZE;
		size -= PGSIZE;
	}
}
f01014b8:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01014bb:	5b                   	pop    %ebx
f01014bc:	5e                   	pop    %esi
f01014bd:	5f                   	pop    %edi
f01014be:	5d                   	pop    %ebp
f01014bf:	c3                   	ret    

f01014c0 <page_lookup>:
//
// Hint: the TA solution uses pgdir_walk and pa2page.
//
struct PageInfo *
page_lookup(pde_t *pgdir, void *va, pte_t **pte_store)
{
f01014c0:	55                   	push   %ebp
f01014c1:	89 e5                	mov    %esp,%ebp
f01014c3:	53                   	push   %ebx
f01014c4:	83 ec 08             	sub    $0x8,%esp
f01014c7:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f01014ca:	6a 00                	push   $0x0
f01014cc:	ff 75 0c             	pushl  0xc(%ebp)
f01014cf:	ff 75 08             	pushl  0x8(%ebp)
f01014d2:	e8 29 fe ff ff       	call   f0101300 <pgdir_walk>
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
f01014d7:	83 c4 10             	add    $0x10,%esp
f01014da:	85 c0                	test   %eax,%eax
f01014dc:	74 5a                	je     f0101538 <page_lookup+0x78>
		return NULL; //no page mapped at va (not even a table)
	}

	if(pte_store != 0){
f01014de:	85 db                	test   %ebx,%ebx
f01014e0:	74 02                	je     f01014e4 <page_lookup+0x24>
		*pte_store = pte_Ptr;
f01014e2:	89 03                	mov    %eax,(%ebx)
	}

	pte_t pte = *pte_Ptr;
f01014e4:	8b 00                	mov    (%eax),%eax
	if(pte & PTE_P){ //if present
f01014e6:	a8 01                	test   $0x1,%al
f01014e8:	74 55                	je     f010153f <page_lookup+0x7f>
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
f01014ea:	8b 15 24 34 27 f0    	mov    0xf0273424,%edx
f01014f0:	89 c1                	mov    %eax,%ecx
f01014f2:	c1 e9 0c             	shr    $0xc,%ecx
f01014f5:	39 d1                	cmp    %edx,%ecx
f01014f7:	72 19                	jb     f0101512 <page_lookup+0x52>
f01014f9:	68 34 77 10 f0       	push   $0xf0107734
f01014fe:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101503:	68 e3 02 00 00       	push   $0x2e3
f0101508:	68 61 7f 10 f0       	push   $0xf0107f61
f010150d:	e8 3a eb ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101512:	c1 e8 0c             	shr    $0xc,%eax
f0101515:	39 c2                	cmp    %eax,%edx
f0101517:	77 14                	ja     f010152d <page_lookup+0x6d>
		panic("pa2page called with invalid pa");
f0101519:	83 ec 04             	sub    $0x4,%esp
f010151c:	68 f4 76 10 f0       	push   $0xf01076f4
f0101521:	6a 51                	push   $0x51
f0101523:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0101528:	e8 1f eb ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f010152d:	8b 15 2c 34 27 f0    	mov    0xf027342c,%edx
f0101533:	8d 04 c2             	lea    (%edx,%eax,8),%eax
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
f0101536:	eb 0c                	jmp    f0101544 <page_lookup+0x84>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
		return NULL; //no page mapped at va (not even a table)
f0101538:	b8 00 00 00 00       	mov    $0x0,%eax
f010153d:	eb 05                	jmp    f0101544 <page_lookup+0x84>
	pte_t pte = *pte_Ptr;
	if(pte & PTE_P){ //if present
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
	}else{
		return NULL;
f010153f:	b8 00 00 00 00       	mov    $0x0,%eax
	}
}
f0101544:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101547:	c9                   	leave  
f0101548:	c3                   	ret    

f0101549 <print_fl>:

}

void
print_fl()
{	
f0101549:	55                   	push   %ebp
f010154a:	89 e5                	mov    %esp,%ebp
f010154c:	53                   	push   %ebx
f010154d:	83 ec 10             	sub    $0x10,%esp
	struct PageInfo *pp = page_free_list;
f0101550:	8b 1d 40 22 27 f0    	mov    0xf0272240,%ebx
	cprintf("Freelist is: ");
f0101556:	68 e5 80 10 f0       	push   $0xf01080e5
f010155b:	e8 98 26 00 00       	call   f0103bf8 <cprintf>
	while(pp != NULL){
f0101560:	83 c4 10             	add    $0x10,%esp
f0101563:	eb 1e                	jmp    f0101583 <print_fl+0x3a>
		cprintf("-> p%x ", (pp-pages));
f0101565:	83 ec 08             	sub    $0x8,%esp
f0101568:	89 d8                	mov    %ebx,%eax
f010156a:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0101570:	c1 f8 03             	sar    $0x3,%eax
f0101573:	50                   	push   %eax
f0101574:	68 f3 80 10 f0       	push   $0xf01080f3
f0101579:	e8 7a 26 00 00       	call   f0103bf8 <cprintf>
		pp = pp->pp_link;
f010157e:	8b 1b                	mov    (%ebx),%ebx
f0101580:	83 c4 10             	add    $0x10,%esp
void
print_fl()
{	
	struct PageInfo *pp = page_free_list;
	cprintf("Freelist is: ");
	while(pp != NULL){
f0101583:	85 db                	test   %ebx,%ebx
f0101585:	75 de                	jne    f0101565 <print_fl+0x1c>
		cprintf("-> p%x ", (pp-pages));
		pp = pp->pp_link;
	}
	cprintf("->end\n");
f0101587:	83 ec 0c             	sub    $0xc,%esp
f010158a:	68 fb 80 10 f0       	push   $0xf01080fb
f010158f:	e8 64 26 00 00       	call   f0103bf8 <cprintf>
}
f0101594:	83 c4 10             	add    $0x10,%esp
f0101597:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010159a:	c9                   	leave  
f010159b:	c3                   	ret    

f010159c <tlb_invalidate>:
// Invalidate a TLB entry, but only if the page tables being
// edited are the ones currently in use by the processor.
//
void
tlb_invalidate(pde_t *pgdir, void *va)
{
f010159c:	55                   	push   %ebp
f010159d:	89 e5                	mov    %esp,%ebp
f010159f:	83 ec 08             	sub    $0x8,%esp
	// Flush the entry only if we're modifying the current address space.
	if (!curenv || curenv->env_pgdir == pgdir)
f01015a2:	e8 63 51 00 00       	call   f010670a <cpunum>
f01015a7:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01015aa:	01 c2                	add    %eax,%edx
f01015ac:	01 d2                	add    %edx,%edx
f01015ae:	01 c2                	add    %eax,%edx
f01015b0:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01015b3:	83 3c 85 08 40 27 f0 	cmpl   $0x0,-0xfd8bff8(,%eax,4)
f01015ba:	00 
f01015bb:	74 20                	je     f01015dd <tlb_invalidate+0x41>
f01015bd:	e8 48 51 00 00       	call   f010670a <cpunum>
f01015c2:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01015c5:	01 c2                	add    %eax,%edx
f01015c7:	01 d2                	add    %edx,%edx
f01015c9:	01 c2                	add    %eax,%edx
f01015cb:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01015ce:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f01015d5:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01015d8:	39 48 60             	cmp    %ecx,0x60(%eax)
f01015db:	75 06                	jne    f01015e3 <tlb_invalidate+0x47>
}

static inline void
invlpg(void *addr)
{
	asm volatile("invlpg (%0)" : : "r" (addr) : "memory");
f01015dd:	8b 45 0c             	mov    0xc(%ebp),%eax
f01015e0:	0f 01 38             	invlpg (%eax)
		invlpg(va);
}
f01015e3:	c9                   	leave  
f01015e4:	c3                   	ret    

f01015e5 <page_remove>:
// Hint: The TA solution is implemented using page_lookup,
// 	tlb_invalidate, and page_decref.
//
void
page_remove(pde_t *pgdir, void *va)
{
f01015e5:	55                   	push   %ebp
f01015e6:	89 e5                	mov    %esp,%ebp
f01015e8:	56                   	push   %esi
f01015e9:	53                   	push   %ebx
f01015ea:	8b 5d 08             	mov    0x8(%ebp),%ebx
f01015ed:	8b 75 0c             	mov    0xc(%ebp),%esi
	// Fill this function in
	//pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	if( page_lookup(pgdir, va, 0) == NULL){
f01015f0:	83 ec 04             	sub    $0x4,%esp
f01015f3:	6a 00                	push   $0x0
f01015f5:	56                   	push   %esi
f01015f6:	53                   	push   %ebx
f01015f7:	e8 c4 fe ff ff       	call   f01014c0 <page_lookup>
f01015fc:	83 c4 10             	add    $0x10,%esp
f01015ff:	85 c0                	test   %eax,%eax
f0101601:	74 7b                	je     f010167e <page_remove+0x99>
		return;
	}
	tlb_invalidate(pgdir, va);
f0101603:	83 ec 08             	sub    $0x8,%esp
f0101606:	56                   	push   %esi
f0101607:	53                   	push   %ebx
f0101608:	e8 8f ff ff ff       	call   f010159c <tlb_invalidate>
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f010160d:	83 c4 0c             	add    $0xc,%esp
f0101610:	6a 00                	push   $0x0
f0101612:	56                   	push   %esi
f0101613:	53                   	push   %ebx
f0101614:	e8 e7 fc ff ff       	call   f0101300 <pgdir_walk>
	pte_t pte = *pte_Ptr;
f0101619:	8b 10                	mov    (%eax),%edx
	*pte_Ptr = 0;
f010161b:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	assert(PGNUM(PTE_ADDR(pte)) < npages);
f0101621:	8b 0d 24 34 27 f0    	mov    0xf0273424,%ecx
f0101627:	89 d0                	mov    %edx,%eax
f0101629:	c1 e8 0c             	shr    $0xc,%eax
f010162c:	83 c4 10             	add    $0x10,%esp
f010162f:	39 c8                	cmp    %ecx,%eax
f0101631:	72 19                	jb     f010164c <page_remove+0x67>
f0101633:	68 02 81 10 f0       	push   $0xf0108102
f0101638:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010163d:	68 05 03 00 00       	push   $0x305
f0101642:	68 61 7f 10 f0       	push   $0xf0107f61
f0101647:	e8 00 ea ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010164c:	89 d0                	mov    %edx,%eax
f010164e:	c1 e8 0c             	shr    $0xc,%eax
f0101651:	39 c1                	cmp    %eax,%ecx
f0101653:	77 14                	ja     f0101669 <page_remove+0x84>
		panic("pa2page called with invalid pa");
f0101655:	83 ec 04             	sub    $0x4,%esp
f0101658:	68 f4 76 10 f0       	push   $0xf01076f4
f010165d:	6a 51                	push   $0x51
f010165f:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0101664:	e8 e3 e9 ff ff       	call   f010004c <_panic>
	struct PageInfo* pp = pa2page( PTE_ADDR(pte));
	page_decref(pp);
f0101669:	83 ec 0c             	sub    $0xc,%esp
f010166c:	8b 15 2c 34 27 f0    	mov    0xf027342c,%edx
f0101672:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f0101675:	50                   	push   %eax
f0101676:	e8 61 fc ff ff       	call   f01012dc <page_decref>
f010167b:	83 c4 10             	add    $0x10,%esp

}
f010167e:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0101681:	5b                   	pop    %ebx
f0101682:	5e                   	pop    %esi
f0101683:	5d                   	pop    %ebp
f0101684:	c3                   	ret    

f0101685 <page_insert>:
// Hint: The TA solution is implemented using pgdir_walk, page_remove,
// and page2pa.
//
int
page_insert(pde_t *pgdir, struct PageInfo *pp, void *va, int perm)
{
f0101685:	55                   	push   %ebp
f0101686:	89 e5                	mov    %esp,%ebp
f0101688:	57                   	push   %edi
f0101689:	56                   	push   %esi
f010168a:	53                   	push   %ebx
f010168b:	83 ec 10             	sub    $0x10,%esp
f010168e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0101691:	8b 7d 10             	mov    0x10(%ebp),%edi
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
f0101694:	6a 01                	push   $0x1
f0101696:	57                   	push   %edi
f0101697:	ff 75 08             	pushl  0x8(%ebp)
f010169a:	e8 61 fc ff ff       	call   f0101300 <pgdir_walk>
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
f010169f:	83 c4 10             	add    $0x10,%esp
f01016a2:	85 c0                	test   %eax,%eax
f01016a4:	74 38                	je     f01016de <page_insert+0x59>
f01016a6:	89 c6                	mov    %eax,%esi
		return -E_NO_MEM; //page table couldn't be allocated
	}
	pte_t pte = *pte_Ptr;
f01016a8:	8b 00                	mov    (%eax),%eax
	pp->pp_ref++;
f01016aa:	66 ff 43 04          	incw   0x4(%ebx)
	if(pte & PTE_P){ //check if there is already a page mapped at 'va'
f01016ae:	a8 01                	test   $0x1,%al
f01016b0:	74 0f                	je     f01016c1 <page_insert+0x3c>
		page_remove(pgdir, va); //remove the exising page
f01016b2:	83 ec 08             	sub    $0x8,%esp
f01016b5:	57                   	push   %edi
f01016b6:	ff 75 08             	pushl  0x8(%ebp)
f01016b9:	e8 27 ff ff ff       	call   f01015e5 <page_remove>
f01016be:	83 c4 10             	add    $0x10,%esp
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
f01016c1:	2b 1d 2c 34 27 f0    	sub    0xf027342c,%ebx
f01016c7:	c1 fb 03             	sar    $0x3,%ebx
f01016ca:	c1 e3 0c             	shl    $0xc,%ebx
f01016cd:	8b 45 14             	mov    0x14(%ebp),%eax
f01016d0:	83 c8 01             	or     $0x1,%eax
f01016d3:	09 c3                	or     %eax,%ebx
f01016d5:	89 1e                	mov    %ebx,(%esi)
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
f01016d7:	b8 00 00 00 00       	mov    $0x0,%eax
f01016dc:	eb 05                	jmp    f01016e3 <page_insert+0x5e>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
		return -E_NO_MEM; //page table couldn't be allocated
f01016de:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
}
f01016e3:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01016e6:	5b                   	pop    %ebx
f01016e7:	5e                   	pop    %esi
f01016e8:	5f                   	pop    %edi
f01016e9:	5d                   	pop    %ebp
f01016ea:	c3                   	ret    

f01016eb <mmio_map_region>:
// location.  Return the base of the reserved region.  size does *not*
// have to be multiple of PGSIZE.
//
void *
mmio_map_region(physaddr_t pa, size_t size)
{
f01016eb:	55                   	push   %ebp
f01016ec:	89 e5                	mov    %esp,%ebp
f01016ee:	53                   	push   %ebx
f01016ef:	83 ec 04             	sub    $0x4,%esp
f01016f2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//
	// Your code here:

	//round up the size
	//size_t round_Up_Size;
	if(size % PGSIZE != 0){
f01016f5:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
f01016fb:	74 0c                	je     f0101709 <mmio_map_region+0x1e>
		size = ((size / PGSIZE) + 1)*PGSIZE;
f01016fd:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
f0101703:	8d 9b 00 10 00 00    	lea    0x1000(%ebx),%ebx
	}//else{
	//cprintf("In mmio_map_region, the rounded size is: %d\n\n\n", size);
	//	round_Up_Size = size;
	//}
	//check overflow
	if(base + size > MMIOLIM){
f0101709:	8b 15 00 33 12 f0    	mov    0xf0123300,%edx
f010170f:	8d 04 13             	lea    (%ebx,%edx,1),%eax
f0101712:	3d 00 00 c0 ef       	cmp    $0xefc00000,%eax
f0101717:	76 17                	jbe    f0101730 <mmio_map_region+0x45>
		panic("MMIOLIM overflow!!\n");
f0101719:	83 ec 04             	sub    $0x4,%esp
f010171c:	68 20 81 10 f0       	push   $0xf0108120
f0101721:	68 4d 03 00 00       	push   $0x34d
f0101726:	68 61 7f 10 f0       	push   $0xf0107f61
f010172b:	e8 1c e9 ff ff       	call   f010004c <_panic>
	}
	//boot_map_region(pde_t *pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm)
	boot_map_region(kern_pgdir, base, size, pa, PTE_PCD|PTE_PWT|PTE_P|PTE_W);
f0101730:	83 ec 08             	sub    $0x8,%esp
f0101733:	6a 1b                	push   $0x1b
f0101735:	ff 75 08             	pushl  0x8(%ebp)
f0101738:	89 d9                	mov    %ebx,%ecx
f010173a:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f010173f:	e8 a0 fc ff ff       	call   f01013e4 <boot_map_region>
	ret = base;
f0101744:	a1 00 33 12 f0       	mov    0xf0123300,%eax
	base += size;
f0101749:	01 c3                	add    %eax,%ebx
f010174b:	89 1d 00 33 12 f0    	mov    %ebx,0xf0123300
	return ( (void*)ret );
	//panic("mmio_map_region not implemented");
}
f0101751:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101754:	c9                   	leave  
f0101755:	c3                   	ret    

f0101756 <mem_init>:
//
// From UTOP to ULIM, the user is allowed to read but not write.
// Above ULIM the user cannot read or write.
void
mem_init(void)
{
f0101756:	55                   	push   %ebp
f0101757:	89 e5                	mov    %esp,%ebp
f0101759:	57                   	push   %edi
f010175a:	56                   	push   %esi
f010175b:	53                   	push   %ebx
f010175c:	83 ec 3c             	sub    $0x3c,%esp
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f010175f:	8b 1d 20 2f 27 f0    	mov    0xf0272f20,%ebx
static void
detect_memory(void)
{
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;
f0101765:	be 00 00 00 00       	mov    $0x0,%esi

	e = e820_map.entries;
f010176a:	b8 24 2f 27 f0       	mov    $0xf0272f24,%eax
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f010176f:	b9 00 00 00 00       	mov    $0x0,%ecx
f0101774:	eb 1d                	jmp    f0101793 <mem_init+0x3d>
		if (e->addr >= mem_max)
f0101776:	8b 10                	mov    (%eax),%edx
f0101778:	83 78 04 00          	cmpl   $0x0,0x4(%eax)
f010177c:	75 11                	jne    f010178f <mem_init+0x39>
f010177e:	81 fa ff ff ff 0f    	cmp    $0xfffffff,%edx
f0101784:	77 09                	ja     f010178f <mem_init+0x39>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
f0101786:	03 50 08             	add    0x8(%eax),%edx
f0101789:	39 d6                	cmp    %edx,%esi
f010178b:	73 02                	jae    f010178f <mem_init+0x39>
f010178d:	89 d6                	mov    %edx,%esi
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f010178f:	41                   	inc    %ecx
f0101790:	83 c0 14             	add    $0x14,%eax
f0101793:	39 d9                	cmp    %ebx,%ecx
f0101795:	75 df                	jne    f0101776 <mem_init+0x20>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
	}

	// Limit memory to 256MB.
	mem = MIN(mem, mem_max);
f0101797:	89 f0                	mov    %esi,%eax
f0101799:	81 fe 00 00 00 10    	cmp    $0x10000000,%esi
f010179f:	76 05                	jbe    f01017a6 <mem_init+0x50>
f01017a1:	b8 00 00 00 10       	mov    $0x10000000,%eax
	npages = mem / PGSIZE;
f01017a6:	89 c2                	mov    %eax,%edx
f01017a8:	c1 ea 0c             	shr    $0xc,%edx
f01017ab:	89 15 24 34 27 f0    	mov    %edx,0xf0273424
	cprintf("E820: physical memory %uMB\n", mem / 1024 / 1024);
f01017b1:	83 ec 08             	sub    $0x8,%esp
f01017b4:	c1 e8 14             	shr    $0x14,%eax
f01017b7:	50                   	push   %eax
f01017b8:	68 34 81 10 f0       	push   $0xf0108134
f01017bd:	e8 36 24 00 00       	call   f0103bf8 <cprintf>
	// Remove this line when you're ready to test this function.
	//panic("mem_init: This function is not finished\n");

	//////////////////////////////////////////////////////////////////////
	// create initial page directory.
	kern_pgdir = (pde_t *) boot_alloc(PGSIZE);
f01017c2:	b8 00 10 00 00       	mov    $0x1000,%eax
f01017c7:	e8 fe f4 ff ff       	call   f0100cca <boot_alloc>
f01017cc:	a3 28 34 27 f0       	mov    %eax,0xf0273428
	memset(kern_pgdir, 0, PGSIZE);
f01017d1:	83 c4 0c             	add    $0xc,%esp
f01017d4:	68 00 10 00 00       	push   $0x1000
f01017d9:	6a 00                	push   $0x0
f01017db:	50                   	push   %eax
f01017dc:	e8 23 48 00 00       	call   f0106004 <memset>
	// a virtual page table at virtual address UVPT.
	// (For now, you don't have understand the greater purpose of the
	// following line.)

	// Permissions: kernel R, user R
	kern_pgdir[PDX(UVPT)] = PADDR(kern_pgdir) | PTE_U | PTE_P;
f01017e1:	a1 28 34 27 f0       	mov    0xf0273428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01017e6:	83 c4 10             	add    $0x10,%esp
f01017e9:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01017ee:	77 15                	ja     f0101805 <mem_init+0xaf>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01017f0:	50                   	push   %eax
f01017f1:	68 8c 6f 10 f0       	push   $0xf0106f8c
f01017f6:	68 8e 00 00 00       	push   $0x8e
f01017fb:	68 61 7f 10 f0       	push   $0xf0107f61
f0101800:	e8 47 e8 ff ff       	call   f010004c <_panic>
f0101805:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f010180b:	83 ca 05             	or     $0x5,%edx
f010180e:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)
	// The kernel uses this array to keep track of physical pages: for
	// each physical page, there is a corresponding struct PageInfo in this
	// array.  'npages' is the number of physical pages in memory.  Use memset
	// to initialize all fields of each struct PageInfo to 0.
	// Your code goes here:
	pages = boot_alloc(npages * sizeof(struct PageInfo)); 
f0101814:	a1 24 34 27 f0       	mov    0xf0273424,%eax
f0101819:	c1 e0 03             	shl    $0x3,%eax
f010181c:	e8 a9 f4 ff ff       	call   f0100cca <boot_alloc>
f0101821:	a3 2c 34 27 f0       	mov    %eax,0xf027342c

	//Lab3 Exercise 1: allocate and map the envs array
	//pages = boot_alloc(npages * sizeof(struct PageInfo)); 
	envs = (struct Env *)boot_alloc(NENV * sizeof(struct Env));
f0101826:	b8 00 f0 01 00       	mov    $0x1f000,%eax
f010182b:	e8 9a f4 ff ff       	call   f0100cca <boot_alloc>
f0101830:	a3 44 22 27 f0       	mov    %eax,0xf0272244

	//initialize all fields of each struct PageInfo to 0.
	memset(pages, 0, npages * sizeof(struct PageInfo)); //npages * 8 is the total number of bytes
f0101835:	83 ec 04             	sub    $0x4,%esp
f0101838:	a1 24 34 27 f0       	mov    0xf0273424,%eax
f010183d:	c1 e0 03             	shl    $0x3,%eax
f0101840:	50                   	push   %eax
f0101841:	6a 00                	push   $0x0
f0101843:	ff 35 2c 34 27 f0    	pushl  0xf027342c
f0101849:	e8 b6 47 00 00       	call   f0106004 <memset>
	// Now that we've allocated the initial kernel data structures, we set
	// up the list of free physical pages. Once we've done so, all further
	// memory management will go through the page_* functions. In
	// particular, we can now map memory using boot_map_region
	// or page_insert
	page_init();
f010184e:	e8 0d f8 ff ff       	call   f0101060 <page_init>

	check_page_free_list(1);
f0101853:	b8 01 00 00 00       	mov    $0x1,%eax
f0101858:	e8 1a f5 ff ff       	call   f0100d77 <check_page_free_list>
	int nfree;
	struct PageInfo *fl;
	char *c;
	int i;

	if (!pages)
f010185d:	83 c4 10             	add    $0x10,%esp
f0101860:	83 3d 2c 34 27 f0 00 	cmpl   $0x0,0xf027342c
f0101867:	75 17                	jne    f0101880 <mem_init+0x12a>
		panic("'pages' is a null pointer!");
f0101869:	83 ec 04             	sub    $0x4,%esp
f010186c:	68 50 81 10 f0       	push   $0xf0108150
f0101871:	68 fa 03 00 00       	push   $0x3fa
f0101876:	68 61 7f 10 f0       	push   $0xf0107f61
f010187b:	e8 cc e7 ff ff       	call   f010004c <_panic>

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f0101880:	a1 40 22 27 f0       	mov    0xf0272240,%eax
f0101885:	bb 00 00 00 00       	mov    $0x0,%ebx
f010188a:	eb 03                	jmp    f010188f <mem_init+0x139>
		++nfree;
f010188c:	43                   	inc    %ebx

	if (!pages)
		panic("'pages' is a null pointer!");

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f010188d:	8b 00                	mov    (%eax),%eax
f010188f:	85 c0                	test   %eax,%eax
f0101891:	75 f9                	jne    f010188c <mem_init+0x136>
		++nfree;

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101893:	83 ec 0c             	sub    $0xc,%esp
f0101896:	6a 00                	push   $0x0
f0101898:	e8 6c f9 ff ff       	call   f0101209 <page_alloc>
f010189d:	89 c7                	mov    %eax,%edi
f010189f:	83 c4 10             	add    $0x10,%esp
f01018a2:	85 c0                	test   %eax,%eax
f01018a4:	75 19                	jne    f01018bf <mem_init+0x169>
f01018a6:	68 6b 81 10 f0       	push   $0xf010816b
f01018ab:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01018b0:	68 02 04 00 00       	push   $0x402
f01018b5:	68 61 7f 10 f0       	push   $0xf0107f61
f01018ba:	e8 8d e7 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f01018bf:	83 ec 0c             	sub    $0xc,%esp
f01018c2:	6a 00                	push   $0x0
f01018c4:	e8 40 f9 ff ff       	call   f0101209 <page_alloc>
f01018c9:	89 c6                	mov    %eax,%esi
f01018cb:	83 c4 10             	add    $0x10,%esp
f01018ce:	85 c0                	test   %eax,%eax
f01018d0:	75 19                	jne    f01018eb <mem_init+0x195>
f01018d2:	68 81 81 10 f0       	push   $0xf0108181
f01018d7:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01018dc:	68 03 04 00 00       	push   $0x403
f01018e1:	68 61 7f 10 f0       	push   $0xf0107f61
f01018e6:	e8 61 e7 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01018eb:	83 ec 0c             	sub    $0xc,%esp
f01018ee:	6a 00                	push   $0x0
f01018f0:	e8 14 f9 ff ff       	call   f0101209 <page_alloc>
f01018f5:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01018f8:	83 c4 10             	add    $0x10,%esp
f01018fb:	85 c0                	test   %eax,%eax
f01018fd:	75 19                	jne    f0101918 <mem_init+0x1c2>
f01018ff:	68 97 81 10 f0       	push   $0xf0108197
f0101904:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101909:	68 04 04 00 00       	push   $0x404
f010190e:	68 61 7f 10 f0       	push   $0xf0107f61
f0101913:	e8 34 e7 ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101918:	39 f7                	cmp    %esi,%edi
f010191a:	75 19                	jne    f0101935 <mem_init+0x1df>
f010191c:	68 ad 81 10 f0       	push   $0xf01081ad
f0101921:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101926:	68 07 04 00 00       	push   $0x407
f010192b:	68 61 7f 10 f0       	push   $0xf0107f61
f0101930:	e8 17 e7 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101935:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101938:	39 c6                	cmp    %eax,%esi
f010193a:	74 04                	je     f0101940 <mem_init+0x1ea>
f010193c:	39 c7                	cmp    %eax,%edi
f010193e:	75 19                	jne    f0101959 <mem_init+0x203>
f0101940:	68 54 77 10 f0       	push   $0xf0107754
f0101945:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010194a:	68 08 04 00 00       	push   $0x408
f010194f:	68 61 7f 10 f0       	push   $0xf0107f61
f0101954:	e8 f3 e6 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101959:	8b 0d 2c 34 27 f0    	mov    0xf027342c,%ecx
	assert(page2pa(pp0) < npages*PGSIZE);
f010195f:	8b 15 24 34 27 f0    	mov    0xf0273424,%edx
f0101965:	c1 e2 0c             	shl    $0xc,%edx
f0101968:	89 f8                	mov    %edi,%eax
f010196a:	29 c8                	sub    %ecx,%eax
f010196c:	c1 f8 03             	sar    $0x3,%eax
f010196f:	c1 e0 0c             	shl    $0xc,%eax
f0101972:	39 d0                	cmp    %edx,%eax
f0101974:	72 19                	jb     f010198f <mem_init+0x239>
f0101976:	68 bf 81 10 f0       	push   $0xf01081bf
f010197b:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101980:	68 09 04 00 00       	push   $0x409
f0101985:	68 61 7f 10 f0       	push   $0xf0107f61
f010198a:	e8 bd e6 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp1) < npages*PGSIZE);
f010198f:	89 f0                	mov    %esi,%eax
f0101991:	29 c8                	sub    %ecx,%eax
f0101993:	c1 f8 03             	sar    $0x3,%eax
f0101996:	c1 e0 0c             	shl    $0xc,%eax
f0101999:	39 c2                	cmp    %eax,%edx
f010199b:	77 19                	ja     f01019b6 <mem_init+0x260>
f010199d:	68 dc 81 10 f0       	push   $0xf01081dc
f01019a2:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01019a7:	68 0a 04 00 00       	push   $0x40a
f01019ac:	68 61 7f 10 f0       	push   $0xf0107f61
f01019b1:	e8 96 e6 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp2) < npages*PGSIZE);
f01019b6:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01019b9:	29 c8                	sub    %ecx,%eax
f01019bb:	c1 f8 03             	sar    $0x3,%eax
f01019be:	c1 e0 0c             	shl    $0xc,%eax
f01019c1:	39 c2                	cmp    %eax,%edx
f01019c3:	77 19                	ja     f01019de <mem_init+0x288>
f01019c5:	68 f9 81 10 f0       	push   $0xf01081f9
f01019ca:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01019cf:	68 0b 04 00 00       	push   $0x40b
f01019d4:	68 61 7f 10 f0       	push   $0xf0107f61
f01019d9:	e8 6e e6 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f01019de:	a1 40 22 27 f0       	mov    0xf0272240,%eax
f01019e3:	89 45 d0             	mov    %eax,-0x30(%ebp)
	page_free_list = 0;
f01019e6:	c7 05 40 22 27 f0 00 	movl   $0x0,0xf0272240
f01019ed:	00 00 00 

	// should be no free memory
	assert(!page_alloc(0));
f01019f0:	83 ec 0c             	sub    $0xc,%esp
f01019f3:	6a 00                	push   $0x0
f01019f5:	e8 0f f8 ff ff       	call   f0101209 <page_alloc>
f01019fa:	83 c4 10             	add    $0x10,%esp
f01019fd:	85 c0                	test   %eax,%eax
f01019ff:	74 19                	je     f0101a1a <mem_init+0x2c4>
f0101a01:	68 16 82 10 f0       	push   $0xf0108216
f0101a06:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101a0b:	68 12 04 00 00       	push   $0x412
f0101a10:	68 61 7f 10 f0       	push   $0xf0107f61
f0101a15:	e8 32 e6 ff ff       	call   f010004c <_panic>

	// free and re-allocate?
	page_free(pp0);
f0101a1a:	83 ec 0c             	sub    $0xc,%esp
f0101a1d:	57                   	push   %edi
f0101a1e:	e8 5c f8 ff ff       	call   f010127f <page_free>
	page_free(pp1);
f0101a23:	89 34 24             	mov    %esi,(%esp)
f0101a26:	e8 54 f8 ff ff       	call   f010127f <page_free>
	page_free(pp2);
f0101a2b:	83 c4 04             	add    $0x4,%esp
f0101a2e:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101a31:	e8 49 f8 ff ff       	call   f010127f <page_free>
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101a36:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101a3d:	e8 c7 f7 ff ff       	call   f0101209 <page_alloc>
f0101a42:	89 c6                	mov    %eax,%esi
f0101a44:	83 c4 10             	add    $0x10,%esp
f0101a47:	85 c0                	test   %eax,%eax
f0101a49:	75 19                	jne    f0101a64 <mem_init+0x30e>
f0101a4b:	68 6b 81 10 f0       	push   $0xf010816b
f0101a50:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101a55:	68 19 04 00 00       	push   $0x419
f0101a5a:	68 61 7f 10 f0       	push   $0xf0107f61
f0101a5f:	e8 e8 e5 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0101a64:	83 ec 0c             	sub    $0xc,%esp
f0101a67:	6a 00                	push   $0x0
f0101a69:	e8 9b f7 ff ff       	call   f0101209 <page_alloc>
f0101a6e:	89 c7                	mov    %eax,%edi
f0101a70:	83 c4 10             	add    $0x10,%esp
f0101a73:	85 c0                	test   %eax,%eax
f0101a75:	75 19                	jne    f0101a90 <mem_init+0x33a>
f0101a77:	68 81 81 10 f0       	push   $0xf0108181
f0101a7c:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101a81:	68 1a 04 00 00       	push   $0x41a
f0101a86:	68 61 7f 10 f0       	push   $0xf0107f61
f0101a8b:	e8 bc e5 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0101a90:	83 ec 0c             	sub    $0xc,%esp
f0101a93:	6a 00                	push   $0x0
f0101a95:	e8 6f f7 ff ff       	call   f0101209 <page_alloc>
f0101a9a:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101a9d:	83 c4 10             	add    $0x10,%esp
f0101aa0:	85 c0                	test   %eax,%eax
f0101aa2:	75 19                	jne    f0101abd <mem_init+0x367>
f0101aa4:	68 97 81 10 f0       	push   $0xf0108197
f0101aa9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101aae:	68 1b 04 00 00       	push   $0x41b
f0101ab3:	68 61 7f 10 f0       	push   $0xf0107f61
f0101ab8:	e8 8f e5 ff ff       	call   f010004c <_panic>
	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101abd:	39 fe                	cmp    %edi,%esi
f0101abf:	75 19                	jne    f0101ada <mem_init+0x384>
f0101ac1:	68 ad 81 10 f0       	push   $0xf01081ad
f0101ac6:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101acb:	68 1d 04 00 00       	push   $0x41d
f0101ad0:	68 61 7f 10 f0       	push   $0xf0107f61
f0101ad5:	e8 72 e5 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101ada:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101add:	39 c7                	cmp    %eax,%edi
f0101adf:	74 04                	je     f0101ae5 <mem_init+0x38f>
f0101ae1:	39 c6                	cmp    %eax,%esi
f0101ae3:	75 19                	jne    f0101afe <mem_init+0x3a8>
f0101ae5:	68 54 77 10 f0       	push   $0xf0107754
f0101aea:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101aef:	68 1e 04 00 00       	push   $0x41e
f0101af4:	68 61 7f 10 f0       	push   $0xf0107f61
f0101af9:	e8 4e e5 ff ff       	call   f010004c <_panic>
	assert(!page_alloc(0));
f0101afe:	83 ec 0c             	sub    $0xc,%esp
f0101b01:	6a 00                	push   $0x0
f0101b03:	e8 01 f7 ff ff       	call   f0101209 <page_alloc>
f0101b08:	83 c4 10             	add    $0x10,%esp
f0101b0b:	85 c0                	test   %eax,%eax
f0101b0d:	74 19                	je     f0101b28 <mem_init+0x3d2>
f0101b0f:	68 16 82 10 f0       	push   $0xf0108216
f0101b14:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101b19:	68 1f 04 00 00       	push   $0x41f
f0101b1e:	68 61 7f 10 f0       	push   $0xf0107f61
f0101b23:	e8 24 e5 ff ff       	call   f010004c <_panic>
f0101b28:	89 f0                	mov    %esi,%eax
f0101b2a:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0101b30:	c1 f8 03             	sar    $0x3,%eax
f0101b33:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101b36:	89 c2                	mov    %eax,%edx
f0101b38:	c1 ea 0c             	shr    $0xc,%edx
f0101b3b:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f0101b41:	72 12                	jb     f0101b55 <mem_init+0x3ff>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101b43:	50                   	push   %eax
f0101b44:	68 68 6f 10 f0       	push   $0xf0106f68
f0101b49:	6a 58                	push   $0x58
f0101b4b:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0101b50:	e8 f7 e4 ff ff       	call   f010004c <_panic>

	// test flags
	memset(page2kva(pp0), 1, PGSIZE);
f0101b55:	83 ec 04             	sub    $0x4,%esp
f0101b58:	68 00 10 00 00       	push   $0x1000
f0101b5d:	6a 01                	push   $0x1
f0101b5f:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101b64:	50                   	push   %eax
f0101b65:	e8 9a 44 00 00       	call   f0106004 <memset>
	page_free(pp0);
f0101b6a:	89 34 24             	mov    %esi,(%esp)
f0101b6d:	e8 0d f7 ff ff       	call   f010127f <page_free>
	assert((pp = page_alloc(ALLOC_ZERO)));
f0101b72:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
f0101b79:	e8 8b f6 ff ff       	call   f0101209 <page_alloc>
f0101b7e:	83 c4 10             	add    $0x10,%esp
f0101b81:	85 c0                	test   %eax,%eax
f0101b83:	75 19                	jne    f0101b9e <mem_init+0x448>
f0101b85:	68 25 82 10 f0       	push   $0xf0108225
f0101b8a:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101b8f:	68 24 04 00 00       	push   $0x424
f0101b94:	68 61 7f 10 f0       	push   $0xf0107f61
f0101b99:	e8 ae e4 ff ff       	call   f010004c <_panic>
	assert(pp && pp0 == pp);
f0101b9e:	39 c6                	cmp    %eax,%esi
f0101ba0:	74 19                	je     f0101bbb <mem_init+0x465>
f0101ba2:	68 43 82 10 f0       	push   $0xf0108243
f0101ba7:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101bac:	68 25 04 00 00       	push   $0x425
f0101bb1:	68 61 7f 10 f0       	push   $0xf0107f61
f0101bb6:	e8 91 e4 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101bbb:	89 f0                	mov    %esi,%eax
f0101bbd:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0101bc3:	c1 f8 03             	sar    $0x3,%eax
f0101bc6:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101bc9:	89 c2                	mov    %eax,%edx
f0101bcb:	c1 ea 0c             	shr    $0xc,%edx
f0101bce:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f0101bd4:	72 12                	jb     f0101be8 <mem_init+0x492>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101bd6:	50                   	push   %eax
f0101bd7:	68 68 6f 10 f0       	push   $0xf0106f68
f0101bdc:	6a 58                	push   $0x58
f0101bde:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0101be3:	e8 64 e4 ff ff       	call   f010004c <_panic>
f0101be8:	8d 90 00 10 00 f0    	lea    -0xffff000(%eax),%edx
	return (void *)(pa + KERNBASE);
f0101bee:	8d 80 00 00 00 f0    	lea    -0x10000000(%eax),%eax
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
		assert(c[i] == 0);
f0101bf4:	80 38 00             	cmpb   $0x0,(%eax)
f0101bf7:	74 19                	je     f0101c12 <mem_init+0x4bc>
f0101bf9:	68 53 82 10 f0       	push   $0xf0108253
f0101bfe:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101c03:	68 28 04 00 00       	push   $0x428
f0101c08:	68 61 7f 10 f0       	push   $0xf0107f61
f0101c0d:	e8 3a e4 ff ff       	call   f010004c <_panic>
f0101c12:	40                   	inc    %eax
	memset(page2kva(pp0), 1, PGSIZE);
	page_free(pp0);
	assert((pp = page_alloc(ALLOC_ZERO)));
	assert(pp && pp0 == pp);
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
f0101c13:	39 d0                	cmp    %edx,%eax
f0101c15:	75 dd                	jne    f0101bf4 <mem_init+0x49e>
		assert(c[i] == 0);

	// give free list back
	page_free_list = fl;
f0101c17:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0101c1a:	a3 40 22 27 f0       	mov    %eax,0xf0272240

	// free the pages we took
	page_free(pp0);
f0101c1f:	83 ec 0c             	sub    $0xc,%esp
f0101c22:	56                   	push   %esi
f0101c23:	e8 57 f6 ff ff       	call   f010127f <page_free>
	page_free(pp1);
f0101c28:	89 3c 24             	mov    %edi,(%esp)
f0101c2b:	e8 4f f6 ff ff       	call   f010127f <page_free>
	page_free(pp2);
f0101c30:	83 c4 04             	add    $0x4,%esp
f0101c33:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101c36:	e8 44 f6 ff ff       	call   f010127f <page_free>

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101c3b:	a1 40 22 27 f0       	mov    0xf0272240,%eax
f0101c40:	83 c4 10             	add    $0x10,%esp
f0101c43:	eb 03                	jmp    f0101c48 <mem_init+0x4f2>
		--nfree;
f0101c45:	4b                   	dec    %ebx
	page_free(pp0);
	page_free(pp1);
	page_free(pp2);

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101c46:	8b 00                	mov    (%eax),%eax
f0101c48:	85 c0                	test   %eax,%eax
f0101c4a:	75 f9                	jne    f0101c45 <mem_init+0x4ef>
		--nfree;
	assert(nfree == 0);
f0101c4c:	85 db                	test   %ebx,%ebx
f0101c4e:	74 19                	je     f0101c69 <mem_init+0x513>
f0101c50:	68 5d 82 10 f0       	push   $0xf010825d
f0101c55:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101c5a:	68 35 04 00 00       	push   $0x435
f0101c5f:	68 61 7f 10 f0       	push   $0xf0107f61
f0101c64:	e8 e3 e3 ff ff       	call   f010004c <_panic>

	cprintf("check_page_alloc() succeeded!\n");
f0101c69:	83 ec 0c             	sub    $0xc,%esp
f0101c6c:	68 74 77 10 f0       	push   $0xf0107774
f0101c71:	e8 82 1f 00 00       	call   f0103bf8 <cprintf>
	int i;
	extern pde_t entry_pgdir[];

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101c76:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101c7d:	e8 87 f5 ff ff       	call   f0101209 <page_alloc>
f0101c82:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101c85:	83 c4 10             	add    $0x10,%esp
f0101c88:	85 c0                	test   %eax,%eax
f0101c8a:	75 19                	jne    f0101ca5 <mem_init+0x54f>
f0101c8c:	68 6b 81 10 f0       	push   $0xf010816b
f0101c91:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101c96:	68 ac 04 00 00       	push   $0x4ac
f0101c9b:	68 61 7f 10 f0       	push   $0xf0107f61
f0101ca0:	e8 a7 e3 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0101ca5:	83 ec 0c             	sub    $0xc,%esp
f0101ca8:	6a 00                	push   $0x0
f0101caa:	e8 5a f5 ff ff       	call   f0101209 <page_alloc>
f0101caf:	89 c6                	mov    %eax,%esi
f0101cb1:	83 c4 10             	add    $0x10,%esp
f0101cb4:	85 c0                	test   %eax,%eax
f0101cb6:	75 19                	jne    f0101cd1 <mem_init+0x57b>
f0101cb8:	68 81 81 10 f0       	push   $0xf0108181
f0101cbd:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101cc2:	68 ad 04 00 00       	push   $0x4ad
f0101cc7:	68 61 7f 10 f0       	push   $0xf0107f61
f0101ccc:	e8 7b e3 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0101cd1:	83 ec 0c             	sub    $0xc,%esp
f0101cd4:	6a 00                	push   $0x0
f0101cd6:	e8 2e f5 ff ff       	call   f0101209 <page_alloc>
f0101cdb:	89 c3                	mov    %eax,%ebx
f0101cdd:	83 c4 10             	add    $0x10,%esp
f0101ce0:	85 c0                	test   %eax,%eax
f0101ce2:	75 19                	jne    f0101cfd <mem_init+0x5a7>
f0101ce4:	68 97 81 10 f0       	push   $0xf0108197
f0101ce9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101cee:	68 ae 04 00 00       	push   $0x4ae
f0101cf3:	68 61 7f 10 f0       	push   $0xf0107f61
f0101cf8:	e8 4f e3 ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101cfd:	39 75 d4             	cmp    %esi,-0x2c(%ebp)
f0101d00:	75 19                	jne    f0101d1b <mem_init+0x5c5>
f0101d02:	68 ad 81 10 f0       	push   $0xf01081ad
f0101d07:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101d0c:	68 b1 04 00 00       	push   $0x4b1
f0101d11:	68 61 7f 10 f0       	push   $0xf0107f61
f0101d16:	e8 31 e3 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101d1b:	39 c6                	cmp    %eax,%esi
f0101d1d:	74 05                	je     f0101d24 <mem_init+0x5ce>
f0101d1f:	39 45 d4             	cmp    %eax,-0x2c(%ebp)
f0101d22:	75 19                	jne    f0101d3d <mem_init+0x5e7>
f0101d24:	68 54 77 10 f0       	push   $0xf0107754
f0101d29:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101d2e:	68 b2 04 00 00       	push   $0x4b2
f0101d33:	68 61 7f 10 f0       	push   $0xf0107f61
f0101d38:	e8 0f e3 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f0101d3d:	a1 40 22 27 f0       	mov    0xf0272240,%eax
f0101d42:	89 45 d0             	mov    %eax,-0x30(%ebp)

	page_free_list = 0;
f0101d45:	c7 05 40 22 27 f0 00 	movl   $0x0,0xf0272240
f0101d4c:	00 00 00 
	//print_fl();
	// should be no free memory
	assert(!page_alloc(0));
f0101d4f:	83 ec 0c             	sub    $0xc,%esp
f0101d52:	6a 00                	push   $0x0
f0101d54:	e8 b0 f4 ff ff       	call   f0101209 <page_alloc>
f0101d59:	83 c4 10             	add    $0x10,%esp
f0101d5c:	85 c0                	test   %eax,%eax
f0101d5e:	74 19                	je     f0101d79 <mem_init+0x623>
f0101d60:	68 16 82 10 f0       	push   $0xf0108216
f0101d65:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101d6a:	68 ba 04 00 00       	push   $0x4ba
f0101d6f:	68 61 7f 10 f0       	push   $0xf0107f61
f0101d74:	e8 d3 e2 ff ff       	call   f010004c <_panic>
	//print_fl();
	// there is no page allocated at address 0
	assert(page_lookup(kern_pgdir, (void *) 0x0, &ptep) == NULL);
f0101d79:	83 ec 04             	sub    $0x4,%esp
f0101d7c:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0101d7f:	50                   	push   %eax
f0101d80:	6a 00                	push   $0x0
f0101d82:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0101d88:	e8 33 f7 ff ff       	call   f01014c0 <page_lookup>
f0101d8d:	83 c4 10             	add    $0x10,%esp
f0101d90:	85 c0                	test   %eax,%eax
f0101d92:	74 19                	je     f0101dad <mem_init+0x657>
f0101d94:	68 94 77 10 f0       	push   $0xf0107794
f0101d99:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101d9e:	68 bd 04 00 00       	push   $0x4bd
f0101da3:	68 61 7f 10 f0       	push   $0xf0107f61
f0101da8:	e8 9f e2 ff ff       	call   f010004c <_panic>
	//cprintf("H1\n");
	// there is no free memory, so we can't allocate a page table
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) < 0); //insert fail
f0101dad:	6a 02                	push   $0x2
f0101daf:	6a 00                	push   $0x0
f0101db1:	56                   	push   %esi
f0101db2:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0101db8:	e8 c8 f8 ff ff       	call   f0101685 <page_insert>
f0101dbd:	83 c4 10             	add    $0x10,%esp
f0101dc0:	85 c0                	test   %eax,%eax
f0101dc2:	78 19                	js     f0101ddd <mem_init+0x687>
f0101dc4:	68 cc 77 10 f0       	push   $0xf01077cc
f0101dc9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101dce:	68 c0 04 00 00       	push   $0x4c0
f0101dd3:	68 61 7f 10 f0       	push   $0xf0107f61
f0101dd8:	e8 6f e2 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H2\n");
	// free pp0 and try again: pp0 should be used for page table
	page_free(pp0);
f0101ddd:	83 ec 0c             	sub    $0xc,%esp
f0101de0:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101de3:	e8 97 f4 ff ff       	call   f010127f <page_free>
	//cprintf("H3\n");
	
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) == 0); //insert success
f0101de8:	6a 02                	push   $0x2
f0101dea:	6a 00                	push   $0x0
f0101dec:	56                   	push   %esi
f0101ded:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0101df3:	e8 8d f8 ff ff       	call   f0101685 <page_insert>
f0101df8:	83 c4 20             	add    $0x20,%esp
f0101dfb:	85 c0                	test   %eax,%eax
f0101dfd:	74 19                	je     f0101e18 <mem_init+0x6c2>
f0101dff:	68 fc 77 10 f0       	push   $0xf01077fc
f0101e04:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101e09:	68 c7 04 00 00       	push   $0x4c7
f0101e0e:	68 61 7f 10 f0       	push   $0xf0107f61
f0101e13:	e8 34 e2 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H3\n");
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0)); //problem
f0101e18:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101e1e:	a1 2c 34 27 f0       	mov    0xf027342c,%eax
f0101e23:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0101e26:	8b 17                	mov    (%edi),%edx
f0101e28:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0101e2e:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
f0101e31:	29 c1                	sub    %eax,%ecx
f0101e33:	89 c8                	mov    %ecx,%eax
f0101e35:	c1 f8 03             	sar    $0x3,%eax
f0101e38:	c1 e0 0c             	shl    $0xc,%eax
f0101e3b:	39 c2                	cmp    %eax,%edx
f0101e3d:	74 19                	je     f0101e58 <mem_init+0x702>
f0101e3f:	68 2c 78 10 f0       	push   $0xf010782c
f0101e44:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101e49:	68 ca 04 00 00       	push   $0x4ca
f0101e4e:	68 61 7f 10 f0       	push   $0xf0107f61
f0101e53:	e8 f4 e1 ff ff       	call   f010004c <_panic>
	//cprintf("H4\n");
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, 0x0));
	assert(check_va2pa(kern_pgdir, 0x0) == page2pa(pp1)); //problem
f0101e58:	ba 00 00 00 00       	mov    $0x0,%edx
f0101e5d:	89 f8                	mov    %edi,%eax
f0101e5f:	e8 b4 ee ff ff       	call   f0100d18 <check_va2pa>
f0101e64:	89 f2                	mov    %esi,%edx
f0101e66:	2b 55 cc             	sub    -0x34(%ebp),%edx
f0101e69:	c1 fa 03             	sar    $0x3,%edx
f0101e6c:	c1 e2 0c             	shl    $0xc,%edx
f0101e6f:	39 d0                	cmp    %edx,%eax
f0101e71:	74 19                	je     f0101e8c <mem_init+0x736>
f0101e73:	68 54 78 10 f0       	push   $0xf0107854
f0101e78:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101e7d:	68 cd 04 00 00       	push   $0x4cd
f0101e82:	68 61 7f 10 f0       	push   $0xf0107f61
f0101e87:	e8 c0 e1 ff ff       	call   f010004c <_panic>
	//cprintf("H5\n");
	assert(pp1->pp_ref == 1);
f0101e8c:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0101e91:	74 19                	je     f0101eac <mem_init+0x756>
f0101e93:	68 68 82 10 f0       	push   $0xf0108268
f0101e98:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101e9d:	68 cf 04 00 00       	push   $0x4cf
f0101ea2:	68 61 7f 10 f0       	push   $0xf0107f61
f0101ea7:	e8 a0 e1 ff ff       	call   f010004c <_panic>
	assert(pp0->pp_ref == 1);
f0101eac:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101eaf:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f0101eb4:	74 19                	je     f0101ecf <mem_init+0x779>
f0101eb6:	68 79 82 10 f0       	push   $0xf0108279
f0101ebb:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101ec0:	68 d0 04 00 00       	push   $0x4d0
f0101ec5:	68 61 7f 10 f0       	push   $0xf0107f61
f0101eca:	e8 7d e1 ff ff       	call   f010004c <_panic>

	//print_fl();
	// should be able to map pp2 at PGSIZE because pp0 is already allocated for page table
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101ecf:	6a 02                	push   $0x2
f0101ed1:	68 00 10 00 00       	push   $0x1000
f0101ed6:	53                   	push   %ebx
f0101ed7:	57                   	push   %edi
f0101ed8:	e8 a8 f7 ff ff       	call   f0101685 <page_insert>
f0101edd:	83 c4 10             	add    $0x10,%esp
f0101ee0:	85 c0                	test   %eax,%eax
f0101ee2:	74 19                	je     f0101efd <mem_init+0x7a7>
f0101ee4:	68 84 78 10 f0       	push   $0xf0107884
f0101ee9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101eee:	68 d4 04 00 00       	push   $0x4d4
f0101ef3:	68 61 7f 10 f0       	push   $0xf0107f61
f0101ef8:	e8 4f e1 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, PGSIZE));
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101efd:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101f02:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0101f07:	e8 0c ee ff ff       	call   f0100d18 <check_va2pa>
f0101f0c:	89 da                	mov    %ebx,%edx
f0101f0e:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f0101f14:	c1 fa 03             	sar    $0x3,%edx
f0101f17:	c1 e2 0c             	shl    $0xc,%edx
f0101f1a:	39 d0                	cmp    %edx,%eax
f0101f1c:	74 19                	je     f0101f37 <mem_init+0x7e1>
f0101f1e:	68 c0 78 10 f0       	push   $0xf01078c0
f0101f23:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101f28:	68 d7 04 00 00       	push   $0x4d7
f0101f2d:	68 61 7f 10 f0       	push   $0xf0107f61
f0101f32:	e8 15 e1 ff ff       	call   f010004c <_panic>
	//cprintf("H6\n");
	assert(pp2->pp_ref == 1);
f0101f37:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101f3c:	74 19                	je     f0101f57 <mem_init+0x801>
f0101f3e:	68 8a 82 10 f0       	push   $0xf010828a
f0101f43:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101f48:	68 d9 04 00 00       	push   $0x4d9
f0101f4d:	68 61 7f 10 f0       	push   $0xf0107f61
f0101f52:	e8 f5 e0 ff ff       	call   f010004c <_panic>

	// should be no free memory
	//print_fl();
	assert(!page_alloc(0));
f0101f57:	83 ec 0c             	sub    $0xc,%esp
f0101f5a:	6a 00                	push   $0x0
f0101f5c:	e8 a8 f2 ff ff       	call   f0101209 <page_alloc>
f0101f61:	83 c4 10             	add    $0x10,%esp
f0101f64:	85 c0                	test   %eax,%eax
f0101f66:	74 19                	je     f0101f81 <mem_init+0x82b>
f0101f68:	68 16 82 10 f0       	push   $0xf0108216
f0101f6d:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101f72:	68 dd 04 00 00       	push   $0x4dd
f0101f77:	68 61 7f 10 f0       	push   $0xf0107f61
f0101f7c:	e8 cb e0 ff ff       	call   f010004c <_panic>
	//print_fl();
	// should be able to map pp2 at PGSIZE because it's already there
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101f81:	6a 02                	push   $0x2
f0101f83:	68 00 10 00 00       	push   $0x1000
f0101f88:	53                   	push   %ebx
f0101f89:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0101f8f:	e8 f1 f6 ff ff       	call   f0101685 <page_insert>
f0101f94:	83 c4 10             	add    $0x10,%esp
f0101f97:	85 c0                	test   %eax,%eax
f0101f99:	74 19                	je     f0101fb4 <mem_init+0x85e>
f0101f9b:	68 84 78 10 f0       	push   $0xf0107884
f0101fa0:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101fa5:	68 e0 04 00 00       	push   $0x4e0
f0101faa:	68 61 7f 10 f0       	push   $0xf0107f61
f0101faf:	e8 98 e0 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101fb4:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101fb9:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0101fbe:	e8 55 ed ff ff       	call   f0100d18 <check_va2pa>
f0101fc3:	89 da                	mov    %ebx,%edx
f0101fc5:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f0101fcb:	c1 fa 03             	sar    $0x3,%edx
f0101fce:	c1 e2 0c             	shl    $0xc,%edx
f0101fd1:	39 d0                	cmp    %edx,%eax
f0101fd3:	74 19                	je     f0101fee <mem_init+0x898>
f0101fd5:	68 c0 78 10 f0       	push   $0xf01078c0
f0101fda:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101fdf:	68 e1 04 00 00       	push   $0x4e1
f0101fe4:	68 61 7f 10 f0       	push   $0xf0107f61
f0101fe9:	e8 5e e0 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0101fee:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101ff3:	74 19                	je     f010200e <mem_init+0x8b8>
f0101ff5:	68 8a 82 10 f0       	push   $0xf010828a
f0101ffa:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0101fff:	68 e2 04 00 00       	push   $0x4e2
f0102004:	68 61 7f 10 f0       	push   $0xf0107f61
f0102009:	e8 3e e0 ff ff       	call   f010004c <_panic>
	//print_fl();
	// pp2 should NOT be on the free list
	// could happen in ref counts are handled sloppily in page_insert
	assert(!page_alloc(0));
f010200e:	83 ec 0c             	sub    $0xc,%esp
f0102011:	6a 00                	push   $0x0
f0102013:	e8 f1 f1 ff ff       	call   f0101209 <page_alloc>
f0102018:	83 c4 10             	add    $0x10,%esp
f010201b:	85 c0                	test   %eax,%eax
f010201d:	74 19                	je     f0102038 <mem_init+0x8e2>
f010201f:	68 16 82 10 f0       	push   $0xf0108216
f0102024:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102029:	68 e6 04 00 00       	push   $0x4e6
f010202e:	68 61 7f 10 f0       	push   $0xf0107f61
f0102033:	e8 14 e0 ff ff       	call   f010004c <_panic>

	// check that pgdir_walk returns a pointer to the pte
	ptep = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(PGSIZE)]));
f0102038:	8b 15 28 34 27 f0    	mov    0xf0273428,%edx
f010203e:	8b 02                	mov    (%edx),%eax
f0102040:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102045:	89 c1                	mov    %eax,%ecx
f0102047:	c1 e9 0c             	shr    $0xc,%ecx
f010204a:	3b 0d 24 34 27 f0    	cmp    0xf0273424,%ecx
f0102050:	72 15                	jb     f0102067 <mem_init+0x911>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102052:	50                   	push   %eax
f0102053:	68 68 6f 10 f0       	push   $0xf0106f68
f0102058:	68 e9 04 00 00       	push   $0x4e9
f010205d:	68 61 7f 10 f0       	push   $0xf0107f61
f0102062:	e8 e5 df ff ff       	call   f010004c <_panic>
f0102067:	2d 00 00 00 10       	sub    $0x10000000,%eax
f010206c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	assert(pgdir_walk(kern_pgdir, (void*)PGSIZE, 0) == ptep+PTX(PGSIZE));
f010206f:	83 ec 04             	sub    $0x4,%esp
f0102072:	6a 00                	push   $0x0
f0102074:	68 00 10 00 00       	push   $0x1000
f0102079:	52                   	push   %edx
f010207a:	e8 81 f2 ff ff       	call   f0101300 <pgdir_walk>
f010207f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0102082:	8d 57 04             	lea    0x4(%edi),%edx
f0102085:	83 c4 10             	add    $0x10,%esp
f0102088:	39 d0                	cmp    %edx,%eax
f010208a:	74 19                	je     f01020a5 <mem_init+0x94f>
f010208c:	68 f0 78 10 f0       	push   $0xf01078f0
f0102091:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102096:	68 ea 04 00 00       	push   $0x4ea
f010209b:	68 61 7f 10 f0       	push   $0xf0107f61
f01020a0:	e8 a7 df ff ff       	call   f010004c <_panic>

	// should be able to change permissions too.
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W|PTE_U) == 0);
f01020a5:	6a 06                	push   $0x6
f01020a7:	68 00 10 00 00       	push   $0x1000
f01020ac:	53                   	push   %ebx
f01020ad:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01020b3:	e8 cd f5 ff ff       	call   f0101685 <page_insert>
f01020b8:	83 c4 10             	add    $0x10,%esp
f01020bb:	85 c0                	test   %eax,%eax
f01020bd:	74 19                	je     f01020d8 <mem_init+0x982>
f01020bf:	68 30 79 10 f0       	push   $0xf0107930
f01020c4:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01020c9:	68 ed 04 00 00       	push   $0x4ed
f01020ce:	68 61 7f 10 f0       	push   $0xf0107f61
f01020d3:	e8 74 df ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f01020d8:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi
f01020de:	ba 00 10 00 00       	mov    $0x1000,%edx
f01020e3:	89 f8                	mov    %edi,%eax
f01020e5:	e8 2e ec ff ff       	call   f0100d18 <check_va2pa>
f01020ea:	89 da                	mov    %ebx,%edx
f01020ec:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f01020f2:	c1 fa 03             	sar    $0x3,%edx
f01020f5:	c1 e2 0c             	shl    $0xc,%edx
f01020f8:	39 d0                	cmp    %edx,%eax
f01020fa:	74 19                	je     f0102115 <mem_init+0x9bf>
f01020fc:	68 c0 78 10 f0       	push   $0xf01078c0
f0102101:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102106:	68 ee 04 00 00       	push   $0x4ee
f010210b:	68 61 7f 10 f0       	push   $0xf0107f61
f0102110:	e8 37 df ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0102115:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f010211a:	74 19                	je     f0102135 <mem_init+0x9df>
f010211c:	68 8a 82 10 f0       	push   $0xf010828a
f0102121:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102126:	68 ef 04 00 00       	push   $0x4ef
f010212b:	68 61 7f 10 f0       	push   $0xf0107f61
f0102130:	e8 17 df ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U);
f0102135:	83 ec 04             	sub    $0x4,%esp
f0102138:	6a 00                	push   $0x0
f010213a:	68 00 10 00 00       	push   $0x1000
f010213f:	57                   	push   %edi
f0102140:	e8 bb f1 ff ff       	call   f0101300 <pgdir_walk>
f0102145:	83 c4 10             	add    $0x10,%esp
f0102148:	f6 00 04             	testb  $0x4,(%eax)
f010214b:	75 19                	jne    f0102166 <mem_init+0xa10>
f010214d:	68 70 79 10 f0       	push   $0xf0107970
f0102152:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102157:	68 f0 04 00 00       	push   $0x4f0
f010215c:	68 61 7f 10 f0       	push   $0xf0107f61
f0102161:	e8 e6 de ff ff       	call   f010004c <_panic>
	assert(kern_pgdir[0] & PTE_U);
f0102166:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f010216b:	f6 00 04             	testb  $0x4,(%eax)
f010216e:	75 19                	jne    f0102189 <mem_init+0xa33>
f0102170:	68 9b 82 10 f0       	push   $0xf010829b
f0102175:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010217a:	68 f1 04 00 00       	push   $0x4f1
f010217f:	68 61 7f 10 f0       	push   $0xf0107f61
f0102184:	e8 c3 de ff ff       	call   f010004c <_panic>

	// should be able to remap with fewer permissions
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0102189:	6a 02                	push   $0x2
f010218b:	68 00 10 00 00       	push   $0x1000
f0102190:	53                   	push   %ebx
f0102191:	50                   	push   %eax
f0102192:	e8 ee f4 ff ff       	call   f0101685 <page_insert>
f0102197:	83 c4 10             	add    $0x10,%esp
f010219a:	85 c0                	test   %eax,%eax
f010219c:	74 19                	je     f01021b7 <mem_init+0xa61>
f010219e:	68 84 78 10 f0       	push   $0xf0107884
f01021a3:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01021a8:	68 f4 04 00 00       	push   $0x4f4
f01021ad:	68 61 7f 10 f0       	push   $0xf0107f61
f01021b2:	e8 95 de ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_W);
f01021b7:	83 ec 04             	sub    $0x4,%esp
f01021ba:	6a 00                	push   $0x0
f01021bc:	68 00 10 00 00       	push   $0x1000
f01021c1:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01021c7:	e8 34 f1 ff ff       	call   f0101300 <pgdir_walk>
f01021cc:	83 c4 10             	add    $0x10,%esp
f01021cf:	f6 00 02             	testb  $0x2,(%eax)
f01021d2:	75 19                	jne    f01021ed <mem_init+0xa97>
f01021d4:	68 a4 79 10 f0       	push   $0xf01079a4
f01021d9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01021de:	68 f5 04 00 00       	push   $0x4f5
f01021e3:	68 61 7f 10 f0       	push   $0xf0107f61
f01021e8:	e8 5f de ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f01021ed:	83 ec 04             	sub    $0x4,%esp
f01021f0:	6a 00                	push   $0x0
f01021f2:	68 00 10 00 00       	push   $0x1000
f01021f7:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01021fd:	e8 fe f0 ff ff       	call   f0101300 <pgdir_walk>
f0102202:	83 c4 10             	add    $0x10,%esp
f0102205:	f6 00 04             	testb  $0x4,(%eax)
f0102208:	74 19                	je     f0102223 <mem_init+0xacd>
f010220a:	68 d8 79 10 f0       	push   $0xf01079d8
f010220f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102214:	68 f6 04 00 00       	push   $0x4f6
f0102219:	68 61 7f 10 f0       	push   $0xf0107f61
f010221e:	e8 29 de ff ff       	call   f010004c <_panic>

	// should not be able to map at PTSIZE because need free page for page table
	assert(page_insert(kern_pgdir, pp0, (void*) PTSIZE, PTE_W) < 0);
f0102223:	6a 02                	push   $0x2
f0102225:	68 00 00 40 00       	push   $0x400000
f010222a:	ff 75 d4             	pushl  -0x2c(%ebp)
f010222d:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0102233:	e8 4d f4 ff ff       	call   f0101685 <page_insert>
f0102238:	83 c4 10             	add    $0x10,%esp
f010223b:	85 c0                	test   %eax,%eax
f010223d:	78 19                	js     f0102258 <mem_init+0xb02>
f010223f:	68 10 7a 10 f0       	push   $0xf0107a10
f0102244:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102249:	68 f9 04 00 00       	push   $0x4f9
f010224e:	68 61 7f 10 f0       	push   $0xf0107f61
f0102253:	e8 f4 dd ff ff       	call   f010004c <_panic>

	// insert pp1 at PGSIZE (replacing pp2)
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W) == 0);
f0102258:	6a 02                	push   $0x2
f010225a:	68 00 10 00 00       	push   $0x1000
f010225f:	56                   	push   %esi
f0102260:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0102266:	e8 1a f4 ff ff       	call   f0101685 <page_insert>
f010226b:	83 c4 10             	add    $0x10,%esp
f010226e:	85 c0                	test   %eax,%eax
f0102270:	74 19                	je     f010228b <mem_init+0xb35>
f0102272:	68 48 7a 10 f0       	push   $0xf0107a48
f0102277:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010227c:	68 fc 04 00 00       	push   $0x4fc
f0102281:	68 61 7f 10 f0       	push   $0xf0107f61
f0102286:	e8 c1 dd ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f010228b:	83 ec 04             	sub    $0x4,%esp
f010228e:	6a 00                	push   $0x0
f0102290:	68 00 10 00 00       	push   $0x1000
f0102295:	ff 35 28 34 27 f0    	pushl  0xf0273428
f010229b:	e8 60 f0 ff ff       	call   f0101300 <pgdir_walk>
f01022a0:	83 c4 10             	add    $0x10,%esp
f01022a3:	f6 00 04             	testb  $0x4,(%eax)
f01022a6:	74 19                	je     f01022c1 <mem_init+0xb6b>
f01022a8:	68 d8 79 10 f0       	push   $0xf01079d8
f01022ad:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01022b2:	68 fd 04 00 00       	push   $0x4fd
f01022b7:	68 61 7f 10 f0       	push   $0xf0107f61
f01022bc:	e8 8b dd ff ff       	call   f010004c <_panic>

	// should have pp1 at both 0 and PGSIZE, pp2 nowhere, ...
	assert(check_va2pa(kern_pgdir, 0) == page2pa(pp1));
f01022c1:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi
f01022c7:	ba 00 00 00 00       	mov    $0x0,%edx
f01022cc:	89 f8                	mov    %edi,%eax
f01022ce:	e8 45 ea ff ff       	call   f0100d18 <check_va2pa>
f01022d3:	89 c1                	mov    %eax,%ecx
f01022d5:	89 45 cc             	mov    %eax,-0x34(%ebp)
f01022d8:	89 f0                	mov    %esi,%eax
f01022da:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f01022e0:	c1 f8 03             	sar    $0x3,%eax
f01022e3:	c1 e0 0c             	shl    $0xc,%eax
f01022e6:	39 c1                	cmp    %eax,%ecx
f01022e8:	74 19                	je     f0102303 <mem_init+0xbad>
f01022ea:	68 84 7a 10 f0       	push   $0xf0107a84
f01022ef:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01022f4:	68 00 05 00 00       	push   $0x500
f01022f9:	68 61 7f 10 f0       	push   $0xf0107f61
f01022fe:	e8 49 dd ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f0102303:	ba 00 10 00 00       	mov    $0x1000,%edx
f0102308:	89 f8                	mov    %edi,%eax
f010230a:	e8 09 ea ff ff       	call   f0100d18 <check_va2pa>
f010230f:	39 45 cc             	cmp    %eax,-0x34(%ebp)
f0102312:	74 19                	je     f010232d <mem_init+0xbd7>
f0102314:	68 b0 7a 10 f0       	push   $0xf0107ab0
f0102319:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010231e:	68 01 05 00 00       	push   $0x501
f0102323:	68 61 7f 10 f0       	push   $0xf0107f61
f0102328:	e8 1f dd ff ff       	call   f010004c <_panic>
	// ... and ref counts should reflect this
	assert(pp1->pp_ref == 2);
f010232d:	66 83 7e 04 02       	cmpw   $0x2,0x4(%esi)
f0102332:	74 19                	je     f010234d <mem_init+0xbf7>
f0102334:	68 b1 82 10 f0       	push   $0xf01082b1
f0102339:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010233e:	68 03 05 00 00       	push   $0x503
f0102343:	68 61 7f 10 f0       	push   $0xf0107f61
f0102348:	e8 ff dc ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f010234d:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102352:	74 19                	je     f010236d <mem_init+0xc17>
f0102354:	68 c2 82 10 f0       	push   $0xf01082c2
f0102359:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010235e:	68 04 05 00 00       	push   $0x504
f0102363:	68 61 7f 10 f0       	push   $0xf0107f61
f0102368:	e8 df dc ff ff       	call   f010004c <_panic>

	// pp2 should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp2);
f010236d:	83 ec 0c             	sub    $0xc,%esp
f0102370:	6a 00                	push   $0x0
f0102372:	e8 92 ee ff ff       	call   f0101209 <page_alloc>
f0102377:	83 c4 10             	add    $0x10,%esp
f010237a:	85 c0                	test   %eax,%eax
f010237c:	74 04                	je     f0102382 <mem_init+0xc2c>
f010237e:	39 c3                	cmp    %eax,%ebx
f0102380:	74 19                	je     f010239b <mem_init+0xc45>
f0102382:	68 e0 7a 10 f0       	push   $0xf0107ae0
f0102387:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010238c:	68 07 05 00 00       	push   $0x507
f0102391:	68 61 7f 10 f0       	push   $0xf0107f61
f0102396:	e8 b1 dc ff ff       	call   f010004c <_panic>

	// unmapping pp1 at 0 should keep pp1 at PGSIZE
	page_remove(kern_pgdir, 0x0);
f010239b:	83 ec 08             	sub    $0x8,%esp
f010239e:	6a 00                	push   $0x0
f01023a0:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01023a6:	e8 3a f2 ff ff       	call   f01015e5 <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f01023ab:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi
f01023b1:	ba 00 00 00 00       	mov    $0x0,%edx
f01023b6:	89 f8                	mov    %edi,%eax
f01023b8:	e8 5b e9 ff ff       	call   f0100d18 <check_va2pa>
f01023bd:	83 c4 10             	add    $0x10,%esp
f01023c0:	83 f8 ff             	cmp    $0xffffffff,%eax
f01023c3:	74 19                	je     f01023de <mem_init+0xc88>
f01023c5:	68 04 7b 10 f0       	push   $0xf0107b04
f01023ca:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01023cf:	68 0b 05 00 00       	push   $0x50b
f01023d4:	68 61 7f 10 f0       	push   $0xf0107f61
f01023d9:	e8 6e dc ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f01023de:	ba 00 10 00 00       	mov    $0x1000,%edx
f01023e3:	89 f8                	mov    %edi,%eax
f01023e5:	e8 2e e9 ff ff       	call   f0100d18 <check_va2pa>
f01023ea:	89 f2                	mov    %esi,%edx
f01023ec:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f01023f2:	c1 fa 03             	sar    $0x3,%edx
f01023f5:	c1 e2 0c             	shl    $0xc,%edx
f01023f8:	39 d0                	cmp    %edx,%eax
f01023fa:	74 19                	je     f0102415 <mem_init+0xcbf>
f01023fc:	68 b0 7a 10 f0       	push   $0xf0107ab0
f0102401:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102406:	68 0c 05 00 00       	push   $0x50c
f010240b:	68 61 7f 10 f0       	push   $0xf0107f61
f0102410:	e8 37 dc ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 1);
f0102415:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f010241a:	74 19                	je     f0102435 <mem_init+0xcdf>
f010241c:	68 68 82 10 f0       	push   $0xf0108268
f0102421:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102426:	68 0d 05 00 00       	push   $0x50d
f010242b:	68 61 7f 10 f0       	push   $0xf0107f61
f0102430:	e8 17 dc ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f0102435:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f010243a:	74 19                	je     f0102455 <mem_init+0xcff>
f010243c:	68 c2 82 10 f0       	push   $0xf01082c2
f0102441:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102446:	68 0e 05 00 00       	push   $0x50e
f010244b:	68 61 7f 10 f0       	push   $0xf0107f61
f0102450:	e8 f7 db ff ff       	call   f010004c <_panic>

	// test re-inserting pp1 at PGSIZE
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, 0) == 0);
f0102455:	6a 00                	push   $0x0
f0102457:	68 00 10 00 00       	push   $0x1000
f010245c:	56                   	push   %esi
f010245d:	57                   	push   %edi
f010245e:	e8 22 f2 ff ff       	call   f0101685 <page_insert>
f0102463:	83 c4 10             	add    $0x10,%esp
f0102466:	85 c0                	test   %eax,%eax
f0102468:	74 19                	je     f0102483 <mem_init+0xd2d>
f010246a:	68 28 7b 10 f0       	push   $0xf0107b28
f010246f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102474:	68 11 05 00 00       	push   $0x511
f0102479:	68 61 7f 10 f0       	push   $0xf0107f61
f010247e:	e8 c9 db ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref);
f0102483:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f0102488:	75 19                	jne    f01024a3 <mem_init+0xd4d>
f010248a:	68 d3 82 10 f0       	push   $0xf01082d3
f010248f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102494:	68 12 05 00 00       	push   $0x512
f0102499:	68 61 7f 10 f0       	push   $0xf0107f61
f010249e:	e8 a9 db ff ff       	call   f010004c <_panic>
	assert(pp1->pp_link == NULL);
f01024a3:	83 3e 00             	cmpl   $0x0,(%esi)
f01024a6:	74 19                	je     f01024c1 <mem_init+0xd6b>
f01024a8:	68 df 82 10 f0       	push   $0xf01082df
f01024ad:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01024b2:	68 13 05 00 00       	push   $0x513
f01024b7:	68 61 7f 10 f0       	push   $0xf0107f61
f01024bc:	e8 8b db ff ff       	call   f010004c <_panic>

	// unmapping pp1 at PGSIZE should free it
	page_remove(kern_pgdir, (void*) PGSIZE);
f01024c1:	83 ec 08             	sub    $0x8,%esp
f01024c4:	68 00 10 00 00       	push   $0x1000
f01024c9:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01024cf:	e8 11 f1 ff ff       	call   f01015e5 <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f01024d4:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi
f01024da:	ba 00 00 00 00       	mov    $0x0,%edx
f01024df:	89 f8                	mov    %edi,%eax
f01024e1:	e8 32 e8 ff ff       	call   f0100d18 <check_va2pa>
f01024e6:	83 c4 10             	add    $0x10,%esp
f01024e9:	83 f8 ff             	cmp    $0xffffffff,%eax
f01024ec:	74 19                	je     f0102507 <mem_init+0xdb1>
f01024ee:	68 04 7b 10 f0       	push   $0xf0107b04
f01024f3:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01024f8:	68 17 05 00 00       	push   $0x517
f01024fd:	68 61 7f 10 f0       	push   $0xf0107f61
f0102502:	e8 45 db ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == ~0);
f0102507:	ba 00 10 00 00       	mov    $0x1000,%edx
f010250c:	89 f8                	mov    %edi,%eax
f010250e:	e8 05 e8 ff ff       	call   f0100d18 <check_va2pa>
f0102513:	83 f8 ff             	cmp    $0xffffffff,%eax
f0102516:	74 19                	je     f0102531 <mem_init+0xddb>
f0102518:	68 60 7b 10 f0       	push   $0xf0107b60
f010251d:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102522:	68 18 05 00 00       	push   $0x518
f0102527:	68 61 7f 10 f0       	push   $0xf0107f61
f010252c:	e8 1b db ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f0102531:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f0102536:	74 19                	je     f0102551 <mem_init+0xdfb>
f0102538:	68 f4 82 10 f0       	push   $0xf01082f4
f010253d:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102542:	68 19 05 00 00       	push   $0x519
f0102547:	68 61 7f 10 f0       	push   $0xf0107f61
f010254c:	e8 fb da ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f0102551:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102556:	74 19                	je     f0102571 <mem_init+0xe1b>
f0102558:	68 c2 82 10 f0       	push   $0xf01082c2
f010255d:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102562:	68 1a 05 00 00       	push   $0x51a
f0102567:	68 61 7f 10 f0       	push   $0xf0107f61
f010256c:	e8 db da ff ff       	call   f010004c <_panic>

	// so it should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp1);
f0102571:	83 ec 0c             	sub    $0xc,%esp
f0102574:	6a 00                	push   $0x0
f0102576:	e8 8e ec ff ff       	call   f0101209 <page_alloc>
f010257b:	83 c4 10             	add    $0x10,%esp
f010257e:	85 c0                	test   %eax,%eax
f0102580:	74 04                	je     f0102586 <mem_init+0xe30>
f0102582:	39 c6                	cmp    %eax,%esi
f0102584:	74 19                	je     f010259f <mem_init+0xe49>
f0102586:	68 88 7b 10 f0       	push   $0xf0107b88
f010258b:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102590:	68 1d 05 00 00       	push   $0x51d
f0102595:	68 61 7f 10 f0       	push   $0xf0107f61
f010259a:	e8 ad da ff ff       	call   f010004c <_panic>

	// should be no free memory
	assert(!page_alloc(0));
f010259f:	83 ec 0c             	sub    $0xc,%esp
f01025a2:	6a 00                	push   $0x0
f01025a4:	e8 60 ec ff ff       	call   f0101209 <page_alloc>
f01025a9:	83 c4 10             	add    $0x10,%esp
f01025ac:	85 c0                	test   %eax,%eax
f01025ae:	74 19                	je     f01025c9 <mem_init+0xe73>
f01025b0:	68 16 82 10 f0       	push   $0xf0108216
f01025b5:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01025ba:	68 20 05 00 00       	push   $0x520
f01025bf:	68 61 7f 10 f0       	push   $0xf0107f61
f01025c4:	e8 83 da ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f01025c9:	8b 0d 28 34 27 f0    	mov    0xf0273428,%ecx
f01025cf:	8b 11                	mov    (%ecx),%edx
f01025d1:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f01025d7:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01025da:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f01025e0:	c1 f8 03             	sar    $0x3,%eax
f01025e3:	c1 e0 0c             	shl    $0xc,%eax
f01025e6:	39 c2                	cmp    %eax,%edx
f01025e8:	74 19                	je     f0102603 <mem_init+0xead>
f01025ea:	68 2c 78 10 f0       	push   $0xf010782c
f01025ef:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01025f4:	68 23 05 00 00       	push   $0x523
f01025f9:	68 61 7f 10 f0       	push   $0xf0107f61
f01025fe:	e8 49 da ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f0102603:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f0102609:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010260c:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f0102611:	74 19                	je     f010262c <mem_init+0xed6>
f0102613:	68 79 82 10 f0       	push   $0xf0108279
f0102618:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010261d:	68 25 05 00 00       	push   $0x525
f0102622:	68 61 7f 10 f0       	push   $0xf0107f61
f0102627:	e8 20 da ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f010262c:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010262f:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// check pointer arithmetic in pgdir_walk
	page_free(pp0);
f0102635:	83 ec 0c             	sub    $0xc,%esp
f0102638:	50                   	push   %eax
f0102639:	e8 41 ec ff ff       	call   f010127f <page_free>
	va = (void*)(PGSIZE * NPDENTRIES + PGSIZE);
	ptep = pgdir_walk(kern_pgdir, va, 1);
f010263e:	83 c4 0c             	add    $0xc,%esp
f0102641:	6a 01                	push   $0x1
f0102643:	68 00 10 40 00       	push   $0x401000
f0102648:	ff 35 28 34 27 f0    	pushl  0xf0273428
f010264e:	e8 ad ec ff ff       	call   f0101300 <pgdir_walk>
f0102653:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0102656:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	ptep1 = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(va)]));
f0102659:	8b 0d 28 34 27 f0    	mov    0xf0273428,%ecx
f010265f:	8b 51 04             	mov    0x4(%ecx),%edx
f0102662:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102668:	8b 3d 24 34 27 f0    	mov    0xf0273424,%edi
f010266e:	89 d0                	mov    %edx,%eax
f0102670:	c1 e8 0c             	shr    $0xc,%eax
f0102673:	83 c4 10             	add    $0x10,%esp
f0102676:	39 f8                	cmp    %edi,%eax
f0102678:	72 15                	jb     f010268f <mem_init+0xf39>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010267a:	52                   	push   %edx
f010267b:	68 68 6f 10 f0       	push   $0xf0106f68
f0102680:	68 2c 05 00 00       	push   $0x52c
f0102685:	68 61 7f 10 f0       	push   $0xf0107f61
f010268a:	e8 bd d9 ff ff       	call   f010004c <_panic>
	assert(ptep == ptep1 + PTX(va));
f010268f:	81 ea fc ff ff 0f    	sub    $0xffffffc,%edx
f0102695:	39 55 cc             	cmp    %edx,-0x34(%ebp)
f0102698:	74 19                	je     f01026b3 <mem_init+0xf5d>
f010269a:	68 05 83 10 f0       	push   $0xf0108305
f010269f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01026a4:	68 2d 05 00 00       	push   $0x52d
f01026a9:	68 61 7f 10 f0       	push   $0xf0107f61
f01026ae:	e8 99 d9 ff ff       	call   f010004c <_panic>
	kern_pgdir[PDX(va)] = 0;
f01026b3:	c7 41 04 00 00 00 00 	movl   $0x0,0x4(%ecx)
	pp0->pp_ref = 0;
f01026ba:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01026bd:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01026c3:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f01026c9:	c1 f8 03             	sar    $0x3,%eax
f01026cc:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01026cf:	89 c2                	mov    %eax,%edx
f01026d1:	c1 ea 0c             	shr    $0xc,%edx
f01026d4:	39 d7                	cmp    %edx,%edi
f01026d6:	77 12                	ja     f01026ea <mem_init+0xf94>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01026d8:	50                   	push   %eax
f01026d9:	68 68 6f 10 f0       	push   $0xf0106f68
f01026de:	6a 58                	push   $0x58
f01026e0:	68 6d 7f 10 f0       	push   $0xf0107f6d
f01026e5:	e8 62 d9 ff ff       	call   f010004c <_panic>

	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
f01026ea:	83 ec 04             	sub    $0x4,%esp
f01026ed:	68 00 10 00 00       	push   $0x1000
f01026f2:	68 ff 00 00 00       	push   $0xff
f01026f7:	2d 00 00 00 10       	sub    $0x10000000,%eax
f01026fc:	50                   	push   %eax
f01026fd:	e8 02 39 00 00       	call   f0106004 <memset>
	page_free(pp0);
f0102702:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0102705:	89 3c 24             	mov    %edi,(%esp)
f0102708:	e8 72 eb ff ff       	call   f010127f <page_free>
	pgdir_walk(kern_pgdir, 0x0, 1);
f010270d:	83 c4 0c             	add    $0xc,%esp
f0102710:	6a 01                	push   $0x1
f0102712:	6a 00                	push   $0x0
f0102714:	ff 35 28 34 27 f0    	pushl  0xf0273428
f010271a:	e8 e1 eb ff ff       	call   f0101300 <pgdir_walk>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010271f:	89 fa                	mov    %edi,%edx
f0102721:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f0102727:	c1 fa 03             	sar    $0x3,%edx
f010272a:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010272d:	89 d0                	mov    %edx,%eax
f010272f:	c1 e8 0c             	shr    $0xc,%eax
f0102732:	83 c4 10             	add    $0x10,%esp
f0102735:	3b 05 24 34 27 f0    	cmp    0xf0273424,%eax
f010273b:	72 12                	jb     f010274f <mem_init+0xff9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010273d:	52                   	push   %edx
f010273e:	68 68 6f 10 f0       	push   $0xf0106f68
f0102743:	6a 58                	push   $0x58
f0102745:	68 6d 7f 10 f0       	push   $0xf0107f6d
f010274a:	e8 fd d8 ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f010274f:	8d 82 00 00 00 f0    	lea    -0x10000000(%edx),%eax
	ptep = (pte_t *) page2kva(pp0);
f0102755:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0102758:	81 ea 00 f0 ff 0f    	sub    $0xffff000,%edx
	for(i=0; i<NPTENTRIES; i++)
		assert((ptep[i] & PTE_P) == 0);
f010275e:	f6 00 01             	testb  $0x1,(%eax)
f0102761:	74 19                	je     f010277c <mem_init+0x1026>
f0102763:	68 1d 83 10 f0       	push   $0xf010831d
f0102768:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010276d:	68 37 05 00 00       	push   $0x537
f0102772:	68 61 7f 10 f0       	push   $0xf0107f61
f0102777:	e8 d0 d8 ff ff       	call   f010004c <_panic>
f010277c:	83 c0 04             	add    $0x4,%eax
	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
	page_free(pp0);
	pgdir_walk(kern_pgdir, 0x0, 1);
	ptep = (pte_t *) page2kva(pp0);
	for(i=0; i<NPTENTRIES; i++)
f010277f:	39 d0                	cmp    %edx,%eax
f0102781:	75 db                	jne    f010275e <mem_init+0x1008>
		assert((ptep[i] & PTE_P) == 0);
	kern_pgdir[0] = 0;
f0102783:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0102788:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	pp0->pp_ref = 0;
f010278e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102791:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// give free list back
	page_free_list = fl;
f0102797:	8b 7d d0             	mov    -0x30(%ebp),%edi
f010279a:	89 3d 40 22 27 f0    	mov    %edi,0xf0272240

	// free the pages we took
	page_free(pp0);
f01027a0:	83 ec 0c             	sub    $0xc,%esp
f01027a3:	50                   	push   %eax
f01027a4:	e8 d6 ea ff ff       	call   f010127f <page_free>
	page_free(pp1);
f01027a9:	89 34 24             	mov    %esi,(%esp)
f01027ac:	e8 ce ea ff ff       	call   f010127f <page_free>
	page_free(pp2);
f01027b1:	89 1c 24             	mov    %ebx,(%esp)
f01027b4:	e8 c6 ea ff ff       	call   f010127f <page_free>

	// test mmio_map_region
	//mmio_map_region(physaddr_t pa, size_t size)
	mm1 = (uintptr_t) mmio_map_region(0, 4097);
f01027b9:	83 c4 08             	add    $0x8,%esp
f01027bc:	68 01 10 00 00       	push   $0x1001
f01027c1:	6a 00                	push   $0x0
f01027c3:	e8 23 ef ff ff       	call   f01016eb <mmio_map_region>
f01027c8:	89 c3                	mov    %eax,%ebx
	mm2 = (uintptr_t) mmio_map_region(0, 4096);
f01027ca:	83 c4 08             	add    $0x8,%esp
f01027cd:	68 00 10 00 00       	push   $0x1000
f01027d2:	6a 00                	push   $0x0
f01027d4:	e8 12 ef ff ff       	call   f01016eb <mmio_map_region>
f01027d9:	89 c6                	mov    %eax,%esi
	// check that they're in the right region
	assert(mm1 >= MMIOBASE && mm1 + 8096 < MMIOLIM);
f01027db:	83 c4 10             	add    $0x10,%esp
f01027de:	81 fb ff ff 7f ef    	cmp    $0xef7fffff,%ebx
f01027e4:	76 0e                	jbe    f01027f4 <mem_init+0x109e>
f01027e6:	8d bb a0 1f 00 00    	lea    0x1fa0(%ebx),%edi
f01027ec:	81 ff ff ff bf ef    	cmp    $0xefbfffff,%edi
f01027f2:	76 19                	jbe    f010280d <mem_init+0x10b7>
f01027f4:	68 ac 7b 10 f0       	push   $0xf0107bac
f01027f9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01027fe:	68 48 05 00 00       	push   $0x548
f0102803:	68 61 7f 10 f0       	push   $0xf0107f61
f0102808:	e8 3f d8 ff ff       	call   f010004c <_panic>
	assert(mm2 >= MMIOBASE && mm2 + 8096 < MMIOLIM);
f010280d:	3d ff ff 7f ef       	cmp    $0xef7fffff,%eax
f0102812:	76 0d                	jbe    f0102821 <mem_init+0x10cb>
f0102814:	8d 80 a0 1f 00 00    	lea    0x1fa0(%eax),%eax
f010281a:	3d ff ff bf ef       	cmp    $0xefbfffff,%eax
f010281f:	76 19                	jbe    f010283a <mem_init+0x10e4>
f0102821:	68 d4 7b 10 f0       	push   $0xf0107bd4
f0102826:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010282b:	68 49 05 00 00       	push   $0x549
f0102830:	68 61 7f 10 f0       	push   $0xf0107f61
f0102835:	e8 12 d8 ff ff       	call   f010004c <_panic>
	// check that they're page-aligned
	assert(mm1 % PGSIZE == 0 && mm2 % PGSIZE == 0);
f010283a:	89 d8                	mov    %ebx,%eax
f010283c:	09 f0                	or     %esi,%eax
f010283e:	a9 ff 0f 00 00       	test   $0xfff,%eax
f0102843:	74 19                	je     f010285e <mem_init+0x1108>
f0102845:	68 fc 7b 10 f0       	push   $0xf0107bfc
f010284a:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010284f:	68 4b 05 00 00       	push   $0x54b
f0102854:	68 61 7f 10 f0       	push   $0xf0107f61
f0102859:	e8 ee d7 ff ff       	call   f010004c <_panic>
	// check that they don't overlap
	cprintf("mm1 is: %p\n", mm1);
f010285e:	83 ec 08             	sub    $0x8,%esp
f0102861:	53                   	push   %ebx
f0102862:	68 34 83 10 f0       	push   $0xf0108334
f0102867:	e8 8c 13 00 00       	call   f0103bf8 <cprintf>
	cprintf("mm2 is: %p\n", mm2);
f010286c:	83 c4 08             	add    $0x8,%esp
f010286f:	56                   	push   %esi
f0102870:	68 40 83 10 f0       	push   $0xf0108340
f0102875:	e8 7e 13 00 00       	call   f0103bf8 <cprintf>
	assert(mm1 + 8096 <= mm2);
f010287a:	83 c4 10             	add    $0x10,%esp
f010287d:	39 fe                	cmp    %edi,%esi
f010287f:	73 19                	jae    f010289a <mem_init+0x1144>
f0102881:	68 4c 83 10 f0       	push   $0xf010834c
f0102886:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010288b:	68 4f 05 00 00       	push   $0x54f
f0102890:	68 61 7f 10 f0       	push   $0xf0107f61
f0102895:	e8 b2 d7 ff ff       	call   f010004c <_panic>
	// check page mappings
	assert(check_va2pa(kern_pgdir, mm1) == 0);
f010289a:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi
f01028a0:	89 da                	mov    %ebx,%edx
f01028a2:	89 f8                	mov    %edi,%eax
f01028a4:	e8 6f e4 ff ff       	call   f0100d18 <check_va2pa>
f01028a9:	85 c0                	test   %eax,%eax
f01028ab:	74 19                	je     f01028c6 <mem_init+0x1170>
f01028ad:	68 24 7c 10 f0       	push   $0xf0107c24
f01028b2:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01028b7:	68 51 05 00 00       	push   $0x551
f01028bc:	68 61 7f 10 f0       	push   $0xf0107f61
f01028c1:	e8 86 d7 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, mm1+PGSIZE) == PGSIZE);
f01028c6:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
f01028cc:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01028cf:	89 c2                	mov    %eax,%edx
f01028d1:	89 f8                	mov    %edi,%eax
f01028d3:	e8 40 e4 ff ff       	call   f0100d18 <check_va2pa>
f01028d8:	3d 00 10 00 00       	cmp    $0x1000,%eax
f01028dd:	74 19                	je     f01028f8 <mem_init+0x11a2>
f01028df:	68 48 7c 10 f0       	push   $0xf0107c48
f01028e4:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01028e9:	68 52 05 00 00       	push   $0x552
f01028ee:	68 61 7f 10 f0       	push   $0xf0107f61
f01028f3:	e8 54 d7 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, mm2) == 0);
f01028f8:	89 f2                	mov    %esi,%edx
f01028fa:	89 f8                	mov    %edi,%eax
f01028fc:	e8 17 e4 ff ff       	call   f0100d18 <check_va2pa>
f0102901:	85 c0                	test   %eax,%eax
f0102903:	74 19                	je     f010291e <mem_init+0x11c8>
f0102905:	68 78 7c 10 f0       	push   $0xf0107c78
f010290a:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010290f:	68 53 05 00 00       	push   $0x553
f0102914:	68 61 7f 10 f0       	push   $0xf0107f61
f0102919:	e8 2e d7 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, mm2+PGSIZE) == ~0);
f010291e:	8d 96 00 10 00 00    	lea    0x1000(%esi),%edx
f0102924:	89 f8                	mov    %edi,%eax
f0102926:	e8 ed e3 ff ff       	call   f0100d18 <check_va2pa>
f010292b:	83 f8 ff             	cmp    $0xffffffff,%eax
f010292e:	74 19                	je     f0102949 <mem_init+0x11f3>
f0102930:	68 9c 7c 10 f0       	push   $0xf0107c9c
f0102935:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010293a:	68 54 05 00 00       	push   $0x554
f010293f:	68 61 7f 10 f0       	push   $0xf0107f61
f0102944:	e8 03 d7 ff ff       	call   f010004c <_panic>
	// check permissions
	assert(*pgdir_walk(kern_pgdir, (void*) mm1, 0) & (PTE_W|PTE_PWT|PTE_PCD));
f0102949:	83 ec 04             	sub    $0x4,%esp
f010294c:	6a 00                	push   $0x0
f010294e:	53                   	push   %ebx
f010294f:	57                   	push   %edi
f0102950:	e8 ab e9 ff ff       	call   f0101300 <pgdir_walk>
f0102955:	83 c4 10             	add    $0x10,%esp
f0102958:	f6 00 1a             	testb  $0x1a,(%eax)
f010295b:	75 19                	jne    f0102976 <mem_init+0x1220>
f010295d:	68 c8 7c 10 f0       	push   $0xf0107cc8
f0102962:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102967:	68 56 05 00 00       	push   $0x556
f010296c:	68 61 7f 10 f0       	push   $0xf0107f61
f0102971:	e8 d6 d6 ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) mm1, 0) & PTE_U));
f0102976:	83 ec 04             	sub    $0x4,%esp
f0102979:	6a 00                	push   $0x0
f010297b:	53                   	push   %ebx
f010297c:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0102982:	e8 79 e9 ff ff       	call   f0101300 <pgdir_walk>
f0102987:	8b 00                	mov    (%eax),%eax
f0102989:	83 c4 10             	add    $0x10,%esp
f010298c:	83 e0 04             	and    $0x4,%eax
f010298f:	89 45 c8             	mov    %eax,-0x38(%ebp)
f0102992:	74 19                	je     f01029ad <mem_init+0x1257>
f0102994:	68 0c 7d 10 f0       	push   $0xf0107d0c
f0102999:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010299e:	68 57 05 00 00       	push   $0x557
f01029a3:	68 61 7f 10 f0       	push   $0xf0107f61
f01029a8:	e8 9f d6 ff ff       	call   f010004c <_panic>
	// clear the mappings
	*pgdir_walk(kern_pgdir, (void*) mm1, 0) = 0;
f01029ad:	83 ec 04             	sub    $0x4,%esp
f01029b0:	6a 00                	push   $0x0
f01029b2:	53                   	push   %ebx
f01029b3:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01029b9:	e8 42 e9 ff ff       	call   f0101300 <pgdir_walk>
f01029be:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	*pgdir_walk(kern_pgdir, (void*) mm1 + PGSIZE, 0) = 0;
f01029c4:	83 c4 0c             	add    $0xc,%esp
f01029c7:	6a 00                	push   $0x0
f01029c9:	ff 75 d4             	pushl  -0x2c(%ebp)
f01029cc:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01029d2:	e8 29 e9 ff ff       	call   f0101300 <pgdir_walk>
f01029d7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	*pgdir_walk(kern_pgdir, (void*) mm2, 0) = 0;
f01029dd:	83 c4 0c             	add    $0xc,%esp
f01029e0:	6a 00                	push   $0x0
f01029e2:	56                   	push   %esi
f01029e3:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01029e9:	e8 12 e9 ff ff       	call   f0101300 <pgdir_walk>
f01029ee:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

	cprintf("check_page() succeeded!\n");
f01029f4:	c7 04 24 5e 83 10 f0 	movl   $0xf010835e,(%esp)
f01029fb:	e8 f8 11 00 00       	call   f0103bf8 <cprintf>
	//    - the new image at UPAGES -- kernel R, user R
	//      (ie. perm = PTE_U | PTE_P)
	//    - pages itself -- kernel RW, user NONE
	// Your code goes here:
	size_t pags_Size = npages * sizeof(struct PageInfo);
	physaddr_t pages_PA = PADDR(pages);//page2pa(pages);
f0102a00:	a1 2c 34 27 f0       	mov    0xf027342c,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102a05:	83 c4 10             	add    $0x10,%esp
f0102a08:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102a0d:	77 15                	ja     f0102a24 <mem_init+0x12ce>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102a0f:	50                   	push   %eax
f0102a10:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102a15:	68 cd 00 00 00       	push   $0xcd
f0102a1a:	68 61 7f 10 f0       	push   $0xf0107f61
f0102a1f:	e8 28 d6 ff ff       	call   f010004c <_panic>
//boot_map_region(kern_pgdir, UPAGES, PTSIZE, pages_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U);
f0102a24:	83 ec 08             	sub    $0x8,%esp
f0102a27:	6a 04                	push   $0x4
f0102a29:	05 00 00 00 10       	add    $0x10000000,%eax
f0102a2e:	50                   	push   %eax
f0102a2f:	b9 00 00 40 00       	mov    $0x400000,%ecx
f0102a34:	ba 00 00 00 ef       	mov    $0xef000000,%edx
f0102a39:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0102a3e:	e8 a1 e9 ff ff       	call   f01013e4 <boot_map_region>

	size_t envs_Size = NENV * sizeof(struct Env);
	physaddr_t envs_PA = PADDR(envs);
f0102a43:	a1 44 22 27 f0       	mov    0xf0272244,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102a48:	83 c4 10             	add    $0x10,%esp
f0102a4b:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102a50:	77 15                	ja     f0102a67 <mem_init+0x1311>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102a52:	50                   	push   %eax
f0102a53:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102a58:	68 d3 00 00 00       	push   $0xd3
f0102a5d:	68 61 7f 10 f0       	push   $0xf0107f61
f0102a62:	e8 e5 d5 ff ff       	call   f010004c <_panic>
	//the memory backing envs should also be mapped user read-only at UENVS
	//boot_map_region(kern_pgdir, UENVS, envs_Size, envs_PA, PTE_U | PTE_P);
//PTSIZE
//boot_map_region(kern_pgdir, UENVS, PTSIZE, envs_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UENVS, PTSIZE, PADDR(envs), PTE_U);
f0102a67:	83 ec 08             	sub    $0x8,%esp
f0102a6a:	6a 04                	push   $0x4
f0102a6c:	05 00 00 00 10       	add    $0x10000000,%eax
f0102a71:	50                   	push   %eax
f0102a72:	b9 00 00 40 00       	mov    $0x400000,%ecx
f0102a77:	ba 00 00 c0 ee       	mov    $0xeec00000,%edx
f0102a7c:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0102a81:	e8 5e e9 ff ff       	call   f01013e4 <boot_map_region>
	cprintf("In mem_init(), UENVS is %p, and envs is: %p\n\n\n\n", UENVS, envs);
f0102a86:	83 c4 0c             	add    $0xc,%esp
f0102a89:	ff 35 44 22 27 f0    	pushl  0xf0272244
f0102a8f:	68 00 00 c0 ee       	push   $0xeec00000
f0102a94:	68 40 7d 10 f0       	push   $0xf0107d40
f0102a99:	e8 5a 11 00 00       	call   f0103bf8 <cprintf>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102a9e:	83 c4 10             	add    $0x10,%esp
f0102aa1:	b8 00 a0 11 f0       	mov    $0xf011a000,%eax
f0102aa6:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102aab:	77 15                	ja     f0102ac2 <mem_init+0x136c>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102aad:	50                   	push   %eax
f0102aae:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102ab3:	68 f8 00 00 00       	push   $0xf8
f0102ab8:	68 61 7f 10 f0       	push   $0xf0107f61
f0102abd:	e8 8a d5 ff ff       	call   f010004c <_panic>
	  #### (unmapped)
	  KSTACKTOP - PTSIZE
	*/
//boot_map_region(kern_pgdir, (KSTACKTOP-KSTKSIZE), KSTKSIZE, ((physaddr_t)PADDR(bootstack)), PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KSTACKTOP - KSTKSIZE, KSTKSIZE, PADDR(bootstack), PTE_W);  
f0102ac2:	83 ec 08             	sub    $0x8,%esp
f0102ac5:	6a 02                	push   $0x2
f0102ac7:	68 00 a0 11 00       	push   $0x11a000
f0102acc:	b9 00 80 00 00       	mov    $0x8000,%ecx
f0102ad1:	ba 00 80 ff ef       	mov    $0xefff8000,%edx
f0102ad6:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0102adb:	e8 04 e9 ff ff       	call   f01013e4 <boot_map_region>
	// we just set up the mapping anyway.
	// Permissions: kernel RW, user NONE
	// Your code goes here:
//boot_map_region(kern_pgdir, KERNBASE, (0xFFFFFFFF - KERNBASE), 0, PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KERNBASE, -KERNBASE, 0, PTE_W); // -KERNBASE <==> ~KERNBASE + 1 
f0102ae0:	83 c4 08             	add    $0x8,%esp
f0102ae3:	6a 02                	push   $0x2
f0102ae5:	6a 00                	push   $0x0
f0102ae7:	b9 00 00 00 10       	mov    $0x10000000,%ecx
f0102aec:	ba 00 00 00 f0       	mov    $0xf0000000,%edx
f0102af1:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0102af6:	e8 e9 e8 ff ff       	call   f01013e4 <boot_map_region>
f0102afb:	c7 45 bc 00 50 27 f0 	movl   $0xf0275000,-0x44(%ebp)
f0102b02:	83 c4 10             	add    $0x10,%esp
f0102b05:	bb 00 50 27 f0       	mov    $0xf0275000,%ebx
	//             it will fault rather than overwrite another CPU's stack.
	//             Known as a "guard page".
	//     Permissions: kernel RW, user NONE
	//
	// LAB 4: Your code here:
	uintptr_t kernel_Stack_top = KSTACKTOP - KSTKSIZE;
f0102b0a:	be 00 80 ff ef       	mov    $0xefff8000,%esi
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102b0f:	81 fb ff ff ff ef    	cmp    $0xefffffff,%ebx
f0102b15:	77 15                	ja     f0102b2c <mem_init+0x13d6>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102b17:	53                   	push   %ebx
f0102b18:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102b1d:	68 44 01 00 00       	push   $0x144
f0102b22:	68 61 7f 10 f0       	push   $0xf0107f61
f0102b27:	e8 20 d5 ff ff       	call   f010004c <_panic>

    int i;
    for (i = 0; i < NCPU; i++) {
        boot_map_region(kern_pgdir, kernel_Stack_top, KSTKSIZE, PADDR(percpu_kstacks[i]), PTE_W);
f0102b2c:	83 ec 08             	sub    $0x8,%esp
f0102b2f:	6a 02                	push   $0x2
f0102b31:	8d 83 00 00 00 10    	lea    0x10000000(%ebx),%eax
f0102b37:	50                   	push   %eax
f0102b38:	b9 00 80 00 00       	mov    $0x8000,%ecx
f0102b3d:	89 f2                	mov    %esi,%edx
f0102b3f:	a1 28 34 27 f0       	mov    0xf0273428,%eax
f0102b44:	e8 9b e8 ff ff       	call   f01013e4 <boot_map_region>
        kernel_Stack_top -= (KSTKSIZE + KSTKGAP);
f0102b49:	81 ee 00 00 01 00    	sub    $0x10000,%esi
f0102b4f:	81 c3 00 80 00 00    	add    $0x8000,%ebx
	//
	// LAB 4: Your code here:
	uintptr_t kernel_Stack_top = KSTACKTOP - KSTKSIZE;

    int i;
    for (i = 0; i < NCPU; i++) {
f0102b55:	83 c4 10             	add    $0x10,%esp
f0102b58:	81 fe 00 80 f7 ef    	cmp    $0xeff78000,%esi
f0102b5e:	75 af                	jne    f0102b0f <mem_init+0x13b9>
check_kern_pgdir(void)
{
	uint32_t i, n;
	pde_t *pgdir;

	pgdir = kern_pgdir;
f0102b60:	8b 3d 28 34 27 f0    	mov    0xf0273428,%edi

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
f0102b66:	a1 24 34 27 f0       	mov    0xf0273424,%eax
f0102b6b:	89 45 c4             	mov    %eax,-0x3c(%ebp)
f0102b6e:	8d 04 c5 ff 0f 00 00 	lea    0xfff(,%eax,8),%eax
f0102b75:	25 00 f0 ff ff       	and    $0xfffff000,%eax
	for (i = 0; i < n; i += PGSIZE){
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f0102b7a:	8b 35 2c 34 27 f0    	mov    0xf027342c,%esi
f0102b80:	89 75 c0             	mov    %esi,-0x40(%ebp)
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102b83:	89 75 d0             	mov    %esi,-0x30(%ebp)
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
	return (physaddr_t)kva - KERNBASE;
f0102b86:	81 c6 00 00 00 10    	add    $0x10000000,%esi
f0102b8c:	89 75 cc             	mov    %esi,-0x34(%ebp)
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f0102b8f:	8b 35 44 22 27 f0    	mov    0xf0272244,%esi
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102b95:	89 75 d4             	mov    %esi,-0x2c(%ebp)

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f0102b98:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102b9d:	e9 b8 00 00 00       	jmp    f0102c5a <mem_init+0x1504>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f0102ba2:	8d 93 00 00 00 ef    	lea    -0x11000000(%ebx),%edx
f0102ba8:	89 f8                	mov    %edi,%eax
f0102baa:	e8 69 e1 ff ff       	call   f0100d18 <check_va2pa>
f0102baf:	81 7d d0 ff ff ff ef 	cmpl   $0xefffffff,-0x30(%ebp)
f0102bb6:	77 17                	ja     f0102bcf <mem_init+0x1479>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102bb8:	ff 75 c0             	pushl  -0x40(%ebp)
f0102bbb:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102bc0:	68 4d 04 00 00       	push   $0x44d
f0102bc5:	68 61 7f 10 f0       	push   $0xf0107f61
f0102bca:	e8 7d d4 ff ff       	call   f010004c <_panic>
f0102bcf:	03 5d cc             	add    -0x34(%ebp),%ebx
f0102bd2:	39 d8                	cmp    %ebx,%eax
f0102bd4:	74 19                	je     f0102bef <mem_init+0x1499>
f0102bd6:	68 70 7d 10 f0       	push   $0xf0107d70
f0102bdb:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102be0:	68 4d 04 00 00       	push   $0x44d
f0102be5:	68 61 7f 10 f0       	push   $0xf0107f61
f0102bea:	e8 5d d4 ff ff       	call   f010004c <_panic>
f0102bef:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102bf4:	eb 02                	jmp    f0102bf8 <mem_init+0x14a2>
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102bf6:	89 c3                	mov    %eax,%ebx
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f0102bf8:	8d 93 00 00 c0 ee    	lea    -0x11400000(%ebx),%edx
f0102bfe:	89 f8                	mov    %edi,%eax
f0102c00:	e8 13 e1 ff ff       	call   f0100d18 <check_va2pa>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102c05:	81 7d d4 ff ff ff ef 	cmpl   $0xefffffff,-0x2c(%ebp)
f0102c0c:	77 15                	ja     f0102c23 <mem_init+0x14cd>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102c0e:	56                   	push   %esi
f0102c0f:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102c14:	68 53 04 00 00       	push   $0x453
f0102c19:	68 61 7f 10 f0       	push   $0xf0107f61
f0102c1e:	e8 29 d4 ff ff       	call   f010004c <_panic>
f0102c23:	8d 94 1e 00 00 00 10 	lea    0x10000000(%esi,%ebx,1),%edx
f0102c2a:	39 d0                	cmp    %edx,%eax
f0102c2c:	74 19                	je     f0102c47 <mem_init+0x14f1>
f0102c2e:	68 a4 7d 10 f0       	push   $0xf0107da4
f0102c33:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102c38:	68 53 04 00 00       	push   $0x453
f0102c3d:	68 61 7f 10 f0       	push   $0xf0107f61
f0102c42:	e8 05 d4 ff ff       	call   f010004c <_panic>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102c47:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
f0102c4d:	3d 00 f0 01 00       	cmp    $0x1f000,%eax
f0102c52:	75 a2                	jne    f0102bf6 <mem_init+0x14a0>

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f0102c54:	81 c3 00 20 00 00    	add    $0x2000,%ebx
f0102c5a:	39 c3                	cmp    %eax,%ebx
f0102c5c:	0f 82 40 ff ff ff    	jb     f0102ba2 <mem_init+0x144c>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f0102c62:	8b 75 c4             	mov    -0x3c(%ebp),%esi
f0102c65:	c1 e6 0c             	shl    $0xc,%esi
f0102c68:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102c6d:	eb 30                	jmp    f0102c9f <mem_init+0x1549>
		assert(check_va2pa(pgdir, KERNBASE + i) == i);
f0102c6f:	8d 93 00 00 00 f0    	lea    -0x10000000(%ebx),%edx
f0102c75:	89 f8                	mov    %edi,%eax
f0102c77:	e8 9c e0 ff ff       	call   f0100d18 <check_va2pa>
f0102c7c:	39 c3                	cmp    %eax,%ebx
f0102c7e:	74 19                	je     f0102c99 <mem_init+0x1543>
f0102c80:	68 d8 7d 10 f0       	push   $0xf0107dd8
f0102c85:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102c8a:	68 5a 04 00 00       	push   $0x45a
f0102c8f:	68 61 7f 10 f0       	push   $0xf0107f61
f0102c94:	e8 b3 d3 ff ff       	call   f010004c <_panic>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f0102c99:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f0102c9f:	39 f3                	cmp    %esi,%ebx
f0102ca1:	72 cc                	jb     f0102c6f <mem_init+0x1519>
f0102ca3:	c7 45 cc 00 80 ff ef 	movl   $0xefff8000,-0x34(%ebp)
f0102caa:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f0102cad:	8b 7d bc             	mov    -0x44(%ebp),%edi
f0102cb0:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0102cb3:	89 c3                	mov    %eax,%ebx
f0102cb5:	05 00 80 00 00       	add    $0x8000,%eax
f0102cba:	89 45 d0             	mov    %eax,-0x30(%ebp)
	// check kernel stack
	// (updated in lab 4 to check per-CPU kernel stacks)
	for (n = 0; n < NCPU; n++) {
		uint32_t base = KSTACKTOP - (KSTKSIZE + KSTKGAP) * (n + 1);
		for (i = 0; i < KSTKSIZE; i += PGSIZE)
			assert(check_va2pa(pgdir, base + KSTKGAP + i)
f0102cbd:	8b 45 c8             	mov    -0x38(%ebp),%eax
f0102cc0:	8d b0 00 80 00 20    	lea    0x20008000(%eax),%esi
f0102cc6:	89 da                	mov    %ebx,%edx
f0102cc8:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102ccb:	e8 48 e0 ff ff       	call   f0100d18 <check_va2pa>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102cd0:	81 ff ff ff ff ef    	cmp    $0xefffffff,%edi
f0102cd6:	77 15                	ja     f0102ced <mem_init+0x1597>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102cd8:	57                   	push   %edi
f0102cd9:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102cde:	68 62 04 00 00       	push   $0x462
f0102ce3:	68 61 7f 10 f0       	push   $0xf0107f61
f0102ce8:	e8 5f d3 ff ff       	call   f010004c <_panic>
f0102ced:	8d 94 33 00 50 27 f0 	lea    -0xfd8b000(%ebx,%esi,1),%edx
f0102cf4:	39 d0                	cmp    %edx,%eax
f0102cf6:	74 19                	je     f0102d11 <mem_init+0x15bb>
f0102cf8:	68 00 7e 10 f0       	push   $0xf0107e00
f0102cfd:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102d02:	68 62 04 00 00       	push   $0x462
f0102d07:	68 61 7f 10 f0       	push   $0xf0107f61
f0102d0c:	e8 3b d3 ff ff       	call   f010004c <_panic>
f0102d11:	81 c3 00 10 00 00    	add    $0x1000,%ebx

	// check kernel stack
	// (updated in lab 4 to check per-CPU kernel stacks)
	for (n = 0; n < NCPU; n++) {
		uint32_t base = KSTACKTOP - (KSTKSIZE + KSTKGAP) * (n + 1);
		for (i = 0; i < KSTKSIZE; i += PGSIZE)
f0102d17:	3b 5d d0             	cmp    -0x30(%ebp),%ebx
f0102d1a:	75 aa                	jne    f0102cc6 <mem_init+0x1570>
f0102d1c:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0102d1f:	8d 98 00 80 ff ff    	lea    -0x8000(%eax),%ebx
f0102d25:	89 c6                	mov    %eax,%esi
f0102d27:	89 7d d0             	mov    %edi,-0x30(%ebp)
f0102d2a:	8b 7d d4             	mov    -0x2c(%ebp),%edi
			assert(check_va2pa(pgdir, base + KSTKGAP + i)
				== PADDR(percpu_kstacks[n]) + i);
		for (i = 0; i < KSTKGAP; i += PGSIZE)
			assert(check_va2pa(pgdir, base + i) == ~0);
f0102d2d:	89 da                	mov    %ebx,%edx
f0102d2f:	89 f8                	mov    %edi,%eax
f0102d31:	e8 e2 df ff ff       	call   f0100d18 <check_va2pa>
f0102d36:	83 f8 ff             	cmp    $0xffffffff,%eax
f0102d39:	74 19                	je     f0102d54 <mem_init+0x15fe>
f0102d3b:	68 48 7e 10 f0       	push   $0xf0107e48
f0102d40:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102d45:	68 64 04 00 00       	push   $0x464
f0102d4a:	68 61 7f 10 f0       	push   $0xf0107f61
f0102d4f:	e8 f8 d2 ff ff       	call   f010004c <_panic>
f0102d54:	81 c3 00 10 00 00    	add    $0x1000,%ebx
	for (n = 0; n < NCPU; n++) {
		uint32_t base = KSTACKTOP - (KSTKSIZE + KSTKGAP) * (n + 1);
		for (i = 0; i < KSTKSIZE; i += PGSIZE)
			assert(check_va2pa(pgdir, base + KSTKGAP + i)
				== PADDR(percpu_kstacks[n]) + i);
		for (i = 0; i < KSTKGAP; i += PGSIZE)
f0102d5a:	39 f3                	cmp    %esi,%ebx
f0102d5c:	75 cf                	jne    f0102d2d <mem_init+0x15d7>
f0102d5e:	8b 7d d0             	mov    -0x30(%ebp),%edi
f0102d61:	81 6d cc 00 00 01 00 	subl   $0x10000,-0x34(%ebp)
f0102d68:	81 45 c8 00 80 01 00 	addl   $0x18000,-0x38(%ebp)
f0102d6f:	81 c7 00 80 00 00    	add    $0x8000,%edi
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	// (updated in lab 4 to check per-CPU kernel stacks)
	for (n = 0; n < NCPU; n++) {
f0102d75:	81 ff 00 50 2b f0    	cmp    $0xf02b5000,%edi
f0102d7b:	0f 85 2f ff ff ff    	jne    f0102cb0 <mem_init+0x155a>
f0102d81:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0102d84:	b8 00 00 00 00       	mov    $0x0,%eax
f0102d89:	eb 2a                	jmp    f0102db5 <mem_init+0x165f>
			assert(check_va2pa(pgdir, base + i) == ~0);
	}

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
		switch (i) {
f0102d8b:	8d 90 45 fc ff ff    	lea    -0x3bb(%eax),%edx
f0102d91:	83 fa 04             	cmp    $0x4,%edx
f0102d94:	77 1f                	ja     f0102db5 <mem_init+0x165f>
		case PDX(UVPT):
		case PDX(KSTACKTOP-1):
		case PDX(UPAGES):
		case PDX(UENVS):
		case PDX(MMIOBASE):
			assert(pgdir[i] & PTE_P);
f0102d96:	f6 04 87 01          	testb  $0x1,(%edi,%eax,4)
f0102d9a:	75 7e                	jne    f0102e1a <mem_init+0x16c4>
f0102d9c:	68 77 83 10 f0       	push   $0xf0108377
f0102da1:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102da6:	68 6f 04 00 00       	push   $0x46f
f0102dab:	68 61 7f 10 f0       	push   $0xf0107f61
f0102db0:	e8 97 d2 ff ff       	call   f010004c <_panic>
			break;
		default:
			if (i >= PDX(KERNBASE)) {
f0102db5:	3d bf 03 00 00       	cmp    $0x3bf,%eax
f0102dba:	76 3f                	jbe    f0102dfb <mem_init+0x16a5>
				assert(pgdir[i] & PTE_P);
f0102dbc:	8b 14 87             	mov    (%edi,%eax,4),%edx
f0102dbf:	f6 c2 01             	test   $0x1,%dl
f0102dc2:	75 19                	jne    f0102ddd <mem_init+0x1687>
f0102dc4:	68 77 83 10 f0       	push   $0xf0108377
f0102dc9:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102dce:	68 73 04 00 00       	push   $0x473
f0102dd3:	68 61 7f 10 f0       	push   $0xf0107f61
f0102dd8:	e8 6f d2 ff ff       	call   f010004c <_panic>
				assert(pgdir[i] & PTE_W);
f0102ddd:	f6 c2 02             	test   $0x2,%dl
f0102de0:	75 38                	jne    f0102e1a <mem_init+0x16c4>
f0102de2:	68 88 83 10 f0       	push   $0xf0108388
f0102de7:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102dec:	68 74 04 00 00       	push   $0x474
f0102df1:	68 61 7f 10 f0       	push   $0xf0107f61
f0102df6:	e8 51 d2 ff ff       	call   f010004c <_panic>
			} else
				assert(pgdir[i] == 0);
f0102dfb:	83 3c 87 00          	cmpl   $0x0,(%edi,%eax,4)
f0102dff:	74 19                	je     f0102e1a <mem_init+0x16c4>
f0102e01:	68 99 83 10 f0       	push   $0xf0108399
f0102e06:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102e0b:	68 76 04 00 00       	push   $0x476
f0102e10:	68 61 7f 10 f0       	push   $0xf0107f61
f0102e15:	e8 32 d2 ff ff       	call   f010004c <_panic>
		for (i = 0; i < KSTKGAP; i += PGSIZE)
			assert(check_va2pa(pgdir, base + i) == ~0);
	}

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
f0102e1a:	40                   	inc    %eax
f0102e1b:	3d ff 03 00 00       	cmp    $0x3ff,%eax
f0102e20:	0f 86 65 ff ff ff    	jbe    f0102d8b <mem_init+0x1635>
			} else
				assert(pgdir[i] == 0);
			break;
		}
	}
	cprintf("check_kern_pgdir() succeeded!\n");
f0102e26:	83 ec 0c             	sub    $0xc,%esp
f0102e29:	68 6c 7e 10 f0       	push   $0xf0107e6c
f0102e2e:	e8 c5 0d 00 00       	call   f0103bf8 <cprintf>
	// somewhere between KERNBASE and KERNBASE+4MB right now, which is
	// mapped the same way by both page tables.
	//
	// If the machine reboots at this point, you've probably set up your
	// kern_pgdir wrong.
	lcr3(PADDR(kern_pgdir));
f0102e33:	a1 28 34 27 f0       	mov    0xf0273428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102e38:	83 c4 10             	add    $0x10,%esp
f0102e3b:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102e40:	77 15                	ja     f0102e57 <mem_init+0x1701>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102e42:	50                   	push   %eax
f0102e43:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0102e48:	68 16 01 00 00       	push   $0x116
f0102e4d:	68 61 7f 10 f0       	push   $0xf0107f61
f0102e52:	e8 f5 d1 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0102e57:	05 00 00 00 10       	add    $0x10000000,%eax
f0102e5c:	0f 22 d8             	mov    %eax,%cr3

	check_page_free_list(0);
f0102e5f:	b8 00 00 00 00       	mov    $0x0,%eax
f0102e64:	e8 0e df ff ff       	call   f0100d77 <check_page_free_list>

static inline uint32_t
rcr0(void)
{
	uint32_t val;
	asm volatile("movl %%cr0,%0" : "=r" (val));
f0102e69:	0f 20 c0             	mov    %cr0,%eax
f0102e6c:	83 e0 f3             	and    $0xfffffff3,%eax
}

static inline void
lcr0(uint32_t val)
{
	asm volatile("movl %0,%%cr0" : : "r" (val));
f0102e6f:	0d 23 00 05 80       	or     $0x80050023,%eax
f0102e74:	0f 22 c0             	mov    %eax,%cr0
	uintptr_t va;
	int i;

	// check that we can read and write installed pages
	pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0102e77:	83 ec 0c             	sub    $0xc,%esp
f0102e7a:	6a 00                	push   $0x0
f0102e7c:	e8 88 e3 ff ff       	call   f0101209 <page_alloc>
f0102e81:	89 c3                	mov    %eax,%ebx
f0102e83:	83 c4 10             	add    $0x10,%esp
f0102e86:	85 c0                	test   %eax,%eax
f0102e88:	75 19                	jne    f0102ea3 <mem_init+0x174d>
f0102e8a:	68 6b 81 10 f0       	push   $0xf010816b
f0102e8f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102e94:	68 6c 05 00 00       	push   $0x56c
f0102e99:	68 61 7f 10 f0       	push   $0xf0107f61
f0102e9e:	e8 a9 d1 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0102ea3:	83 ec 0c             	sub    $0xc,%esp
f0102ea6:	6a 00                	push   $0x0
f0102ea8:	e8 5c e3 ff ff       	call   f0101209 <page_alloc>
f0102ead:	89 c7                	mov    %eax,%edi
f0102eaf:	83 c4 10             	add    $0x10,%esp
f0102eb2:	85 c0                	test   %eax,%eax
f0102eb4:	75 19                	jne    f0102ecf <mem_init+0x1779>
f0102eb6:	68 81 81 10 f0       	push   $0xf0108181
f0102ebb:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102ec0:	68 6d 05 00 00       	push   $0x56d
f0102ec5:	68 61 7f 10 f0       	push   $0xf0107f61
f0102eca:	e8 7d d1 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0102ecf:	83 ec 0c             	sub    $0xc,%esp
f0102ed2:	6a 00                	push   $0x0
f0102ed4:	e8 30 e3 ff ff       	call   f0101209 <page_alloc>
f0102ed9:	89 c6                	mov    %eax,%esi
f0102edb:	83 c4 10             	add    $0x10,%esp
f0102ede:	85 c0                	test   %eax,%eax
f0102ee0:	75 19                	jne    f0102efb <mem_init+0x17a5>
f0102ee2:	68 97 81 10 f0       	push   $0xf0108197
f0102ee7:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102eec:	68 6e 05 00 00       	push   $0x56e
f0102ef1:	68 61 7f 10 f0       	push   $0xf0107f61
f0102ef6:	e8 51 d1 ff ff       	call   f010004c <_panic>
	page_free(pp0);
f0102efb:	83 ec 0c             	sub    $0xc,%esp
f0102efe:	53                   	push   %ebx
f0102eff:	e8 7b e3 ff ff       	call   f010127f <page_free>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102f04:	89 f8                	mov    %edi,%eax
f0102f06:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0102f0c:	c1 f8 03             	sar    $0x3,%eax
f0102f0f:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102f12:	89 c2                	mov    %eax,%edx
f0102f14:	c1 ea 0c             	shr    $0xc,%edx
f0102f17:	83 c4 10             	add    $0x10,%esp
f0102f1a:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f0102f20:	72 12                	jb     f0102f34 <mem_init+0x17de>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102f22:	50                   	push   %eax
f0102f23:	68 68 6f 10 f0       	push   $0xf0106f68
f0102f28:	6a 58                	push   $0x58
f0102f2a:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0102f2f:	e8 18 d1 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp1), 1, PGSIZE);
f0102f34:	83 ec 04             	sub    $0x4,%esp
f0102f37:	68 00 10 00 00       	push   $0x1000
f0102f3c:	6a 01                	push   $0x1
f0102f3e:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102f43:	50                   	push   %eax
f0102f44:	e8 bb 30 00 00       	call   f0106004 <memset>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102f49:	89 f0                	mov    %esi,%eax
f0102f4b:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0102f51:	c1 f8 03             	sar    $0x3,%eax
f0102f54:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102f57:	89 c2                	mov    %eax,%edx
f0102f59:	c1 ea 0c             	shr    $0xc,%edx
f0102f5c:	83 c4 10             	add    $0x10,%esp
f0102f5f:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f0102f65:	72 12                	jb     f0102f79 <mem_init+0x1823>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102f67:	50                   	push   %eax
f0102f68:	68 68 6f 10 f0       	push   $0xf0106f68
f0102f6d:	6a 58                	push   $0x58
f0102f6f:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0102f74:	e8 d3 d0 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp2), 2, PGSIZE);
f0102f79:	83 ec 04             	sub    $0x4,%esp
f0102f7c:	68 00 10 00 00       	push   $0x1000
f0102f81:	6a 02                	push   $0x2
f0102f83:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102f88:	50                   	push   %eax
f0102f89:	e8 76 30 00 00       	call   f0106004 <memset>
	page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W);
f0102f8e:	6a 02                	push   $0x2
f0102f90:	68 00 10 00 00       	push   $0x1000
f0102f95:	57                   	push   %edi
f0102f96:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0102f9c:	e8 e4 e6 ff ff       	call   f0101685 <page_insert>
	assert(pp1->pp_ref == 1);
f0102fa1:	83 c4 20             	add    $0x20,%esp
f0102fa4:	66 83 7f 04 01       	cmpw   $0x1,0x4(%edi)
f0102fa9:	74 19                	je     f0102fc4 <mem_init+0x186e>
f0102fab:	68 68 82 10 f0       	push   $0xf0108268
f0102fb0:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102fb5:	68 73 05 00 00       	push   $0x573
f0102fba:	68 61 7f 10 f0       	push   $0xf0107f61
f0102fbf:	e8 88 d0 ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)PGSIZE == 0x01010101U);
f0102fc4:	81 3d 00 10 00 00 01 	cmpl   $0x1010101,0x1000
f0102fcb:	01 01 01 
f0102fce:	74 19                	je     f0102fe9 <mem_init+0x1893>
f0102fd0:	68 8c 7e 10 f0       	push   $0xf0107e8c
f0102fd5:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0102fda:	68 74 05 00 00       	push   $0x574
f0102fdf:	68 61 7f 10 f0       	push   $0xf0107f61
f0102fe4:	e8 63 d0 ff ff       	call   f010004c <_panic>
	page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W);
f0102fe9:	6a 02                	push   $0x2
f0102feb:	68 00 10 00 00       	push   $0x1000
f0102ff0:	56                   	push   %esi
f0102ff1:	ff 35 28 34 27 f0    	pushl  0xf0273428
f0102ff7:	e8 89 e6 ff ff       	call   f0101685 <page_insert>
	assert(*(uint32_t *)PGSIZE == 0x02020202U);
f0102ffc:	83 c4 10             	add    $0x10,%esp
f0102fff:	81 3d 00 10 00 00 02 	cmpl   $0x2020202,0x1000
f0103006:	02 02 02 
f0103009:	74 19                	je     f0103024 <mem_init+0x18ce>
f010300b:	68 b0 7e 10 f0       	push   $0xf0107eb0
f0103010:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0103015:	68 76 05 00 00       	push   $0x576
f010301a:	68 61 7f 10 f0       	push   $0xf0107f61
f010301f:	e8 28 d0 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0103024:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0103029:	74 19                	je     f0103044 <mem_init+0x18ee>
f010302b:	68 8a 82 10 f0       	push   $0xf010828a
f0103030:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0103035:	68 77 05 00 00       	push   $0x577
f010303a:	68 61 7f 10 f0       	push   $0xf0107f61
f010303f:	e8 08 d0 ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f0103044:	66 83 7f 04 00       	cmpw   $0x0,0x4(%edi)
f0103049:	74 19                	je     f0103064 <mem_init+0x190e>
f010304b:	68 f4 82 10 f0       	push   $0xf01082f4
f0103050:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0103055:	68 78 05 00 00       	push   $0x578
f010305a:	68 61 7f 10 f0       	push   $0xf0107f61
f010305f:	e8 e8 cf ff ff       	call   f010004c <_panic>
	*(uint32_t *)PGSIZE = 0x03030303U;
f0103064:	c7 05 00 10 00 00 03 	movl   $0x3030303,0x1000
f010306b:	03 03 03 
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010306e:	89 f0                	mov    %esi,%eax
f0103070:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f0103076:	c1 f8 03             	sar    $0x3,%eax
f0103079:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010307c:	89 c2                	mov    %eax,%edx
f010307e:	c1 ea 0c             	shr    $0xc,%edx
f0103081:	3b 15 24 34 27 f0    	cmp    0xf0273424,%edx
f0103087:	72 12                	jb     f010309b <mem_init+0x1945>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0103089:	50                   	push   %eax
f010308a:	68 68 6f 10 f0       	push   $0xf0106f68
f010308f:	6a 58                	push   $0x58
f0103091:	68 6d 7f 10 f0       	push   $0xf0107f6d
f0103096:	e8 b1 cf ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)page2kva(pp2) == 0x03030303U);
f010309b:	81 b8 00 00 00 f0 03 	cmpl   $0x3030303,-0x10000000(%eax)
f01030a2:	03 03 03 
f01030a5:	74 19                	je     f01030c0 <mem_init+0x196a>
f01030a7:	68 d4 7e 10 f0       	push   $0xf0107ed4
f01030ac:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01030b1:	68 7a 05 00 00       	push   $0x57a
f01030b6:	68 61 7f 10 f0       	push   $0xf0107f61
f01030bb:	e8 8c cf ff ff       	call   f010004c <_panic>
	page_remove(kern_pgdir, (void*) PGSIZE);
f01030c0:	83 ec 08             	sub    $0x8,%esp
f01030c3:	68 00 10 00 00       	push   $0x1000
f01030c8:	ff 35 28 34 27 f0    	pushl  0xf0273428
f01030ce:	e8 12 e5 ff ff       	call   f01015e5 <page_remove>
	assert(pp2->pp_ref == 0);
f01030d3:	83 c4 10             	add    $0x10,%esp
f01030d6:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f01030db:	74 19                	je     f01030f6 <mem_init+0x19a0>
f01030dd:	68 c2 82 10 f0       	push   $0xf01082c2
f01030e2:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01030e7:	68 7c 05 00 00       	push   $0x57c
f01030ec:	68 61 7f 10 f0       	push   $0xf0107f61
f01030f1:	e8 56 cf ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f01030f6:	8b 0d 28 34 27 f0    	mov    0xf0273428,%ecx
f01030fc:	8b 11                	mov    (%ecx),%edx
f01030fe:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0103104:	89 d8                	mov    %ebx,%eax
f0103106:	2b 05 2c 34 27 f0    	sub    0xf027342c,%eax
f010310c:	c1 f8 03             	sar    $0x3,%eax
f010310f:	c1 e0 0c             	shl    $0xc,%eax
f0103112:	39 c2                	cmp    %eax,%edx
f0103114:	74 19                	je     f010312f <mem_init+0x19d9>
f0103116:	68 2c 78 10 f0       	push   $0xf010782c
f010311b:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0103120:	68 7f 05 00 00       	push   $0x57f
f0103125:	68 61 7f 10 f0       	push   $0xf0107f61
f010312a:	e8 1d cf ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f010312f:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f0103135:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f010313a:	74 19                	je     f0103155 <mem_init+0x19ff>
f010313c:	68 79 82 10 f0       	push   $0xf0108279
f0103141:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0103146:	68 81 05 00 00       	push   $0x581
f010314b:	68 61 7f 10 f0       	push   $0xf0107f61
f0103150:	e8 f7 ce ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f0103155:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)

	// free the pages we took
	page_free(pp0);
f010315b:	83 ec 0c             	sub    $0xc,%esp
f010315e:	53                   	push   %ebx
f010315f:	e8 1b e1 ff ff       	call   f010127f <page_free>

	cprintf("check_page_installed_pgdir() succeeded!\n");
f0103164:	c7 04 24 00 7f 10 f0 	movl   $0xf0107f00,(%esp)
f010316b:	e8 88 0a 00 00       	call   f0103bf8 <cprintf>
	cr0 &= ~(CR0_TS|CR0_EM);
	lcr0(cr0);

	// Some more checks, only possible after kern_pgdir is installed.
	check_page_installed_pgdir();
}
f0103170:	83 c4 10             	add    $0x10,%esp
f0103173:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103176:	5b                   	pop    %ebx
f0103177:	5e                   	pop    %esi
f0103178:	5f                   	pop    %edi
f0103179:	5d                   	pop    %ebp
f010317a:	c3                   	ret    

f010317b <user_mem_check>:
// Returns 0 if the user program can access this range of addresses,
// and -E_FAULT otherwise.
//
int
user_mem_check(struct Env *env, const void *va, size_t len, int perm)
{
f010317b:	55                   	push   %ebp
f010317c:	89 e5                	mov    %esp,%ebp
f010317e:	57                   	push   %edi
f010317f:	56                   	push   %esi
f0103180:	53                   	push   %ebx
f0103181:	83 ec 1c             	sub    $0x1c,%esp
f0103184:	8b 7d 08             	mov    0x8(%ebp),%edi
	num_Page = (end_Va - va_temp)/PGSIZE;

	//PGSIZE
	return 0;*/
		// LAB 3: Your code here.
    perm |= PTE_P;
f0103187:	8b 75 14             	mov    0x14(%ebp),%esi
f010318a:	83 ce 01             	or     $0x1,%esi

	uintptr_t addr = ROUNDDOWN((uint32_t)va, PGSIZE);
f010318d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0103190:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
	uintptr_t end = ROUNDUP((uint32_t)va + len, PGSIZE);
f0103196:	8b 45 10             	mov    0x10(%ebp),%eax
f0103199:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f010319c:	8d 84 01 ff 0f 00 00 	lea    0xfff(%ecx,%eax,1),%eax
f01031a3:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f01031a8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (addr < end) {
f01031ab:	eb 2b                	jmp    f01031d8 <user_mem_check+0x5d>
		if (addr >= ULIM)
f01031ad:	81 fb ff ff 7f ef    	cmp    $0xef7fffff,%ebx
f01031b3:	77 2f                	ja     f01031e4 <user_mem_check+0x69>
            goto bad;
        
        pte_t *ppte = pgdir_walk(env->env_pgdir, (void *)addr, 0);
f01031b5:	83 ec 04             	sub    $0x4,%esp
f01031b8:	6a 00                	push   $0x0
f01031ba:	53                   	push   %ebx
f01031bb:	ff 77 60             	pushl  0x60(%edi)
f01031be:	e8 3d e1 ff ff       	call   f0101300 <pgdir_walk>
        if (!ppte)
f01031c3:	83 c4 10             	add    $0x10,%esp
f01031c6:	85 c0                	test   %eax,%eax
f01031c8:	74 1a                	je     f01031e4 <user_mem_check+0x69>
            goto bad;
        
        if ((*ppte & perm) != perm)
f01031ca:	89 f2                	mov    %esi,%edx
f01031cc:	23 10                	and    (%eax),%edx
f01031ce:	39 d6                	cmp    %edx,%esi
f01031d0:	75 12                	jne    f01031e4 <user_mem_check+0x69>
            goto bad;

        addr += PGSIZE;
f01031d2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		// LAB 3: Your code here.
    perm |= PTE_P;

	uintptr_t addr = ROUNDDOWN((uint32_t)va, PGSIZE);
	uintptr_t end = ROUNDUP((uint32_t)va + len, PGSIZE);
	while (addr < end) {
f01031d8:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f01031db:	72 d0                	jb     f01031ad <user_mem_check+0x32>
            goto bad;

        addr += PGSIZE;
	}

	return 0;
f01031dd:	b8 00 00 00 00       	mov    $0x0,%eax
f01031e2:	eb 1f                	jmp    f0103203 <user_mem_check+0x88>

bad:
    user_mem_check_addr = (uintptr_t)va;
    if (user_mem_check_addr < addr)
f01031e4:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
f01031e7:	77 0f                	ja     f01031f8 <user_mem_check+0x7d>
	}

	return 0;

bad:
    user_mem_check_addr = (uintptr_t)va;
f01031e9:	8b 45 0c             	mov    0xc(%ebp),%eax
f01031ec:	a3 3c 22 27 f0       	mov    %eax,0xf027223c
    if (user_mem_check_addr < addr)
        user_mem_check_addr = addr;
    return -E_FAULT;
f01031f1:	b8 fa ff ff ff       	mov    $0xfffffffa,%eax
f01031f6:	eb 0b                	jmp    f0103203 <user_mem_check+0x88>
	return 0;

bad:
    user_mem_check_addr = (uintptr_t)va;
    if (user_mem_check_addr < addr)
        user_mem_check_addr = addr;
f01031f8:	89 1d 3c 22 27 f0    	mov    %ebx,0xf027223c
    return -E_FAULT;
f01031fe:	b8 fa ff ff ff       	mov    $0xfffffffa,%eax
}
f0103203:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103206:	5b                   	pop    %ebx
f0103207:	5e                   	pop    %esi
f0103208:	5f                   	pop    %edi
f0103209:	5d                   	pop    %ebp
f010320a:	c3                   	ret    

f010320b <user_mem_assert>:
// If it cannot, 'env' is destroyed and, if env is the current
// environment, this function will not return.
//
void
user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
{
f010320b:	55                   	push   %ebp
f010320c:	89 e5                	mov    %esp,%ebp
f010320e:	53                   	push   %ebx
f010320f:	83 ec 04             	sub    $0x4,%esp
f0103212:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (user_mem_check(env, va, len, perm | PTE_U) < 0) {
f0103215:	8b 45 14             	mov    0x14(%ebp),%eax
f0103218:	83 c8 04             	or     $0x4,%eax
f010321b:	50                   	push   %eax
f010321c:	ff 75 10             	pushl  0x10(%ebp)
f010321f:	ff 75 0c             	pushl  0xc(%ebp)
f0103222:	53                   	push   %ebx
f0103223:	e8 53 ff ff ff       	call   f010317b <user_mem_check>
f0103228:	83 c4 10             	add    $0x10,%esp
f010322b:	85 c0                	test   %eax,%eax
f010322d:	79 21                	jns    f0103250 <user_mem_assert+0x45>
		cprintf("[%08x] user_mem_check assertion failure for "
f010322f:	83 ec 04             	sub    $0x4,%esp
f0103232:	ff 35 3c 22 27 f0    	pushl  0xf027223c
f0103238:	ff 73 48             	pushl  0x48(%ebx)
f010323b:	68 2c 7f 10 f0       	push   $0xf0107f2c
f0103240:	e8 b3 09 00 00       	call   f0103bf8 <cprintf>
			"va %08x\n", env->env_id, user_mem_check_addr);
		env_destroy(env);	// may not return
f0103245:	89 1c 24             	mov    %ebx,(%esp)
f0103248:	e8 09 07 00 00       	call   f0103956 <env_destroy>
f010324d:	83 c4 10             	add    $0x10,%esp
	}
}
f0103250:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103253:	c9                   	leave  
f0103254:	c3                   	ret    

f0103255 <region_alloc>:
{
	// LAB 3: Your code here.
	// (But only if you need it for load_icode.)
	//
	int num_Page;
	if(len == 0){
f0103255:	85 c9                	test   %ecx,%ecx
f0103257:	0f 84 9b 00 00 00    	je     f01032f8 <region_alloc+0xa3>
// Pages should be writable by user and kernel.
// Panic if any allocation attempt fails.
//
static void
region_alloc(struct Env *e, void *va, size_t len)
{
f010325d:	55                   	push   %ebp
f010325e:	89 e5                	mov    %esp,%ebp
f0103260:	57                   	push   %edi
f0103261:	56                   	push   %esi
f0103262:	53                   	push   %ebx
f0103263:	83 ec 1c             	sub    $0x1c,%esp
	//
	int num_Page;
	if(len == 0){
		return;
	}
	void* end_Va = va + len;
f0103266:	01 d1                	add    %edx,%ecx
	void* start_Va = (void*)PTE_ADDR(va); //va & ~0xFFF; //round down va
f0103268:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f010326e:	89 d3                	mov    %edx,%ebx
	//figure out how many pages needs to be allocated
	if(((int)end_Va % PGSIZE) == 0){ //already aligned 
f0103270:	f7 c1 ff 0f 00 00    	test   $0xfff,%ecx
f0103276:	75 16                	jne    f010328e <region_alloc+0x39>
		num_Page = ((end_Va - start_Va) / PGSIZE);
f0103278:	29 d1                	sub    %edx,%ecx
f010327a:	89 ca                	mov    %ecx,%edx
f010327c:	85 c9                	test   %ecx,%ecx
f010327e:	79 06                	jns    f0103286 <region_alloc+0x31>
f0103280:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f0103286:	c1 fa 0c             	sar    $0xc,%edx
f0103289:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f010328c:	eb 17                	jmp    f01032a5 <region_alloc+0x50>
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
f010328e:	29 d1                	sub    %edx,%ecx
f0103290:	89 ca                	mov    %ecx,%edx
f0103292:	85 c9                	test   %ecx,%ecx
f0103294:	79 06                	jns    f010329c <region_alloc+0x47>
f0103296:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f010329c:	c1 fa 0c             	sar    $0xc,%edx
f010329f:	8d 4a 01             	lea    0x1(%edx),%ecx
f01032a2:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f01032a5:	89 c7                	mov    %eax,%edi
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f01032a7:	be 00 00 00 00       	mov    $0x0,%esi
f01032ac:	eb 3e                	jmp    f01032ec <region_alloc+0x97>
		struct PageInfo* pp = page_alloc((!ALLOC_ZERO)); //Does not zero
f01032ae:	83 ec 0c             	sub    $0xc,%esp
f01032b1:	6a 00                	push   $0x0
f01032b3:	e8 51 df ff ff       	call   f0101209 <page_alloc>
		if(pp == NULL){
f01032b8:	83 c4 10             	add    $0x10,%esp
f01032bb:	85 c0                	test   %eax,%eax
f01032bd:	75 17                	jne    f01032d6 <region_alloc+0x81>
			panic("allocation attempt fails in region_alloc");
f01032bf:	83 ec 04             	sub    $0x4,%esp
f01032c2:	68 a8 83 10 f0       	push   $0xf01083a8
f01032c7:	68 3f 01 00 00       	push   $0x13f
f01032cc:	68 f3 83 10 f0       	push   $0xf01083f3
f01032d1:	e8 76 cd ff ff       	call   f010004c <_panic>
			//return -E_NO_MEM; //page table couldn't be allocated
		}
		page_insert(e->env_pgdir, pp, start_Va+i*PGSIZE, PTE_W|PTE_U|PTE_P); //user read and write
f01032d6:	6a 07                	push   $0x7
f01032d8:	53                   	push   %ebx
f01032d9:	50                   	push   %eax
f01032da:	ff 77 60             	pushl  0x60(%edi)
f01032dd:	e8 a3 e3 ff ff       	call   f0101685 <page_insert>
		num_Page = ((end_Va - start_Va) / PGSIZE);
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f01032e2:	46                   	inc    %esi
f01032e3:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f01032e9:	83 c4 10             	add    $0x10,%esp
f01032ec:	39 75 e4             	cmp    %esi,-0x1c(%ebp)
f01032ef:	7f bd                	jg     f01032ae <region_alloc+0x59>

	// Hint: It is easier to use region_alloc if the caller can pass
	//   'va' and 'len' values that are not page-aligned.
	//   You should round va down, and round (va + len) up.
	//   (Watch out for corner-cases!)
}
f01032f1:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01032f4:	5b                   	pop    %ebx
f01032f5:	5e                   	pop    %esi
f01032f6:	5f                   	pop    %edi
f01032f7:	5d                   	pop    %ebp
f01032f8:	c3                   	ret    

f01032f9 <envid2env>:
//   On success, sets *env_store to the environment.
//   On error, sets *env_store to NULL.
//
int
envid2env(envid_t envid, struct Env **env_store, bool checkperm)
{
f01032f9:	55                   	push   %ebp
f01032fa:	89 e5                	mov    %esp,%ebp
f01032fc:	56                   	push   %esi
f01032fd:	53                   	push   %ebx
f01032fe:	8b 45 08             	mov    0x8(%ebp),%eax
f0103301:	8b 55 10             	mov    0x10(%ebp),%edx
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
f0103304:	85 c0                	test   %eax,%eax
f0103306:	75 27                	jne    f010332f <envid2env+0x36>
		*env_store = curenv;
f0103308:	e8 fd 33 00 00       	call   f010670a <cpunum>
f010330d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103310:	01 c2                	add    %eax,%edx
f0103312:	01 d2                	add    %edx,%edx
f0103314:	01 c2                	add    %eax,%edx
f0103316:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103319:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0103320:	8b 75 0c             	mov    0xc(%ebp),%esi
f0103323:	89 06                	mov    %eax,(%esi)
		return 0;
f0103325:	b8 00 00 00 00       	mov    $0x0,%eax
f010332a:	e9 8d 00 00 00       	jmp    f01033bc <envid2env+0xc3>
	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
f010332f:	89 c3                	mov    %eax,%ebx
f0103331:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
f0103337:	8d 0c 9d 00 00 00 00 	lea    0x0(,%ebx,4),%ecx
f010333e:	c1 e3 07             	shl    $0x7,%ebx
f0103341:	29 cb                	sub    %ecx,%ebx
f0103343:	03 1d 44 22 27 f0    	add    0xf0272244,%ebx
	if (e->env_status == ENV_FREE || e->env_id != envid) {
f0103349:	83 7b 54 00          	cmpl   $0x0,0x54(%ebx)
f010334d:	74 05                	je     f0103354 <envid2env+0x5b>
f010334f:	3b 43 48             	cmp    0x48(%ebx),%eax
f0103352:	74 10                	je     f0103364 <envid2env+0x6b>
		*env_store = 0;
f0103354:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103357:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f010335d:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f0103362:	eb 58                	jmp    f01033bc <envid2env+0xc3>
	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
f0103364:	84 d2                	test   %dl,%dl
f0103366:	74 4a                	je     f01033b2 <envid2env+0xb9>
f0103368:	e8 9d 33 00 00       	call   f010670a <cpunum>
f010336d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103370:	01 c2                	add    %eax,%edx
f0103372:	01 d2                	add    %edx,%edx
f0103374:	01 c2                	add    %eax,%edx
f0103376:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103379:	3b 1c 85 08 40 27 f0 	cmp    -0xfd8bff8(,%eax,4),%ebx
f0103380:	74 30                	je     f01033b2 <envid2env+0xb9>
f0103382:	8b 73 4c             	mov    0x4c(%ebx),%esi
f0103385:	e8 80 33 00 00       	call   f010670a <cpunum>
f010338a:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010338d:	01 c2                	add    %eax,%edx
f010338f:	01 d2                	add    %edx,%edx
f0103391:	01 c2                	add    %eax,%edx
f0103393:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103396:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f010339d:	3b 70 48             	cmp    0x48(%eax),%esi
f01033a0:	74 10                	je     f01033b2 <envid2env+0xb9>
		*env_store = 0;
f01033a2:	8b 45 0c             	mov    0xc(%ebp),%eax
f01033a5:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f01033ab:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f01033b0:	eb 0a                	jmp    f01033bc <envid2env+0xc3>
	}

	*env_store = e;
f01033b2:	8b 45 0c             	mov    0xc(%ebp),%eax
f01033b5:	89 18                	mov    %ebx,(%eax)
	return 0;
f01033b7:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01033bc:	5b                   	pop    %ebx
f01033bd:	5e                   	pop    %esi
f01033be:	5d                   	pop    %ebp
f01033bf:	c3                   	ret    

f01033c0 <env_init_percpu>:
}

// Load GDT and segment descriptors.
void
env_init_percpu(void)
{
f01033c0:	55                   	push   %ebp
f01033c1:	89 e5                	mov    %esp,%ebp
}

static inline void
lgdt(void *p)
{
	asm volatile("lgdt (%0)" : : "r" (p));
f01033c3:	b8 20 33 12 f0       	mov    $0xf0123320,%eax
f01033c8:	0f 01 10             	lgdtl  (%eax)
	lgdt(&gdt_pd);
	// The kernel never uses GS or FS, so we leave those set to
	// the user data segment.
	asm volatile("movw %%ax,%%gs" : : "a" (GD_UD|3));
f01033cb:	b8 23 00 00 00       	mov    $0x23,%eax
f01033d0:	8e e8                	mov    %eax,%gs
	asm volatile("movw %%ax,%%fs" : : "a" (GD_UD|3));
f01033d2:	8e e0                	mov    %eax,%fs
	// The kernel does use ES, DS, and SS.  We'll change between
	// the kernel and user data segments as needed.
	asm volatile("movw %%ax,%%es" : : "a" (GD_KD));
f01033d4:	b8 10 00 00 00       	mov    $0x10,%eax
f01033d9:	8e c0                	mov    %eax,%es
	asm volatile("movw %%ax,%%ds" : : "a" (GD_KD));
f01033db:	8e d8                	mov    %eax,%ds
	asm volatile("movw %%ax,%%ss" : : "a" (GD_KD));
f01033dd:	8e d0                	mov    %eax,%ss
	// Load the kernel text segment into CS.
	asm volatile("ljmp %0,$1f\n 1:\n" : : "i" (GD_KT));
f01033df:	ea e6 33 10 f0 08 00 	ljmp   $0x8,$0xf01033e6
}

static inline void
lldt(uint16_t sel)
{
	asm volatile("lldt %0" : : "r" (sel));
f01033e6:	b8 00 00 00 00       	mov    $0x0,%eax
f01033eb:	0f 00 d0             	lldt   %ax
	// For good measure, clear the local descriptor table (LDT),
	// since we don't use it.
	lldt(0);
}
f01033ee:	5d                   	pop    %ebp
f01033ef:	c3                   	ret    

f01033f0 <env_init>:
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
f01033f0:	8b 15 44 22 27 f0    	mov    0xf0272244,%edx
f01033f6:	8d 42 7c             	lea    0x7c(%edx),%eax
f01033f9:	81 c2 00 f0 01 00    	add    $0x1f000,%edx
f01033ff:	c7 40 cc 00 00 00 00 	movl   $0x0,-0x34(%eax)
		envs[i].env_link = &envs[i + 1];
f0103406:	89 40 c8             	mov    %eax,-0x38(%eax)
f0103409:	83 c0 7c             	add    $0x7c,%eax
	// Per-CPU part of the initialization
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
f010340c:	39 d0                	cmp    %edx,%eax
f010340e:	75 ef                	jne    f01033ff <env_init+0xf>
// they are in the envs array (i.e., so that the first call to
// env_alloc() returns envs[0]).
//
void
env_init(void)
{
f0103410:	55                   	push   %ebp
f0103411:	89 e5                	mov    %esp,%ebp
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
		envs[i].env_link = &envs[i + 1];
	}
	envs[NENV - 1].env_link = NULL;
f0103413:	a1 44 22 27 f0       	mov    0xf0272244,%eax
f0103418:	c7 80 c8 ef 01 00 00 	movl   $0x0,0x1efc8(%eax)
f010341f:	00 00 00 
	env_free_list = &envs[0];
f0103422:	a3 48 22 27 f0       	mov    %eax,0xf0272248

	// Per-CPU part of the initialization
	env_init_percpu();
f0103427:	e8 94 ff ff ff       	call   f01033c0 <env_init_percpu>
}
f010342c:	5d                   	pop    %ebp
f010342d:	c3                   	ret    

f010342e <env_alloc>:
//	-E_NO_FREE_ENV if all NENVS environments are allocated
//	-E_NO_MEM on memory exhaustion
//
int
env_alloc(struct Env **newenv_store, envid_t parent_id)
{
f010342e:	55                   	push   %ebp
f010342f:	89 e5                	mov    %esp,%ebp
f0103431:	56                   	push   %esi
f0103432:	53                   	push   %ebx
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
f0103433:	8b 1d 48 22 27 f0    	mov    0xf0272248,%ebx
f0103439:	85 db                	test   %ebx,%ebx
f010343b:	0f 84 a0 01 00 00    	je     f01035e1 <env_alloc+0x1b3>
{
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
f0103441:	83 ec 0c             	sub    $0xc,%esp
f0103444:	6a 01                	push   $0x1
f0103446:	e8 be dd ff ff       	call   f0101209 <page_alloc>
f010344b:	83 c4 10             	add    $0x10,%esp
f010344e:	85 c0                	test   %eax,%eax
f0103450:	0f 84 92 01 00 00    	je     f01035e8 <env_alloc+0x1ba>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0103456:	89 c2                	mov    %eax,%edx
f0103458:	2b 15 2c 34 27 f0    	sub    0xf027342c,%edx
f010345e:	c1 fa 03             	sar    $0x3,%edx
f0103461:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103464:	89 d1                	mov    %edx,%ecx
f0103466:	c1 e9 0c             	shr    $0xc,%ecx
f0103469:	3b 0d 24 34 27 f0    	cmp    0xf0273424,%ecx
f010346f:	72 12                	jb     f0103483 <env_alloc+0x55>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0103471:	52                   	push   %edx
f0103472:	68 68 6f 10 f0       	push   $0xf0106f68
f0103477:	6a 58                	push   $0x58
f0103479:	68 6d 7f 10 f0       	push   $0xf0107f6d
f010347e:	e8 c9 cb ff ff       	call   f010004c <_panic>
	//	is an exception -- you need to increment env_pgdir's
	//	pp_ref for env_free to work correctly.
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
f0103483:	81 ea 00 00 00 10    	sub    $0x10000000,%edx
f0103489:	89 53 60             	mov    %edx,0x60(%ebx)
f010348c:	ba ec 0e 00 00       	mov    $0xeec,%edx
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
		e->env_pgdir[i] = kern_pgdir[i];
f0103491:	8b 0d 28 34 27 f0    	mov    0xf0273428,%ecx
f0103497:	8b 34 11             	mov    (%ecx,%edx,1),%esi
f010349a:	8b 4b 60             	mov    0x60(%ebx),%ecx
f010349d:	89 34 11             	mov    %esi,(%ecx,%edx,1)
f01034a0:	83 c2 04             	add    $0x4,%edx
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
f01034a3:	81 fa 00 10 00 00    	cmp    $0x1000,%edx
f01034a9:	75 e6                	jne    f0103491 <env_alloc+0x63>
		e->env_pgdir[i] = kern_pgdir[i];
	}
	p->pp_ref++;
f01034ab:	66 ff 40 04          	incw   0x4(%eax)
	// UVPT maps the env's own page table read-only.
	// Permissions: kernel R, user R
	e->env_pgdir[PDX(UVPT)] = PADDR(e->env_pgdir) | PTE_P | PTE_U;
f01034af:	8b 43 60             	mov    0x60(%ebx),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01034b2:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01034b7:	77 15                	ja     f01034ce <env_alloc+0xa0>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01034b9:	50                   	push   %eax
f01034ba:	68 8c 6f 10 f0       	push   $0xf0106f8c
f01034bf:	68 d3 00 00 00       	push   $0xd3
f01034c4:	68 f3 83 10 f0       	push   $0xf01083f3
f01034c9:	e8 7e cb ff ff       	call   f010004c <_panic>
f01034ce:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f01034d4:	83 ca 05             	or     $0x5,%edx
f01034d7:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)
	if ((r = env_setup_vm(e)) < 0){
		//cprintf("In env_alloc(), Allocate and set up the page directory for this environment");
		return r;
	}
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1);
f01034dd:	8b 43 48             	mov    0x48(%ebx),%eax
f01034e0:	8d 90 00 10 00 00    	lea    0x1000(%eax),%edx
	if (generation <= 0)	// Don't create a negative env_id.
f01034e6:	81 e2 00 fc ff ff    	and    $0xfffffc00,%edx
f01034ec:	7f 05                	jg     f01034f3 <env_alloc+0xc5>
		generation = 1 << ENVGENSHIFT;
f01034ee:	ba 00 10 00 00       	mov    $0x1000,%edx
	e->env_id = generation | (e - envs);
f01034f3:	89 d8                	mov    %ebx,%eax
f01034f5:	2b 05 44 22 27 f0    	sub    0xf0272244,%eax
f01034fb:	c1 f8 02             	sar    $0x2,%eax
f01034fe:	89 c6                	mov    %eax,%esi
f0103500:	c1 e6 05             	shl    $0x5,%esi
f0103503:	89 c1                	mov    %eax,%ecx
f0103505:	c1 e1 0a             	shl    $0xa,%ecx
f0103508:	01 f1                	add    %esi,%ecx
f010350a:	01 c1                	add    %eax,%ecx
f010350c:	89 ce                	mov    %ecx,%esi
f010350e:	c1 e6 0f             	shl    $0xf,%esi
f0103511:	01 f1                	add    %esi,%ecx
f0103513:	c1 e1 05             	shl    $0x5,%ecx
f0103516:	01 c8                	add    %ecx,%eax
f0103518:	f7 d8                	neg    %eax
f010351a:	09 d0                	or     %edx,%eax
f010351c:	89 43 48             	mov    %eax,0x48(%ebx)
	//cprintf("New env_id is: %d\n", (generation | (e - envs)) );
	// Set the basic status variables.
	e->env_parent_id = parent_id;
f010351f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103522:	89 43 4c             	mov    %eax,0x4c(%ebx)
	e->env_type = ENV_TYPE_USER;
f0103525:	c7 43 50 00 00 00 00 	movl   $0x0,0x50(%ebx)
	e->env_status = ENV_RUNNABLE;
f010352c:	c7 43 54 02 00 00 00 	movl   $0x2,0x54(%ebx)
	e->env_runs = 0;
f0103533:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf));
f010353a:	83 ec 04             	sub    $0x4,%esp
f010353d:	6a 44                	push   $0x44
f010353f:	6a 00                	push   $0x0
f0103541:	53                   	push   %ebx
f0103542:	e8 bd 2a 00 00       	call   f0106004 <memset>
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.  When
	// we switch privilege levels, the hardware does various
	// checks involving the RPL and the Descriptor Privilege Level
	// (DPL) stored in the descriptors themselves.
	e->env_tf.tf_ds = GD_UD | 3;
f0103547:	66 c7 43 24 23 00    	movw   $0x23,0x24(%ebx)
	e->env_tf.tf_es = GD_UD | 3;
f010354d:	66 c7 43 20 23 00    	movw   $0x23,0x20(%ebx)
	e->env_tf.tf_ss = GD_UD | 3;
f0103553:	66 c7 43 40 23 00    	movw   $0x23,0x40(%ebx)
	e->env_tf.tf_esp = USTACKTOP;
f0103559:	c7 43 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%ebx)
	e->env_tf.tf_cs = GD_UT | 3;
f0103560:	66 c7 43 34 1b 00    	movw   $0x1b,0x34(%ebx)
	// You will set e->env_tf.tf_eip later.

	// Enable interrupts while in user mode.
	// LAB 4: Your code here.
	e->env_tf.tf_eflags |= FL_IF; // Enable interrupts
f0103566:	81 4b 38 00 02 00 00 	orl    $0x200,0x38(%ebx)

	// Clear the page fault handler until user installs one.
	e->env_pgfault_upcall = 0;
f010356d:	c7 43 64 00 00 00 00 	movl   $0x0,0x64(%ebx)

	// Also clear the IPC receiving flag.
	e->env_ipc_recving = 0;
f0103574:	c6 43 68 00          	movb   $0x0,0x68(%ebx)

	// commit the allocation
	env_free_list = e->env_link;
f0103578:	8b 43 44             	mov    0x44(%ebx),%eax
f010357b:	a3 48 22 27 f0       	mov    %eax,0xf0272248
	*newenv_store = e;
f0103580:	8b 45 08             	mov    0x8(%ebp),%eax
f0103583:	89 18                	mov    %ebx,(%eax)
	//cprintf("In env_alloc(), current env is: %x, parent env is: %x\n", curenv ? curenv->env_id: 0, parent_id);
	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f0103585:	8b 5b 48             	mov    0x48(%ebx),%ebx
f0103588:	e8 7d 31 00 00       	call   f010670a <cpunum>
f010358d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103590:	01 c2                	add    %eax,%edx
f0103592:	01 d2                	add    %edx,%edx
f0103594:	01 c2                	add    %eax,%edx
f0103596:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103599:	83 c4 10             	add    $0x10,%esp
f010359c:	83 3c 85 08 40 27 f0 	cmpl   $0x0,-0xfd8bff8(,%eax,4)
f01035a3:	00 
f01035a4:	74 1d                	je     f01035c3 <env_alloc+0x195>
f01035a6:	e8 5f 31 00 00       	call   f010670a <cpunum>
f01035ab:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01035ae:	01 c2                	add    %eax,%edx
f01035b0:	01 d2                	add    %edx,%edx
f01035b2:	01 c2                	add    %eax,%edx
f01035b4:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01035b7:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f01035be:	8b 40 48             	mov    0x48(%eax),%eax
f01035c1:	eb 05                	jmp    f01035c8 <env_alloc+0x19a>
f01035c3:	b8 00 00 00 00       	mov    $0x0,%eax
f01035c8:	83 ec 04             	sub    $0x4,%esp
f01035cb:	53                   	push   %ebx
f01035cc:	50                   	push   %eax
f01035cd:	68 fe 83 10 f0       	push   $0xf01083fe
f01035d2:	e8 21 06 00 00       	call   f0103bf8 <cprintf>
	return 0;
f01035d7:	83 c4 10             	add    $0x10,%esp
f01035da:	b8 00 00 00 00       	mov    $0x0,%eax
f01035df:	eb 0c                	jmp    f01035ed <env_alloc+0x1bf>
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
		return -E_NO_FREE_ENV;
f01035e1:	b8 fb ff ff ff       	mov    $0xfffffffb,%eax
f01035e6:	eb 05                	jmp    f01035ed <env_alloc+0x1bf>
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
		return -E_NO_MEM;
f01035e8:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	env_free_list = e->env_link;
	*newenv_store = e;
	//cprintf("In env_alloc(), current env is: %x, parent env is: %x\n", curenv ? curenv->env_id: 0, parent_id);
	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
	return 0;
}
f01035ed:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01035f0:	5b                   	pop    %ebx
f01035f1:	5e                   	pop    %esi
f01035f2:	5d                   	pop    %ebp
f01035f3:	c3                   	ret    

f01035f4 <env_create>:
// before running the first user-mode environment.
// The new env's parent ID is set to 0.
//
void
env_create(uint8_t *binary, enum EnvType type)
{
f01035f4:	55                   	push   %ebp
f01035f5:	89 e5                	mov    %esp,%ebp
f01035f7:	57                   	push   %edi
f01035f8:	56                   	push   %esi
f01035f9:	53                   	push   %ebx
f01035fa:	83 ec 2c             	sub    $0x2c,%esp
	struct Env *newenv;
	int env_alloc_Result;
	//cprintf("In env_create(), about to call env_alloc()\n");
	//env_alloc_Result = env_alloc(&newenv, 0); //new env's parent ID is set to 0.
	//curenv ? curenv->env_id: 0
	env_alloc_Result = env_alloc(&newenv, curenv ? curenv->env_id:0); //new env's parent ID is set to 0.
f01035fd:	e8 08 31 00 00       	call   f010670a <cpunum>
f0103602:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103605:	01 c2                	add    %eax,%edx
f0103607:	01 d2                	add    %edx,%edx
f0103609:	01 c2                	add    %eax,%edx
f010360b:	8d 04 90             	lea    (%eax,%edx,4),%eax
f010360e:	83 3c 85 08 40 27 f0 	cmpl   $0x0,-0xfd8bff8(,%eax,4)
f0103615:	00 
f0103616:	74 1d                	je     f0103635 <env_create+0x41>
f0103618:	e8 ed 30 00 00       	call   f010670a <cpunum>
f010361d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103620:	01 c2                	add    %eax,%edx
f0103622:	01 d2                	add    %edx,%edx
f0103624:	01 c2                	add    %eax,%edx
f0103626:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103629:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0103630:	8b 40 48             	mov    0x48(%eax),%eax
f0103633:	eb 05                	jmp    f010363a <env_create+0x46>
f0103635:	b8 00 00 00 00       	mov    $0x0,%eax
f010363a:	83 ec 08             	sub    $0x8,%esp
f010363d:	50                   	push   %eax
f010363e:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0103641:	50                   	push   %eax
f0103642:	e8 e7 fd ff ff       	call   f010342e <env_alloc>
	if(env_alloc_Result == -E_NO_FREE_ENV){
f0103647:	83 c4 10             	add    $0x10,%esp
f010364a:	83 f8 fb             	cmp    $0xfffffffb,%eax
f010364d:	75 16                	jne    f0103665 <env_create+0x71>
		panic("env_create: %e", env_alloc_Result);
f010364f:	6a fb                	push   $0xfffffffb
f0103651:	68 13 84 10 f0       	push   $0xf0108413
f0103656:	68 b1 01 00 00       	push   $0x1b1
f010365b:	68 f3 83 10 f0       	push   $0xf01083f3
f0103660:	e8 e7 c9 ff ff       	call   f010004c <_panic>
	}
	if(env_alloc_Result == -E_NO_MEM){
f0103665:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0103668:	75 16                	jne    f0103680 <env_create+0x8c>
		panic("env_create: %e", env_alloc_Result);
f010366a:	6a fc                	push   $0xfffffffc
f010366c:	68 13 84 10 f0       	push   $0xf0108413
f0103671:	68 b4 01 00 00       	push   $0x1b4
f0103676:	68 f3 83 10 f0       	push   $0xf01083f3
f010367b:	e8 cc c9 ff ff       	call   f010004c <_panic>
	}
	newenv->env_type = type;
f0103680:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0103683:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103686:	89 47 50             	mov    %eax,0x50(%edi)

static inline uint32_t
rcr3(void)
{
	uint32_t val;
	asm volatile("movl %%cr3,%0" : "=r" (val));
f0103689:	0f 20 d8             	mov    %cr3,%eax
f010368c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
	// LAB 3: Your code here.
	//cprintf("load_icode starts:\n");
	physaddr_t  old_cr3 = rcr3(); 

	struct Proghdr *ph, *eph;
	if(((struct Elf *)binary)->e_magic != ELF_MAGIC) {
f010368f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103692:	81 38 7f 45 4c 46    	cmpl   $0x464c457f,(%eax)
f0103698:	74 17                	je     f01036b1 <env_create+0xbd>
		panic("Invalid binary in load_icode.\n");
f010369a:	83 ec 04             	sub    $0x4,%esp
f010369d:	68 d4 83 10 f0       	push   $0xf01083d4
f01036a2:	68 86 01 00 00       	push   $0x186
f01036a7:	68 f3 83 10 f0       	push   $0xf01083f3
f01036ac:	e8 9b c9 ff ff       	call   f010004c <_panic>
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
f01036b1:	8b 45 08             	mov    0x8(%ebp),%eax
f01036b4:	89 c3                	mov    %eax,%ebx
f01036b6:	03 58 1c             	add    0x1c(%eax),%ebx
	eph = ph + ((struct Elf *)binary)->e_phnum;
f01036b9:	0f b7 70 2c          	movzwl 0x2c(%eax),%esi
f01036bd:	c1 e6 05             	shl    $0x5,%esi
f01036c0:	01 de                	add    %ebx,%esi
	lcr3( PADDR(e->env_pgdir) );
f01036c2:	8b 47 60             	mov    0x60(%edi),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01036c5:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01036ca:	77 15                	ja     f01036e1 <env_create+0xed>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01036cc:	50                   	push   %eax
f01036cd:	68 8c 6f 10 f0       	push   $0xf0106f8c
f01036d2:	68 8a 01 00 00       	push   $0x18a
f01036d7:	68 f3 83 10 f0       	push   $0xf01083f3
f01036dc:	e8 6b c9 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f01036e1:	05 00 00 00 10       	add    $0x10000000,%eax
f01036e6:	0f 22 d8             	mov    %eax,%cr3
f01036e9:	eb 44                	jmp    f010372f <env_create+0x13b>

	for (; ph < eph; ph++){
		if(ph->p_type == ELF_PROG_LOAD){
f01036eb:	83 3b 01             	cmpl   $0x1,(%ebx)
f01036ee:	75 3c                	jne    f010372c <env_create+0x138>
			region_alloc(e, (void *)ph->p_va, ph->p_memsz);
f01036f0:	8b 4b 14             	mov    0x14(%ebx),%ecx
f01036f3:	8b 53 08             	mov    0x8(%ebx),%edx
f01036f6:	89 f8                	mov    %edi,%eax
f01036f8:	e8 58 fb ff ff       	call   f0103255 <region_alloc>
			memmove((void *)ph->p_va,(binary  + ph->p_offset), ph->p_filesz);
f01036fd:	83 ec 04             	sub    $0x4,%esp
f0103700:	ff 73 10             	pushl  0x10(%ebx)
f0103703:	8b 45 08             	mov    0x8(%ebp),%eax
f0103706:	03 43 04             	add    0x4(%ebx),%eax
f0103709:	50                   	push   %eax
f010370a:	ff 73 08             	pushl  0x8(%ebx)
f010370d:	e8 3f 29 00 00       	call   f0106051 <memmove>
			memset((void *)ph->p_va+ph->p_filesz, 0, ph->p_memsz-ph->p_filesz);
f0103712:	8b 43 10             	mov    0x10(%ebx),%eax
f0103715:	83 c4 0c             	add    $0xc,%esp
f0103718:	8b 53 14             	mov    0x14(%ebx),%edx
f010371b:	29 c2                	sub    %eax,%edx
f010371d:	52                   	push   %edx
f010371e:	6a 00                	push   $0x0
f0103720:	03 43 08             	add    0x8(%ebx),%eax
f0103723:	50                   	push   %eax
f0103724:	e8 db 28 00 00       	call   f0106004 <memset>
f0103729:	83 c4 10             	add    $0x10,%esp
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
	eph = ph + ((struct Elf *)binary)->e_phnum;
	lcr3( PADDR(e->env_pgdir) );

	for (; ph < eph; ph++){
f010372c:	83 c3 20             	add    $0x20,%ebx
f010372f:	39 de                	cmp    %ebx,%esi
f0103731:	77 b8                	ja     f01036eb <env_create+0xf7>
f0103733:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103736:	0f 22 d8             	mov    %eax,%cr3
	}

	lcr3(old_cr3);
	// Now map one page for the program's initial stack
	// at virtual address USTACKTOP - PGSIZE.
	(e->env_tf).tf_eip = ((struct Elf *)binary)->e_entry;
f0103739:	8b 45 08             	mov    0x8(%ebp),%eax
f010373c:	8b 40 18             	mov    0x18(%eax),%eax
f010373f:	89 47 30             	mov    %eax,0x30(%edi)
	region_alloc(e, (void*)(USTACKTOP - PGSIZE), PGSIZE);
f0103742:	b9 00 10 00 00       	mov    $0x1000,%ecx
f0103747:	ba 00 d0 bf ee       	mov    $0xeebfd000,%edx
f010374c:	89 f8                	mov    %edi,%eax
f010374e:	e8 02 fb ff ff       	call   f0103255 <region_alloc>
		panic("env_create: %e", env_alloc_Result);
	}
	newenv->env_type = type;
	//cprintf("Before load_icode.\n\n");
	load_icode(newenv, binary);
}
f0103753:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103756:	5b                   	pop    %ebx
f0103757:	5e                   	pop    %esi
f0103758:	5f                   	pop    %edi
f0103759:	5d                   	pop    %ebp
f010375a:	c3                   	ret    

f010375b <env_free>:
//
// Frees env e and all memory it uses.
//
void
env_free(struct Env *e)
{
f010375b:	55                   	push   %ebp
f010375c:	89 e5                	mov    %esp,%ebp
f010375e:	57                   	push   %edi
f010375f:	56                   	push   %esi
f0103760:	53                   	push   %ebx
f0103761:	83 ec 1c             	sub    $0x1c,%esp
f0103764:	8b 7d 08             	mov    0x8(%ebp),%edi
	physaddr_t pa;

	// If freeing the current environment, switch to kern_pgdir
	// before freeing the page directory, just in case the page
	// gets reused.
	if (e == curenv)
f0103767:	e8 9e 2f 00 00       	call   f010670a <cpunum>
f010376c:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010376f:	01 c2                	add    %eax,%edx
f0103771:	01 d2                	add    %edx,%edx
f0103773:	01 c2                	add    %eax,%edx
f0103775:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103778:	39 3c 85 08 40 27 f0 	cmp    %edi,-0xfd8bff8(,%eax,4)
f010377f:	75 29                	jne    f01037aa <env_free+0x4f>
		lcr3(PADDR(kern_pgdir));
f0103781:	a1 28 34 27 f0       	mov    0xf0273428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103786:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f010378b:	77 15                	ja     f01037a2 <env_free+0x47>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010378d:	50                   	push   %eax
f010378e:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0103793:	68 c9 01 00 00       	push   $0x1c9
f0103798:	68 f3 83 10 f0       	push   $0xf01083f3
f010379d:	e8 aa c8 ff ff       	call   f010004c <_panic>
f01037a2:	05 00 00 00 10       	add    $0x10000000,%eax
f01037a7:	0f 22 d8             	mov    %eax,%cr3

	// Note the environment's demise.
	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f01037aa:	8b 5f 48             	mov    0x48(%edi),%ebx
f01037ad:	e8 58 2f 00 00       	call   f010670a <cpunum>
f01037b2:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01037b5:	01 c2                	add    %eax,%edx
f01037b7:	01 d2                	add    %edx,%edx
f01037b9:	01 c2                	add    %eax,%edx
f01037bb:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01037be:	83 3c 85 08 40 27 f0 	cmpl   $0x0,-0xfd8bff8(,%eax,4)
f01037c5:	00 
f01037c6:	74 1d                	je     f01037e5 <env_free+0x8a>
f01037c8:	e8 3d 2f 00 00       	call   f010670a <cpunum>
f01037cd:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01037d0:	01 c2                	add    %eax,%edx
f01037d2:	01 d2                	add    %edx,%edx
f01037d4:	01 c2                	add    %eax,%edx
f01037d6:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01037d9:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f01037e0:	8b 40 48             	mov    0x48(%eax),%eax
f01037e3:	eb 05                	jmp    f01037ea <env_free+0x8f>
f01037e5:	b8 00 00 00 00       	mov    $0x0,%eax
f01037ea:	83 ec 04             	sub    $0x4,%esp
f01037ed:	53                   	push   %ebx
f01037ee:	50                   	push   %eax
f01037ef:	68 22 84 10 f0       	push   $0xf0108422
f01037f4:	e8 ff 03 00 00       	call   f0103bf8 <cprintf>
f01037f9:	83 c4 10             	add    $0x10,%esp
f01037fc:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f0103803:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)

		// only look at mapped page tables
		if (!(e->env_pgdir[pdeno] & PTE_P))
f010380a:	8b 47 60             	mov    0x60(%edi),%eax
f010380d:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0103810:	8b 34 10             	mov    (%eax,%edx,1),%esi
f0103813:	f7 c6 01 00 00 00    	test   $0x1,%esi
f0103819:	0f 84 a6 00 00 00    	je     f01038c5 <env_free+0x16a>
			continue;

		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
f010381f:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103825:	89 f0                	mov    %esi,%eax
f0103827:	c1 e8 0c             	shr    $0xc,%eax
f010382a:	89 45 d8             	mov    %eax,-0x28(%ebp)
f010382d:	39 05 24 34 27 f0    	cmp    %eax,0xf0273424
f0103833:	77 15                	ja     f010384a <env_free+0xef>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0103835:	56                   	push   %esi
f0103836:	68 68 6f 10 f0       	push   $0xf0106f68
f010383b:	68 d8 01 00 00       	push   $0x1d8
f0103840:	68 f3 83 10 f0       	push   $0xf01083f3
f0103845:	e8 02 c8 ff ff       	call   f010004c <_panic>
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f010384a:	8b 45 dc             	mov    -0x24(%ebp),%eax
f010384d:	c1 e0 16             	shl    $0x16,%eax
f0103850:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f0103853:	bb 00 00 00 00       	mov    $0x0,%ebx
			if (pt[pteno] & PTE_P)
f0103858:	f6 84 9e 00 00 00 f0 	testb  $0x1,-0x10000000(%esi,%ebx,4)
f010385f:	01 
f0103860:	74 17                	je     f0103879 <env_free+0x11e>
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f0103862:	83 ec 08             	sub    $0x8,%esp
f0103865:	89 d8                	mov    %ebx,%eax
f0103867:	c1 e0 0c             	shl    $0xc,%eax
f010386a:	0b 45 e4             	or     -0x1c(%ebp),%eax
f010386d:	50                   	push   %eax
f010386e:	ff 77 60             	pushl  0x60(%edi)
f0103871:	e8 6f dd ff ff       	call   f01015e5 <page_remove>
f0103876:	83 c4 10             	add    $0x10,%esp
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f0103879:	43                   	inc    %ebx
f010387a:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
f0103880:	75 d6                	jne    f0103858 <env_free+0xfd>
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
		}

		// free the page table itself
		e->env_pgdir[pdeno] = 0;
f0103882:	8b 47 60             	mov    0x60(%edi),%eax
f0103885:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0103888:	c7 04 10 00 00 00 00 	movl   $0x0,(%eax,%edx,1)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010388f:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103892:	3b 05 24 34 27 f0    	cmp    0xf0273424,%eax
f0103898:	72 14                	jb     f01038ae <env_free+0x153>
		panic("pa2page called with invalid pa");
f010389a:	83 ec 04             	sub    $0x4,%esp
f010389d:	68 f4 76 10 f0       	push   $0xf01076f4
f01038a2:	6a 51                	push   $0x51
f01038a4:	68 6d 7f 10 f0       	push   $0xf0107f6d
f01038a9:	e8 9e c7 ff ff       	call   f010004c <_panic>
		page_decref(pa2page(pa));
f01038ae:	83 ec 0c             	sub    $0xc,%esp
f01038b1:	a1 2c 34 27 f0       	mov    0xf027342c,%eax
f01038b6:	8b 55 d8             	mov    -0x28(%ebp),%edx
f01038b9:	8d 04 d0             	lea    (%eax,%edx,8),%eax
f01038bc:	50                   	push   %eax
f01038bd:	e8 1a da ff ff       	call   f01012dc <page_decref>
f01038c2:	83 c4 10             	add    $0x10,%esp
	// Note the environment's demise.
	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f01038c5:	ff 45 dc             	incl   -0x24(%ebp)
f01038c8:	83 45 e0 04          	addl   $0x4,-0x20(%ebp)
f01038cc:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01038cf:	3d ec 0e 00 00       	cmp    $0xeec,%eax
f01038d4:	0f 85 30 ff ff ff    	jne    f010380a <env_free+0xaf>
		e->env_pgdir[pdeno] = 0;
		page_decref(pa2page(pa));
	}

	// free the page directory
	pa = PADDR(e->env_pgdir);
f01038da:	8b 47 60             	mov    0x60(%edi),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01038dd:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01038e2:	77 15                	ja     f01038f9 <env_free+0x19e>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01038e4:	50                   	push   %eax
f01038e5:	68 8c 6f 10 f0       	push   $0xf0106f8c
f01038ea:	68 e6 01 00 00       	push   $0x1e6
f01038ef:	68 f3 83 10 f0       	push   $0xf01083f3
f01038f4:	e8 53 c7 ff ff       	call   f010004c <_panic>
	e->env_pgdir = 0;
f01038f9:	c7 47 60 00 00 00 00 	movl   $0x0,0x60(%edi)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103900:	05 00 00 00 10       	add    $0x10000000,%eax
f0103905:	c1 e8 0c             	shr    $0xc,%eax
f0103908:	3b 05 24 34 27 f0    	cmp    0xf0273424,%eax
f010390e:	72 14                	jb     f0103924 <env_free+0x1c9>
		panic("pa2page called with invalid pa");
f0103910:	83 ec 04             	sub    $0x4,%esp
f0103913:	68 f4 76 10 f0       	push   $0xf01076f4
f0103918:	6a 51                	push   $0x51
f010391a:	68 6d 7f 10 f0       	push   $0xf0107f6d
f010391f:	e8 28 c7 ff ff       	call   f010004c <_panic>
	page_decref(pa2page(pa));
f0103924:	83 ec 0c             	sub    $0xc,%esp
f0103927:	8b 15 2c 34 27 f0    	mov    0xf027342c,%edx
f010392d:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f0103930:	50                   	push   %eax
f0103931:	e8 a6 d9 ff ff       	call   f01012dc <page_decref>

	// return the environment to the free list
	e->env_status = ENV_FREE;
f0103936:	c7 47 54 00 00 00 00 	movl   $0x0,0x54(%edi)
	e->env_link = env_free_list;
f010393d:	a1 48 22 27 f0       	mov    0xf0272248,%eax
f0103942:	89 47 44             	mov    %eax,0x44(%edi)
	env_free_list = e;
f0103945:	89 3d 48 22 27 f0    	mov    %edi,0xf0272248
	//cprintf("End of env_free()\n");
}
f010394b:	83 c4 10             	add    $0x10,%esp
f010394e:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103951:	5b                   	pop    %ebx
f0103952:	5e                   	pop    %esi
f0103953:	5f                   	pop    %edi
f0103954:	5d                   	pop    %ebp
f0103955:	c3                   	ret    

f0103956 <env_destroy>:
// If e was the current env, then runs a new environment (and does not return
// to the caller).
//
void
env_destroy(struct Env *e)
{
f0103956:	55                   	push   %ebp
f0103957:	89 e5                	mov    %esp,%ebp
f0103959:	53                   	push   %ebx
f010395a:	83 ec 04             	sub    $0x4,%esp
f010395d:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// If e is currently running on other CPUs, we change its state to
	// ENV_DYING. A zombie environment will be freed the next time
	// it traps to the kernel.
	if (e->env_status == ENV_RUNNING && curenv != e) {
f0103960:	83 7b 54 03          	cmpl   $0x3,0x54(%ebx)
f0103964:	75 23                	jne    f0103989 <env_destroy+0x33>
f0103966:	e8 9f 2d 00 00       	call   f010670a <cpunum>
f010396b:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010396e:	01 c2                	add    %eax,%edx
f0103970:	01 d2                	add    %edx,%edx
f0103972:	01 c2                	add    %eax,%edx
f0103974:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103977:	3b 1c 85 08 40 27 f0 	cmp    -0xfd8bff8(,%eax,4),%ebx
f010397e:	74 09                	je     f0103989 <env_destroy+0x33>
		e->env_status = ENV_DYING;
f0103980:	c7 43 54 01 00 00 00 	movl   $0x1,0x54(%ebx)
		return;
f0103987:	eb 5a                	jmp    f01039e3 <env_destroy+0x8d>
	}

	env_free(e);
f0103989:	83 ec 0c             	sub    $0xc,%esp
f010398c:	53                   	push   %ebx
f010398d:	e8 c9 fd ff ff       	call   f010375b <env_free>

	if (curenv == e) {
f0103992:	e8 73 2d 00 00       	call   f010670a <cpunum>
f0103997:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010399a:	01 c2                	add    %eax,%edx
f010399c:	01 d2                	add    %edx,%edx
f010399e:	01 c2                	add    %eax,%edx
f01039a0:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01039a3:	83 c4 10             	add    $0x10,%esp
f01039a6:	3b 1c 85 08 40 27 f0 	cmp    -0xfd8bff8(,%eax,4),%ebx
f01039ad:	75 24                	jne    f01039d3 <env_destroy+0x7d>
		cprintf("Destroy courrent env\n");
f01039af:	83 ec 0c             	sub    $0xc,%esp
f01039b2:	68 38 84 10 f0       	push   $0xf0108438
f01039b7:	e8 3c 02 00 00       	call   f0103bf8 <cprintf>
		curenv = NULL;
f01039bc:	e8 49 2d 00 00       	call   f010670a <cpunum>
f01039c1:	6b c0 74             	imul   $0x74,%eax,%eax
f01039c4:	c7 80 08 40 27 f0 00 	movl   $0x0,-0xfd8bff8(%eax)
f01039cb:	00 00 00 
		sched_yield();
f01039ce:	e8 18 11 00 00       	call   f0104aeb <sched_yield>
	}
	cprintf("end of env_destroy()\n");
f01039d3:	83 ec 0c             	sub    $0xc,%esp
f01039d6:	68 4e 84 10 f0       	push   $0xf010844e
f01039db:	e8 18 02 00 00       	call   f0103bf8 <cprintf>
f01039e0:	83 c4 10             	add    $0x10,%esp
}
f01039e3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01039e6:	c9                   	leave  
f01039e7:	c3                   	ret    

f01039e8 <env_pop_tf>:
//
// This function does not return.
//
void
env_pop_tf(struct Trapframe *tf)
{
f01039e8:	55                   	push   %ebp
f01039e9:	89 e5                	mov    %esp,%ebp
f01039eb:	53                   	push   %ebx
f01039ec:	83 ec 04             	sub    $0x4,%esp
	// Record the CPU we are running on for user-space debugging
	curenv->env_cpunum = cpunum();
f01039ef:	e8 16 2d 00 00       	call   f010670a <cpunum>
f01039f4:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01039f7:	01 c2                	add    %eax,%edx
f01039f9:	01 d2                	add    %edx,%edx
f01039fb:	01 c2                	add    %eax,%edx
f01039fd:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103a00:	8b 1c 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%ebx
f0103a07:	e8 fe 2c 00 00       	call   f010670a <cpunum>
f0103a0c:	89 43 5c             	mov    %eax,0x5c(%ebx)

	asm volatile(
f0103a0f:	8b 65 08             	mov    0x8(%ebp),%esp
f0103a12:	61                   	popa   
f0103a13:	07                   	pop    %es
f0103a14:	1f                   	pop    %ds
f0103a15:	83 c4 08             	add    $0x8,%esp
f0103a18:	cf                   	iret   
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret\n"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
f0103a19:	83 ec 04             	sub    $0x4,%esp
f0103a1c:	68 64 84 10 f0       	push   $0xf0108464
f0103a21:	68 20 02 00 00       	push   $0x220
f0103a26:	68 f3 83 10 f0       	push   $0xf01083f3
f0103a2b:	e8 1c c6 ff ff       	call   f010004c <_panic>

f0103a30 <env_run>:
//
// This function does not return.
//
void
env_run(struct Env *e)
{
f0103a30:	55                   	push   %ebp
f0103a31:	89 e5                	mov    %esp,%ebp
f0103a33:	53                   	push   %ebx
f0103a34:	83 ec 04             	sub    $0x4,%esp
f0103a37:	8b 5d 08             	mov    0x8(%ebp),%ebx
	//if(curenv == NULL && (curenv->env_id == e->env_id) ){
	//	goto end;
	//}
	//if(curenv != NULL && (curenv->env_id != e->env_id)){ //check if it's a context switch
	
	if(curenv != NULL && (curenv->env_id != e->env_id)){
f0103a3a:	e8 cb 2c 00 00       	call   f010670a <cpunum>
f0103a3f:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103a42:	01 c2                	add    %eax,%edx
f0103a44:	01 d2                	add    %edx,%edx
f0103a46:	01 c2                	add    %eax,%edx
f0103a48:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103a4b:	83 3c 85 08 40 27 f0 	cmpl   $0x0,-0xfd8bff8(,%eax,4)
f0103a52:	00 
f0103a53:	74 59                	je     f0103aae <env_run+0x7e>
f0103a55:	e8 b0 2c 00 00       	call   f010670a <cpunum>
f0103a5a:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103a5d:	01 c2                	add    %eax,%edx
f0103a5f:	01 d2                	add    %edx,%edx
f0103a61:	01 c2                	add    %eax,%edx
f0103a63:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103a66:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0103a6d:	8b 4b 48             	mov    0x48(%ebx),%ecx
f0103a70:	39 48 48             	cmp    %ecx,0x48(%eax)
f0103a73:	74 39                	je     f0103aae <env_run+0x7e>
		if(curenv->env_ipc_recving != 1){
f0103a75:	e8 90 2c 00 00       	call   f010670a <cpunum>
f0103a7a:	6b c0 74             	imul   $0x74,%eax,%eax
f0103a7d:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f0103a83:	80 78 68 00          	cmpb   $0x0,0x68(%eax)
f0103a87:	75 15                	jne    f0103a9e <env_run+0x6e>
			curenv->env_status = ENV_RUNNABLE;
f0103a89:	e8 7c 2c 00 00       	call   f010670a <cpunum>
f0103a8e:	6b c0 74             	imul   $0x74,%eax,%eax
f0103a91:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f0103a97:	c7 40 54 02 00 00 00 	movl   $0x2,0x54(%eax)
		}
		curenv = e;
f0103a9e:	e8 67 2c 00 00       	call   f010670a <cpunum>
f0103aa3:	6b c0 74             	imul   $0x74,%eax,%eax
f0103aa6:	89 98 08 40 27 f0    	mov    %ebx,-0xfd8bff8(%eax)
f0103aac:	eb 0e                	jmp    f0103abc <env_run+0x8c>
	}else{
		curenv = e;
f0103aae:	e8 57 2c 00 00       	call   f010670a <cpunum>
f0103ab3:	6b c0 74             	imul   $0x74,%eax,%eax
f0103ab6:	89 98 08 40 27 f0    	mov    %ebx,-0xfd8bff8(%eax)
	}
		//cprintf("In env_run()\n");
		curenv->env_status = ENV_RUNNING;
f0103abc:	e8 49 2c 00 00       	call   f010670a <cpunum>
f0103ac1:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103ac4:	01 c2                	add    %eax,%edx
f0103ac6:	01 d2                	add    %edx,%edx
f0103ac8:	01 c2                	add    %eax,%edx
f0103aca:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103acd:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0103ad4:	c7 40 54 03 00 00 00 	movl   $0x3,0x54(%eax)
		(curenv->env_runs)++;
f0103adb:	e8 2a 2c 00 00       	call   f010670a <cpunum>
f0103ae0:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103ae3:	01 c2                	add    %eax,%edx
f0103ae5:	01 d2                	add    %edx,%edx
f0103ae7:	01 c2                	add    %eax,%edx
f0103ae9:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103aec:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0103af3:	ff 40 58             	incl   0x58(%eax)
		lcr3(PADDR(curenv->env_pgdir));
f0103af6:	e8 0f 2c 00 00       	call   f010670a <cpunum>
f0103afb:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103afe:	01 c2                	add    %eax,%edx
f0103b00:	01 d2                	add    %edx,%edx
f0103b02:	01 c2                	add    %eax,%edx
f0103b04:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103b07:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0103b0e:	8b 40 60             	mov    0x60(%eax),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103b11:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0103b16:	77 15                	ja     f0103b2d <env_run+0xfd>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103b18:	50                   	push   %eax
f0103b19:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0103b1e:	68 4e 02 00 00       	push   $0x24e
f0103b23:	68 f3 83 10 f0       	push   $0xf01083f3
f0103b28:	e8 1f c5 ff ff       	call   f010004c <_panic>
f0103b2d:	05 00 00 00 10       	add    $0x10000000,%eax
f0103b32:	0f 22 d8             	mov    %eax,%cr3
}

static inline void
unlock_kernel(void)
{
	spin_unlock(&kernel_lock);
f0103b35:	83 ec 0c             	sub    $0xc,%esp
f0103b38:	68 c0 33 12 f0       	push   $0xf01233c0
f0103b3d:	e8 33 30 00 00       	call   f0106b75 <spin_unlock>

	// Normally we wouldn't need to do this, but QEMU only runs
	// one CPU at a time and has a long time-slice.  Without the
	// pause, this CPU is likely to reacquire the lock before
	// another CPU has even been given a chance to acquire it.
	asm volatile("pause");
f0103b42:	f3 90                	pause  

		unlock_kernel();
		env_pop_tf(&(curenv->env_tf));
f0103b44:	e8 c1 2b 00 00       	call   f010670a <cpunum>
f0103b49:	83 c4 04             	add    $0x4,%esp
f0103b4c:	6b c0 74             	imul   $0x74,%eax,%eax
f0103b4f:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f0103b55:	e8 8e fe ff ff       	call   f01039e8 <env_pop_tf>

f0103b5a <pic_init>:
}

/* Initialize the 8259A interrupt controllers. */
void
pic_init(void)
{
f0103b5a:	55                   	push   %ebp
f0103b5b:	89 e5                	mov    %esp,%ebp
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0103b5d:	ba 21 00 00 00       	mov    $0x21,%edx
f0103b62:	b0 ff                	mov    $0xff,%al
f0103b64:	ee                   	out    %al,(%dx)
f0103b65:	ba a1 00 00 00       	mov    $0xa1,%edx
f0103b6a:	ee                   	out    %al,(%dx)
f0103b6b:	ba 20 00 00 00       	mov    $0x20,%edx
f0103b70:	b0 11                	mov    $0x11,%al
f0103b72:	ee                   	out    %al,(%dx)
f0103b73:	ba 21 00 00 00       	mov    $0x21,%edx
f0103b78:	b0 20                	mov    $0x20,%al
f0103b7a:	ee                   	out    %al,(%dx)
f0103b7b:	b0 04                	mov    $0x4,%al
f0103b7d:	ee                   	out    %al,(%dx)
f0103b7e:	b0 03                	mov    $0x3,%al
f0103b80:	ee                   	out    %al,(%dx)
f0103b81:	ba a0 00 00 00       	mov    $0xa0,%edx
f0103b86:	b0 11                	mov    $0x11,%al
f0103b88:	ee                   	out    %al,(%dx)
f0103b89:	ba a1 00 00 00       	mov    $0xa1,%edx
f0103b8e:	b0 28                	mov    $0x28,%al
f0103b90:	ee                   	out    %al,(%dx)
f0103b91:	b0 02                	mov    $0x2,%al
f0103b93:	ee                   	out    %al,(%dx)
f0103b94:	b0 01                	mov    $0x1,%al
f0103b96:	ee                   	out    %al,(%dx)
f0103b97:	ba 20 00 00 00       	mov    $0x20,%edx
f0103b9c:	b0 68                	mov    $0x68,%al
f0103b9e:	ee                   	out    %al,(%dx)
f0103b9f:	b0 0a                	mov    $0xa,%al
f0103ba1:	ee                   	out    %al,(%dx)
f0103ba2:	ba a0 00 00 00       	mov    $0xa0,%edx
f0103ba7:	b0 68                	mov    $0x68,%al
f0103ba9:	ee                   	out    %al,(%dx)
f0103baa:	b0 0a                	mov    $0xa,%al
f0103bac:	ee                   	out    %al,(%dx)
f0103bad:	ba 21 00 00 00       	mov    $0x21,%edx
f0103bb2:	b0 fb                	mov    $0xfb,%al
f0103bb4:	ee                   	out    %al,(%dx)
f0103bb5:	ba a1 00 00 00       	mov    $0xa1,%edx
f0103bba:	b0 ff                	mov    $0xff,%al
f0103bbc:	ee                   	out    %al,(%dx)
	outb(IO_PIC2, 0x68);               /* OCW3 */
	outb(IO_PIC2, 0x0a);               /* OCW3 */

	// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
	irq_setmask_8259A(0xffff & ~(BIT(IRQ_SLAVE)));
}
f0103bbd:	5d                   	pop    %ebp
f0103bbe:	c3                   	ret    

f0103bbf <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0103bbf:	55                   	push   %ebp
f0103bc0:	89 e5                	mov    %esp,%ebp
f0103bc2:	83 ec 14             	sub    $0x14,%esp
	cputchar(ch);
f0103bc5:	ff 75 08             	pushl  0x8(%ebp)
f0103bc8:	e8 0e cc ff ff       	call   f01007db <cputchar>
	*cnt++;
}
f0103bcd:	83 c4 10             	add    $0x10,%esp
f0103bd0:	c9                   	leave  
f0103bd1:	c3                   	ret    

f0103bd2 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f0103bd2:	55                   	push   %ebp
f0103bd3:	89 e5                	mov    %esp,%ebp
f0103bd5:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f0103bd8:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0103bdf:	ff 75 0c             	pushl  0xc(%ebp)
f0103be2:	ff 75 08             	pushl  0x8(%ebp)
f0103be5:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0103be8:	50                   	push   %eax
f0103be9:	68 bf 3b 10 f0       	push   $0xf0103bbf
f0103bee:	e8 c9 1d 00 00       	call   f01059bc <vprintfmt>
	return cnt;
}
f0103bf3:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103bf6:	c9                   	leave  
f0103bf7:	c3                   	ret    

f0103bf8 <cprintf>:

int
cprintf(const char *fmt, ...)
{
f0103bf8:	55                   	push   %ebp
f0103bf9:	89 e5                	mov    %esp,%ebp
f0103bfb:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0103bfe:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
f0103c01:	50                   	push   %eax
f0103c02:	ff 75 08             	pushl  0x8(%ebp)
f0103c05:	e8 c8 ff ff ff       	call   f0103bd2 <vcprintf>
	va_end(ap);

	return cnt;
}
f0103c0a:	c9                   	leave  
f0103c0b:	c3                   	ret    

f0103c0c <trap_init_percpu>:
}

// Initialize and load the per-CPU TSS and IDT
void
trap_init_percpu(void)
{
f0103c0c:	55                   	push   %ebp
f0103c0d:	89 e5                	mov    %esp,%ebp
f0103c0f:	57                   	push   %edi
f0103c10:	56                   	push   %esi
f0103c11:	53                   	push   %ebx
f0103c12:	83 ec 1c             	sub    $0x1c,%esp
	lidt(&idt_pd);
	*/

	//reference code
	// LAB 4: Your code here:
    uint8_t cpu_id = cpunum();//thiscpu->cpu_id;
f0103c15:	e8 f0 2a 00 00       	call   f010670a <cpunum>
f0103c1a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0103c1d:	0f b6 5d e4          	movzbl -0x1c(%ebp),%ebx
    cprintf("Print CPU_id: %d\n", cpu_id);
f0103c21:	83 ec 08             	sub    $0x8,%esp
f0103c24:	53                   	push   %ebx
f0103c25:	68 70 84 10 f0       	push   $0xf0108470
f0103c2a:	e8 c9 ff ff ff       	call   f0103bf8 <cprintf>
    struct Taskstate *this_TS = &thiscpu->cpu_ts;
f0103c2f:	e8 d6 2a 00 00       	call   f010670a <cpunum>
f0103c34:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103c37:	01 c2                	add    %eax,%edx
f0103c39:	01 d2                	add    %edx,%edx
f0103c3b:	01 c2                	add    %eax,%edx
f0103c3d:	c1 e2 02             	shl    $0x2,%edx
f0103c40:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
f0103c43:	8d 3c 8d 00 40 27 f0 	lea    -0xfd8c000(,%ecx,4),%edi
f0103c4a:	8d 4f 0c             	lea    0xc(%edi),%ecx

    this_TS->ts_esp0 = KSTACKTOP - (KSTKSIZE + KSTKGAP) * cpu_id;
f0103c4d:	89 de                	mov    %ebx,%esi
f0103c4f:	f7 de                	neg    %esi
f0103c51:	c1 e6 10             	shl    $0x10,%esi
f0103c54:	81 ee 00 00 00 10    	sub    $0x10000000,%esi
f0103c5a:	89 77 10             	mov    %esi,0x10(%edi)
    this_TS->ts_ss0 = GD_KD;
f0103c5d:	01 d0                	add    %edx,%eax
f0103c5f:	66 c7 04 85 14 40 27 	movw   $0x10,-0xfd8bfec(,%eax,4)
f0103c66:	f0 10 00 
f0103c69:	8d 04 dd 28 00 00 00 	lea    0x28(,%ebx,8),%eax
f0103c70:	c1 e8 03             	shr    $0x3,%eax
    uint16_t gdt_offset = GD_TSS0 + (cpu_id << 3);
    uint16_t gdt_index = (GD_TSS0 + (cpu_id << 3)) >> 3;

    // selector
    //#define SEG16(type, base, lim, dpl)
    gdt[gdt_index] = SEG16(STS_T32A, (uint32_t) (this_TS), sizeof(struct Taskstate) - 1, 0);
f0103c73:	66 c7 04 c5 40 33 12 	movw   $0x67,-0xfedccc0(,%eax,8)
f0103c7a:	f0 67 00 
f0103c7d:	66 89 0c c5 42 33 12 	mov    %cx,-0xfedccbe(,%eax,8)
f0103c84:	f0 
f0103c85:	89 ca                	mov    %ecx,%edx
f0103c87:	c1 ea 10             	shr    $0x10,%edx
f0103c8a:	88 14 c5 44 33 12 f0 	mov    %dl,-0xfedccbc(,%eax,8)
f0103c91:	c6 04 c5 46 33 12 f0 	movb   $0x40,-0xfedccba(,%eax,8)
f0103c98:	40 
f0103c99:	c1 e9 18             	shr    $0x18,%ecx
f0103c9c:	88 0c c5 47 33 12 f0 	mov    %cl,-0xfedccb9(,%eax,8)
    gdt[gdt_index].sd_s = 0;
f0103ca3:	c6 04 c5 45 33 12 f0 	movb   $0x89,-0xfedccbb(,%eax,8)
f0103caa:	89 
}

static inline void
ltr(uint16_t sel)
{
	asm volatile("ltr %0" : : "r" (sel));
f0103cab:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
f0103caf:	8d 04 c5 28 00 00 00 	lea    0x28(,%eax,8),%eax
f0103cb6:	0f 00 d8             	ltr    %ax
}

static inline void
lidt(void *p)
{
	asm volatile("lidt (%0)" : : "r" (p));
f0103cb9:	b8 ac 33 12 f0       	mov    $0xf01233ac,%eax
f0103cbe:	0f 01 18             	lidtl  (%eax)

    ltr(gdt_offset);

	lidt(&idt_pd);
}
f0103cc1:	83 c4 10             	add    $0x10,%esp
f0103cc4:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103cc7:	5b                   	pop    %ebx
f0103cc8:	5e                   	pop    %esi
f0103cc9:	5f                   	pop    %edi
f0103cca:	5d                   	pop    %ebp
f0103ccb:	c3                   	ret    

f0103ccc <trap_init>:
    } while (0)

//GD_KT is "kernel text"
void
trap_init(void)
{
f0103ccc:	55                   	push   %ebp
f0103ccd:	89 e5                	mov    %esp,%ebp
f0103ccf:	83 ec 08             	sub    $0x8,%esp
	//SETGATE(idt[0], 0, GD_KT, );

	// Note that ISTRAP must be zero for all gates
    // because JOS says when it enters kernel,
    // all interrupts are masked.
	SG(0, 0, 0);
f0103cd2:	b8 9c 48 10 f0       	mov    $0xf010489c,%eax
f0103cd7:	66 a3 60 22 27 f0    	mov    %ax,0xf0272260
f0103cdd:	66 c7 05 62 22 27 f0 	movw   $0x8,0xf0272262
f0103ce4:	08 00 
f0103ce6:	c6 05 64 22 27 f0 00 	movb   $0x0,0xf0272264
f0103ced:	c6 05 65 22 27 f0 8e 	movb   $0x8e,0xf0272265
f0103cf4:	c1 e8 10             	shr    $0x10,%eax
f0103cf7:	66 a3 66 22 27 f0    	mov    %ax,0xf0272266
	SG(1, 0, 0);
f0103cfd:	b8 a6 48 10 f0       	mov    $0xf01048a6,%eax
f0103d02:	66 a3 68 22 27 f0    	mov    %ax,0xf0272268
f0103d08:	66 c7 05 6a 22 27 f0 	movw   $0x8,0xf027226a
f0103d0f:	08 00 
f0103d11:	c6 05 6c 22 27 f0 00 	movb   $0x0,0xf027226c
f0103d18:	c6 05 6d 22 27 f0 8e 	movb   $0x8e,0xf027226d
f0103d1f:	c1 e8 10             	shr    $0x10,%eax
f0103d22:	66 a3 6e 22 27 f0    	mov    %ax,0xf027226e
	SG(2, 0, 0);
f0103d28:	b8 b0 48 10 f0       	mov    $0xf01048b0,%eax
f0103d2d:	66 a3 70 22 27 f0    	mov    %ax,0xf0272270
f0103d33:	66 c7 05 72 22 27 f0 	movw   $0x8,0xf0272272
f0103d3a:	08 00 
f0103d3c:	c6 05 74 22 27 f0 00 	movb   $0x0,0xf0272274
f0103d43:	c6 05 75 22 27 f0 8e 	movb   $0x8e,0xf0272275
f0103d4a:	c1 e8 10             	shr    $0x10,%eax
f0103d4d:	66 a3 76 22 27 f0    	mov    %ax,0xf0272276
	SG(3, 0, 3);
f0103d53:	b8 ba 48 10 f0       	mov    $0xf01048ba,%eax
f0103d58:	66 a3 78 22 27 f0    	mov    %ax,0xf0272278
f0103d5e:	66 c7 05 7a 22 27 f0 	movw   $0x8,0xf027227a
f0103d65:	08 00 
f0103d67:	c6 05 7c 22 27 f0 00 	movb   $0x0,0xf027227c
f0103d6e:	c6 05 7d 22 27 f0 ee 	movb   $0xee,0xf027227d
f0103d75:	c1 e8 10             	shr    $0x10,%eax
f0103d78:	66 a3 7e 22 27 f0    	mov    %ax,0xf027227e
	SG(4, 0, 0);
f0103d7e:	b8 c4 48 10 f0       	mov    $0xf01048c4,%eax
f0103d83:	66 a3 80 22 27 f0    	mov    %ax,0xf0272280
f0103d89:	66 c7 05 82 22 27 f0 	movw   $0x8,0xf0272282
f0103d90:	08 00 
f0103d92:	c6 05 84 22 27 f0 00 	movb   $0x0,0xf0272284
f0103d99:	c6 05 85 22 27 f0 8e 	movb   $0x8e,0xf0272285
f0103da0:	c1 e8 10             	shr    $0x10,%eax
f0103da3:	66 a3 86 22 27 f0    	mov    %ax,0xf0272286
	SG(5, 0, 0);
f0103da9:	b8 ce 48 10 f0       	mov    $0xf01048ce,%eax
f0103dae:	66 a3 88 22 27 f0    	mov    %ax,0xf0272288
f0103db4:	66 c7 05 8a 22 27 f0 	movw   $0x8,0xf027228a
f0103dbb:	08 00 
f0103dbd:	c6 05 8c 22 27 f0 00 	movb   $0x0,0xf027228c
f0103dc4:	c6 05 8d 22 27 f0 8e 	movb   $0x8e,0xf027228d
f0103dcb:	c1 e8 10             	shr    $0x10,%eax
f0103dce:	66 a3 8e 22 27 f0    	mov    %ax,0xf027228e
	SG(6, 0, 0);
f0103dd4:	b8 d8 48 10 f0       	mov    $0xf01048d8,%eax
f0103dd9:	66 a3 90 22 27 f0    	mov    %ax,0xf0272290
f0103ddf:	66 c7 05 92 22 27 f0 	movw   $0x8,0xf0272292
f0103de6:	08 00 
f0103de8:	c6 05 94 22 27 f0 00 	movb   $0x0,0xf0272294
f0103def:	c6 05 95 22 27 f0 8e 	movb   $0x8e,0xf0272295
f0103df6:	c1 e8 10             	shr    $0x10,%eax
f0103df9:	66 a3 96 22 27 f0    	mov    %ax,0xf0272296
	SG(7, 0, 0);
f0103dff:	b8 e2 48 10 f0       	mov    $0xf01048e2,%eax
f0103e04:	66 a3 98 22 27 f0    	mov    %ax,0xf0272298
f0103e0a:	66 c7 05 9a 22 27 f0 	movw   $0x8,0xf027229a
f0103e11:	08 00 
f0103e13:	c6 05 9c 22 27 f0 00 	movb   $0x0,0xf027229c
f0103e1a:	c6 05 9d 22 27 f0 8e 	movb   $0x8e,0xf027229d
f0103e21:	c1 e8 10             	shr    $0x10,%eax
f0103e24:	66 a3 9e 22 27 f0    	mov    %ax,0xf027229e
	SG(8, 0, 0);
f0103e2a:	b8 ec 48 10 f0       	mov    $0xf01048ec,%eax
f0103e2f:	66 a3 a0 22 27 f0    	mov    %ax,0xf02722a0
f0103e35:	66 c7 05 a2 22 27 f0 	movw   $0x8,0xf02722a2
f0103e3c:	08 00 
f0103e3e:	c6 05 a4 22 27 f0 00 	movb   $0x0,0xf02722a4
f0103e45:	c6 05 a5 22 27 f0 8e 	movb   $0x8e,0xf02722a5
f0103e4c:	c1 e8 10             	shr    $0x10,%eax
f0103e4f:	66 a3 a6 22 27 f0    	mov    %ax,0xf02722a6
	SG(10, 0, 0);
f0103e55:	b8 f4 48 10 f0       	mov    $0xf01048f4,%eax
f0103e5a:	66 a3 b0 22 27 f0    	mov    %ax,0xf02722b0
f0103e60:	66 c7 05 b2 22 27 f0 	movw   $0x8,0xf02722b2
f0103e67:	08 00 
f0103e69:	c6 05 b4 22 27 f0 00 	movb   $0x0,0xf02722b4
f0103e70:	c6 05 b5 22 27 f0 8e 	movb   $0x8e,0xf02722b5
f0103e77:	c1 e8 10             	shr    $0x10,%eax
f0103e7a:	66 a3 b6 22 27 f0    	mov    %ax,0xf02722b6
	SG(11, 0, 0);
f0103e80:	b8 fc 48 10 f0       	mov    $0xf01048fc,%eax
f0103e85:	66 a3 b8 22 27 f0    	mov    %ax,0xf02722b8
f0103e8b:	66 c7 05 ba 22 27 f0 	movw   $0x8,0xf02722ba
f0103e92:	08 00 
f0103e94:	c6 05 bc 22 27 f0 00 	movb   $0x0,0xf02722bc
f0103e9b:	c6 05 bd 22 27 f0 8e 	movb   $0x8e,0xf02722bd
f0103ea2:	c1 e8 10             	shr    $0x10,%eax
f0103ea5:	66 a3 be 22 27 f0    	mov    %ax,0xf02722be
	SG(12, 0, 0);
f0103eab:	b8 04 49 10 f0       	mov    $0xf0104904,%eax
f0103eb0:	66 a3 c0 22 27 f0    	mov    %ax,0xf02722c0
f0103eb6:	66 c7 05 c2 22 27 f0 	movw   $0x8,0xf02722c2
f0103ebd:	08 00 
f0103ebf:	c6 05 c4 22 27 f0 00 	movb   $0x0,0xf02722c4
f0103ec6:	c6 05 c5 22 27 f0 8e 	movb   $0x8e,0xf02722c5
f0103ecd:	c1 e8 10             	shr    $0x10,%eax
f0103ed0:	66 a3 c6 22 27 f0    	mov    %ax,0xf02722c6
	SG(13, 0, 0);
f0103ed6:	b8 0c 49 10 f0       	mov    $0xf010490c,%eax
f0103edb:	66 a3 c8 22 27 f0    	mov    %ax,0xf02722c8
f0103ee1:	66 c7 05 ca 22 27 f0 	movw   $0x8,0xf02722ca
f0103ee8:	08 00 
f0103eea:	c6 05 cc 22 27 f0 00 	movb   $0x0,0xf02722cc
f0103ef1:	c6 05 cd 22 27 f0 8e 	movb   $0x8e,0xf02722cd
f0103ef8:	c1 e8 10             	shr    $0x10,%eax
f0103efb:	66 a3 ce 22 27 f0    	mov    %ax,0xf02722ce
	SG(14, 0, 0);
f0103f01:	b8 14 49 10 f0       	mov    $0xf0104914,%eax
f0103f06:	66 a3 d0 22 27 f0    	mov    %ax,0xf02722d0
f0103f0c:	66 c7 05 d2 22 27 f0 	movw   $0x8,0xf02722d2
f0103f13:	08 00 
f0103f15:	c6 05 d4 22 27 f0 00 	movb   $0x0,0xf02722d4
f0103f1c:	c6 05 d5 22 27 f0 8e 	movb   $0x8e,0xf02722d5
f0103f23:	c1 e8 10             	shr    $0x10,%eax
f0103f26:	66 a3 d6 22 27 f0    	mov    %ax,0xf02722d6
	SG(16, 0, 0);
f0103f2c:	b8 1c 49 10 f0       	mov    $0xf010491c,%eax
f0103f31:	66 a3 e0 22 27 f0    	mov    %ax,0xf02722e0
f0103f37:	66 c7 05 e2 22 27 f0 	movw   $0x8,0xf02722e2
f0103f3e:	08 00 
f0103f40:	c6 05 e4 22 27 f0 00 	movb   $0x0,0xf02722e4
f0103f47:	c6 05 e5 22 27 f0 8e 	movb   $0x8e,0xf02722e5
f0103f4e:	c1 e8 10             	shr    $0x10,%eax
f0103f51:	66 a3 e6 22 27 f0    	mov    %ax,0xf02722e6
	SG(17, 0, 0);
f0103f57:	b8 26 49 10 f0       	mov    $0xf0104926,%eax
f0103f5c:	66 a3 e8 22 27 f0    	mov    %ax,0xf02722e8
f0103f62:	66 c7 05 ea 22 27 f0 	movw   $0x8,0xf02722ea
f0103f69:	08 00 
f0103f6b:	c6 05 ec 22 27 f0 00 	movb   $0x0,0xf02722ec
f0103f72:	c6 05 ed 22 27 f0 8e 	movb   $0x8e,0xf02722ed
f0103f79:	c1 e8 10             	shr    $0x10,%eax
f0103f7c:	66 a3 ee 22 27 f0    	mov    %ax,0xf02722ee
	SG(18, 0, 0);
f0103f82:	b8 2e 49 10 f0       	mov    $0xf010492e,%eax
f0103f87:	66 a3 f0 22 27 f0    	mov    %ax,0xf02722f0
f0103f8d:	66 c7 05 f2 22 27 f0 	movw   $0x8,0xf02722f2
f0103f94:	08 00 
f0103f96:	c6 05 f4 22 27 f0 00 	movb   $0x0,0xf02722f4
f0103f9d:	c6 05 f5 22 27 f0 8e 	movb   $0x8e,0xf02722f5
f0103fa4:	c1 e8 10             	shr    $0x10,%eax
f0103fa7:	66 a3 f6 22 27 f0    	mov    %ax,0xf02722f6
	SG(19, 0, 0);
f0103fad:	b8 38 49 10 f0       	mov    $0xf0104938,%eax
f0103fb2:	66 a3 f8 22 27 f0    	mov    %ax,0xf02722f8
f0103fb8:	66 c7 05 fa 22 27 f0 	movw   $0x8,0xf02722fa
f0103fbf:	08 00 
f0103fc1:	c6 05 fc 22 27 f0 00 	movb   $0x0,0xf02722fc
f0103fc8:	c6 05 fd 22 27 f0 8e 	movb   $0x8e,0xf02722fd
f0103fcf:	c1 e8 10             	shr    $0x10,%eax
f0103fd2:	66 a3 fe 22 27 f0    	mov    %ax,0xf02722fe

	// 16 IRQs from reference code
	SG(32, 0, 0);
f0103fd8:	b8 42 49 10 f0       	mov    $0xf0104942,%eax
f0103fdd:	66 a3 60 23 27 f0    	mov    %ax,0xf0272360
f0103fe3:	66 c7 05 62 23 27 f0 	movw   $0x8,0xf0272362
f0103fea:	08 00 
f0103fec:	c6 05 64 23 27 f0 00 	movb   $0x0,0xf0272364
f0103ff3:	c6 05 65 23 27 f0 8e 	movb   $0x8e,0xf0272365
f0103ffa:	c1 e8 10             	shr    $0x10,%eax
f0103ffd:	66 a3 66 23 27 f0    	mov    %ax,0xf0272366
	SG(33, 0, 0);
f0104003:	b8 4c 49 10 f0       	mov    $0xf010494c,%eax
f0104008:	66 a3 68 23 27 f0    	mov    %ax,0xf0272368
f010400e:	66 c7 05 6a 23 27 f0 	movw   $0x8,0xf027236a
f0104015:	08 00 
f0104017:	c6 05 6c 23 27 f0 00 	movb   $0x0,0xf027236c
f010401e:	c6 05 6d 23 27 f0 8e 	movb   $0x8e,0xf027236d
f0104025:	c1 e8 10             	shr    $0x10,%eax
f0104028:	66 a3 6e 23 27 f0    	mov    %ax,0xf027236e
	SG(34, 0, 0);
f010402e:	b8 56 49 10 f0       	mov    $0xf0104956,%eax
f0104033:	66 a3 70 23 27 f0    	mov    %ax,0xf0272370
f0104039:	66 c7 05 72 23 27 f0 	movw   $0x8,0xf0272372
f0104040:	08 00 
f0104042:	c6 05 74 23 27 f0 00 	movb   $0x0,0xf0272374
f0104049:	c6 05 75 23 27 f0 8e 	movb   $0x8e,0xf0272375
f0104050:	c1 e8 10             	shr    $0x10,%eax
f0104053:	66 a3 76 23 27 f0    	mov    %ax,0xf0272376
	SG(35, 0, 0);
f0104059:	b8 60 49 10 f0       	mov    $0xf0104960,%eax
f010405e:	66 a3 78 23 27 f0    	mov    %ax,0xf0272378
f0104064:	66 c7 05 7a 23 27 f0 	movw   $0x8,0xf027237a
f010406b:	08 00 
f010406d:	c6 05 7c 23 27 f0 00 	movb   $0x0,0xf027237c
f0104074:	c6 05 7d 23 27 f0 8e 	movb   $0x8e,0xf027237d
f010407b:	c1 e8 10             	shr    $0x10,%eax
f010407e:	66 a3 7e 23 27 f0    	mov    %ax,0xf027237e
	SG(36, 0, 0);
f0104084:	b8 6a 49 10 f0       	mov    $0xf010496a,%eax
f0104089:	66 a3 80 23 27 f0    	mov    %ax,0xf0272380
f010408f:	66 c7 05 82 23 27 f0 	movw   $0x8,0xf0272382
f0104096:	08 00 
f0104098:	c6 05 84 23 27 f0 00 	movb   $0x0,0xf0272384
f010409f:	c6 05 85 23 27 f0 8e 	movb   $0x8e,0xf0272385
f01040a6:	c1 e8 10             	shr    $0x10,%eax
f01040a9:	66 a3 86 23 27 f0    	mov    %ax,0xf0272386
	SG(37, 0, 0);
f01040af:	b8 74 49 10 f0       	mov    $0xf0104974,%eax
f01040b4:	66 a3 88 23 27 f0    	mov    %ax,0xf0272388
f01040ba:	66 c7 05 8a 23 27 f0 	movw   $0x8,0xf027238a
f01040c1:	08 00 
f01040c3:	c6 05 8c 23 27 f0 00 	movb   $0x0,0xf027238c
f01040ca:	c6 05 8d 23 27 f0 8e 	movb   $0x8e,0xf027238d
f01040d1:	c1 e8 10             	shr    $0x10,%eax
f01040d4:	66 a3 8e 23 27 f0    	mov    %ax,0xf027238e
	SG(38, 0, 0);
f01040da:	b8 7e 49 10 f0       	mov    $0xf010497e,%eax
f01040df:	66 a3 90 23 27 f0    	mov    %ax,0xf0272390
f01040e5:	66 c7 05 92 23 27 f0 	movw   $0x8,0xf0272392
f01040ec:	08 00 
f01040ee:	c6 05 94 23 27 f0 00 	movb   $0x0,0xf0272394
f01040f5:	c6 05 95 23 27 f0 8e 	movb   $0x8e,0xf0272395
f01040fc:	c1 e8 10             	shr    $0x10,%eax
f01040ff:	66 a3 96 23 27 f0    	mov    %ax,0xf0272396
	SG(39, 0, 0);
f0104105:	b8 88 49 10 f0       	mov    $0xf0104988,%eax
f010410a:	66 a3 98 23 27 f0    	mov    %ax,0xf0272398
f0104110:	66 c7 05 9a 23 27 f0 	movw   $0x8,0xf027239a
f0104117:	08 00 
f0104119:	c6 05 9c 23 27 f0 00 	movb   $0x0,0xf027239c
f0104120:	c6 05 9d 23 27 f0 8e 	movb   $0x8e,0xf027239d
f0104127:	c1 e8 10             	shr    $0x10,%eax
f010412a:	66 a3 9e 23 27 f0    	mov    %ax,0xf027239e
	SG(40, 0, 0);
f0104130:	b8 92 49 10 f0       	mov    $0xf0104992,%eax
f0104135:	66 a3 a0 23 27 f0    	mov    %ax,0xf02723a0
f010413b:	66 c7 05 a2 23 27 f0 	movw   $0x8,0xf02723a2
f0104142:	08 00 
f0104144:	c6 05 a4 23 27 f0 00 	movb   $0x0,0xf02723a4
f010414b:	c6 05 a5 23 27 f0 8e 	movb   $0x8e,0xf02723a5
f0104152:	c1 e8 10             	shr    $0x10,%eax
f0104155:	66 a3 a6 23 27 f0    	mov    %ax,0xf02723a6
	SG(41, 0, 0);
f010415b:	b8 9c 49 10 f0       	mov    $0xf010499c,%eax
f0104160:	66 a3 a8 23 27 f0    	mov    %ax,0xf02723a8
f0104166:	66 c7 05 aa 23 27 f0 	movw   $0x8,0xf02723aa
f010416d:	08 00 
f010416f:	c6 05 ac 23 27 f0 00 	movb   $0x0,0xf02723ac
f0104176:	c6 05 ad 23 27 f0 8e 	movb   $0x8e,0xf02723ad
f010417d:	c1 e8 10             	shr    $0x10,%eax
f0104180:	66 a3 ae 23 27 f0    	mov    %ax,0xf02723ae
	SG(42, 0, 0);
f0104186:	b8 a6 49 10 f0       	mov    $0xf01049a6,%eax
f010418b:	66 a3 b0 23 27 f0    	mov    %ax,0xf02723b0
f0104191:	66 c7 05 b2 23 27 f0 	movw   $0x8,0xf02723b2
f0104198:	08 00 
f010419a:	c6 05 b4 23 27 f0 00 	movb   $0x0,0xf02723b4
f01041a1:	c6 05 b5 23 27 f0 8e 	movb   $0x8e,0xf02723b5
f01041a8:	c1 e8 10             	shr    $0x10,%eax
f01041ab:	66 a3 b6 23 27 f0    	mov    %ax,0xf02723b6
	SG(43, 0, 0);
f01041b1:	b8 b0 49 10 f0       	mov    $0xf01049b0,%eax
f01041b6:	66 a3 b8 23 27 f0    	mov    %ax,0xf02723b8
f01041bc:	66 c7 05 ba 23 27 f0 	movw   $0x8,0xf02723ba
f01041c3:	08 00 
f01041c5:	c6 05 bc 23 27 f0 00 	movb   $0x0,0xf02723bc
f01041cc:	c6 05 bd 23 27 f0 8e 	movb   $0x8e,0xf02723bd
f01041d3:	c1 e8 10             	shr    $0x10,%eax
f01041d6:	66 a3 be 23 27 f0    	mov    %ax,0xf02723be
	SG(44, 0, 0);
f01041dc:	b8 ba 49 10 f0       	mov    $0xf01049ba,%eax
f01041e1:	66 a3 c0 23 27 f0    	mov    %ax,0xf02723c0
f01041e7:	66 c7 05 c2 23 27 f0 	movw   $0x8,0xf02723c2
f01041ee:	08 00 
f01041f0:	c6 05 c4 23 27 f0 00 	movb   $0x0,0xf02723c4
f01041f7:	c6 05 c5 23 27 f0 8e 	movb   $0x8e,0xf02723c5
f01041fe:	c1 e8 10             	shr    $0x10,%eax
f0104201:	66 a3 c6 23 27 f0    	mov    %ax,0xf02723c6
	SG(45, 0, 0);
f0104207:	b8 c4 49 10 f0       	mov    $0xf01049c4,%eax
f010420c:	66 a3 c8 23 27 f0    	mov    %ax,0xf02723c8
f0104212:	66 c7 05 ca 23 27 f0 	movw   $0x8,0xf02723ca
f0104219:	08 00 
f010421b:	c6 05 cc 23 27 f0 00 	movb   $0x0,0xf02723cc
f0104222:	c6 05 cd 23 27 f0 8e 	movb   $0x8e,0xf02723cd
f0104229:	c1 e8 10             	shr    $0x10,%eax
f010422c:	66 a3 ce 23 27 f0    	mov    %ax,0xf02723ce
	SG(46, 0, 0);
f0104232:	b8 ce 49 10 f0       	mov    $0xf01049ce,%eax
f0104237:	66 a3 d0 23 27 f0    	mov    %ax,0xf02723d0
f010423d:	66 c7 05 d2 23 27 f0 	movw   $0x8,0xf02723d2
f0104244:	08 00 
f0104246:	c6 05 d4 23 27 f0 00 	movb   $0x0,0xf02723d4
f010424d:	c6 05 d5 23 27 f0 8e 	movb   $0x8e,0xf02723d5
f0104254:	c1 e8 10             	shr    $0x10,%eax
f0104257:	66 a3 d6 23 27 f0    	mov    %ax,0xf02723d6
	SG(47, 0, 0);
f010425d:	b8 d8 49 10 f0       	mov    $0xf01049d8,%eax
f0104262:	66 a3 d8 23 27 f0    	mov    %ax,0xf02723d8
f0104268:	66 c7 05 da 23 27 f0 	movw   $0x8,0xf02723da
f010426f:	08 00 
f0104271:	c6 05 dc 23 27 f0 00 	movb   $0x0,0xf02723dc
f0104278:	c6 05 dd 23 27 f0 8e 	movb   $0x8e,0xf02723dd
f010427f:	c1 e8 10             	shr    $0x10,%eax
f0104282:	66 a3 de 23 27 f0    	mov    %ax,0xf02723de
 
	SG(48, 0, 3); //for interrupt
f0104288:	b8 e2 49 10 f0       	mov    $0xf01049e2,%eax
f010428d:	66 a3 e0 23 27 f0    	mov    %ax,0xf02723e0
f0104293:	66 c7 05 e2 23 27 f0 	movw   $0x8,0xf02723e2
f010429a:	08 00 
f010429c:	c6 05 e4 23 27 f0 00 	movb   $0x0,0xf02723e4
f01042a3:	c6 05 e5 23 27 f0 ee 	movb   $0xee,0xf02723e5
f01042aa:	c1 e8 10             	shr    $0x10,%eax
f01042ad:	66 a3 e6 23 27 f0    	mov    %ax,0xf02723e6
	// warn("trap_init: treat system call as not trap");

	// Per-CPU setup 
	trap_init_percpu();
f01042b3:	e8 54 f9 ff ff       	call   f0103c0c <trap_init_percpu>
}
f01042b8:	c9                   	leave  
f01042b9:	c3                   	ret    

f01042ba <print_regs>:
	}
}

void
print_regs(struct PushRegs *regs)
{
f01042ba:	55                   	push   %ebp
f01042bb:	89 e5                	mov    %esp,%ebp
f01042bd:	53                   	push   %ebx
f01042be:	83 ec 0c             	sub    $0xc,%esp
f01042c1:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("  edi  0x%08x\n", regs->reg_edi);
f01042c4:	ff 33                	pushl  (%ebx)
f01042c6:	68 82 84 10 f0       	push   $0xf0108482
f01042cb:	e8 28 f9 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  esi  0x%08x\n", regs->reg_esi);
f01042d0:	83 c4 08             	add    $0x8,%esp
f01042d3:	ff 73 04             	pushl  0x4(%ebx)
f01042d6:	68 91 84 10 f0       	push   $0xf0108491
f01042db:	e8 18 f9 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
f01042e0:	83 c4 08             	add    $0x8,%esp
f01042e3:	ff 73 08             	pushl  0x8(%ebx)
f01042e6:	68 a0 84 10 f0       	push   $0xf01084a0
f01042eb:	e8 08 f9 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
f01042f0:	83 c4 08             	add    $0x8,%esp
f01042f3:	ff 73 0c             	pushl  0xc(%ebx)
f01042f6:	68 af 84 10 f0       	push   $0xf01084af
f01042fb:	e8 f8 f8 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
f0104300:	83 c4 08             	add    $0x8,%esp
f0104303:	ff 73 10             	pushl  0x10(%ebx)
f0104306:	68 be 84 10 f0       	push   $0xf01084be
f010430b:	e8 e8 f8 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  edx  0x%08x\n", regs->reg_edx);
f0104310:	83 c4 08             	add    $0x8,%esp
f0104313:	ff 73 14             	pushl  0x14(%ebx)
f0104316:	68 cd 84 10 f0       	push   $0xf01084cd
f010431b:	e8 d8 f8 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
f0104320:	83 c4 08             	add    $0x8,%esp
f0104323:	ff 73 18             	pushl  0x18(%ebx)
f0104326:	68 dc 84 10 f0       	push   $0xf01084dc
f010432b:	e8 c8 f8 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  eax  0x%08x\n", regs->reg_eax);
f0104330:	83 c4 08             	add    $0x8,%esp
f0104333:	ff 73 1c             	pushl  0x1c(%ebx)
f0104336:	68 eb 84 10 f0       	push   $0xf01084eb
f010433b:	e8 b8 f8 ff ff       	call   f0103bf8 <cprintf>
}
f0104340:	83 c4 10             	add    $0x10,%esp
f0104343:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0104346:	c9                   	leave  
f0104347:	c3                   	ret    

f0104348 <print_trapframe>:
	lidt(&idt_pd);
}

void
print_trapframe(struct Trapframe *tf)
{
f0104348:	55                   	push   %ebp
f0104349:	89 e5                	mov    %esp,%ebp
f010434b:	53                   	push   %ebx
f010434c:	83 ec 04             	sub    $0x4,%esp
f010434f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("TRAP frame at %p from CPU %d\n", tf, cpunum());
f0104352:	e8 b3 23 00 00       	call   f010670a <cpunum>
f0104357:	83 ec 04             	sub    $0x4,%esp
f010435a:	50                   	push   %eax
f010435b:	53                   	push   %ebx
f010435c:	68 4f 85 10 f0       	push   $0xf010854f
f0104361:	e8 92 f8 ff ff       	call   f0103bf8 <cprintf>
	print_regs(&tf->tf_regs);
f0104366:	89 1c 24             	mov    %ebx,(%esp)
f0104369:	e8 4c ff ff ff       	call   f01042ba <print_regs>
	cprintf("  es   0x----%04x\n", tf->tf_es);
f010436e:	83 c4 08             	add    $0x8,%esp
f0104371:	0f b7 43 20          	movzwl 0x20(%ebx),%eax
f0104375:	50                   	push   %eax
f0104376:	68 6d 85 10 f0       	push   $0xf010856d
f010437b:	e8 78 f8 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
f0104380:	83 c4 08             	add    $0x8,%esp
f0104383:	0f b7 43 24          	movzwl 0x24(%ebx),%eax
f0104387:	50                   	push   %eax
f0104388:	68 80 85 10 f0       	push   $0xf0108580
f010438d:	e8 66 f8 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f0104392:	8b 43 28             	mov    0x28(%ebx),%eax
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < ARRAY_SIZE(excnames))
f0104395:	83 c4 10             	add    $0x10,%esp
f0104398:	83 f8 13             	cmp    $0x13,%eax
f010439b:	77 09                	ja     f01043a6 <print_trapframe+0x5e>
		return excnames[trapno];
f010439d:	8b 14 85 20 88 10 f0 	mov    -0xfef77e0(,%eax,4),%edx
f01043a4:	eb 20                	jmp    f01043c6 <print_trapframe+0x7e>
	if (trapno == T_SYSCALL)
f01043a6:	83 f8 30             	cmp    $0x30,%eax
f01043a9:	74 0f                	je     f01043ba <print_trapframe+0x72>
		return "System call";
	if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
f01043ab:	8d 50 e0             	lea    -0x20(%eax),%edx
f01043ae:	83 fa 0f             	cmp    $0xf,%edx
f01043b1:	76 0e                	jbe    f01043c1 <print_trapframe+0x79>
		return "Hardware Interrupt";
	return "(unknown trap)";
f01043b3:	ba 19 85 10 f0       	mov    $0xf0108519,%edx
f01043b8:	eb 0c                	jmp    f01043c6 <print_trapframe+0x7e>
	};

	if (trapno < ARRAY_SIZE(excnames))
		return excnames[trapno];
	if (trapno == T_SYSCALL)
		return "System call";
f01043ba:	ba fa 84 10 f0       	mov    $0xf01084fa,%edx
f01043bf:	eb 05                	jmp    f01043c6 <print_trapframe+0x7e>
	if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
		return "Hardware Interrupt";
f01043c1:	ba 06 85 10 f0       	mov    $0xf0108506,%edx
{
	cprintf("TRAP frame at %p from CPU %d\n", tf, cpunum());
	print_regs(&tf->tf_regs);
	cprintf("  es   0x----%04x\n", tf->tf_es);
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f01043c6:	83 ec 04             	sub    $0x4,%esp
f01043c9:	52                   	push   %edx
f01043ca:	50                   	push   %eax
f01043cb:	68 93 85 10 f0       	push   $0xf0108593
f01043d0:	e8 23 f8 ff ff       	call   f0103bf8 <cprintf>
	// If this trap was a page fault that just happened
	// (so %cr2 is meaningful), print the faulting linear address.
	if (tf == last_tf && tf->tf_trapno == T_PGFLT)
f01043d5:	83 c4 10             	add    $0x10,%esp
f01043d8:	3b 1d 60 2a 27 f0    	cmp    0xf0272a60,%ebx
f01043de:	75 1a                	jne    f01043fa <print_trapframe+0xb2>
f01043e0:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f01043e4:	75 14                	jne    f01043fa <print_trapframe+0xb2>

static inline uint32_t
rcr2(void)
{
	uint32_t val;
	asm volatile("movl %%cr2,%0" : "=r" (val));
f01043e6:	0f 20 d0             	mov    %cr2,%eax
		cprintf("  cr2  0x%08x\n", rcr2());
f01043e9:	83 ec 08             	sub    $0x8,%esp
f01043ec:	50                   	push   %eax
f01043ed:	68 a5 85 10 f0       	push   $0xf01085a5
f01043f2:	e8 01 f8 ff ff       	call   f0103bf8 <cprintf>
f01043f7:	83 c4 10             	add    $0x10,%esp
	cprintf("  err  0x%08x", tf->tf_err);
f01043fa:	83 ec 08             	sub    $0x8,%esp
f01043fd:	ff 73 2c             	pushl  0x2c(%ebx)
f0104400:	68 b4 85 10 f0       	push   $0xf01085b4
f0104405:	e8 ee f7 ff ff       	call   f0103bf8 <cprintf>
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
f010440a:	83 c4 10             	add    $0x10,%esp
f010440d:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f0104411:	75 45                	jne    f0104458 <print_trapframe+0x110>
		cprintf(" [%s, %s, %s]\n",
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
f0104413:	8b 43 2c             	mov    0x2c(%ebx),%eax
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
		cprintf(" [%s, %s, %s]\n",
f0104416:	a8 01                	test   $0x1,%al
f0104418:	75 07                	jne    f0104421 <print_trapframe+0xd9>
f010441a:	b9 33 85 10 f0       	mov    $0xf0108533,%ecx
f010441f:	eb 05                	jmp    f0104426 <print_trapframe+0xde>
f0104421:	b9 28 85 10 f0       	mov    $0xf0108528,%ecx
f0104426:	a8 02                	test   $0x2,%al
f0104428:	75 07                	jne    f0104431 <print_trapframe+0xe9>
f010442a:	ba 45 85 10 f0       	mov    $0xf0108545,%edx
f010442f:	eb 05                	jmp    f0104436 <print_trapframe+0xee>
f0104431:	ba 3f 85 10 f0       	mov    $0xf010853f,%edx
f0104436:	a8 04                	test   $0x4,%al
f0104438:	75 07                	jne    f0104441 <print_trapframe+0xf9>
f010443a:	b8 9a 86 10 f0       	mov    $0xf010869a,%eax
f010443f:	eb 05                	jmp    f0104446 <print_trapframe+0xfe>
f0104441:	b8 4a 85 10 f0       	mov    $0xf010854a,%eax
f0104446:	51                   	push   %ecx
f0104447:	52                   	push   %edx
f0104448:	50                   	push   %eax
f0104449:	68 c2 85 10 f0       	push   $0xf01085c2
f010444e:	e8 a5 f7 ff ff       	call   f0103bf8 <cprintf>
f0104453:	83 c4 10             	add    $0x10,%esp
f0104456:	eb 10                	jmp    f0104468 <print_trapframe+0x120>
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
	else
		cprintf("\n");
f0104458:	83 ec 0c             	sub    $0xc,%esp
f010445b:	68 32 81 10 f0       	push   $0xf0108132
f0104460:	e8 93 f7 ff ff       	call   f0103bf8 <cprintf>
f0104465:	83 c4 10             	add    $0x10,%esp
	cprintf("  eip  0x%08x\n", tf->tf_eip);
f0104468:	83 ec 08             	sub    $0x8,%esp
f010446b:	ff 73 30             	pushl  0x30(%ebx)
f010446e:	68 d1 85 10 f0       	push   $0xf01085d1
f0104473:	e8 80 f7 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
f0104478:	83 c4 08             	add    $0x8,%esp
f010447b:	0f b7 43 34          	movzwl 0x34(%ebx),%eax
f010447f:	50                   	push   %eax
f0104480:	68 e0 85 10 f0       	push   $0xf01085e0
f0104485:	e8 6e f7 ff ff       	call   f0103bf8 <cprintf>
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
f010448a:	83 c4 08             	add    $0x8,%esp
f010448d:	ff 73 38             	pushl  0x38(%ebx)
f0104490:	68 f3 85 10 f0       	push   $0xf01085f3
f0104495:	e8 5e f7 ff ff       	call   f0103bf8 <cprintf>
	if ((tf->tf_cs & 3) != 0) {
f010449a:	83 c4 10             	add    $0x10,%esp
f010449d:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f01044a1:	74 25                	je     f01044c8 <print_trapframe+0x180>
		cprintf("  esp  0x%08x\n", tf->tf_esp);
f01044a3:	83 ec 08             	sub    $0x8,%esp
f01044a6:	ff 73 3c             	pushl  0x3c(%ebx)
f01044a9:	68 02 86 10 f0       	push   $0xf0108602
f01044ae:	e8 45 f7 ff ff       	call   f0103bf8 <cprintf>
		cprintf("  ss   0x----%04x\n", tf->tf_ss);
f01044b3:	83 c4 08             	add    $0x8,%esp
f01044b6:	0f b7 43 40          	movzwl 0x40(%ebx),%eax
f01044ba:	50                   	push   %eax
f01044bb:	68 11 86 10 f0       	push   $0xf0108611
f01044c0:	e8 33 f7 ff ff       	call   f0103bf8 <cprintf>
f01044c5:	83 c4 10             	add    $0x10,%esp
	}
}
f01044c8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01044cb:	c9                   	leave  
f01044cc:	c3                   	ret    

f01044cd <page_fault_handler>:
}

//special function to handle page fault
void
page_fault_handler(struct Trapframe *tf)
{
f01044cd:	55                   	push   %ebp
f01044ce:	89 e5                	mov    %esp,%ebp
f01044d0:	57                   	push   %edi
f01044d1:	56                   	push   %esi
f01044d2:	53                   	push   %ebx
f01044d3:	83 ec 1c             	sub    $0x1c,%esp
f01044d6:	8b 5d 08             	mov    0x8(%ebp),%ebx
f01044d9:	0f 20 d6             	mov    %cr2,%esi

	// Handle kernel-mode page faults.

	// LAB 3: Your code here.
//------------
	if ((tf->tf_cs & 0x3) == 0) { //if page fault happens in kernel
f01044dc:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f01044e0:	75 20                	jne    f0104502 <page_fault_handler+0x35>
        print_trapframe(tf);
f01044e2:	83 ec 0c             	sub    $0xc,%esp
f01044e5:	53                   	push   %ebx
f01044e6:	e8 5d fe ff ff       	call   f0104348 <print_trapframe>
		panic("page fault in kernel space");
f01044eb:	83 c4 0c             	add    $0xc,%esp
f01044ee:	68 24 86 10 f0       	push   $0xf0108624
f01044f3:	68 95 01 00 00       	push   $0x195
f01044f8:	68 3f 86 10 f0       	push   $0xf010863f
f01044fd:	e8 4a bb ff ff       	call   f010004c <_panic>

	// LAB 4: Your code here.
    // If no pgfault_upcall, fall to destruction.
    // If page faults take place in the region [USTACKTOP, UXSTACKTOP - PGSIZE),
    // it means the exception stack is exhausted.
    if (curenv->env_pgfault_upcall &&
f0104502:	e8 03 22 00 00       	call   f010670a <cpunum>
f0104507:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010450a:	01 c2                	add    %eax,%edx
f010450c:	01 d2                	add    %edx,%edx
f010450e:	01 c2                	add    %eax,%edx
f0104510:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104513:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f010451a:	83 78 64 00          	cmpl   $0x0,0x64(%eax)
f010451e:	0f 84 df 00 00 00    	je     f0104603 <page_fault_handler+0x136>
        (tf->tf_esp < USTACKTOP ||
f0104524:	8b 43 3c             	mov    0x3c(%ebx),%eax

	// LAB 4: Your code here.
    // If no pgfault_upcall, fall to destruction.
    // If page faults take place in the region [USTACKTOP, UXSTACKTOP - PGSIZE),
    // it means the exception stack is exhausted.
    if (curenv->env_pgfault_upcall &&
f0104527:	8d 90 00 20 40 11    	lea    0x11402000(%eax),%edx
f010452d:	81 fa ff 0f 00 00    	cmp    $0xfff,%edx
f0104533:	0f 86 ca 00 00 00    	jbe    f0104603 <page_fault_handler+0x136>
        (tf->tf_esp < USTACKTOP ||
         tf->tf_esp >= UXSTACKTOP - PGSIZE)) {
    //if(curenv->env_pgfault_upcall){
        // Determine the starting address of the stack frame.
        uint32_t xtop;
        if (tf->tf_esp >= UXSTACKTOP - PGSIZE &&
f0104539:	8d 90 00 10 40 11    	lea    0x11401000(%eax),%edx
            tf->tf_esp < UXSTACKTOP)
            xtop = tf->tf_esp - sizeof(struct UTrapframe) - 4/* an required empty word */;
        else
            xtop = UXSTACKTOP - sizeof(struct UTrapframe);
f010453f:	c7 45 e4 cc ff bf ee 	movl   $0xeebfffcc,-0x1c(%ebp)
        (tf->tf_esp < USTACKTOP ||
         tf->tf_esp >= UXSTACKTOP - PGSIZE)) {
    //if(curenv->env_pgfault_upcall){
        // Determine the starting address of the stack frame.
        uint32_t xtop;
        if (tf->tf_esp >= UXSTACKTOP - PGSIZE &&
f0104546:	81 fa ff 0f 00 00    	cmp    $0xfff,%edx
f010454c:	77 06                	ja     f0104554 <page_fault_handler+0x87>
            tf->tf_esp < UXSTACKTOP)
            xtop = tf->tf_esp - sizeof(struct UTrapframe) - 4/* an required empty word */;
f010454e:	83 e8 38             	sub    $0x38,%eax
f0104551:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        else
            xtop = UXSTACKTOP - sizeof(struct UTrapframe);
        
        // Check permission for the whole exception stack, but for only once.
        static int is_xstack_checked = 1;
        if (!is_xstack_checked) {
f0104554:	83 3d a8 33 12 f0 00 	cmpl   $0x0,0xf01233a8
f010455b:	75 2c                	jne    f0104589 <page_fault_handler+0xbc>
            user_mem_assert(curenv, (void *)(UXSTACKTOP - PGSIZE), PGSIZE, PTE_W | PTE_U);
f010455d:	e8 a8 21 00 00       	call   f010670a <cpunum>
f0104562:	6a 06                	push   $0x6
f0104564:	68 00 10 00 00       	push   $0x1000
f0104569:	68 00 f0 bf ee       	push   $0xeebff000
f010456e:	6b c0 74             	imul   $0x74,%eax,%eax
f0104571:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f0104577:	e8 8f ec ff ff       	call   f010320b <user_mem_assert>
            is_xstack_checked = 1;
f010457c:	c7 05 a8 33 12 f0 01 	movl   $0x1,0xf01233a8
f0104583:	00 00 00 
f0104586:	83 c4 10             	add    $0x10,%esp
        }
        
        // For the damn test to be OK.
        user_mem_assert(curenv, (void *)xtop, UXSTACKTOP - xtop, PTE_W | PTE_U);
f0104589:	e8 7c 21 00 00       	call   f010670a <cpunum>
f010458e:	6a 06                	push   $0x6
f0104590:	ba 00 00 c0 ee       	mov    $0xeec00000,%edx
f0104595:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0104598:	29 fa                	sub    %edi,%edx
f010459a:	52                   	push   %edx
f010459b:	57                   	push   %edi
f010459c:	6b c0 74             	imul   $0x74,%eax,%eax
f010459f:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f01045a5:	e8 61 ec ff ff       	call   f010320b <user_mem_assert>

        // Push the struct UTrapframe onto the stack.
        struct UTrapframe *utf = (struct UTrapframe *)xtop;
        utf->utf_eflags = tf->tf_eflags;
f01045aa:	8b 43 38             	mov    0x38(%ebx),%eax
f01045ad:	89 47 2c             	mov    %eax,0x2c(%edi)
        utf->utf_eip = tf->tf_eip;
f01045b0:	8b 43 30             	mov    0x30(%ebx),%eax
f01045b3:	89 47 28             	mov    %eax,0x28(%edi)
        utf->utf_err = tf->tf_err;
f01045b6:	8b 43 2c             	mov    0x2c(%ebx),%eax
f01045b9:	89 47 04             	mov    %eax,0x4(%edi)
        utf->utf_esp = tf->tf_esp;
f01045bc:	8b 43 3c             	mov    0x3c(%ebx),%eax
f01045bf:	89 47 30             	mov    %eax,0x30(%edi)
        utf->utf_fault_va = fault_va;
f01045c2:	89 37                	mov    %esi,(%edi)
        utf->utf_regs = tf->tf_regs;
f01045c4:	89 7d e4             	mov    %edi,-0x1c(%ebp)
f01045c7:	8d 7f 08             	lea    0x8(%edi),%edi
f01045ca:	b9 08 00 00 00       	mov    $0x8,%ecx
f01045cf:	89 de                	mov    %ebx,%esi
f01045d1:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
        
        // Run current env with user-page fault handler.
        tf->tf_eip = (uint32_t)curenv->env_pgfault_upcall;
f01045d3:	e8 32 21 00 00       	call   f010670a <cpunum>
f01045d8:	6b c0 74             	imul   $0x74,%eax,%eax
f01045db:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f01045e1:	8b 40 64             	mov    0x64(%eax),%eax
f01045e4:	89 43 30             	mov    %eax,0x30(%ebx)
        tf->tf_esp = xtop;
f01045e7:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f01045ea:	89 4b 3c             	mov    %ecx,0x3c(%ebx)
        env_run(curenv);
f01045ed:	e8 18 21 00 00       	call   f010670a <cpunum>
f01045f2:	83 c4 04             	add    $0x4,%esp
f01045f5:	6b c0 74             	imul   $0x74,%eax,%eax
f01045f8:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f01045fe:	e8 2d f4 ff ff       	call   f0103a30 <env_run>

        // Never reach here.   
    }
	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0104603:	8b 7b 30             	mov    0x30(%ebx),%edi
		curenv->env_id, fault_va, tf->tf_eip);
f0104606:	e8 ff 20 00 00       	call   f010670a <cpunum>
        env_run(curenv);

        // Never reach here.   
    }
	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f010460b:	57                   	push   %edi
f010460c:	56                   	push   %esi
		curenv->env_id, fault_va, tf->tf_eip);
f010460d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104610:	01 c2                	add    %eax,%edx
f0104612:	01 d2                	add    %edx,%edx
f0104614:	01 c2                	add    %eax,%edx
f0104616:	8d 04 90             	lea    (%eax,%edx,4),%eax
        env_run(curenv);

        // Never reach here.   
    }
	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0104619:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0104620:	ff 70 48             	pushl  0x48(%eax)
f0104623:	68 e4 87 10 f0       	push   $0xf01087e4
f0104628:	e8 cb f5 ff ff       	call   f0103bf8 <cprintf>
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
f010462d:	89 1c 24             	mov    %ebx,(%esp)
f0104630:	e8 13 fd ff ff       	call   f0104348 <print_trapframe>
	env_destroy(curenv);
f0104635:	e8 d0 20 00 00       	call   f010670a <cpunum>
f010463a:	83 c4 04             	add    $0x4,%esp
f010463d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104640:	01 c2                	add    %eax,%edx
f0104642:	01 d2                	add    %edx,%edx
f0104644:	01 c2                	add    %eax,%edx
f0104646:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104649:	ff 34 85 08 40 27 f0 	pushl  -0xfd8bff8(,%eax,4)
f0104650:	e8 01 f3 ff ff       	call   f0103956 <env_destroy>
}
f0104655:	83 c4 10             	add    $0x10,%esp
f0104658:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010465b:	5b                   	pop    %ebx
f010465c:	5e                   	pop    %esi
f010465d:	5f                   	pop    %edi
f010465e:	5d                   	pop    %ebp
f010465f:	c3                   	ret    

f0104660 <trap>:
	}*/
}

void
trap(struct Trapframe *tf)
{
f0104660:	55                   	push   %ebp
f0104661:	89 e5                	mov    %esp,%ebp
f0104663:	57                   	push   %edi
f0104664:	56                   	push   %esi
f0104665:	8b 75 08             	mov    0x8(%ebp),%esi
	// The environment may have set DF and some versions
	// of GCC rely on DF being clear
	asm volatile("cld" ::: "cc");
f0104668:	fc                   	cld    

	// Halt the CPU if some other CPU has called panic()
	extern char *panicstr;
	if (panicstr)
f0104669:	83 3d 10 2f 27 f0 00 	cmpl   $0x0,0xf0272f10
f0104670:	74 01                	je     f0104673 <trap+0x13>
		asm volatile("hlt");
f0104672:	f4                   	hlt    

	// Re-acqurie the big kernel lock if we were halted in
	// sched_yield()
	if (xchg(&thiscpu->cpu_status, CPU_STARTED) == CPU_HALTED)
f0104673:	e8 92 20 00 00       	call   f010670a <cpunum>
f0104678:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010467b:	01 c2                	add    %eax,%edx
f010467d:	01 d2                	add    %edx,%edx
f010467f:	01 c2                	add    %eax,%edx
f0104681:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104684:	8d 14 85 00 40 27 f0 	lea    -0xfd8c000(,%eax,4),%edx
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f010468b:	b8 01 00 00 00       	mov    $0x1,%eax
f0104690:	f0 87 42 04          	lock xchg %eax,0x4(%edx)
f0104694:	83 f8 02             	cmp    $0x2,%eax
f0104697:	75 10                	jne    f01046a9 <trap+0x49>
extern struct spinlock kernel_lock;

static inline void
lock_kernel(void)
{
	spin_lock(&kernel_lock);
f0104699:	83 ec 0c             	sub    $0xc,%esp
f010469c:	68 c0 33 12 f0       	push   $0xf01233c0
f01046a1:	e8 22 24 00 00       	call   f0106ac8 <spin_lock>
f01046a6:	83 c4 10             	add    $0x10,%esp

static inline uint32_t
read_eflags(void)
{
	uint32_t eflags;
	asm volatile("pushfl; popl %0" : "=r" (eflags));
f01046a9:	9c                   	pushf  
f01046aa:	58                   	pop    %eax
		lock_kernel();
	// Check that interrupts are disabled.  If this assertion
	// fails, DO NOT be tempted to fix it by inserting a "cli" in
	// the interrupt path.
	assert(!(read_eflags() & FL_IF));
f01046ab:	f6 c4 02             	test   $0x2,%ah
f01046ae:	74 19                	je     f01046c9 <trap+0x69>
f01046b0:	68 4b 86 10 f0       	push   $0xf010864b
f01046b5:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01046ba:	68 54 01 00 00       	push   $0x154
f01046bf:	68 3f 86 10 f0       	push   $0xf010863f
f01046c4:	e8 83 b9 ff ff       	call   f010004c <_panic>

	if ((tf->tf_cs & 3) == 3) {
f01046c9:	66 8b 46 34          	mov    0x34(%esi),%ax
f01046cd:	83 e0 03             	and    $0x3,%eax
f01046d0:	66 83 f8 03          	cmp    $0x3,%ax
f01046d4:	0f 85 a0 00 00 00    	jne    f010477a <trap+0x11a>
		// Trapped from user mode.
		// Acquire the big kernel lock before doing any
		// serious kernel work.
		// LAB 4: Your code here.
		assert(curenv);
f01046da:	e8 2b 20 00 00       	call   f010670a <cpunum>
f01046df:	6b c0 74             	imul   $0x74,%eax,%eax
f01046e2:	83 b8 08 40 27 f0 00 	cmpl   $0x0,-0xfd8bff8(%eax)
f01046e9:	75 19                	jne    f0104704 <trap+0xa4>
f01046eb:	68 64 86 10 f0       	push   $0xf0108664
f01046f0:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01046f5:	68 5b 01 00 00       	push   $0x15b
f01046fa:	68 3f 86 10 f0       	push   $0xf010863f
f01046ff:	e8 48 b9 ff ff       	call   f010004c <_panic>
f0104704:	83 ec 0c             	sub    $0xc,%esp
f0104707:	68 c0 33 12 f0       	push   $0xf01233c0
f010470c:	e8 b7 23 00 00       	call   f0106ac8 <spin_lock>
		lock_kernel();
		// Garbage collect if current enviroment is a zombie
		if (curenv->env_status == ENV_DYING) {
f0104711:	e8 f4 1f 00 00       	call   f010670a <cpunum>
f0104716:	6b c0 74             	imul   $0x74,%eax,%eax
f0104719:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f010471f:	83 c4 10             	add    $0x10,%esp
f0104722:	83 78 54 01          	cmpl   $0x1,0x54(%eax)
f0104726:	75 2d                	jne    f0104755 <trap+0xf5>
			env_free(curenv);
f0104728:	e8 dd 1f 00 00       	call   f010670a <cpunum>
f010472d:	83 ec 0c             	sub    $0xc,%esp
f0104730:	6b c0 74             	imul   $0x74,%eax,%eax
f0104733:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f0104739:	e8 1d f0 ff ff       	call   f010375b <env_free>
			curenv = NULL;
f010473e:	e8 c7 1f 00 00       	call   f010670a <cpunum>
f0104743:	6b c0 74             	imul   $0x74,%eax,%eax
f0104746:	c7 80 08 40 27 f0 00 	movl   $0x0,-0xfd8bff8(%eax)
f010474d:	00 00 00 
			sched_yield();
f0104750:	e8 96 03 00 00       	call   f0104aeb <sched_yield>
		}

		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		curenv->env_tf = *tf;
f0104755:	e8 b0 1f 00 00       	call   f010670a <cpunum>
f010475a:	6b c0 74             	imul   $0x74,%eax,%eax
f010475d:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f0104763:	b9 11 00 00 00       	mov    $0x11,%ecx
f0104768:	89 c7                	mov    %eax,%edi
f010476a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
f010476c:	e8 99 1f 00 00       	call   f010670a <cpunum>
f0104771:	6b c0 74             	imul   $0x74,%eax,%eax
f0104774:	8b b0 08 40 27 f0    	mov    -0xfd8bff8(%eax),%esi
	}

	// Record that tf is the last real trapframe so
	// print_trapframe can print some additional information.
	last_tf = tf;
f010477a:	89 35 60 2a 27 f0    	mov    %esi,0xf0272a60
	// LAB 3: Your code here.

	// Handle spurious interrupts
	// The hardware sometimes raises these because of noise on the
	// IRQ line or other reasons. We don't care.
	if (tf->tf_trapno == IRQ_OFFSET + IRQ_SPURIOUS) {
f0104780:	8b 46 28             	mov    0x28(%esi),%eax
f0104783:	83 f8 27             	cmp    $0x27,%eax
f0104786:	75 1d                	jne    f01047a5 <trap+0x145>
		cprintf("Spurious interrupt on irq 7\n");
f0104788:	83 ec 0c             	sub    $0xc,%esp
f010478b:	68 6b 86 10 f0       	push   $0xf010866b
f0104790:	e8 63 f4 ff ff       	call   f0103bf8 <cprintf>
		print_trapframe(tf);
f0104795:	89 34 24             	mov    %esi,(%esp)
f0104798:	e8 ab fb ff ff       	call   f0104348 <print_trapframe>
f010479d:	83 c4 10             	add    $0x10,%esp
f01047a0:	e9 b7 00 00 00       	jmp    f010485c <trap+0x1fc>
	// triggered on every CPU.
	// LAB 4: Your code here.


//=======
	switch (tf->tf_trapno) {
f01047a5:	83 f8 0e             	cmp    $0xe,%eax
f01047a8:	74 18                	je     f01047c2 <trap+0x162>
f01047aa:	83 f8 0e             	cmp    $0xe,%eax
f01047ad:	77 07                	ja     f01047b6 <trap+0x156>
f01047af:	83 f8 03             	cmp    $0x3,%eax
f01047b2:	74 1f                	je     f01047d3 <trap+0x173>
f01047b4:	eb 63                	jmp    f0104819 <trap+0x1b9>
f01047b6:	83 f8 20             	cmp    $0x20,%eax
f01047b9:	74 4f                	je     f010480a <trap+0x1aa>
f01047bb:	83 f8 30             	cmp    $0x30,%eax
f01047be:	74 29                	je     f01047e9 <trap+0x189>
f01047c0:	eb 57                	jmp    f0104819 <trap+0x1b9>
		case T_PGFLT:
			page_fault_handler(tf);
f01047c2:	83 ec 0c             	sub    $0xc,%esp
f01047c5:	56                   	push   %esi
f01047c6:	e8 02 fd ff ff       	call   f01044cd <page_fault_handler>
f01047cb:	83 c4 10             	add    $0x10,%esp
f01047ce:	e9 89 00 00 00       	jmp    f010485c <trap+0x1fc>
			break;
		case T_BRKPT:
			print_trapframe(tf);
f01047d3:	83 ec 0c             	sub    $0xc,%esp
f01047d6:	56                   	push   %esi
f01047d7:	e8 6c fb ff ff       	call   f0104348 <print_trapframe>
			monitor(tf); //invoke the kernel monitor
f01047dc:	89 34 24             	mov    %esi,(%esp)
f01047df:	e8 74 c2 ff ff       	call   f0100a58 <monitor>
f01047e4:	83 c4 10             	add    $0x10,%esp
f01047e7:	eb 73                	jmp    f010485c <trap+0x1fc>

		//case IRQ_OFFSET+IRQ_TIMER:
		//	break;
		case T_SYSCALL:
			//syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
			tf->tf_regs.reg_eax = syscall(
f01047e9:	83 ec 08             	sub    $0x8,%esp
f01047ec:	ff 76 04             	pushl  0x4(%esi)
f01047ef:	ff 36                	pushl  (%esi)
f01047f1:	ff 76 10             	pushl  0x10(%esi)
f01047f4:	ff 76 18             	pushl  0x18(%esi)
f01047f7:	ff 76 14             	pushl  0x14(%esi)
f01047fa:	ff 76 1c             	pushl  0x1c(%esi)
f01047fd:	e8 7d 03 00 00       	call   f0104b7f <syscall>
f0104802:	89 46 1c             	mov    %eax,0x1c(%esi)
f0104805:	83 c4 20             	add    $0x20,%esp
f0104808:	eb 52                	jmp    f010485c <trap+0x1fc>
            tf->tf_regs.reg_ebx,
            tf->tf_regs.reg_edi,
            tf->tf_regs.reg_esi);
            break;
         case IRQ_OFFSET + IRQ_TIMER:
         	time_tick();
f010480a:	e8 69 24 00 00       	call   f0106c78 <time_tick>
         	lapic_eoi();
f010480f:	e8 ca 20 00 00       	call   f01068de <lapic_eoi>
         	sched_yield();
f0104814:	e8 d2 02 00 00       	call   f0104aeb <sched_yield>
         	break;

		default:
			// Unexpected trap: The user process or the kernel has a bug.
			print_trapframe(tf);
f0104819:	83 ec 0c             	sub    $0xc,%esp
f010481c:	56                   	push   %esi
f010481d:	e8 26 fb ff ff       	call   f0104348 <print_trapframe>
			if (tf->tf_cs == GD_KT)
f0104822:	83 c4 10             	add    $0x10,%esp
f0104825:	66 83 7e 34 08       	cmpw   $0x8,0x34(%esi)
f010482a:	75 17                	jne    f0104843 <trap+0x1e3>
				panic("unhandled trap in kernel");
f010482c:	83 ec 04             	sub    $0x4,%esp
f010482f:	68 88 86 10 f0       	push   $0xf0108688
f0104834:	68 30 01 00 00       	push   $0x130
f0104839:	68 3f 86 10 f0       	push   $0xf010863f
f010483e:	e8 09 b8 ff ff       	call   f010004c <_panic>
			else {
				env_destroy(curenv);
f0104843:	e8 c2 1e 00 00       	call   f010670a <cpunum>
f0104848:	83 ec 0c             	sub    $0xc,%esp
f010484b:	6b c0 74             	imul   $0x74,%eax,%eax
f010484e:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f0104854:	e8 fd f0 ff ff       	call   f0103956 <env_destroy>
f0104859:	83 c4 10             	add    $0x10,%esp

//<<<<<<< HEAD
	// If we made it to this point, then no other environment was
	// scheduled, so we should return to the current environment
	// if doing so makes sense.
	if (curenv && curenv->env_status == ENV_RUNNING)
f010485c:	e8 a9 1e 00 00       	call   f010670a <cpunum>
f0104861:	6b c0 74             	imul   $0x74,%eax,%eax
f0104864:	83 b8 08 40 27 f0 00 	cmpl   $0x0,-0xfd8bff8(%eax)
f010486b:	74 2a                	je     f0104897 <trap+0x237>
f010486d:	e8 98 1e 00 00       	call   f010670a <cpunum>
f0104872:	6b c0 74             	imul   $0x74,%eax,%eax
f0104875:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f010487b:	83 78 54 03          	cmpl   $0x3,0x54(%eax)
f010487f:	75 16                	jne    f0104897 <trap+0x237>
		
		env_run(curenv);
f0104881:	e8 84 1e 00 00       	call   f010670a <cpunum>
f0104886:	83 ec 0c             	sub    $0xc,%esp
f0104889:	6b c0 74             	imul   $0x74,%eax,%eax
f010488c:	ff b0 08 40 27 f0    	pushl  -0xfd8bff8(%eax)
f0104892:	e8 99 f1 ff ff       	call   f0103a30 <env_run>
		//panic("should we get to the end of trap()???\n");
	else
		sched_yield();
f0104897:	e8 4f 02 00 00       	call   f0104aeb <sched_yield>

f010489c <handler0>:
#define TY(N) TRAPHANDLER(H(N), N)
#define TN(N) TRAPHANDLER_NOEC(H(N), N)
#define L(N) .long H(N)
#define S(N) .long 0 /* skip */

TN(0)
f010489c:	6a 00                	push   $0x0
f010489e:	6a 00                	push   $0x0
f01048a0:	e9 46 01 00 00       	jmp    f01049eb <_alltraps>
f01048a5:	90                   	nop

f01048a6 <handler1>:
TN(1)
f01048a6:	6a 00                	push   $0x0
f01048a8:	6a 01                	push   $0x1
f01048aa:	e9 3c 01 00 00       	jmp    f01049eb <_alltraps>
f01048af:	90                   	nop

f01048b0 <handler2>:
TN(2)
f01048b0:	6a 00                	push   $0x0
f01048b2:	6a 02                	push   $0x2
f01048b4:	e9 32 01 00 00       	jmp    f01049eb <_alltraps>
f01048b9:	90                   	nop

f01048ba <handler3>:
TN(3)
f01048ba:	6a 00                	push   $0x0
f01048bc:	6a 03                	push   $0x3
f01048be:	e9 28 01 00 00       	jmp    f01049eb <_alltraps>
f01048c3:	90                   	nop

f01048c4 <handler4>:
TN(4)
f01048c4:	6a 00                	push   $0x0
f01048c6:	6a 04                	push   $0x4
f01048c8:	e9 1e 01 00 00       	jmp    f01049eb <_alltraps>
f01048cd:	90                   	nop

f01048ce <handler5>:
TN(5)
f01048ce:	6a 00                	push   $0x0
f01048d0:	6a 05                	push   $0x5
f01048d2:	e9 14 01 00 00       	jmp    f01049eb <_alltraps>
f01048d7:	90                   	nop

f01048d8 <handler6>:
TN(6)
f01048d8:	6a 00                	push   $0x0
f01048da:	6a 06                	push   $0x6
f01048dc:	e9 0a 01 00 00       	jmp    f01049eb <_alltraps>
f01048e1:	90                   	nop

f01048e2 <handler7>:
TN(7)
f01048e2:	6a 00                	push   $0x0
f01048e4:	6a 07                	push   $0x7
f01048e6:	e9 00 01 00 00       	jmp    f01049eb <_alltraps>
f01048eb:	90                   	nop

f01048ec <handler8>:
TY(8)
f01048ec:	6a 08                	push   $0x8
f01048ee:	e9 f8 00 00 00       	jmp    f01049eb <_alltraps>
f01048f3:	90                   	nop

f01048f4 <handler10>:
// TN(9) /* reserved */
TY(10)
f01048f4:	6a 0a                	push   $0xa
f01048f6:	e9 f0 00 00 00       	jmp    f01049eb <_alltraps>
f01048fb:	90                   	nop

f01048fc <handler11>:
TY(11)
f01048fc:	6a 0b                	push   $0xb
f01048fe:	e9 e8 00 00 00       	jmp    f01049eb <_alltraps>
f0104903:	90                   	nop

f0104904 <handler12>:
TY(12)
f0104904:	6a 0c                	push   $0xc
f0104906:	e9 e0 00 00 00       	jmp    f01049eb <_alltraps>
f010490b:	90                   	nop

f010490c <handler13>:
TY(13)
f010490c:	6a 0d                	push   $0xd
f010490e:	e9 d8 00 00 00       	jmp    f01049eb <_alltraps>
f0104913:	90                   	nop

f0104914 <handler14>:
TY(14)
f0104914:	6a 0e                	push   $0xe
f0104916:	e9 d0 00 00 00       	jmp    f01049eb <_alltraps>
f010491b:	90                   	nop

f010491c <handler16>:
// TN(15) /* reserved */
TN(16)
f010491c:	6a 00                	push   $0x0
f010491e:	6a 10                	push   $0x10
f0104920:	e9 c6 00 00 00       	jmp    f01049eb <_alltraps>
f0104925:	90                   	nop

f0104926 <handler17>:
TY(17)
f0104926:	6a 11                	push   $0x11
f0104928:	e9 be 00 00 00       	jmp    f01049eb <_alltraps>
f010492d:	90                   	nop

f010492e <handler18>:
TN(18)
f010492e:	6a 00                	push   $0x0
f0104930:	6a 12                	push   $0x12
f0104932:	e9 b4 00 00 00       	jmp    f01049eb <_alltraps>
f0104937:	90                   	nop

f0104938 <handler19>:
TN(19)
f0104938:	6a 00                	push   $0x0
f010493a:	6a 13                	push   $0x13
f010493c:	e9 aa 00 00 00       	jmp    f01049eb <_alltraps>
f0104941:	90                   	nop

f0104942 <handler32>:

/* 16 IRQs */
TN(32)
f0104942:	6a 00                	push   $0x0
f0104944:	6a 20                	push   $0x20
f0104946:	e9 a0 00 00 00       	jmp    f01049eb <_alltraps>
f010494b:	90                   	nop

f010494c <handler33>:
TN(33)
f010494c:	6a 00                	push   $0x0
f010494e:	6a 21                	push   $0x21
f0104950:	e9 96 00 00 00       	jmp    f01049eb <_alltraps>
f0104955:	90                   	nop

f0104956 <handler34>:
TN(34)
f0104956:	6a 00                	push   $0x0
f0104958:	6a 22                	push   $0x22
f010495a:	e9 8c 00 00 00       	jmp    f01049eb <_alltraps>
f010495f:	90                   	nop

f0104960 <handler35>:
TN(35)
f0104960:	6a 00                	push   $0x0
f0104962:	6a 23                	push   $0x23
f0104964:	e9 82 00 00 00       	jmp    f01049eb <_alltraps>
f0104969:	90                   	nop

f010496a <handler36>:
TN(36)
f010496a:	6a 00                	push   $0x0
f010496c:	6a 24                	push   $0x24
f010496e:	e9 78 00 00 00       	jmp    f01049eb <_alltraps>
f0104973:	90                   	nop

f0104974 <handler37>:
TN(37)
f0104974:	6a 00                	push   $0x0
f0104976:	6a 25                	push   $0x25
f0104978:	e9 6e 00 00 00       	jmp    f01049eb <_alltraps>
f010497d:	90                   	nop

f010497e <handler38>:
TN(38)
f010497e:	6a 00                	push   $0x0
f0104980:	6a 26                	push   $0x26
f0104982:	e9 64 00 00 00       	jmp    f01049eb <_alltraps>
f0104987:	90                   	nop

f0104988 <handler39>:
TN(39)
f0104988:	6a 00                	push   $0x0
f010498a:	6a 27                	push   $0x27
f010498c:	e9 5a 00 00 00       	jmp    f01049eb <_alltraps>
f0104991:	90                   	nop

f0104992 <handler40>:
TN(40)
f0104992:	6a 00                	push   $0x0
f0104994:	6a 28                	push   $0x28
f0104996:	e9 50 00 00 00       	jmp    f01049eb <_alltraps>
f010499b:	90                   	nop

f010499c <handler41>:
TN(41)
f010499c:	6a 00                	push   $0x0
f010499e:	6a 29                	push   $0x29
f01049a0:	e9 46 00 00 00       	jmp    f01049eb <_alltraps>
f01049a5:	90                   	nop

f01049a6 <handler42>:
TN(42)
f01049a6:	6a 00                	push   $0x0
f01049a8:	6a 2a                	push   $0x2a
f01049aa:	e9 3c 00 00 00       	jmp    f01049eb <_alltraps>
f01049af:	90                   	nop

f01049b0 <handler43>:
TN(43)
f01049b0:	6a 00                	push   $0x0
f01049b2:	6a 2b                	push   $0x2b
f01049b4:	e9 32 00 00 00       	jmp    f01049eb <_alltraps>
f01049b9:	90                   	nop

f01049ba <handler44>:
TN(44)
f01049ba:	6a 00                	push   $0x0
f01049bc:	6a 2c                	push   $0x2c
f01049be:	e9 28 00 00 00       	jmp    f01049eb <_alltraps>
f01049c3:	90                   	nop

f01049c4 <handler45>:
TN(45)
f01049c4:	6a 00                	push   $0x0
f01049c6:	6a 2d                	push   $0x2d
f01049c8:	e9 1e 00 00 00       	jmp    f01049eb <_alltraps>
f01049cd:	90                   	nop

f01049ce <handler46>:
TN(46)
f01049ce:	6a 00                	push   $0x0
f01049d0:	6a 2e                	push   $0x2e
f01049d2:	e9 14 00 00 00       	jmp    f01049eb <_alltraps>
f01049d7:	90                   	nop

f01049d8 <handler47>:
TN(47)
f01049d8:	6a 00                	push   $0x0
f01049da:	6a 2f                	push   $0x2f
f01049dc:	e9 0a 00 00 00       	jmp    f01049eb <_alltraps>
f01049e1:	90                   	nop

f01049e2 <handler48>:
	
TN(48)
f01049e2:	6a 00                	push   $0x0
f01049e4:	6a 30                	push   $0x30
f01049e6:	e9 00 00 00 00       	jmp    f01049eb <_alltraps>

f01049eb <_alltraps>:
 */

.global _alltraps
_alltraps:
	// Build struct Trapframe
	pushl %ds
f01049eb:	1e                   	push   %ds
	pushl %es
f01049ec:	06                   	push   %es
	pushal
f01049ed:	60                   	pusha  

	movl $GD_KD, %eax
f01049ee:	b8 10 00 00 00       	mov    $0x10,%eax
	movw %ax, %ds
f01049f3:	8e d8                	mov    %eax,%ds
	movw %ax, %es
f01049f5:	8e c0                	mov    %eax,%es

	pushl %esp /* struct Trapframe * as argument */
f01049f7:	54                   	push   %esp
	call trap
f01049f8:	e8 63 fc ff ff       	call   f0104660 <trap>

f01049fd <sched_halt>:
// Halt this CPU when there is nothing to do. Wait until the
// timer interrupt wakes it up. This function never returns.
//
void
sched_halt(void)
{
f01049fd:	55                   	push   %ebp
f01049fe:	89 e5                	mov    %esp,%ebp
f0104a00:	83 ec 08             	sub    $0x8,%esp
f0104a03:	a1 44 22 27 f0       	mov    0xf0272244,%eax
f0104a08:	8d 50 54             	lea    0x54(%eax),%edx
	int i;

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
f0104a0b:	b9 00 00 00 00       	mov    $0x0,%ecx
		if ((envs[i].env_status == ENV_RUNNABLE ||
f0104a10:	8b 02                	mov    (%edx),%eax
f0104a12:	48                   	dec    %eax
f0104a13:	83 f8 02             	cmp    $0x2,%eax
f0104a16:	76 0e                	jbe    f0104a26 <sched_halt+0x29>
{
	int i;

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
f0104a18:	41                   	inc    %ecx
f0104a19:	83 c2 7c             	add    $0x7c,%edx
f0104a1c:	81 f9 00 04 00 00    	cmp    $0x400,%ecx
f0104a22:	75 ec                	jne    f0104a10 <sched_halt+0x13>
f0104a24:	eb 08                	jmp    f0104a2e <sched_halt+0x31>
		if ((envs[i].env_status == ENV_RUNNABLE ||
		     envs[i].env_status == ENV_RUNNING ||
		     envs[i].env_status == ENV_DYING))
			break;
	}
	if (i == NENV) {
f0104a26:	81 f9 00 04 00 00    	cmp    $0x400,%ecx
f0104a2c:	75 1f                	jne    f0104a4d <sched_halt+0x50>
		cprintf("No runnable environments in the system!\n");
f0104a2e:	83 ec 0c             	sub    $0xc,%esp
f0104a31:	68 70 88 10 f0       	push   $0xf0108870
f0104a36:	e8 bd f1 ff ff       	call   f0103bf8 <cprintf>
f0104a3b:	83 c4 10             	add    $0x10,%esp
		while (1)
			monitor(NULL);
f0104a3e:	83 ec 0c             	sub    $0xc,%esp
f0104a41:	6a 00                	push   $0x0
f0104a43:	e8 10 c0 ff ff       	call   f0100a58 <monitor>
f0104a48:	83 c4 10             	add    $0x10,%esp
f0104a4b:	eb f1                	jmp    f0104a3e <sched_halt+0x41>
	}

	// Mark that no environment is running on this CPU
	curenv = NULL;
f0104a4d:	e8 b8 1c 00 00       	call   f010670a <cpunum>
f0104a52:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104a55:	01 c2                	add    %eax,%edx
f0104a57:	01 d2                	add    %edx,%edx
f0104a59:	01 c2                	add    %eax,%edx
f0104a5b:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104a5e:	c7 04 85 08 40 27 f0 	movl   $0x0,-0xfd8bff8(,%eax,4)
f0104a65:	00 00 00 00 
	lcr3(PADDR(kern_pgdir));
f0104a69:	a1 28 34 27 f0       	mov    0xf0273428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0104a6e:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0104a73:	77 12                	ja     f0104a87 <sched_halt+0x8a>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0104a75:	50                   	push   %eax
f0104a76:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0104a7b:	6a 55                	push   $0x55
f0104a7d:	68 99 88 10 f0       	push   $0xf0108899
f0104a82:	e8 c5 b5 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0104a87:	05 00 00 00 10       	add    $0x10000000,%eax
f0104a8c:	0f 22 d8             	mov    %eax,%cr3

	// Mark that this CPU is in the HALT state, so that when
	// timer interupts come in, we know we should re-acquire the
	// big kernel lock
	xchg(&thiscpu->cpu_status, CPU_HALTED);
f0104a8f:	e8 76 1c 00 00       	call   f010670a <cpunum>
f0104a94:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104a97:	01 c2                	add    %eax,%edx
f0104a99:	01 d2                	add    %edx,%edx
f0104a9b:	01 c2                	add    %eax,%edx
f0104a9d:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104aa0:	8d 14 85 00 40 27 f0 	lea    -0xfd8c000(,%eax,4),%edx
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0104aa7:	b8 02 00 00 00       	mov    $0x2,%eax
f0104aac:	f0 87 42 04          	lock xchg %eax,0x4(%edx)
}

static inline void
unlock_kernel(void)
{
	spin_unlock(&kernel_lock);
f0104ab0:	83 ec 0c             	sub    $0xc,%esp
f0104ab3:	68 c0 33 12 f0       	push   $0xf01233c0
f0104ab8:	e8 b8 20 00 00       	call   f0106b75 <spin_unlock>

	// Normally we wouldn't need to do this, but QEMU only runs
	// one CPU at a time and has a long time-slice.  Without the
	// pause, this CPU is likely to reacquire the lock before
	// another CPU has even been given a chance to acquire it.
	asm volatile("pause");
f0104abd:	f3 90                	pause  
		"pushl $0\n"
		"sti\n"
		"1:\n"
		"hlt\n"
		"jmp 1b\n"
	: : "a" (thiscpu->cpu_ts.ts_esp0));
f0104abf:	e8 46 1c 00 00       	call   f010670a <cpunum>
f0104ac4:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104ac7:	01 c2                	add    %eax,%edx
f0104ac9:	01 d2                	add    %edx,%edx
f0104acb:	01 c2                	add    %eax,%edx
f0104acd:	8d 04 90             	lea    (%eax,%edx,4),%eax

	// Release the big kernel lock as if we were "leaving" the kernel
	unlock_kernel();

	// Reset stack pointer, enable interrupts and then halt.
	asm volatile (
f0104ad0:	8b 04 85 10 40 27 f0 	mov    -0xfd8bff0(,%eax,4),%eax
f0104ad7:	bd 00 00 00 00       	mov    $0x0,%ebp
f0104adc:	89 c4                	mov    %eax,%esp
f0104ade:	6a 00                	push   $0x0
f0104ae0:	6a 00                	push   $0x0
f0104ae2:	fb                   	sti    
f0104ae3:	f4                   	hlt    
f0104ae4:	eb fd                	jmp    f0104ae3 <sched_halt+0xe6>
		"sti\n"
		"1:\n"
		"hlt\n"
		"jmp 1b\n"
	: : "a" (thiscpu->cpu_ts.ts_esp0));
}
f0104ae6:	83 c4 10             	add    $0x10,%esp
f0104ae9:	c9                   	leave  
f0104aea:	c3                   	ret    

f0104aeb <sched_yield>:
void sched_halt(void);

// Choose a user environment to run and run it.
void
sched_yield(void)
{
f0104aeb:	55                   	push   %ebp
f0104aec:	89 e5                	mov    %esp,%ebp
f0104aee:	57                   	push   %edi
f0104aef:	56                   	push   %esi
f0104af0:	53                   	push   %ebx
f0104af1:	83 ec 0c             	sub    $0xc,%esp
	// no runnable environments, simply drop through to the code
	// below to halt the cpu.

	// LAB 4: Your code here.
	// Find the environment this CPU was last running.
    struct Env *curr = thiscpu->cpu_env;
f0104af4:	e8 11 1c 00 00       	call   f010670a <cpunum>
f0104af9:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104afc:	01 c2                	add    %eax,%edx
f0104afe:	01 d2                	add    %edx,%edx
f0104b00:	01 c2                	add    %eax,%edx
f0104b02:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104b05:	8b 34 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%esi
    	indx = ENVX(curr->env_id);
    }else{
    	indx = 0;
    }*/

    indx = curr ? ENVX(curr->env_id) % NENV : 0; //if there is no environment running, starting from index 0
f0104b0c:	85 f6                	test   %esi,%esi
f0104b0e:	74 0b                	je     f0104b1b <sched_yield+0x30>
f0104b10:	8b 7e 48             	mov    0x48(%esi),%edi
f0104b13:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
f0104b19:	eb 05                	jmp    f0104b20 <sched_yield+0x35>
f0104b1b:	bf 00 00 00 00       	mov    $0x0,%edi
	//cprintf("indx is %d\n", indx);
	for (i = 0; i < NENV; i++) {
        indx = (indx + 1) % NENV;
        
        if (envs[indx].env_status == ENV_RUNNABLE){
f0104b20:	8b 1d 44 22 27 f0    	mov    0xf0272244,%ebx
f0104b26:	b9 00 04 00 00       	mov    $0x400,%ecx
    }*/

    indx = curr ? ENVX(curr->env_id) % NENV : 0; //if there is no environment running, starting from index 0
	//cprintf("indx is %d\n", indx);
	for (i = 0; i < NENV; i++) {
        indx = (indx + 1) % NENV;
f0104b2b:	8d 47 01             	lea    0x1(%edi),%eax
f0104b2e:	25 ff 03 00 80       	and    $0x800003ff,%eax
f0104b33:	79 07                	jns    f0104b3c <sched_yield+0x51>
f0104b35:	48                   	dec    %eax
f0104b36:	0d 00 fc ff ff       	or     $0xfffffc00,%eax
f0104b3b:	40                   	inc    %eax
f0104b3c:	89 c7                	mov    %eax,%edi
        
        if (envs[indx].env_status == ENV_RUNNABLE){
f0104b3e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104b45:	c1 e0 07             	shl    $0x7,%eax
f0104b48:	29 d0                	sub    %edx,%eax
f0104b4a:	8d 14 03             	lea    (%ebx,%eax,1),%edx
f0104b4d:	83 7a 54 02          	cmpl   $0x2,0x54(%edx)
f0104b51:	75 09                	jne    f0104b5c <sched_yield+0x71>
        	//cprintf("About to run env: %d\n", envs[indx].env_id);
            env_run(&envs[indx]);
f0104b53:	83 ec 0c             	sub    $0xc,%esp
f0104b56:	52                   	push   %edx
f0104b57:	e8 d4 ee ff ff       	call   f0103a30 <env_run>
    	indx = 0;
    }*/

    indx = curr ? ENVX(curr->env_id) % NENV : 0; //if there is no environment running, starting from index 0
	//cprintf("indx is %d\n", indx);
	for (i = 0; i < NENV; i++) {
f0104b5c:	49                   	dec    %ecx
f0104b5d:	75 cc                	jne    f0104b2b <sched_yield+0x40>
            env_run(&envs[indx]);

        }
	}

    if (curr && curr->env_status == ENV_RUNNING) {
f0104b5f:	85 f6                	test   %esi,%esi
f0104b61:	74 0f                	je     f0104b72 <sched_yield+0x87>
f0104b63:	83 7e 54 03          	cmpl   $0x3,0x54(%esi)
f0104b67:	75 09                	jne    f0104b72 <sched_yield+0x87>
        // If not found and current environment is running, then continue running.
        env_run(curr);
f0104b69:	83 ec 0c             	sub    $0xc,%esp
f0104b6c:	56                   	push   %esi
f0104b6d:	e8 be ee ff ff       	call   f0103a30 <env_run>
    }
	// sched_halt never returns
	sched_halt();
f0104b72:	e8 86 fe ff ff       	call   f01049fd <sched_halt>
}
f0104b77:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104b7a:	5b                   	pop    %ebx
f0104b7b:	5e                   	pop    %esi
f0104b7c:	5f                   	pop    %edi
f0104b7d:	5d                   	pop    %ebp
f0104b7e:	c3                   	ret    

f0104b7f <syscall>:
}

// Dispatches to the correct kernel function, passing the arguments.
int32_t
syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
f0104b7f:	55                   	push   %ebp
f0104b80:	89 e5                	mov    %esp,%ebp
f0104b82:	57                   	push   %edi
f0104b83:	56                   	push   %esi
f0104b84:	53                   	push   %ebx
f0104b85:	83 ec 1c             	sub    $0x1c,%esp
f0104b88:	8b 45 08             	mov    0x8(%ebp),%eax
	// Return any appropriate return value.
	// LAB 3: Your code here.

	//panic("syscall not implemented");

	switch (syscallno) {
f0104b8b:	83 f8 0d             	cmp    $0xd,%eax
f0104b8e:	0f 87 b5 07 00 00    	ja     f0105349 <syscall+0x7ca>
f0104b94:	ff 24 85 88 8b 10 f0 	jmp    *-0xfef7478(,%eax,4)
	// Check that the user has permission to read memory [s, s+len).
	// Destroy the environment if not.

	// LAB 3: Your code here.
	//user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
	user_mem_assert(curenv, (void*)s, len, 0);
f0104b9b:	e8 6a 1b 00 00       	call   f010670a <cpunum>
f0104ba0:	6a 00                	push   $0x0
f0104ba2:	ff 75 10             	pushl  0x10(%ebp)
f0104ba5:	ff 75 0c             	pushl  0xc(%ebp)
f0104ba8:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104bab:	01 c2                	add    %eax,%edx
f0104bad:	01 d2                	add    %edx,%edx
f0104baf:	01 c2                	add    %eax,%edx
f0104bb1:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104bb4:	ff 34 85 08 40 27 f0 	pushl  -0xfd8bff8(,%eax,4)
f0104bbb:	e8 4b e6 ff ff       	call   f010320b <user_mem_assert>
	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
f0104bc0:	83 c4 0c             	add    $0xc,%esp
f0104bc3:	ff 75 0c             	pushl  0xc(%ebp)
f0104bc6:	ff 75 10             	pushl  0x10(%ebp)
f0104bc9:	68 a6 88 10 f0       	push   $0xf01088a6
f0104bce:	e8 25 f0 ff ff       	call   f0103bf8 <cprintf>
f0104bd3:	83 c4 10             	add    $0x10,%esp
	//panic("syscall not implemented");

	switch (syscallno) {
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;
f0104bd6:	bb 00 00 00 00       	mov    $0x0,%ebx
f0104bdb:	e9 80 07 00 00       	jmp    f0105360 <syscall+0x7e1>
// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
static int
sys_cgetc(void)
{
	return cons_getc();
f0104be0:	e8 c0 ba ff ff       	call   f01006a5 <cons_getc>
f0104be5:	89 c3                	mov    %eax,%ebx
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;

		case SYS_cgetc:
			return sys_cgetc(); //Read a character from the system console without blocking.
f0104be7:	e9 74 07 00 00       	jmp    f0105360 <syscall+0x7e1>
sys_env_destroy(envid_t envid_In)
{
	int r;
    envid_t envid = envid_In;
	struct Env *e;
    cprintf("envid in sys_env_destroy() is:%d\n", envid);
f0104bec:	83 ec 08             	sub    $0x8,%esp
f0104bef:	ff 75 0c             	pushl  0xc(%ebp)
f0104bf2:	68 b8 8a 10 f0       	push   $0xf0108ab8
f0104bf7:	e8 fc ef ff ff       	call   f0103bf8 <cprintf>
	if ((r = envid2env(envid, &e, 1)) < 0)
f0104bfc:	83 c4 0c             	add    $0xc,%esp
f0104bff:	6a 01                	push   $0x1
f0104c01:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104c04:	50                   	push   %eax
f0104c05:	ff 75 0c             	pushl  0xc(%ebp)
f0104c08:	e8 ec e6 ff ff       	call   f01032f9 <envid2env>
f0104c0d:	83 c4 10             	add    $0x10,%esp
f0104c10:	85 c0                	test   %eax,%eax
f0104c12:	0f 88 91 00 00 00    	js     f0104ca9 <syscall+0x12a>
		return r;
	if (e == curenv)
f0104c18:	e8 ed 1a 00 00       	call   f010670a <cpunum>
f0104c1d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f0104c20:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104c23:	01 c2                	add    %eax,%edx
f0104c25:	01 d2                	add    %edx,%edx
f0104c27:	01 c2                	add    %eax,%edx
f0104c29:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104c2c:	39 0c 85 08 40 27 f0 	cmp    %ecx,-0xfd8bff8(,%eax,4)
f0104c33:	75 2d                	jne    f0104c62 <syscall+0xe3>
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
f0104c35:	e8 d0 1a 00 00       	call   f010670a <cpunum>
f0104c3a:	83 ec 08             	sub    $0x8,%esp
f0104c3d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104c40:	01 c2                	add    %eax,%edx
f0104c42:	01 d2                	add    %edx,%edx
f0104c44:	01 c2                	add    %eax,%edx
f0104c46:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104c49:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0104c50:	ff 70 48             	pushl  0x48(%eax)
f0104c53:	68 ab 88 10 f0       	push   $0xf01088ab
f0104c58:	e8 9b ef ff ff       	call   f0103bf8 <cprintf>
f0104c5d:	83 c4 10             	add    $0x10,%esp
f0104c60:	eb 2f                	jmp    f0104c91 <syscall+0x112>
	else
		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
f0104c62:	8b 59 48             	mov    0x48(%ecx),%ebx
f0104c65:	e8 a0 1a 00 00       	call   f010670a <cpunum>
f0104c6a:	83 ec 04             	sub    $0x4,%esp
f0104c6d:	53                   	push   %ebx
f0104c6e:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104c71:	01 c2                	add    %eax,%edx
f0104c73:	01 d2                	add    %edx,%edx
f0104c75:	01 c2                	add    %eax,%edx
f0104c77:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104c7a:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0104c81:	ff 70 48             	pushl  0x48(%eax)
f0104c84:	68 c6 88 10 f0       	push   $0xf01088c6
f0104c89:	e8 6a ef ff ff       	call   f0103bf8 <cprintf>
f0104c8e:	83 c4 10             	add    $0x10,%esp
	env_destroy(e);
f0104c91:	83 ec 0c             	sub    $0xc,%esp
f0104c94:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104c97:	e8 ba ec ff ff       	call   f0103956 <env_destroy>
f0104c9c:	83 c4 10             	add    $0x10,%esp
	return 0;
f0104c9f:	bb 00 00 00 00       	mov    $0x0,%ebx
f0104ca4:	e9 b7 06 00 00       	jmp    f0105360 <syscall+0x7e1>
	int r;
    envid_t envid = envid_In;
	struct Env *e;
    cprintf("envid in sys_env_destroy() is:%d\n", envid);
	if ((r = envid2env(envid, &e, 1)) < 0)
		return r;
f0104ca9:	89 c3                	mov    %eax,%ebx
		case SYS_cgetc:
			return sys_cgetc(); //Read a character from the system console without blocking.

		case SYS_env_destroy:
			//return sys_env_destroy(a2); //Destroy a given environment (possibly the currently running environment).
            return sys_env_destroy(a1);
f0104cab:	e9 b0 06 00 00       	jmp    f0105360 <syscall+0x7e1>

// Returns the current environment's envid.
static envid_t
sys_getenvid(void)
{
	return curenv->env_id;
f0104cb0:	e8 55 1a 00 00       	call   f010670a <cpunum>
f0104cb5:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104cb8:	01 c2                	add    %eax,%edx
f0104cba:	01 d2                	add    %edx,%edx
f0104cbc:	01 c2                	add    %eax,%edx
f0104cbe:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104cc1:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0104cc8:	8b 58 48             	mov    0x48(%eax),%ebx

		case SYS_env_destroy:
			//return sys_env_destroy(a2); //Destroy a given environment (possibly the currently running environment).
            return sys_env_destroy(a1);
		case SYS_getenvid:
			return sys_getenvid(); //get the current environment's envid.
f0104ccb:	e9 90 06 00 00       	jmp    f0105360 <syscall+0x7e1>
// Deschedule current environment and pick a different one to run.
static void
sys_yield(void)
{   
    //cprintf("timer interrupted!\n");
	sched_yield();
f0104cd0:	e8 16 fe ff ff       	call   f0104aeb <sched_yield>
	// LAB 4: Your code here.
	//cprintf("sys_exofork() starts\n\n");
	struct Env *env;
    //cprintf("In sys_exofork(), the envid to env_alloc() is: %d\n\n\n", thiscpu->cpu_env->env_id);
    //env_alloc(struct Env **newenv_store, envid_t parent_id)
    int result = env_alloc(&env, thiscpu->cpu_env->env_id);
f0104cd5:	e8 30 1a 00 00       	call   f010670a <cpunum>
f0104cda:	83 ec 08             	sub    $0x8,%esp
f0104cdd:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104ce0:	01 c2                	add    %eax,%edx
f0104ce2:	01 d2                	add    %edx,%edx
f0104ce4:	01 c2                	add    %eax,%edx
f0104ce6:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104ce9:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0104cf0:	ff 70 48             	pushl  0x48(%eax)
f0104cf3:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104cf6:	50                   	push   %eax
f0104cf7:	e8 32 e7 ff ff       	call   f010342e <env_alloc>
    //cprintf("In sys_exofork(), result from env_alloc() is: %d\n\n\n", result);
    if (result) {
f0104cfc:	83 c4 10             	add    $0x10,%esp
f0104cff:	85 c0                	test   %eax,%eax
f0104d01:	75 3d                	jne    f0104d40 <syscall+0x1c1>
        //cprintf("sys_exofork: no free env");
        return -E_NO_FREE_ENV;
    }

    env->env_status = ENV_NOT_RUNNABLE;
f0104d03:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f0104d06:	c7 43 54 04 00 00 00 	movl   $0x4,0x54(%ebx)
    env->env_tf = thiscpu->cpu_env->env_tf;
f0104d0d:	e8 f8 19 00 00       	call   f010670a <cpunum>
f0104d12:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104d15:	01 c2                	add    %eax,%edx
f0104d17:	01 d2                	add    %edx,%edx
f0104d19:	01 c2                	add    %eax,%edx
f0104d1b:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104d1e:	8b 34 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%esi
f0104d25:	b9 11 00 00 00       	mov    $0x11,%ecx
f0104d2a:	89 df                	mov    %ebx,%edi
f0104d2c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
    
    // Make the new environment return zero.
    env->env_tf.tf_regs.reg_eax = 0;
f0104d2e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104d31:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

    return env->env_id;
f0104d38:	8b 58 48             	mov    0x48(%eax),%ebx
f0104d3b:	e9 20 06 00 00       	jmp    f0105360 <syscall+0x7e1>
    //env_alloc(struct Env **newenv_store, envid_t parent_id)
    int result = env_alloc(&env, thiscpu->cpu_env->env_id);
    //cprintf("In sys_exofork(), result from env_alloc() is: %d\n\n\n", result);
    if (result) {
        //cprintf("sys_exofork: no free env");
        return -E_NO_FREE_ENV;
f0104d40:	bb fb ff ff ff       	mov    $0xfffffffb,%ebx

		case SYS_yield:
        	sys_yield();
        	return 0;
        case SYS_exofork:
        	return sys_exofork();
f0104d45:	e9 16 06 00 00       	jmp    f0105360 <syscall+0x7e1>
	// check whether the current environment has permission to set
	// envid's status.

	// LAB 4: Your code here.
    // check status.
    if (status != ENV_RUNNABLE &&
f0104d4a:	83 7d 10 02          	cmpl   $0x2,0x10(%ebp)
f0104d4e:	74 20                	je     f0104d70 <syscall+0x1f1>
f0104d50:	83 7d 10 04          	cmpl   $0x4,0x10(%ebp)
f0104d54:	74 1a                	je     f0104d70 <syscall+0x1f1>
        status != ENV_NOT_RUNNABLE) {
        cprintf("sys_env_set_status: bad status");
f0104d56:	83 ec 0c             	sub    $0xc,%esp
f0104d59:	68 dc 8a 10 f0       	push   $0xf0108adc
f0104d5e:	e8 95 ee ff ff       	call   f0103bf8 <cprintf>
f0104d63:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104d66:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104d6b:	e9 f0 05 00 00       	jmp    f0105360 <syscall+0x7e1>
    }
    
    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f0104d70:	83 ec 04             	sub    $0x4,%esp
f0104d73:	6a 01                	push   $0x1
f0104d75:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104d78:	50                   	push   %eax
f0104d79:	ff 75 0c             	pushl  0xc(%ebp)
f0104d7c:	e8 78 e5 ff ff       	call   f01032f9 <envid2env>
f0104d81:	89 c3                	mov    %eax,%ebx
f0104d83:	83 c4 10             	add    $0x10,%esp
f0104d86:	85 c0                	test   %eax,%eax
f0104d88:	74 1a                	je     f0104da4 <syscall+0x225>
        cprintf("sys_env_set_status: bad env");
f0104d8a:	83 ec 0c             	sub    $0xc,%esp
f0104d8d:	68 de 88 10 f0       	push   $0xf01088de
f0104d92:	e8 61 ee ff ff       	call   f0103bf8 <cprintf>
f0104d97:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104d9a:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104d9f:	e9 bc 05 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // change status.
    env->env_status = status;
f0104da4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104da7:	8b 4d 10             	mov    0x10(%ebp),%ecx
f0104daa:	89 48 54             	mov    %ecx,0x54(%eax)
        	sys_yield();
        	return 0;
        case SYS_exofork:
        	return sys_exofork();
    	case SYS_env_set_status:
        	return sys_env_set_status(a1, a2);
f0104dad:	e9 ae 05 00 00       	jmp    f0105360 <syscall+0x7e1>

	// LAB 4: Your code here.
#define PGMASK 0xfff

    // check address.
    if ((uint32_t)va >= UTOP || (uint32_t)va & PGMASK) {
f0104db2:	81 7d 10 ff ff bf ee 	cmpl   $0xeebfffff,0x10(%ebp)
f0104db9:	77 09                	ja     f0104dc4 <syscall+0x245>
f0104dbb:	f7 45 10 ff 0f 00 00 	testl  $0xfff,0x10(%ebp)
f0104dc2:	74 1a                	je     f0104dde <syscall+0x25f>
        cprintf("sys_page_alloc: bad va");
f0104dc4:	83 ec 0c             	sub    $0xc,%esp
f0104dc7:	68 fa 88 10 f0       	push   $0xf01088fa
f0104dcc:	e8 27 ee ff ff       	call   f0103bf8 <cprintf>
f0104dd1:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104dd4:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104dd9:	e9 82 05 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // check permission.
    if (!(perm & PTE_U) || !(perm & PTE_P) || (perm & ~PTE_SYSCALL)) {
f0104dde:	8b 45 14             	mov    0x14(%ebp),%eax
f0104de1:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f0104de6:	83 f8 05             	cmp    $0x5,%eax
f0104de9:	74 1a                	je     f0104e05 <syscall+0x286>
        cprintf("sys_page_alloc: bad perm");
f0104deb:	83 ec 0c             	sub    $0xc,%esp
f0104dee:	68 11 89 10 f0       	push   $0xf0108911
f0104df3:	e8 00 ee ff ff       	call   f0103bf8 <cprintf>
f0104df8:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104dfb:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104e00:	e9 5b 05 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1) < 0) {
f0104e05:	83 ec 04             	sub    $0x4,%esp
f0104e08:	6a 01                	push   $0x1
f0104e0a:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104e0d:	50                   	push   %eax
f0104e0e:	ff 75 0c             	pushl  0xc(%ebp)
f0104e11:	e8 e3 e4 ff ff       	call   f01032f9 <envid2env>
f0104e16:	83 c4 10             	add    $0x10,%esp
f0104e19:	85 c0                	test   %eax,%eax
f0104e1b:	79 1a                	jns    f0104e37 <syscall+0x2b8>
        cprintf("sys_page_alloc: bad env");
f0104e1d:	83 ec 0c             	sub    $0xc,%esp
f0104e20:	68 2a 89 10 f0       	push   $0xf010892a
f0104e25:	e8 ce ed ff ff       	call   f0103bf8 <cprintf>
f0104e2a:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104e2d:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104e32:	e9 29 05 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    struct PageInfo *page;
    page = page_alloc(ALLOC_ZERO);
f0104e37:	83 ec 0c             	sub    $0xc,%esp
f0104e3a:	6a 01                	push   $0x1
f0104e3c:	e8 c8 c3 ff ff       	call   f0101209 <page_alloc>
f0104e41:	89 c6                	mov    %eax,%esi
    if (!page) {
f0104e43:	83 c4 10             	add    $0x10,%esp
f0104e46:	85 c0                	test   %eax,%eax
f0104e48:	75 1a                	jne    f0104e64 <syscall+0x2e5>
        cprintf("sys_page_alloc: no mem");
f0104e4a:	83 ec 0c             	sub    $0xc,%esp
f0104e4d:	68 42 89 10 f0       	push   $0xf0108942
f0104e52:	e8 a1 ed ff ff       	call   f0103bf8 <cprintf>
f0104e57:	83 c4 10             	add    $0x10,%esp
        return -E_NO_MEM;
f0104e5a:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
f0104e5f:	e9 fc 04 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // Side effect is within page_insert.
    if (page_insert(env->env_pgdir, page, va, perm)) {
f0104e64:	ff 75 14             	pushl  0x14(%ebp)
f0104e67:	ff 75 10             	pushl  0x10(%ebp)
f0104e6a:	50                   	push   %eax
f0104e6b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104e6e:	ff 70 60             	pushl  0x60(%eax)
f0104e71:	e8 0f c8 ff ff       	call   f0101685 <page_insert>
f0104e76:	89 c3                	mov    %eax,%ebx
f0104e78:	83 c4 10             	add    $0x10,%esp
f0104e7b:	85 c0                	test   %eax,%eax
f0104e7d:	0f 84 dd 04 00 00    	je     f0105360 <syscall+0x7e1>
        cprintf("sys_page_alloc: no mem");
f0104e83:	83 ec 0c             	sub    $0xc,%esp
f0104e86:	68 42 89 10 f0       	push   $0xf0108942
f0104e8b:	e8 68 ed ff ff       	call   f0103bf8 <cprintf>
        page_free(page);
f0104e90:	89 34 24             	mov    %esi,(%esp)
f0104e93:	e8 e7 c3 ff ff       	call   f010127f <page_free>
f0104e98:	83 c4 10             	add    $0x10,%esp
        return -E_NO_MEM;
f0104e9b:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
        case SYS_exofork:
        	return sys_exofork();
    	case SYS_env_set_status:
        	return sys_env_set_status(a1, a2);
    	case SYS_page_alloc:
        	return sys_page_alloc(a1, (void *)a2, a3);
f0104ea0:	e9 bb 04 00 00       	jmp    f0105360 <syscall+0x7e1>
	//   check the current permissions on the page.

	// LAB 4: Your code here.
    
    // check address.
    if ((uint32_t)srcva >= UTOP ||
f0104ea5:	81 7d 10 ff ff bf ee 	cmpl   $0xeebfffff,0x10(%ebp)
f0104eac:	77 1b                	ja     f0104ec9 <syscall+0x34a>
f0104eae:	f7 45 10 ff 0f 00 00 	testl  $0xfff,0x10(%ebp)
f0104eb5:	75 12                	jne    f0104ec9 <syscall+0x34a>
        ((uint32_t)srcva & PGMASK) ||
f0104eb7:	81 7d 18 ff ff bf ee 	cmpl   $0xeebfffff,0x18(%ebp)
f0104ebe:	77 09                	ja     f0104ec9 <syscall+0x34a>
        (uint32_t)dstva >= UTOP ||
f0104ec0:	f7 45 18 ff 0f 00 00 	testl  $0xfff,0x18(%ebp)
f0104ec7:	74 1a                	je     f0104ee3 <syscall+0x364>
        ((uint32_t)dstva & PGMASK)) {
        cprintf("sys_page_map: bad va");
f0104ec9:	83 ec 0c             	sub    $0xc,%esp
f0104ecc:	68 59 89 10 f0       	push   $0xf0108959
f0104ed1:	e8 22 ed ff ff       	call   f0103bf8 <cprintf>
f0104ed6:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104ed9:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104ede:	e9 7d 04 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // check permission.
    if (!(perm & PTE_U) || !(perm & PTE_P) || (perm & ~PTE_SYSCALL)) {
f0104ee3:	8b 45 1c             	mov    0x1c(%ebp),%eax
f0104ee6:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f0104eeb:	83 f8 05             	cmp    $0x5,%eax
f0104eee:	74 1a                	je     f0104f0a <syscall+0x38b>
        cprintf("sys_page_map: bad perm");
f0104ef0:	83 ec 0c             	sub    $0xc,%esp
f0104ef3:	68 6e 89 10 f0       	push   $0xf010896e
f0104ef8:	e8 fb ec ff ff       	call   f0103bf8 <cprintf>
f0104efd:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104f00:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104f05:	e9 56 04 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // find environments.
    struct Env *srcenv, *dstenv;
    if (envid2env(srcenvid, &srcenv, 1) ||
f0104f0a:	83 ec 04             	sub    $0x4,%esp
f0104f0d:	6a 01                	push   $0x1
f0104f0f:	8d 45 dc             	lea    -0x24(%ebp),%eax
f0104f12:	50                   	push   %eax
f0104f13:	ff 75 0c             	pushl  0xc(%ebp)
f0104f16:	e8 de e3 ff ff       	call   f01032f9 <envid2env>
f0104f1b:	83 c4 10             	add    $0x10,%esp
f0104f1e:	85 c0                	test   %eax,%eax
f0104f20:	75 18                	jne    f0104f3a <syscall+0x3bb>
        envid2env(dstenvid, &dstenv, 1)) {
f0104f22:	83 ec 04             	sub    $0x4,%esp
f0104f25:	6a 01                	push   $0x1
f0104f27:	8d 45 e0             	lea    -0x20(%ebp),%eax
f0104f2a:	50                   	push   %eax
f0104f2b:	ff 75 14             	pushl  0x14(%ebp)
f0104f2e:	e8 c6 e3 ff ff       	call   f01032f9 <envid2env>
        return -E_INVAL;
    }

    // find environments.
    struct Env *srcenv, *dstenv;
    if (envid2env(srcenvid, &srcenv, 1) ||
f0104f33:	83 c4 10             	add    $0x10,%esp
f0104f36:	85 c0                	test   %eax,%eax
f0104f38:	74 1a                	je     f0104f54 <syscall+0x3d5>
        envid2env(dstenvid, &dstenv, 1)) {
        cprintf("sys_page_map: bad env");
f0104f3a:	83 ec 0c             	sub    $0xc,%esp
f0104f3d:	68 85 89 10 f0       	push   $0xf0108985
f0104f42:	e8 b1 ec ff ff       	call   f0103bf8 <cprintf>
f0104f47:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104f4a:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104f4f:	e9 0c 04 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // is source mapped?
    pte_t *srcpte;
    struct PageInfo *page = page_lookup(srcenv->env_pgdir, srcva, &srcpte);
f0104f54:	83 ec 04             	sub    $0x4,%esp
f0104f57:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104f5a:	50                   	push   %eax
f0104f5b:	ff 75 10             	pushl  0x10(%ebp)
f0104f5e:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0104f61:	ff 70 60             	pushl  0x60(%eax)
f0104f64:	e8 57 c5 ff ff       	call   f01014c0 <page_lookup>
    if (!page) {
f0104f69:	83 c4 10             	add    $0x10,%esp
f0104f6c:	85 c0                	test   %eax,%eax
f0104f6e:	75 1a                	jne    f0104f8a <syscall+0x40b>
        cprintf("sys_page_map: src not mapped");
f0104f70:	83 ec 0c             	sub    $0xc,%esp
f0104f73:	68 9b 89 10 f0       	push   $0xf010899b
f0104f78:	e8 7b ec ff ff       	call   f0103bf8 <cprintf>
f0104f7d:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104f80:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104f85:	e9 d6 03 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // is source read-only but dest writable?
    if ((perm & PTE_W) && !(*srcpte & PTE_W)) {
f0104f8a:	f6 45 1c 02          	testb  $0x2,0x1c(%ebp)
f0104f8e:	74 22                	je     f0104fb2 <syscall+0x433>
f0104f90:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0104f93:	f6 02 02             	testb  $0x2,(%edx)
f0104f96:	75 1a                	jne    f0104fb2 <syscall+0x433>
        cprintf("sys_page_map: src ro but dst w");
f0104f98:	83 ec 0c             	sub    $0xc,%esp
f0104f9b:	68 fc 8a 10 f0       	push   $0xf0108afc
f0104fa0:	e8 53 ec ff ff       	call   f0103bf8 <cprintf>
f0104fa5:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104fa8:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104fad:	e9 ae 03 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // try mapping
    if (page_insert(dstenv->env_pgdir, page, dstva, perm)) {
f0104fb2:	ff 75 1c             	pushl  0x1c(%ebp)
f0104fb5:	ff 75 18             	pushl  0x18(%ebp)
f0104fb8:	50                   	push   %eax
f0104fb9:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0104fbc:	ff 70 60             	pushl  0x60(%eax)
f0104fbf:	e8 c1 c6 ff ff       	call   f0101685 <page_insert>
f0104fc4:	89 c3                	mov    %eax,%ebx
f0104fc6:	83 c4 10             	add    $0x10,%esp
f0104fc9:	85 c0                	test   %eax,%eax
f0104fcb:	0f 84 8f 03 00 00    	je     f0105360 <syscall+0x7e1>
        cprintf("sys_page_map: no mem");
f0104fd1:	83 ec 0c             	sub    $0xc,%esp
f0104fd4:	68 b8 89 10 f0       	push   $0xf01089b8
f0104fd9:	e8 1a ec ff ff       	call   f0103bf8 <cprintf>
f0104fde:	83 c4 10             	add    $0x10,%esp
        return -E_NO_MEM;
f0104fe1:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
    	case SYS_env_set_status:
        	return sys_env_set_status(a1, a2);
    	case SYS_page_alloc:
        	return sys_page_alloc(a1, (void *)a2, a3);
    	case SYS_page_map:
        	return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
f0104fe6:	e9 75 03 00 00       	jmp    f0105360 <syscall+0x7e1>
{
	// Hint: This function is a wrapper around page_remove().

	// LAB 4: Your code here.
    // check va.
    if ((uint32_t)va >= UTOP ||
f0104feb:	81 7d 10 ff ff bf ee 	cmpl   $0xeebfffff,0x10(%ebp)
f0104ff2:	77 09                	ja     f0104ffd <syscall+0x47e>
f0104ff4:	f7 45 10 ff 0f 00 00 	testl  $0xfff,0x10(%ebp)
f0104ffb:	74 1a                	je     f0105017 <syscall+0x498>
        ((uint32_t)va & PGMASK)) {
        cprintf("sys_page_unmap: bad va");
f0104ffd:	83 ec 0c             	sub    $0xc,%esp
f0105000:	68 cd 89 10 f0       	push   $0xf01089cd
f0105005:	e8 ee eb ff ff       	call   f0103bf8 <cprintf>
f010500a:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f010500d:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0105012:	e9 49 03 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f0105017:	83 ec 04             	sub    $0x4,%esp
f010501a:	6a 01                	push   $0x1
f010501c:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f010501f:	50                   	push   %eax
f0105020:	ff 75 0c             	pushl  0xc(%ebp)
f0105023:	e8 d1 e2 ff ff       	call   f01032f9 <envid2env>
f0105028:	89 c3                	mov    %eax,%ebx
f010502a:	83 c4 10             	add    $0x10,%esp
f010502d:	85 c0                	test   %eax,%eax
f010502f:	74 1a                	je     f010504b <syscall+0x4cc>
        cprintf("sys_page_unmap: bad env");
f0105031:	83 ec 0c             	sub    $0xc,%esp
f0105034:	68 e4 89 10 f0       	push   $0xf01089e4
f0105039:	e8 ba eb ff ff       	call   f0103bf8 <cprintf>
f010503e:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0105041:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0105046:	e9 15 03 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    page_remove(env->env_pgdir, va);
f010504b:	83 ec 08             	sub    $0x8,%esp
f010504e:	ff 75 10             	pushl  0x10(%ebp)
f0105051:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0105054:	ff 70 60             	pushl  0x60(%eax)
f0105057:	e8 89 c5 ff ff       	call   f01015e5 <page_remove>
f010505c:	83 c4 10             	add    $0x10,%esp
    	case SYS_page_alloc:
        	return sys_page_alloc(a1, (void *)a2, a3);
    	case SYS_page_map:
        	return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
	    case SYS_page_unmap:
	        return sys_page_unmap(a1, (void *)a2);
f010505f:	e9 fc 02 00 00       	jmp    f0105360 <syscall+0x7e1>
static int
sys_env_set_pgfault_upcall(envid_t envid, void *func)
{
	// LAB 4: Your code here.
	//panic("sys_env_set_pgfault_upcall not implemented");
    assert(func != NULL);
f0105064:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0105068:	75 19                	jne    f0105083 <syscall+0x504>
f010506a:	68 fc 89 10 f0       	push   $0xf01089fc
f010506f:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0105074:	68 a3 00 00 00       	push   $0xa3
f0105079:	68 09 8a 10 f0       	push   $0xf0108a09
f010507e:	e8 c9 af ff ff       	call   f010004c <_panic>
    
    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f0105083:	83 ec 04             	sub    $0x4,%esp
f0105086:	6a 01                	push   $0x1
f0105088:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f010508b:	50                   	push   %eax
f010508c:	ff 75 0c             	pushl  0xc(%ebp)
f010508f:	e8 65 e2 ff ff       	call   f01032f9 <envid2env>
f0105094:	89 c3                	mov    %eax,%ebx
f0105096:	83 c4 10             	add    $0x10,%esp
f0105099:	85 c0                	test   %eax,%eax
f010509b:	74 1a                	je     f01050b7 <syscall+0x538>
        cprintf("sys_env_set_pgfault_upcall: bad env");
f010509d:	83 ec 0c             	sub    $0xc,%esp
f01050a0:	68 1c 8b 10 f0       	push   $0xf0108b1c
f01050a5:	e8 4e eb ff ff       	call   f0103bf8 <cprintf>
f01050aa:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f01050ad:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f01050b2:	e9 a9 02 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    env->env_pgfault_upcall = func;
f01050b7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01050ba:	8b 7d 10             	mov    0x10(%ebp),%edi
f01050bd:	89 78 64             	mov    %edi,0x64(%eax)
    	case SYS_page_map:
        	return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
	    case SYS_page_unmap:
	        return sys_page_unmap(a1, (void *)a2);
	    case SYS_env_set_pgfault_upcall:
	        return sys_env_set_pgfault_upcall(a1, (void *)a2);
f01050c0:	e9 9b 02 00 00       	jmp    f0105360 <syscall+0x7e1>
static int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm)
{
	// LAB 4: Your code here.
    struct Env *env;
    if (envid2env(envid, &env, 0)) {
f01050c5:	83 ec 04             	sub    $0x4,%esp
f01050c8:	6a 00                	push   $0x0
f01050ca:	8d 45 e0             	lea    -0x20(%ebp),%eax
f01050cd:	50                   	push   %eax
f01050ce:	ff 75 0c             	pushl  0xc(%ebp)
f01050d1:	e8 23 e2 ff ff       	call   f01032f9 <envid2env>
f01050d6:	89 c3                	mov    %eax,%ebx
f01050d8:	83 c4 10             	add    $0x10,%esp
f01050db:	85 c0                	test   %eax,%eax
f01050dd:	74 24                	je     f0105103 <syscall+0x584>
        warn("sys_ipc_try_send: bad env");
f01050df:	83 ec 04             	sub    $0x4,%esp
f01050e2:	68 18 8a 10 f0       	push   $0xf0108a18
f01050e7:	68 99 01 00 00       	push   $0x199
f01050ec:	68 09 8a 10 f0       	push   $0xf0108a09
f01050f1:	e8 0b b2 ff ff       	call   f0100301 <_warn>
f01050f6:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f01050f9:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f01050fe:	e9 5d 02 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    if (!env->env_ipc_recving) {
f0105103:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0105106:	80 78 68 00          	cmpb   $0x0,0x68(%eax)
f010510a:	0f 84 84 01 00 00    	je     f0105294 <syscall+0x715>
        // If uncomment this, user/primes will make the screen a big mass.
        // warn("sys_ipc_try_send: not recv");
        return -E_IPC_NOT_RECV;
    }

    if ((uintptr_t)srcva < UTOP) {
f0105110:	81 7d 14 ff ff bf ee 	cmpl   $0xeebfffff,0x14(%ebp)
f0105117:	0f 87 36 01 00 00    	ja     f0105253 <syscall+0x6d4>
        if ((uintptr_t)srcva & 0xfff) {
f010511d:	f7 45 14 ff 0f 00 00 	testl  $0xfff,0x14(%ebp)
f0105124:	74 24                	je     f010514a <syscall+0x5cb>
            warn("sys_ipc_try_send: not aligned");
f0105126:	83 ec 04             	sub    $0x4,%esp
f0105129:	68 32 8a 10 f0       	push   $0xf0108a32
f010512e:	68 a5 01 00 00       	push   $0x1a5
f0105133:	68 09 8a 10 f0       	push   $0xf0108a09
f0105138:	e8 c4 b1 ff ff       	call   f0100301 <_warn>
f010513d:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f0105140:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0105145:	e9 16 02 00 00       	jmp    f0105360 <syscall+0x7e1>
        }

        if (!(perm & PTE_U) ||
f010514a:	8b 45 18             	mov    0x18(%ebp),%eax
f010514d:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f0105152:	83 f8 05             	cmp    $0x5,%eax
f0105155:	74 24                	je     f010517b <syscall+0x5fc>
            !(perm & PTE_P) ||
            (perm & ~PTE_SYSCALL)) {
            warn("sys_ipc_try_send: bad perm");
f0105157:	83 ec 04             	sub    $0x4,%esp
f010515a:	68 50 8a 10 f0       	push   $0xf0108a50
f010515f:	68 ac 01 00 00       	push   $0x1ac
f0105164:	68 09 8a 10 f0       	push   $0xf0108a09
f0105169:	e8 93 b1 ff ff       	call   f0100301 <_warn>
f010516e:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f0105171:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0105176:	e9 e5 01 00 00       	jmp    f0105360 <syscall+0x7e1>
        }

        pte_t *pte;
        struct PageInfo *page = page_lookup(curenv->env_pgdir, srcva, &pte);
f010517b:	e8 8a 15 00 00       	call   f010670a <cpunum>
f0105180:	83 ec 04             	sub    $0x4,%esp
f0105183:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f0105186:	52                   	push   %edx
f0105187:	ff 75 14             	pushl  0x14(%ebp)
f010518a:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010518d:	01 c2                	add    %eax,%edx
f010518f:	01 d2                	add    %edx,%edx
f0105191:	01 c2                	add    %eax,%edx
f0105193:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105196:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f010519d:	ff 70 60             	pushl  0x60(%eax)
f01051a0:	e8 1b c3 ff ff       	call   f01014c0 <page_lookup>
        if (!page) {
f01051a5:	83 c4 10             	add    $0x10,%esp
f01051a8:	85 c0                	test   %eax,%eax
f01051aa:	75 24                	jne    f01051d0 <syscall+0x651>
            warn("sys_ipc_try_send: src not mapped");
f01051ac:	83 ec 04             	sub    $0x4,%esp
f01051af:	68 40 8b 10 f0       	push   $0xf0108b40
f01051b4:	68 b3 01 00 00       	push   $0x1b3
f01051b9:	68 09 8a 10 f0       	push   $0xf0108a09
f01051be:	e8 3e b1 ff ff       	call   f0100301 <_warn>
f01051c3:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f01051c6:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f01051cb:	e9 90 01 00 00       	jmp    f0105360 <syscall+0x7e1>
        }

        if ((perm & PTE_W) && !(*pte & PTE_W)) {
f01051d0:	f6 45 18 02          	testb  $0x2,0x18(%ebp)
f01051d4:	74 2c                	je     f0105202 <syscall+0x683>
f01051d6:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f01051d9:	f6 02 02             	testb  $0x2,(%edx)
f01051dc:	75 24                	jne    f0105202 <syscall+0x683>
            warn("sys_ipc_try_send: src not writable");
f01051de:	83 ec 04             	sub    $0x4,%esp
f01051e1:	68 64 8b 10 f0       	push   $0xf0108b64
f01051e6:	68 b8 01 00 00       	push   $0x1b8
f01051eb:	68 09 8a 10 f0       	push   $0xf0108a09
f01051f0:	e8 0c b1 ff ff       	call   f0100301 <_warn>
f01051f5:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f01051f8:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f01051fd:	e9 5e 01 00 00       	jmp    f0105360 <syscall+0x7e1>
        }

        if ((uintptr_t)env->env_ipc_dstva < UTOP &&
f0105202:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0105205:	8b 4a 6c             	mov    0x6c(%edx),%ecx
f0105208:	81 f9 ff ff bf ee    	cmp    $0xeebfffff,%ecx
f010520e:	77 38                	ja     f0105248 <syscall+0x6c9>
            page_insert(env->env_pgdir, page, env->env_ipc_dstva, perm)) {
f0105210:	ff 75 18             	pushl  0x18(%ebp)
f0105213:	51                   	push   %ecx
f0105214:	50                   	push   %eax
f0105215:	ff 72 60             	pushl  0x60(%edx)
f0105218:	e8 68 c4 ff ff       	call   f0101685 <page_insert>
        if ((perm & PTE_W) && !(*pte & PTE_W)) {
            warn("sys_ipc_try_send: src not writable");
            return -E_INVAL;
        }

        if ((uintptr_t)env->env_ipc_dstva < UTOP &&
f010521d:	83 c4 10             	add    $0x10,%esp
f0105220:	85 c0                	test   %eax,%eax
f0105222:	74 24                	je     f0105248 <syscall+0x6c9>
            page_insert(env->env_pgdir, page, env->env_ipc_dstva, perm)) {
            warn("sys_ipc_try_send: no mem");
f0105224:	83 ec 04             	sub    $0x4,%esp
f0105227:	68 6b 8a 10 f0       	push   $0xf0108a6b
f010522c:	68 be 01 00 00       	push   $0x1be
f0105231:	68 09 8a 10 f0       	push   $0xf0108a09
f0105236:	e8 c6 b0 ff ff       	call   f0100301 <_warn>
f010523b:	83 c4 10             	add    $0x10,%esp
            return -E_NO_MEM;
f010523e:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
f0105243:	e9 18 01 00 00       	jmp    f0105360 <syscall+0x7e1>
        }
        env->env_ipc_perm = perm;
f0105248:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010524b:	8b 7d 18             	mov    0x18(%ebp),%edi
f010524e:	89 78 78             	mov    %edi,0x78(%eax)
f0105251:	eb 07                	jmp    f010525a <syscall+0x6db>
    } else {
        env->env_ipc_perm = 0;
f0105253:	c7 40 78 00 00 00 00 	movl   $0x0,0x78(%eax)
    }
    //cprintf("val in sys_ipc_try_send() is: %d\n", value);
    env->env_ipc_recving = 0;
f010525a:	8b 75 e0             	mov    -0x20(%ebp),%esi
f010525d:	c6 46 68 00          	movb   $0x0,0x68(%esi)
    env->env_ipc_from = curenv->env_id;
f0105261:	e8 a4 14 00 00       	call   f010670a <cpunum>
f0105266:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105269:	01 c2                	add    %eax,%edx
f010526b:	01 d2                	add    %edx,%edx
f010526d:	01 c2                	add    %eax,%edx
f010526f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105272:	8b 04 85 08 40 27 f0 	mov    -0xfd8bff8(,%eax,4),%eax
f0105279:	8b 40 48             	mov    0x48(%eax),%eax
f010527c:	89 46 74             	mov    %eax,0x74(%esi)
    env->env_ipc_value = value;
f010527f:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0105282:	8b 7d 10             	mov    0x10(%ebp),%edi
f0105285:	89 78 70             	mov    %edi,0x70(%eax)
    env->env_status = ENV_RUNNABLE;
f0105288:	c7 40 54 02 00 00 00 	movl   $0x2,0x54(%eax)
f010528f:	e9 cc 00 00 00       	jmp    f0105360 <syscall+0x7e1>
    }

    if (!env->env_ipc_recving) {
        // If uncomment this, user/primes will make the screen a big mass.
        // warn("sys_ipc_try_send: not recv");
        return -E_IPC_NOT_RECV;
f0105294:	bb f9 ff ff ff       	mov    $0xfffffff9,%ebx
	    case SYS_page_unmap:
	        return sys_page_unmap(a1, (void *)a2);
	    case SYS_env_set_pgfault_upcall:
	        return sys_env_set_pgfault_upcall(a1, (void *)a2);
	    case SYS_ipc_try_send: //lab4 last part 
	        return sys_ipc_try_send(a1, a2, (void *)a3, a4);
f0105299:	e9 c2 00 00 00       	jmp    f0105360 <syscall+0x7e1>
//	-E_INVAL if dstva < UTOP but dstva is not page-aligned.
static int
sys_ipc_recv(void *dstva)
{
	// LAB 4: Your code here.
    if ((uintptr_t)dstva < UTOP) {
f010529e:	81 7d 0c ff ff bf ee 	cmpl   $0xeebfffff,0xc(%ebp)
f01052a5:	77 43                	ja     f01052ea <syscall+0x76b>
        if ((uintptr_t)dstva & 0xfff) {
f01052a7:	f7 45 0c ff 0f 00 00 	testl  $0xfff,0xc(%ebp)
f01052ae:	74 24                	je     f01052d4 <syscall+0x755>
            warn("sys_ipc_recv: not aligned");
f01052b0:	83 ec 04             	sub    $0x4,%esp
f01052b3:	68 84 8a 10 f0       	push   $0xf0108a84
f01052b8:	68 e0 01 00 00       	push   $0x1e0
f01052bd:	68 09 8a 10 f0       	push   $0xf0108a09
f01052c2:	e8 3a b0 ff ff       	call   f0100301 <_warn>
	    case SYS_env_set_pgfault_upcall:
	        return sys_env_set_pgfault_upcall(a1, (void *)a2);
	    case SYS_ipc_try_send: //lab4 last part 
	        return sys_ipc_try_send(a1, a2, (void *)a3, a4);
	    case SYS_ipc_recv: //lab4 last part 
	        return sys_ipc_recv((void *)a1);
f01052c7:	83 c4 10             	add    $0x10,%esp
f01052ca:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f01052cf:	e9 8c 00 00 00       	jmp    f0105360 <syscall+0x7e1>
        if ((uintptr_t)dstva & 0xfff) {
            warn("sys_ipc_recv: not aligned");
            return -E_INVAL;
        }

        curenv->env_ipc_dstva = dstva;
f01052d4:	e8 31 14 00 00       	call   f010670a <cpunum>
f01052d9:	6b c0 74             	imul   $0x74,%eax,%eax
f01052dc:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f01052e2:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01052e5:	89 78 6c             	mov    %edi,0x6c(%eax)
f01052e8:	eb 15                	jmp    f01052ff <syscall+0x780>
    } else {
        // in case it's less than UTOP,
        // which would result in error.
        curenv->env_ipc_dstva = (void *)UTOP;
f01052ea:	e8 1b 14 00 00       	call   f010670a <cpunum>
f01052ef:	6b c0 74             	imul   $0x74,%eax,%eax
f01052f2:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f01052f8:	c7 40 6c 00 00 c0 ee 	movl   $0xeec00000,0x6c(%eax)
    }
    //if(curenv->env_ipc_recving != 0){
        curenv->env_ipc_recving = 1;
f01052ff:	e8 06 14 00 00       	call   f010670a <cpunum>
f0105304:	6b c0 74             	imul   $0x74,%eax,%eax
f0105307:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f010530d:	c6 40 68 01          	movb   $0x1,0x68(%eax)
        curenv->env_status = ENV_NOT_RUNNABLE;
f0105311:	e8 f4 13 00 00       	call   f010670a <cpunum>
f0105316:	6b c0 74             	imul   $0x74,%eax,%eax
f0105319:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f010531f:	c7 40 54 04 00 00 00 	movl   $0x4,0x54(%eax)
        //cprintf("In sys_ipc_recv(), env %d set itself not runable\n", curenv->env_id);
        // The call will never return on success,
        // but the system call needs to return 0.
        curenv->env_tf.tf_regs.reg_eax = 0;
f0105326:	e8 df 13 00 00       	call   f010670a <cpunum>
f010532b:	6b c0 74             	imul   $0x74,%eax,%eax
f010532e:	8b 80 08 40 27 f0    	mov    -0xfd8bff8(%eax),%eax
f0105334:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
// Deschedule current environment and pick a different one to run.
static void
sys_yield(void)
{   
    //cprintf("timer interrupted!\n");
	sched_yield();
f010533b:	e8 ab f7 ff ff       	call   f0104aeb <sched_yield>
// Return the current time.
static int
sys_time_msec(void)
{
	// LAB 4: Your code here.
    return (time_msec());
f0105340:	e8 5f 19 00 00       	call   f0106ca4 <time_msec>
f0105345:	89 c3                	mov    %eax,%ebx
	    case SYS_ipc_try_send: //lab4 last part 
	        return sys_ipc_try_send(a1, a2, (void *)a3, a4);
	    case SYS_ipc_recv: //lab4 last part 
	        return sys_ipc_recv((void *)a1);
        case SYS_time_msec:
            return sys_time_msec();
f0105347:	eb 17                	jmp    f0105360 <syscall+0x7e1>
		//case SYS_env_set_trapframe:
	    //    return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
		default:
			panic("syscall not implemented");
f0105349:	83 ec 04             	sub    $0x4,%esp
f010534c:	68 9e 8a 10 f0       	push   $0xf0108a9e
f0105351:	68 2b 02 00 00       	push   $0x22b
f0105356:	68 09 8a 10 f0       	push   $0xf0108a09
f010535b:	e8 ec ac ff ff       	call   f010004c <_panic>
			return -E_INVAL;
	}
}
f0105360:	89 d8                	mov    %ebx,%eax
f0105362:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105365:	5b                   	pop    %ebx
f0105366:	5e                   	pop    %esi
f0105367:	5f                   	pop    %edi
f0105368:	5d                   	pop    %ebp
f0105369:	c3                   	ret    

f010536a <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f010536a:	55                   	push   %ebp
f010536b:	89 e5                	mov    %esp,%ebp
f010536d:	57                   	push   %edi
f010536e:	56                   	push   %esi
f010536f:	53                   	push   %ebx
f0105370:	83 ec 14             	sub    $0x14,%esp
f0105373:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0105376:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0105379:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f010537c:	8b 7d 08             	mov    0x8(%ebp),%edi
	int l = *region_left, r = *region_right, any_matches = 0;
f010537f:	8b 1a                	mov    (%edx),%ebx
f0105381:	8b 01                	mov    (%ecx),%eax
f0105383:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0105386:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	while (l <= r) {
f010538d:	eb 7e                	jmp    f010540d <stab_binsearch+0xa3>
		int true_m = (l + r) / 2, m = true_m;
f010538f:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0105392:	01 d8                	add    %ebx,%eax
f0105394:	89 c6                	mov    %eax,%esi
f0105396:	c1 ee 1f             	shr    $0x1f,%esi
f0105399:	01 c6                	add    %eax,%esi
f010539b:	d1 fe                	sar    %esi
f010539d:	8d 04 36             	lea    (%esi,%esi,1),%eax
f01053a0:	01 f0                	add    %esi,%eax
f01053a2:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f01053a5:	8d 54 81 04          	lea    0x4(%ecx,%eax,4),%edx
f01053a9:	89 f0                	mov    %esi,%eax

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f01053ab:	eb 01                	jmp    f01053ae <stab_binsearch+0x44>
			m--;
f01053ad:	48                   	dec    %eax

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f01053ae:	39 c3                	cmp    %eax,%ebx
f01053b0:	7f 0c                	jg     f01053be <stab_binsearch+0x54>
f01053b2:	0f b6 0a             	movzbl (%edx),%ecx
f01053b5:	83 ea 0c             	sub    $0xc,%edx
f01053b8:	39 f9                	cmp    %edi,%ecx
f01053ba:	75 f1                	jne    f01053ad <stab_binsearch+0x43>
f01053bc:	eb 05                	jmp    f01053c3 <stab_binsearch+0x59>
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f01053be:	8d 5e 01             	lea    0x1(%esi),%ebx
			continue;
f01053c1:	eb 4a                	jmp    f010540d <stab_binsearch+0xa3>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f01053c3:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01053c6:	01 c2                	add    %eax,%edx
f01053c8:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f01053cb:	8b 54 91 08          	mov    0x8(%ecx,%edx,4),%edx
f01053cf:	39 55 0c             	cmp    %edx,0xc(%ebp)
f01053d2:	76 11                	jbe    f01053e5 <stab_binsearch+0x7b>
			*region_left = m;
f01053d4:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f01053d7:	89 03                	mov    %eax,(%ebx)
			l = true_m + 1;
f01053d9:	8d 5e 01             	lea    0x1(%esi),%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f01053dc:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f01053e3:	eb 28                	jmp    f010540d <stab_binsearch+0xa3>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f01053e5:	39 55 0c             	cmp    %edx,0xc(%ebp)
f01053e8:	73 12                	jae    f01053fc <stab_binsearch+0x92>
			*region_right = m - 1;
f01053ea:	48                   	dec    %eax
f01053eb:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01053ee:	8b 75 e0             	mov    -0x20(%ebp),%esi
f01053f1:	89 06                	mov    %eax,(%esi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f01053f3:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f01053fa:	eb 11                	jmp    f010540d <stab_binsearch+0xa3>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f01053fc:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f01053ff:	89 06                	mov    %eax,(%esi)
			l = m;
			addr++;
f0105401:	ff 45 0c             	incl   0xc(%ebp)
f0105404:	89 c3                	mov    %eax,%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0105406:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f010540d:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
f0105410:	0f 8e 79 ff ff ff    	jle    f010538f <stab_binsearch+0x25>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0105416:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
f010541a:	75 0d                	jne    f0105429 <stab_binsearch+0xbf>
		*region_right = *region_left - 1;
f010541c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010541f:	8b 00                	mov    (%eax),%eax
f0105421:	48                   	dec    %eax
f0105422:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105425:	89 07                	mov    %eax,(%edi)
f0105427:	eb 2c                	jmp    f0105455 <stab_binsearch+0xeb>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0105429:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010542c:	8b 00                	mov    (%eax),%eax
		     l > *region_left && stabs[l].n_type != type;
f010542e:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0105431:	8b 0e                	mov    (%esi),%ecx
f0105433:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105436:	01 c2                	add    %eax,%edx
f0105438:	8b 75 ec             	mov    -0x14(%ebp),%esi
f010543b:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f010543f:	eb 01                	jmp    f0105442 <stab_binsearch+0xd8>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0105441:	48                   	dec    %eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0105442:	39 c8                	cmp    %ecx,%eax
f0105444:	7e 0a                	jle    f0105450 <stab_binsearch+0xe6>
		     l > *region_left && stabs[l].n_type != type;
f0105446:	0f b6 1a             	movzbl (%edx),%ebx
f0105449:	83 ea 0c             	sub    $0xc,%edx
f010544c:	39 df                	cmp    %ebx,%edi
f010544e:	75 f1                	jne    f0105441 <stab_binsearch+0xd7>
		     l--)
			/* do nothing */;
		*region_left = l;
f0105450:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105453:	89 07                	mov    %eax,(%edi)
	}
}
f0105455:	83 c4 14             	add    $0x14,%esp
f0105458:	5b                   	pop    %ebx
f0105459:	5e                   	pop    %esi
f010545a:	5f                   	pop    %edi
f010545b:	5d                   	pop    %ebp
f010545c:	c3                   	ret    

f010545d <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f010545d:	55                   	push   %ebp
f010545e:	89 e5                	mov    %esp,%ebp
f0105460:	57                   	push   %edi
f0105461:	56                   	push   %esi
f0105462:	53                   	push   %ebx
f0105463:	83 ec 3c             	sub    $0x3c,%esp
f0105466:	8b 7d 0c             	mov    0xc(%ebp),%edi
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0105469:	c7 07 c0 8b 10 f0    	movl   $0xf0108bc0,(%edi)
	info->eip_line = 0;
f010546f:	c7 47 04 00 00 00 00 	movl   $0x0,0x4(%edi)
	info->eip_fn_name = "<unknown>";
f0105476:	c7 47 08 c0 8b 10 f0 	movl   $0xf0108bc0,0x8(%edi)
	info->eip_fn_namelen = 9;
f010547d:	c7 47 0c 09 00 00 00 	movl   $0x9,0xc(%edi)
	info->eip_fn_addr = addr;
f0105484:	8b 45 08             	mov    0x8(%ebp),%eax
f0105487:	89 47 10             	mov    %eax,0x10(%edi)
	info->eip_fn_narg = 0;
f010548a:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f0105491:	3d ff ff 7f ef       	cmp    $0xef7fffff,%eax
f0105496:	0f 87 a7 00 00 00    	ja     f0105543 <debuginfo_eip+0xe6>

		// Make sure this memory is valid.
		// Return -1 if it is not.  Hint: Call user_mem_check.
		// LAB 3: Your code here.

		stabs = usd->stabs;
f010549c:	a1 00 00 20 00       	mov    0x200000,%eax
f01054a1:	89 c3                	mov    %eax,%ebx
f01054a3:	89 45 c0             	mov    %eax,-0x40(%ebp)
		stab_end = usd->stab_end;
f01054a6:	8b 35 04 00 20 00    	mov    0x200004,%esi
		stabstr = usd->stabstr;
f01054ac:	a1 08 00 20 00       	mov    0x200008,%eax
f01054b1:	89 45 bc             	mov    %eax,-0x44(%ebp)
		stabstr_end = usd->stabstr_end;
f01054b4:	8b 0d 0c 00 20 00    	mov    0x20000c,%ecx
f01054ba:	89 4d b8             	mov    %ecx,-0x48(%ebp)

		// Make sure the STABS and string table memory is valid.
		// LAB 3: Your code here.
		user_mem_assert(curenv, usd, sizeof(struct UserStabData), PTE_U);
f01054bd:	e8 48 12 00 00       	call   f010670a <cpunum>
f01054c2:	6a 04                	push   $0x4
f01054c4:	6a 10                	push   $0x10
f01054c6:	68 00 00 20 00       	push   $0x200000
f01054cb:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01054ce:	01 c2                	add    %eax,%edx
f01054d0:	01 d2                	add    %edx,%edx
f01054d2:	01 c2                	add    %eax,%edx
f01054d4:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01054d7:	ff 34 85 08 40 27 f0 	pushl  -0xfd8bff8(,%eax,4)
f01054de:	e8 28 dd ff ff       	call   f010320b <user_mem_assert>
		user_mem_assert(curenv, stabs, (uintptr_t)stab_end - (uintptr_t)stabs, PTE_U);
f01054e3:	89 f2                	mov    %esi,%edx
f01054e5:	29 da                	sub    %ebx,%edx
f01054e7:	89 55 c4             	mov    %edx,-0x3c(%ebp)
f01054ea:	e8 1b 12 00 00       	call   f010670a <cpunum>
f01054ef:	6a 04                	push   $0x4
f01054f1:	ff 75 c4             	pushl  -0x3c(%ebp)
f01054f4:	53                   	push   %ebx
f01054f5:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01054f8:	01 c2                	add    %eax,%edx
f01054fa:	01 d2                	add    %edx,%edx
f01054fc:	01 c2                	add    %eax,%edx
f01054fe:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105501:	ff 34 85 08 40 27 f0 	pushl  -0xfd8bff8(,%eax,4)
f0105508:	e8 fe dc ff ff       	call   f010320b <user_mem_assert>
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
f010550d:	8b 4d b8             	mov    -0x48(%ebp),%ecx
f0105510:	8b 5d bc             	mov    -0x44(%ebp),%ebx
f0105513:	29 d9                	sub    %ebx,%ecx
f0105515:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
f0105518:	83 c4 20             	add    $0x20,%esp
f010551b:	e8 ea 11 00 00       	call   f010670a <cpunum>
f0105520:	6a 04                	push   $0x4
f0105522:	ff 75 c4             	pushl  -0x3c(%ebp)
f0105525:	53                   	push   %ebx
f0105526:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105529:	01 c2                	add    %eax,%edx
f010552b:	01 d2                	add    %edx,%edx
f010552d:	01 c2                	add    %eax,%edx
f010552f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105532:	ff 34 85 08 40 27 f0 	pushl  -0xfd8bff8(,%eax,4)
f0105539:	e8 cd dc ff ff       	call   f010320b <user_mem_assert>
f010553e:	83 c4 10             	add    $0x10,%esp
f0105541:	eb 1a                	jmp    f010555d <debuginfo_eip+0x100>
	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
f0105543:	c7 45 b8 1c 95 11 f0 	movl   $0xf011951c,-0x48(%ebp)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
f010554a:	c7 45 bc f1 49 11 f0 	movl   $0xf01149f1,-0x44(%ebp)
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
f0105551:	be f0 49 11 f0       	mov    $0xf01149f0,%esi
	info->eip_fn_addr = addr;
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
f0105556:	c7 45 c0 78 95 10 f0 	movl   $0xf0109578,-0x40(%ebp)
		user_mem_assert(curenv, stabs, (uintptr_t)stab_end - (uintptr_t)stabs, PTE_U);
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f010555d:	8b 45 b8             	mov    -0x48(%ebp),%eax
f0105560:	39 45 bc             	cmp    %eax,-0x44(%ebp)
f0105563:	0f 83 a3 01 00 00    	jae    f010570c <debuginfo_eip+0x2af>
f0105569:	89 c3                	mov    %eax,%ebx
f010556b:	80 78 ff 00          	cmpb   $0x0,-0x1(%eax)
f010556f:	0f 85 9e 01 00 00    	jne    f0105713 <debuginfo_eip+0x2b6>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0105575:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	rfile = (stab_end - stabs) - 1;
f010557c:	2b 75 c0             	sub    -0x40(%ebp),%esi
f010557f:	c1 fe 02             	sar    $0x2,%esi
f0105582:	8d 04 b6             	lea    (%esi,%esi,4),%eax
f0105585:	8d 04 86             	lea    (%esi,%eax,4),%eax
f0105588:	8d 14 86             	lea    (%esi,%eax,4),%edx
f010558b:	89 d0                	mov    %edx,%eax
f010558d:	c1 e0 08             	shl    $0x8,%eax
f0105590:	01 c2                	add    %eax,%edx
f0105592:	89 d0                	mov    %edx,%eax
f0105594:	c1 e0 10             	shl    $0x10,%eax
f0105597:	01 d0                	add    %edx,%eax
f0105599:	01 c0                	add    %eax,%eax
f010559b:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f010559f:	89 45 e0             	mov    %eax,-0x20(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f01055a2:	83 ec 08             	sub    $0x8,%esp
f01055a5:	ff 75 08             	pushl  0x8(%ebp)
f01055a8:	6a 64                	push   $0x64
f01055aa:	8d 4d e0             	lea    -0x20(%ebp),%ecx
f01055ad:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f01055b0:	8b 75 c0             	mov    -0x40(%ebp),%esi
f01055b3:	89 f0                	mov    %esi,%eax
f01055b5:	e8 b0 fd ff ff       	call   f010536a <stab_binsearch>
	if (lfile == 0)
f01055ba:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01055bd:	83 c4 10             	add    $0x10,%esp
f01055c0:	85 c0                	test   %eax,%eax
f01055c2:	0f 84 52 01 00 00    	je     f010571a <debuginfo_eip+0x2bd>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f01055c8:	89 45 dc             	mov    %eax,-0x24(%ebp)
	rfun = rfile;
f01055cb:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01055ce:	89 45 d8             	mov    %eax,-0x28(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f01055d1:	83 ec 08             	sub    $0x8,%esp
f01055d4:	ff 75 08             	pushl  0x8(%ebp)
f01055d7:	6a 24                	push   $0x24
f01055d9:	8d 4d d8             	lea    -0x28(%ebp),%ecx
f01055dc:	8d 55 dc             	lea    -0x24(%ebp),%edx
f01055df:	89 f0                	mov    %esi,%eax
f01055e1:	e8 84 fd ff ff       	call   f010536a <stab_binsearch>

	if (lfun <= rfun) {
f01055e6:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01055e9:	8b 55 d8             	mov    -0x28(%ebp),%edx
f01055ec:	83 c4 10             	add    $0x10,%esp
f01055ef:	39 d0                	cmp    %edx,%eax
f01055f1:	7f 28                	jg     f010561b <debuginfo_eip+0x1be>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f01055f3:	8d 0c 00             	lea    (%eax,%eax,1),%ecx
f01055f6:	01 c1                	add    %eax,%ecx
f01055f8:	8d 34 8e             	lea    (%esi,%ecx,4),%esi
f01055fb:	8b 0e                	mov    (%esi),%ecx
f01055fd:	2b 5d bc             	sub    -0x44(%ebp),%ebx
f0105600:	39 d9                	cmp    %ebx,%ecx
f0105602:	73 06                	jae    f010560a <debuginfo_eip+0x1ad>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0105604:	03 4d bc             	add    -0x44(%ebp),%ecx
f0105607:	89 4f 08             	mov    %ecx,0x8(%edi)
		info->eip_fn_addr = stabs[lfun].n_value;
f010560a:	8b 4e 08             	mov    0x8(%esi),%ecx
f010560d:	89 4f 10             	mov    %ecx,0x10(%edi)
		addr -= info->eip_fn_addr;
f0105610:	29 4d 08             	sub    %ecx,0x8(%ebp)
		// Search within the function definition for the line number.
		lline = lfun;
f0105613:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfun;
f0105616:	89 55 d0             	mov    %edx,-0x30(%ebp)
f0105619:	eb 12                	jmp    f010562d <debuginfo_eip+0x1d0>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f010561b:	8b 45 08             	mov    0x8(%ebp),%eax
f010561e:	89 47 10             	mov    %eax,0x10(%edi)
		lline = lfile;
f0105621:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0105624:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfile;
f0105627:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010562a:	89 45 d0             	mov    %eax,-0x30(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f010562d:	83 ec 08             	sub    $0x8,%esp
f0105630:	6a 3a                	push   $0x3a
f0105632:	ff 77 08             	pushl  0x8(%edi)
f0105635:	e8 b2 09 00 00       	call   f0105fec <strfind>
f010563a:	2b 47 08             	sub    0x8(%edi),%eax
f010563d:	89 47 0c             	mov    %eax,0xc(%edi)
	// Hint:
	//	There's a particular stabs type used for line numbers.
	//	Look at the STABS documentation and <inc/stab.h> to find
	//	which one.
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
f0105640:	83 c4 08             	add    $0x8,%esp
f0105643:	ff 75 08             	pushl  0x8(%ebp)
f0105646:	6a 44                	push   $0x44
f0105648:	8d 4d d0             	lea    -0x30(%ebp),%ecx
f010564b:	8d 55 d4             	lea    -0x2c(%ebp),%edx
f010564e:	8b 5d c0             	mov    -0x40(%ebp),%ebx
f0105651:	89 d8                	mov    %ebx,%eax
f0105653:	e8 12 fd ff ff       	call   f010536a <stab_binsearch>
	info->eip_line = stabs[lline].n_value;
f0105658:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010565b:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010565e:	01 c2                	add    %eax,%edx
f0105660:	c1 e2 02             	shl    $0x2,%edx
f0105663:	8b 4c 13 08          	mov    0x8(%ebx,%edx,1),%ecx
f0105667:	89 4f 04             	mov    %ecx,0x4(%edi)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f010566a:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f010566d:	8d 54 13 08          	lea    0x8(%ebx,%edx,1),%edx
f0105671:	83 c4 10             	add    $0x10,%esp
f0105674:	c6 45 c4 00          	movb   $0x0,-0x3c(%ebp)
f0105678:	89 7d 0c             	mov    %edi,0xc(%ebp)
f010567b:	eb 08                	jmp    f0105685 <debuginfo_eip+0x228>
f010567d:	48                   	dec    %eax
f010567e:	83 ea 0c             	sub    $0xc,%edx
f0105681:	c6 45 c4 01          	movb   $0x1,-0x3c(%ebp)
f0105685:	39 c6                	cmp    %eax,%esi
f0105687:	7e 05                	jle    f010568e <debuginfo_eip+0x231>
f0105689:	8b 7d 0c             	mov    0xc(%ebp),%edi
f010568c:	eb 47                	jmp    f01056d5 <debuginfo_eip+0x278>
	       && stabs[lline].n_type != N_SOL
f010568e:	8a 4a fc             	mov    -0x4(%edx),%cl
f0105691:	80 f9 84             	cmp    $0x84,%cl
f0105694:	75 0e                	jne    f01056a4 <debuginfo_eip+0x247>
f0105696:	8b 7d 0c             	mov    0xc(%ebp),%edi
f0105699:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f010569d:	74 1b                	je     f01056ba <debuginfo_eip+0x25d>
f010569f:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01056a2:	eb 16                	jmp    f01056ba <debuginfo_eip+0x25d>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f01056a4:	80 f9 64             	cmp    $0x64,%cl
f01056a7:	75 d4                	jne    f010567d <debuginfo_eip+0x220>
f01056a9:	83 3a 00             	cmpl   $0x0,(%edx)
f01056ac:	74 cf                	je     f010567d <debuginfo_eip+0x220>
f01056ae:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01056b1:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01056b5:	74 03                	je     f01056ba <debuginfo_eip+0x25d>
f01056b7:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f01056ba:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01056bd:	01 d0                	add    %edx,%eax
f01056bf:	8b 75 c0             	mov    -0x40(%ebp),%esi
f01056c2:	8b 14 86             	mov    (%esi,%eax,4),%edx
f01056c5:	8b 45 b8             	mov    -0x48(%ebp),%eax
f01056c8:	8b 75 bc             	mov    -0x44(%ebp),%esi
f01056cb:	29 f0                	sub    %esi,%eax
f01056cd:	39 c2                	cmp    %eax,%edx
f01056cf:	73 04                	jae    f01056d5 <debuginfo_eip+0x278>
		info->eip_file = stabstr + stabs[lline].n_strx;
f01056d1:	01 f2                	add    %esi,%edx
f01056d3:	89 17                	mov    %edx,(%edi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f01056d5:	8b 55 dc             	mov    -0x24(%ebp),%edx
f01056d8:	8b 5d d8             	mov    -0x28(%ebp),%ebx
f01056db:	39 da                	cmp    %ebx,%edx
f01056dd:	7d 42                	jge    f0105721 <debuginfo_eip+0x2c4>
		for (lline = lfun + 1;
f01056df:	42                   	inc    %edx
f01056e0:	89 55 d4             	mov    %edx,-0x2c(%ebp)
f01056e3:	89 d0                	mov    %edx,%eax
f01056e5:	8d 0c 12             	lea    (%edx,%edx,1),%ecx
f01056e8:	01 ca                	add    %ecx,%edx
f01056ea:	8b 75 c0             	mov    -0x40(%ebp),%esi
f01056ed:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx
f01056f1:	eb 03                	jmp    f01056f6 <debuginfo_eip+0x299>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;
f01056f3:	ff 47 14             	incl   0x14(%edi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
		for (lline = lfun + 1;
f01056f6:	39 c3                	cmp    %eax,%ebx
f01056f8:	7e 2e                	jle    f0105728 <debuginfo_eip+0x2cb>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
f01056fa:	8a 0a                	mov    (%edx),%cl
f01056fc:	40                   	inc    %eax
f01056fd:	83 c2 0c             	add    $0xc,%edx
f0105700:	80 f9 a0             	cmp    $0xa0,%cl
f0105703:	74 ee                	je     f01056f3 <debuginfo_eip+0x296>
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0105705:	b8 00 00 00 00       	mov    $0x0,%eax
f010570a:	eb 21                	jmp    f010572d <debuginfo_eip+0x2d0>
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f010570c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0105711:	eb 1a                	jmp    f010572d <debuginfo_eip+0x2d0>
f0105713:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0105718:	eb 13                	jmp    f010572d <debuginfo_eip+0x2d0>
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
		return -1;
f010571a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f010571f:	eb 0c                	jmp    f010572d <debuginfo_eip+0x2d0>
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0105721:	b8 00 00 00 00       	mov    $0x0,%eax
f0105726:	eb 05                	jmp    f010572d <debuginfo_eip+0x2d0>
f0105728:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010572d:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105730:	5b                   	pop    %ebx
f0105731:	5e                   	pop    %esi
f0105732:	5f                   	pop    %edi
f0105733:	5d                   	pop    %ebp
f0105734:	c3                   	ret    

f0105735 <cpuid_print>:
	return feature[bit / 32] & BIT(bit % 32);
}

void
cpuid_print(void)
{
f0105735:	55                   	push   %ebp
f0105736:	89 e5                	mov    %esp,%ebp
f0105738:	57                   	push   %edi
f0105739:	56                   	push   %esi
f010573a:	53                   	push   %ebx
f010573b:	83 ec 6c             	sub    $0x6c,%esp
	uint32_t eax, brand[12], feature[CPUID_NR_FLAGS] = {0};
f010573e:	8d 7d a4             	lea    -0x5c(%ebp),%edi
f0105741:	b9 05 00 00 00       	mov    $0x5,%ecx
f0105746:	b8 00 00 00 00       	mov    $0x0,%eax
f010574b:	f3 ab                	rep stos %eax,%es:(%edi)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f010574d:	b8 00 00 00 80       	mov    $0x80000000,%eax
f0105752:	0f a2                	cpuid  

	cpuid(0x80000000, &eax, NULL, NULL, NULL);
	if (eax < 0x80000004)
f0105754:	3d 03 00 00 80       	cmp    $0x80000003,%eax
f0105759:	77 17                	ja     f0105772 <cpuid_print+0x3d>
		panic("CPU too old!");
f010575b:	83 ec 04             	sub    $0x4,%esp
f010575e:	68 ca 8b 10 f0       	push   $0xf0108bca
f0105763:	68 8d 00 00 00       	push   $0x8d
f0105768:	68 d7 8b 10 f0       	push   $0xf0108bd7
f010576d:	e8 da a8 ff ff       	call   f010004c <_panic>
f0105772:	b8 02 00 00 80       	mov    $0x80000002,%eax
f0105777:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f0105779:	89 45 b8             	mov    %eax,-0x48(%ebp)
	if (ebxp)
		*ebxp = ebx;
f010577c:	89 5d bc             	mov    %ebx,-0x44(%ebp)
	if (ecxp)
		*ecxp = ecx;
f010577f:	89 4d c0             	mov    %ecx,-0x40(%ebp)
	if (edxp)
		*edxp = edx;
f0105782:	89 55 c4             	mov    %edx,-0x3c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0105785:	b8 03 00 00 80       	mov    $0x80000003,%eax
f010578a:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f010578c:	89 45 c8             	mov    %eax,-0x38(%ebp)
	if (ebxp)
		*ebxp = ebx;
f010578f:	89 5d cc             	mov    %ebx,-0x34(%ebp)
	if (ecxp)
		*ecxp = ecx;
f0105792:	89 4d d0             	mov    %ecx,-0x30(%ebp)
	if (edxp)
		*edxp = edx;
f0105795:	89 55 d4             	mov    %edx,-0x2c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0105798:	b8 04 00 00 80       	mov    $0x80000004,%eax
f010579d:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f010579f:	89 45 d8             	mov    %eax,-0x28(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01057a2:	89 5d dc             	mov    %ebx,-0x24(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01057a5:	89 4d e0             	mov    %ecx,-0x20(%ebp)
	if (edxp)
		*edxp = edx;
f01057a8:	89 55 e4             	mov    %edx,-0x1c(%ebp)

	cpuid(0x80000002, &brand[0], &brand[1], &brand[2], &brand[3]);
	cpuid(0x80000003, &brand[4], &brand[5], &brand[6], &brand[7]);
	cpuid(0x80000004, &brand[8], &brand[9], &brand[10], &brand[11]);
	cprintf("CPU: %.48s\n", brand);
f01057ab:	83 ec 08             	sub    $0x8,%esp
f01057ae:	8d 45 b8             	lea    -0x48(%ebp),%eax
f01057b1:	50                   	push   %eax
f01057b2:	68 e3 8b 10 f0       	push   $0xf0108be3
f01057b7:	e8 3c e4 ff ff       	call   f0103bf8 <cprintf>

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01057bc:	b8 01 00 00 00       	mov    $0x1,%eax
f01057c1:	0f a2                	cpuid  
f01057c3:	89 55 90             	mov    %edx,-0x70(%ebp)
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f01057c6:	89 4d a8             	mov    %ecx,-0x58(%ebp)
	if (edxp)
		*edxp = edx;
f01057c9:	89 55 a4             	mov    %edx,-0x5c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01057cc:	b8 01 00 00 80       	mov    $0x80000001,%eax
f01057d1:	0f a2                	cpuid  
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f01057d3:	89 4d b4             	mov    %ecx,-0x4c(%ebp)
	if (edxp)
		*edxp = edx;
f01057d6:	89 55 b0             	mov    %edx,-0x50(%ebp)
f01057d9:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f01057dc:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
		if (!feature[i])
f01057e3:	8b 45 94             	mov    -0x6c(%ebp),%eax
f01057e6:	8b 74 85 a4          	mov    -0x5c(%ebp,%eax,4),%esi
f01057ea:	85 f6                	test   %esi,%esi
f01057ec:	74 5a                	je     f0105848 <cpuid_print+0x113>
			continue;
		cprintf(" ");
f01057ee:	83 ec 0c             	sub    $0xc,%esp
f01057f1:	68 4c 73 10 f0       	push   $0xf010734c
f01057f6:	e8 fd e3 ff ff       	call   f0103bf8 <cprintf>
f01057fb:	8b 7d 94             	mov    -0x6c(%ebp),%edi
f01057fe:	c1 e7 05             	shl    $0x5,%edi
f0105801:	83 c4 10             	add    $0x10,%esp
		for (j = 0; j < 32; ++j) {
f0105804:	bb 00 00 00 00       	mov    $0x0,%ebx
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
f0105809:	89 f0                	mov    %esi,%eax
f010580b:	88 d9                	mov    %bl,%cl
f010580d:	d3 e8                	shr    %cl,%eax
f010580f:	a8 01                	test   $0x1,%al
f0105811:	74 1f                	je     f0105832 <cpuid_print+0xfd>
	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
			const char *name = names[CPUID_BIT(i, j)];
f0105813:	8d 04 3b             	lea    (%ebx,%edi,1),%eax
f0105816:	8b 04 85 40 8e 10 f0 	mov    -0xfef71c0(,%eax,4),%eax

			if ((feature[i] & BIT(j)) && name)
f010581d:	85 c0                	test   %eax,%eax
f010581f:	74 11                	je     f0105832 <cpuid_print+0xfd>
				cprintf(" %s", name);
f0105821:	83 ec 08             	sub    $0x8,%esp
f0105824:	50                   	push   %eax
f0105825:	68 e2 6f 10 f0       	push   $0xf0106fe2
f010582a:	e8 c9 e3 ff ff       	call   f0103bf8 <cprintf>
f010582f:	83 c4 10             	add    $0x10,%esp

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
f0105832:	43                   	inc    %ebx
f0105833:	83 fb 20             	cmp    $0x20,%ebx
f0105836:	75 d1                	jne    f0105809 <cpuid_print+0xd4>
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
				cprintf(" %s", name);
		}
		cprintf("\n");
f0105838:	83 ec 0c             	sub    $0xc,%esp
f010583b:	68 32 81 10 f0       	push   $0xf0108132
f0105840:	e8 b3 e3 ff ff       	call   f0103bf8 <cprintf>
f0105845:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f0105848:	ff 45 94             	incl   -0x6c(%ebp)
f010584b:	8b 45 94             	mov    -0x6c(%ebp),%eax
f010584e:	83 f8 05             	cmp    $0x5,%eax
f0105851:	75 90                	jne    f01057e3 <cpuid_print+0xae>
	      &feature[CPUID_1_ECX], &feature[CPUID_1_EDX]);
	cpuid(0x80000001, NULL, NULL,
	      &feature[CPUID_80000001_ECX], &feature[CPUID_80000001_EDX]);
	print_feature(feature);
	// Check feature bits.
	assert(cpuid_has(feature, CPUID_FEATURE_PSE));
f0105853:	f6 45 90 08          	testb  $0x8,-0x70(%ebp)
f0105857:	75 19                	jne    f0105872 <cpuid_print+0x13d>
f0105859:	68 e8 8d 10 f0       	push   $0xf0108de8
f010585e:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0105863:	68 9a 00 00 00       	push   $0x9a
f0105868:	68 d7 8b 10 f0       	push   $0xf0108bd7
f010586d:	e8 da a7 ff ff       	call   f010004c <_panic>
	assert(cpuid_has(feature, CPUID_FEATURE_APIC));
f0105872:	f7 45 90 00 02 00 00 	testl  $0x200,-0x70(%ebp)
f0105879:	75 19                	jne    f0105894 <cpuid_print+0x15f>
f010587b:	68 10 8e 10 f0       	push   $0xf0108e10
f0105880:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0105885:	68 9b 00 00 00       	push   $0x9b
f010588a:	68 d7 8b 10 f0       	push   $0xf0108bd7
f010588f:	e8 b8 a7 ff ff       	call   f010004c <_panic>
}
f0105894:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105897:	5b                   	pop    %ebx
f0105898:	5e                   	pop    %esi
f0105899:	5f                   	pop    %edi
f010589a:	5d                   	pop    %ebp
f010589b:	c3                   	ret    

f010589c <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f010589c:	55                   	push   %ebp
f010589d:	89 e5                	mov    %esp,%ebp
f010589f:	57                   	push   %edi
f01058a0:	56                   	push   %esi
f01058a1:	53                   	push   %ebx
f01058a2:	83 ec 1c             	sub    $0x1c,%esp
f01058a5:	89 c7                	mov    %eax,%edi
f01058a7:	89 d6                	mov    %edx,%esi
f01058a9:	8b 45 08             	mov    0x8(%ebp),%eax
f01058ac:	8b 55 0c             	mov    0xc(%ebp),%edx
f01058af:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01058b2:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f01058b5:	8b 4d 10             	mov    0x10(%ebp),%ecx
f01058b8:	bb 00 00 00 00       	mov    $0x0,%ebx
f01058bd:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f01058c0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
f01058c3:	39 d3                	cmp    %edx,%ebx
f01058c5:	72 05                	jb     f01058cc <printnum+0x30>
f01058c7:	39 45 10             	cmp    %eax,0x10(%ebp)
f01058ca:	77 45                	ja     f0105911 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f01058cc:	83 ec 0c             	sub    $0xc,%esp
f01058cf:	ff 75 18             	pushl  0x18(%ebp)
f01058d2:	8b 45 14             	mov    0x14(%ebp),%eax
f01058d5:	8d 58 ff             	lea    -0x1(%eax),%ebx
f01058d8:	53                   	push   %ebx
f01058d9:	ff 75 10             	pushl  0x10(%ebp)
f01058dc:	83 ec 08             	sub    $0x8,%esp
f01058df:	ff 75 e4             	pushl  -0x1c(%ebp)
f01058e2:	ff 75 e0             	pushl  -0x20(%ebp)
f01058e5:	ff 75 dc             	pushl  -0x24(%ebp)
f01058e8:	ff 75 d8             	pushl  -0x28(%ebp)
f01058eb:	e8 c4 13 00 00       	call   f0106cb4 <__udivdi3>
f01058f0:	83 c4 18             	add    $0x18,%esp
f01058f3:	52                   	push   %edx
f01058f4:	50                   	push   %eax
f01058f5:	89 f2                	mov    %esi,%edx
f01058f7:	89 f8                	mov    %edi,%eax
f01058f9:	e8 9e ff ff ff       	call   f010589c <printnum>
f01058fe:	83 c4 20             	add    $0x20,%esp
f0105901:	eb 16                	jmp    f0105919 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0105903:	83 ec 08             	sub    $0x8,%esp
f0105906:	56                   	push   %esi
f0105907:	ff 75 18             	pushl  0x18(%ebp)
f010590a:	ff d7                	call   *%edi
f010590c:	83 c4 10             	add    $0x10,%esp
f010590f:	eb 03                	jmp    f0105914 <printnum+0x78>
f0105911:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0105914:	4b                   	dec    %ebx
f0105915:	85 db                	test   %ebx,%ebx
f0105917:	7f ea                	jg     f0105903 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0105919:	83 ec 08             	sub    $0x8,%esp
f010591c:	56                   	push   %esi
f010591d:	83 ec 04             	sub    $0x4,%esp
f0105920:	ff 75 e4             	pushl  -0x1c(%ebp)
f0105923:	ff 75 e0             	pushl  -0x20(%ebp)
f0105926:	ff 75 dc             	pushl  -0x24(%ebp)
f0105929:	ff 75 d8             	pushl  -0x28(%ebp)
f010592c:	e8 93 14 00 00       	call   f0106dc4 <__umoddi3>
f0105931:	83 c4 14             	add    $0x14,%esp
f0105934:	0f be 80 c0 90 10 f0 	movsbl -0xfef6f40(%eax),%eax
f010593b:	50                   	push   %eax
f010593c:	ff d7                	call   *%edi
}
f010593e:	83 c4 10             	add    $0x10,%esp
f0105941:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105944:	5b                   	pop    %ebx
f0105945:	5e                   	pop    %esi
f0105946:	5f                   	pop    %edi
f0105947:	5d                   	pop    %ebp
f0105948:	c3                   	ret    

f0105949 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0105949:	55                   	push   %ebp
f010594a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f010594c:	83 fa 01             	cmp    $0x1,%edx
f010594f:	7e 0e                	jle    f010595f <getuint+0x16>
		return va_arg(*ap, unsigned long long);
f0105951:	8b 10                	mov    (%eax),%edx
f0105953:	8d 4a 08             	lea    0x8(%edx),%ecx
f0105956:	89 08                	mov    %ecx,(%eax)
f0105958:	8b 02                	mov    (%edx),%eax
f010595a:	8b 52 04             	mov    0x4(%edx),%edx
f010595d:	eb 22                	jmp    f0105981 <getuint+0x38>
	else if (lflag)
f010595f:	85 d2                	test   %edx,%edx
f0105961:	74 10                	je     f0105973 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
f0105963:	8b 10                	mov    (%eax),%edx
f0105965:	8d 4a 04             	lea    0x4(%edx),%ecx
f0105968:	89 08                	mov    %ecx,(%eax)
f010596a:	8b 02                	mov    (%edx),%eax
f010596c:	ba 00 00 00 00       	mov    $0x0,%edx
f0105971:	eb 0e                	jmp    f0105981 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
f0105973:	8b 10                	mov    (%eax),%edx
f0105975:	8d 4a 04             	lea    0x4(%edx),%ecx
f0105978:	89 08                	mov    %ecx,(%eax)
f010597a:	8b 02                	mov    (%edx),%eax
f010597c:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0105981:	5d                   	pop    %ebp
f0105982:	c3                   	ret    

f0105983 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f0105983:	55                   	push   %ebp
f0105984:	89 e5                	mov    %esp,%ebp
f0105986:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
f0105989:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
f010598c:	8b 10                	mov    (%eax),%edx
f010598e:	3b 50 04             	cmp    0x4(%eax),%edx
f0105991:	73 0a                	jae    f010599d <sprintputch+0x1a>
		*b->buf++ = ch;
f0105993:	8d 4a 01             	lea    0x1(%edx),%ecx
f0105996:	89 08                	mov    %ecx,(%eax)
f0105998:	8b 45 08             	mov    0x8(%ebp),%eax
f010599b:	88 02                	mov    %al,(%edx)
}
f010599d:	5d                   	pop    %ebp
f010599e:	c3                   	ret    

f010599f <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f010599f:	55                   	push   %ebp
f01059a0:	89 e5                	mov    %esp,%ebp
f01059a2:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f01059a5:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f01059a8:	50                   	push   %eax
f01059a9:	ff 75 10             	pushl  0x10(%ebp)
f01059ac:	ff 75 0c             	pushl  0xc(%ebp)
f01059af:	ff 75 08             	pushl  0x8(%ebp)
f01059b2:	e8 05 00 00 00       	call   f01059bc <vprintfmt>
	va_end(ap);
}
f01059b7:	83 c4 10             	add    $0x10,%esp
f01059ba:	c9                   	leave  
f01059bb:	c3                   	ret    

f01059bc <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f01059bc:	55                   	push   %ebp
f01059bd:	89 e5                	mov    %esp,%ebp
f01059bf:	57                   	push   %edi
f01059c0:	56                   	push   %esi
f01059c1:	53                   	push   %ebx
f01059c2:	83 ec 2c             	sub    $0x2c,%esp
f01059c5:	8b 75 08             	mov    0x8(%ebp),%esi
f01059c8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01059cb:	8b 7d 10             	mov    0x10(%ebp),%edi
f01059ce:	eb 12                	jmp    f01059e2 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
f01059d0:	85 c0                	test   %eax,%eax
f01059d2:	0f 84 68 03 00 00    	je     f0105d40 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
f01059d8:	83 ec 08             	sub    $0x8,%esp
f01059db:	53                   	push   %ebx
f01059dc:	50                   	push   %eax
f01059dd:	ff d6                	call   *%esi
f01059df:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f01059e2:	47                   	inc    %edi
f01059e3:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f01059e7:	83 f8 25             	cmp    $0x25,%eax
f01059ea:	75 e4                	jne    f01059d0 <vprintfmt+0x14>
f01059ec:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
f01059f0:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f01059f7:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f01059fe:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
f0105a05:	ba 00 00 00 00       	mov    $0x0,%edx
f0105a0a:	eb 07                	jmp    f0105a13 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a0c:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
f0105a0f:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a13:	8d 47 01             	lea    0x1(%edi),%eax
f0105a16:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0105a19:	0f b6 0f             	movzbl (%edi),%ecx
f0105a1c:	8a 07                	mov    (%edi),%al
f0105a1e:	83 e8 23             	sub    $0x23,%eax
f0105a21:	3c 55                	cmp    $0x55,%al
f0105a23:	0f 87 fe 02 00 00    	ja     f0105d27 <vprintfmt+0x36b>
f0105a29:	0f b6 c0             	movzbl %al,%eax
f0105a2c:	ff 24 85 80 91 10 f0 	jmp    *-0xfef6e80(,%eax,4)
f0105a33:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f0105a36:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
f0105a3a:	eb d7                	jmp    f0105a13 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a3c:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105a3f:	b8 00 00 00 00       	mov    $0x0,%eax
f0105a44:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f0105a47:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0105a4a:	01 c0                	add    %eax,%eax
f0105a4c:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
f0105a50:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
f0105a53:	8d 51 d0             	lea    -0x30(%ecx),%edx
f0105a56:	83 fa 09             	cmp    $0x9,%edx
f0105a59:	77 34                	ja     f0105a8f <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0105a5b:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f0105a5c:	eb e9                	jmp    f0105a47 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f0105a5e:	8b 45 14             	mov    0x14(%ebp),%eax
f0105a61:	8d 48 04             	lea    0x4(%eax),%ecx
f0105a64:	89 4d 14             	mov    %ecx,0x14(%ebp)
f0105a67:	8b 00                	mov    (%eax),%eax
f0105a69:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a6c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f0105a6f:	eb 24                	jmp    f0105a95 <vprintfmt+0xd9>
f0105a71:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0105a75:	79 07                	jns    f0105a7e <vprintfmt+0xc2>
f0105a77:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a7e:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105a81:	eb 90                	jmp    f0105a13 <vprintfmt+0x57>
f0105a83:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f0105a86:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
f0105a8d:	eb 84                	jmp    f0105a13 <vprintfmt+0x57>
f0105a8f:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0105a92:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
f0105a95:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0105a99:	0f 89 74 ff ff ff    	jns    f0105a13 <vprintfmt+0x57>
				width = precision, precision = -1;
f0105a9f:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0105aa2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0105aa5:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0105aac:	e9 62 ff ff ff       	jmp    f0105a13 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f0105ab1:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105ab2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f0105ab5:	e9 59 ff ff ff       	jmp    f0105a13 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f0105aba:	8b 45 14             	mov    0x14(%ebp),%eax
f0105abd:	8d 50 04             	lea    0x4(%eax),%edx
f0105ac0:	89 55 14             	mov    %edx,0x14(%ebp)
f0105ac3:	83 ec 08             	sub    $0x8,%esp
f0105ac6:	53                   	push   %ebx
f0105ac7:	ff 30                	pushl  (%eax)
f0105ac9:	ff d6                	call   *%esi
			break;
f0105acb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105ace:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
f0105ad1:	e9 0c ff ff ff       	jmp    f01059e2 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
f0105ad6:	8b 45 14             	mov    0x14(%ebp),%eax
f0105ad9:	8d 50 04             	lea    0x4(%eax),%edx
f0105adc:	89 55 14             	mov    %edx,0x14(%ebp)
f0105adf:	8b 00                	mov    (%eax),%eax
f0105ae1:	85 c0                	test   %eax,%eax
f0105ae3:	79 02                	jns    f0105ae7 <vprintfmt+0x12b>
f0105ae5:	f7 d8                	neg    %eax
f0105ae7:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
f0105ae9:	83 f8 08             	cmp    $0x8,%eax
f0105aec:	7f 0b                	jg     f0105af9 <vprintfmt+0x13d>
f0105aee:	8b 04 85 e0 92 10 f0 	mov    -0xfef6d20(,%eax,4),%eax
f0105af5:	85 c0                	test   %eax,%eax
f0105af7:	75 18                	jne    f0105b11 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
f0105af9:	52                   	push   %edx
f0105afa:	68 d8 90 10 f0       	push   $0xf01090d8
f0105aff:	53                   	push   %ebx
f0105b00:	56                   	push   %esi
f0105b01:	e8 99 fe ff ff       	call   f010599f <printfmt>
f0105b06:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105b09:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
f0105b0c:	e9 d1 fe ff ff       	jmp    f01059e2 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
f0105b11:	50                   	push   %eax
f0105b12:	68 e3 6f 10 f0       	push   $0xf0106fe3
f0105b17:	53                   	push   %ebx
f0105b18:	56                   	push   %esi
f0105b19:	e8 81 fe ff ff       	call   f010599f <printfmt>
f0105b1e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105b21:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105b24:	e9 b9 fe ff ff       	jmp    f01059e2 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0105b29:	8b 45 14             	mov    0x14(%ebp),%eax
f0105b2c:	8d 50 04             	lea    0x4(%eax),%edx
f0105b2f:	89 55 14             	mov    %edx,0x14(%ebp)
f0105b32:	8b 38                	mov    (%eax),%edi
f0105b34:	85 ff                	test   %edi,%edi
f0105b36:	75 05                	jne    f0105b3d <vprintfmt+0x181>
				p = "(null)";
f0105b38:	bf d1 90 10 f0       	mov    $0xf01090d1,%edi
			if (width > 0 && padc != '-')
f0105b3d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0105b41:	0f 8e 90 00 00 00    	jle    f0105bd7 <vprintfmt+0x21b>
f0105b47:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
f0105b4b:	0f 84 8e 00 00 00    	je     f0105bdf <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
f0105b51:	83 ec 08             	sub    $0x8,%esp
f0105b54:	ff 75 d0             	pushl  -0x30(%ebp)
f0105b57:	57                   	push   %edi
f0105b58:	e8 60 03 00 00       	call   f0105ebd <strnlen>
f0105b5d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f0105b60:	29 c1                	sub    %eax,%ecx
f0105b62:	89 4d cc             	mov    %ecx,-0x34(%ebp)
f0105b65:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
f0105b68:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
f0105b6c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0105b6f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f0105b72:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0105b74:	eb 0d                	jmp    f0105b83 <vprintfmt+0x1c7>
					putch(padc, putdat);
f0105b76:	83 ec 08             	sub    $0x8,%esp
f0105b79:	53                   	push   %ebx
f0105b7a:	ff 75 e4             	pushl  -0x1c(%ebp)
f0105b7d:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0105b7f:	4f                   	dec    %edi
f0105b80:	83 c4 10             	add    $0x10,%esp
f0105b83:	85 ff                	test   %edi,%edi
f0105b85:	7f ef                	jg     f0105b76 <vprintfmt+0x1ba>
f0105b87:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0105b8a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f0105b8d:	89 c8                	mov    %ecx,%eax
f0105b8f:	85 c9                	test   %ecx,%ecx
f0105b91:	79 05                	jns    f0105b98 <vprintfmt+0x1dc>
f0105b93:	b8 00 00 00 00       	mov    $0x0,%eax
f0105b98:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f0105b9b:	29 c1                	sub    %eax,%ecx
f0105b9d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f0105ba0:	89 75 08             	mov    %esi,0x8(%ebp)
f0105ba3:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0105ba6:	eb 3d                	jmp    f0105be5 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f0105ba8:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
f0105bac:	74 19                	je     f0105bc7 <vprintfmt+0x20b>
f0105bae:	0f be c0             	movsbl %al,%eax
f0105bb1:	83 e8 20             	sub    $0x20,%eax
f0105bb4:	83 f8 5e             	cmp    $0x5e,%eax
f0105bb7:	76 0e                	jbe    f0105bc7 <vprintfmt+0x20b>
					putch('?', putdat);
f0105bb9:	83 ec 08             	sub    $0x8,%esp
f0105bbc:	53                   	push   %ebx
f0105bbd:	6a 3f                	push   $0x3f
f0105bbf:	ff 55 08             	call   *0x8(%ebp)
f0105bc2:	83 c4 10             	add    $0x10,%esp
f0105bc5:	eb 0b                	jmp    f0105bd2 <vprintfmt+0x216>
				else
					putch(ch, putdat);
f0105bc7:	83 ec 08             	sub    $0x8,%esp
f0105bca:	53                   	push   %ebx
f0105bcb:	52                   	push   %edx
f0105bcc:	ff 55 08             	call   *0x8(%ebp)
f0105bcf:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f0105bd2:	ff 4d e4             	decl   -0x1c(%ebp)
f0105bd5:	eb 0e                	jmp    f0105be5 <vprintfmt+0x229>
f0105bd7:	89 75 08             	mov    %esi,0x8(%ebp)
f0105bda:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0105bdd:	eb 06                	jmp    f0105be5 <vprintfmt+0x229>
f0105bdf:	89 75 08             	mov    %esi,0x8(%ebp)
f0105be2:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0105be5:	47                   	inc    %edi
f0105be6:	8a 47 ff             	mov    -0x1(%edi),%al
f0105be9:	0f be d0             	movsbl %al,%edx
f0105bec:	85 d2                	test   %edx,%edx
f0105bee:	74 1d                	je     f0105c0d <vprintfmt+0x251>
f0105bf0:	85 f6                	test   %esi,%esi
f0105bf2:	78 b4                	js     f0105ba8 <vprintfmt+0x1ec>
f0105bf4:	4e                   	dec    %esi
f0105bf5:	79 b1                	jns    f0105ba8 <vprintfmt+0x1ec>
f0105bf7:	8b 75 08             	mov    0x8(%ebp),%esi
f0105bfa:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105bfd:	eb 14                	jmp    f0105c13 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
f0105bff:	83 ec 08             	sub    $0x8,%esp
f0105c02:	53                   	push   %ebx
f0105c03:	6a 20                	push   $0x20
f0105c05:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f0105c07:	4f                   	dec    %edi
f0105c08:	83 c4 10             	add    $0x10,%esp
f0105c0b:	eb 06                	jmp    f0105c13 <vprintfmt+0x257>
f0105c0d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105c10:	8b 75 08             	mov    0x8(%ebp),%esi
f0105c13:	85 ff                	test   %edi,%edi
f0105c15:	7f e8                	jg     f0105bff <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105c17:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105c1a:	e9 c3 fd ff ff       	jmp    f01059e2 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0105c1f:	83 fa 01             	cmp    $0x1,%edx
f0105c22:	7e 16                	jle    f0105c3a <vprintfmt+0x27e>
		return va_arg(*ap, long long);
f0105c24:	8b 45 14             	mov    0x14(%ebp),%eax
f0105c27:	8d 50 08             	lea    0x8(%eax),%edx
f0105c2a:	89 55 14             	mov    %edx,0x14(%ebp)
f0105c2d:	8b 50 04             	mov    0x4(%eax),%edx
f0105c30:	8b 00                	mov    (%eax),%eax
f0105c32:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0105c35:	89 55 dc             	mov    %edx,-0x24(%ebp)
f0105c38:	eb 32                	jmp    f0105c6c <vprintfmt+0x2b0>
	else if (lflag)
f0105c3a:	85 d2                	test   %edx,%edx
f0105c3c:	74 18                	je     f0105c56 <vprintfmt+0x29a>
		return va_arg(*ap, long);
f0105c3e:	8b 45 14             	mov    0x14(%ebp),%eax
f0105c41:	8d 50 04             	lea    0x4(%eax),%edx
f0105c44:	89 55 14             	mov    %edx,0x14(%ebp)
f0105c47:	8b 00                	mov    (%eax),%eax
f0105c49:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0105c4c:	89 c1                	mov    %eax,%ecx
f0105c4e:	c1 f9 1f             	sar    $0x1f,%ecx
f0105c51:	89 4d dc             	mov    %ecx,-0x24(%ebp)
f0105c54:	eb 16                	jmp    f0105c6c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
f0105c56:	8b 45 14             	mov    0x14(%ebp),%eax
f0105c59:	8d 50 04             	lea    0x4(%eax),%edx
f0105c5c:	89 55 14             	mov    %edx,0x14(%ebp)
f0105c5f:	8b 00                	mov    (%eax),%eax
f0105c61:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0105c64:	89 c1                	mov    %eax,%ecx
f0105c66:	c1 f9 1f             	sar    $0x1f,%ecx
f0105c69:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f0105c6c:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0105c6f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
f0105c72:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f0105c76:	79 76                	jns    f0105cee <vprintfmt+0x332>
				putch('-', putdat);
f0105c78:	83 ec 08             	sub    $0x8,%esp
f0105c7b:	53                   	push   %ebx
f0105c7c:	6a 2d                	push   $0x2d
f0105c7e:	ff d6                	call   *%esi
				num = -(long long) num;
f0105c80:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0105c83:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0105c86:	f7 d8                	neg    %eax
f0105c88:	83 d2 00             	adc    $0x0,%edx
f0105c8b:	f7 da                	neg    %edx
f0105c8d:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
f0105c90:	b9 0a 00 00 00       	mov    $0xa,%ecx
f0105c95:	eb 5c                	jmp    f0105cf3 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f0105c97:	8d 45 14             	lea    0x14(%ebp),%eax
f0105c9a:	e8 aa fc ff ff       	call   f0105949 <getuint>
			base = 10;
f0105c9f:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
f0105ca4:	eb 4d                	jmp    f0105cf3 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
f0105ca6:	8d 45 14             	lea    0x14(%ebp),%eax
f0105ca9:	e8 9b fc ff ff       	call   f0105949 <getuint>
			base = 8;
f0105cae:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
f0105cb3:	eb 3e                	jmp    f0105cf3 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
f0105cb5:	83 ec 08             	sub    $0x8,%esp
f0105cb8:	53                   	push   %ebx
f0105cb9:	6a 30                	push   $0x30
f0105cbb:	ff d6                	call   *%esi
			putch('x', putdat);
f0105cbd:	83 c4 08             	add    $0x8,%esp
f0105cc0:	53                   	push   %ebx
f0105cc1:	6a 78                	push   $0x78
f0105cc3:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f0105cc5:	8b 45 14             	mov    0x14(%ebp),%eax
f0105cc8:	8d 50 04             	lea    0x4(%eax),%edx
f0105ccb:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f0105cce:	8b 00                	mov    (%eax),%eax
f0105cd0:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
f0105cd5:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
f0105cd8:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
f0105cdd:	eb 14                	jmp    f0105cf3 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f0105cdf:	8d 45 14             	lea    0x14(%ebp),%eax
f0105ce2:	e8 62 fc ff ff       	call   f0105949 <getuint>
			base = 16;
f0105ce7:	b9 10 00 00 00       	mov    $0x10,%ecx
f0105cec:	eb 05                	jmp    f0105cf3 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f0105cee:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
f0105cf3:	83 ec 0c             	sub    $0xc,%esp
f0105cf6:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
f0105cfa:	57                   	push   %edi
f0105cfb:	ff 75 e4             	pushl  -0x1c(%ebp)
f0105cfe:	51                   	push   %ecx
f0105cff:	52                   	push   %edx
f0105d00:	50                   	push   %eax
f0105d01:	89 da                	mov    %ebx,%edx
f0105d03:	89 f0                	mov    %esi,%eax
f0105d05:	e8 92 fb ff ff       	call   f010589c <printnum>
			break;
f0105d0a:	83 c4 20             	add    $0x20,%esp
f0105d0d:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105d10:	e9 cd fc ff ff       	jmp    f01059e2 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0105d15:	83 ec 08             	sub    $0x8,%esp
f0105d18:	53                   	push   %ebx
f0105d19:	51                   	push   %ecx
f0105d1a:	ff d6                	call   *%esi
			break;
f0105d1c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105d1f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
f0105d22:	e9 bb fc ff ff       	jmp    f01059e2 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0105d27:	83 ec 08             	sub    $0x8,%esp
f0105d2a:	53                   	push   %ebx
f0105d2b:	6a 25                	push   $0x25
f0105d2d:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
f0105d2f:	83 c4 10             	add    $0x10,%esp
f0105d32:	eb 01                	jmp    f0105d35 <vprintfmt+0x379>
f0105d34:	4f                   	dec    %edi
f0105d35:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
f0105d39:	75 f9                	jne    f0105d34 <vprintfmt+0x378>
f0105d3b:	e9 a2 fc ff ff       	jmp    f01059e2 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
f0105d40:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105d43:	5b                   	pop    %ebx
f0105d44:	5e                   	pop    %esi
f0105d45:	5f                   	pop    %edi
f0105d46:	5d                   	pop    %ebp
f0105d47:	c3                   	ret    

f0105d48 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f0105d48:	55                   	push   %ebp
f0105d49:	89 e5                	mov    %esp,%ebp
f0105d4b:	83 ec 18             	sub    $0x18,%esp
f0105d4e:	8b 45 08             	mov    0x8(%ebp),%eax
f0105d51:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f0105d54:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0105d57:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f0105d5b:	89 4d f0             	mov    %ecx,-0x10(%ebp)
f0105d5e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f0105d65:	85 c0                	test   %eax,%eax
f0105d67:	74 26                	je     f0105d8f <vsnprintf+0x47>
f0105d69:	85 d2                	test   %edx,%edx
f0105d6b:	7e 29                	jle    f0105d96 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f0105d6d:	ff 75 14             	pushl  0x14(%ebp)
f0105d70:	ff 75 10             	pushl  0x10(%ebp)
f0105d73:	8d 45 ec             	lea    -0x14(%ebp),%eax
f0105d76:	50                   	push   %eax
f0105d77:	68 83 59 10 f0       	push   $0xf0105983
f0105d7c:	e8 3b fc ff ff       	call   f01059bc <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f0105d81:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0105d84:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f0105d87:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0105d8a:	83 c4 10             	add    $0x10,%esp
f0105d8d:	eb 0c                	jmp    f0105d9b <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f0105d8f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f0105d94:	eb 05                	jmp    f0105d9b <vsnprintf+0x53>
f0105d96:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f0105d9b:	c9                   	leave  
f0105d9c:	c3                   	ret    

f0105d9d <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f0105d9d:	55                   	push   %ebp
f0105d9e:	89 e5                	mov    %esp,%ebp
f0105da0:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f0105da3:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f0105da6:	50                   	push   %eax
f0105da7:	ff 75 10             	pushl  0x10(%ebp)
f0105daa:	ff 75 0c             	pushl  0xc(%ebp)
f0105dad:	ff 75 08             	pushl  0x8(%ebp)
f0105db0:	e8 93 ff ff ff       	call   f0105d48 <vsnprintf>
	va_end(ap);

	return rc;
}
f0105db5:	c9                   	leave  
f0105db6:	c3                   	ret    

f0105db7 <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f0105db7:	55                   	push   %ebp
f0105db8:	89 e5                	mov    %esp,%ebp
f0105dba:	57                   	push   %edi
f0105dbb:	56                   	push   %esi
f0105dbc:	53                   	push   %ebx
f0105dbd:	83 ec 0c             	sub    $0xc,%esp
f0105dc0:	8b 45 08             	mov    0x8(%ebp),%eax
	int i, c, echoing;

	if (prompt != NULL)
f0105dc3:	85 c0                	test   %eax,%eax
f0105dc5:	74 11                	je     f0105dd8 <readline+0x21>
		cprintf("%s", prompt);
f0105dc7:	83 ec 08             	sub    $0x8,%esp
f0105dca:	50                   	push   %eax
f0105dcb:	68 e3 6f 10 f0       	push   $0xf0106fe3
f0105dd0:	e8 23 de ff ff       	call   f0103bf8 <cprintf>
f0105dd5:	83 c4 10             	add    $0x10,%esp

	i = 0;
	echoing = iscons(0);
f0105dd8:	83 ec 0c             	sub    $0xc,%esp
f0105ddb:	6a 00                	push   $0x0
f0105ddd:	e8 1a aa ff ff       	call   f01007fc <iscons>
f0105de2:	89 c7                	mov    %eax,%edi
f0105de4:	83 c4 10             	add    $0x10,%esp
	int i, c, echoing;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
f0105de7:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
f0105dec:	e8 fa a9 ff ff       	call   f01007eb <getchar>
f0105df1:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f0105df3:	85 c0                	test   %eax,%eax
f0105df5:	79 1b                	jns    f0105e12 <readline+0x5b>
			cprintf("read error: %e\n", c);
f0105df7:	83 ec 08             	sub    $0x8,%esp
f0105dfa:	50                   	push   %eax
f0105dfb:	68 04 93 10 f0       	push   $0xf0109304
f0105e00:	e8 f3 dd ff ff       	call   f0103bf8 <cprintf>
			return NULL;
f0105e05:	83 c4 10             	add    $0x10,%esp
f0105e08:	b8 00 00 00 00       	mov    $0x0,%eax
f0105e0d:	e9 8d 00 00 00       	jmp    f0105e9f <readline+0xe8>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0105e12:	83 f8 08             	cmp    $0x8,%eax
f0105e15:	74 72                	je     f0105e89 <readline+0xd2>
f0105e17:	83 f8 7f             	cmp    $0x7f,%eax
f0105e1a:	75 16                	jne    f0105e32 <readline+0x7b>
f0105e1c:	eb 65                	jmp    f0105e83 <readline+0xcc>
			if (echoing)
f0105e1e:	85 ff                	test   %edi,%edi
f0105e20:	74 0d                	je     f0105e2f <readline+0x78>
				cputchar('\b');
f0105e22:	83 ec 0c             	sub    $0xc,%esp
f0105e25:	6a 08                	push   $0x8
f0105e27:	e8 af a9 ff ff       	call   f01007db <cputchar>
f0105e2c:	83 c4 10             	add    $0x10,%esp
			i--;
f0105e2f:	4e                   	dec    %esi
f0105e30:	eb ba                	jmp    f0105dec <readline+0x35>
		} else if (c >= ' ' && i < BUFLEN-1) {
f0105e32:	83 f8 1f             	cmp    $0x1f,%eax
f0105e35:	7e 23                	jle    f0105e5a <readline+0xa3>
f0105e37:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0105e3d:	7f 1b                	jg     f0105e5a <readline+0xa3>
			if (echoing)
f0105e3f:	85 ff                	test   %edi,%edi
f0105e41:	74 0c                	je     f0105e4f <readline+0x98>
				cputchar(c);
f0105e43:	83 ec 0c             	sub    $0xc,%esp
f0105e46:	53                   	push   %ebx
f0105e47:	e8 8f a9 ff ff       	call   f01007db <cputchar>
f0105e4c:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f0105e4f:	88 9e 80 2a 27 f0    	mov    %bl,-0xfd8d580(%esi)
f0105e55:	8d 76 01             	lea    0x1(%esi),%esi
f0105e58:	eb 92                	jmp    f0105dec <readline+0x35>
		} else if (c == '\n' || c == '\r') {
f0105e5a:	83 fb 0a             	cmp    $0xa,%ebx
f0105e5d:	74 05                	je     f0105e64 <readline+0xad>
f0105e5f:	83 fb 0d             	cmp    $0xd,%ebx
f0105e62:	75 88                	jne    f0105dec <readline+0x35>
			if (echoing)
f0105e64:	85 ff                	test   %edi,%edi
f0105e66:	74 0d                	je     f0105e75 <readline+0xbe>
				cputchar('\n');
f0105e68:	83 ec 0c             	sub    $0xc,%esp
f0105e6b:	6a 0a                	push   $0xa
f0105e6d:	e8 69 a9 ff ff       	call   f01007db <cputchar>
f0105e72:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
f0105e75:	c6 86 80 2a 27 f0 00 	movb   $0x0,-0xfd8d580(%esi)
			return buf;
f0105e7c:	b8 80 2a 27 f0       	mov    $0xf0272a80,%eax
f0105e81:	eb 1c                	jmp    f0105e9f <readline+0xe8>
	while (1) {
		c = getchar();
		if (c < 0) {
			cprintf("read error: %e\n", c);
			return NULL;
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0105e83:	85 f6                	test   %esi,%esi
f0105e85:	7f 97                	jg     f0105e1e <readline+0x67>
f0105e87:	eb 09                	jmp    f0105e92 <readline+0xdb>
f0105e89:	85 f6                	test   %esi,%esi
f0105e8b:	7f 91                	jg     f0105e1e <readline+0x67>
f0105e8d:	e9 5a ff ff ff       	jmp    f0105dec <readline+0x35>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
f0105e92:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0105e98:	7e a5                	jle    f0105e3f <readline+0x88>
f0105e9a:	e9 4d ff ff ff       	jmp    f0105dec <readline+0x35>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
f0105e9f:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105ea2:	5b                   	pop    %ebx
f0105ea3:	5e                   	pop    %esi
f0105ea4:	5f                   	pop    %edi
f0105ea5:	5d                   	pop    %ebp
f0105ea6:	c3                   	ret    

f0105ea7 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f0105ea7:	55                   	push   %ebp
f0105ea8:	89 e5                	mov    %esp,%ebp
f0105eaa:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f0105ead:	b8 00 00 00 00       	mov    $0x0,%eax
f0105eb2:	eb 01                	jmp    f0105eb5 <strlen+0xe>
		n++;
f0105eb4:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f0105eb5:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f0105eb9:	75 f9                	jne    f0105eb4 <strlen+0xd>
		n++;
	return n;
}
f0105ebb:	5d                   	pop    %ebp
f0105ebc:	c3                   	ret    

f0105ebd <strnlen>:

int
strnlen(const char *s, size_t size)
{
f0105ebd:	55                   	push   %ebp
f0105ebe:	89 e5                	mov    %esp,%ebp
f0105ec0:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0105ec3:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0105ec6:	ba 00 00 00 00       	mov    $0x0,%edx
f0105ecb:	eb 01                	jmp    f0105ece <strnlen+0x11>
		n++;
f0105ecd:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0105ece:	39 c2                	cmp    %eax,%edx
f0105ed0:	74 08                	je     f0105eda <strnlen+0x1d>
f0105ed2:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
f0105ed6:	75 f5                	jne    f0105ecd <strnlen+0x10>
f0105ed8:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
f0105eda:	5d                   	pop    %ebp
f0105edb:	c3                   	ret    

f0105edc <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f0105edc:	55                   	push   %ebp
f0105edd:	89 e5                	mov    %esp,%ebp
f0105edf:	53                   	push   %ebx
f0105ee0:	8b 45 08             	mov    0x8(%ebp),%eax
f0105ee3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
f0105ee6:	89 c2                	mov    %eax,%edx
f0105ee8:	42                   	inc    %edx
f0105ee9:	41                   	inc    %ecx
f0105eea:	8a 59 ff             	mov    -0x1(%ecx),%bl
f0105eed:	88 5a ff             	mov    %bl,-0x1(%edx)
f0105ef0:	84 db                	test   %bl,%bl
f0105ef2:	75 f4                	jne    f0105ee8 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
f0105ef4:	5b                   	pop    %ebx
f0105ef5:	5d                   	pop    %ebp
f0105ef6:	c3                   	ret    

f0105ef7 <strcat>:

char *
strcat(char *dst, const char *src)
{
f0105ef7:	55                   	push   %ebp
f0105ef8:	89 e5                	mov    %esp,%ebp
f0105efa:	53                   	push   %ebx
f0105efb:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
f0105efe:	53                   	push   %ebx
f0105eff:	e8 a3 ff ff ff       	call   f0105ea7 <strlen>
f0105f04:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
f0105f07:	ff 75 0c             	pushl  0xc(%ebp)
f0105f0a:	01 d8                	add    %ebx,%eax
f0105f0c:	50                   	push   %eax
f0105f0d:	e8 ca ff ff ff       	call   f0105edc <strcpy>
	return dst;
}
f0105f12:	89 d8                	mov    %ebx,%eax
f0105f14:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0105f17:	c9                   	leave  
f0105f18:	c3                   	ret    

f0105f19 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
f0105f19:	55                   	push   %ebp
f0105f1a:	89 e5                	mov    %esp,%ebp
f0105f1c:	56                   	push   %esi
f0105f1d:	53                   	push   %ebx
f0105f1e:	8b 75 08             	mov    0x8(%ebp),%esi
f0105f21:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0105f24:	89 f3                	mov    %esi,%ebx
f0105f26:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0105f29:	89 f2                	mov    %esi,%edx
f0105f2b:	eb 0c                	jmp    f0105f39 <strncpy+0x20>
		*dst++ = *src;
f0105f2d:	42                   	inc    %edx
f0105f2e:	8a 01                	mov    (%ecx),%al
f0105f30:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f0105f33:	80 39 01             	cmpb   $0x1,(%ecx)
f0105f36:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0105f39:	39 da                	cmp    %ebx,%edx
f0105f3b:	75 f0                	jne    f0105f2d <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f0105f3d:	89 f0                	mov    %esi,%eax
f0105f3f:	5b                   	pop    %ebx
f0105f40:	5e                   	pop    %esi
f0105f41:	5d                   	pop    %ebp
f0105f42:	c3                   	ret    

f0105f43 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f0105f43:	55                   	push   %ebp
f0105f44:	89 e5                	mov    %esp,%ebp
f0105f46:	56                   	push   %esi
f0105f47:	53                   	push   %ebx
f0105f48:	8b 75 08             	mov    0x8(%ebp),%esi
f0105f4b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0105f4e:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
f0105f51:	85 c0                	test   %eax,%eax
f0105f53:	74 1e                	je     f0105f73 <strlcpy+0x30>
f0105f55:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f0105f59:	89 f2                	mov    %esi,%edx
f0105f5b:	eb 05                	jmp    f0105f62 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
f0105f5d:	42                   	inc    %edx
f0105f5e:	41                   	inc    %ecx
f0105f5f:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f0105f62:	39 c2                	cmp    %eax,%edx
f0105f64:	74 08                	je     f0105f6e <strlcpy+0x2b>
f0105f66:	8a 19                	mov    (%ecx),%bl
f0105f68:	84 db                	test   %bl,%bl
f0105f6a:	75 f1                	jne    f0105f5d <strlcpy+0x1a>
f0105f6c:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
f0105f6e:	c6 00 00             	movb   $0x0,(%eax)
f0105f71:	eb 02                	jmp    f0105f75 <strlcpy+0x32>
f0105f73:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
f0105f75:	29 f0                	sub    %esi,%eax
}
f0105f77:	5b                   	pop    %ebx
f0105f78:	5e                   	pop    %esi
f0105f79:	5d                   	pop    %ebp
f0105f7a:	c3                   	ret    

f0105f7b <strcmp>:

int
strcmp(const char *p, const char *q)
{
f0105f7b:	55                   	push   %ebp
f0105f7c:	89 e5                	mov    %esp,%ebp
f0105f7e:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0105f81:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
f0105f84:	eb 02                	jmp    f0105f88 <strcmp+0xd>
		p++, q++;
f0105f86:	41                   	inc    %ecx
f0105f87:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f0105f88:	8a 01                	mov    (%ecx),%al
f0105f8a:	84 c0                	test   %al,%al
f0105f8c:	74 04                	je     f0105f92 <strcmp+0x17>
f0105f8e:	3a 02                	cmp    (%edx),%al
f0105f90:	74 f4                	je     f0105f86 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f0105f92:	0f b6 c0             	movzbl %al,%eax
f0105f95:	0f b6 12             	movzbl (%edx),%edx
f0105f98:	29 d0                	sub    %edx,%eax
}
f0105f9a:	5d                   	pop    %ebp
f0105f9b:	c3                   	ret    

f0105f9c <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
f0105f9c:	55                   	push   %ebp
f0105f9d:	89 e5                	mov    %esp,%ebp
f0105f9f:	53                   	push   %ebx
f0105fa0:	8b 45 08             	mov    0x8(%ebp),%eax
f0105fa3:	8b 55 0c             	mov    0xc(%ebp),%edx
f0105fa6:	89 c3                	mov    %eax,%ebx
f0105fa8:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
f0105fab:	eb 02                	jmp    f0105faf <strncmp+0x13>
		n--, p++, q++;
f0105fad:	40                   	inc    %eax
f0105fae:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
f0105faf:	39 d8                	cmp    %ebx,%eax
f0105fb1:	74 14                	je     f0105fc7 <strncmp+0x2b>
f0105fb3:	8a 08                	mov    (%eax),%cl
f0105fb5:	84 c9                	test   %cl,%cl
f0105fb7:	74 04                	je     f0105fbd <strncmp+0x21>
f0105fb9:	3a 0a                	cmp    (%edx),%cl
f0105fbb:	74 f0                	je     f0105fad <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0105fbd:	0f b6 00             	movzbl (%eax),%eax
f0105fc0:	0f b6 12             	movzbl (%edx),%edx
f0105fc3:	29 d0                	sub    %edx,%eax
f0105fc5:	eb 05                	jmp    f0105fcc <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
f0105fc7:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
f0105fcc:	5b                   	pop    %ebx
f0105fcd:	5d                   	pop    %ebp
f0105fce:	c3                   	ret    

f0105fcf <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f0105fcf:	55                   	push   %ebp
f0105fd0:	89 e5                	mov    %esp,%ebp
f0105fd2:	8b 45 08             	mov    0x8(%ebp),%eax
f0105fd5:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0105fd8:	eb 05                	jmp    f0105fdf <strchr+0x10>
		if (*s == c)
f0105fda:	38 ca                	cmp    %cl,%dl
f0105fdc:	74 0c                	je     f0105fea <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f0105fde:	40                   	inc    %eax
f0105fdf:	8a 10                	mov    (%eax),%dl
f0105fe1:	84 d2                	test   %dl,%dl
f0105fe3:	75 f5                	jne    f0105fda <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
f0105fe5:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0105fea:	5d                   	pop    %ebp
f0105feb:	c3                   	ret    

f0105fec <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f0105fec:	55                   	push   %ebp
f0105fed:	89 e5                	mov    %esp,%ebp
f0105fef:	8b 45 08             	mov    0x8(%ebp),%eax
f0105ff2:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0105ff5:	eb 05                	jmp    f0105ffc <strfind+0x10>
		if (*s == c)
f0105ff7:	38 ca                	cmp    %cl,%dl
f0105ff9:	74 07                	je     f0106002 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f0105ffb:	40                   	inc    %eax
f0105ffc:	8a 10                	mov    (%eax),%dl
f0105ffe:	84 d2                	test   %dl,%dl
f0106000:	75 f5                	jne    f0105ff7 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
f0106002:	5d                   	pop    %ebp
f0106003:	c3                   	ret    

f0106004 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
f0106004:	55                   	push   %ebp
f0106005:	89 e5                	mov    %esp,%ebp
f0106007:	57                   	push   %edi
f0106008:	56                   	push   %esi
f0106009:	53                   	push   %ebx
f010600a:	8b 7d 08             	mov    0x8(%ebp),%edi
f010600d:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
f0106010:	85 c9                	test   %ecx,%ecx
f0106012:	74 36                	je     f010604a <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
f0106014:	f7 c7 03 00 00 00    	test   $0x3,%edi
f010601a:	75 28                	jne    f0106044 <memset+0x40>
f010601c:	f6 c1 03             	test   $0x3,%cl
f010601f:	75 23                	jne    f0106044 <memset+0x40>
		c &= 0xFF;
f0106021:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f0106025:	89 d3                	mov    %edx,%ebx
f0106027:	c1 e3 08             	shl    $0x8,%ebx
f010602a:	89 d6                	mov    %edx,%esi
f010602c:	c1 e6 18             	shl    $0x18,%esi
f010602f:	89 d0                	mov    %edx,%eax
f0106031:	c1 e0 10             	shl    $0x10,%eax
f0106034:	09 f0                	or     %esi,%eax
f0106036:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
f0106038:	89 d8                	mov    %ebx,%eax
f010603a:	09 d0                	or     %edx,%eax
f010603c:	c1 e9 02             	shr    $0x2,%ecx
f010603f:	fc                   	cld    
f0106040:	f3 ab                	rep stos %eax,%es:(%edi)
f0106042:	eb 06                	jmp    f010604a <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
f0106044:	8b 45 0c             	mov    0xc(%ebp),%eax
f0106047:	fc                   	cld    
f0106048:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
f010604a:	89 f8                	mov    %edi,%eax
f010604c:	5b                   	pop    %ebx
f010604d:	5e                   	pop    %esi
f010604e:	5f                   	pop    %edi
f010604f:	5d                   	pop    %ebp
f0106050:	c3                   	ret    

f0106051 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
f0106051:	55                   	push   %ebp
f0106052:	89 e5                	mov    %esp,%ebp
f0106054:	57                   	push   %edi
f0106055:	56                   	push   %esi
f0106056:	8b 45 08             	mov    0x8(%ebp),%eax
f0106059:	8b 75 0c             	mov    0xc(%ebp),%esi
f010605c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
f010605f:	39 c6                	cmp    %eax,%esi
f0106061:	73 33                	jae    f0106096 <memmove+0x45>
f0106063:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f0106066:	39 d0                	cmp    %edx,%eax
f0106068:	73 2c                	jae    f0106096 <memmove+0x45>
		s += n;
		d += n;
f010606a:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f010606d:	89 d6                	mov    %edx,%esi
f010606f:	09 fe                	or     %edi,%esi
f0106071:	f7 c6 03 00 00 00    	test   $0x3,%esi
f0106077:	75 13                	jne    f010608c <memmove+0x3b>
f0106079:	f6 c1 03             	test   $0x3,%cl
f010607c:	75 0e                	jne    f010608c <memmove+0x3b>
			asm volatile("std; rep movsl\n"
f010607e:	83 ef 04             	sub    $0x4,%edi
f0106081:	8d 72 fc             	lea    -0x4(%edx),%esi
f0106084:	c1 e9 02             	shr    $0x2,%ecx
f0106087:	fd                   	std    
f0106088:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f010608a:	eb 07                	jmp    f0106093 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
f010608c:	4f                   	dec    %edi
f010608d:	8d 72 ff             	lea    -0x1(%edx),%esi
f0106090:	fd                   	std    
f0106091:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f0106093:	fc                   	cld    
f0106094:	eb 1d                	jmp    f01060b3 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f0106096:	89 f2                	mov    %esi,%edx
f0106098:	09 c2                	or     %eax,%edx
f010609a:	f6 c2 03             	test   $0x3,%dl
f010609d:	75 0f                	jne    f01060ae <memmove+0x5d>
f010609f:	f6 c1 03             	test   $0x3,%cl
f01060a2:	75 0a                	jne    f01060ae <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
f01060a4:	c1 e9 02             	shr    $0x2,%ecx
f01060a7:	89 c7                	mov    %eax,%edi
f01060a9:	fc                   	cld    
f01060aa:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01060ac:	eb 05                	jmp    f01060b3 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
f01060ae:	89 c7                	mov    %eax,%edi
f01060b0:	fc                   	cld    
f01060b1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
f01060b3:	5e                   	pop    %esi
f01060b4:	5f                   	pop    %edi
f01060b5:	5d                   	pop    %ebp
f01060b6:	c3                   	ret    

f01060b7 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
f01060b7:	55                   	push   %ebp
f01060b8:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
f01060ba:	ff 75 10             	pushl  0x10(%ebp)
f01060bd:	ff 75 0c             	pushl  0xc(%ebp)
f01060c0:	ff 75 08             	pushl  0x8(%ebp)
f01060c3:	e8 89 ff ff ff       	call   f0106051 <memmove>
}
f01060c8:	c9                   	leave  
f01060c9:	c3                   	ret    

f01060ca <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
f01060ca:	55                   	push   %ebp
f01060cb:	89 e5                	mov    %esp,%ebp
f01060cd:	56                   	push   %esi
f01060ce:	53                   	push   %ebx
f01060cf:	8b 45 08             	mov    0x8(%ebp),%eax
f01060d2:	8b 55 0c             	mov    0xc(%ebp),%edx
f01060d5:	89 c6                	mov    %eax,%esi
f01060d7:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f01060da:	eb 14                	jmp    f01060f0 <memcmp+0x26>
		if (*s1 != *s2)
f01060dc:	8a 08                	mov    (%eax),%cl
f01060de:	8a 1a                	mov    (%edx),%bl
f01060e0:	38 d9                	cmp    %bl,%cl
f01060e2:	74 0a                	je     f01060ee <memcmp+0x24>
			return (int) *s1 - (int) *s2;
f01060e4:	0f b6 c1             	movzbl %cl,%eax
f01060e7:	0f b6 db             	movzbl %bl,%ebx
f01060ea:	29 d8                	sub    %ebx,%eax
f01060ec:	eb 0b                	jmp    f01060f9 <memcmp+0x2f>
		s1++, s2++;
f01060ee:	40                   	inc    %eax
f01060ef:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f01060f0:	39 f0                	cmp    %esi,%eax
f01060f2:	75 e8                	jne    f01060dc <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f01060f4:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01060f9:	5b                   	pop    %ebx
f01060fa:	5e                   	pop    %esi
f01060fb:	5d                   	pop    %ebp
f01060fc:	c3                   	ret    

f01060fd <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
f01060fd:	55                   	push   %ebp
f01060fe:	89 e5                	mov    %esp,%ebp
f0106100:	53                   	push   %ebx
f0106101:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
f0106104:	89 c1                	mov    %eax,%ecx
f0106106:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
f0106109:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f010610d:	eb 08                	jmp    f0106117 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
f010610f:	0f b6 10             	movzbl (%eax),%edx
f0106112:	39 da                	cmp    %ebx,%edx
f0106114:	74 05                	je     f010611b <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0106116:	40                   	inc    %eax
f0106117:	39 c8                	cmp    %ecx,%eax
f0106119:	72 f4                	jb     f010610f <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
f010611b:	5b                   	pop    %ebx
f010611c:	5d                   	pop    %ebp
f010611d:	c3                   	ret    

f010611e <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f010611e:	55                   	push   %ebp
f010611f:	89 e5                	mov    %esp,%ebp
f0106121:	57                   	push   %edi
f0106122:	56                   	push   %esi
f0106123:	53                   	push   %ebx
f0106124:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0106127:	eb 01                	jmp    f010612a <strtol+0xc>
		s++;
f0106129:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f010612a:	8a 01                	mov    (%ecx),%al
f010612c:	3c 20                	cmp    $0x20,%al
f010612e:	74 f9                	je     f0106129 <strtol+0xb>
f0106130:	3c 09                	cmp    $0x9,%al
f0106132:	74 f5                	je     f0106129 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
f0106134:	3c 2b                	cmp    $0x2b,%al
f0106136:	75 08                	jne    f0106140 <strtol+0x22>
		s++;
f0106138:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0106139:	bf 00 00 00 00       	mov    $0x0,%edi
f010613e:	eb 11                	jmp    f0106151 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f0106140:	3c 2d                	cmp    $0x2d,%al
f0106142:	75 08                	jne    f010614c <strtol+0x2e>
		s++, neg = 1;
f0106144:	41                   	inc    %ecx
f0106145:	bf 01 00 00 00       	mov    $0x1,%edi
f010614a:	eb 05                	jmp    f0106151 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f010614c:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0106151:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0106155:	0f 84 87 00 00 00    	je     f01061e2 <strtol+0xc4>
f010615b:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f010615f:	75 27                	jne    f0106188 <strtol+0x6a>
f0106161:	80 39 30             	cmpb   $0x30,(%ecx)
f0106164:	75 22                	jne    f0106188 <strtol+0x6a>
f0106166:	e9 88 00 00 00       	jmp    f01061f3 <strtol+0xd5>
		s += 2, base = 16;
f010616b:	83 c1 02             	add    $0x2,%ecx
f010616e:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f0106175:	eb 11                	jmp    f0106188 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
f0106177:	41                   	inc    %ecx
f0106178:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f010617f:	eb 07                	jmp    f0106188 <strtol+0x6a>
	else if (base == 0)
		base = 10;
f0106181:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
f0106188:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f010618d:	8a 11                	mov    (%ecx),%dl
f010618f:	8d 5a d0             	lea    -0x30(%edx),%ebx
f0106192:	80 fb 09             	cmp    $0x9,%bl
f0106195:	77 08                	ja     f010619f <strtol+0x81>
			dig = *s - '0';
f0106197:	0f be d2             	movsbl %dl,%edx
f010619a:	83 ea 30             	sub    $0x30,%edx
f010619d:	eb 22                	jmp    f01061c1 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
f010619f:	8d 72 9f             	lea    -0x61(%edx),%esi
f01061a2:	89 f3                	mov    %esi,%ebx
f01061a4:	80 fb 19             	cmp    $0x19,%bl
f01061a7:	77 08                	ja     f01061b1 <strtol+0x93>
			dig = *s - 'a' + 10;
f01061a9:	0f be d2             	movsbl %dl,%edx
f01061ac:	83 ea 57             	sub    $0x57,%edx
f01061af:	eb 10                	jmp    f01061c1 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
f01061b1:	8d 72 bf             	lea    -0x41(%edx),%esi
f01061b4:	89 f3                	mov    %esi,%ebx
f01061b6:	80 fb 19             	cmp    $0x19,%bl
f01061b9:	77 14                	ja     f01061cf <strtol+0xb1>
			dig = *s - 'A' + 10;
f01061bb:	0f be d2             	movsbl %dl,%edx
f01061be:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
f01061c1:	3b 55 10             	cmp    0x10(%ebp),%edx
f01061c4:	7d 09                	jge    f01061cf <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
f01061c6:	41                   	inc    %ecx
f01061c7:	0f af 45 10          	imul   0x10(%ebp),%eax
f01061cb:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
f01061cd:	eb be                	jmp    f010618d <strtol+0x6f>

	if (endptr)
f01061cf:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f01061d3:	74 05                	je     f01061da <strtol+0xbc>
		*endptr = (char *) s;
f01061d5:	8b 75 0c             	mov    0xc(%ebp),%esi
f01061d8:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
f01061da:	85 ff                	test   %edi,%edi
f01061dc:	74 21                	je     f01061ff <strtol+0xe1>
f01061de:	f7 d8                	neg    %eax
f01061e0:	eb 1d                	jmp    f01061ff <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f01061e2:	80 39 30             	cmpb   $0x30,(%ecx)
f01061e5:	75 9a                	jne    f0106181 <strtol+0x63>
f01061e7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f01061eb:	0f 84 7a ff ff ff    	je     f010616b <strtol+0x4d>
f01061f1:	eb 84                	jmp    f0106177 <strtol+0x59>
f01061f3:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f01061f7:	0f 84 6e ff ff ff    	je     f010616b <strtol+0x4d>
f01061fd:	eb 89                	jmp    f0106188 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
f01061ff:	5b                   	pop    %ebx
f0106200:	5e                   	pop    %esi
f0106201:	5f                   	pop    %edi
f0106202:	5d                   	pop    %ebp
f0106203:	c3                   	ret    

f0106204 <mpentry_start>:
.set PROT_MODE_DSEG, 0x10			# kernel data segment selector

.code16
.globl mpentry_start
mpentry_start:
	cli
f0106204:	fa                   	cli    

	xorw	%ax, %ax
f0106205:	31 c0                	xor    %eax,%eax
	movw	%ax, %ds
f0106207:	8e d8                	mov    %eax,%ds
	movw	%ax, %es
f0106209:	8e c0                	mov    %eax,%es
	movw	%ax, %ss
f010620b:	8e d0                	mov    %eax,%ss

	lgdt	MPBOOTPHYS(gdtdesc) # load the GDTR
f010620d:	0f 01 16             	lgdtl  (%esi)
f0106210:	7c 70                	jl     f0106282 <gdtdesc+0x2>
	movl	%cr0, %eax
f0106212:	0f 20 c0             	mov    %cr0,%eax
	orl	$CR0_PE, %eax
f0106215:	66 83 c8 01          	or     $0x1,%ax
	movl	%eax, %cr0
f0106219:	0f 22 c0             	mov    %eax,%cr0

	ljmpl	$(PROT_MODE_CSEG), $(MPBOOTPHYS(start32))
f010621c:	66 ea 20 70 00 00    	ljmpw  $0x0,$0x7020
f0106222:	08 00                	or     %al,(%eax)

f0106224 <start32>:

.code32
start32:
	movw	$(PROT_MODE_DSEG), %ax
f0106224:	66 b8 10 00          	mov    $0x10,%ax
	movw	%ax, %ds
f0106228:	8e d8                	mov    %eax,%ds
	movw	%ax, %es
f010622a:	8e c0                	mov    %eax,%es
	movw	%ax, %ss
f010622c:	8e d0                	mov    %eax,%ss
	movw	$0, %ax
f010622e:	66 b8 00 00          	mov    $0x0,%ax
	movw	%ax, %fs
f0106232:	8e e0                	mov    %eax,%fs
	movw	%ax, %gs
f0106234:	8e e8                	mov    %eax,%gs

	# Set up initial page table. We cannot use kern_pgdir yet because
	# we are still running at a low EIP.
	movl	$(RELOC(entry_pgdir)), %eax
f0106236:	b8 00 20 12 00       	mov    $0x122000,%eax
	movl	%eax, %cr3
f010623b:	0f 22 d8             	mov    %eax,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %eax
f010623e:	0f 20 e0             	mov    %cr4,%eax
	orl	$(CR4_PSE), %eax
f0106241:	83 c8 10             	or     $0x10,%eax
	movl	%eax, %cr4
f0106244:	0f 22 e0             	mov    %eax,%cr4
	# Turn on paging.
	movl	%cr0, %eax
f0106247:	0f 20 c0             	mov    %cr0,%eax
	orl	$(CR0_PE|CR0_PG|CR0_WP), %eax
f010624a:	0d 01 00 01 80       	or     $0x80010001,%eax
	movl	%eax, %cr0
f010624f:	0f 22 c0             	mov    %eax,%cr0

	# Switch to the per-cpu stack allocated in boot_aps()
	movl	mpentry_kstack, %esp
f0106252:	8b 25 14 2f 27 f0    	mov    0xf0272f14,%esp
	movl	$0x0, %ebp			# nuke frame pointer
f0106258:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Call mp_main().  (Exercise for the reader: why the indirect call?)
	movl	$mp_main, %eax
f010625d:	b8 77 02 10 f0       	mov    $0xf0100277,%eax
	call	*%eax
f0106262:	ff d0                	call   *%eax

f0106264 <spin>:

	# If mp_main returns (it shouldn't), loop.
spin:
	jmp	spin
f0106264:	eb fe                	jmp    f0106264 <spin>
f0106266:	66 90                	xchg   %ax,%ax

f0106268 <gdt>:
	...
f0106270:	ff                   	(bad)  
f0106271:	ff 00                	incl   (%eax)
f0106273:	00 00                	add    %al,(%eax)
f0106275:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf
f010627c:	00 92 cf 00 17 00    	add    %dl,0x1700cf(%edx)

f0106280 <gdtdesc>:
f0106280:	17                   	pop    %ss
f0106281:	00 64 70 00          	add    %ah,0x0(%eax,%esi,2)
	...

f0106286 <mpentry_end>:
	.word	0x17				# sizeof(gdt) - 1
	.long	MPBOOTPHYS(gdt)			# address gdt

.globl mpentry_end
mpentry_end:
	nop
f0106286:	90                   	nop

f0106287 <print_table_header>:
		rsdp->revision, rsdp->oem_id);
}

static void
print_table_header(struct acpi_table_header *hdr)
{
f0106287:	55                   	push   %ebp
f0106288:	89 e5                	mov    %esp,%ebp
f010628a:	57                   	push   %edi
f010628b:	56                   	push   %esi
f010628c:	53                   	push   %ebx
f010628d:	83 ec 1c             	sub    $0x1c,%esp
f0106290:	89 c2                	mov    %eax,%edx
	cprintf("ACPI: %.4s %08p %06x v%02d %.6s %.8s %02d %.4s %02d\n",
f0106292:	8b 78 20             	mov    0x20(%eax),%edi
		hdr->signature, PADDR(hdr), hdr->length, hdr->revision,
		hdr->oem_id, hdr->oem_table_id, hdr->oem_revision,
		hdr->asl_compiler_id, hdr->asl_compiler_revision);
f0106295:	8d 70 1c             	lea    0x1c(%eax),%esi
}

static void
print_table_header(struct acpi_table_header *hdr)
{
	cprintf("ACPI: %.4s %08p %06x v%02d %.6s %.8s %02d %.4s %02d\n",
f0106298:	8b 58 18             	mov    0x18(%eax),%ebx
		hdr->signature, PADDR(hdr), hdr->length, hdr->revision,
		hdr->oem_id, hdr->oem_table_id, hdr->oem_revision,
f010629b:	8d 48 10             	lea    0x10(%eax),%ecx
f010629e:	8d 40 0a             	lea    0xa(%eax),%eax
f01062a1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
}

static void
print_table_header(struct acpi_table_header *hdr)
{
	cprintf("ACPI: %.4s %08p %06x v%02d %.6s %.8s %02d %.4s %02d\n",
f01062a4:	0f b6 42 08          	movzbl 0x8(%edx),%eax
f01062a8:	89 45 e0             	mov    %eax,-0x20(%ebp)
f01062ab:	8b 42 04             	mov    0x4(%edx),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01062ae:	81 fa ff ff ff ef    	cmp    $0xefffffff,%edx
f01062b4:	77 12                	ja     f01062c8 <print_table_header+0x41>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01062b6:	52                   	push   %edx
f01062b7:	68 8c 6f 10 f0       	push   $0xf0106f8c
f01062bc:	6a 1e                	push   $0x1e
f01062be:	68 6d 93 10 f0       	push   $0xf010936d
f01062c3:	e8 84 9d ff ff       	call   f010004c <_panic>
f01062c8:	83 ec 08             	sub    $0x8,%esp
f01062cb:	57                   	push   %edi
f01062cc:	56                   	push   %esi
f01062cd:	53                   	push   %ebx
f01062ce:	51                   	push   %ecx
f01062cf:	ff 75 e4             	pushl  -0x1c(%ebp)
f01062d2:	ff 75 e0             	pushl  -0x20(%ebp)
f01062d5:	50                   	push   %eax
f01062d6:	8d 82 00 00 00 10    	lea    0x10000000(%edx),%eax
f01062dc:	50                   	push   %eax
f01062dd:	52                   	push   %edx
f01062de:	68 14 93 10 f0       	push   $0xf0109314
f01062e3:	e8 10 d9 ff ff       	call   f0103bf8 <cprintf>
		hdr->signature, PADDR(hdr), hdr->length, hdr->revision,
		hdr->oem_id, hdr->oem_table_id, hdr->oem_revision,
		hdr->asl_compiler_id, hdr->asl_compiler_revision);
}
f01062e8:	83 c4 30             	add    $0x30,%esp
f01062eb:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01062ee:	5b                   	pop    %ebx
f01062ef:	5e                   	pop    %esi
f01062f0:	5f                   	pop    %edi
f01062f1:	5d                   	pop    %ebp
f01062f2:	c3                   	ret    

f01062f3 <rsdp_search1>:
}

// Look for the RSDP in the len bytes at physical address addr.
static struct acpi_table_rsdp *
rsdp_search1(physaddr_t a, int len)
{
f01062f3:	55                   	push   %ebp
f01062f4:	89 e5                	mov    %esp,%ebp
f01062f6:	57                   	push   %edi
f01062f7:	56                   	push   %esi
f01062f8:	53                   	push   %ebx
f01062f9:	83 ec 0c             	sub    $0xc,%esp
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01062fc:	8b 0d 24 34 27 f0    	mov    0xf0273424,%ecx
f0106302:	89 c3                	mov    %eax,%ebx
f0106304:	c1 eb 0c             	shr    $0xc,%ebx
f0106307:	39 cb                	cmp    %ecx,%ebx
f0106309:	72 12                	jb     f010631d <rsdp_search1+0x2a>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010630b:	50                   	push   %eax
f010630c:	68 68 6f 10 f0       	push   $0xf0106f68
f0106311:	6a 32                	push   $0x32
f0106313:	68 6d 93 10 f0       	push   $0xf010936d
f0106318:	e8 2f 9d ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f010631d:	8d 98 00 00 00 f0    	lea    -0x10000000(%eax),%ebx
	void *p = KADDR(a), *e = KADDR(a + len);
f0106323:	01 d0                	add    %edx,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106325:	89 c2                	mov    %eax,%edx
f0106327:	c1 ea 0c             	shr    $0xc,%edx
f010632a:	39 ca                	cmp    %ecx,%edx
f010632c:	72 12                	jb     f0106340 <rsdp_search1+0x4d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010632e:	50                   	push   %eax
f010632f:	68 68 6f 10 f0       	push   $0xf0106f68
f0106334:	6a 32                	push   $0x32
f0106336:	68 6d 93 10 f0       	push   $0xf010936d
f010633b:	e8 0c 9d ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0106340:	8d b0 00 00 00 f0    	lea    -0x10000000(%eax),%esi

	// The signature is on a 16-byte boundary.
	for (; p < e; p += 16) {
f0106346:	eb 51                	jmp    f0106399 <rsdp_search1+0xa6>
		struct acpi_table_rsdp *rsdp = p;

		if (memcmp(rsdp->signature, ACPI_SIG_RSDP, 8) ||
f0106348:	83 ec 04             	sub    $0x4,%esp
f010634b:	6a 08                	push   $0x8
f010634d:	68 79 93 10 f0       	push   $0xf0109379
f0106352:	53                   	push   %ebx
f0106353:	e8 72 fd ff ff       	call   f01060ca <memcmp>
f0106358:	83 c4 10             	add    $0x10,%esp
f010635b:	85 c0                	test   %eax,%eax
f010635d:	75 37                	jne    f0106396 <rsdp_search1+0xa3>
f010635f:	89 da                	mov    %ebx,%edx
f0106361:	8d 7b 14             	lea    0x14(%ebx),%edi
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
		sum += ((uint8_t *)addr)[i];
f0106364:	0f b6 0a             	movzbl (%edx),%ecx
f0106367:	01 c8                	add    %ecx,%eax
f0106369:	42                   	inc    %edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f010636a:	39 d7                	cmp    %edx,%edi
f010636c:	75 f6                	jne    f0106364 <rsdp_search1+0x71>

	// The signature is on a 16-byte boundary.
	for (; p < e; p += 16) {
		struct acpi_table_rsdp *rsdp = p;

		if (memcmp(rsdp->signature, ACPI_SIG_RSDP, 8) ||
f010636e:	84 c0                	test   %al,%al
f0106370:	75 24                	jne    f0106396 <rsdp_search1+0xa3>
		    sum(rsdp, 20))
			continue;
		// ACPI 2.0+
		if (rsdp->revision && sum(rsdp, rsdp->length))
f0106372:	80 7b 0f 00          	cmpb   $0x0,0xf(%ebx)
f0106376:	74 2c                	je     f01063a4 <rsdp_search1+0xb1>
f0106378:	8b 7b 14             	mov    0x14(%ebx),%edi
static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
f010637b:	ba 00 00 00 00       	mov    $0x0,%edx
	for (i = 0; i < len; i++)
f0106380:	b8 00 00 00 00       	mov    $0x0,%eax
f0106385:	eb 07                	jmp    f010638e <rsdp_search1+0x9b>
		sum += ((uint8_t *)addr)[i];
f0106387:	0f b6 0c 03          	movzbl (%ebx,%eax,1),%ecx
f010638b:	01 ca                	add    %ecx,%edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f010638d:	40                   	inc    %eax
f010638e:	39 c7                	cmp    %eax,%edi
f0106390:	7f f5                	jg     f0106387 <rsdp_search1+0x94>

		if (memcmp(rsdp->signature, ACPI_SIG_RSDP, 8) ||
		    sum(rsdp, 20))
			continue;
		// ACPI 2.0+
		if (rsdp->revision && sum(rsdp, rsdp->length))
f0106392:	84 d2                	test   %dl,%dl
f0106394:	74 12                	je     f01063a8 <rsdp_search1+0xb5>
rsdp_search1(physaddr_t a, int len)
{
	void *p = KADDR(a), *e = KADDR(a + len);

	// The signature is on a 16-byte boundary.
	for (; p < e; p += 16) {
f0106396:	83 c3 10             	add    $0x10,%ebx
f0106399:	39 f3                	cmp    %esi,%ebx
f010639b:	72 ab                	jb     f0106348 <rsdp_search1+0x55>
		// ACPI 2.0+
		if (rsdp->revision && sum(rsdp, rsdp->length))
			continue;
		return rsdp;
	}
	return NULL;
f010639d:	b8 00 00 00 00       	mov    $0x0,%eax
f01063a2:	eb 06                	jmp    f01063aa <rsdp_search1+0xb7>
f01063a4:	89 d8                	mov    %ebx,%eax
f01063a6:	eb 02                	jmp    f01063aa <rsdp_search1+0xb7>
f01063a8:	89 d8                	mov    %ebx,%eax
}
f01063aa:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01063ad:	5b                   	pop    %ebx
f01063ae:	5e                   	pop    %esi
f01063af:	5f                   	pop    %edi
f01063b0:	5d                   	pop    %ebp
f01063b1:	c3                   	ret    

f01063b2 <acpi_init>:
	return rsdp_search1(0xE0000, 0x20000);
}

void
acpi_init(void)
{
f01063b2:	55                   	push   %ebp
f01063b3:	89 e5                	mov    %esp,%ebp
f01063b5:	57                   	push   %edi
f01063b6:	56                   	push   %esi
f01063b7:	53                   	push   %ebx
f01063b8:	83 ec 1c             	sub    $0x1c,%esp
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01063bb:	83 3d 24 34 27 f0 00 	cmpl   $0x0,0xf0273424
f01063c2:	75 16                	jne    f01063da <acpi_init+0x28>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01063c4:	68 0e 04 00 00       	push   $0x40e
f01063c9:	68 68 6f 10 f0       	push   $0xf0106f68
f01063ce:	6a 4d                	push   $0x4d
f01063d0:	68 6d 93 10 f0       	push   $0xf010936d
f01063d5:	e8 72 9c ff ff       	call   f010004c <_panic>
{
	physaddr_t ebda;
	struct acpi_table_rsdp *rsdp;

	// The 16-bit segment of the EBDA is in the two byte at 0x40:0x0E.
	ebda = *(uint16_t *) KADDR(0x40E);
f01063da:	0f b7 05 0e 04 00 f0 	movzwl 0xf000040e,%eax
	ebda <<= 4;
	if ((rsdp = rsdp_search1(ebda, 1024)))
f01063e1:	c1 e0 04             	shl    $0x4,%eax
f01063e4:	ba 00 04 00 00       	mov    $0x400,%edx
f01063e9:	e8 05 ff ff ff       	call   f01062f3 <rsdp_search1>
		return rsdp;
f01063ee:	89 c3                	mov    %eax,%ebx
	struct acpi_table_rsdp *rsdp;

	// The 16-bit segment of the EBDA is in the two byte at 0x40:0x0E.
	ebda = *(uint16_t *) KADDR(0x40E);
	ebda <<= 4;
	if ((rsdp = rsdp_search1(ebda, 1024)))
f01063f0:	85 c0                	test   %eax,%eax
f01063f2:	75 29                	jne    f010641d <acpi_init+0x6b>
		return rsdp;
	return rsdp_search1(0xE0000, 0x20000);
f01063f4:	ba 00 00 02 00       	mov    $0x20000,%edx
f01063f9:	b8 00 00 0e 00       	mov    $0xe0000,%eax
f01063fe:	e8 f0 fe ff ff       	call   f01062f3 <rsdp_search1>
f0106403:	89 c3                	mov    %eax,%ebx
	size_t entry_size;
	void *p, *e;
	uint32_t i;

	rsdp = rsdp_search();
	if (!rsdp)
f0106405:	85 c0                	test   %eax,%eax
f0106407:	75 14                	jne    f010641d <acpi_init+0x6b>
		panic("ACPI: No RSDP found");
f0106409:	83 ec 04             	sub    $0x4,%esp
f010640c:	68 8c 93 10 f0       	push   $0xf010938c
f0106411:	6a 60                	push   $0x60
f0106413:	68 6d 93 10 f0       	push   $0xf010936d
f0106418:	e8 2f 9c ff ff       	call   f010004c <_panic>
static void
print_table_rsdp(struct acpi_table_rsdp *rsdp)
{
	cprintf("ACPI: RSDP %08p %06x v%02d %.6s\n",
		PADDR(rsdp), (rsdp->revision) ? rsdp->length : 20,
		rsdp->revision, rsdp->oem_id);
f010641d:	8d 4b 09             	lea    0x9(%ebx),%ecx
static struct acpi_tables acpi_tables;

static void
print_table_rsdp(struct acpi_table_rsdp *rsdp)
{
	cprintf("ACPI: RSDP %08p %06x v%02d %.6s\n",
f0106420:	0f b6 53 0f          	movzbl 0xf(%ebx),%edx
f0106424:	80 7b 0f 00          	cmpb   $0x0,0xf(%ebx)
f0106428:	74 05                	je     f010642f <acpi_init+0x7d>
f010642a:	8b 43 14             	mov    0x14(%ebx),%eax
f010642d:	eb 05                	jmp    f0106434 <acpi_init+0x82>
f010642f:	b8 14 00 00 00       	mov    $0x14,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0106434:	81 fb ff ff ff ef    	cmp    $0xefffffff,%ebx
f010643a:	77 12                	ja     f010644e <acpi_init+0x9c>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010643c:	53                   	push   %ebx
f010643d:	68 8c 6f 10 f0       	push   $0xf0106f8c
f0106442:	6a 16                	push   $0x16
f0106444:	68 6d 93 10 f0       	push   $0xf010936d
f0106449:	e8 fe 9b ff ff       	call   f010004c <_panic>
f010644e:	83 ec 0c             	sub    $0xc,%esp
f0106451:	51                   	push   %ecx
f0106452:	52                   	push   %edx
f0106453:	50                   	push   %eax
f0106454:	8d 83 00 00 00 10    	lea    0x10000000(%ebx),%eax
f010645a:	50                   	push   %eax
f010645b:	68 4c 93 10 f0       	push   $0xf010934c
f0106460:	e8 93 d7 ff ff       	call   f0103bf8 <cprintf>
	rsdp = rsdp_search();
	if (!rsdp)
		panic("ACPI: No RSDP found");
	print_table_rsdp(rsdp);

	if (rsdp->revision) {
f0106465:	83 c4 20             	add    $0x20,%esp
f0106468:	80 7b 0f 00          	cmpb   $0x0,0xf(%ebx)
f010646c:	74 36                	je     f01064a4 <acpi_init+0xf2>
		hdr = KADDR(rsdp->xsdt_physical_address);
f010646e:	8b 5b 18             	mov    0x18(%ebx),%ebx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106471:	89 d8                	mov    %ebx,%eax
f0106473:	c1 e8 0c             	shr    $0xc,%eax
f0106476:	39 05 24 34 27 f0    	cmp    %eax,0xf0273424
f010647c:	77 12                	ja     f0106490 <acpi_init+0xde>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010647e:	53                   	push   %ebx
f010647f:	68 68 6f 10 f0       	push   $0xf0106f68
f0106484:	6a 64                	push   $0x64
f0106486:	68 6d 93 10 f0       	push   $0xf010936d
f010648b:	e8 bc 9b ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0106490:	81 eb 00 00 00 10    	sub    $0x10000000,%ebx
		sig = ACPI_SIG_XSDT;
		entry_size = 8;
f0106496:	c7 45 dc 08 00 00 00 	movl   $0x8,-0x24(%ebp)
		panic("ACPI: No RSDP found");
	print_table_rsdp(rsdp);

	if (rsdp->revision) {
		hdr = KADDR(rsdp->xsdt_physical_address);
		sig = ACPI_SIG_XSDT;
f010649d:	bf 82 93 10 f0       	mov    $0xf0109382,%edi
f01064a2:	eb 34                	jmp    f01064d8 <acpi_init+0x126>
		entry_size = 8;
	} else {
		hdr = KADDR(rsdp->rsdt_physical_address);
f01064a4:	8b 5b 10             	mov    0x10(%ebx),%ebx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01064a7:	89 d8                	mov    %ebx,%eax
f01064a9:	c1 e8 0c             	shr    $0xc,%eax
f01064ac:	3b 05 24 34 27 f0    	cmp    0xf0273424,%eax
f01064b2:	72 12                	jb     f01064c6 <acpi_init+0x114>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01064b4:	53                   	push   %ebx
f01064b5:	68 68 6f 10 f0       	push   $0xf0106f68
f01064ba:	6a 68                	push   $0x68
f01064bc:	68 6d 93 10 f0       	push   $0xf010936d
f01064c1:	e8 86 9b ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01064c6:	81 eb 00 00 00 10    	sub    $0x10000000,%ebx
		sig = ACPI_SIG_RSDT;
		entry_size = 4;
f01064cc:	c7 45 dc 04 00 00 00 	movl   $0x4,-0x24(%ebp)
		hdr = KADDR(rsdp->xsdt_physical_address);
		sig = ACPI_SIG_XSDT;
		entry_size = 8;
	} else {
		hdr = KADDR(rsdp->rsdt_physical_address);
		sig = ACPI_SIG_RSDT;
f01064d3:	bf 87 93 10 f0       	mov    $0xf0109387,%edi
		entry_size = 4;
	}

	if (memcmp(hdr->signature, sig, 4))
f01064d8:	83 ec 04             	sub    $0x4,%esp
f01064db:	6a 04                	push   $0x4
f01064dd:	57                   	push   %edi
f01064de:	53                   	push   %ebx
f01064df:	e8 e6 fb ff ff       	call   f01060ca <memcmp>
f01064e4:	83 c4 10             	add    $0x10,%esp
f01064e7:	85 c0                	test   %eax,%eax
f01064e9:	74 12                	je     f01064fd <acpi_init+0x14b>
		panic("ACPI: Incorrect %s signature", sig);
f01064eb:	57                   	push   %edi
f01064ec:	68 a0 93 10 f0       	push   $0xf01093a0
f01064f1:	6a 6e                	push   $0x6e
f01064f3:	68 6d 93 10 f0       	push   $0xf010936d
f01064f8:	e8 4f 9b ff ff       	call   f010004c <_panic>
	if (sum(hdr, hdr->length))
f01064fd:	8b 73 04             	mov    0x4(%ebx),%esi
static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
f0106500:	ba 00 00 00 00       	mov    $0x0,%edx
	for (i = 0; i < len; i++)
f0106505:	b8 00 00 00 00       	mov    $0x0,%eax
f010650a:	eb 07                	jmp    f0106513 <acpi_init+0x161>
		sum += ((uint8_t *)addr)[i];
f010650c:	0f b6 0c 03          	movzbl (%ebx,%eax,1),%ecx
f0106510:	01 ca                	add    %ecx,%edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f0106512:	40                   	inc    %eax
f0106513:	39 f0                	cmp    %esi,%eax
f0106515:	7c f5                	jl     f010650c <acpi_init+0x15a>
		entry_size = 4;
	}

	if (memcmp(hdr->signature, sig, 4))
		panic("ACPI: Incorrect %s signature", sig);
	if (sum(hdr, hdr->length))
f0106517:	84 d2                	test   %dl,%dl
f0106519:	74 12                	je     f010652d <acpi_init+0x17b>
		panic("ACPI: Bad %s checksum", sig);
f010651b:	57                   	push   %edi
f010651c:	68 bd 93 10 f0       	push   $0xf01093bd
f0106521:	6a 70                	push   $0x70
f0106523:	68 6d 93 10 f0       	push   $0xf010936d
f0106528:	e8 1f 9b ff ff       	call   f010004c <_panic>
	print_table_header(hdr);
f010652d:	89 d8                	mov    %ebx,%eax
f010652f:	e8 53 fd ff ff       	call   f0106287 <print_table_header>

	p = hdr + 1;
f0106534:	8d 43 24             	lea    0x24(%ebx),%eax
f0106537:	89 c7                	mov    %eax,%edi
	e = (void *)hdr + hdr->length;
f0106539:	89 d8                	mov    %ebx,%eax
f010653b:	03 43 04             	add    0x4(%ebx),%eax
f010653e:	89 45 e0             	mov    %eax,-0x20(%ebp)
	for (i = 0; p < e; p += entry_size) {
f0106541:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f0106548:	89 fb                	mov    %edi,%ebx
f010654a:	e9 89 00 00 00       	jmp    f01065d8 <acpi_init+0x226>
		hdr = KADDR(*(uint32_t *)p);
f010654f:	8b 33                	mov    (%ebx),%esi
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106551:	89 f0                	mov    %esi,%eax
f0106553:	c1 e8 0c             	shr    $0xc,%eax
f0106556:	3b 05 24 34 27 f0    	cmp    0xf0273424,%eax
f010655c:	72 12                	jb     f0106570 <acpi_init+0x1be>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010655e:	56                   	push   %esi
f010655f:	68 68 6f 10 f0       	push   $0xf0106f68
f0106564:	6a 76                	push   $0x76
f0106566:	68 6d 93 10 f0       	push   $0xf010936d
f010656b:	e8 dc 9a ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0106570:	8d 86 00 00 00 f0    	lea    -0x10000000(%esi),%eax
f0106576:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		if (sum(hdr, hdr->length))
f0106579:	8b be 04 00 00 f0    	mov    -0xffffffc(%esi),%edi
static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
f010657f:	ba 00 00 00 00       	mov    $0x0,%edx
	for (i = 0; i < len; i++)
f0106584:	b8 00 00 00 00       	mov    $0x0,%eax
f0106589:	eb 0b                	jmp    f0106596 <acpi_init+0x1e4>
		sum += ((uint8_t *)addr)[i];
f010658b:	0f b6 8c 30 00 00 00 	movzbl -0x10000000(%eax,%esi,1),%ecx
f0106592:	f0 
f0106593:	01 ca                	add    %ecx,%edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f0106595:	40                   	inc    %eax
f0106596:	39 c7                	cmp    %eax,%edi
f0106598:	7f f1                	jg     f010658b <acpi_init+0x1d9>

	p = hdr + 1;
	e = (void *)hdr + hdr->length;
	for (i = 0; p < e; p += entry_size) {
		hdr = KADDR(*(uint32_t *)p);
		if (sum(hdr, hdr->length))
f010659a:	84 d2                	test   %dl,%dl
f010659c:	75 37                	jne    f01065d5 <acpi_init+0x223>
			continue;
		print_table_header(hdr);
f010659e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01065a1:	e8 e1 fc ff ff       	call   f0106287 <print_table_header>
		assert(i < ACPI_NR_MAX);
f01065a6:	83 7d d8 1f          	cmpl   $0x1f,-0x28(%ebp)
f01065aa:	76 16                	jbe    f01065c2 <acpi_init+0x210>
f01065ac:	68 d3 93 10 f0       	push   $0xf01093d3
f01065b1:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01065b6:	6a 7a                	push   $0x7a
f01065b8:	68 6d 93 10 f0       	push   $0xf010936d
f01065bd:	e8 8a 9a ff ff       	call   f010004c <_panic>
		acpi_tables.entries[i++] = hdr;
f01065c2:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01065c5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f01065c8:	89 3c 85 84 2e 27 f0 	mov    %edi,-0xfd8d17c(,%eax,4)
f01065cf:	8d 40 01             	lea    0x1(%eax),%eax
f01065d2:	89 45 d8             	mov    %eax,-0x28(%ebp)
		panic("ACPI: Bad %s checksum", sig);
	print_table_header(hdr);

	p = hdr + 1;
	e = (void *)hdr + hdr->length;
	for (i = 0; p < e; p += entry_size) {
f01065d5:	03 5d dc             	add    -0x24(%ebp),%ebx
f01065d8:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
f01065db:	0f 82 6e ff ff ff    	jb     f010654f <acpi_init+0x19d>
			continue;
		print_table_header(hdr);
		assert(i < ACPI_NR_MAX);
		acpi_tables.entries[i++] = hdr;
	}
	acpi_tables.nr = i;
f01065e1:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01065e4:	a3 80 2e 27 f0       	mov    %eax,0xf0272e80
}
f01065e9:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01065ec:	5b                   	pop    %ebx
f01065ed:	5e                   	pop    %esi
f01065ee:	5f                   	pop    %edi
f01065ef:	5d                   	pop    %ebp
f01065f0:	c3                   	ret    

f01065f1 <acpi_get_table>:

void *
acpi_get_table(const char *signature)
{
f01065f1:	55                   	push   %ebp
f01065f2:	89 e5                	mov    %esp,%ebp
f01065f4:	57                   	push   %edi
f01065f5:	56                   	push   %esi
f01065f6:	53                   	push   %ebx
f01065f7:	83 ec 0c             	sub    $0xc,%esp
f01065fa:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint32_t i;
	struct acpi_table_header **phdr = acpi_tables.entries;
f01065fd:	be 84 2e 27 f0       	mov    $0xf0272e84,%esi

	for (i = 0; i < acpi_tables.nr; ++i, ++phdr) {
f0106602:	bb 00 00 00 00       	mov    $0x0,%ebx
f0106607:	eb 1c                	jmp    f0106625 <acpi_get_table+0x34>
		if (!memcmp((*phdr)->signature, signature, 4))
f0106609:	83 ec 04             	sub    $0x4,%esp
f010660c:	6a 04                	push   $0x4
f010660e:	57                   	push   %edi
f010660f:	ff 36                	pushl  (%esi)
f0106611:	e8 b4 fa ff ff       	call   f01060ca <memcmp>
f0106616:	83 c4 10             	add    $0x10,%esp
f0106619:	85 c0                	test   %eax,%eax
f010661b:	75 04                	jne    f0106621 <acpi_get_table+0x30>
			return *phdr;
f010661d:	8b 06                	mov    (%esi),%eax
f010661f:	eb 11                	jmp    f0106632 <acpi_get_table+0x41>
acpi_get_table(const char *signature)
{
	uint32_t i;
	struct acpi_table_header **phdr = acpi_tables.entries;

	for (i = 0; i < acpi_tables.nr; ++i, ++phdr) {
f0106621:	43                   	inc    %ebx
f0106622:	83 c6 04             	add    $0x4,%esi
f0106625:	3b 1d 80 2e 27 f0    	cmp    0xf0272e80,%ebx
f010662b:	72 dc                	jb     f0106609 <acpi_get_table+0x18>
		if (!memcmp((*phdr)->signature, signature, 4))
			return *phdr;
	}
	return NULL;
f010662d:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0106632:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106635:	5b                   	pop    %ebx
f0106636:	5e                   	pop    %esi
f0106637:	5f                   	pop    %edi
f0106638:	5d                   	pop    %ebp
f0106639:	c3                   	ret    

f010663a <mp_init>:
unsigned char percpu_kstacks[NCPU][KSTKSIZE]
__attribute__ ((aligned(PGSIZE)));

void
mp_init(void)
{
f010663a:	55                   	push   %ebp
f010663b:	89 e5                	mov    %esp,%ebp
f010663d:	56                   	push   %esi
f010663e:	53                   	push   %ebx
	struct acpi_subtable_header *hdr, *end;

	// 5.2.12.1 MADT Processor Local APIC / SAPIC Structure Entry Order
	// * initialize processors in the order that they appear in MADT;
	// * the boot processor is the first processor entry.
	bootcpu->cpu_status = CPU_STARTED;
f010663f:	c7 05 04 40 27 f0 01 	movl   $0x1,0xf0274004
f0106646:	00 00 00 

	madt = acpi_get_table(ACPI_SIG_MADT);
f0106649:	83 ec 0c             	sub    $0xc,%esp
f010664c:	68 e3 93 10 f0       	push   $0xf01093e3
f0106651:	e8 9b ff ff ff       	call   f01065f1 <acpi_get_table>
	if (!madt)
f0106656:	83 c4 10             	add    $0x10,%esp
f0106659:	85 c0                	test   %eax,%eax
f010665b:	75 14                	jne    f0106671 <mp_init+0x37>
		panic("ACPI: No MADT found");
f010665d:	83 ec 04             	sub    $0x4,%esp
f0106660:	68 e8 93 10 f0       	push   $0xf01093e8
f0106665:	6a 1c                	push   $0x1c
f0106667:	68 fc 93 10 f0       	push   $0xf01093fc
f010666c:	e8 db 99 ff ff       	call   f010004c <_panic>

	lapic_addr = madt->address;
f0106671:	8b 50 24             	mov    0x24(%eax),%edx
f0106674:	89 15 00 50 2b f0    	mov    %edx,0xf02b5000

	hdr = (void *)madt + sizeof(*madt);
f010667a:	8d 50 2c             	lea    0x2c(%eax),%edx
	end = (void *)madt + madt->header.length;
f010667d:	03 40 04             	add    0x4(%eax),%eax
	for (; hdr < end; hdr = (void *)hdr + hdr->length) {
f0106680:	eb 54                	jmp    f01066d6 <mp_init+0x9c>
		switch (hdr->type) {
f0106682:	8a 0a                	mov    (%edx),%cl
f0106684:	84 c9                	test   %cl,%cl
f0106686:	74 07                	je     f010668f <mp_init+0x55>
f0106688:	80 f9 01             	cmp    $0x1,%cl
f010668b:	74 34                	je     f01066c1 <mp_init+0x87>
f010668d:	eb 41                	jmp    f01066d0 <mp_init+0x96>
		case ACPI_MADT_TYPE_LOCAL_APIC: {
			struct acpi_madt_local_apic *p = (void *)hdr;
			bool enabled = p->lapic_flags & BIT(0);

			if (ncpu < NCPU && enabled) {
f010668f:	8b 35 a0 43 27 f0    	mov    0xf02743a0,%esi
f0106695:	83 fe 07             	cmp    $0x7,%esi
f0106698:	7f 36                	jg     f01066d0 <mp_init+0x96>
f010669a:	f6 42 04 01          	testb  $0x1,0x4(%edx)
f010669e:	74 30                	je     f01066d0 <mp_init+0x96>
				// Be careful: cpu_apicid may differ from cpus index
				cpus[ncpu].cpu_apicid = p->id;
f01066a0:	8d 1c 36             	lea    (%esi,%esi,1),%ebx
f01066a3:	01 f3                	add    %esi,%ebx
f01066a5:	01 db                	add    %ebx,%ebx
f01066a7:	01 f3                	add    %esi,%ebx
f01066a9:	8d 1c 9e             	lea    (%esi,%ebx,4),%ebx
f01066ac:	8a 4a 03             	mov    0x3(%edx),%cl
f01066af:	88 0c 9d 00 40 27 f0 	mov    %cl,-0xfd8c000(,%ebx,4)
				ncpu++;
f01066b6:	8d 4e 01             	lea    0x1(%esi),%ecx
f01066b9:	89 0d a0 43 27 f0    	mov    %ecx,0xf02743a0
f01066bf:	eb 0f                	jmp    f01066d0 <mp_init+0x96>
		}
		case ACPI_MADT_TYPE_IO_APIC: {
			struct acpi_madt_io_apic *p = (void *)hdr;

			// We use one IOAPIC.
			if (p->global_irq_base == 0)
f01066c1:	83 7a 08 00          	cmpl   $0x0,0x8(%edx)
f01066c5:	75 09                	jne    f01066d0 <mp_init+0x96>
				ioapic_addr = p->address;
f01066c7:	8b 4a 04             	mov    0x4(%edx),%ecx
f01066ca:	89 0d 04 50 2b f0    	mov    %ecx,0xf02b5004

	lapic_addr = madt->address;

	hdr = (void *)madt + sizeof(*madt);
	end = (void *)madt + madt->header.length;
	for (; hdr < end; hdr = (void *)hdr + hdr->length) {
f01066d0:	0f b6 4a 01          	movzbl 0x1(%edx),%ecx
f01066d4:	01 ca                	add    %ecx,%edx
f01066d6:	39 c2                	cmp    %eax,%edx
f01066d8:	72 a8                	jb     f0106682 <mp_init+0x48>
		default:
			break;
		}
	}

	cprintf("SMP: %d CPU(s)\n", ncpu);
f01066da:	83 ec 08             	sub    $0x8,%esp
f01066dd:	ff 35 a0 43 27 f0    	pushl  0xf02743a0
f01066e3:	68 0c 94 10 f0       	push   $0xf010940c
f01066e8:	e8 0b d5 ff ff       	call   f0103bf8 <cprintf>
}
f01066ed:	83 c4 10             	add    $0x10,%esp
f01066f0:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01066f3:	5b                   	pop    %ebx
f01066f4:	5e                   	pop    %esi
f01066f5:	5d                   	pop    %ebp
f01066f6:	c3                   	ret    

f01066f7 <lapic_write>:
	return lapic[index];
}

static void
lapic_write(uint32_t index, uint32_t value)
{
f01066f7:	55                   	push   %ebp
f01066f8:	89 e5                	mov    %esp,%ebp
	lapic[index] = value;
f01066fa:	8b 0d 04 2f 27 f0    	mov    0xf0272f04,%ecx
f0106700:	8d 04 81             	lea    (%ecx,%eax,4),%eax
f0106703:	89 10                	mov    %edx,(%eax)
	lapic[ID];  // wait for write to finish, by reading
f0106705:	8b 41 20             	mov    0x20(%ecx),%eax
}
f0106708:	5d                   	pop    %ebp
f0106709:	c3                   	ret    

f010670a <cpunum>:
int
cpunum(void)
{
	int apicid, i;

	if (!lapic)
f010670a:	a1 04 2f 27 f0       	mov    0xf0272f04,%eax
f010670f:	85 c0                	test   %eax,%eax
f0106711:	74 45                	je     f0106758 <cpunum+0x4e>
	lapic_write(TPR, 0);
}

int
cpunum(void)
{
f0106713:	55                   	push   %ebp
f0106714:	89 e5                	mov    %esp,%ebp
f0106716:	56                   	push   %esi
f0106717:	53                   	push   %ebx
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f0106718:	8b 58 20             	mov    0x20(%eax),%ebx
{
	int apicid, i;

	if (!lapic)
		return 0;
	apicid = lapic_read(ID) >> 24;
f010671b:	c1 eb 18             	shr    $0x18,%ebx
	for (i = 0; i < ncpu; ++i) {
f010671e:	8b 35 a0 43 27 f0    	mov    0xf02743a0,%esi
f0106724:	ba 00 40 27 f0       	mov    $0xf0274000,%edx
f0106729:	b8 00 00 00 00       	mov    $0x0,%eax
f010672e:	eb 0b                	jmp    f010673b <cpunum+0x31>
		if (cpus[i].cpu_apicid == apicid)
f0106730:	0f b6 0a             	movzbl (%edx),%ecx
f0106733:	83 c2 74             	add    $0x74,%edx
f0106736:	39 cb                	cmp    %ecx,%ebx
f0106738:	74 24                	je     f010675e <cpunum+0x54>
	int apicid, i;

	if (!lapic)
		return 0;
	apicid = lapic_read(ID) >> 24;
	for (i = 0; i < ncpu; ++i) {
f010673a:	40                   	inc    %eax
f010673b:	39 f0                	cmp    %esi,%eax
f010673d:	7c f1                	jl     f0106730 <cpunum+0x26>
		if (cpus[i].cpu_apicid == apicid)
			return i;
	}
	assert(0);
f010673f:	68 aa 7f 10 f0       	push   $0xf0107faa
f0106744:	68 d1 6f 10 f0       	push   $0xf0106fd1
f0106749:	68 86 00 00 00       	push   $0x86
f010674e:	68 1c 94 10 f0       	push   $0xf010941c
f0106753:	e8 f4 98 ff ff       	call   f010004c <_panic>
cpunum(void)
{
	int apicid, i;

	if (!lapic)
		return 0;
f0106758:	b8 00 00 00 00       	mov    $0x0,%eax
f010675d:	c3                   	ret    
	for (i = 0; i < ncpu; ++i) {
		if (cpus[i].cpu_apicid == apicid)
			return i;
	}
	assert(0);
}
f010675e:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106761:	5b                   	pop    %ebx
f0106762:	5e                   	pop    %esi
f0106763:	5d                   	pop    %ebp
f0106764:	c3                   	ret    

f0106765 <lapic_init>:
	lapic[ID];  // wait for write to finish, by reading
}

void
lapic_init(void)
{
f0106765:	55                   	push   %ebp
f0106766:	89 e5                	mov    %esp,%ebp
f0106768:	53                   	push   %ebx
f0106769:	83 ec 04             	sub    $0x4,%esp
	assert(lapic_addr);
f010676c:	a1 00 50 2b f0       	mov    0xf02b5000,%eax
f0106771:	85 c0                	test   %eax,%eax
f0106773:	75 16                	jne    f010678b <lapic_init+0x26>
f0106775:	68 29 94 10 f0       	push   $0xf0109429
f010677a:	68 d1 6f 10 f0       	push   $0xf0106fd1
f010677f:	6a 40                	push   $0x40
f0106781:	68 1c 94 10 f0       	push   $0xf010941c
f0106786:	e8 c1 98 ff ff       	call   f010004c <_panic>

	// lapic_addr is the physical address of the LAPIC's 4K MMIO
	// region.  Map it in to virtual memory so we can access it.
	lapic = mmio_map_region(lapic_addr, 4096);
f010678b:	83 ec 08             	sub    $0x8,%esp
f010678e:	68 00 10 00 00       	push   $0x1000
f0106793:	50                   	push   %eax
f0106794:	e8 52 af ff ff       	call   f01016eb <mmio_map_region>
f0106799:	89 c3                	mov    %eax,%ebx
f010679b:	a3 04 2f 27 f0       	mov    %eax,0xf0272f04

	if (thiscpu == bootcpu)
f01067a0:	e8 65 ff ff ff       	call   f010670a <cpunum>
f01067a5:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01067a8:	01 c2                	add    %eax,%edx
f01067aa:	01 d2                	add    %edx,%edx
f01067ac:	01 c2                	add    %eax,%edx
f01067ae:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01067b1:	83 c4 10             	add    $0x10,%esp
f01067b4:	c1 e0 02             	shl    $0x2,%eax
f01067b7:	75 1d                	jne    f01067d6 <lapic_init+0x71>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f01067b9:	8b 43 30             	mov    0x30(%ebx),%eax
	// lapic_addr is the physical address of the LAPIC's 4K MMIO
	// region.  Map it in to virtual memory so we can access it.
	lapic = mmio_map_region(lapic_addr, 4096);

	if (thiscpu == bootcpu)
		cprintf("SMP: LAPIC %08p v%02x\n", lapic_addr, lapic_read(VER) & 0xFF);
f01067bc:	83 ec 04             	sub    $0x4,%esp
f01067bf:	0f b6 c0             	movzbl %al,%eax
f01067c2:	50                   	push   %eax
f01067c3:	ff 35 00 50 2b f0    	pushl  0xf02b5000
f01067c9:	68 34 94 10 f0       	push   $0xf0109434
f01067ce:	e8 25 d4 ff ff       	call   f0103bf8 <cprintf>
f01067d3:	83 c4 10             	add    $0x10,%esp

	// Enable local APIC; set spurious interrupt vector.
	lapic_write(SVR, ENABLE | (IRQ_OFFSET + IRQ_SPURIOUS));
f01067d6:	ba 27 01 00 00       	mov    $0x127,%edx
f01067db:	b8 3c 00 00 00       	mov    $0x3c,%eax
f01067e0:	e8 12 ff ff ff       	call   f01066f7 <lapic_write>

	// The timer repeatedly counts down at bus frequency
	// from lapic[TICR] and then issues an interrupt.
	// If we cared more about precise timekeeping,
	// TICR would be calibrated using an external time source.
	lapic_write(TDCR, X1);
f01067e5:	ba 0b 00 00 00       	mov    $0xb,%edx
f01067ea:	b8 f8 00 00 00       	mov    $0xf8,%eax
f01067ef:	e8 03 ff ff ff       	call   f01066f7 <lapic_write>
	lapic_write(TIMER, PERIODIC | (IRQ_OFFSET + IRQ_TIMER));
f01067f4:	ba 20 00 02 00       	mov    $0x20020,%edx
f01067f9:	b8 c8 00 00 00       	mov    $0xc8,%eax
f01067fe:	e8 f4 fe ff ff       	call   f01066f7 <lapic_write>
	lapic_write(TICR, 10000000);
f0106803:	ba 80 96 98 00       	mov    $0x989680,%edx
f0106808:	b8 e0 00 00 00       	mov    $0xe0,%eax
f010680d:	e8 e5 fe ff ff       	call   f01066f7 <lapic_write>
	//
	// According to Intel MP Specification, the BIOS should initialize
	// BSP's local APIC in Virtual Wire Mode, in which 8259A's
	// INTR is virtually connected to BSP's LINTIN0. In this mode,
	// we do not need to program the IOAPIC.
	if (thiscpu != bootcpu)
f0106812:	e8 f3 fe ff ff       	call   f010670a <cpunum>
f0106817:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010681a:	01 c2                	add    %eax,%edx
f010681c:	01 d2                	add    %edx,%edx
f010681e:	01 c2                	add    %eax,%edx
f0106820:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106823:	c1 e0 02             	shl    $0x2,%eax
f0106826:	74 0f                	je     f0106837 <lapic_init+0xd2>
		lapic_write(LINT0, MASKED);
f0106828:	ba 00 00 01 00       	mov    $0x10000,%edx
f010682d:	b8 d4 00 00 00       	mov    $0xd4,%eax
f0106832:	e8 c0 fe ff ff       	call   f01066f7 <lapic_write>

	// Disable NMI (LINT1) on all CPUs
	lapic_write(LINT1, MASKED);
f0106837:	ba 00 00 01 00       	mov    $0x10000,%edx
f010683c:	b8 d8 00 00 00       	mov    $0xd8,%eax
f0106841:	e8 b1 fe ff ff       	call   f01066f7 <lapic_write>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f0106846:	8b 1d 04 2f 27 f0    	mov    0xf0272f04,%ebx
f010684c:	8b 43 30             	mov    0x30(%ebx),%eax
	// Disable NMI (LINT1) on all CPUs
	lapic_write(LINT1, MASKED);

	// Disable performance counter overflow interrupts
	// on machines that provide that interrupt entry.
	if (((lapic_read(VER)>>16) & 0xFF) >= 4)
f010684f:	c1 e8 10             	shr    $0x10,%eax
f0106852:	3c 03                	cmp    $0x3,%al
f0106854:	76 0f                	jbe    f0106865 <lapic_init+0x100>
		lapic_write(PCINT, MASKED);
f0106856:	ba 00 00 01 00       	mov    $0x10000,%edx
f010685b:	b8 d0 00 00 00       	mov    $0xd0,%eax
f0106860:	e8 92 fe ff ff       	call   f01066f7 <lapic_write>

	// Map error interrupt to IRQ_ERROR.
	lapic_write(ERROR, IRQ_OFFSET + IRQ_ERROR);
f0106865:	ba 33 00 00 00       	mov    $0x33,%edx
f010686a:	b8 dc 00 00 00       	mov    $0xdc,%eax
f010686f:	e8 83 fe ff ff       	call   f01066f7 <lapic_write>

	// Clear error status register (requires back-to-back writes).
	lapic_write(ESR, 0);
f0106874:	ba 00 00 00 00       	mov    $0x0,%edx
f0106879:	b8 a0 00 00 00       	mov    $0xa0,%eax
f010687e:	e8 74 fe ff ff       	call   f01066f7 <lapic_write>
	lapic_write(ESR, 0);
f0106883:	ba 00 00 00 00       	mov    $0x0,%edx
f0106888:	b8 a0 00 00 00       	mov    $0xa0,%eax
f010688d:	e8 65 fe ff ff       	call   f01066f7 <lapic_write>

	// Ack any outstanding interrupts.
	lapic_write(EOI, 0);
f0106892:	ba 00 00 00 00       	mov    $0x0,%edx
f0106897:	b8 2c 00 00 00       	mov    $0x2c,%eax
f010689c:	e8 56 fe ff ff       	call   f01066f7 <lapic_write>

	// Send an Init Level De-Assert to synchronize arbitration ID's.
	lapic_write(ICRHI, 0);
f01068a1:	ba 00 00 00 00       	mov    $0x0,%edx
f01068a6:	b8 c4 00 00 00       	mov    $0xc4,%eax
f01068ab:	e8 47 fe ff ff       	call   f01066f7 <lapic_write>
	lapic_write(ICRLO, BCAST | INIT | LEVEL);
f01068b0:	ba 00 85 08 00       	mov    $0x88500,%edx
f01068b5:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01068ba:	e8 38 fe ff ff       	call   f01066f7 <lapic_write>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f01068bf:	8b 83 00 03 00 00    	mov    0x300(%ebx),%eax
	lapic_write(EOI, 0);

	// Send an Init Level De-Assert to synchronize arbitration ID's.
	lapic_write(ICRHI, 0);
	lapic_write(ICRLO, BCAST | INIT | LEVEL);
	while(lapic_read(ICRLO) & DELIVS)
f01068c5:	f6 c4 10             	test   $0x10,%ah
f01068c8:	75 f5                	jne    f01068bf <lapic_init+0x15a>
		;

	// Enable interrupts on the APIC (but not on the processor).
	lapic_write(TPR, 0);
f01068ca:	ba 00 00 00 00       	mov    $0x0,%edx
f01068cf:	b8 20 00 00 00       	mov    $0x20,%eax
f01068d4:	e8 1e fe ff ff       	call   f01066f7 <lapic_write>
}
f01068d9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01068dc:	c9                   	leave  
f01068dd:	c3                   	ret    

f01068de <lapic_eoi>:

// Acknowledge interrupt.
void
lapic_eoi(void)
{
	if (lapic)
f01068de:	83 3d 04 2f 27 f0 00 	cmpl   $0x0,0xf0272f04
f01068e5:	74 13                	je     f01068fa <lapic_eoi+0x1c>
}

// Acknowledge interrupt.
void
lapic_eoi(void)
{
f01068e7:	55                   	push   %ebp
f01068e8:	89 e5                	mov    %esp,%ebp
	if (lapic)
		lapic_write(EOI, 0);
f01068ea:	ba 00 00 00 00       	mov    $0x0,%edx
f01068ef:	b8 2c 00 00 00       	mov    $0x2c,%eax
f01068f4:	e8 fe fd ff ff       	call   f01066f7 <lapic_write>
}
f01068f9:	5d                   	pop    %ebp
f01068fa:	c3                   	ret    

f01068fb <lapic_startap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapic_startap(uint8_t apicid, uint32_t addr)
{
f01068fb:	55                   	push   %ebp
f01068fc:	89 e5                	mov    %esp,%ebp
f01068fe:	56                   	push   %esi
f01068ff:	53                   	push   %ebx
f0106900:	8b 75 08             	mov    0x8(%ebp),%esi
f0106903:	8b 5d 0c             	mov    0xc(%ebp),%ebx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0106906:	ba 70 00 00 00       	mov    $0x70,%edx
f010690b:	b0 0f                	mov    $0xf,%al
f010690d:	ee                   	out    %al,(%dx)
f010690e:	ba 71 00 00 00       	mov    $0x71,%edx
f0106913:	b0 0a                	mov    $0xa,%al
f0106915:	ee                   	out    %al,(%dx)
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106916:	83 3d 24 34 27 f0 00 	cmpl   $0x0,0xf0273424
f010691d:	75 19                	jne    f0106938 <lapic_startap+0x3d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010691f:	68 67 04 00 00       	push   $0x467
f0106924:	68 68 6f 10 f0       	push   $0xf0106f68
f0106929:	68 a7 00 00 00       	push   $0xa7
f010692e:	68 1c 94 10 f0       	push   $0xf010941c
f0106933:	e8 14 97 ff ff       	call   f010004c <_panic>
	// and the warm reset vector (DWORD based at 40:67) to point at
	// the AP startup code prior to the [universal startup algorithm]."
	outb(IO_RTC, 0xF);  // offset 0xF is shutdown code
	outb(IO_RTC+1, 0x0A);
	wrv = (uint16_t *)KADDR((0x40 << 4 | 0x67));  // Warm reset vector
	wrv[0] = 0;
f0106938:	66 c7 05 67 04 00 f0 	movw   $0x0,0xf0000467
f010693f:	00 00 
	wrv[1] = addr >> 4;
f0106941:	89 d8                	mov    %ebx,%eax
f0106943:	c1 e8 04             	shr    $0x4,%eax
f0106946:	66 a3 69 04 00 f0    	mov    %ax,0xf0000469

	// "Universal startup algorithm."
	// Send INIT (level-triggered) interrupt to reset other CPU.
	lapic_write(ICRHI, apicid << 24);
f010694c:	c1 e6 18             	shl    $0x18,%esi
f010694f:	89 f2                	mov    %esi,%edx
f0106951:	b8 c4 00 00 00       	mov    $0xc4,%eax
f0106956:	e8 9c fd ff ff       	call   f01066f7 <lapic_write>
	lapic_write(ICRLO, INIT | LEVEL | ASSERT);
f010695b:	ba 00 c5 00 00       	mov    $0xc500,%edx
f0106960:	b8 c0 00 00 00       	mov    $0xc0,%eax
f0106965:	e8 8d fd ff ff       	call   f01066f7 <lapic_write>
	microdelay(200);
	lapic_write(ICRLO, INIT | LEVEL);
f010696a:	ba 00 85 00 00       	mov    $0x8500,%edx
f010696f:	b8 c0 00 00 00       	mov    $0xc0,%eax
f0106974:	e8 7e fd ff ff       	call   f01066f7 <lapic_write>
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapic_write(ICRHI, apicid << 24);
		lapic_write(ICRLO, STARTUP | (addr >> 12));
f0106979:	c1 eb 0c             	shr    $0xc,%ebx
f010697c:	80 cf 06             	or     $0x6,%bh
	// Regular hardware is supposed to only accept a STARTUP
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapic_write(ICRHI, apicid << 24);
f010697f:	89 f2                	mov    %esi,%edx
f0106981:	b8 c4 00 00 00       	mov    $0xc4,%eax
f0106986:	e8 6c fd ff ff       	call   f01066f7 <lapic_write>
		lapic_write(ICRLO, STARTUP | (addr >> 12));
f010698b:	89 da                	mov    %ebx,%edx
f010698d:	b8 c0 00 00 00       	mov    $0xc0,%eax
f0106992:	e8 60 fd ff ff       	call   f01066f7 <lapic_write>
	// Regular hardware is supposed to only accept a STARTUP
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapic_write(ICRHI, apicid << 24);
f0106997:	89 f2                	mov    %esi,%edx
f0106999:	b8 c4 00 00 00       	mov    $0xc4,%eax
f010699e:	e8 54 fd ff ff       	call   f01066f7 <lapic_write>
		lapic_write(ICRLO, STARTUP | (addr >> 12));
f01069a3:	89 da                	mov    %ebx,%edx
f01069a5:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01069aa:	e8 48 fd ff ff       	call   f01066f7 <lapic_write>
		microdelay(200);
	}
}
f01069af:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01069b2:	5b                   	pop    %ebx
f01069b3:	5e                   	pop    %esi
f01069b4:	5d                   	pop    %ebp
f01069b5:	c3                   	ret    

f01069b6 <lapic_ipi>:

void
lapic_ipi(int vector)
{
f01069b6:	55                   	push   %ebp
f01069b7:	89 e5                	mov    %esp,%ebp
	lapic_write(ICRLO, OTHERS | FIXED | vector);
f01069b9:	8b 55 08             	mov    0x8(%ebp),%edx
f01069bc:	81 ca 00 00 0c 00    	or     $0xc0000,%edx
f01069c2:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01069c7:	e8 2b fd ff ff       	call   f01066f7 <lapic_write>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f01069cc:	8b 15 04 2f 27 f0    	mov    0xf0272f04,%edx
f01069d2:	8b 82 00 03 00 00    	mov    0x300(%edx),%eax

void
lapic_ipi(int vector)
{
	lapic_write(ICRLO, OTHERS | FIXED | vector);
	while (lapic_read(ICRLO) & DELIVS)
f01069d8:	f6 c4 10             	test   $0x10,%ah
f01069db:	75 f5                	jne    f01069d2 <lapic_ipi+0x1c>
		;
}
f01069dd:	5d                   	pop    %ebp
f01069de:	c3                   	ret    

f01069df <ioapic_init>:
	*(ioapic + IOWIN) = data;
}

void
ioapic_init(void)
{
f01069df:	55                   	push   %ebp
f01069e0:	89 e5                	mov    %esp,%ebp
f01069e2:	56                   	push   %esi
f01069e3:	53                   	push   %ebx
	int i, reg_ver, ver, pins;

	// Default physical address.
	assert(ioapic_addr == 0xfec00000);
f01069e4:	81 3d 04 50 2b f0 00 	cmpl   $0xfec00000,0xf02b5004
f01069eb:	00 c0 fe 
f01069ee:	74 16                	je     f0106a06 <ioapic_init+0x27>
f01069f0:	68 4b 94 10 f0       	push   $0xf010944b
f01069f5:	68 d1 6f 10 f0       	push   $0xf0106fd1
f01069fa:	6a 33                	push   $0x33
f01069fc:	68 65 94 10 f0       	push   $0xf0109465
f0106a01:	e8 46 96 ff ff       	call   f010004c <_panic>

	// IOAPIC is the default physical address.  Map it in to
	// virtual memory so we can access it.
	ioapic = mmio_map_region(ioapic_addr, 4096);
f0106a06:	83 ec 08             	sub    $0x8,%esp
f0106a09:	68 00 10 00 00       	push   $0x1000
f0106a0e:	68 00 00 c0 fe       	push   $0xfec00000
f0106a13:	e8 d3 ac ff ff       	call   f01016eb <mmio_map_region>
f0106a18:	a3 08 2f 27 f0       	mov    %eax,0xf0272f08
static volatile uint32_t *ioapic;

static uint32_t
ioapic_read(int reg)
{
	*(ioapic + IOREGSEL) = reg;
f0106a1d:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
	return *(ioapic + IOWIN);
f0106a23:	8b 40 10             	mov    0x10(%eax),%eax
	// virtual memory so we can access it.
	ioapic = mmio_map_region(ioapic_addr, 4096);

	reg_ver = ioapic_read(IOAPICVER);
	ver = reg_ver & 0xff;
	pins = ((reg_ver >> 16) & 0xff) + 1;
f0106a26:	89 c2                	mov    %eax,%edx
f0106a28:	c1 fa 10             	sar    $0x10,%edx
f0106a2b:	0f b6 d2             	movzbl %dl,%edx
f0106a2e:	8d 5a 01             	lea    0x1(%edx),%ebx
	cprintf("SMP: IOAPIC %08p v%02x [global_irq %02d-%02d]\n",
f0106a31:	89 14 24             	mov    %edx,(%esp)
f0106a34:	6a 00                	push   $0x0
f0106a36:	0f b6 c0             	movzbl %al,%eax
f0106a39:	50                   	push   %eax
f0106a3a:	ff 35 04 50 2b f0    	pushl  0xf02b5004
f0106a40:	68 74 94 10 f0       	push   $0xf0109474
f0106a45:	e8 ae d1 ff ff       	call   f0103bf8 <cprintf>
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a4a:	8b 0d 08 2f 27 f0    	mov    0xf0272f08,%ecx
	cprintf("SMP: IOAPIC %08p v%02x [global_irq %02d-%02d]\n",
		ioapic_addr, ver, 0, pins - 1);

	// Mark all interrupts edge-triggered, active high, disabled,
	// and not routed to any CPUs.
	for (i = 0; i < pins; ++i) {
f0106a50:	83 c4 20             	add    $0x20,%esp
f0106a53:	ba 10 00 00 00       	mov    $0x10,%edx
f0106a58:	b8 00 00 00 00       	mov    $0x0,%eax
f0106a5d:	eb 1e                	jmp    f0106a7d <ioapic_init+0x9e>
		ioapic_write(IOREDTBL+2*i, INT_DISABLED | (IRQ_OFFSET + i));
f0106a5f:	8d 70 20             	lea    0x20(%eax),%esi
f0106a62:	81 ce 00 00 01 00    	or     $0x10000,%esi
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a68:	89 11                	mov    %edx,(%ecx)
	*(ioapic + IOWIN) = data;
f0106a6a:	89 71 10             	mov    %esi,0x10(%ecx)
f0106a6d:	8d 72 01             	lea    0x1(%edx),%esi
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a70:	89 31                	mov    %esi,(%ecx)
	*(ioapic + IOWIN) = data;
f0106a72:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
	cprintf("SMP: IOAPIC %08p v%02x [global_irq %02d-%02d]\n",
		ioapic_addr, ver, 0, pins - 1);

	// Mark all interrupts edge-triggered, active high, disabled,
	// and not routed to any CPUs.
	for (i = 0; i < pins; ++i) {
f0106a79:	40                   	inc    %eax
f0106a7a:	83 c2 02             	add    $0x2,%edx
f0106a7d:	39 d8                	cmp    %ebx,%eax
f0106a7f:	7c de                	jl     f0106a5f <ioapic_init+0x80>
		ioapic_write(IOREDTBL+2*i, INT_DISABLED | (IRQ_OFFSET + i));
		ioapic_write(IOREDTBL+2*i+1, 0);
	}
}
f0106a81:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106a84:	5b                   	pop    %ebx
f0106a85:	5e                   	pop    %esi
f0106a86:	5d                   	pop    %ebp
f0106a87:	c3                   	ret    

f0106a88 <ioapic_enable>:

void
ioapic_enable(int irq, int apicid)
{
f0106a88:	55                   	push   %ebp
f0106a89:	89 e5                	mov    %esp,%ebp
f0106a8b:	8b 45 08             	mov    0x8(%ebp),%eax
	// Mark interrupt edge-triggered, active high, enabled,
	// and routed to the given cpu's APIC ID.
	ioapic_write(IOREDTBL+2*irq, IRQ_OFFSET + irq);
f0106a8e:	8d 48 20             	lea    0x20(%eax),%ecx
f0106a91:	8d 54 00 10          	lea    0x10(%eax,%eax,1),%edx
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a95:	a1 08 2f 27 f0       	mov    0xf0272f08,%eax
f0106a9a:	89 10                	mov    %edx,(%eax)
	*(ioapic + IOWIN) = data;
f0106a9c:	89 48 10             	mov    %ecx,0x10(%eax)
ioapic_enable(int irq, int apicid)
{
	// Mark interrupt edge-triggered, active high, enabled,
	// and routed to the given cpu's APIC ID.
	ioapic_write(IOREDTBL+2*irq, IRQ_OFFSET + irq);
	ioapic_write(IOREDTBL+2*irq+1, apicid << 24);
f0106a9f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0106aa2:	c1 e1 18             	shl    $0x18,%ecx
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106aa5:	42                   	inc    %edx
f0106aa6:	89 10                	mov    %edx,(%eax)
	*(ioapic + IOWIN) = data;
f0106aa8:	89 48 10             	mov    %ecx,0x10(%eax)
{
	// Mark interrupt edge-triggered, active high, enabled,
	// and routed to the given cpu's APIC ID.
	ioapic_write(IOREDTBL+2*irq, IRQ_OFFSET + irq);
	ioapic_write(IOREDTBL+2*irq+1, apicid << 24);
}
f0106aab:	5d                   	pop    %ebp
f0106aac:	c3                   	ret    

f0106aad <__spin_initlock>:
}
#endif

void
__spin_initlock(struct spinlock *lk, char *name)
{
f0106aad:	55                   	push   %ebp
f0106aae:	89 e5                	mov    %esp,%ebp
f0106ab0:	8b 45 08             	mov    0x8(%ebp),%eax
	lk->locked = 0;
f0106ab3:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
#ifdef DEBUG_SPINLOCK
	lk->name = name;
f0106ab9:	8b 55 0c             	mov    0xc(%ebp),%edx
f0106abc:	89 50 04             	mov    %edx,0x4(%eax)
	lk->cpu = 0;
f0106abf:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
#endif
}
f0106ac6:	5d                   	pop    %ebp
f0106ac7:	c3                   	ret    

f0106ac8 <spin_lock>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
spin_lock(struct spinlock *lk)
{
f0106ac8:	55                   	push   %ebp
f0106ac9:	89 e5                	mov    %esp,%ebp
f0106acb:	56                   	push   %esi
f0106acc:	53                   	push   %ebx
f0106acd:	8b 5d 08             	mov    0x8(%ebp),%ebx

// Check whether this CPU is holding the lock.
static int
holding(struct spinlock *lock)
{
	return lock->locked && lock->cpu == thiscpu;
f0106ad0:	83 3b 00             	cmpl   $0x0,(%ebx)
f0106ad3:	74 1f                	je     f0106af4 <spin_lock+0x2c>
f0106ad5:	8b 73 08             	mov    0x8(%ebx),%esi
f0106ad8:	e8 2d fc ff ff       	call   f010670a <cpunum>
f0106add:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0106ae0:	01 c2                	add    %eax,%edx
f0106ae2:	01 d2                	add    %edx,%edx
f0106ae4:	01 c2                	add    %eax,%edx
f0106ae6:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106ae9:	8d 04 85 00 40 27 f0 	lea    -0xfd8c000(,%eax,4),%eax
// other CPUs to waste time spinning to acquire it.
void
spin_lock(struct spinlock *lk)
{
#ifdef DEBUG_SPINLOCK
	if (holding(lk))
f0106af0:	39 c6                	cmp    %eax,%esi
f0106af2:	74 07                	je     f0106afb <spin_lock+0x33>
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0106af4:	ba 01 00 00 00       	mov    $0x1,%edx
f0106af9:	eb 20                	jmp    f0106b1b <spin_lock+0x53>
		panic("CPU %d cannot acquire %s: already holding", cpunum(), lk->name);
f0106afb:	8b 5b 04             	mov    0x4(%ebx),%ebx
f0106afe:	e8 07 fc ff ff       	call   f010670a <cpunum>
f0106b03:	83 ec 0c             	sub    $0xc,%esp
f0106b06:	53                   	push   %ebx
f0106b07:	50                   	push   %eax
f0106b08:	68 a4 94 10 f0       	push   $0xf01094a4
f0106b0d:	6a 41                	push   $0x41
f0106b0f:	68 08 95 10 f0       	push   $0xf0109508
f0106b14:	e8 33 95 ff ff       	call   f010004c <_panic>

	// The xchg is atomic.
	// It also serializes, so that reads after acquire are not
	// reordered before it. 
	while (xchg(&lk->locked, 1) != 0)
		asm volatile ("pause");
f0106b19:	f3 90                	pause  
f0106b1b:	89 d0                	mov    %edx,%eax
f0106b1d:	f0 87 03             	lock xchg %eax,(%ebx)
#endif

	// The xchg is atomic.
	// It also serializes, so that reads after acquire are not
	// reordered before it. 
	while (xchg(&lk->locked, 1) != 0)
f0106b20:	85 c0                	test   %eax,%eax
f0106b22:	75 f5                	jne    f0106b19 <spin_lock+0x51>
		asm volatile ("pause");

	// Record info about lock acquisition for debugging.
#ifdef DEBUG_SPINLOCK
	lk->cpu = thiscpu;
f0106b24:	e8 e1 fb ff ff       	call   f010670a <cpunum>
f0106b29:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0106b2c:	01 c2                	add    %eax,%edx
f0106b2e:	01 d2                	add    %edx,%edx
f0106b30:	01 c2                	add    %eax,%edx
f0106b32:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106b35:	8d 04 85 00 40 27 f0 	lea    -0xfd8c000(,%eax,4),%eax
f0106b3c:	89 43 08             	mov    %eax,0x8(%ebx)
	get_caller_pcs(lk->pcs);
f0106b3f:	83 c3 0c             	add    $0xc,%ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f0106b42:	89 ea                	mov    %ebp,%edx
{
	uint32_t *ebp;
	int i;

	ebp = (uint32_t *)read_ebp();
	for (i = 0; i < 10; i++){
f0106b44:	b8 00 00 00 00       	mov    $0x0,%eax
		if (ebp == 0 || ebp < (uint32_t *)ULIM)
f0106b49:	81 fa ff ff 7f ef    	cmp    $0xef7fffff,%edx
f0106b4f:	76 18                	jbe    f0106b69 <spin_lock+0xa1>
			break;
		pcs[i] = ebp[1];          // saved %eip
f0106b51:	8b 4a 04             	mov    0x4(%edx),%ecx
f0106b54:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
		ebp = (uint32_t *)ebp[0]; // saved %ebp
f0106b57:	8b 12                	mov    (%edx),%edx
{
	uint32_t *ebp;
	int i;

	ebp = (uint32_t *)read_ebp();
	for (i = 0; i < 10; i++){
f0106b59:	40                   	inc    %eax
f0106b5a:	83 f8 0a             	cmp    $0xa,%eax
f0106b5d:	75 ea                	jne    f0106b49 <spin_lock+0x81>
f0106b5f:	eb 0d                	jmp    f0106b6e <spin_lock+0xa6>
			break;
		pcs[i] = ebp[1];          // saved %eip
		ebp = (uint32_t *)ebp[0]; // saved %ebp
	}
	for (; i < 10; i++)
		pcs[i] = 0;
f0106b61:	c7 04 83 00 00 00 00 	movl   $0x0,(%ebx,%eax,4)
		if (ebp == 0 || ebp < (uint32_t *)ULIM)
			break;
		pcs[i] = ebp[1];          // saved %eip
		ebp = (uint32_t *)ebp[0]; // saved %ebp
	}
	for (; i < 10; i++)
f0106b68:	40                   	inc    %eax
f0106b69:	83 f8 09             	cmp    $0x9,%eax
f0106b6c:	7e f3                	jle    f0106b61 <spin_lock+0x99>
	// Record info about lock acquisition for debugging.
#ifdef DEBUG_SPINLOCK
	lk->cpu = thiscpu;
	get_caller_pcs(lk->pcs);
#endif
}
f0106b6e:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106b71:	5b                   	pop    %ebx
f0106b72:	5e                   	pop    %esi
f0106b73:	5d                   	pop    %ebp
f0106b74:	c3                   	ret    

f0106b75 <spin_unlock>:

// Release the lock.
void
spin_unlock(struct spinlock *lk)
{
f0106b75:	55                   	push   %ebp
f0106b76:	89 e5                	mov    %esp,%ebp
f0106b78:	57                   	push   %edi
f0106b79:	56                   	push   %esi
f0106b7a:	53                   	push   %ebx
f0106b7b:	83 ec 4c             	sub    $0x4c,%esp
f0106b7e:	8b 75 08             	mov    0x8(%ebp),%esi

// Check whether this CPU is holding the lock.
static int
holding(struct spinlock *lock)
{
	return lock->locked && lock->cpu == thiscpu;
f0106b81:	83 3e 00             	cmpl   $0x0,(%esi)
f0106b84:	74 23                	je     f0106ba9 <spin_unlock+0x34>
f0106b86:	8b 5e 08             	mov    0x8(%esi),%ebx
f0106b89:	e8 7c fb ff ff       	call   f010670a <cpunum>
f0106b8e:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0106b91:	01 c2                	add    %eax,%edx
f0106b93:	01 d2                	add    %edx,%edx
f0106b95:	01 c2                	add    %eax,%edx
f0106b97:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106b9a:	8d 04 85 00 40 27 f0 	lea    -0xfd8c000(,%eax,4),%eax
// Release the lock.
void
spin_unlock(struct spinlock *lk)
{
#ifdef DEBUG_SPINLOCK
	if (!holding(lk)) {
f0106ba1:	39 c3                	cmp    %eax,%ebx
f0106ba3:	0f 84 b1 00 00 00    	je     f0106c5a <spin_unlock+0xe5>
		int i;
		uint32_t pcs[10];
		// Nab the acquiring EIP chain before it gets released
		memmove(pcs, lk->pcs, sizeof pcs);
f0106ba9:	83 ec 04             	sub    $0x4,%esp
f0106bac:	6a 28                	push   $0x28
f0106bae:	8d 46 0c             	lea    0xc(%esi),%eax
f0106bb1:	50                   	push   %eax
f0106bb2:	8d 5d c0             	lea    -0x40(%ebp),%ebx
f0106bb5:	53                   	push   %ebx
f0106bb6:	e8 96 f4 ff ff       	call   f0106051 <memmove>
		cprintf("CPU %d cannot release %s: held by CPU %d\nAcquired at:", 
f0106bbb:	8b 7e 08             	mov    0x8(%esi),%edi
f0106bbe:	81 ef 00 40 27 f0    	sub    $0xf0274000,%edi
f0106bc4:	c1 ff 02             	sar    $0x2,%edi
f0106bc7:	69 ff 35 c2 72 4f    	imul   $0x4f72c235,%edi,%edi
f0106bcd:	8b 76 04             	mov    0x4(%esi),%esi
f0106bd0:	e8 35 fb ff ff       	call   f010670a <cpunum>
f0106bd5:	57                   	push   %edi
f0106bd6:	56                   	push   %esi
f0106bd7:	50                   	push   %eax
f0106bd8:	68 d0 94 10 f0       	push   $0xf01094d0
f0106bdd:	e8 16 d0 ff ff       	call   f0103bf8 <cprintf>
f0106be2:	83 c4 20             	add    $0x20,%esp
			cpunum(), lk->name, lk->cpu - cpus);
		for (i = 0; i < 10 && pcs[i]; i++) {
			struct Eipdebuginfo info;
			if (debuginfo_eip(pcs[i], &info) >= 0)
f0106be5:	8d 7d a8             	lea    -0x58(%ebp),%edi
f0106be8:	eb 54                	jmp    f0106c3e <spin_unlock+0xc9>
f0106bea:	83 ec 08             	sub    $0x8,%esp
f0106bed:	57                   	push   %edi
f0106bee:	50                   	push   %eax
f0106bef:	e8 69 e8 ff ff       	call   f010545d <debuginfo_eip>
f0106bf4:	83 c4 10             	add    $0x10,%esp
f0106bf7:	85 c0                	test   %eax,%eax
f0106bf9:	78 27                	js     f0106c22 <spin_unlock+0xad>
				cprintf("  %08x %s:%d: %.*s+%x\n", pcs[i],
					info.eip_file, info.eip_line,
					info.eip_fn_namelen, info.eip_fn_name,
					pcs[i] - info.eip_fn_addr);
f0106bfb:	8b 06                	mov    (%esi),%eax
		cprintf("CPU %d cannot release %s: held by CPU %d\nAcquired at:", 
			cpunum(), lk->name, lk->cpu - cpus);
		for (i = 0; i < 10 && pcs[i]; i++) {
			struct Eipdebuginfo info;
			if (debuginfo_eip(pcs[i], &info) >= 0)
				cprintf("  %08x %s:%d: %.*s+%x\n", pcs[i],
f0106bfd:	83 ec 04             	sub    $0x4,%esp
f0106c00:	89 c2                	mov    %eax,%edx
f0106c02:	2b 55 b8             	sub    -0x48(%ebp),%edx
f0106c05:	52                   	push   %edx
f0106c06:	ff 75 b0             	pushl  -0x50(%ebp)
f0106c09:	ff 75 b4             	pushl  -0x4c(%ebp)
f0106c0c:	ff 75 ac             	pushl  -0x54(%ebp)
f0106c0f:	ff 75 a8             	pushl  -0x58(%ebp)
f0106c12:	50                   	push   %eax
f0106c13:	68 18 95 10 f0       	push   $0xf0109518
f0106c18:	e8 db cf ff ff       	call   f0103bf8 <cprintf>
f0106c1d:	83 c4 20             	add    $0x20,%esp
f0106c20:	eb 12                	jmp    f0106c34 <spin_unlock+0xbf>
					info.eip_file, info.eip_line,
					info.eip_fn_namelen, info.eip_fn_name,
					pcs[i] - info.eip_fn_addr);
			else
				cprintf("  %08x\n", pcs[i]);
f0106c22:	83 ec 08             	sub    $0x8,%esp
f0106c25:	ff 36                	pushl  (%esi)
f0106c27:	68 2f 95 10 f0       	push   $0xf010952f
f0106c2c:	e8 c7 cf ff ff       	call   f0103bf8 <cprintf>
f0106c31:	83 c4 10             	add    $0x10,%esp
f0106c34:	83 c3 04             	add    $0x4,%ebx
		uint32_t pcs[10];
		// Nab the acquiring EIP chain before it gets released
		memmove(pcs, lk->pcs, sizeof pcs);
		cprintf("CPU %d cannot release %s: held by CPU %d\nAcquired at:", 
			cpunum(), lk->name, lk->cpu - cpus);
		for (i = 0; i < 10 && pcs[i]; i++) {
f0106c37:	8d 45 e8             	lea    -0x18(%ebp),%eax
f0106c3a:	39 c3                	cmp    %eax,%ebx
f0106c3c:	74 08                	je     f0106c46 <spin_unlock+0xd1>
f0106c3e:	89 de                	mov    %ebx,%esi
f0106c40:	8b 03                	mov    (%ebx),%eax
f0106c42:	85 c0                	test   %eax,%eax
f0106c44:	75 a4                	jne    f0106bea <spin_unlock+0x75>
					info.eip_fn_namelen, info.eip_fn_name,
					pcs[i] - info.eip_fn_addr);
			else
				cprintf("  %08x\n", pcs[i]);
		}
		panic("spin_unlock");
f0106c46:	83 ec 04             	sub    $0x4,%esp
f0106c49:	68 37 95 10 f0       	push   $0xf0109537
f0106c4e:	6a 67                	push   $0x67
f0106c50:	68 08 95 10 f0       	push   $0xf0109508
f0106c55:	e8 f2 93 ff ff       	call   f010004c <_panic>
	}

	lk->pcs[0] = 0;
f0106c5a:	c7 46 0c 00 00 00 00 	movl   $0x0,0xc(%esi)
	lk->cpu = 0;
f0106c61:	c7 46 08 00 00 00 00 	movl   $0x0,0x8(%esi)
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0106c68:	b8 00 00 00 00       	mov    $0x0,%eax
f0106c6d:	f0 87 06             	lock xchg %eax,(%esi)
	// Paper says that Intel 64 and IA-32 will not move a load
	// after a store. So lock->locked = 0 would work here.
	// The xchg being asm volatile ensures gcc emits it after
	// the above assignments (and after the critical section).
	xchg(&lk->locked, 0);
}
f0106c70:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106c73:	5b                   	pop    %ebx
f0106c74:	5e                   	pop    %esi
f0106c75:	5f                   	pop    %edi
f0106c76:	5d                   	pop    %ebp
f0106c77:	c3                   	ret    

f0106c78 <time_tick>:
// This should be called once per timer interrupt.  A timer interrupt
// fires every 10 ms.
void
time_tick(void)
{
	ticks++;
f0106c78:	a1 0c 2f 27 f0       	mov    0xf0272f0c,%eax
f0106c7d:	40                   	inc    %eax
f0106c7e:	a3 0c 2f 27 f0       	mov    %eax,0xf0272f0c
	if (ticks * 10 < ticks)
f0106c83:	8d 14 80             	lea    (%eax,%eax,4),%edx
f0106c86:	01 d2                	add    %edx,%edx
f0106c88:	39 d0                	cmp    %edx,%eax
f0106c8a:	76 17                	jbe    f0106ca3 <time_tick+0x2b>

// This should be called once per timer interrupt.  A timer interrupt
// fires every 10 ms.
void
time_tick(void)
{
f0106c8c:	55                   	push   %ebp
f0106c8d:	89 e5                	mov    %esp,%ebp
f0106c8f:	83 ec 0c             	sub    $0xc,%esp
	ticks++;
	if (ticks * 10 < ticks)
		panic("time_tick: time overflowed");
f0106c92:	68 4f 95 10 f0       	push   $0xf010954f
f0106c97:	6a 0d                	push   $0xd
f0106c99:	68 6a 95 10 f0       	push   $0xf010956a
f0106c9e:	e8 a9 93 ff ff       	call   f010004c <_panic>
f0106ca3:	c3                   	ret    

f0106ca4 <time_msec>:
}

unsigned int
time_msec(void)
{
f0106ca4:	55                   	push   %ebp
f0106ca5:	89 e5                	mov    %esp,%ebp
	return ticks * 10;
f0106ca7:	a1 0c 2f 27 f0       	mov    0xf0272f0c,%eax
f0106cac:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0106caf:	01 c0                	add    %eax,%eax
}
f0106cb1:	5d                   	pop    %ebp
f0106cb2:	c3                   	ret    
f0106cb3:	90                   	nop

f0106cb4 <__udivdi3>:
f0106cb4:	55                   	push   %ebp
f0106cb5:	57                   	push   %edi
f0106cb6:	56                   	push   %esi
f0106cb7:	53                   	push   %ebx
f0106cb8:	83 ec 1c             	sub    $0x1c,%esp
f0106cbb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f0106cbf:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f0106cc3:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0106cc7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0106ccb:	89 ca                	mov    %ecx,%edx
f0106ccd:	89 f8                	mov    %edi,%eax
f0106ccf:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f0106cd3:	85 f6                	test   %esi,%esi
f0106cd5:	75 2d                	jne    f0106d04 <__udivdi3+0x50>
f0106cd7:	39 cf                	cmp    %ecx,%edi
f0106cd9:	77 65                	ja     f0106d40 <__udivdi3+0x8c>
f0106cdb:	89 fd                	mov    %edi,%ebp
f0106cdd:	85 ff                	test   %edi,%edi
f0106cdf:	75 0b                	jne    f0106cec <__udivdi3+0x38>
f0106ce1:	b8 01 00 00 00       	mov    $0x1,%eax
f0106ce6:	31 d2                	xor    %edx,%edx
f0106ce8:	f7 f7                	div    %edi
f0106cea:	89 c5                	mov    %eax,%ebp
f0106cec:	31 d2                	xor    %edx,%edx
f0106cee:	89 c8                	mov    %ecx,%eax
f0106cf0:	f7 f5                	div    %ebp
f0106cf2:	89 c1                	mov    %eax,%ecx
f0106cf4:	89 d8                	mov    %ebx,%eax
f0106cf6:	f7 f5                	div    %ebp
f0106cf8:	89 cf                	mov    %ecx,%edi
f0106cfa:	89 fa                	mov    %edi,%edx
f0106cfc:	83 c4 1c             	add    $0x1c,%esp
f0106cff:	5b                   	pop    %ebx
f0106d00:	5e                   	pop    %esi
f0106d01:	5f                   	pop    %edi
f0106d02:	5d                   	pop    %ebp
f0106d03:	c3                   	ret    
f0106d04:	39 ce                	cmp    %ecx,%esi
f0106d06:	77 28                	ja     f0106d30 <__udivdi3+0x7c>
f0106d08:	0f bd fe             	bsr    %esi,%edi
f0106d0b:	83 f7 1f             	xor    $0x1f,%edi
f0106d0e:	75 40                	jne    f0106d50 <__udivdi3+0x9c>
f0106d10:	39 ce                	cmp    %ecx,%esi
f0106d12:	72 0a                	jb     f0106d1e <__udivdi3+0x6a>
f0106d14:	3b 44 24 08          	cmp    0x8(%esp),%eax
f0106d18:	0f 87 9e 00 00 00    	ja     f0106dbc <__udivdi3+0x108>
f0106d1e:	b8 01 00 00 00       	mov    $0x1,%eax
f0106d23:	89 fa                	mov    %edi,%edx
f0106d25:	83 c4 1c             	add    $0x1c,%esp
f0106d28:	5b                   	pop    %ebx
f0106d29:	5e                   	pop    %esi
f0106d2a:	5f                   	pop    %edi
f0106d2b:	5d                   	pop    %ebp
f0106d2c:	c3                   	ret    
f0106d2d:	8d 76 00             	lea    0x0(%esi),%esi
f0106d30:	31 ff                	xor    %edi,%edi
f0106d32:	31 c0                	xor    %eax,%eax
f0106d34:	89 fa                	mov    %edi,%edx
f0106d36:	83 c4 1c             	add    $0x1c,%esp
f0106d39:	5b                   	pop    %ebx
f0106d3a:	5e                   	pop    %esi
f0106d3b:	5f                   	pop    %edi
f0106d3c:	5d                   	pop    %ebp
f0106d3d:	c3                   	ret    
f0106d3e:	66 90                	xchg   %ax,%ax
f0106d40:	89 d8                	mov    %ebx,%eax
f0106d42:	f7 f7                	div    %edi
f0106d44:	31 ff                	xor    %edi,%edi
f0106d46:	89 fa                	mov    %edi,%edx
f0106d48:	83 c4 1c             	add    $0x1c,%esp
f0106d4b:	5b                   	pop    %ebx
f0106d4c:	5e                   	pop    %esi
f0106d4d:	5f                   	pop    %edi
f0106d4e:	5d                   	pop    %ebp
f0106d4f:	c3                   	ret    
f0106d50:	bd 20 00 00 00       	mov    $0x20,%ebp
f0106d55:	89 eb                	mov    %ebp,%ebx
f0106d57:	29 fb                	sub    %edi,%ebx
f0106d59:	89 f9                	mov    %edi,%ecx
f0106d5b:	d3 e6                	shl    %cl,%esi
f0106d5d:	89 c5                	mov    %eax,%ebp
f0106d5f:	88 d9                	mov    %bl,%cl
f0106d61:	d3 ed                	shr    %cl,%ebp
f0106d63:	89 e9                	mov    %ebp,%ecx
f0106d65:	09 f1                	or     %esi,%ecx
f0106d67:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f0106d6b:	89 f9                	mov    %edi,%ecx
f0106d6d:	d3 e0                	shl    %cl,%eax
f0106d6f:	89 c5                	mov    %eax,%ebp
f0106d71:	89 d6                	mov    %edx,%esi
f0106d73:	88 d9                	mov    %bl,%cl
f0106d75:	d3 ee                	shr    %cl,%esi
f0106d77:	89 f9                	mov    %edi,%ecx
f0106d79:	d3 e2                	shl    %cl,%edx
f0106d7b:	8b 44 24 08          	mov    0x8(%esp),%eax
f0106d7f:	88 d9                	mov    %bl,%cl
f0106d81:	d3 e8                	shr    %cl,%eax
f0106d83:	09 c2                	or     %eax,%edx
f0106d85:	89 d0                	mov    %edx,%eax
f0106d87:	89 f2                	mov    %esi,%edx
f0106d89:	f7 74 24 0c          	divl   0xc(%esp)
f0106d8d:	89 d6                	mov    %edx,%esi
f0106d8f:	89 c3                	mov    %eax,%ebx
f0106d91:	f7 e5                	mul    %ebp
f0106d93:	39 d6                	cmp    %edx,%esi
f0106d95:	72 19                	jb     f0106db0 <__udivdi3+0xfc>
f0106d97:	74 0b                	je     f0106da4 <__udivdi3+0xf0>
f0106d99:	89 d8                	mov    %ebx,%eax
f0106d9b:	31 ff                	xor    %edi,%edi
f0106d9d:	e9 58 ff ff ff       	jmp    f0106cfa <__udivdi3+0x46>
f0106da2:	66 90                	xchg   %ax,%ax
f0106da4:	8b 54 24 08          	mov    0x8(%esp),%edx
f0106da8:	89 f9                	mov    %edi,%ecx
f0106daa:	d3 e2                	shl    %cl,%edx
f0106dac:	39 c2                	cmp    %eax,%edx
f0106dae:	73 e9                	jae    f0106d99 <__udivdi3+0xe5>
f0106db0:	8d 43 ff             	lea    -0x1(%ebx),%eax
f0106db3:	31 ff                	xor    %edi,%edi
f0106db5:	e9 40 ff ff ff       	jmp    f0106cfa <__udivdi3+0x46>
f0106dba:	66 90                	xchg   %ax,%ax
f0106dbc:	31 c0                	xor    %eax,%eax
f0106dbe:	e9 37 ff ff ff       	jmp    f0106cfa <__udivdi3+0x46>
f0106dc3:	90                   	nop

f0106dc4 <__umoddi3>:
f0106dc4:	55                   	push   %ebp
f0106dc5:	57                   	push   %edi
f0106dc6:	56                   	push   %esi
f0106dc7:	53                   	push   %ebx
f0106dc8:	83 ec 1c             	sub    $0x1c,%esp
f0106dcb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f0106dcf:	8b 74 24 34          	mov    0x34(%esp),%esi
f0106dd3:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0106dd7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
f0106ddb:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0106ddf:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0106de3:	89 f3                	mov    %esi,%ebx
f0106de5:	89 fa                	mov    %edi,%edx
f0106de7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0106deb:	89 34 24             	mov    %esi,(%esp)
f0106dee:	85 c0                	test   %eax,%eax
f0106df0:	75 1a                	jne    f0106e0c <__umoddi3+0x48>
f0106df2:	39 f7                	cmp    %esi,%edi
f0106df4:	0f 86 a2 00 00 00    	jbe    f0106e9c <__umoddi3+0xd8>
f0106dfa:	89 c8                	mov    %ecx,%eax
f0106dfc:	89 f2                	mov    %esi,%edx
f0106dfe:	f7 f7                	div    %edi
f0106e00:	89 d0                	mov    %edx,%eax
f0106e02:	31 d2                	xor    %edx,%edx
f0106e04:	83 c4 1c             	add    $0x1c,%esp
f0106e07:	5b                   	pop    %ebx
f0106e08:	5e                   	pop    %esi
f0106e09:	5f                   	pop    %edi
f0106e0a:	5d                   	pop    %ebp
f0106e0b:	c3                   	ret    
f0106e0c:	39 f0                	cmp    %esi,%eax
f0106e0e:	0f 87 ac 00 00 00    	ja     f0106ec0 <__umoddi3+0xfc>
f0106e14:	0f bd e8             	bsr    %eax,%ebp
f0106e17:	83 f5 1f             	xor    $0x1f,%ebp
f0106e1a:	0f 84 ac 00 00 00    	je     f0106ecc <__umoddi3+0x108>
f0106e20:	bf 20 00 00 00       	mov    $0x20,%edi
f0106e25:	29 ef                	sub    %ebp,%edi
f0106e27:	89 fe                	mov    %edi,%esi
f0106e29:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0106e2d:	89 e9                	mov    %ebp,%ecx
f0106e2f:	d3 e0                	shl    %cl,%eax
f0106e31:	89 d7                	mov    %edx,%edi
f0106e33:	89 f1                	mov    %esi,%ecx
f0106e35:	d3 ef                	shr    %cl,%edi
f0106e37:	09 c7                	or     %eax,%edi
f0106e39:	89 e9                	mov    %ebp,%ecx
f0106e3b:	d3 e2                	shl    %cl,%edx
f0106e3d:	89 14 24             	mov    %edx,(%esp)
f0106e40:	89 d8                	mov    %ebx,%eax
f0106e42:	d3 e0                	shl    %cl,%eax
f0106e44:	89 c2                	mov    %eax,%edx
f0106e46:	8b 44 24 08          	mov    0x8(%esp),%eax
f0106e4a:	d3 e0                	shl    %cl,%eax
f0106e4c:	89 44 24 04          	mov    %eax,0x4(%esp)
f0106e50:	8b 44 24 08          	mov    0x8(%esp),%eax
f0106e54:	89 f1                	mov    %esi,%ecx
f0106e56:	d3 e8                	shr    %cl,%eax
f0106e58:	09 d0                	or     %edx,%eax
f0106e5a:	d3 eb                	shr    %cl,%ebx
f0106e5c:	89 da                	mov    %ebx,%edx
f0106e5e:	f7 f7                	div    %edi
f0106e60:	89 d3                	mov    %edx,%ebx
f0106e62:	f7 24 24             	mull   (%esp)
f0106e65:	89 c6                	mov    %eax,%esi
f0106e67:	89 d1                	mov    %edx,%ecx
f0106e69:	39 d3                	cmp    %edx,%ebx
f0106e6b:	0f 82 87 00 00 00    	jb     f0106ef8 <__umoddi3+0x134>
f0106e71:	0f 84 91 00 00 00    	je     f0106f08 <__umoddi3+0x144>
f0106e77:	8b 54 24 04          	mov    0x4(%esp),%edx
f0106e7b:	29 f2                	sub    %esi,%edx
f0106e7d:	19 cb                	sbb    %ecx,%ebx
f0106e7f:	89 d8                	mov    %ebx,%eax
f0106e81:	8a 4c 24 0c          	mov    0xc(%esp),%cl
f0106e85:	d3 e0                	shl    %cl,%eax
f0106e87:	89 e9                	mov    %ebp,%ecx
f0106e89:	d3 ea                	shr    %cl,%edx
f0106e8b:	09 d0                	or     %edx,%eax
f0106e8d:	89 e9                	mov    %ebp,%ecx
f0106e8f:	d3 eb                	shr    %cl,%ebx
f0106e91:	89 da                	mov    %ebx,%edx
f0106e93:	83 c4 1c             	add    $0x1c,%esp
f0106e96:	5b                   	pop    %ebx
f0106e97:	5e                   	pop    %esi
f0106e98:	5f                   	pop    %edi
f0106e99:	5d                   	pop    %ebp
f0106e9a:	c3                   	ret    
f0106e9b:	90                   	nop
f0106e9c:	89 fd                	mov    %edi,%ebp
f0106e9e:	85 ff                	test   %edi,%edi
f0106ea0:	75 0b                	jne    f0106ead <__umoddi3+0xe9>
f0106ea2:	b8 01 00 00 00       	mov    $0x1,%eax
f0106ea7:	31 d2                	xor    %edx,%edx
f0106ea9:	f7 f7                	div    %edi
f0106eab:	89 c5                	mov    %eax,%ebp
f0106ead:	89 f0                	mov    %esi,%eax
f0106eaf:	31 d2                	xor    %edx,%edx
f0106eb1:	f7 f5                	div    %ebp
f0106eb3:	89 c8                	mov    %ecx,%eax
f0106eb5:	f7 f5                	div    %ebp
f0106eb7:	89 d0                	mov    %edx,%eax
f0106eb9:	e9 44 ff ff ff       	jmp    f0106e02 <__umoddi3+0x3e>
f0106ebe:	66 90                	xchg   %ax,%ax
f0106ec0:	89 c8                	mov    %ecx,%eax
f0106ec2:	89 f2                	mov    %esi,%edx
f0106ec4:	83 c4 1c             	add    $0x1c,%esp
f0106ec7:	5b                   	pop    %ebx
f0106ec8:	5e                   	pop    %esi
f0106ec9:	5f                   	pop    %edi
f0106eca:	5d                   	pop    %ebp
f0106ecb:	c3                   	ret    
f0106ecc:	3b 04 24             	cmp    (%esp),%eax
f0106ecf:	72 06                	jb     f0106ed7 <__umoddi3+0x113>
f0106ed1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
f0106ed5:	77 0f                	ja     f0106ee6 <__umoddi3+0x122>
f0106ed7:	89 f2                	mov    %esi,%edx
f0106ed9:	29 f9                	sub    %edi,%ecx
f0106edb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
f0106edf:	89 14 24             	mov    %edx,(%esp)
f0106ee2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0106ee6:	8b 44 24 04          	mov    0x4(%esp),%eax
f0106eea:	8b 14 24             	mov    (%esp),%edx
f0106eed:	83 c4 1c             	add    $0x1c,%esp
f0106ef0:	5b                   	pop    %ebx
f0106ef1:	5e                   	pop    %esi
f0106ef2:	5f                   	pop    %edi
f0106ef3:	5d                   	pop    %ebp
f0106ef4:	c3                   	ret    
f0106ef5:	8d 76 00             	lea    0x0(%esi),%esi
f0106ef8:	2b 04 24             	sub    (%esp),%eax
f0106efb:	19 fa                	sbb    %edi,%edx
f0106efd:	89 d1                	mov    %edx,%ecx
f0106eff:	89 c6                	mov    %eax,%esi
f0106f01:	e9 71 ff ff ff       	jmp    f0106e77 <__umoddi3+0xb3>
f0106f06:	66 90                	xchg   %ax,%ax
f0106f08:	39 44 24 04          	cmp    %eax,0x4(%esp)
f0106f0c:	72 ea                	jb     f0106ef8 <__umoddi3+0x134>
f0106f0e:	89 d9                	mov    %ebx,%ecx
f0106f10:	e9 62 ff ff ff       	jmp    f0106e77 <__umoddi3+0xb3>
