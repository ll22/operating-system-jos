
obj/user/hello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 2d 00 00 00       	call   80005e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
// hello, world
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 14             	sub    $0x14,%esp
	cprintf("hello, world\n");
  800039:	68 20 0f 80 00       	push   $0x800f20
  80003e:	e8 0e 01 00 00       	call   800151 <cprintf>
	cprintf("i am environment %08x\n", thisenv->env_id);
  800043:	a1 04 20 80 00       	mov    0x802004,%eax
  800048:	8b 40 48             	mov    0x48(%eax),%eax
  80004b:	83 c4 08             	add    $0x8,%esp
  80004e:	50                   	push   %eax
  80004f:	68 2e 0f 80 00       	push   $0x800f2e
  800054:	e8 f8 00 00 00       	call   800151 <cprintf>
}
  800059:	83 c4 10             	add    $0x10,%esp
  80005c:	c9                   	leave  
  80005d:	c3                   	ret    

0080005e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80005e:	55                   	push   %ebp
  80005f:	89 e5                	mov    %esp,%ebp
  800061:	56                   	push   %esi
  800062:	53                   	push   %ebx
  800063:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800066:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800069:	e8 ed 09 00 00       	call   800a5b <sys_getenvid>
  80006e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800073:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80007a:	c1 e0 07             	shl    $0x7,%eax
  80007d:	29 d0                	sub    %edx,%eax
  80007f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800084:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800089:	85 db                	test   %ebx,%ebx
  80008b:	7e 07                	jle    800094 <libmain+0x36>
		binaryname = argv[0];
  80008d:	8b 06                	mov    (%esi),%eax
  80008f:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800094:	83 ec 08             	sub    $0x8,%esp
  800097:	56                   	push   %esi
  800098:	53                   	push   %ebx
  800099:	e8 95 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80009e:	e8 0a 00 00 00       	call   8000ad <exit>
}
  8000a3:	83 c4 10             	add    $0x10,%esp
  8000a6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000a9:	5b                   	pop    %ebx
  8000aa:	5e                   	pop    %esi
  8000ab:	5d                   	pop    %ebp
  8000ac:	c3                   	ret    

008000ad <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000ad:	55                   	push   %ebp
  8000ae:	89 e5                	mov    %esp,%ebp
  8000b0:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000b3:	6a 00                	push   $0x0
  8000b5:	e8 60 09 00 00       	call   800a1a <sys_env_destroy>
}
  8000ba:	83 c4 10             	add    $0x10,%esp
  8000bd:	c9                   	leave  
  8000be:	c3                   	ret    

008000bf <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000bf:	55                   	push   %ebp
  8000c0:	89 e5                	mov    %esp,%ebp
  8000c2:	53                   	push   %ebx
  8000c3:	83 ec 04             	sub    $0x4,%esp
  8000c6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000c9:	8b 13                	mov    (%ebx),%edx
  8000cb:	8d 42 01             	lea    0x1(%edx),%eax
  8000ce:	89 03                	mov    %eax,(%ebx)
  8000d0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000d3:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000d7:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000dc:	75 1a                	jne    8000f8 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000de:	83 ec 08             	sub    $0x8,%esp
  8000e1:	68 ff 00 00 00       	push   $0xff
  8000e6:	8d 43 08             	lea    0x8(%ebx),%eax
  8000e9:	50                   	push   %eax
  8000ea:	e8 ee 08 00 00       	call   8009dd <sys_cputs>
		b->idx = 0;
  8000ef:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8000f5:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8000f8:	ff 43 04             	incl   0x4(%ebx)
}
  8000fb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000fe:	c9                   	leave  
  8000ff:	c3                   	ret    

00800100 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800100:	55                   	push   %ebp
  800101:	89 e5                	mov    %esp,%ebp
  800103:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800109:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800110:	00 00 00 
	b.cnt = 0;
  800113:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80011a:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80011d:	ff 75 0c             	pushl  0xc(%ebp)
  800120:	ff 75 08             	pushl  0x8(%ebp)
  800123:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800129:	50                   	push   %eax
  80012a:	68 bf 00 80 00       	push   $0x8000bf
  80012f:	e8 51 01 00 00       	call   800285 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800134:	83 c4 08             	add    $0x8,%esp
  800137:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80013d:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800143:	50                   	push   %eax
  800144:	e8 94 08 00 00       	call   8009dd <sys_cputs>

	return b.cnt;
}
  800149:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80014f:	c9                   	leave  
  800150:	c3                   	ret    

00800151 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800151:	55                   	push   %ebp
  800152:	89 e5                	mov    %esp,%ebp
  800154:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800157:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80015a:	50                   	push   %eax
  80015b:	ff 75 08             	pushl  0x8(%ebp)
  80015e:	e8 9d ff ff ff       	call   800100 <vcprintf>
	va_end(ap);

	return cnt;
}
  800163:	c9                   	leave  
  800164:	c3                   	ret    

00800165 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800165:	55                   	push   %ebp
  800166:	89 e5                	mov    %esp,%ebp
  800168:	57                   	push   %edi
  800169:	56                   	push   %esi
  80016a:	53                   	push   %ebx
  80016b:	83 ec 1c             	sub    $0x1c,%esp
  80016e:	89 c7                	mov    %eax,%edi
  800170:	89 d6                	mov    %edx,%esi
  800172:	8b 45 08             	mov    0x8(%ebp),%eax
  800175:	8b 55 0c             	mov    0xc(%ebp),%edx
  800178:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80017b:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80017e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800181:	bb 00 00 00 00       	mov    $0x0,%ebx
  800186:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800189:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80018c:	39 d3                	cmp    %edx,%ebx
  80018e:	72 05                	jb     800195 <printnum+0x30>
  800190:	39 45 10             	cmp    %eax,0x10(%ebp)
  800193:	77 45                	ja     8001da <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800195:	83 ec 0c             	sub    $0xc,%esp
  800198:	ff 75 18             	pushl  0x18(%ebp)
  80019b:	8b 45 14             	mov    0x14(%ebp),%eax
  80019e:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001a1:	53                   	push   %ebx
  8001a2:	ff 75 10             	pushl  0x10(%ebp)
  8001a5:	83 ec 08             	sub    $0x8,%esp
  8001a8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ab:	ff 75 e0             	pushl  -0x20(%ebp)
  8001ae:	ff 75 dc             	pushl  -0x24(%ebp)
  8001b1:	ff 75 d8             	pushl  -0x28(%ebp)
  8001b4:	e8 f7 0a 00 00       	call   800cb0 <__udivdi3>
  8001b9:	83 c4 18             	add    $0x18,%esp
  8001bc:	52                   	push   %edx
  8001bd:	50                   	push   %eax
  8001be:	89 f2                	mov    %esi,%edx
  8001c0:	89 f8                	mov    %edi,%eax
  8001c2:	e8 9e ff ff ff       	call   800165 <printnum>
  8001c7:	83 c4 20             	add    $0x20,%esp
  8001ca:	eb 16                	jmp    8001e2 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001cc:	83 ec 08             	sub    $0x8,%esp
  8001cf:	56                   	push   %esi
  8001d0:	ff 75 18             	pushl  0x18(%ebp)
  8001d3:	ff d7                	call   *%edi
  8001d5:	83 c4 10             	add    $0x10,%esp
  8001d8:	eb 03                	jmp    8001dd <printnum+0x78>
  8001da:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001dd:	4b                   	dec    %ebx
  8001de:	85 db                	test   %ebx,%ebx
  8001e0:	7f ea                	jg     8001cc <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001e2:	83 ec 08             	sub    $0x8,%esp
  8001e5:	56                   	push   %esi
  8001e6:	83 ec 04             	sub    $0x4,%esp
  8001e9:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ec:	ff 75 e0             	pushl  -0x20(%ebp)
  8001ef:	ff 75 dc             	pushl  -0x24(%ebp)
  8001f2:	ff 75 d8             	pushl  -0x28(%ebp)
  8001f5:	e8 c6 0b 00 00       	call   800dc0 <__umoddi3>
  8001fa:	83 c4 14             	add    $0x14,%esp
  8001fd:	0f be 80 4f 0f 80 00 	movsbl 0x800f4f(%eax),%eax
  800204:	50                   	push   %eax
  800205:	ff d7                	call   *%edi
}
  800207:	83 c4 10             	add    $0x10,%esp
  80020a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80020d:	5b                   	pop    %ebx
  80020e:	5e                   	pop    %esi
  80020f:	5f                   	pop    %edi
  800210:	5d                   	pop    %ebp
  800211:	c3                   	ret    

00800212 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800212:	55                   	push   %ebp
  800213:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800215:	83 fa 01             	cmp    $0x1,%edx
  800218:	7e 0e                	jle    800228 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80021a:	8b 10                	mov    (%eax),%edx
  80021c:	8d 4a 08             	lea    0x8(%edx),%ecx
  80021f:	89 08                	mov    %ecx,(%eax)
  800221:	8b 02                	mov    (%edx),%eax
  800223:	8b 52 04             	mov    0x4(%edx),%edx
  800226:	eb 22                	jmp    80024a <getuint+0x38>
	else if (lflag)
  800228:	85 d2                	test   %edx,%edx
  80022a:	74 10                	je     80023c <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80022c:	8b 10                	mov    (%eax),%edx
  80022e:	8d 4a 04             	lea    0x4(%edx),%ecx
  800231:	89 08                	mov    %ecx,(%eax)
  800233:	8b 02                	mov    (%edx),%eax
  800235:	ba 00 00 00 00       	mov    $0x0,%edx
  80023a:	eb 0e                	jmp    80024a <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80023c:	8b 10                	mov    (%eax),%edx
  80023e:	8d 4a 04             	lea    0x4(%edx),%ecx
  800241:	89 08                	mov    %ecx,(%eax)
  800243:	8b 02                	mov    (%edx),%eax
  800245:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80024a:	5d                   	pop    %ebp
  80024b:	c3                   	ret    

0080024c <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80024c:	55                   	push   %ebp
  80024d:	89 e5                	mov    %esp,%ebp
  80024f:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800252:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800255:	8b 10                	mov    (%eax),%edx
  800257:	3b 50 04             	cmp    0x4(%eax),%edx
  80025a:	73 0a                	jae    800266 <sprintputch+0x1a>
		*b->buf++ = ch;
  80025c:	8d 4a 01             	lea    0x1(%edx),%ecx
  80025f:	89 08                	mov    %ecx,(%eax)
  800261:	8b 45 08             	mov    0x8(%ebp),%eax
  800264:	88 02                	mov    %al,(%edx)
}
  800266:	5d                   	pop    %ebp
  800267:	c3                   	ret    

00800268 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800268:	55                   	push   %ebp
  800269:	89 e5                	mov    %esp,%ebp
  80026b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80026e:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800271:	50                   	push   %eax
  800272:	ff 75 10             	pushl  0x10(%ebp)
  800275:	ff 75 0c             	pushl  0xc(%ebp)
  800278:	ff 75 08             	pushl  0x8(%ebp)
  80027b:	e8 05 00 00 00       	call   800285 <vprintfmt>
	va_end(ap);
}
  800280:	83 c4 10             	add    $0x10,%esp
  800283:	c9                   	leave  
  800284:	c3                   	ret    

00800285 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800285:	55                   	push   %ebp
  800286:	89 e5                	mov    %esp,%ebp
  800288:	57                   	push   %edi
  800289:	56                   	push   %esi
  80028a:	53                   	push   %ebx
  80028b:	83 ec 2c             	sub    $0x2c,%esp
  80028e:	8b 75 08             	mov    0x8(%ebp),%esi
  800291:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800294:	8b 7d 10             	mov    0x10(%ebp),%edi
  800297:	eb 12                	jmp    8002ab <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800299:	85 c0                	test   %eax,%eax
  80029b:	0f 84 68 03 00 00    	je     800609 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002a1:	83 ec 08             	sub    $0x8,%esp
  8002a4:	53                   	push   %ebx
  8002a5:	50                   	push   %eax
  8002a6:	ff d6                	call   *%esi
  8002a8:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002ab:	47                   	inc    %edi
  8002ac:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002b0:	83 f8 25             	cmp    $0x25,%eax
  8002b3:	75 e4                	jne    800299 <vprintfmt+0x14>
  8002b5:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002b9:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002c0:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002c7:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002ce:	ba 00 00 00 00       	mov    $0x0,%edx
  8002d3:	eb 07                	jmp    8002dc <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002d5:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002d8:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002dc:	8d 47 01             	lea    0x1(%edi),%eax
  8002df:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002e2:	0f b6 0f             	movzbl (%edi),%ecx
  8002e5:	8a 07                	mov    (%edi),%al
  8002e7:	83 e8 23             	sub    $0x23,%eax
  8002ea:	3c 55                	cmp    $0x55,%al
  8002ec:	0f 87 fe 02 00 00    	ja     8005f0 <vprintfmt+0x36b>
  8002f2:	0f b6 c0             	movzbl %al,%eax
  8002f5:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8002fc:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8002ff:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800303:	eb d7                	jmp    8002dc <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800305:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800308:	b8 00 00 00 00       	mov    $0x0,%eax
  80030d:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800310:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800313:	01 c0                	add    %eax,%eax
  800315:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800319:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80031c:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80031f:	83 fa 09             	cmp    $0x9,%edx
  800322:	77 34                	ja     800358 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800324:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800325:	eb e9                	jmp    800310 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800327:	8b 45 14             	mov    0x14(%ebp),%eax
  80032a:	8d 48 04             	lea    0x4(%eax),%ecx
  80032d:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800330:	8b 00                	mov    (%eax),%eax
  800332:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800335:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800338:	eb 24                	jmp    80035e <vprintfmt+0xd9>
  80033a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80033e:	79 07                	jns    800347 <vprintfmt+0xc2>
  800340:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800347:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80034a:	eb 90                	jmp    8002dc <vprintfmt+0x57>
  80034c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80034f:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800356:	eb 84                	jmp    8002dc <vprintfmt+0x57>
  800358:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80035b:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80035e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800362:	0f 89 74 ff ff ff    	jns    8002dc <vprintfmt+0x57>
				width = precision, precision = -1;
  800368:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80036b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80036e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800375:	e9 62 ff ff ff       	jmp    8002dc <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80037a:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80037b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80037e:	e9 59 ff ff ff       	jmp    8002dc <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800383:	8b 45 14             	mov    0x14(%ebp),%eax
  800386:	8d 50 04             	lea    0x4(%eax),%edx
  800389:	89 55 14             	mov    %edx,0x14(%ebp)
  80038c:	83 ec 08             	sub    $0x8,%esp
  80038f:	53                   	push   %ebx
  800390:	ff 30                	pushl  (%eax)
  800392:	ff d6                	call   *%esi
			break;
  800394:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800397:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80039a:	e9 0c ff ff ff       	jmp    8002ab <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80039f:	8b 45 14             	mov    0x14(%ebp),%eax
  8003a2:	8d 50 04             	lea    0x4(%eax),%edx
  8003a5:	89 55 14             	mov    %edx,0x14(%ebp)
  8003a8:	8b 00                	mov    (%eax),%eax
  8003aa:	85 c0                	test   %eax,%eax
  8003ac:	79 02                	jns    8003b0 <vprintfmt+0x12b>
  8003ae:	f7 d8                	neg    %eax
  8003b0:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003b2:	83 f8 08             	cmp    $0x8,%eax
  8003b5:	7f 0b                	jg     8003c2 <vprintfmt+0x13d>
  8003b7:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  8003be:	85 c0                	test   %eax,%eax
  8003c0:	75 18                	jne    8003da <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003c2:	52                   	push   %edx
  8003c3:	68 67 0f 80 00       	push   $0x800f67
  8003c8:	53                   	push   %ebx
  8003c9:	56                   	push   %esi
  8003ca:	e8 99 fe ff ff       	call   800268 <printfmt>
  8003cf:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d2:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003d5:	e9 d1 fe ff ff       	jmp    8002ab <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003da:	50                   	push   %eax
  8003db:	68 70 0f 80 00       	push   $0x800f70
  8003e0:	53                   	push   %ebx
  8003e1:	56                   	push   %esi
  8003e2:	e8 81 fe ff ff       	call   800268 <printfmt>
  8003e7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ea:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003ed:	e9 b9 fe ff ff       	jmp    8002ab <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8003f2:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f5:	8d 50 04             	lea    0x4(%eax),%edx
  8003f8:	89 55 14             	mov    %edx,0x14(%ebp)
  8003fb:	8b 38                	mov    (%eax),%edi
  8003fd:	85 ff                	test   %edi,%edi
  8003ff:	75 05                	jne    800406 <vprintfmt+0x181>
				p = "(null)";
  800401:	bf 60 0f 80 00       	mov    $0x800f60,%edi
			if (width > 0 && padc != '-')
  800406:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80040a:	0f 8e 90 00 00 00    	jle    8004a0 <vprintfmt+0x21b>
  800410:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800414:	0f 84 8e 00 00 00    	je     8004a8 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80041a:	83 ec 08             	sub    $0x8,%esp
  80041d:	ff 75 d0             	pushl  -0x30(%ebp)
  800420:	57                   	push   %edi
  800421:	e8 70 02 00 00       	call   800696 <strnlen>
  800426:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800429:	29 c1                	sub    %eax,%ecx
  80042b:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80042e:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800431:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800435:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800438:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80043b:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80043d:	eb 0d                	jmp    80044c <vprintfmt+0x1c7>
					putch(padc, putdat);
  80043f:	83 ec 08             	sub    $0x8,%esp
  800442:	53                   	push   %ebx
  800443:	ff 75 e4             	pushl  -0x1c(%ebp)
  800446:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800448:	4f                   	dec    %edi
  800449:	83 c4 10             	add    $0x10,%esp
  80044c:	85 ff                	test   %edi,%edi
  80044e:	7f ef                	jg     80043f <vprintfmt+0x1ba>
  800450:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800453:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800456:	89 c8                	mov    %ecx,%eax
  800458:	85 c9                	test   %ecx,%ecx
  80045a:	79 05                	jns    800461 <vprintfmt+0x1dc>
  80045c:	b8 00 00 00 00       	mov    $0x0,%eax
  800461:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800464:	29 c1                	sub    %eax,%ecx
  800466:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800469:	89 75 08             	mov    %esi,0x8(%ebp)
  80046c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80046f:	eb 3d                	jmp    8004ae <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800471:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800475:	74 19                	je     800490 <vprintfmt+0x20b>
  800477:	0f be c0             	movsbl %al,%eax
  80047a:	83 e8 20             	sub    $0x20,%eax
  80047d:	83 f8 5e             	cmp    $0x5e,%eax
  800480:	76 0e                	jbe    800490 <vprintfmt+0x20b>
					putch('?', putdat);
  800482:	83 ec 08             	sub    $0x8,%esp
  800485:	53                   	push   %ebx
  800486:	6a 3f                	push   $0x3f
  800488:	ff 55 08             	call   *0x8(%ebp)
  80048b:	83 c4 10             	add    $0x10,%esp
  80048e:	eb 0b                	jmp    80049b <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800490:	83 ec 08             	sub    $0x8,%esp
  800493:	53                   	push   %ebx
  800494:	52                   	push   %edx
  800495:	ff 55 08             	call   *0x8(%ebp)
  800498:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80049b:	ff 4d e4             	decl   -0x1c(%ebp)
  80049e:	eb 0e                	jmp    8004ae <vprintfmt+0x229>
  8004a0:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a3:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004a6:	eb 06                	jmp    8004ae <vprintfmt+0x229>
  8004a8:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ab:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004ae:	47                   	inc    %edi
  8004af:	8a 47 ff             	mov    -0x1(%edi),%al
  8004b2:	0f be d0             	movsbl %al,%edx
  8004b5:	85 d2                	test   %edx,%edx
  8004b7:	74 1d                	je     8004d6 <vprintfmt+0x251>
  8004b9:	85 f6                	test   %esi,%esi
  8004bb:	78 b4                	js     800471 <vprintfmt+0x1ec>
  8004bd:	4e                   	dec    %esi
  8004be:	79 b1                	jns    800471 <vprintfmt+0x1ec>
  8004c0:	8b 75 08             	mov    0x8(%ebp),%esi
  8004c3:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004c6:	eb 14                	jmp    8004dc <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004c8:	83 ec 08             	sub    $0x8,%esp
  8004cb:	53                   	push   %ebx
  8004cc:	6a 20                	push   $0x20
  8004ce:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004d0:	4f                   	dec    %edi
  8004d1:	83 c4 10             	add    $0x10,%esp
  8004d4:	eb 06                	jmp    8004dc <vprintfmt+0x257>
  8004d6:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004d9:	8b 75 08             	mov    0x8(%ebp),%esi
  8004dc:	85 ff                	test   %edi,%edi
  8004de:	7f e8                	jg     8004c8 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004e3:	e9 c3 fd ff ff       	jmp    8002ab <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004e8:	83 fa 01             	cmp    $0x1,%edx
  8004eb:	7e 16                	jle    800503 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8004ed:	8b 45 14             	mov    0x14(%ebp),%eax
  8004f0:	8d 50 08             	lea    0x8(%eax),%edx
  8004f3:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f6:	8b 50 04             	mov    0x4(%eax),%edx
  8004f9:	8b 00                	mov    (%eax),%eax
  8004fb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004fe:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800501:	eb 32                	jmp    800535 <vprintfmt+0x2b0>
	else if (lflag)
  800503:	85 d2                	test   %edx,%edx
  800505:	74 18                	je     80051f <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800507:	8b 45 14             	mov    0x14(%ebp),%eax
  80050a:	8d 50 04             	lea    0x4(%eax),%edx
  80050d:	89 55 14             	mov    %edx,0x14(%ebp)
  800510:	8b 00                	mov    (%eax),%eax
  800512:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800515:	89 c1                	mov    %eax,%ecx
  800517:	c1 f9 1f             	sar    $0x1f,%ecx
  80051a:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80051d:	eb 16                	jmp    800535 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80051f:	8b 45 14             	mov    0x14(%ebp),%eax
  800522:	8d 50 04             	lea    0x4(%eax),%edx
  800525:	89 55 14             	mov    %edx,0x14(%ebp)
  800528:	8b 00                	mov    (%eax),%eax
  80052a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80052d:	89 c1                	mov    %eax,%ecx
  80052f:	c1 f9 1f             	sar    $0x1f,%ecx
  800532:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800535:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800538:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80053b:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80053f:	79 76                	jns    8005b7 <vprintfmt+0x332>
				putch('-', putdat);
  800541:	83 ec 08             	sub    $0x8,%esp
  800544:	53                   	push   %ebx
  800545:	6a 2d                	push   $0x2d
  800547:	ff d6                	call   *%esi
				num = -(long long) num;
  800549:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80054c:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80054f:	f7 d8                	neg    %eax
  800551:	83 d2 00             	adc    $0x0,%edx
  800554:	f7 da                	neg    %edx
  800556:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800559:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80055e:	eb 5c                	jmp    8005bc <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800560:	8d 45 14             	lea    0x14(%ebp),%eax
  800563:	e8 aa fc ff ff       	call   800212 <getuint>
			base = 10;
  800568:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80056d:	eb 4d                	jmp    8005bc <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80056f:	8d 45 14             	lea    0x14(%ebp),%eax
  800572:	e8 9b fc ff ff       	call   800212 <getuint>
			base = 8;
  800577:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80057c:	eb 3e                	jmp    8005bc <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80057e:	83 ec 08             	sub    $0x8,%esp
  800581:	53                   	push   %ebx
  800582:	6a 30                	push   $0x30
  800584:	ff d6                	call   *%esi
			putch('x', putdat);
  800586:	83 c4 08             	add    $0x8,%esp
  800589:	53                   	push   %ebx
  80058a:	6a 78                	push   $0x78
  80058c:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80058e:	8b 45 14             	mov    0x14(%ebp),%eax
  800591:	8d 50 04             	lea    0x4(%eax),%edx
  800594:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800597:	8b 00                	mov    (%eax),%eax
  800599:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80059e:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005a1:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005a6:	eb 14                	jmp    8005bc <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005a8:	8d 45 14             	lea    0x14(%ebp),%eax
  8005ab:	e8 62 fc ff ff       	call   800212 <getuint>
			base = 16;
  8005b0:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005b5:	eb 05                	jmp    8005bc <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005b7:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005bc:	83 ec 0c             	sub    $0xc,%esp
  8005bf:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005c3:	57                   	push   %edi
  8005c4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005c7:	51                   	push   %ecx
  8005c8:	52                   	push   %edx
  8005c9:	50                   	push   %eax
  8005ca:	89 da                	mov    %ebx,%edx
  8005cc:	89 f0                	mov    %esi,%eax
  8005ce:	e8 92 fb ff ff       	call   800165 <printnum>
			break;
  8005d3:	83 c4 20             	add    $0x20,%esp
  8005d6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005d9:	e9 cd fc ff ff       	jmp    8002ab <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005de:	83 ec 08             	sub    $0x8,%esp
  8005e1:	53                   	push   %ebx
  8005e2:	51                   	push   %ecx
  8005e3:	ff d6                	call   *%esi
			break;
  8005e5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8005eb:	e9 bb fc ff ff       	jmp    8002ab <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8005f0:	83 ec 08             	sub    $0x8,%esp
  8005f3:	53                   	push   %ebx
  8005f4:	6a 25                	push   $0x25
  8005f6:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8005f8:	83 c4 10             	add    $0x10,%esp
  8005fb:	eb 01                	jmp    8005fe <vprintfmt+0x379>
  8005fd:	4f                   	dec    %edi
  8005fe:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800602:	75 f9                	jne    8005fd <vprintfmt+0x378>
  800604:	e9 a2 fc ff ff       	jmp    8002ab <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800609:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80060c:	5b                   	pop    %ebx
  80060d:	5e                   	pop    %esi
  80060e:	5f                   	pop    %edi
  80060f:	5d                   	pop    %ebp
  800610:	c3                   	ret    

00800611 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800611:	55                   	push   %ebp
  800612:	89 e5                	mov    %esp,%ebp
  800614:	83 ec 18             	sub    $0x18,%esp
  800617:	8b 45 08             	mov    0x8(%ebp),%eax
  80061a:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80061d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800620:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800624:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800627:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80062e:	85 c0                	test   %eax,%eax
  800630:	74 26                	je     800658 <vsnprintf+0x47>
  800632:	85 d2                	test   %edx,%edx
  800634:	7e 29                	jle    80065f <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800636:	ff 75 14             	pushl  0x14(%ebp)
  800639:	ff 75 10             	pushl  0x10(%ebp)
  80063c:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80063f:	50                   	push   %eax
  800640:	68 4c 02 80 00       	push   $0x80024c
  800645:	e8 3b fc ff ff       	call   800285 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80064a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80064d:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800650:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800653:	83 c4 10             	add    $0x10,%esp
  800656:	eb 0c                	jmp    800664 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800658:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80065d:	eb 05                	jmp    800664 <vsnprintf+0x53>
  80065f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800664:	c9                   	leave  
  800665:	c3                   	ret    

00800666 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800666:	55                   	push   %ebp
  800667:	89 e5                	mov    %esp,%ebp
  800669:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80066c:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80066f:	50                   	push   %eax
  800670:	ff 75 10             	pushl  0x10(%ebp)
  800673:	ff 75 0c             	pushl  0xc(%ebp)
  800676:	ff 75 08             	pushl  0x8(%ebp)
  800679:	e8 93 ff ff ff       	call   800611 <vsnprintf>
	va_end(ap);

	return rc;
}
  80067e:	c9                   	leave  
  80067f:	c3                   	ret    

00800680 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800680:	55                   	push   %ebp
  800681:	89 e5                	mov    %esp,%ebp
  800683:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800686:	b8 00 00 00 00       	mov    $0x0,%eax
  80068b:	eb 01                	jmp    80068e <strlen+0xe>
		n++;
  80068d:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80068e:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800692:	75 f9                	jne    80068d <strlen+0xd>
		n++;
	return n;
}
  800694:	5d                   	pop    %ebp
  800695:	c3                   	ret    

00800696 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800696:	55                   	push   %ebp
  800697:	89 e5                	mov    %esp,%ebp
  800699:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80069c:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80069f:	ba 00 00 00 00       	mov    $0x0,%edx
  8006a4:	eb 01                	jmp    8006a7 <strnlen+0x11>
		n++;
  8006a6:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006a7:	39 c2                	cmp    %eax,%edx
  8006a9:	74 08                	je     8006b3 <strnlen+0x1d>
  8006ab:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006af:	75 f5                	jne    8006a6 <strnlen+0x10>
  8006b1:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006b3:	5d                   	pop    %ebp
  8006b4:	c3                   	ret    

008006b5 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006b5:	55                   	push   %ebp
  8006b6:	89 e5                	mov    %esp,%ebp
  8006b8:	53                   	push   %ebx
  8006b9:	8b 45 08             	mov    0x8(%ebp),%eax
  8006bc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006bf:	89 c2                	mov    %eax,%edx
  8006c1:	42                   	inc    %edx
  8006c2:	41                   	inc    %ecx
  8006c3:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006c6:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006c9:	84 db                	test   %bl,%bl
  8006cb:	75 f4                	jne    8006c1 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006cd:	5b                   	pop    %ebx
  8006ce:	5d                   	pop    %ebp
  8006cf:	c3                   	ret    

008006d0 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006d0:	55                   	push   %ebp
  8006d1:	89 e5                	mov    %esp,%ebp
  8006d3:	53                   	push   %ebx
  8006d4:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006d7:	53                   	push   %ebx
  8006d8:	e8 a3 ff ff ff       	call   800680 <strlen>
  8006dd:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006e0:	ff 75 0c             	pushl  0xc(%ebp)
  8006e3:	01 d8                	add    %ebx,%eax
  8006e5:	50                   	push   %eax
  8006e6:	e8 ca ff ff ff       	call   8006b5 <strcpy>
	return dst;
}
  8006eb:	89 d8                	mov    %ebx,%eax
  8006ed:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006f0:	c9                   	leave  
  8006f1:	c3                   	ret    

008006f2 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8006f2:	55                   	push   %ebp
  8006f3:	89 e5                	mov    %esp,%ebp
  8006f5:	56                   	push   %esi
  8006f6:	53                   	push   %ebx
  8006f7:	8b 75 08             	mov    0x8(%ebp),%esi
  8006fa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006fd:	89 f3                	mov    %esi,%ebx
  8006ff:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800702:	89 f2                	mov    %esi,%edx
  800704:	eb 0c                	jmp    800712 <strncpy+0x20>
		*dst++ = *src;
  800706:	42                   	inc    %edx
  800707:	8a 01                	mov    (%ecx),%al
  800709:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80070c:	80 39 01             	cmpb   $0x1,(%ecx)
  80070f:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800712:	39 da                	cmp    %ebx,%edx
  800714:	75 f0                	jne    800706 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800716:	89 f0                	mov    %esi,%eax
  800718:	5b                   	pop    %ebx
  800719:	5e                   	pop    %esi
  80071a:	5d                   	pop    %ebp
  80071b:	c3                   	ret    

0080071c <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80071c:	55                   	push   %ebp
  80071d:	89 e5                	mov    %esp,%ebp
  80071f:	56                   	push   %esi
  800720:	53                   	push   %ebx
  800721:	8b 75 08             	mov    0x8(%ebp),%esi
  800724:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800727:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80072a:	85 c0                	test   %eax,%eax
  80072c:	74 1e                	je     80074c <strlcpy+0x30>
  80072e:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800732:	89 f2                	mov    %esi,%edx
  800734:	eb 05                	jmp    80073b <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800736:	42                   	inc    %edx
  800737:	41                   	inc    %ecx
  800738:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80073b:	39 c2                	cmp    %eax,%edx
  80073d:	74 08                	je     800747 <strlcpy+0x2b>
  80073f:	8a 19                	mov    (%ecx),%bl
  800741:	84 db                	test   %bl,%bl
  800743:	75 f1                	jne    800736 <strlcpy+0x1a>
  800745:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800747:	c6 00 00             	movb   $0x0,(%eax)
  80074a:	eb 02                	jmp    80074e <strlcpy+0x32>
  80074c:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80074e:	29 f0                	sub    %esi,%eax
}
  800750:	5b                   	pop    %ebx
  800751:	5e                   	pop    %esi
  800752:	5d                   	pop    %ebp
  800753:	c3                   	ret    

00800754 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800754:	55                   	push   %ebp
  800755:	89 e5                	mov    %esp,%ebp
  800757:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80075a:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80075d:	eb 02                	jmp    800761 <strcmp+0xd>
		p++, q++;
  80075f:	41                   	inc    %ecx
  800760:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800761:	8a 01                	mov    (%ecx),%al
  800763:	84 c0                	test   %al,%al
  800765:	74 04                	je     80076b <strcmp+0x17>
  800767:	3a 02                	cmp    (%edx),%al
  800769:	74 f4                	je     80075f <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80076b:	0f b6 c0             	movzbl %al,%eax
  80076e:	0f b6 12             	movzbl (%edx),%edx
  800771:	29 d0                	sub    %edx,%eax
}
  800773:	5d                   	pop    %ebp
  800774:	c3                   	ret    

00800775 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800775:	55                   	push   %ebp
  800776:	89 e5                	mov    %esp,%ebp
  800778:	53                   	push   %ebx
  800779:	8b 45 08             	mov    0x8(%ebp),%eax
  80077c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80077f:	89 c3                	mov    %eax,%ebx
  800781:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800784:	eb 02                	jmp    800788 <strncmp+0x13>
		n--, p++, q++;
  800786:	40                   	inc    %eax
  800787:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800788:	39 d8                	cmp    %ebx,%eax
  80078a:	74 14                	je     8007a0 <strncmp+0x2b>
  80078c:	8a 08                	mov    (%eax),%cl
  80078e:	84 c9                	test   %cl,%cl
  800790:	74 04                	je     800796 <strncmp+0x21>
  800792:	3a 0a                	cmp    (%edx),%cl
  800794:	74 f0                	je     800786 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800796:	0f b6 00             	movzbl (%eax),%eax
  800799:	0f b6 12             	movzbl (%edx),%edx
  80079c:	29 d0                	sub    %edx,%eax
  80079e:	eb 05                	jmp    8007a5 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007a0:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007a5:	5b                   	pop    %ebx
  8007a6:	5d                   	pop    %ebp
  8007a7:	c3                   	ret    

008007a8 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007a8:	55                   	push   %ebp
  8007a9:	89 e5                	mov    %esp,%ebp
  8007ab:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ae:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007b1:	eb 05                	jmp    8007b8 <strchr+0x10>
		if (*s == c)
  8007b3:	38 ca                	cmp    %cl,%dl
  8007b5:	74 0c                	je     8007c3 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007b7:	40                   	inc    %eax
  8007b8:	8a 10                	mov    (%eax),%dl
  8007ba:	84 d2                	test   %dl,%dl
  8007bc:	75 f5                	jne    8007b3 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007be:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007c3:	5d                   	pop    %ebp
  8007c4:	c3                   	ret    

008007c5 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007c5:	55                   	push   %ebp
  8007c6:	89 e5                	mov    %esp,%ebp
  8007c8:	8b 45 08             	mov    0x8(%ebp),%eax
  8007cb:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ce:	eb 05                	jmp    8007d5 <strfind+0x10>
		if (*s == c)
  8007d0:	38 ca                	cmp    %cl,%dl
  8007d2:	74 07                	je     8007db <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007d4:	40                   	inc    %eax
  8007d5:	8a 10                	mov    (%eax),%dl
  8007d7:	84 d2                	test   %dl,%dl
  8007d9:	75 f5                	jne    8007d0 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007db:	5d                   	pop    %ebp
  8007dc:	c3                   	ret    

008007dd <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007dd:	55                   	push   %ebp
  8007de:	89 e5                	mov    %esp,%ebp
  8007e0:	57                   	push   %edi
  8007e1:	56                   	push   %esi
  8007e2:	53                   	push   %ebx
  8007e3:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007e6:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007e9:	85 c9                	test   %ecx,%ecx
  8007eb:	74 36                	je     800823 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8007ed:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8007f3:	75 28                	jne    80081d <memset+0x40>
  8007f5:	f6 c1 03             	test   $0x3,%cl
  8007f8:	75 23                	jne    80081d <memset+0x40>
		c &= 0xFF;
  8007fa:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8007fe:	89 d3                	mov    %edx,%ebx
  800800:	c1 e3 08             	shl    $0x8,%ebx
  800803:	89 d6                	mov    %edx,%esi
  800805:	c1 e6 18             	shl    $0x18,%esi
  800808:	89 d0                	mov    %edx,%eax
  80080a:	c1 e0 10             	shl    $0x10,%eax
  80080d:	09 f0                	or     %esi,%eax
  80080f:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800811:	89 d8                	mov    %ebx,%eax
  800813:	09 d0                	or     %edx,%eax
  800815:	c1 e9 02             	shr    $0x2,%ecx
  800818:	fc                   	cld    
  800819:	f3 ab                	rep stos %eax,%es:(%edi)
  80081b:	eb 06                	jmp    800823 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80081d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800820:	fc                   	cld    
  800821:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800823:	89 f8                	mov    %edi,%eax
  800825:	5b                   	pop    %ebx
  800826:	5e                   	pop    %esi
  800827:	5f                   	pop    %edi
  800828:	5d                   	pop    %ebp
  800829:	c3                   	ret    

0080082a <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80082a:	55                   	push   %ebp
  80082b:	89 e5                	mov    %esp,%ebp
  80082d:	57                   	push   %edi
  80082e:	56                   	push   %esi
  80082f:	8b 45 08             	mov    0x8(%ebp),%eax
  800832:	8b 75 0c             	mov    0xc(%ebp),%esi
  800835:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800838:	39 c6                	cmp    %eax,%esi
  80083a:	73 33                	jae    80086f <memmove+0x45>
  80083c:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80083f:	39 d0                	cmp    %edx,%eax
  800841:	73 2c                	jae    80086f <memmove+0x45>
		s += n;
		d += n;
  800843:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800846:	89 d6                	mov    %edx,%esi
  800848:	09 fe                	or     %edi,%esi
  80084a:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800850:	75 13                	jne    800865 <memmove+0x3b>
  800852:	f6 c1 03             	test   $0x3,%cl
  800855:	75 0e                	jne    800865 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800857:	83 ef 04             	sub    $0x4,%edi
  80085a:	8d 72 fc             	lea    -0x4(%edx),%esi
  80085d:	c1 e9 02             	shr    $0x2,%ecx
  800860:	fd                   	std    
  800861:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800863:	eb 07                	jmp    80086c <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800865:	4f                   	dec    %edi
  800866:	8d 72 ff             	lea    -0x1(%edx),%esi
  800869:	fd                   	std    
  80086a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80086c:	fc                   	cld    
  80086d:	eb 1d                	jmp    80088c <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80086f:	89 f2                	mov    %esi,%edx
  800871:	09 c2                	or     %eax,%edx
  800873:	f6 c2 03             	test   $0x3,%dl
  800876:	75 0f                	jne    800887 <memmove+0x5d>
  800878:	f6 c1 03             	test   $0x3,%cl
  80087b:	75 0a                	jne    800887 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80087d:	c1 e9 02             	shr    $0x2,%ecx
  800880:	89 c7                	mov    %eax,%edi
  800882:	fc                   	cld    
  800883:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800885:	eb 05                	jmp    80088c <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800887:	89 c7                	mov    %eax,%edi
  800889:	fc                   	cld    
  80088a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80088c:	5e                   	pop    %esi
  80088d:	5f                   	pop    %edi
  80088e:	5d                   	pop    %ebp
  80088f:	c3                   	ret    

00800890 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800890:	55                   	push   %ebp
  800891:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800893:	ff 75 10             	pushl  0x10(%ebp)
  800896:	ff 75 0c             	pushl  0xc(%ebp)
  800899:	ff 75 08             	pushl  0x8(%ebp)
  80089c:	e8 89 ff ff ff       	call   80082a <memmove>
}
  8008a1:	c9                   	leave  
  8008a2:	c3                   	ret    

008008a3 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008a3:	55                   	push   %ebp
  8008a4:	89 e5                	mov    %esp,%ebp
  8008a6:	56                   	push   %esi
  8008a7:	53                   	push   %ebx
  8008a8:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ab:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008ae:	89 c6                	mov    %eax,%esi
  8008b0:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008b3:	eb 14                	jmp    8008c9 <memcmp+0x26>
		if (*s1 != *s2)
  8008b5:	8a 08                	mov    (%eax),%cl
  8008b7:	8a 1a                	mov    (%edx),%bl
  8008b9:	38 d9                	cmp    %bl,%cl
  8008bb:	74 0a                	je     8008c7 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008bd:	0f b6 c1             	movzbl %cl,%eax
  8008c0:	0f b6 db             	movzbl %bl,%ebx
  8008c3:	29 d8                	sub    %ebx,%eax
  8008c5:	eb 0b                	jmp    8008d2 <memcmp+0x2f>
		s1++, s2++;
  8008c7:	40                   	inc    %eax
  8008c8:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008c9:	39 f0                	cmp    %esi,%eax
  8008cb:	75 e8                	jne    8008b5 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008cd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008d2:	5b                   	pop    %ebx
  8008d3:	5e                   	pop    %esi
  8008d4:	5d                   	pop    %ebp
  8008d5:	c3                   	ret    

008008d6 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008d6:	55                   	push   %ebp
  8008d7:	89 e5                	mov    %esp,%ebp
  8008d9:	53                   	push   %ebx
  8008da:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008dd:	89 c1                	mov    %eax,%ecx
  8008df:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008e2:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008e6:	eb 08                	jmp    8008f0 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008e8:	0f b6 10             	movzbl (%eax),%edx
  8008eb:	39 da                	cmp    %ebx,%edx
  8008ed:	74 05                	je     8008f4 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008ef:	40                   	inc    %eax
  8008f0:	39 c8                	cmp    %ecx,%eax
  8008f2:	72 f4                	jb     8008e8 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8008f4:	5b                   	pop    %ebx
  8008f5:	5d                   	pop    %ebp
  8008f6:	c3                   	ret    

008008f7 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8008f7:	55                   	push   %ebp
  8008f8:	89 e5                	mov    %esp,%ebp
  8008fa:	57                   	push   %edi
  8008fb:	56                   	push   %esi
  8008fc:	53                   	push   %ebx
  8008fd:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800900:	eb 01                	jmp    800903 <strtol+0xc>
		s++;
  800902:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800903:	8a 01                	mov    (%ecx),%al
  800905:	3c 20                	cmp    $0x20,%al
  800907:	74 f9                	je     800902 <strtol+0xb>
  800909:	3c 09                	cmp    $0x9,%al
  80090b:	74 f5                	je     800902 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80090d:	3c 2b                	cmp    $0x2b,%al
  80090f:	75 08                	jne    800919 <strtol+0x22>
		s++;
  800911:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800912:	bf 00 00 00 00       	mov    $0x0,%edi
  800917:	eb 11                	jmp    80092a <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800919:	3c 2d                	cmp    $0x2d,%al
  80091b:	75 08                	jne    800925 <strtol+0x2e>
		s++, neg = 1;
  80091d:	41                   	inc    %ecx
  80091e:	bf 01 00 00 00       	mov    $0x1,%edi
  800923:	eb 05                	jmp    80092a <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800925:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80092a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80092e:	0f 84 87 00 00 00    	je     8009bb <strtol+0xc4>
  800934:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800938:	75 27                	jne    800961 <strtol+0x6a>
  80093a:	80 39 30             	cmpb   $0x30,(%ecx)
  80093d:	75 22                	jne    800961 <strtol+0x6a>
  80093f:	e9 88 00 00 00       	jmp    8009cc <strtol+0xd5>
		s += 2, base = 16;
  800944:	83 c1 02             	add    $0x2,%ecx
  800947:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80094e:	eb 11                	jmp    800961 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800950:	41                   	inc    %ecx
  800951:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800958:	eb 07                	jmp    800961 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  80095a:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800961:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800966:	8a 11                	mov    (%ecx),%dl
  800968:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80096b:	80 fb 09             	cmp    $0x9,%bl
  80096e:	77 08                	ja     800978 <strtol+0x81>
			dig = *s - '0';
  800970:	0f be d2             	movsbl %dl,%edx
  800973:	83 ea 30             	sub    $0x30,%edx
  800976:	eb 22                	jmp    80099a <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800978:	8d 72 9f             	lea    -0x61(%edx),%esi
  80097b:	89 f3                	mov    %esi,%ebx
  80097d:	80 fb 19             	cmp    $0x19,%bl
  800980:	77 08                	ja     80098a <strtol+0x93>
			dig = *s - 'a' + 10;
  800982:	0f be d2             	movsbl %dl,%edx
  800985:	83 ea 57             	sub    $0x57,%edx
  800988:	eb 10                	jmp    80099a <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  80098a:	8d 72 bf             	lea    -0x41(%edx),%esi
  80098d:	89 f3                	mov    %esi,%ebx
  80098f:	80 fb 19             	cmp    $0x19,%bl
  800992:	77 14                	ja     8009a8 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800994:	0f be d2             	movsbl %dl,%edx
  800997:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  80099a:	3b 55 10             	cmp    0x10(%ebp),%edx
  80099d:	7d 09                	jge    8009a8 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80099f:	41                   	inc    %ecx
  8009a0:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009a4:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009a6:	eb be                	jmp    800966 <strtol+0x6f>

	if (endptr)
  8009a8:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009ac:	74 05                	je     8009b3 <strtol+0xbc>
		*endptr = (char *) s;
  8009ae:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009b1:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009b3:	85 ff                	test   %edi,%edi
  8009b5:	74 21                	je     8009d8 <strtol+0xe1>
  8009b7:	f7 d8                	neg    %eax
  8009b9:	eb 1d                	jmp    8009d8 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009bb:	80 39 30             	cmpb   $0x30,(%ecx)
  8009be:	75 9a                	jne    80095a <strtol+0x63>
  8009c0:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009c4:	0f 84 7a ff ff ff    	je     800944 <strtol+0x4d>
  8009ca:	eb 84                	jmp    800950 <strtol+0x59>
  8009cc:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009d0:	0f 84 6e ff ff ff    	je     800944 <strtol+0x4d>
  8009d6:	eb 89                	jmp    800961 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009d8:	5b                   	pop    %ebx
  8009d9:	5e                   	pop    %esi
  8009da:	5f                   	pop    %edi
  8009db:	5d                   	pop    %ebp
  8009dc:	c3                   	ret    

008009dd <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009dd:	55                   	push   %ebp
  8009de:	89 e5                	mov    %esp,%ebp
  8009e0:	57                   	push   %edi
  8009e1:	56                   	push   %esi
  8009e2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009e3:	b8 00 00 00 00       	mov    $0x0,%eax
  8009e8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009eb:	8b 55 08             	mov    0x8(%ebp),%edx
  8009ee:	89 c3                	mov    %eax,%ebx
  8009f0:	89 c7                	mov    %eax,%edi
  8009f2:	89 c6                	mov    %eax,%esi
  8009f4:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8009f6:	5b                   	pop    %ebx
  8009f7:	5e                   	pop    %esi
  8009f8:	5f                   	pop    %edi
  8009f9:	5d                   	pop    %ebp
  8009fa:	c3                   	ret    

008009fb <sys_cgetc>:

int
sys_cgetc(void)
{
  8009fb:	55                   	push   %ebp
  8009fc:	89 e5                	mov    %esp,%ebp
  8009fe:	57                   	push   %edi
  8009ff:	56                   	push   %esi
  800a00:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a01:	ba 00 00 00 00       	mov    $0x0,%edx
  800a06:	b8 01 00 00 00       	mov    $0x1,%eax
  800a0b:	89 d1                	mov    %edx,%ecx
  800a0d:	89 d3                	mov    %edx,%ebx
  800a0f:	89 d7                	mov    %edx,%edi
  800a11:	89 d6                	mov    %edx,%esi
  800a13:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a15:	5b                   	pop    %ebx
  800a16:	5e                   	pop    %esi
  800a17:	5f                   	pop    %edi
  800a18:	5d                   	pop    %ebp
  800a19:	c3                   	ret    

00800a1a <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a1a:	55                   	push   %ebp
  800a1b:	89 e5                	mov    %esp,%ebp
  800a1d:	57                   	push   %edi
  800a1e:	56                   	push   %esi
  800a1f:	53                   	push   %ebx
  800a20:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a23:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a28:	b8 03 00 00 00       	mov    $0x3,%eax
  800a2d:	8b 55 08             	mov    0x8(%ebp),%edx
  800a30:	89 cb                	mov    %ecx,%ebx
  800a32:	89 cf                	mov    %ecx,%edi
  800a34:	89 ce                	mov    %ecx,%esi
  800a36:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a38:	85 c0                	test   %eax,%eax
  800a3a:	7e 17                	jle    800a53 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a3c:	83 ec 0c             	sub    $0xc,%esp
  800a3f:	50                   	push   %eax
  800a40:	6a 03                	push   $0x3
  800a42:	68 a4 11 80 00       	push   $0x8011a4
  800a47:	6a 23                	push   $0x23
  800a49:	68 c1 11 80 00       	push   $0x8011c1
  800a4e:	e8 14 02 00 00       	call   800c67 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a53:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a56:	5b                   	pop    %ebx
  800a57:	5e                   	pop    %esi
  800a58:	5f                   	pop    %edi
  800a59:	5d                   	pop    %ebp
  800a5a:	c3                   	ret    

00800a5b <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a5b:	55                   	push   %ebp
  800a5c:	89 e5                	mov    %esp,%ebp
  800a5e:	57                   	push   %edi
  800a5f:	56                   	push   %esi
  800a60:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a61:	ba 00 00 00 00       	mov    $0x0,%edx
  800a66:	b8 02 00 00 00       	mov    $0x2,%eax
  800a6b:	89 d1                	mov    %edx,%ecx
  800a6d:	89 d3                	mov    %edx,%ebx
  800a6f:	89 d7                	mov    %edx,%edi
  800a71:	89 d6                	mov    %edx,%esi
  800a73:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a75:	5b                   	pop    %ebx
  800a76:	5e                   	pop    %esi
  800a77:	5f                   	pop    %edi
  800a78:	5d                   	pop    %ebp
  800a79:	c3                   	ret    

00800a7a <sys_yield>:

void
sys_yield(void)
{
  800a7a:	55                   	push   %ebp
  800a7b:	89 e5                	mov    %esp,%ebp
  800a7d:	57                   	push   %edi
  800a7e:	56                   	push   %esi
  800a7f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a80:	ba 00 00 00 00       	mov    $0x0,%edx
  800a85:	b8 0a 00 00 00       	mov    $0xa,%eax
  800a8a:	89 d1                	mov    %edx,%ecx
  800a8c:	89 d3                	mov    %edx,%ebx
  800a8e:	89 d7                	mov    %edx,%edi
  800a90:	89 d6                	mov    %edx,%esi
  800a92:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800a94:	5b                   	pop    %ebx
  800a95:	5e                   	pop    %esi
  800a96:	5f                   	pop    %edi
  800a97:	5d                   	pop    %ebp
  800a98:	c3                   	ret    

00800a99 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800a99:	55                   	push   %ebp
  800a9a:	89 e5                	mov    %esp,%ebp
  800a9c:	57                   	push   %edi
  800a9d:	56                   	push   %esi
  800a9e:	53                   	push   %ebx
  800a9f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aa2:	be 00 00 00 00       	mov    $0x0,%esi
  800aa7:	b8 04 00 00 00       	mov    $0x4,%eax
  800aac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800aaf:	8b 55 08             	mov    0x8(%ebp),%edx
  800ab2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ab5:	89 f7                	mov    %esi,%edi
  800ab7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ab9:	85 c0                	test   %eax,%eax
  800abb:	7e 17                	jle    800ad4 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800abd:	83 ec 0c             	sub    $0xc,%esp
  800ac0:	50                   	push   %eax
  800ac1:	6a 04                	push   $0x4
  800ac3:	68 a4 11 80 00       	push   $0x8011a4
  800ac8:	6a 23                	push   $0x23
  800aca:	68 c1 11 80 00       	push   $0x8011c1
  800acf:	e8 93 01 00 00       	call   800c67 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800ad4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ad7:	5b                   	pop    %ebx
  800ad8:	5e                   	pop    %esi
  800ad9:	5f                   	pop    %edi
  800ada:	5d                   	pop    %ebp
  800adb:	c3                   	ret    

00800adc <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800adc:	55                   	push   %ebp
  800add:	89 e5                	mov    %esp,%ebp
  800adf:	57                   	push   %edi
  800ae0:	56                   	push   %esi
  800ae1:	53                   	push   %ebx
  800ae2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae5:	b8 05 00 00 00       	mov    $0x5,%eax
  800aea:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800aed:	8b 55 08             	mov    0x8(%ebp),%edx
  800af0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800af3:	8b 7d 14             	mov    0x14(%ebp),%edi
  800af6:	8b 75 18             	mov    0x18(%ebp),%esi
  800af9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800afb:	85 c0                	test   %eax,%eax
  800afd:	7e 17                	jle    800b16 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800aff:	83 ec 0c             	sub    $0xc,%esp
  800b02:	50                   	push   %eax
  800b03:	6a 05                	push   $0x5
  800b05:	68 a4 11 80 00       	push   $0x8011a4
  800b0a:	6a 23                	push   $0x23
  800b0c:	68 c1 11 80 00       	push   $0x8011c1
  800b11:	e8 51 01 00 00       	call   800c67 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b16:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b19:	5b                   	pop    %ebx
  800b1a:	5e                   	pop    %esi
  800b1b:	5f                   	pop    %edi
  800b1c:	5d                   	pop    %ebp
  800b1d:	c3                   	ret    

00800b1e <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b1e:	55                   	push   %ebp
  800b1f:	89 e5                	mov    %esp,%ebp
  800b21:	57                   	push   %edi
  800b22:	56                   	push   %esi
  800b23:	53                   	push   %ebx
  800b24:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b27:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b2c:	b8 06 00 00 00       	mov    $0x6,%eax
  800b31:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b34:	8b 55 08             	mov    0x8(%ebp),%edx
  800b37:	89 df                	mov    %ebx,%edi
  800b39:	89 de                	mov    %ebx,%esi
  800b3b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b3d:	85 c0                	test   %eax,%eax
  800b3f:	7e 17                	jle    800b58 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b41:	83 ec 0c             	sub    $0xc,%esp
  800b44:	50                   	push   %eax
  800b45:	6a 06                	push   $0x6
  800b47:	68 a4 11 80 00       	push   $0x8011a4
  800b4c:	6a 23                	push   $0x23
  800b4e:	68 c1 11 80 00       	push   $0x8011c1
  800b53:	e8 0f 01 00 00       	call   800c67 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800b58:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b5b:	5b                   	pop    %ebx
  800b5c:	5e                   	pop    %esi
  800b5d:	5f                   	pop    %edi
  800b5e:	5d                   	pop    %ebp
  800b5f:	c3                   	ret    

00800b60 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800b60:	55                   	push   %ebp
  800b61:	89 e5                	mov    %esp,%ebp
  800b63:	57                   	push   %edi
  800b64:	56                   	push   %esi
  800b65:	53                   	push   %ebx
  800b66:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b69:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b6e:	b8 08 00 00 00       	mov    $0x8,%eax
  800b73:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b76:	8b 55 08             	mov    0x8(%ebp),%edx
  800b79:	89 df                	mov    %ebx,%edi
  800b7b:	89 de                	mov    %ebx,%esi
  800b7d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b7f:	85 c0                	test   %eax,%eax
  800b81:	7e 17                	jle    800b9a <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b83:	83 ec 0c             	sub    $0xc,%esp
  800b86:	50                   	push   %eax
  800b87:	6a 08                	push   $0x8
  800b89:	68 a4 11 80 00       	push   $0x8011a4
  800b8e:	6a 23                	push   $0x23
  800b90:	68 c1 11 80 00       	push   $0x8011c1
  800b95:	e8 cd 00 00 00       	call   800c67 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800b9a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b9d:	5b                   	pop    %ebx
  800b9e:	5e                   	pop    %esi
  800b9f:	5f                   	pop    %edi
  800ba0:	5d                   	pop    %ebp
  800ba1:	c3                   	ret    

00800ba2 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800ba2:	55                   	push   %ebp
  800ba3:	89 e5                	mov    %esp,%ebp
  800ba5:	57                   	push   %edi
  800ba6:	56                   	push   %esi
  800ba7:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ba8:	ba 00 00 00 00       	mov    $0x0,%edx
  800bad:	b8 0b 00 00 00       	mov    $0xb,%eax
  800bb2:	89 d1                	mov    %edx,%ecx
  800bb4:	89 d3                	mov    %edx,%ebx
  800bb6:	89 d7                	mov    %edx,%edi
  800bb8:	89 d6                	mov    %edx,%esi
  800bba:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800bbc:	5b                   	pop    %ebx
  800bbd:	5e                   	pop    %esi
  800bbe:	5f                   	pop    %edi
  800bbf:	5d                   	pop    %ebp
  800bc0:	c3                   	ret    

00800bc1 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800bc1:	55                   	push   %ebp
  800bc2:	89 e5                	mov    %esp,%ebp
  800bc4:	57                   	push   %edi
  800bc5:	56                   	push   %esi
  800bc6:	53                   	push   %ebx
  800bc7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bca:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bcf:	b8 09 00 00 00       	mov    $0x9,%eax
  800bd4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bd7:	8b 55 08             	mov    0x8(%ebp),%edx
  800bda:	89 df                	mov    %ebx,%edi
  800bdc:	89 de                	mov    %ebx,%esi
  800bde:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800be0:	85 c0                	test   %eax,%eax
  800be2:	7e 17                	jle    800bfb <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800be4:	83 ec 0c             	sub    $0xc,%esp
  800be7:	50                   	push   %eax
  800be8:	6a 09                	push   $0x9
  800bea:	68 a4 11 80 00       	push   $0x8011a4
  800bef:	6a 23                	push   $0x23
  800bf1:	68 c1 11 80 00       	push   $0x8011c1
  800bf6:	e8 6c 00 00 00       	call   800c67 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800bfb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bfe:	5b                   	pop    %ebx
  800bff:	5e                   	pop    %esi
  800c00:	5f                   	pop    %edi
  800c01:	5d                   	pop    %ebp
  800c02:	c3                   	ret    

00800c03 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800c03:	55                   	push   %ebp
  800c04:	89 e5                	mov    %esp,%ebp
  800c06:	57                   	push   %edi
  800c07:	56                   	push   %esi
  800c08:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c09:	be 00 00 00 00       	mov    $0x0,%esi
  800c0e:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c13:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c16:	8b 55 08             	mov    0x8(%ebp),%edx
  800c19:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c1c:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c1f:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800c21:	5b                   	pop    %ebx
  800c22:	5e                   	pop    %esi
  800c23:	5f                   	pop    %edi
  800c24:	5d                   	pop    %ebp
  800c25:	c3                   	ret    

00800c26 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800c26:	55                   	push   %ebp
  800c27:	89 e5                	mov    %esp,%ebp
  800c29:	57                   	push   %edi
  800c2a:	56                   	push   %esi
  800c2b:	53                   	push   %ebx
  800c2c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c34:	b8 0d 00 00 00       	mov    $0xd,%eax
  800c39:	8b 55 08             	mov    0x8(%ebp),%edx
  800c3c:	89 cb                	mov    %ecx,%ebx
  800c3e:	89 cf                	mov    %ecx,%edi
  800c40:	89 ce                	mov    %ecx,%esi
  800c42:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c44:	85 c0                	test   %eax,%eax
  800c46:	7e 17                	jle    800c5f <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c48:	83 ec 0c             	sub    $0xc,%esp
  800c4b:	50                   	push   %eax
  800c4c:	6a 0d                	push   $0xd
  800c4e:	68 a4 11 80 00       	push   $0x8011a4
  800c53:	6a 23                	push   $0x23
  800c55:	68 c1 11 80 00       	push   $0x8011c1
  800c5a:	e8 08 00 00 00       	call   800c67 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800c5f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c62:	5b                   	pop    %ebx
  800c63:	5e                   	pop    %esi
  800c64:	5f                   	pop    %edi
  800c65:	5d                   	pop    %ebp
  800c66:	c3                   	ret    

00800c67 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800c67:	55                   	push   %ebp
  800c68:	89 e5                	mov    %esp,%ebp
  800c6a:	56                   	push   %esi
  800c6b:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800c6c:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800c6f:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800c75:	e8 e1 fd ff ff       	call   800a5b <sys_getenvid>
  800c7a:	83 ec 0c             	sub    $0xc,%esp
  800c7d:	ff 75 0c             	pushl  0xc(%ebp)
  800c80:	ff 75 08             	pushl  0x8(%ebp)
  800c83:	56                   	push   %esi
  800c84:	50                   	push   %eax
  800c85:	68 d0 11 80 00       	push   $0x8011d0
  800c8a:	e8 c2 f4 ff ff       	call   800151 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800c8f:	83 c4 18             	add    $0x18,%esp
  800c92:	53                   	push   %ebx
  800c93:	ff 75 10             	pushl  0x10(%ebp)
  800c96:	e8 65 f4 ff ff       	call   800100 <vcprintf>
	cprintf("\n");
  800c9b:	c7 04 24 2c 0f 80 00 	movl   $0x800f2c,(%esp)
  800ca2:	e8 aa f4 ff ff       	call   800151 <cprintf>
  800ca7:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800caa:	cc                   	int3   
  800cab:	eb fd                	jmp    800caa <_panic+0x43>
  800cad:	66 90                	xchg   %ax,%ax
  800caf:	90                   	nop

00800cb0 <__udivdi3>:
  800cb0:	55                   	push   %ebp
  800cb1:	57                   	push   %edi
  800cb2:	56                   	push   %esi
  800cb3:	53                   	push   %ebx
  800cb4:	83 ec 1c             	sub    $0x1c,%esp
  800cb7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cbb:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cbf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cc3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cc7:	89 ca                	mov    %ecx,%edx
  800cc9:	89 f8                	mov    %edi,%eax
  800ccb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ccf:	85 f6                	test   %esi,%esi
  800cd1:	75 2d                	jne    800d00 <__udivdi3+0x50>
  800cd3:	39 cf                	cmp    %ecx,%edi
  800cd5:	77 65                	ja     800d3c <__udivdi3+0x8c>
  800cd7:	89 fd                	mov    %edi,%ebp
  800cd9:	85 ff                	test   %edi,%edi
  800cdb:	75 0b                	jne    800ce8 <__udivdi3+0x38>
  800cdd:	b8 01 00 00 00       	mov    $0x1,%eax
  800ce2:	31 d2                	xor    %edx,%edx
  800ce4:	f7 f7                	div    %edi
  800ce6:	89 c5                	mov    %eax,%ebp
  800ce8:	31 d2                	xor    %edx,%edx
  800cea:	89 c8                	mov    %ecx,%eax
  800cec:	f7 f5                	div    %ebp
  800cee:	89 c1                	mov    %eax,%ecx
  800cf0:	89 d8                	mov    %ebx,%eax
  800cf2:	f7 f5                	div    %ebp
  800cf4:	89 cf                	mov    %ecx,%edi
  800cf6:	89 fa                	mov    %edi,%edx
  800cf8:	83 c4 1c             	add    $0x1c,%esp
  800cfb:	5b                   	pop    %ebx
  800cfc:	5e                   	pop    %esi
  800cfd:	5f                   	pop    %edi
  800cfe:	5d                   	pop    %ebp
  800cff:	c3                   	ret    
  800d00:	39 ce                	cmp    %ecx,%esi
  800d02:	77 28                	ja     800d2c <__udivdi3+0x7c>
  800d04:	0f bd fe             	bsr    %esi,%edi
  800d07:	83 f7 1f             	xor    $0x1f,%edi
  800d0a:	75 40                	jne    800d4c <__udivdi3+0x9c>
  800d0c:	39 ce                	cmp    %ecx,%esi
  800d0e:	72 0a                	jb     800d1a <__udivdi3+0x6a>
  800d10:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d14:	0f 87 9e 00 00 00    	ja     800db8 <__udivdi3+0x108>
  800d1a:	b8 01 00 00 00       	mov    $0x1,%eax
  800d1f:	89 fa                	mov    %edi,%edx
  800d21:	83 c4 1c             	add    $0x1c,%esp
  800d24:	5b                   	pop    %ebx
  800d25:	5e                   	pop    %esi
  800d26:	5f                   	pop    %edi
  800d27:	5d                   	pop    %ebp
  800d28:	c3                   	ret    
  800d29:	8d 76 00             	lea    0x0(%esi),%esi
  800d2c:	31 ff                	xor    %edi,%edi
  800d2e:	31 c0                	xor    %eax,%eax
  800d30:	89 fa                	mov    %edi,%edx
  800d32:	83 c4 1c             	add    $0x1c,%esp
  800d35:	5b                   	pop    %ebx
  800d36:	5e                   	pop    %esi
  800d37:	5f                   	pop    %edi
  800d38:	5d                   	pop    %ebp
  800d39:	c3                   	ret    
  800d3a:	66 90                	xchg   %ax,%ax
  800d3c:	89 d8                	mov    %ebx,%eax
  800d3e:	f7 f7                	div    %edi
  800d40:	31 ff                	xor    %edi,%edi
  800d42:	89 fa                	mov    %edi,%edx
  800d44:	83 c4 1c             	add    $0x1c,%esp
  800d47:	5b                   	pop    %ebx
  800d48:	5e                   	pop    %esi
  800d49:	5f                   	pop    %edi
  800d4a:	5d                   	pop    %ebp
  800d4b:	c3                   	ret    
  800d4c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d51:	89 eb                	mov    %ebp,%ebx
  800d53:	29 fb                	sub    %edi,%ebx
  800d55:	89 f9                	mov    %edi,%ecx
  800d57:	d3 e6                	shl    %cl,%esi
  800d59:	89 c5                	mov    %eax,%ebp
  800d5b:	88 d9                	mov    %bl,%cl
  800d5d:	d3 ed                	shr    %cl,%ebp
  800d5f:	89 e9                	mov    %ebp,%ecx
  800d61:	09 f1                	or     %esi,%ecx
  800d63:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d67:	89 f9                	mov    %edi,%ecx
  800d69:	d3 e0                	shl    %cl,%eax
  800d6b:	89 c5                	mov    %eax,%ebp
  800d6d:	89 d6                	mov    %edx,%esi
  800d6f:	88 d9                	mov    %bl,%cl
  800d71:	d3 ee                	shr    %cl,%esi
  800d73:	89 f9                	mov    %edi,%ecx
  800d75:	d3 e2                	shl    %cl,%edx
  800d77:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d7b:	88 d9                	mov    %bl,%cl
  800d7d:	d3 e8                	shr    %cl,%eax
  800d7f:	09 c2                	or     %eax,%edx
  800d81:	89 d0                	mov    %edx,%eax
  800d83:	89 f2                	mov    %esi,%edx
  800d85:	f7 74 24 0c          	divl   0xc(%esp)
  800d89:	89 d6                	mov    %edx,%esi
  800d8b:	89 c3                	mov    %eax,%ebx
  800d8d:	f7 e5                	mul    %ebp
  800d8f:	39 d6                	cmp    %edx,%esi
  800d91:	72 19                	jb     800dac <__udivdi3+0xfc>
  800d93:	74 0b                	je     800da0 <__udivdi3+0xf0>
  800d95:	89 d8                	mov    %ebx,%eax
  800d97:	31 ff                	xor    %edi,%edi
  800d99:	e9 58 ff ff ff       	jmp    800cf6 <__udivdi3+0x46>
  800d9e:	66 90                	xchg   %ax,%ax
  800da0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800da4:	89 f9                	mov    %edi,%ecx
  800da6:	d3 e2                	shl    %cl,%edx
  800da8:	39 c2                	cmp    %eax,%edx
  800daa:	73 e9                	jae    800d95 <__udivdi3+0xe5>
  800dac:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800daf:	31 ff                	xor    %edi,%edi
  800db1:	e9 40 ff ff ff       	jmp    800cf6 <__udivdi3+0x46>
  800db6:	66 90                	xchg   %ax,%ax
  800db8:	31 c0                	xor    %eax,%eax
  800dba:	e9 37 ff ff ff       	jmp    800cf6 <__udivdi3+0x46>
  800dbf:	90                   	nop

00800dc0 <__umoddi3>:
  800dc0:	55                   	push   %ebp
  800dc1:	57                   	push   %edi
  800dc2:	56                   	push   %esi
  800dc3:	53                   	push   %ebx
  800dc4:	83 ec 1c             	sub    $0x1c,%esp
  800dc7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800dcb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dcf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dd3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dd7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800ddb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800ddf:	89 f3                	mov    %esi,%ebx
  800de1:	89 fa                	mov    %edi,%edx
  800de3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800de7:	89 34 24             	mov    %esi,(%esp)
  800dea:	85 c0                	test   %eax,%eax
  800dec:	75 1a                	jne    800e08 <__umoddi3+0x48>
  800dee:	39 f7                	cmp    %esi,%edi
  800df0:	0f 86 a2 00 00 00    	jbe    800e98 <__umoddi3+0xd8>
  800df6:	89 c8                	mov    %ecx,%eax
  800df8:	89 f2                	mov    %esi,%edx
  800dfa:	f7 f7                	div    %edi
  800dfc:	89 d0                	mov    %edx,%eax
  800dfe:	31 d2                	xor    %edx,%edx
  800e00:	83 c4 1c             	add    $0x1c,%esp
  800e03:	5b                   	pop    %ebx
  800e04:	5e                   	pop    %esi
  800e05:	5f                   	pop    %edi
  800e06:	5d                   	pop    %ebp
  800e07:	c3                   	ret    
  800e08:	39 f0                	cmp    %esi,%eax
  800e0a:	0f 87 ac 00 00 00    	ja     800ebc <__umoddi3+0xfc>
  800e10:	0f bd e8             	bsr    %eax,%ebp
  800e13:	83 f5 1f             	xor    $0x1f,%ebp
  800e16:	0f 84 ac 00 00 00    	je     800ec8 <__umoddi3+0x108>
  800e1c:	bf 20 00 00 00       	mov    $0x20,%edi
  800e21:	29 ef                	sub    %ebp,%edi
  800e23:	89 fe                	mov    %edi,%esi
  800e25:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e29:	89 e9                	mov    %ebp,%ecx
  800e2b:	d3 e0                	shl    %cl,%eax
  800e2d:	89 d7                	mov    %edx,%edi
  800e2f:	89 f1                	mov    %esi,%ecx
  800e31:	d3 ef                	shr    %cl,%edi
  800e33:	09 c7                	or     %eax,%edi
  800e35:	89 e9                	mov    %ebp,%ecx
  800e37:	d3 e2                	shl    %cl,%edx
  800e39:	89 14 24             	mov    %edx,(%esp)
  800e3c:	89 d8                	mov    %ebx,%eax
  800e3e:	d3 e0                	shl    %cl,%eax
  800e40:	89 c2                	mov    %eax,%edx
  800e42:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e46:	d3 e0                	shl    %cl,%eax
  800e48:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e4c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e50:	89 f1                	mov    %esi,%ecx
  800e52:	d3 e8                	shr    %cl,%eax
  800e54:	09 d0                	or     %edx,%eax
  800e56:	d3 eb                	shr    %cl,%ebx
  800e58:	89 da                	mov    %ebx,%edx
  800e5a:	f7 f7                	div    %edi
  800e5c:	89 d3                	mov    %edx,%ebx
  800e5e:	f7 24 24             	mull   (%esp)
  800e61:	89 c6                	mov    %eax,%esi
  800e63:	89 d1                	mov    %edx,%ecx
  800e65:	39 d3                	cmp    %edx,%ebx
  800e67:	0f 82 87 00 00 00    	jb     800ef4 <__umoddi3+0x134>
  800e6d:	0f 84 91 00 00 00    	je     800f04 <__umoddi3+0x144>
  800e73:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e77:	29 f2                	sub    %esi,%edx
  800e79:	19 cb                	sbb    %ecx,%ebx
  800e7b:	89 d8                	mov    %ebx,%eax
  800e7d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e81:	d3 e0                	shl    %cl,%eax
  800e83:	89 e9                	mov    %ebp,%ecx
  800e85:	d3 ea                	shr    %cl,%edx
  800e87:	09 d0                	or     %edx,%eax
  800e89:	89 e9                	mov    %ebp,%ecx
  800e8b:	d3 eb                	shr    %cl,%ebx
  800e8d:	89 da                	mov    %ebx,%edx
  800e8f:	83 c4 1c             	add    $0x1c,%esp
  800e92:	5b                   	pop    %ebx
  800e93:	5e                   	pop    %esi
  800e94:	5f                   	pop    %edi
  800e95:	5d                   	pop    %ebp
  800e96:	c3                   	ret    
  800e97:	90                   	nop
  800e98:	89 fd                	mov    %edi,%ebp
  800e9a:	85 ff                	test   %edi,%edi
  800e9c:	75 0b                	jne    800ea9 <__umoddi3+0xe9>
  800e9e:	b8 01 00 00 00       	mov    $0x1,%eax
  800ea3:	31 d2                	xor    %edx,%edx
  800ea5:	f7 f7                	div    %edi
  800ea7:	89 c5                	mov    %eax,%ebp
  800ea9:	89 f0                	mov    %esi,%eax
  800eab:	31 d2                	xor    %edx,%edx
  800ead:	f7 f5                	div    %ebp
  800eaf:	89 c8                	mov    %ecx,%eax
  800eb1:	f7 f5                	div    %ebp
  800eb3:	89 d0                	mov    %edx,%eax
  800eb5:	e9 44 ff ff ff       	jmp    800dfe <__umoddi3+0x3e>
  800eba:	66 90                	xchg   %ax,%ax
  800ebc:	89 c8                	mov    %ecx,%eax
  800ebe:	89 f2                	mov    %esi,%edx
  800ec0:	83 c4 1c             	add    $0x1c,%esp
  800ec3:	5b                   	pop    %ebx
  800ec4:	5e                   	pop    %esi
  800ec5:	5f                   	pop    %edi
  800ec6:	5d                   	pop    %ebp
  800ec7:	c3                   	ret    
  800ec8:	3b 04 24             	cmp    (%esp),%eax
  800ecb:	72 06                	jb     800ed3 <__umoddi3+0x113>
  800ecd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ed1:	77 0f                	ja     800ee2 <__umoddi3+0x122>
  800ed3:	89 f2                	mov    %esi,%edx
  800ed5:	29 f9                	sub    %edi,%ecx
  800ed7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800edb:	89 14 24             	mov    %edx,(%esp)
  800ede:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ee2:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ee6:	8b 14 24             	mov    (%esp),%edx
  800ee9:	83 c4 1c             	add    $0x1c,%esp
  800eec:	5b                   	pop    %ebx
  800eed:	5e                   	pop    %esi
  800eee:	5f                   	pop    %edi
  800eef:	5d                   	pop    %ebp
  800ef0:	c3                   	ret    
  800ef1:	8d 76 00             	lea    0x0(%esi),%esi
  800ef4:	2b 04 24             	sub    (%esp),%eax
  800ef7:	19 fa                	sbb    %edi,%edx
  800ef9:	89 d1                	mov    %edx,%ecx
  800efb:	89 c6                	mov    %eax,%esi
  800efd:	e9 71 ff ff ff       	jmp    800e73 <__umoddi3+0xb3>
  800f02:	66 90                	xchg   %ax,%ax
  800f04:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f08:	72 ea                	jb     800ef4 <__umoddi3+0x134>
  800f0a:	89 d9                	mov    %ebx,%ecx
  800f0c:	e9 62 ff ff ff       	jmp    800e73 <__umoddi3+0xb3>
