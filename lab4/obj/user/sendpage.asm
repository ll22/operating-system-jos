
obj/user/sendpage:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 64 01 00 00       	call   800195 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
#define TEMP_ADDR	((char*)0xa00000)
#define TEMP_ADDR_CHILD	((char*)0xb00000)

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 18             	sub    $0x18,%esp
	envid_t who;

	if ((who = fork()) == 0) {
  800039:	e8 12 0e 00 00       	call   800e50 <fork>
  80003e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800041:	85 c0                	test   %eax,%eax
  800043:	0f 85 9d 00 00 00    	jne    8000e6 <umain+0xb3>
		// Child
		ipc_recv(&who, TEMP_ADDR_CHILD, 0);
  800049:	83 ec 04             	sub    $0x4,%esp
  80004c:	6a 00                	push   $0x0
  80004e:	68 00 00 b0 00       	push   $0xb00000
  800053:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800056:	50                   	push   %eax
  800057:	e8 0c 10 00 00       	call   801068 <ipc_recv>
		cprintf("%x got message: %s\n", who, TEMP_ADDR_CHILD);
  80005c:	83 c4 0c             	add    $0xc,%esp
  80005f:	68 00 00 b0 00       	push   $0xb00000
  800064:	ff 75 f4             	pushl  -0xc(%ebp)
  800067:	68 a0 14 80 00       	push   $0x8014a0
  80006c:	e8 17 02 00 00       	call   800288 <cprintf>
		if (strncmp(TEMP_ADDR_CHILD, str1, strlen(str1)) == 0)
  800071:	83 c4 04             	add    $0x4,%esp
  800074:	ff 35 04 20 80 00    	pushl  0x802004
  80007a:	e8 38 07 00 00       	call   8007b7 <strlen>
  80007f:	83 c4 0c             	add    $0xc,%esp
  800082:	50                   	push   %eax
  800083:	ff 35 04 20 80 00    	pushl  0x802004
  800089:	68 00 00 b0 00       	push   $0xb00000
  80008e:	e8 19 08 00 00       	call   8008ac <strncmp>
  800093:	83 c4 10             	add    $0x10,%esp
  800096:	85 c0                	test   %eax,%eax
  800098:	75 10                	jne    8000aa <umain+0x77>
			cprintf("child received correct message\n");
  80009a:	83 ec 0c             	sub    $0xc,%esp
  80009d:	68 b4 14 80 00       	push   $0x8014b4
  8000a2:	e8 e1 01 00 00       	call   800288 <cprintf>
  8000a7:	83 c4 10             	add    $0x10,%esp

		memcpy(TEMP_ADDR_CHILD, str2, strlen(str2) + 1);
  8000aa:	83 ec 0c             	sub    $0xc,%esp
  8000ad:	ff 35 00 20 80 00    	pushl  0x802000
  8000b3:	e8 ff 06 00 00       	call   8007b7 <strlen>
  8000b8:	83 c4 0c             	add    $0xc,%esp
  8000bb:	40                   	inc    %eax
  8000bc:	50                   	push   %eax
  8000bd:	ff 35 00 20 80 00    	pushl  0x802000
  8000c3:	68 00 00 b0 00       	push   $0xb00000
  8000c8:	e8 fa 08 00 00       	call   8009c7 <memcpy>
		ipc_send(who, 0, TEMP_ADDR_CHILD, PTE_P | PTE_W | PTE_U);
  8000cd:	6a 07                	push   $0x7
  8000cf:	68 00 00 b0 00       	push   $0xb00000
  8000d4:	6a 00                	push   $0x0
  8000d6:	ff 75 f4             	pushl  -0xc(%ebp)
  8000d9:	e8 f2 0f 00 00       	call   8010d0 <ipc_send>
		return;
  8000de:	83 c4 20             	add    $0x20,%esp
  8000e1:	e9 ad 00 00 00       	jmp    800193 <umain+0x160>
	}

	// Parent
	sys_page_alloc(thisenv->env_id, TEMP_ADDR, PTE_P | PTE_W | PTE_U);
  8000e6:	a1 0c 20 80 00       	mov    0x80200c,%eax
  8000eb:	8b 40 48             	mov    0x48(%eax),%eax
  8000ee:	83 ec 04             	sub    $0x4,%esp
  8000f1:	6a 07                	push   $0x7
  8000f3:	68 00 00 a0 00       	push   $0xa00000
  8000f8:	50                   	push   %eax
  8000f9:	e8 d2 0a 00 00       	call   800bd0 <sys_page_alloc>
	memcpy(TEMP_ADDR, str1, strlen(str1) + 1);
  8000fe:	83 c4 04             	add    $0x4,%esp
  800101:	ff 35 04 20 80 00    	pushl  0x802004
  800107:	e8 ab 06 00 00       	call   8007b7 <strlen>
  80010c:	83 c4 0c             	add    $0xc,%esp
  80010f:	40                   	inc    %eax
  800110:	50                   	push   %eax
  800111:	ff 35 04 20 80 00    	pushl  0x802004
  800117:	68 00 00 a0 00       	push   $0xa00000
  80011c:	e8 a6 08 00 00       	call   8009c7 <memcpy>
	ipc_send(who, 0, TEMP_ADDR, PTE_P | PTE_W | PTE_U);
  800121:	6a 07                	push   $0x7
  800123:	68 00 00 a0 00       	push   $0xa00000
  800128:	6a 00                	push   $0x0
  80012a:	ff 75 f4             	pushl  -0xc(%ebp)
  80012d:	e8 9e 0f 00 00       	call   8010d0 <ipc_send>

	ipc_recv(&who, TEMP_ADDR, 0);
  800132:	83 c4 1c             	add    $0x1c,%esp
  800135:	6a 00                	push   $0x0
  800137:	68 00 00 a0 00       	push   $0xa00000
  80013c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80013f:	50                   	push   %eax
  800140:	e8 23 0f 00 00       	call   801068 <ipc_recv>
	cprintf("%x got message: %s\n", who, TEMP_ADDR);
  800145:	83 c4 0c             	add    $0xc,%esp
  800148:	68 00 00 a0 00       	push   $0xa00000
  80014d:	ff 75 f4             	pushl  -0xc(%ebp)
  800150:	68 a0 14 80 00       	push   $0x8014a0
  800155:	e8 2e 01 00 00       	call   800288 <cprintf>
	if (strncmp(TEMP_ADDR, str2, strlen(str2)) == 0)
  80015a:	83 c4 04             	add    $0x4,%esp
  80015d:	ff 35 00 20 80 00    	pushl  0x802000
  800163:	e8 4f 06 00 00       	call   8007b7 <strlen>
  800168:	83 c4 0c             	add    $0xc,%esp
  80016b:	50                   	push   %eax
  80016c:	ff 35 00 20 80 00    	pushl  0x802000
  800172:	68 00 00 a0 00       	push   $0xa00000
  800177:	e8 30 07 00 00       	call   8008ac <strncmp>
  80017c:	83 c4 10             	add    $0x10,%esp
  80017f:	85 c0                	test   %eax,%eax
  800181:	75 10                	jne    800193 <umain+0x160>
		cprintf("parent received correct message\n");
  800183:	83 ec 0c             	sub    $0xc,%esp
  800186:	68 d4 14 80 00       	push   $0x8014d4
  80018b:	e8 f8 00 00 00       	call   800288 <cprintf>
  800190:	83 c4 10             	add    $0x10,%esp
	return;
}
  800193:	c9                   	leave  
  800194:	c3                   	ret    

00800195 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800195:	55                   	push   %ebp
  800196:	89 e5                	mov    %esp,%ebp
  800198:	56                   	push   %esi
  800199:	53                   	push   %ebx
  80019a:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80019d:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8001a0:	e8 ed 09 00 00       	call   800b92 <sys_getenvid>
  8001a5:	25 ff 03 00 00       	and    $0x3ff,%eax
  8001aa:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001b1:	c1 e0 07             	shl    $0x7,%eax
  8001b4:	29 d0                	sub    %edx,%eax
  8001b6:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8001bb:	a3 0c 20 80 00       	mov    %eax,0x80200c
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8001c0:	85 db                	test   %ebx,%ebx
  8001c2:	7e 07                	jle    8001cb <libmain+0x36>
		binaryname = argv[0];
  8001c4:	8b 06                	mov    (%esi),%eax
  8001c6:	a3 08 20 80 00       	mov    %eax,0x802008

	// call user main routine
	umain(argc, argv);
  8001cb:	83 ec 08             	sub    $0x8,%esp
  8001ce:	56                   	push   %esi
  8001cf:	53                   	push   %ebx
  8001d0:	e8 5e fe ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8001d5:	e8 0a 00 00 00       	call   8001e4 <exit>
}
  8001da:	83 c4 10             	add    $0x10,%esp
  8001dd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8001e0:	5b                   	pop    %ebx
  8001e1:	5e                   	pop    %esi
  8001e2:	5d                   	pop    %ebp
  8001e3:	c3                   	ret    

008001e4 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8001e4:	55                   	push   %ebp
  8001e5:	89 e5                	mov    %esp,%ebp
  8001e7:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8001ea:	6a 00                	push   $0x0
  8001ec:	e8 60 09 00 00       	call   800b51 <sys_env_destroy>
}
  8001f1:	83 c4 10             	add    $0x10,%esp
  8001f4:	c9                   	leave  
  8001f5:	c3                   	ret    

008001f6 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8001f6:	55                   	push   %ebp
  8001f7:	89 e5                	mov    %esp,%ebp
  8001f9:	53                   	push   %ebx
  8001fa:	83 ec 04             	sub    $0x4,%esp
  8001fd:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800200:	8b 13                	mov    (%ebx),%edx
  800202:	8d 42 01             	lea    0x1(%edx),%eax
  800205:	89 03                	mov    %eax,(%ebx)
  800207:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80020a:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80020e:	3d ff 00 00 00       	cmp    $0xff,%eax
  800213:	75 1a                	jne    80022f <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800215:	83 ec 08             	sub    $0x8,%esp
  800218:	68 ff 00 00 00       	push   $0xff
  80021d:	8d 43 08             	lea    0x8(%ebx),%eax
  800220:	50                   	push   %eax
  800221:	e8 ee 08 00 00       	call   800b14 <sys_cputs>
		b->idx = 0;
  800226:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80022c:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80022f:	ff 43 04             	incl   0x4(%ebx)
}
  800232:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800235:	c9                   	leave  
  800236:	c3                   	ret    

00800237 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800237:	55                   	push   %ebp
  800238:	89 e5                	mov    %esp,%ebp
  80023a:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800240:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800247:	00 00 00 
	b.cnt = 0;
  80024a:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800251:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800254:	ff 75 0c             	pushl  0xc(%ebp)
  800257:	ff 75 08             	pushl  0x8(%ebp)
  80025a:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800260:	50                   	push   %eax
  800261:	68 f6 01 80 00       	push   $0x8001f6
  800266:	e8 51 01 00 00       	call   8003bc <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80026b:	83 c4 08             	add    $0x8,%esp
  80026e:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800274:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80027a:	50                   	push   %eax
  80027b:	e8 94 08 00 00       	call   800b14 <sys_cputs>

	return b.cnt;
}
  800280:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800286:	c9                   	leave  
  800287:	c3                   	ret    

00800288 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800288:	55                   	push   %ebp
  800289:	89 e5                	mov    %esp,%ebp
  80028b:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80028e:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800291:	50                   	push   %eax
  800292:	ff 75 08             	pushl  0x8(%ebp)
  800295:	e8 9d ff ff ff       	call   800237 <vcprintf>
	va_end(ap);

	return cnt;
}
  80029a:	c9                   	leave  
  80029b:	c3                   	ret    

0080029c <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80029c:	55                   	push   %ebp
  80029d:	89 e5                	mov    %esp,%ebp
  80029f:	57                   	push   %edi
  8002a0:	56                   	push   %esi
  8002a1:	53                   	push   %ebx
  8002a2:	83 ec 1c             	sub    $0x1c,%esp
  8002a5:	89 c7                	mov    %eax,%edi
  8002a7:	89 d6                	mov    %edx,%esi
  8002a9:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ac:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002af:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8002b2:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8002b5:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8002b8:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002bd:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8002c0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8002c3:	39 d3                	cmp    %edx,%ebx
  8002c5:	72 05                	jb     8002cc <printnum+0x30>
  8002c7:	39 45 10             	cmp    %eax,0x10(%ebp)
  8002ca:	77 45                	ja     800311 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8002cc:	83 ec 0c             	sub    $0xc,%esp
  8002cf:	ff 75 18             	pushl  0x18(%ebp)
  8002d2:	8b 45 14             	mov    0x14(%ebp),%eax
  8002d5:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8002d8:	53                   	push   %ebx
  8002d9:	ff 75 10             	pushl  0x10(%ebp)
  8002dc:	83 ec 08             	sub    $0x8,%esp
  8002df:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002e2:	ff 75 e0             	pushl  -0x20(%ebp)
  8002e5:	ff 75 dc             	pushl  -0x24(%ebp)
  8002e8:	ff 75 d8             	pushl  -0x28(%ebp)
  8002eb:	e8 40 0f 00 00       	call   801230 <__udivdi3>
  8002f0:	83 c4 18             	add    $0x18,%esp
  8002f3:	52                   	push   %edx
  8002f4:	50                   	push   %eax
  8002f5:	89 f2                	mov    %esi,%edx
  8002f7:	89 f8                	mov    %edi,%eax
  8002f9:	e8 9e ff ff ff       	call   80029c <printnum>
  8002fe:	83 c4 20             	add    $0x20,%esp
  800301:	eb 16                	jmp    800319 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800303:	83 ec 08             	sub    $0x8,%esp
  800306:	56                   	push   %esi
  800307:	ff 75 18             	pushl  0x18(%ebp)
  80030a:	ff d7                	call   *%edi
  80030c:	83 c4 10             	add    $0x10,%esp
  80030f:	eb 03                	jmp    800314 <printnum+0x78>
  800311:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800314:	4b                   	dec    %ebx
  800315:	85 db                	test   %ebx,%ebx
  800317:	7f ea                	jg     800303 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800319:	83 ec 08             	sub    $0x8,%esp
  80031c:	56                   	push   %esi
  80031d:	83 ec 04             	sub    $0x4,%esp
  800320:	ff 75 e4             	pushl  -0x1c(%ebp)
  800323:	ff 75 e0             	pushl  -0x20(%ebp)
  800326:	ff 75 dc             	pushl  -0x24(%ebp)
  800329:	ff 75 d8             	pushl  -0x28(%ebp)
  80032c:	e8 0f 10 00 00       	call   801340 <__umoddi3>
  800331:	83 c4 14             	add    $0x14,%esp
  800334:	0f be 80 4c 15 80 00 	movsbl 0x80154c(%eax),%eax
  80033b:	50                   	push   %eax
  80033c:	ff d7                	call   *%edi
}
  80033e:	83 c4 10             	add    $0x10,%esp
  800341:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800344:	5b                   	pop    %ebx
  800345:	5e                   	pop    %esi
  800346:	5f                   	pop    %edi
  800347:	5d                   	pop    %ebp
  800348:	c3                   	ret    

00800349 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800349:	55                   	push   %ebp
  80034a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80034c:	83 fa 01             	cmp    $0x1,%edx
  80034f:	7e 0e                	jle    80035f <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800351:	8b 10                	mov    (%eax),%edx
  800353:	8d 4a 08             	lea    0x8(%edx),%ecx
  800356:	89 08                	mov    %ecx,(%eax)
  800358:	8b 02                	mov    (%edx),%eax
  80035a:	8b 52 04             	mov    0x4(%edx),%edx
  80035d:	eb 22                	jmp    800381 <getuint+0x38>
	else if (lflag)
  80035f:	85 d2                	test   %edx,%edx
  800361:	74 10                	je     800373 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800363:	8b 10                	mov    (%eax),%edx
  800365:	8d 4a 04             	lea    0x4(%edx),%ecx
  800368:	89 08                	mov    %ecx,(%eax)
  80036a:	8b 02                	mov    (%edx),%eax
  80036c:	ba 00 00 00 00       	mov    $0x0,%edx
  800371:	eb 0e                	jmp    800381 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800373:	8b 10                	mov    (%eax),%edx
  800375:	8d 4a 04             	lea    0x4(%edx),%ecx
  800378:	89 08                	mov    %ecx,(%eax)
  80037a:	8b 02                	mov    (%edx),%eax
  80037c:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800381:	5d                   	pop    %ebp
  800382:	c3                   	ret    

00800383 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800383:	55                   	push   %ebp
  800384:	89 e5                	mov    %esp,%ebp
  800386:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800389:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80038c:	8b 10                	mov    (%eax),%edx
  80038e:	3b 50 04             	cmp    0x4(%eax),%edx
  800391:	73 0a                	jae    80039d <sprintputch+0x1a>
		*b->buf++ = ch;
  800393:	8d 4a 01             	lea    0x1(%edx),%ecx
  800396:	89 08                	mov    %ecx,(%eax)
  800398:	8b 45 08             	mov    0x8(%ebp),%eax
  80039b:	88 02                	mov    %al,(%edx)
}
  80039d:	5d                   	pop    %ebp
  80039e:	c3                   	ret    

0080039f <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80039f:	55                   	push   %ebp
  8003a0:	89 e5                	mov    %esp,%ebp
  8003a2:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8003a5:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8003a8:	50                   	push   %eax
  8003a9:	ff 75 10             	pushl  0x10(%ebp)
  8003ac:	ff 75 0c             	pushl  0xc(%ebp)
  8003af:	ff 75 08             	pushl  0x8(%ebp)
  8003b2:	e8 05 00 00 00       	call   8003bc <vprintfmt>
	va_end(ap);
}
  8003b7:	83 c4 10             	add    $0x10,%esp
  8003ba:	c9                   	leave  
  8003bb:	c3                   	ret    

008003bc <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8003bc:	55                   	push   %ebp
  8003bd:	89 e5                	mov    %esp,%ebp
  8003bf:	57                   	push   %edi
  8003c0:	56                   	push   %esi
  8003c1:	53                   	push   %ebx
  8003c2:	83 ec 2c             	sub    $0x2c,%esp
  8003c5:	8b 75 08             	mov    0x8(%ebp),%esi
  8003c8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8003cb:	8b 7d 10             	mov    0x10(%ebp),%edi
  8003ce:	eb 12                	jmp    8003e2 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8003d0:	85 c0                	test   %eax,%eax
  8003d2:	0f 84 68 03 00 00    	je     800740 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8003d8:	83 ec 08             	sub    $0x8,%esp
  8003db:	53                   	push   %ebx
  8003dc:	50                   	push   %eax
  8003dd:	ff d6                	call   *%esi
  8003df:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003e2:	47                   	inc    %edi
  8003e3:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8003e7:	83 f8 25             	cmp    $0x25,%eax
  8003ea:	75 e4                	jne    8003d0 <vprintfmt+0x14>
  8003ec:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8003f0:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8003f7:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003fe:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800405:	ba 00 00 00 00       	mov    $0x0,%edx
  80040a:	eb 07                	jmp    800413 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040c:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80040f:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800413:	8d 47 01             	lea    0x1(%edi),%eax
  800416:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800419:	0f b6 0f             	movzbl (%edi),%ecx
  80041c:	8a 07                	mov    (%edi),%al
  80041e:	83 e8 23             	sub    $0x23,%eax
  800421:	3c 55                	cmp    $0x55,%al
  800423:	0f 87 fe 02 00 00    	ja     800727 <vprintfmt+0x36b>
  800429:	0f b6 c0             	movzbl %al,%eax
  80042c:	ff 24 85 20 16 80 00 	jmp    *0x801620(,%eax,4)
  800433:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800436:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80043a:	eb d7                	jmp    800413 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80043c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80043f:	b8 00 00 00 00       	mov    $0x0,%eax
  800444:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800447:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80044a:	01 c0                	add    %eax,%eax
  80044c:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800450:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800453:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800456:	83 fa 09             	cmp    $0x9,%edx
  800459:	77 34                	ja     80048f <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80045b:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80045c:	eb e9                	jmp    800447 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80045e:	8b 45 14             	mov    0x14(%ebp),%eax
  800461:	8d 48 04             	lea    0x4(%eax),%ecx
  800464:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800467:	8b 00                	mov    (%eax),%eax
  800469:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80046c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80046f:	eb 24                	jmp    800495 <vprintfmt+0xd9>
  800471:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800475:	79 07                	jns    80047e <vprintfmt+0xc2>
  800477:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80047e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800481:	eb 90                	jmp    800413 <vprintfmt+0x57>
  800483:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800486:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80048d:	eb 84                	jmp    800413 <vprintfmt+0x57>
  80048f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800492:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800495:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800499:	0f 89 74 ff ff ff    	jns    800413 <vprintfmt+0x57>
				width = precision, precision = -1;
  80049f:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8004a2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004a5:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8004ac:	e9 62 ff ff ff       	jmp    800413 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8004b1:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8004b5:	e9 59 ff ff ff       	jmp    800413 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8004ba:	8b 45 14             	mov    0x14(%ebp),%eax
  8004bd:	8d 50 04             	lea    0x4(%eax),%edx
  8004c0:	89 55 14             	mov    %edx,0x14(%ebp)
  8004c3:	83 ec 08             	sub    $0x8,%esp
  8004c6:	53                   	push   %ebx
  8004c7:	ff 30                	pushl  (%eax)
  8004c9:	ff d6                	call   *%esi
			break;
  8004cb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ce:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8004d1:	e9 0c ff ff ff       	jmp    8003e2 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004d6:	8b 45 14             	mov    0x14(%ebp),%eax
  8004d9:	8d 50 04             	lea    0x4(%eax),%edx
  8004dc:	89 55 14             	mov    %edx,0x14(%ebp)
  8004df:	8b 00                	mov    (%eax),%eax
  8004e1:	85 c0                	test   %eax,%eax
  8004e3:	79 02                	jns    8004e7 <vprintfmt+0x12b>
  8004e5:	f7 d8                	neg    %eax
  8004e7:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8004e9:	83 f8 08             	cmp    $0x8,%eax
  8004ec:	7f 0b                	jg     8004f9 <vprintfmt+0x13d>
  8004ee:	8b 04 85 80 17 80 00 	mov    0x801780(,%eax,4),%eax
  8004f5:	85 c0                	test   %eax,%eax
  8004f7:	75 18                	jne    800511 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8004f9:	52                   	push   %edx
  8004fa:	68 64 15 80 00       	push   $0x801564
  8004ff:	53                   	push   %ebx
  800500:	56                   	push   %esi
  800501:	e8 99 fe ff ff       	call   80039f <printfmt>
  800506:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800509:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80050c:	e9 d1 fe ff ff       	jmp    8003e2 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800511:	50                   	push   %eax
  800512:	68 6d 15 80 00       	push   $0x80156d
  800517:	53                   	push   %ebx
  800518:	56                   	push   %esi
  800519:	e8 81 fe ff ff       	call   80039f <printfmt>
  80051e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800521:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800524:	e9 b9 fe ff ff       	jmp    8003e2 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800529:	8b 45 14             	mov    0x14(%ebp),%eax
  80052c:	8d 50 04             	lea    0x4(%eax),%edx
  80052f:	89 55 14             	mov    %edx,0x14(%ebp)
  800532:	8b 38                	mov    (%eax),%edi
  800534:	85 ff                	test   %edi,%edi
  800536:	75 05                	jne    80053d <vprintfmt+0x181>
				p = "(null)";
  800538:	bf 5d 15 80 00       	mov    $0x80155d,%edi
			if (width > 0 && padc != '-')
  80053d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800541:	0f 8e 90 00 00 00    	jle    8005d7 <vprintfmt+0x21b>
  800547:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80054b:	0f 84 8e 00 00 00    	je     8005df <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800551:	83 ec 08             	sub    $0x8,%esp
  800554:	ff 75 d0             	pushl  -0x30(%ebp)
  800557:	57                   	push   %edi
  800558:	e8 70 02 00 00       	call   8007cd <strnlen>
  80055d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800560:	29 c1                	sub    %eax,%ecx
  800562:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800565:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800568:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80056c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80056f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800572:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800574:	eb 0d                	jmp    800583 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800576:	83 ec 08             	sub    $0x8,%esp
  800579:	53                   	push   %ebx
  80057a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80057d:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80057f:	4f                   	dec    %edi
  800580:	83 c4 10             	add    $0x10,%esp
  800583:	85 ff                	test   %edi,%edi
  800585:	7f ef                	jg     800576 <vprintfmt+0x1ba>
  800587:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80058a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80058d:	89 c8                	mov    %ecx,%eax
  80058f:	85 c9                	test   %ecx,%ecx
  800591:	79 05                	jns    800598 <vprintfmt+0x1dc>
  800593:	b8 00 00 00 00       	mov    $0x0,%eax
  800598:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80059b:	29 c1                	sub    %eax,%ecx
  80059d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8005a0:	89 75 08             	mov    %esi,0x8(%ebp)
  8005a3:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005a6:	eb 3d                	jmp    8005e5 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8005a8:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8005ac:	74 19                	je     8005c7 <vprintfmt+0x20b>
  8005ae:	0f be c0             	movsbl %al,%eax
  8005b1:	83 e8 20             	sub    $0x20,%eax
  8005b4:	83 f8 5e             	cmp    $0x5e,%eax
  8005b7:	76 0e                	jbe    8005c7 <vprintfmt+0x20b>
					putch('?', putdat);
  8005b9:	83 ec 08             	sub    $0x8,%esp
  8005bc:	53                   	push   %ebx
  8005bd:	6a 3f                	push   $0x3f
  8005bf:	ff 55 08             	call   *0x8(%ebp)
  8005c2:	83 c4 10             	add    $0x10,%esp
  8005c5:	eb 0b                	jmp    8005d2 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8005c7:	83 ec 08             	sub    $0x8,%esp
  8005ca:	53                   	push   %ebx
  8005cb:	52                   	push   %edx
  8005cc:	ff 55 08             	call   *0x8(%ebp)
  8005cf:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005d2:	ff 4d e4             	decl   -0x1c(%ebp)
  8005d5:	eb 0e                	jmp    8005e5 <vprintfmt+0x229>
  8005d7:	89 75 08             	mov    %esi,0x8(%ebp)
  8005da:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005dd:	eb 06                	jmp    8005e5 <vprintfmt+0x229>
  8005df:	89 75 08             	mov    %esi,0x8(%ebp)
  8005e2:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005e5:	47                   	inc    %edi
  8005e6:	8a 47 ff             	mov    -0x1(%edi),%al
  8005e9:	0f be d0             	movsbl %al,%edx
  8005ec:	85 d2                	test   %edx,%edx
  8005ee:	74 1d                	je     80060d <vprintfmt+0x251>
  8005f0:	85 f6                	test   %esi,%esi
  8005f2:	78 b4                	js     8005a8 <vprintfmt+0x1ec>
  8005f4:	4e                   	dec    %esi
  8005f5:	79 b1                	jns    8005a8 <vprintfmt+0x1ec>
  8005f7:	8b 75 08             	mov    0x8(%ebp),%esi
  8005fa:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005fd:	eb 14                	jmp    800613 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8005ff:	83 ec 08             	sub    $0x8,%esp
  800602:	53                   	push   %ebx
  800603:	6a 20                	push   $0x20
  800605:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800607:	4f                   	dec    %edi
  800608:	83 c4 10             	add    $0x10,%esp
  80060b:	eb 06                	jmp    800613 <vprintfmt+0x257>
  80060d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800610:	8b 75 08             	mov    0x8(%ebp),%esi
  800613:	85 ff                	test   %edi,%edi
  800615:	7f e8                	jg     8005ff <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800617:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80061a:	e9 c3 fd ff ff       	jmp    8003e2 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80061f:	83 fa 01             	cmp    $0x1,%edx
  800622:	7e 16                	jle    80063a <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800624:	8b 45 14             	mov    0x14(%ebp),%eax
  800627:	8d 50 08             	lea    0x8(%eax),%edx
  80062a:	89 55 14             	mov    %edx,0x14(%ebp)
  80062d:	8b 50 04             	mov    0x4(%eax),%edx
  800630:	8b 00                	mov    (%eax),%eax
  800632:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800635:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800638:	eb 32                	jmp    80066c <vprintfmt+0x2b0>
	else if (lflag)
  80063a:	85 d2                	test   %edx,%edx
  80063c:	74 18                	je     800656 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80063e:	8b 45 14             	mov    0x14(%ebp),%eax
  800641:	8d 50 04             	lea    0x4(%eax),%edx
  800644:	89 55 14             	mov    %edx,0x14(%ebp)
  800647:	8b 00                	mov    (%eax),%eax
  800649:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80064c:	89 c1                	mov    %eax,%ecx
  80064e:	c1 f9 1f             	sar    $0x1f,%ecx
  800651:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800654:	eb 16                	jmp    80066c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800656:	8b 45 14             	mov    0x14(%ebp),%eax
  800659:	8d 50 04             	lea    0x4(%eax),%edx
  80065c:	89 55 14             	mov    %edx,0x14(%ebp)
  80065f:	8b 00                	mov    (%eax),%eax
  800661:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800664:	89 c1                	mov    %eax,%ecx
  800666:	c1 f9 1f             	sar    $0x1f,%ecx
  800669:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80066c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80066f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800672:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800676:	79 76                	jns    8006ee <vprintfmt+0x332>
				putch('-', putdat);
  800678:	83 ec 08             	sub    $0x8,%esp
  80067b:	53                   	push   %ebx
  80067c:	6a 2d                	push   $0x2d
  80067e:	ff d6                	call   *%esi
				num = -(long long) num;
  800680:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800683:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800686:	f7 d8                	neg    %eax
  800688:	83 d2 00             	adc    $0x0,%edx
  80068b:	f7 da                	neg    %edx
  80068d:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800690:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800695:	eb 5c                	jmp    8006f3 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800697:	8d 45 14             	lea    0x14(%ebp),%eax
  80069a:	e8 aa fc ff ff       	call   800349 <getuint>
			base = 10;
  80069f:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8006a4:	eb 4d                	jmp    8006f3 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8006a6:	8d 45 14             	lea    0x14(%ebp),%eax
  8006a9:	e8 9b fc ff ff       	call   800349 <getuint>
			base = 8;
  8006ae:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8006b3:	eb 3e                	jmp    8006f3 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8006b5:	83 ec 08             	sub    $0x8,%esp
  8006b8:	53                   	push   %ebx
  8006b9:	6a 30                	push   $0x30
  8006bb:	ff d6                	call   *%esi
			putch('x', putdat);
  8006bd:	83 c4 08             	add    $0x8,%esp
  8006c0:	53                   	push   %ebx
  8006c1:	6a 78                	push   $0x78
  8006c3:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8006c5:	8b 45 14             	mov    0x14(%ebp),%eax
  8006c8:	8d 50 04             	lea    0x4(%eax),%edx
  8006cb:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006ce:	8b 00                	mov    (%eax),%eax
  8006d0:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8006d5:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8006d8:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8006dd:	eb 14                	jmp    8006f3 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006df:	8d 45 14             	lea    0x14(%ebp),%eax
  8006e2:	e8 62 fc ff ff       	call   800349 <getuint>
			base = 16;
  8006e7:	b9 10 00 00 00       	mov    $0x10,%ecx
  8006ec:	eb 05                	jmp    8006f3 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8006ee:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006f3:	83 ec 0c             	sub    $0xc,%esp
  8006f6:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8006fa:	57                   	push   %edi
  8006fb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006fe:	51                   	push   %ecx
  8006ff:	52                   	push   %edx
  800700:	50                   	push   %eax
  800701:	89 da                	mov    %ebx,%edx
  800703:	89 f0                	mov    %esi,%eax
  800705:	e8 92 fb ff ff       	call   80029c <printnum>
			break;
  80070a:	83 c4 20             	add    $0x20,%esp
  80070d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800710:	e9 cd fc ff ff       	jmp    8003e2 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800715:	83 ec 08             	sub    $0x8,%esp
  800718:	53                   	push   %ebx
  800719:	51                   	push   %ecx
  80071a:	ff d6                	call   *%esi
			break;
  80071c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80071f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800722:	e9 bb fc ff ff       	jmp    8003e2 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800727:	83 ec 08             	sub    $0x8,%esp
  80072a:	53                   	push   %ebx
  80072b:	6a 25                	push   $0x25
  80072d:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80072f:	83 c4 10             	add    $0x10,%esp
  800732:	eb 01                	jmp    800735 <vprintfmt+0x379>
  800734:	4f                   	dec    %edi
  800735:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800739:	75 f9                	jne    800734 <vprintfmt+0x378>
  80073b:	e9 a2 fc ff ff       	jmp    8003e2 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800740:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800743:	5b                   	pop    %ebx
  800744:	5e                   	pop    %esi
  800745:	5f                   	pop    %edi
  800746:	5d                   	pop    %ebp
  800747:	c3                   	ret    

00800748 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800748:	55                   	push   %ebp
  800749:	89 e5                	mov    %esp,%ebp
  80074b:	83 ec 18             	sub    $0x18,%esp
  80074e:	8b 45 08             	mov    0x8(%ebp),%eax
  800751:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800754:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800757:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80075b:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80075e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800765:	85 c0                	test   %eax,%eax
  800767:	74 26                	je     80078f <vsnprintf+0x47>
  800769:	85 d2                	test   %edx,%edx
  80076b:	7e 29                	jle    800796 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80076d:	ff 75 14             	pushl  0x14(%ebp)
  800770:	ff 75 10             	pushl  0x10(%ebp)
  800773:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800776:	50                   	push   %eax
  800777:	68 83 03 80 00       	push   $0x800383
  80077c:	e8 3b fc ff ff       	call   8003bc <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800781:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800784:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800787:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80078a:	83 c4 10             	add    $0x10,%esp
  80078d:	eb 0c                	jmp    80079b <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80078f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800794:	eb 05                	jmp    80079b <vsnprintf+0x53>
  800796:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80079b:	c9                   	leave  
  80079c:	c3                   	ret    

0080079d <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80079d:	55                   	push   %ebp
  80079e:	89 e5                	mov    %esp,%ebp
  8007a0:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8007a3:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8007a6:	50                   	push   %eax
  8007a7:	ff 75 10             	pushl  0x10(%ebp)
  8007aa:	ff 75 0c             	pushl  0xc(%ebp)
  8007ad:	ff 75 08             	pushl  0x8(%ebp)
  8007b0:	e8 93 ff ff ff       	call   800748 <vsnprintf>
	va_end(ap);

	return rc;
}
  8007b5:	c9                   	leave  
  8007b6:	c3                   	ret    

008007b7 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8007b7:	55                   	push   %ebp
  8007b8:	89 e5                	mov    %esp,%ebp
  8007ba:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8007bd:	b8 00 00 00 00       	mov    $0x0,%eax
  8007c2:	eb 01                	jmp    8007c5 <strlen+0xe>
		n++;
  8007c4:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8007c5:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8007c9:	75 f9                	jne    8007c4 <strlen+0xd>
		n++;
	return n;
}
  8007cb:	5d                   	pop    %ebp
  8007cc:	c3                   	ret    

008007cd <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8007cd:	55                   	push   %ebp
  8007ce:	89 e5                	mov    %esp,%ebp
  8007d0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007d3:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007d6:	ba 00 00 00 00       	mov    $0x0,%edx
  8007db:	eb 01                	jmp    8007de <strnlen+0x11>
		n++;
  8007dd:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007de:	39 c2                	cmp    %eax,%edx
  8007e0:	74 08                	je     8007ea <strnlen+0x1d>
  8007e2:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8007e6:	75 f5                	jne    8007dd <strnlen+0x10>
  8007e8:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8007ea:	5d                   	pop    %ebp
  8007eb:	c3                   	ret    

008007ec <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8007ec:	55                   	push   %ebp
  8007ed:	89 e5                	mov    %esp,%ebp
  8007ef:	53                   	push   %ebx
  8007f0:	8b 45 08             	mov    0x8(%ebp),%eax
  8007f3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8007f6:	89 c2                	mov    %eax,%edx
  8007f8:	42                   	inc    %edx
  8007f9:	41                   	inc    %ecx
  8007fa:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8007fd:	88 5a ff             	mov    %bl,-0x1(%edx)
  800800:	84 db                	test   %bl,%bl
  800802:	75 f4                	jne    8007f8 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800804:	5b                   	pop    %ebx
  800805:	5d                   	pop    %ebp
  800806:	c3                   	ret    

00800807 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800807:	55                   	push   %ebp
  800808:	89 e5                	mov    %esp,%ebp
  80080a:	53                   	push   %ebx
  80080b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80080e:	53                   	push   %ebx
  80080f:	e8 a3 ff ff ff       	call   8007b7 <strlen>
  800814:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800817:	ff 75 0c             	pushl  0xc(%ebp)
  80081a:	01 d8                	add    %ebx,%eax
  80081c:	50                   	push   %eax
  80081d:	e8 ca ff ff ff       	call   8007ec <strcpy>
	return dst;
}
  800822:	89 d8                	mov    %ebx,%eax
  800824:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800827:	c9                   	leave  
  800828:	c3                   	ret    

00800829 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800829:	55                   	push   %ebp
  80082a:	89 e5                	mov    %esp,%ebp
  80082c:	56                   	push   %esi
  80082d:	53                   	push   %ebx
  80082e:	8b 75 08             	mov    0x8(%ebp),%esi
  800831:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800834:	89 f3                	mov    %esi,%ebx
  800836:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800839:	89 f2                	mov    %esi,%edx
  80083b:	eb 0c                	jmp    800849 <strncpy+0x20>
		*dst++ = *src;
  80083d:	42                   	inc    %edx
  80083e:	8a 01                	mov    (%ecx),%al
  800840:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800843:	80 39 01             	cmpb   $0x1,(%ecx)
  800846:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800849:	39 da                	cmp    %ebx,%edx
  80084b:	75 f0                	jne    80083d <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80084d:	89 f0                	mov    %esi,%eax
  80084f:	5b                   	pop    %ebx
  800850:	5e                   	pop    %esi
  800851:	5d                   	pop    %ebp
  800852:	c3                   	ret    

00800853 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800853:	55                   	push   %ebp
  800854:	89 e5                	mov    %esp,%ebp
  800856:	56                   	push   %esi
  800857:	53                   	push   %ebx
  800858:	8b 75 08             	mov    0x8(%ebp),%esi
  80085b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80085e:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800861:	85 c0                	test   %eax,%eax
  800863:	74 1e                	je     800883 <strlcpy+0x30>
  800865:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800869:	89 f2                	mov    %esi,%edx
  80086b:	eb 05                	jmp    800872 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80086d:	42                   	inc    %edx
  80086e:	41                   	inc    %ecx
  80086f:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800872:	39 c2                	cmp    %eax,%edx
  800874:	74 08                	je     80087e <strlcpy+0x2b>
  800876:	8a 19                	mov    (%ecx),%bl
  800878:	84 db                	test   %bl,%bl
  80087a:	75 f1                	jne    80086d <strlcpy+0x1a>
  80087c:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80087e:	c6 00 00             	movb   $0x0,(%eax)
  800881:	eb 02                	jmp    800885 <strlcpy+0x32>
  800883:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800885:	29 f0                	sub    %esi,%eax
}
  800887:	5b                   	pop    %ebx
  800888:	5e                   	pop    %esi
  800889:	5d                   	pop    %ebp
  80088a:	c3                   	ret    

0080088b <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80088b:	55                   	push   %ebp
  80088c:	89 e5                	mov    %esp,%ebp
  80088e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800891:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800894:	eb 02                	jmp    800898 <strcmp+0xd>
		p++, q++;
  800896:	41                   	inc    %ecx
  800897:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800898:	8a 01                	mov    (%ecx),%al
  80089a:	84 c0                	test   %al,%al
  80089c:	74 04                	je     8008a2 <strcmp+0x17>
  80089e:	3a 02                	cmp    (%edx),%al
  8008a0:	74 f4                	je     800896 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8008a2:	0f b6 c0             	movzbl %al,%eax
  8008a5:	0f b6 12             	movzbl (%edx),%edx
  8008a8:	29 d0                	sub    %edx,%eax
}
  8008aa:	5d                   	pop    %ebp
  8008ab:	c3                   	ret    

008008ac <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8008ac:	55                   	push   %ebp
  8008ad:	89 e5                	mov    %esp,%ebp
  8008af:	53                   	push   %ebx
  8008b0:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b3:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008b6:	89 c3                	mov    %eax,%ebx
  8008b8:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8008bb:	eb 02                	jmp    8008bf <strncmp+0x13>
		n--, p++, q++;
  8008bd:	40                   	inc    %eax
  8008be:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8008bf:	39 d8                	cmp    %ebx,%eax
  8008c1:	74 14                	je     8008d7 <strncmp+0x2b>
  8008c3:	8a 08                	mov    (%eax),%cl
  8008c5:	84 c9                	test   %cl,%cl
  8008c7:	74 04                	je     8008cd <strncmp+0x21>
  8008c9:	3a 0a                	cmp    (%edx),%cl
  8008cb:	74 f0                	je     8008bd <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8008cd:	0f b6 00             	movzbl (%eax),%eax
  8008d0:	0f b6 12             	movzbl (%edx),%edx
  8008d3:	29 d0                	sub    %edx,%eax
  8008d5:	eb 05                	jmp    8008dc <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8008d7:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8008dc:	5b                   	pop    %ebx
  8008dd:	5d                   	pop    %ebp
  8008de:	c3                   	ret    

008008df <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8008df:	55                   	push   %ebp
  8008e0:	89 e5                	mov    %esp,%ebp
  8008e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e5:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008e8:	eb 05                	jmp    8008ef <strchr+0x10>
		if (*s == c)
  8008ea:	38 ca                	cmp    %cl,%dl
  8008ec:	74 0c                	je     8008fa <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8008ee:	40                   	inc    %eax
  8008ef:	8a 10                	mov    (%eax),%dl
  8008f1:	84 d2                	test   %dl,%dl
  8008f3:	75 f5                	jne    8008ea <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8008f5:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008fa:	5d                   	pop    %ebp
  8008fb:	c3                   	ret    

008008fc <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8008fc:	55                   	push   %ebp
  8008fd:	89 e5                	mov    %esp,%ebp
  8008ff:	8b 45 08             	mov    0x8(%ebp),%eax
  800902:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800905:	eb 05                	jmp    80090c <strfind+0x10>
		if (*s == c)
  800907:	38 ca                	cmp    %cl,%dl
  800909:	74 07                	je     800912 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80090b:	40                   	inc    %eax
  80090c:	8a 10                	mov    (%eax),%dl
  80090e:	84 d2                	test   %dl,%dl
  800910:	75 f5                	jne    800907 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800912:	5d                   	pop    %ebp
  800913:	c3                   	ret    

00800914 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800914:	55                   	push   %ebp
  800915:	89 e5                	mov    %esp,%ebp
  800917:	57                   	push   %edi
  800918:	56                   	push   %esi
  800919:	53                   	push   %ebx
  80091a:	8b 7d 08             	mov    0x8(%ebp),%edi
  80091d:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800920:	85 c9                	test   %ecx,%ecx
  800922:	74 36                	je     80095a <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800924:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80092a:	75 28                	jne    800954 <memset+0x40>
  80092c:	f6 c1 03             	test   $0x3,%cl
  80092f:	75 23                	jne    800954 <memset+0x40>
		c &= 0xFF;
  800931:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800935:	89 d3                	mov    %edx,%ebx
  800937:	c1 e3 08             	shl    $0x8,%ebx
  80093a:	89 d6                	mov    %edx,%esi
  80093c:	c1 e6 18             	shl    $0x18,%esi
  80093f:	89 d0                	mov    %edx,%eax
  800941:	c1 e0 10             	shl    $0x10,%eax
  800944:	09 f0                	or     %esi,%eax
  800946:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800948:	89 d8                	mov    %ebx,%eax
  80094a:	09 d0                	or     %edx,%eax
  80094c:	c1 e9 02             	shr    $0x2,%ecx
  80094f:	fc                   	cld    
  800950:	f3 ab                	rep stos %eax,%es:(%edi)
  800952:	eb 06                	jmp    80095a <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800954:	8b 45 0c             	mov    0xc(%ebp),%eax
  800957:	fc                   	cld    
  800958:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80095a:	89 f8                	mov    %edi,%eax
  80095c:	5b                   	pop    %ebx
  80095d:	5e                   	pop    %esi
  80095e:	5f                   	pop    %edi
  80095f:	5d                   	pop    %ebp
  800960:	c3                   	ret    

00800961 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800961:	55                   	push   %ebp
  800962:	89 e5                	mov    %esp,%ebp
  800964:	57                   	push   %edi
  800965:	56                   	push   %esi
  800966:	8b 45 08             	mov    0x8(%ebp),%eax
  800969:	8b 75 0c             	mov    0xc(%ebp),%esi
  80096c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80096f:	39 c6                	cmp    %eax,%esi
  800971:	73 33                	jae    8009a6 <memmove+0x45>
  800973:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800976:	39 d0                	cmp    %edx,%eax
  800978:	73 2c                	jae    8009a6 <memmove+0x45>
		s += n;
		d += n;
  80097a:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80097d:	89 d6                	mov    %edx,%esi
  80097f:	09 fe                	or     %edi,%esi
  800981:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800987:	75 13                	jne    80099c <memmove+0x3b>
  800989:	f6 c1 03             	test   $0x3,%cl
  80098c:	75 0e                	jne    80099c <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80098e:	83 ef 04             	sub    $0x4,%edi
  800991:	8d 72 fc             	lea    -0x4(%edx),%esi
  800994:	c1 e9 02             	shr    $0x2,%ecx
  800997:	fd                   	std    
  800998:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80099a:	eb 07                	jmp    8009a3 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80099c:	4f                   	dec    %edi
  80099d:	8d 72 ff             	lea    -0x1(%edx),%esi
  8009a0:	fd                   	std    
  8009a1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8009a3:	fc                   	cld    
  8009a4:	eb 1d                	jmp    8009c3 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009a6:	89 f2                	mov    %esi,%edx
  8009a8:	09 c2                	or     %eax,%edx
  8009aa:	f6 c2 03             	test   $0x3,%dl
  8009ad:	75 0f                	jne    8009be <memmove+0x5d>
  8009af:	f6 c1 03             	test   $0x3,%cl
  8009b2:	75 0a                	jne    8009be <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8009b4:	c1 e9 02             	shr    $0x2,%ecx
  8009b7:	89 c7                	mov    %eax,%edi
  8009b9:	fc                   	cld    
  8009ba:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8009bc:	eb 05                	jmp    8009c3 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8009be:	89 c7                	mov    %eax,%edi
  8009c0:	fc                   	cld    
  8009c1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8009c3:	5e                   	pop    %esi
  8009c4:	5f                   	pop    %edi
  8009c5:	5d                   	pop    %ebp
  8009c6:	c3                   	ret    

008009c7 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8009c7:	55                   	push   %ebp
  8009c8:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8009ca:	ff 75 10             	pushl  0x10(%ebp)
  8009cd:	ff 75 0c             	pushl  0xc(%ebp)
  8009d0:	ff 75 08             	pushl  0x8(%ebp)
  8009d3:	e8 89 ff ff ff       	call   800961 <memmove>
}
  8009d8:	c9                   	leave  
  8009d9:	c3                   	ret    

008009da <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8009da:	55                   	push   %ebp
  8009db:	89 e5                	mov    %esp,%ebp
  8009dd:	56                   	push   %esi
  8009de:	53                   	push   %ebx
  8009df:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009e5:	89 c6                	mov    %eax,%esi
  8009e7:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009ea:	eb 14                	jmp    800a00 <memcmp+0x26>
		if (*s1 != *s2)
  8009ec:	8a 08                	mov    (%eax),%cl
  8009ee:	8a 1a                	mov    (%edx),%bl
  8009f0:	38 d9                	cmp    %bl,%cl
  8009f2:	74 0a                	je     8009fe <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8009f4:	0f b6 c1             	movzbl %cl,%eax
  8009f7:	0f b6 db             	movzbl %bl,%ebx
  8009fa:	29 d8                	sub    %ebx,%eax
  8009fc:	eb 0b                	jmp    800a09 <memcmp+0x2f>
		s1++, s2++;
  8009fe:	40                   	inc    %eax
  8009ff:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a00:	39 f0                	cmp    %esi,%eax
  800a02:	75 e8                	jne    8009ec <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800a04:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a09:	5b                   	pop    %ebx
  800a0a:	5e                   	pop    %esi
  800a0b:	5d                   	pop    %ebp
  800a0c:	c3                   	ret    

00800a0d <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800a0d:	55                   	push   %ebp
  800a0e:	89 e5                	mov    %esp,%ebp
  800a10:	53                   	push   %ebx
  800a11:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800a14:	89 c1                	mov    %eax,%ecx
  800a16:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800a19:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a1d:	eb 08                	jmp    800a27 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a1f:	0f b6 10             	movzbl (%eax),%edx
  800a22:	39 da                	cmp    %ebx,%edx
  800a24:	74 05                	je     800a2b <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a26:	40                   	inc    %eax
  800a27:	39 c8                	cmp    %ecx,%eax
  800a29:	72 f4                	jb     800a1f <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800a2b:	5b                   	pop    %ebx
  800a2c:	5d                   	pop    %ebp
  800a2d:	c3                   	ret    

00800a2e <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800a2e:	55                   	push   %ebp
  800a2f:	89 e5                	mov    %esp,%ebp
  800a31:	57                   	push   %edi
  800a32:	56                   	push   %esi
  800a33:	53                   	push   %ebx
  800a34:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a37:	eb 01                	jmp    800a3a <strtol+0xc>
		s++;
  800a39:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a3a:	8a 01                	mov    (%ecx),%al
  800a3c:	3c 20                	cmp    $0x20,%al
  800a3e:	74 f9                	je     800a39 <strtol+0xb>
  800a40:	3c 09                	cmp    $0x9,%al
  800a42:	74 f5                	je     800a39 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800a44:	3c 2b                	cmp    $0x2b,%al
  800a46:	75 08                	jne    800a50 <strtol+0x22>
		s++;
  800a48:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a49:	bf 00 00 00 00       	mov    $0x0,%edi
  800a4e:	eb 11                	jmp    800a61 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800a50:	3c 2d                	cmp    $0x2d,%al
  800a52:	75 08                	jne    800a5c <strtol+0x2e>
		s++, neg = 1;
  800a54:	41                   	inc    %ecx
  800a55:	bf 01 00 00 00       	mov    $0x1,%edi
  800a5a:	eb 05                	jmp    800a61 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a5c:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a61:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a65:	0f 84 87 00 00 00    	je     800af2 <strtol+0xc4>
  800a6b:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a6f:	75 27                	jne    800a98 <strtol+0x6a>
  800a71:	80 39 30             	cmpb   $0x30,(%ecx)
  800a74:	75 22                	jne    800a98 <strtol+0x6a>
  800a76:	e9 88 00 00 00       	jmp    800b03 <strtol+0xd5>
		s += 2, base = 16;
  800a7b:	83 c1 02             	add    $0x2,%ecx
  800a7e:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a85:	eb 11                	jmp    800a98 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a87:	41                   	inc    %ecx
  800a88:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a8f:	eb 07                	jmp    800a98 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a91:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a98:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a9d:	8a 11                	mov    (%ecx),%dl
  800a9f:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800aa2:	80 fb 09             	cmp    $0x9,%bl
  800aa5:	77 08                	ja     800aaf <strtol+0x81>
			dig = *s - '0';
  800aa7:	0f be d2             	movsbl %dl,%edx
  800aaa:	83 ea 30             	sub    $0x30,%edx
  800aad:	eb 22                	jmp    800ad1 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800aaf:	8d 72 9f             	lea    -0x61(%edx),%esi
  800ab2:	89 f3                	mov    %esi,%ebx
  800ab4:	80 fb 19             	cmp    $0x19,%bl
  800ab7:	77 08                	ja     800ac1 <strtol+0x93>
			dig = *s - 'a' + 10;
  800ab9:	0f be d2             	movsbl %dl,%edx
  800abc:	83 ea 57             	sub    $0x57,%edx
  800abf:	eb 10                	jmp    800ad1 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800ac1:	8d 72 bf             	lea    -0x41(%edx),%esi
  800ac4:	89 f3                	mov    %esi,%ebx
  800ac6:	80 fb 19             	cmp    $0x19,%bl
  800ac9:	77 14                	ja     800adf <strtol+0xb1>
			dig = *s - 'A' + 10;
  800acb:	0f be d2             	movsbl %dl,%edx
  800ace:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800ad1:	3b 55 10             	cmp    0x10(%ebp),%edx
  800ad4:	7d 09                	jge    800adf <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800ad6:	41                   	inc    %ecx
  800ad7:	0f af 45 10          	imul   0x10(%ebp),%eax
  800adb:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800add:	eb be                	jmp    800a9d <strtol+0x6f>

	if (endptr)
  800adf:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ae3:	74 05                	je     800aea <strtol+0xbc>
		*endptr = (char *) s;
  800ae5:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ae8:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800aea:	85 ff                	test   %edi,%edi
  800aec:	74 21                	je     800b0f <strtol+0xe1>
  800aee:	f7 d8                	neg    %eax
  800af0:	eb 1d                	jmp    800b0f <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800af2:	80 39 30             	cmpb   $0x30,(%ecx)
  800af5:	75 9a                	jne    800a91 <strtol+0x63>
  800af7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800afb:	0f 84 7a ff ff ff    	je     800a7b <strtol+0x4d>
  800b01:	eb 84                	jmp    800a87 <strtol+0x59>
  800b03:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b07:	0f 84 6e ff ff ff    	je     800a7b <strtol+0x4d>
  800b0d:	eb 89                	jmp    800a98 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800b0f:	5b                   	pop    %ebx
  800b10:	5e                   	pop    %esi
  800b11:	5f                   	pop    %edi
  800b12:	5d                   	pop    %ebp
  800b13:	c3                   	ret    

00800b14 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800b14:	55                   	push   %ebp
  800b15:	89 e5                	mov    %esp,%ebp
  800b17:	57                   	push   %edi
  800b18:	56                   	push   %esi
  800b19:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1a:	b8 00 00 00 00       	mov    $0x0,%eax
  800b1f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b22:	8b 55 08             	mov    0x8(%ebp),%edx
  800b25:	89 c3                	mov    %eax,%ebx
  800b27:	89 c7                	mov    %eax,%edi
  800b29:	89 c6                	mov    %eax,%esi
  800b2b:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800b2d:	5b                   	pop    %ebx
  800b2e:	5e                   	pop    %esi
  800b2f:	5f                   	pop    %edi
  800b30:	5d                   	pop    %ebp
  800b31:	c3                   	ret    

00800b32 <sys_cgetc>:

int
sys_cgetc(void)
{
  800b32:	55                   	push   %ebp
  800b33:	89 e5                	mov    %esp,%ebp
  800b35:	57                   	push   %edi
  800b36:	56                   	push   %esi
  800b37:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b38:	ba 00 00 00 00       	mov    $0x0,%edx
  800b3d:	b8 01 00 00 00       	mov    $0x1,%eax
  800b42:	89 d1                	mov    %edx,%ecx
  800b44:	89 d3                	mov    %edx,%ebx
  800b46:	89 d7                	mov    %edx,%edi
  800b48:	89 d6                	mov    %edx,%esi
  800b4a:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800b4c:	5b                   	pop    %ebx
  800b4d:	5e                   	pop    %esi
  800b4e:	5f                   	pop    %edi
  800b4f:	5d                   	pop    %ebp
  800b50:	c3                   	ret    

00800b51 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800b51:	55                   	push   %ebp
  800b52:	89 e5                	mov    %esp,%ebp
  800b54:	57                   	push   %edi
  800b55:	56                   	push   %esi
  800b56:	53                   	push   %ebx
  800b57:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b5a:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b5f:	b8 03 00 00 00       	mov    $0x3,%eax
  800b64:	8b 55 08             	mov    0x8(%ebp),%edx
  800b67:	89 cb                	mov    %ecx,%ebx
  800b69:	89 cf                	mov    %ecx,%edi
  800b6b:	89 ce                	mov    %ecx,%esi
  800b6d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b6f:	85 c0                	test   %eax,%eax
  800b71:	7e 17                	jle    800b8a <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b73:	83 ec 0c             	sub    $0xc,%esp
  800b76:	50                   	push   %eax
  800b77:	6a 03                	push   $0x3
  800b79:	68 a4 17 80 00       	push   $0x8017a4
  800b7e:	6a 23                	push   $0x23
  800b80:	68 c1 17 80 00       	push   $0x8017c1
  800b85:	e8 df 05 00 00       	call   801169 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b8a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b8d:	5b                   	pop    %ebx
  800b8e:	5e                   	pop    %esi
  800b8f:	5f                   	pop    %edi
  800b90:	5d                   	pop    %ebp
  800b91:	c3                   	ret    

00800b92 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b92:	55                   	push   %ebp
  800b93:	89 e5                	mov    %esp,%ebp
  800b95:	57                   	push   %edi
  800b96:	56                   	push   %esi
  800b97:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b98:	ba 00 00 00 00       	mov    $0x0,%edx
  800b9d:	b8 02 00 00 00       	mov    $0x2,%eax
  800ba2:	89 d1                	mov    %edx,%ecx
  800ba4:	89 d3                	mov    %edx,%ebx
  800ba6:	89 d7                	mov    %edx,%edi
  800ba8:	89 d6                	mov    %edx,%esi
  800baa:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800bac:	5b                   	pop    %ebx
  800bad:	5e                   	pop    %esi
  800bae:	5f                   	pop    %edi
  800baf:	5d                   	pop    %ebp
  800bb0:	c3                   	ret    

00800bb1 <sys_yield>:

void
sys_yield(void)
{
  800bb1:	55                   	push   %ebp
  800bb2:	89 e5                	mov    %esp,%ebp
  800bb4:	57                   	push   %edi
  800bb5:	56                   	push   %esi
  800bb6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bb7:	ba 00 00 00 00       	mov    $0x0,%edx
  800bbc:	b8 0a 00 00 00       	mov    $0xa,%eax
  800bc1:	89 d1                	mov    %edx,%ecx
  800bc3:	89 d3                	mov    %edx,%ebx
  800bc5:	89 d7                	mov    %edx,%edi
  800bc7:	89 d6                	mov    %edx,%esi
  800bc9:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800bcb:	5b                   	pop    %ebx
  800bcc:	5e                   	pop    %esi
  800bcd:	5f                   	pop    %edi
  800bce:	5d                   	pop    %ebp
  800bcf:	c3                   	ret    

00800bd0 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800bd0:	55                   	push   %ebp
  800bd1:	89 e5                	mov    %esp,%ebp
  800bd3:	57                   	push   %edi
  800bd4:	56                   	push   %esi
  800bd5:	53                   	push   %ebx
  800bd6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bd9:	be 00 00 00 00       	mov    $0x0,%esi
  800bde:	b8 04 00 00 00       	mov    $0x4,%eax
  800be3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800be6:	8b 55 08             	mov    0x8(%ebp),%edx
  800be9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800bec:	89 f7                	mov    %esi,%edi
  800bee:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bf0:	85 c0                	test   %eax,%eax
  800bf2:	7e 17                	jle    800c0b <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bf4:	83 ec 0c             	sub    $0xc,%esp
  800bf7:	50                   	push   %eax
  800bf8:	6a 04                	push   $0x4
  800bfa:	68 a4 17 80 00       	push   $0x8017a4
  800bff:	6a 23                	push   $0x23
  800c01:	68 c1 17 80 00       	push   $0x8017c1
  800c06:	e8 5e 05 00 00       	call   801169 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800c0b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c0e:	5b                   	pop    %ebx
  800c0f:	5e                   	pop    %esi
  800c10:	5f                   	pop    %edi
  800c11:	5d                   	pop    %ebp
  800c12:	c3                   	ret    

00800c13 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800c13:	55                   	push   %ebp
  800c14:	89 e5                	mov    %esp,%ebp
  800c16:	57                   	push   %edi
  800c17:	56                   	push   %esi
  800c18:	53                   	push   %ebx
  800c19:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c1c:	b8 05 00 00 00       	mov    $0x5,%eax
  800c21:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c24:	8b 55 08             	mov    0x8(%ebp),%edx
  800c27:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c2a:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c2d:	8b 75 18             	mov    0x18(%ebp),%esi
  800c30:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c32:	85 c0                	test   %eax,%eax
  800c34:	7e 17                	jle    800c4d <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c36:	83 ec 0c             	sub    $0xc,%esp
  800c39:	50                   	push   %eax
  800c3a:	6a 05                	push   $0x5
  800c3c:	68 a4 17 80 00       	push   $0x8017a4
  800c41:	6a 23                	push   $0x23
  800c43:	68 c1 17 80 00       	push   $0x8017c1
  800c48:	e8 1c 05 00 00       	call   801169 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800c4d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c50:	5b                   	pop    %ebx
  800c51:	5e                   	pop    %esi
  800c52:	5f                   	pop    %edi
  800c53:	5d                   	pop    %ebp
  800c54:	c3                   	ret    

00800c55 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800c55:	55                   	push   %ebp
  800c56:	89 e5                	mov    %esp,%ebp
  800c58:	57                   	push   %edi
  800c59:	56                   	push   %esi
  800c5a:	53                   	push   %ebx
  800c5b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c5e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c63:	b8 06 00 00 00       	mov    $0x6,%eax
  800c68:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c6b:	8b 55 08             	mov    0x8(%ebp),%edx
  800c6e:	89 df                	mov    %ebx,%edi
  800c70:	89 de                	mov    %ebx,%esi
  800c72:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c74:	85 c0                	test   %eax,%eax
  800c76:	7e 17                	jle    800c8f <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c78:	83 ec 0c             	sub    $0xc,%esp
  800c7b:	50                   	push   %eax
  800c7c:	6a 06                	push   $0x6
  800c7e:	68 a4 17 80 00       	push   $0x8017a4
  800c83:	6a 23                	push   $0x23
  800c85:	68 c1 17 80 00       	push   $0x8017c1
  800c8a:	e8 da 04 00 00       	call   801169 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c8f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c92:	5b                   	pop    %ebx
  800c93:	5e                   	pop    %esi
  800c94:	5f                   	pop    %edi
  800c95:	5d                   	pop    %ebp
  800c96:	c3                   	ret    

00800c97 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c97:	55                   	push   %ebp
  800c98:	89 e5                	mov    %esp,%ebp
  800c9a:	57                   	push   %edi
  800c9b:	56                   	push   %esi
  800c9c:	53                   	push   %ebx
  800c9d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ca0:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ca5:	b8 08 00 00 00       	mov    $0x8,%eax
  800caa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cad:	8b 55 08             	mov    0x8(%ebp),%edx
  800cb0:	89 df                	mov    %ebx,%edi
  800cb2:	89 de                	mov    %ebx,%esi
  800cb4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cb6:	85 c0                	test   %eax,%eax
  800cb8:	7e 17                	jle    800cd1 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cba:	83 ec 0c             	sub    $0xc,%esp
  800cbd:	50                   	push   %eax
  800cbe:	6a 08                	push   $0x8
  800cc0:	68 a4 17 80 00       	push   $0x8017a4
  800cc5:	6a 23                	push   $0x23
  800cc7:	68 c1 17 80 00       	push   $0x8017c1
  800ccc:	e8 98 04 00 00       	call   801169 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800cd1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cd4:	5b                   	pop    %ebx
  800cd5:	5e                   	pop    %esi
  800cd6:	5f                   	pop    %edi
  800cd7:	5d                   	pop    %ebp
  800cd8:	c3                   	ret    

00800cd9 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800cd9:	55                   	push   %ebp
  800cda:	89 e5                	mov    %esp,%ebp
  800cdc:	57                   	push   %edi
  800cdd:	56                   	push   %esi
  800cde:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cdf:	ba 00 00 00 00       	mov    $0x0,%edx
  800ce4:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ce9:	89 d1                	mov    %edx,%ecx
  800ceb:	89 d3                	mov    %edx,%ebx
  800ced:	89 d7                	mov    %edx,%edi
  800cef:	89 d6                	mov    %edx,%esi
  800cf1:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800cf3:	5b                   	pop    %ebx
  800cf4:	5e                   	pop    %esi
  800cf5:	5f                   	pop    %edi
  800cf6:	5d                   	pop    %ebp
  800cf7:	c3                   	ret    

00800cf8 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800cf8:	55                   	push   %ebp
  800cf9:	89 e5                	mov    %esp,%ebp
  800cfb:	57                   	push   %edi
  800cfc:	56                   	push   %esi
  800cfd:	53                   	push   %ebx
  800cfe:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d01:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d06:	b8 09 00 00 00       	mov    $0x9,%eax
  800d0b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d0e:	8b 55 08             	mov    0x8(%ebp),%edx
  800d11:	89 df                	mov    %ebx,%edi
  800d13:	89 de                	mov    %ebx,%esi
  800d15:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d17:	85 c0                	test   %eax,%eax
  800d19:	7e 17                	jle    800d32 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d1b:	83 ec 0c             	sub    $0xc,%esp
  800d1e:	50                   	push   %eax
  800d1f:	6a 09                	push   $0x9
  800d21:	68 a4 17 80 00       	push   $0x8017a4
  800d26:	6a 23                	push   $0x23
  800d28:	68 c1 17 80 00       	push   $0x8017c1
  800d2d:	e8 37 04 00 00       	call   801169 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800d32:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d35:	5b                   	pop    %ebx
  800d36:	5e                   	pop    %esi
  800d37:	5f                   	pop    %edi
  800d38:	5d                   	pop    %ebp
  800d39:	c3                   	ret    

00800d3a <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800d3a:	55                   	push   %ebp
  800d3b:	89 e5                	mov    %esp,%ebp
  800d3d:	57                   	push   %edi
  800d3e:	56                   	push   %esi
  800d3f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d40:	be 00 00 00 00       	mov    $0x0,%esi
  800d45:	b8 0c 00 00 00       	mov    $0xc,%eax
  800d4a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d4d:	8b 55 08             	mov    0x8(%ebp),%edx
  800d50:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d53:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d56:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d58:	5b                   	pop    %ebx
  800d59:	5e                   	pop    %esi
  800d5a:	5f                   	pop    %edi
  800d5b:	5d                   	pop    %ebp
  800d5c:	c3                   	ret    

00800d5d <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d5d:	55                   	push   %ebp
  800d5e:	89 e5                	mov    %esp,%ebp
  800d60:	57                   	push   %edi
  800d61:	56                   	push   %esi
  800d62:	53                   	push   %ebx
  800d63:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d66:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d6b:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d70:	8b 55 08             	mov    0x8(%ebp),%edx
  800d73:	89 cb                	mov    %ecx,%ebx
  800d75:	89 cf                	mov    %ecx,%edi
  800d77:	89 ce                	mov    %ecx,%esi
  800d79:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d7b:	85 c0                	test   %eax,%eax
  800d7d:	7e 17                	jle    800d96 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d7f:	83 ec 0c             	sub    $0xc,%esp
  800d82:	50                   	push   %eax
  800d83:	6a 0d                	push   $0xd
  800d85:	68 a4 17 80 00       	push   $0x8017a4
  800d8a:	6a 23                	push   $0x23
  800d8c:	68 c1 17 80 00       	push   $0x8017c1
  800d91:	e8 d3 03 00 00       	call   801169 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d96:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d99:	5b                   	pop    %ebx
  800d9a:	5e                   	pop    %esi
  800d9b:	5f                   	pop    %edi
  800d9c:	5d                   	pop    %ebp
  800d9d:	c3                   	ret    

00800d9e <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800d9e:	55                   	push   %ebp
  800d9f:	89 e5                	mov    %esp,%ebp
  800da1:	53                   	push   %ebx
  800da2:	83 ec 04             	sub    $0x4,%esp
  800da5:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800da8:	8b 18                	mov    (%eax),%ebx
	//   You should make three system calls.


    
    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800daa:	89 da                	mov    %ebx,%edx
  800dac:	c1 ea 0c             	shr    $0xc,%edx
  800daf:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800db6:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800dba:	74 05                	je     800dc1 <pgfault+0x23>
  800dbc:	f6 c6 08             	test   $0x8,%dh
  800dbf:	75 14                	jne    800dd5 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800dc1:	83 ec 04             	sub    $0x4,%esp
  800dc4:	68 d0 17 80 00       	push   $0x8017d0
  800dc9:	6a 28                	push   $0x28
  800dcb:	68 34 18 80 00       	push   $0x801834
  800dd0:	e8 94 03 00 00       	call   801169 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800dd5:	83 ec 04             	sub    $0x4,%esp
  800dd8:	6a 07                	push   $0x7
  800dda:	68 00 f0 7f 00       	push   $0x7ff000
  800ddf:	6a 00                	push   $0x0
  800de1:	e8 ea fd ff ff       	call   800bd0 <sys_page_alloc>
  800de6:	83 c4 10             	add    $0x10,%esp
  800de9:	85 c0                	test   %eax,%eax
  800deb:	74 14                	je     800e01 <pgfault+0x63>
        panic("pgfault: no phys mem");
  800ded:	83 ec 04             	sub    $0x4,%esp
  800df0:	68 3f 18 80 00       	push   $0x80183f
  800df5:	6a 34                	push   $0x34
  800df7:	68 34 18 80 00       	push   $0x801834
  800dfc:	e8 68 03 00 00       	call   801169 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800e01:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800e07:	83 ec 04             	sub    $0x4,%esp
  800e0a:	68 00 10 00 00       	push   $0x1000
  800e0f:	53                   	push   %ebx
  800e10:	68 00 f0 7f 00       	push   $0x7ff000
  800e15:	e8 47 fb ff ff       	call   800961 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800e1a:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800e21:	53                   	push   %ebx
  800e22:	6a 00                	push   $0x0
  800e24:	68 00 f0 7f 00       	push   $0x7ff000
  800e29:	6a 00                	push   $0x0
  800e2b:	e8 e3 fd ff ff       	call   800c13 <sys_page_map>
  800e30:	83 c4 20             	add    $0x20,%esp
  800e33:	85 c0                	test   %eax,%eax
  800e35:	74 14                	je     800e4b <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800e37:	83 ec 04             	sub    $0x4,%esp
  800e3a:	68 54 18 80 00       	push   $0x801854
  800e3f:	6a 41                	push   $0x41
  800e41:	68 34 18 80 00       	push   $0x801834
  800e46:	e8 1e 03 00 00       	call   801169 <_panic>


	//panic("pgfault not implemented");
}
  800e4b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800e4e:	c9                   	leave  
  800e4f:	c3                   	ret    

00800e50 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800e50:	55                   	push   %ebp
  800e51:	89 e5                	mov    %esp,%ebp
  800e53:	57                   	push   %edi
  800e54:	56                   	push   %esi
  800e55:	53                   	push   %ebx
  800e56:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800e59:	68 9e 0d 80 00       	push   $0x800d9e
  800e5e:	e8 4c 03 00 00       	call   8011af <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800e63:	b8 07 00 00 00       	mov    $0x7,%eax
  800e68:	cd 30                	int    $0x30
  800e6a:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800e6d:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800e70:	83 c4 10             	add    $0x10,%esp
  800e73:	85 c0                	test   %eax,%eax
  800e75:	79 17                	jns    800e8e <fork+0x3e>
        panic("fork: cannot create child env");
  800e77:	83 ec 04             	sub    $0x4,%esp
  800e7a:	68 67 18 80 00       	push   $0x801867
  800e7f:	68 96 00 00 00       	push   $0x96
  800e84:	68 34 18 80 00       	push   $0x801834
  800e89:	e8 db 02 00 00       	call   801169 <_panic>
    } else if (envid == 0) {
  800e8e:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800e92:	75 2a                	jne    800ebe <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800e94:	e8 f9 fc ff ff       	call   800b92 <sys_getenvid>
  800e99:	25 ff 03 00 00       	and    $0x3ff,%eax
  800e9e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ea5:	c1 e0 07             	shl    $0x7,%eax
  800ea8:	29 d0                	sub    %edx,%eax
  800eaa:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800eaf:	a3 0c 20 80 00       	mov    %eax,0x80200c
        return 0;
  800eb4:	b8 00 00 00 00       	mov    $0x0,%eax
  800eb9:	e9 88 01 00 00       	jmp    801046 <fork+0x1f6>
  800ebe:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800ec5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800ec8:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800ecf:	a8 01                	test   $0x1,%al
  800ed1:	0f 84 fe 00 00 00    	je     800fd5 <fork+0x185>
            continue;

        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800ed7:	c1 e7 0a             	shl    $0xa,%edi
  800eda:	be 00 00 00 00       	mov    $0x0,%esi
  800edf:	89 fb                	mov    %edi,%ebx
  800ee1:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800ee3:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800ee9:	75 34                	jne    800f1f <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800eeb:	83 ec 04             	sub    $0x4,%esp
  800eee:	6a 07                	push   $0x7
  800ef0:	68 00 f0 bf ee       	push   $0xeebff000
  800ef5:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ef8:	e8 d3 fc ff ff       	call   800bd0 <sys_page_alloc>
  800efd:	83 c4 10             	add    $0x10,%esp
  800f00:	85 c0                	test   %eax,%eax
  800f02:	0f 84 c0 00 00 00    	je     800fc8 <fork+0x178>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800f08:	83 ec 04             	sub    $0x4,%esp
  800f0b:	68 85 18 80 00       	push   $0x801885
  800f10:	68 ac 00 00 00       	push   $0xac
  800f15:	68 34 18 80 00       	push   $0x801834
  800f1a:	e8 4a 02 00 00       	call   801169 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800f1f:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800f26:	a8 01                	test   $0x1,%al
  800f28:	0f 84 9a 00 00 00    	je     800fc8 <fork+0x178>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800f2e:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800f35:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if ((pte & PTE_W) || (pte & PTE_COW)) {
  800f38:	a9 02 08 00 00       	test   $0x802,%eax
  800f3d:	74 5d                	je     800f9c <fork+0x14c>
        if (sys_page_map(0,
  800f3f:	83 ec 0c             	sub    $0xc,%esp
  800f42:	68 05 08 00 00       	push   $0x805
  800f47:	53                   	push   %ebx
  800f48:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f4b:	53                   	push   %ebx
  800f4c:	6a 00                	push   $0x0
  800f4e:	e8 c0 fc ff ff       	call   800c13 <sys_page_map>
  800f53:	83 c4 20             	add    $0x20,%esp
  800f56:	85 c0                	test   %eax,%eax
  800f58:	74 14                	je     800f6e <fork+0x11e>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800f5a:	83 ec 04             	sub    $0x4,%esp
  800f5d:	68 a0 18 80 00       	push   $0x8018a0
  800f62:	6a 66                	push   $0x66
  800f64:	68 34 18 80 00       	push   $0x801834
  800f69:	e8 fb 01 00 00       	call   801169 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800f6e:	83 ec 0c             	sub    $0xc,%esp
  800f71:	68 05 08 00 00       	push   $0x805
  800f76:	53                   	push   %ebx
  800f77:	6a 00                	push   $0x0
  800f79:	53                   	push   %ebx
  800f7a:	6a 00                	push   $0x0
  800f7c:	e8 92 fc ff ff       	call   800c13 <sys_page_map>
  800f81:	83 c4 20             	add    $0x20,%esp
  800f84:	85 c0                	test   %eax,%eax
  800f86:	74 40                	je     800fc8 <fork+0x178>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  800f88:	83 ec 04             	sub    $0x4,%esp
  800f8b:	68 b7 18 80 00       	push   $0x8018b7
  800f90:	6a 6f                	push   $0x6f
  800f92:	68 34 18 80 00       	push   $0x801834
  800f97:	e8 cd 01 00 00       	call   801169 <_panic>
    } else if (sys_page_map(0,
  800f9c:	83 ec 0c             	sub    $0xc,%esp
  800f9f:	6a 05                	push   $0x5
  800fa1:	53                   	push   %ebx
  800fa2:	ff 75 e4             	pushl  -0x1c(%ebp)
  800fa5:	53                   	push   %ebx
  800fa6:	6a 00                	push   $0x0
  800fa8:	e8 66 fc ff ff       	call   800c13 <sys_page_map>
  800fad:	83 c4 20             	add    $0x20,%esp
  800fb0:	85 c0                	test   %eax,%eax
  800fb2:	74 14                	je     800fc8 <fork+0x178>
                            va,
                            envid,
                            va,
                            PTE_U | PTE_P))
        panic("duppage: map ro error");
  800fb4:	83 ec 04             	sub    $0x4,%esp
  800fb7:	68 d2 18 80 00       	push   $0x8018d2
  800fbc:	6a 75                	push   $0x75
  800fbe:	68 34 18 80 00       	push   $0x801834
  800fc3:	e8 a1 01 00 00       	call   801169 <_panic>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;

        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  800fc8:	46                   	inc    %esi
  800fc9:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  800fcf:	0f 85 0a ff ff ff    	jne    800edf <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  800fd5:	ff 45 e0             	incl   -0x20(%ebp)
  800fd8:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800fdb:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  800fe0:	0f 85 df fe ff ff    	jne    800ec5 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  800fe6:	a1 0c 20 80 00       	mov    0x80200c,%eax
  800feb:	8b 40 64             	mov    0x64(%eax),%eax
  800fee:	83 ec 08             	sub    $0x8,%esp
  800ff1:	50                   	push   %eax
  800ff2:	ff 75 dc             	pushl  -0x24(%ebp)
  800ff5:	e8 fe fc ff ff       	call   800cf8 <sys_env_set_pgfault_upcall>
  800ffa:	83 c4 10             	add    $0x10,%esp
  800ffd:	85 c0                	test   %eax,%eax
  800fff:	74 17                	je     801018 <fork+0x1c8>
        panic("fork: cannot set pgfault upcall");
  801001:	83 ec 04             	sub    $0x4,%esp
  801004:	68 14 18 80 00       	push   $0x801814
  801009:	68 b8 00 00 00       	push   $0xb8
  80100e:	68 34 18 80 00       	push   $0x801834
  801013:	e8 51 01 00 00       	call   801169 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  801018:	83 ec 08             	sub    $0x8,%esp
  80101b:	6a 02                	push   $0x2
  80101d:	ff 75 dc             	pushl  -0x24(%ebp)
  801020:	e8 72 fc ff ff       	call   800c97 <sys_env_set_status>
  801025:	83 c4 10             	add    $0x10,%esp
  801028:	85 c0                	test   %eax,%eax
  80102a:	74 17                	je     801043 <fork+0x1f3>
        panic("fork: cannot set env status");
  80102c:	83 ec 04             	sub    $0x4,%esp
  80102f:	68 e8 18 80 00       	push   $0x8018e8
  801034:	68 bc 00 00 00       	push   $0xbc
  801039:	68 34 18 80 00       	push   $0x801834
  80103e:	e8 26 01 00 00       	call   801169 <_panic>

    return envid;
  801043:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  801046:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801049:	5b                   	pop    %ebx
  80104a:	5e                   	pop    %esi
  80104b:	5f                   	pop    %edi
  80104c:	5d                   	pop    %ebp
  80104d:	c3                   	ret    

0080104e <sfork>:

// Challenge!
int
sfork(void)
{
  80104e:	55                   	push   %ebp
  80104f:	89 e5                	mov    %esp,%ebp
  801051:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  801054:	68 04 19 80 00       	push   $0x801904
  801059:	68 c7 00 00 00       	push   $0xc7
  80105e:	68 34 18 80 00       	push   $0x801834
  801063:	e8 01 01 00 00       	call   801169 <_panic>

00801068 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801068:	55                   	push   %ebp
  801069:	89 e5                	mov    %esp,%ebp
  80106b:	56                   	push   %esi
  80106c:	53                   	push   %ebx
  80106d:	8b 75 08             	mov    0x8(%ebp),%esi
  801070:	8b 45 0c             	mov    0xc(%ebp),%eax
  801073:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801076:	85 c0                	test   %eax,%eax
  801078:	75 05                	jne    80107f <ipc_recv+0x17>
        pg = (void *)UTOP;
  80107a:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  80107f:	83 ec 0c             	sub    $0xc,%esp
  801082:	50                   	push   %eax
  801083:	e8 d5 fc ff ff       	call   800d5d <sys_ipc_recv>
  801088:	83 c4 10             	add    $0x10,%esp
  80108b:	85 c0                	test   %eax,%eax
  80108d:	74 16                	je     8010a5 <ipc_recv+0x3d>
        if (from_env_store)
  80108f:	85 f6                	test   %esi,%esi
  801091:	74 06                	je     801099 <ipc_recv+0x31>
            *from_env_store = 0;
  801093:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801099:	85 db                	test   %ebx,%ebx
  80109b:	74 2c                	je     8010c9 <ipc_recv+0x61>
            *perm_store = 0;
  80109d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8010a3:	eb 24                	jmp    8010c9 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  8010a5:	85 f6                	test   %esi,%esi
  8010a7:	74 0a                	je     8010b3 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  8010a9:	a1 0c 20 80 00       	mov    0x80200c,%eax
  8010ae:	8b 40 74             	mov    0x74(%eax),%eax
  8010b1:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  8010b3:	85 db                	test   %ebx,%ebx
  8010b5:	74 0a                	je     8010c1 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  8010b7:	a1 0c 20 80 00       	mov    0x80200c,%eax
  8010bc:	8b 40 78             	mov    0x78(%eax),%eax
  8010bf:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  8010c1:	a1 0c 20 80 00       	mov    0x80200c,%eax
  8010c6:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  8010c9:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8010cc:	5b                   	pop    %ebx
  8010cd:	5e                   	pop    %esi
  8010ce:	5d                   	pop    %ebp
  8010cf:	c3                   	ret    

008010d0 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  8010d0:	55                   	push   %ebp
  8010d1:	89 e5                	mov    %esp,%ebp
  8010d3:	57                   	push   %edi
  8010d4:	56                   	push   %esi
  8010d5:	53                   	push   %ebx
  8010d6:	83 ec 0c             	sub    $0xc,%esp
  8010d9:	8b 75 0c             	mov    0xc(%ebp),%esi
  8010dc:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8010df:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  8010e2:	85 db                	test   %ebx,%ebx
  8010e4:	75 0c                	jne    8010f2 <ipc_send+0x22>
        pg = (void *)UTOP;
  8010e6:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  8010eb:	eb 05                	jmp    8010f2 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  8010ed:	e8 bf fa ff ff       	call   800bb1 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  8010f2:	57                   	push   %edi
  8010f3:	53                   	push   %ebx
  8010f4:	56                   	push   %esi
  8010f5:	ff 75 08             	pushl  0x8(%ebp)
  8010f8:	e8 3d fc ff ff       	call   800d3a <sys_ipc_try_send>
  8010fd:	83 c4 10             	add    $0x10,%esp
  801100:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801103:	74 e8                	je     8010ed <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801105:	85 c0                	test   %eax,%eax
  801107:	74 14                	je     80111d <ipc_send+0x4d>
        panic("ipc_send: error");
  801109:	83 ec 04             	sub    $0x4,%esp
  80110c:	68 1a 19 80 00       	push   $0x80191a
  801111:	6a 53                	push   $0x53
  801113:	68 2a 19 80 00       	push   $0x80192a
  801118:	e8 4c 00 00 00       	call   801169 <_panic>
    }
	//panic("ipc_send not implemented");
}
  80111d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801120:	5b                   	pop    %ebx
  801121:	5e                   	pop    %esi
  801122:	5f                   	pop    %edi
  801123:	5d                   	pop    %ebp
  801124:	c3                   	ret    

00801125 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801125:	55                   	push   %ebp
  801126:	89 e5                	mov    %esp,%ebp
  801128:	53                   	push   %ebx
  801129:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  80112c:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801131:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801138:	89 d0                	mov    %edx,%eax
  80113a:	c1 e0 07             	shl    $0x7,%eax
  80113d:	29 d8                	sub    %ebx,%eax
  80113f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801144:	8b 40 50             	mov    0x50(%eax),%eax
  801147:	39 c8                	cmp    %ecx,%eax
  801149:	75 0d                	jne    801158 <ipc_find_env+0x33>
			return envs[i].env_id;
  80114b:	c1 e2 07             	shl    $0x7,%edx
  80114e:	29 da                	sub    %ebx,%edx
  801150:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801156:	eb 0e                	jmp    801166 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801158:	42                   	inc    %edx
  801159:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  80115f:	75 d0                	jne    801131 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801161:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801166:	5b                   	pop    %ebx
  801167:	5d                   	pop    %ebp
  801168:	c3                   	ret    

00801169 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  801169:	55                   	push   %ebp
  80116a:	89 e5                	mov    %esp,%ebp
  80116c:	56                   	push   %esi
  80116d:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80116e:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  801171:	8b 35 08 20 80 00    	mov    0x802008,%esi
  801177:	e8 16 fa ff ff       	call   800b92 <sys_getenvid>
  80117c:	83 ec 0c             	sub    $0xc,%esp
  80117f:	ff 75 0c             	pushl  0xc(%ebp)
  801182:	ff 75 08             	pushl  0x8(%ebp)
  801185:	56                   	push   %esi
  801186:	50                   	push   %eax
  801187:	68 34 19 80 00       	push   $0x801934
  80118c:	e8 f7 f0 ff ff       	call   800288 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  801191:	83 c4 18             	add    $0x18,%esp
  801194:	53                   	push   %ebx
  801195:	ff 75 10             	pushl  0x10(%ebp)
  801198:	e8 9a f0 ff ff       	call   800237 <vcprintf>
	cprintf("\n");
  80119d:	c7 04 24 b2 14 80 00 	movl   $0x8014b2,(%esp)
  8011a4:	e8 df f0 ff ff       	call   800288 <cprintf>
  8011a9:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8011ac:	cc                   	int3   
  8011ad:	eb fd                	jmp    8011ac <_panic+0x43>

008011af <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  8011af:	55                   	push   %ebp
  8011b0:	89 e5                	mov    %esp,%ebp
  8011b2:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  8011b5:	83 3d 10 20 80 00 00 	cmpl   $0x0,0x802010
  8011bc:	75 3e                	jne    8011fc <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  8011be:	83 ec 04             	sub    $0x4,%esp
  8011c1:	6a 07                	push   $0x7
  8011c3:	68 00 f0 bf ee       	push   $0xeebff000
  8011c8:	6a 00                	push   $0x0
  8011ca:	e8 01 fa ff ff       	call   800bd0 <sys_page_alloc>
  8011cf:	83 c4 10             	add    $0x10,%esp
  8011d2:	85 c0                	test   %eax,%eax
  8011d4:	74 14                	je     8011ea <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  8011d6:	83 ec 04             	sub    $0x4,%esp
  8011d9:	68 58 19 80 00       	push   $0x801958
  8011de:	6a 23                	push   $0x23
  8011e0:	68 7c 19 80 00       	push   $0x80197c
  8011e5:	e8 7f ff ff ff       	call   801169 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  8011ea:	83 ec 08             	sub    $0x8,%esp
  8011ed:	68 06 12 80 00       	push   $0x801206
  8011f2:	6a 00                	push   $0x0
  8011f4:	e8 ff fa ff ff       	call   800cf8 <sys_env_set_pgfault_upcall>
  8011f9:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  8011fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8011ff:	a3 10 20 80 00       	mov    %eax,0x802010
}
  801204:	c9                   	leave  
  801205:	c3                   	ret    

00801206 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801206:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801207:	a1 10 20 80 00       	mov    0x802010,%eax
	call *%eax
  80120c:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  80120e:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801211:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801213:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801217:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  80121b:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  80121c:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  80121f:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801221:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801224:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801227:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801228:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  80122b:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  80122c:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  80122d:	c3                   	ret    
  80122e:	66 90                	xchg   %ax,%ax

00801230 <__udivdi3>:
  801230:	55                   	push   %ebp
  801231:	57                   	push   %edi
  801232:	56                   	push   %esi
  801233:	53                   	push   %ebx
  801234:	83 ec 1c             	sub    $0x1c,%esp
  801237:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  80123b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  80123f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801243:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801247:	89 ca                	mov    %ecx,%edx
  801249:	89 f8                	mov    %edi,%eax
  80124b:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  80124f:	85 f6                	test   %esi,%esi
  801251:	75 2d                	jne    801280 <__udivdi3+0x50>
  801253:	39 cf                	cmp    %ecx,%edi
  801255:	77 65                	ja     8012bc <__udivdi3+0x8c>
  801257:	89 fd                	mov    %edi,%ebp
  801259:	85 ff                	test   %edi,%edi
  80125b:	75 0b                	jne    801268 <__udivdi3+0x38>
  80125d:	b8 01 00 00 00       	mov    $0x1,%eax
  801262:	31 d2                	xor    %edx,%edx
  801264:	f7 f7                	div    %edi
  801266:	89 c5                	mov    %eax,%ebp
  801268:	31 d2                	xor    %edx,%edx
  80126a:	89 c8                	mov    %ecx,%eax
  80126c:	f7 f5                	div    %ebp
  80126e:	89 c1                	mov    %eax,%ecx
  801270:	89 d8                	mov    %ebx,%eax
  801272:	f7 f5                	div    %ebp
  801274:	89 cf                	mov    %ecx,%edi
  801276:	89 fa                	mov    %edi,%edx
  801278:	83 c4 1c             	add    $0x1c,%esp
  80127b:	5b                   	pop    %ebx
  80127c:	5e                   	pop    %esi
  80127d:	5f                   	pop    %edi
  80127e:	5d                   	pop    %ebp
  80127f:	c3                   	ret    
  801280:	39 ce                	cmp    %ecx,%esi
  801282:	77 28                	ja     8012ac <__udivdi3+0x7c>
  801284:	0f bd fe             	bsr    %esi,%edi
  801287:	83 f7 1f             	xor    $0x1f,%edi
  80128a:	75 40                	jne    8012cc <__udivdi3+0x9c>
  80128c:	39 ce                	cmp    %ecx,%esi
  80128e:	72 0a                	jb     80129a <__udivdi3+0x6a>
  801290:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801294:	0f 87 9e 00 00 00    	ja     801338 <__udivdi3+0x108>
  80129a:	b8 01 00 00 00       	mov    $0x1,%eax
  80129f:	89 fa                	mov    %edi,%edx
  8012a1:	83 c4 1c             	add    $0x1c,%esp
  8012a4:	5b                   	pop    %ebx
  8012a5:	5e                   	pop    %esi
  8012a6:	5f                   	pop    %edi
  8012a7:	5d                   	pop    %ebp
  8012a8:	c3                   	ret    
  8012a9:	8d 76 00             	lea    0x0(%esi),%esi
  8012ac:	31 ff                	xor    %edi,%edi
  8012ae:	31 c0                	xor    %eax,%eax
  8012b0:	89 fa                	mov    %edi,%edx
  8012b2:	83 c4 1c             	add    $0x1c,%esp
  8012b5:	5b                   	pop    %ebx
  8012b6:	5e                   	pop    %esi
  8012b7:	5f                   	pop    %edi
  8012b8:	5d                   	pop    %ebp
  8012b9:	c3                   	ret    
  8012ba:	66 90                	xchg   %ax,%ax
  8012bc:	89 d8                	mov    %ebx,%eax
  8012be:	f7 f7                	div    %edi
  8012c0:	31 ff                	xor    %edi,%edi
  8012c2:	89 fa                	mov    %edi,%edx
  8012c4:	83 c4 1c             	add    $0x1c,%esp
  8012c7:	5b                   	pop    %ebx
  8012c8:	5e                   	pop    %esi
  8012c9:	5f                   	pop    %edi
  8012ca:	5d                   	pop    %ebp
  8012cb:	c3                   	ret    
  8012cc:	bd 20 00 00 00       	mov    $0x20,%ebp
  8012d1:	89 eb                	mov    %ebp,%ebx
  8012d3:	29 fb                	sub    %edi,%ebx
  8012d5:	89 f9                	mov    %edi,%ecx
  8012d7:	d3 e6                	shl    %cl,%esi
  8012d9:	89 c5                	mov    %eax,%ebp
  8012db:	88 d9                	mov    %bl,%cl
  8012dd:	d3 ed                	shr    %cl,%ebp
  8012df:	89 e9                	mov    %ebp,%ecx
  8012e1:	09 f1                	or     %esi,%ecx
  8012e3:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8012e7:	89 f9                	mov    %edi,%ecx
  8012e9:	d3 e0                	shl    %cl,%eax
  8012eb:	89 c5                	mov    %eax,%ebp
  8012ed:	89 d6                	mov    %edx,%esi
  8012ef:	88 d9                	mov    %bl,%cl
  8012f1:	d3 ee                	shr    %cl,%esi
  8012f3:	89 f9                	mov    %edi,%ecx
  8012f5:	d3 e2                	shl    %cl,%edx
  8012f7:	8b 44 24 08          	mov    0x8(%esp),%eax
  8012fb:	88 d9                	mov    %bl,%cl
  8012fd:	d3 e8                	shr    %cl,%eax
  8012ff:	09 c2                	or     %eax,%edx
  801301:	89 d0                	mov    %edx,%eax
  801303:	89 f2                	mov    %esi,%edx
  801305:	f7 74 24 0c          	divl   0xc(%esp)
  801309:	89 d6                	mov    %edx,%esi
  80130b:	89 c3                	mov    %eax,%ebx
  80130d:	f7 e5                	mul    %ebp
  80130f:	39 d6                	cmp    %edx,%esi
  801311:	72 19                	jb     80132c <__udivdi3+0xfc>
  801313:	74 0b                	je     801320 <__udivdi3+0xf0>
  801315:	89 d8                	mov    %ebx,%eax
  801317:	31 ff                	xor    %edi,%edi
  801319:	e9 58 ff ff ff       	jmp    801276 <__udivdi3+0x46>
  80131e:	66 90                	xchg   %ax,%ax
  801320:	8b 54 24 08          	mov    0x8(%esp),%edx
  801324:	89 f9                	mov    %edi,%ecx
  801326:	d3 e2                	shl    %cl,%edx
  801328:	39 c2                	cmp    %eax,%edx
  80132a:	73 e9                	jae    801315 <__udivdi3+0xe5>
  80132c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80132f:	31 ff                	xor    %edi,%edi
  801331:	e9 40 ff ff ff       	jmp    801276 <__udivdi3+0x46>
  801336:	66 90                	xchg   %ax,%ax
  801338:	31 c0                	xor    %eax,%eax
  80133a:	e9 37 ff ff ff       	jmp    801276 <__udivdi3+0x46>
  80133f:	90                   	nop

00801340 <__umoddi3>:
  801340:	55                   	push   %ebp
  801341:	57                   	push   %edi
  801342:	56                   	push   %esi
  801343:	53                   	push   %ebx
  801344:	83 ec 1c             	sub    $0x1c,%esp
  801347:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  80134b:	8b 74 24 34          	mov    0x34(%esp),%esi
  80134f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801353:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801357:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80135b:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80135f:	89 f3                	mov    %esi,%ebx
  801361:	89 fa                	mov    %edi,%edx
  801363:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801367:	89 34 24             	mov    %esi,(%esp)
  80136a:	85 c0                	test   %eax,%eax
  80136c:	75 1a                	jne    801388 <__umoddi3+0x48>
  80136e:	39 f7                	cmp    %esi,%edi
  801370:	0f 86 a2 00 00 00    	jbe    801418 <__umoddi3+0xd8>
  801376:	89 c8                	mov    %ecx,%eax
  801378:	89 f2                	mov    %esi,%edx
  80137a:	f7 f7                	div    %edi
  80137c:	89 d0                	mov    %edx,%eax
  80137e:	31 d2                	xor    %edx,%edx
  801380:	83 c4 1c             	add    $0x1c,%esp
  801383:	5b                   	pop    %ebx
  801384:	5e                   	pop    %esi
  801385:	5f                   	pop    %edi
  801386:	5d                   	pop    %ebp
  801387:	c3                   	ret    
  801388:	39 f0                	cmp    %esi,%eax
  80138a:	0f 87 ac 00 00 00    	ja     80143c <__umoddi3+0xfc>
  801390:	0f bd e8             	bsr    %eax,%ebp
  801393:	83 f5 1f             	xor    $0x1f,%ebp
  801396:	0f 84 ac 00 00 00    	je     801448 <__umoddi3+0x108>
  80139c:	bf 20 00 00 00       	mov    $0x20,%edi
  8013a1:	29 ef                	sub    %ebp,%edi
  8013a3:	89 fe                	mov    %edi,%esi
  8013a5:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8013a9:	89 e9                	mov    %ebp,%ecx
  8013ab:	d3 e0                	shl    %cl,%eax
  8013ad:	89 d7                	mov    %edx,%edi
  8013af:	89 f1                	mov    %esi,%ecx
  8013b1:	d3 ef                	shr    %cl,%edi
  8013b3:	09 c7                	or     %eax,%edi
  8013b5:	89 e9                	mov    %ebp,%ecx
  8013b7:	d3 e2                	shl    %cl,%edx
  8013b9:	89 14 24             	mov    %edx,(%esp)
  8013bc:	89 d8                	mov    %ebx,%eax
  8013be:	d3 e0                	shl    %cl,%eax
  8013c0:	89 c2                	mov    %eax,%edx
  8013c2:	8b 44 24 08          	mov    0x8(%esp),%eax
  8013c6:	d3 e0                	shl    %cl,%eax
  8013c8:	89 44 24 04          	mov    %eax,0x4(%esp)
  8013cc:	8b 44 24 08          	mov    0x8(%esp),%eax
  8013d0:	89 f1                	mov    %esi,%ecx
  8013d2:	d3 e8                	shr    %cl,%eax
  8013d4:	09 d0                	or     %edx,%eax
  8013d6:	d3 eb                	shr    %cl,%ebx
  8013d8:	89 da                	mov    %ebx,%edx
  8013da:	f7 f7                	div    %edi
  8013dc:	89 d3                	mov    %edx,%ebx
  8013de:	f7 24 24             	mull   (%esp)
  8013e1:	89 c6                	mov    %eax,%esi
  8013e3:	89 d1                	mov    %edx,%ecx
  8013e5:	39 d3                	cmp    %edx,%ebx
  8013e7:	0f 82 87 00 00 00    	jb     801474 <__umoddi3+0x134>
  8013ed:	0f 84 91 00 00 00    	je     801484 <__umoddi3+0x144>
  8013f3:	8b 54 24 04          	mov    0x4(%esp),%edx
  8013f7:	29 f2                	sub    %esi,%edx
  8013f9:	19 cb                	sbb    %ecx,%ebx
  8013fb:	89 d8                	mov    %ebx,%eax
  8013fd:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801401:	d3 e0                	shl    %cl,%eax
  801403:	89 e9                	mov    %ebp,%ecx
  801405:	d3 ea                	shr    %cl,%edx
  801407:	09 d0                	or     %edx,%eax
  801409:	89 e9                	mov    %ebp,%ecx
  80140b:	d3 eb                	shr    %cl,%ebx
  80140d:	89 da                	mov    %ebx,%edx
  80140f:	83 c4 1c             	add    $0x1c,%esp
  801412:	5b                   	pop    %ebx
  801413:	5e                   	pop    %esi
  801414:	5f                   	pop    %edi
  801415:	5d                   	pop    %ebp
  801416:	c3                   	ret    
  801417:	90                   	nop
  801418:	89 fd                	mov    %edi,%ebp
  80141a:	85 ff                	test   %edi,%edi
  80141c:	75 0b                	jne    801429 <__umoddi3+0xe9>
  80141e:	b8 01 00 00 00       	mov    $0x1,%eax
  801423:	31 d2                	xor    %edx,%edx
  801425:	f7 f7                	div    %edi
  801427:	89 c5                	mov    %eax,%ebp
  801429:	89 f0                	mov    %esi,%eax
  80142b:	31 d2                	xor    %edx,%edx
  80142d:	f7 f5                	div    %ebp
  80142f:	89 c8                	mov    %ecx,%eax
  801431:	f7 f5                	div    %ebp
  801433:	89 d0                	mov    %edx,%eax
  801435:	e9 44 ff ff ff       	jmp    80137e <__umoddi3+0x3e>
  80143a:	66 90                	xchg   %ax,%ax
  80143c:	89 c8                	mov    %ecx,%eax
  80143e:	89 f2                	mov    %esi,%edx
  801440:	83 c4 1c             	add    $0x1c,%esp
  801443:	5b                   	pop    %ebx
  801444:	5e                   	pop    %esi
  801445:	5f                   	pop    %edi
  801446:	5d                   	pop    %ebp
  801447:	c3                   	ret    
  801448:	3b 04 24             	cmp    (%esp),%eax
  80144b:	72 06                	jb     801453 <__umoddi3+0x113>
  80144d:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801451:	77 0f                	ja     801462 <__umoddi3+0x122>
  801453:	89 f2                	mov    %esi,%edx
  801455:	29 f9                	sub    %edi,%ecx
  801457:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  80145b:	89 14 24             	mov    %edx,(%esp)
  80145e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801462:	8b 44 24 04          	mov    0x4(%esp),%eax
  801466:	8b 14 24             	mov    (%esp),%edx
  801469:	83 c4 1c             	add    $0x1c,%esp
  80146c:	5b                   	pop    %ebx
  80146d:	5e                   	pop    %esi
  80146e:	5f                   	pop    %edi
  80146f:	5d                   	pop    %ebp
  801470:	c3                   	ret    
  801471:	8d 76 00             	lea    0x0(%esi),%esi
  801474:	2b 04 24             	sub    (%esp),%eax
  801477:	19 fa                	sbb    %edi,%edx
  801479:	89 d1                	mov    %edx,%ecx
  80147b:	89 c6                	mov    %eax,%esi
  80147d:	e9 71 ff ff ff       	jmp    8013f3 <__umoddi3+0xb3>
  801482:	66 90                	xchg   %ax,%ax
  801484:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801488:	72 ea                	jb     801474 <__umoddi3+0x134>
  80148a:	89 d9                	mov    %ebx,%ecx
  80148c:	e9 62 ff ff ff       	jmp    8013f3 <__umoddi3+0xb3>
