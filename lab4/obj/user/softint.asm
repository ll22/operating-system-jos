
obj/user/softint:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 09 00 00 00       	call   80003a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $14");	// page fault
  800036:	cd 0e                	int    $0xe
}
  800038:	5d                   	pop    %ebp
  800039:	c3                   	ret    

0080003a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003a:	55                   	push   %ebp
  80003b:	89 e5                	mov    %esp,%ebp
  80003d:	56                   	push   %esi
  80003e:	53                   	push   %ebx
  80003f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800042:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800045:	e8 cf 00 00 00       	call   800119 <sys_getenvid>
  80004a:	25 ff 03 00 00       	and    $0x3ff,%eax
  80004f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800056:	c1 e0 07             	shl    $0x7,%eax
  800059:	29 d0                	sub    %edx,%eax
  80005b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800060:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800065:	85 db                	test   %ebx,%ebx
  800067:	7e 07                	jle    800070 <libmain+0x36>
		binaryname = argv[0];
  800069:	8b 06                	mov    (%esi),%eax
  80006b:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800070:	83 ec 08             	sub    $0x8,%esp
  800073:	56                   	push   %esi
  800074:	53                   	push   %ebx
  800075:	e8 b9 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80007a:	e8 0a 00 00 00       	call   800089 <exit>
}
  80007f:	83 c4 10             	add    $0x10,%esp
  800082:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800085:	5b                   	pop    %ebx
  800086:	5e                   	pop    %esi
  800087:	5d                   	pop    %ebp
  800088:	c3                   	ret    

00800089 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800089:	55                   	push   %ebp
  80008a:	89 e5                	mov    %esp,%ebp
  80008c:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80008f:	6a 00                	push   $0x0
  800091:	e8 42 00 00 00       	call   8000d8 <sys_env_destroy>
}
  800096:	83 c4 10             	add    $0x10,%esp
  800099:	c9                   	leave  
  80009a:	c3                   	ret    

0080009b <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009b:	55                   	push   %ebp
  80009c:	89 e5                	mov    %esp,%ebp
  80009e:	57                   	push   %edi
  80009f:	56                   	push   %esi
  8000a0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a1:	b8 00 00 00 00       	mov    $0x0,%eax
  8000a6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000a9:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ac:	89 c3                	mov    %eax,%ebx
  8000ae:	89 c7                	mov    %eax,%edi
  8000b0:	89 c6                	mov    %eax,%esi
  8000b2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b4:	5b                   	pop    %ebx
  8000b5:	5e                   	pop    %esi
  8000b6:	5f                   	pop    %edi
  8000b7:	5d                   	pop    %ebp
  8000b8:	c3                   	ret    

008000b9 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000b9:	55                   	push   %ebp
  8000ba:	89 e5                	mov    %esp,%ebp
  8000bc:	57                   	push   %edi
  8000bd:	56                   	push   %esi
  8000be:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000bf:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c4:	b8 01 00 00 00       	mov    $0x1,%eax
  8000c9:	89 d1                	mov    %edx,%ecx
  8000cb:	89 d3                	mov    %edx,%ebx
  8000cd:	89 d7                	mov    %edx,%edi
  8000cf:	89 d6                	mov    %edx,%esi
  8000d1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d3:	5b                   	pop    %ebx
  8000d4:	5e                   	pop    %esi
  8000d5:	5f                   	pop    %edi
  8000d6:	5d                   	pop    %ebp
  8000d7:	c3                   	ret    

008000d8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000d8:	55                   	push   %ebp
  8000d9:	89 e5                	mov    %esp,%ebp
  8000db:	57                   	push   %edi
  8000dc:	56                   	push   %esi
  8000dd:	53                   	push   %ebx
  8000de:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e1:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000e6:	b8 03 00 00 00       	mov    $0x3,%eax
  8000eb:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ee:	89 cb                	mov    %ecx,%ebx
  8000f0:	89 cf                	mov    %ecx,%edi
  8000f2:	89 ce                	mov    %ecx,%esi
  8000f4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000f6:	85 c0                	test   %eax,%eax
  8000f8:	7e 17                	jle    800111 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000fa:	83 ec 0c             	sub    $0xc,%esp
  8000fd:	50                   	push   %eax
  8000fe:	6a 03                	push   $0x3
  800100:	68 0a 0f 80 00       	push   $0x800f0a
  800105:	6a 23                	push   $0x23
  800107:	68 27 0f 80 00       	push   $0x800f27
  80010c:	e8 14 02 00 00       	call   800325 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800111:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800114:	5b                   	pop    %ebx
  800115:	5e                   	pop    %esi
  800116:	5f                   	pop    %edi
  800117:	5d                   	pop    %ebp
  800118:	c3                   	ret    

00800119 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800119:	55                   	push   %ebp
  80011a:	89 e5                	mov    %esp,%ebp
  80011c:	57                   	push   %edi
  80011d:	56                   	push   %esi
  80011e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80011f:	ba 00 00 00 00       	mov    $0x0,%edx
  800124:	b8 02 00 00 00       	mov    $0x2,%eax
  800129:	89 d1                	mov    %edx,%ecx
  80012b:	89 d3                	mov    %edx,%ebx
  80012d:	89 d7                	mov    %edx,%edi
  80012f:	89 d6                	mov    %edx,%esi
  800131:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800133:	5b                   	pop    %ebx
  800134:	5e                   	pop    %esi
  800135:	5f                   	pop    %edi
  800136:	5d                   	pop    %ebp
  800137:	c3                   	ret    

00800138 <sys_yield>:

void
sys_yield(void)
{
  800138:	55                   	push   %ebp
  800139:	89 e5                	mov    %esp,%ebp
  80013b:	57                   	push   %edi
  80013c:	56                   	push   %esi
  80013d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80013e:	ba 00 00 00 00       	mov    $0x0,%edx
  800143:	b8 0a 00 00 00       	mov    $0xa,%eax
  800148:	89 d1                	mov    %edx,%ecx
  80014a:	89 d3                	mov    %edx,%ebx
  80014c:	89 d7                	mov    %edx,%edi
  80014e:	89 d6                	mov    %edx,%esi
  800150:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800152:	5b                   	pop    %ebx
  800153:	5e                   	pop    %esi
  800154:	5f                   	pop    %edi
  800155:	5d                   	pop    %ebp
  800156:	c3                   	ret    

00800157 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800157:	55                   	push   %ebp
  800158:	89 e5                	mov    %esp,%ebp
  80015a:	57                   	push   %edi
  80015b:	56                   	push   %esi
  80015c:	53                   	push   %ebx
  80015d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800160:	be 00 00 00 00       	mov    $0x0,%esi
  800165:	b8 04 00 00 00       	mov    $0x4,%eax
  80016a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80016d:	8b 55 08             	mov    0x8(%ebp),%edx
  800170:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800173:	89 f7                	mov    %esi,%edi
  800175:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800177:	85 c0                	test   %eax,%eax
  800179:	7e 17                	jle    800192 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80017b:	83 ec 0c             	sub    $0xc,%esp
  80017e:	50                   	push   %eax
  80017f:	6a 04                	push   $0x4
  800181:	68 0a 0f 80 00       	push   $0x800f0a
  800186:	6a 23                	push   $0x23
  800188:	68 27 0f 80 00       	push   $0x800f27
  80018d:	e8 93 01 00 00       	call   800325 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800192:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800195:	5b                   	pop    %ebx
  800196:	5e                   	pop    %esi
  800197:	5f                   	pop    %edi
  800198:	5d                   	pop    %ebp
  800199:	c3                   	ret    

0080019a <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  80019a:	55                   	push   %ebp
  80019b:	89 e5                	mov    %esp,%ebp
  80019d:	57                   	push   %edi
  80019e:	56                   	push   %esi
  80019f:	53                   	push   %ebx
  8001a0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001a3:	b8 05 00 00 00       	mov    $0x5,%eax
  8001a8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001ab:	8b 55 08             	mov    0x8(%ebp),%edx
  8001ae:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b1:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001b4:	8b 75 18             	mov    0x18(%ebp),%esi
  8001b7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001b9:	85 c0                	test   %eax,%eax
  8001bb:	7e 17                	jle    8001d4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001bd:	83 ec 0c             	sub    $0xc,%esp
  8001c0:	50                   	push   %eax
  8001c1:	6a 05                	push   $0x5
  8001c3:	68 0a 0f 80 00       	push   $0x800f0a
  8001c8:	6a 23                	push   $0x23
  8001ca:	68 27 0f 80 00       	push   $0x800f27
  8001cf:	e8 51 01 00 00       	call   800325 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001d4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001d7:	5b                   	pop    %ebx
  8001d8:	5e                   	pop    %esi
  8001d9:	5f                   	pop    %edi
  8001da:	5d                   	pop    %ebp
  8001db:	c3                   	ret    

008001dc <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001dc:	55                   	push   %ebp
  8001dd:	89 e5                	mov    %esp,%ebp
  8001df:	57                   	push   %edi
  8001e0:	56                   	push   %esi
  8001e1:	53                   	push   %ebx
  8001e2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001e5:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001ea:	b8 06 00 00 00       	mov    $0x6,%eax
  8001ef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001f2:	8b 55 08             	mov    0x8(%ebp),%edx
  8001f5:	89 df                	mov    %ebx,%edi
  8001f7:	89 de                	mov    %ebx,%esi
  8001f9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001fb:	85 c0                	test   %eax,%eax
  8001fd:	7e 17                	jle    800216 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001ff:	83 ec 0c             	sub    $0xc,%esp
  800202:	50                   	push   %eax
  800203:	6a 06                	push   $0x6
  800205:	68 0a 0f 80 00       	push   $0x800f0a
  80020a:	6a 23                	push   $0x23
  80020c:	68 27 0f 80 00       	push   $0x800f27
  800211:	e8 0f 01 00 00       	call   800325 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800216:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800219:	5b                   	pop    %ebx
  80021a:	5e                   	pop    %esi
  80021b:	5f                   	pop    %edi
  80021c:	5d                   	pop    %ebp
  80021d:	c3                   	ret    

0080021e <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80021e:	55                   	push   %ebp
  80021f:	89 e5                	mov    %esp,%ebp
  800221:	57                   	push   %edi
  800222:	56                   	push   %esi
  800223:	53                   	push   %ebx
  800224:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800227:	bb 00 00 00 00       	mov    $0x0,%ebx
  80022c:	b8 08 00 00 00       	mov    $0x8,%eax
  800231:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800234:	8b 55 08             	mov    0x8(%ebp),%edx
  800237:	89 df                	mov    %ebx,%edi
  800239:	89 de                	mov    %ebx,%esi
  80023b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80023d:	85 c0                	test   %eax,%eax
  80023f:	7e 17                	jle    800258 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800241:	83 ec 0c             	sub    $0xc,%esp
  800244:	50                   	push   %eax
  800245:	6a 08                	push   $0x8
  800247:	68 0a 0f 80 00       	push   $0x800f0a
  80024c:	6a 23                	push   $0x23
  80024e:	68 27 0f 80 00       	push   $0x800f27
  800253:	e8 cd 00 00 00       	call   800325 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800258:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80025b:	5b                   	pop    %ebx
  80025c:	5e                   	pop    %esi
  80025d:	5f                   	pop    %edi
  80025e:	5d                   	pop    %ebp
  80025f:	c3                   	ret    

00800260 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800260:	55                   	push   %ebp
  800261:	89 e5                	mov    %esp,%ebp
  800263:	57                   	push   %edi
  800264:	56                   	push   %esi
  800265:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800266:	ba 00 00 00 00       	mov    $0x0,%edx
  80026b:	b8 0b 00 00 00       	mov    $0xb,%eax
  800270:	89 d1                	mov    %edx,%ecx
  800272:	89 d3                	mov    %edx,%ebx
  800274:	89 d7                	mov    %edx,%edi
  800276:	89 d6                	mov    %edx,%esi
  800278:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80027a:	5b                   	pop    %ebx
  80027b:	5e                   	pop    %esi
  80027c:	5f                   	pop    %edi
  80027d:	5d                   	pop    %ebp
  80027e:	c3                   	ret    

0080027f <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80027f:	55                   	push   %ebp
  800280:	89 e5                	mov    %esp,%ebp
  800282:	57                   	push   %edi
  800283:	56                   	push   %esi
  800284:	53                   	push   %ebx
  800285:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800288:	bb 00 00 00 00       	mov    $0x0,%ebx
  80028d:	b8 09 00 00 00       	mov    $0x9,%eax
  800292:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800295:	8b 55 08             	mov    0x8(%ebp),%edx
  800298:	89 df                	mov    %ebx,%edi
  80029a:	89 de                	mov    %ebx,%esi
  80029c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80029e:	85 c0                	test   %eax,%eax
  8002a0:	7e 17                	jle    8002b9 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002a2:	83 ec 0c             	sub    $0xc,%esp
  8002a5:	50                   	push   %eax
  8002a6:	6a 09                	push   $0x9
  8002a8:	68 0a 0f 80 00       	push   $0x800f0a
  8002ad:	6a 23                	push   $0x23
  8002af:	68 27 0f 80 00       	push   $0x800f27
  8002b4:	e8 6c 00 00 00       	call   800325 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002b9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002bc:	5b                   	pop    %ebx
  8002bd:	5e                   	pop    %esi
  8002be:	5f                   	pop    %edi
  8002bf:	5d                   	pop    %ebp
  8002c0:	c3                   	ret    

008002c1 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002c1:	55                   	push   %ebp
  8002c2:	89 e5                	mov    %esp,%ebp
  8002c4:	57                   	push   %edi
  8002c5:	56                   	push   %esi
  8002c6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002c7:	be 00 00 00 00       	mov    $0x0,%esi
  8002cc:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002d1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002d4:	8b 55 08             	mov    0x8(%ebp),%edx
  8002d7:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002da:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002dd:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002df:	5b                   	pop    %ebx
  8002e0:	5e                   	pop    %esi
  8002e1:	5f                   	pop    %edi
  8002e2:	5d                   	pop    %ebp
  8002e3:	c3                   	ret    

008002e4 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002e4:	55                   	push   %ebp
  8002e5:	89 e5                	mov    %esp,%ebp
  8002e7:	57                   	push   %edi
  8002e8:	56                   	push   %esi
  8002e9:	53                   	push   %ebx
  8002ea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002ed:	b9 00 00 00 00       	mov    $0x0,%ecx
  8002f2:	b8 0d 00 00 00       	mov    $0xd,%eax
  8002f7:	8b 55 08             	mov    0x8(%ebp),%edx
  8002fa:	89 cb                	mov    %ecx,%ebx
  8002fc:	89 cf                	mov    %ecx,%edi
  8002fe:	89 ce                	mov    %ecx,%esi
  800300:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800302:	85 c0                	test   %eax,%eax
  800304:	7e 17                	jle    80031d <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800306:	83 ec 0c             	sub    $0xc,%esp
  800309:	50                   	push   %eax
  80030a:	6a 0d                	push   $0xd
  80030c:	68 0a 0f 80 00       	push   $0x800f0a
  800311:	6a 23                	push   $0x23
  800313:	68 27 0f 80 00       	push   $0x800f27
  800318:	e8 08 00 00 00       	call   800325 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80031d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800320:	5b                   	pop    %ebx
  800321:	5e                   	pop    %esi
  800322:	5f                   	pop    %edi
  800323:	5d                   	pop    %ebp
  800324:	c3                   	ret    

00800325 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800325:	55                   	push   %ebp
  800326:	89 e5                	mov    %esp,%ebp
  800328:	56                   	push   %esi
  800329:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80032a:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80032d:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800333:	e8 e1 fd ff ff       	call   800119 <sys_getenvid>
  800338:	83 ec 0c             	sub    $0xc,%esp
  80033b:	ff 75 0c             	pushl  0xc(%ebp)
  80033e:	ff 75 08             	pushl  0x8(%ebp)
  800341:	56                   	push   %esi
  800342:	50                   	push   %eax
  800343:	68 38 0f 80 00       	push   $0x800f38
  800348:	e8 b0 00 00 00       	call   8003fd <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80034d:	83 c4 18             	add    $0x18,%esp
  800350:	53                   	push   %ebx
  800351:	ff 75 10             	pushl  0x10(%ebp)
  800354:	e8 53 00 00 00       	call   8003ac <vcprintf>
	cprintf("\n");
  800359:	c7 04 24 5c 0f 80 00 	movl   $0x800f5c,(%esp)
  800360:	e8 98 00 00 00       	call   8003fd <cprintf>
  800365:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800368:	cc                   	int3   
  800369:	eb fd                	jmp    800368 <_panic+0x43>

0080036b <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80036b:	55                   	push   %ebp
  80036c:	89 e5                	mov    %esp,%ebp
  80036e:	53                   	push   %ebx
  80036f:	83 ec 04             	sub    $0x4,%esp
  800372:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800375:	8b 13                	mov    (%ebx),%edx
  800377:	8d 42 01             	lea    0x1(%edx),%eax
  80037a:	89 03                	mov    %eax,(%ebx)
  80037c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80037f:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800383:	3d ff 00 00 00       	cmp    $0xff,%eax
  800388:	75 1a                	jne    8003a4 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80038a:	83 ec 08             	sub    $0x8,%esp
  80038d:	68 ff 00 00 00       	push   $0xff
  800392:	8d 43 08             	lea    0x8(%ebx),%eax
  800395:	50                   	push   %eax
  800396:	e8 00 fd ff ff       	call   80009b <sys_cputs>
		b->idx = 0;
  80039b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003a1:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003a4:	ff 43 04             	incl   0x4(%ebx)
}
  8003a7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003aa:	c9                   	leave  
  8003ab:	c3                   	ret    

008003ac <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003ac:	55                   	push   %ebp
  8003ad:	89 e5                	mov    %esp,%ebp
  8003af:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003b5:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003bc:	00 00 00 
	b.cnt = 0;
  8003bf:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003c6:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003c9:	ff 75 0c             	pushl  0xc(%ebp)
  8003cc:	ff 75 08             	pushl  0x8(%ebp)
  8003cf:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003d5:	50                   	push   %eax
  8003d6:	68 6b 03 80 00       	push   $0x80036b
  8003db:	e8 51 01 00 00       	call   800531 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003e0:	83 c4 08             	add    $0x8,%esp
  8003e3:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003e9:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003ef:	50                   	push   %eax
  8003f0:	e8 a6 fc ff ff       	call   80009b <sys_cputs>

	return b.cnt;
}
  8003f5:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8003fb:	c9                   	leave  
  8003fc:	c3                   	ret    

008003fd <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8003fd:	55                   	push   %ebp
  8003fe:	89 e5                	mov    %esp,%ebp
  800400:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800403:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800406:	50                   	push   %eax
  800407:	ff 75 08             	pushl  0x8(%ebp)
  80040a:	e8 9d ff ff ff       	call   8003ac <vcprintf>
	va_end(ap);

	return cnt;
}
  80040f:	c9                   	leave  
  800410:	c3                   	ret    

00800411 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800411:	55                   	push   %ebp
  800412:	89 e5                	mov    %esp,%ebp
  800414:	57                   	push   %edi
  800415:	56                   	push   %esi
  800416:	53                   	push   %ebx
  800417:	83 ec 1c             	sub    $0x1c,%esp
  80041a:	89 c7                	mov    %eax,%edi
  80041c:	89 d6                	mov    %edx,%esi
  80041e:	8b 45 08             	mov    0x8(%ebp),%eax
  800421:	8b 55 0c             	mov    0xc(%ebp),%edx
  800424:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800427:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80042a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80042d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800432:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800435:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800438:	39 d3                	cmp    %edx,%ebx
  80043a:	72 05                	jb     800441 <printnum+0x30>
  80043c:	39 45 10             	cmp    %eax,0x10(%ebp)
  80043f:	77 45                	ja     800486 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800441:	83 ec 0c             	sub    $0xc,%esp
  800444:	ff 75 18             	pushl  0x18(%ebp)
  800447:	8b 45 14             	mov    0x14(%ebp),%eax
  80044a:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80044d:	53                   	push   %ebx
  80044e:	ff 75 10             	pushl  0x10(%ebp)
  800451:	83 ec 08             	sub    $0x8,%esp
  800454:	ff 75 e4             	pushl  -0x1c(%ebp)
  800457:	ff 75 e0             	pushl  -0x20(%ebp)
  80045a:	ff 75 dc             	pushl  -0x24(%ebp)
  80045d:	ff 75 d8             	pushl  -0x28(%ebp)
  800460:	e8 27 08 00 00       	call   800c8c <__udivdi3>
  800465:	83 c4 18             	add    $0x18,%esp
  800468:	52                   	push   %edx
  800469:	50                   	push   %eax
  80046a:	89 f2                	mov    %esi,%edx
  80046c:	89 f8                	mov    %edi,%eax
  80046e:	e8 9e ff ff ff       	call   800411 <printnum>
  800473:	83 c4 20             	add    $0x20,%esp
  800476:	eb 16                	jmp    80048e <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800478:	83 ec 08             	sub    $0x8,%esp
  80047b:	56                   	push   %esi
  80047c:	ff 75 18             	pushl  0x18(%ebp)
  80047f:	ff d7                	call   *%edi
  800481:	83 c4 10             	add    $0x10,%esp
  800484:	eb 03                	jmp    800489 <printnum+0x78>
  800486:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800489:	4b                   	dec    %ebx
  80048a:	85 db                	test   %ebx,%ebx
  80048c:	7f ea                	jg     800478 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80048e:	83 ec 08             	sub    $0x8,%esp
  800491:	56                   	push   %esi
  800492:	83 ec 04             	sub    $0x4,%esp
  800495:	ff 75 e4             	pushl  -0x1c(%ebp)
  800498:	ff 75 e0             	pushl  -0x20(%ebp)
  80049b:	ff 75 dc             	pushl  -0x24(%ebp)
  80049e:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a1:	e8 f6 08 00 00       	call   800d9c <__umoddi3>
  8004a6:	83 c4 14             	add    $0x14,%esp
  8004a9:	0f be 80 5e 0f 80 00 	movsbl 0x800f5e(%eax),%eax
  8004b0:	50                   	push   %eax
  8004b1:	ff d7                	call   *%edi
}
  8004b3:	83 c4 10             	add    $0x10,%esp
  8004b6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004b9:	5b                   	pop    %ebx
  8004ba:	5e                   	pop    %esi
  8004bb:	5f                   	pop    %edi
  8004bc:	5d                   	pop    %ebp
  8004bd:	c3                   	ret    

008004be <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004be:	55                   	push   %ebp
  8004bf:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004c1:	83 fa 01             	cmp    $0x1,%edx
  8004c4:	7e 0e                	jle    8004d4 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004c6:	8b 10                	mov    (%eax),%edx
  8004c8:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004cb:	89 08                	mov    %ecx,(%eax)
  8004cd:	8b 02                	mov    (%edx),%eax
  8004cf:	8b 52 04             	mov    0x4(%edx),%edx
  8004d2:	eb 22                	jmp    8004f6 <getuint+0x38>
	else if (lflag)
  8004d4:	85 d2                	test   %edx,%edx
  8004d6:	74 10                	je     8004e8 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004d8:	8b 10                	mov    (%eax),%edx
  8004da:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004dd:	89 08                	mov    %ecx,(%eax)
  8004df:	8b 02                	mov    (%edx),%eax
  8004e1:	ba 00 00 00 00       	mov    $0x0,%edx
  8004e6:	eb 0e                	jmp    8004f6 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004e8:	8b 10                	mov    (%eax),%edx
  8004ea:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004ed:	89 08                	mov    %ecx,(%eax)
  8004ef:	8b 02                	mov    (%edx),%eax
  8004f1:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8004f6:	5d                   	pop    %ebp
  8004f7:	c3                   	ret    

008004f8 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8004f8:	55                   	push   %ebp
  8004f9:	89 e5                	mov    %esp,%ebp
  8004fb:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8004fe:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800501:	8b 10                	mov    (%eax),%edx
  800503:	3b 50 04             	cmp    0x4(%eax),%edx
  800506:	73 0a                	jae    800512 <sprintputch+0x1a>
		*b->buf++ = ch;
  800508:	8d 4a 01             	lea    0x1(%edx),%ecx
  80050b:	89 08                	mov    %ecx,(%eax)
  80050d:	8b 45 08             	mov    0x8(%ebp),%eax
  800510:	88 02                	mov    %al,(%edx)
}
  800512:	5d                   	pop    %ebp
  800513:	c3                   	ret    

00800514 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800514:	55                   	push   %ebp
  800515:	89 e5                	mov    %esp,%ebp
  800517:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80051a:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80051d:	50                   	push   %eax
  80051e:	ff 75 10             	pushl  0x10(%ebp)
  800521:	ff 75 0c             	pushl  0xc(%ebp)
  800524:	ff 75 08             	pushl  0x8(%ebp)
  800527:	e8 05 00 00 00       	call   800531 <vprintfmt>
	va_end(ap);
}
  80052c:	83 c4 10             	add    $0x10,%esp
  80052f:	c9                   	leave  
  800530:	c3                   	ret    

00800531 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800531:	55                   	push   %ebp
  800532:	89 e5                	mov    %esp,%ebp
  800534:	57                   	push   %edi
  800535:	56                   	push   %esi
  800536:	53                   	push   %ebx
  800537:	83 ec 2c             	sub    $0x2c,%esp
  80053a:	8b 75 08             	mov    0x8(%ebp),%esi
  80053d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800540:	8b 7d 10             	mov    0x10(%ebp),%edi
  800543:	eb 12                	jmp    800557 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800545:	85 c0                	test   %eax,%eax
  800547:	0f 84 68 03 00 00    	je     8008b5 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80054d:	83 ec 08             	sub    $0x8,%esp
  800550:	53                   	push   %ebx
  800551:	50                   	push   %eax
  800552:	ff d6                	call   *%esi
  800554:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800557:	47                   	inc    %edi
  800558:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80055c:	83 f8 25             	cmp    $0x25,%eax
  80055f:	75 e4                	jne    800545 <vprintfmt+0x14>
  800561:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800565:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80056c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800573:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80057a:	ba 00 00 00 00       	mov    $0x0,%edx
  80057f:	eb 07                	jmp    800588 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800581:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800584:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800588:	8d 47 01             	lea    0x1(%edi),%eax
  80058b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80058e:	0f b6 0f             	movzbl (%edi),%ecx
  800591:	8a 07                	mov    (%edi),%al
  800593:	83 e8 23             	sub    $0x23,%eax
  800596:	3c 55                	cmp    $0x55,%al
  800598:	0f 87 fe 02 00 00    	ja     80089c <vprintfmt+0x36b>
  80059e:	0f b6 c0             	movzbl %al,%eax
  8005a1:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8005a8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005ab:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005af:	eb d7                	jmp    800588 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005b4:	b8 00 00 00 00       	mov    $0x0,%eax
  8005b9:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005bc:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005bf:	01 c0                	add    %eax,%eax
  8005c1:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005c5:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005c8:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005cb:	83 fa 09             	cmp    $0x9,%edx
  8005ce:	77 34                	ja     800604 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005d0:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005d1:	eb e9                	jmp    8005bc <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005d3:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d6:	8d 48 04             	lea    0x4(%eax),%ecx
  8005d9:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005dc:	8b 00                	mov    (%eax),%eax
  8005de:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005e4:	eb 24                	jmp    80060a <vprintfmt+0xd9>
  8005e6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005ea:	79 07                	jns    8005f3 <vprintfmt+0xc2>
  8005ec:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f6:	eb 90                	jmp    800588 <vprintfmt+0x57>
  8005f8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8005fb:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800602:	eb 84                	jmp    800588 <vprintfmt+0x57>
  800604:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800607:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80060a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80060e:	0f 89 74 ff ff ff    	jns    800588 <vprintfmt+0x57>
				width = precision, precision = -1;
  800614:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800617:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80061a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800621:	e9 62 ff ff ff       	jmp    800588 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800626:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800627:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80062a:	e9 59 ff ff ff       	jmp    800588 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80062f:	8b 45 14             	mov    0x14(%ebp),%eax
  800632:	8d 50 04             	lea    0x4(%eax),%edx
  800635:	89 55 14             	mov    %edx,0x14(%ebp)
  800638:	83 ec 08             	sub    $0x8,%esp
  80063b:	53                   	push   %ebx
  80063c:	ff 30                	pushl  (%eax)
  80063e:	ff d6                	call   *%esi
			break;
  800640:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800643:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800646:	e9 0c ff ff ff       	jmp    800557 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80064b:	8b 45 14             	mov    0x14(%ebp),%eax
  80064e:	8d 50 04             	lea    0x4(%eax),%edx
  800651:	89 55 14             	mov    %edx,0x14(%ebp)
  800654:	8b 00                	mov    (%eax),%eax
  800656:	85 c0                	test   %eax,%eax
  800658:	79 02                	jns    80065c <vprintfmt+0x12b>
  80065a:	f7 d8                	neg    %eax
  80065c:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80065e:	83 f8 08             	cmp    $0x8,%eax
  800661:	7f 0b                	jg     80066e <vprintfmt+0x13d>
  800663:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  80066a:	85 c0                	test   %eax,%eax
  80066c:	75 18                	jne    800686 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80066e:	52                   	push   %edx
  80066f:	68 76 0f 80 00       	push   $0x800f76
  800674:	53                   	push   %ebx
  800675:	56                   	push   %esi
  800676:	e8 99 fe ff ff       	call   800514 <printfmt>
  80067b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80067e:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800681:	e9 d1 fe ff ff       	jmp    800557 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800686:	50                   	push   %eax
  800687:	68 7f 0f 80 00       	push   $0x800f7f
  80068c:	53                   	push   %ebx
  80068d:	56                   	push   %esi
  80068e:	e8 81 fe ff ff       	call   800514 <printfmt>
  800693:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800696:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800699:	e9 b9 fe ff ff       	jmp    800557 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80069e:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a1:	8d 50 04             	lea    0x4(%eax),%edx
  8006a4:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a7:	8b 38                	mov    (%eax),%edi
  8006a9:	85 ff                	test   %edi,%edi
  8006ab:	75 05                	jne    8006b2 <vprintfmt+0x181>
				p = "(null)";
  8006ad:	bf 6f 0f 80 00       	mov    $0x800f6f,%edi
			if (width > 0 && padc != '-')
  8006b2:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006b6:	0f 8e 90 00 00 00    	jle    80074c <vprintfmt+0x21b>
  8006bc:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006c0:	0f 84 8e 00 00 00    	je     800754 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006c6:	83 ec 08             	sub    $0x8,%esp
  8006c9:	ff 75 d0             	pushl  -0x30(%ebp)
  8006cc:	57                   	push   %edi
  8006cd:	e8 70 02 00 00       	call   800942 <strnlen>
  8006d2:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006d5:	29 c1                	sub    %eax,%ecx
  8006d7:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006da:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006dd:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006e1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006e4:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006e7:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006e9:	eb 0d                	jmp    8006f8 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006eb:	83 ec 08             	sub    $0x8,%esp
  8006ee:	53                   	push   %ebx
  8006ef:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006f2:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f4:	4f                   	dec    %edi
  8006f5:	83 c4 10             	add    $0x10,%esp
  8006f8:	85 ff                	test   %edi,%edi
  8006fa:	7f ef                	jg     8006eb <vprintfmt+0x1ba>
  8006fc:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8006ff:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800702:	89 c8                	mov    %ecx,%eax
  800704:	85 c9                	test   %ecx,%ecx
  800706:	79 05                	jns    80070d <vprintfmt+0x1dc>
  800708:	b8 00 00 00 00       	mov    $0x0,%eax
  80070d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800710:	29 c1                	sub    %eax,%ecx
  800712:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800715:	89 75 08             	mov    %esi,0x8(%ebp)
  800718:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80071b:	eb 3d                	jmp    80075a <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80071d:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800721:	74 19                	je     80073c <vprintfmt+0x20b>
  800723:	0f be c0             	movsbl %al,%eax
  800726:	83 e8 20             	sub    $0x20,%eax
  800729:	83 f8 5e             	cmp    $0x5e,%eax
  80072c:	76 0e                	jbe    80073c <vprintfmt+0x20b>
					putch('?', putdat);
  80072e:	83 ec 08             	sub    $0x8,%esp
  800731:	53                   	push   %ebx
  800732:	6a 3f                	push   $0x3f
  800734:	ff 55 08             	call   *0x8(%ebp)
  800737:	83 c4 10             	add    $0x10,%esp
  80073a:	eb 0b                	jmp    800747 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80073c:	83 ec 08             	sub    $0x8,%esp
  80073f:	53                   	push   %ebx
  800740:	52                   	push   %edx
  800741:	ff 55 08             	call   *0x8(%ebp)
  800744:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800747:	ff 4d e4             	decl   -0x1c(%ebp)
  80074a:	eb 0e                	jmp    80075a <vprintfmt+0x229>
  80074c:	89 75 08             	mov    %esi,0x8(%ebp)
  80074f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800752:	eb 06                	jmp    80075a <vprintfmt+0x229>
  800754:	89 75 08             	mov    %esi,0x8(%ebp)
  800757:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075a:	47                   	inc    %edi
  80075b:	8a 47 ff             	mov    -0x1(%edi),%al
  80075e:	0f be d0             	movsbl %al,%edx
  800761:	85 d2                	test   %edx,%edx
  800763:	74 1d                	je     800782 <vprintfmt+0x251>
  800765:	85 f6                	test   %esi,%esi
  800767:	78 b4                	js     80071d <vprintfmt+0x1ec>
  800769:	4e                   	dec    %esi
  80076a:	79 b1                	jns    80071d <vprintfmt+0x1ec>
  80076c:	8b 75 08             	mov    0x8(%ebp),%esi
  80076f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800772:	eb 14                	jmp    800788 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800774:	83 ec 08             	sub    $0x8,%esp
  800777:	53                   	push   %ebx
  800778:	6a 20                	push   $0x20
  80077a:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80077c:	4f                   	dec    %edi
  80077d:	83 c4 10             	add    $0x10,%esp
  800780:	eb 06                	jmp    800788 <vprintfmt+0x257>
  800782:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800785:	8b 75 08             	mov    0x8(%ebp),%esi
  800788:	85 ff                	test   %edi,%edi
  80078a:	7f e8                	jg     800774 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80078c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80078f:	e9 c3 fd ff ff       	jmp    800557 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800794:	83 fa 01             	cmp    $0x1,%edx
  800797:	7e 16                	jle    8007af <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800799:	8b 45 14             	mov    0x14(%ebp),%eax
  80079c:	8d 50 08             	lea    0x8(%eax),%edx
  80079f:	89 55 14             	mov    %edx,0x14(%ebp)
  8007a2:	8b 50 04             	mov    0x4(%eax),%edx
  8007a5:	8b 00                	mov    (%eax),%eax
  8007a7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007aa:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ad:	eb 32                	jmp    8007e1 <vprintfmt+0x2b0>
	else if (lflag)
  8007af:	85 d2                	test   %edx,%edx
  8007b1:	74 18                	je     8007cb <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007b3:	8b 45 14             	mov    0x14(%ebp),%eax
  8007b6:	8d 50 04             	lea    0x4(%eax),%edx
  8007b9:	89 55 14             	mov    %edx,0x14(%ebp)
  8007bc:	8b 00                	mov    (%eax),%eax
  8007be:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007c1:	89 c1                	mov    %eax,%ecx
  8007c3:	c1 f9 1f             	sar    $0x1f,%ecx
  8007c6:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007c9:	eb 16                	jmp    8007e1 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007cb:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ce:	8d 50 04             	lea    0x4(%eax),%edx
  8007d1:	89 55 14             	mov    %edx,0x14(%ebp)
  8007d4:	8b 00                	mov    (%eax),%eax
  8007d6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007d9:	89 c1                	mov    %eax,%ecx
  8007db:	c1 f9 1f             	sar    $0x1f,%ecx
  8007de:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007e1:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007e4:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007e7:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007eb:	79 76                	jns    800863 <vprintfmt+0x332>
				putch('-', putdat);
  8007ed:	83 ec 08             	sub    $0x8,%esp
  8007f0:	53                   	push   %ebx
  8007f1:	6a 2d                	push   $0x2d
  8007f3:	ff d6                	call   *%esi
				num = -(long long) num;
  8007f5:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007f8:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8007fb:	f7 d8                	neg    %eax
  8007fd:	83 d2 00             	adc    $0x0,%edx
  800800:	f7 da                	neg    %edx
  800802:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800805:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80080a:	eb 5c                	jmp    800868 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80080c:	8d 45 14             	lea    0x14(%ebp),%eax
  80080f:	e8 aa fc ff ff       	call   8004be <getuint>
			base = 10;
  800814:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800819:	eb 4d                	jmp    800868 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80081b:	8d 45 14             	lea    0x14(%ebp),%eax
  80081e:	e8 9b fc ff ff       	call   8004be <getuint>
			base = 8;
  800823:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800828:	eb 3e                	jmp    800868 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80082a:	83 ec 08             	sub    $0x8,%esp
  80082d:	53                   	push   %ebx
  80082e:	6a 30                	push   $0x30
  800830:	ff d6                	call   *%esi
			putch('x', putdat);
  800832:	83 c4 08             	add    $0x8,%esp
  800835:	53                   	push   %ebx
  800836:	6a 78                	push   $0x78
  800838:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80083a:	8b 45 14             	mov    0x14(%ebp),%eax
  80083d:	8d 50 04             	lea    0x4(%eax),%edx
  800840:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800843:	8b 00                	mov    (%eax),%eax
  800845:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80084a:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80084d:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800852:	eb 14                	jmp    800868 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800854:	8d 45 14             	lea    0x14(%ebp),%eax
  800857:	e8 62 fc ff ff       	call   8004be <getuint>
			base = 16;
  80085c:	b9 10 00 00 00       	mov    $0x10,%ecx
  800861:	eb 05                	jmp    800868 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800863:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800868:	83 ec 0c             	sub    $0xc,%esp
  80086b:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80086f:	57                   	push   %edi
  800870:	ff 75 e4             	pushl  -0x1c(%ebp)
  800873:	51                   	push   %ecx
  800874:	52                   	push   %edx
  800875:	50                   	push   %eax
  800876:	89 da                	mov    %ebx,%edx
  800878:	89 f0                	mov    %esi,%eax
  80087a:	e8 92 fb ff ff       	call   800411 <printnum>
			break;
  80087f:	83 c4 20             	add    $0x20,%esp
  800882:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800885:	e9 cd fc ff ff       	jmp    800557 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80088a:	83 ec 08             	sub    $0x8,%esp
  80088d:	53                   	push   %ebx
  80088e:	51                   	push   %ecx
  80088f:	ff d6                	call   *%esi
			break;
  800891:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800894:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800897:	e9 bb fc ff ff       	jmp    800557 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80089c:	83 ec 08             	sub    $0x8,%esp
  80089f:	53                   	push   %ebx
  8008a0:	6a 25                	push   $0x25
  8008a2:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008a4:	83 c4 10             	add    $0x10,%esp
  8008a7:	eb 01                	jmp    8008aa <vprintfmt+0x379>
  8008a9:	4f                   	dec    %edi
  8008aa:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008ae:	75 f9                	jne    8008a9 <vprintfmt+0x378>
  8008b0:	e9 a2 fc ff ff       	jmp    800557 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008b5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008b8:	5b                   	pop    %ebx
  8008b9:	5e                   	pop    %esi
  8008ba:	5f                   	pop    %edi
  8008bb:	5d                   	pop    %ebp
  8008bc:	c3                   	ret    

008008bd <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008bd:	55                   	push   %ebp
  8008be:	89 e5                	mov    %esp,%ebp
  8008c0:	83 ec 18             	sub    $0x18,%esp
  8008c3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c6:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008cc:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008d0:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008d3:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008da:	85 c0                	test   %eax,%eax
  8008dc:	74 26                	je     800904 <vsnprintf+0x47>
  8008de:	85 d2                	test   %edx,%edx
  8008e0:	7e 29                	jle    80090b <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008e2:	ff 75 14             	pushl  0x14(%ebp)
  8008e5:	ff 75 10             	pushl  0x10(%ebp)
  8008e8:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008eb:	50                   	push   %eax
  8008ec:	68 f8 04 80 00       	push   $0x8004f8
  8008f1:	e8 3b fc ff ff       	call   800531 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8008f6:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8008f9:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8008fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8008ff:	83 c4 10             	add    $0x10,%esp
  800902:	eb 0c                	jmp    800910 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800904:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800909:	eb 05                	jmp    800910 <vsnprintf+0x53>
  80090b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800910:	c9                   	leave  
  800911:	c3                   	ret    

00800912 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800912:	55                   	push   %ebp
  800913:	89 e5                	mov    %esp,%ebp
  800915:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800918:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80091b:	50                   	push   %eax
  80091c:	ff 75 10             	pushl  0x10(%ebp)
  80091f:	ff 75 0c             	pushl  0xc(%ebp)
  800922:	ff 75 08             	pushl  0x8(%ebp)
  800925:	e8 93 ff ff ff       	call   8008bd <vsnprintf>
	va_end(ap);

	return rc;
}
  80092a:	c9                   	leave  
  80092b:	c3                   	ret    

0080092c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80092c:	55                   	push   %ebp
  80092d:	89 e5                	mov    %esp,%ebp
  80092f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800932:	b8 00 00 00 00       	mov    $0x0,%eax
  800937:	eb 01                	jmp    80093a <strlen+0xe>
		n++;
  800939:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80093a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80093e:	75 f9                	jne    800939 <strlen+0xd>
		n++;
	return n;
}
  800940:	5d                   	pop    %ebp
  800941:	c3                   	ret    

00800942 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800942:	55                   	push   %ebp
  800943:	89 e5                	mov    %esp,%ebp
  800945:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800948:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80094b:	ba 00 00 00 00       	mov    $0x0,%edx
  800950:	eb 01                	jmp    800953 <strnlen+0x11>
		n++;
  800952:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800953:	39 c2                	cmp    %eax,%edx
  800955:	74 08                	je     80095f <strnlen+0x1d>
  800957:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80095b:	75 f5                	jne    800952 <strnlen+0x10>
  80095d:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80095f:	5d                   	pop    %ebp
  800960:	c3                   	ret    

00800961 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800961:	55                   	push   %ebp
  800962:	89 e5                	mov    %esp,%ebp
  800964:	53                   	push   %ebx
  800965:	8b 45 08             	mov    0x8(%ebp),%eax
  800968:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80096b:	89 c2                	mov    %eax,%edx
  80096d:	42                   	inc    %edx
  80096e:	41                   	inc    %ecx
  80096f:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800972:	88 5a ff             	mov    %bl,-0x1(%edx)
  800975:	84 db                	test   %bl,%bl
  800977:	75 f4                	jne    80096d <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800979:	5b                   	pop    %ebx
  80097a:	5d                   	pop    %ebp
  80097b:	c3                   	ret    

0080097c <strcat>:

char *
strcat(char *dst, const char *src)
{
  80097c:	55                   	push   %ebp
  80097d:	89 e5                	mov    %esp,%ebp
  80097f:	53                   	push   %ebx
  800980:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800983:	53                   	push   %ebx
  800984:	e8 a3 ff ff ff       	call   80092c <strlen>
  800989:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80098c:	ff 75 0c             	pushl  0xc(%ebp)
  80098f:	01 d8                	add    %ebx,%eax
  800991:	50                   	push   %eax
  800992:	e8 ca ff ff ff       	call   800961 <strcpy>
	return dst;
}
  800997:	89 d8                	mov    %ebx,%eax
  800999:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80099c:	c9                   	leave  
  80099d:	c3                   	ret    

0080099e <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80099e:	55                   	push   %ebp
  80099f:	89 e5                	mov    %esp,%ebp
  8009a1:	56                   	push   %esi
  8009a2:	53                   	push   %ebx
  8009a3:	8b 75 08             	mov    0x8(%ebp),%esi
  8009a6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009a9:	89 f3                	mov    %esi,%ebx
  8009ab:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ae:	89 f2                	mov    %esi,%edx
  8009b0:	eb 0c                	jmp    8009be <strncpy+0x20>
		*dst++ = *src;
  8009b2:	42                   	inc    %edx
  8009b3:	8a 01                	mov    (%ecx),%al
  8009b5:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009b8:	80 39 01             	cmpb   $0x1,(%ecx)
  8009bb:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009be:	39 da                	cmp    %ebx,%edx
  8009c0:	75 f0                	jne    8009b2 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009c2:	89 f0                	mov    %esi,%eax
  8009c4:	5b                   	pop    %ebx
  8009c5:	5e                   	pop    %esi
  8009c6:	5d                   	pop    %ebp
  8009c7:	c3                   	ret    

008009c8 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009c8:	55                   	push   %ebp
  8009c9:	89 e5                	mov    %esp,%ebp
  8009cb:	56                   	push   %esi
  8009cc:	53                   	push   %ebx
  8009cd:	8b 75 08             	mov    0x8(%ebp),%esi
  8009d0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009d3:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009d6:	85 c0                	test   %eax,%eax
  8009d8:	74 1e                	je     8009f8 <strlcpy+0x30>
  8009da:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009de:	89 f2                	mov    %esi,%edx
  8009e0:	eb 05                	jmp    8009e7 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009e2:	42                   	inc    %edx
  8009e3:	41                   	inc    %ecx
  8009e4:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009e7:	39 c2                	cmp    %eax,%edx
  8009e9:	74 08                	je     8009f3 <strlcpy+0x2b>
  8009eb:	8a 19                	mov    (%ecx),%bl
  8009ed:	84 db                	test   %bl,%bl
  8009ef:	75 f1                	jne    8009e2 <strlcpy+0x1a>
  8009f1:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8009f3:	c6 00 00             	movb   $0x0,(%eax)
  8009f6:	eb 02                	jmp    8009fa <strlcpy+0x32>
  8009f8:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8009fa:	29 f0                	sub    %esi,%eax
}
  8009fc:	5b                   	pop    %ebx
  8009fd:	5e                   	pop    %esi
  8009fe:	5d                   	pop    %ebp
  8009ff:	c3                   	ret    

00800a00 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a00:	55                   	push   %ebp
  800a01:	89 e5                	mov    %esp,%ebp
  800a03:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a06:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a09:	eb 02                	jmp    800a0d <strcmp+0xd>
		p++, q++;
  800a0b:	41                   	inc    %ecx
  800a0c:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a0d:	8a 01                	mov    (%ecx),%al
  800a0f:	84 c0                	test   %al,%al
  800a11:	74 04                	je     800a17 <strcmp+0x17>
  800a13:	3a 02                	cmp    (%edx),%al
  800a15:	74 f4                	je     800a0b <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a17:	0f b6 c0             	movzbl %al,%eax
  800a1a:	0f b6 12             	movzbl (%edx),%edx
  800a1d:	29 d0                	sub    %edx,%eax
}
  800a1f:	5d                   	pop    %ebp
  800a20:	c3                   	ret    

00800a21 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a21:	55                   	push   %ebp
  800a22:	89 e5                	mov    %esp,%ebp
  800a24:	53                   	push   %ebx
  800a25:	8b 45 08             	mov    0x8(%ebp),%eax
  800a28:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a2b:	89 c3                	mov    %eax,%ebx
  800a2d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a30:	eb 02                	jmp    800a34 <strncmp+0x13>
		n--, p++, q++;
  800a32:	40                   	inc    %eax
  800a33:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a34:	39 d8                	cmp    %ebx,%eax
  800a36:	74 14                	je     800a4c <strncmp+0x2b>
  800a38:	8a 08                	mov    (%eax),%cl
  800a3a:	84 c9                	test   %cl,%cl
  800a3c:	74 04                	je     800a42 <strncmp+0x21>
  800a3e:	3a 0a                	cmp    (%edx),%cl
  800a40:	74 f0                	je     800a32 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a42:	0f b6 00             	movzbl (%eax),%eax
  800a45:	0f b6 12             	movzbl (%edx),%edx
  800a48:	29 d0                	sub    %edx,%eax
  800a4a:	eb 05                	jmp    800a51 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a4c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a51:	5b                   	pop    %ebx
  800a52:	5d                   	pop    %ebp
  800a53:	c3                   	ret    

00800a54 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a54:	55                   	push   %ebp
  800a55:	89 e5                	mov    %esp,%ebp
  800a57:	8b 45 08             	mov    0x8(%ebp),%eax
  800a5a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a5d:	eb 05                	jmp    800a64 <strchr+0x10>
		if (*s == c)
  800a5f:	38 ca                	cmp    %cl,%dl
  800a61:	74 0c                	je     800a6f <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a63:	40                   	inc    %eax
  800a64:	8a 10                	mov    (%eax),%dl
  800a66:	84 d2                	test   %dl,%dl
  800a68:	75 f5                	jne    800a5f <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a6a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a6f:	5d                   	pop    %ebp
  800a70:	c3                   	ret    

00800a71 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a71:	55                   	push   %ebp
  800a72:	89 e5                	mov    %esp,%ebp
  800a74:	8b 45 08             	mov    0x8(%ebp),%eax
  800a77:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a7a:	eb 05                	jmp    800a81 <strfind+0x10>
		if (*s == c)
  800a7c:	38 ca                	cmp    %cl,%dl
  800a7e:	74 07                	je     800a87 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a80:	40                   	inc    %eax
  800a81:	8a 10                	mov    (%eax),%dl
  800a83:	84 d2                	test   %dl,%dl
  800a85:	75 f5                	jne    800a7c <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a87:	5d                   	pop    %ebp
  800a88:	c3                   	ret    

00800a89 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a89:	55                   	push   %ebp
  800a8a:	89 e5                	mov    %esp,%ebp
  800a8c:	57                   	push   %edi
  800a8d:	56                   	push   %esi
  800a8e:	53                   	push   %ebx
  800a8f:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a92:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a95:	85 c9                	test   %ecx,%ecx
  800a97:	74 36                	je     800acf <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800a99:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800a9f:	75 28                	jne    800ac9 <memset+0x40>
  800aa1:	f6 c1 03             	test   $0x3,%cl
  800aa4:	75 23                	jne    800ac9 <memset+0x40>
		c &= 0xFF;
  800aa6:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aaa:	89 d3                	mov    %edx,%ebx
  800aac:	c1 e3 08             	shl    $0x8,%ebx
  800aaf:	89 d6                	mov    %edx,%esi
  800ab1:	c1 e6 18             	shl    $0x18,%esi
  800ab4:	89 d0                	mov    %edx,%eax
  800ab6:	c1 e0 10             	shl    $0x10,%eax
  800ab9:	09 f0                	or     %esi,%eax
  800abb:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800abd:	89 d8                	mov    %ebx,%eax
  800abf:	09 d0                	or     %edx,%eax
  800ac1:	c1 e9 02             	shr    $0x2,%ecx
  800ac4:	fc                   	cld    
  800ac5:	f3 ab                	rep stos %eax,%es:(%edi)
  800ac7:	eb 06                	jmp    800acf <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ac9:	8b 45 0c             	mov    0xc(%ebp),%eax
  800acc:	fc                   	cld    
  800acd:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800acf:	89 f8                	mov    %edi,%eax
  800ad1:	5b                   	pop    %ebx
  800ad2:	5e                   	pop    %esi
  800ad3:	5f                   	pop    %edi
  800ad4:	5d                   	pop    %ebp
  800ad5:	c3                   	ret    

00800ad6 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ad6:	55                   	push   %ebp
  800ad7:	89 e5                	mov    %esp,%ebp
  800ad9:	57                   	push   %edi
  800ada:	56                   	push   %esi
  800adb:	8b 45 08             	mov    0x8(%ebp),%eax
  800ade:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ae1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ae4:	39 c6                	cmp    %eax,%esi
  800ae6:	73 33                	jae    800b1b <memmove+0x45>
  800ae8:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800aeb:	39 d0                	cmp    %edx,%eax
  800aed:	73 2c                	jae    800b1b <memmove+0x45>
		s += n;
		d += n;
  800aef:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800af2:	89 d6                	mov    %edx,%esi
  800af4:	09 fe                	or     %edi,%esi
  800af6:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800afc:	75 13                	jne    800b11 <memmove+0x3b>
  800afe:	f6 c1 03             	test   $0x3,%cl
  800b01:	75 0e                	jne    800b11 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b03:	83 ef 04             	sub    $0x4,%edi
  800b06:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b09:	c1 e9 02             	shr    $0x2,%ecx
  800b0c:	fd                   	std    
  800b0d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b0f:	eb 07                	jmp    800b18 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b11:	4f                   	dec    %edi
  800b12:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b15:	fd                   	std    
  800b16:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b18:	fc                   	cld    
  800b19:	eb 1d                	jmp    800b38 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b1b:	89 f2                	mov    %esi,%edx
  800b1d:	09 c2                	or     %eax,%edx
  800b1f:	f6 c2 03             	test   $0x3,%dl
  800b22:	75 0f                	jne    800b33 <memmove+0x5d>
  800b24:	f6 c1 03             	test   $0x3,%cl
  800b27:	75 0a                	jne    800b33 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b29:	c1 e9 02             	shr    $0x2,%ecx
  800b2c:	89 c7                	mov    %eax,%edi
  800b2e:	fc                   	cld    
  800b2f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b31:	eb 05                	jmp    800b38 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b33:	89 c7                	mov    %eax,%edi
  800b35:	fc                   	cld    
  800b36:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b38:	5e                   	pop    %esi
  800b39:	5f                   	pop    %edi
  800b3a:	5d                   	pop    %ebp
  800b3b:	c3                   	ret    

00800b3c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b3c:	55                   	push   %ebp
  800b3d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b3f:	ff 75 10             	pushl  0x10(%ebp)
  800b42:	ff 75 0c             	pushl  0xc(%ebp)
  800b45:	ff 75 08             	pushl  0x8(%ebp)
  800b48:	e8 89 ff ff ff       	call   800ad6 <memmove>
}
  800b4d:	c9                   	leave  
  800b4e:	c3                   	ret    

00800b4f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b4f:	55                   	push   %ebp
  800b50:	89 e5                	mov    %esp,%ebp
  800b52:	56                   	push   %esi
  800b53:	53                   	push   %ebx
  800b54:	8b 45 08             	mov    0x8(%ebp),%eax
  800b57:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b5a:	89 c6                	mov    %eax,%esi
  800b5c:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b5f:	eb 14                	jmp    800b75 <memcmp+0x26>
		if (*s1 != *s2)
  800b61:	8a 08                	mov    (%eax),%cl
  800b63:	8a 1a                	mov    (%edx),%bl
  800b65:	38 d9                	cmp    %bl,%cl
  800b67:	74 0a                	je     800b73 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b69:	0f b6 c1             	movzbl %cl,%eax
  800b6c:	0f b6 db             	movzbl %bl,%ebx
  800b6f:	29 d8                	sub    %ebx,%eax
  800b71:	eb 0b                	jmp    800b7e <memcmp+0x2f>
		s1++, s2++;
  800b73:	40                   	inc    %eax
  800b74:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b75:	39 f0                	cmp    %esi,%eax
  800b77:	75 e8                	jne    800b61 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b79:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b7e:	5b                   	pop    %ebx
  800b7f:	5e                   	pop    %esi
  800b80:	5d                   	pop    %ebp
  800b81:	c3                   	ret    

00800b82 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b82:	55                   	push   %ebp
  800b83:	89 e5                	mov    %esp,%ebp
  800b85:	53                   	push   %ebx
  800b86:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b89:	89 c1                	mov    %eax,%ecx
  800b8b:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b8e:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b92:	eb 08                	jmp    800b9c <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b94:	0f b6 10             	movzbl (%eax),%edx
  800b97:	39 da                	cmp    %ebx,%edx
  800b99:	74 05                	je     800ba0 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b9b:	40                   	inc    %eax
  800b9c:	39 c8                	cmp    %ecx,%eax
  800b9e:	72 f4                	jb     800b94 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800ba0:	5b                   	pop    %ebx
  800ba1:	5d                   	pop    %ebp
  800ba2:	c3                   	ret    

00800ba3 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800ba3:	55                   	push   %ebp
  800ba4:	89 e5                	mov    %esp,%ebp
  800ba6:	57                   	push   %edi
  800ba7:	56                   	push   %esi
  800ba8:	53                   	push   %ebx
  800ba9:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bac:	eb 01                	jmp    800baf <strtol+0xc>
		s++;
  800bae:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800baf:	8a 01                	mov    (%ecx),%al
  800bb1:	3c 20                	cmp    $0x20,%al
  800bb3:	74 f9                	je     800bae <strtol+0xb>
  800bb5:	3c 09                	cmp    $0x9,%al
  800bb7:	74 f5                	je     800bae <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bb9:	3c 2b                	cmp    $0x2b,%al
  800bbb:	75 08                	jne    800bc5 <strtol+0x22>
		s++;
  800bbd:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bbe:	bf 00 00 00 00       	mov    $0x0,%edi
  800bc3:	eb 11                	jmp    800bd6 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bc5:	3c 2d                	cmp    $0x2d,%al
  800bc7:	75 08                	jne    800bd1 <strtol+0x2e>
		s++, neg = 1;
  800bc9:	41                   	inc    %ecx
  800bca:	bf 01 00 00 00       	mov    $0x1,%edi
  800bcf:	eb 05                	jmp    800bd6 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bd1:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bd6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bda:	0f 84 87 00 00 00    	je     800c67 <strtol+0xc4>
  800be0:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800be4:	75 27                	jne    800c0d <strtol+0x6a>
  800be6:	80 39 30             	cmpb   $0x30,(%ecx)
  800be9:	75 22                	jne    800c0d <strtol+0x6a>
  800beb:	e9 88 00 00 00       	jmp    800c78 <strtol+0xd5>
		s += 2, base = 16;
  800bf0:	83 c1 02             	add    $0x2,%ecx
  800bf3:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800bfa:	eb 11                	jmp    800c0d <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800bfc:	41                   	inc    %ecx
  800bfd:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c04:	eb 07                	jmp    800c0d <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c06:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c0d:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c12:	8a 11                	mov    (%ecx),%dl
  800c14:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c17:	80 fb 09             	cmp    $0x9,%bl
  800c1a:	77 08                	ja     800c24 <strtol+0x81>
			dig = *s - '0';
  800c1c:	0f be d2             	movsbl %dl,%edx
  800c1f:	83 ea 30             	sub    $0x30,%edx
  800c22:	eb 22                	jmp    800c46 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c24:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c27:	89 f3                	mov    %esi,%ebx
  800c29:	80 fb 19             	cmp    $0x19,%bl
  800c2c:	77 08                	ja     800c36 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c2e:	0f be d2             	movsbl %dl,%edx
  800c31:	83 ea 57             	sub    $0x57,%edx
  800c34:	eb 10                	jmp    800c46 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c36:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c39:	89 f3                	mov    %esi,%ebx
  800c3b:	80 fb 19             	cmp    $0x19,%bl
  800c3e:	77 14                	ja     800c54 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c40:	0f be d2             	movsbl %dl,%edx
  800c43:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c46:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c49:	7d 09                	jge    800c54 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c4b:	41                   	inc    %ecx
  800c4c:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c50:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c52:	eb be                	jmp    800c12 <strtol+0x6f>

	if (endptr)
  800c54:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c58:	74 05                	je     800c5f <strtol+0xbc>
		*endptr = (char *) s;
  800c5a:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c5d:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c5f:	85 ff                	test   %edi,%edi
  800c61:	74 21                	je     800c84 <strtol+0xe1>
  800c63:	f7 d8                	neg    %eax
  800c65:	eb 1d                	jmp    800c84 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c67:	80 39 30             	cmpb   $0x30,(%ecx)
  800c6a:	75 9a                	jne    800c06 <strtol+0x63>
  800c6c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c70:	0f 84 7a ff ff ff    	je     800bf0 <strtol+0x4d>
  800c76:	eb 84                	jmp    800bfc <strtol+0x59>
  800c78:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c7c:	0f 84 6e ff ff ff    	je     800bf0 <strtol+0x4d>
  800c82:	eb 89                	jmp    800c0d <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c84:	5b                   	pop    %ebx
  800c85:	5e                   	pop    %esi
  800c86:	5f                   	pop    %edi
  800c87:	5d                   	pop    %ebp
  800c88:	c3                   	ret    
  800c89:	66 90                	xchg   %ax,%ax
  800c8b:	90                   	nop

00800c8c <__udivdi3>:
  800c8c:	55                   	push   %ebp
  800c8d:	57                   	push   %edi
  800c8e:	56                   	push   %esi
  800c8f:	53                   	push   %ebx
  800c90:	83 ec 1c             	sub    $0x1c,%esp
  800c93:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800c97:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800c9b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c9f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ca3:	89 ca                	mov    %ecx,%edx
  800ca5:	89 f8                	mov    %edi,%eax
  800ca7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cab:	85 f6                	test   %esi,%esi
  800cad:	75 2d                	jne    800cdc <__udivdi3+0x50>
  800caf:	39 cf                	cmp    %ecx,%edi
  800cb1:	77 65                	ja     800d18 <__udivdi3+0x8c>
  800cb3:	89 fd                	mov    %edi,%ebp
  800cb5:	85 ff                	test   %edi,%edi
  800cb7:	75 0b                	jne    800cc4 <__udivdi3+0x38>
  800cb9:	b8 01 00 00 00       	mov    $0x1,%eax
  800cbe:	31 d2                	xor    %edx,%edx
  800cc0:	f7 f7                	div    %edi
  800cc2:	89 c5                	mov    %eax,%ebp
  800cc4:	31 d2                	xor    %edx,%edx
  800cc6:	89 c8                	mov    %ecx,%eax
  800cc8:	f7 f5                	div    %ebp
  800cca:	89 c1                	mov    %eax,%ecx
  800ccc:	89 d8                	mov    %ebx,%eax
  800cce:	f7 f5                	div    %ebp
  800cd0:	89 cf                	mov    %ecx,%edi
  800cd2:	89 fa                	mov    %edi,%edx
  800cd4:	83 c4 1c             	add    $0x1c,%esp
  800cd7:	5b                   	pop    %ebx
  800cd8:	5e                   	pop    %esi
  800cd9:	5f                   	pop    %edi
  800cda:	5d                   	pop    %ebp
  800cdb:	c3                   	ret    
  800cdc:	39 ce                	cmp    %ecx,%esi
  800cde:	77 28                	ja     800d08 <__udivdi3+0x7c>
  800ce0:	0f bd fe             	bsr    %esi,%edi
  800ce3:	83 f7 1f             	xor    $0x1f,%edi
  800ce6:	75 40                	jne    800d28 <__udivdi3+0x9c>
  800ce8:	39 ce                	cmp    %ecx,%esi
  800cea:	72 0a                	jb     800cf6 <__udivdi3+0x6a>
  800cec:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800cf0:	0f 87 9e 00 00 00    	ja     800d94 <__udivdi3+0x108>
  800cf6:	b8 01 00 00 00       	mov    $0x1,%eax
  800cfb:	89 fa                	mov    %edi,%edx
  800cfd:	83 c4 1c             	add    $0x1c,%esp
  800d00:	5b                   	pop    %ebx
  800d01:	5e                   	pop    %esi
  800d02:	5f                   	pop    %edi
  800d03:	5d                   	pop    %ebp
  800d04:	c3                   	ret    
  800d05:	8d 76 00             	lea    0x0(%esi),%esi
  800d08:	31 ff                	xor    %edi,%edi
  800d0a:	31 c0                	xor    %eax,%eax
  800d0c:	89 fa                	mov    %edi,%edx
  800d0e:	83 c4 1c             	add    $0x1c,%esp
  800d11:	5b                   	pop    %ebx
  800d12:	5e                   	pop    %esi
  800d13:	5f                   	pop    %edi
  800d14:	5d                   	pop    %ebp
  800d15:	c3                   	ret    
  800d16:	66 90                	xchg   %ax,%ax
  800d18:	89 d8                	mov    %ebx,%eax
  800d1a:	f7 f7                	div    %edi
  800d1c:	31 ff                	xor    %edi,%edi
  800d1e:	89 fa                	mov    %edi,%edx
  800d20:	83 c4 1c             	add    $0x1c,%esp
  800d23:	5b                   	pop    %ebx
  800d24:	5e                   	pop    %esi
  800d25:	5f                   	pop    %edi
  800d26:	5d                   	pop    %ebp
  800d27:	c3                   	ret    
  800d28:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d2d:	89 eb                	mov    %ebp,%ebx
  800d2f:	29 fb                	sub    %edi,%ebx
  800d31:	89 f9                	mov    %edi,%ecx
  800d33:	d3 e6                	shl    %cl,%esi
  800d35:	89 c5                	mov    %eax,%ebp
  800d37:	88 d9                	mov    %bl,%cl
  800d39:	d3 ed                	shr    %cl,%ebp
  800d3b:	89 e9                	mov    %ebp,%ecx
  800d3d:	09 f1                	or     %esi,%ecx
  800d3f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d43:	89 f9                	mov    %edi,%ecx
  800d45:	d3 e0                	shl    %cl,%eax
  800d47:	89 c5                	mov    %eax,%ebp
  800d49:	89 d6                	mov    %edx,%esi
  800d4b:	88 d9                	mov    %bl,%cl
  800d4d:	d3 ee                	shr    %cl,%esi
  800d4f:	89 f9                	mov    %edi,%ecx
  800d51:	d3 e2                	shl    %cl,%edx
  800d53:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d57:	88 d9                	mov    %bl,%cl
  800d59:	d3 e8                	shr    %cl,%eax
  800d5b:	09 c2                	or     %eax,%edx
  800d5d:	89 d0                	mov    %edx,%eax
  800d5f:	89 f2                	mov    %esi,%edx
  800d61:	f7 74 24 0c          	divl   0xc(%esp)
  800d65:	89 d6                	mov    %edx,%esi
  800d67:	89 c3                	mov    %eax,%ebx
  800d69:	f7 e5                	mul    %ebp
  800d6b:	39 d6                	cmp    %edx,%esi
  800d6d:	72 19                	jb     800d88 <__udivdi3+0xfc>
  800d6f:	74 0b                	je     800d7c <__udivdi3+0xf0>
  800d71:	89 d8                	mov    %ebx,%eax
  800d73:	31 ff                	xor    %edi,%edi
  800d75:	e9 58 ff ff ff       	jmp    800cd2 <__udivdi3+0x46>
  800d7a:	66 90                	xchg   %ax,%ax
  800d7c:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d80:	89 f9                	mov    %edi,%ecx
  800d82:	d3 e2                	shl    %cl,%edx
  800d84:	39 c2                	cmp    %eax,%edx
  800d86:	73 e9                	jae    800d71 <__udivdi3+0xe5>
  800d88:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d8b:	31 ff                	xor    %edi,%edi
  800d8d:	e9 40 ff ff ff       	jmp    800cd2 <__udivdi3+0x46>
  800d92:	66 90                	xchg   %ax,%ax
  800d94:	31 c0                	xor    %eax,%eax
  800d96:	e9 37 ff ff ff       	jmp    800cd2 <__udivdi3+0x46>
  800d9b:	90                   	nop

00800d9c <__umoddi3>:
  800d9c:	55                   	push   %ebp
  800d9d:	57                   	push   %edi
  800d9e:	56                   	push   %esi
  800d9f:	53                   	push   %ebx
  800da0:	83 ec 1c             	sub    $0x1c,%esp
  800da3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800da7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dab:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800daf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800db3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800db7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dbb:	89 f3                	mov    %esi,%ebx
  800dbd:	89 fa                	mov    %edi,%edx
  800dbf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dc3:	89 34 24             	mov    %esi,(%esp)
  800dc6:	85 c0                	test   %eax,%eax
  800dc8:	75 1a                	jne    800de4 <__umoddi3+0x48>
  800dca:	39 f7                	cmp    %esi,%edi
  800dcc:	0f 86 a2 00 00 00    	jbe    800e74 <__umoddi3+0xd8>
  800dd2:	89 c8                	mov    %ecx,%eax
  800dd4:	89 f2                	mov    %esi,%edx
  800dd6:	f7 f7                	div    %edi
  800dd8:	89 d0                	mov    %edx,%eax
  800dda:	31 d2                	xor    %edx,%edx
  800ddc:	83 c4 1c             	add    $0x1c,%esp
  800ddf:	5b                   	pop    %ebx
  800de0:	5e                   	pop    %esi
  800de1:	5f                   	pop    %edi
  800de2:	5d                   	pop    %ebp
  800de3:	c3                   	ret    
  800de4:	39 f0                	cmp    %esi,%eax
  800de6:	0f 87 ac 00 00 00    	ja     800e98 <__umoddi3+0xfc>
  800dec:	0f bd e8             	bsr    %eax,%ebp
  800def:	83 f5 1f             	xor    $0x1f,%ebp
  800df2:	0f 84 ac 00 00 00    	je     800ea4 <__umoddi3+0x108>
  800df8:	bf 20 00 00 00       	mov    $0x20,%edi
  800dfd:	29 ef                	sub    %ebp,%edi
  800dff:	89 fe                	mov    %edi,%esi
  800e01:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e05:	89 e9                	mov    %ebp,%ecx
  800e07:	d3 e0                	shl    %cl,%eax
  800e09:	89 d7                	mov    %edx,%edi
  800e0b:	89 f1                	mov    %esi,%ecx
  800e0d:	d3 ef                	shr    %cl,%edi
  800e0f:	09 c7                	or     %eax,%edi
  800e11:	89 e9                	mov    %ebp,%ecx
  800e13:	d3 e2                	shl    %cl,%edx
  800e15:	89 14 24             	mov    %edx,(%esp)
  800e18:	89 d8                	mov    %ebx,%eax
  800e1a:	d3 e0                	shl    %cl,%eax
  800e1c:	89 c2                	mov    %eax,%edx
  800e1e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e22:	d3 e0                	shl    %cl,%eax
  800e24:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e28:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e2c:	89 f1                	mov    %esi,%ecx
  800e2e:	d3 e8                	shr    %cl,%eax
  800e30:	09 d0                	or     %edx,%eax
  800e32:	d3 eb                	shr    %cl,%ebx
  800e34:	89 da                	mov    %ebx,%edx
  800e36:	f7 f7                	div    %edi
  800e38:	89 d3                	mov    %edx,%ebx
  800e3a:	f7 24 24             	mull   (%esp)
  800e3d:	89 c6                	mov    %eax,%esi
  800e3f:	89 d1                	mov    %edx,%ecx
  800e41:	39 d3                	cmp    %edx,%ebx
  800e43:	0f 82 87 00 00 00    	jb     800ed0 <__umoddi3+0x134>
  800e49:	0f 84 91 00 00 00    	je     800ee0 <__umoddi3+0x144>
  800e4f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e53:	29 f2                	sub    %esi,%edx
  800e55:	19 cb                	sbb    %ecx,%ebx
  800e57:	89 d8                	mov    %ebx,%eax
  800e59:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e5d:	d3 e0                	shl    %cl,%eax
  800e5f:	89 e9                	mov    %ebp,%ecx
  800e61:	d3 ea                	shr    %cl,%edx
  800e63:	09 d0                	or     %edx,%eax
  800e65:	89 e9                	mov    %ebp,%ecx
  800e67:	d3 eb                	shr    %cl,%ebx
  800e69:	89 da                	mov    %ebx,%edx
  800e6b:	83 c4 1c             	add    $0x1c,%esp
  800e6e:	5b                   	pop    %ebx
  800e6f:	5e                   	pop    %esi
  800e70:	5f                   	pop    %edi
  800e71:	5d                   	pop    %ebp
  800e72:	c3                   	ret    
  800e73:	90                   	nop
  800e74:	89 fd                	mov    %edi,%ebp
  800e76:	85 ff                	test   %edi,%edi
  800e78:	75 0b                	jne    800e85 <__umoddi3+0xe9>
  800e7a:	b8 01 00 00 00       	mov    $0x1,%eax
  800e7f:	31 d2                	xor    %edx,%edx
  800e81:	f7 f7                	div    %edi
  800e83:	89 c5                	mov    %eax,%ebp
  800e85:	89 f0                	mov    %esi,%eax
  800e87:	31 d2                	xor    %edx,%edx
  800e89:	f7 f5                	div    %ebp
  800e8b:	89 c8                	mov    %ecx,%eax
  800e8d:	f7 f5                	div    %ebp
  800e8f:	89 d0                	mov    %edx,%eax
  800e91:	e9 44 ff ff ff       	jmp    800dda <__umoddi3+0x3e>
  800e96:	66 90                	xchg   %ax,%ax
  800e98:	89 c8                	mov    %ecx,%eax
  800e9a:	89 f2                	mov    %esi,%edx
  800e9c:	83 c4 1c             	add    $0x1c,%esp
  800e9f:	5b                   	pop    %ebx
  800ea0:	5e                   	pop    %esi
  800ea1:	5f                   	pop    %edi
  800ea2:	5d                   	pop    %ebp
  800ea3:	c3                   	ret    
  800ea4:	3b 04 24             	cmp    (%esp),%eax
  800ea7:	72 06                	jb     800eaf <__umoddi3+0x113>
  800ea9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ead:	77 0f                	ja     800ebe <__umoddi3+0x122>
  800eaf:	89 f2                	mov    %esi,%edx
  800eb1:	29 f9                	sub    %edi,%ecx
  800eb3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800eb7:	89 14 24             	mov    %edx,(%esp)
  800eba:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ebe:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ec2:	8b 14 24             	mov    (%esp),%edx
  800ec5:	83 c4 1c             	add    $0x1c,%esp
  800ec8:	5b                   	pop    %ebx
  800ec9:	5e                   	pop    %esi
  800eca:	5f                   	pop    %edi
  800ecb:	5d                   	pop    %ebp
  800ecc:	c3                   	ret    
  800ecd:	8d 76 00             	lea    0x0(%esi),%esi
  800ed0:	2b 04 24             	sub    (%esp),%eax
  800ed3:	19 fa                	sbb    %edi,%edx
  800ed5:	89 d1                	mov    %edx,%ecx
  800ed7:	89 c6                	mov    %eax,%esi
  800ed9:	e9 71 ff ff ff       	jmp    800e4f <__umoddi3+0xb3>
  800ede:	66 90                	xchg   %ax,%ax
  800ee0:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800ee4:	72 ea                	jb     800ed0 <__umoddi3+0x134>
  800ee6:	89 d9                	mov    %ebx,%ecx
  800ee8:	e9 62 ff ff ff       	jmp    800e4f <__umoddi3+0xb3>
