
obj/user/fairness:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 70 00 00 00       	call   8000a1 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	83 ec 10             	sub    $0x10,%esp
	envid_t who, id;

	id = sys_getenvid();
  80003b:	e8 5e 0a 00 00       	call   800a9e <sys_getenvid>
  800040:	89 c3                	mov    %eax,%ebx

	if (thisenv == &envs[1]) {
  800042:	81 3d 04 20 80 00 7c 	cmpl   $0xeec0007c,0x802004
  800049:	00 c0 ee 
  80004c:	75 26                	jne    800074 <umain+0x41>
		while (1) {
			ipc_recv(&who, 0, 0);
  80004e:	8d 75 f4             	lea    -0xc(%ebp),%esi
  800051:	83 ec 04             	sub    $0x4,%esp
  800054:	6a 00                	push   $0x0
  800056:	6a 00                	push   $0x0
  800058:	56                   	push   %esi
  800059:	e8 4c 0c 00 00       	call   800caa <ipc_recv>
			cprintf("%x recv from %x\n", id, who);
  80005e:	83 c4 0c             	add    $0xc,%esp
  800061:	ff 75 f4             	pushl  -0xc(%ebp)
  800064:	53                   	push   %ebx
  800065:	68 60 10 80 00       	push   $0x801060
  80006a:	e8 25 01 00 00       	call   800194 <cprintf>
  80006f:	83 c4 10             	add    $0x10,%esp
  800072:	eb dd                	jmp    800051 <umain+0x1e>
		}
	} else {
		cprintf("%x loop sending to %x\n", id, envs[1].env_id);
  800074:	a1 c4 00 c0 ee       	mov    0xeec000c4,%eax
  800079:	83 ec 04             	sub    $0x4,%esp
  80007c:	50                   	push   %eax
  80007d:	53                   	push   %ebx
  80007e:	68 71 10 80 00       	push   $0x801071
  800083:	e8 0c 01 00 00       	call   800194 <cprintf>
  800088:	83 c4 10             	add    $0x10,%esp
		while (1)
			ipc_send(envs[1].env_id, 0, 0, 0);
  80008b:	a1 c4 00 c0 ee       	mov    0xeec000c4,%eax
  800090:	6a 00                	push   $0x0
  800092:	6a 00                	push   $0x0
  800094:	6a 00                	push   $0x0
  800096:	50                   	push   %eax
  800097:	e8 76 0c 00 00       	call   800d12 <ipc_send>
  80009c:	83 c4 10             	add    $0x10,%esp
  80009f:	eb ea                	jmp    80008b <umain+0x58>

008000a1 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000a1:	55                   	push   %ebp
  8000a2:	89 e5                	mov    %esp,%ebp
  8000a4:	56                   	push   %esi
  8000a5:	53                   	push   %ebx
  8000a6:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000a9:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000ac:	e8 ed 09 00 00       	call   800a9e <sys_getenvid>
  8000b1:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000b6:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000bd:	c1 e0 07             	shl    $0x7,%eax
  8000c0:	29 d0                	sub    %edx,%eax
  8000c2:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000c7:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000cc:	85 db                	test   %ebx,%ebx
  8000ce:	7e 07                	jle    8000d7 <libmain+0x36>
		binaryname = argv[0];
  8000d0:	8b 06                	mov    (%esi),%eax
  8000d2:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000d7:	83 ec 08             	sub    $0x8,%esp
  8000da:	56                   	push   %esi
  8000db:	53                   	push   %ebx
  8000dc:	e8 52 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000e1:	e8 0a 00 00 00       	call   8000f0 <exit>
}
  8000e6:	83 c4 10             	add    $0x10,%esp
  8000e9:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000ec:	5b                   	pop    %ebx
  8000ed:	5e                   	pop    %esi
  8000ee:	5d                   	pop    %ebp
  8000ef:	c3                   	ret    

008000f0 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000f0:	55                   	push   %ebp
  8000f1:	89 e5                	mov    %esp,%ebp
  8000f3:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000f6:	6a 00                	push   $0x0
  8000f8:	e8 60 09 00 00       	call   800a5d <sys_env_destroy>
}
  8000fd:	83 c4 10             	add    $0x10,%esp
  800100:	c9                   	leave  
  800101:	c3                   	ret    

00800102 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800102:	55                   	push   %ebp
  800103:	89 e5                	mov    %esp,%ebp
  800105:	53                   	push   %ebx
  800106:	83 ec 04             	sub    $0x4,%esp
  800109:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80010c:	8b 13                	mov    (%ebx),%edx
  80010e:	8d 42 01             	lea    0x1(%edx),%eax
  800111:	89 03                	mov    %eax,(%ebx)
  800113:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800116:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80011a:	3d ff 00 00 00       	cmp    $0xff,%eax
  80011f:	75 1a                	jne    80013b <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800121:	83 ec 08             	sub    $0x8,%esp
  800124:	68 ff 00 00 00       	push   $0xff
  800129:	8d 43 08             	lea    0x8(%ebx),%eax
  80012c:	50                   	push   %eax
  80012d:	e8 ee 08 00 00       	call   800a20 <sys_cputs>
		b->idx = 0;
  800132:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800138:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80013b:	ff 43 04             	incl   0x4(%ebx)
}
  80013e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800141:	c9                   	leave  
  800142:	c3                   	ret    

00800143 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800143:	55                   	push   %ebp
  800144:	89 e5                	mov    %esp,%ebp
  800146:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80014c:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800153:	00 00 00 
	b.cnt = 0;
  800156:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80015d:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800160:	ff 75 0c             	pushl  0xc(%ebp)
  800163:	ff 75 08             	pushl  0x8(%ebp)
  800166:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80016c:	50                   	push   %eax
  80016d:	68 02 01 80 00       	push   $0x800102
  800172:	e8 51 01 00 00       	call   8002c8 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800177:	83 c4 08             	add    $0x8,%esp
  80017a:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800180:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800186:	50                   	push   %eax
  800187:	e8 94 08 00 00       	call   800a20 <sys_cputs>

	return b.cnt;
}
  80018c:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800192:	c9                   	leave  
  800193:	c3                   	ret    

00800194 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800194:	55                   	push   %ebp
  800195:	89 e5                	mov    %esp,%ebp
  800197:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80019a:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80019d:	50                   	push   %eax
  80019e:	ff 75 08             	pushl  0x8(%ebp)
  8001a1:	e8 9d ff ff ff       	call   800143 <vcprintf>
	va_end(ap);

	return cnt;
}
  8001a6:	c9                   	leave  
  8001a7:	c3                   	ret    

008001a8 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001a8:	55                   	push   %ebp
  8001a9:	89 e5                	mov    %esp,%ebp
  8001ab:	57                   	push   %edi
  8001ac:	56                   	push   %esi
  8001ad:	53                   	push   %ebx
  8001ae:	83 ec 1c             	sub    $0x1c,%esp
  8001b1:	89 c7                	mov    %eax,%edi
  8001b3:	89 d6                	mov    %edx,%esi
  8001b5:	8b 45 08             	mov    0x8(%ebp),%eax
  8001b8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001bb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001be:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001c1:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001c4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001c9:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001cc:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001cf:	39 d3                	cmp    %edx,%ebx
  8001d1:	72 05                	jb     8001d8 <printnum+0x30>
  8001d3:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001d6:	77 45                	ja     80021d <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001d8:	83 ec 0c             	sub    $0xc,%esp
  8001db:	ff 75 18             	pushl  0x18(%ebp)
  8001de:	8b 45 14             	mov    0x14(%ebp),%eax
  8001e1:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001e4:	53                   	push   %ebx
  8001e5:	ff 75 10             	pushl  0x10(%ebp)
  8001e8:	83 ec 08             	sub    $0x8,%esp
  8001eb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ee:	ff 75 e0             	pushl  -0x20(%ebp)
  8001f1:	ff 75 dc             	pushl  -0x24(%ebp)
  8001f4:	ff 75 d8             	pushl  -0x28(%ebp)
  8001f7:	e8 f8 0b 00 00       	call   800df4 <__udivdi3>
  8001fc:	83 c4 18             	add    $0x18,%esp
  8001ff:	52                   	push   %edx
  800200:	50                   	push   %eax
  800201:	89 f2                	mov    %esi,%edx
  800203:	89 f8                	mov    %edi,%eax
  800205:	e8 9e ff ff ff       	call   8001a8 <printnum>
  80020a:	83 c4 20             	add    $0x20,%esp
  80020d:	eb 16                	jmp    800225 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80020f:	83 ec 08             	sub    $0x8,%esp
  800212:	56                   	push   %esi
  800213:	ff 75 18             	pushl  0x18(%ebp)
  800216:	ff d7                	call   *%edi
  800218:	83 c4 10             	add    $0x10,%esp
  80021b:	eb 03                	jmp    800220 <printnum+0x78>
  80021d:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800220:	4b                   	dec    %ebx
  800221:	85 db                	test   %ebx,%ebx
  800223:	7f ea                	jg     80020f <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800225:	83 ec 08             	sub    $0x8,%esp
  800228:	56                   	push   %esi
  800229:	83 ec 04             	sub    $0x4,%esp
  80022c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80022f:	ff 75 e0             	pushl  -0x20(%ebp)
  800232:	ff 75 dc             	pushl  -0x24(%ebp)
  800235:	ff 75 d8             	pushl  -0x28(%ebp)
  800238:	e8 c7 0c 00 00       	call   800f04 <__umoddi3>
  80023d:	83 c4 14             	add    $0x14,%esp
  800240:	0f be 80 92 10 80 00 	movsbl 0x801092(%eax),%eax
  800247:	50                   	push   %eax
  800248:	ff d7                	call   *%edi
}
  80024a:	83 c4 10             	add    $0x10,%esp
  80024d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800250:	5b                   	pop    %ebx
  800251:	5e                   	pop    %esi
  800252:	5f                   	pop    %edi
  800253:	5d                   	pop    %ebp
  800254:	c3                   	ret    

00800255 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800255:	55                   	push   %ebp
  800256:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800258:	83 fa 01             	cmp    $0x1,%edx
  80025b:	7e 0e                	jle    80026b <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80025d:	8b 10                	mov    (%eax),%edx
  80025f:	8d 4a 08             	lea    0x8(%edx),%ecx
  800262:	89 08                	mov    %ecx,(%eax)
  800264:	8b 02                	mov    (%edx),%eax
  800266:	8b 52 04             	mov    0x4(%edx),%edx
  800269:	eb 22                	jmp    80028d <getuint+0x38>
	else if (lflag)
  80026b:	85 d2                	test   %edx,%edx
  80026d:	74 10                	je     80027f <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80026f:	8b 10                	mov    (%eax),%edx
  800271:	8d 4a 04             	lea    0x4(%edx),%ecx
  800274:	89 08                	mov    %ecx,(%eax)
  800276:	8b 02                	mov    (%edx),%eax
  800278:	ba 00 00 00 00       	mov    $0x0,%edx
  80027d:	eb 0e                	jmp    80028d <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80027f:	8b 10                	mov    (%eax),%edx
  800281:	8d 4a 04             	lea    0x4(%edx),%ecx
  800284:	89 08                	mov    %ecx,(%eax)
  800286:	8b 02                	mov    (%edx),%eax
  800288:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80028d:	5d                   	pop    %ebp
  80028e:	c3                   	ret    

0080028f <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80028f:	55                   	push   %ebp
  800290:	89 e5                	mov    %esp,%ebp
  800292:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800295:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800298:	8b 10                	mov    (%eax),%edx
  80029a:	3b 50 04             	cmp    0x4(%eax),%edx
  80029d:	73 0a                	jae    8002a9 <sprintputch+0x1a>
		*b->buf++ = ch;
  80029f:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002a2:	89 08                	mov    %ecx,(%eax)
  8002a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a7:	88 02                	mov    %al,(%edx)
}
  8002a9:	5d                   	pop    %ebp
  8002aa:	c3                   	ret    

008002ab <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002ab:	55                   	push   %ebp
  8002ac:	89 e5                	mov    %esp,%ebp
  8002ae:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002b1:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002b4:	50                   	push   %eax
  8002b5:	ff 75 10             	pushl  0x10(%ebp)
  8002b8:	ff 75 0c             	pushl  0xc(%ebp)
  8002bb:	ff 75 08             	pushl  0x8(%ebp)
  8002be:	e8 05 00 00 00       	call   8002c8 <vprintfmt>
	va_end(ap);
}
  8002c3:	83 c4 10             	add    $0x10,%esp
  8002c6:	c9                   	leave  
  8002c7:	c3                   	ret    

008002c8 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002c8:	55                   	push   %ebp
  8002c9:	89 e5                	mov    %esp,%ebp
  8002cb:	57                   	push   %edi
  8002cc:	56                   	push   %esi
  8002cd:	53                   	push   %ebx
  8002ce:	83 ec 2c             	sub    $0x2c,%esp
  8002d1:	8b 75 08             	mov    0x8(%ebp),%esi
  8002d4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002d7:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002da:	eb 12                	jmp    8002ee <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002dc:	85 c0                	test   %eax,%eax
  8002de:	0f 84 68 03 00 00    	je     80064c <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002e4:	83 ec 08             	sub    $0x8,%esp
  8002e7:	53                   	push   %ebx
  8002e8:	50                   	push   %eax
  8002e9:	ff d6                	call   *%esi
  8002eb:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002ee:	47                   	inc    %edi
  8002ef:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002f3:	83 f8 25             	cmp    $0x25,%eax
  8002f6:	75 e4                	jne    8002dc <vprintfmt+0x14>
  8002f8:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002fc:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800303:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80030a:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800311:	ba 00 00 00 00       	mov    $0x0,%edx
  800316:	eb 07                	jmp    80031f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800318:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80031b:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80031f:	8d 47 01             	lea    0x1(%edi),%eax
  800322:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800325:	0f b6 0f             	movzbl (%edi),%ecx
  800328:	8a 07                	mov    (%edi),%al
  80032a:	83 e8 23             	sub    $0x23,%eax
  80032d:	3c 55                	cmp    $0x55,%al
  80032f:	0f 87 fe 02 00 00    	ja     800633 <vprintfmt+0x36b>
  800335:	0f b6 c0             	movzbl %al,%eax
  800338:	ff 24 85 60 11 80 00 	jmp    *0x801160(,%eax,4)
  80033f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800342:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800346:	eb d7                	jmp    80031f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800348:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80034b:	b8 00 00 00 00       	mov    $0x0,%eax
  800350:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800353:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800356:	01 c0                	add    %eax,%eax
  800358:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80035c:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80035f:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800362:	83 fa 09             	cmp    $0x9,%edx
  800365:	77 34                	ja     80039b <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800367:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800368:	eb e9                	jmp    800353 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80036a:	8b 45 14             	mov    0x14(%ebp),%eax
  80036d:	8d 48 04             	lea    0x4(%eax),%ecx
  800370:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800373:	8b 00                	mov    (%eax),%eax
  800375:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800378:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80037b:	eb 24                	jmp    8003a1 <vprintfmt+0xd9>
  80037d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800381:	79 07                	jns    80038a <vprintfmt+0xc2>
  800383:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80038a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80038d:	eb 90                	jmp    80031f <vprintfmt+0x57>
  80038f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800392:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800399:	eb 84                	jmp    80031f <vprintfmt+0x57>
  80039b:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80039e:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003a1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003a5:	0f 89 74 ff ff ff    	jns    80031f <vprintfmt+0x57>
				width = precision, precision = -1;
  8003ab:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003ae:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003b1:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003b8:	e9 62 ff ff ff       	jmp    80031f <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003bd:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003be:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003c1:	e9 59 ff ff ff       	jmp    80031f <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003c6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c9:	8d 50 04             	lea    0x4(%eax),%edx
  8003cc:	89 55 14             	mov    %edx,0x14(%ebp)
  8003cf:	83 ec 08             	sub    $0x8,%esp
  8003d2:	53                   	push   %ebx
  8003d3:	ff 30                	pushl  (%eax)
  8003d5:	ff d6                	call   *%esi
			break;
  8003d7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003da:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003dd:	e9 0c ff ff ff       	jmp    8002ee <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003e2:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e5:	8d 50 04             	lea    0x4(%eax),%edx
  8003e8:	89 55 14             	mov    %edx,0x14(%ebp)
  8003eb:	8b 00                	mov    (%eax),%eax
  8003ed:	85 c0                	test   %eax,%eax
  8003ef:	79 02                	jns    8003f3 <vprintfmt+0x12b>
  8003f1:	f7 d8                	neg    %eax
  8003f3:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003f5:	83 f8 08             	cmp    $0x8,%eax
  8003f8:	7f 0b                	jg     800405 <vprintfmt+0x13d>
  8003fa:	8b 04 85 c0 12 80 00 	mov    0x8012c0(,%eax,4),%eax
  800401:	85 c0                	test   %eax,%eax
  800403:	75 18                	jne    80041d <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800405:	52                   	push   %edx
  800406:	68 aa 10 80 00       	push   $0x8010aa
  80040b:	53                   	push   %ebx
  80040c:	56                   	push   %esi
  80040d:	e8 99 fe ff ff       	call   8002ab <printfmt>
  800412:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800415:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800418:	e9 d1 fe ff ff       	jmp    8002ee <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80041d:	50                   	push   %eax
  80041e:	68 b3 10 80 00       	push   $0x8010b3
  800423:	53                   	push   %ebx
  800424:	56                   	push   %esi
  800425:	e8 81 fe ff ff       	call   8002ab <printfmt>
  80042a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80042d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800430:	e9 b9 fe ff ff       	jmp    8002ee <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800435:	8b 45 14             	mov    0x14(%ebp),%eax
  800438:	8d 50 04             	lea    0x4(%eax),%edx
  80043b:	89 55 14             	mov    %edx,0x14(%ebp)
  80043e:	8b 38                	mov    (%eax),%edi
  800440:	85 ff                	test   %edi,%edi
  800442:	75 05                	jne    800449 <vprintfmt+0x181>
				p = "(null)";
  800444:	bf a3 10 80 00       	mov    $0x8010a3,%edi
			if (width > 0 && padc != '-')
  800449:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80044d:	0f 8e 90 00 00 00    	jle    8004e3 <vprintfmt+0x21b>
  800453:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800457:	0f 84 8e 00 00 00    	je     8004eb <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80045d:	83 ec 08             	sub    $0x8,%esp
  800460:	ff 75 d0             	pushl  -0x30(%ebp)
  800463:	57                   	push   %edi
  800464:	e8 70 02 00 00       	call   8006d9 <strnlen>
  800469:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80046c:	29 c1                	sub    %eax,%ecx
  80046e:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800471:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800474:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800478:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80047b:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80047e:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800480:	eb 0d                	jmp    80048f <vprintfmt+0x1c7>
					putch(padc, putdat);
  800482:	83 ec 08             	sub    $0x8,%esp
  800485:	53                   	push   %ebx
  800486:	ff 75 e4             	pushl  -0x1c(%ebp)
  800489:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80048b:	4f                   	dec    %edi
  80048c:	83 c4 10             	add    $0x10,%esp
  80048f:	85 ff                	test   %edi,%edi
  800491:	7f ef                	jg     800482 <vprintfmt+0x1ba>
  800493:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800496:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800499:	89 c8                	mov    %ecx,%eax
  80049b:	85 c9                	test   %ecx,%ecx
  80049d:	79 05                	jns    8004a4 <vprintfmt+0x1dc>
  80049f:	b8 00 00 00 00       	mov    $0x0,%eax
  8004a4:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004a7:	29 c1                	sub    %eax,%ecx
  8004a9:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004ac:	89 75 08             	mov    %esi,0x8(%ebp)
  8004af:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004b2:	eb 3d                	jmp    8004f1 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004b4:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004b8:	74 19                	je     8004d3 <vprintfmt+0x20b>
  8004ba:	0f be c0             	movsbl %al,%eax
  8004bd:	83 e8 20             	sub    $0x20,%eax
  8004c0:	83 f8 5e             	cmp    $0x5e,%eax
  8004c3:	76 0e                	jbe    8004d3 <vprintfmt+0x20b>
					putch('?', putdat);
  8004c5:	83 ec 08             	sub    $0x8,%esp
  8004c8:	53                   	push   %ebx
  8004c9:	6a 3f                	push   $0x3f
  8004cb:	ff 55 08             	call   *0x8(%ebp)
  8004ce:	83 c4 10             	add    $0x10,%esp
  8004d1:	eb 0b                	jmp    8004de <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004d3:	83 ec 08             	sub    $0x8,%esp
  8004d6:	53                   	push   %ebx
  8004d7:	52                   	push   %edx
  8004d8:	ff 55 08             	call   *0x8(%ebp)
  8004db:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004de:	ff 4d e4             	decl   -0x1c(%ebp)
  8004e1:	eb 0e                	jmp    8004f1 <vprintfmt+0x229>
  8004e3:	89 75 08             	mov    %esi,0x8(%ebp)
  8004e6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004e9:	eb 06                	jmp    8004f1 <vprintfmt+0x229>
  8004eb:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ee:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004f1:	47                   	inc    %edi
  8004f2:	8a 47 ff             	mov    -0x1(%edi),%al
  8004f5:	0f be d0             	movsbl %al,%edx
  8004f8:	85 d2                	test   %edx,%edx
  8004fa:	74 1d                	je     800519 <vprintfmt+0x251>
  8004fc:	85 f6                	test   %esi,%esi
  8004fe:	78 b4                	js     8004b4 <vprintfmt+0x1ec>
  800500:	4e                   	dec    %esi
  800501:	79 b1                	jns    8004b4 <vprintfmt+0x1ec>
  800503:	8b 75 08             	mov    0x8(%ebp),%esi
  800506:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800509:	eb 14                	jmp    80051f <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80050b:	83 ec 08             	sub    $0x8,%esp
  80050e:	53                   	push   %ebx
  80050f:	6a 20                	push   $0x20
  800511:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800513:	4f                   	dec    %edi
  800514:	83 c4 10             	add    $0x10,%esp
  800517:	eb 06                	jmp    80051f <vprintfmt+0x257>
  800519:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80051c:	8b 75 08             	mov    0x8(%ebp),%esi
  80051f:	85 ff                	test   %edi,%edi
  800521:	7f e8                	jg     80050b <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800523:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800526:	e9 c3 fd ff ff       	jmp    8002ee <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80052b:	83 fa 01             	cmp    $0x1,%edx
  80052e:	7e 16                	jle    800546 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800530:	8b 45 14             	mov    0x14(%ebp),%eax
  800533:	8d 50 08             	lea    0x8(%eax),%edx
  800536:	89 55 14             	mov    %edx,0x14(%ebp)
  800539:	8b 50 04             	mov    0x4(%eax),%edx
  80053c:	8b 00                	mov    (%eax),%eax
  80053e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800541:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800544:	eb 32                	jmp    800578 <vprintfmt+0x2b0>
	else if (lflag)
  800546:	85 d2                	test   %edx,%edx
  800548:	74 18                	je     800562 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80054a:	8b 45 14             	mov    0x14(%ebp),%eax
  80054d:	8d 50 04             	lea    0x4(%eax),%edx
  800550:	89 55 14             	mov    %edx,0x14(%ebp)
  800553:	8b 00                	mov    (%eax),%eax
  800555:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800558:	89 c1                	mov    %eax,%ecx
  80055a:	c1 f9 1f             	sar    $0x1f,%ecx
  80055d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800560:	eb 16                	jmp    800578 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800562:	8b 45 14             	mov    0x14(%ebp),%eax
  800565:	8d 50 04             	lea    0x4(%eax),%edx
  800568:	89 55 14             	mov    %edx,0x14(%ebp)
  80056b:	8b 00                	mov    (%eax),%eax
  80056d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800570:	89 c1                	mov    %eax,%ecx
  800572:	c1 f9 1f             	sar    $0x1f,%ecx
  800575:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800578:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80057b:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80057e:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800582:	79 76                	jns    8005fa <vprintfmt+0x332>
				putch('-', putdat);
  800584:	83 ec 08             	sub    $0x8,%esp
  800587:	53                   	push   %ebx
  800588:	6a 2d                	push   $0x2d
  80058a:	ff d6                	call   *%esi
				num = -(long long) num;
  80058c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80058f:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800592:	f7 d8                	neg    %eax
  800594:	83 d2 00             	adc    $0x0,%edx
  800597:	f7 da                	neg    %edx
  800599:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80059c:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005a1:	eb 5c                	jmp    8005ff <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005a3:	8d 45 14             	lea    0x14(%ebp),%eax
  8005a6:	e8 aa fc ff ff       	call   800255 <getuint>
			base = 10;
  8005ab:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005b0:	eb 4d                	jmp    8005ff <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005b2:	8d 45 14             	lea    0x14(%ebp),%eax
  8005b5:	e8 9b fc ff ff       	call   800255 <getuint>
			base = 8;
  8005ba:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005bf:	eb 3e                	jmp    8005ff <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005c1:	83 ec 08             	sub    $0x8,%esp
  8005c4:	53                   	push   %ebx
  8005c5:	6a 30                	push   $0x30
  8005c7:	ff d6                	call   *%esi
			putch('x', putdat);
  8005c9:	83 c4 08             	add    $0x8,%esp
  8005cc:	53                   	push   %ebx
  8005cd:	6a 78                	push   $0x78
  8005cf:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005d1:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d4:	8d 50 04             	lea    0x4(%eax),%edx
  8005d7:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005da:	8b 00                	mov    (%eax),%eax
  8005dc:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005e1:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005e4:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005e9:	eb 14                	jmp    8005ff <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005eb:	8d 45 14             	lea    0x14(%ebp),%eax
  8005ee:	e8 62 fc ff ff       	call   800255 <getuint>
			base = 16;
  8005f3:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005f8:	eb 05                	jmp    8005ff <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005fa:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005ff:	83 ec 0c             	sub    $0xc,%esp
  800602:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800606:	57                   	push   %edi
  800607:	ff 75 e4             	pushl  -0x1c(%ebp)
  80060a:	51                   	push   %ecx
  80060b:	52                   	push   %edx
  80060c:	50                   	push   %eax
  80060d:	89 da                	mov    %ebx,%edx
  80060f:	89 f0                	mov    %esi,%eax
  800611:	e8 92 fb ff ff       	call   8001a8 <printnum>
			break;
  800616:	83 c4 20             	add    $0x20,%esp
  800619:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80061c:	e9 cd fc ff ff       	jmp    8002ee <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800621:	83 ec 08             	sub    $0x8,%esp
  800624:	53                   	push   %ebx
  800625:	51                   	push   %ecx
  800626:	ff d6                	call   *%esi
			break;
  800628:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80062b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80062e:	e9 bb fc ff ff       	jmp    8002ee <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800633:	83 ec 08             	sub    $0x8,%esp
  800636:	53                   	push   %ebx
  800637:	6a 25                	push   $0x25
  800639:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80063b:	83 c4 10             	add    $0x10,%esp
  80063e:	eb 01                	jmp    800641 <vprintfmt+0x379>
  800640:	4f                   	dec    %edi
  800641:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800645:	75 f9                	jne    800640 <vprintfmt+0x378>
  800647:	e9 a2 fc ff ff       	jmp    8002ee <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80064c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80064f:	5b                   	pop    %ebx
  800650:	5e                   	pop    %esi
  800651:	5f                   	pop    %edi
  800652:	5d                   	pop    %ebp
  800653:	c3                   	ret    

00800654 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800654:	55                   	push   %ebp
  800655:	89 e5                	mov    %esp,%ebp
  800657:	83 ec 18             	sub    $0x18,%esp
  80065a:	8b 45 08             	mov    0x8(%ebp),%eax
  80065d:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800660:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800663:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800667:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80066a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800671:	85 c0                	test   %eax,%eax
  800673:	74 26                	je     80069b <vsnprintf+0x47>
  800675:	85 d2                	test   %edx,%edx
  800677:	7e 29                	jle    8006a2 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800679:	ff 75 14             	pushl  0x14(%ebp)
  80067c:	ff 75 10             	pushl  0x10(%ebp)
  80067f:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800682:	50                   	push   %eax
  800683:	68 8f 02 80 00       	push   $0x80028f
  800688:	e8 3b fc ff ff       	call   8002c8 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80068d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800690:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800693:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800696:	83 c4 10             	add    $0x10,%esp
  800699:	eb 0c                	jmp    8006a7 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80069b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006a0:	eb 05                	jmp    8006a7 <vsnprintf+0x53>
  8006a2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8006a7:	c9                   	leave  
  8006a8:	c3                   	ret    

008006a9 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006a9:	55                   	push   %ebp
  8006aa:	89 e5                	mov    %esp,%ebp
  8006ac:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006af:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006b2:	50                   	push   %eax
  8006b3:	ff 75 10             	pushl  0x10(%ebp)
  8006b6:	ff 75 0c             	pushl  0xc(%ebp)
  8006b9:	ff 75 08             	pushl  0x8(%ebp)
  8006bc:	e8 93 ff ff ff       	call   800654 <vsnprintf>
	va_end(ap);

	return rc;
}
  8006c1:	c9                   	leave  
  8006c2:	c3                   	ret    

008006c3 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006c3:	55                   	push   %ebp
  8006c4:	89 e5                	mov    %esp,%ebp
  8006c6:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006c9:	b8 00 00 00 00       	mov    $0x0,%eax
  8006ce:	eb 01                	jmp    8006d1 <strlen+0xe>
		n++;
  8006d0:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006d1:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006d5:	75 f9                	jne    8006d0 <strlen+0xd>
		n++;
	return n;
}
  8006d7:	5d                   	pop    %ebp
  8006d8:	c3                   	ret    

008006d9 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006d9:	55                   	push   %ebp
  8006da:	89 e5                	mov    %esp,%ebp
  8006dc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006df:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006e2:	ba 00 00 00 00       	mov    $0x0,%edx
  8006e7:	eb 01                	jmp    8006ea <strnlen+0x11>
		n++;
  8006e9:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006ea:	39 c2                	cmp    %eax,%edx
  8006ec:	74 08                	je     8006f6 <strnlen+0x1d>
  8006ee:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006f2:	75 f5                	jne    8006e9 <strnlen+0x10>
  8006f4:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006f6:	5d                   	pop    %ebp
  8006f7:	c3                   	ret    

008006f8 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006f8:	55                   	push   %ebp
  8006f9:	89 e5                	mov    %esp,%ebp
  8006fb:	53                   	push   %ebx
  8006fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ff:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800702:	89 c2                	mov    %eax,%edx
  800704:	42                   	inc    %edx
  800705:	41                   	inc    %ecx
  800706:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800709:	88 5a ff             	mov    %bl,-0x1(%edx)
  80070c:	84 db                	test   %bl,%bl
  80070e:	75 f4                	jne    800704 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800710:	5b                   	pop    %ebx
  800711:	5d                   	pop    %ebp
  800712:	c3                   	ret    

00800713 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800713:	55                   	push   %ebp
  800714:	89 e5                	mov    %esp,%ebp
  800716:	53                   	push   %ebx
  800717:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80071a:	53                   	push   %ebx
  80071b:	e8 a3 ff ff ff       	call   8006c3 <strlen>
  800720:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800723:	ff 75 0c             	pushl  0xc(%ebp)
  800726:	01 d8                	add    %ebx,%eax
  800728:	50                   	push   %eax
  800729:	e8 ca ff ff ff       	call   8006f8 <strcpy>
	return dst;
}
  80072e:	89 d8                	mov    %ebx,%eax
  800730:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800733:	c9                   	leave  
  800734:	c3                   	ret    

00800735 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800735:	55                   	push   %ebp
  800736:	89 e5                	mov    %esp,%ebp
  800738:	56                   	push   %esi
  800739:	53                   	push   %ebx
  80073a:	8b 75 08             	mov    0x8(%ebp),%esi
  80073d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800740:	89 f3                	mov    %esi,%ebx
  800742:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800745:	89 f2                	mov    %esi,%edx
  800747:	eb 0c                	jmp    800755 <strncpy+0x20>
		*dst++ = *src;
  800749:	42                   	inc    %edx
  80074a:	8a 01                	mov    (%ecx),%al
  80074c:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80074f:	80 39 01             	cmpb   $0x1,(%ecx)
  800752:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800755:	39 da                	cmp    %ebx,%edx
  800757:	75 f0                	jne    800749 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800759:	89 f0                	mov    %esi,%eax
  80075b:	5b                   	pop    %ebx
  80075c:	5e                   	pop    %esi
  80075d:	5d                   	pop    %ebp
  80075e:	c3                   	ret    

0080075f <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80075f:	55                   	push   %ebp
  800760:	89 e5                	mov    %esp,%ebp
  800762:	56                   	push   %esi
  800763:	53                   	push   %ebx
  800764:	8b 75 08             	mov    0x8(%ebp),%esi
  800767:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80076a:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80076d:	85 c0                	test   %eax,%eax
  80076f:	74 1e                	je     80078f <strlcpy+0x30>
  800771:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800775:	89 f2                	mov    %esi,%edx
  800777:	eb 05                	jmp    80077e <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800779:	42                   	inc    %edx
  80077a:	41                   	inc    %ecx
  80077b:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80077e:	39 c2                	cmp    %eax,%edx
  800780:	74 08                	je     80078a <strlcpy+0x2b>
  800782:	8a 19                	mov    (%ecx),%bl
  800784:	84 db                	test   %bl,%bl
  800786:	75 f1                	jne    800779 <strlcpy+0x1a>
  800788:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80078a:	c6 00 00             	movb   $0x0,(%eax)
  80078d:	eb 02                	jmp    800791 <strlcpy+0x32>
  80078f:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800791:	29 f0                	sub    %esi,%eax
}
  800793:	5b                   	pop    %ebx
  800794:	5e                   	pop    %esi
  800795:	5d                   	pop    %ebp
  800796:	c3                   	ret    

00800797 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800797:	55                   	push   %ebp
  800798:	89 e5                	mov    %esp,%ebp
  80079a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80079d:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007a0:	eb 02                	jmp    8007a4 <strcmp+0xd>
		p++, q++;
  8007a2:	41                   	inc    %ecx
  8007a3:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007a4:	8a 01                	mov    (%ecx),%al
  8007a6:	84 c0                	test   %al,%al
  8007a8:	74 04                	je     8007ae <strcmp+0x17>
  8007aa:	3a 02                	cmp    (%edx),%al
  8007ac:	74 f4                	je     8007a2 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007ae:	0f b6 c0             	movzbl %al,%eax
  8007b1:	0f b6 12             	movzbl (%edx),%edx
  8007b4:	29 d0                	sub    %edx,%eax
}
  8007b6:	5d                   	pop    %ebp
  8007b7:	c3                   	ret    

008007b8 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007b8:	55                   	push   %ebp
  8007b9:	89 e5                	mov    %esp,%ebp
  8007bb:	53                   	push   %ebx
  8007bc:	8b 45 08             	mov    0x8(%ebp),%eax
  8007bf:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007c2:	89 c3                	mov    %eax,%ebx
  8007c4:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007c7:	eb 02                	jmp    8007cb <strncmp+0x13>
		n--, p++, q++;
  8007c9:	40                   	inc    %eax
  8007ca:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007cb:	39 d8                	cmp    %ebx,%eax
  8007cd:	74 14                	je     8007e3 <strncmp+0x2b>
  8007cf:	8a 08                	mov    (%eax),%cl
  8007d1:	84 c9                	test   %cl,%cl
  8007d3:	74 04                	je     8007d9 <strncmp+0x21>
  8007d5:	3a 0a                	cmp    (%edx),%cl
  8007d7:	74 f0                	je     8007c9 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007d9:	0f b6 00             	movzbl (%eax),%eax
  8007dc:	0f b6 12             	movzbl (%edx),%edx
  8007df:	29 d0                	sub    %edx,%eax
  8007e1:	eb 05                	jmp    8007e8 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007e3:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007e8:	5b                   	pop    %ebx
  8007e9:	5d                   	pop    %ebp
  8007ea:	c3                   	ret    

008007eb <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007eb:	55                   	push   %ebp
  8007ec:	89 e5                	mov    %esp,%ebp
  8007ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8007f1:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007f4:	eb 05                	jmp    8007fb <strchr+0x10>
		if (*s == c)
  8007f6:	38 ca                	cmp    %cl,%dl
  8007f8:	74 0c                	je     800806 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007fa:	40                   	inc    %eax
  8007fb:	8a 10                	mov    (%eax),%dl
  8007fd:	84 d2                	test   %dl,%dl
  8007ff:	75 f5                	jne    8007f6 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800801:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800806:	5d                   	pop    %ebp
  800807:	c3                   	ret    

00800808 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800808:	55                   	push   %ebp
  800809:	89 e5                	mov    %esp,%ebp
  80080b:	8b 45 08             	mov    0x8(%ebp),%eax
  80080e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800811:	eb 05                	jmp    800818 <strfind+0x10>
		if (*s == c)
  800813:	38 ca                	cmp    %cl,%dl
  800815:	74 07                	je     80081e <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800817:	40                   	inc    %eax
  800818:	8a 10                	mov    (%eax),%dl
  80081a:	84 d2                	test   %dl,%dl
  80081c:	75 f5                	jne    800813 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80081e:	5d                   	pop    %ebp
  80081f:	c3                   	ret    

00800820 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800820:	55                   	push   %ebp
  800821:	89 e5                	mov    %esp,%ebp
  800823:	57                   	push   %edi
  800824:	56                   	push   %esi
  800825:	53                   	push   %ebx
  800826:	8b 7d 08             	mov    0x8(%ebp),%edi
  800829:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80082c:	85 c9                	test   %ecx,%ecx
  80082e:	74 36                	je     800866 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800830:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800836:	75 28                	jne    800860 <memset+0x40>
  800838:	f6 c1 03             	test   $0x3,%cl
  80083b:	75 23                	jne    800860 <memset+0x40>
		c &= 0xFF;
  80083d:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800841:	89 d3                	mov    %edx,%ebx
  800843:	c1 e3 08             	shl    $0x8,%ebx
  800846:	89 d6                	mov    %edx,%esi
  800848:	c1 e6 18             	shl    $0x18,%esi
  80084b:	89 d0                	mov    %edx,%eax
  80084d:	c1 e0 10             	shl    $0x10,%eax
  800850:	09 f0                	or     %esi,%eax
  800852:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800854:	89 d8                	mov    %ebx,%eax
  800856:	09 d0                	or     %edx,%eax
  800858:	c1 e9 02             	shr    $0x2,%ecx
  80085b:	fc                   	cld    
  80085c:	f3 ab                	rep stos %eax,%es:(%edi)
  80085e:	eb 06                	jmp    800866 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800860:	8b 45 0c             	mov    0xc(%ebp),%eax
  800863:	fc                   	cld    
  800864:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800866:	89 f8                	mov    %edi,%eax
  800868:	5b                   	pop    %ebx
  800869:	5e                   	pop    %esi
  80086a:	5f                   	pop    %edi
  80086b:	5d                   	pop    %ebp
  80086c:	c3                   	ret    

0080086d <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80086d:	55                   	push   %ebp
  80086e:	89 e5                	mov    %esp,%ebp
  800870:	57                   	push   %edi
  800871:	56                   	push   %esi
  800872:	8b 45 08             	mov    0x8(%ebp),%eax
  800875:	8b 75 0c             	mov    0xc(%ebp),%esi
  800878:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80087b:	39 c6                	cmp    %eax,%esi
  80087d:	73 33                	jae    8008b2 <memmove+0x45>
  80087f:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800882:	39 d0                	cmp    %edx,%eax
  800884:	73 2c                	jae    8008b2 <memmove+0x45>
		s += n;
		d += n;
  800886:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800889:	89 d6                	mov    %edx,%esi
  80088b:	09 fe                	or     %edi,%esi
  80088d:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800893:	75 13                	jne    8008a8 <memmove+0x3b>
  800895:	f6 c1 03             	test   $0x3,%cl
  800898:	75 0e                	jne    8008a8 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80089a:	83 ef 04             	sub    $0x4,%edi
  80089d:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008a0:	c1 e9 02             	shr    $0x2,%ecx
  8008a3:	fd                   	std    
  8008a4:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008a6:	eb 07                	jmp    8008af <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008a8:	4f                   	dec    %edi
  8008a9:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008ac:	fd                   	std    
  8008ad:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008af:	fc                   	cld    
  8008b0:	eb 1d                	jmp    8008cf <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008b2:	89 f2                	mov    %esi,%edx
  8008b4:	09 c2                	or     %eax,%edx
  8008b6:	f6 c2 03             	test   $0x3,%dl
  8008b9:	75 0f                	jne    8008ca <memmove+0x5d>
  8008bb:	f6 c1 03             	test   $0x3,%cl
  8008be:	75 0a                	jne    8008ca <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008c0:	c1 e9 02             	shr    $0x2,%ecx
  8008c3:	89 c7                	mov    %eax,%edi
  8008c5:	fc                   	cld    
  8008c6:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008c8:	eb 05                	jmp    8008cf <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008ca:	89 c7                	mov    %eax,%edi
  8008cc:	fc                   	cld    
  8008cd:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008cf:	5e                   	pop    %esi
  8008d0:	5f                   	pop    %edi
  8008d1:	5d                   	pop    %ebp
  8008d2:	c3                   	ret    

008008d3 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008d3:	55                   	push   %ebp
  8008d4:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008d6:	ff 75 10             	pushl  0x10(%ebp)
  8008d9:	ff 75 0c             	pushl  0xc(%ebp)
  8008dc:	ff 75 08             	pushl  0x8(%ebp)
  8008df:	e8 89 ff ff ff       	call   80086d <memmove>
}
  8008e4:	c9                   	leave  
  8008e5:	c3                   	ret    

008008e6 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008e6:	55                   	push   %ebp
  8008e7:	89 e5                	mov    %esp,%ebp
  8008e9:	56                   	push   %esi
  8008ea:	53                   	push   %ebx
  8008eb:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ee:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008f1:	89 c6                	mov    %eax,%esi
  8008f3:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008f6:	eb 14                	jmp    80090c <memcmp+0x26>
		if (*s1 != *s2)
  8008f8:	8a 08                	mov    (%eax),%cl
  8008fa:	8a 1a                	mov    (%edx),%bl
  8008fc:	38 d9                	cmp    %bl,%cl
  8008fe:	74 0a                	je     80090a <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800900:	0f b6 c1             	movzbl %cl,%eax
  800903:	0f b6 db             	movzbl %bl,%ebx
  800906:	29 d8                	sub    %ebx,%eax
  800908:	eb 0b                	jmp    800915 <memcmp+0x2f>
		s1++, s2++;
  80090a:	40                   	inc    %eax
  80090b:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80090c:	39 f0                	cmp    %esi,%eax
  80090e:	75 e8                	jne    8008f8 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800910:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800915:	5b                   	pop    %ebx
  800916:	5e                   	pop    %esi
  800917:	5d                   	pop    %ebp
  800918:	c3                   	ret    

00800919 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800919:	55                   	push   %ebp
  80091a:	89 e5                	mov    %esp,%ebp
  80091c:	53                   	push   %ebx
  80091d:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800920:	89 c1                	mov    %eax,%ecx
  800922:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800925:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800929:	eb 08                	jmp    800933 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80092b:	0f b6 10             	movzbl (%eax),%edx
  80092e:	39 da                	cmp    %ebx,%edx
  800930:	74 05                	je     800937 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800932:	40                   	inc    %eax
  800933:	39 c8                	cmp    %ecx,%eax
  800935:	72 f4                	jb     80092b <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800937:	5b                   	pop    %ebx
  800938:	5d                   	pop    %ebp
  800939:	c3                   	ret    

0080093a <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80093a:	55                   	push   %ebp
  80093b:	89 e5                	mov    %esp,%ebp
  80093d:	57                   	push   %edi
  80093e:	56                   	push   %esi
  80093f:	53                   	push   %ebx
  800940:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800943:	eb 01                	jmp    800946 <strtol+0xc>
		s++;
  800945:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800946:	8a 01                	mov    (%ecx),%al
  800948:	3c 20                	cmp    $0x20,%al
  80094a:	74 f9                	je     800945 <strtol+0xb>
  80094c:	3c 09                	cmp    $0x9,%al
  80094e:	74 f5                	je     800945 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800950:	3c 2b                	cmp    $0x2b,%al
  800952:	75 08                	jne    80095c <strtol+0x22>
		s++;
  800954:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800955:	bf 00 00 00 00       	mov    $0x0,%edi
  80095a:	eb 11                	jmp    80096d <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80095c:	3c 2d                	cmp    $0x2d,%al
  80095e:	75 08                	jne    800968 <strtol+0x2e>
		s++, neg = 1;
  800960:	41                   	inc    %ecx
  800961:	bf 01 00 00 00       	mov    $0x1,%edi
  800966:	eb 05                	jmp    80096d <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800968:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80096d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800971:	0f 84 87 00 00 00    	je     8009fe <strtol+0xc4>
  800977:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80097b:	75 27                	jne    8009a4 <strtol+0x6a>
  80097d:	80 39 30             	cmpb   $0x30,(%ecx)
  800980:	75 22                	jne    8009a4 <strtol+0x6a>
  800982:	e9 88 00 00 00       	jmp    800a0f <strtol+0xd5>
		s += 2, base = 16;
  800987:	83 c1 02             	add    $0x2,%ecx
  80098a:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800991:	eb 11                	jmp    8009a4 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800993:	41                   	inc    %ecx
  800994:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80099b:	eb 07                	jmp    8009a4 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  80099d:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009a4:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009a9:	8a 11                	mov    (%ecx),%dl
  8009ab:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009ae:	80 fb 09             	cmp    $0x9,%bl
  8009b1:	77 08                	ja     8009bb <strtol+0x81>
			dig = *s - '0';
  8009b3:	0f be d2             	movsbl %dl,%edx
  8009b6:	83 ea 30             	sub    $0x30,%edx
  8009b9:	eb 22                	jmp    8009dd <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009bb:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009be:	89 f3                	mov    %esi,%ebx
  8009c0:	80 fb 19             	cmp    $0x19,%bl
  8009c3:	77 08                	ja     8009cd <strtol+0x93>
			dig = *s - 'a' + 10;
  8009c5:	0f be d2             	movsbl %dl,%edx
  8009c8:	83 ea 57             	sub    $0x57,%edx
  8009cb:	eb 10                	jmp    8009dd <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009cd:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009d0:	89 f3                	mov    %esi,%ebx
  8009d2:	80 fb 19             	cmp    $0x19,%bl
  8009d5:	77 14                	ja     8009eb <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009d7:	0f be d2             	movsbl %dl,%edx
  8009da:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009dd:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009e0:	7d 09                	jge    8009eb <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009e2:	41                   	inc    %ecx
  8009e3:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009e7:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009e9:	eb be                	jmp    8009a9 <strtol+0x6f>

	if (endptr)
  8009eb:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009ef:	74 05                	je     8009f6 <strtol+0xbc>
		*endptr = (char *) s;
  8009f1:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009f4:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009f6:	85 ff                	test   %edi,%edi
  8009f8:	74 21                	je     800a1b <strtol+0xe1>
  8009fa:	f7 d8                	neg    %eax
  8009fc:	eb 1d                	jmp    800a1b <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009fe:	80 39 30             	cmpb   $0x30,(%ecx)
  800a01:	75 9a                	jne    80099d <strtol+0x63>
  800a03:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a07:	0f 84 7a ff ff ff    	je     800987 <strtol+0x4d>
  800a0d:	eb 84                	jmp    800993 <strtol+0x59>
  800a0f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a13:	0f 84 6e ff ff ff    	je     800987 <strtol+0x4d>
  800a19:	eb 89                	jmp    8009a4 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a1b:	5b                   	pop    %ebx
  800a1c:	5e                   	pop    %esi
  800a1d:	5f                   	pop    %edi
  800a1e:	5d                   	pop    %ebp
  800a1f:	c3                   	ret    

00800a20 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a20:	55                   	push   %ebp
  800a21:	89 e5                	mov    %esp,%ebp
  800a23:	57                   	push   %edi
  800a24:	56                   	push   %esi
  800a25:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a26:	b8 00 00 00 00       	mov    $0x0,%eax
  800a2b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a2e:	8b 55 08             	mov    0x8(%ebp),%edx
  800a31:	89 c3                	mov    %eax,%ebx
  800a33:	89 c7                	mov    %eax,%edi
  800a35:	89 c6                	mov    %eax,%esi
  800a37:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a39:	5b                   	pop    %ebx
  800a3a:	5e                   	pop    %esi
  800a3b:	5f                   	pop    %edi
  800a3c:	5d                   	pop    %ebp
  800a3d:	c3                   	ret    

00800a3e <sys_cgetc>:

int
sys_cgetc(void)
{
  800a3e:	55                   	push   %ebp
  800a3f:	89 e5                	mov    %esp,%ebp
  800a41:	57                   	push   %edi
  800a42:	56                   	push   %esi
  800a43:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a44:	ba 00 00 00 00       	mov    $0x0,%edx
  800a49:	b8 01 00 00 00       	mov    $0x1,%eax
  800a4e:	89 d1                	mov    %edx,%ecx
  800a50:	89 d3                	mov    %edx,%ebx
  800a52:	89 d7                	mov    %edx,%edi
  800a54:	89 d6                	mov    %edx,%esi
  800a56:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a58:	5b                   	pop    %ebx
  800a59:	5e                   	pop    %esi
  800a5a:	5f                   	pop    %edi
  800a5b:	5d                   	pop    %ebp
  800a5c:	c3                   	ret    

00800a5d <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a5d:	55                   	push   %ebp
  800a5e:	89 e5                	mov    %esp,%ebp
  800a60:	57                   	push   %edi
  800a61:	56                   	push   %esi
  800a62:	53                   	push   %ebx
  800a63:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a66:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a6b:	b8 03 00 00 00       	mov    $0x3,%eax
  800a70:	8b 55 08             	mov    0x8(%ebp),%edx
  800a73:	89 cb                	mov    %ecx,%ebx
  800a75:	89 cf                	mov    %ecx,%edi
  800a77:	89 ce                	mov    %ecx,%esi
  800a79:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a7b:	85 c0                	test   %eax,%eax
  800a7d:	7e 17                	jle    800a96 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a7f:	83 ec 0c             	sub    $0xc,%esp
  800a82:	50                   	push   %eax
  800a83:	6a 03                	push   $0x3
  800a85:	68 e4 12 80 00       	push   $0x8012e4
  800a8a:	6a 23                	push   $0x23
  800a8c:	68 01 13 80 00       	push   $0x801301
  800a91:	e8 15 03 00 00       	call   800dab <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a96:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a99:	5b                   	pop    %ebx
  800a9a:	5e                   	pop    %esi
  800a9b:	5f                   	pop    %edi
  800a9c:	5d                   	pop    %ebp
  800a9d:	c3                   	ret    

00800a9e <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a9e:	55                   	push   %ebp
  800a9f:	89 e5                	mov    %esp,%ebp
  800aa1:	57                   	push   %edi
  800aa2:	56                   	push   %esi
  800aa3:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aa4:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa9:	b8 02 00 00 00       	mov    $0x2,%eax
  800aae:	89 d1                	mov    %edx,%ecx
  800ab0:	89 d3                	mov    %edx,%ebx
  800ab2:	89 d7                	mov    %edx,%edi
  800ab4:	89 d6                	mov    %edx,%esi
  800ab6:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800ab8:	5b                   	pop    %ebx
  800ab9:	5e                   	pop    %esi
  800aba:	5f                   	pop    %edi
  800abb:	5d                   	pop    %ebp
  800abc:	c3                   	ret    

00800abd <sys_yield>:

void
sys_yield(void)
{
  800abd:	55                   	push   %ebp
  800abe:	89 e5                	mov    %esp,%ebp
  800ac0:	57                   	push   %edi
  800ac1:	56                   	push   %esi
  800ac2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac3:	ba 00 00 00 00       	mov    $0x0,%edx
  800ac8:	b8 0a 00 00 00       	mov    $0xa,%eax
  800acd:	89 d1                	mov    %edx,%ecx
  800acf:	89 d3                	mov    %edx,%ebx
  800ad1:	89 d7                	mov    %edx,%edi
  800ad3:	89 d6                	mov    %edx,%esi
  800ad5:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800ad7:	5b                   	pop    %ebx
  800ad8:	5e                   	pop    %esi
  800ad9:	5f                   	pop    %edi
  800ada:	5d                   	pop    %ebp
  800adb:	c3                   	ret    

00800adc <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800adc:	55                   	push   %ebp
  800add:	89 e5                	mov    %esp,%ebp
  800adf:	57                   	push   %edi
  800ae0:	56                   	push   %esi
  800ae1:	53                   	push   %ebx
  800ae2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae5:	be 00 00 00 00       	mov    $0x0,%esi
  800aea:	b8 04 00 00 00       	mov    $0x4,%eax
  800aef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800af2:	8b 55 08             	mov    0x8(%ebp),%edx
  800af5:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800af8:	89 f7                	mov    %esi,%edi
  800afa:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800afc:	85 c0                	test   %eax,%eax
  800afe:	7e 17                	jle    800b17 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b00:	83 ec 0c             	sub    $0xc,%esp
  800b03:	50                   	push   %eax
  800b04:	6a 04                	push   $0x4
  800b06:	68 e4 12 80 00       	push   $0x8012e4
  800b0b:	6a 23                	push   $0x23
  800b0d:	68 01 13 80 00       	push   $0x801301
  800b12:	e8 94 02 00 00       	call   800dab <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b17:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b1a:	5b                   	pop    %ebx
  800b1b:	5e                   	pop    %esi
  800b1c:	5f                   	pop    %edi
  800b1d:	5d                   	pop    %ebp
  800b1e:	c3                   	ret    

00800b1f <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b1f:	55                   	push   %ebp
  800b20:	89 e5                	mov    %esp,%ebp
  800b22:	57                   	push   %edi
  800b23:	56                   	push   %esi
  800b24:	53                   	push   %ebx
  800b25:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b28:	b8 05 00 00 00       	mov    $0x5,%eax
  800b2d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b30:	8b 55 08             	mov    0x8(%ebp),%edx
  800b33:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b36:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b39:	8b 75 18             	mov    0x18(%ebp),%esi
  800b3c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b3e:	85 c0                	test   %eax,%eax
  800b40:	7e 17                	jle    800b59 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b42:	83 ec 0c             	sub    $0xc,%esp
  800b45:	50                   	push   %eax
  800b46:	6a 05                	push   $0x5
  800b48:	68 e4 12 80 00       	push   $0x8012e4
  800b4d:	6a 23                	push   $0x23
  800b4f:	68 01 13 80 00       	push   $0x801301
  800b54:	e8 52 02 00 00       	call   800dab <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b59:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b5c:	5b                   	pop    %ebx
  800b5d:	5e                   	pop    %esi
  800b5e:	5f                   	pop    %edi
  800b5f:	5d                   	pop    %ebp
  800b60:	c3                   	ret    

00800b61 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b61:	55                   	push   %ebp
  800b62:	89 e5                	mov    %esp,%ebp
  800b64:	57                   	push   %edi
  800b65:	56                   	push   %esi
  800b66:	53                   	push   %ebx
  800b67:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b6a:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b6f:	b8 06 00 00 00       	mov    $0x6,%eax
  800b74:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b77:	8b 55 08             	mov    0x8(%ebp),%edx
  800b7a:	89 df                	mov    %ebx,%edi
  800b7c:	89 de                	mov    %ebx,%esi
  800b7e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b80:	85 c0                	test   %eax,%eax
  800b82:	7e 17                	jle    800b9b <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b84:	83 ec 0c             	sub    $0xc,%esp
  800b87:	50                   	push   %eax
  800b88:	6a 06                	push   $0x6
  800b8a:	68 e4 12 80 00       	push   $0x8012e4
  800b8f:	6a 23                	push   $0x23
  800b91:	68 01 13 80 00       	push   $0x801301
  800b96:	e8 10 02 00 00       	call   800dab <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800b9b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b9e:	5b                   	pop    %ebx
  800b9f:	5e                   	pop    %esi
  800ba0:	5f                   	pop    %edi
  800ba1:	5d                   	pop    %ebp
  800ba2:	c3                   	ret    

00800ba3 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800ba3:	55                   	push   %ebp
  800ba4:	89 e5                	mov    %esp,%ebp
  800ba6:	57                   	push   %edi
  800ba7:	56                   	push   %esi
  800ba8:	53                   	push   %ebx
  800ba9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bac:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bb1:	b8 08 00 00 00       	mov    $0x8,%eax
  800bb6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bb9:	8b 55 08             	mov    0x8(%ebp),%edx
  800bbc:	89 df                	mov    %ebx,%edi
  800bbe:	89 de                	mov    %ebx,%esi
  800bc0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bc2:	85 c0                	test   %eax,%eax
  800bc4:	7e 17                	jle    800bdd <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bc6:	83 ec 0c             	sub    $0xc,%esp
  800bc9:	50                   	push   %eax
  800bca:	6a 08                	push   $0x8
  800bcc:	68 e4 12 80 00       	push   $0x8012e4
  800bd1:	6a 23                	push   $0x23
  800bd3:	68 01 13 80 00       	push   $0x801301
  800bd8:	e8 ce 01 00 00       	call   800dab <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800bdd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800be0:	5b                   	pop    %ebx
  800be1:	5e                   	pop    %esi
  800be2:	5f                   	pop    %edi
  800be3:	5d                   	pop    %ebp
  800be4:	c3                   	ret    

00800be5 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800be5:	55                   	push   %ebp
  800be6:	89 e5                	mov    %esp,%ebp
  800be8:	57                   	push   %edi
  800be9:	56                   	push   %esi
  800bea:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800beb:	ba 00 00 00 00       	mov    $0x0,%edx
  800bf0:	b8 0b 00 00 00       	mov    $0xb,%eax
  800bf5:	89 d1                	mov    %edx,%ecx
  800bf7:	89 d3                	mov    %edx,%ebx
  800bf9:	89 d7                	mov    %edx,%edi
  800bfb:	89 d6                	mov    %edx,%esi
  800bfd:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800bff:	5b                   	pop    %ebx
  800c00:	5e                   	pop    %esi
  800c01:	5f                   	pop    %edi
  800c02:	5d                   	pop    %ebp
  800c03:	c3                   	ret    

00800c04 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c04:	55                   	push   %ebp
  800c05:	89 e5                	mov    %esp,%ebp
  800c07:	57                   	push   %edi
  800c08:	56                   	push   %esi
  800c09:	53                   	push   %ebx
  800c0a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c0d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c12:	b8 09 00 00 00       	mov    $0x9,%eax
  800c17:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c1a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c1d:	89 df                	mov    %ebx,%edi
  800c1f:	89 de                	mov    %ebx,%esi
  800c21:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c23:	85 c0                	test   %eax,%eax
  800c25:	7e 17                	jle    800c3e <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c27:	83 ec 0c             	sub    $0xc,%esp
  800c2a:	50                   	push   %eax
  800c2b:	6a 09                	push   $0x9
  800c2d:	68 e4 12 80 00       	push   $0x8012e4
  800c32:	6a 23                	push   $0x23
  800c34:	68 01 13 80 00       	push   $0x801301
  800c39:	e8 6d 01 00 00       	call   800dab <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800c3e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c41:	5b                   	pop    %ebx
  800c42:	5e                   	pop    %esi
  800c43:	5f                   	pop    %edi
  800c44:	5d                   	pop    %ebp
  800c45:	c3                   	ret    

00800c46 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800c46:	55                   	push   %ebp
  800c47:	89 e5                	mov    %esp,%ebp
  800c49:	57                   	push   %edi
  800c4a:	56                   	push   %esi
  800c4b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c4c:	be 00 00 00 00       	mov    $0x0,%esi
  800c51:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c56:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c59:	8b 55 08             	mov    0x8(%ebp),%edx
  800c5c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c5f:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c62:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800c64:	5b                   	pop    %ebx
  800c65:	5e                   	pop    %esi
  800c66:	5f                   	pop    %edi
  800c67:	5d                   	pop    %ebp
  800c68:	c3                   	ret    

00800c69 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800c69:	55                   	push   %ebp
  800c6a:	89 e5                	mov    %esp,%ebp
  800c6c:	57                   	push   %edi
  800c6d:	56                   	push   %esi
  800c6e:	53                   	push   %ebx
  800c6f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c72:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c77:	b8 0d 00 00 00       	mov    $0xd,%eax
  800c7c:	8b 55 08             	mov    0x8(%ebp),%edx
  800c7f:	89 cb                	mov    %ecx,%ebx
  800c81:	89 cf                	mov    %ecx,%edi
  800c83:	89 ce                	mov    %ecx,%esi
  800c85:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c87:	85 c0                	test   %eax,%eax
  800c89:	7e 17                	jle    800ca2 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c8b:	83 ec 0c             	sub    $0xc,%esp
  800c8e:	50                   	push   %eax
  800c8f:	6a 0d                	push   $0xd
  800c91:	68 e4 12 80 00       	push   $0x8012e4
  800c96:	6a 23                	push   $0x23
  800c98:	68 01 13 80 00       	push   $0x801301
  800c9d:	e8 09 01 00 00       	call   800dab <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800ca2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ca5:	5b                   	pop    %ebx
  800ca6:	5e                   	pop    %esi
  800ca7:	5f                   	pop    %edi
  800ca8:	5d                   	pop    %ebp
  800ca9:	c3                   	ret    

00800caa <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  800caa:	55                   	push   %ebp
  800cab:	89 e5                	mov    %esp,%ebp
  800cad:	56                   	push   %esi
  800cae:	53                   	push   %ebx
  800caf:	8b 75 08             	mov    0x8(%ebp),%esi
  800cb2:	8b 45 0c             	mov    0xc(%ebp),%eax
  800cb5:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  800cb8:	85 c0                	test   %eax,%eax
  800cba:	75 05                	jne    800cc1 <ipc_recv+0x17>
        pg = (void *)UTOP;
  800cbc:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  800cc1:	83 ec 0c             	sub    $0xc,%esp
  800cc4:	50                   	push   %eax
  800cc5:	e8 9f ff ff ff       	call   800c69 <sys_ipc_recv>
  800cca:	83 c4 10             	add    $0x10,%esp
  800ccd:	85 c0                	test   %eax,%eax
  800ccf:	74 16                	je     800ce7 <ipc_recv+0x3d>
        if (from_env_store)
  800cd1:	85 f6                	test   %esi,%esi
  800cd3:	74 06                	je     800cdb <ipc_recv+0x31>
            *from_env_store = 0;
  800cd5:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  800cdb:	85 db                	test   %ebx,%ebx
  800cdd:	74 2c                	je     800d0b <ipc_recv+0x61>
            *perm_store = 0;
  800cdf:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800ce5:	eb 24                	jmp    800d0b <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  800ce7:	85 f6                	test   %esi,%esi
  800ce9:	74 0a                	je     800cf5 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  800ceb:	a1 04 20 80 00       	mov    0x802004,%eax
  800cf0:	8b 40 74             	mov    0x74(%eax),%eax
  800cf3:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  800cf5:	85 db                	test   %ebx,%ebx
  800cf7:	74 0a                	je     800d03 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  800cf9:	a1 04 20 80 00       	mov    0x802004,%eax
  800cfe:	8b 40 78             	mov    0x78(%eax),%eax
  800d01:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  800d03:	a1 04 20 80 00       	mov    0x802004,%eax
  800d08:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  800d0b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800d0e:	5b                   	pop    %ebx
  800d0f:	5e                   	pop    %esi
  800d10:	5d                   	pop    %ebp
  800d11:	c3                   	ret    

00800d12 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  800d12:	55                   	push   %ebp
  800d13:	89 e5                	mov    %esp,%ebp
  800d15:	57                   	push   %edi
  800d16:	56                   	push   %esi
  800d17:	53                   	push   %ebx
  800d18:	83 ec 0c             	sub    $0xc,%esp
  800d1b:	8b 75 0c             	mov    0xc(%ebp),%esi
  800d1e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d21:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  800d24:	85 db                	test   %ebx,%ebx
  800d26:	75 0c                	jne    800d34 <ipc_send+0x22>
        pg = (void *)UTOP;
  800d28:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  800d2d:	eb 05                	jmp    800d34 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  800d2f:	e8 89 fd ff ff       	call   800abd <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  800d34:	57                   	push   %edi
  800d35:	53                   	push   %ebx
  800d36:	56                   	push   %esi
  800d37:	ff 75 08             	pushl  0x8(%ebp)
  800d3a:	e8 07 ff ff ff       	call   800c46 <sys_ipc_try_send>
  800d3f:	83 c4 10             	add    $0x10,%esp
  800d42:	83 f8 f9             	cmp    $0xfffffff9,%eax
  800d45:	74 e8                	je     800d2f <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  800d47:	85 c0                	test   %eax,%eax
  800d49:	74 14                	je     800d5f <ipc_send+0x4d>
        panic("ipc_send: error");
  800d4b:	83 ec 04             	sub    $0x4,%esp
  800d4e:	68 0f 13 80 00       	push   $0x80130f
  800d53:	6a 53                	push   $0x53
  800d55:	68 1f 13 80 00       	push   $0x80131f
  800d5a:	e8 4c 00 00 00       	call   800dab <_panic>
    }
	//panic("ipc_send not implemented");
}
  800d5f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d62:	5b                   	pop    %ebx
  800d63:	5e                   	pop    %esi
  800d64:	5f                   	pop    %edi
  800d65:	5d                   	pop    %ebp
  800d66:	c3                   	ret    

00800d67 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  800d67:	55                   	push   %ebp
  800d68:	89 e5                	mov    %esp,%ebp
  800d6a:	53                   	push   %ebx
  800d6b:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  800d6e:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  800d73:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  800d7a:	89 d0                	mov    %edx,%eax
  800d7c:	c1 e0 07             	shl    $0x7,%eax
  800d7f:	29 d8                	sub    %ebx,%eax
  800d81:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800d86:	8b 40 50             	mov    0x50(%eax),%eax
  800d89:	39 c8                	cmp    %ecx,%eax
  800d8b:	75 0d                	jne    800d9a <ipc_find_env+0x33>
			return envs[i].env_id;
  800d8d:	c1 e2 07             	shl    $0x7,%edx
  800d90:	29 da                	sub    %ebx,%edx
  800d92:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  800d98:	eb 0e                	jmp    800da8 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  800d9a:	42                   	inc    %edx
  800d9b:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  800da1:	75 d0                	jne    800d73 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  800da3:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800da8:	5b                   	pop    %ebx
  800da9:	5d                   	pop    %ebp
  800daa:	c3                   	ret    

00800dab <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800dab:	55                   	push   %ebp
  800dac:	89 e5                	mov    %esp,%ebp
  800dae:	56                   	push   %esi
  800daf:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800db0:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800db3:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800db9:	e8 e0 fc ff ff       	call   800a9e <sys_getenvid>
  800dbe:	83 ec 0c             	sub    $0xc,%esp
  800dc1:	ff 75 0c             	pushl  0xc(%ebp)
  800dc4:	ff 75 08             	pushl  0x8(%ebp)
  800dc7:	56                   	push   %esi
  800dc8:	50                   	push   %eax
  800dc9:	68 2c 13 80 00       	push   $0x80132c
  800dce:	e8 c1 f3 ff ff       	call   800194 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800dd3:	83 c4 18             	add    $0x18,%esp
  800dd6:	53                   	push   %ebx
  800dd7:	ff 75 10             	pushl  0x10(%ebp)
  800dda:	e8 64 f3 ff ff       	call   800143 <vcprintf>
	cprintf("\n");
  800ddf:	c7 04 24 6f 10 80 00 	movl   $0x80106f,(%esp)
  800de6:	e8 a9 f3 ff ff       	call   800194 <cprintf>
  800deb:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800dee:	cc                   	int3   
  800def:	eb fd                	jmp    800dee <_panic+0x43>
  800df1:	66 90                	xchg   %ax,%ax
  800df3:	90                   	nop

00800df4 <__udivdi3>:
  800df4:	55                   	push   %ebp
  800df5:	57                   	push   %edi
  800df6:	56                   	push   %esi
  800df7:	53                   	push   %ebx
  800df8:	83 ec 1c             	sub    $0x1c,%esp
  800dfb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800dff:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800e03:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e07:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800e0b:	89 ca                	mov    %ecx,%edx
  800e0d:	89 f8                	mov    %edi,%eax
  800e0f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800e13:	85 f6                	test   %esi,%esi
  800e15:	75 2d                	jne    800e44 <__udivdi3+0x50>
  800e17:	39 cf                	cmp    %ecx,%edi
  800e19:	77 65                	ja     800e80 <__udivdi3+0x8c>
  800e1b:	89 fd                	mov    %edi,%ebp
  800e1d:	85 ff                	test   %edi,%edi
  800e1f:	75 0b                	jne    800e2c <__udivdi3+0x38>
  800e21:	b8 01 00 00 00       	mov    $0x1,%eax
  800e26:	31 d2                	xor    %edx,%edx
  800e28:	f7 f7                	div    %edi
  800e2a:	89 c5                	mov    %eax,%ebp
  800e2c:	31 d2                	xor    %edx,%edx
  800e2e:	89 c8                	mov    %ecx,%eax
  800e30:	f7 f5                	div    %ebp
  800e32:	89 c1                	mov    %eax,%ecx
  800e34:	89 d8                	mov    %ebx,%eax
  800e36:	f7 f5                	div    %ebp
  800e38:	89 cf                	mov    %ecx,%edi
  800e3a:	89 fa                	mov    %edi,%edx
  800e3c:	83 c4 1c             	add    $0x1c,%esp
  800e3f:	5b                   	pop    %ebx
  800e40:	5e                   	pop    %esi
  800e41:	5f                   	pop    %edi
  800e42:	5d                   	pop    %ebp
  800e43:	c3                   	ret    
  800e44:	39 ce                	cmp    %ecx,%esi
  800e46:	77 28                	ja     800e70 <__udivdi3+0x7c>
  800e48:	0f bd fe             	bsr    %esi,%edi
  800e4b:	83 f7 1f             	xor    $0x1f,%edi
  800e4e:	75 40                	jne    800e90 <__udivdi3+0x9c>
  800e50:	39 ce                	cmp    %ecx,%esi
  800e52:	72 0a                	jb     800e5e <__udivdi3+0x6a>
  800e54:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800e58:	0f 87 9e 00 00 00    	ja     800efc <__udivdi3+0x108>
  800e5e:	b8 01 00 00 00       	mov    $0x1,%eax
  800e63:	89 fa                	mov    %edi,%edx
  800e65:	83 c4 1c             	add    $0x1c,%esp
  800e68:	5b                   	pop    %ebx
  800e69:	5e                   	pop    %esi
  800e6a:	5f                   	pop    %edi
  800e6b:	5d                   	pop    %ebp
  800e6c:	c3                   	ret    
  800e6d:	8d 76 00             	lea    0x0(%esi),%esi
  800e70:	31 ff                	xor    %edi,%edi
  800e72:	31 c0                	xor    %eax,%eax
  800e74:	89 fa                	mov    %edi,%edx
  800e76:	83 c4 1c             	add    $0x1c,%esp
  800e79:	5b                   	pop    %ebx
  800e7a:	5e                   	pop    %esi
  800e7b:	5f                   	pop    %edi
  800e7c:	5d                   	pop    %ebp
  800e7d:	c3                   	ret    
  800e7e:	66 90                	xchg   %ax,%ax
  800e80:	89 d8                	mov    %ebx,%eax
  800e82:	f7 f7                	div    %edi
  800e84:	31 ff                	xor    %edi,%edi
  800e86:	89 fa                	mov    %edi,%edx
  800e88:	83 c4 1c             	add    $0x1c,%esp
  800e8b:	5b                   	pop    %ebx
  800e8c:	5e                   	pop    %esi
  800e8d:	5f                   	pop    %edi
  800e8e:	5d                   	pop    %ebp
  800e8f:	c3                   	ret    
  800e90:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e95:	89 eb                	mov    %ebp,%ebx
  800e97:	29 fb                	sub    %edi,%ebx
  800e99:	89 f9                	mov    %edi,%ecx
  800e9b:	d3 e6                	shl    %cl,%esi
  800e9d:	89 c5                	mov    %eax,%ebp
  800e9f:	88 d9                	mov    %bl,%cl
  800ea1:	d3 ed                	shr    %cl,%ebp
  800ea3:	89 e9                	mov    %ebp,%ecx
  800ea5:	09 f1                	or     %esi,%ecx
  800ea7:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800eab:	89 f9                	mov    %edi,%ecx
  800ead:	d3 e0                	shl    %cl,%eax
  800eaf:	89 c5                	mov    %eax,%ebp
  800eb1:	89 d6                	mov    %edx,%esi
  800eb3:	88 d9                	mov    %bl,%cl
  800eb5:	d3 ee                	shr    %cl,%esi
  800eb7:	89 f9                	mov    %edi,%ecx
  800eb9:	d3 e2                	shl    %cl,%edx
  800ebb:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ebf:	88 d9                	mov    %bl,%cl
  800ec1:	d3 e8                	shr    %cl,%eax
  800ec3:	09 c2                	or     %eax,%edx
  800ec5:	89 d0                	mov    %edx,%eax
  800ec7:	89 f2                	mov    %esi,%edx
  800ec9:	f7 74 24 0c          	divl   0xc(%esp)
  800ecd:	89 d6                	mov    %edx,%esi
  800ecf:	89 c3                	mov    %eax,%ebx
  800ed1:	f7 e5                	mul    %ebp
  800ed3:	39 d6                	cmp    %edx,%esi
  800ed5:	72 19                	jb     800ef0 <__udivdi3+0xfc>
  800ed7:	74 0b                	je     800ee4 <__udivdi3+0xf0>
  800ed9:	89 d8                	mov    %ebx,%eax
  800edb:	31 ff                	xor    %edi,%edi
  800edd:	e9 58 ff ff ff       	jmp    800e3a <__udivdi3+0x46>
  800ee2:	66 90                	xchg   %ax,%ax
  800ee4:	8b 54 24 08          	mov    0x8(%esp),%edx
  800ee8:	89 f9                	mov    %edi,%ecx
  800eea:	d3 e2                	shl    %cl,%edx
  800eec:	39 c2                	cmp    %eax,%edx
  800eee:	73 e9                	jae    800ed9 <__udivdi3+0xe5>
  800ef0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ef3:	31 ff                	xor    %edi,%edi
  800ef5:	e9 40 ff ff ff       	jmp    800e3a <__udivdi3+0x46>
  800efa:	66 90                	xchg   %ax,%ax
  800efc:	31 c0                	xor    %eax,%eax
  800efe:	e9 37 ff ff ff       	jmp    800e3a <__udivdi3+0x46>
  800f03:	90                   	nop

00800f04 <__umoddi3>:
  800f04:	55                   	push   %ebp
  800f05:	57                   	push   %edi
  800f06:	56                   	push   %esi
  800f07:	53                   	push   %ebx
  800f08:	83 ec 1c             	sub    $0x1c,%esp
  800f0b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800f0f:	8b 74 24 34          	mov    0x34(%esp),%esi
  800f13:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800f17:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800f1b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800f1f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800f23:	89 f3                	mov    %esi,%ebx
  800f25:	89 fa                	mov    %edi,%edx
  800f27:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f2b:	89 34 24             	mov    %esi,(%esp)
  800f2e:	85 c0                	test   %eax,%eax
  800f30:	75 1a                	jne    800f4c <__umoddi3+0x48>
  800f32:	39 f7                	cmp    %esi,%edi
  800f34:	0f 86 a2 00 00 00    	jbe    800fdc <__umoddi3+0xd8>
  800f3a:	89 c8                	mov    %ecx,%eax
  800f3c:	89 f2                	mov    %esi,%edx
  800f3e:	f7 f7                	div    %edi
  800f40:	89 d0                	mov    %edx,%eax
  800f42:	31 d2                	xor    %edx,%edx
  800f44:	83 c4 1c             	add    $0x1c,%esp
  800f47:	5b                   	pop    %ebx
  800f48:	5e                   	pop    %esi
  800f49:	5f                   	pop    %edi
  800f4a:	5d                   	pop    %ebp
  800f4b:	c3                   	ret    
  800f4c:	39 f0                	cmp    %esi,%eax
  800f4e:	0f 87 ac 00 00 00    	ja     801000 <__umoddi3+0xfc>
  800f54:	0f bd e8             	bsr    %eax,%ebp
  800f57:	83 f5 1f             	xor    $0x1f,%ebp
  800f5a:	0f 84 ac 00 00 00    	je     80100c <__umoddi3+0x108>
  800f60:	bf 20 00 00 00       	mov    $0x20,%edi
  800f65:	29 ef                	sub    %ebp,%edi
  800f67:	89 fe                	mov    %edi,%esi
  800f69:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800f6d:	89 e9                	mov    %ebp,%ecx
  800f6f:	d3 e0                	shl    %cl,%eax
  800f71:	89 d7                	mov    %edx,%edi
  800f73:	89 f1                	mov    %esi,%ecx
  800f75:	d3 ef                	shr    %cl,%edi
  800f77:	09 c7                	or     %eax,%edi
  800f79:	89 e9                	mov    %ebp,%ecx
  800f7b:	d3 e2                	shl    %cl,%edx
  800f7d:	89 14 24             	mov    %edx,(%esp)
  800f80:	89 d8                	mov    %ebx,%eax
  800f82:	d3 e0                	shl    %cl,%eax
  800f84:	89 c2                	mov    %eax,%edx
  800f86:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f8a:	d3 e0                	shl    %cl,%eax
  800f8c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f90:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f94:	89 f1                	mov    %esi,%ecx
  800f96:	d3 e8                	shr    %cl,%eax
  800f98:	09 d0                	or     %edx,%eax
  800f9a:	d3 eb                	shr    %cl,%ebx
  800f9c:	89 da                	mov    %ebx,%edx
  800f9e:	f7 f7                	div    %edi
  800fa0:	89 d3                	mov    %edx,%ebx
  800fa2:	f7 24 24             	mull   (%esp)
  800fa5:	89 c6                	mov    %eax,%esi
  800fa7:	89 d1                	mov    %edx,%ecx
  800fa9:	39 d3                	cmp    %edx,%ebx
  800fab:	0f 82 87 00 00 00    	jb     801038 <__umoddi3+0x134>
  800fb1:	0f 84 91 00 00 00    	je     801048 <__umoddi3+0x144>
  800fb7:	8b 54 24 04          	mov    0x4(%esp),%edx
  800fbb:	29 f2                	sub    %esi,%edx
  800fbd:	19 cb                	sbb    %ecx,%ebx
  800fbf:	89 d8                	mov    %ebx,%eax
  800fc1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800fc5:	d3 e0                	shl    %cl,%eax
  800fc7:	89 e9                	mov    %ebp,%ecx
  800fc9:	d3 ea                	shr    %cl,%edx
  800fcb:	09 d0                	or     %edx,%eax
  800fcd:	89 e9                	mov    %ebp,%ecx
  800fcf:	d3 eb                	shr    %cl,%ebx
  800fd1:	89 da                	mov    %ebx,%edx
  800fd3:	83 c4 1c             	add    $0x1c,%esp
  800fd6:	5b                   	pop    %ebx
  800fd7:	5e                   	pop    %esi
  800fd8:	5f                   	pop    %edi
  800fd9:	5d                   	pop    %ebp
  800fda:	c3                   	ret    
  800fdb:	90                   	nop
  800fdc:	89 fd                	mov    %edi,%ebp
  800fde:	85 ff                	test   %edi,%edi
  800fe0:	75 0b                	jne    800fed <__umoddi3+0xe9>
  800fe2:	b8 01 00 00 00       	mov    $0x1,%eax
  800fe7:	31 d2                	xor    %edx,%edx
  800fe9:	f7 f7                	div    %edi
  800feb:	89 c5                	mov    %eax,%ebp
  800fed:	89 f0                	mov    %esi,%eax
  800fef:	31 d2                	xor    %edx,%edx
  800ff1:	f7 f5                	div    %ebp
  800ff3:	89 c8                	mov    %ecx,%eax
  800ff5:	f7 f5                	div    %ebp
  800ff7:	89 d0                	mov    %edx,%eax
  800ff9:	e9 44 ff ff ff       	jmp    800f42 <__umoddi3+0x3e>
  800ffe:	66 90                	xchg   %ax,%ax
  801000:	89 c8                	mov    %ecx,%eax
  801002:	89 f2                	mov    %esi,%edx
  801004:	83 c4 1c             	add    $0x1c,%esp
  801007:	5b                   	pop    %ebx
  801008:	5e                   	pop    %esi
  801009:	5f                   	pop    %edi
  80100a:	5d                   	pop    %ebp
  80100b:	c3                   	ret    
  80100c:	3b 04 24             	cmp    (%esp),%eax
  80100f:	72 06                	jb     801017 <__umoddi3+0x113>
  801011:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801015:	77 0f                	ja     801026 <__umoddi3+0x122>
  801017:	89 f2                	mov    %esi,%edx
  801019:	29 f9                	sub    %edi,%ecx
  80101b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  80101f:	89 14 24             	mov    %edx,(%esp)
  801022:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801026:	8b 44 24 04          	mov    0x4(%esp),%eax
  80102a:	8b 14 24             	mov    (%esp),%edx
  80102d:	83 c4 1c             	add    $0x1c,%esp
  801030:	5b                   	pop    %ebx
  801031:	5e                   	pop    %esi
  801032:	5f                   	pop    %edi
  801033:	5d                   	pop    %ebp
  801034:	c3                   	ret    
  801035:	8d 76 00             	lea    0x0(%esi),%esi
  801038:	2b 04 24             	sub    (%esp),%eax
  80103b:	19 fa                	sbb    %edi,%edx
  80103d:	89 d1                	mov    %edx,%ecx
  80103f:	89 c6                	mov    %eax,%esi
  801041:	e9 71 ff ff ff       	jmp    800fb7 <__umoddi3+0xb3>
  801046:	66 90                	xchg   %ax,%ax
  801048:	39 44 24 04          	cmp    %eax,0x4(%esp)
  80104c:	72 ea                	jb     801038 <__umoddi3+0x134>
  80104e:	89 d9                	mov    %ebx,%ecx
  801050:	e9 62 ff ff ff       	jmp    800fb7 <__umoddi3+0xb3>
