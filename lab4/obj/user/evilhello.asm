
obj/user/evilhello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 19 00 00 00       	call   80004a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	// try to print the kernel entry point as a string!  mua ha ha!
	sys_cputs((char*)0xf010000c, 100);
  800039:	6a 64                	push   $0x64
  80003b:	68 0c 00 10 f0       	push   $0xf010000c
  800040:	e8 66 00 00 00       	call   8000ab <sys_cputs>
}
  800045:	83 c4 10             	add    $0x10,%esp
  800048:	c9                   	leave  
  800049:	c3                   	ret    

0080004a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004a:	55                   	push   %ebp
  80004b:	89 e5                	mov    %esp,%ebp
  80004d:	56                   	push   %esi
  80004e:	53                   	push   %ebx
  80004f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800052:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800055:	e8 cf 00 00 00       	call   800129 <sys_getenvid>
  80005a:	25 ff 03 00 00       	and    $0x3ff,%eax
  80005f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800066:	c1 e0 07             	shl    $0x7,%eax
  800069:	29 d0                	sub    %edx,%eax
  80006b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800070:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800075:	85 db                	test   %ebx,%ebx
  800077:	7e 07                	jle    800080 <libmain+0x36>
		binaryname = argv[0];
  800079:	8b 06                	mov    (%esi),%eax
  80007b:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800080:	83 ec 08             	sub    $0x8,%esp
  800083:	56                   	push   %esi
  800084:	53                   	push   %ebx
  800085:	e8 a9 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008a:	e8 0a 00 00 00       	call   800099 <exit>
}
  80008f:	83 c4 10             	add    $0x10,%esp
  800092:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800095:	5b                   	pop    %ebx
  800096:	5e                   	pop    %esi
  800097:	5d                   	pop    %ebp
  800098:	c3                   	ret    

00800099 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800099:	55                   	push   %ebp
  80009a:	89 e5                	mov    %esp,%ebp
  80009c:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80009f:	6a 00                	push   $0x0
  8000a1:	e8 42 00 00 00       	call   8000e8 <sys_env_destroy>
}
  8000a6:	83 c4 10             	add    $0x10,%esp
  8000a9:	c9                   	leave  
  8000aa:	c3                   	ret    

008000ab <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000ab:	55                   	push   %ebp
  8000ac:	89 e5                	mov    %esp,%ebp
  8000ae:	57                   	push   %edi
  8000af:	56                   	push   %esi
  8000b0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000b1:	b8 00 00 00 00       	mov    $0x0,%eax
  8000b6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b9:	8b 55 08             	mov    0x8(%ebp),%edx
  8000bc:	89 c3                	mov    %eax,%ebx
  8000be:	89 c7                	mov    %eax,%edi
  8000c0:	89 c6                	mov    %eax,%esi
  8000c2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c4:	5b                   	pop    %ebx
  8000c5:	5e                   	pop    %esi
  8000c6:	5f                   	pop    %edi
  8000c7:	5d                   	pop    %ebp
  8000c8:	c3                   	ret    

008000c9 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c9:	55                   	push   %ebp
  8000ca:	89 e5                	mov    %esp,%ebp
  8000cc:	57                   	push   %edi
  8000cd:	56                   	push   %esi
  8000ce:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d4:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d9:	89 d1                	mov    %edx,%ecx
  8000db:	89 d3                	mov    %edx,%ebx
  8000dd:	89 d7                	mov    %edx,%edi
  8000df:	89 d6                	mov    %edx,%esi
  8000e1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e3:	5b                   	pop    %ebx
  8000e4:	5e                   	pop    %esi
  8000e5:	5f                   	pop    %edi
  8000e6:	5d                   	pop    %ebp
  8000e7:	c3                   	ret    

008000e8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e8:	55                   	push   %ebp
  8000e9:	89 e5                	mov    %esp,%ebp
  8000eb:	57                   	push   %edi
  8000ec:	56                   	push   %esi
  8000ed:	53                   	push   %ebx
  8000ee:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000f1:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000f6:	b8 03 00 00 00       	mov    $0x3,%eax
  8000fb:	8b 55 08             	mov    0x8(%ebp),%edx
  8000fe:	89 cb                	mov    %ecx,%ebx
  800100:	89 cf                	mov    %ecx,%edi
  800102:	89 ce                	mov    %ecx,%esi
  800104:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800106:	85 c0                	test   %eax,%eax
  800108:	7e 17                	jle    800121 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80010a:	83 ec 0c             	sub    $0xc,%esp
  80010d:	50                   	push   %eax
  80010e:	6a 03                	push   $0x3
  800110:	68 0a 0f 80 00       	push   $0x800f0a
  800115:	6a 23                	push   $0x23
  800117:	68 27 0f 80 00       	push   $0x800f27
  80011c:	e8 14 02 00 00       	call   800335 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800121:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800124:	5b                   	pop    %ebx
  800125:	5e                   	pop    %esi
  800126:	5f                   	pop    %edi
  800127:	5d                   	pop    %ebp
  800128:	c3                   	ret    

00800129 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800129:	55                   	push   %ebp
  80012a:	89 e5                	mov    %esp,%ebp
  80012c:	57                   	push   %edi
  80012d:	56                   	push   %esi
  80012e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012f:	ba 00 00 00 00       	mov    $0x0,%edx
  800134:	b8 02 00 00 00       	mov    $0x2,%eax
  800139:	89 d1                	mov    %edx,%ecx
  80013b:	89 d3                	mov    %edx,%ebx
  80013d:	89 d7                	mov    %edx,%edi
  80013f:	89 d6                	mov    %edx,%esi
  800141:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800143:	5b                   	pop    %ebx
  800144:	5e                   	pop    %esi
  800145:	5f                   	pop    %edi
  800146:	5d                   	pop    %ebp
  800147:	c3                   	ret    

00800148 <sys_yield>:

void
sys_yield(void)
{
  800148:	55                   	push   %ebp
  800149:	89 e5                	mov    %esp,%ebp
  80014b:	57                   	push   %edi
  80014c:	56                   	push   %esi
  80014d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80014e:	ba 00 00 00 00       	mov    $0x0,%edx
  800153:	b8 0a 00 00 00       	mov    $0xa,%eax
  800158:	89 d1                	mov    %edx,%ecx
  80015a:	89 d3                	mov    %edx,%ebx
  80015c:	89 d7                	mov    %edx,%edi
  80015e:	89 d6                	mov    %edx,%esi
  800160:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800162:	5b                   	pop    %ebx
  800163:	5e                   	pop    %esi
  800164:	5f                   	pop    %edi
  800165:	5d                   	pop    %ebp
  800166:	c3                   	ret    

00800167 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800167:	55                   	push   %ebp
  800168:	89 e5                	mov    %esp,%ebp
  80016a:	57                   	push   %edi
  80016b:	56                   	push   %esi
  80016c:	53                   	push   %ebx
  80016d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800170:	be 00 00 00 00       	mov    $0x0,%esi
  800175:	b8 04 00 00 00       	mov    $0x4,%eax
  80017a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80017d:	8b 55 08             	mov    0x8(%ebp),%edx
  800180:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800183:	89 f7                	mov    %esi,%edi
  800185:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800187:	85 c0                	test   %eax,%eax
  800189:	7e 17                	jle    8001a2 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80018b:	83 ec 0c             	sub    $0xc,%esp
  80018e:	50                   	push   %eax
  80018f:	6a 04                	push   $0x4
  800191:	68 0a 0f 80 00       	push   $0x800f0a
  800196:	6a 23                	push   $0x23
  800198:	68 27 0f 80 00       	push   $0x800f27
  80019d:	e8 93 01 00 00       	call   800335 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001a2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a5:	5b                   	pop    %ebx
  8001a6:	5e                   	pop    %esi
  8001a7:	5f                   	pop    %edi
  8001a8:	5d                   	pop    %ebp
  8001a9:	c3                   	ret    

008001aa <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001aa:	55                   	push   %ebp
  8001ab:	89 e5                	mov    %esp,%ebp
  8001ad:	57                   	push   %edi
  8001ae:	56                   	push   %esi
  8001af:	53                   	push   %ebx
  8001b0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001b3:	b8 05 00 00 00       	mov    $0x5,%eax
  8001b8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001bb:	8b 55 08             	mov    0x8(%ebp),%edx
  8001be:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001c1:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001c4:	8b 75 18             	mov    0x18(%ebp),%esi
  8001c7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c9:	85 c0                	test   %eax,%eax
  8001cb:	7e 17                	jle    8001e4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001cd:	83 ec 0c             	sub    $0xc,%esp
  8001d0:	50                   	push   %eax
  8001d1:	6a 05                	push   $0x5
  8001d3:	68 0a 0f 80 00       	push   $0x800f0a
  8001d8:	6a 23                	push   $0x23
  8001da:	68 27 0f 80 00       	push   $0x800f27
  8001df:	e8 51 01 00 00       	call   800335 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001e4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001e7:	5b                   	pop    %ebx
  8001e8:	5e                   	pop    %esi
  8001e9:	5f                   	pop    %edi
  8001ea:	5d                   	pop    %ebp
  8001eb:	c3                   	ret    

008001ec <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001ec:	55                   	push   %ebp
  8001ed:	89 e5                	mov    %esp,%ebp
  8001ef:	57                   	push   %edi
  8001f0:	56                   	push   %esi
  8001f1:	53                   	push   %ebx
  8001f2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001f5:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001fa:	b8 06 00 00 00       	mov    $0x6,%eax
  8001ff:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800202:	8b 55 08             	mov    0x8(%ebp),%edx
  800205:	89 df                	mov    %ebx,%edi
  800207:	89 de                	mov    %ebx,%esi
  800209:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80020b:	85 c0                	test   %eax,%eax
  80020d:	7e 17                	jle    800226 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80020f:	83 ec 0c             	sub    $0xc,%esp
  800212:	50                   	push   %eax
  800213:	6a 06                	push   $0x6
  800215:	68 0a 0f 80 00       	push   $0x800f0a
  80021a:	6a 23                	push   $0x23
  80021c:	68 27 0f 80 00       	push   $0x800f27
  800221:	e8 0f 01 00 00       	call   800335 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800226:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800229:	5b                   	pop    %ebx
  80022a:	5e                   	pop    %esi
  80022b:	5f                   	pop    %edi
  80022c:	5d                   	pop    %ebp
  80022d:	c3                   	ret    

0080022e <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80022e:	55                   	push   %ebp
  80022f:	89 e5                	mov    %esp,%ebp
  800231:	57                   	push   %edi
  800232:	56                   	push   %esi
  800233:	53                   	push   %ebx
  800234:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800237:	bb 00 00 00 00       	mov    $0x0,%ebx
  80023c:	b8 08 00 00 00       	mov    $0x8,%eax
  800241:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800244:	8b 55 08             	mov    0x8(%ebp),%edx
  800247:	89 df                	mov    %ebx,%edi
  800249:	89 de                	mov    %ebx,%esi
  80024b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80024d:	85 c0                	test   %eax,%eax
  80024f:	7e 17                	jle    800268 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800251:	83 ec 0c             	sub    $0xc,%esp
  800254:	50                   	push   %eax
  800255:	6a 08                	push   $0x8
  800257:	68 0a 0f 80 00       	push   $0x800f0a
  80025c:	6a 23                	push   $0x23
  80025e:	68 27 0f 80 00       	push   $0x800f27
  800263:	e8 cd 00 00 00       	call   800335 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800268:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80026b:	5b                   	pop    %ebx
  80026c:	5e                   	pop    %esi
  80026d:	5f                   	pop    %edi
  80026e:	5d                   	pop    %ebp
  80026f:	c3                   	ret    

00800270 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800270:	55                   	push   %ebp
  800271:	89 e5                	mov    %esp,%ebp
  800273:	57                   	push   %edi
  800274:	56                   	push   %esi
  800275:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800276:	ba 00 00 00 00       	mov    $0x0,%edx
  80027b:	b8 0b 00 00 00       	mov    $0xb,%eax
  800280:	89 d1                	mov    %edx,%ecx
  800282:	89 d3                	mov    %edx,%ebx
  800284:	89 d7                	mov    %edx,%edi
  800286:	89 d6                	mov    %edx,%esi
  800288:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80028a:	5b                   	pop    %ebx
  80028b:	5e                   	pop    %esi
  80028c:	5f                   	pop    %edi
  80028d:	5d                   	pop    %ebp
  80028e:	c3                   	ret    

0080028f <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80028f:	55                   	push   %ebp
  800290:	89 e5                	mov    %esp,%ebp
  800292:	57                   	push   %edi
  800293:	56                   	push   %esi
  800294:	53                   	push   %ebx
  800295:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800298:	bb 00 00 00 00       	mov    $0x0,%ebx
  80029d:	b8 09 00 00 00       	mov    $0x9,%eax
  8002a2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002a5:	8b 55 08             	mov    0x8(%ebp),%edx
  8002a8:	89 df                	mov    %ebx,%edi
  8002aa:	89 de                	mov    %ebx,%esi
  8002ac:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002ae:	85 c0                	test   %eax,%eax
  8002b0:	7e 17                	jle    8002c9 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002b2:	83 ec 0c             	sub    $0xc,%esp
  8002b5:	50                   	push   %eax
  8002b6:	6a 09                	push   $0x9
  8002b8:	68 0a 0f 80 00       	push   $0x800f0a
  8002bd:	6a 23                	push   $0x23
  8002bf:	68 27 0f 80 00       	push   $0x800f27
  8002c4:	e8 6c 00 00 00       	call   800335 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002cc:	5b                   	pop    %ebx
  8002cd:	5e                   	pop    %esi
  8002ce:	5f                   	pop    %edi
  8002cf:	5d                   	pop    %ebp
  8002d0:	c3                   	ret    

008002d1 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002d1:	55                   	push   %ebp
  8002d2:	89 e5                	mov    %esp,%ebp
  8002d4:	57                   	push   %edi
  8002d5:	56                   	push   %esi
  8002d6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002d7:	be 00 00 00 00       	mov    $0x0,%esi
  8002dc:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002e1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002e4:	8b 55 08             	mov    0x8(%ebp),%edx
  8002e7:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002ea:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002ed:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002ef:	5b                   	pop    %ebx
  8002f0:	5e                   	pop    %esi
  8002f1:	5f                   	pop    %edi
  8002f2:	5d                   	pop    %ebp
  8002f3:	c3                   	ret    

008002f4 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002f4:	55                   	push   %ebp
  8002f5:	89 e5                	mov    %esp,%ebp
  8002f7:	57                   	push   %edi
  8002f8:	56                   	push   %esi
  8002f9:	53                   	push   %ebx
  8002fa:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002fd:	b9 00 00 00 00       	mov    $0x0,%ecx
  800302:	b8 0d 00 00 00       	mov    $0xd,%eax
  800307:	8b 55 08             	mov    0x8(%ebp),%edx
  80030a:	89 cb                	mov    %ecx,%ebx
  80030c:	89 cf                	mov    %ecx,%edi
  80030e:	89 ce                	mov    %ecx,%esi
  800310:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800312:	85 c0                	test   %eax,%eax
  800314:	7e 17                	jle    80032d <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800316:	83 ec 0c             	sub    $0xc,%esp
  800319:	50                   	push   %eax
  80031a:	6a 0d                	push   $0xd
  80031c:	68 0a 0f 80 00       	push   $0x800f0a
  800321:	6a 23                	push   $0x23
  800323:	68 27 0f 80 00       	push   $0x800f27
  800328:	e8 08 00 00 00       	call   800335 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80032d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800330:	5b                   	pop    %ebx
  800331:	5e                   	pop    %esi
  800332:	5f                   	pop    %edi
  800333:	5d                   	pop    %ebp
  800334:	c3                   	ret    

00800335 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800335:	55                   	push   %ebp
  800336:	89 e5                	mov    %esp,%ebp
  800338:	56                   	push   %esi
  800339:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80033a:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80033d:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800343:	e8 e1 fd ff ff       	call   800129 <sys_getenvid>
  800348:	83 ec 0c             	sub    $0xc,%esp
  80034b:	ff 75 0c             	pushl  0xc(%ebp)
  80034e:	ff 75 08             	pushl  0x8(%ebp)
  800351:	56                   	push   %esi
  800352:	50                   	push   %eax
  800353:	68 38 0f 80 00       	push   $0x800f38
  800358:	e8 b0 00 00 00       	call   80040d <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80035d:	83 c4 18             	add    $0x18,%esp
  800360:	53                   	push   %ebx
  800361:	ff 75 10             	pushl  0x10(%ebp)
  800364:	e8 53 00 00 00       	call   8003bc <vcprintf>
	cprintf("\n");
  800369:	c7 04 24 5c 0f 80 00 	movl   $0x800f5c,(%esp)
  800370:	e8 98 00 00 00       	call   80040d <cprintf>
  800375:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800378:	cc                   	int3   
  800379:	eb fd                	jmp    800378 <_panic+0x43>

0080037b <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80037b:	55                   	push   %ebp
  80037c:	89 e5                	mov    %esp,%ebp
  80037e:	53                   	push   %ebx
  80037f:	83 ec 04             	sub    $0x4,%esp
  800382:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800385:	8b 13                	mov    (%ebx),%edx
  800387:	8d 42 01             	lea    0x1(%edx),%eax
  80038a:	89 03                	mov    %eax,(%ebx)
  80038c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80038f:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800393:	3d ff 00 00 00       	cmp    $0xff,%eax
  800398:	75 1a                	jne    8003b4 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80039a:	83 ec 08             	sub    $0x8,%esp
  80039d:	68 ff 00 00 00       	push   $0xff
  8003a2:	8d 43 08             	lea    0x8(%ebx),%eax
  8003a5:	50                   	push   %eax
  8003a6:	e8 00 fd ff ff       	call   8000ab <sys_cputs>
		b->idx = 0;
  8003ab:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003b1:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003b4:	ff 43 04             	incl   0x4(%ebx)
}
  8003b7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003ba:	c9                   	leave  
  8003bb:	c3                   	ret    

008003bc <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003bc:	55                   	push   %ebp
  8003bd:	89 e5                	mov    %esp,%ebp
  8003bf:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003c5:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003cc:	00 00 00 
	b.cnt = 0;
  8003cf:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003d6:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003d9:	ff 75 0c             	pushl  0xc(%ebp)
  8003dc:	ff 75 08             	pushl  0x8(%ebp)
  8003df:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003e5:	50                   	push   %eax
  8003e6:	68 7b 03 80 00       	push   $0x80037b
  8003eb:	e8 51 01 00 00       	call   800541 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003f0:	83 c4 08             	add    $0x8,%esp
  8003f3:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003f9:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003ff:	50                   	push   %eax
  800400:	e8 a6 fc ff ff       	call   8000ab <sys_cputs>

	return b.cnt;
}
  800405:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80040b:	c9                   	leave  
  80040c:	c3                   	ret    

0080040d <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80040d:	55                   	push   %ebp
  80040e:	89 e5                	mov    %esp,%ebp
  800410:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800413:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800416:	50                   	push   %eax
  800417:	ff 75 08             	pushl  0x8(%ebp)
  80041a:	e8 9d ff ff ff       	call   8003bc <vcprintf>
	va_end(ap);

	return cnt;
}
  80041f:	c9                   	leave  
  800420:	c3                   	ret    

00800421 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800421:	55                   	push   %ebp
  800422:	89 e5                	mov    %esp,%ebp
  800424:	57                   	push   %edi
  800425:	56                   	push   %esi
  800426:	53                   	push   %ebx
  800427:	83 ec 1c             	sub    $0x1c,%esp
  80042a:	89 c7                	mov    %eax,%edi
  80042c:	89 d6                	mov    %edx,%esi
  80042e:	8b 45 08             	mov    0x8(%ebp),%eax
  800431:	8b 55 0c             	mov    0xc(%ebp),%edx
  800434:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800437:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80043a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80043d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800442:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800445:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800448:	39 d3                	cmp    %edx,%ebx
  80044a:	72 05                	jb     800451 <printnum+0x30>
  80044c:	39 45 10             	cmp    %eax,0x10(%ebp)
  80044f:	77 45                	ja     800496 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800451:	83 ec 0c             	sub    $0xc,%esp
  800454:	ff 75 18             	pushl  0x18(%ebp)
  800457:	8b 45 14             	mov    0x14(%ebp),%eax
  80045a:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80045d:	53                   	push   %ebx
  80045e:	ff 75 10             	pushl  0x10(%ebp)
  800461:	83 ec 08             	sub    $0x8,%esp
  800464:	ff 75 e4             	pushl  -0x1c(%ebp)
  800467:	ff 75 e0             	pushl  -0x20(%ebp)
  80046a:	ff 75 dc             	pushl  -0x24(%ebp)
  80046d:	ff 75 d8             	pushl  -0x28(%ebp)
  800470:	e8 27 08 00 00       	call   800c9c <__udivdi3>
  800475:	83 c4 18             	add    $0x18,%esp
  800478:	52                   	push   %edx
  800479:	50                   	push   %eax
  80047a:	89 f2                	mov    %esi,%edx
  80047c:	89 f8                	mov    %edi,%eax
  80047e:	e8 9e ff ff ff       	call   800421 <printnum>
  800483:	83 c4 20             	add    $0x20,%esp
  800486:	eb 16                	jmp    80049e <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800488:	83 ec 08             	sub    $0x8,%esp
  80048b:	56                   	push   %esi
  80048c:	ff 75 18             	pushl  0x18(%ebp)
  80048f:	ff d7                	call   *%edi
  800491:	83 c4 10             	add    $0x10,%esp
  800494:	eb 03                	jmp    800499 <printnum+0x78>
  800496:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800499:	4b                   	dec    %ebx
  80049a:	85 db                	test   %ebx,%ebx
  80049c:	7f ea                	jg     800488 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80049e:	83 ec 08             	sub    $0x8,%esp
  8004a1:	56                   	push   %esi
  8004a2:	83 ec 04             	sub    $0x4,%esp
  8004a5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a8:	ff 75 e0             	pushl  -0x20(%ebp)
  8004ab:	ff 75 dc             	pushl  -0x24(%ebp)
  8004ae:	ff 75 d8             	pushl  -0x28(%ebp)
  8004b1:	e8 f6 08 00 00       	call   800dac <__umoddi3>
  8004b6:	83 c4 14             	add    $0x14,%esp
  8004b9:	0f be 80 5e 0f 80 00 	movsbl 0x800f5e(%eax),%eax
  8004c0:	50                   	push   %eax
  8004c1:	ff d7                	call   *%edi
}
  8004c3:	83 c4 10             	add    $0x10,%esp
  8004c6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004c9:	5b                   	pop    %ebx
  8004ca:	5e                   	pop    %esi
  8004cb:	5f                   	pop    %edi
  8004cc:	5d                   	pop    %ebp
  8004cd:	c3                   	ret    

008004ce <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004ce:	55                   	push   %ebp
  8004cf:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004d1:	83 fa 01             	cmp    $0x1,%edx
  8004d4:	7e 0e                	jle    8004e4 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004d6:	8b 10                	mov    (%eax),%edx
  8004d8:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004db:	89 08                	mov    %ecx,(%eax)
  8004dd:	8b 02                	mov    (%edx),%eax
  8004df:	8b 52 04             	mov    0x4(%edx),%edx
  8004e2:	eb 22                	jmp    800506 <getuint+0x38>
	else if (lflag)
  8004e4:	85 d2                	test   %edx,%edx
  8004e6:	74 10                	je     8004f8 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004e8:	8b 10                	mov    (%eax),%edx
  8004ea:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004ed:	89 08                	mov    %ecx,(%eax)
  8004ef:	8b 02                	mov    (%edx),%eax
  8004f1:	ba 00 00 00 00       	mov    $0x0,%edx
  8004f6:	eb 0e                	jmp    800506 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004f8:	8b 10                	mov    (%eax),%edx
  8004fa:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004fd:	89 08                	mov    %ecx,(%eax)
  8004ff:	8b 02                	mov    (%edx),%eax
  800501:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800506:	5d                   	pop    %ebp
  800507:	c3                   	ret    

00800508 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800508:	55                   	push   %ebp
  800509:	89 e5                	mov    %esp,%ebp
  80050b:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80050e:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800511:	8b 10                	mov    (%eax),%edx
  800513:	3b 50 04             	cmp    0x4(%eax),%edx
  800516:	73 0a                	jae    800522 <sprintputch+0x1a>
		*b->buf++ = ch;
  800518:	8d 4a 01             	lea    0x1(%edx),%ecx
  80051b:	89 08                	mov    %ecx,(%eax)
  80051d:	8b 45 08             	mov    0x8(%ebp),%eax
  800520:	88 02                	mov    %al,(%edx)
}
  800522:	5d                   	pop    %ebp
  800523:	c3                   	ret    

00800524 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800524:	55                   	push   %ebp
  800525:	89 e5                	mov    %esp,%ebp
  800527:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80052a:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80052d:	50                   	push   %eax
  80052e:	ff 75 10             	pushl  0x10(%ebp)
  800531:	ff 75 0c             	pushl  0xc(%ebp)
  800534:	ff 75 08             	pushl  0x8(%ebp)
  800537:	e8 05 00 00 00       	call   800541 <vprintfmt>
	va_end(ap);
}
  80053c:	83 c4 10             	add    $0x10,%esp
  80053f:	c9                   	leave  
  800540:	c3                   	ret    

00800541 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800541:	55                   	push   %ebp
  800542:	89 e5                	mov    %esp,%ebp
  800544:	57                   	push   %edi
  800545:	56                   	push   %esi
  800546:	53                   	push   %ebx
  800547:	83 ec 2c             	sub    $0x2c,%esp
  80054a:	8b 75 08             	mov    0x8(%ebp),%esi
  80054d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800550:	8b 7d 10             	mov    0x10(%ebp),%edi
  800553:	eb 12                	jmp    800567 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800555:	85 c0                	test   %eax,%eax
  800557:	0f 84 68 03 00 00    	je     8008c5 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80055d:	83 ec 08             	sub    $0x8,%esp
  800560:	53                   	push   %ebx
  800561:	50                   	push   %eax
  800562:	ff d6                	call   *%esi
  800564:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800567:	47                   	inc    %edi
  800568:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80056c:	83 f8 25             	cmp    $0x25,%eax
  80056f:	75 e4                	jne    800555 <vprintfmt+0x14>
  800571:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800575:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80057c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800583:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80058a:	ba 00 00 00 00       	mov    $0x0,%edx
  80058f:	eb 07                	jmp    800598 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800591:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800594:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800598:	8d 47 01             	lea    0x1(%edi),%eax
  80059b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80059e:	0f b6 0f             	movzbl (%edi),%ecx
  8005a1:	8a 07                	mov    (%edi),%al
  8005a3:	83 e8 23             	sub    $0x23,%eax
  8005a6:	3c 55                	cmp    $0x55,%al
  8005a8:	0f 87 fe 02 00 00    	ja     8008ac <vprintfmt+0x36b>
  8005ae:	0f b6 c0             	movzbl %al,%eax
  8005b1:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8005b8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005bb:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005bf:	eb d7                	jmp    800598 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005c4:	b8 00 00 00 00       	mov    $0x0,%eax
  8005c9:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005cc:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005cf:	01 c0                	add    %eax,%eax
  8005d1:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005d5:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005d8:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005db:	83 fa 09             	cmp    $0x9,%edx
  8005de:	77 34                	ja     800614 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005e0:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005e1:	eb e9                	jmp    8005cc <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005e3:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e6:	8d 48 04             	lea    0x4(%eax),%ecx
  8005e9:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005ec:	8b 00                	mov    (%eax),%eax
  8005ee:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005f4:	eb 24                	jmp    80061a <vprintfmt+0xd9>
  8005f6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005fa:	79 07                	jns    800603 <vprintfmt+0xc2>
  8005fc:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800603:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800606:	eb 90                	jmp    800598 <vprintfmt+0x57>
  800608:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80060b:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800612:	eb 84                	jmp    800598 <vprintfmt+0x57>
  800614:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800617:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80061a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80061e:	0f 89 74 ff ff ff    	jns    800598 <vprintfmt+0x57>
				width = precision, precision = -1;
  800624:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800627:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80062a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800631:	e9 62 ff ff ff       	jmp    800598 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800636:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800637:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80063a:	e9 59 ff ff ff       	jmp    800598 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80063f:	8b 45 14             	mov    0x14(%ebp),%eax
  800642:	8d 50 04             	lea    0x4(%eax),%edx
  800645:	89 55 14             	mov    %edx,0x14(%ebp)
  800648:	83 ec 08             	sub    $0x8,%esp
  80064b:	53                   	push   %ebx
  80064c:	ff 30                	pushl  (%eax)
  80064e:	ff d6                	call   *%esi
			break;
  800650:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800653:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800656:	e9 0c ff ff ff       	jmp    800567 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80065b:	8b 45 14             	mov    0x14(%ebp),%eax
  80065e:	8d 50 04             	lea    0x4(%eax),%edx
  800661:	89 55 14             	mov    %edx,0x14(%ebp)
  800664:	8b 00                	mov    (%eax),%eax
  800666:	85 c0                	test   %eax,%eax
  800668:	79 02                	jns    80066c <vprintfmt+0x12b>
  80066a:	f7 d8                	neg    %eax
  80066c:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80066e:	83 f8 08             	cmp    $0x8,%eax
  800671:	7f 0b                	jg     80067e <vprintfmt+0x13d>
  800673:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  80067a:	85 c0                	test   %eax,%eax
  80067c:	75 18                	jne    800696 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80067e:	52                   	push   %edx
  80067f:	68 76 0f 80 00       	push   $0x800f76
  800684:	53                   	push   %ebx
  800685:	56                   	push   %esi
  800686:	e8 99 fe ff ff       	call   800524 <printfmt>
  80068b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80068e:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800691:	e9 d1 fe ff ff       	jmp    800567 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800696:	50                   	push   %eax
  800697:	68 7f 0f 80 00       	push   $0x800f7f
  80069c:	53                   	push   %ebx
  80069d:	56                   	push   %esi
  80069e:	e8 81 fe ff ff       	call   800524 <printfmt>
  8006a3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006a9:	e9 b9 fe ff ff       	jmp    800567 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006ae:	8b 45 14             	mov    0x14(%ebp),%eax
  8006b1:	8d 50 04             	lea    0x4(%eax),%edx
  8006b4:	89 55 14             	mov    %edx,0x14(%ebp)
  8006b7:	8b 38                	mov    (%eax),%edi
  8006b9:	85 ff                	test   %edi,%edi
  8006bb:	75 05                	jne    8006c2 <vprintfmt+0x181>
				p = "(null)";
  8006bd:	bf 6f 0f 80 00       	mov    $0x800f6f,%edi
			if (width > 0 && padc != '-')
  8006c2:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006c6:	0f 8e 90 00 00 00    	jle    80075c <vprintfmt+0x21b>
  8006cc:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006d0:	0f 84 8e 00 00 00    	je     800764 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006d6:	83 ec 08             	sub    $0x8,%esp
  8006d9:	ff 75 d0             	pushl  -0x30(%ebp)
  8006dc:	57                   	push   %edi
  8006dd:	e8 70 02 00 00       	call   800952 <strnlen>
  8006e2:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006e5:	29 c1                	sub    %eax,%ecx
  8006e7:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006ea:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006ed:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006f1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006f4:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006f7:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f9:	eb 0d                	jmp    800708 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006fb:	83 ec 08             	sub    $0x8,%esp
  8006fe:	53                   	push   %ebx
  8006ff:	ff 75 e4             	pushl  -0x1c(%ebp)
  800702:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800704:	4f                   	dec    %edi
  800705:	83 c4 10             	add    $0x10,%esp
  800708:	85 ff                	test   %edi,%edi
  80070a:	7f ef                	jg     8006fb <vprintfmt+0x1ba>
  80070c:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80070f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800712:	89 c8                	mov    %ecx,%eax
  800714:	85 c9                	test   %ecx,%ecx
  800716:	79 05                	jns    80071d <vprintfmt+0x1dc>
  800718:	b8 00 00 00 00       	mov    $0x0,%eax
  80071d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800720:	29 c1                	sub    %eax,%ecx
  800722:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800725:	89 75 08             	mov    %esi,0x8(%ebp)
  800728:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80072b:	eb 3d                	jmp    80076a <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80072d:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800731:	74 19                	je     80074c <vprintfmt+0x20b>
  800733:	0f be c0             	movsbl %al,%eax
  800736:	83 e8 20             	sub    $0x20,%eax
  800739:	83 f8 5e             	cmp    $0x5e,%eax
  80073c:	76 0e                	jbe    80074c <vprintfmt+0x20b>
					putch('?', putdat);
  80073e:	83 ec 08             	sub    $0x8,%esp
  800741:	53                   	push   %ebx
  800742:	6a 3f                	push   $0x3f
  800744:	ff 55 08             	call   *0x8(%ebp)
  800747:	83 c4 10             	add    $0x10,%esp
  80074a:	eb 0b                	jmp    800757 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80074c:	83 ec 08             	sub    $0x8,%esp
  80074f:	53                   	push   %ebx
  800750:	52                   	push   %edx
  800751:	ff 55 08             	call   *0x8(%ebp)
  800754:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800757:	ff 4d e4             	decl   -0x1c(%ebp)
  80075a:	eb 0e                	jmp    80076a <vprintfmt+0x229>
  80075c:	89 75 08             	mov    %esi,0x8(%ebp)
  80075f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800762:	eb 06                	jmp    80076a <vprintfmt+0x229>
  800764:	89 75 08             	mov    %esi,0x8(%ebp)
  800767:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80076a:	47                   	inc    %edi
  80076b:	8a 47 ff             	mov    -0x1(%edi),%al
  80076e:	0f be d0             	movsbl %al,%edx
  800771:	85 d2                	test   %edx,%edx
  800773:	74 1d                	je     800792 <vprintfmt+0x251>
  800775:	85 f6                	test   %esi,%esi
  800777:	78 b4                	js     80072d <vprintfmt+0x1ec>
  800779:	4e                   	dec    %esi
  80077a:	79 b1                	jns    80072d <vprintfmt+0x1ec>
  80077c:	8b 75 08             	mov    0x8(%ebp),%esi
  80077f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800782:	eb 14                	jmp    800798 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800784:	83 ec 08             	sub    $0x8,%esp
  800787:	53                   	push   %ebx
  800788:	6a 20                	push   $0x20
  80078a:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80078c:	4f                   	dec    %edi
  80078d:	83 c4 10             	add    $0x10,%esp
  800790:	eb 06                	jmp    800798 <vprintfmt+0x257>
  800792:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800795:	8b 75 08             	mov    0x8(%ebp),%esi
  800798:	85 ff                	test   %edi,%edi
  80079a:	7f e8                	jg     800784 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80079c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80079f:	e9 c3 fd ff ff       	jmp    800567 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007a4:	83 fa 01             	cmp    $0x1,%edx
  8007a7:	7e 16                	jle    8007bf <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007a9:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ac:	8d 50 08             	lea    0x8(%eax),%edx
  8007af:	89 55 14             	mov    %edx,0x14(%ebp)
  8007b2:	8b 50 04             	mov    0x4(%eax),%edx
  8007b5:	8b 00                	mov    (%eax),%eax
  8007b7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007ba:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007bd:	eb 32                	jmp    8007f1 <vprintfmt+0x2b0>
	else if (lflag)
  8007bf:	85 d2                	test   %edx,%edx
  8007c1:	74 18                	je     8007db <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007c3:	8b 45 14             	mov    0x14(%ebp),%eax
  8007c6:	8d 50 04             	lea    0x4(%eax),%edx
  8007c9:	89 55 14             	mov    %edx,0x14(%ebp)
  8007cc:	8b 00                	mov    (%eax),%eax
  8007ce:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007d1:	89 c1                	mov    %eax,%ecx
  8007d3:	c1 f9 1f             	sar    $0x1f,%ecx
  8007d6:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007d9:	eb 16                	jmp    8007f1 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007db:	8b 45 14             	mov    0x14(%ebp),%eax
  8007de:	8d 50 04             	lea    0x4(%eax),%edx
  8007e1:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e4:	8b 00                	mov    (%eax),%eax
  8007e6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007e9:	89 c1                	mov    %eax,%ecx
  8007eb:	c1 f9 1f             	sar    $0x1f,%ecx
  8007ee:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007f1:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007f4:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007f7:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007fb:	79 76                	jns    800873 <vprintfmt+0x332>
				putch('-', putdat);
  8007fd:	83 ec 08             	sub    $0x8,%esp
  800800:	53                   	push   %ebx
  800801:	6a 2d                	push   $0x2d
  800803:	ff d6                	call   *%esi
				num = -(long long) num;
  800805:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800808:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80080b:	f7 d8                	neg    %eax
  80080d:	83 d2 00             	adc    $0x0,%edx
  800810:	f7 da                	neg    %edx
  800812:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800815:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80081a:	eb 5c                	jmp    800878 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80081c:	8d 45 14             	lea    0x14(%ebp),%eax
  80081f:	e8 aa fc ff ff       	call   8004ce <getuint>
			base = 10;
  800824:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800829:	eb 4d                	jmp    800878 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80082b:	8d 45 14             	lea    0x14(%ebp),%eax
  80082e:	e8 9b fc ff ff       	call   8004ce <getuint>
			base = 8;
  800833:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800838:	eb 3e                	jmp    800878 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80083a:	83 ec 08             	sub    $0x8,%esp
  80083d:	53                   	push   %ebx
  80083e:	6a 30                	push   $0x30
  800840:	ff d6                	call   *%esi
			putch('x', putdat);
  800842:	83 c4 08             	add    $0x8,%esp
  800845:	53                   	push   %ebx
  800846:	6a 78                	push   $0x78
  800848:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80084a:	8b 45 14             	mov    0x14(%ebp),%eax
  80084d:	8d 50 04             	lea    0x4(%eax),%edx
  800850:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800853:	8b 00                	mov    (%eax),%eax
  800855:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80085a:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80085d:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800862:	eb 14                	jmp    800878 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800864:	8d 45 14             	lea    0x14(%ebp),%eax
  800867:	e8 62 fc ff ff       	call   8004ce <getuint>
			base = 16;
  80086c:	b9 10 00 00 00       	mov    $0x10,%ecx
  800871:	eb 05                	jmp    800878 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800873:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800878:	83 ec 0c             	sub    $0xc,%esp
  80087b:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80087f:	57                   	push   %edi
  800880:	ff 75 e4             	pushl  -0x1c(%ebp)
  800883:	51                   	push   %ecx
  800884:	52                   	push   %edx
  800885:	50                   	push   %eax
  800886:	89 da                	mov    %ebx,%edx
  800888:	89 f0                	mov    %esi,%eax
  80088a:	e8 92 fb ff ff       	call   800421 <printnum>
			break;
  80088f:	83 c4 20             	add    $0x20,%esp
  800892:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800895:	e9 cd fc ff ff       	jmp    800567 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80089a:	83 ec 08             	sub    $0x8,%esp
  80089d:	53                   	push   %ebx
  80089e:	51                   	push   %ecx
  80089f:	ff d6                	call   *%esi
			break;
  8008a1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008a4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008a7:	e9 bb fc ff ff       	jmp    800567 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008ac:	83 ec 08             	sub    $0x8,%esp
  8008af:	53                   	push   %ebx
  8008b0:	6a 25                	push   $0x25
  8008b2:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008b4:	83 c4 10             	add    $0x10,%esp
  8008b7:	eb 01                	jmp    8008ba <vprintfmt+0x379>
  8008b9:	4f                   	dec    %edi
  8008ba:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008be:	75 f9                	jne    8008b9 <vprintfmt+0x378>
  8008c0:	e9 a2 fc ff ff       	jmp    800567 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008c5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008c8:	5b                   	pop    %ebx
  8008c9:	5e                   	pop    %esi
  8008ca:	5f                   	pop    %edi
  8008cb:	5d                   	pop    %ebp
  8008cc:	c3                   	ret    

008008cd <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008cd:	55                   	push   %ebp
  8008ce:	89 e5                	mov    %esp,%ebp
  8008d0:	83 ec 18             	sub    $0x18,%esp
  8008d3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008d6:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008d9:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008dc:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008e0:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008e3:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008ea:	85 c0                	test   %eax,%eax
  8008ec:	74 26                	je     800914 <vsnprintf+0x47>
  8008ee:	85 d2                	test   %edx,%edx
  8008f0:	7e 29                	jle    80091b <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008f2:	ff 75 14             	pushl  0x14(%ebp)
  8008f5:	ff 75 10             	pushl  0x10(%ebp)
  8008f8:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008fb:	50                   	push   %eax
  8008fc:	68 08 05 80 00       	push   $0x800508
  800901:	e8 3b fc ff ff       	call   800541 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800906:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800909:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80090c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80090f:	83 c4 10             	add    $0x10,%esp
  800912:	eb 0c                	jmp    800920 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800914:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800919:	eb 05                	jmp    800920 <vsnprintf+0x53>
  80091b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800920:	c9                   	leave  
  800921:	c3                   	ret    

00800922 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800922:	55                   	push   %ebp
  800923:	89 e5                	mov    %esp,%ebp
  800925:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800928:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80092b:	50                   	push   %eax
  80092c:	ff 75 10             	pushl  0x10(%ebp)
  80092f:	ff 75 0c             	pushl  0xc(%ebp)
  800932:	ff 75 08             	pushl  0x8(%ebp)
  800935:	e8 93 ff ff ff       	call   8008cd <vsnprintf>
	va_end(ap);

	return rc;
}
  80093a:	c9                   	leave  
  80093b:	c3                   	ret    

0080093c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80093c:	55                   	push   %ebp
  80093d:	89 e5                	mov    %esp,%ebp
  80093f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800942:	b8 00 00 00 00       	mov    $0x0,%eax
  800947:	eb 01                	jmp    80094a <strlen+0xe>
		n++;
  800949:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80094a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80094e:	75 f9                	jne    800949 <strlen+0xd>
		n++;
	return n;
}
  800950:	5d                   	pop    %ebp
  800951:	c3                   	ret    

00800952 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800952:	55                   	push   %ebp
  800953:	89 e5                	mov    %esp,%ebp
  800955:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800958:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80095b:	ba 00 00 00 00       	mov    $0x0,%edx
  800960:	eb 01                	jmp    800963 <strnlen+0x11>
		n++;
  800962:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800963:	39 c2                	cmp    %eax,%edx
  800965:	74 08                	je     80096f <strnlen+0x1d>
  800967:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80096b:	75 f5                	jne    800962 <strnlen+0x10>
  80096d:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80096f:	5d                   	pop    %ebp
  800970:	c3                   	ret    

00800971 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800971:	55                   	push   %ebp
  800972:	89 e5                	mov    %esp,%ebp
  800974:	53                   	push   %ebx
  800975:	8b 45 08             	mov    0x8(%ebp),%eax
  800978:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80097b:	89 c2                	mov    %eax,%edx
  80097d:	42                   	inc    %edx
  80097e:	41                   	inc    %ecx
  80097f:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800982:	88 5a ff             	mov    %bl,-0x1(%edx)
  800985:	84 db                	test   %bl,%bl
  800987:	75 f4                	jne    80097d <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800989:	5b                   	pop    %ebx
  80098a:	5d                   	pop    %ebp
  80098b:	c3                   	ret    

0080098c <strcat>:

char *
strcat(char *dst, const char *src)
{
  80098c:	55                   	push   %ebp
  80098d:	89 e5                	mov    %esp,%ebp
  80098f:	53                   	push   %ebx
  800990:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800993:	53                   	push   %ebx
  800994:	e8 a3 ff ff ff       	call   80093c <strlen>
  800999:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80099c:	ff 75 0c             	pushl  0xc(%ebp)
  80099f:	01 d8                	add    %ebx,%eax
  8009a1:	50                   	push   %eax
  8009a2:	e8 ca ff ff ff       	call   800971 <strcpy>
	return dst;
}
  8009a7:	89 d8                	mov    %ebx,%eax
  8009a9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009ac:	c9                   	leave  
  8009ad:	c3                   	ret    

008009ae <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009ae:	55                   	push   %ebp
  8009af:	89 e5                	mov    %esp,%ebp
  8009b1:	56                   	push   %esi
  8009b2:	53                   	push   %ebx
  8009b3:	8b 75 08             	mov    0x8(%ebp),%esi
  8009b6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009b9:	89 f3                	mov    %esi,%ebx
  8009bb:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009be:	89 f2                	mov    %esi,%edx
  8009c0:	eb 0c                	jmp    8009ce <strncpy+0x20>
		*dst++ = *src;
  8009c2:	42                   	inc    %edx
  8009c3:	8a 01                	mov    (%ecx),%al
  8009c5:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009c8:	80 39 01             	cmpb   $0x1,(%ecx)
  8009cb:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ce:	39 da                	cmp    %ebx,%edx
  8009d0:	75 f0                	jne    8009c2 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009d2:	89 f0                	mov    %esi,%eax
  8009d4:	5b                   	pop    %ebx
  8009d5:	5e                   	pop    %esi
  8009d6:	5d                   	pop    %ebp
  8009d7:	c3                   	ret    

008009d8 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009d8:	55                   	push   %ebp
  8009d9:	89 e5                	mov    %esp,%ebp
  8009db:	56                   	push   %esi
  8009dc:	53                   	push   %ebx
  8009dd:	8b 75 08             	mov    0x8(%ebp),%esi
  8009e0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009e3:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009e6:	85 c0                	test   %eax,%eax
  8009e8:	74 1e                	je     800a08 <strlcpy+0x30>
  8009ea:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009ee:	89 f2                	mov    %esi,%edx
  8009f0:	eb 05                	jmp    8009f7 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009f2:	42                   	inc    %edx
  8009f3:	41                   	inc    %ecx
  8009f4:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009f7:	39 c2                	cmp    %eax,%edx
  8009f9:	74 08                	je     800a03 <strlcpy+0x2b>
  8009fb:	8a 19                	mov    (%ecx),%bl
  8009fd:	84 db                	test   %bl,%bl
  8009ff:	75 f1                	jne    8009f2 <strlcpy+0x1a>
  800a01:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a03:	c6 00 00             	movb   $0x0,(%eax)
  800a06:	eb 02                	jmp    800a0a <strlcpy+0x32>
  800a08:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a0a:	29 f0                	sub    %esi,%eax
}
  800a0c:	5b                   	pop    %ebx
  800a0d:	5e                   	pop    %esi
  800a0e:	5d                   	pop    %ebp
  800a0f:	c3                   	ret    

00800a10 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a10:	55                   	push   %ebp
  800a11:	89 e5                	mov    %esp,%ebp
  800a13:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a16:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a19:	eb 02                	jmp    800a1d <strcmp+0xd>
		p++, q++;
  800a1b:	41                   	inc    %ecx
  800a1c:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a1d:	8a 01                	mov    (%ecx),%al
  800a1f:	84 c0                	test   %al,%al
  800a21:	74 04                	je     800a27 <strcmp+0x17>
  800a23:	3a 02                	cmp    (%edx),%al
  800a25:	74 f4                	je     800a1b <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a27:	0f b6 c0             	movzbl %al,%eax
  800a2a:	0f b6 12             	movzbl (%edx),%edx
  800a2d:	29 d0                	sub    %edx,%eax
}
  800a2f:	5d                   	pop    %ebp
  800a30:	c3                   	ret    

00800a31 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a31:	55                   	push   %ebp
  800a32:	89 e5                	mov    %esp,%ebp
  800a34:	53                   	push   %ebx
  800a35:	8b 45 08             	mov    0x8(%ebp),%eax
  800a38:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a3b:	89 c3                	mov    %eax,%ebx
  800a3d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a40:	eb 02                	jmp    800a44 <strncmp+0x13>
		n--, p++, q++;
  800a42:	40                   	inc    %eax
  800a43:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a44:	39 d8                	cmp    %ebx,%eax
  800a46:	74 14                	je     800a5c <strncmp+0x2b>
  800a48:	8a 08                	mov    (%eax),%cl
  800a4a:	84 c9                	test   %cl,%cl
  800a4c:	74 04                	je     800a52 <strncmp+0x21>
  800a4e:	3a 0a                	cmp    (%edx),%cl
  800a50:	74 f0                	je     800a42 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a52:	0f b6 00             	movzbl (%eax),%eax
  800a55:	0f b6 12             	movzbl (%edx),%edx
  800a58:	29 d0                	sub    %edx,%eax
  800a5a:	eb 05                	jmp    800a61 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a5c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a61:	5b                   	pop    %ebx
  800a62:	5d                   	pop    %ebp
  800a63:	c3                   	ret    

00800a64 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a64:	55                   	push   %ebp
  800a65:	89 e5                	mov    %esp,%ebp
  800a67:	8b 45 08             	mov    0x8(%ebp),%eax
  800a6a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a6d:	eb 05                	jmp    800a74 <strchr+0x10>
		if (*s == c)
  800a6f:	38 ca                	cmp    %cl,%dl
  800a71:	74 0c                	je     800a7f <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a73:	40                   	inc    %eax
  800a74:	8a 10                	mov    (%eax),%dl
  800a76:	84 d2                	test   %dl,%dl
  800a78:	75 f5                	jne    800a6f <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a7a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a7f:	5d                   	pop    %ebp
  800a80:	c3                   	ret    

00800a81 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a81:	55                   	push   %ebp
  800a82:	89 e5                	mov    %esp,%ebp
  800a84:	8b 45 08             	mov    0x8(%ebp),%eax
  800a87:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a8a:	eb 05                	jmp    800a91 <strfind+0x10>
		if (*s == c)
  800a8c:	38 ca                	cmp    %cl,%dl
  800a8e:	74 07                	je     800a97 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a90:	40                   	inc    %eax
  800a91:	8a 10                	mov    (%eax),%dl
  800a93:	84 d2                	test   %dl,%dl
  800a95:	75 f5                	jne    800a8c <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a97:	5d                   	pop    %ebp
  800a98:	c3                   	ret    

00800a99 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a99:	55                   	push   %ebp
  800a9a:	89 e5                	mov    %esp,%ebp
  800a9c:	57                   	push   %edi
  800a9d:	56                   	push   %esi
  800a9e:	53                   	push   %ebx
  800a9f:	8b 7d 08             	mov    0x8(%ebp),%edi
  800aa2:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800aa5:	85 c9                	test   %ecx,%ecx
  800aa7:	74 36                	je     800adf <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800aa9:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800aaf:	75 28                	jne    800ad9 <memset+0x40>
  800ab1:	f6 c1 03             	test   $0x3,%cl
  800ab4:	75 23                	jne    800ad9 <memset+0x40>
		c &= 0xFF;
  800ab6:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aba:	89 d3                	mov    %edx,%ebx
  800abc:	c1 e3 08             	shl    $0x8,%ebx
  800abf:	89 d6                	mov    %edx,%esi
  800ac1:	c1 e6 18             	shl    $0x18,%esi
  800ac4:	89 d0                	mov    %edx,%eax
  800ac6:	c1 e0 10             	shl    $0x10,%eax
  800ac9:	09 f0                	or     %esi,%eax
  800acb:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800acd:	89 d8                	mov    %ebx,%eax
  800acf:	09 d0                	or     %edx,%eax
  800ad1:	c1 e9 02             	shr    $0x2,%ecx
  800ad4:	fc                   	cld    
  800ad5:	f3 ab                	rep stos %eax,%es:(%edi)
  800ad7:	eb 06                	jmp    800adf <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ad9:	8b 45 0c             	mov    0xc(%ebp),%eax
  800adc:	fc                   	cld    
  800add:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800adf:	89 f8                	mov    %edi,%eax
  800ae1:	5b                   	pop    %ebx
  800ae2:	5e                   	pop    %esi
  800ae3:	5f                   	pop    %edi
  800ae4:	5d                   	pop    %ebp
  800ae5:	c3                   	ret    

00800ae6 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ae6:	55                   	push   %ebp
  800ae7:	89 e5                	mov    %esp,%ebp
  800ae9:	57                   	push   %edi
  800aea:	56                   	push   %esi
  800aeb:	8b 45 08             	mov    0x8(%ebp),%eax
  800aee:	8b 75 0c             	mov    0xc(%ebp),%esi
  800af1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800af4:	39 c6                	cmp    %eax,%esi
  800af6:	73 33                	jae    800b2b <memmove+0x45>
  800af8:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800afb:	39 d0                	cmp    %edx,%eax
  800afd:	73 2c                	jae    800b2b <memmove+0x45>
		s += n;
		d += n;
  800aff:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b02:	89 d6                	mov    %edx,%esi
  800b04:	09 fe                	or     %edi,%esi
  800b06:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b0c:	75 13                	jne    800b21 <memmove+0x3b>
  800b0e:	f6 c1 03             	test   $0x3,%cl
  800b11:	75 0e                	jne    800b21 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b13:	83 ef 04             	sub    $0x4,%edi
  800b16:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b19:	c1 e9 02             	shr    $0x2,%ecx
  800b1c:	fd                   	std    
  800b1d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b1f:	eb 07                	jmp    800b28 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b21:	4f                   	dec    %edi
  800b22:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b25:	fd                   	std    
  800b26:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b28:	fc                   	cld    
  800b29:	eb 1d                	jmp    800b48 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b2b:	89 f2                	mov    %esi,%edx
  800b2d:	09 c2                	or     %eax,%edx
  800b2f:	f6 c2 03             	test   $0x3,%dl
  800b32:	75 0f                	jne    800b43 <memmove+0x5d>
  800b34:	f6 c1 03             	test   $0x3,%cl
  800b37:	75 0a                	jne    800b43 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b39:	c1 e9 02             	shr    $0x2,%ecx
  800b3c:	89 c7                	mov    %eax,%edi
  800b3e:	fc                   	cld    
  800b3f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b41:	eb 05                	jmp    800b48 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b43:	89 c7                	mov    %eax,%edi
  800b45:	fc                   	cld    
  800b46:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b48:	5e                   	pop    %esi
  800b49:	5f                   	pop    %edi
  800b4a:	5d                   	pop    %ebp
  800b4b:	c3                   	ret    

00800b4c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b4c:	55                   	push   %ebp
  800b4d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b4f:	ff 75 10             	pushl  0x10(%ebp)
  800b52:	ff 75 0c             	pushl  0xc(%ebp)
  800b55:	ff 75 08             	pushl  0x8(%ebp)
  800b58:	e8 89 ff ff ff       	call   800ae6 <memmove>
}
  800b5d:	c9                   	leave  
  800b5e:	c3                   	ret    

00800b5f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b5f:	55                   	push   %ebp
  800b60:	89 e5                	mov    %esp,%ebp
  800b62:	56                   	push   %esi
  800b63:	53                   	push   %ebx
  800b64:	8b 45 08             	mov    0x8(%ebp),%eax
  800b67:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b6a:	89 c6                	mov    %eax,%esi
  800b6c:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b6f:	eb 14                	jmp    800b85 <memcmp+0x26>
		if (*s1 != *s2)
  800b71:	8a 08                	mov    (%eax),%cl
  800b73:	8a 1a                	mov    (%edx),%bl
  800b75:	38 d9                	cmp    %bl,%cl
  800b77:	74 0a                	je     800b83 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b79:	0f b6 c1             	movzbl %cl,%eax
  800b7c:	0f b6 db             	movzbl %bl,%ebx
  800b7f:	29 d8                	sub    %ebx,%eax
  800b81:	eb 0b                	jmp    800b8e <memcmp+0x2f>
		s1++, s2++;
  800b83:	40                   	inc    %eax
  800b84:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b85:	39 f0                	cmp    %esi,%eax
  800b87:	75 e8                	jne    800b71 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b89:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b8e:	5b                   	pop    %ebx
  800b8f:	5e                   	pop    %esi
  800b90:	5d                   	pop    %ebp
  800b91:	c3                   	ret    

00800b92 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b92:	55                   	push   %ebp
  800b93:	89 e5                	mov    %esp,%ebp
  800b95:	53                   	push   %ebx
  800b96:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b99:	89 c1                	mov    %eax,%ecx
  800b9b:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b9e:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ba2:	eb 08                	jmp    800bac <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800ba4:	0f b6 10             	movzbl (%eax),%edx
  800ba7:	39 da                	cmp    %ebx,%edx
  800ba9:	74 05                	je     800bb0 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bab:	40                   	inc    %eax
  800bac:	39 c8                	cmp    %ecx,%eax
  800bae:	72 f4                	jb     800ba4 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bb0:	5b                   	pop    %ebx
  800bb1:	5d                   	pop    %ebp
  800bb2:	c3                   	ret    

00800bb3 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bb3:	55                   	push   %ebp
  800bb4:	89 e5                	mov    %esp,%ebp
  800bb6:	57                   	push   %edi
  800bb7:	56                   	push   %esi
  800bb8:	53                   	push   %ebx
  800bb9:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bbc:	eb 01                	jmp    800bbf <strtol+0xc>
		s++;
  800bbe:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bbf:	8a 01                	mov    (%ecx),%al
  800bc1:	3c 20                	cmp    $0x20,%al
  800bc3:	74 f9                	je     800bbe <strtol+0xb>
  800bc5:	3c 09                	cmp    $0x9,%al
  800bc7:	74 f5                	je     800bbe <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bc9:	3c 2b                	cmp    $0x2b,%al
  800bcb:	75 08                	jne    800bd5 <strtol+0x22>
		s++;
  800bcd:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bce:	bf 00 00 00 00       	mov    $0x0,%edi
  800bd3:	eb 11                	jmp    800be6 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bd5:	3c 2d                	cmp    $0x2d,%al
  800bd7:	75 08                	jne    800be1 <strtol+0x2e>
		s++, neg = 1;
  800bd9:	41                   	inc    %ecx
  800bda:	bf 01 00 00 00       	mov    $0x1,%edi
  800bdf:	eb 05                	jmp    800be6 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800be1:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800be6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bea:	0f 84 87 00 00 00    	je     800c77 <strtol+0xc4>
  800bf0:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bf4:	75 27                	jne    800c1d <strtol+0x6a>
  800bf6:	80 39 30             	cmpb   $0x30,(%ecx)
  800bf9:	75 22                	jne    800c1d <strtol+0x6a>
  800bfb:	e9 88 00 00 00       	jmp    800c88 <strtol+0xd5>
		s += 2, base = 16;
  800c00:	83 c1 02             	add    $0x2,%ecx
  800c03:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c0a:	eb 11                	jmp    800c1d <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c0c:	41                   	inc    %ecx
  800c0d:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c14:	eb 07                	jmp    800c1d <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c16:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c1d:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c22:	8a 11                	mov    (%ecx),%dl
  800c24:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c27:	80 fb 09             	cmp    $0x9,%bl
  800c2a:	77 08                	ja     800c34 <strtol+0x81>
			dig = *s - '0';
  800c2c:	0f be d2             	movsbl %dl,%edx
  800c2f:	83 ea 30             	sub    $0x30,%edx
  800c32:	eb 22                	jmp    800c56 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c34:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c37:	89 f3                	mov    %esi,%ebx
  800c39:	80 fb 19             	cmp    $0x19,%bl
  800c3c:	77 08                	ja     800c46 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c3e:	0f be d2             	movsbl %dl,%edx
  800c41:	83 ea 57             	sub    $0x57,%edx
  800c44:	eb 10                	jmp    800c56 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c46:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c49:	89 f3                	mov    %esi,%ebx
  800c4b:	80 fb 19             	cmp    $0x19,%bl
  800c4e:	77 14                	ja     800c64 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c50:	0f be d2             	movsbl %dl,%edx
  800c53:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c56:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c59:	7d 09                	jge    800c64 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c5b:	41                   	inc    %ecx
  800c5c:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c60:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c62:	eb be                	jmp    800c22 <strtol+0x6f>

	if (endptr)
  800c64:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c68:	74 05                	je     800c6f <strtol+0xbc>
		*endptr = (char *) s;
  800c6a:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c6d:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c6f:	85 ff                	test   %edi,%edi
  800c71:	74 21                	je     800c94 <strtol+0xe1>
  800c73:	f7 d8                	neg    %eax
  800c75:	eb 1d                	jmp    800c94 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c77:	80 39 30             	cmpb   $0x30,(%ecx)
  800c7a:	75 9a                	jne    800c16 <strtol+0x63>
  800c7c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c80:	0f 84 7a ff ff ff    	je     800c00 <strtol+0x4d>
  800c86:	eb 84                	jmp    800c0c <strtol+0x59>
  800c88:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c8c:	0f 84 6e ff ff ff    	je     800c00 <strtol+0x4d>
  800c92:	eb 89                	jmp    800c1d <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c94:	5b                   	pop    %ebx
  800c95:	5e                   	pop    %esi
  800c96:	5f                   	pop    %edi
  800c97:	5d                   	pop    %ebp
  800c98:	c3                   	ret    
  800c99:	66 90                	xchg   %ax,%ax
  800c9b:	90                   	nop

00800c9c <__udivdi3>:
  800c9c:	55                   	push   %ebp
  800c9d:	57                   	push   %edi
  800c9e:	56                   	push   %esi
  800c9f:	53                   	push   %ebx
  800ca0:	83 ec 1c             	sub    $0x1c,%esp
  800ca3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ca7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cab:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800caf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cb3:	89 ca                	mov    %ecx,%edx
  800cb5:	89 f8                	mov    %edi,%eax
  800cb7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cbb:	85 f6                	test   %esi,%esi
  800cbd:	75 2d                	jne    800cec <__udivdi3+0x50>
  800cbf:	39 cf                	cmp    %ecx,%edi
  800cc1:	77 65                	ja     800d28 <__udivdi3+0x8c>
  800cc3:	89 fd                	mov    %edi,%ebp
  800cc5:	85 ff                	test   %edi,%edi
  800cc7:	75 0b                	jne    800cd4 <__udivdi3+0x38>
  800cc9:	b8 01 00 00 00       	mov    $0x1,%eax
  800cce:	31 d2                	xor    %edx,%edx
  800cd0:	f7 f7                	div    %edi
  800cd2:	89 c5                	mov    %eax,%ebp
  800cd4:	31 d2                	xor    %edx,%edx
  800cd6:	89 c8                	mov    %ecx,%eax
  800cd8:	f7 f5                	div    %ebp
  800cda:	89 c1                	mov    %eax,%ecx
  800cdc:	89 d8                	mov    %ebx,%eax
  800cde:	f7 f5                	div    %ebp
  800ce0:	89 cf                	mov    %ecx,%edi
  800ce2:	89 fa                	mov    %edi,%edx
  800ce4:	83 c4 1c             	add    $0x1c,%esp
  800ce7:	5b                   	pop    %ebx
  800ce8:	5e                   	pop    %esi
  800ce9:	5f                   	pop    %edi
  800cea:	5d                   	pop    %ebp
  800ceb:	c3                   	ret    
  800cec:	39 ce                	cmp    %ecx,%esi
  800cee:	77 28                	ja     800d18 <__udivdi3+0x7c>
  800cf0:	0f bd fe             	bsr    %esi,%edi
  800cf3:	83 f7 1f             	xor    $0x1f,%edi
  800cf6:	75 40                	jne    800d38 <__udivdi3+0x9c>
  800cf8:	39 ce                	cmp    %ecx,%esi
  800cfa:	72 0a                	jb     800d06 <__udivdi3+0x6a>
  800cfc:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d00:	0f 87 9e 00 00 00    	ja     800da4 <__udivdi3+0x108>
  800d06:	b8 01 00 00 00       	mov    $0x1,%eax
  800d0b:	89 fa                	mov    %edi,%edx
  800d0d:	83 c4 1c             	add    $0x1c,%esp
  800d10:	5b                   	pop    %ebx
  800d11:	5e                   	pop    %esi
  800d12:	5f                   	pop    %edi
  800d13:	5d                   	pop    %ebp
  800d14:	c3                   	ret    
  800d15:	8d 76 00             	lea    0x0(%esi),%esi
  800d18:	31 ff                	xor    %edi,%edi
  800d1a:	31 c0                	xor    %eax,%eax
  800d1c:	89 fa                	mov    %edi,%edx
  800d1e:	83 c4 1c             	add    $0x1c,%esp
  800d21:	5b                   	pop    %ebx
  800d22:	5e                   	pop    %esi
  800d23:	5f                   	pop    %edi
  800d24:	5d                   	pop    %ebp
  800d25:	c3                   	ret    
  800d26:	66 90                	xchg   %ax,%ax
  800d28:	89 d8                	mov    %ebx,%eax
  800d2a:	f7 f7                	div    %edi
  800d2c:	31 ff                	xor    %edi,%edi
  800d2e:	89 fa                	mov    %edi,%edx
  800d30:	83 c4 1c             	add    $0x1c,%esp
  800d33:	5b                   	pop    %ebx
  800d34:	5e                   	pop    %esi
  800d35:	5f                   	pop    %edi
  800d36:	5d                   	pop    %ebp
  800d37:	c3                   	ret    
  800d38:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d3d:	89 eb                	mov    %ebp,%ebx
  800d3f:	29 fb                	sub    %edi,%ebx
  800d41:	89 f9                	mov    %edi,%ecx
  800d43:	d3 e6                	shl    %cl,%esi
  800d45:	89 c5                	mov    %eax,%ebp
  800d47:	88 d9                	mov    %bl,%cl
  800d49:	d3 ed                	shr    %cl,%ebp
  800d4b:	89 e9                	mov    %ebp,%ecx
  800d4d:	09 f1                	or     %esi,%ecx
  800d4f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d53:	89 f9                	mov    %edi,%ecx
  800d55:	d3 e0                	shl    %cl,%eax
  800d57:	89 c5                	mov    %eax,%ebp
  800d59:	89 d6                	mov    %edx,%esi
  800d5b:	88 d9                	mov    %bl,%cl
  800d5d:	d3 ee                	shr    %cl,%esi
  800d5f:	89 f9                	mov    %edi,%ecx
  800d61:	d3 e2                	shl    %cl,%edx
  800d63:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d67:	88 d9                	mov    %bl,%cl
  800d69:	d3 e8                	shr    %cl,%eax
  800d6b:	09 c2                	or     %eax,%edx
  800d6d:	89 d0                	mov    %edx,%eax
  800d6f:	89 f2                	mov    %esi,%edx
  800d71:	f7 74 24 0c          	divl   0xc(%esp)
  800d75:	89 d6                	mov    %edx,%esi
  800d77:	89 c3                	mov    %eax,%ebx
  800d79:	f7 e5                	mul    %ebp
  800d7b:	39 d6                	cmp    %edx,%esi
  800d7d:	72 19                	jb     800d98 <__udivdi3+0xfc>
  800d7f:	74 0b                	je     800d8c <__udivdi3+0xf0>
  800d81:	89 d8                	mov    %ebx,%eax
  800d83:	31 ff                	xor    %edi,%edi
  800d85:	e9 58 ff ff ff       	jmp    800ce2 <__udivdi3+0x46>
  800d8a:	66 90                	xchg   %ax,%ax
  800d8c:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d90:	89 f9                	mov    %edi,%ecx
  800d92:	d3 e2                	shl    %cl,%edx
  800d94:	39 c2                	cmp    %eax,%edx
  800d96:	73 e9                	jae    800d81 <__udivdi3+0xe5>
  800d98:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d9b:	31 ff                	xor    %edi,%edi
  800d9d:	e9 40 ff ff ff       	jmp    800ce2 <__udivdi3+0x46>
  800da2:	66 90                	xchg   %ax,%ax
  800da4:	31 c0                	xor    %eax,%eax
  800da6:	e9 37 ff ff ff       	jmp    800ce2 <__udivdi3+0x46>
  800dab:	90                   	nop

00800dac <__umoddi3>:
  800dac:	55                   	push   %ebp
  800dad:	57                   	push   %edi
  800dae:	56                   	push   %esi
  800daf:	53                   	push   %ebx
  800db0:	83 ec 1c             	sub    $0x1c,%esp
  800db3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800db7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dbb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dbf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dc3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dc7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dcb:	89 f3                	mov    %esi,%ebx
  800dcd:	89 fa                	mov    %edi,%edx
  800dcf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dd3:	89 34 24             	mov    %esi,(%esp)
  800dd6:	85 c0                	test   %eax,%eax
  800dd8:	75 1a                	jne    800df4 <__umoddi3+0x48>
  800dda:	39 f7                	cmp    %esi,%edi
  800ddc:	0f 86 a2 00 00 00    	jbe    800e84 <__umoddi3+0xd8>
  800de2:	89 c8                	mov    %ecx,%eax
  800de4:	89 f2                	mov    %esi,%edx
  800de6:	f7 f7                	div    %edi
  800de8:	89 d0                	mov    %edx,%eax
  800dea:	31 d2                	xor    %edx,%edx
  800dec:	83 c4 1c             	add    $0x1c,%esp
  800def:	5b                   	pop    %ebx
  800df0:	5e                   	pop    %esi
  800df1:	5f                   	pop    %edi
  800df2:	5d                   	pop    %ebp
  800df3:	c3                   	ret    
  800df4:	39 f0                	cmp    %esi,%eax
  800df6:	0f 87 ac 00 00 00    	ja     800ea8 <__umoddi3+0xfc>
  800dfc:	0f bd e8             	bsr    %eax,%ebp
  800dff:	83 f5 1f             	xor    $0x1f,%ebp
  800e02:	0f 84 ac 00 00 00    	je     800eb4 <__umoddi3+0x108>
  800e08:	bf 20 00 00 00       	mov    $0x20,%edi
  800e0d:	29 ef                	sub    %ebp,%edi
  800e0f:	89 fe                	mov    %edi,%esi
  800e11:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e15:	89 e9                	mov    %ebp,%ecx
  800e17:	d3 e0                	shl    %cl,%eax
  800e19:	89 d7                	mov    %edx,%edi
  800e1b:	89 f1                	mov    %esi,%ecx
  800e1d:	d3 ef                	shr    %cl,%edi
  800e1f:	09 c7                	or     %eax,%edi
  800e21:	89 e9                	mov    %ebp,%ecx
  800e23:	d3 e2                	shl    %cl,%edx
  800e25:	89 14 24             	mov    %edx,(%esp)
  800e28:	89 d8                	mov    %ebx,%eax
  800e2a:	d3 e0                	shl    %cl,%eax
  800e2c:	89 c2                	mov    %eax,%edx
  800e2e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e32:	d3 e0                	shl    %cl,%eax
  800e34:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e38:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e3c:	89 f1                	mov    %esi,%ecx
  800e3e:	d3 e8                	shr    %cl,%eax
  800e40:	09 d0                	or     %edx,%eax
  800e42:	d3 eb                	shr    %cl,%ebx
  800e44:	89 da                	mov    %ebx,%edx
  800e46:	f7 f7                	div    %edi
  800e48:	89 d3                	mov    %edx,%ebx
  800e4a:	f7 24 24             	mull   (%esp)
  800e4d:	89 c6                	mov    %eax,%esi
  800e4f:	89 d1                	mov    %edx,%ecx
  800e51:	39 d3                	cmp    %edx,%ebx
  800e53:	0f 82 87 00 00 00    	jb     800ee0 <__umoddi3+0x134>
  800e59:	0f 84 91 00 00 00    	je     800ef0 <__umoddi3+0x144>
  800e5f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e63:	29 f2                	sub    %esi,%edx
  800e65:	19 cb                	sbb    %ecx,%ebx
  800e67:	89 d8                	mov    %ebx,%eax
  800e69:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e6d:	d3 e0                	shl    %cl,%eax
  800e6f:	89 e9                	mov    %ebp,%ecx
  800e71:	d3 ea                	shr    %cl,%edx
  800e73:	09 d0                	or     %edx,%eax
  800e75:	89 e9                	mov    %ebp,%ecx
  800e77:	d3 eb                	shr    %cl,%ebx
  800e79:	89 da                	mov    %ebx,%edx
  800e7b:	83 c4 1c             	add    $0x1c,%esp
  800e7e:	5b                   	pop    %ebx
  800e7f:	5e                   	pop    %esi
  800e80:	5f                   	pop    %edi
  800e81:	5d                   	pop    %ebp
  800e82:	c3                   	ret    
  800e83:	90                   	nop
  800e84:	89 fd                	mov    %edi,%ebp
  800e86:	85 ff                	test   %edi,%edi
  800e88:	75 0b                	jne    800e95 <__umoddi3+0xe9>
  800e8a:	b8 01 00 00 00       	mov    $0x1,%eax
  800e8f:	31 d2                	xor    %edx,%edx
  800e91:	f7 f7                	div    %edi
  800e93:	89 c5                	mov    %eax,%ebp
  800e95:	89 f0                	mov    %esi,%eax
  800e97:	31 d2                	xor    %edx,%edx
  800e99:	f7 f5                	div    %ebp
  800e9b:	89 c8                	mov    %ecx,%eax
  800e9d:	f7 f5                	div    %ebp
  800e9f:	89 d0                	mov    %edx,%eax
  800ea1:	e9 44 ff ff ff       	jmp    800dea <__umoddi3+0x3e>
  800ea6:	66 90                	xchg   %ax,%ax
  800ea8:	89 c8                	mov    %ecx,%eax
  800eaa:	89 f2                	mov    %esi,%edx
  800eac:	83 c4 1c             	add    $0x1c,%esp
  800eaf:	5b                   	pop    %ebx
  800eb0:	5e                   	pop    %esi
  800eb1:	5f                   	pop    %edi
  800eb2:	5d                   	pop    %ebp
  800eb3:	c3                   	ret    
  800eb4:	3b 04 24             	cmp    (%esp),%eax
  800eb7:	72 06                	jb     800ebf <__umoddi3+0x113>
  800eb9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ebd:	77 0f                	ja     800ece <__umoddi3+0x122>
  800ebf:	89 f2                	mov    %esi,%edx
  800ec1:	29 f9                	sub    %edi,%ecx
  800ec3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ec7:	89 14 24             	mov    %edx,(%esp)
  800eca:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ece:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ed2:	8b 14 24             	mov    (%esp),%edx
  800ed5:	83 c4 1c             	add    $0x1c,%esp
  800ed8:	5b                   	pop    %ebx
  800ed9:	5e                   	pop    %esi
  800eda:	5f                   	pop    %edi
  800edb:	5d                   	pop    %ebp
  800edc:	c3                   	ret    
  800edd:	8d 76 00             	lea    0x0(%esi),%esi
  800ee0:	2b 04 24             	sub    (%esp),%eax
  800ee3:	19 fa                	sbb    %edi,%edx
  800ee5:	89 d1                	mov    %edx,%ecx
  800ee7:	89 c6                	mov    %eax,%esi
  800ee9:	e9 71 ff ff ff       	jmp    800e5f <__umoddi3+0xb3>
  800eee:	66 90                	xchg   %ax,%ax
  800ef0:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800ef4:	72 ea                	jb     800ee0 <__umoddi3+0x134>
  800ef6:	89 d9                	mov    %ebx,%ecx
  800ef8:	e9 62 ff ff ff       	jmp    800e5f <__umoddi3+0xb3>
