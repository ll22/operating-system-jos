
obj/user/buggyhello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 16 00 00 00       	call   800047 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs((char*)1, 1);
  800039:	6a 01                	push   $0x1
  80003b:	6a 01                	push   $0x1
  80003d:	e8 66 00 00 00       	call   8000a8 <sys_cputs>
}
  800042:	83 c4 10             	add    $0x10,%esp
  800045:	c9                   	leave  
  800046:	c3                   	ret    

00800047 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800047:	55                   	push   %ebp
  800048:	89 e5                	mov    %esp,%ebp
  80004a:	56                   	push   %esi
  80004b:	53                   	push   %ebx
  80004c:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004f:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800052:	e8 cf 00 00 00       	call   800126 <sys_getenvid>
  800057:	25 ff 03 00 00       	and    $0x3ff,%eax
  80005c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800063:	c1 e0 07             	shl    $0x7,%eax
  800066:	29 d0                	sub    %edx,%eax
  800068:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80006d:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800072:	85 db                	test   %ebx,%ebx
  800074:	7e 07                	jle    80007d <libmain+0x36>
		binaryname = argv[0];
  800076:	8b 06                	mov    (%esi),%eax
  800078:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80007d:	83 ec 08             	sub    $0x8,%esp
  800080:	56                   	push   %esi
  800081:	53                   	push   %ebx
  800082:	e8 ac ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800087:	e8 0a 00 00 00       	call   800096 <exit>
}
  80008c:	83 c4 10             	add    $0x10,%esp
  80008f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800092:	5b                   	pop    %ebx
  800093:	5e                   	pop    %esi
  800094:	5d                   	pop    %ebp
  800095:	c3                   	ret    

00800096 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800096:	55                   	push   %ebp
  800097:	89 e5                	mov    %esp,%ebp
  800099:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80009c:	6a 00                	push   $0x0
  80009e:	e8 42 00 00 00       	call   8000e5 <sys_env_destroy>
}
  8000a3:	83 c4 10             	add    $0x10,%esp
  8000a6:	c9                   	leave  
  8000a7:	c3                   	ret    

008000a8 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000a8:	55                   	push   %ebp
  8000a9:	89 e5                	mov    %esp,%ebp
  8000ab:	57                   	push   %edi
  8000ac:	56                   	push   %esi
  8000ad:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ae:	b8 00 00 00 00       	mov    $0x0,%eax
  8000b3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b6:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b9:	89 c3                	mov    %eax,%ebx
  8000bb:	89 c7                	mov    %eax,%edi
  8000bd:	89 c6                	mov    %eax,%esi
  8000bf:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c1:	5b                   	pop    %ebx
  8000c2:	5e                   	pop    %esi
  8000c3:	5f                   	pop    %edi
  8000c4:	5d                   	pop    %ebp
  8000c5:	c3                   	ret    

008000c6 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c6:	55                   	push   %ebp
  8000c7:	89 e5                	mov    %esp,%ebp
  8000c9:	57                   	push   %edi
  8000ca:	56                   	push   %esi
  8000cb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cc:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d1:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d6:	89 d1                	mov    %edx,%ecx
  8000d8:	89 d3                	mov    %edx,%ebx
  8000da:	89 d7                	mov    %edx,%edi
  8000dc:	89 d6                	mov    %edx,%esi
  8000de:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e0:	5b                   	pop    %ebx
  8000e1:	5e                   	pop    %esi
  8000e2:	5f                   	pop    %edi
  8000e3:	5d                   	pop    %ebp
  8000e4:	c3                   	ret    

008000e5 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e5:	55                   	push   %ebp
  8000e6:	89 e5                	mov    %esp,%ebp
  8000e8:	57                   	push   %edi
  8000e9:	56                   	push   %esi
  8000ea:	53                   	push   %ebx
  8000eb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ee:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000f3:	b8 03 00 00 00       	mov    $0x3,%eax
  8000f8:	8b 55 08             	mov    0x8(%ebp),%edx
  8000fb:	89 cb                	mov    %ecx,%ebx
  8000fd:	89 cf                	mov    %ecx,%edi
  8000ff:	89 ce                	mov    %ecx,%esi
  800101:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800103:	85 c0                	test   %eax,%eax
  800105:	7e 17                	jle    80011e <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800107:	83 ec 0c             	sub    $0xc,%esp
  80010a:	50                   	push   %eax
  80010b:	6a 03                	push   $0x3
  80010d:	68 0a 0f 80 00       	push   $0x800f0a
  800112:	6a 23                	push   $0x23
  800114:	68 27 0f 80 00       	push   $0x800f27
  800119:	e8 14 02 00 00       	call   800332 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80011e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800121:	5b                   	pop    %ebx
  800122:	5e                   	pop    %esi
  800123:	5f                   	pop    %edi
  800124:	5d                   	pop    %ebp
  800125:	c3                   	ret    

00800126 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800126:	55                   	push   %ebp
  800127:	89 e5                	mov    %esp,%ebp
  800129:	57                   	push   %edi
  80012a:	56                   	push   %esi
  80012b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012c:	ba 00 00 00 00       	mov    $0x0,%edx
  800131:	b8 02 00 00 00       	mov    $0x2,%eax
  800136:	89 d1                	mov    %edx,%ecx
  800138:	89 d3                	mov    %edx,%ebx
  80013a:	89 d7                	mov    %edx,%edi
  80013c:	89 d6                	mov    %edx,%esi
  80013e:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800140:	5b                   	pop    %ebx
  800141:	5e                   	pop    %esi
  800142:	5f                   	pop    %edi
  800143:	5d                   	pop    %ebp
  800144:	c3                   	ret    

00800145 <sys_yield>:

void
sys_yield(void)
{
  800145:	55                   	push   %ebp
  800146:	89 e5                	mov    %esp,%ebp
  800148:	57                   	push   %edi
  800149:	56                   	push   %esi
  80014a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80014b:	ba 00 00 00 00       	mov    $0x0,%edx
  800150:	b8 0a 00 00 00       	mov    $0xa,%eax
  800155:	89 d1                	mov    %edx,%ecx
  800157:	89 d3                	mov    %edx,%ebx
  800159:	89 d7                	mov    %edx,%edi
  80015b:	89 d6                	mov    %edx,%esi
  80015d:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80015f:	5b                   	pop    %ebx
  800160:	5e                   	pop    %esi
  800161:	5f                   	pop    %edi
  800162:	5d                   	pop    %ebp
  800163:	c3                   	ret    

00800164 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800164:	55                   	push   %ebp
  800165:	89 e5                	mov    %esp,%ebp
  800167:	57                   	push   %edi
  800168:	56                   	push   %esi
  800169:	53                   	push   %ebx
  80016a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80016d:	be 00 00 00 00       	mov    $0x0,%esi
  800172:	b8 04 00 00 00       	mov    $0x4,%eax
  800177:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80017a:	8b 55 08             	mov    0x8(%ebp),%edx
  80017d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800180:	89 f7                	mov    %esi,%edi
  800182:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800184:	85 c0                	test   %eax,%eax
  800186:	7e 17                	jle    80019f <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800188:	83 ec 0c             	sub    $0xc,%esp
  80018b:	50                   	push   %eax
  80018c:	6a 04                	push   $0x4
  80018e:	68 0a 0f 80 00       	push   $0x800f0a
  800193:	6a 23                	push   $0x23
  800195:	68 27 0f 80 00       	push   $0x800f27
  80019a:	e8 93 01 00 00       	call   800332 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  80019f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a2:	5b                   	pop    %ebx
  8001a3:	5e                   	pop    %esi
  8001a4:	5f                   	pop    %edi
  8001a5:	5d                   	pop    %ebp
  8001a6:	c3                   	ret    

008001a7 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001a7:	55                   	push   %ebp
  8001a8:	89 e5                	mov    %esp,%ebp
  8001aa:	57                   	push   %edi
  8001ab:	56                   	push   %esi
  8001ac:	53                   	push   %ebx
  8001ad:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001b0:	b8 05 00 00 00       	mov    $0x5,%eax
  8001b5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001b8:	8b 55 08             	mov    0x8(%ebp),%edx
  8001bb:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001be:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001c1:	8b 75 18             	mov    0x18(%ebp),%esi
  8001c4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c6:	85 c0                	test   %eax,%eax
  8001c8:	7e 17                	jle    8001e1 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001ca:	83 ec 0c             	sub    $0xc,%esp
  8001cd:	50                   	push   %eax
  8001ce:	6a 05                	push   $0x5
  8001d0:	68 0a 0f 80 00       	push   $0x800f0a
  8001d5:	6a 23                	push   $0x23
  8001d7:	68 27 0f 80 00       	push   $0x800f27
  8001dc:	e8 51 01 00 00       	call   800332 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001e1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001e4:	5b                   	pop    %ebx
  8001e5:	5e                   	pop    %esi
  8001e6:	5f                   	pop    %edi
  8001e7:	5d                   	pop    %ebp
  8001e8:	c3                   	ret    

008001e9 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001e9:	55                   	push   %ebp
  8001ea:	89 e5                	mov    %esp,%ebp
  8001ec:	57                   	push   %edi
  8001ed:	56                   	push   %esi
  8001ee:	53                   	push   %ebx
  8001ef:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001f2:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001f7:	b8 06 00 00 00       	mov    $0x6,%eax
  8001fc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001ff:	8b 55 08             	mov    0x8(%ebp),%edx
  800202:	89 df                	mov    %ebx,%edi
  800204:	89 de                	mov    %ebx,%esi
  800206:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800208:	85 c0                	test   %eax,%eax
  80020a:	7e 17                	jle    800223 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80020c:	83 ec 0c             	sub    $0xc,%esp
  80020f:	50                   	push   %eax
  800210:	6a 06                	push   $0x6
  800212:	68 0a 0f 80 00       	push   $0x800f0a
  800217:	6a 23                	push   $0x23
  800219:	68 27 0f 80 00       	push   $0x800f27
  80021e:	e8 0f 01 00 00       	call   800332 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800223:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800226:	5b                   	pop    %ebx
  800227:	5e                   	pop    %esi
  800228:	5f                   	pop    %edi
  800229:	5d                   	pop    %ebp
  80022a:	c3                   	ret    

0080022b <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80022b:	55                   	push   %ebp
  80022c:	89 e5                	mov    %esp,%ebp
  80022e:	57                   	push   %edi
  80022f:	56                   	push   %esi
  800230:	53                   	push   %ebx
  800231:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800234:	bb 00 00 00 00       	mov    $0x0,%ebx
  800239:	b8 08 00 00 00       	mov    $0x8,%eax
  80023e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800241:	8b 55 08             	mov    0x8(%ebp),%edx
  800244:	89 df                	mov    %ebx,%edi
  800246:	89 de                	mov    %ebx,%esi
  800248:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80024a:	85 c0                	test   %eax,%eax
  80024c:	7e 17                	jle    800265 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80024e:	83 ec 0c             	sub    $0xc,%esp
  800251:	50                   	push   %eax
  800252:	6a 08                	push   $0x8
  800254:	68 0a 0f 80 00       	push   $0x800f0a
  800259:	6a 23                	push   $0x23
  80025b:	68 27 0f 80 00       	push   $0x800f27
  800260:	e8 cd 00 00 00       	call   800332 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800265:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800268:	5b                   	pop    %ebx
  800269:	5e                   	pop    %esi
  80026a:	5f                   	pop    %edi
  80026b:	5d                   	pop    %ebp
  80026c:	c3                   	ret    

0080026d <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80026d:	55                   	push   %ebp
  80026e:	89 e5                	mov    %esp,%ebp
  800270:	57                   	push   %edi
  800271:	56                   	push   %esi
  800272:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800273:	ba 00 00 00 00       	mov    $0x0,%edx
  800278:	b8 0b 00 00 00       	mov    $0xb,%eax
  80027d:	89 d1                	mov    %edx,%ecx
  80027f:	89 d3                	mov    %edx,%ebx
  800281:	89 d7                	mov    %edx,%edi
  800283:	89 d6                	mov    %edx,%esi
  800285:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800287:	5b                   	pop    %ebx
  800288:	5e                   	pop    %esi
  800289:	5f                   	pop    %edi
  80028a:	5d                   	pop    %ebp
  80028b:	c3                   	ret    

0080028c <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80028c:	55                   	push   %ebp
  80028d:	89 e5                	mov    %esp,%ebp
  80028f:	57                   	push   %edi
  800290:	56                   	push   %esi
  800291:	53                   	push   %ebx
  800292:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800295:	bb 00 00 00 00       	mov    $0x0,%ebx
  80029a:	b8 09 00 00 00       	mov    $0x9,%eax
  80029f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002a2:	8b 55 08             	mov    0x8(%ebp),%edx
  8002a5:	89 df                	mov    %ebx,%edi
  8002a7:	89 de                	mov    %ebx,%esi
  8002a9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002ab:	85 c0                	test   %eax,%eax
  8002ad:	7e 17                	jle    8002c6 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002af:	83 ec 0c             	sub    $0xc,%esp
  8002b2:	50                   	push   %eax
  8002b3:	6a 09                	push   $0x9
  8002b5:	68 0a 0f 80 00       	push   $0x800f0a
  8002ba:	6a 23                	push   $0x23
  8002bc:	68 27 0f 80 00       	push   $0x800f27
  8002c1:	e8 6c 00 00 00       	call   800332 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002c6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c9:	5b                   	pop    %ebx
  8002ca:	5e                   	pop    %esi
  8002cb:	5f                   	pop    %edi
  8002cc:	5d                   	pop    %ebp
  8002cd:	c3                   	ret    

008002ce <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002ce:	55                   	push   %ebp
  8002cf:	89 e5                	mov    %esp,%ebp
  8002d1:	57                   	push   %edi
  8002d2:	56                   	push   %esi
  8002d3:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002d4:	be 00 00 00 00       	mov    $0x0,%esi
  8002d9:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002de:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002e1:	8b 55 08             	mov    0x8(%ebp),%edx
  8002e4:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002e7:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002ea:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002ec:	5b                   	pop    %ebx
  8002ed:	5e                   	pop    %esi
  8002ee:	5f                   	pop    %edi
  8002ef:	5d                   	pop    %ebp
  8002f0:	c3                   	ret    

008002f1 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002f1:	55                   	push   %ebp
  8002f2:	89 e5                	mov    %esp,%ebp
  8002f4:	57                   	push   %edi
  8002f5:	56                   	push   %esi
  8002f6:	53                   	push   %ebx
  8002f7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002fa:	b9 00 00 00 00       	mov    $0x0,%ecx
  8002ff:	b8 0d 00 00 00       	mov    $0xd,%eax
  800304:	8b 55 08             	mov    0x8(%ebp),%edx
  800307:	89 cb                	mov    %ecx,%ebx
  800309:	89 cf                	mov    %ecx,%edi
  80030b:	89 ce                	mov    %ecx,%esi
  80030d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80030f:	85 c0                	test   %eax,%eax
  800311:	7e 17                	jle    80032a <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800313:	83 ec 0c             	sub    $0xc,%esp
  800316:	50                   	push   %eax
  800317:	6a 0d                	push   $0xd
  800319:	68 0a 0f 80 00       	push   $0x800f0a
  80031e:	6a 23                	push   $0x23
  800320:	68 27 0f 80 00       	push   $0x800f27
  800325:	e8 08 00 00 00       	call   800332 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80032a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80032d:	5b                   	pop    %ebx
  80032e:	5e                   	pop    %esi
  80032f:	5f                   	pop    %edi
  800330:	5d                   	pop    %ebp
  800331:	c3                   	ret    

00800332 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800332:	55                   	push   %ebp
  800333:	89 e5                	mov    %esp,%ebp
  800335:	56                   	push   %esi
  800336:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800337:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80033a:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800340:	e8 e1 fd ff ff       	call   800126 <sys_getenvid>
  800345:	83 ec 0c             	sub    $0xc,%esp
  800348:	ff 75 0c             	pushl  0xc(%ebp)
  80034b:	ff 75 08             	pushl  0x8(%ebp)
  80034e:	56                   	push   %esi
  80034f:	50                   	push   %eax
  800350:	68 38 0f 80 00       	push   $0x800f38
  800355:	e8 b0 00 00 00       	call   80040a <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80035a:	83 c4 18             	add    $0x18,%esp
  80035d:	53                   	push   %ebx
  80035e:	ff 75 10             	pushl  0x10(%ebp)
  800361:	e8 53 00 00 00       	call   8003b9 <vcprintf>
	cprintf("\n");
  800366:	c7 04 24 5c 0f 80 00 	movl   $0x800f5c,(%esp)
  80036d:	e8 98 00 00 00       	call   80040a <cprintf>
  800372:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800375:	cc                   	int3   
  800376:	eb fd                	jmp    800375 <_panic+0x43>

00800378 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800378:	55                   	push   %ebp
  800379:	89 e5                	mov    %esp,%ebp
  80037b:	53                   	push   %ebx
  80037c:	83 ec 04             	sub    $0x4,%esp
  80037f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800382:	8b 13                	mov    (%ebx),%edx
  800384:	8d 42 01             	lea    0x1(%edx),%eax
  800387:	89 03                	mov    %eax,(%ebx)
  800389:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80038c:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800390:	3d ff 00 00 00       	cmp    $0xff,%eax
  800395:	75 1a                	jne    8003b1 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800397:	83 ec 08             	sub    $0x8,%esp
  80039a:	68 ff 00 00 00       	push   $0xff
  80039f:	8d 43 08             	lea    0x8(%ebx),%eax
  8003a2:	50                   	push   %eax
  8003a3:	e8 00 fd ff ff       	call   8000a8 <sys_cputs>
		b->idx = 0;
  8003a8:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003ae:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003b1:	ff 43 04             	incl   0x4(%ebx)
}
  8003b4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003b7:	c9                   	leave  
  8003b8:	c3                   	ret    

008003b9 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003b9:	55                   	push   %ebp
  8003ba:	89 e5                	mov    %esp,%ebp
  8003bc:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003c2:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003c9:	00 00 00 
	b.cnt = 0;
  8003cc:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003d3:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003d6:	ff 75 0c             	pushl  0xc(%ebp)
  8003d9:	ff 75 08             	pushl  0x8(%ebp)
  8003dc:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003e2:	50                   	push   %eax
  8003e3:	68 78 03 80 00       	push   $0x800378
  8003e8:	e8 51 01 00 00       	call   80053e <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003ed:	83 c4 08             	add    $0x8,%esp
  8003f0:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003f6:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003fc:	50                   	push   %eax
  8003fd:	e8 a6 fc ff ff       	call   8000a8 <sys_cputs>

	return b.cnt;
}
  800402:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800408:	c9                   	leave  
  800409:	c3                   	ret    

0080040a <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80040a:	55                   	push   %ebp
  80040b:	89 e5                	mov    %esp,%ebp
  80040d:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800410:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800413:	50                   	push   %eax
  800414:	ff 75 08             	pushl  0x8(%ebp)
  800417:	e8 9d ff ff ff       	call   8003b9 <vcprintf>
	va_end(ap);

	return cnt;
}
  80041c:	c9                   	leave  
  80041d:	c3                   	ret    

0080041e <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80041e:	55                   	push   %ebp
  80041f:	89 e5                	mov    %esp,%ebp
  800421:	57                   	push   %edi
  800422:	56                   	push   %esi
  800423:	53                   	push   %ebx
  800424:	83 ec 1c             	sub    $0x1c,%esp
  800427:	89 c7                	mov    %eax,%edi
  800429:	89 d6                	mov    %edx,%esi
  80042b:	8b 45 08             	mov    0x8(%ebp),%eax
  80042e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800431:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800434:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800437:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80043a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80043f:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800442:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800445:	39 d3                	cmp    %edx,%ebx
  800447:	72 05                	jb     80044e <printnum+0x30>
  800449:	39 45 10             	cmp    %eax,0x10(%ebp)
  80044c:	77 45                	ja     800493 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80044e:	83 ec 0c             	sub    $0xc,%esp
  800451:	ff 75 18             	pushl  0x18(%ebp)
  800454:	8b 45 14             	mov    0x14(%ebp),%eax
  800457:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80045a:	53                   	push   %ebx
  80045b:	ff 75 10             	pushl  0x10(%ebp)
  80045e:	83 ec 08             	sub    $0x8,%esp
  800461:	ff 75 e4             	pushl  -0x1c(%ebp)
  800464:	ff 75 e0             	pushl  -0x20(%ebp)
  800467:	ff 75 dc             	pushl  -0x24(%ebp)
  80046a:	ff 75 d8             	pushl  -0x28(%ebp)
  80046d:	e8 26 08 00 00       	call   800c98 <__udivdi3>
  800472:	83 c4 18             	add    $0x18,%esp
  800475:	52                   	push   %edx
  800476:	50                   	push   %eax
  800477:	89 f2                	mov    %esi,%edx
  800479:	89 f8                	mov    %edi,%eax
  80047b:	e8 9e ff ff ff       	call   80041e <printnum>
  800480:	83 c4 20             	add    $0x20,%esp
  800483:	eb 16                	jmp    80049b <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800485:	83 ec 08             	sub    $0x8,%esp
  800488:	56                   	push   %esi
  800489:	ff 75 18             	pushl  0x18(%ebp)
  80048c:	ff d7                	call   *%edi
  80048e:	83 c4 10             	add    $0x10,%esp
  800491:	eb 03                	jmp    800496 <printnum+0x78>
  800493:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800496:	4b                   	dec    %ebx
  800497:	85 db                	test   %ebx,%ebx
  800499:	7f ea                	jg     800485 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80049b:	83 ec 08             	sub    $0x8,%esp
  80049e:	56                   	push   %esi
  80049f:	83 ec 04             	sub    $0x4,%esp
  8004a2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a5:	ff 75 e0             	pushl  -0x20(%ebp)
  8004a8:	ff 75 dc             	pushl  -0x24(%ebp)
  8004ab:	ff 75 d8             	pushl  -0x28(%ebp)
  8004ae:	e8 f5 08 00 00       	call   800da8 <__umoddi3>
  8004b3:	83 c4 14             	add    $0x14,%esp
  8004b6:	0f be 80 5e 0f 80 00 	movsbl 0x800f5e(%eax),%eax
  8004bd:	50                   	push   %eax
  8004be:	ff d7                	call   *%edi
}
  8004c0:	83 c4 10             	add    $0x10,%esp
  8004c3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004c6:	5b                   	pop    %ebx
  8004c7:	5e                   	pop    %esi
  8004c8:	5f                   	pop    %edi
  8004c9:	5d                   	pop    %ebp
  8004ca:	c3                   	ret    

008004cb <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004cb:	55                   	push   %ebp
  8004cc:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004ce:	83 fa 01             	cmp    $0x1,%edx
  8004d1:	7e 0e                	jle    8004e1 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004d3:	8b 10                	mov    (%eax),%edx
  8004d5:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004d8:	89 08                	mov    %ecx,(%eax)
  8004da:	8b 02                	mov    (%edx),%eax
  8004dc:	8b 52 04             	mov    0x4(%edx),%edx
  8004df:	eb 22                	jmp    800503 <getuint+0x38>
	else if (lflag)
  8004e1:	85 d2                	test   %edx,%edx
  8004e3:	74 10                	je     8004f5 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004e5:	8b 10                	mov    (%eax),%edx
  8004e7:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004ea:	89 08                	mov    %ecx,(%eax)
  8004ec:	8b 02                	mov    (%edx),%eax
  8004ee:	ba 00 00 00 00       	mov    $0x0,%edx
  8004f3:	eb 0e                	jmp    800503 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004f5:	8b 10                	mov    (%eax),%edx
  8004f7:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004fa:	89 08                	mov    %ecx,(%eax)
  8004fc:	8b 02                	mov    (%edx),%eax
  8004fe:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800503:	5d                   	pop    %ebp
  800504:	c3                   	ret    

00800505 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800505:	55                   	push   %ebp
  800506:	89 e5                	mov    %esp,%ebp
  800508:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80050b:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80050e:	8b 10                	mov    (%eax),%edx
  800510:	3b 50 04             	cmp    0x4(%eax),%edx
  800513:	73 0a                	jae    80051f <sprintputch+0x1a>
		*b->buf++ = ch;
  800515:	8d 4a 01             	lea    0x1(%edx),%ecx
  800518:	89 08                	mov    %ecx,(%eax)
  80051a:	8b 45 08             	mov    0x8(%ebp),%eax
  80051d:	88 02                	mov    %al,(%edx)
}
  80051f:	5d                   	pop    %ebp
  800520:	c3                   	ret    

00800521 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800521:	55                   	push   %ebp
  800522:	89 e5                	mov    %esp,%ebp
  800524:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800527:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80052a:	50                   	push   %eax
  80052b:	ff 75 10             	pushl  0x10(%ebp)
  80052e:	ff 75 0c             	pushl  0xc(%ebp)
  800531:	ff 75 08             	pushl  0x8(%ebp)
  800534:	e8 05 00 00 00       	call   80053e <vprintfmt>
	va_end(ap);
}
  800539:	83 c4 10             	add    $0x10,%esp
  80053c:	c9                   	leave  
  80053d:	c3                   	ret    

0080053e <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80053e:	55                   	push   %ebp
  80053f:	89 e5                	mov    %esp,%ebp
  800541:	57                   	push   %edi
  800542:	56                   	push   %esi
  800543:	53                   	push   %ebx
  800544:	83 ec 2c             	sub    $0x2c,%esp
  800547:	8b 75 08             	mov    0x8(%ebp),%esi
  80054a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80054d:	8b 7d 10             	mov    0x10(%ebp),%edi
  800550:	eb 12                	jmp    800564 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800552:	85 c0                	test   %eax,%eax
  800554:	0f 84 68 03 00 00    	je     8008c2 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80055a:	83 ec 08             	sub    $0x8,%esp
  80055d:	53                   	push   %ebx
  80055e:	50                   	push   %eax
  80055f:	ff d6                	call   *%esi
  800561:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800564:	47                   	inc    %edi
  800565:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800569:	83 f8 25             	cmp    $0x25,%eax
  80056c:	75 e4                	jne    800552 <vprintfmt+0x14>
  80056e:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800572:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800579:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800580:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800587:	ba 00 00 00 00       	mov    $0x0,%edx
  80058c:	eb 07                	jmp    800595 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80058e:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800591:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800595:	8d 47 01             	lea    0x1(%edi),%eax
  800598:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80059b:	0f b6 0f             	movzbl (%edi),%ecx
  80059e:	8a 07                	mov    (%edi),%al
  8005a0:	83 e8 23             	sub    $0x23,%eax
  8005a3:	3c 55                	cmp    $0x55,%al
  8005a5:	0f 87 fe 02 00 00    	ja     8008a9 <vprintfmt+0x36b>
  8005ab:	0f b6 c0             	movzbl %al,%eax
  8005ae:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8005b5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005b8:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005bc:	eb d7                	jmp    800595 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005be:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005c1:	b8 00 00 00 00       	mov    $0x0,%eax
  8005c6:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005c9:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005cc:	01 c0                	add    %eax,%eax
  8005ce:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005d2:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005d5:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005d8:	83 fa 09             	cmp    $0x9,%edx
  8005db:	77 34                	ja     800611 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005dd:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005de:	eb e9                	jmp    8005c9 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005e0:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e3:	8d 48 04             	lea    0x4(%eax),%ecx
  8005e6:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005e9:	8b 00                	mov    (%eax),%eax
  8005eb:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ee:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005f1:	eb 24                	jmp    800617 <vprintfmt+0xd9>
  8005f3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005f7:	79 07                	jns    800600 <vprintfmt+0xc2>
  8005f9:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800600:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800603:	eb 90                	jmp    800595 <vprintfmt+0x57>
  800605:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800608:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80060f:	eb 84                	jmp    800595 <vprintfmt+0x57>
  800611:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800614:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800617:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80061b:	0f 89 74 ff ff ff    	jns    800595 <vprintfmt+0x57>
				width = precision, precision = -1;
  800621:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800624:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800627:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80062e:	e9 62 ff ff ff       	jmp    800595 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800633:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800634:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800637:	e9 59 ff ff ff       	jmp    800595 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80063c:	8b 45 14             	mov    0x14(%ebp),%eax
  80063f:	8d 50 04             	lea    0x4(%eax),%edx
  800642:	89 55 14             	mov    %edx,0x14(%ebp)
  800645:	83 ec 08             	sub    $0x8,%esp
  800648:	53                   	push   %ebx
  800649:	ff 30                	pushl  (%eax)
  80064b:	ff d6                	call   *%esi
			break;
  80064d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800650:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800653:	e9 0c ff ff ff       	jmp    800564 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800658:	8b 45 14             	mov    0x14(%ebp),%eax
  80065b:	8d 50 04             	lea    0x4(%eax),%edx
  80065e:	89 55 14             	mov    %edx,0x14(%ebp)
  800661:	8b 00                	mov    (%eax),%eax
  800663:	85 c0                	test   %eax,%eax
  800665:	79 02                	jns    800669 <vprintfmt+0x12b>
  800667:	f7 d8                	neg    %eax
  800669:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80066b:	83 f8 08             	cmp    $0x8,%eax
  80066e:	7f 0b                	jg     80067b <vprintfmt+0x13d>
  800670:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  800677:	85 c0                	test   %eax,%eax
  800679:	75 18                	jne    800693 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80067b:	52                   	push   %edx
  80067c:	68 76 0f 80 00       	push   $0x800f76
  800681:	53                   	push   %ebx
  800682:	56                   	push   %esi
  800683:	e8 99 fe ff ff       	call   800521 <printfmt>
  800688:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80068b:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80068e:	e9 d1 fe ff ff       	jmp    800564 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800693:	50                   	push   %eax
  800694:	68 7f 0f 80 00       	push   $0x800f7f
  800699:	53                   	push   %ebx
  80069a:	56                   	push   %esi
  80069b:	e8 81 fe ff ff       	call   800521 <printfmt>
  8006a0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006a6:	e9 b9 fe ff ff       	jmp    800564 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006ab:	8b 45 14             	mov    0x14(%ebp),%eax
  8006ae:	8d 50 04             	lea    0x4(%eax),%edx
  8006b1:	89 55 14             	mov    %edx,0x14(%ebp)
  8006b4:	8b 38                	mov    (%eax),%edi
  8006b6:	85 ff                	test   %edi,%edi
  8006b8:	75 05                	jne    8006bf <vprintfmt+0x181>
				p = "(null)";
  8006ba:	bf 6f 0f 80 00       	mov    $0x800f6f,%edi
			if (width > 0 && padc != '-')
  8006bf:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006c3:	0f 8e 90 00 00 00    	jle    800759 <vprintfmt+0x21b>
  8006c9:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006cd:	0f 84 8e 00 00 00    	je     800761 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006d3:	83 ec 08             	sub    $0x8,%esp
  8006d6:	ff 75 d0             	pushl  -0x30(%ebp)
  8006d9:	57                   	push   %edi
  8006da:	e8 70 02 00 00       	call   80094f <strnlen>
  8006df:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006e2:	29 c1                	sub    %eax,%ecx
  8006e4:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006e7:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006ea:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006ee:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006f1:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006f4:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f6:	eb 0d                	jmp    800705 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006f8:	83 ec 08             	sub    $0x8,%esp
  8006fb:	53                   	push   %ebx
  8006fc:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006ff:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800701:	4f                   	dec    %edi
  800702:	83 c4 10             	add    $0x10,%esp
  800705:	85 ff                	test   %edi,%edi
  800707:	7f ef                	jg     8006f8 <vprintfmt+0x1ba>
  800709:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80070c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80070f:	89 c8                	mov    %ecx,%eax
  800711:	85 c9                	test   %ecx,%ecx
  800713:	79 05                	jns    80071a <vprintfmt+0x1dc>
  800715:	b8 00 00 00 00       	mov    $0x0,%eax
  80071a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80071d:	29 c1                	sub    %eax,%ecx
  80071f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800722:	89 75 08             	mov    %esi,0x8(%ebp)
  800725:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800728:	eb 3d                	jmp    800767 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80072a:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80072e:	74 19                	je     800749 <vprintfmt+0x20b>
  800730:	0f be c0             	movsbl %al,%eax
  800733:	83 e8 20             	sub    $0x20,%eax
  800736:	83 f8 5e             	cmp    $0x5e,%eax
  800739:	76 0e                	jbe    800749 <vprintfmt+0x20b>
					putch('?', putdat);
  80073b:	83 ec 08             	sub    $0x8,%esp
  80073e:	53                   	push   %ebx
  80073f:	6a 3f                	push   $0x3f
  800741:	ff 55 08             	call   *0x8(%ebp)
  800744:	83 c4 10             	add    $0x10,%esp
  800747:	eb 0b                	jmp    800754 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800749:	83 ec 08             	sub    $0x8,%esp
  80074c:	53                   	push   %ebx
  80074d:	52                   	push   %edx
  80074e:	ff 55 08             	call   *0x8(%ebp)
  800751:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800754:	ff 4d e4             	decl   -0x1c(%ebp)
  800757:	eb 0e                	jmp    800767 <vprintfmt+0x229>
  800759:	89 75 08             	mov    %esi,0x8(%ebp)
  80075c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075f:	eb 06                	jmp    800767 <vprintfmt+0x229>
  800761:	89 75 08             	mov    %esi,0x8(%ebp)
  800764:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800767:	47                   	inc    %edi
  800768:	8a 47 ff             	mov    -0x1(%edi),%al
  80076b:	0f be d0             	movsbl %al,%edx
  80076e:	85 d2                	test   %edx,%edx
  800770:	74 1d                	je     80078f <vprintfmt+0x251>
  800772:	85 f6                	test   %esi,%esi
  800774:	78 b4                	js     80072a <vprintfmt+0x1ec>
  800776:	4e                   	dec    %esi
  800777:	79 b1                	jns    80072a <vprintfmt+0x1ec>
  800779:	8b 75 08             	mov    0x8(%ebp),%esi
  80077c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80077f:	eb 14                	jmp    800795 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800781:	83 ec 08             	sub    $0x8,%esp
  800784:	53                   	push   %ebx
  800785:	6a 20                	push   $0x20
  800787:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800789:	4f                   	dec    %edi
  80078a:	83 c4 10             	add    $0x10,%esp
  80078d:	eb 06                	jmp    800795 <vprintfmt+0x257>
  80078f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800792:	8b 75 08             	mov    0x8(%ebp),%esi
  800795:	85 ff                	test   %edi,%edi
  800797:	7f e8                	jg     800781 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800799:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80079c:	e9 c3 fd ff ff       	jmp    800564 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007a1:	83 fa 01             	cmp    $0x1,%edx
  8007a4:	7e 16                	jle    8007bc <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007a6:	8b 45 14             	mov    0x14(%ebp),%eax
  8007a9:	8d 50 08             	lea    0x8(%eax),%edx
  8007ac:	89 55 14             	mov    %edx,0x14(%ebp)
  8007af:	8b 50 04             	mov    0x4(%eax),%edx
  8007b2:	8b 00                	mov    (%eax),%eax
  8007b4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007b7:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ba:	eb 32                	jmp    8007ee <vprintfmt+0x2b0>
	else if (lflag)
  8007bc:	85 d2                	test   %edx,%edx
  8007be:	74 18                	je     8007d8 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007c0:	8b 45 14             	mov    0x14(%ebp),%eax
  8007c3:	8d 50 04             	lea    0x4(%eax),%edx
  8007c6:	89 55 14             	mov    %edx,0x14(%ebp)
  8007c9:	8b 00                	mov    (%eax),%eax
  8007cb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007ce:	89 c1                	mov    %eax,%ecx
  8007d0:	c1 f9 1f             	sar    $0x1f,%ecx
  8007d3:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007d6:	eb 16                	jmp    8007ee <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007d8:	8b 45 14             	mov    0x14(%ebp),%eax
  8007db:	8d 50 04             	lea    0x4(%eax),%edx
  8007de:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e1:	8b 00                	mov    (%eax),%eax
  8007e3:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007e6:	89 c1                	mov    %eax,%ecx
  8007e8:	c1 f9 1f             	sar    $0x1f,%ecx
  8007eb:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007ee:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007f1:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007f4:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007f8:	79 76                	jns    800870 <vprintfmt+0x332>
				putch('-', putdat);
  8007fa:	83 ec 08             	sub    $0x8,%esp
  8007fd:	53                   	push   %ebx
  8007fe:	6a 2d                	push   $0x2d
  800800:	ff d6                	call   *%esi
				num = -(long long) num;
  800802:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800805:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800808:	f7 d8                	neg    %eax
  80080a:	83 d2 00             	adc    $0x0,%edx
  80080d:	f7 da                	neg    %edx
  80080f:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800812:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800817:	eb 5c                	jmp    800875 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800819:	8d 45 14             	lea    0x14(%ebp),%eax
  80081c:	e8 aa fc ff ff       	call   8004cb <getuint>
			base = 10;
  800821:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800826:	eb 4d                	jmp    800875 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800828:	8d 45 14             	lea    0x14(%ebp),%eax
  80082b:	e8 9b fc ff ff       	call   8004cb <getuint>
			base = 8;
  800830:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800835:	eb 3e                	jmp    800875 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800837:	83 ec 08             	sub    $0x8,%esp
  80083a:	53                   	push   %ebx
  80083b:	6a 30                	push   $0x30
  80083d:	ff d6                	call   *%esi
			putch('x', putdat);
  80083f:	83 c4 08             	add    $0x8,%esp
  800842:	53                   	push   %ebx
  800843:	6a 78                	push   $0x78
  800845:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800847:	8b 45 14             	mov    0x14(%ebp),%eax
  80084a:	8d 50 04             	lea    0x4(%eax),%edx
  80084d:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800850:	8b 00                	mov    (%eax),%eax
  800852:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800857:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80085a:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80085f:	eb 14                	jmp    800875 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800861:	8d 45 14             	lea    0x14(%ebp),%eax
  800864:	e8 62 fc ff ff       	call   8004cb <getuint>
			base = 16;
  800869:	b9 10 00 00 00       	mov    $0x10,%ecx
  80086e:	eb 05                	jmp    800875 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800870:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800875:	83 ec 0c             	sub    $0xc,%esp
  800878:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80087c:	57                   	push   %edi
  80087d:	ff 75 e4             	pushl  -0x1c(%ebp)
  800880:	51                   	push   %ecx
  800881:	52                   	push   %edx
  800882:	50                   	push   %eax
  800883:	89 da                	mov    %ebx,%edx
  800885:	89 f0                	mov    %esi,%eax
  800887:	e8 92 fb ff ff       	call   80041e <printnum>
			break;
  80088c:	83 c4 20             	add    $0x20,%esp
  80088f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800892:	e9 cd fc ff ff       	jmp    800564 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800897:	83 ec 08             	sub    $0x8,%esp
  80089a:	53                   	push   %ebx
  80089b:	51                   	push   %ecx
  80089c:	ff d6                	call   *%esi
			break;
  80089e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008a1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008a4:	e9 bb fc ff ff       	jmp    800564 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008a9:	83 ec 08             	sub    $0x8,%esp
  8008ac:	53                   	push   %ebx
  8008ad:	6a 25                	push   $0x25
  8008af:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008b1:	83 c4 10             	add    $0x10,%esp
  8008b4:	eb 01                	jmp    8008b7 <vprintfmt+0x379>
  8008b6:	4f                   	dec    %edi
  8008b7:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008bb:	75 f9                	jne    8008b6 <vprintfmt+0x378>
  8008bd:	e9 a2 fc ff ff       	jmp    800564 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008c2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008c5:	5b                   	pop    %ebx
  8008c6:	5e                   	pop    %esi
  8008c7:	5f                   	pop    %edi
  8008c8:	5d                   	pop    %ebp
  8008c9:	c3                   	ret    

008008ca <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008ca:	55                   	push   %ebp
  8008cb:	89 e5                	mov    %esp,%ebp
  8008cd:	83 ec 18             	sub    $0x18,%esp
  8008d0:	8b 45 08             	mov    0x8(%ebp),%eax
  8008d3:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008d6:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008d9:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008dd:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008e0:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008e7:	85 c0                	test   %eax,%eax
  8008e9:	74 26                	je     800911 <vsnprintf+0x47>
  8008eb:	85 d2                	test   %edx,%edx
  8008ed:	7e 29                	jle    800918 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008ef:	ff 75 14             	pushl  0x14(%ebp)
  8008f2:	ff 75 10             	pushl  0x10(%ebp)
  8008f5:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008f8:	50                   	push   %eax
  8008f9:	68 05 05 80 00       	push   $0x800505
  8008fe:	e8 3b fc ff ff       	call   80053e <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800903:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800906:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800909:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80090c:	83 c4 10             	add    $0x10,%esp
  80090f:	eb 0c                	jmp    80091d <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800911:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800916:	eb 05                	jmp    80091d <vsnprintf+0x53>
  800918:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80091d:	c9                   	leave  
  80091e:	c3                   	ret    

0080091f <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80091f:	55                   	push   %ebp
  800920:	89 e5                	mov    %esp,%ebp
  800922:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800925:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800928:	50                   	push   %eax
  800929:	ff 75 10             	pushl  0x10(%ebp)
  80092c:	ff 75 0c             	pushl  0xc(%ebp)
  80092f:	ff 75 08             	pushl  0x8(%ebp)
  800932:	e8 93 ff ff ff       	call   8008ca <vsnprintf>
	va_end(ap);

	return rc;
}
  800937:	c9                   	leave  
  800938:	c3                   	ret    

00800939 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800939:	55                   	push   %ebp
  80093a:	89 e5                	mov    %esp,%ebp
  80093c:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80093f:	b8 00 00 00 00       	mov    $0x0,%eax
  800944:	eb 01                	jmp    800947 <strlen+0xe>
		n++;
  800946:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800947:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80094b:	75 f9                	jne    800946 <strlen+0xd>
		n++;
	return n;
}
  80094d:	5d                   	pop    %ebp
  80094e:	c3                   	ret    

0080094f <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80094f:	55                   	push   %ebp
  800950:	89 e5                	mov    %esp,%ebp
  800952:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800955:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800958:	ba 00 00 00 00       	mov    $0x0,%edx
  80095d:	eb 01                	jmp    800960 <strnlen+0x11>
		n++;
  80095f:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800960:	39 c2                	cmp    %eax,%edx
  800962:	74 08                	je     80096c <strnlen+0x1d>
  800964:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800968:	75 f5                	jne    80095f <strnlen+0x10>
  80096a:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80096c:	5d                   	pop    %ebp
  80096d:	c3                   	ret    

0080096e <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80096e:	55                   	push   %ebp
  80096f:	89 e5                	mov    %esp,%ebp
  800971:	53                   	push   %ebx
  800972:	8b 45 08             	mov    0x8(%ebp),%eax
  800975:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800978:	89 c2                	mov    %eax,%edx
  80097a:	42                   	inc    %edx
  80097b:	41                   	inc    %ecx
  80097c:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80097f:	88 5a ff             	mov    %bl,-0x1(%edx)
  800982:	84 db                	test   %bl,%bl
  800984:	75 f4                	jne    80097a <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800986:	5b                   	pop    %ebx
  800987:	5d                   	pop    %ebp
  800988:	c3                   	ret    

00800989 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800989:	55                   	push   %ebp
  80098a:	89 e5                	mov    %esp,%ebp
  80098c:	53                   	push   %ebx
  80098d:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800990:	53                   	push   %ebx
  800991:	e8 a3 ff ff ff       	call   800939 <strlen>
  800996:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800999:	ff 75 0c             	pushl  0xc(%ebp)
  80099c:	01 d8                	add    %ebx,%eax
  80099e:	50                   	push   %eax
  80099f:	e8 ca ff ff ff       	call   80096e <strcpy>
	return dst;
}
  8009a4:	89 d8                	mov    %ebx,%eax
  8009a6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009a9:	c9                   	leave  
  8009aa:	c3                   	ret    

008009ab <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009ab:	55                   	push   %ebp
  8009ac:	89 e5                	mov    %esp,%ebp
  8009ae:	56                   	push   %esi
  8009af:	53                   	push   %ebx
  8009b0:	8b 75 08             	mov    0x8(%ebp),%esi
  8009b3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009b6:	89 f3                	mov    %esi,%ebx
  8009b8:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009bb:	89 f2                	mov    %esi,%edx
  8009bd:	eb 0c                	jmp    8009cb <strncpy+0x20>
		*dst++ = *src;
  8009bf:	42                   	inc    %edx
  8009c0:	8a 01                	mov    (%ecx),%al
  8009c2:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009c5:	80 39 01             	cmpb   $0x1,(%ecx)
  8009c8:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009cb:	39 da                	cmp    %ebx,%edx
  8009cd:	75 f0                	jne    8009bf <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009cf:	89 f0                	mov    %esi,%eax
  8009d1:	5b                   	pop    %ebx
  8009d2:	5e                   	pop    %esi
  8009d3:	5d                   	pop    %ebp
  8009d4:	c3                   	ret    

008009d5 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009d5:	55                   	push   %ebp
  8009d6:	89 e5                	mov    %esp,%ebp
  8009d8:	56                   	push   %esi
  8009d9:	53                   	push   %ebx
  8009da:	8b 75 08             	mov    0x8(%ebp),%esi
  8009dd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009e0:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009e3:	85 c0                	test   %eax,%eax
  8009e5:	74 1e                	je     800a05 <strlcpy+0x30>
  8009e7:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009eb:	89 f2                	mov    %esi,%edx
  8009ed:	eb 05                	jmp    8009f4 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009ef:	42                   	inc    %edx
  8009f0:	41                   	inc    %ecx
  8009f1:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009f4:	39 c2                	cmp    %eax,%edx
  8009f6:	74 08                	je     800a00 <strlcpy+0x2b>
  8009f8:	8a 19                	mov    (%ecx),%bl
  8009fa:	84 db                	test   %bl,%bl
  8009fc:	75 f1                	jne    8009ef <strlcpy+0x1a>
  8009fe:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a00:	c6 00 00             	movb   $0x0,(%eax)
  800a03:	eb 02                	jmp    800a07 <strlcpy+0x32>
  800a05:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a07:	29 f0                	sub    %esi,%eax
}
  800a09:	5b                   	pop    %ebx
  800a0a:	5e                   	pop    %esi
  800a0b:	5d                   	pop    %ebp
  800a0c:	c3                   	ret    

00800a0d <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a0d:	55                   	push   %ebp
  800a0e:	89 e5                	mov    %esp,%ebp
  800a10:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a13:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a16:	eb 02                	jmp    800a1a <strcmp+0xd>
		p++, q++;
  800a18:	41                   	inc    %ecx
  800a19:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a1a:	8a 01                	mov    (%ecx),%al
  800a1c:	84 c0                	test   %al,%al
  800a1e:	74 04                	je     800a24 <strcmp+0x17>
  800a20:	3a 02                	cmp    (%edx),%al
  800a22:	74 f4                	je     800a18 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a24:	0f b6 c0             	movzbl %al,%eax
  800a27:	0f b6 12             	movzbl (%edx),%edx
  800a2a:	29 d0                	sub    %edx,%eax
}
  800a2c:	5d                   	pop    %ebp
  800a2d:	c3                   	ret    

00800a2e <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a2e:	55                   	push   %ebp
  800a2f:	89 e5                	mov    %esp,%ebp
  800a31:	53                   	push   %ebx
  800a32:	8b 45 08             	mov    0x8(%ebp),%eax
  800a35:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a38:	89 c3                	mov    %eax,%ebx
  800a3a:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a3d:	eb 02                	jmp    800a41 <strncmp+0x13>
		n--, p++, q++;
  800a3f:	40                   	inc    %eax
  800a40:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a41:	39 d8                	cmp    %ebx,%eax
  800a43:	74 14                	je     800a59 <strncmp+0x2b>
  800a45:	8a 08                	mov    (%eax),%cl
  800a47:	84 c9                	test   %cl,%cl
  800a49:	74 04                	je     800a4f <strncmp+0x21>
  800a4b:	3a 0a                	cmp    (%edx),%cl
  800a4d:	74 f0                	je     800a3f <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a4f:	0f b6 00             	movzbl (%eax),%eax
  800a52:	0f b6 12             	movzbl (%edx),%edx
  800a55:	29 d0                	sub    %edx,%eax
  800a57:	eb 05                	jmp    800a5e <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a59:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a5e:	5b                   	pop    %ebx
  800a5f:	5d                   	pop    %ebp
  800a60:	c3                   	ret    

00800a61 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a61:	55                   	push   %ebp
  800a62:	89 e5                	mov    %esp,%ebp
  800a64:	8b 45 08             	mov    0x8(%ebp),%eax
  800a67:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a6a:	eb 05                	jmp    800a71 <strchr+0x10>
		if (*s == c)
  800a6c:	38 ca                	cmp    %cl,%dl
  800a6e:	74 0c                	je     800a7c <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a70:	40                   	inc    %eax
  800a71:	8a 10                	mov    (%eax),%dl
  800a73:	84 d2                	test   %dl,%dl
  800a75:	75 f5                	jne    800a6c <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a77:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a7c:	5d                   	pop    %ebp
  800a7d:	c3                   	ret    

00800a7e <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a7e:	55                   	push   %ebp
  800a7f:	89 e5                	mov    %esp,%ebp
  800a81:	8b 45 08             	mov    0x8(%ebp),%eax
  800a84:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a87:	eb 05                	jmp    800a8e <strfind+0x10>
		if (*s == c)
  800a89:	38 ca                	cmp    %cl,%dl
  800a8b:	74 07                	je     800a94 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a8d:	40                   	inc    %eax
  800a8e:	8a 10                	mov    (%eax),%dl
  800a90:	84 d2                	test   %dl,%dl
  800a92:	75 f5                	jne    800a89 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a94:	5d                   	pop    %ebp
  800a95:	c3                   	ret    

00800a96 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a96:	55                   	push   %ebp
  800a97:	89 e5                	mov    %esp,%ebp
  800a99:	57                   	push   %edi
  800a9a:	56                   	push   %esi
  800a9b:	53                   	push   %ebx
  800a9c:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a9f:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800aa2:	85 c9                	test   %ecx,%ecx
  800aa4:	74 36                	je     800adc <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800aa6:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800aac:	75 28                	jne    800ad6 <memset+0x40>
  800aae:	f6 c1 03             	test   $0x3,%cl
  800ab1:	75 23                	jne    800ad6 <memset+0x40>
		c &= 0xFF;
  800ab3:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800ab7:	89 d3                	mov    %edx,%ebx
  800ab9:	c1 e3 08             	shl    $0x8,%ebx
  800abc:	89 d6                	mov    %edx,%esi
  800abe:	c1 e6 18             	shl    $0x18,%esi
  800ac1:	89 d0                	mov    %edx,%eax
  800ac3:	c1 e0 10             	shl    $0x10,%eax
  800ac6:	09 f0                	or     %esi,%eax
  800ac8:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800aca:	89 d8                	mov    %ebx,%eax
  800acc:	09 d0                	or     %edx,%eax
  800ace:	c1 e9 02             	shr    $0x2,%ecx
  800ad1:	fc                   	cld    
  800ad2:	f3 ab                	rep stos %eax,%es:(%edi)
  800ad4:	eb 06                	jmp    800adc <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ad6:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ad9:	fc                   	cld    
  800ada:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800adc:	89 f8                	mov    %edi,%eax
  800ade:	5b                   	pop    %ebx
  800adf:	5e                   	pop    %esi
  800ae0:	5f                   	pop    %edi
  800ae1:	5d                   	pop    %ebp
  800ae2:	c3                   	ret    

00800ae3 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ae3:	55                   	push   %ebp
  800ae4:	89 e5                	mov    %esp,%ebp
  800ae6:	57                   	push   %edi
  800ae7:	56                   	push   %esi
  800ae8:	8b 45 08             	mov    0x8(%ebp),%eax
  800aeb:	8b 75 0c             	mov    0xc(%ebp),%esi
  800aee:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800af1:	39 c6                	cmp    %eax,%esi
  800af3:	73 33                	jae    800b28 <memmove+0x45>
  800af5:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800af8:	39 d0                	cmp    %edx,%eax
  800afa:	73 2c                	jae    800b28 <memmove+0x45>
		s += n;
		d += n;
  800afc:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800aff:	89 d6                	mov    %edx,%esi
  800b01:	09 fe                	or     %edi,%esi
  800b03:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b09:	75 13                	jne    800b1e <memmove+0x3b>
  800b0b:	f6 c1 03             	test   $0x3,%cl
  800b0e:	75 0e                	jne    800b1e <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b10:	83 ef 04             	sub    $0x4,%edi
  800b13:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b16:	c1 e9 02             	shr    $0x2,%ecx
  800b19:	fd                   	std    
  800b1a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b1c:	eb 07                	jmp    800b25 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b1e:	4f                   	dec    %edi
  800b1f:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b22:	fd                   	std    
  800b23:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b25:	fc                   	cld    
  800b26:	eb 1d                	jmp    800b45 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b28:	89 f2                	mov    %esi,%edx
  800b2a:	09 c2                	or     %eax,%edx
  800b2c:	f6 c2 03             	test   $0x3,%dl
  800b2f:	75 0f                	jne    800b40 <memmove+0x5d>
  800b31:	f6 c1 03             	test   $0x3,%cl
  800b34:	75 0a                	jne    800b40 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b36:	c1 e9 02             	shr    $0x2,%ecx
  800b39:	89 c7                	mov    %eax,%edi
  800b3b:	fc                   	cld    
  800b3c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b3e:	eb 05                	jmp    800b45 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b40:	89 c7                	mov    %eax,%edi
  800b42:	fc                   	cld    
  800b43:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b45:	5e                   	pop    %esi
  800b46:	5f                   	pop    %edi
  800b47:	5d                   	pop    %ebp
  800b48:	c3                   	ret    

00800b49 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b49:	55                   	push   %ebp
  800b4a:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b4c:	ff 75 10             	pushl  0x10(%ebp)
  800b4f:	ff 75 0c             	pushl  0xc(%ebp)
  800b52:	ff 75 08             	pushl  0x8(%ebp)
  800b55:	e8 89 ff ff ff       	call   800ae3 <memmove>
}
  800b5a:	c9                   	leave  
  800b5b:	c3                   	ret    

00800b5c <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b5c:	55                   	push   %ebp
  800b5d:	89 e5                	mov    %esp,%ebp
  800b5f:	56                   	push   %esi
  800b60:	53                   	push   %ebx
  800b61:	8b 45 08             	mov    0x8(%ebp),%eax
  800b64:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b67:	89 c6                	mov    %eax,%esi
  800b69:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b6c:	eb 14                	jmp    800b82 <memcmp+0x26>
		if (*s1 != *s2)
  800b6e:	8a 08                	mov    (%eax),%cl
  800b70:	8a 1a                	mov    (%edx),%bl
  800b72:	38 d9                	cmp    %bl,%cl
  800b74:	74 0a                	je     800b80 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b76:	0f b6 c1             	movzbl %cl,%eax
  800b79:	0f b6 db             	movzbl %bl,%ebx
  800b7c:	29 d8                	sub    %ebx,%eax
  800b7e:	eb 0b                	jmp    800b8b <memcmp+0x2f>
		s1++, s2++;
  800b80:	40                   	inc    %eax
  800b81:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b82:	39 f0                	cmp    %esi,%eax
  800b84:	75 e8                	jne    800b6e <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b86:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b8b:	5b                   	pop    %ebx
  800b8c:	5e                   	pop    %esi
  800b8d:	5d                   	pop    %ebp
  800b8e:	c3                   	ret    

00800b8f <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b8f:	55                   	push   %ebp
  800b90:	89 e5                	mov    %esp,%ebp
  800b92:	53                   	push   %ebx
  800b93:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b96:	89 c1                	mov    %eax,%ecx
  800b98:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b9b:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b9f:	eb 08                	jmp    800ba9 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800ba1:	0f b6 10             	movzbl (%eax),%edx
  800ba4:	39 da                	cmp    %ebx,%edx
  800ba6:	74 05                	je     800bad <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ba8:	40                   	inc    %eax
  800ba9:	39 c8                	cmp    %ecx,%eax
  800bab:	72 f4                	jb     800ba1 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bad:	5b                   	pop    %ebx
  800bae:	5d                   	pop    %ebp
  800baf:	c3                   	ret    

00800bb0 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bb0:	55                   	push   %ebp
  800bb1:	89 e5                	mov    %esp,%ebp
  800bb3:	57                   	push   %edi
  800bb4:	56                   	push   %esi
  800bb5:	53                   	push   %ebx
  800bb6:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb9:	eb 01                	jmp    800bbc <strtol+0xc>
		s++;
  800bbb:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bbc:	8a 01                	mov    (%ecx),%al
  800bbe:	3c 20                	cmp    $0x20,%al
  800bc0:	74 f9                	je     800bbb <strtol+0xb>
  800bc2:	3c 09                	cmp    $0x9,%al
  800bc4:	74 f5                	je     800bbb <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bc6:	3c 2b                	cmp    $0x2b,%al
  800bc8:	75 08                	jne    800bd2 <strtol+0x22>
		s++;
  800bca:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bcb:	bf 00 00 00 00       	mov    $0x0,%edi
  800bd0:	eb 11                	jmp    800be3 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bd2:	3c 2d                	cmp    $0x2d,%al
  800bd4:	75 08                	jne    800bde <strtol+0x2e>
		s++, neg = 1;
  800bd6:	41                   	inc    %ecx
  800bd7:	bf 01 00 00 00       	mov    $0x1,%edi
  800bdc:	eb 05                	jmp    800be3 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bde:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800be3:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800be7:	0f 84 87 00 00 00    	je     800c74 <strtol+0xc4>
  800bed:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bf1:	75 27                	jne    800c1a <strtol+0x6a>
  800bf3:	80 39 30             	cmpb   $0x30,(%ecx)
  800bf6:	75 22                	jne    800c1a <strtol+0x6a>
  800bf8:	e9 88 00 00 00       	jmp    800c85 <strtol+0xd5>
		s += 2, base = 16;
  800bfd:	83 c1 02             	add    $0x2,%ecx
  800c00:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c07:	eb 11                	jmp    800c1a <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c09:	41                   	inc    %ecx
  800c0a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c11:	eb 07                	jmp    800c1a <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c13:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c1a:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c1f:	8a 11                	mov    (%ecx),%dl
  800c21:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c24:	80 fb 09             	cmp    $0x9,%bl
  800c27:	77 08                	ja     800c31 <strtol+0x81>
			dig = *s - '0';
  800c29:	0f be d2             	movsbl %dl,%edx
  800c2c:	83 ea 30             	sub    $0x30,%edx
  800c2f:	eb 22                	jmp    800c53 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c31:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c34:	89 f3                	mov    %esi,%ebx
  800c36:	80 fb 19             	cmp    $0x19,%bl
  800c39:	77 08                	ja     800c43 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c3b:	0f be d2             	movsbl %dl,%edx
  800c3e:	83 ea 57             	sub    $0x57,%edx
  800c41:	eb 10                	jmp    800c53 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c43:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c46:	89 f3                	mov    %esi,%ebx
  800c48:	80 fb 19             	cmp    $0x19,%bl
  800c4b:	77 14                	ja     800c61 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c4d:	0f be d2             	movsbl %dl,%edx
  800c50:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c53:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c56:	7d 09                	jge    800c61 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c58:	41                   	inc    %ecx
  800c59:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c5d:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c5f:	eb be                	jmp    800c1f <strtol+0x6f>

	if (endptr)
  800c61:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c65:	74 05                	je     800c6c <strtol+0xbc>
		*endptr = (char *) s;
  800c67:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c6a:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c6c:	85 ff                	test   %edi,%edi
  800c6e:	74 21                	je     800c91 <strtol+0xe1>
  800c70:	f7 d8                	neg    %eax
  800c72:	eb 1d                	jmp    800c91 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c74:	80 39 30             	cmpb   $0x30,(%ecx)
  800c77:	75 9a                	jne    800c13 <strtol+0x63>
  800c79:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c7d:	0f 84 7a ff ff ff    	je     800bfd <strtol+0x4d>
  800c83:	eb 84                	jmp    800c09 <strtol+0x59>
  800c85:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c89:	0f 84 6e ff ff ff    	je     800bfd <strtol+0x4d>
  800c8f:	eb 89                	jmp    800c1a <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c91:	5b                   	pop    %ebx
  800c92:	5e                   	pop    %esi
  800c93:	5f                   	pop    %edi
  800c94:	5d                   	pop    %ebp
  800c95:	c3                   	ret    
  800c96:	66 90                	xchg   %ax,%ax

00800c98 <__udivdi3>:
  800c98:	55                   	push   %ebp
  800c99:	57                   	push   %edi
  800c9a:	56                   	push   %esi
  800c9b:	53                   	push   %ebx
  800c9c:	83 ec 1c             	sub    $0x1c,%esp
  800c9f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ca3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ca7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cab:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800caf:	89 ca                	mov    %ecx,%edx
  800cb1:	89 f8                	mov    %edi,%eax
  800cb3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cb7:	85 f6                	test   %esi,%esi
  800cb9:	75 2d                	jne    800ce8 <__udivdi3+0x50>
  800cbb:	39 cf                	cmp    %ecx,%edi
  800cbd:	77 65                	ja     800d24 <__udivdi3+0x8c>
  800cbf:	89 fd                	mov    %edi,%ebp
  800cc1:	85 ff                	test   %edi,%edi
  800cc3:	75 0b                	jne    800cd0 <__udivdi3+0x38>
  800cc5:	b8 01 00 00 00       	mov    $0x1,%eax
  800cca:	31 d2                	xor    %edx,%edx
  800ccc:	f7 f7                	div    %edi
  800cce:	89 c5                	mov    %eax,%ebp
  800cd0:	31 d2                	xor    %edx,%edx
  800cd2:	89 c8                	mov    %ecx,%eax
  800cd4:	f7 f5                	div    %ebp
  800cd6:	89 c1                	mov    %eax,%ecx
  800cd8:	89 d8                	mov    %ebx,%eax
  800cda:	f7 f5                	div    %ebp
  800cdc:	89 cf                	mov    %ecx,%edi
  800cde:	89 fa                	mov    %edi,%edx
  800ce0:	83 c4 1c             	add    $0x1c,%esp
  800ce3:	5b                   	pop    %ebx
  800ce4:	5e                   	pop    %esi
  800ce5:	5f                   	pop    %edi
  800ce6:	5d                   	pop    %ebp
  800ce7:	c3                   	ret    
  800ce8:	39 ce                	cmp    %ecx,%esi
  800cea:	77 28                	ja     800d14 <__udivdi3+0x7c>
  800cec:	0f bd fe             	bsr    %esi,%edi
  800cef:	83 f7 1f             	xor    $0x1f,%edi
  800cf2:	75 40                	jne    800d34 <__udivdi3+0x9c>
  800cf4:	39 ce                	cmp    %ecx,%esi
  800cf6:	72 0a                	jb     800d02 <__udivdi3+0x6a>
  800cf8:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800cfc:	0f 87 9e 00 00 00    	ja     800da0 <__udivdi3+0x108>
  800d02:	b8 01 00 00 00       	mov    $0x1,%eax
  800d07:	89 fa                	mov    %edi,%edx
  800d09:	83 c4 1c             	add    $0x1c,%esp
  800d0c:	5b                   	pop    %ebx
  800d0d:	5e                   	pop    %esi
  800d0e:	5f                   	pop    %edi
  800d0f:	5d                   	pop    %ebp
  800d10:	c3                   	ret    
  800d11:	8d 76 00             	lea    0x0(%esi),%esi
  800d14:	31 ff                	xor    %edi,%edi
  800d16:	31 c0                	xor    %eax,%eax
  800d18:	89 fa                	mov    %edi,%edx
  800d1a:	83 c4 1c             	add    $0x1c,%esp
  800d1d:	5b                   	pop    %ebx
  800d1e:	5e                   	pop    %esi
  800d1f:	5f                   	pop    %edi
  800d20:	5d                   	pop    %ebp
  800d21:	c3                   	ret    
  800d22:	66 90                	xchg   %ax,%ax
  800d24:	89 d8                	mov    %ebx,%eax
  800d26:	f7 f7                	div    %edi
  800d28:	31 ff                	xor    %edi,%edi
  800d2a:	89 fa                	mov    %edi,%edx
  800d2c:	83 c4 1c             	add    $0x1c,%esp
  800d2f:	5b                   	pop    %ebx
  800d30:	5e                   	pop    %esi
  800d31:	5f                   	pop    %edi
  800d32:	5d                   	pop    %ebp
  800d33:	c3                   	ret    
  800d34:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d39:	89 eb                	mov    %ebp,%ebx
  800d3b:	29 fb                	sub    %edi,%ebx
  800d3d:	89 f9                	mov    %edi,%ecx
  800d3f:	d3 e6                	shl    %cl,%esi
  800d41:	89 c5                	mov    %eax,%ebp
  800d43:	88 d9                	mov    %bl,%cl
  800d45:	d3 ed                	shr    %cl,%ebp
  800d47:	89 e9                	mov    %ebp,%ecx
  800d49:	09 f1                	or     %esi,%ecx
  800d4b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d4f:	89 f9                	mov    %edi,%ecx
  800d51:	d3 e0                	shl    %cl,%eax
  800d53:	89 c5                	mov    %eax,%ebp
  800d55:	89 d6                	mov    %edx,%esi
  800d57:	88 d9                	mov    %bl,%cl
  800d59:	d3 ee                	shr    %cl,%esi
  800d5b:	89 f9                	mov    %edi,%ecx
  800d5d:	d3 e2                	shl    %cl,%edx
  800d5f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d63:	88 d9                	mov    %bl,%cl
  800d65:	d3 e8                	shr    %cl,%eax
  800d67:	09 c2                	or     %eax,%edx
  800d69:	89 d0                	mov    %edx,%eax
  800d6b:	89 f2                	mov    %esi,%edx
  800d6d:	f7 74 24 0c          	divl   0xc(%esp)
  800d71:	89 d6                	mov    %edx,%esi
  800d73:	89 c3                	mov    %eax,%ebx
  800d75:	f7 e5                	mul    %ebp
  800d77:	39 d6                	cmp    %edx,%esi
  800d79:	72 19                	jb     800d94 <__udivdi3+0xfc>
  800d7b:	74 0b                	je     800d88 <__udivdi3+0xf0>
  800d7d:	89 d8                	mov    %ebx,%eax
  800d7f:	31 ff                	xor    %edi,%edi
  800d81:	e9 58 ff ff ff       	jmp    800cde <__udivdi3+0x46>
  800d86:	66 90                	xchg   %ax,%ax
  800d88:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d8c:	89 f9                	mov    %edi,%ecx
  800d8e:	d3 e2                	shl    %cl,%edx
  800d90:	39 c2                	cmp    %eax,%edx
  800d92:	73 e9                	jae    800d7d <__udivdi3+0xe5>
  800d94:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d97:	31 ff                	xor    %edi,%edi
  800d99:	e9 40 ff ff ff       	jmp    800cde <__udivdi3+0x46>
  800d9e:	66 90                	xchg   %ax,%ax
  800da0:	31 c0                	xor    %eax,%eax
  800da2:	e9 37 ff ff ff       	jmp    800cde <__udivdi3+0x46>
  800da7:	90                   	nop

00800da8 <__umoddi3>:
  800da8:	55                   	push   %ebp
  800da9:	57                   	push   %edi
  800daa:	56                   	push   %esi
  800dab:	53                   	push   %ebx
  800dac:	83 ec 1c             	sub    $0x1c,%esp
  800daf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800db3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800db7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dbb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dbf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dc3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dc7:	89 f3                	mov    %esi,%ebx
  800dc9:	89 fa                	mov    %edi,%edx
  800dcb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dcf:	89 34 24             	mov    %esi,(%esp)
  800dd2:	85 c0                	test   %eax,%eax
  800dd4:	75 1a                	jne    800df0 <__umoddi3+0x48>
  800dd6:	39 f7                	cmp    %esi,%edi
  800dd8:	0f 86 a2 00 00 00    	jbe    800e80 <__umoddi3+0xd8>
  800dde:	89 c8                	mov    %ecx,%eax
  800de0:	89 f2                	mov    %esi,%edx
  800de2:	f7 f7                	div    %edi
  800de4:	89 d0                	mov    %edx,%eax
  800de6:	31 d2                	xor    %edx,%edx
  800de8:	83 c4 1c             	add    $0x1c,%esp
  800deb:	5b                   	pop    %ebx
  800dec:	5e                   	pop    %esi
  800ded:	5f                   	pop    %edi
  800dee:	5d                   	pop    %ebp
  800def:	c3                   	ret    
  800df0:	39 f0                	cmp    %esi,%eax
  800df2:	0f 87 ac 00 00 00    	ja     800ea4 <__umoddi3+0xfc>
  800df8:	0f bd e8             	bsr    %eax,%ebp
  800dfb:	83 f5 1f             	xor    $0x1f,%ebp
  800dfe:	0f 84 ac 00 00 00    	je     800eb0 <__umoddi3+0x108>
  800e04:	bf 20 00 00 00       	mov    $0x20,%edi
  800e09:	29 ef                	sub    %ebp,%edi
  800e0b:	89 fe                	mov    %edi,%esi
  800e0d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e11:	89 e9                	mov    %ebp,%ecx
  800e13:	d3 e0                	shl    %cl,%eax
  800e15:	89 d7                	mov    %edx,%edi
  800e17:	89 f1                	mov    %esi,%ecx
  800e19:	d3 ef                	shr    %cl,%edi
  800e1b:	09 c7                	or     %eax,%edi
  800e1d:	89 e9                	mov    %ebp,%ecx
  800e1f:	d3 e2                	shl    %cl,%edx
  800e21:	89 14 24             	mov    %edx,(%esp)
  800e24:	89 d8                	mov    %ebx,%eax
  800e26:	d3 e0                	shl    %cl,%eax
  800e28:	89 c2                	mov    %eax,%edx
  800e2a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e2e:	d3 e0                	shl    %cl,%eax
  800e30:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e34:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e38:	89 f1                	mov    %esi,%ecx
  800e3a:	d3 e8                	shr    %cl,%eax
  800e3c:	09 d0                	or     %edx,%eax
  800e3e:	d3 eb                	shr    %cl,%ebx
  800e40:	89 da                	mov    %ebx,%edx
  800e42:	f7 f7                	div    %edi
  800e44:	89 d3                	mov    %edx,%ebx
  800e46:	f7 24 24             	mull   (%esp)
  800e49:	89 c6                	mov    %eax,%esi
  800e4b:	89 d1                	mov    %edx,%ecx
  800e4d:	39 d3                	cmp    %edx,%ebx
  800e4f:	0f 82 87 00 00 00    	jb     800edc <__umoddi3+0x134>
  800e55:	0f 84 91 00 00 00    	je     800eec <__umoddi3+0x144>
  800e5b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e5f:	29 f2                	sub    %esi,%edx
  800e61:	19 cb                	sbb    %ecx,%ebx
  800e63:	89 d8                	mov    %ebx,%eax
  800e65:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e69:	d3 e0                	shl    %cl,%eax
  800e6b:	89 e9                	mov    %ebp,%ecx
  800e6d:	d3 ea                	shr    %cl,%edx
  800e6f:	09 d0                	or     %edx,%eax
  800e71:	89 e9                	mov    %ebp,%ecx
  800e73:	d3 eb                	shr    %cl,%ebx
  800e75:	89 da                	mov    %ebx,%edx
  800e77:	83 c4 1c             	add    $0x1c,%esp
  800e7a:	5b                   	pop    %ebx
  800e7b:	5e                   	pop    %esi
  800e7c:	5f                   	pop    %edi
  800e7d:	5d                   	pop    %ebp
  800e7e:	c3                   	ret    
  800e7f:	90                   	nop
  800e80:	89 fd                	mov    %edi,%ebp
  800e82:	85 ff                	test   %edi,%edi
  800e84:	75 0b                	jne    800e91 <__umoddi3+0xe9>
  800e86:	b8 01 00 00 00       	mov    $0x1,%eax
  800e8b:	31 d2                	xor    %edx,%edx
  800e8d:	f7 f7                	div    %edi
  800e8f:	89 c5                	mov    %eax,%ebp
  800e91:	89 f0                	mov    %esi,%eax
  800e93:	31 d2                	xor    %edx,%edx
  800e95:	f7 f5                	div    %ebp
  800e97:	89 c8                	mov    %ecx,%eax
  800e99:	f7 f5                	div    %ebp
  800e9b:	89 d0                	mov    %edx,%eax
  800e9d:	e9 44 ff ff ff       	jmp    800de6 <__umoddi3+0x3e>
  800ea2:	66 90                	xchg   %ax,%ax
  800ea4:	89 c8                	mov    %ecx,%eax
  800ea6:	89 f2                	mov    %esi,%edx
  800ea8:	83 c4 1c             	add    $0x1c,%esp
  800eab:	5b                   	pop    %ebx
  800eac:	5e                   	pop    %esi
  800ead:	5f                   	pop    %edi
  800eae:	5d                   	pop    %ebp
  800eaf:	c3                   	ret    
  800eb0:	3b 04 24             	cmp    (%esp),%eax
  800eb3:	72 06                	jb     800ebb <__umoddi3+0x113>
  800eb5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800eb9:	77 0f                	ja     800eca <__umoddi3+0x122>
  800ebb:	89 f2                	mov    %esi,%edx
  800ebd:	29 f9                	sub    %edi,%ecx
  800ebf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ec3:	89 14 24             	mov    %edx,(%esp)
  800ec6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800eca:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ece:	8b 14 24             	mov    (%esp),%edx
  800ed1:	83 c4 1c             	add    $0x1c,%esp
  800ed4:	5b                   	pop    %ebx
  800ed5:	5e                   	pop    %esi
  800ed6:	5f                   	pop    %edi
  800ed7:	5d                   	pop    %ebp
  800ed8:	c3                   	ret    
  800ed9:	8d 76 00             	lea    0x0(%esi),%esi
  800edc:	2b 04 24             	sub    (%esp),%eax
  800edf:	19 fa                	sbb    %edi,%edx
  800ee1:	89 d1                	mov    %edx,%ecx
  800ee3:	89 c6                	mov    %eax,%esi
  800ee5:	e9 71 ff ff ff       	jmp    800e5b <__umoddi3+0xb3>
  800eea:	66 90                	xchg   %ax,%ax
  800eec:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800ef0:	72 ea                	jb     800edc <__umoddi3+0x134>
  800ef2:	89 d9                	mov    %ebx,%ecx
  800ef4:	e9 62 ff ff ff       	jmp    800e5b <__umoddi3+0xb3>
