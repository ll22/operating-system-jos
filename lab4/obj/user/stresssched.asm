
obj/user/stresssched:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 c0 00 00 00       	call   8000f1 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

volatile int counter;

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
	int i, j;
	int seen;
	envid_t parent = sys_getenvid();
  800038:	e8 f7 0a 00 00       	call   800b34 <sys_getenvid>
  80003d:	89 c6                	mov    %eax,%esi

	// Fork several environments
	for (i = 0; i < 20; i++)
  80003f:	bb 00 00 00 00       	mov    $0x0,%ebx
		if (fork() == 0)
  800044:	e8 a9 0d 00 00       	call   800df2 <fork>
  800049:	85 c0                	test   %eax,%eax
  80004b:	74 08                	je     800055 <umain+0x22>
	int i, j;
	int seen;
	envid_t parent = sys_getenvid();

	// Fork several environments
	for (i = 0; i < 20; i++)
  80004d:	43                   	inc    %ebx
  80004e:	83 fb 14             	cmp    $0x14,%ebx
  800051:	75 f1                	jne    800044 <umain+0x11>
  800053:	eb 05                	jmp    80005a <umain+0x27>
		if (fork() == 0)
			break;
	if (i == 20) {
  800055:	83 fb 14             	cmp    $0x14,%ebx
  800058:	75 0e                	jne    800068 <umain+0x35>
		sys_yield();
  80005a:	e8 f4 0a 00 00       	call   800b53 <sys_yield>
		return;
  80005f:	e9 86 00 00 00       	jmp    8000ea <umain+0xb7>
	}

	// Wait for the parent to finish forking
	while (envs[ENVX(parent)].env_status != ENV_FREE)
		asm volatile("pause");
  800064:	f3 90                	pause  
  800066:	eb 1b                	jmp    800083 <umain+0x50>
		sys_yield();
		return;
	}

	// Wait for the parent to finish forking
	while (envs[ENVX(parent)].env_status != ENV_FREE)
  800068:	89 f0                	mov    %esi,%eax
  80006a:	25 ff 03 00 00       	and    $0x3ff,%eax
  80006f:	8d 0c 85 00 00 00 00 	lea    0x0(,%eax,4),%ecx
  800076:	c1 e0 07             	shl    $0x7,%eax
  800079:	89 c2                	mov    %eax,%edx
  80007b:	29 ca                	sub    %ecx,%edx
  80007d:	81 c2 00 00 c0 ee    	add    $0xeec00000,%edx
  800083:	8b 42 54             	mov    0x54(%edx),%eax
  800086:	85 c0                	test   %eax,%eax
  800088:	75 da                	jne    800064 <umain+0x31>
  80008a:	bb 0a 00 00 00       	mov    $0xa,%ebx
		asm volatile("pause");

	// Check that one environment doesn't run on two CPUs at once
	for (i = 0; i < 10; i++) {
		sys_yield();
  80008f:	e8 bf 0a 00 00       	call   800b53 <sys_yield>
  800094:	ba 10 27 00 00       	mov    $0x2710,%edx
		for (j = 0; j < 10000; j++)
			counter++;
  800099:	a1 04 20 80 00       	mov    0x802004,%eax
  80009e:	40                   	inc    %eax
  80009f:	a3 04 20 80 00       	mov    %eax,0x802004
		asm volatile("pause");

	// Check that one environment doesn't run on two CPUs at once
	for (i = 0; i < 10; i++) {
		sys_yield();
		for (j = 0; j < 10000; j++)
  8000a4:	4a                   	dec    %edx
  8000a5:	75 f2                	jne    800099 <umain+0x66>
	// Wait for the parent to finish forking
	while (envs[ENVX(parent)].env_status != ENV_FREE)
		asm volatile("pause");

	// Check that one environment doesn't run on two CPUs at once
	for (i = 0; i < 10; i++) {
  8000a7:	4b                   	dec    %ebx
  8000a8:	75 e5                	jne    80008f <umain+0x5c>
		sys_yield();
		for (j = 0; j < 10000; j++)
			counter++;
	}

	if (counter != 10*10000)
  8000aa:	a1 04 20 80 00       	mov    0x802004,%eax
  8000af:	3d a0 86 01 00       	cmp    $0x186a0,%eax
  8000b4:	74 17                	je     8000cd <umain+0x9a>
		panic("ran on two CPUs at once (counter is %d)", counter);
  8000b6:	a1 04 20 80 00       	mov    0x802004,%eax
  8000bb:	50                   	push   %eax
  8000bc:	68 00 13 80 00       	push   $0x801300
  8000c1:	6a 21                	push   $0x21
  8000c3:	68 28 13 80 00       	push   $0x801328
  8000c8:	e8 85 00 00 00       	call   800152 <_panic>

	// Check that we see environments running on different CPUs
	cprintf("[%08x] stresssched on CPU %d\n", thisenv->env_id, thisenv->env_cpunum);
  8000cd:	a1 08 20 80 00       	mov    0x802008,%eax
  8000d2:	8b 50 5c             	mov    0x5c(%eax),%edx
  8000d5:	8b 40 48             	mov    0x48(%eax),%eax
  8000d8:	83 ec 04             	sub    $0x4,%esp
  8000db:	52                   	push   %edx
  8000dc:	50                   	push   %eax
  8000dd:	68 3b 13 80 00       	push   $0x80133b
  8000e2:	e8 43 01 00 00       	call   80022a <cprintf>
  8000e7:	83 c4 10             	add    $0x10,%esp

}
  8000ea:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000ed:	5b                   	pop    %ebx
  8000ee:	5e                   	pop    %esi
  8000ef:	5d                   	pop    %ebp
  8000f0:	c3                   	ret    

008000f1 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000f1:	55                   	push   %ebp
  8000f2:	89 e5                	mov    %esp,%ebp
  8000f4:	56                   	push   %esi
  8000f5:	53                   	push   %ebx
  8000f6:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000f9:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000fc:	e8 33 0a 00 00       	call   800b34 <sys_getenvid>
  800101:	25 ff 03 00 00       	and    $0x3ff,%eax
  800106:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80010d:	c1 e0 07             	shl    $0x7,%eax
  800110:	29 d0                	sub    %edx,%eax
  800112:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800117:	a3 08 20 80 00       	mov    %eax,0x802008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80011c:	85 db                	test   %ebx,%ebx
  80011e:	7e 07                	jle    800127 <libmain+0x36>
		binaryname = argv[0];
  800120:	8b 06                	mov    (%esi),%eax
  800122:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800127:	83 ec 08             	sub    $0x8,%esp
  80012a:	56                   	push   %esi
  80012b:	53                   	push   %ebx
  80012c:	e8 02 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800131:	e8 0a 00 00 00       	call   800140 <exit>
}
  800136:	83 c4 10             	add    $0x10,%esp
  800139:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80013c:	5b                   	pop    %ebx
  80013d:	5e                   	pop    %esi
  80013e:	5d                   	pop    %ebp
  80013f:	c3                   	ret    

00800140 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800140:	55                   	push   %ebp
  800141:	89 e5                	mov    %esp,%ebp
  800143:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800146:	6a 00                	push   $0x0
  800148:	e8 a6 09 00 00       	call   800af3 <sys_env_destroy>
}
  80014d:	83 c4 10             	add    $0x10,%esp
  800150:	c9                   	leave  
  800151:	c3                   	ret    

00800152 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800152:	55                   	push   %ebp
  800153:	89 e5                	mov    %esp,%ebp
  800155:	56                   	push   %esi
  800156:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800157:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80015a:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800160:	e8 cf 09 00 00       	call   800b34 <sys_getenvid>
  800165:	83 ec 0c             	sub    $0xc,%esp
  800168:	ff 75 0c             	pushl  0xc(%ebp)
  80016b:	ff 75 08             	pushl  0x8(%ebp)
  80016e:	56                   	push   %esi
  80016f:	50                   	push   %eax
  800170:	68 64 13 80 00       	push   $0x801364
  800175:	e8 b0 00 00 00       	call   80022a <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80017a:	83 c4 18             	add    $0x18,%esp
  80017d:	53                   	push   %ebx
  80017e:	ff 75 10             	pushl  0x10(%ebp)
  800181:	e8 53 00 00 00       	call   8001d9 <vcprintf>
	cprintf("\n");
  800186:	c7 04 24 57 13 80 00 	movl   $0x801357,(%esp)
  80018d:	e8 98 00 00 00       	call   80022a <cprintf>
  800192:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800195:	cc                   	int3   
  800196:	eb fd                	jmp    800195 <_panic+0x43>

00800198 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800198:	55                   	push   %ebp
  800199:	89 e5                	mov    %esp,%ebp
  80019b:	53                   	push   %ebx
  80019c:	83 ec 04             	sub    $0x4,%esp
  80019f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8001a2:	8b 13                	mov    (%ebx),%edx
  8001a4:	8d 42 01             	lea    0x1(%edx),%eax
  8001a7:	89 03                	mov    %eax,(%ebx)
  8001a9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8001ac:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001b0:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001b5:	75 1a                	jne    8001d1 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001b7:	83 ec 08             	sub    $0x8,%esp
  8001ba:	68 ff 00 00 00       	push   $0xff
  8001bf:	8d 43 08             	lea    0x8(%ebx),%eax
  8001c2:	50                   	push   %eax
  8001c3:	e8 ee 08 00 00       	call   800ab6 <sys_cputs>
		b->idx = 0;
  8001c8:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001ce:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001d1:	ff 43 04             	incl   0x4(%ebx)
}
  8001d4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001d7:	c9                   	leave  
  8001d8:	c3                   	ret    

008001d9 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001d9:	55                   	push   %ebp
  8001da:	89 e5                	mov    %esp,%ebp
  8001dc:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001e2:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001e9:	00 00 00 
	b.cnt = 0;
  8001ec:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001f3:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001f6:	ff 75 0c             	pushl  0xc(%ebp)
  8001f9:	ff 75 08             	pushl  0x8(%ebp)
  8001fc:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800202:	50                   	push   %eax
  800203:	68 98 01 80 00       	push   $0x800198
  800208:	e8 51 01 00 00       	call   80035e <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80020d:	83 c4 08             	add    $0x8,%esp
  800210:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800216:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80021c:	50                   	push   %eax
  80021d:	e8 94 08 00 00       	call   800ab6 <sys_cputs>

	return b.cnt;
}
  800222:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800228:	c9                   	leave  
  800229:	c3                   	ret    

0080022a <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80022a:	55                   	push   %ebp
  80022b:	89 e5                	mov    %esp,%ebp
  80022d:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800230:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800233:	50                   	push   %eax
  800234:	ff 75 08             	pushl  0x8(%ebp)
  800237:	e8 9d ff ff ff       	call   8001d9 <vcprintf>
	va_end(ap);

	return cnt;
}
  80023c:	c9                   	leave  
  80023d:	c3                   	ret    

0080023e <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80023e:	55                   	push   %ebp
  80023f:	89 e5                	mov    %esp,%ebp
  800241:	57                   	push   %edi
  800242:	56                   	push   %esi
  800243:	53                   	push   %ebx
  800244:	83 ec 1c             	sub    $0x1c,%esp
  800247:	89 c7                	mov    %eax,%edi
  800249:	89 d6                	mov    %edx,%esi
  80024b:	8b 45 08             	mov    0x8(%ebp),%eax
  80024e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800251:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800254:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800257:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80025a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80025f:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800262:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800265:	39 d3                	cmp    %edx,%ebx
  800267:	72 05                	jb     80026e <printnum+0x30>
  800269:	39 45 10             	cmp    %eax,0x10(%ebp)
  80026c:	77 45                	ja     8002b3 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80026e:	83 ec 0c             	sub    $0xc,%esp
  800271:	ff 75 18             	pushl  0x18(%ebp)
  800274:	8b 45 14             	mov    0x14(%ebp),%eax
  800277:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80027a:	53                   	push   %ebx
  80027b:	ff 75 10             	pushl  0x10(%ebp)
  80027e:	83 ec 08             	sub    $0x8,%esp
  800281:	ff 75 e4             	pushl  -0x1c(%ebp)
  800284:	ff 75 e0             	pushl  -0x20(%ebp)
  800287:	ff 75 dc             	pushl  -0x24(%ebp)
  80028a:	ff 75 d8             	pushl  -0x28(%ebp)
  80028d:	e8 fa 0d 00 00       	call   80108c <__udivdi3>
  800292:	83 c4 18             	add    $0x18,%esp
  800295:	52                   	push   %edx
  800296:	50                   	push   %eax
  800297:	89 f2                	mov    %esi,%edx
  800299:	89 f8                	mov    %edi,%eax
  80029b:	e8 9e ff ff ff       	call   80023e <printnum>
  8002a0:	83 c4 20             	add    $0x20,%esp
  8002a3:	eb 16                	jmp    8002bb <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8002a5:	83 ec 08             	sub    $0x8,%esp
  8002a8:	56                   	push   %esi
  8002a9:	ff 75 18             	pushl  0x18(%ebp)
  8002ac:	ff d7                	call   *%edi
  8002ae:	83 c4 10             	add    $0x10,%esp
  8002b1:	eb 03                	jmp    8002b6 <printnum+0x78>
  8002b3:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002b6:	4b                   	dec    %ebx
  8002b7:	85 db                	test   %ebx,%ebx
  8002b9:	7f ea                	jg     8002a5 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002bb:	83 ec 08             	sub    $0x8,%esp
  8002be:	56                   	push   %esi
  8002bf:	83 ec 04             	sub    $0x4,%esp
  8002c2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002c5:	ff 75 e0             	pushl  -0x20(%ebp)
  8002c8:	ff 75 dc             	pushl  -0x24(%ebp)
  8002cb:	ff 75 d8             	pushl  -0x28(%ebp)
  8002ce:	e8 c9 0e 00 00       	call   80119c <__umoddi3>
  8002d3:	83 c4 14             	add    $0x14,%esp
  8002d6:	0f be 80 87 13 80 00 	movsbl 0x801387(%eax),%eax
  8002dd:	50                   	push   %eax
  8002de:	ff d7                	call   *%edi
}
  8002e0:	83 c4 10             	add    $0x10,%esp
  8002e3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002e6:	5b                   	pop    %ebx
  8002e7:	5e                   	pop    %esi
  8002e8:	5f                   	pop    %edi
  8002e9:	5d                   	pop    %ebp
  8002ea:	c3                   	ret    

008002eb <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002eb:	55                   	push   %ebp
  8002ec:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002ee:	83 fa 01             	cmp    $0x1,%edx
  8002f1:	7e 0e                	jle    800301 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002f3:	8b 10                	mov    (%eax),%edx
  8002f5:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002f8:	89 08                	mov    %ecx,(%eax)
  8002fa:	8b 02                	mov    (%edx),%eax
  8002fc:	8b 52 04             	mov    0x4(%edx),%edx
  8002ff:	eb 22                	jmp    800323 <getuint+0x38>
	else if (lflag)
  800301:	85 d2                	test   %edx,%edx
  800303:	74 10                	je     800315 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800305:	8b 10                	mov    (%eax),%edx
  800307:	8d 4a 04             	lea    0x4(%edx),%ecx
  80030a:	89 08                	mov    %ecx,(%eax)
  80030c:	8b 02                	mov    (%edx),%eax
  80030e:	ba 00 00 00 00       	mov    $0x0,%edx
  800313:	eb 0e                	jmp    800323 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800315:	8b 10                	mov    (%eax),%edx
  800317:	8d 4a 04             	lea    0x4(%edx),%ecx
  80031a:	89 08                	mov    %ecx,(%eax)
  80031c:	8b 02                	mov    (%edx),%eax
  80031e:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800323:	5d                   	pop    %ebp
  800324:	c3                   	ret    

00800325 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800325:	55                   	push   %ebp
  800326:	89 e5                	mov    %esp,%ebp
  800328:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80032b:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80032e:	8b 10                	mov    (%eax),%edx
  800330:	3b 50 04             	cmp    0x4(%eax),%edx
  800333:	73 0a                	jae    80033f <sprintputch+0x1a>
		*b->buf++ = ch;
  800335:	8d 4a 01             	lea    0x1(%edx),%ecx
  800338:	89 08                	mov    %ecx,(%eax)
  80033a:	8b 45 08             	mov    0x8(%ebp),%eax
  80033d:	88 02                	mov    %al,(%edx)
}
  80033f:	5d                   	pop    %ebp
  800340:	c3                   	ret    

00800341 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800341:	55                   	push   %ebp
  800342:	89 e5                	mov    %esp,%ebp
  800344:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800347:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80034a:	50                   	push   %eax
  80034b:	ff 75 10             	pushl  0x10(%ebp)
  80034e:	ff 75 0c             	pushl  0xc(%ebp)
  800351:	ff 75 08             	pushl  0x8(%ebp)
  800354:	e8 05 00 00 00       	call   80035e <vprintfmt>
	va_end(ap);
}
  800359:	83 c4 10             	add    $0x10,%esp
  80035c:	c9                   	leave  
  80035d:	c3                   	ret    

0080035e <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80035e:	55                   	push   %ebp
  80035f:	89 e5                	mov    %esp,%ebp
  800361:	57                   	push   %edi
  800362:	56                   	push   %esi
  800363:	53                   	push   %ebx
  800364:	83 ec 2c             	sub    $0x2c,%esp
  800367:	8b 75 08             	mov    0x8(%ebp),%esi
  80036a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80036d:	8b 7d 10             	mov    0x10(%ebp),%edi
  800370:	eb 12                	jmp    800384 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800372:	85 c0                	test   %eax,%eax
  800374:	0f 84 68 03 00 00    	je     8006e2 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80037a:	83 ec 08             	sub    $0x8,%esp
  80037d:	53                   	push   %ebx
  80037e:	50                   	push   %eax
  80037f:	ff d6                	call   *%esi
  800381:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800384:	47                   	inc    %edi
  800385:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800389:	83 f8 25             	cmp    $0x25,%eax
  80038c:	75 e4                	jne    800372 <vprintfmt+0x14>
  80038e:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800392:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800399:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003a0:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8003a7:	ba 00 00 00 00       	mov    $0x0,%edx
  8003ac:	eb 07                	jmp    8003b5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ae:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003b1:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b5:	8d 47 01             	lea    0x1(%edi),%eax
  8003b8:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003bb:	0f b6 0f             	movzbl (%edi),%ecx
  8003be:	8a 07                	mov    (%edi),%al
  8003c0:	83 e8 23             	sub    $0x23,%eax
  8003c3:	3c 55                	cmp    $0x55,%al
  8003c5:	0f 87 fe 02 00 00    	ja     8006c9 <vprintfmt+0x36b>
  8003cb:	0f b6 c0             	movzbl %al,%eax
  8003ce:	ff 24 85 40 14 80 00 	jmp    *0x801440(,%eax,4)
  8003d5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003d8:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003dc:	eb d7                	jmp    8003b5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003de:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003e1:	b8 00 00 00 00       	mov    $0x0,%eax
  8003e6:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003e9:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003ec:	01 c0                	add    %eax,%eax
  8003ee:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003f2:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003f5:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003f8:	83 fa 09             	cmp    $0x9,%edx
  8003fb:	77 34                	ja     800431 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003fd:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003fe:	eb e9                	jmp    8003e9 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800400:	8b 45 14             	mov    0x14(%ebp),%eax
  800403:	8d 48 04             	lea    0x4(%eax),%ecx
  800406:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800409:	8b 00                	mov    (%eax),%eax
  80040b:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800411:	eb 24                	jmp    800437 <vprintfmt+0xd9>
  800413:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800417:	79 07                	jns    800420 <vprintfmt+0xc2>
  800419:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800420:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800423:	eb 90                	jmp    8003b5 <vprintfmt+0x57>
  800425:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800428:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80042f:	eb 84                	jmp    8003b5 <vprintfmt+0x57>
  800431:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800434:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800437:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80043b:	0f 89 74 ff ff ff    	jns    8003b5 <vprintfmt+0x57>
				width = precision, precision = -1;
  800441:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800444:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800447:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80044e:	e9 62 ff ff ff       	jmp    8003b5 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800453:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800454:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800457:	e9 59 ff ff ff       	jmp    8003b5 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80045c:	8b 45 14             	mov    0x14(%ebp),%eax
  80045f:	8d 50 04             	lea    0x4(%eax),%edx
  800462:	89 55 14             	mov    %edx,0x14(%ebp)
  800465:	83 ec 08             	sub    $0x8,%esp
  800468:	53                   	push   %ebx
  800469:	ff 30                	pushl  (%eax)
  80046b:	ff d6                	call   *%esi
			break;
  80046d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800470:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800473:	e9 0c ff ff ff       	jmp    800384 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800478:	8b 45 14             	mov    0x14(%ebp),%eax
  80047b:	8d 50 04             	lea    0x4(%eax),%edx
  80047e:	89 55 14             	mov    %edx,0x14(%ebp)
  800481:	8b 00                	mov    (%eax),%eax
  800483:	85 c0                	test   %eax,%eax
  800485:	79 02                	jns    800489 <vprintfmt+0x12b>
  800487:	f7 d8                	neg    %eax
  800489:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80048b:	83 f8 08             	cmp    $0x8,%eax
  80048e:	7f 0b                	jg     80049b <vprintfmt+0x13d>
  800490:	8b 04 85 a0 15 80 00 	mov    0x8015a0(,%eax,4),%eax
  800497:	85 c0                	test   %eax,%eax
  800499:	75 18                	jne    8004b3 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80049b:	52                   	push   %edx
  80049c:	68 9f 13 80 00       	push   $0x80139f
  8004a1:	53                   	push   %ebx
  8004a2:	56                   	push   %esi
  8004a3:	e8 99 fe ff ff       	call   800341 <printfmt>
  8004a8:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ab:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004ae:	e9 d1 fe ff ff       	jmp    800384 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004b3:	50                   	push   %eax
  8004b4:	68 a8 13 80 00       	push   $0x8013a8
  8004b9:	53                   	push   %ebx
  8004ba:	56                   	push   %esi
  8004bb:	e8 81 fe ff ff       	call   800341 <printfmt>
  8004c0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004c3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004c6:	e9 b9 fe ff ff       	jmp    800384 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004cb:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ce:	8d 50 04             	lea    0x4(%eax),%edx
  8004d1:	89 55 14             	mov    %edx,0x14(%ebp)
  8004d4:	8b 38                	mov    (%eax),%edi
  8004d6:	85 ff                	test   %edi,%edi
  8004d8:	75 05                	jne    8004df <vprintfmt+0x181>
				p = "(null)";
  8004da:	bf 98 13 80 00       	mov    $0x801398,%edi
			if (width > 0 && padc != '-')
  8004df:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004e3:	0f 8e 90 00 00 00    	jle    800579 <vprintfmt+0x21b>
  8004e9:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004ed:	0f 84 8e 00 00 00    	je     800581 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004f3:	83 ec 08             	sub    $0x8,%esp
  8004f6:	ff 75 d0             	pushl  -0x30(%ebp)
  8004f9:	57                   	push   %edi
  8004fa:	e8 70 02 00 00       	call   80076f <strnlen>
  8004ff:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800502:	29 c1                	sub    %eax,%ecx
  800504:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800507:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80050a:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80050e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800511:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800514:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800516:	eb 0d                	jmp    800525 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800518:	83 ec 08             	sub    $0x8,%esp
  80051b:	53                   	push   %ebx
  80051c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80051f:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800521:	4f                   	dec    %edi
  800522:	83 c4 10             	add    $0x10,%esp
  800525:	85 ff                	test   %edi,%edi
  800527:	7f ef                	jg     800518 <vprintfmt+0x1ba>
  800529:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80052c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80052f:	89 c8                	mov    %ecx,%eax
  800531:	85 c9                	test   %ecx,%ecx
  800533:	79 05                	jns    80053a <vprintfmt+0x1dc>
  800535:	b8 00 00 00 00       	mov    $0x0,%eax
  80053a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80053d:	29 c1                	sub    %eax,%ecx
  80053f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800542:	89 75 08             	mov    %esi,0x8(%ebp)
  800545:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800548:	eb 3d                	jmp    800587 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80054a:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80054e:	74 19                	je     800569 <vprintfmt+0x20b>
  800550:	0f be c0             	movsbl %al,%eax
  800553:	83 e8 20             	sub    $0x20,%eax
  800556:	83 f8 5e             	cmp    $0x5e,%eax
  800559:	76 0e                	jbe    800569 <vprintfmt+0x20b>
					putch('?', putdat);
  80055b:	83 ec 08             	sub    $0x8,%esp
  80055e:	53                   	push   %ebx
  80055f:	6a 3f                	push   $0x3f
  800561:	ff 55 08             	call   *0x8(%ebp)
  800564:	83 c4 10             	add    $0x10,%esp
  800567:	eb 0b                	jmp    800574 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800569:	83 ec 08             	sub    $0x8,%esp
  80056c:	53                   	push   %ebx
  80056d:	52                   	push   %edx
  80056e:	ff 55 08             	call   *0x8(%ebp)
  800571:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800574:	ff 4d e4             	decl   -0x1c(%ebp)
  800577:	eb 0e                	jmp    800587 <vprintfmt+0x229>
  800579:	89 75 08             	mov    %esi,0x8(%ebp)
  80057c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80057f:	eb 06                	jmp    800587 <vprintfmt+0x229>
  800581:	89 75 08             	mov    %esi,0x8(%ebp)
  800584:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800587:	47                   	inc    %edi
  800588:	8a 47 ff             	mov    -0x1(%edi),%al
  80058b:	0f be d0             	movsbl %al,%edx
  80058e:	85 d2                	test   %edx,%edx
  800590:	74 1d                	je     8005af <vprintfmt+0x251>
  800592:	85 f6                	test   %esi,%esi
  800594:	78 b4                	js     80054a <vprintfmt+0x1ec>
  800596:	4e                   	dec    %esi
  800597:	79 b1                	jns    80054a <vprintfmt+0x1ec>
  800599:	8b 75 08             	mov    0x8(%ebp),%esi
  80059c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80059f:	eb 14                	jmp    8005b5 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8005a1:	83 ec 08             	sub    $0x8,%esp
  8005a4:	53                   	push   %ebx
  8005a5:	6a 20                	push   $0x20
  8005a7:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005a9:	4f                   	dec    %edi
  8005aa:	83 c4 10             	add    $0x10,%esp
  8005ad:	eb 06                	jmp    8005b5 <vprintfmt+0x257>
  8005af:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005b2:	8b 75 08             	mov    0x8(%ebp),%esi
  8005b5:	85 ff                	test   %edi,%edi
  8005b7:	7f e8                	jg     8005a1 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005bc:	e9 c3 fd ff ff       	jmp    800384 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005c1:	83 fa 01             	cmp    $0x1,%edx
  8005c4:	7e 16                	jle    8005dc <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005c6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c9:	8d 50 08             	lea    0x8(%eax),%edx
  8005cc:	89 55 14             	mov    %edx,0x14(%ebp)
  8005cf:	8b 50 04             	mov    0x4(%eax),%edx
  8005d2:	8b 00                	mov    (%eax),%eax
  8005d4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005d7:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005da:	eb 32                	jmp    80060e <vprintfmt+0x2b0>
	else if (lflag)
  8005dc:	85 d2                	test   %edx,%edx
  8005de:	74 18                	je     8005f8 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005e0:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e3:	8d 50 04             	lea    0x4(%eax),%edx
  8005e6:	89 55 14             	mov    %edx,0x14(%ebp)
  8005e9:	8b 00                	mov    (%eax),%eax
  8005eb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005ee:	89 c1                	mov    %eax,%ecx
  8005f0:	c1 f9 1f             	sar    $0x1f,%ecx
  8005f3:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005f6:	eb 16                	jmp    80060e <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005f8:	8b 45 14             	mov    0x14(%ebp),%eax
  8005fb:	8d 50 04             	lea    0x4(%eax),%edx
  8005fe:	89 55 14             	mov    %edx,0x14(%ebp)
  800601:	8b 00                	mov    (%eax),%eax
  800603:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800606:	89 c1                	mov    %eax,%ecx
  800608:	c1 f9 1f             	sar    $0x1f,%ecx
  80060b:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80060e:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800611:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800614:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800618:	79 76                	jns    800690 <vprintfmt+0x332>
				putch('-', putdat);
  80061a:	83 ec 08             	sub    $0x8,%esp
  80061d:	53                   	push   %ebx
  80061e:	6a 2d                	push   $0x2d
  800620:	ff d6                	call   *%esi
				num = -(long long) num;
  800622:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800625:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800628:	f7 d8                	neg    %eax
  80062a:	83 d2 00             	adc    $0x0,%edx
  80062d:	f7 da                	neg    %edx
  80062f:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800632:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800637:	eb 5c                	jmp    800695 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800639:	8d 45 14             	lea    0x14(%ebp),%eax
  80063c:	e8 aa fc ff ff       	call   8002eb <getuint>
			base = 10;
  800641:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800646:	eb 4d                	jmp    800695 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800648:	8d 45 14             	lea    0x14(%ebp),%eax
  80064b:	e8 9b fc ff ff       	call   8002eb <getuint>
			base = 8;
  800650:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800655:	eb 3e                	jmp    800695 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800657:	83 ec 08             	sub    $0x8,%esp
  80065a:	53                   	push   %ebx
  80065b:	6a 30                	push   $0x30
  80065d:	ff d6                	call   *%esi
			putch('x', putdat);
  80065f:	83 c4 08             	add    $0x8,%esp
  800662:	53                   	push   %ebx
  800663:	6a 78                	push   $0x78
  800665:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800667:	8b 45 14             	mov    0x14(%ebp),%eax
  80066a:	8d 50 04             	lea    0x4(%eax),%edx
  80066d:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800670:	8b 00                	mov    (%eax),%eax
  800672:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800677:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80067a:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80067f:	eb 14                	jmp    800695 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800681:	8d 45 14             	lea    0x14(%ebp),%eax
  800684:	e8 62 fc ff ff       	call   8002eb <getuint>
			base = 16;
  800689:	b9 10 00 00 00       	mov    $0x10,%ecx
  80068e:	eb 05                	jmp    800695 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800690:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800695:	83 ec 0c             	sub    $0xc,%esp
  800698:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80069c:	57                   	push   %edi
  80069d:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006a0:	51                   	push   %ecx
  8006a1:	52                   	push   %edx
  8006a2:	50                   	push   %eax
  8006a3:	89 da                	mov    %ebx,%edx
  8006a5:	89 f0                	mov    %esi,%eax
  8006a7:	e8 92 fb ff ff       	call   80023e <printnum>
			break;
  8006ac:	83 c4 20             	add    $0x20,%esp
  8006af:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006b2:	e9 cd fc ff ff       	jmp    800384 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006b7:	83 ec 08             	sub    $0x8,%esp
  8006ba:	53                   	push   %ebx
  8006bb:	51                   	push   %ecx
  8006bc:	ff d6                	call   *%esi
			break;
  8006be:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006c4:	e9 bb fc ff ff       	jmp    800384 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006c9:	83 ec 08             	sub    $0x8,%esp
  8006cc:	53                   	push   %ebx
  8006cd:	6a 25                	push   $0x25
  8006cf:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006d1:	83 c4 10             	add    $0x10,%esp
  8006d4:	eb 01                	jmp    8006d7 <vprintfmt+0x379>
  8006d6:	4f                   	dec    %edi
  8006d7:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006db:	75 f9                	jne    8006d6 <vprintfmt+0x378>
  8006dd:	e9 a2 fc ff ff       	jmp    800384 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006e5:	5b                   	pop    %ebx
  8006e6:	5e                   	pop    %esi
  8006e7:	5f                   	pop    %edi
  8006e8:	5d                   	pop    %ebp
  8006e9:	c3                   	ret    

008006ea <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006ea:	55                   	push   %ebp
  8006eb:	89 e5                	mov    %esp,%ebp
  8006ed:	83 ec 18             	sub    $0x18,%esp
  8006f0:	8b 45 08             	mov    0x8(%ebp),%eax
  8006f3:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006f6:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006f9:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006fd:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800700:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800707:	85 c0                	test   %eax,%eax
  800709:	74 26                	je     800731 <vsnprintf+0x47>
  80070b:	85 d2                	test   %edx,%edx
  80070d:	7e 29                	jle    800738 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80070f:	ff 75 14             	pushl  0x14(%ebp)
  800712:	ff 75 10             	pushl  0x10(%ebp)
  800715:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800718:	50                   	push   %eax
  800719:	68 25 03 80 00       	push   $0x800325
  80071e:	e8 3b fc ff ff       	call   80035e <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800723:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800726:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800729:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80072c:	83 c4 10             	add    $0x10,%esp
  80072f:	eb 0c                	jmp    80073d <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800731:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800736:	eb 05                	jmp    80073d <vsnprintf+0x53>
  800738:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80073d:	c9                   	leave  
  80073e:	c3                   	ret    

0080073f <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80073f:	55                   	push   %ebp
  800740:	89 e5                	mov    %esp,%ebp
  800742:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800745:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800748:	50                   	push   %eax
  800749:	ff 75 10             	pushl  0x10(%ebp)
  80074c:	ff 75 0c             	pushl  0xc(%ebp)
  80074f:	ff 75 08             	pushl  0x8(%ebp)
  800752:	e8 93 ff ff ff       	call   8006ea <vsnprintf>
	va_end(ap);

	return rc;
}
  800757:	c9                   	leave  
  800758:	c3                   	ret    

00800759 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800759:	55                   	push   %ebp
  80075a:	89 e5                	mov    %esp,%ebp
  80075c:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80075f:	b8 00 00 00 00       	mov    $0x0,%eax
  800764:	eb 01                	jmp    800767 <strlen+0xe>
		n++;
  800766:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800767:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80076b:	75 f9                	jne    800766 <strlen+0xd>
		n++;
	return n;
}
  80076d:	5d                   	pop    %ebp
  80076e:	c3                   	ret    

0080076f <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80076f:	55                   	push   %ebp
  800770:	89 e5                	mov    %esp,%ebp
  800772:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800775:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800778:	ba 00 00 00 00       	mov    $0x0,%edx
  80077d:	eb 01                	jmp    800780 <strnlen+0x11>
		n++;
  80077f:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800780:	39 c2                	cmp    %eax,%edx
  800782:	74 08                	je     80078c <strnlen+0x1d>
  800784:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800788:	75 f5                	jne    80077f <strnlen+0x10>
  80078a:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80078c:	5d                   	pop    %ebp
  80078d:	c3                   	ret    

0080078e <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80078e:	55                   	push   %ebp
  80078f:	89 e5                	mov    %esp,%ebp
  800791:	53                   	push   %ebx
  800792:	8b 45 08             	mov    0x8(%ebp),%eax
  800795:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800798:	89 c2                	mov    %eax,%edx
  80079a:	42                   	inc    %edx
  80079b:	41                   	inc    %ecx
  80079c:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80079f:	88 5a ff             	mov    %bl,-0x1(%edx)
  8007a2:	84 db                	test   %bl,%bl
  8007a4:	75 f4                	jne    80079a <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8007a6:	5b                   	pop    %ebx
  8007a7:	5d                   	pop    %ebp
  8007a8:	c3                   	ret    

008007a9 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8007a9:	55                   	push   %ebp
  8007aa:	89 e5                	mov    %esp,%ebp
  8007ac:	53                   	push   %ebx
  8007ad:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007b0:	53                   	push   %ebx
  8007b1:	e8 a3 ff ff ff       	call   800759 <strlen>
  8007b6:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007b9:	ff 75 0c             	pushl  0xc(%ebp)
  8007bc:	01 d8                	add    %ebx,%eax
  8007be:	50                   	push   %eax
  8007bf:	e8 ca ff ff ff       	call   80078e <strcpy>
	return dst;
}
  8007c4:	89 d8                	mov    %ebx,%eax
  8007c6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007c9:	c9                   	leave  
  8007ca:	c3                   	ret    

008007cb <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007cb:	55                   	push   %ebp
  8007cc:	89 e5                	mov    %esp,%ebp
  8007ce:	56                   	push   %esi
  8007cf:	53                   	push   %ebx
  8007d0:	8b 75 08             	mov    0x8(%ebp),%esi
  8007d3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007d6:	89 f3                	mov    %esi,%ebx
  8007d8:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007db:	89 f2                	mov    %esi,%edx
  8007dd:	eb 0c                	jmp    8007eb <strncpy+0x20>
		*dst++ = *src;
  8007df:	42                   	inc    %edx
  8007e0:	8a 01                	mov    (%ecx),%al
  8007e2:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007e5:	80 39 01             	cmpb   $0x1,(%ecx)
  8007e8:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007eb:	39 da                	cmp    %ebx,%edx
  8007ed:	75 f0                	jne    8007df <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007ef:	89 f0                	mov    %esi,%eax
  8007f1:	5b                   	pop    %ebx
  8007f2:	5e                   	pop    %esi
  8007f3:	5d                   	pop    %ebp
  8007f4:	c3                   	ret    

008007f5 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007f5:	55                   	push   %ebp
  8007f6:	89 e5                	mov    %esp,%ebp
  8007f8:	56                   	push   %esi
  8007f9:	53                   	push   %ebx
  8007fa:	8b 75 08             	mov    0x8(%ebp),%esi
  8007fd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800800:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800803:	85 c0                	test   %eax,%eax
  800805:	74 1e                	je     800825 <strlcpy+0x30>
  800807:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80080b:	89 f2                	mov    %esi,%edx
  80080d:	eb 05                	jmp    800814 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80080f:	42                   	inc    %edx
  800810:	41                   	inc    %ecx
  800811:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800814:	39 c2                	cmp    %eax,%edx
  800816:	74 08                	je     800820 <strlcpy+0x2b>
  800818:	8a 19                	mov    (%ecx),%bl
  80081a:	84 db                	test   %bl,%bl
  80081c:	75 f1                	jne    80080f <strlcpy+0x1a>
  80081e:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800820:	c6 00 00             	movb   $0x0,(%eax)
  800823:	eb 02                	jmp    800827 <strlcpy+0x32>
  800825:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800827:	29 f0                	sub    %esi,%eax
}
  800829:	5b                   	pop    %ebx
  80082a:	5e                   	pop    %esi
  80082b:	5d                   	pop    %ebp
  80082c:	c3                   	ret    

0080082d <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80082d:	55                   	push   %ebp
  80082e:	89 e5                	mov    %esp,%ebp
  800830:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800833:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800836:	eb 02                	jmp    80083a <strcmp+0xd>
		p++, q++;
  800838:	41                   	inc    %ecx
  800839:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80083a:	8a 01                	mov    (%ecx),%al
  80083c:	84 c0                	test   %al,%al
  80083e:	74 04                	je     800844 <strcmp+0x17>
  800840:	3a 02                	cmp    (%edx),%al
  800842:	74 f4                	je     800838 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800844:	0f b6 c0             	movzbl %al,%eax
  800847:	0f b6 12             	movzbl (%edx),%edx
  80084a:	29 d0                	sub    %edx,%eax
}
  80084c:	5d                   	pop    %ebp
  80084d:	c3                   	ret    

0080084e <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80084e:	55                   	push   %ebp
  80084f:	89 e5                	mov    %esp,%ebp
  800851:	53                   	push   %ebx
  800852:	8b 45 08             	mov    0x8(%ebp),%eax
  800855:	8b 55 0c             	mov    0xc(%ebp),%edx
  800858:	89 c3                	mov    %eax,%ebx
  80085a:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80085d:	eb 02                	jmp    800861 <strncmp+0x13>
		n--, p++, q++;
  80085f:	40                   	inc    %eax
  800860:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800861:	39 d8                	cmp    %ebx,%eax
  800863:	74 14                	je     800879 <strncmp+0x2b>
  800865:	8a 08                	mov    (%eax),%cl
  800867:	84 c9                	test   %cl,%cl
  800869:	74 04                	je     80086f <strncmp+0x21>
  80086b:	3a 0a                	cmp    (%edx),%cl
  80086d:	74 f0                	je     80085f <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  80086f:	0f b6 00             	movzbl (%eax),%eax
  800872:	0f b6 12             	movzbl (%edx),%edx
  800875:	29 d0                	sub    %edx,%eax
  800877:	eb 05                	jmp    80087e <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800879:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80087e:	5b                   	pop    %ebx
  80087f:	5d                   	pop    %ebp
  800880:	c3                   	ret    

00800881 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800881:	55                   	push   %ebp
  800882:	89 e5                	mov    %esp,%ebp
  800884:	8b 45 08             	mov    0x8(%ebp),%eax
  800887:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80088a:	eb 05                	jmp    800891 <strchr+0x10>
		if (*s == c)
  80088c:	38 ca                	cmp    %cl,%dl
  80088e:	74 0c                	je     80089c <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800890:	40                   	inc    %eax
  800891:	8a 10                	mov    (%eax),%dl
  800893:	84 d2                	test   %dl,%dl
  800895:	75 f5                	jne    80088c <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800897:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80089c:	5d                   	pop    %ebp
  80089d:	c3                   	ret    

0080089e <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80089e:	55                   	push   %ebp
  80089f:	89 e5                	mov    %esp,%ebp
  8008a1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008a7:	eb 05                	jmp    8008ae <strfind+0x10>
		if (*s == c)
  8008a9:	38 ca                	cmp    %cl,%dl
  8008ab:	74 07                	je     8008b4 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8008ad:	40                   	inc    %eax
  8008ae:	8a 10                	mov    (%eax),%dl
  8008b0:	84 d2                	test   %dl,%dl
  8008b2:	75 f5                	jne    8008a9 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008b4:	5d                   	pop    %ebp
  8008b5:	c3                   	ret    

008008b6 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008b6:	55                   	push   %ebp
  8008b7:	89 e5                	mov    %esp,%ebp
  8008b9:	57                   	push   %edi
  8008ba:	56                   	push   %esi
  8008bb:	53                   	push   %ebx
  8008bc:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008bf:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008c2:	85 c9                	test   %ecx,%ecx
  8008c4:	74 36                	je     8008fc <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008c6:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008cc:	75 28                	jne    8008f6 <memset+0x40>
  8008ce:	f6 c1 03             	test   $0x3,%cl
  8008d1:	75 23                	jne    8008f6 <memset+0x40>
		c &= 0xFF;
  8008d3:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008d7:	89 d3                	mov    %edx,%ebx
  8008d9:	c1 e3 08             	shl    $0x8,%ebx
  8008dc:	89 d6                	mov    %edx,%esi
  8008de:	c1 e6 18             	shl    $0x18,%esi
  8008e1:	89 d0                	mov    %edx,%eax
  8008e3:	c1 e0 10             	shl    $0x10,%eax
  8008e6:	09 f0                	or     %esi,%eax
  8008e8:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008ea:	89 d8                	mov    %ebx,%eax
  8008ec:	09 d0                	or     %edx,%eax
  8008ee:	c1 e9 02             	shr    $0x2,%ecx
  8008f1:	fc                   	cld    
  8008f2:	f3 ab                	rep stos %eax,%es:(%edi)
  8008f4:	eb 06                	jmp    8008fc <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008f6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008f9:	fc                   	cld    
  8008fa:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008fc:	89 f8                	mov    %edi,%eax
  8008fe:	5b                   	pop    %ebx
  8008ff:	5e                   	pop    %esi
  800900:	5f                   	pop    %edi
  800901:	5d                   	pop    %ebp
  800902:	c3                   	ret    

00800903 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800903:	55                   	push   %ebp
  800904:	89 e5                	mov    %esp,%ebp
  800906:	57                   	push   %edi
  800907:	56                   	push   %esi
  800908:	8b 45 08             	mov    0x8(%ebp),%eax
  80090b:	8b 75 0c             	mov    0xc(%ebp),%esi
  80090e:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800911:	39 c6                	cmp    %eax,%esi
  800913:	73 33                	jae    800948 <memmove+0x45>
  800915:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800918:	39 d0                	cmp    %edx,%eax
  80091a:	73 2c                	jae    800948 <memmove+0x45>
		s += n;
		d += n;
  80091c:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80091f:	89 d6                	mov    %edx,%esi
  800921:	09 fe                	or     %edi,%esi
  800923:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800929:	75 13                	jne    80093e <memmove+0x3b>
  80092b:	f6 c1 03             	test   $0x3,%cl
  80092e:	75 0e                	jne    80093e <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800930:	83 ef 04             	sub    $0x4,%edi
  800933:	8d 72 fc             	lea    -0x4(%edx),%esi
  800936:	c1 e9 02             	shr    $0x2,%ecx
  800939:	fd                   	std    
  80093a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80093c:	eb 07                	jmp    800945 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80093e:	4f                   	dec    %edi
  80093f:	8d 72 ff             	lea    -0x1(%edx),%esi
  800942:	fd                   	std    
  800943:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800945:	fc                   	cld    
  800946:	eb 1d                	jmp    800965 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800948:	89 f2                	mov    %esi,%edx
  80094a:	09 c2                	or     %eax,%edx
  80094c:	f6 c2 03             	test   $0x3,%dl
  80094f:	75 0f                	jne    800960 <memmove+0x5d>
  800951:	f6 c1 03             	test   $0x3,%cl
  800954:	75 0a                	jne    800960 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800956:	c1 e9 02             	shr    $0x2,%ecx
  800959:	89 c7                	mov    %eax,%edi
  80095b:	fc                   	cld    
  80095c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80095e:	eb 05                	jmp    800965 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800960:	89 c7                	mov    %eax,%edi
  800962:	fc                   	cld    
  800963:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800965:	5e                   	pop    %esi
  800966:	5f                   	pop    %edi
  800967:	5d                   	pop    %ebp
  800968:	c3                   	ret    

00800969 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800969:	55                   	push   %ebp
  80096a:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80096c:	ff 75 10             	pushl  0x10(%ebp)
  80096f:	ff 75 0c             	pushl  0xc(%ebp)
  800972:	ff 75 08             	pushl  0x8(%ebp)
  800975:	e8 89 ff ff ff       	call   800903 <memmove>
}
  80097a:	c9                   	leave  
  80097b:	c3                   	ret    

0080097c <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80097c:	55                   	push   %ebp
  80097d:	89 e5                	mov    %esp,%ebp
  80097f:	56                   	push   %esi
  800980:	53                   	push   %ebx
  800981:	8b 45 08             	mov    0x8(%ebp),%eax
  800984:	8b 55 0c             	mov    0xc(%ebp),%edx
  800987:	89 c6                	mov    %eax,%esi
  800989:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80098c:	eb 14                	jmp    8009a2 <memcmp+0x26>
		if (*s1 != *s2)
  80098e:	8a 08                	mov    (%eax),%cl
  800990:	8a 1a                	mov    (%edx),%bl
  800992:	38 d9                	cmp    %bl,%cl
  800994:	74 0a                	je     8009a0 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800996:	0f b6 c1             	movzbl %cl,%eax
  800999:	0f b6 db             	movzbl %bl,%ebx
  80099c:	29 d8                	sub    %ebx,%eax
  80099e:	eb 0b                	jmp    8009ab <memcmp+0x2f>
		s1++, s2++;
  8009a0:	40                   	inc    %eax
  8009a1:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009a2:	39 f0                	cmp    %esi,%eax
  8009a4:	75 e8                	jne    80098e <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8009a6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009ab:	5b                   	pop    %ebx
  8009ac:	5e                   	pop    %esi
  8009ad:	5d                   	pop    %ebp
  8009ae:	c3                   	ret    

008009af <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009af:	55                   	push   %ebp
  8009b0:	89 e5                	mov    %esp,%ebp
  8009b2:	53                   	push   %ebx
  8009b3:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009b6:	89 c1                	mov    %eax,%ecx
  8009b8:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009bb:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009bf:	eb 08                	jmp    8009c9 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009c1:	0f b6 10             	movzbl (%eax),%edx
  8009c4:	39 da                	cmp    %ebx,%edx
  8009c6:	74 05                	je     8009cd <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009c8:	40                   	inc    %eax
  8009c9:	39 c8                	cmp    %ecx,%eax
  8009cb:	72 f4                	jb     8009c1 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009cd:	5b                   	pop    %ebx
  8009ce:	5d                   	pop    %ebp
  8009cf:	c3                   	ret    

008009d0 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009d0:	55                   	push   %ebp
  8009d1:	89 e5                	mov    %esp,%ebp
  8009d3:	57                   	push   %edi
  8009d4:	56                   	push   %esi
  8009d5:	53                   	push   %ebx
  8009d6:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009d9:	eb 01                	jmp    8009dc <strtol+0xc>
		s++;
  8009db:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009dc:	8a 01                	mov    (%ecx),%al
  8009de:	3c 20                	cmp    $0x20,%al
  8009e0:	74 f9                	je     8009db <strtol+0xb>
  8009e2:	3c 09                	cmp    $0x9,%al
  8009e4:	74 f5                	je     8009db <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009e6:	3c 2b                	cmp    $0x2b,%al
  8009e8:	75 08                	jne    8009f2 <strtol+0x22>
		s++;
  8009ea:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009eb:	bf 00 00 00 00       	mov    $0x0,%edi
  8009f0:	eb 11                	jmp    800a03 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009f2:	3c 2d                	cmp    $0x2d,%al
  8009f4:	75 08                	jne    8009fe <strtol+0x2e>
		s++, neg = 1;
  8009f6:	41                   	inc    %ecx
  8009f7:	bf 01 00 00 00       	mov    $0x1,%edi
  8009fc:	eb 05                	jmp    800a03 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009fe:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a03:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a07:	0f 84 87 00 00 00    	je     800a94 <strtol+0xc4>
  800a0d:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a11:	75 27                	jne    800a3a <strtol+0x6a>
  800a13:	80 39 30             	cmpb   $0x30,(%ecx)
  800a16:	75 22                	jne    800a3a <strtol+0x6a>
  800a18:	e9 88 00 00 00       	jmp    800aa5 <strtol+0xd5>
		s += 2, base = 16;
  800a1d:	83 c1 02             	add    $0x2,%ecx
  800a20:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a27:	eb 11                	jmp    800a3a <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a29:	41                   	inc    %ecx
  800a2a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a31:	eb 07                	jmp    800a3a <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a33:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a3a:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a3f:	8a 11                	mov    (%ecx),%dl
  800a41:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a44:	80 fb 09             	cmp    $0x9,%bl
  800a47:	77 08                	ja     800a51 <strtol+0x81>
			dig = *s - '0';
  800a49:	0f be d2             	movsbl %dl,%edx
  800a4c:	83 ea 30             	sub    $0x30,%edx
  800a4f:	eb 22                	jmp    800a73 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a51:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a54:	89 f3                	mov    %esi,%ebx
  800a56:	80 fb 19             	cmp    $0x19,%bl
  800a59:	77 08                	ja     800a63 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a5b:	0f be d2             	movsbl %dl,%edx
  800a5e:	83 ea 57             	sub    $0x57,%edx
  800a61:	eb 10                	jmp    800a73 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a63:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a66:	89 f3                	mov    %esi,%ebx
  800a68:	80 fb 19             	cmp    $0x19,%bl
  800a6b:	77 14                	ja     800a81 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a6d:	0f be d2             	movsbl %dl,%edx
  800a70:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a73:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a76:	7d 09                	jge    800a81 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a78:	41                   	inc    %ecx
  800a79:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a7d:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a7f:	eb be                	jmp    800a3f <strtol+0x6f>

	if (endptr)
  800a81:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a85:	74 05                	je     800a8c <strtol+0xbc>
		*endptr = (char *) s;
  800a87:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a8a:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a8c:	85 ff                	test   %edi,%edi
  800a8e:	74 21                	je     800ab1 <strtol+0xe1>
  800a90:	f7 d8                	neg    %eax
  800a92:	eb 1d                	jmp    800ab1 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a94:	80 39 30             	cmpb   $0x30,(%ecx)
  800a97:	75 9a                	jne    800a33 <strtol+0x63>
  800a99:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a9d:	0f 84 7a ff ff ff    	je     800a1d <strtol+0x4d>
  800aa3:	eb 84                	jmp    800a29 <strtol+0x59>
  800aa5:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800aa9:	0f 84 6e ff ff ff    	je     800a1d <strtol+0x4d>
  800aaf:	eb 89                	jmp    800a3a <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800ab1:	5b                   	pop    %ebx
  800ab2:	5e                   	pop    %esi
  800ab3:	5f                   	pop    %edi
  800ab4:	5d                   	pop    %ebp
  800ab5:	c3                   	ret    

00800ab6 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800ab6:	55                   	push   %ebp
  800ab7:	89 e5                	mov    %esp,%ebp
  800ab9:	57                   	push   %edi
  800aba:	56                   	push   %esi
  800abb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800abc:	b8 00 00 00 00       	mov    $0x0,%eax
  800ac1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ac4:	8b 55 08             	mov    0x8(%ebp),%edx
  800ac7:	89 c3                	mov    %eax,%ebx
  800ac9:	89 c7                	mov    %eax,%edi
  800acb:	89 c6                	mov    %eax,%esi
  800acd:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800acf:	5b                   	pop    %ebx
  800ad0:	5e                   	pop    %esi
  800ad1:	5f                   	pop    %edi
  800ad2:	5d                   	pop    %ebp
  800ad3:	c3                   	ret    

00800ad4 <sys_cgetc>:

int
sys_cgetc(void)
{
  800ad4:	55                   	push   %ebp
  800ad5:	89 e5                	mov    %esp,%ebp
  800ad7:	57                   	push   %edi
  800ad8:	56                   	push   %esi
  800ad9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ada:	ba 00 00 00 00       	mov    $0x0,%edx
  800adf:	b8 01 00 00 00       	mov    $0x1,%eax
  800ae4:	89 d1                	mov    %edx,%ecx
  800ae6:	89 d3                	mov    %edx,%ebx
  800ae8:	89 d7                	mov    %edx,%edi
  800aea:	89 d6                	mov    %edx,%esi
  800aec:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800aee:	5b                   	pop    %ebx
  800aef:	5e                   	pop    %esi
  800af0:	5f                   	pop    %edi
  800af1:	5d                   	pop    %ebp
  800af2:	c3                   	ret    

00800af3 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800af3:	55                   	push   %ebp
  800af4:	89 e5                	mov    %esp,%ebp
  800af6:	57                   	push   %edi
  800af7:	56                   	push   %esi
  800af8:	53                   	push   %ebx
  800af9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800afc:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b01:	b8 03 00 00 00       	mov    $0x3,%eax
  800b06:	8b 55 08             	mov    0x8(%ebp),%edx
  800b09:	89 cb                	mov    %ecx,%ebx
  800b0b:	89 cf                	mov    %ecx,%edi
  800b0d:	89 ce                	mov    %ecx,%esi
  800b0f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b11:	85 c0                	test   %eax,%eax
  800b13:	7e 17                	jle    800b2c <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b15:	83 ec 0c             	sub    $0xc,%esp
  800b18:	50                   	push   %eax
  800b19:	6a 03                	push   $0x3
  800b1b:	68 c4 15 80 00       	push   $0x8015c4
  800b20:	6a 23                	push   $0x23
  800b22:	68 e1 15 80 00       	push   $0x8015e1
  800b27:	e8 26 f6 ff ff       	call   800152 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b2c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b2f:	5b                   	pop    %ebx
  800b30:	5e                   	pop    %esi
  800b31:	5f                   	pop    %edi
  800b32:	5d                   	pop    %ebp
  800b33:	c3                   	ret    

00800b34 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b34:	55                   	push   %ebp
  800b35:	89 e5                	mov    %esp,%ebp
  800b37:	57                   	push   %edi
  800b38:	56                   	push   %esi
  800b39:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b3a:	ba 00 00 00 00       	mov    $0x0,%edx
  800b3f:	b8 02 00 00 00       	mov    $0x2,%eax
  800b44:	89 d1                	mov    %edx,%ecx
  800b46:	89 d3                	mov    %edx,%ebx
  800b48:	89 d7                	mov    %edx,%edi
  800b4a:	89 d6                	mov    %edx,%esi
  800b4c:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b4e:	5b                   	pop    %ebx
  800b4f:	5e                   	pop    %esi
  800b50:	5f                   	pop    %edi
  800b51:	5d                   	pop    %ebp
  800b52:	c3                   	ret    

00800b53 <sys_yield>:

void
sys_yield(void)
{
  800b53:	55                   	push   %ebp
  800b54:	89 e5                	mov    %esp,%ebp
  800b56:	57                   	push   %edi
  800b57:	56                   	push   %esi
  800b58:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b59:	ba 00 00 00 00       	mov    $0x0,%edx
  800b5e:	b8 0a 00 00 00       	mov    $0xa,%eax
  800b63:	89 d1                	mov    %edx,%ecx
  800b65:	89 d3                	mov    %edx,%ebx
  800b67:	89 d7                	mov    %edx,%edi
  800b69:	89 d6                	mov    %edx,%esi
  800b6b:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b6d:	5b                   	pop    %ebx
  800b6e:	5e                   	pop    %esi
  800b6f:	5f                   	pop    %edi
  800b70:	5d                   	pop    %ebp
  800b71:	c3                   	ret    

00800b72 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b72:	55                   	push   %ebp
  800b73:	89 e5                	mov    %esp,%ebp
  800b75:	57                   	push   %edi
  800b76:	56                   	push   %esi
  800b77:	53                   	push   %ebx
  800b78:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b7b:	be 00 00 00 00       	mov    $0x0,%esi
  800b80:	b8 04 00 00 00       	mov    $0x4,%eax
  800b85:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b88:	8b 55 08             	mov    0x8(%ebp),%edx
  800b8b:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b8e:	89 f7                	mov    %esi,%edi
  800b90:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b92:	85 c0                	test   %eax,%eax
  800b94:	7e 17                	jle    800bad <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b96:	83 ec 0c             	sub    $0xc,%esp
  800b99:	50                   	push   %eax
  800b9a:	6a 04                	push   $0x4
  800b9c:	68 c4 15 80 00       	push   $0x8015c4
  800ba1:	6a 23                	push   $0x23
  800ba3:	68 e1 15 80 00       	push   $0x8015e1
  800ba8:	e8 a5 f5 ff ff       	call   800152 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800bad:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bb0:	5b                   	pop    %ebx
  800bb1:	5e                   	pop    %esi
  800bb2:	5f                   	pop    %edi
  800bb3:	5d                   	pop    %ebp
  800bb4:	c3                   	ret    

00800bb5 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800bb5:	55                   	push   %ebp
  800bb6:	89 e5                	mov    %esp,%ebp
  800bb8:	57                   	push   %edi
  800bb9:	56                   	push   %esi
  800bba:	53                   	push   %ebx
  800bbb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bbe:	b8 05 00 00 00       	mov    $0x5,%eax
  800bc3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bc6:	8b 55 08             	mov    0x8(%ebp),%edx
  800bc9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800bcc:	8b 7d 14             	mov    0x14(%ebp),%edi
  800bcf:	8b 75 18             	mov    0x18(%ebp),%esi
  800bd2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bd4:	85 c0                	test   %eax,%eax
  800bd6:	7e 17                	jle    800bef <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bd8:	83 ec 0c             	sub    $0xc,%esp
  800bdb:	50                   	push   %eax
  800bdc:	6a 05                	push   $0x5
  800bde:	68 c4 15 80 00       	push   $0x8015c4
  800be3:	6a 23                	push   $0x23
  800be5:	68 e1 15 80 00       	push   $0x8015e1
  800bea:	e8 63 f5 ff ff       	call   800152 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bef:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf2:	5b                   	pop    %ebx
  800bf3:	5e                   	pop    %esi
  800bf4:	5f                   	pop    %edi
  800bf5:	5d                   	pop    %ebp
  800bf6:	c3                   	ret    

00800bf7 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bf7:	55                   	push   %ebp
  800bf8:	89 e5                	mov    %esp,%ebp
  800bfa:	57                   	push   %edi
  800bfb:	56                   	push   %esi
  800bfc:	53                   	push   %ebx
  800bfd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c00:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c05:	b8 06 00 00 00       	mov    $0x6,%eax
  800c0a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c0d:	8b 55 08             	mov    0x8(%ebp),%edx
  800c10:	89 df                	mov    %ebx,%edi
  800c12:	89 de                	mov    %ebx,%esi
  800c14:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c16:	85 c0                	test   %eax,%eax
  800c18:	7e 17                	jle    800c31 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c1a:	83 ec 0c             	sub    $0xc,%esp
  800c1d:	50                   	push   %eax
  800c1e:	6a 06                	push   $0x6
  800c20:	68 c4 15 80 00       	push   $0x8015c4
  800c25:	6a 23                	push   $0x23
  800c27:	68 e1 15 80 00       	push   $0x8015e1
  800c2c:	e8 21 f5 ff ff       	call   800152 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c31:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c34:	5b                   	pop    %ebx
  800c35:	5e                   	pop    %esi
  800c36:	5f                   	pop    %edi
  800c37:	5d                   	pop    %ebp
  800c38:	c3                   	ret    

00800c39 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c39:	55                   	push   %ebp
  800c3a:	89 e5                	mov    %esp,%ebp
  800c3c:	57                   	push   %edi
  800c3d:	56                   	push   %esi
  800c3e:	53                   	push   %ebx
  800c3f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c42:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c47:	b8 08 00 00 00       	mov    $0x8,%eax
  800c4c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c4f:	8b 55 08             	mov    0x8(%ebp),%edx
  800c52:	89 df                	mov    %ebx,%edi
  800c54:	89 de                	mov    %ebx,%esi
  800c56:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c58:	85 c0                	test   %eax,%eax
  800c5a:	7e 17                	jle    800c73 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c5c:	83 ec 0c             	sub    $0xc,%esp
  800c5f:	50                   	push   %eax
  800c60:	6a 08                	push   $0x8
  800c62:	68 c4 15 80 00       	push   $0x8015c4
  800c67:	6a 23                	push   $0x23
  800c69:	68 e1 15 80 00       	push   $0x8015e1
  800c6e:	e8 df f4 ff ff       	call   800152 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c73:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c76:	5b                   	pop    %ebx
  800c77:	5e                   	pop    %esi
  800c78:	5f                   	pop    %edi
  800c79:	5d                   	pop    %ebp
  800c7a:	c3                   	ret    

00800c7b <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c7b:	55                   	push   %ebp
  800c7c:	89 e5                	mov    %esp,%ebp
  800c7e:	57                   	push   %edi
  800c7f:	56                   	push   %esi
  800c80:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c81:	ba 00 00 00 00       	mov    $0x0,%edx
  800c86:	b8 0b 00 00 00       	mov    $0xb,%eax
  800c8b:	89 d1                	mov    %edx,%ecx
  800c8d:	89 d3                	mov    %edx,%ebx
  800c8f:	89 d7                	mov    %edx,%edi
  800c91:	89 d6                	mov    %edx,%esi
  800c93:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c95:	5b                   	pop    %ebx
  800c96:	5e                   	pop    %esi
  800c97:	5f                   	pop    %edi
  800c98:	5d                   	pop    %ebp
  800c99:	c3                   	ret    

00800c9a <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c9a:	55                   	push   %ebp
  800c9b:	89 e5                	mov    %esp,%ebp
  800c9d:	57                   	push   %edi
  800c9e:	56                   	push   %esi
  800c9f:	53                   	push   %ebx
  800ca0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ca3:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ca8:	b8 09 00 00 00       	mov    $0x9,%eax
  800cad:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cb0:	8b 55 08             	mov    0x8(%ebp),%edx
  800cb3:	89 df                	mov    %ebx,%edi
  800cb5:	89 de                	mov    %ebx,%esi
  800cb7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cb9:	85 c0                	test   %eax,%eax
  800cbb:	7e 17                	jle    800cd4 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cbd:	83 ec 0c             	sub    $0xc,%esp
  800cc0:	50                   	push   %eax
  800cc1:	6a 09                	push   $0x9
  800cc3:	68 c4 15 80 00       	push   $0x8015c4
  800cc8:	6a 23                	push   $0x23
  800cca:	68 e1 15 80 00       	push   $0x8015e1
  800ccf:	e8 7e f4 ff ff       	call   800152 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cd4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cd7:	5b                   	pop    %ebx
  800cd8:	5e                   	pop    %esi
  800cd9:	5f                   	pop    %edi
  800cda:	5d                   	pop    %ebp
  800cdb:	c3                   	ret    

00800cdc <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800cdc:	55                   	push   %ebp
  800cdd:	89 e5                	mov    %esp,%ebp
  800cdf:	57                   	push   %edi
  800ce0:	56                   	push   %esi
  800ce1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ce2:	be 00 00 00 00       	mov    $0x0,%esi
  800ce7:	b8 0c 00 00 00       	mov    $0xc,%eax
  800cec:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cef:	8b 55 08             	mov    0x8(%ebp),%edx
  800cf2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cf5:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cf8:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800cfa:	5b                   	pop    %ebx
  800cfb:	5e                   	pop    %esi
  800cfc:	5f                   	pop    %edi
  800cfd:	5d                   	pop    %ebp
  800cfe:	c3                   	ret    

00800cff <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800cff:	55                   	push   %ebp
  800d00:	89 e5                	mov    %esp,%ebp
  800d02:	57                   	push   %edi
  800d03:	56                   	push   %esi
  800d04:	53                   	push   %ebx
  800d05:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d08:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d0d:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d12:	8b 55 08             	mov    0x8(%ebp),%edx
  800d15:	89 cb                	mov    %ecx,%ebx
  800d17:	89 cf                	mov    %ecx,%edi
  800d19:	89 ce                	mov    %ecx,%esi
  800d1b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d1d:	85 c0                	test   %eax,%eax
  800d1f:	7e 17                	jle    800d38 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d21:	83 ec 0c             	sub    $0xc,%esp
  800d24:	50                   	push   %eax
  800d25:	6a 0d                	push   $0xd
  800d27:	68 c4 15 80 00       	push   $0x8015c4
  800d2c:	6a 23                	push   $0x23
  800d2e:	68 e1 15 80 00       	push   $0x8015e1
  800d33:	e8 1a f4 ff ff       	call   800152 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d38:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d3b:	5b                   	pop    %ebx
  800d3c:	5e                   	pop    %esi
  800d3d:	5f                   	pop    %edi
  800d3e:	5d                   	pop    %ebp
  800d3f:	c3                   	ret    

00800d40 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800d40:	55                   	push   %ebp
  800d41:	89 e5                	mov    %esp,%ebp
  800d43:	53                   	push   %ebx
  800d44:	83 ec 04             	sub    $0x4,%esp
  800d47:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800d4a:	8b 18                	mov    (%eax),%ebx
	//   You should make three system calls.


    
    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800d4c:	89 da                	mov    %ebx,%edx
  800d4e:	c1 ea 0c             	shr    $0xc,%edx
  800d51:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800d58:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800d5c:	74 05                	je     800d63 <pgfault+0x23>
  800d5e:	f6 c6 08             	test   $0x8,%dh
  800d61:	75 14                	jne    800d77 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800d63:	83 ec 04             	sub    $0x4,%esp
  800d66:	68 f0 15 80 00       	push   $0x8015f0
  800d6b:	6a 28                	push   $0x28
  800d6d:	68 54 16 80 00       	push   $0x801654
  800d72:	e8 db f3 ff ff       	call   800152 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800d77:	83 ec 04             	sub    $0x4,%esp
  800d7a:	6a 07                	push   $0x7
  800d7c:	68 00 f0 7f 00       	push   $0x7ff000
  800d81:	6a 00                	push   $0x0
  800d83:	e8 ea fd ff ff       	call   800b72 <sys_page_alloc>
  800d88:	83 c4 10             	add    $0x10,%esp
  800d8b:	85 c0                	test   %eax,%eax
  800d8d:	74 14                	je     800da3 <pgfault+0x63>
        panic("pgfault: no phys mem");
  800d8f:	83 ec 04             	sub    $0x4,%esp
  800d92:	68 5f 16 80 00       	push   $0x80165f
  800d97:	6a 34                	push   $0x34
  800d99:	68 54 16 80 00       	push   $0x801654
  800d9e:	e8 af f3 ff ff       	call   800152 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800da3:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800da9:	83 ec 04             	sub    $0x4,%esp
  800dac:	68 00 10 00 00       	push   $0x1000
  800db1:	53                   	push   %ebx
  800db2:	68 00 f0 7f 00       	push   $0x7ff000
  800db7:	e8 47 fb ff ff       	call   800903 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800dbc:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800dc3:	53                   	push   %ebx
  800dc4:	6a 00                	push   $0x0
  800dc6:	68 00 f0 7f 00       	push   $0x7ff000
  800dcb:	6a 00                	push   $0x0
  800dcd:	e8 e3 fd ff ff       	call   800bb5 <sys_page_map>
  800dd2:	83 c4 20             	add    $0x20,%esp
  800dd5:	85 c0                	test   %eax,%eax
  800dd7:	74 14                	je     800ded <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800dd9:	83 ec 04             	sub    $0x4,%esp
  800ddc:	68 74 16 80 00       	push   $0x801674
  800de1:	6a 41                	push   $0x41
  800de3:	68 54 16 80 00       	push   $0x801654
  800de8:	e8 65 f3 ff ff       	call   800152 <_panic>


	//panic("pgfault not implemented");
}
  800ded:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800df0:	c9                   	leave  
  800df1:	c3                   	ret    

00800df2 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800df2:	55                   	push   %ebp
  800df3:	89 e5                	mov    %esp,%ebp
  800df5:	57                   	push   %edi
  800df6:	56                   	push   %esi
  800df7:	53                   	push   %ebx
  800df8:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800dfb:	68 40 0d 80 00       	push   $0x800d40
  800e00:	e8 05 02 00 00       	call   80100a <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800e05:	b8 07 00 00 00       	mov    $0x7,%eax
  800e0a:	cd 30                	int    $0x30
  800e0c:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800e0f:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800e12:	83 c4 10             	add    $0x10,%esp
  800e15:	85 c0                	test   %eax,%eax
  800e17:	79 17                	jns    800e30 <fork+0x3e>
        panic("fork: cannot create child env");
  800e19:	83 ec 04             	sub    $0x4,%esp
  800e1c:	68 87 16 80 00       	push   $0x801687
  800e21:	68 96 00 00 00       	push   $0x96
  800e26:	68 54 16 80 00       	push   $0x801654
  800e2b:	e8 22 f3 ff ff       	call   800152 <_panic>
    } else if (envid == 0) {
  800e30:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800e34:	75 2a                	jne    800e60 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800e36:	e8 f9 fc ff ff       	call   800b34 <sys_getenvid>
  800e3b:	25 ff 03 00 00       	and    $0x3ff,%eax
  800e40:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e47:	c1 e0 07             	shl    $0x7,%eax
  800e4a:	29 d0                	sub    %edx,%eax
  800e4c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800e51:	a3 08 20 80 00       	mov    %eax,0x802008
        return 0;
  800e56:	b8 00 00 00 00       	mov    $0x0,%eax
  800e5b:	e9 88 01 00 00       	jmp    800fe8 <fork+0x1f6>
  800e60:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800e67:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800e6a:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800e71:	a8 01                	test   $0x1,%al
  800e73:	0f 84 fe 00 00 00    	je     800f77 <fork+0x185>
            continue;

        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800e79:	c1 e7 0a             	shl    $0xa,%edi
  800e7c:	be 00 00 00 00       	mov    $0x0,%esi
  800e81:	89 fb                	mov    %edi,%ebx
  800e83:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800e85:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800e8b:	75 34                	jne    800ec1 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800e8d:	83 ec 04             	sub    $0x4,%esp
  800e90:	6a 07                	push   $0x7
  800e92:	68 00 f0 bf ee       	push   $0xeebff000
  800e97:	ff 75 e4             	pushl  -0x1c(%ebp)
  800e9a:	e8 d3 fc ff ff       	call   800b72 <sys_page_alloc>
  800e9f:	83 c4 10             	add    $0x10,%esp
  800ea2:	85 c0                	test   %eax,%eax
  800ea4:	0f 84 c0 00 00 00    	je     800f6a <fork+0x178>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800eaa:	83 ec 04             	sub    $0x4,%esp
  800ead:	68 a5 16 80 00       	push   $0x8016a5
  800eb2:	68 ac 00 00 00       	push   $0xac
  800eb7:	68 54 16 80 00       	push   $0x801654
  800ebc:	e8 91 f2 ff ff       	call   800152 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800ec1:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800ec8:	a8 01                	test   $0x1,%al
  800eca:	0f 84 9a 00 00 00    	je     800f6a <fork+0x178>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800ed0:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800ed7:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if ((pte & PTE_W) || (pte & PTE_COW)) {
  800eda:	a9 02 08 00 00       	test   $0x802,%eax
  800edf:	74 5d                	je     800f3e <fork+0x14c>
        if (sys_page_map(0,
  800ee1:	83 ec 0c             	sub    $0xc,%esp
  800ee4:	68 05 08 00 00       	push   $0x805
  800ee9:	53                   	push   %ebx
  800eea:	ff 75 e4             	pushl  -0x1c(%ebp)
  800eed:	53                   	push   %ebx
  800eee:	6a 00                	push   $0x0
  800ef0:	e8 c0 fc ff ff       	call   800bb5 <sys_page_map>
  800ef5:	83 c4 20             	add    $0x20,%esp
  800ef8:	85 c0                	test   %eax,%eax
  800efa:	74 14                	je     800f10 <fork+0x11e>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800efc:	83 ec 04             	sub    $0x4,%esp
  800eff:	68 c0 16 80 00       	push   $0x8016c0
  800f04:	6a 66                	push   $0x66
  800f06:	68 54 16 80 00       	push   $0x801654
  800f0b:	e8 42 f2 ff ff       	call   800152 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800f10:	83 ec 0c             	sub    $0xc,%esp
  800f13:	68 05 08 00 00       	push   $0x805
  800f18:	53                   	push   %ebx
  800f19:	6a 00                	push   $0x0
  800f1b:	53                   	push   %ebx
  800f1c:	6a 00                	push   $0x0
  800f1e:	e8 92 fc ff ff       	call   800bb5 <sys_page_map>
  800f23:	83 c4 20             	add    $0x20,%esp
  800f26:	85 c0                	test   %eax,%eax
  800f28:	74 40                	je     800f6a <fork+0x178>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  800f2a:	83 ec 04             	sub    $0x4,%esp
  800f2d:	68 d7 16 80 00       	push   $0x8016d7
  800f32:	6a 6f                	push   $0x6f
  800f34:	68 54 16 80 00       	push   $0x801654
  800f39:	e8 14 f2 ff ff       	call   800152 <_panic>
    } else if (sys_page_map(0,
  800f3e:	83 ec 0c             	sub    $0xc,%esp
  800f41:	6a 05                	push   $0x5
  800f43:	53                   	push   %ebx
  800f44:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f47:	53                   	push   %ebx
  800f48:	6a 00                	push   $0x0
  800f4a:	e8 66 fc ff ff       	call   800bb5 <sys_page_map>
  800f4f:	83 c4 20             	add    $0x20,%esp
  800f52:	85 c0                	test   %eax,%eax
  800f54:	74 14                	je     800f6a <fork+0x178>
                            va,
                            envid,
                            va,
                            PTE_U | PTE_P))
        panic("duppage: map ro error");
  800f56:	83 ec 04             	sub    $0x4,%esp
  800f59:	68 f2 16 80 00       	push   $0x8016f2
  800f5e:	6a 75                	push   $0x75
  800f60:	68 54 16 80 00       	push   $0x801654
  800f65:	e8 e8 f1 ff ff       	call   800152 <_panic>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;

        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  800f6a:	46                   	inc    %esi
  800f6b:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  800f71:	0f 85 0a ff ff ff    	jne    800e81 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  800f77:	ff 45 e0             	incl   -0x20(%ebp)
  800f7a:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800f7d:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  800f82:	0f 85 df fe ff ff    	jne    800e67 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  800f88:	a1 08 20 80 00       	mov    0x802008,%eax
  800f8d:	8b 40 64             	mov    0x64(%eax),%eax
  800f90:	83 ec 08             	sub    $0x8,%esp
  800f93:	50                   	push   %eax
  800f94:	ff 75 dc             	pushl  -0x24(%ebp)
  800f97:	e8 fe fc ff ff       	call   800c9a <sys_env_set_pgfault_upcall>
  800f9c:	83 c4 10             	add    $0x10,%esp
  800f9f:	85 c0                	test   %eax,%eax
  800fa1:	74 17                	je     800fba <fork+0x1c8>
        panic("fork: cannot set pgfault upcall");
  800fa3:	83 ec 04             	sub    $0x4,%esp
  800fa6:	68 34 16 80 00       	push   $0x801634
  800fab:	68 b8 00 00 00       	push   $0xb8
  800fb0:	68 54 16 80 00       	push   $0x801654
  800fb5:	e8 98 f1 ff ff       	call   800152 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  800fba:	83 ec 08             	sub    $0x8,%esp
  800fbd:	6a 02                	push   $0x2
  800fbf:	ff 75 dc             	pushl  -0x24(%ebp)
  800fc2:	e8 72 fc ff ff       	call   800c39 <sys_env_set_status>
  800fc7:	83 c4 10             	add    $0x10,%esp
  800fca:	85 c0                	test   %eax,%eax
  800fcc:	74 17                	je     800fe5 <fork+0x1f3>
        panic("fork: cannot set env status");
  800fce:	83 ec 04             	sub    $0x4,%esp
  800fd1:	68 08 17 80 00       	push   $0x801708
  800fd6:	68 bc 00 00 00       	push   $0xbc
  800fdb:	68 54 16 80 00       	push   $0x801654
  800fe0:	e8 6d f1 ff ff       	call   800152 <_panic>

    return envid;
  800fe5:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  800fe8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800feb:	5b                   	pop    %ebx
  800fec:	5e                   	pop    %esi
  800fed:	5f                   	pop    %edi
  800fee:	5d                   	pop    %ebp
  800fef:	c3                   	ret    

00800ff0 <sfork>:

// Challenge!
int
sfork(void)
{
  800ff0:	55                   	push   %ebp
  800ff1:	89 e5                	mov    %esp,%ebp
  800ff3:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  800ff6:	68 24 17 80 00       	push   $0x801724
  800ffb:	68 c7 00 00 00       	push   $0xc7
  801000:	68 54 16 80 00       	push   $0x801654
  801005:	e8 48 f1 ff ff       	call   800152 <_panic>

0080100a <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  80100a:	55                   	push   %ebp
  80100b:	89 e5                	mov    %esp,%ebp
  80100d:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801010:	83 3d 0c 20 80 00 00 	cmpl   $0x0,0x80200c
  801017:	75 3e                	jne    801057 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801019:	83 ec 04             	sub    $0x4,%esp
  80101c:	6a 07                	push   $0x7
  80101e:	68 00 f0 bf ee       	push   $0xeebff000
  801023:	6a 00                	push   $0x0
  801025:	e8 48 fb ff ff       	call   800b72 <sys_page_alloc>
  80102a:	83 c4 10             	add    $0x10,%esp
  80102d:	85 c0                	test   %eax,%eax
  80102f:	74 14                	je     801045 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801031:	83 ec 04             	sub    $0x4,%esp
  801034:	68 3c 17 80 00       	push   $0x80173c
  801039:	6a 23                	push   $0x23
  80103b:	68 60 17 80 00       	push   $0x801760
  801040:	e8 0d f1 ff ff       	call   800152 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801045:	83 ec 08             	sub    $0x8,%esp
  801048:	68 61 10 80 00       	push   $0x801061
  80104d:	6a 00                	push   $0x0
  80104f:	e8 46 fc ff ff       	call   800c9a <sys_env_set_pgfault_upcall>
  801054:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801057:	8b 45 08             	mov    0x8(%ebp),%eax
  80105a:	a3 0c 20 80 00       	mov    %eax,0x80200c
}
  80105f:	c9                   	leave  
  801060:	c3                   	ret    

00801061 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801061:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801062:	a1 0c 20 80 00       	mov    0x80200c,%eax
	call *%eax
  801067:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801069:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  80106c:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  80106e:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801072:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801076:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801077:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  80107a:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  80107c:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  80107f:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801082:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801083:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  801086:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801087:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801088:	c3                   	ret    
  801089:	66 90                	xchg   %ax,%ax
  80108b:	90                   	nop

0080108c <__udivdi3>:
  80108c:	55                   	push   %ebp
  80108d:	57                   	push   %edi
  80108e:	56                   	push   %esi
  80108f:	53                   	push   %ebx
  801090:	83 ec 1c             	sub    $0x1c,%esp
  801093:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801097:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  80109b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80109f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8010a3:	89 ca                	mov    %ecx,%edx
  8010a5:	89 f8                	mov    %edi,%eax
  8010a7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8010ab:	85 f6                	test   %esi,%esi
  8010ad:	75 2d                	jne    8010dc <__udivdi3+0x50>
  8010af:	39 cf                	cmp    %ecx,%edi
  8010b1:	77 65                	ja     801118 <__udivdi3+0x8c>
  8010b3:	89 fd                	mov    %edi,%ebp
  8010b5:	85 ff                	test   %edi,%edi
  8010b7:	75 0b                	jne    8010c4 <__udivdi3+0x38>
  8010b9:	b8 01 00 00 00       	mov    $0x1,%eax
  8010be:	31 d2                	xor    %edx,%edx
  8010c0:	f7 f7                	div    %edi
  8010c2:	89 c5                	mov    %eax,%ebp
  8010c4:	31 d2                	xor    %edx,%edx
  8010c6:	89 c8                	mov    %ecx,%eax
  8010c8:	f7 f5                	div    %ebp
  8010ca:	89 c1                	mov    %eax,%ecx
  8010cc:	89 d8                	mov    %ebx,%eax
  8010ce:	f7 f5                	div    %ebp
  8010d0:	89 cf                	mov    %ecx,%edi
  8010d2:	89 fa                	mov    %edi,%edx
  8010d4:	83 c4 1c             	add    $0x1c,%esp
  8010d7:	5b                   	pop    %ebx
  8010d8:	5e                   	pop    %esi
  8010d9:	5f                   	pop    %edi
  8010da:	5d                   	pop    %ebp
  8010db:	c3                   	ret    
  8010dc:	39 ce                	cmp    %ecx,%esi
  8010de:	77 28                	ja     801108 <__udivdi3+0x7c>
  8010e0:	0f bd fe             	bsr    %esi,%edi
  8010e3:	83 f7 1f             	xor    $0x1f,%edi
  8010e6:	75 40                	jne    801128 <__udivdi3+0x9c>
  8010e8:	39 ce                	cmp    %ecx,%esi
  8010ea:	72 0a                	jb     8010f6 <__udivdi3+0x6a>
  8010ec:	3b 44 24 08          	cmp    0x8(%esp),%eax
  8010f0:	0f 87 9e 00 00 00    	ja     801194 <__udivdi3+0x108>
  8010f6:	b8 01 00 00 00       	mov    $0x1,%eax
  8010fb:	89 fa                	mov    %edi,%edx
  8010fd:	83 c4 1c             	add    $0x1c,%esp
  801100:	5b                   	pop    %ebx
  801101:	5e                   	pop    %esi
  801102:	5f                   	pop    %edi
  801103:	5d                   	pop    %ebp
  801104:	c3                   	ret    
  801105:	8d 76 00             	lea    0x0(%esi),%esi
  801108:	31 ff                	xor    %edi,%edi
  80110a:	31 c0                	xor    %eax,%eax
  80110c:	89 fa                	mov    %edi,%edx
  80110e:	83 c4 1c             	add    $0x1c,%esp
  801111:	5b                   	pop    %ebx
  801112:	5e                   	pop    %esi
  801113:	5f                   	pop    %edi
  801114:	5d                   	pop    %ebp
  801115:	c3                   	ret    
  801116:	66 90                	xchg   %ax,%ax
  801118:	89 d8                	mov    %ebx,%eax
  80111a:	f7 f7                	div    %edi
  80111c:	31 ff                	xor    %edi,%edi
  80111e:	89 fa                	mov    %edi,%edx
  801120:	83 c4 1c             	add    $0x1c,%esp
  801123:	5b                   	pop    %ebx
  801124:	5e                   	pop    %esi
  801125:	5f                   	pop    %edi
  801126:	5d                   	pop    %ebp
  801127:	c3                   	ret    
  801128:	bd 20 00 00 00       	mov    $0x20,%ebp
  80112d:	89 eb                	mov    %ebp,%ebx
  80112f:	29 fb                	sub    %edi,%ebx
  801131:	89 f9                	mov    %edi,%ecx
  801133:	d3 e6                	shl    %cl,%esi
  801135:	89 c5                	mov    %eax,%ebp
  801137:	88 d9                	mov    %bl,%cl
  801139:	d3 ed                	shr    %cl,%ebp
  80113b:	89 e9                	mov    %ebp,%ecx
  80113d:	09 f1                	or     %esi,%ecx
  80113f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801143:	89 f9                	mov    %edi,%ecx
  801145:	d3 e0                	shl    %cl,%eax
  801147:	89 c5                	mov    %eax,%ebp
  801149:	89 d6                	mov    %edx,%esi
  80114b:	88 d9                	mov    %bl,%cl
  80114d:	d3 ee                	shr    %cl,%esi
  80114f:	89 f9                	mov    %edi,%ecx
  801151:	d3 e2                	shl    %cl,%edx
  801153:	8b 44 24 08          	mov    0x8(%esp),%eax
  801157:	88 d9                	mov    %bl,%cl
  801159:	d3 e8                	shr    %cl,%eax
  80115b:	09 c2                	or     %eax,%edx
  80115d:	89 d0                	mov    %edx,%eax
  80115f:	89 f2                	mov    %esi,%edx
  801161:	f7 74 24 0c          	divl   0xc(%esp)
  801165:	89 d6                	mov    %edx,%esi
  801167:	89 c3                	mov    %eax,%ebx
  801169:	f7 e5                	mul    %ebp
  80116b:	39 d6                	cmp    %edx,%esi
  80116d:	72 19                	jb     801188 <__udivdi3+0xfc>
  80116f:	74 0b                	je     80117c <__udivdi3+0xf0>
  801171:	89 d8                	mov    %ebx,%eax
  801173:	31 ff                	xor    %edi,%edi
  801175:	e9 58 ff ff ff       	jmp    8010d2 <__udivdi3+0x46>
  80117a:	66 90                	xchg   %ax,%ax
  80117c:	8b 54 24 08          	mov    0x8(%esp),%edx
  801180:	89 f9                	mov    %edi,%ecx
  801182:	d3 e2                	shl    %cl,%edx
  801184:	39 c2                	cmp    %eax,%edx
  801186:	73 e9                	jae    801171 <__udivdi3+0xe5>
  801188:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80118b:	31 ff                	xor    %edi,%edi
  80118d:	e9 40 ff ff ff       	jmp    8010d2 <__udivdi3+0x46>
  801192:	66 90                	xchg   %ax,%ax
  801194:	31 c0                	xor    %eax,%eax
  801196:	e9 37 ff ff ff       	jmp    8010d2 <__udivdi3+0x46>
  80119b:	90                   	nop

0080119c <__umoddi3>:
  80119c:	55                   	push   %ebp
  80119d:	57                   	push   %edi
  80119e:	56                   	push   %esi
  80119f:	53                   	push   %ebx
  8011a0:	83 ec 1c             	sub    $0x1c,%esp
  8011a3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8011a7:	8b 74 24 34          	mov    0x34(%esp),%esi
  8011ab:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8011af:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8011b3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8011b7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8011bb:	89 f3                	mov    %esi,%ebx
  8011bd:	89 fa                	mov    %edi,%edx
  8011bf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8011c3:	89 34 24             	mov    %esi,(%esp)
  8011c6:	85 c0                	test   %eax,%eax
  8011c8:	75 1a                	jne    8011e4 <__umoddi3+0x48>
  8011ca:	39 f7                	cmp    %esi,%edi
  8011cc:	0f 86 a2 00 00 00    	jbe    801274 <__umoddi3+0xd8>
  8011d2:	89 c8                	mov    %ecx,%eax
  8011d4:	89 f2                	mov    %esi,%edx
  8011d6:	f7 f7                	div    %edi
  8011d8:	89 d0                	mov    %edx,%eax
  8011da:	31 d2                	xor    %edx,%edx
  8011dc:	83 c4 1c             	add    $0x1c,%esp
  8011df:	5b                   	pop    %ebx
  8011e0:	5e                   	pop    %esi
  8011e1:	5f                   	pop    %edi
  8011e2:	5d                   	pop    %ebp
  8011e3:	c3                   	ret    
  8011e4:	39 f0                	cmp    %esi,%eax
  8011e6:	0f 87 ac 00 00 00    	ja     801298 <__umoddi3+0xfc>
  8011ec:	0f bd e8             	bsr    %eax,%ebp
  8011ef:	83 f5 1f             	xor    $0x1f,%ebp
  8011f2:	0f 84 ac 00 00 00    	je     8012a4 <__umoddi3+0x108>
  8011f8:	bf 20 00 00 00       	mov    $0x20,%edi
  8011fd:	29 ef                	sub    %ebp,%edi
  8011ff:	89 fe                	mov    %edi,%esi
  801201:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801205:	89 e9                	mov    %ebp,%ecx
  801207:	d3 e0                	shl    %cl,%eax
  801209:	89 d7                	mov    %edx,%edi
  80120b:	89 f1                	mov    %esi,%ecx
  80120d:	d3 ef                	shr    %cl,%edi
  80120f:	09 c7                	or     %eax,%edi
  801211:	89 e9                	mov    %ebp,%ecx
  801213:	d3 e2                	shl    %cl,%edx
  801215:	89 14 24             	mov    %edx,(%esp)
  801218:	89 d8                	mov    %ebx,%eax
  80121a:	d3 e0                	shl    %cl,%eax
  80121c:	89 c2                	mov    %eax,%edx
  80121e:	8b 44 24 08          	mov    0x8(%esp),%eax
  801222:	d3 e0                	shl    %cl,%eax
  801224:	89 44 24 04          	mov    %eax,0x4(%esp)
  801228:	8b 44 24 08          	mov    0x8(%esp),%eax
  80122c:	89 f1                	mov    %esi,%ecx
  80122e:	d3 e8                	shr    %cl,%eax
  801230:	09 d0                	or     %edx,%eax
  801232:	d3 eb                	shr    %cl,%ebx
  801234:	89 da                	mov    %ebx,%edx
  801236:	f7 f7                	div    %edi
  801238:	89 d3                	mov    %edx,%ebx
  80123a:	f7 24 24             	mull   (%esp)
  80123d:	89 c6                	mov    %eax,%esi
  80123f:	89 d1                	mov    %edx,%ecx
  801241:	39 d3                	cmp    %edx,%ebx
  801243:	0f 82 87 00 00 00    	jb     8012d0 <__umoddi3+0x134>
  801249:	0f 84 91 00 00 00    	je     8012e0 <__umoddi3+0x144>
  80124f:	8b 54 24 04          	mov    0x4(%esp),%edx
  801253:	29 f2                	sub    %esi,%edx
  801255:	19 cb                	sbb    %ecx,%ebx
  801257:	89 d8                	mov    %ebx,%eax
  801259:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  80125d:	d3 e0                	shl    %cl,%eax
  80125f:	89 e9                	mov    %ebp,%ecx
  801261:	d3 ea                	shr    %cl,%edx
  801263:	09 d0                	or     %edx,%eax
  801265:	89 e9                	mov    %ebp,%ecx
  801267:	d3 eb                	shr    %cl,%ebx
  801269:	89 da                	mov    %ebx,%edx
  80126b:	83 c4 1c             	add    $0x1c,%esp
  80126e:	5b                   	pop    %ebx
  80126f:	5e                   	pop    %esi
  801270:	5f                   	pop    %edi
  801271:	5d                   	pop    %ebp
  801272:	c3                   	ret    
  801273:	90                   	nop
  801274:	89 fd                	mov    %edi,%ebp
  801276:	85 ff                	test   %edi,%edi
  801278:	75 0b                	jne    801285 <__umoddi3+0xe9>
  80127a:	b8 01 00 00 00       	mov    $0x1,%eax
  80127f:	31 d2                	xor    %edx,%edx
  801281:	f7 f7                	div    %edi
  801283:	89 c5                	mov    %eax,%ebp
  801285:	89 f0                	mov    %esi,%eax
  801287:	31 d2                	xor    %edx,%edx
  801289:	f7 f5                	div    %ebp
  80128b:	89 c8                	mov    %ecx,%eax
  80128d:	f7 f5                	div    %ebp
  80128f:	89 d0                	mov    %edx,%eax
  801291:	e9 44 ff ff ff       	jmp    8011da <__umoddi3+0x3e>
  801296:	66 90                	xchg   %ax,%ax
  801298:	89 c8                	mov    %ecx,%eax
  80129a:	89 f2                	mov    %esi,%edx
  80129c:	83 c4 1c             	add    $0x1c,%esp
  80129f:	5b                   	pop    %ebx
  8012a0:	5e                   	pop    %esi
  8012a1:	5f                   	pop    %edi
  8012a2:	5d                   	pop    %ebp
  8012a3:	c3                   	ret    
  8012a4:	3b 04 24             	cmp    (%esp),%eax
  8012a7:	72 06                	jb     8012af <__umoddi3+0x113>
  8012a9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8012ad:	77 0f                	ja     8012be <__umoddi3+0x122>
  8012af:	89 f2                	mov    %esi,%edx
  8012b1:	29 f9                	sub    %edi,%ecx
  8012b3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8012b7:	89 14 24             	mov    %edx,(%esp)
  8012ba:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8012be:	8b 44 24 04          	mov    0x4(%esp),%eax
  8012c2:	8b 14 24             	mov    (%esp),%edx
  8012c5:	83 c4 1c             	add    $0x1c,%esp
  8012c8:	5b                   	pop    %ebx
  8012c9:	5e                   	pop    %esi
  8012ca:	5f                   	pop    %edi
  8012cb:	5d                   	pop    %ebp
  8012cc:	c3                   	ret    
  8012cd:	8d 76 00             	lea    0x0(%esi),%esi
  8012d0:	2b 04 24             	sub    (%esp),%eax
  8012d3:	19 fa                	sbb    %edi,%edx
  8012d5:	89 d1                	mov    %edx,%ecx
  8012d7:	89 c6                	mov    %eax,%esi
  8012d9:	e9 71 ff ff ff       	jmp    80124f <__umoddi3+0xb3>
  8012de:	66 90                	xchg   %ax,%ax
  8012e0:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8012e4:	72 ea                	jb     8012d0 <__umoddi3+0x134>
  8012e6:	89 d9                	mov    %ebx,%ecx
  8012e8:	e9 62 ff ff ff       	jmp    80124f <__umoddi3+0xb3>
