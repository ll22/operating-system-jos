
obj/user/faultevilhandler:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 34 00 00 00       	call   800065 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 0c             	sub    $0xc,%esp
	sys_page_alloc(0, (void*) (UXSTACKTOP - PGSIZE), PTE_P|PTE_U|PTE_W);
  800039:	6a 07                	push   $0x7
  80003b:	68 00 f0 bf ee       	push   $0xeebff000
  800040:	6a 00                	push   $0x0
  800042:	e8 3b 01 00 00       	call   800182 <sys_page_alloc>
	sys_env_set_pgfault_upcall(0, (void*) 0xF0100020);
  800047:	83 c4 08             	add    $0x8,%esp
  80004a:	68 20 00 10 f0       	push   $0xf0100020
  80004f:	6a 00                	push   $0x0
  800051:	e8 54 02 00 00       	call   8002aa <sys_env_set_pgfault_upcall>
	*(int*)0 = 0;
  800056:	c7 05 00 00 00 00 00 	movl   $0x0,0x0
  80005d:	00 00 00 
}
  800060:	83 c4 10             	add    $0x10,%esp
  800063:	c9                   	leave  
  800064:	c3                   	ret    

00800065 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800065:	55                   	push   %ebp
  800066:	89 e5                	mov    %esp,%ebp
  800068:	56                   	push   %esi
  800069:	53                   	push   %ebx
  80006a:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80006d:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800070:	e8 cf 00 00 00       	call   800144 <sys_getenvid>
  800075:	25 ff 03 00 00       	and    $0x3ff,%eax
  80007a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800081:	c1 e0 07             	shl    $0x7,%eax
  800084:	29 d0                	sub    %edx,%eax
  800086:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80008b:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800090:	85 db                	test   %ebx,%ebx
  800092:	7e 07                	jle    80009b <libmain+0x36>
		binaryname = argv[0];
  800094:	8b 06                	mov    (%esi),%eax
  800096:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80009b:	83 ec 08             	sub    $0x8,%esp
  80009e:	56                   	push   %esi
  80009f:	53                   	push   %ebx
  8000a0:	e8 8e ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000a5:	e8 0a 00 00 00       	call   8000b4 <exit>
}
  8000aa:	83 c4 10             	add    $0x10,%esp
  8000ad:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000b0:	5b                   	pop    %ebx
  8000b1:	5e                   	pop    %esi
  8000b2:	5d                   	pop    %ebp
  8000b3:	c3                   	ret    

008000b4 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000b4:	55                   	push   %ebp
  8000b5:	89 e5                	mov    %esp,%ebp
  8000b7:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000ba:	6a 00                	push   $0x0
  8000bc:	e8 42 00 00 00       	call   800103 <sys_env_destroy>
}
  8000c1:	83 c4 10             	add    $0x10,%esp
  8000c4:	c9                   	leave  
  8000c5:	c3                   	ret    

008000c6 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000c6:	55                   	push   %ebp
  8000c7:	89 e5                	mov    %esp,%ebp
  8000c9:	57                   	push   %edi
  8000ca:	56                   	push   %esi
  8000cb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cc:	b8 00 00 00 00       	mov    $0x0,%eax
  8000d1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000d4:	8b 55 08             	mov    0x8(%ebp),%edx
  8000d7:	89 c3                	mov    %eax,%ebx
  8000d9:	89 c7                	mov    %eax,%edi
  8000db:	89 c6                	mov    %eax,%esi
  8000dd:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000df:	5b                   	pop    %ebx
  8000e0:	5e                   	pop    %esi
  8000e1:	5f                   	pop    %edi
  8000e2:	5d                   	pop    %ebp
  8000e3:	c3                   	ret    

008000e4 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000e4:	55                   	push   %ebp
  8000e5:	89 e5                	mov    %esp,%ebp
  8000e7:	57                   	push   %edi
  8000e8:	56                   	push   %esi
  8000e9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ea:	ba 00 00 00 00       	mov    $0x0,%edx
  8000ef:	b8 01 00 00 00       	mov    $0x1,%eax
  8000f4:	89 d1                	mov    %edx,%ecx
  8000f6:	89 d3                	mov    %edx,%ebx
  8000f8:	89 d7                	mov    %edx,%edi
  8000fa:	89 d6                	mov    %edx,%esi
  8000fc:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000fe:	5b                   	pop    %ebx
  8000ff:	5e                   	pop    %esi
  800100:	5f                   	pop    %edi
  800101:	5d                   	pop    %ebp
  800102:	c3                   	ret    

00800103 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800103:	55                   	push   %ebp
  800104:	89 e5                	mov    %esp,%ebp
  800106:	57                   	push   %edi
  800107:	56                   	push   %esi
  800108:	53                   	push   %ebx
  800109:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80010c:	b9 00 00 00 00       	mov    $0x0,%ecx
  800111:	b8 03 00 00 00       	mov    $0x3,%eax
  800116:	8b 55 08             	mov    0x8(%ebp),%edx
  800119:	89 cb                	mov    %ecx,%ebx
  80011b:	89 cf                	mov    %ecx,%edi
  80011d:	89 ce                	mov    %ecx,%esi
  80011f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800121:	85 c0                	test   %eax,%eax
  800123:	7e 17                	jle    80013c <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800125:	83 ec 0c             	sub    $0xc,%esp
  800128:	50                   	push   %eax
  800129:	6a 03                	push   $0x3
  80012b:	68 2a 0f 80 00       	push   $0x800f2a
  800130:	6a 23                	push   $0x23
  800132:	68 47 0f 80 00       	push   $0x800f47
  800137:	e8 14 02 00 00       	call   800350 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80013c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80013f:	5b                   	pop    %ebx
  800140:	5e                   	pop    %esi
  800141:	5f                   	pop    %edi
  800142:	5d                   	pop    %ebp
  800143:	c3                   	ret    

00800144 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800144:	55                   	push   %ebp
  800145:	89 e5                	mov    %esp,%ebp
  800147:	57                   	push   %edi
  800148:	56                   	push   %esi
  800149:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80014a:	ba 00 00 00 00       	mov    $0x0,%edx
  80014f:	b8 02 00 00 00       	mov    $0x2,%eax
  800154:	89 d1                	mov    %edx,%ecx
  800156:	89 d3                	mov    %edx,%ebx
  800158:	89 d7                	mov    %edx,%edi
  80015a:	89 d6                	mov    %edx,%esi
  80015c:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80015e:	5b                   	pop    %ebx
  80015f:	5e                   	pop    %esi
  800160:	5f                   	pop    %edi
  800161:	5d                   	pop    %ebp
  800162:	c3                   	ret    

00800163 <sys_yield>:

void
sys_yield(void)
{
  800163:	55                   	push   %ebp
  800164:	89 e5                	mov    %esp,%ebp
  800166:	57                   	push   %edi
  800167:	56                   	push   %esi
  800168:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800169:	ba 00 00 00 00       	mov    $0x0,%edx
  80016e:	b8 0a 00 00 00       	mov    $0xa,%eax
  800173:	89 d1                	mov    %edx,%ecx
  800175:	89 d3                	mov    %edx,%ebx
  800177:	89 d7                	mov    %edx,%edi
  800179:	89 d6                	mov    %edx,%esi
  80017b:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80017d:	5b                   	pop    %ebx
  80017e:	5e                   	pop    %esi
  80017f:	5f                   	pop    %edi
  800180:	5d                   	pop    %ebp
  800181:	c3                   	ret    

00800182 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800182:	55                   	push   %ebp
  800183:	89 e5                	mov    %esp,%ebp
  800185:	57                   	push   %edi
  800186:	56                   	push   %esi
  800187:	53                   	push   %ebx
  800188:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80018b:	be 00 00 00 00       	mov    $0x0,%esi
  800190:	b8 04 00 00 00       	mov    $0x4,%eax
  800195:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800198:	8b 55 08             	mov    0x8(%ebp),%edx
  80019b:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80019e:	89 f7                	mov    %esi,%edi
  8001a0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001a2:	85 c0                	test   %eax,%eax
  8001a4:	7e 17                	jle    8001bd <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001a6:	83 ec 0c             	sub    $0xc,%esp
  8001a9:	50                   	push   %eax
  8001aa:	6a 04                	push   $0x4
  8001ac:	68 2a 0f 80 00       	push   $0x800f2a
  8001b1:	6a 23                	push   $0x23
  8001b3:	68 47 0f 80 00       	push   $0x800f47
  8001b8:	e8 93 01 00 00       	call   800350 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001c0:	5b                   	pop    %ebx
  8001c1:	5e                   	pop    %esi
  8001c2:	5f                   	pop    %edi
  8001c3:	5d                   	pop    %ebp
  8001c4:	c3                   	ret    

008001c5 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001c5:	55                   	push   %ebp
  8001c6:	89 e5                	mov    %esp,%ebp
  8001c8:	57                   	push   %edi
  8001c9:	56                   	push   %esi
  8001ca:	53                   	push   %ebx
  8001cb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ce:	b8 05 00 00 00       	mov    $0x5,%eax
  8001d3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001d6:	8b 55 08             	mov    0x8(%ebp),%edx
  8001d9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001dc:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001df:	8b 75 18             	mov    0x18(%ebp),%esi
  8001e2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001e4:	85 c0                	test   %eax,%eax
  8001e6:	7e 17                	jle    8001ff <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001e8:	83 ec 0c             	sub    $0xc,%esp
  8001eb:	50                   	push   %eax
  8001ec:	6a 05                	push   $0x5
  8001ee:	68 2a 0f 80 00       	push   $0x800f2a
  8001f3:	6a 23                	push   $0x23
  8001f5:	68 47 0f 80 00       	push   $0x800f47
  8001fa:	e8 51 01 00 00       	call   800350 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800202:	5b                   	pop    %ebx
  800203:	5e                   	pop    %esi
  800204:	5f                   	pop    %edi
  800205:	5d                   	pop    %ebp
  800206:	c3                   	ret    

00800207 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800207:	55                   	push   %ebp
  800208:	89 e5                	mov    %esp,%ebp
  80020a:	57                   	push   %edi
  80020b:	56                   	push   %esi
  80020c:	53                   	push   %ebx
  80020d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800210:	bb 00 00 00 00       	mov    $0x0,%ebx
  800215:	b8 06 00 00 00       	mov    $0x6,%eax
  80021a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80021d:	8b 55 08             	mov    0x8(%ebp),%edx
  800220:	89 df                	mov    %ebx,%edi
  800222:	89 de                	mov    %ebx,%esi
  800224:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800226:	85 c0                	test   %eax,%eax
  800228:	7e 17                	jle    800241 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80022a:	83 ec 0c             	sub    $0xc,%esp
  80022d:	50                   	push   %eax
  80022e:	6a 06                	push   $0x6
  800230:	68 2a 0f 80 00       	push   $0x800f2a
  800235:	6a 23                	push   $0x23
  800237:	68 47 0f 80 00       	push   $0x800f47
  80023c:	e8 0f 01 00 00       	call   800350 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800241:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800244:	5b                   	pop    %ebx
  800245:	5e                   	pop    %esi
  800246:	5f                   	pop    %edi
  800247:	5d                   	pop    %ebp
  800248:	c3                   	ret    

00800249 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800249:	55                   	push   %ebp
  80024a:	89 e5                	mov    %esp,%ebp
  80024c:	57                   	push   %edi
  80024d:	56                   	push   %esi
  80024e:	53                   	push   %ebx
  80024f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800252:	bb 00 00 00 00       	mov    $0x0,%ebx
  800257:	b8 08 00 00 00       	mov    $0x8,%eax
  80025c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80025f:	8b 55 08             	mov    0x8(%ebp),%edx
  800262:	89 df                	mov    %ebx,%edi
  800264:	89 de                	mov    %ebx,%esi
  800266:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800268:	85 c0                	test   %eax,%eax
  80026a:	7e 17                	jle    800283 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80026c:	83 ec 0c             	sub    $0xc,%esp
  80026f:	50                   	push   %eax
  800270:	6a 08                	push   $0x8
  800272:	68 2a 0f 80 00       	push   $0x800f2a
  800277:	6a 23                	push   $0x23
  800279:	68 47 0f 80 00       	push   $0x800f47
  80027e:	e8 cd 00 00 00       	call   800350 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800283:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800286:	5b                   	pop    %ebx
  800287:	5e                   	pop    %esi
  800288:	5f                   	pop    %edi
  800289:	5d                   	pop    %ebp
  80028a:	c3                   	ret    

0080028b <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80028b:	55                   	push   %ebp
  80028c:	89 e5                	mov    %esp,%ebp
  80028e:	57                   	push   %edi
  80028f:	56                   	push   %esi
  800290:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800291:	ba 00 00 00 00       	mov    $0x0,%edx
  800296:	b8 0b 00 00 00       	mov    $0xb,%eax
  80029b:	89 d1                	mov    %edx,%ecx
  80029d:	89 d3                	mov    %edx,%ebx
  80029f:	89 d7                	mov    %edx,%edi
  8002a1:	89 d6                	mov    %edx,%esi
  8002a3:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  8002a5:	5b                   	pop    %ebx
  8002a6:	5e                   	pop    %esi
  8002a7:	5f                   	pop    %edi
  8002a8:	5d                   	pop    %ebp
  8002a9:	c3                   	ret    

008002aa <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002aa:	55                   	push   %ebp
  8002ab:	89 e5                	mov    %esp,%ebp
  8002ad:	57                   	push   %edi
  8002ae:	56                   	push   %esi
  8002af:	53                   	push   %ebx
  8002b0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002b3:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002b8:	b8 09 00 00 00       	mov    $0x9,%eax
  8002bd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002c0:	8b 55 08             	mov    0x8(%ebp),%edx
  8002c3:	89 df                	mov    %ebx,%edi
  8002c5:	89 de                	mov    %ebx,%esi
  8002c7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002c9:	85 c0                	test   %eax,%eax
  8002cb:	7e 17                	jle    8002e4 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002cd:	83 ec 0c             	sub    $0xc,%esp
  8002d0:	50                   	push   %eax
  8002d1:	6a 09                	push   $0x9
  8002d3:	68 2a 0f 80 00       	push   $0x800f2a
  8002d8:	6a 23                	push   $0x23
  8002da:	68 47 0f 80 00       	push   $0x800f47
  8002df:	e8 6c 00 00 00       	call   800350 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002e4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002e7:	5b                   	pop    %ebx
  8002e8:	5e                   	pop    %esi
  8002e9:	5f                   	pop    %edi
  8002ea:	5d                   	pop    %ebp
  8002eb:	c3                   	ret    

008002ec <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002ec:	55                   	push   %ebp
  8002ed:	89 e5                	mov    %esp,%ebp
  8002ef:	57                   	push   %edi
  8002f0:	56                   	push   %esi
  8002f1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002f2:	be 00 00 00 00       	mov    $0x0,%esi
  8002f7:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002fc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002ff:	8b 55 08             	mov    0x8(%ebp),%edx
  800302:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800305:	8b 7d 14             	mov    0x14(%ebp),%edi
  800308:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  80030a:	5b                   	pop    %ebx
  80030b:	5e                   	pop    %esi
  80030c:	5f                   	pop    %edi
  80030d:	5d                   	pop    %ebp
  80030e:	c3                   	ret    

0080030f <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  80030f:	55                   	push   %ebp
  800310:	89 e5                	mov    %esp,%ebp
  800312:	57                   	push   %edi
  800313:	56                   	push   %esi
  800314:	53                   	push   %ebx
  800315:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800318:	b9 00 00 00 00       	mov    $0x0,%ecx
  80031d:	b8 0d 00 00 00       	mov    $0xd,%eax
  800322:	8b 55 08             	mov    0x8(%ebp),%edx
  800325:	89 cb                	mov    %ecx,%ebx
  800327:	89 cf                	mov    %ecx,%edi
  800329:	89 ce                	mov    %ecx,%esi
  80032b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80032d:	85 c0                	test   %eax,%eax
  80032f:	7e 17                	jle    800348 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800331:	83 ec 0c             	sub    $0xc,%esp
  800334:	50                   	push   %eax
  800335:	6a 0d                	push   $0xd
  800337:	68 2a 0f 80 00       	push   $0x800f2a
  80033c:	6a 23                	push   $0x23
  80033e:	68 47 0f 80 00       	push   $0x800f47
  800343:	e8 08 00 00 00       	call   800350 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800348:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80034b:	5b                   	pop    %ebx
  80034c:	5e                   	pop    %esi
  80034d:	5f                   	pop    %edi
  80034e:	5d                   	pop    %ebp
  80034f:	c3                   	ret    

00800350 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800350:	55                   	push   %ebp
  800351:	89 e5                	mov    %esp,%ebp
  800353:	56                   	push   %esi
  800354:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800355:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800358:	8b 35 00 20 80 00    	mov    0x802000,%esi
  80035e:	e8 e1 fd ff ff       	call   800144 <sys_getenvid>
  800363:	83 ec 0c             	sub    $0xc,%esp
  800366:	ff 75 0c             	pushl  0xc(%ebp)
  800369:	ff 75 08             	pushl  0x8(%ebp)
  80036c:	56                   	push   %esi
  80036d:	50                   	push   %eax
  80036e:	68 58 0f 80 00       	push   $0x800f58
  800373:	e8 b0 00 00 00       	call   800428 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800378:	83 c4 18             	add    $0x18,%esp
  80037b:	53                   	push   %ebx
  80037c:	ff 75 10             	pushl  0x10(%ebp)
  80037f:	e8 53 00 00 00       	call   8003d7 <vcprintf>
	cprintf("\n");
  800384:	c7 04 24 7c 0f 80 00 	movl   $0x800f7c,(%esp)
  80038b:	e8 98 00 00 00       	call   800428 <cprintf>
  800390:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800393:	cc                   	int3   
  800394:	eb fd                	jmp    800393 <_panic+0x43>

00800396 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800396:	55                   	push   %ebp
  800397:	89 e5                	mov    %esp,%ebp
  800399:	53                   	push   %ebx
  80039a:	83 ec 04             	sub    $0x4,%esp
  80039d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003a0:	8b 13                	mov    (%ebx),%edx
  8003a2:	8d 42 01             	lea    0x1(%edx),%eax
  8003a5:	89 03                	mov    %eax,(%ebx)
  8003a7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003aa:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003ae:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003b3:	75 1a                	jne    8003cf <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003b5:	83 ec 08             	sub    $0x8,%esp
  8003b8:	68 ff 00 00 00       	push   $0xff
  8003bd:	8d 43 08             	lea    0x8(%ebx),%eax
  8003c0:	50                   	push   %eax
  8003c1:	e8 00 fd ff ff       	call   8000c6 <sys_cputs>
		b->idx = 0;
  8003c6:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003cc:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003cf:	ff 43 04             	incl   0x4(%ebx)
}
  8003d2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003d5:	c9                   	leave  
  8003d6:	c3                   	ret    

008003d7 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003d7:	55                   	push   %ebp
  8003d8:	89 e5                	mov    %esp,%ebp
  8003da:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003e0:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003e7:	00 00 00 
	b.cnt = 0;
  8003ea:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003f1:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003f4:	ff 75 0c             	pushl  0xc(%ebp)
  8003f7:	ff 75 08             	pushl  0x8(%ebp)
  8003fa:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800400:	50                   	push   %eax
  800401:	68 96 03 80 00       	push   $0x800396
  800406:	e8 51 01 00 00       	call   80055c <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80040b:	83 c4 08             	add    $0x8,%esp
  80040e:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800414:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80041a:	50                   	push   %eax
  80041b:	e8 a6 fc ff ff       	call   8000c6 <sys_cputs>

	return b.cnt;
}
  800420:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800426:	c9                   	leave  
  800427:	c3                   	ret    

00800428 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800428:	55                   	push   %ebp
  800429:	89 e5                	mov    %esp,%ebp
  80042b:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80042e:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800431:	50                   	push   %eax
  800432:	ff 75 08             	pushl  0x8(%ebp)
  800435:	e8 9d ff ff ff       	call   8003d7 <vcprintf>
	va_end(ap);

	return cnt;
}
  80043a:	c9                   	leave  
  80043b:	c3                   	ret    

0080043c <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80043c:	55                   	push   %ebp
  80043d:	89 e5                	mov    %esp,%ebp
  80043f:	57                   	push   %edi
  800440:	56                   	push   %esi
  800441:	53                   	push   %ebx
  800442:	83 ec 1c             	sub    $0x1c,%esp
  800445:	89 c7                	mov    %eax,%edi
  800447:	89 d6                	mov    %edx,%esi
  800449:	8b 45 08             	mov    0x8(%ebp),%eax
  80044c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80044f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800452:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800455:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800458:	bb 00 00 00 00       	mov    $0x0,%ebx
  80045d:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800460:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800463:	39 d3                	cmp    %edx,%ebx
  800465:	72 05                	jb     80046c <printnum+0x30>
  800467:	39 45 10             	cmp    %eax,0x10(%ebp)
  80046a:	77 45                	ja     8004b1 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80046c:	83 ec 0c             	sub    $0xc,%esp
  80046f:	ff 75 18             	pushl  0x18(%ebp)
  800472:	8b 45 14             	mov    0x14(%ebp),%eax
  800475:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800478:	53                   	push   %ebx
  800479:	ff 75 10             	pushl  0x10(%ebp)
  80047c:	83 ec 08             	sub    $0x8,%esp
  80047f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800482:	ff 75 e0             	pushl  -0x20(%ebp)
  800485:	ff 75 dc             	pushl  -0x24(%ebp)
  800488:	ff 75 d8             	pushl  -0x28(%ebp)
  80048b:	e8 24 08 00 00       	call   800cb4 <__udivdi3>
  800490:	83 c4 18             	add    $0x18,%esp
  800493:	52                   	push   %edx
  800494:	50                   	push   %eax
  800495:	89 f2                	mov    %esi,%edx
  800497:	89 f8                	mov    %edi,%eax
  800499:	e8 9e ff ff ff       	call   80043c <printnum>
  80049e:	83 c4 20             	add    $0x20,%esp
  8004a1:	eb 16                	jmp    8004b9 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004a3:	83 ec 08             	sub    $0x8,%esp
  8004a6:	56                   	push   %esi
  8004a7:	ff 75 18             	pushl  0x18(%ebp)
  8004aa:	ff d7                	call   *%edi
  8004ac:	83 c4 10             	add    $0x10,%esp
  8004af:	eb 03                	jmp    8004b4 <printnum+0x78>
  8004b1:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004b4:	4b                   	dec    %ebx
  8004b5:	85 db                	test   %ebx,%ebx
  8004b7:	7f ea                	jg     8004a3 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004b9:	83 ec 08             	sub    $0x8,%esp
  8004bc:	56                   	push   %esi
  8004bd:	83 ec 04             	sub    $0x4,%esp
  8004c0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004c3:	ff 75 e0             	pushl  -0x20(%ebp)
  8004c6:	ff 75 dc             	pushl  -0x24(%ebp)
  8004c9:	ff 75 d8             	pushl  -0x28(%ebp)
  8004cc:	e8 f3 08 00 00       	call   800dc4 <__umoddi3>
  8004d1:	83 c4 14             	add    $0x14,%esp
  8004d4:	0f be 80 7e 0f 80 00 	movsbl 0x800f7e(%eax),%eax
  8004db:	50                   	push   %eax
  8004dc:	ff d7                	call   *%edi
}
  8004de:	83 c4 10             	add    $0x10,%esp
  8004e1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004e4:	5b                   	pop    %ebx
  8004e5:	5e                   	pop    %esi
  8004e6:	5f                   	pop    %edi
  8004e7:	5d                   	pop    %ebp
  8004e8:	c3                   	ret    

008004e9 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004e9:	55                   	push   %ebp
  8004ea:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004ec:	83 fa 01             	cmp    $0x1,%edx
  8004ef:	7e 0e                	jle    8004ff <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004f1:	8b 10                	mov    (%eax),%edx
  8004f3:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004f6:	89 08                	mov    %ecx,(%eax)
  8004f8:	8b 02                	mov    (%edx),%eax
  8004fa:	8b 52 04             	mov    0x4(%edx),%edx
  8004fd:	eb 22                	jmp    800521 <getuint+0x38>
	else if (lflag)
  8004ff:	85 d2                	test   %edx,%edx
  800501:	74 10                	je     800513 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800503:	8b 10                	mov    (%eax),%edx
  800505:	8d 4a 04             	lea    0x4(%edx),%ecx
  800508:	89 08                	mov    %ecx,(%eax)
  80050a:	8b 02                	mov    (%edx),%eax
  80050c:	ba 00 00 00 00       	mov    $0x0,%edx
  800511:	eb 0e                	jmp    800521 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800513:	8b 10                	mov    (%eax),%edx
  800515:	8d 4a 04             	lea    0x4(%edx),%ecx
  800518:	89 08                	mov    %ecx,(%eax)
  80051a:	8b 02                	mov    (%edx),%eax
  80051c:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800521:	5d                   	pop    %ebp
  800522:	c3                   	ret    

00800523 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800523:	55                   	push   %ebp
  800524:	89 e5                	mov    %esp,%ebp
  800526:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800529:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80052c:	8b 10                	mov    (%eax),%edx
  80052e:	3b 50 04             	cmp    0x4(%eax),%edx
  800531:	73 0a                	jae    80053d <sprintputch+0x1a>
		*b->buf++ = ch;
  800533:	8d 4a 01             	lea    0x1(%edx),%ecx
  800536:	89 08                	mov    %ecx,(%eax)
  800538:	8b 45 08             	mov    0x8(%ebp),%eax
  80053b:	88 02                	mov    %al,(%edx)
}
  80053d:	5d                   	pop    %ebp
  80053e:	c3                   	ret    

0080053f <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80053f:	55                   	push   %ebp
  800540:	89 e5                	mov    %esp,%ebp
  800542:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800545:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800548:	50                   	push   %eax
  800549:	ff 75 10             	pushl  0x10(%ebp)
  80054c:	ff 75 0c             	pushl  0xc(%ebp)
  80054f:	ff 75 08             	pushl  0x8(%ebp)
  800552:	e8 05 00 00 00       	call   80055c <vprintfmt>
	va_end(ap);
}
  800557:	83 c4 10             	add    $0x10,%esp
  80055a:	c9                   	leave  
  80055b:	c3                   	ret    

0080055c <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80055c:	55                   	push   %ebp
  80055d:	89 e5                	mov    %esp,%ebp
  80055f:	57                   	push   %edi
  800560:	56                   	push   %esi
  800561:	53                   	push   %ebx
  800562:	83 ec 2c             	sub    $0x2c,%esp
  800565:	8b 75 08             	mov    0x8(%ebp),%esi
  800568:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80056b:	8b 7d 10             	mov    0x10(%ebp),%edi
  80056e:	eb 12                	jmp    800582 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800570:	85 c0                	test   %eax,%eax
  800572:	0f 84 68 03 00 00    	je     8008e0 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800578:	83 ec 08             	sub    $0x8,%esp
  80057b:	53                   	push   %ebx
  80057c:	50                   	push   %eax
  80057d:	ff d6                	call   *%esi
  80057f:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800582:	47                   	inc    %edi
  800583:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800587:	83 f8 25             	cmp    $0x25,%eax
  80058a:	75 e4                	jne    800570 <vprintfmt+0x14>
  80058c:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800590:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800597:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80059e:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005a5:	ba 00 00 00 00       	mov    $0x0,%edx
  8005aa:	eb 07                	jmp    8005b3 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ac:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005af:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b3:	8d 47 01             	lea    0x1(%edi),%eax
  8005b6:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005b9:	0f b6 0f             	movzbl (%edi),%ecx
  8005bc:	8a 07                	mov    (%edi),%al
  8005be:	83 e8 23             	sub    $0x23,%eax
  8005c1:	3c 55                	cmp    $0x55,%al
  8005c3:	0f 87 fe 02 00 00    	ja     8008c7 <vprintfmt+0x36b>
  8005c9:	0f b6 c0             	movzbl %al,%eax
  8005cc:	ff 24 85 40 10 80 00 	jmp    *0x801040(,%eax,4)
  8005d3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005d6:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005da:	eb d7                	jmp    8005b3 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005dc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005df:	b8 00 00 00 00       	mov    $0x0,%eax
  8005e4:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005e7:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005ea:	01 c0                	add    %eax,%eax
  8005ec:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005f0:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005f3:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005f6:	83 fa 09             	cmp    $0x9,%edx
  8005f9:	77 34                	ja     80062f <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005fb:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005fc:	eb e9                	jmp    8005e7 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005fe:	8b 45 14             	mov    0x14(%ebp),%eax
  800601:	8d 48 04             	lea    0x4(%eax),%ecx
  800604:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800607:	8b 00                	mov    (%eax),%eax
  800609:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80060c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80060f:	eb 24                	jmp    800635 <vprintfmt+0xd9>
  800611:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800615:	79 07                	jns    80061e <vprintfmt+0xc2>
  800617:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80061e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800621:	eb 90                	jmp    8005b3 <vprintfmt+0x57>
  800623:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800626:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80062d:	eb 84                	jmp    8005b3 <vprintfmt+0x57>
  80062f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800632:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800635:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800639:	0f 89 74 ff ff ff    	jns    8005b3 <vprintfmt+0x57>
				width = precision, precision = -1;
  80063f:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800642:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800645:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80064c:	e9 62 ff ff ff       	jmp    8005b3 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800651:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800652:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800655:	e9 59 ff ff ff       	jmp    8005b3 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80065a:	8b 45 14             	mov    0x14(%ebp),%eax
  80065d:	8d 50 04             	lea    0x4(%eax),%edx
  800660:	89 55 14             	mov    %edx,0x14(%ebp)
  800663:	83 ec 08             	sub    $0x8,%esp
  800666:	53                   	push   %ebx
  800667:	ff 30                	pushl  (%eax)
  800669:	ff d6                	call   *%esi
			break;
  80066b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80066e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800671:	e9 0c ff ff ff       	jmp    800582 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800676:	8b 45 14             	mov    0x14(%ebp),%eax
  800679:	8d 50 04             	lea    0x4(%eax),%edx
  80067c:	89 55 14             	mov    %edx,0x14(%ebp)
  80067f:	8b 00                	mov    (%eax),%eax
  800681:	85 c0                	test   %eax,%eax
  800683:	79 02                	jns    800687 <vprintfmt+0x12b>
  800685:	f7 d8                	neg    %eax
  800687:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800689:	83 f8 08             	cmp    $0x8,%eax
  80068c:	7f 0b                	jg     800699 <vprintfmt+0x13d>
  80068e:	8b 04 85 a0 11 80 00 	mov    0x8011a0(,%eax,4),%eax
  800695:	85 c0                	test   %eax,%eax
  800697:	75 18                	jne    8006b1 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800699:	52                   	push   %edx
  80069a:	68 96 0f 80 00       	push   $0x800f96
  80069f:	53                   	push   %ebx
  8006a0:	56                   	push   %esi
  8006a1:	e8 99 fe ff ff       	call   80053f <printfmt>
  8006a6:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a9:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006ac:	e9 d1 fe ff ff       	jmp    800582 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006b1:	50                   	push   %eax
  8006b2:	68 9f 0f 80 00       	push   $0x800f9f
  8006b7:	53                   	push   %ebx
  8006b8:	56                   	push   %esi
  8006b9:	e8 81 fe ff ff       	call   80053f <printfmt>
  8006be:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006c4:	e9 b9 fe ff ff       	jmp    800582 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006c9:	8b 45 14             	mov    0x14(%ebp),%eax
  8006cc:	8d 50 04             	lea    0x4(%eax),%edx
  8006cf:	89 55 14             	mov    %edx,0x14(%ebp)
  8006d2:	8b 38                	mov    (%eax),%edi
  8006d4:	85 ff                	test   %edi,%edi
  8006d6:	75 05                	jne    8006dd <vprintfmt+0x181>
				p = "(null)";
  8006d8:	bf 8f 0f 80 00       	mov    $0x800f8f,%edi
			if (width > 0 && padc != '-')
  8006dd:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006e1:	0f 8e 90 00 00 00    	jle    800777 <vprintfmt+0x21b>
  8006e7:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006eb:	0f 84 8e 00 00 00    	je     80077f <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f1:	83 ec 08             	sub    $0x8,%esp
  8006f4:	ff 75 d0             	pushl  -0x30(%ebp)
  8006f7:	57                   	push   %edi
  8006f8:	e8 70 02 00 00       	call   80096d <strnlen>
  8006fd:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800700:	29 c1                	sub    %eax,%ecx
  800702:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800705:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800708:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80070c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80070f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800712:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800714:	eb 0d                	jmp    800723 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800716:	83 ec 08             	sub    $0x8,%esp
  800719:	53                   	push   %ebx
  80071a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80071d:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80071f:	4f                   	dec    %edi
  800720:	83 c4 10             	add    $0x10,%esp
  800723:	85 ff                	test   %edi,%edi
  800725:	7f ef                	jg     800716 <vprintfmt+0x1ba>
  800727:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80072a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80072d:	89 c8                	mov    %ecx,%eax
  80072f:	85 c9                	test   %ecx,%ecx
  800731:	79 05                	jns    800738 <vprintfmt+0x1dc>
  800733:	b8 00 00 00 00       	mov    $0x0,%eax
  800738:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80073b:	29 c1                	sub    %eax,%ecx
  80073d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800740:	89 75 08             	mov    %esi,0x8(%ebp)
  800743:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800746:	eb 3d                	jmp    800785 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800748:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80074c:	74 19                	je     800767 <vprintfmt+0x20b>
  80074e:	0f be c0             	movsbl %al,%eax
  800751:	83 e8 20             	sub    $0x20,%eax
  800754:	83 f8 5e             	cmp    $0x5e,%eax
  800757:	76 0e                	jbe    800767 <vprintfmt+0x20b>
					putch('?', putdat);
  800759:	83 ec 08             	sub    $0x8,%esp
  80075c:	53                   	push   %ebx
  80075d:	6a 3f                	push   $0x3f
  80075f:	ff 55 08             	call   *0x8(%ebp)
  800762:	83 c4 10             	add    $0x10,%esp
  800765:	eb 0b                	jmp    800772 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800767:	83 ec 08             	sub    $0x8,%esp
  80076a:	53                   	push   %ebx
  80076b:	52                   	push   %edx
  80076c:	ff 55 08             	call   *0x8(%ebp)
  80076f:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800772:	ff 4d e4             	decl   -0x1c(%ebp)
  800775:	eb 0e                	jmp    800785 <vprintfmt+0x229>
  800777:	89 75 08             	mov    %esi,0x8(%ebp)
  80077a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80077d:	eb 06                	jmp    800785 <vprintfmt+0x229>
  80077f:	89 75 08             	mov    %esi,0x8(%ebp)
  800782:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800785:	47                   	inc    %edi
  800786:	8a 47 ff             	mov    -0x1(%edi),%al
  800789:	0f be d0             	movsbl %al,%edx
  80078c:	85 d2                	test   %edx,%edx
  80078e:	74 1d                	je     8007ad <vprintfmt+0x251>
  800790:	85 f6                	test   %esi,%esi
  800792:	78 b4                	js     800748 <vprintfmt+0x1ec>
  800794:	4e                   	dec    %esi
  800795:	79 b1                	jns    800748 <vprintfmt+0x1ec>
  800797:	8b 75 08             	mov    0x8(%ebp),%esi
  80079a:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80079d:	eb 14                	jmp    8007b3 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80079f:	83 ec 08             	sub    $0x8,%esp
  8007a2:	53                   	push   %ebx
  8007a3:	6a 20                	push   $0x20
  8007a5:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007a7:	4f                   	dec    %edi
  8007a8:	83 c4 10             	add    $0x10,%esp
  8007ab:	eb 06                	jmp    8007b3 <vprintfmt+0x257>
  8007ad:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007b0:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b3:	85 ff                	test   %edi,%edi
  8007b5:	7f e8                	jg     80079f <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007b7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007ba:	e9 c3 fd ff ff       	jmp    800582 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007bf:	83 fa 01             	cmp    $0x1,%edx
  8007c2:	7e 16                	jle    8007da <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007c4:	8b 45 14             	mov    0x14(%ebp),%eax
  8007c7:	8d 50 08             	lea    0x8(%eax),%edx
  8007ca:	89 55 14             	mov    %edx,0x14(%ebp)
  8007cd:	8b 50 04             	mov    0x4(%eax),%edx
  8007d0:	8b 00                	mov    (%eax),%eax
  8007d2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007d5:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007d8:	eb 32                	jmp    80080c <vprintfmt+0x2b0>
	else if (lflag)
  8007da:	85 d2                	test   %edx,%edx
  8007dc:	74 18                	je     8007f6 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007de:	8b 45 14             	mov    0x14(%ebp),%eax
  8007e1:	8d 50 04             	lea    0x4(%eax),%edx
  8007e4:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e7:	8b 00                	mov    (%eax),%eax
  8007e9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007ec:	89 c1                	mov    %eax,%ecx
  8007ee:	c1 f9 1f             	sar    $0x1f,%ecx
  8007f1:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007f4:	eb 16                	jmp    80080c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007f6:	8b 45 14             	mov    0x14(%ebp),%eax
  8007f9:	8d 50 04             	lea    0x4(%eax),%edx
  8007fc:	89 55 14             	mov    %edx,0x14(%ebp)
  8007ff:	8b 00                	mov    (%eax),%eax
  800801:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800804:	89 c1                	mov    %eax,%ecx
  800806:	c1 f9 1f             	sar    $0x1f,%ecx
  800809:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80080c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80080f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800812:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800816:	79 76                	jns    80088e <vprintfmt+0x332>
				putch('-', putdat);
  800818:	83 ec 08             	sub    $0x8,%esp
  80081b:	53                   	push   %ebx
  80081c:	6a 2d                	push   $0x2d
  80081e:	ff d6                	call   *%esi
				num = -(long long) num;
  800820:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800823:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800826:	f7 d8                	neg    %eax
  800828:	83 d2 00             	adc    $0x0,%edx
  80082b:	f7 da                	neg    %edx
  80082d:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800830:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800835:	eb 5c                	jmp    800893 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800837:	8d 45 14             	lea    0x14(%ebp),%eax
  80083a:	e8 aa fc ff ff       	call   8004e9 <getuint>
			base = 10;
  80083f:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800844:	eb 4d                	jmp    800893 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800846:	8d 45 14             	lea    0x14(%ebp),%eax
  800849:	e8 9b fc ff ff       	call   8004e9 <getuint>
			base = 8;
  80084e:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800853:	eb 3e                	jmp    800893 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800855:	83 ec 08             	sub    $0x8,%esp
  800858:	53                   	push   %ebx
  800859:	6a 30                	push   $0x30
  80085b:	ff d6                	call   *%esi
			putch('x', putdat);
  80085d:	83 c4 08             	add    $0x8,%esp
  800860:	53                   	push   %ebx
  800861:	6a 78                	push   $0x78
  800863:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800865:	8b 45 14             	mov    0x14(%ebp),%eax
  800868:	8d 50 04             	lea    0x4(%eax),%edx
  80086b:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80086e:	8b 00                	mov    (%eax),%eax
  800870:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800875:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800878:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80087d:	eb 14                	jmp    800893 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80087f:	8d 45 14             	lea    0x14(%ebp),%eax
  800882:	e8 62 fc ff ff       	call   8004e9 <getuint>
			base = 16;
  800887:	b9 10 00 00 00       	mov    $0x10,%ecx
  80088c:	eb 05                	jmp    800893 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80088e:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800893:	83 ec 0c             	sub    $0xc,%esp
  800896:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80089a:	57                   	push   %edi
  80089b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80089e:	51                   	push   %ecx
  80089f:	52                   	push   %edx
  8008a0:	50                   	push   %eax
  8008a1:	89 da                	mov    %ebx,%edx
  8008a3:	89 f0                	mov    %esi,%eax
  8008a5:	e8 92 fb ff ff       	call   80043c <printnum>
			break;
  8008aa:	83 c4 20             	add    $0x20,%esp
  8008ad:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008b0:	e9 cd fc ff ff       	jmp    800582 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008b5:	83 ec 08             	sub    $0x8,%esp
  8008b8:	53                   	push   %ebx
  8008b9:	51                   	push   %ecx
  8008ba:	ff d6                	call   *%esi
			break;
  8008bc:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008bf:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008c2:	e9 bb fc ff ff       	jmp    800582 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008c7:	83 ec 08             	sub    $0x8,%esp
  8008ca:	53                   	push   %ebx
  8008cb:	6a 25                	push   $0x25
  8008cd:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008cf:	83 c4 10             	add    $0x10,%esp
  8008d2:	eb 01                	jmp    8008d5 <vprintfmt+0x379>
  8008d4:	4f                   	dec    %edi
  8008d5:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008d9:	75 f9                	jne    8008d4 <vprintfmt+0x378>
  8008db:	e9 a2 fc ff ff       	jmp    800582 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008e0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008e3:	5b                   	pop    %ebx
  8008e4:	5e                   	pop    %esi
  8008e5:	5f                   	pop    %edi
  8008e6:	5d                   	pop    %ebp
  8008e7:	c3                   	ret    

008008e8 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008e8:	55                   	push   %ebp
  8008e9:	89 e5                	mov    %esp,%ebp
  8008eb:	83 ec 18             	sub    $0x18,%esp
  8008ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f1:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008f4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008f7:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008fb:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008fe:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800905:	85 c0                	test   %eax,%eax
  800907:	74 26                	je     80092f <vsnprintf+0x47>
  800909:	85 d2                	test   %edx,%edx
  80090b:	7e 29                	jle    800936 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80090d:	ff 75 14             	pushl  0x14(%ebp)
  800910:	ff 75 10             	pushl  0x10(%ebp)
  800913:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800916:	50                   	push   %eax
  800917:	68 23 05 80 00       	push   $0x800523
  80091c:	e8 3b fc ff ff       	call   80055c <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800921:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800924:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800927:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80092a:	83 c4 10             	add    $0x10,%esp
  80092d:	eb 0c                	jmp    80093b <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80092f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800934:	eb 05                	jmp    80093b <vsnprintf+0x53>
  800936:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80093b:	c9                   	leave  
  80093c:	c3                   	ret    

0080093d <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80093d:	55                   	push   %ebp
  80093e:	89 e5                	mov    %esp,%ebp
  800940:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800943:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800946:	50                   	push   %eax
  800947:	ff 75 10             	pushl  0x10(%ebp)
  80094a:	ff 75 0c             	pushl  0xc(%ebp)
  80094d:	ff 75 08             	pushl  0x8(%ebp)
  800950:	e8 93 ff ff ff       	call   8008e8 <vsnprintf>
	va_end(ap);

	return rc;
}
  800955:	c9                   	leave  
  800956:	c3                   	ret    

00800957 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800957:	55                   	push   %ebp
  800958:	89 e5                	mov    %esp,%ebp
  80095a:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80095d:	b8 00 00 00 00       	mov    $0x0,%eax
  800962:	eb 01                	jmp    800965 <strlen+0xe>
		n++;
  800964:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800965:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800969:	75 f9                	jne    800964 <strlen+0xd>
		n++;
	return n;
}
  80096b:	5d                   	pop    %ebp
  80096c:	c3                   	ret    

0080096d <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80096d:	55                   	push   %ebp
  80096e:	89 e5                	mov    %esp,%ebp
  800970:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800973:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800976:	ba 00 00 00 00       	mov    $0x0,%edx
  80097b:	eb 01                	jmp    80097e <strnlen+0x11>
		n++;
  80097d:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80097e:	39 c2                	cmp    %eax,%edx
  800980:	74 08                	je     80098a <strnlen+0x1d>
  800982:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800986:	75 f5                	jne    80097d <strnlen+0x10>
  800988:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80098a:	5d                   	pop    %ebp
  80098b:	c3                   	ret    

0080098c <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80098c:	55                   	push   %ebp
  80098d:	89 e5                	mov    %esp,%ebp
  80098f:	53                   	push   %ebx
  800990:	8b 45 08             	mov    0x8(%ebp),%eax
  800993:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800996:	89 c2                	mov    %eax,%edx
  800998:	42                   	inc    %edx
  800999:	41                   	inc    %ecx
  80099a:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80099d:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009a0:	84 db                	test   %bl,%bl
  8009a2:	75 f4                	jne    800998 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009a4:	5b                   	pop    %ebx
  8009a5:	5d                   	pop    %ebp
  8009a6:	c3                   	ret    

008009a7 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009a7:	55                   	push   %ebp
  8009a8:	89 e5                	mov    %esp,%ebp
  8009aa:	53                   	push   %ebx
  8009ab:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009ae:	53                   	push   %ebx
  8009af:	e8 a3 ff ff ff       	call   800957 <strlen>
  8009b4:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009b7:	ff 75 0c             	pushl  0xc(%ebp)
  8009ba:	01 d8                	add    %ebx,%eax
  8009bc:	50                   	push   %eax
  8009bd:	e8 ca ff ff ff       	call   80098c <strcpy>
	return dst;
}
  8009c2:	89 d8                	mov    %ebx,%eax
  8009c4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009c7:	c9                   	leave  
  8009c8:	c3                   	ret    

008009c9 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009c9:	55                   	push   %ebp
  8009ca:	89 e5                	mov    %esp,%ebp
  8009cc:	56                   	push   %esi
  8009cd:	53                   	push   %ebx
  8009ce:	8b 75 08             	mov    0x8(%ebp),%esi
  8009d1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009d4:	89 f3                	mov    %esi,%ebx
  8009d6:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009d9:	89 f2                	mov    %esi,%edx
  8009db:	eb 0c                	jmp    8009e9 <strncpy+0x20>
		*dst++ = *src;
  8009dd:	42                   	inc    %edx
  8009de:	8a 01                	mov    (%ecx),%al
  8009e0:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009e3:	80 39 01             	cmpb   $0x1,(%ecx)
  8009e6:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009e9:	39 da                	cmp    %ebx,%edx
  8009eb:	75 f0                	jne    8009dd <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009ed:	89 f0                	mov    %esi,%eax
  8009ef:	5b                   	pop    %ebx
  8009f0:	5e                   	pop    %esi
  8009f1:	5d                   	pop    %ebp
  8009f2:	c3                   	ret    

008009f3 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009f3:	55                   	push   %ebp
  8009f4:	89 e5                	mov    %esp,%ebp
  8009f6:	56                   	push   %esi
  8009f7:	53                   	push   %ebx
  8009f8:	8b 75 08             	mov    0x8(%ebp),%esi
  8009fb:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009fe:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a01:	85 c0                	test   %eax,%eax
  800a03:	74 1e                	je     800a23 <strlcpy+0x30>
  800a05:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a09:	89 f2                	mov    %esi,%edx
  800a0b:	eb 05                	jmp    800a12 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a0d:	42                   	inc    %edx
  800a0e:	41                   	inc    %ecx
  800a0f:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a12:	39 c2                	cmp    %eax,%edx
  800a14:	74 08                	je     800a1e <strlcpy+0x2b>
  800a16:	8a 19                	mov    (%ecx),%bl
  800a18:	84 db                	test   %bl,%bl
  800a1a:	75 f1                	jne    800a0d <strlcpy+0x1a>
  800a1c:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a1e:	c6 00 00             	movb   $0x0,(%eax)
  800a21:	eb 02                	jmp    800a25 <strlcpy+0x32>
  800a23:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a25:	29 f0                	sub    %esi,%eax
}
  800a27:	5b                   	pop    %ebx
  800a28:	5e                   	pop    %esi
  800a29:	5d                   	pop    %ebp
  800a2a:	c3                   	ret    

00800a2b <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a2b:	55                   	push   %ebp
  800a2c:	89 e5                	mov    %esp,%ebp
  800a2e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a31:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a34:	eb 02                	jmp    800a38 <strcmp+0xd>
		p++, q++;
  800a36:	41                   	inc    %ecx
  800a37:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a38:	8a 01                	mov    (%ecx),%al
  800a3a:	84 c0                	test   %al,%al
  800a3c:	74 04                	je     800a42 <strcmp+0x17>
  800a3e:	3a 02                	cmp    (%edx),%al
  800a40:	74 f4                	je     800a36 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a42:	0f b6 c0             	movzbl %al,%eax
  800a45:	0f b6 12             	movzbl (%edx),%edx
  800a48:	29 d0                	sub    %edx,%eax
}
  800a4a:	5d                   	pop    %ebp
  800a4b:	c3                   	ret    

00800a4c <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a4c:	55                   	push   %ebp
  800a4d:	89 e5                	mov    %esp,%ebp
  800a4f:	53                   	push   %ebx
  800a50:	8b 45 08             	mov    0x8(%ebp),%eax
  800a53:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a56:	89 c3                	mov    %eax,%ebx
  800a58:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a5b:	eb 02                	jmp    800a5f <strncmp+0x13>
		n--, p++, q++;
  800a5d:	40                   	inc    %eax
  800a5e:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a5f:	39 d8                	cmp    %ebx,%eax
  800a61:	74 14                	je     800a77 <strncmp+0x2b>
  800a63:	8a 08                	mov    (%eax),%cl
  800a65:	84 c9                	test   %cl,%cl
  800a67:	74 04                	je     800a6d <strncmp+0x21>
  800a69:	3a 0a                	cmp    (%edx),%cl
  800a6b:	74 f0                	je     800a5d <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a6d:	0f b6 00             	movzbl (%eax),%eax
  800a70:	0f b6 12             	movzbl (%edx),%edx
  800a73:	29 d0                	sub    %edx,%eax
  800a75:	eb 05                	jmp    800a7c <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a77:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a7c:	5b                   	pop    %ebx
  800a7d:	5d                   	pop    %ebp
  800a7e:	c3                   	ret    

00800a7f <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a7f:	55                   	push   %ebp
  800a80:	89 e5                	mov    %esp,%ebp
  800a82:	8b 45 08             	mov    0x8(%ebp),%eax
  800a85:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a88:	eb 05                	jmp    800a8f <strchr+0x10>
		if (*s == c)
  800a8a:	38 ca                	cmp    %cl,%dl
  800a8c:	74 0c                	je     800a9a <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a8e:	40                   	inc    %eax
  800a8f:	8a 10                	mov    (%eax),%dl
  800a91:	84 d2                	test   %dl,%dl
  800a93:	75 f5                	jne    800a8a <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a95:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a9a:	5d                   	pop    %ebp
  800a9b:	c3                   	ret    

00800a9c <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a9c:	55                   	push   %ebp
  800a9d:	89 e5                	mov    %esp,%ebp
  800a9f:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa2:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800aa5:	eb 05                	jmp    800aac <strfind+0x10>
		if (*s == c)
  800aa7:	38 ca                	cmp    %cl,%dl
  800aa9:	74 07                	je     800ab2 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800aab:	40                   	inc    %eax
  800aac:	8a 10                	mov    (%eax),%dl
  800aae:	84 d2                	test   %dl,%dl
  800ab0:	75 f5                	jne    800aa7 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800ab2:	5d                   	pop    %ebp
  800ab3:	c3                   	ret    

00800ab4 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800ab4:	55                   	push   %ebp
  800ab5:	89 e5                	mov    %esp,%ebp
  800ab7:	57                   	push   %edi
  800ab8:	56                   	push   %esi
  800ab9:	53                   	push   %ebx
  800aba:	8b 7d 08             	mov    0x8(%ebp),%edi
  800abd:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800ac0:	85 c9                	test   %ecx,%ecx
  800ac2:	74 36                	je     800afa <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800ac4:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800aca:	75 28                	jne    800af4 <memset+0x40>
  800acc:	f6 c1 03             	test   $0x3,%cl
  800acf:	75 23                	jne    800af4 <memset+0x40>
		c &= 0xFF;
  800ad1:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800ad5:	89 d3                	mov    %edx,%ebx
  800ad7:	c1 e3 08             	shl    $0x8,%ebx
  800ada:	89 d6                	mov    %edx,%esi
  800adc:	c1 e6 18             	shl    $0x18,%esi
  800adf:	89 d0                	mov    %edx,%eax
  800ae1:	c1 e0 10             	shl    $0x10,%eax
  800ae4:	09 f0                	or     %esi,%eax
  800ae6:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800ae8:	89 d8                	mov    %ebx,%eax
  800aea:	09 d0                	or     %edx,%eax
  800aec:	c1 e9 02             	shr    $0x2,%ecx
  800aef:	fc                   	cld    
  800af0:	f3 ab                	rep stos %eax,%es:(%edi)
  800af2:	eb 06                	jmp    800afa <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800af4:	8b 45 0c             	mov    0xc(%ebp),%eax
  800af7:	fc                   	cld    
  800af8:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800afa:	89 f8                	mov    %edi,%eax
  800afc:	5b                   	pop    %ebx
  800afd:	5e                   	pop    %esi
  800afe:	5f                   	pop    %edi
  800aff:	5d                   	pop    %ebp
  800b00:	c3                   	ret    

00800b01 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b01:	55                   	push   %ebp
  800b02:	89 e5                	mov    %esp,%ebp
  800b04:	57                   	push   %edi
  800b05:	56                   	push   %esi
  800b06:	8b 45 08             	mov    0x8(%ebp),%eax
  800b09:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b0c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b0f:	39 c6                	cmp    %eax,%esi
  800b11:	73 33                	jae    800b46 <memmove+0x45>
  800b13:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b16:	39 d0                	cmp    %edx,%eax
  800b18:	73 2c                	jae    800b46 <memmove+0x45>
		s += n;
		d += n;
  800b1a:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b1d:	89 d6                	mov    %edx,%esi
  800b1f:	09 fe                	or     %edi,%esi
  800b21:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b27:	75 13                	jne    800b3c <memmove+0x3b>
  800b29:	f6 c1 03             	test   $0x3,%cl
  800b2c:	75 0e                	jne    800b3c <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b2e:	83 ef 04             	sub    $0x4,%edi
  800b31:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b34:	c1 e9 02             	shr    $0x2,%ecx
  800b37:	fd                   	std    
  800b38:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b3a:	eb 07                	jmp    800b43 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b3c:	4f                   	dec    %edi
  800b3d:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b40:	fd                   	std    
  800b41:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b43:	fc                   	cld    
  800b44:	eb 1d                	jmp    800b63 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b46:	89 f2                	mov    %esi,%edx
  800b48:	09 c2                	or     %eax,%edx
  800b4a:	f6 c2 03             	test   $0x3,%dl
  800b4d:	75 0f                	jne    800b5e <memmove+0x5d>
  800b4f:	f6 c1 03             	test   $0x3,%cl
  800b52:	75 0a                	jne    800b5e <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b54:	c1 e9 02             	shr    $0x2,%ecx
  800b57:	89 c7                	mov    %eax,%edi
  800b59:	fc                   	cld    
  800b5a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b5c:	eb 05                	jmp    800b63 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b5e:	89 c7                	mov    %eax,%edi
  800b60:	fc                   	cld    
  800b61:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b63:	5e                   	pop    %esi
  800b64:	5f                   	pop    %edi
  800b65:	5d                   	pop    %ebp
  800b66:	c3                   	ret    

00800b67 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b67:	55                   	push   %ebp
  800b68:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b6a:	ff 75 10             	pushl  0x10(%ebp)
  800b6d:	ff 75 0c             	pushl  0xc(%ebp)
  800b70:	ff 75 08             	pushl  0x8(%ebp)
  800b73:	e8 89 ff ff ff       	call   800b01 <memmove>
}
  800b78:	c9                   	leave  
  800b79:	c3                   	ret    

00800b7a <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b7a:	55                   	push   %ebp
  800b7b:	89 e5                	mov    %esp,%ebp
  800b7d:	56                   	push   %esi
  800b7e:	53                   	push   %ebx
  800b7f:	8b 45 08             	mov    0x8(%ebp),%eax
  800b82:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b85:	89 c6                	mov    %eax,%esi
  800b87:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b8a:	eb 14                	jmp    800ba0 <memcmp+0x26>
		if (*s1 != *s2)
  800b8c:	8a 08                	mov    (%eax),%cl
  800b8e:	8a 1a                	mov    (%edx),%bl
  800b90:	38 d9                	cmp    %bl,%cl
  800b92:	74 0a                	je     800b9e <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b94:	0f b6 c1             	movzbl %cl,%eax
  800b97:	0f b6 db             	movzbl %bl,%ebx
  800b9a:	29 d8                	sub    %ebx,%eax
  800b9c:	eb 0b                	jmp    800ba9 <memcmp+0x2f>
		s1++, s2++;
  800b9e:	40                   	inc    %eax
  800b9f:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ba0:	39 f0                	cmp    %esi,%eax
  800ba2:	75 e8                	jne    800b8c <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800ba4:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ba9:	5b                   	pop    %ebx
  800baa:	5e                   	pop    %esi
  800bab:	5d                   	pop    %ebp
  800bac:	c3                   	ret    

00800bad <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bad:	55                   	push   %ebp
  800bae:	89 e5                	mov    %esp,%ebp
  800bb0:	53                   	push   %ebx
  800bb1:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bb4:	89 c1                	mov    %eax,%ecx
  800bb6:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bb9:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bbd:	eb 08                	jmp    800bc7 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bbf:	0f b6 10             	movzbl (%eax),%edx
  800bc2:	39 da                	cmp    %ebx,%edx
  800bc4:	74 05                	je     800bcb <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bc6:	40                   	inc    %eax
  800bc7:	39 c8                	cmp    %ecx,%eax
  800bc9:	72 f4                	jb     800bbf <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bcb:	5b                   	pop    %ebx
  800bcc:	5d                   	pop    %ebp
  800bcd:	c3                   	ret    

00800bce <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bce:	55                   	push   %ebp
  800bcf:	89 e5                	mov    %esp,%ebp
  800bd1:	57                   	push   %edi
  800bd2:	56                   	push   %esi
  800bd3:	53                   	push   %ebx
  800bd4:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bd7:	eb 01                	jmp    800bda <strtol+0xc>
		s++;
  800bd9:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bda:	8a 01                	mov    (%ecx),%al
  800bdc:	3c 20                	cmp    $0x20,%al
  800bde:	74 f9                	je     800bd9 <strtol+0xb>
  800be0:	3c 09                	cmp    $0x9,%al
  800be2:	74 f5                	je     800bd9 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800be4:	3c 2b                	cmp    $0x2b,%al
  800be6:	75 08                	jne    800bf0 <strtol+0x22>
		s++;
  800be8:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800be9:	bf 00 00 00 00       	mov    $0x0,%edi
  800bee:	eb 11                	jmp    800c01 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bf0:	3c 2d                	cmp    $0x2d,%al
  800bf2:	75 08                	jne    800bfc <strtol+0x2e>
		s++, neg = 1;
  800bf4:	41                   	inc    %ecx
  800bf5:	bf 01 00 00 00       	mov    $0x1,%edi
  800bfa:	eb 05                	jmp    800c01 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bfc:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c01:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c05:	0f 84 87 00 00 00    	je     800c92 <strtol+0xc4>
  800c0b:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c0f:	75 27                	jne    800c38 <strtol+0x6a>
  800c11:	80 39 30             	cmpb   $0x30,(%ecx)
  800c14:	75 22                	jne    800c38 <strtol+0x6a>
  800c16:	e9 88 00 00 00       	jmp    800ca3 <strtol+0xd5>
		s += 2, base = 16;
  800c1b:	83 c1 02             	add    $0x2,%ecx
  800c1e:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c25:	eb 11                	jmp    800c38 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c27:	41                   	inc    %ecx
  800c28:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c2f:	eb 07                	jmp    800c38 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c31:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c38:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c3d:	8a 11                	mov    (%ecx),%dl
  800c3f:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c42:	80 fb 09             	cmp    $0x9,%bl
  800c45:	77 08                	ja     800c4f <strtol+0x81>
			dig = *s - '0';
  800c47:	0f be d2             	movsbl %dl,%edx
  800c4a:	83 ea 30             	sub    $0x30,%edx
  800c4d:	eb 22                	jmp    800c71 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c4f:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c52:	89 f3                	mov    %esi,%ebx
  800c54:	80 fb 19             	cmp    $0x19,%bl
  800c57:	77 08                	ja     800c61 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c59:	0f be d2             	movsbl %dl,%edx
  800c5c:	83 ea 57             	sub    $0x57,%edx
  800c5f:	eb 10                	jmp    800c71 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c61:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c64:	89 f3                	mov    %esi,%ebx
  800c66:	80 fb 19             	cmp    $0x19,%bl
  800c69:	77 14                	ja     800c7f <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c6b:	0f be d2             	movsbl %dl,%edx
  800c6e:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c71:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c74:	7d 09                	jge    800c7f <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c76:	41                   	inc    %ecx
  800c77:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c7b:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c7d:	eb be                	jmp    800c3d <strtol+0x6f>

	if (endptr)
  800c7f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c83:	74 05                	je     800c8a <strtol+0xbc>
		*endptr = (char *) s;
  800c85:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c88:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c8a:	85 ff                	test   %edi,%edi
  800c8c:	74 21                	je     800caf <strtol+0xe1>
  800c8e:	f7 d8                	neg    %eax
  800c90:	eb 1d                	jmp    800caf <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c92:	80 39 30             	cmpb   $0x30,(%ecx)
  800c95:	75 9a                	jne    800c31 <strtol+0x63>
  800c97:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c9b:	0f 84 7a ff ff ff    	je     800c1b <strtol+0x4d>
  800ca1:	eb 84                	jmp    800c27 <strtol+0x59>
  800ca3:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800ca7:	0f 84 6e ff ff ff    	je     800c1b <strtol+0x4d>
  800cad:	eb 89                	jmp    800c38 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800caf:	5b                   	pop    %ebx
  800cb0:	5e                   	pop    %esi
  800cb1:	5f                   	pop    %edi
  800cb2:	5d                   	pop    %ebp
  800cb3:	c3                   	ret    

00800cb4 <__udivdi3>:
  800cb4:	55                   	push   %ebp
  800cb5:	57                   	push   %edi
  800cb6:	56                   	push   %esi
  800cb7:	53                   	push   %ebx
  800cb8:	83 ec 1c             	sub    $0x1c,%esp
  800cbb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cbf:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cc3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cc7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ccb:	89 ca                	mov    %ecx,%edx
  800ccd:	89 f8                	mov    %edi,%eax
  800ccf:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cd3:	85 f6                	test   %esi,%esi
  800cd5:	75 2d                	jne    800d04 <__udivdi3+0x50>
  800cd7:	39 cf                	cmp    %ecx,%edi
  800cd9:	77 65                	ja     800d40 <__udivdi3+0x8c>
  800cdb:	89 fd                	mov    %edi,%ebp
  800cdd:	85 ff                	test   %edi,%edi
  800cdf:	75 0b                	jne    800cec <__udivdi3+0x38>
  800ce1:	b8 01 00 00 00       	mov    $0x1,%eax
  800ce6:	31 d2                	xor    %edx,%edx
  800ce8:	f7 f7                	div    %edi
  800cea:	89 c5                	mov    %eax,%ebp
  800cec:	31 d2                	xor    %edx,%edx
  800cee:	89 c8                	mov    %ecx,%eax
  800cf0:	f7 f5                	div    %ebp
  800cf2:	89 c1                	mov    %eax,%ecx
  800cf4:	89 d8                	mov    %ebx,%eax
  800cf6:	f7 f5                	div    %ebp
  800cf8:	89 cf                	mov    %ecx,%edi
  800cfa:	89 fa                	mov    %edi,%edx
  800cfc:	83 c4 1c             	add    $0x1c,%esp
  800cff:	5b                   	pop    %ebx
  800d00:	5e                   	pop    %esi
  800d01:	5f                   	pop    %edi
  800d02:	5d                   	pop    %ebp
  800d03:	c3                   	ret    
  800d04:	39 ce                	cmp    %ecx,%esi
  800d06:	77 28                	ja     800d30 <__udivdi3+0x7c>
  800d08:	0f bd fe             	bsr    %esi,%edi
  800d0b:	83 f7 1f             	xor    $0x1f,%edi
  800d0e:	75 40                	jne    800d50 <__udivdi3+0x9c>
  800d10:	39 ce                	cmp    %ecx,%esi
  800d12:	72 0a                	jb     800d1e <__udivdi3+0x6a>
  800d14:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d18:	0f 87 9e 00 00 00    	ja     800dbc <__udivdi3+0x108>
  800d1e:	b8 01 00 00 00       	mov    $0x1,%eax
  800d23:	89 fa                	mov    %edi,%edx
  800d25:	83 c4 1c             	add    $0x1c,%esp
  800d28:	5b                   	pop    %ebx
  800d29:	5e                   	pop    %esi
  800d2a:	5f                   	pop    %edi
  800d2b:	5d                   	pop    %ebp
  800d2c:	c3                   	ret    
  800d2d:	8d 76 00             	lea    0x0(%esi),%esi
  800d30:	31 ff                	xor    %edi,%edi
  800d32:	31 c0                	xor    %eax,%eax
  800d34:	89 fa                	mov    %edi,%edx
  800d36:	83 c4 1c             	add    $0x1c,%esp
  800d39:	5b                   	pop    %ebx
  800d3a:	5e                   	pop    %esi
  800d3b:	5f                   	pop    %edi
  800d3c:	5d                   	pop    %ebp
  800d3d:	c3                   	ret    
  800d3e:	66 90                	xchg   %ax,%ax
  800d40:	89 d8                	mov    %ebx,%eax
  800d42:	f7 f7                	div    %edi
  800d44:	31 ff                	xor    %edi,%edi
  800d46:	89 fa                	mov    %edi,%edx
  800d48:	83 c4 1c             	add    $0x1c,%esp
  800d4b:	5b                   	pop    %ebx
  800d4c:	5e                   	pop    %esi
  800d4d:	5f                   	pop    %edi
  800d4e:	5d                   	pop    %ebp
  800d4f:	c3                   	ret    
  800d50:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d55:	89 eb                	mov    %ebp,%ebx
  800d57:	29 fb                	sub    %edi,%ebx
  800d59:	89 f9                	mov    %edi,%ecx
  800d5b:	d3 e6                	shl    %cl,%esi
  800d5d:	89 c5                	mov    %eax,%ebp
  800d5f:	88 d9                	mov    %bl,%cl
  800d61:	d3 ed                	shr    %cl,%ebp
  800d63:	89 e9                	mov    %ebp,%ecx
  800d65:	09 f1                	or     %esi,%ecx
  800d67:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d6b:	89 f9                	mov    %edi,%ecx
  800d6d:	d3 e0                	shl    %cl,%eax
  800d6f:	89 c5                	mov    %eax,%ebp
  800d71:	89 d6                	mov    %edx,%esi
  800d73:	88 d9                	mov    %bl,%cl
  800d75:	d3 ee                	shr    %cl,%esi
  800d77:	89 f9                	mov    %edi,%ecx
  800d79:	d3 e2                	shl    %cl,%edx
  800d7b:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d7f:	88 d9                	mov    %bl,%cl
  800d81:	d3 e8                	shr    %cl,%eax
  800d83:	09 c2                	or     %eax,%edx
  800d85:	89 d0                	mov    %edx,%eax
  800d87:	89 f2                	mov    %esi,%edx
  800d89:	f7 74 24 0c          	divl   0xc(%esp)
  800d8d:	89 d6                	mov    %edx,%esi
  800d8f:	89 c3                	mov    %eax,%ebx
  800d91:	f7 e5                	mul    %ebp
  800d93:	39 d6                	cmp    %edx,%esi
  800d95:	72 19                	jb     800db0 <__udivdi3+0xfc>
  800d97:	74 0b                	je     800da4 <__udivdi3+0xf0>
  800d99:	89 d8                	mov    %ebx,%eax
  800d9b:	31 ff                	xor    %edi,%edi
  800d9d:	e9 58 ff ff ff       	jmp    800cfa <__udivdi3+0x46>
  800da2:	66 90                	xchg   %ax,%ax
  800da4:	8b 54 24 08          	mov    0x8(%esp),%edx
  800da8:	89 f9                	mov    %edi,%ecx
  800daa:	d3 e2                	shl    %cl,%edx
  800dac:	39 c2                	cmp    %eax,%edx
  800dae:	73 e9                	jae    800d99 <__udivdi3+0xe5>
  800db0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800db3:	31 ff                	xor    %edi,%edi
  800db5:	e9 40 ff ff ff       	jmp    800cfa <__udivdi3+0x46>
  800dba:	66 90                	xchg   %ax,%ax
  800dbc:	31 c0                	xor    %eax,%eax
  800dbe:	e9 37 ff ff ff       	jmp    800cfa <__udivdi3+0x46>
  800dc3:	90                   	nop

00800dc4 <__umoddi3>:
  800dc4:	55                   	push   %ebp
  800dc5:	57                   	push   %edi
  800dc6:	56                   	push   %esi
  800dc7:	53                   	push   %ebx
  800dc8:	83 ec 1c             	sub    $0x1c,%esp
  800dcb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800dcf:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dd3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dd7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800ddb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800ddf:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800de3:	89 f3                	mov    %esi,%ebx
  800de5:	89 fa                	mov    %edi,%edx
  800de7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800deb:	89 34 24             	mov    %esi,(%esp)
  800dee:	85 c0                	test   %eax,%eax
  800df0:	75 1a                	jne    800e0c <__umoddi3+0x48>
  800df2:	39 f7                	cmp    %esi,%edi
  800df4:	0f 86 a2 00 00 00    	jbe    800e9c <__umoddi3+0xd8>
  800dfa:	89 c8                	mov    %ecx,%eax
  800dfc:	89 f2                	mov    %esi,%edx
  800dfe:	f7 f7                	div    %edi
  800e00:	89 d0                	mov    %edx,%eax
  800e02:	31 d2                	xor    %edx,%edx
  800e04:	83 c4 1c             	add    $0x1c,%esp
  800e07:	5b                   	pop    %ebx
  800e08:	5e                   	pop    %esi
  800e09:	5f                   	pop    %edi
  800e0a:	5d                   	pop    %ebp
  800e0b:	c3                   	ret    
  800e0c:	39 f0                	cmp    %esi,%eax
  800e0e:	0f 87 ac 00 00 00    	ja     800ec0 <__umoddi3+0xfc>
  800e14:	0f bd e8             	bsr    %eax,%ebp
  800e17:	83 f5 1f             	xor    $0x1f,%ebp
  800e1a:	0f 84 ac 00 00 00    	je     800ecc <__umoddi3+0x108>
  800e20:	bf 20 00 00 00       	mov    $0x20,%edi
  800e25:	29 ef                	sub    %ebp,%edi
  800e27:	89 fe                	mov    %edi,%esi
  800e29:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e2d:	89 e9                	mov    %ebp,%ecx
  800e2f:	d3 e0                	shl    %cl,%eax
  800e31:	89 d7                	mov    %edx,%edi
  800e33:	89 f1                	mov    %esi,%ecx
  800e35:	d3 ef                	shr    %cl,%edi
  800e37:	09 c7                	or     %eax,%edi
  800e39:	89 e9                	mov    %ebp,%ecx
  800e3b:	d3 e2                	shl    %cl,%edx
  800e3d:	89 14 24             	mov    %edx,(%esp)
  800e40:	89 d8                	mov    %ebx,%eax
  800e42:	d3 e0                	shl    %cl,%eax
  800e44:	89 c2                	mov    %eax,%edx
  800e46:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e4a:	d3 e0                	shl    %cl,%eax
  800e4c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e50:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e54:	89 f1                	mov    %esi,%ecx
  800e56:	d3 e8                	shr    %cl,%eax
  800e58:	09 d0                	or     %edx,%eax
  800e5a:	d3 eb                	shr    %cl,%ebx
  800e5c:	89 da                	mov    %ebx,%edx
  800e5e:	f7 f7                	div    %edi
  800e60:	89 d3                	mov    %edx,%ebx
  800e62:	f7 24 24             	mull   (%esp)
  800e65:	89 c6                	mov    %eax,%esi
  800e67:	89 d1                	mov    %edx,%ecx
  800e69:	39 d3                	cmp    %edx,%ebx
  800e6b:	0f 82 87 00 00 00    	jb     800ef8 <__umoddi3+0x134>
  800e71:	0f 84 91 00 00 00    	je     800f08 <__umoddi3+0x144>
  800e77:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e7b:	29 f2                	sub    %esi,%edx
  800e7d:	19 cb                	sbb    %ecx,%ebx
  800e7f:	89 d8                	mov    %ebx,%eax
  800e81:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e85:	d3 e0                	shl    %cl,%eax
  800e87:	89 e9                	mov    %ebp,%ecx
  800e89:	d3 ea                	shr    %cl,%edx
  800e8b:	09 d0                	or     %edx,%eax
  800e8d:	89 e9                	mov    %ebp,%ecx
  800e8f:	d3 eb                	shr    %cl,%ebx
  800e91:	89 da                	mov    %ebx,%edx
  800e93:	83 c4 1c             	add    $0x1c,%esp
  800e96:	5b                   	pop    %ebx
  800e97:	5e                   	pop    %esi
  800e98:	5f                   	pop    %edi
  800e99:	5d                   	pop    %ebp
  800e9a:	c3                   	ret    
  800e9b:	90                   	nop
  800e9c:	89 fd                	mov    %edi,%ebp
  800e9e:	85 ff                	test   %edi,%edi
  800ea0:	75 0b                	jne    800ead <__umoddi3+0xe9>
  800ea2:	b8 01 00 00 00       	mov    $0x1,%eax
  800ea7:	31 d2                	xor    %edx,%edx
  800ea9:	f7 f7                	div    %edi
  800eab:	89 c5                	mov    %eax,%ebp
  800ead:	89 f0                	mov    %esi,%eax
  800eaf:	31 d2                	xor    %edx,%edx
  800eb1:	f7 f5                	div    %ebp
  800eb3:	89 c8                	mov    %ecx,%eax
  800eb5:	f7 f5                	div    %ebp
  800eb7:	89 d0                	mov    %edx,%eax
  800eb9:	e9 44 ff ff ff       	jmp    800e02 <__umoddi3+0x3e>
  800ebe:	66 90                	xchg   %ax,%ax
  800ec0:	89 c8                	mov    %ecx,%eax
  800ec2:	89 f2                	mov    %esi,%edx
  800ec4:	83 c4 1c             	add    $0x1c,%esp
  800ec7:	5b                   	pop    %ebx
  800ec8:	5e                   	pop    %esi
  800ec9:	5f                   	pop    %edi
  800eca:	5d                   	pop    %ebp
  800ecb:	c3                   	ret    
  800ecc:	3b 04 24             	cmp    (%esp),%eax
  800ecf:	72 06                	jb     800ed7 <__umoddi3+0x113>
  800ed1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ed5:	77 0f                	ja     800ee6 <__umoddi3+0x122>
  800ed7:	89 f2                	mov    %esi,%edx
  800ed9:	29 f9                	sub    %edi,%ecx
  800edb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800edf:	89 14 24             	mov    %edx,(%esp)
  800ee2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ee6:	8b 44 24 04          	mov    0x4(%esp),%eax
  800eea:	8b 14 24             	mov    (%esp),%edx
  800eed:	83 c4 1c             	add    $0x1c,%esp
  800ef0:	5b                   	pop    %ebx
  800ef1:	5e                   	pop    %esi
  800ef2:	5f                   	pop    %edi
  800ef3:	5d                   	pop    %ebp
  800ef4:	c3                   	ret    
  800ef5:	8d 76 00             	lea    0x0(%esi),%esi
  800ef8:	2b 04 24             	sub    (%esp),%eax
  800efb:	19 fa                	sbb    %edi,%edx
  800efd:	89 d1                	mov    %edx,%ecx
  800eff:	89 c6                	mov    %eax,%esi
  800f01:	e9 71 ff ff ff       	jmp    800e77 <__umoddi3+0xb3>
  800f06:	66 90                	xchg   %ax,%ax
  800f08:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f0c:	72 ea                	jb     800ef8 <__umoddi3+0x134>
  800f0e:	89 d9                	mov    %ebx,%ecx
  800f10:	e9 62 ff ff ff       	jmp    800e77 <__umoddi3+0xb3>
