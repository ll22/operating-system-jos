
obj/user/dumbfork:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 b2 01 00 00       	call   8001e3 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <duppage>:
	}
}

void
duppage(envid_t dstenv, void *addr)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	8b 75 08             	mov    0x8(%ebp),%esi
  80003b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	// This is NOT what you should do in your fork.
	if ((r = sys_page_alloc(dstenv, addr, PTE_P|PTE_U|PTE_W)) < 0)
  80003e:	83 ec 04             	sub    $0x4,%esp
  800041:	6a 07                	push   $0x7
  800043:	53                   	push   %ebx
  800044:	56                   	push   %esi
  800045:	e8 1a 0c 00 00       	call   800c64 <sys_page_alloc>
  80004a:	83 c4 10             	add    $0x10,%esp
  80004d:	85 c0                	test   %eax,%eax
  80004f:	79 12                	jns    800063 <duppage+0x30>
		panic("sys_page_alloc: %e", r);
  800051:	50                   	push   %eax
  800052:	68 a0 10 80 00       	push   $0x8010a0
  800057:	6a 20                	push   $0x20
  800059:	68 b3 10 80 00       	push   $0x8010b3
  80005e:	e8 e1 01 00 00       	call   800244 <_panic>
	if ((r = sys_page_map(dstenv, addr, 0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  800063:	83 ec 0c             	sub    $0xc,%esp
  800066:	6a 07                	push   $0x7
  800068:	68 00 00 40 00       	push   $0x400000
  80006d:	6a 00                	push   $0x0
  80006f:	53                   	push   %ebx
  800070:	56                   	push   %esi
  800071:	e8 31 0c 00 00       	call   800ca7 <sys_page_map>
  800076:	83 c4 20             	add    $0x20,%esp
  800079:	85 c0                	test   %eax,%eax
  80007b:	79 12                	jns    80008f <duppage+0x5c>
		panic("sys_page_map: %e", r);
  80007d:	50                   	push   %eax
  80007e:	68 c3 10 80 00       	push   $0x8010c3
  800083:	6a 22                	push   $0x22
  800085:	68 b3 10 80 00       	push   $0x8010b3
  80008a:	e8 b5 01 00 00       	call   800244 <_panic>
	memmove(UTEMP, addr, PGSIZE);
  80008f:	83 ec 04             	sub    $0x4,%esp
  800092:	68 00 10 00 00       	push   $0x1000
  800097:	53                   	push   %ebx
  800098:	68 00 00 40 00       	push   $0x400000
  80009d:	e8 53 09 00 00       	call   8009f5 <memmove>
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  8000a2:	83 c4 08             	add    $0x8,%esp
  8000a5:	68 00 00 40 00       	push   $0x400000
  8000aa:	6a 00                	push   $0x0
  8000ac:	e8 38 0c 00 00       	call   800ce9 <sys_page_unmap>
  8000b1:	83 c4 10             	add    $0x10,%esp
  8000b4:	85 c0                	test   %eax,%eax
  8000b6:	79 12                	jns    8000ca <duppage+0x97>
		panic("sys_page_unmap: %e", r);
  8000b8:	50                   	push   %eax
  8000b9:	68 d4 10 80 00       	push   $0x8010d4
  8000be:	6a 25                	push   $0x25
  8000c0:	68 b3 10 80 00       	push   $0x8010b3
  8000c5:	e8 7a 01 00 00       	call   800244 <_panic>
}
  8000ca:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000cd:	5b                   	pop    %ebx
  8000ce:	5e                   	pop    %esi
  8000cf:	5d                   	pop    %ebp
  8000d0:	c3                   	ret    

008000d1 <dumbfork>:

envid_t
dumbfork(void)
{
  8000d1:	55                   	push   %ebp
  8000d2:	89 e5                	mov    %esp,%ebp
  8000d4:	56                   	push   %esi
  8000d5:	53                   	push   %ebx
  8000d6:	83 ec 10             	sub    $0x10,%esp
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  8000d9:	b8 07 00 00 00       	mov    $0x7,%eax
  8000de:	cd 30                	int    $0x30
  8000e0:	89 c3                	mov    %eax,%ebx
	// so that the child will appear to have called sys_exofork() too -
	// except that in the child, this "fake" call to sys_exofork()
	// will return 0 instead of the envid of the child.
	envid = sys_exofork();
	//cprintf("Returned from sys_exofork() in dumbfork is: %d\n", envid);
	if (envid < 0)
  8000e2:	85 c0                	test   %eax,%eax
  8000e4:	79 12                	jns    8000f8 <dumbfork+0x27>
		panic("sys_exofork: %e", envid);
  8000e6:	50                   	push   %eax
  8000e7:	68 e7 10 80 00       	push   $0x8010e7
  8000ec:	6a 38                	push   $0x38
  8000ee:	68 b3 10 80 00       	push   $0x8010b3
  8000f3:	e8 4c 01 00 00       	call   800244 <_panic>
  8000f8:	89 c6                	mov    %eax,%esi
	if (envid == 0) {
  8000fa:	85 c0                	test   %eax,%eax
  8000fc:	75 27                	jne    800125 <dumbfork+0x54>
		// We're the child.
		// The copied value of the global variable 'thisenv'
		// is no longer valid (it refers to the parent!).
		// Fix it and return 0.
		thisenv = &envs[ENVX(sys_getenvid())];
  8000fe:	e8 23 0b 00 00       	call   800c26 <sys_getenvid>
  800103:	25 ff 03 00 00       	and    $0x3ff,%eax
  800108:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80010f:	c1 e0 07             	shl    $0x7,%eax
  800112:	29 d0                	sub    %edx,%eax
  800114:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800119:	a3 04 20 80 00       	mov    %eax,0x802004
		return 0;
  80011e:	b8 00 00 00 00       	mov    $0x0,%eax
  800123:	eb 60                	jmp    800185 <dumbfork+0xb4>
	}

	// We're the parent.
	// Eagerly copy our entire address space into the child.
	// This is NOT what you should do in your fork implementation.
	for (addr = (uint8_t*) UTEXT; addr < end; addr += PGSIZE)
  800125:	c7 45 f4 00 00 80 00 	movl   $0x800000,-0xc(%ebp)
  80012c:	eb 14                	jmp    800142 <dumbfork+0x71>
		duppage(envid, addr);
  80012e:	83 ec 08             	sub    $0x8,%esp
  800131:	52                   	push   %edx
  800132:	56                   	push   %esi
  800133:	e8 fb fe ff ff       	call   800033 <duppage>
	}

	// We're the parent.
	// Eagerly copy our entire address space into the child.
	// This is NOT what you should do in your fork implementation.
	for (addr = (uint8_t*) UTEXT; addr < end; addr += PGSIZE)
  800138:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
  80013f:	83 c4 10             	add    $0x10,%esp
  800142:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800145:	81 fa 08 20 80 00    	cmp    $0x802008,%edx
  80014b:	72 e1                	jb     80012e <dumbfork+0x5d>
		duppage(envid, addr);

	// Also copy the stack we are currently running on.
	duppage(envid, ROUNDDOWN(&addr, PGSIZE));
  80014d:	83 ec 08             	sub    $0x8,%esp
  800150:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800153:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800158:	50                   	push   %eax
  800159:	53                   	push   %ebx
  80015a:	e8 d4 fe ff ff       	call   800033 <duppage>

	// Start the child environment running
	if ((r = sys_env_set_status(envid, ENV_RUNNABLE)) < 0)
  80015f:	83 c4 08             	add    $0x8,%esp
  800162:	6a 02                	push   $0x2
  800164:	53                   	push   %ebx
  800165:	e8 c1 0b 00 00       	call   800d2b <sys_env_set_status>
  80016a:	83 c4 10             	add    $0x10,%esp
  80016d:	85 c0                	test   %eax,%eax
  80016f:	79 12                	jns    800183 <dumbfork+0xb2>
		panic("sys_env_set_status: %e", r);
  800171:	50                   	push   %eax
  800172:	68 f7 10 80 00       	push   $0x8010f7
  800177:	6a 4d                	push   $0x4d
  800179:	68 b3 10 80 00       	push   $0x8010b3
  80017e:	e8 c1 00 00 00       	call   800244 <_panic>

	return envid;
  800183:	89 d8                	mov    %ebx,%eax
}
  800185:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800188:	5b                   	pop    %ebx
  800189:	5e                   	pop    %esi
  80018a:	5d                   	pop    %ebp
  80018b:	c3                   	ret    

0080018c <umain>:

envid_t dumbfork(void);

void
umain(int argc, char **argv)
{
  80018c:	55                   	push   %ebp
  80018d:	89 e5                	mov    %esp,%ebp
  80018f:	57                   	push   %edi
  800190:	56                   	push   %esi
  800191:	53                   	push   %ebx
  800192:	83 ec 0c             	sub    $0xc,%esp
	envid_t who;
	int i;

	// fork a child process
	who = dumbfork();
  800195:	e8 37 ff ff ff       	call   8000d1 <dumbfork>
  80019a:	89 c7                	mov    %eax,%edi
  80019c:	85 c0                	test   %eax,%eax
  80019e:	74 07                	je     8001a7 <umain+0x1b>
  8001a0:	be 0e 11 80 00       	mov    $0x80110e,%esi
  8001a5:	eb 05                	jmp    8001ac <umain+0x20>
  8001a7:	be 15 11 80 00       	mov    $0x801115,%esi

	// print a message and yield to the other a few times
	for (i = 0; i < (who ? 10 : 20); i++) {
  8001ac:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001b1:	eb 18                	jmp    8001cb <umain+0x3f>
		cprintf("%d: I am the %s!\n", i, who ? "parent" : "child");
  8001b3:	83 ec 04             	sub    $0x4,%esp
  8001b6:	56                   	push   %esi
  8001b7:	53                   	push   %ebx
  8001b8:	68 1b 11 80 00       	push   $0x80111b
  8001bd:	e8 5a 01 00 00       	call   80031c <cprintf>
		sys_yield();
  8001c2:	e8 7e 0a 00 00       	call   800c45 <sys_yield>

	// fork a child process
	who = dumbfork();

	// print a message and yield to the other a few times
	for (i = 0; i < (who ? 10 : 20); i++) {
  8001c7:	43                   	inc    %ebx
  8001c8:	83 c4 10             	add    $0x10,%esp
  8001cb:	85 ff                	test   %edi,%edi
  8001cd:	74 07                	je     8001d6 <umain+0x4a>
  8001cf:	83 fb 09             	cmp    $0x9,%ebx
  8001d2:	7e df                	jle    8001b3 <umain+0x27>
  8001d4:	eb 05                	jmp    8001db <umain+0x4f>
  8001d6:	83 fb 13             	cmp    $0x13,%ebx
  8001d9:	7e d8                	jle    8001b3 <umain+0x27>
		cprintf("%d: I am the %s!\n", i, who ? "parent" : "child");
		sys_yield();
	}
}
  8001db:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001de:	5b                   	pop    %ebx
  8001df:	5e                   	pop    %esi
  8001e0:	5f                   	pop    %edi
  8001e1:	5d                   	pop    %ebp
  8001e2:	c3                   	ret    

008001e3 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8001e3:	55                   	push   %ebp
  8001e4:	89 e5                	mov    %esp,%ebp
  8001e6:	56                   	push   %esi
  8001e7:	53                   	push   %ebx
  8001e8:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8001eb:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8001ee:	e8 33 0a 00 00       	call   800c26 <sys_getenvid>
  8001f3:	25 ff 03 00 00       	and    $0x3ff,%eax
  8001f8:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001ff:	c1 e0 07             	shl    $0x7,%eax
  800202:	29 d0                	sub    %edx,%eax
  800204:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800209:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80020e:	85 db                	test   %ebx,%ebx
  800210:	7e 07                	jle    800219 <libmain+0x36>
		binaryname = argv[0];
  800212:	8b 06                	mov    (%esi),%eax
  800214:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800219:	83 ec 08             	sub    $0x8,%esp
  80021c:	56                   	push   %esi
  80021d:	53                   	push   %ebx
  80021e:	e8 69 ff ff ff       	call   80018c <umain>

	// exit gracefully
	exit();
  800223:	e8 0a 00 00 00       	call   800232 <exit>
}
  800228:	83 c4 10             	add    $0x10,%esp
  80022b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80022e:	5b                   	pop    %ebx
  80022f:	5e                   	pop    %esi
  800230:	5d                   	pop    %ebp
  800231:	c3                   	ret    

00800232 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800232:	55                   	push   %ebp
  800233:	89 e5                	mov    %esp,%ebp
  800235:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800238:	6a 00                	push   $0x0
  80023a:	e8 a6 09 00 00       	call   800be5 <sys_env_destroy>
}
  80023f:	83 c4 10             	add    $0x10,%esp
  800242:	c9                   	leave  
  800243:	c3                   	ret    

00800244 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800244:	55                   	push   %ebp
  800245:	89 e5                	mov    %esp,%ebp
  800247:	56                   	push   %esi
  800248:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800249:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80024c:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800252:	e8 cf 09 00 00       	call   800c26 <sys_getenvid>
  800257:	83 ec 0c             	sub    $0xc,%esp
  80025a:	ff 75 0c             	pushl  0xc(%ebp)
  80025d:	ff 75 08             	pushl  0x8(%ebp)
  800260:	56                   	push   %esi
  800261:	50                   	push   %eax
  800262:	68 38 11 80 00       	push   $0x801138
  800267:	e8 b0 00 00 00       	call   80031c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80026c:	83 c4 18             	add    $0x18,%esp
  80026f:	53                   	push   %ebx
  800270:	ff 75 10             	pushl  0x10(%ebp)
  800273:	e8 53 00 00 00       	call   8002cb <vcprintf>
	cprintf("\n");
  800278:	c7 04 24 2b 11 80 00 	movl   $0x80112b,(%esp)
  80027f:	e8 98 00 00 00       	call   80031c <cprintf>
  800284:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800287:	cc                   	int3   
  800288:	eb fd                	jmp    800287 <_panic+0x43>

0080028a <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80028a:	55                   	push   %ebp
  80028b:	89 e5                	mov    %esp,%ebp
  80028d:	53                   	push   %ebx
  80028e:	83 ec 04             	sub    $0x4,%esp
  800291:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800294:	8b 13                	mov    (%ebx),%edx
  800296:	8d 42 01             	lea    0x1(%edx),%eax
  800299:	89 03                	mov    %eax,(%ebx)
  80029b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80029e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8002a2:	3d ff 00 00 00       	cmp    $0xff,%eax
  8002a7:	75 1a                	jne    8002c3 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8002a9:	83 ec 08             	sub    $0x8,%esp
  8002ac:	68 ff 00 00 00       	push   $0xff
  8002b1:	8d 43 08             	lea    0x8(%ebx),%eax
  8002b4:	50                   	push   %eax
  8002b5:	e8 ee 08 00 00       	call   800ba8 <sys_cputs>
		b->idx = 0;
  8002ba:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8002c0:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8002c3:	ff 43 04             	incl   0x4(%ebx)
}
  8002c6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8002c9:	c9                   	leave  
  8002ca:	c3                   	ret    

008002cb <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8002cb:	55                   	push   %ebp
  8002cc:	89 e5                	mov    %esp,%ebp
  8002ce:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8002d4:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8002db:	00 00 00 
	b.cnt = 0;
  8002de:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8002e5:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8002e8:	ff 75 0c             	pushl  0xc(%ebp)
  8002eb:	ff 75 08             	pushl  0x8(%ebp)
  8002ee:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8002f4:	50                   	push   %eax
  8002f5:	68 8a 02 80 00       	push   $0x80028a
  8002fa:	e8 51 01 00 00       	call   800450 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8002ff:	83 c4 08             	add    $0x8,%esp
  800302:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800308:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80030e:	50                   	push   %eax
  80030f:	e8 94 08 00 00       	call   800ba8 <sys_cputs>

	return b.cnt;
}
  800314:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80031a:	c9                   	leave  
  80031b:	c3                   	ret    

0080031c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80031c:	55                   	push   %ebp
  80031d:	89 e5                	mov    %esp,%ebp
  80031f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800322:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800325:	50                   	push   %eax
  800326:	ff 75 08             	pushl  0x8(%ebp)
  800329:	e8 9d ff ff ff       	call   8002cb <vcprintf>
	va_end(ap);

	return cnt;
}
  80032e:	c9                   	leave  
  80032f:	c3                   	ret    

00800330 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800330:	55                   	push   %ebp
  800331:	89 e5                	mov    %esp,%ebp
  800333:	57                   	push   %edi
  800334:	56                   	push   %esi
  800335:	53                   	push   %ebx
  800336:	83 ec 1c             	sub    $0x1c,%esp
  800339:	89 c7                	mov    %eax,%edi
  80033b:	89 d6                	mov    %edx,%esi
  80033d:	8b 45 08             	mov    0x8(%ebp),%eax
  800340:	8b 55 0c             	mov    0xc(%ebp),%edx
  800343:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800346:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800349:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80034c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800351:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800354:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800357:	39 d3                	cmp    %edx,%ebx
  800359:	72 05                	jb     800360 <printnum+0x30>
  80035b:	39 45 10             	cmp    %eax,0x10(%ebp)
  80035e:	77 45                	ja     8003a5 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800360:	83 ec 0c             	sub    $0xc,%esp
  800363:	ff 75 18             	pushl  0x18(%ebp)
  800366:	8b 45 14             	mov    0x14(%ebp),%eax
  800369:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80036c:	53                   	push   %ebx
  80036d:	ff 75 10             	pushl  0x10(%ebp)
  800370:	83 ec 08             	sub    $0x8,%esp
  800373:	ff 75 e4             	pushl  -0x1c(%ebp)
  800376:	ff 75 e0             	pushl  -0x20(%ebp)
  800379:	ff 75 dc             	pushl  -0x24(%ebp)
  80037c:	ff 75 d8             	pushl  -0x28(%ebp)
  80037f:	e8 b0 0a 00 00       	call   800e34 <__udivdi3>
  800384:	83 c4 18             	add    $0x18,%esp
  800387:	52                   	push   %edx
  800388:	50                   	push   %eax
  800389:	89 f2                	mov    %esi,%edx
  80038b:	89 f8                	mov    %edi,%eax
  80038d:	e8 9e ff ff ff       	call   800330 <printnum>
  800392:	83 c4 20             	add    $0x20,%esp
  800395:	eb 16                	jmp    8003ad <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800397:	83 ec 08             	sub    $0x8,%esp
  80039a:	56                   	push   %esi
  80039b:	ff 75 18             	pushl  0x18(%ebp)
  80039e:	ff d7                	call   *%edi
  8003a0:	83 c4 10             	add    $0x10,%esp
  8003a3:	eb 03                	jmp    8003a8 <printnum+0x78>
  8003a5:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8003a8:	4b                   	dec    %ebx
  8003a9:	85 db                	test   %ebx,%ebx
  8003ab:	7f ea                	jg     800397 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8003ad:	83 ec 08             	sub    $0x8,%esp
  8003b0:	56                   	push   %esi
  8003b1:	83 ec 04             	sub    $0x4,%esp
  8003b4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8003b7:	ff 75 e0             	pushl  -0x20(%ebp)
  8003ba:	ff 75 dc             	pushl  -0x24(%ebp)
  8003bd:	ff 75 d8             	pushl  -0x28(%ebp)
  8003c0:	e8 7f 0b 00 00       	call   800f44 <__umoddi3>
  8003c5:	83 c4 14             	add    $0x14,%esp
  8003c8:	0f be 80 5c 11 80 00 	movsbl 0x80115c(%eax),%eax
  8003cf:	50                   	push   %eax
  8003d0:	ff d7                	call   *%edi
}
  8003d2:	83 c4 10             	add    $0x10,%esp
  8003d5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8003d8:	5b                   	pop    %ebx
  8003d9:	5e                   	pop    %esi
  8003da:	5f                   	pop    %edi
  8003db:	5d                   	pop    %ebp
  8003dc:	c3                   	ret    

008003dd <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8003dd:	55                   	push   %ebp
  8003de:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8003e0:	83 fa 01             	cmp    $0x1,%edx
  8003e3:	7e 0e                	jle    8003f3 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8003e5:	8b 10                	mov    (%eax),%edx
  8003e7:	8d 4a 08             	lea    0x8(%edx),%ecx
  8003ea:	89 08                	mov    %ecx,(%eax)
  8003ec:	8b 02                	mov    (%edx),%eax
  8003ee:	8b 52 04             	mov    0x4(%edx),%edx
  8003f1:	eb 22                	jmp    800415 <getuint+0x38>
	else if (lflag)
  8003f3:	85 d2                	test   %edx,%edx
  8003f5:	74 10                	je     800407 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8003f7:	8b 10                	mov    (%eax),%edx
  8003f9:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003fc:	89 08                	mov    %ecx,(%eax)
  8003fe:	8b 02                	mov    (%edx),%eax
  800400:	ba 00 00 00 00       	mov    $0x0,%edx
  800405:	eb 0e                	jmp    800415 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800407:	8b 10                	mov    (%eax),%edx
  800409:	8d 4a 04             	lea    0x4(%edx),%ecx
  80040c:	89 08                	mov    %ecx,(%eax)
  80040e:	8b 02                	mov    (%edx),%eax
  800410:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800415:	5d                   	pop    %ebp
  800416:	c3                   	ret    

00800417 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800417:	55                   	push   %ebp
  800418:	89 e5                	mov    %esp,%ebp
  80041a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80041d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800420:	8b 10                	mov    (%eax),%edx
  800422:	3b 50 04             	cmp    0x4(%eax),%edx
  800425:	73 0a                	jae    800431 <sprintputch+0x1a>
		*b->buf++ = ch;
  800427:	8d 4a 01             	lea    0x1(%edx),%ecx
  80042a:	89 08                	mov    %ecx,(%eax)
  80042c:	8b 45 08             	mov    0x8(%ebp),%eax
  80042f:	88 02                	mov    %al,(%edx)
}
  800431:	5d                   	pop    %ebp
  800432:	c3                   	ret    

00800433 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800433:	55                   	push   %ebp
  800434:	89 e5                	mov    %esp,%ebp
  800436:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800439:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80043c:	50                   	push   %eax
  80043d:	ff 75 10             	pushl  0x10(%ebp)
  800440:	ff 75 0c             	pushl  0xc(%ebp)
  800443:	ff 75 08             	pushl  0x8(%ebp)
  800446:	e8 05 00 00 00       	call   800450 <vprintfmt>
	va_end(ap);
}
  80044b:	83 c4 10             	add    $0x10,%esp
  80044e:	c9                   	leave  
  80044f:	c3                   	ret    

00800450 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800450:	55                   	push   %ebp
  800451:	89 e5                	mov    %esp,%ebp
  800453:	57                   	push   %edi
  800454:	56                   	push   %esi
  800455:	53                   	push   %ebx
  800456:	83 ec 2c             	sub    $0x2c,%esp
  800459:	8b 75 08             	mov    0x8(%ebp),%esi
  80045c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80045f:	8b 7d 10             	mov    0x10(%ebp),%edi
  800462:	eb 12                	jmp    800476 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800464:	85 c0                	test   %eax,%eax
  800466:	0f 84 68 03 00 00    	je     8007d4 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80046c:	83 ec 08             	sub    $0x8,%esp
  80046f:	53                   	push   %ebx
  800470:	50                   	push   %eax
  800471:	ff d6                	call   *%esi
  800473:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800476:	47                   	inc    %edi
  800477:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80047b:	83 f8 25             	cmp    $0x25,%eax
  80047e:	75 e4                	jne    800464 <vprintfmt+0x14>
  800480:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800484:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80048b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800492:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800499:	ba 00 00 00 00       	mov    $0x0,%edx
  80049e:	eb 07                	jmp    8004a7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a0:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8004a3:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a7:	8d 47 01             	lea    0x1(%edi),%eax
  8004aa:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8004ad:	0f b6 0f             	movzbl (%edi),%ecx
  8004b0:	8a 07                	mov    (%edi),%al
  8004b2:	83 e8 23             	sub    $0x23,%eax
  8004b5:	3c 55                	cmp    $0x55,%al
  8004b7:	0f 87 fe 02 00 00    	ja     8007bb <vprintfmt+0x36b>
  8004bd:	0f b6 c0             	movzbl %al,%eax
  8004c0:	ff 24 85 20 12 80 00 	jmp    *0x801220(,%eax,4)
  8004c7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8004ca:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8004ce:	eb d7                	jmp    8004a7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004d0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004d3:	b8 00 00 00 00       	mov    $0x0,%eax
  8004d8:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8004db:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8004de:	01 c0                	add    %eax,%eax
  8004e0:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8004e4:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8004e7:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8004ea:	83 fa 09             	cmp    $0x9,%edx
  8004ed:	77 34                	ja     800523 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8004ef:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8004f0:	eb e9                	jmp    8004db <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8004f2:	8b 45 14             	mov    0x14(%ebp),%eax
  8004f5:	8d 48 04             	lea    0x4(%eax),%ecx
  8004f8:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8004fb:	8b 00                	mov    (%eax),%eax
  8004fd:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800500:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800503:	eb 24                	jmp    800529 <vprintfmt+0xd9>
  800505:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800509:	79 07                	jns    800512 <vprintfmt+0xc2>
  80050b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800512:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800515:	eb 90                	jmp    8004a7 <vprintfmt+0x57>
  800517:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80051a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800521:	eb 84                	jmp    8004a7 <vprintfmt+0x57>
  800523:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800526:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800529:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80052d:	0f 89 74 ff ff ff    	jns    8004a7 <vprintfmt+0x57>
				width = precision, precision = -1;
  800533:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800536:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800539:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800540:	e9 62 ff ff ff       	jmp    8004a7 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800545:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800546:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800549:	e9 59 ff ff ff       	jmp    8004a7 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80054e:	8b 45 14             	mov    0x14(%ebp),%eax
  800551:	8d 50 04             	lea    0x4(%eax),%edx
  800554:	89 55 14             	mov    %edx,0x14(%ebp)
  800557:	83 ec 08             	sub    $0x8,%esp
  80055a:	53                   	push   %ebx
  80055b:	ff 30                	pushl  (%eax)
  80055d:	ff d6                	call   *%esi
			break;
  80055f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800562:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800565:	e9 0c ff ff ff       	jmp    800476 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80056a:	8b 45 14             	mov    0x14(%ebp),%eax
  80056d:	8d 50 04             	lea    0x4(%eax),%edx
  800570:	89 55 14             	mov    %edx,0x14(%ebp)
  800573:	8b 00                	mov    (%eax),%eax
  800575:	85 c0                	test   %eax,%eax
  800577:	79 02                	jns    80057b <vprintfmt+0x12b>
  800579:	f7 d8                	neg    %eax
  80057b:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80057d:	83 f8 08             	cmp    $0x8,%eax
  800580:	7f 0b                	jg     80058d <vprintfmt+0x13d>
  800582:	8b 04 85 80 13 80 00 	mov    0x801380(,%eax,4),%eax
  800589:	85 c0                	test   %eax,%eax
  80058b:	75 18                	jne    8005a5 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80058d:	52                   	push   %edx
  80058e:	68 74 11 80 00       	push   $0x801174
  800593:	53                   	push   %ebx
  800594:	56                   	push   %esi
  800595:	e8 99 fe ff ff       	call   800433 <printfmt>
  80059a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8005a0:	e9 d1 fe ff ff       	jmp    800476 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8005a5:	50                   	push   %eax
  8005a6:	68 7d 11 80 00       	push   $0x80117d
  8005ab:	53                   	push   %ebx
  8005ac:	56                   	push   %esi
  8005ad:	e8 81 fe ff ff       	call   800433 <printfmt>
  8005b2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005b8:	e9 b9 fe ff ff       	jmp    800476 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8005bd:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c0:	8d 50 04             	lea    0x4(%eax),%edx
  8005c3:	89 55 14             	mov    %edx,0x14(%ebp)
  8005c6:	8b 38                	mov    (%eax),%edi
  8005c8:	85 ff                	test   %edi,%edi
  8005ca:	75 05                	jne    8005d1 <vprintfmt+0x181>
				p = "(null)";
  8005cc:	bf 6d 11 80 00       	mov    $0x80116d,%edi
			if (width > 0 && padc != '-')
  8005d1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005d5:	0f 8e 90 00 00 00    	jle    80066b <vprintfmt+0x21b>
  8005db:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8005df:	0f 84 8e 00 00 00    	je     800673 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8005e5:	83 ec 08             	sub    $0x8,%esp
  8005e8:	ff 75 d0             	pushl  -0x30(%ebp)
  8005eb:	57                   	push   %edi
  8005ec:	e8 70 02 00 00       	call   800861 <strnlen>
  8005f1:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8005f4:	29 c1                	sub    %eax,%ecx
  8005f6:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8005f9:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8005fc:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800600:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800603:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800606:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800608:	eb 0d                	jmp    800617 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80060a:	83 ec 08             	sub    $0x8,%esp
  80060d:	53                   	push   %ebx
  80060e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800611:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800613:	4f                   	dec    %edi
  800614:	83 c4 10             	add    $0x10,%esp
  800617:	85 ff                	test   %edi,%edi
  800619:	7f ef                	jg     80060a <vprintfmt+0x1ba>
  80061b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80061e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800621:	89 c8                	mov    %ecx,%eax
  800623:	85 c9                	test   %ecx,%ecx
  800625:	79 05                	jns    80062c <vprintfmt+0x1dc>
  800627:	b8 00 00 00 00       	mov    $0x0,%eax
  80062c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80062f:	29 c1                	sub    %eax,%ecx
  800631:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800634:	89 75 08             	mov    %esi,0x8(%ebp)
  800637:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80063a:	eb 3d                	jmp    800679 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80063c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800640:	74 19                	je     80065b <vprintfmt+0x20b>
  800642:	0f be c0             	movsbl %al,%eax
  800645:	83 e8 20             	sub    $0x20,%eax
  800648:	83 f8 5e             	cmp    $0x5e,%eax
  80064b:	76 0e                	jbe    80065b <vprintfmt+0x20b>
					putch('?', putdat);
  80064d:	83 ec 08             	sub    $0x8,%esp
  800650:	53                   	push   %ebx
  800651:	6a 3f                	push   $0x3f
  800653:	ff 55 08             	call   *0x8(%ebp)
  800656:	83 c4 10             	add    $0x10,%esp
  800659:	eb 0b                	jmp    800666 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80065b:	83 ec 08             	sub    $0x8,%esp
  80065e:	53                   	push   %ebx
  80065f:	52                   	push   %edx
  800660:	ff 55 08             	call   *0x8(%ebp)
  800663:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800666:	ff 4d e4             	decl   -0x1c(%ebp)
  800669:	eb 0e                	jmp    800679 <vprintfmt+0x229>
  80066b:	89 75 08             	mov    %esi,0x8(%ebp)
  80066e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800671:	eb 06                	jmp    800679 <vprintfmt+0x229>
  800673:	89 75 08             	mov    %esi,0x8(%ebp)
  800676:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800679:	47                   	inc    %edi
  80067a:	8a 47 ff             	mov    -0x1(%edi),%al
  80067d:	0f be d0             	movsbl %al,%edx
  800680:	85 d2                	test   %edx,%edx
  800682:	74 1d                	je     8006a1 <vprintfmt+0x251>
  800684:	85 f6                	test   %esi,%esi
  800686:	78 b4                	js     80063c <vprintfmt+0x1ec>
  800688:	4e                   	dec    %esi
  800689:	79 b1                	jns    80063c <vprintfmt+0x1ec>
  80068b:	8b 75 08             	mov    0x8(%ebp),%esi
  80068e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800691:	eb 14                	jmp    8006a7 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800693:	83 ec 08             	sub    $0x8,%esp
  800696:	53                   	push   %ebx
  800697:	6a 20                	push   $0x20
  800699:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80069b:	4f                   	dec    %edi
  80069c:	83 c4 10             	add    $0x10,%esp
  80069f:	eb 06                	jmp    8006a7 <vprintfmt+0x257>
  8006a1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8006a4:	8b 75 08             	mov    0x8(%ebp),%esi
  8006a7:	85 ff                	test   %edi,%edi
  8006a9:	7f e8                	jg     800693 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006ab:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006ae:	e9 c3 fd ff ff       	jmp    800476 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8006b3:	83 fa 01             	cmp    $0x1,%edx
  8006b6:	7e 16                	jle    8006ce <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8006b8:	8b 45 14             	mov    0x14(%ebp),%eax
  8006bb:	8d 50 08             	lea    0x8(%eax),%edx
  8006be:	89 55 14             	mov    %edx,0x14(%ebp)
  8006c1:	8b 50 04             	mov    0x4(%eax),%edx
  8006c4:	8b 00                	mov    (%eax),%eax
  8006c6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006c9:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8006cc:	eb 32                	jmp    800700 <vprintfmt+0x2b0>
	else if (lflag)
  8006ce:	85 d2                	test   %edx,%edx
  8006d0:	74 18                	je     8006ea <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8006d2:	8b 45 14             	mov    0x14(%ebp),%eax
  8006d5:	8d 50 04             	lea    0x4(%eax),%edx
  8006d8:	89 55 14             	mov    %edx,0x14(%ebp)
  8006db:	8b 00                	mov    (%eax),%eax
  8006dd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006e0:	89 c1                	mov    %eax,%ecx
  8006e2:	c1 f9 1f             	sar    $0x1f,%ecx
  8006e5:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8006e8:	eb 16                	jmp    800700 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8006ea:	8b 45 14             	mov    0x14(%ebp),%eax
  8006ed:	8d 50 04             	lea    0x4(%eax),%edx
  8006f0:	89 55 14             	mov    %edx,0x14(%ebp)
  8006f3:	8b 00                	mov    (%eax),%eax
  8006f5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006f8:	89 c1                	mov    %eax,%ecx
  8006fa:	c1 f9 1f             	sar    $0x1f,%ecx
  8006fd:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800700:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800703:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800706:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80070a:	79 76                	jns    800782 <vprintfmt+0x332>
				putch('-', putdat);
  80070c:	83 ec 08             	sub    $0x8,%esp
  80070f:	53                   	push   %ebx
  800710:	6a 2d                	push   $0x2d
  800712:	ff d6                	call   *%esi
				num = -(long long) num;
  800714:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800717:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80071a:	f7 d8                	neg    %eax
  80071c:	83 d2 00             	adc    $0x0,%edx
  80071f:	f7 da                	neg    %edx
  800721:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800724:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800729:	eb 5c                	jmp    800787 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80072b:	8d 45 14             	lea    0x14(%ebp),%eax
  80072e:	e8 aa fc ff ff       	call   8003dd <getuint>
			base = 10;
  800733:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800738:	eb 4d                	jmp    800787 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80073a:	8d 45 14             	lea    0x14(%ebp),%eax
  80073d:	e8 9b fc ff ff       	call   8003dd <getuint>
			base = 8;
  800742:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800747:	eb 3e                	jmp    800787 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800749:	83 ec 08             	sub    $0x8,%esp
  80074c:	53                   	push   %ebx
  80074d:	6a 30                	push   $0x30
  80074f:	ff d6                	call   *%esi
			putch('x', putdat);
  800751:	83 c4 08             	add    $0x8,%esp
  800754:	53                   	push   %ebx
  800755:	6a 78                	push   $0x78
  800757:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800759:	8b 45 14             	mov    0x14(%ebp),%eax
  80075c:	8d 50 04             	lea    0x4(%eax),%edx
  80075f:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800762:	8b 00                	mov    (%eax),%eax
  800764:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800769:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80076c:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800771:	eb 14                	jmp    800787 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800773:	8d 45 14             	lea    0x14(%ebp),%eax
  800776:	e8 62 fc ff ff       	call   8003dd <getuint>
			base = 16;
  80077b:	b9 10 00 00 00       	mov    $0x10,%ecx
  800780:	eb 05                	jmp    800787 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800782:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800787:	83 ec 0c             	sub    $0xc,%esp
  80078a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80078e:	57                   	push   %edi
  80078f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800792:	51                   	push   %ecx
  800793:	52                   	push   %edx
  800794:	50                   	push   %eax
  800795:	89 da                	mov    %ebx,%edx
  800797:	89 f0                	mov    %esi,%eax
  800799:	e8 92 fb ff ff       	call   800330 <printnum>
			break;
  80079e:	83 c4 20             	add    $0x20,%esp
  8007a1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007a4:	e9 cd fc ff ff       	jmp    800476 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8007a9:	83 ec 08             	sub    $0x8,%esp
  8007ac:	53                   	push   %ebx
  8007ad:	51                   	push   %ecx
  8007ae:	ff d6                	call   *%esi
			break;
  8007b0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007b3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8007b6:	e9 bb fc ff ff       	jmp    800476 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8007bb:	83 ec 08             	sub    $0x8,%esp
  8007be:	53                   	push   %ebx
  8007bf:	6a 25                	push   $0x25
  8007c1:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8007c3:	83 c4 10             	add    $0x10,%esp
  8007c6:	eb 01                	jmp    8007c9 <vprintfmt+0x379>
  8007c8:	4f                   	dec    %edi
  8007c9:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8007cd:	75 f9                	jne    8007c8 <vprintfmt+0x378>
  8007cf:	e9 a2 fc ff ff       	jmp    800476 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8007d4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8007d7:	5b                   	pop    %ebx
  8007d8:	5e                   	pop    %esi
  8007d9:	5f                   	pop    %edi
  8007da:	5d                   	pop    %ebp
  8007db:	c3                   	ret    

008007dc <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8007dc:	55                   	push   %ebp
  8007dd:	89 e5                	mov    %esp,%ebp
  8007df:	83 ec 18             	sub    $0x18,%esp
  8007e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e5:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8007e8:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8007eb:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8007ef:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8007f2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007f9:	85 c0                	test   %eax,%eax
  8007fb:	74 26                	je     800823 <vsnprintf+0x47>
  8007fd:	85 d2                	test   %edx,%edx
  8007ff:	7e 29                	jle    80082a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800801:	ff 75 14             	pushl  0x14(%ebp)
  800804:	ff 75 10             	pushl  0x10(%ebp)
  800807:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80080a:	50                   	push   %eax
  80080b:	68 17 04 80 00       	push   $0x800417
  800810:	e8 3b fc ff ff       	call   800450 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800815:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800818:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80081b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80081e:	83 c4 10             	add    $0x10,%esp
  800821:	eb 0c                	jmp    80082f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800823:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800828:	eb 05                	jmp    80082f <vsnprintf+0x53>
  80082a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80082f:	c9                   	leave  
  800830:	c3                   	ret    

00800831 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800831:	55                   	push   %ebp
  800832:	89 e5                	mov    %esp,%ebp
  800834:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800837:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80083a:	50                   	push   %eax
  80083b:	ff 75 10             	pushl  0x10(%ebp)
  80083e:	ff 75 0c             	pushl  0xc(%ebp)
  800841:	ff 75 08             	pushl  0x8(%ebp)
  800844:	e8 93 ff ff ff       	call   8007dc <vsnprintf>
	va_end(ap);

	return rc;
}
  800849:	c9                   	leave  
  80084a:	c3                   	ret    

0080084b <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80084b:	55                   	push   %ebp
  80084c:	89 e5                	mov    %esp,%ebp
  80084e:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800851:	b8 00 00 00 00       	mov    $0x0,%eax
  800856:	eb 01                	jmp    800859 <strlen+0xe>
		n++;
  800858:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800859:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80085d:	75 f9                	jne    800858 <strlen+0xd>
		n++;
	return n;
}
  80085f:	5d                   	pop    %ebp
  800860:	c3                   	ret    

00800861 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800861:	55                   	push   %ebp
  800862:	89 e5                	mov    %esp,%ebp
  800864:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800867:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80086a:	ba 00 00 00 00       	mov    $0x0,%edx
  80086f:	eb 01                	jmp    800872 <strnlen+0x11>
		n++;
  800871:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800872:	39 c2                	cmp    %eax,%edx
  800874:	74 08                	je     80087e <strnlen+0x1d>
  800876:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80087a:	75 f5                	jne    800871 <strnlen+0x10>
  80087c:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80087e:	5d                   	pop    %ebp
  80087f:	c3                   	ret    

00800880 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800880:	55                   	push   %ebp
  800881:	89 e5                	mov    %esp,%ebp
  800883:	53                   	push   %ebx
  800884:	8b 45 08             	mov    0x8(%ebp),%eax
  800887:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80088a:	89 c2                	mov    %eax,%edx
  80088c:	42                   	inc    %edx
  80088d:	41                   	inc    %ecx
  80088e:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800891:	88 5a ff             	mov    %bl,-0x1(%edx)
  800894:	84 db                	test   %bl,%bl
  800896:	75 f4                	jne    80088c <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800898:	5b                   	pop    %ebx
  800899:	5d                   	pop    %ebp
  80089a:	c3                   	ret    

0080089b <strcat>:

char *
strcat(char *dst, const char *src)
{
  80089b:	55                   	push   %ebp
  80089c:	89 e5                	mov    %esp,%ebp
  80089e:	53                   	push   %ebx
  80089f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8008a2:	53                   	push   %ebx
  8008a3:	e8 a3 ff ff ff       	call   80084b <strlen>
  8008a8:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8008ab:	ff 75 0c             	pushl  0xc(%ebp)
  8008ae:	01 d8                	add    %ebx,%eax
  8008b0:	50                   	push   %eax
  8008b1:	e8 ca ff ff ff       	call   800880 <strcpy>
	return dst;
}
  8008b6:	89 d8                	mov    %ebx,%eax
  8008b8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8008bb:	c9                   	leave  
  8008bc:	c3                   	ret    

008008bd <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8008bd:	55                   	push   %ebp
  8008be:	89 e5                	mov    %esp,%ebp
  8008c0:	56                   	push   %esi
  8008c1:	53                   	push   %ebx
  8008c2:	8b 75 08             	mov    0x8(%ebp),%esi
  8008c5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8008c8:	89 f3                	mov    %esi,%ebx
  8008ca:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008cd:	89 f2                	mov    %esi,%edx
  8008cf:	eb 0c                	jmp    8008dd <strncpy+0x20>
		*dst++ = *src;
  8008d1:	42                   	inc    %edx
  8008d2:	8a 01                	mov    (%ecx),%al
  8008d4:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8008d7:	80 39 01             	cmpb   $0x1,(%ecx)
  8008da:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008dd:	39 da                	cmp    %ebx,%edx
  8008df:	75 f0                	jne    8008d1 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8008e1:	89 f0                	mov    %esi,%eax
  8008e3:	5b                   	pop    %ebx
  8008e4:	5e                   	pop    %esi
  8008e5:	5d                   	pop    %ebp
  8008e6:	c3                   	ret    

008008e7 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8008e7:	55                   	push   %ebp
  8008e8:	89 e5                	mov    %esp,%ebp
  8008ea:	56                   	push   %esi
  8008eb:	53                   	push   %ebx
  8008ec:	8b 75 08             	mov    0x8(%ebp),%esi
  8008ef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8008f2:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8008f5:	85 c0                	test   %eax,%eax
  8008f7:	74 1e                	je     800917 <strlcpy+0x30>
  8008f9:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8008fd:	89 f2                	mov    %esi,%edx
  8008ff:	eb 05                	jmp    800906 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800901:	42                   	inc    %edx
  800902:	41                   	inc    %ecx
  800903:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800906:	39 c2                	cmp    %eax,%edx
  800908:	74 08                	je     800912 <strlcpy+0x2b>
  80090a:	8a 19                	mov    (%ecx),%bl
  80090c:	84 db                	test   %bl,%bl
  80090e:	75 f1                	jne    800901 <strlcpy+0x1a>
  800910:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800912:	c6 00 00             	movb   $0x0,(%eax)
  800915:	eb 02                	jmp    800919 <strlcpy+0x32>
  800917:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800919:	29 f0                	sub    %esi,%eax
}
  80091b:	5b                   	pop    %ebx
  80091c:	5e                   	pop    %esi
  80091d:	5d                   	pop    %ebp
  80091e:	c3                   	ret    

0080091f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80091f:	55                   	push   %ebp
  800920:	89 e5                	mov    %esp,%ebp
  800922:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800925:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800928:	eb 02                	jmp    80092c <strcmp+0xd>
		p++, q++;
  80092a:	41                   	inc    %ecx
  80092b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80092c:	8a 01                	mov    (%ecx),%al
  80092e:	84 c0                	test   %al,%al
  800930:	74 04                	je     800936 <strcmp+0x17>
  800932:	3a 02                	cmp    (%edx),%al
  800934:	74 f4                	je     80092a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800936:	0f b6 c0             	movzbl %al,%eax
  800939:	0f b6 12             	movzbl (%edx),%edx
  80093c:	29 d0                	sub    %edx,%eax
}
  80093e:	5d                   	pop    %ebp
  80093f:	c3                   	ret    

00800940 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800940:	55                   	push   %ebp
  800941:	89 e5                	mov    %esp,%ebp
  800943:	53                   	push   %ebx
  800944:	8b 45 08             	mov    0x8(%ebp),%eax
  800947:	8b 55 0c             	mov    0xc(%ebp),%edx
  80094a:	89 c3                	mov    %eax,%ebx
  80094c:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80094f:	eb 02                	jmp    800953 <strncmp+0x13>
		n--, p++, q++;
  800951:	40                   	inc    %eax
  800952:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800953:	39 d8                	cmp    %ebx,%eax
  800955:	74 14                	je     80096b <strncmp+0x2b>
  800957:	8a 08                	mov    (%eax),%cl
  800959:	84 c9                	test   %cl,%cl
  80095b:	74 04                	je     800961 <strncmp+0x21>
  80095d:	3a 0a                	cmp    (%edx),%cl
  80095f:	74 f0                	je     800951 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800961:	0f b6 00             	movzbl (%eax),%eax
  800964:	0f b6 12             	movzbl (%edx),%edx
  800967:	29 d0                	sub    %edx,%eax
  800969:	eb 05                	jmp    800970 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80096b:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800970:	5b                   	pop    %ebx
  800971:	5d                   	pop    %ebp
  800972:	c3                   	ret    

00800973 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800973:	55                   	push   %ebp
  800974:	89 e5                	mov    %esp,%ebp
  800976:	8b 45 08             	mov    0x8(%ebp),%eax
  800979:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80097c:	eb 05                	jmp    800983 <strchr+0x10>
		if (*s == c)
  80097e:	38 ca                	cmp    %cl,%dl
  800980:	74 0c                	je     80098e <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800982:	40                   	inc    %eax
  800983:	8a 10                	mov    (%eax),%dl
  800985:	84 d2                	test   %dl,%dl
  800987:	75 f5                	jne    80097e <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800989:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80098e:	5d                   	pop    %ebp
  80098f:	c3                   	ret    

00800990 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800990:	55                   	push   %ebp
  800991:	89 e5                	mov    %esp,%ebp
  800993:	8b 45 08             	mov    0x8(%ebp),%eax
  800996:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800999:	eb 05                	jmp    8009a0 <strfind+0x10>
		if (*s == c)
  80099b:	38 ca                	cmp    %cl,%dl
  80099d:	74 07                	je     8009a6 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80099f:	40                   	inc    %eax
  8009a0:	8a 10                	mov    (%eax),%dl
  8009a2:	84 d2                	test   %dl,%dl
  8009a4:	75 f5                	jne    80099b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8009a6:	5d                   	pop    %ebp
  8009a7:	c3                   	ret    

008009a8 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8009a8:	55                   	push   %ebp
  8009a9:	89 e5                	mov    %esp,%ebp
  8009ab:	57                   	push   %edi
  8009ac:	56                   	push   %esi
  8009ad:	53                   	push   %ebx
  8009ae:	8b 7d 08             	mov    0x8(%ebp),%edi
  8009b1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8009b4:	85 c9                	test   %ecx,%ecx
  8009b6:	74 36                	je     8009ee <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8009b8:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8009be:	75 28                	jne    8009e8 <memset+0x40>
  8009c0:	f6 c1 03             	test   $0x3,%cl
  8009c3:	75 23                	jne    8009e8 <memset+0x40>
		c &= 0xFF;
  8009c5:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8009c9:	89 d3                	mov    %edx,%ebx
  8009cb:	c1 e3 08             	shl    $0x8,%ebx
  8009ce:	89 d6                	mov    %edx,%esi
  8009d0:	c1 e6 18             	shl    $0x18,%esi
  8009d3:	89 d0                	mov    %edx,%eax
  8009d5:	c1 e0 10             	shl    $0x10,%eax
  8009d8:	09 f0                	or     %esi,%eax
  8009da:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8009dc:	89 d8                	mov    %ebx,%eax
  8009de:	09 d0                	or     %edx,%eax
  8009e0:	c1 e9 02             	shr    $0x2,%ecx
  8009e3:	fc                   	cld    
  8009e4:	f3 ab                	rep stos %eax,%es:(%edi)
  8009e6:	eb 06                	jmp    8009ee <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8009e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009eb:	fc                   	cld    
  8009ec:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8009ee:	89 f8                	mov    %edi,%eax
  8009f0:	5b                   	pop    %ebx
  8009f1:	5e                   	pop    %esi
  8009f2:	5f                   	pop    %edi
  8009f3:	5d                   	pop    %ebp
  8009f4:	c3                   	ret    

008009f5 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8009f5:	55                   	push   %ebp
  8009f6:	89 e5                	mov    %esp,%ebp
  8009f8:	57                   	push   %edi
  8009f9:	56                   	push   %esi
  8009fa:	8b 45 08             	mov    0x8(%ebp),%eax
  8009fd:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a00:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800a03:	39 c6                	cmp    %eax,%esi
  800a05:	73 33                	jae    800a3a <memmove+0x45>
  800a07:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800a0a:	39 d0                	cmp    %edx,%eax
  800a0c:	73 2c                	jae    800a3a <memmove+0x45>
		s += n;
		d += n;
  800a0e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a11:	89 d6                	mov    %edx,%esi
  800a13:	09 fe                	or     %edi,%esi
  800a15:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800a1b:	75 13                	jne    800a30 <memmove+0x3b>
  800a1d:	f6 c1 03             	test   $0x3,%cl
  800a20:	75 0e                	jne    800a30 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800a22:	83 ef 04             	sub    $0x4,%edi
  800a25:	8d 72 fc             	lea    -0x4(%edx),%esi
  800a28:	c1 e9 02             	shr    $0x2,%ecx
  800a2b:	fd                   	std    
  800a2c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a2e:	eb 07                	jmp    800a37 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800a30:	4f                   	dec    %edi
  800a31:	8d 72 ff             	lea    -0x1(%edx),%esi
  800a34:	fd                   	std    
  800a35:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800a37:	fc                   	cld    
  800a38:	eb 1d                	jmp    800a57 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a3a:	89 f2                	mov    %esi,%edx
  800a3c:	09 c2                	or     %eax,%edx
  800a3e:	f6 c2 03             	test   $0x3,%dl
  800a41:	75 0f                	jne    800a52 <memmove+0x5d>
  800a43:	f6 c1 03             	test   $0x3,%cl
  800a46:	75 0a                	jne    800a52 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800a48:	c1 e9 02             	shr    $0x2,%ecx
  800a4b:	89 c7                	mov    %eax,%edi
  800a4d:	fc                   	cld    
  800a4e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a50:	eb 05                	jmp    800a57 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800a52:	89 c7                	mov    %eax,%edi
  800a54:	fc                   	cld    
  800a55:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800a57:	5e                   	pop    %esi
  800a58:	5f                   	pop    %edi
  800a59:	5d                   	pop    %ebp
  800a5a:	c3                   	ret    

00800a5b <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800a5b:	55                   	push   %ebp
  800a5c:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800a5e:	ff 75 10             	pushl  0x10(%ebp)
  800a61:	ff 75 0c             	pushl  0xc(%ebp)
  800a64:	ff 75 08             	pushl  0x8(%ebp)
  800a67:	e8 89 ff ff ff       	call   8009f5 <memmove>
}
  800a6c:	c9                   	leave  
  800a6d:	c3                   	ret    

00800a6e <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800a6e:	55                   	push   %ebp
  800a6f:	89 e5                	mov    %esp,%ebp
  800a71:	56                   	push   %esi
  800a72:	53                   	push   %ebx
  800a73:	8b 45 08             	mov    0x8(%ebp),%eax
  800a76:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a79:	89 c6                	mov    %eax,%esi
  800a7b:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a7e:	eb 14                	jmp    800a94 <memcmp+0x26>
		if (*s1 != *s2)
  800a80:	8a 08                	mov    (%eax),%cl
  800a82:	8a 1a                	mov    (%edx),%bl
  800a84:	38 d9                	cmp    %bl,%cl
  800a86:	74 0a                	je     800a92 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800a88:	0f b6 c1             	movzbl %cl,%eax
  800a8b:	0f b6 db             	movzbl %bl,%ebx
  800a8e:	29 d8                	sub    %ebx,%eax
  800a90:	eb 0b                	jmp    800a9d <memcmp+0x2f>
		s1++, s2++;
  800a92:	40                   	inc    %eax
  800a93:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a94:	39 f0                	cmp    %esi,%eax
  800a96:	75 e8                	jne    800a80 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800a98:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a9d:	5b                   	pop    %ebx
  800a9e:	5e                   	pop    %esi
  800a9f:	5d                   	pop    %ebp
  800aa0:	c3                   	ret    

00800aa1 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800aa1:	55                   	push   %ebp
  800aa2:	89 e5                	mov    %esp,%ebp
  800aa4:	53                   	push   %ebx
  800aa5:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800aa8:	89 c1                	mov    %eax,%ecx
  800aaa:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800aad:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ab1:	eb 08                	jmp    800abb <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800ab3:	0f b6 10             	movzbl (%eax),%edx
  800ab6:	39 da                	cmp    %ebx,%edx
  800ab8:	74 05                	je     800abf <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800aba:	40                   	inc    %eax
  800abb:	39 c8                	cmp    %ecx,%eax
  800abd:	72 f4                	jb     800ab3 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800abf:	5b                   	pop    %ebx
  800ac0:	5d                   	pop    %ebp
  800ac1:	c3                   	ret    

00800ac2 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800ac2:	55                   	push   %ebp
  800ac3:	89 e5                	mov    %esp,%ebp
  800ac5:	57                   	push   %edi
  800ac6:	56                   	push   %esi
  800ac7:	53                   	push   %ebx
  800ac8:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800acb:	eb 01                	jmp    800ace <strtol+0xc>
		s++;
  800acd:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800ace:	8a 01                	mov    (%ecx),%al
  800ad0:	3c 20                	cmp    $0x20,%al
  800ad2:	74 f9                	je     800acd <strtol+0xb>
  800ad4:	3c 09                	cmp    $0x9,%al
  800ad6:	74 f5                	je     800acd <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800ad8:	3c 2b                	cmp    $0x2b,%al
  800ada:	75 08                	jne    800ae4 <strtol+0x22>
		s++;
  800adc:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800add:	bf 00 00 00 00       	mov    $0x0,%edi
  800ae2:	eb 11                	jmp    800af5 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800ae4:	3c 2d                	cmp    $0x2d,%al
  800ae6:	75 08                	jne    800af0 <strtol+0x2e>
		s++, neg = 1;
  800ae8:	41                   	inc    %ecx
  800ae9:	bf 01 00 00 00       	mov    $0x1,%edi
  800aee:	eb 05                	jmp    800af5 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800af0:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800af5:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800af9:	0f 84 87 00 00 00    	je     800b86 <strtol+0xc4>
  800aff:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800b03:	75 27                	jne    800b2c <strtol+0x6a>
  800b05:	80 39 30             	cmpb   $0x30,(%ecx)
  800b08:	75 22                	jne    800b2c <strtol+0x6a>
  800b0a:	e9 88 00 00 00       	jmp    800b97 <strtol+0xd5>
		s += 2, base = 16;
  800b0f:	83 c1 02             	add    $0x2,%ecx
  800b12:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800b19:	eb 11                	jmp    800b2c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800b1b:	41                   	inc    %ecx
  800b1c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800b23:	eb 07                	jmp    800b2c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800b25:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800b2c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800b31:	8a 11                	mov    (%ecx),%dl
  800b33:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800b36:	80 fb 09             	cmp    $0x9,%bl
  800b39:	77 08                	ja     800b43 <strtol+0x81>
			dig = *s - '0';
  800b3b:	0f be d2             	movsbl %dl,%edx
  800b3e:	83 ea 30             	sub    $0x30,%edx
  800b41:	eb 22                	jmp    800b65 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800b43:	8d 72 9f             	lea    -0x61(%edx),%esi
  800b46:	89 f3                	mov    %esi,%ebx
  800b48:	80 fb 19             	cmp    $0x19,%bl
  800b4b:	77 08                	ja     800b55 <strtol+0x93>
			dig = *s - 'a' + 10;
  800b4d:	0f be d2             	movsbl %dl,%edx
  800b50:	83 ea 57             	sub    $0x57,%edx
  800b53:	eb 10                	jmp    800b65 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800b55:	8d 72 bf             	lea    -0x41(%edx),%esi
  800b58:	89 f3                	mov    %esi,%ebx
  800b5a:	80 fb 19             	cmp    $0x19,%bl
  800b5d:	77 14                	ja     800b73 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800b5f:	0f be d2             	movsbl %dl,%edx
  800b62:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800b65:	3b 55 10             	cmp    0x10(%ebp),%edx
  800b68:	7d 09                	jge    800b73 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800b6a:	41                   	inc    %ecx
  800b6b:	0f af 45 10          	imul   0x10(%ebp),%eax
  800b6f:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800b71:	eb be                	jmp    800b31 <strtol+0x6f>

	if (endptr)
  800b73:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800b77:	74 05                	je     800b7e <strtol+0xbc>
		*endptr = (char *) s;
  800b79:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b7c:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800b7e:	85 ff                	test   %edi,%edi
  800b80:	74 21                	je     800ba3 <strtol+0xe1>
  800b82:	f7 d8                	neg    %eax
  800b84:	eb 1d                	jmp    800ba3 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b86:	80 39 30             	cmpb   $0x30,(%ecx)
  800b89:	75 9a                	jne    800b25 <strtol+0x63>
  800b8b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b8f:	0f 84 7a ff ff ff    	je     800b0f <strtol+0x4d>
  800b95:	eb 84                	jmp    800b1b <strtol+0x59>
  800b97:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b9b:	0f 84 6e ff ff ff    	je     800b0f <strtol+0x4d>
  800ba1:	eb 89                	jmp    800b2c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800ba3:	5b                   	pop    %ebx
  800ba4:	5e                   	pop    %esi
  800ba5:	5f                   	pop    %edi
  800ba6:	5d                   	pop    %ebp
  800ba7:	c3                   	ret    

00800ba8 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800ba8:	55                   	push   %ebp
  800ba9:	89 e5                	mov    %esp,%ebp
  800bab:	57                   	push   %edi
  800bac:	56                   	push   %esi
  800bad:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bae:	b8 00 00 00 00       	mov    $0x0,%eax
  800bb3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bb6:	8b 55 08             	mov    0x8(%ebp),%edx
  800bb9:	89 c3                	mov    %eax,%ebx
  800bbb:	89 c7                	mov    %eax,%edi
  800bbd:	89 c6                	mov    %eax,%esi
  800bbf:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800bc1:	5b                   	pop    %ebx
  800bc2:	5e                   	pop    %esi
  800bc3:	5f                   	pop    %edi
  800bc4:	5d                   	pop    %ebp
  800bc5:	c3                   	ret    

00800bc6 <sys_cgetc>:

int
sys_cgetc(void)
{
  800bc6:	55                   	push   %ebp
  800bc7:	89 e5                	mov    %esp,%ebp
  800bc9:	57                   	push   %edi
  800bca:	56                   	push   %esi
  800bcb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bcc:	ba 00 00 00 00       	mov    $0x0,%edx
  800bd1:	b8 01 00 00 00       	mov    $0x1,%eax
  800bd6:	89 d1                	mov    %edx,%ecx
  800bd8:	89 d3                	mov    %edx,%ebx
  800bda:	89 d7                	mov    %edx,%edi
  800bdc:	89 d6                	mov    %edx,%esi
  800bde:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800be0:	5b                   	pop    %ebx
  800be1:	5e                   	pop    %esi
  800be2:	5f                   	pop    %edi
  800be3:	5d                   	pop    %ebp
  800be4:	c3                   	ret    

00800be5 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800be5:	55                   	push   %ebp
  800be6:	89 e5                	mov    %esp,%ebp
  800be8:	57                   	push   %edi
  800be9:	56                   	push   %esi
  800bea:	53                   	push   %ebx
  800beb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bee:	b9 00 00 00 00       	mov    $0x0,%ecx
  800bf3:	b8 03 00 00 00       	mov    $0x3,%eax
  800bf8:	8b 55 08             	mov    0x8(%ebp),%edx
  800bfb:	89 cb                	mov    %ecx,%ebx
  800bfd:	89 cf                	mov    %ecx,%edi
  800bff:	89 ce                	mov    %ecx,%esi
  800c01:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c03:	85 c0                	test   %eax,%eax
  800c05:	7e 17                	jle    800c1e <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c07:	83 ec 0c             	sub    $0xc,%esp
  800c0a:	50                   	push   %eax
  800c0b:	6a 03                	push   $0x3
  800c0d:	68 a4 13 80 00       	push   $0x8013a4
  800c12:	6a 23                	push   $0x23
  800c14:	68 c1 13 80 00       	push   $0x8013c1
  800c19:	e8 26 f6 ff ff       	call   800244 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800c1e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c21:	5b                   	pop    %ebx
  800c22:	5e                   	pop    %esi
  800c23:	5f                   	pop    %edi
  800c24:	5d                   	pop    %ebp
  800c25:	c3                   	ret    

00800c26 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800c26:	55                   	push   %ebp
  800c27:	89 e5                	mov    %esp,%ebp
  800c29:	57                   	push   %edi
  800c2a:	56                   	push   %esi
  800c2b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2c:	ba 00 00 00 00       	mov    $0x0,%edx
  800c31:	b8 02 00 00 00       	mov    $0x2,%eax
  800c36:	89 d1                	mov    %edx,%ecx
  800c38:	89 d3                	mov    %edx,%ebx
  800c3a:	89 d7                	mov    %edx,%edi
  800c3c:	89 d6                	mov    %edx,%esi
  800c3e:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800c40:	5b                   	pop    %ebx
  800c41:	5e                   	pop    %esi
  800c42:	5f                   	pop    %edi
  800c43:	5d                   	pop    %ebp
  800c44:	c3                   	ret    

00800c45 <sys_yield>:

void
sys_yield(void)
{
  800c45:	55                   	push   %ebp
  800c46:	89 e5                	mov    %esp,%ebp
  800c48:	57                   	push   %edi
  800c49:	56                   	push   %esi
  800c4a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c4b:	ba 00 00 00 00       	mov    $0x0,%edx
  800c50:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c55:	89 d1                	mov    %edx,%ecx
  800c57:	89 d3                	mov    %edx,%ebx
  800c59:	89 d7                	mov    %edx,%edi
  800c5b:	89 d6                	mov    %edx,%esi
  800c5d:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800c5f:	5b                   	pop    %ebx
  800c60:	5e                   	pop    %esi
  800c61:	5f                   	pop    %edi
  800c62:	5d                   	pop    %ebp
  800c63:	c3                   	ret    

00800c64 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800c64:	55                   	push   %ebp
  800c65:	89 e5                	mov    %esp,%ebp
  800c67:	57                   	push   %edi
  800c68:	56                   	push   %esi
  800c69:	53                   	push   %ebx
  800c6a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c6d:	be 00 00 00 00       	mov    $0x0,%esi
  800c72:	b8 04 00 00 00       	mov    $0x4,%eax
  800c77:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c7a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c7d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c80:	89 f7                	mov    %esi,%edi
  800c82:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c84:	85 c0                	test   %eax,%eax
  800c86:	7e 17                	jle    800c9f <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c88:	83 ec 0c             	sub    $0xc,%esp
  800c8b:	50                   	push   %eax
  800c8c:	6a 04                	push   $0x4
  800c8e:	68 a4 13 80 00       	push   $0x8013a4
  800c93:	6a 23                	push   $0x23
  800c95:	68 c1 13 80 00       	push   $0x8013c1
  800c9a:	e8 a5 f5 ff ff       	call   800244 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800c9f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ca2:	5b                   	pop    %ebx
  800ca3:	5e                   	pop    %esi
  800ca4:	5f                   	pop    %edi
  800ca5:	5d                   	pop    %ebp
  800ca6:	c3                   	ret    

00800ca7 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800ca7:	55                   	push   %ebp
  800ca8:	89 e5                	mov    %esp,%ebp
  800caa:	57                   	push   %edi
  800cab:	56                   	push   %esi
  800cac:	53                   	push   %ebx
  800cad:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cb0:	b8 05 00 00 00       	mov    $0x5,%eax
  800cb5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cb8:	8b 55 08             	mov    0x8(%ebp),%edx
  800cbb:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cbe:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cc1:	8b 75 18             	mov    0x18(%ebp),%esi
  800cc4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cc6:	85 c0                	test   %eax,%eax
  800cc8:	7e 17                	jle    800ce1 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cca:	83 ec 0c             	sub    $0xc,%esp
  800ccd:	50                   	push   %eax
  800cce:	6a 05                	push   $0x5
  800cd0:	68 a4 13 80 00       	push   $0x8013a4
  800cd5:	6a 23                	push   $0x23
  800cd7:	68 c1 13 80 00       	push   $0x8013c1
  800cdc:	e8 63 f5 ff ff       	call   800244 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800ce1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ce4:	5b                   	pop    %ebx
  800ce5:	5e                   	pop    %esi
  800ce6:	5f                   	pop    %edi
  800ce7:	5d                   	pop    %ebp
  800ce8:	c3                   	ret    

00800ce9 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800ce9:	55                   	push   %ebp
  800cea:	89 e5                	mov    %esp,%ebp
  800cec:	57                   	push   %edi
  800ced:	56                   	push   %esi
  800cee:	53                   	push   %ebx
  800cef:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cf2:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cf7:	b8 06 00 00 00       	mov    $0x6,%eax
  800cfc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cff:	8b 55 08             	mov    0x8(%ebp),%edx
  800d02:	89 df                	mov    %ebx,%edi
  800d04:	89 de                	mov    %ebx,%esi
  800d06:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d08:	85 c0                	test   %eax,%eax
  800d0a:	7e 17                	jle    800d23 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d0c:	83 ec 0c             	sub    $0xc,%esp
  800d0f:	50                   	push   %eax
  800d10:	6a 06                	push   $0x6
  800d12:	68 a4 13 80 00       	push   $0x8013a4
  800d17:	6a 23                	push   $0x23
  800d19:	68 c1 13 80 00       	push   $0x8013c1
  800d1e:	e8 21 f5 ff ff       	call   800244 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800d23:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d26:	5b                   	pop    %ebx
  800d27:	5e                   	pop    %esi
  800d28:	5f                   	pop    %edi
  800d29:	5d                   	pop    %ebp
  800d2a:	c3                   	ret    

00800d2b <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800d2b:	55                   	push   %ebp
  800d2c:	89 e5                	mov    %esp,%ebp
  800d2e:	57                   	push   %edi
  800d2f:	56                   	push   %esi
  800d30:	53                   	push   %ebx
  800d31:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d34:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d39:	b8 08 00 00 00       	mov    $0x8,%eax
  800d3e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d41:	8b 55 08             	mov    0x8(%ebp),%edx
  800d44:	89 df                	mov    %ebx,%edi
  800d46:	89 de                	mov    %ebx,%esi
  800d48:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d4a:	85 c0                	test   %eax,%eax
  800d4c:	7e 17                	jle    800d65 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d4e:	83 ec 0c             	sub    $0xc,%esp
  800d51:	50                   	push   %eax
  800d52:	6a 08                	push   $0x8
  800d54:	68 a4 13 80 00       	push   $0x8013a4
  800d59:	6a 23                	push   $0x23
  800d5b:	68 c1 13 80 00       	push   $0x8013c1
  800d60:	e8 df f4 ff ff       	call   800244 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800d65:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d68:	5b                   	pop    %ebx
  800d69:	5e                   	pop    %esi
  800d6a:	5f                   	pop    %edi
  800d6b:	5d                   	pop    %ebp
  800d6c:	c3                   	ret    

00800d6d <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800d6d:	55                   	push   %ebp
  800d6e:	89 e5                	mov    %esp,%ebp
  800d70:	57                   	push   %edi
  800d71:	56                   	push   %esi
  800d72:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d73:	ba 00 00 00 00       	mov    $0x0,%edx
  800d78:	b8 0b 00 00 00       	mov    $0xb,%eax
  800d7d:	89 d1                	mov    %edx,%ecx
  800d7f:	89 d3                	mov    %edx,%ebx
  800d81:	89 d7                	mov    %edx,%edi
  800d83:	89 d6                	mov    %edx,%esi
  800d85:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800d87:	5b                   	pop    %ebx
  800d88:	5e                   	pop    %esi
  800d89:	5f                   	pop    %edi
  800d8a:	5d                   	pop    %ebp
  800d8b:	c3                   	ret    

00800d8c <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800d8c:	55                   	push   %ebp
  800d8d:	89 e5                	mov    %esp,%ebp
  800d8f:	57                   	push   %edi
  800d90:	56                   	push   %esi
  800d91:	53                   	push   %ebx
  800d92:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d95:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d9a:	b8 09 00 00 00       	mov    $0x9,%eax
  800d9f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800da2:	8b 55 08             	mov    0x8(%ebp),%edx
  800da5:	89 df                	mov    %ebx,%edi
  800da7:	89 de                	mov    %ebx,%esi
  800da9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800dab:	85 c0                	test   %eax,%eax
  800dad:	7e 17                	jle    800dc6 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800daf:	83 ec 0c             	sub    $0xc,%esp
  800db2:	50                   	push   %eax
  800db3:	6a 09                	push   $0x9
  800db5:	68 a4 13 80 00       	push   $0x8013a4
  800dba:	6a 23                	push   $0x23
  800dbc:	68 c1 13 80 00       	push   $0x8013c1
  800dc1:	e8 7e f4 ff ff       	call   800244 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800dc6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dc9:	5b                   	pop    %ebx
  800dca:	5e                   	pop    %esi
  800dcb:	5f                   	pop    %edi
  800dcc:	5d                   	pop    %ebp
  800dcd:	c3                   	ret    

00800dce <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800dce:	55                   	push   %ebp
  800dcf:	89 e5                	mov    %esp,%ebp
  800dd1:	57                   	push   %edi
  800dd2:	56                   	push   %esi
  800dd3:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dd4:	be 00 00 00 00       	mov    $0x0,%esi
  800dd9:	b8 0c 00 00 00       	mov    $0xc,%eax
  800dde:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800de1:	8b 55 08             	mov    0x8(%ebp),%edx
  800de4:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800de7:	8b 7d 14             	mov    0x14(%ebp),%edi
  800dea:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800dec:	5b                   	pop    %ebx
  800ded:	5e                   	pop    %esi
  800dee:	5f                   	pop    %edi
  800def:	5d                   	pop    %ebp
  800df0:	c3                   	ret    

00800df1 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800df1:	55                   	push   %ebp
  800df2:	89 e5                	mov    %esp,%ebp
  800df4:	57                   	push   %edi
  800df5:	56                   	push   %esi
  800df6:	53                   	push   %ebx
  800df7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dfa:	b9 00 00 00 00       	mov    $0x0,%ecx
  800dff:	b8 0d 00 00 00       	mov    $0xd,%eax
  800e04:	8b 55 08             	mov    0x8(%ebp),%edx
  800e07:	89 cb                	mov    %ecx,%ebx
  800e09:	89 cf                	mov    %ecx,%edi
  800e0b:	89 ce                	mov    %ecx,%esi
  800e0d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e0f:	85 c0                	test   %eax,%eax
  800e11:	7e 17                	jle    800e2a <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e13:	83 ec 0c             	sub    $0xc,%esp
  800e16:	50                   	push   %eax
  800e17:	6a 0d                	push   $0xd
  800e19:	68 a4 13 80 00       	push   $0x8013a4
  800e1e:	6a 23                	push   $0x23
  800e20:	68 c1 13 80 00       	push   $0x8013c1
  800e25:	e8 1a f4 ff ff       	call   800244 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800e2a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e2d:	5b                   	pop    %ebx
  800e2e:	5e                   	pop    %esi
  800e2f:	5f                   	pop    %edi
  800e30:	5d                   	pop    %ebp
  800e31:	c3                   	ret    
  800e32:	66 90                	xchg   %ax,%ax

00800e34 <__udivdi3>:
  800e34:	55                   	push   %ebp
  800e35:	57                   	push   %edi
  800e36:	56                   	push   %esi
  800e37:	53                   	push   %ebx
  800e38:	83 ec 1c             	sub    $0x1c,%esp
  800e3b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800e3f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800e43:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e47:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800e4b:	89 ca                	mov    %ecx,%edx
  800e4d:	89 f8                	mov    %edi,%eax
  800e4f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800e53:	85 f6                	test   %esi,%esi
  800e55:	75 2d                	jne    800e84 <__udivdi3+0x50>
  800e57:	39 cf                	cmp    %ecx,%edi
  800e59:	77 65                	ja     800ec0 <__udivdi3+0x8c>
  800e5b:	89 fd                	mov    %edi,%ebp
  800e5d:	85 ff                	test   %edi,%edi
  800e5f:	75 0b                	jne    800e6c <__udivdi3+0x38>
  800e61:	b8 01 00 00 00       	mov    $0x1,%eax
  800e66:	31 d2                	xor    %edx,%edx
  800e68:	f7 f7                	div    %edi
  800e6a:	89 c5                	mov    %eax,%ebp
  800e6c:	31 d2                	xor    %edx,%edx
  800e6e:	89 c8                	mov    %ecx,%eax
  800e70:	f7 f5                	div    %ebp
  800e72:	89 c1                	mov    %eax,%ecx
  800e74:	89 d8                	mov    %ebx,%eax
  800e76:	f7 f5                	div    %ebp
  800e78:	89 cf                	mov    %ecx,%edi
  800e7a:	89 fa                	mov    %edi,%edx
  800e7c:	83 c4 1c             	add    $0x1c,%esp
  800e7f:	5b                   	pop    %ebx
  800e80:	5e                   	pop    %esi
  800e81:	5f                   	pop    %edi
  800e82:	5d                   	pop    %ebp
  800e83:	c3                   	ret    
  800e84:	39 ce                	cmp    %ecx,%esi
  800e86:	77 28                	ja     800eb0 <__udivdi3+0x7c>
  800e88:	0f bd fe             	bsr    %esi,%edi
  800e8b:	83 f7 1f             	xor    $0x1f,%edi
  800e8e:	75 40                	jne    800ed0 <__udivdi3+0x9c>
  800e90:	39 ce                	cmp    %ecx,%esi
  800e92:	72 0a                	jb     800e9e <__udivdi3+0x6a>
  800e94:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800e98:	0f 87 9e 00 00 00    	ja     800f3c <__udivdi3+0x108>
  800e9e:	b8 01 00 00 00       	mov    $0x1,%eax
  800ea3:	89 fa                	mov    %edi,%edx
  800ea5:	83 c4 1c             	add    $0x1c,%esp
  800ea8:	5b                   	pop    %ebx
  800ea9:	5e                   	pop    %esi
  800eaa:	5f                   	pop    %edi
  800eab:	5d                   	pop    %ebp
  800eac:	c3                   	ret    
  800ead:	8d 76 00             	lea    0x0(%esi),%esi
  800eb0:	31 ff                	xor    %edi,%edi
  800eb2:	31 c0                	xor    %eax,%eax
  800eb4:	89 fa                	mov    %edi,%edx
  800eb6:	83 c4 1c             	add    $0x1c,%esp
  800eb9:	5b                   	pop    %ebx
  800eba:	5e                   	pop    %esi
  800ebb:	5f                   	pop    %edi
  800ebc:	5d                   	pop    %ebp
  800ebd:	c3                   	ret    
  800ebe:	66 90                	xchg   %ax,%ax
  800ec0:	89 d8                	mov    %ebx,%eax
  800ec2:	f7 f7                	div    %edi
  800ec4:	31 ff                	xor    %edi,%edi
  800ec6:	89 fa                	mov    %edi,%edx
  800ec8:	83 c4 1c             	add    $0x1c,%esp
  800ecb:	5b                   	pop    %ebx
  800ecc:	5e                   	pop    %esi
  800ecd:	5f                   	pop    %edi
  800ece:	5d                   	pop    %ebp
  800ecf:	c3                   	ret    
  800ed0:	bd 20 00 00 00       	mov    $0x20,%ebp
  800ed5:	89 eb                	mov    %ebp,%ebx
  800ed7:	29 fb                	sub    %edi,%ebx
  800ed9:	89 f9                	mov    %edi,%ecx
  800edb:	d3 e6                	shl    %cl,%esi
  800edd:	89 c5                	mov    %eax,%ebp
  800edf:	88 d9                	mov    %bl,%cl
  800ee1:	d3 ed                	shr    %cl,%ebp
  800ee3:	89 e9                	mov    %ebp,%ecx
  800ee5:	09 f1                	or     %esi,%ecx
  800ee7:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800eeb:	89 f9                	mov    %edi,%ecx
  800eed:	d3 e0                	shl    %cl,%eax
  800eef:	89 c5                	mov    %eax,%ebp
  800ef1:	89 d6                	mov    %edx,%esi
  800ef3:	88 d9                	mov    %bl,%cl
  800ef5:	d3 ee                	shr    %cl,%esi
  800ef7:	89 f9                	mov    %edi,%ecx
  800ef9:	d3 e2                	shl    %cl,%edx
  800efb:	8b 44 24 08          	mov    0x8(%esp),%eax
  800eff:	88 d9                	mov    %bl,%cl
  800f01:	d3 e8                	shr    %cl,%eax
  800f03:	09 c2                	or     %eax,%edx
  800f05:	89 d0                	mov    %edx,%eax
  800f07:	89 f2                	mov    %esi,%edx
  800f09:	f7 74 24 0c          	divl   0xc(%esp)
  800f0d:	89 d6                	mov    %edx,%esi
  800f0f:	89 c3                	mov    %eax,%ebx
  800f11:	f7 e5                	mul    %ebp
  800f13:	39 d6                	cmp    %edx,%esi
  800f15:	72 19                	jb     800f30 <__udivdi3+0xfc>
  800f17:	74 0b                	je     800f24 <__udivdi3+0xf0>
  800f19:	89 d8                	mov    %ebx,%eax
  800f1b:	31 ff                	xor    %edi,%edi
  800f1d:	e9 58 ff ff ff       	jmp    800e7a <__udivdi3+0x46>
  800f22:	66 90                	xchg   %ax,%ax
  800f24:	8b 54 24 08          	mov    0x8(%esp),%edx
  800f28:	89 f9                	mov    %edi,%ecx
  800f2a:	d3 e2                	shl    %cl,%edx
  800f2c:	39 c2                	cmp    %eax,%edx
  800f2e:	73 e9                	jae    800f19 <__udivdi3+0xe5>
  800f30:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800f33:	31 ff                	xor    %edi,%edi
  800f35:	e9 40 ff ff ff       	jmp    800e7a <__udivdi3+0x46>
  800f3a:	66 90                	xchg   %ax,%ax
  800f3c:	31 c0                	xor    %eax,%eax
  800f3e:	e9 37 ff ff ff       	jmp    800e7a <__udivdi3+0x46>
  800f43:	90                   	nop

00800f44 <__umoddi3>:
  800f44:	55                   	push   %ebp
  800f45:	57                   	push   %edi
  800f46:	56                   	push   %esi
  800f47:	53                   	push   %ebx
  800f48:	83 ec 1c             	sub    $0x1c,%esp
  800f4b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800f4f:	8b 74 24 34          	mov    0x34(%esp),%esi
  800f53:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800f57:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800f5b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800f5f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800f63:	89 f3                	mov    %esi,%ebx
  800f65:	89 fa                	mov    %edi,%edx
  800f67:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f6b:	89 34 24             	mov    %esi,(%esp)
  800f6e:	85 c0                	test   %eax,%eax
  800f70:	75 1a                	jne    800f8c <__umoddi3+0x48>
  800f72:	39 f7                	cmp    %esi,%edi
  800f74:	0f 86 a2 00 00 00    	jbe    80101c <__umoddi3+0xd8>
  800f7a:	89 c8                	mov    %ecx,%eax
  800f7c:	89 f2                	mov    %esi,%edx
  800f7e:	f7 f7                	div    %edi
  800f80:	89 d0                	mov    %edx,%eax
  800f82:	31 d2                	xor    %edx,%edx
  800f84:	83 c4 1c             	add    $0x1c,%esp
  800f87:	5b                   	pop    %ebx
  800f88:	5e                   	pop    %esi
  800f89:	5f                   	pop    %edi
  800f8a:	5d                   	pop    %ebp
  800f8b:	c3                   	ret    
  800f8c:	39 f0                	cmp    %esi,%eax
  800f8e:	0f 87 ac 00 00 00    	ja     801040 <__umoddi3+0xfc>
  800f94:	0f bd e8             	bsr    %eax,%ebp
  800f97:	83 f5 1f             	xor    $0x1f,%ebp
  800f9a:	0f 84 ac 00 00 00    	je     80104c <__umoddi3+0x108>
  800fa0:	bf 20 00 00 00       	mov    $0x20,%edi
  800fa5:	29 ef                	sub    %ebp,%edi
  800fa7:	89 fe                	mov    %edi,%esi
  800fa9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800fad:	89 e9                	mov    %ebp,%ecx
  800faf:	d3 e0                	shl    %cl,%eax
  800fb1:	89 d7                	mov    %edx,%edi
  800fb3:	89 f1                	mov    %esi,%ecx
  800fb5:	d3 ef                	shr    %cl,%edi
  800fb7:	09 c7                	or     %eax,%edi
  800fb9:	89 e9                	mov    %ebp,%ecx
  800fbb:	d3 e2                	shl    %cl,%edx
  800fbd:	89 14 24             	mov    %edx,(%esp)
  800fc0:	89 d8                	mov    %ebx,%eax
  800fc2:	d3 e0                	shl    %cl,%eax
  800fc4:	89 c2                	mov    %eax,%edx
  800fc6:	8b 44 24 08          	mov    0x8(%esp),%eax
  800fca:	d3 e0                	shl    %cl,%eax
  800fcc:	89 44 24 04          	mov    %eax,0x4(%esp)
  800fd0:	8b 44 24 08          	mov    0x8(%esp),%eax
  800fd4:	89 f1                	mov    %esi,%ecx
  800fd6:	d3 e8                	shr    %cl,%eax
  800fd8:	09 d0                	or     %edx,%eax
  800fda:	d3 eb                	shr    %cl,%ebx
  800fdc:	89 da                	mov    %ebx,%edx
  800fde:	f7 f7                	div    %edi
  800fe0:	89 d3                	mov    %edx,%ebx
  800fe2:	f7 24 24             	mull   (%esp)
  800fe5:	89 c6                	mov    %eax,%esi
  800fe7:	89 d1                	mov    %edx,%ecx
  800fe9:	39 d3                	cmp    %edx,%ebx
  800feb:	0f 82 87 00 00 00    	jb     801078 <__umoddi3+0x134>
  800ff1:	0f 84 91 00 00 00    	je     801088 <__umoddi3+0x144>
  800ff7:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ffb:	29 f2                	sub    %esi,%edx
  800ffd:	19 cb                	sbb    %ecx,%ebx
  800fff:	89 d8                	mov    %ebx,%eax
  801001:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801005:	d3 e0                	shl    %cl,%eax
  801007:	89 e9                	mov    %ebp,%ecx
  801009:	d3 ea                	shr    %cl,%edx
  80100b:	09 d0                	or     %edx,%eax
  80100d:	89 e9                	mov    %ebp,%ecx
  80100f:	d3 eb                	shr    %cl,%ebx
  801011:	89 da                	mov    %ebx,%edx
  801013:	83 c4 1c             	add    $0x1c,%esp
  801016:	5b                   	pop    %ebx
  801017:	5e                   	pop    %esi
  801018:	5f                   	pop    %edi
  801019:	5d                   	pop    %ebp
  80101a:	c3                   	ret    
  80101b:	90                   	nop
  80101c:	89 fd                	mov    %edi,%ebp
  80101e:	85 ff                	test   %edi,%edi
  801020:	75 0b                	jne    80102d <__umoddi3+0xe9>
  801022:	b8 01 00 00 00       	mov    $0x1,%eax
  801027:	31 d2                	xor    %edx,%edx
  801029:	f7 f7                	div    %edi
  80102b:	89 c5                	mov    %eax,%ebp
  80102d:	89 f0                	mov    %esi,%eax
  80102f:	31 d2                	xor    %edx,%edx
  801031:	f7 f5                	div    %ebp
  801033:	89 c8                	mov    %ecx,%eax
  801035:	f7 f5                	div    %ebp
  801037:	89 d0                	mov    %edx,%eax
  801039:	e9 44 ff ff ff       	jmp    800f82 <__umoddi3+0x3e>
  80103e:	66 90                	xchg   %ax,%ax
  801040:	89 c8                	mov    %ecx,%eax
  801042:	89 f2                	mov    %esi,%edx
  801044:	83 c4 1c             	add    $0x1c,%esp
  801047:	5b                   	pop    %ebx
  801048:	5e                   	pop    %esi
  801049:	5f                   	pop    %edi
  80104a:	5d                   	pop    %ebp
  80104b:	c3                   	ret    
  80104c:	3b 04 24             	cmp    (%esp),%eax
  80104f:	72 06                	jb     801057 <__umoddi3+0x113>
  801051:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801055:	77 0f                	ja     801066 <__umoddi3+0x122>
  801057:	89 f2                	mov    %esi,%edx
  801059:	29 f9                	sub    %edi,%ecx
  80105b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  80105f:	89 14 24             	mov    %edx,(%esp)
  801062:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801066:	8b 44 24 04          	mov    0x4(%esp),%eax
  80106a:	8b 14 24             	mov    (%esp),%edx
  80106d:	83 c4 1c             	add    $0x1c,%esp
  801070:	5b                   	pop    %ebx
  801071:	5e                   	pop    %esi
  801072:	5f                   	pop    %edi
  801073:	5d                   	pop    %ebp
  801074:	c3                   	ret    
  801075:	8d 76 00             	lea    0x0(%esi),%esi
  801078:	2b 04 24             	sub    (%esp),%eax
  80107b:	19 fa                	sbb    %edi,%edx
  80107d:	89 d1                	mov    %edx,%ecx
  80107f:	89 c6                	mov    %eax,%esi
  801081:	e9 71 ff ff ff       	jmp    800ff7 <__umoddi3+0xb3>
  801086:	66 90                	xchg   %ax,%ax
  801088:	39 44 24 04          	cmp    %eax,0x4(%esp)
  80108c:	72 ea                	jb     801078 <__umoddi3+0x134>
  80108e:	89 d9                	mov    %ebx,%ecx
  801090:	e9 62 ff ff ff       	jmp    800ff7 <__umoddi3+0xb3>
