
obj/user/faultwritekernel:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 11 00 00 00       	call   800042 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	*(unsigned*)0xf0100000 = 0;
  800036:	c7 05 00 00 10 f0 00 	movl   $0x0,0xf0100000
  80003d:	00 00 00 
}
  800040:	5d                   	pop    %ebp
  800041:	c3                   	ret    

00800042 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800042:	55                   	push   %ebp
  800043:	89 e5                	mov    %esp,%ebp
  800045:	56                   	push   %esi
  800046:	53                   	push   %ebx
  800047:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004a:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80004d:	e8 cf 00 00 00       	call   800121 <sys_getenvid>
  800052:	25 ff 03 00 00       	and    $0x3ff,%eax
  800057:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80005e:	c1 e0 07             	shl    $0x7,%eax
  800061:	29 d0                	sub    %edx,%eax
  800063:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800068:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80006d:	85 db                	test   %ebx,%ebx
  80006f:	7e 07                	jle    800078 <libmain+0x36>
		binaryname = argv[0];
  800071:	8b 06                	mov    (%esi),%eax
  800073:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800078:	83 ec 08             	sub    $0x8,%esp
  80007b:	56                   	push   %esi
  80007c:	53                   	push   %ebx
  80007d:	e8 b1 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800082:	e8 0a 00 00 00       	call   800091 <exit>
}
  800087:	83 c4 10             	add    $0x10,%esp
  80008a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80008d:	5b                   	pop    %ebx
  80008e:	5e                   	pop    %esi
  80008f:	5d                   	pop    %ebp
  800090:	c3                   	ret    

00800091 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800091:	55                   	push   %ebp
  800092:	89 e5                	mov    %esp,%ebp
  800094:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800097:	6a 00                	push   $0x0
  800099:	e8 42 00 00 00       	call   8000e0 <sys_env_destroy>
}
  80009e:	83 c4 10             	add    $0x10,%esp
  8000a1:	c9                   	leave  
  8000a2:	c3                   	ret    

008000a3 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000a3:	55                   	push   %ebp
  8000a4:	89 e5                	mov    %esp,%ebp
  8000a6:	57                   	push   %edi
  8000a7:	56                   	push   %esi
  8000a8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a9:	b8 00 00 00 00       	mov    $0x0,%eax
  8000ae:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b1:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b4:	89 c3                	mov    %eax,%ebx
  8000b6:	89 c7                	mov    %eax,%edi
  8000b8:	89 c6                	mov    %eax,%esi
  8000ba:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000bc:	5b                   	pop    %ebx
  8000bd:	5e                   	pop    %esi
  8000be:	5f                   	pop    %edi
  8000bf:	5d                   	pop    %ebp
  8000c0:	c3                   	ret    

008000c1 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c1:	55                   	push   %ebp
  8000c2:	89 e5                	mov    %esp,%ebp
  8000c4:	57                   	push   %edi
  8000c5:	56                   	push   %esi
  8000c6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000c7:	ba 00 00 00 00       	mov    $0x0,%edx
  8000cc:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d1:	89 d1                	mov    %edx,%ecx
  8000d3:	89 d3                	mov    %edx,%ebx
  8000d5:	89 d7                	mov    %edx,%edi
  8000d7:	89 d6                	mov    %edx,%esi
  8000d9:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000db:	5b                   	pop    %ebx
  8000dc:	5e                   	pop    %esi
  8000dd:	5f                   	pop    %edi
  8000de:	5d                   	pop    %ebp
  8000df:	c3                   	ret    

008000e0 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e0:	55                   	push   %ebp
  8000e1:	89 e5                	mov    %esp,%ebp
  8000e3:	57                   	push   %edi
  8000e4:	56                   	push   %esi
  8000e5:	53                   	push   %ebx
  8000e6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e9:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000ee:	b8 03 00 00 00       	mov    $0x3,%eax
  8000f3:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f6:	89 cb                	mov    %ecx,%ebx
  8000f8:	89 cf                	mov    %ecx,%edi
  8000fa:	89 ce                	mov    %ecx,%esi
  8000fc:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000fe:	85 c0                	test   %eax,%eax
  800100:	7e 17                	jle    800119 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800102:	83 ec 0c             	sub    $0xc,%esp
  800105:	50                   	push   %eax
  800106:	6a 03                	push   $0x3
  800108:	68 0a 0f 80 00       	push   $0x800f0a
  80010d:	6a 23                	push   $0x23
  80010f:	68 27 0f 80 00       	push   $0x800f27
  800114:	e8 14 02 00 00       	call   80032d <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800119:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80011c:	5b                   	pop    %ebx
  80011d:	5e                   	pop    %esi
  80011e:	5f                   	pop    %edi
  80011f:	5d                   	pop    %ebp
  800120:	c3                   	ret    

00800121 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800121:	55                   	push   %ebp
  800122:	89 e5                	mov    %esp,%ebp
  800124:	57                   	push   %edi
  800125:	56                   	push   %esi
  800126:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800127:	ba 00 00 00 00       	mov    $0x0,%edx
  80012c:	b8 02 00 00 00       	mov    $0x2,%eax
  800131:	89 d1                	mov    %edx,%ecx
  800133:	89 d3                	mov    %edx,%ebx
  800135:	89 d7                	mov    %edx,%edi
  800137:	89 d6                	mov    %edx,%esi
  800139:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80013b:	5b                   	pop    %ebx
  80013c:	5e                   	pop    %esi
  80013d:	5f                   	pop    %edi
  80013e:	5d                   	pop    %ebp
  80013f:	c3                   	ret    

00800140 <sys_yield>:

void
sys_yield(void)
{
  800140:	55                   	push   %ebp
  800141:	89 e5                	mov    %esp,%ebp
  800143:	57                   	push   %edi
  800144:	56                   	push   %esi
  800145:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800146:	ba 00 00 00 00       	mov    $0x0,%edx
  80014b:	b8 0a 00 00 00       	mov    $0xa,%eax
  800150:	89 d1                	mov    %edx,%ecx
  800152:	89 d3                	mov    %edx,%ebx
  800154:	89 d7                	mov    %edx,%edi
  800156:	89 d6                	mov    %edx,%esi
  800158:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80015a:	5b                   	pop    %ebx
  80015b:	5e                   	pop    %esi
  80015c:	5f                   	pop    %edi
  80015d:	5d                   	pop    %ebp
  80015e:	c3                   	ret    

0080015f <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80015f:	55                   	push   %ebp
  800160:	89 e5                	mov    %esp,%ebp
  800162:	57                   	push   %edi
  800163:	56                   	push   %esi
  800164:	53                   	push   %ebx
  800165:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800168:	be 00 00 00 00       	mov    $0x0,%esi
  80016d:	b8 04 00 00 00       	mov    $0x4,%eax
  800172:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800175:	8b 55 08             	mov    0x8(%ebp),%edx
  800178:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80017b:	89 f7                	mov    %esi,%edi
  80017d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80017f:	85 c0                	test   %eax,%eax
  800181:	7e 17                	jle    80019a <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800183:	83 ec 0c             	sub    $0xc,%esp
  800186:	50                   	push   %eax
  800187:	6a 04                	push   $0x4
  800189:	68 0a 0f 80 00       	push   $0x800f0a
  80018e:	6a 23                	push   $0x23
  800190:	68 27 0f 80 00       	push   $0x800f27
  800195:	e8 93 01 00 00       	call   80032d <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  80019a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80019d:	5b                   	pop    %ebx
  80019e:	5e                   	pop    %esi
  80019f:	5f                   	pop    %edi
  8001a0:	5d                   	pop    %ebp
  8001a1:	c3                   	ret    

008001a2 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001a2:	55                   	push   %ebp
  8001a3:	89 e5                	mov    %esp,%ebp
  8001a5:	57                   	push   %edi
  8001a6:	56                   	push   %esi
  8001a7:	53                   	push   %ebx
  8001a8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ab:	b8 05 00 00 00       	mov    $0x5,%eax
  8001b0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001b3:	8b 55 08             	mov    0x8(%ebp),%edx
  8001b6:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b9:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001bc:	8b 75 18             	mov    0x18(%ebp),%esi
  8001bf:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c1:	85 c0                	test   %eax,%eax
  8001c3:	7e 17                	jle    8001dc <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001c5:	83 ec 0c             	sub    $0xc,%esp
  8001c8:	50                   	push   %eax
  8001c9:	6a 05                	push   $0x5
  8001cb:	68 0a 0f 80 00       	push   $0x800f0a
  8001d0:	6a 23                	push   $0x23
  8001d2:	68 27 0f 80 00       	push   $0x800f27
  8001d7:	e8 51 01 00 00       	call   80032d <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001df:	5b                   	pop    %ebx
  8001e0:	5e                   	pop    %esi
  8001e1:	5f                   	pop    %edi
  8001e2:	5d                   	pop    %ebp
  8001e3:	c3                   	ret    

008001e4 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001e4:	55                   	push   %ebp
  8001e5:	89 e5                	mov    %esp,%ebp
  8001e7:	57                   	push   %edi
  8001e8:	56                   	push   %esi
  8001e9:	53                   	push   %ebx
  8001ea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ed:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001f2:	b8 06 00 00 00       	mov    $0x6,%eax
  8001f7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001fa:	8b 55 08             	mov    0x8(%ebp),%edx
  8001fd:	89 df                	mov    %ebx,%edi
  8001ff:	89 de                	mov    %ebx,%esi
  800201:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800203:	85 c0                	test   %eax,%eax
  800205:	7e 17                	jle    80021e <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800207:	83 ec 0c             	sub    $0xc,%esp
  80020a:	50                   	push   %eax
  80020b:	6a 06                	push   $0x6
  80020d:	68 0a 0f 80 00       	push   $0x800f0a
  800212:	6a 23                	push   $0x23
  800214:	68 27 0f 80 00       	push   $0x800f27
  800219:	e8 0f 01 00 00       	call   80032d <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80021e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800221:	5b                   	pop    %ebx
  800222:	5e                   	pop    %esi
  800223:	5f                   	pop    %edi
  800224:	5d                   	pop    %ebp
  800225:	c3                   	ret    

00800226 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800226:	55                   	push   %ebp
  800227:	89 e5                	mov    %esp,%ebp
  800229:	57                   	push   %edi
  80022a:	56                   	push   %esi
  80022b:	53                   	push   %ebx
  80022c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80022f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800234:	b8 08 00 00 00       	mov    $0x8,%eax
  800239:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80023c:	8b 55 08             	mov    0x8(%ebp),%edx
  80023f:	89 df                	mov    %ebx,%edi
  800241:	89 de                	mov    %ebx,%esi
  800243:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800245:	85 c0                	test   %eax,%eax
  800247:	7e 17                	jle    800260 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800249:	83 ec 0c             	sub    $0xc,%esp
  80024c:	50                   	push   %eax
  80024d:	6a 08                	push   $0x8
  80024f:	68 0a 0f 80 00       	push   $0x800f0a
  800254:	6a 23                	push   $0x23
  800256:	68 27 0f 80 00       	push   $0x800f27
  80025b:	e8 cd 00 00 00       	call   80032d <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800260:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800263:	5b                   	pop    %ebx
  800264:	5e                   	pop    %esi
  800265:	5f                   	pop    %edi
  800266:	5d                   	pop    %ebp
  800267:	c3                   	ret    

00800268 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800268:	55                   	push   %ebp
  800269:	89 e5                	mov    %esp,%ebp
  80026b:	57                   	push   %edi
  80026c:	56                   	push   %esi
  80026d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80026e:	ba 00 00 00 00       	mov    $0x0,%edx
  800273:	b8 0b 00 00 00       	mov    $0xb,%eax
  800278:	89 d1                	mov    %edx,%ecx
  80027a:	89 d3                	mov    %edx,%ebx
  80027c:	89 d7                	mov    %edx,%edi
  80027e:	89 d6                	mov    %edx,%esi
  800280:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800282:	5b                   	pop    %ebx
  800283:	5e                   	pop    %esi
  800284:	5f                   	pop    %edi
  800285:	5d                   	pop    %ebp
  800286:	c3                   	ret    

00800287 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800287:	55                   	push   %ebp
  800288:	89 e5                	mov    %esp,%ebp
  80028a:	57                   	push   %edi
  80028b:	56                   	push   %esi
  80028c:	53                   	push   %ebx
  80028d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800290:	bb 00 00 00 00       	mov    $0x0,%ebx
  800295:	b8 09 00 00 00       	mov    $0x9,%eax
  80029a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80029d:	8b 55 08             	mov    0x8(%ebp),%edx
  8002a0:	89 df                	mov    %ebx,%edi
  8002a2:	89 de                	mov    %ebx,%esi
  8002a4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002a6:	85 c0                	test   %eax,%eax
  8002a8:	7e 17                	jle    8002c1 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002aa:	83 ec 0c             	sub    $0xc,%esp
  8002ad:	50                   	push   %eax
  8002ae:	6a 09                	push   $0x9
  8002b0:	68 0a 0f 80 00       	push   $0x800f0a
  8002b5:	6a 23                	push   $0x23
  8002b7:	68 27 0f 80 00       	push   $0x800f27
  8002bc:	e8 6c 00 00 00       	call   80032d <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002c1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c4:	5b                   	pop    %ebx
  8002c5:	5e                   	pop    %esi
  8002c6:	5f                   	pop    %edi
  8002c7:	5d                   	pop    %ebp
  8002c8:	c3                   	ret    

008002c9 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002c9:	55                   	push   %ebp
  8002ca:	89 e5                	mov    %esp,%ebp
  8002cc:	57                   	push   %edi
  8002cd:	56                   	push   %esi
  8002ce:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002cf:	be 00 00 00 00       	mov    $0x0,%esi
  8002d4:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002d9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002dc:	8b 55 08             	mov    0x8(%ebp),%edx
  8002df:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002e2:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002e5:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002e7:	5b                   	pop    %ebx
  8002e8:	5e                   	pop    %esi
  8002e9:	5f                   	pop    %edi
  8002ea:	5d                   	pop    %ebp
  8002eb:	c3                   	ret    

008002ec <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002ec:	55                   	push   %ebp
  8002ed:	89 e5                	mov    %esp,%ebp
  8002ef:	57                   	push   %edi
  8002f0:	56                   	push   %esi
  8002f1:	53                   	push   %ebx
  8002f2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002f5:	b9 00 00 00 00       	mov    $0x0,%ecx
  8002fa:	b8 0d 00 00 00       	mov    $0xd,%eax
  8002ff:	8b 55 08             	mov    0x8(%ebp),%edx
  800302:	89 cb                	mov    %ecx,%ebx
  800304:	89 cf                	mov    %ecx,%edi
  800306:	89 ce                	mov    %ecx,%esi
  800308:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80030a:	85 c0                	test   %eax,%eax
  80030c:	7e 17                	jle    800325 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80030e:	83 ec 0c             	sub    $0xc,%esp
  800311:	50                   	push   %eax
  800312:	6a 0d                	push   $0xd
  800314:	68 0a 0f 80 00       	push   $0x800f0a
  800319:	6a 23                	push   $0x23
  80031b:	68 27 0f 80 00       	push   $0x800f27
  800320:	e8 08 00 00 00       	call   80032d <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800325:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800328:	5b                   	pop    %ebx
  800329:	5e                   	pop    %esi
  80032a:	5f                   	pop    %edi
  80032b:	5d                   	pop    %ebp
  80032c:	c3                   	ret    

0080032d <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80032d:	55                   	push   %ebp
  80032e:	89 e5                	mov    %esp,%ebp
  800330:	56                   	push   %esi
  800331:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800332:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800335:	8b 35 00 20 80 00    	mov    0x802000,%esi
  80033b:	e8 e1 fd ff ff       	call   800121 <sys_getenvid>
  800340:	83 ec 0c             	sub    $0xc,%esp
  800343:	ff 75 0c             	pushl  0xc(%ebp)
  800346:	ff 75 08             	pushl  0x8(%ebp)
  800349:	56                   	push   %esi
  80034a:	50                   	push   %eax
  80034b:	68 38 0f 80 00       	push   $0x800f38
  800350:	e8 b0 00 00 00       	call   800405 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800355:	83 c4 18             	add    $0x18,%esp
  800358:	53                   	push   %ebx
  800359:	ff 75 10             	pushl  0x10(%ebp)
  80035c:	e8 53 00 00 00       	call   8003b4 <vcprintf>
	cprintf("\n");
  800361:	c7 04 24 5c 0f 80 00 	movl   $0x800f5c,(%esp)
  800368:	e8 98 00 00 00       	call   800405 <cprintf>
  80036d:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800370:	cc                   	int3   
  800371:	eb fd                	jmp    800370 <_panic+0x43>

00800373 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800373:	55                   	push   %ebp
  800374:	89 e5                	mov    %esp,%ebp
  800376:	53                   	push   %ebx
  800377:	83 ec 04             	sub    $0x4,%esp
  80037a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80037d:	8b 13                	mov    (%ebx),%edx
  80037f:	8d 42 01             	lea    0x1(%edx),%eax
  800382:	89 03                	mov    %eax,(%ebx)
  800384:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800387:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80038b:	3d ff 00 00 00       	cmp    $0xff,%eax
  800390:	75 1a                	jne    8003ac <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800392:	83 ec 08             	sub    $0x8,%esp
  800395:	68 ff 00 00 00       	push   $0xff
  80039a:	8d 43 08             	lea    0x8(%ebx),%eax
  80039d:	50                   	push   %eax
  80039e:	e8 00 fd ff ff       	call   8000a3 <sys_cputs>
		b->idx = 0;
  8003a3:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003a9:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003ac:	ff 43 04             	incl   0x4(%ebx)
}
  8003af:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003b2:	c9                   	leave  
  8003b3:	c3                   	ret    

008003b4 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003b4:	55                   	push   %ebp
  8003b5:	89 e5                	mov    %esp,%ebp
  8003b7:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003bd:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003c4:	00 00 00 
	b.cnt = 0;
  8003c7:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003ce:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003d1:	ff 75 0c             	pushl  0xc(%ebp)
  8003d4:	ff 75 08             	pushl  0x8(%ebp)
  8003d7:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003dd:	50                   	push   %eax
  8003de:	68 73 03 80 00       	push   $0x800373
  8003e3:	e8 51 01 00 00       	call   800539 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003e8:	83 c4 08             	add    $0x8,%esp
  8003eb:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003f1:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003f7:	50                   	push   %eax
  8003f8:	e8 a6 fc ff ff       	call   8000a3 <sys_cputs>

	return b.cnt;
}
  8003fd:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800403:	c9                   	leave  
  800404:	c3                   	ret    

00800405 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800405:	55                   	push   %ebp
  800406:	89 e5                	mov    %esp,%ebp
  800408:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80040b:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80040e:	50                   	push   %eax
  80040f:	ff 75 08             	pushl  0x8(%ebp)
  800412:	e8 9d ff ff ff       	call   8003b4 <vcprintf>
	va_end(ap);

	return cnt;
}
  800417:	c9                   	leave  
  800418:	c3                   	ret    

00800419 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800419:	55                   	push   %ebp
  80041a:	89 e5                	mov    %esp,%ebp
  80041c:	57                   	push   %edi
  80041d:	56                   	push   %esi
  80041e:	53                   	push   %ebx
  80041f:	83 ec 1c             	sub    $0x1c,%esp
  800422:	89 c7                	mov    %eax,%edi
  800424:	89 d6                	mov    %edx,%esi
  800426:	8b 45 08             	mov    0x8(%ebp),%eax
  800429:	8b 55 0c             	mov    0xc(%ebp),%edx
  80042c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80042f:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800432:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800435:	bb 00 00 00 00       	mov    $0x0,%ebx
  80043a:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80043d:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800440:	39 d3                	cmp    %edx,%ebx
  800442:	72 05                	jb     800449 <printnum+0x30>
  800444:	39 45 10             	cmp    %eax,0x10(%ebp)
  800447:	77 45                	ja     80048e <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800449:	83 ec 0c             	sub    $0xc,%esp
  80044c:	ff 75 18             	pushl  0x18(%ebp)
  80044f:	8b 45 14             	mov    0x14(%ebp),%eax
  800452:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800455:	53                   	push   %ebx
  800456:	ff 75 10             	pushl  0x10(%ebp)
  800459:	83 ec 08             	sub    $0x8,%esp
  80045c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80045f:	ff 75 e0             	pushl  -0x20(%ebp)
  800462:	ff 75 dc             	pushl  -0x24(%ebp)
  800465:	ff 75 d8             	pushl  -0x28(%ebp)
  800468:	e8 27 08 00 00       	call   800c94 <__udivdi3>
  80046d:	83 c4 18             	add    $0x18,%esp
  800470:	52                   	push   %edx
  800471:	50                   	push   %eax
  800472:	89 f2                	mov    %esi,%edx
  800474:	89 f8                	mov    %edi,%eax
  800476:	e8 9e ff ff ff       	call   800419 <printnum>
  80047b:	83 c4 20             	add    $0x20,%esp
  80047e:	eb 16                	jmp    800496 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800480:	83 ec 08             	sub    $0x8,%esp
  800483:	56                   	push   %esi
  800484:	ff 75 18             	pushl  0x18(%ebp)
  800487:	ff d7                	call   *%edi
  800489:	83 c4 10             	add    $0x10,%esp
  80048c:	eb 03                	jmp    800491 <printnum+0x78>
  80048e:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800491:	4b                   	dec    %ebx
  800492:	85 db                	test   %ebx,%ebx
  800494:	7f ea                	jg     800480 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800496:	83 ec 08             	sub    $0x8,%esp
  800499:	56                   	push   %esi
  80049a:	83 ec 04             	sub    $0x4,%esp
  80049d:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a0:	ff 75 e0             	pushl  -0x20(%ebp)
  8004a3:	ff 75 dc             	pushl  -0x24(%ebp)
  8004a6:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a9:	e8 f6 08 00 00       	call   800da4 <__umoddi3>
  8004ae:	83 c4 14             	add    $0x14,%esp
  8004b1:	0f be 80 5e 0f 80 00 	movsbl 0x800f5e(%eax),%eax
  8004b8:	50                   	push   %eax
  8004b9:	ff d7                	call   *%edi
}
  8004bb:	83 c4 10             	add    $0x10,%esp
  8004be:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004c1:	5b                   	pop    %ebx
  8004c2:	5e                   	pop    %esi
  8004c3:	5f                   	pop    %edi
  8004c4:	5d                   	pop    %ebp
  8004c5:	c3                   	ret    

008004c6 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004c6:	55                   	push   %ebp
  8004c7:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004c9:	83 fa 01             	cmp    $0x1,%edx
  8004cc:	7e 0e                	jle    8004dc <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004ce:	8b 10                	mov    (%eax),%edx
  8004d0:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004d3:	89 08                	mov    %ecx,(%eax)
  8004d5:	8b 02                	mov    (%edx),%eax
  8004d7:	8b 52 04             	mov    0x4(%edx),%edx
  8004da:	eb 22                	jmp    8004fe <getuint+0x38>
	else if (lflag)
  8004dc:	85 d2                	test   %edx,%edx
  8004de:	74 10                	je     8004f0 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004e0:	8b 10                	mov    (%eax),%edx
  8004e2:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004e5:	89 08                	mov    %ecx,(%eax)
  8004e7:	8b 02                	mov    (%edx),%eax
  8004e9:	ba 00 00 00 00       	mov    $0x0,%edx
  8004ee:	eb 0e                	jmp    8004fe <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004f0:	8b 10                	mov    (%eax),%edx
  8004f2:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004f5:	89 08                	mov    %ecx,(%eax)
  8004f7:	8b 02                	mov    (%edx),%eax
  8004f9:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8004fe:	5d                   	pop    %ebp
  8004ff:	c3                   	ret    

00800500 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800500:	55                   	push   %ebp
  800501:	89 e5                	mov    %esp,%ebp
  800503:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800506:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800509:	8b 10                	mov    (%eax),%edx
  80050b:	3b 50 04             	cmp    0x4(%eax),%edx
  80050e:	73 0a                	jae    80051a <sprintputch+0x1a>
		*b->buf++ = ch;
  800510:	8d 4a 01             	lea    0x1(%edx),%ecx
  800513:	89 08                	mov    %ecx,(%eax)
  800515:	8b 45 08             	mov    0x8(%ebp),%eax
  800518:	88 02                	mov    %al,(%edx)
}
  80051a:	5d                   	pop    %ebp
  80051b:	c3                   	ret    

0080051c <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80051c:	55                   	push   %ebp
  80051d:	89 e5                	mov    %esp,%ebp
  80051f:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800522:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800525:	50                   	push   %eax
  800526:	ff 75 10             	pushl  0x10(%ebp)
  800529:	ff 75 0c             	pushl  0xc(%ebp)
  80052c:	ff 75 08             	pushl  0x8(%ebp)
  80052f:	e8 05 00 00 00       	call   800539 <vprintfmt>
	va_end(ap);
}
  800534:	83 c4 10             	add    $0x10,%esp
  800537:	c9                   	leave  
  800538:	c3                   	ret    

00800539 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800539:	55                   	push   %ebp
  80053a:	89 e5                	mov    %esp,%ebp
  80053c:	57                   	push   %edi
  80053d:	56                   	push   %esi
  80053e:	53                   	push   %ebx
  80053f:	83 ec 2c             	sub    $0x2c,%esp
  800542:	8b 75 08             	mov    0x8(%ebp),%esi
  800545:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800548:	8b 7d 10             	mov    0x10(%ebp),%edi
  80054b:	eb 12                	jmp    80055f <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80054d:	85 c0                	test   %eax,%eax
  80054f:	0f 84 68 03 00 00    	je     8008bd <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800555:	83 ec 08             	sub    $0x8,%esp
  800558:	53                   	push   %ebx
  800559:	50                   	push   %eax
  80055a:	ff d6                	call   *%esi
  80055c:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80055f:	47                   	inc    %edi
  800560:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800564:	83 f8 25             	cmp    $0x25,%eax
  800567:	75 e4                	jne    80054d <vprintfmt+0x14>
  800569:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80056d:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800574:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80057b:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800582:	ba 00 00 00 00       	mov    $0x0,%edx
  800587:	eb 07                	jmp    800590 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800589:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80058c:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800590:	8d 47 01             	lea    0x1(%edi),%eax
  800593:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800596:	0f b6 0f             	movzbl (%edi),%ecx
  800599:	8a 07                	mov    (%edi),%al
  80059b:	83 e8 23             	sub    $0x23,%eax
  80059e:	3c 55                	cmp    $0x55,%al
  8005a0:	0f 87 fe 02 00 00    	ja     8008a4 <vprintfmt+0x36b>
  8005a6:	0f b6 c0             	movzbl %al,%eax
  8005a9:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8005b0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005b3:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005b7:	eb d7                	jmp    800590 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005bc:	b8 00 00 00 00       	mov    $0x0,%eax
  8005c1:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005c4:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005c7:	01 c0                	add    %eax,%eax
  8005c9:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005cd:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005d0:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005d3:	83 fa 09             	cmp    $0x9,%edx
  8005d6:	77 34                	ja     80060c <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005d8:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005d9:	eb e9                	jmp    8005c4 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005db:	8b 45 14             	mov    0x14(%ebp),%eax
  8005de:	8d 48 04             	lea    0x4(%eax),%ecx
  8005e1:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005e4:	8b 00                	mov    (%eax),%eax
  8005e6:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005ec:	eb 24                	jmp    800612 <vprintfmt+0xd9>
  8005ee:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005f2:	79 07                	jns    8005fb <vprintfmt+0xc2>
  8005f4:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005fb:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fe:	eb 90                	jmp    800590 <vprintfmt+0x57>
  800600:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800603:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80060a:	eb 84                	jmp    800590 <vprintfmt+0x57>
  80060c:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80060f:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800612:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800616:	0f 89 74 ff ff ff    	jns    800590 <vprintfmt+0x57>
				width = precision, precision = -1;
  80061c:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80061f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800622:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800629:	e9 62 ff ff ff       	jmp    800590 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80062e:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80062f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800632:	e9 59 ff ff ff       	jmp    800590 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800637:	8b 45 14             	mov    0x14(%ebp),%eax
  80063a:	8d 50 04             	lea    0x4(%eax),%edx
  80063d:	89 55 14             	mov    %edx,0x14(%ebp)
  800640:	83 ec 08             	sub    $0x8,%esp
  800643:	53                   	push   %ebx
  800644:	ff 30                	pushl  (%eax)
  800646:	ff d6                	call   *%esi
			break;
  800648:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80064b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80064e:	e9 0c ff ff ff       	jmp    80055f <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800653:	8b 45 14             	mov    0x14(%ebp),%eax
  800656:	8d 50 04             	lea    0x4(%eax),%edx
  800659:	89 55 14             	mov    %edx,0x14(%ebp)
  80065c:	8b 00                	mov    (%eax),%eax
  80065e:	85 c0                	test   %eax,%eax
  800660:	79 02                	jns    800664 <vprintfmt+0x12b>
  800662:	f7 d8                	neg    %eax
  800664:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800666:	83 f8 08             	cmp    $0x8,%eax
  800669:	7f 0b                	jg     800676 <vprintfmt+0x13d>
  80066b:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  800672:	85 c0                	test   %eax,%eax
  800674:	75 18                	jne    80068e <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800676:	52                   	push   %edx
  800677:	68 76 0f 80 00       	push   $0x800f76
  80067c:	53                   	push   %ebx
  80067d:	56                   	push   %esi
  80067e:	e8 99 fe ff ff       	call   80051c <printfmt>
  800683:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800686:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800689:	e9 d1 fe ff ff       	jmp    80055f <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80068e:	50                   	push   %eax
  80068f:	68 7f 0f 80 00       	push   $0x800f7f
  800694:	53                   	push   %ebx
  800695:	56                   	push   %esi
  800696:	e8 81 fe ff ff       	call   80051c <printfmt>
  80069b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80069e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006a1:	e9 b9 fe ff ff       	jmp    80055f <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006a6:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a9:	8d 50 04             	lea    0x4(%eax),%edx
  8006ac:	89 55 14             	mov    %edx,0x14(%ebp)
  8006af:	8b 38                	mov    (%eax),%edi
  8006b1:	85 ff                	test   %edi,%edi
  8006b3:	75 05                	jne    8006ba <vprintfmt+0x181>
				p = "(null)";
  8006b5:	bf 6f 0f 80 00       	mov    $0x800f6f,%edi
			if (width > 0 && padc != '-')
  8006ba:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006be:	0f 8e 90 00 00 00    	jle    800754 <vprintfmt+0x21b>
  8006c4:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006c8:	0f 84 8e 00 00 00    	je     80075c <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006ce:	83 ec 08             	sub    $0x8,%esp
  8006d1:	ff 75 d0             	pushl  -0x30(%ebp)
  8006d4:	57                   	push   %edi
  8006d5:	e8 70 02 00 00       	call   80094a <strnlen>
  8006da:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006dd:	29 c1                	sub    %eax,%ecx
  8006df:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006e2:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006e5:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006e9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006ec:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006ef:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f1:	eb 0d                	jmp    800700 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006f3:	83 ec 08             	sub    $0x8,%esp
  8006f6:	53                   	push   %ebx
  8006f7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006fa:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006fc:	4f                   	dec    %edi
  8006fd:	83 c4 10             	add    $0x10,%esp
  800700:	85 ff                	test   %edi,%edi
  800702:	7f ef                	jg     8006f3 <vprintfmt+0x1ba>
  800704:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800707:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80070a:	89 c8                	mov    %ecx,%eax
  80070c:	85 c9                	test   %ecx,%ecx
  80070e:	79 05                	jns    800715 <vprintfmt+0x1dc>
  800710:	b8 00 00 00 00       	mov    $0x0,%eax
  800715:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800718:	29 c1                	sub    %eax,%ecx
  80071a:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80071d:	89 75 08             	mov    %esi,0x8(%ebp)
  800720:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800723:	eb 3d                	jmp    800762 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800725:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800729:	74 19                	je     800744 <vprintfmt+0x20b>
  80072b:	0f be c0             	movsbl %al,%eax
  80072e:	83 e8 20             	sub    $0x20,%eax
  800731:	83 f8 5e             	cmp    $0x5e,%eax
  800734:	76 0e                	jbe    800744 <vprintfmt+0x20b>
					putch('?', putdat);
  800736:	83 ec 08             	sub    $0x8,%esp
  800739:	53                   	push   %ebx
  80073a:	6a 3f                	push   $0x3f
  80073c:	ff 55 08             	call   *0x8(%ebp)
  80073f:	83 c4 10             	add    $0x10,%esp
  800742:	eb 0b                	jmp    80074f <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800744:	83 ec 08             	sub    $0x8,%esp
  800747:	53                   	push   %ebx
  800748:	52                   	push   %edx
  800749:	ff 55 08             	call   *0x8(%ebp)
  80074c:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80074f:	ff 4d e4             	decl   -0x1c(%ebp)
  800752:	eb 0e                	jmp    800762 <vprintfmt+0x229>
  800754:	89 75 08             	mov    %esi,0x8(%ebp)
  800757:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075a:	eb 06                	jmp    800762 <vprintfmt+0x229>
  80075c:	89 75 08             	mov    %esi,0x8(%ebp)
  80075f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800762:	47                   	inc    %edi
  800763:	8a 47 ff             	mov    -0x1(%edi),%al
  800766:	0f be d0             	movsbl %al,%edx
  800769:	85 d2                	test   %edx,%edx
  80076b:	74 1d                	je     80078a <vprintfmt+0x251>
  80076d:	85 f6                	test   %esi,%esi
  80076f:	78 b4                	js     800725 <vprintfmt+0x1ec>
  800771:	4e                   	dec    %esi
  800772:	79 b1                	jns    800725 <vprintfmt+0x1ec>
  800774:	8b 75 08             	mov    0x8(%ebp),%esi
  800777:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80077a:	eb 14                	jmp    800790 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80077c:	83 ec 08             	sub    $0x8,%esp
  80077f:	53                   	push   %ebx
  800780:	6a 20                	push   $0x20
  800782:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800784:	4f                   	dec    %edi
  800785:	83 c4 10             	add    $0x10,%esp
  800788:	eb 06                	jmp    800790 <vprintfmt+0x257>
  80078a:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80078d:	8b 75 08             	mov    0x8(%ebp),%esi
  800790:	85 ff                	test   %edi,%edi
  800792:	7f e8                	jg     80077c <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800794:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800797:	e9 c3 fd ff ff       	jmp    80055f <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80079c:	83 fa 01             	cmp    $0x1,%edx
  80079f:	7e 16                	jle    8007b7 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007a1:	8b 45 14             	mov    0x14(%ebp),%eax
  8007a4:	8d 50 08             	lea    0x8(%eax),%edx
  8007a7:	89 55 14             	mov    %edx,0x14(%ebp)
  8007aa:	8b 50 04             	mov    0x4(%eax),%edx
  8007ad:	8b 00                	mov    (%eax),%eax
  8007af:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007b2:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007b5:	eb 32                	jmp    8007e9 <vprintfmt+0x2b0>
	else if (lflag)
  8007b7:	85 d2                	test   %edx,%edx
  8007b9:	74 18                	je     8007d3 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007bb:	8b 45 14             	mov    0x14(%ebp),%eax
  8007be:	8d 50 04             	lea    0x4(%eax),%edx
  8007c1:	89 55 14             	mov    %edx,0x14(%ebp)
  8007c4:	8b 00                	mov    (%eax),%eax
  8007c6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007c9:	89 c1                	mov    %eax,%ecx
  8007cb:	c1 f9 1f             	sar    $0x1f,%ecx
  8007ce:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007d1:	eb 16                	jmp    8007e9 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007d3:	8b 45 14             	mov    0x14(%ebp),%eax
  8007d6:	8d 50 04             	lea    0x4(%eax),%edx
  8007d9:	89 55 14             	mov    %edx,0x14(%ebp)
  8007dc:	8b 00                	mov    (%eax),%eax
  8007de:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007e1:	89 c1                	mov    %eax,%ecx
  8007e3:	c1 f9 1f             	sar    $0x1f,%ecx
  8007e6:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007e9:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007ec:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007ef:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007f3:	79 76                	jns    80086b <vprintfmt+0x332>
				putch('-', putdat);
  8007f5:	83 ec 08             	sub    $0x8,%esp
  8007f8:	53                   	push   %ebx
  8007f9:	6a 2d                	push   $0x2d
  8007fb:	ff d6                	call   *%esi
				num = -(long long) num;
  8007fd:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800800:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800803:	f7 d8                	neg    %eax
  800805:	83 d2 00             	adc    $0x0,%edx
  800808:	f7 da                	neg    %edx
  80080a:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80080d:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800812:	eb 5c                	jmp    800870 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800814:	8d 45 14             	lea    0x14(%ebp),%eax
  800817:	e8 aa fc ff ff       	call   8004c6 <getuint>
			base = 10;
  80081c:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800821:	eb 4d                	jmp    800870 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800823:	8d 45 14             	lea    0x14(%ebp),%eax
  800826:	e8 9b fc ff ff       	call   8004c6 <getuint>
			base = 8;
  80082b:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800830:	eb 3e                	jmp    800870 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800832:	83 ec 08             	sub    $0x8,%esp
  800835:	53                   	push   %ebx
  800836:	6a 30                	push   $0x30
  800838:	ff d6                	call   *%esi
			putch('x', putdat);
  80083a:	83 c4 08             	add    $0x8,%esp
  80083d:	53                   	push   %ebx
  80083e:	6a 78                	push   $0x78
  800840:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800842:	8b 45 14             	mov    0x14(%ebp),%eax
  800845:	8d 50 04             	lea    0x4(%eax),%edx
  800848:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80084b:	8b 00                	mov    (%eax),%eax
  80084d:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800852:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800855:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80085a:	eb 14                	jmp    800870 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80085c:	8d 45 14             	lea    0x14(%ebp),%eax
  80085f:	e8 62 fc ff ff       	call   8004c6 <getuint>
			base = 16;
  800864:	b9 10 00 00 00       	mov    $0x10,%ecx
  800869:	eb 05                	jmp    800870 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80086b:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800870:	83 ec 0c             	sub    $0xc,%esp
  800873:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800877:	57                   	push   %edi
  800878:	ff 75 e4             	pushl  -0x1c(%ebp)
  80087b:	51                   	push   %ecx
  80087c:	52                   	push   %edx
  80087d:	50                   	push   %eax
  80087e:	89 da                	mov    %ebx,%edx
  800880:	89 f0                	mov    %esi,%eax
  800882:	e8 92 fb ff ff       	call   800419 <printnum>
			break;
  800887:	83 c4 20             	add    $0x20,%esp
  80088a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80088d:	e9 cd fc ff ff       	jmp    80055f <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800892:	83 ec 08             	sub    $0x8,%esp
  800895:	53                   	push   %ebx
  800896:	51                   	push   %ecx
  800897:	ff d6                	call   *%esi
			break;
  800899:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80089c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80089f:	e9 bb fc ff ff       	jmp    80055f <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008a4:	83 ec 08             	sub    $0x8,%esp
  8008a7:	53                   	push   %ebx
  8008a8:	6a 25                	push   $0x25
  8008aa:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008ac:	83 c4 10             	add    $0x10,%esp
  8008af:	eb 01                	jmp    8008b2 <vprintfmt+0x379>
  8008b1:	4f                   	dec    %edi
  8008b2:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008b6:	75 f9                	jne    8008b1 <vprintfmt+0x378>
  8008b8:	e9 a2 fc ff ff       	jmp    80055f <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008c0:	5b                   	pop    %ebx
  8008c1:	5e                   	pop    %esi
  8008c2:	5f                   	pop    %edi
  8008c3:	5d                   	pop    %ebp
  8008c4:	c3                   	ret    

008008c5 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008c5:	55                   	push   %ebp
  8008c6:	89 e5                	mov    %esp,%ebp
  8008c8:	83 ec 18             	sub    $0x18,%esp
  8008cb:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ce:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008d1:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008d4:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008d8:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008db:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008e2:	85 c0                	test   %eax,%eax
  8008e4:	74 26                	je     80090c <vsnprintf+0x47>
  8008e6:	85 d2                	test   %edx,%edx
  8008e8:	7e 29                	jle    800913 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008ea:	ff 75 14             	pushl  0x14(%ebp)
  8008ed:	ff 75 10             	pushl  0x10(%ebp)
  8008f0:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008f3:	50                   	push   %eax
  8008f4:	68 00 05 80 00       	push   $0x800500
  8008f9:	e8 3b fc ff ff       	call   800539 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8008fe:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800901:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800904:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800907:	83 c4 10             	add    $0x10,%esp
  80090a:	eb 0c                	jmp    800918 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80090c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800911:	eb 05                	jmp    800918 <vsnprintf+0x53>
  800913:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800918:	c9                   	leave  
  800919:	c3                   	ret    

0080091a <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80091a:	55                   	push   %ebp
  80091b:	89 e5                	mov    %esp,%ebp
  80091d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800920:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800923:	50                   	push   %eax
  800924:	ff 75 10             	pushl  0x10(%ebp)
  800927:	ff 75 0c             	pushl  0xc(%ebp)
  80092a:	ff 75 08             	pushl  0x8(%ebp)
  80092d:	e8 93 ff ff ff       	call   8008c5 <vsnprintf>
	va_end(ap);

	return rc;
}
  800932:	c9                   	leave  
  800933:	c3                   	ret    

00800934 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800934:	55                   	push   %ebp
  800935:	89 e5                	mov    %esp,%ebp
  800937:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80093a:	b8 00 00 00 00       	mov    $0x0,%eax
  80093f:	eb 01                	jmp    800942 <strlen+0xe>
		n++;
  800941:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800942:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800946:	75 f9                	jne    800941 <strlen+0xd>
		n++;
	return n;
}
  800948:	5d                   	pop    %ebp
  800949:	c3                   	ret    

0080094a <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80094a:	55                   	push   %ebp
  80094b:	89 e5                	mov    %esp,%ebp
  80094d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800950:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800953:	ba 00 00 00 00       	mov    $0x0,%edx
  800958:	eb 01                	jmp    80095b <strnlen+0x11>
		n++;
  80095a:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80095b:	39 c2                	cmp    %eax,%edx
  80095d:	74 08                	je     800967 <strnlen+0x1d>
  80095f:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800963:	75 f5                	jne    80095a <strnlen+0x10>
  800965:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800967:	5d                   	pop    %ebp
  800968:	c3                   	ret    

00800969 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800969:	55                   	push   %ebp
  80096a:	89 e5                	mov    %esp,%ebp
  80096c:	53                   	push   %ebx
  80096d:	8b 45 08             	mov    0x8(%ebp),%eax
  800970:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800973:	89 c2                	mov    %eax,%edx
  800975:	42                   	inc    %edx
  800976:	41                   	inc    %ecx
  800977:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80097a:	88 5a ff             	mov    %bl,-0x1(%edx)
  80097d:	84 db                	test   %bl,%bl
  80097f:	75 f4                	jne    800975 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800981:	5b                   	pop    %ebx
  800982:	5d                   	pop    %ebp
  800983:	c3                   	ret    

00800984 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800984:	55                   	push   %ebp
  800985:	89 e5                	mov    %esp,%ebp
  800987:	53                   	push   %ebx
  800988:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80098b:	53                   	push   %ebx
  80098c:	e8 a3 ff ff ff       	call   800934 <strlen>
  800991:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800994:	ff 75 0c             	pushl  0xc(%ebp)
  800997:	01 d8                	add    %ebx,%eax
  800999:	50                   	push   %eax
  80099a:	e8 ca ff ff ff       	call   800969 <strcpy>
	return dst;
}
  80099f:	89 d8                	mov    %ebx,%eax
  8009a1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009a4:	c9                   	leave  
  8009a5:	c3                   	ret    

008009a6 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009a6:	55                   	push   %ebp
  8009a7:	89 e5                	mov    %esp,%ebp
  8009a9:	56                   	push   %esi
  8009aa:	53                   	push   %ebx
  8009ab:	8b 75 08             	mov    0x8(%ebp),%esi
  8009ae:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009b1:	89 f3                	mov    %esi,%ebx
  8009b3:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009b6:	89 f2                	mov    %esi,%edx
  8009b8:	eb 0c                	jmp    8009c6 <strncpy+0x20>
		*dst++ = *src;
  8009ba:	42                   	inc    %edx
  8009bb:	8a 01                	mov    (%ecx),%al
  8009bd:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009c0:	80 39 01             	cmpb   $0x1,(%ecx)
  8009c3:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009c6:	39 da                	cmp    %ebx,%edx
  8009c8:	75 f0                	jne    8009ba <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009ca:	89 f0                	mov    %esi,%eax
  8009cc:	5b                   	pop    %ebx
  8009cd:	5e                   	pop    %esi
  8009ce:	5d                   	pop    %ebp
  8009cf:	c3                   	ret    

008009d0 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009d0:	55                   	push   %ebp
  8009d1:	89 e5                	mov    %esp,%ebp
  8009d3:	56                   	push   %esi
  8009d4:	53                   	push   %ebx
  8009d5:	8b 75 08             	mov    0x8(%ebp),%esi
  8009d8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009db:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009de:	85 c0                	test   %eax,%eax
  8009e0:	74 1e                	je     800a00 <strlcpy+0x30>
  8009e2:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009e6:	89 f2                	mov    %esi,%edx
  8009e8:	eb 05                	jmp    8009ef <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009ea:	42                   	inc    %edx
  8009eb:	41                   	inc    %ecx
  8009ec:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009ef:	39 c2                	cmp    %eax,%edx
  8009f1:	74 08                	je     8009fb <strlcpy+0x2b>
  8009f3:	8a 19                	mov    (%ecx),%bl
  8009f5:	84 db                	test   %bl,%bl
  8009f7:	75 f1                	jne    8009ea <strlcpy+0x1a>
  8009f9:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8009fb:	c6 00 00             	movb   $0x0,(%eax)
  8009fe:	eb 02                	jmp    800a02 <strlcpy+0x32>
  800a00:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a02:	29 f0                	sub    %esi,%eax
}
  800a04:	5b                   	pop    %ebx
  800a05:	5e                   	pop    %esi
  800a06:	5d                   	pop    %ebp
  800a07:	c3                   	ret    

00800a08 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a08:	55                   	push   %ebp
  800a09:	89 e5                	mov    %esp,%ebp
  800a0b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a0e:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a11:	eb 02                	jmp    800a15 <strcmp+0xd>
		p++, q++;
  800a13:	41                   	inc    %ecx
  800a14:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a15:	8a 01                	mov    (%ecx),%al
  800a17:	84 c0                	test   %al,%al
  800a19:	74 04                	je     800a1f <strcmp+0x17>
  800a1b:	3a 02                	cmp    (%edx),%al
  800a1d:	74 f4                	je     800a13 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a1f:	0f b6 c0             	movzbl %al,%eax
  800a22:	0f b6 12             	movzbl (%edx),%edx
  800a25:	29 d0                	sub    %edx,%eax
}
  800a27:	5d                   	pop    %ebp
  800a28:	c3                   	ret    

00800a29 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a29:	55                   	push   %ebp
  800a2a:	89 e5                	mov    %esp,%ebp
  800a2c:	53                   	push   %ebx
  800a2d:	8b 45 08             	mov    0x8(%ebp),%eax
  800a30:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a33:	89 c3                	mov    %eax,%ebx
  800a35:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a38:	eb 02                	jmp    800a3c <strncmp+0x13>
		n--, p++, q++;
  800a3a:	40                   	inc    %eax
  800a3b:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a3c:	39 d8                	cmp    %ebx,%eax
  800a3e:	74 14                	je     800a54 <strncmp+0x2b>
  800a40:	8a 08                	mov    (%eax),%cl
  800a42:	84 c9                	test   %cl,%cl
  800a44:	74 04                	je     800a4a <strncmp+0x21>
  800a46:	3a 0a                	cmp    (%edx),%cl
  800a48:	74 f0                	je     800a3a <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a4a:	0f b6 00             	movzbl (%eax),%eax
  800a4d:	0f b6 12             	movzbl (%edx),%edx
  800a50:	29 d0                	sub    %edx,%eax
  800a52:	eb 05                	jmp    800a59 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a54:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a59:	5b                   	pop    %ebx
  800a5a:	5d                   	pop    %ebp
  800a5b:	c3                   	ret    

00800a5c <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a5c:	55                   	push   %ebp
  800a5d:	89 e5                	mov    %esp,%ebp
  800a5f:	8b 45 08             	mov    0x8(%ebp),%eax
  800a62:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a65:	eb 05                	jmp    800a6c <strchr+0x10>
		if (*s == c)
  800a67:	38 ca                	cmp    %cl,%dl
  800a69:	74 0c                	je     800a77 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a6b:	40                   	inc    %eax
  800a6c:	8a 10                	mov    (%eax),%dl
  800a6e:	84 d2                	test   %dl,%dl
  800a70:	75 f5                	jne    800a67 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a72:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a77:	5d                   	pop    %ebp
  800a78:	c3                   	ret    

00800a79 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a79:	55                   	push   %ebp
  800a7a:	89 e5                	mov    %esp,%ebp
  800a7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7f:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a82:	eb 05                	jmp    800a89 <strfind+0x10>
		if (*s == c)
  800a84:	38 ca                	cmp    %cl,%dl
  800a86:	74 07                	je     800a8f <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a88:	40                   	inc    %eax
  800a89:	8a 10                	mov    (%eax),%dl
  800a8b:	84 d2                	test   %dl,%dl
  800a8d:	75 f5                	jne    800a84 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a8f:	5d                   	pop    %ebp
  800a90:	c3                   	ret    

00800a91 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a91:	55                   	push   %ebp
  800a92:	89 e5                	mov    %esp,%ebp
  800a94:	57                   	push   %edi
  800a95:	56                   	push   %esi
  800a96:	53                   	push   %ebx
  800a97:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a9a:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a9d:	85 c9                	test   %ecx,%ecx
  800a9f:	74 36                	je     800ad7 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800aa1:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800aa7:	75 28                	jne    800ad1 <memset+0x40>
  800aa9:	f6 c1 03             	test   $0x3,%cl
  800aac:	75 23                	jne    800ad1 <memset+0x40>
		c &= 0xFF;
  800aae:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800ab2:	89 d3                	mov    %edx,%ebx
  800ab4:	c1 e3 08             	shl    $0x8,%ebx
  800ab7:	89 d6                	mov    %edx,%esi
  800ab9:	c1 e6 18             	shl    $0x18,%esi
  800abc:	89 d0                	mov    %edx,%eax
  800abe:	c1 e0 10             	shl    $0x10,%eax
  800ac1:	09 f0                	or     %esi,%eax
  800ac3:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800ac5:	89 d8                	mov    %ebx,%eax
  800ac7:	09 d0                	or     %edx,%eax
  800ac9:	c1 e9 02             	shr    $0x2,%ecx
  800acc:	fc                   	cld    
  800acd:	f3 ab                	rep stos %eax,%es:(%edi)
  800acf:	eb 06                	jmp    800ad7 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ad1:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ad4:	fc                   	cld    
  800ad5:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800ad7:	89 f8                	mov    %edi,%eax
  800ad9:	5b                   	pop    %ebx
  800ada:	5e                   	pop    %esi
  800adb:	5f                   	pop    %edi
  800adc:	5d                   	pop    %ebp
  800add:	c3                   	ret    

00800ade <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ade:	55                   	push   %ebp
  800adf:	89 e5                	mov    %esp,%ebp
  800ae1:	57                   	push   %edi
  800ae2:	56                   	push   %esi
  800ae3:	8b 45 08             	mov    0x8(%ebp),%eax
  800ae6:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ae9:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800aec:	39 c6                	cmp    %eax,%esi
  800aee:	73 33                	jae    800b23 <memmove+0x45>
  800af0:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800af3:	39 d0                	cmp    %edx,%eax
  800af5:	73 2c                	jae    800b23 <memmove+0x45>
		s += n;
		d += n;
  800af7:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800afa:	89 d6                	mov    %edx,%esi
  800afc:	09 fe                	or     %edi,%esi
  800afe:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b04:	75 13                	jne    800b19 <memmove+0x3b>
  800b06:	f6 c1 03             	test   $0x3,%cl
  800b09:	75 0e                	jne    800b19 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b0b:	83 ef 04             	sub    $0x4,%edi
  800b0e:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b11:	c1 e9 02             	shr    $0x2,%ecx
  800b14:	fd                   	std    
  800b15:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b17:	eb 07                	jmp    800b20 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b19:	4f                   	dec    %edi
  800b1a:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b1d:	fd                   	std    
  800b1e:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b20:	fc                   	cld    
  800b21:	eb 1d                	jmp    800b40 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b23:	89 f2                	mov    %esi,%edx
  800b25:	09 c2                	or     %eax,%edx
  800b27:	f6 c2 03             	test   $0x3,%dl
  800b2a:	75 0f                	jne    800b3b <memmove+0x5d>
  800b2c:	f6 c1 03             	test   $0x3,%cl
  800b2f:	75 0a                	jne    800b3b <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b31:	c1 e9 02             	shr    $0x2,%ecx
  800b34:	89 c7                	mov    %eax,%edi
  800b36:	fc                   	cld    
  800b37:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b39:	eb 05                	jmp    800b40 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b3b:	89 c7                	mov    %eax,%edi
  800b3d:	fc                   	cld    
  800b3e:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b40:	5e                   	pop    %esi
  800b41:	5f                   	pop    %edi
  800b42:	5d                   	pop    %ebp
  800b43:	c3                   	ret    

00800b44 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b44:	55                   	push   %ebp
  800b45:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b47:	ff 75 10             	pushl  0x10(%ebp)
  800b4a:	ff 75 0c             	pushl  0xc(%ebp)
  800b4d:	ff 75 08             	pushl  0x8(%ebp)
  800b50:	e8 89 ff ff ff       	call   800ade <memmove>
}
  800b55:	c9                   	leave  
  800b56:	c3                   	ret    

00800b57 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b57:	55                   	push   %ebp
  800b58:	89 e5                	mov    %esp,%ebp
  800b5a:	56                   	push   %esi
  800b5b:	53                   	push   %ebx
  800b5c:	8b 45 08             	mov    0x8(%ebp),%eax
  800b5f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b62:	89 c6                	mov    %eax,%esi
  800b64:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b67:	eb 14                	jmp    800b7d <memcmp+0x26>
		if (*s1 != *s2)
  800b69:	8a 08                	mov    (%eax),%cl
  800b6b:	8a 1a                	mov    (%edx),%bl
  800b6d:	38 d9                	cmp    %bl,%cl
  800b6f:	74 0a                	je     800b7b <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b71:	0f b6 c1             	movzbl %cl,%eax
  800b74:	0f b6 db             	movzbl %bl,%ebx
  800b77:	29 d8                	sub    %ebx,%eax
  800b79:	eb 0b                	jmp    800b86 <memcmp+0x2f>
		s1++, s2++;
  800b7b:	40                   	inc    %eax
  800b7c:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b7d:	39 f0                	cmp    %esi,%eax
  800b7f:	75 e8                	jne    800b69 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b81:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b86:	5b                   	pop    %ebx
  800b87:	5e                   	pop    %esi
  800b88:	5d                   	pop    %ebp
  800b89:	c3                   	ret    

00800b8a <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b8a:	55                   	push   %ebp
  800b8b:	89 e5                	mov    %esp,%ebp
  800b8d:	53                   	push   %ebx
  800b8e:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b91:	89 c1                	mov    %eax,%ecx
  800b93:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b96:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b9a:	eb 08                	jmp    800ba4 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b9c:	0f b6 10             	movzbl (%eax),%edx
  800b9f:	39 da                	cmp    %ebx,%edx
  800ba1:	74 05                	je     800ba8 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ba3:	40                   	inc    %eax
  800ba4:	39 c8                	cmp    %ecx,%eax
  800ba6:	72 f4                	jb     800b9c <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800ba8:	5b                   	pop    %ebx
  800ba9:	5d                   	pop    %ebp
  800baa:	c3                   	ret    

00800bab <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bab:	55                   	push   %ebp
  800bac:	89 e5                	mov    %esp,%ebp
  800bae:	57                   	push   %edi
  800baf:	56                   	push   %esi
  800bb0:	53                   	push   %ebx
  800bb1:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb4:	eb 01                	jmp    800bb7 <strtol+0xc>
		s++;
  800bb6:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb7:	8a 01                	mov    (%ecx),%al
  800bb9:	3c 20                	cmp    $0x20,%al
  800bbb:	74 f9                	je     800bb6 <strtol+0xb>
  800bbd:	3c 09                	cmp    $0x9,%al
  800bbf:	74 f5                	je     800bb6 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bc1:	3c 2b                	cmp    $0x2b,%al
  800bc3:	75 08                	jne    800bcd <strtol+0x22>
		s++;
  800bc5:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bc6:	bf 00 00 00 00       	mov    $0x0,%edi
  800bcb:	eb 11                	jmp    800bde <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bcd:	3c 2d                	cmp    $0x2d,%al
  800bcf:	75 08                	jne    800bd9 <strtol+0x2e>
		s++, neg = 1;
  800bd1:	41                   	inc    %ecx
  800bd2:	bf 01 00 00 00       	mov    $0x1,%edi
  800bd7:	eb 05                	jmp    800bde <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bd9:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bde:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800be2:	0f 84 87 00 00 00    	je     800c6f <strtol+0xc4>
  800be8:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bec:	75 27                	jne    800c15 <strtol+0x6a>
  800bee:	80 39 30             	cmpb   $0x30,(%ecx)
  800bf1:	75 22                	jne    800c15 <strtol+0x6a>
  800bf3:	e9 88 00 00 00       	jmp    800c80 <strtol+0xd5>
		s += 2, base = 16;
  800bf8:	83 c1 02             	add    $0x2,%ecx
  800bfb:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c02:	eb 11                	jmp    800c15 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c04:	41                   	inc    %ecx
  800c05:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c0c:	eb 07                	jmp    800c15 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c0e:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c15:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c1a:	8a 11                	mov    (%ecx),%dl
  800c1c:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c1f:	80 fb 09             	cmp    $0x9,%bl
  800c22:	77 08                	ja     800c2c <strtol+0x81>
			dig = *s - '0';
  800c24:	0f be d2             	movsbl %dl,%edx
  800c27:	83 ea 30             	sub    $0x30,%edx
  800c2a:	eb 22                	jmp    800c4e <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c2c:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c2f:	89 f3                	mov    %esi,%ebx
  800c31:	80 fb 19             	cmp    $0x19,%bl
  800c34:	77 08                	ja     800c3e <strtol+0x93>
			dig = *s - 'a' + 10;
  800c36:	0f be d2             	movsbl %dl,%edx
  800c39:	83 ea 57             	sub    $0x57,%edx
  800c3c:	eb 10                	jmp    800c4e <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c3e:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c41:	89 f3                	mov    %esi,%ebx
  800c43:	80 fb 19             	cmp    $0x19,%bl
  800c46:	77 14                	ja     800c5c <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c48:	0f be d2             	movsbl %dl,%edx
  800c4b:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c4e:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c51:	7d 09                	jge    800c5c <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c53:	41                   	inc    %ecx
  800c54:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c58:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c5a:	eb be                	jmp    800c1a <strtol+0x6f>

	if (endptr)
  800c5c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c60:	74 05                	je     800c67 <strtol+0xbc>
		*endptr = (char *) s;
  800c62:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c65:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c67:	85 ff                	test   %edi,%edi
  800c69:	74 21                	je     800c8c <strtol+0xe1>
  800c6b:	f7 d8                	neg    %eax
  800c6d:	eb 1d                	jmp    800c8c <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c6f:	80 39 30             	cmpb   $0x30,(%ecx)
  800c72:	75 9a                	jne    800c0e <strtol+0x63>
  800c74:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c78:	0f 84 7a ff ff ff    	je     800bf8 <strtol+0x4d>
  800c7e:	eb 84                	jmp    800c04 <strtol+0x59>
  800c80:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c84:	0f 84 6e ff ff ff    	je     800bf8 <strtol+0x4d>
  800c8a:	eb 89                	jmp    800c15 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c8c:	5b                   	pop    %ebx
  800c8d:	5e                   	pop    %esi
  800c8e:	5f                   	pop    %edi
  800c8f:	5d                   	pop    %ebp
  800c90:	c3                   	ret    
  800c91:	66 90                	xchg   %ax,%ax
  800c93:	90                   	nop

00800c94 <__udivdi3>:
  800c94:	55                   	push   %ebp
  800c95:	57                   	push   %edi
  800c96:	56                   	push   %esi
  800c97:	53                   	push   %ebx
  800c98:	83 ec 1c             	sub    $0x1c,%esp
  800c9b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800c9f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ca3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ca7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cab:	89 ca                	mov    %ecx,%edx
  800cad:	89 f8                	mov    %edi,%eax
  800caf:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cb3:	85 f6                	test   %esi,%esi
  800cb5:	75 2d                	jne    800ce4 <__udivdi3+0x50>
  800cb7:	39 cf                	cmp    %ecx,%edi
  800cb9:	77 65                	ja     800d20 <__udivdi3+0x8c>
  800cbb:	89 fd                	mov    %edi,%ebp
  800cbd:	85 ff                	test   %edi,%edi
  800cbf:	75 0b                	jne    800ccc <__udivdi3+0x38>
  800cc1:	b8 01 00 00 00       	mov    $0x1,%eax
  800cc6:	31 d2                	xor    %edx,%edx
  800cc8:	f7 f7                	div    %edi
  800cca:	89 c5                	mov    %eax,%ebp
  800ccc:	31 d2                	xor    %edx,%edx
  800cce:	89 c8                	mov    %ecx,%eax
  800cd0:	f7 f5                	div    %ebp
  800cd2:	89 c1                	mov    %eax,%ecx
  800cd4:	89 d8                	mov    %ebx,%eax
  800cd6:	f7 f5                	div    %ebp
  800cd8:	89 cf                	mov    %ecx,%edi
  800cda:	89 fa                	mov    %edi,%edx
  800cdc:	83 c4 1c             	add    $0x1c,%esp
  800cdf:	5b                   	pop    %ebx
  800ce0:	5e                   	pop    %esi
  800ce1:	5f                   	pop    %edi
  800ce2:	5d                   	pop    %ebp
  800ce3:	c3                   	ret    
  800ce4:	39 ce                	cmp    %ecx,%esi
  800ce6:	77 28                	ja     800d10 <__udivdi3+0x7c>
  800ce8:	0f bd fe             	bsr    %esi,%edi
  800ceb:	83 f7 1f             	xor    $0x1f,%edi
  800cee:	75 40                	jne    800d30 <__udivdi3+0x9c>
  800cf0:	39 ce                	cmp    %ecx,%esi
  800cf2:	72 0a                	jb     800cfe <__udivdi3+0x6a>
  800cf4:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800cf8:	0f 87 9e 00 00 00    	ja     800d9c <__udivdi3+0x108>
  800cfe:	b8 01 00 00 00       	mov    $0x1,%eax
  800d03:	89 fa                	mov    %edi,%edx
  800d05:	83 c4 1c             	add    $0x1c,%esp
  800d08:	5b                   	pop    %ebx
  800d09:	5e                   	pop    %esi
  800d0a:	5f                   	pop    %edi
  800d0b:	5d                   	pop    %ebp
  800d0c:	c3                   	ret    
  800d0d:	8d 76 00             	lea    0x0(%esi),%esi
  800d10:	31 ff                	xor    %edi,%edi
  800d12:	31 c0                	xor    %eax,%eax
  800d14:	89 fa                	mov    %edi,%edx
  800d16:	83 c4 1c             	add    $0x1c,%esp
  800d19:	5b                   	pop    %ebx
  800d1a:	5e                   	pop    %esi
  800d1b:	5f                   	pop    %edi
  800d1c:	5d                   	pop    %ebp
  800d1d:	c3                   	ret    
  800d1e:	66 90                	xchg   %ax,%ax
  800d20:	89 d8                	mov    %ebx,%eax
  800d22:	f7 f7                	div    %edi
  800d24:	31 ff                	xor    %edi,%edi
  800d26:	89 fa                	mov    %edi,%edx
  800d28:	83 c4 1c             	add    $0x1c,%esp
  800d2b:	5b                   	pop    %ebx
  800d2c:	5e                   	pop    %esi
  800d2d:	5f                   	pop    %edi
  800d2e:	5d                   	pop    %ebp
  800d2f:	c3                   	ret    
  800d30:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d35:	89 eb                	mov    %ebp,%ebx
  800d37:	29 fb                	sub    %edi,%ebx
  800d39:	89 f9                	mov    %edi,%ecx
  800d3b:	d3 e6                	shl    %cl,%esi
  800d3d:	89 c5                	mov    %eax,%ebp
  800d3f:	88 d9                	mov    %bl,%cl
  800d41:	d3 ed                	shr    %cl,%ebp
  800d43:	89 e9                	mov    %ebp,%ecx
  800d45:	09 f1                	or     %esi,%ecx
  800d47:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d4b:	89 f9                	mov    %edi,%ecx
  800d4d:	d3 e0                	shl    %cl,%eax
  800d4f:	89 c5                	mov    %eax,%ebp
  800d51:	89 d6                	mov    %edx,%esi
  800d53:	88 d9                	mov    %bl,%cl
  800d55:	d3 ee                	shr    %cl,%esi
  800d57:	89 f9                	mov    %edi,%ecx
  800d59:	d3 e2                	shl    %cl,%edx
  800d5b:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d5f:	88 d9                	mov    %bl,%cl
  800d61:	d3 e8                	shr    %cl,%eax
  800d63:	09 c2                	or     %eax,%edx
  800d65:	89 d0                	mov    %edx,%eax
  800d67:	89 f2                	mov    %esi,%edx
  800d69:	f7 74 24 0c          	divl   0xc(%esp)
  800d6d:	89 d6                	mov    %edx,%esi
  800d6f:	89 c3                	mov    %eax,%ebx
  800d71:	f7 e5                	mul    %ebp
  800d73:	39 d6                	cmp    %edx,%esi
  800d75:	72 19                	jb     800d90 <__udivdi3+0xfc>
  800d77:	74 0b                	je     800d84 <__udivdi3+0xf0>
  800d79:	89 d8                	mov    %ebx,%eax
  800d7b:	31 ff                	xor    %edi,%edi
  800d7d:	e9 58 ff ff ff       	jmp    800cda <__udivdi3+0x46>
  800d82:	66 90                	xchg   %ax,%ax
  800d84:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d88:	89 f9                	mov    %edi,%ecx
  800d8a:	d3 e2                	shl    %cl,%edx
  800d8c:	39 c2                	cmp    %eax,%edx
  800d8e:	73 e9                	jae    800d79 <__udivdi3+0xe5>
  800d90:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d93:	31 ff                	xor    %edi,%edi
  800d95:	e9 40 ff ff ff       	jmp    800cda <__udivdi3+0x46>
  800d9a:	66 90                	xchg   %ax,%ax
  800d9c:	31 c0                	xor    %eax,%eax
  800d9e:	e9 37 ff ff ff       	jmp    800cda <__udivdi3+0x46>
  800da3:	90                   	nop

00800da4 <__umoddi3>:
  800da4:	55                   	push   %ebp
  800da5:	57                   	push   %edi
  800da6:	56                   	push   %esi
  800da7:	53                   	push   %ebx
  800da8:	83 ec 1c             	sub    $0x1c,%esp
  800dab:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800daf:	8b 74 24 34          	mov    0x34(%esp),%esi
  800db3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800db7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dbb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dbf:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dc3:	89 f3                	mov    %esi,%ebx
  800dc5:	89 fa                	mov    %edi,%edx
  800dc7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dcb:	89 34 24             	mov    %esi,(%esp)
  800dce:	85 c0                	test   %eax,%eax
  800dd0:	75 1a                	jne    800dec <__umoddi3+0x48>
  800dd2:	39 f7                	cmp    %esi,%edi
  800dd4:	0f 86 a2 00 00 00    	jbe    800e7c <__umoddi3+0xd8>
  800dda:	89 c8                	mov    %ecx,%eax
  800ddc:	89 f2                	mov    %esi,%edx
  800dde:	f7 f7                	div    %edi
  800de0:	89 d0                	mov    %edx,%eax
  800de2:	31 d2                	xor    %edx,%edx
  800de4:	83 c4 1c             	add    $0x1c,%esp
  800de7:	5b                   	pop    %ebx
  800de8:	5e                   	pop    %esi
  800de9:	5f                   	pop    %edi
  800dea:	5d                   	pop    %ebp
  800deb:	c3                   	ret    
  800dec:	39 f0                	cmp    %esi,%eax
  800dee:	0f 87 ac 00 00 00    	ja     800ea0 <__umoddi3+0xfc>
  800df4:	0f bd e8             	bsr    %eax,%ebp
  800df7:	83 f5 1f             	xor    $0x1f,%ebp
  800dfa:	0f 84 ac 00 00 00    	je     800eac <__umoddi3+0x108>
  800e00:	bf 20 00 00 00       	mov    $0x20,%edi
  800e05:	29 ef                	sub    %ebp,%edi
  800e07:	89 fe                	mov    %edi,%esi
  800e09:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e0d:	89 e9                	mov    %ebp,%ecx
  800e0f:	d3 e0                	shl    %cl,%eax
  800e11:	89 d7                	mov    %edx,%edi
  800e13:	89 f1                	mov    %esi,%ecx
  800e15:	d3 ef                	shr    %cl,%edi
  800e17:	09 c7                	or     %eax,%edi
  800e19:	89 e9                	mov    %ebp,%ecx
  800e1b:	d3 e2                	shl    %cl,%edx
  800e1d:	89 14 24             	mov    %edx,(%esp)
  800e20:	89 d8                	mov    %ebx,%eax
  800e22:	d3 e0                	shl    %cl,%eax
  800e24:	89 c2                	mov    %eax,%edx
  800e26:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e2a:	d3 e0                	shl    %cl,%eax
  800e2c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e30:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e34:	89 f1                	mov    %esi,%ecx
  800e36:	d3 e8                	shr    %cl,%eax
  800e38:	09 d0                	or     %edx,%eax
  800e3a:	d3 eb                	shr    %cl,%ebx
  800e3c:	89 da                	mov    %ebx,%edx
  800e3e:	f7 f7                	div    %edi
  800e40:	89 d3                	mov    %edx,%ebx
  800e42:	f7 24 24             	mull   (%esp)
  800e45:	89 c6                	mov    %eax,%esi
  800e47:	89 d1                	mov    %edx,%ecx
  800e49:	39 d3                	cmp    %edx,%ebx
  800e4b:	0f 82 87 00 00 00    	jb     800ed8 <__umoddi3+0x134>
  800e51:	0f 84 91 00 00 00    	je     800ee8 <__umoddi3+0x144>
  800e57:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e5b:	29 f2                	sub    %esi,%edx
  800e5d:	19 cb                	sbb    %ecx,%ebx
  800e5f:	89 d8                	mov    %ebx,%eax
  800e61:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e65:	d3 e0                	shl    %cl,%eax
  800e67:	89 e9                	mov    %ebp,%ecx
  800e69:	d3 ea                	shr    %cl,%edx
  800e6b:	09 d0                	or     %edx,%eax
  800e6d:	89 e9                	mov    %ebp,%ecx
  800e6f:	d3 eb                	shr    %cl,%ebx
  800e71:	89 da                	mov    %ebx,%edx
  800e73:	83 c4 1c             	add    $0x1c,%esp
  800e76:	5b                   	pop    %ebx
  800e77:	5e                   	pop    %esi
  800e78:	5f                   	pop    %edi
  800e79:	5d                   	pop    %ebp
  800e7a:	c3                   	ret    
  800e7b:	90                   	nop
  800e7c:	89 fd                	mov    %edi,%ebp
  800e7e:	85 ff                	test   %edi,%edi
  800e80:	75 0b                	jne    800e8d <__umoddi3+0xe9>
  800e82:	b8 01 00 00 00       	mov    $0x1,%eax
  800e87:	31 d2                	xor    %edx,%edx
  800e89:	f7 f7                	div    %edi
  800e8b:	89 c5                	mov    %eax,%ebp
  800e8d:	89 f0                	mov    %esi,%eax
  800e8f:	31 d2                	xor    %edx,%edx
  800e91:	f7 f5                	div    %ebp
  800e93:	89 c8                	mov    %ecx,%eax
  800e95:	f7 f5                	div    %ebp
  800e97:	89 d0                	mov    %edx,%eax
  800e99:	e9 44 ff ff ff       	jmp    800de2 <__umoddi3+0x3e>
  800e9e:	66 90                	xchg   %ax,%ax
  800ea0:	89 c8                	mov    %ecx,%eax
  800ea2:	89 f2                	mov    %esi,%edx
  800ea4:	83 c4 1c             	add    $0x1c,%esp
  800ea7:	5b                   	pop    %ebx
  800ea8:	5e                   	pop    %esi
  800ea9:	5f                   	pop    %edi
  800eaa:	5d                   	pop    %ebp
  800eab:	c3                   	ret    
  800eac:	3b 04 24             	cmp    (%esp),%eax
  800eaf:	72 06                	jb     800eb7 <__umoddi3+0x113>
  800eb1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800eb5:	77 0f                	ja     800ec6 <__umoddi3+0x122>
  800eb7:	89 f2                	mov    %esi,%edx
  800eb9:	29 f9                	sub    %edi,%ecx
  800ebb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ebf:	89 14 24             	mov    %edx,(%esp)
  800ec2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ec6:	8b 44 24 04          	mov    0x4(%esp),%eax
  800eca:	8b 14 24             	mov    (%esp),%edx
  800ecd:	83 c4 1c             	add    $0x1c,%esp
  800ed0:	5b                   	pop    %ebx
  800ed1:	5e                   	pop    %esi
  800ed2:	5f                   	pop    %edi
  800ed3:	5d                   	pop    %ebp
  800ed4:	c3                   	ret    
  800ed5:	8d 76 00             	lea    0x0(%esi),%esi
  800ed8:	2b 04 24             	sub    (%esp),%eax
  800edb:	19 fa                	sbb    %edi,%edx
  800edd:	89 d1                	mov    %edx,%ecx
  800edf:	89 c6                	mov    %eax,%esi
  800ee1:	e9 71 ff ff ff       	jmp    800e57 <__umoddi3+0xb3>
  800ee6:	66 90                	xchg   %ax,%ax
  800ee8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800eec:	72 ea                	jb     800ed8 <__umoddi3+0x134>
  800eee:	89 d9                	mov    %ebx,%ecx
  800ef0:	e9 62 ff ff ff       	jmp    800e57 <__umoddi3+0xb3>
