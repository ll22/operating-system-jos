
obj/user/badsegment:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 0d 00 00 00       	call   80003e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	// Try to load the kernel's TSS selector into the DS register.
	asm volatile("movw $0x28,%ax; movw %ax,%ds");
  800036:	66 b8 28 00          	mov    $0x28,%ax
  80003a:	8e d8                	mov    %eax,%ds
}
  80003c:	5d                   	pop    %ebp
  80003d:	c3                   	ret    

0080003e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003e:	55                   	push   %ebp
  80003f:	89 e5                	mov    %esp,%ebp
  800041:	56                   	push   %esi
  800042:	53                   	push   %ebx
  800043:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800046:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800049:	e8 cf 00 00 00       	call   80011d <sys_getenvid>
  80004e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800053:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80005a:	c1 e0 07             	shl    $0x7,%eax
  80005d:	29 d0                	sub    %edx,%eax
  80005f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800064:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800069:	85 db                	test   %ebx,%ebx
  80006b:	7e 07                	jle    800074 <libmain+0x36>
		binaryname = argv[0];
  80006d:	8b 06                	mov    (%esi),%eax
  80006f:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800074:	83 ec 08             	sub    $0x8,%esp
  800077:	56                   	push   %esi
  800078:	53                   	push   %ebx
  800079:	e8 b5 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80007e:	e8 0a 00 00 00       	call   80008d <exit>
}
  800083:	83 c4 10             	add    $0x10,%esp
  800086:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800089:	5b                   	pop    %ebx
  80008a:	5e                   	pop    %esi
  80008b:	5d                   	pop    %ebp
  80008c:	c3                   	ret    

0080008d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80008d:	55                   	push   %ebp
  80008e:	89 e5                	mov    %esp,%ebp
  800090:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800093:	6a 00                	push   $0x0
  800095:	e8 42 00 00 00       	call   8000dc <sys_env_destroy>
}
  80009a:	83 c4 10             	add    $0x10,%esp
  80009d:	c9                   	leave  
  80009e:	c3                   	ret    

0080009f <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009f:	55                   	push   %ebp
  8000a0:	89 e5                	mov    %esp,%ebp
  8000a2:	57                   	push   %edi
  8000a3:	56                   	push   %esi
  8000a4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a5:	b8 00 00 00 00       	mov    $0x0,%eax
  8000aa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000ad:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b0:	89 c3                	mov    %eax,%ebx
  8000b2:	89 c7                	mov    %eax,%edi
  8000b4:	89 c6                	mov    %eax,%esi
  8000b6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b8:	5b                   	pop    %ebx
  8000b9:	5e                   	pop    %esi
  8000ba:	5f                   	pop    %edi
  8000bb:	5d                   	pop    %ebp
  8000bc:	c3                   	ret    

008000bd <sys_cgetc>:

int
sys_cgetc(void)
{
  8000bd:	55                   	push   %ebp
  8000be:	89 e5                	mov    %esp,%ebp
  8000c0:	57                   	push   %edi
  8000c1:	56                   	push   %esi
  8000c2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000c3:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c8:	b8 01 00 00 00       	mov    $0x1,%eax
  8000cd:	89 d1                	mov    %edx,%ecx
  8000cf:	89 d3                	mov    %edx,%ebx
  8000d1:	89 d7                	mov    %edx,%edi
  8000d3:	89 d6                	mov    %edx,%esi
  8000d5:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d7:	5b                   	pop    %ebx
  8000d8:	5e                   	pop    %esi
  8000d9:	5f                   	pop    %edi
  8000da:	5d                   	pop    %ebp
  8000db:	c3                   	ret    

008000dc <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000dc:	55                   	push   %ebp
  8000dd:	89 e5                	mov    %esp,%ebp
  8000df:	57                   	push   %edi
  8000e0:	56                   	push   %esi
  8000e1:	53                   	push   %ebx
  8000e2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e5:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000ea:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ef:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f2:	89 cb                	mov    %ecx,%ebx
  8000f4:	89 cf                	mov    %ecx,%edi
  8000f6:	89 ce                	mov    %ecx,%esi
  8000f8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000fa:	85 c0                	test   %eax,%eax
  8000fc:	7e 17                	jle    800115 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000fe:	83 ec 0c             	sub    $0xc,%esp
  800101:	50                   	push   %eax
  800102:	6a 03                	push   $0x3
  800104:	68 0a 0f 80 00       	push   $0x800f0a
  800109:	6a 23                	push   $0x23
  80010b:	68 27 0f 80 00       	push   $0x800f27
  800110:	e8 14 02 00 00       	call   800329 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800115:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800118:	5b                   	pop    %ebx
  800119:	5e                   	pop    %esi
  80011a:	5f                   	pop    %edi
  80011b:	5d                   	pop    %ebp
  80011c:	c3                   	ret    

0080011d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  80011d:	55                   	push   %ebp
  80011e:	89 e5                	mov    %esp,%ebp
  800120:	57                   	push   %edi
  800121:	56                   	push   %esi
  800122:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800123:	ba 00 00 00 00       	mov    $0x0,%edx
  800128:	b8 02 00 00 00       	mov    $0x2,%eax
  80012d:	89 d1                	mov    %edx,%ecx
  80012f:	89 d3                	mov    %edx,%ebx
  800131:	89 d7                	mov    %edx,%edi
  800133:	89 d6                	mov    %edx,%esi
  800135:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800137:	5b                   	pop    %ebx
  800138:	5e                   	pop    %esi
  800139:	5f                   	pop    %edi
  80013a:	5d                   	pop    %ebp
  80013b:	c3                   	ret    

0080013c <sys_yield>:

void
sys_yield(void)
{
  80013c:	55                   	push   %ebp
  80013d:	89 e5                	mov    %esp,%ebp
  80013f:	57                   	push   %edi
  800140:	56                   	push   %esi
  800141:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800142:	ba 00 00 00 00       	mov    $0x0,%edx
  800147:	b8 0a 00 00 00       	mov    $0xa,%eax
  80014c:	89 d1                	mov    %edx,%ecx
  80014e:	89 d3                	mov    %edx,%ebx
  800150:	89 d7                	mov    %edx,%edi
  800152:	89 d6                	mov    %edx,%esi
  800154:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800156:	5b                   	pop    %ebx
  800157:	5e                   	pop    %esi
  800158:	5f                   	pop    %edi
  800159:	5d                   	pop    %ebp
  80015a:	c3                   	ret    

0080015b <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80015b:	55                   	push   %ebp
  80015c:	89 e5                	mov    %esp,%ebp
  80015e:	57                   	push   %edi
  80015f:	56                   	push   %esi
  800160:	53                   	push   %ebx
  800161:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800164:	be 00 00 00 00       	mov    $0x0,%esi
  800169:	b8 04 00 00 00       	mov    $0x4,%eax
  80016e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800171:	8b 55 08             	mov    0x8(%ebp),%edx
  800174:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800177:	89 f7                	mov    %esi,%edi
  800179:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80017b:	85 c0                	test   %eax,%eax
  80017d:	7e 17                	jle    800196 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80017f:	83 ec 0c             	sub    $0xc,%esp
  800182:	50                   	push   %eax
  800183:	6a 04                	push   $0x4
  800185:	68 0a 0f 80 00       	push   $0x800f0a
  80018a:	6a 23                	push   $0x23
  80018c:	68 27 0f 80 00       	push   $0x800f27
  800191:	e8 93 01 00 00       	call   800329 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800196:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800199:	5b                   	pop    %ebx
  80019a:	5e                   	pop    %esi
  80019b:	5f                   	pop    %edi
  80019c:	5d                   	pop    %ebp
  80019d:	c3                   	ret    

0080019e <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  80019e:	55                   	push   %ebp
  80019f:	89 e5                	mov    %esp,%ebp
  8001a1:	57                   	push   %edi
  8001a2:	56                   	push   %esi
  8001a3:	53                   	push   %ebx
  8001a4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001a7:	b8 05 00 00 00       	mov    $0x5,%eax
  8001ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001af:	8b 55 08             	mov    0x8(%ebp),%edx
  8001b2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b5:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001b8:	8b 75 18             	mov    0x18(%ebp),%esi
  8001bb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001bd:	85 c0                	test   %eax,%eax
  8001bf:	7e 17                	jle    8001d8 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001c1:	83 ec 0c             	sub    $0xc,%esp
  8001c4:	50                   	push   %eax
  8001c5:	6a 05                	push   $0x5
  8001c7:	68 0a 0f 80 00       	push   $0x800f0a
  8001cc:	6a 23                	push   $0x23
  8001ce:	68 27 0f 80 00       	push   $0x800f27
  8001d3:	e8 51 01 00 00       	call   800329 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001d8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001db:	5b                   	pop    %ebx
  8001dc:	5e                   	pop    %esi
  8001dd:	5f                   	pop    %edi
  8001de:	5d                   	pop    %ebp
  8001df:	c3                   	ret    

008001e0 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001e0:	55                   	push   %ebp
  8001e1:	89 e5                	mov    %esp,%ebp
  8001e3:	57                   	push   %edi
  8001e4:	56                   	push   %esi
  8001e5:	53                   	push   %ebx
  8001e6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001e9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001ee:	b8 06 00 00 00       	mov    $0x6,%eax
  8001f3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001f6:	8b 55 08             	mov    0x8(%ebp),%edx
  8001f9:	89 df                	mov    %ebx,%edi
  8001fb:	89 de                	mov    %ebx,%esi
  8001fd:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001ff:	85 c0                	test   %eax,%eax
  800201:	7e 17                	jle    80021a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800203:	83 ec 0c             	sub    $0xc,%esp
  800206:	50                   	push   %eax
  800207:	6a 06                	push   $0x6
  800209:	68 0a 0f 80 00       	push   $0x800f0a
  80020e:	6a 23                	push   $0x23
  800210:	68 27 0f 80 00       	push   $0x800f27
  800215:	e8 0f 01 00 00       	call   800329 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80021a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80021d:	5b                   	pop    %ebx
  80021e:	5e                   	pop    %esi
  80021f:	5f                   	pop    %edi
  800220:	5d                   	pop    %ebp
  800221:	c3                   	ret    

00800222 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800222:	55                   	push   %ebp
  800223:	89 e5                	mov    %esp,%ebp
  800225:	57                   	push   %edi
  800226:	56                   	push   %esi
  800227:	53                   	push   %ebx
  800228:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80022b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800230:	b8 08 00 00 00       	mov    $0x8,%eax
  800235:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800238:	8b 55 08             	mov    0x8(%ebp),%edx
  80023b:	89 df                	mov    %ebx,%edi
  80023d:	89 de                	mov    %ebx,%esi
  80023f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800241:	85 c0                	test   %eax,%eax
  800243:	7e 17                	jle    80025c <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800245:	83 ec 0c             	sub    $0xc,%esp
  800248:	50                   	push   %eax
  800249:	6a 08                	push   $0x8
  80024b:	68 0a 0f 80 00       	push   $0x800f0a
  800250:	6a 23                	push   $0x23
  800252:	68 27 0f 80 00       	push   $0x800f27
  800257:	e8 cd 00 00 00       	call   800329 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  80025c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80025f:	5b                   	pop    %ebx
  800260:	5e                   	pop    %esi
  800261:	5f                   	pop    %edi
  800262:	5d                   	pop    %ebp
  800263:	c3                   	ret    

00800264 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800264:	55                   	push   %ebp
  800265:	89 e5                	mov    %esp,%ebp
  800267:	57                   	push   %edi
  800268:	56                   	push   %esi
  800269:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80026a:	ba 00 00 00 00       	mov    $0x0,%edx
  80026f:	b8 0b 00 00 00       	mov    $0xb,%eax
  800274:	89 d1                	mov    %edx,%ecx
  800276:	89 d3                	mov    %edx,%ebx
  800278:	89 d7                	mov    %edx,%edi
  80027a:	89 d6                	mov    %edx,%esi
  80027c:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80027e:	5b                   	pop    %ebx
  80027f:	5e                   	pop    %esi
  800280:	5f                   	pop    %edi
  800281:	5d                   	pop    %ebp
  800282:	c3                   	ret    

00800283 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800283:	55                   	push   %ebp
  800284:	89 e5                	mov    %esp,%ebp
  800286:	57                   	push   %edi
  800287:	56                   	push   %esi
  800288:	53                   	push   %ebx
  800289:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80028c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800291:	b8 09 00 00 00       	mov    $0x9,%eax
  800296:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800299:	8b 55 08             	mov    0x8(%ebp),%edx
  80029c:	89 df                	mov    %ebx,%edi
  80029e:	89 de                	mov    %ebx,%esi
  8002a0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002a2:	85 c0                	test   %eax,%eax
  8002a4:	7e 17                	jle    8002bd <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002a6:	83 ec 0c             	sub    $0xc,%esp
  8002a9:	50                   	push   %eax
  8002aa:	6a 09                	push   $0x9
  8002ac:	68 0a 0f 80 00       	push   $0x800f0a
  8002b1:	6a 23                	push   $0x23
  8002b3:	68 27 0f 80 00       	push   $0x800f27
  8002b8:	e8 6c 00 00 00       	call   800329 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c0:	5b                   	pop    %ebx
  8002c1:	5e                   	pop    %esi
  8002c2:	5f                   	pop    %edi
  8002c3:	5d                   	pop    %ebp
  8002c4:	c3                   	ret    

008002c5 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002c5:	55                   	push   %ebp
  8002c6:	89 e5                	mov    %esp,%ebp
  8002c8:	57                   	push   %edi
  8002c9:	56                   	push   %esi
  8002ca:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002cb:	be 00 00 00 00       	mov    $0x0,%esi
  8002d0:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002d5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002d8:	8b 55 08             	mov    0x8(%ebp),%edx
  8002db:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002de:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002e1:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002e3:	5b                   	pop    %ebx
  8002e4:	5e                   	pop    %esi
  8002e5:	5f                   	pop    %edi
  8002e6:	5d                   	pop    %ebp
  8002e7:	c3                   	ret    

008002e8 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002e8:	55                   	push   %ebp
  8002e9:	89 e5                	mov    %esp,%ebp
  8002eb:	57                   	push   %edi
  8002ec:	56                   	push   %esi
  8002ed:	53                   	push   %ebx
  8002ee:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002f1:	b9 00 00 00 00       	mov    $0x0,%ecx
  8002f6:	b8 0d 00 00 00       	mov    $0xd,%eax
  8002fb:	8b 55 08             	mov    0x8(%ebp),%edx
  8002fe:	89 cb                	mov    %ecx,%ebx
  800300:	89 cf                	mov    %ecx,%edi
  800302:	89 ce                	mov    %ecx,%esi
  800304:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800306:	85 c0                	test   %eax,%eax
  800308:	7e 17                	jle    800321 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80030a:	83 ec 0c             	sub    $0xc,%esp
  80030d:	50                   	push   %eax
  80030e:	6a 0d                	push   $0xd
  800310:	68 0a 0f 80 00       	push   $0x800f0a
  800315:	6a 23                	push   $0x23
  800317:	68 27 0f 80 00       	push   $0x800f27
  80031c:	e8 08 00 00 00       	call   800329 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800321:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800324:	5b                   	pop    %ebx
  800325:	5e                   	pop    %esi
  800326:	5f                   	pop    %edi
  800327:	5d                   	pop    %ebp
  800328:	c3                   	ret    

00800329 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800329:	55                   	push   %ebp
  80032a:	89 e5                	mov    %esp,%ebp
  80032c:	56                   	push   %esi
  80032d:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80032e:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800331:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800337:	e8 e1 fd ff ff       	call   80011d <sys_getenvid>
  80033c:	83 ec 0c             	sub    $0xc,%esp
  80033f:	ff 75 0c             	pushl  0xc(%ebp)
  800342:	ff 75 08             	pushl  0x8(%ebp)
  800345:	56                   	push   %esi
  800346:	50                   	push   %eax
  800347:	68 38 0f 80 00       	push   $0x800f38
  80034c:	e8 b0 00 00 00       	call   800401 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800351:	83 c4 18             	add    $0x18,%esp
  800354:	53                   	push   %ebx
  800355:	ff 75 10             	pushl  0x10(%ebp)
  800358:	e8 53 00 00 00       	call   8003b0 <vcprintf>
	cprintf("\n");
  80035d:	c7 04 24 5c 0f 80 00 	movl   $0x800f5c,(%esp)
  800364:	e8 98 00 00 00       	call   800401 <cprintf>
  800369:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80036c:	cc                   	int3   
  80036d:	eb fd                	jmp    80036c <_panic+0x43>

0080036f <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80036f:	55                   	push   %ebp
  800370:	89 e5                	mov    %esp,%ebp
  800372:	53                   	push   %ebx
  800373:	83 ec 04             	sub    $0x4,%esp
  800376:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800379:	8b 13                	mov    (%ebx),%edx
  80037b:	8d 42 01             	lea    0x1(%edx),%eax
  80037e:	89 03                	mov    %eax,(%ebx)
  800380:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800383:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800387:	3d ff 00 00 00       	cmp    $0xff,%eax
  80038c:	75 1a                	jne    8003a8 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80038e:	83 ec 08             	sub    $0x8,%esp
  800391:	68 ff 00 00 00       	push   $0xff
  800396:	8d 43 08             	lea    0x8(%ebx),%eax
  800399:	50                   	push   %eax
  80039a:	e8 00 fd ff ff       	call   80009f <sys_cputs>
		b->idx = 0;
  80039f:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003a5:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003a8:	ff 43 04             	incl   0x4(%ebx)
}
  8003ab:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003ae:	c9                   	leave  
  8003af:	c3                   	ret    

008003b0 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003b0:	55                   	push   %ebp
  8003b1:	89 e5                	mov    %esp,%ebp
  8003b3:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003b9:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003c0:	00 00 00 
	b.cnt = 0;
  8003c3:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003ca:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003cd:	ff 75 0c             	pushl  0xc(%ebp)
  8003d0:	ff 75 08             	pushl  0x8(%ebp)
  8003d3:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003d9:	50                   	push   %eax
  8003da:	68 6f 03 80 00       	push   $0x80036f
  8003df:	e8 51 01 00 00       	call   800535 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003e4:	83 c4 08             	add    $0x8,%esp
  8003e7:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003ed:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003f3:	50                   	push   %eax
  8003f4:	e8 a6 fc ff ff       	call   80009f <sys_cputs>

	return b.cnt;
}
  8003f9:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8003ff:	c9                   	leave  
  800400:	c3                   	ret    

00800401 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800401:	55                   	push   %ebp
  800402:	89 e5                	mov    %esp,%ebp
  800404:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800407:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80040a:	50                   	push   %eax
  80040b:	ff 75 08             	pushl  0x8(%ebp)
  80040e:	e8 9d ff ff ff       	call   8003b0 <vcprintf>
	va_end(ap);

	return cnt;
}
  800413:	c9                   	leave  
  800414:	c3                   	ret    

00800415 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800415:	55                   	push   %ebp
  800416:	89 e5                	mov    %esp,%ebp
  800418:	57                   	push   %edi
  800419:	56                   	push   %esi
  80041a:	53                   	push   %ebx
  80041b:	83 ec 1c             	sub    $0x1c,%esp
  80041e:	89 c7                	mov    %eax,%edi
  800420:	89 d6                	mov    %edx,%esi
  800422:	8b 45 08             	mov    0x8(%ebp),%eax
  800425:	8b 55 0c             	mov    0xc(%ebp),%edx
  800428:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80042b:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80042e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800431:	bb 00 00 00 00       	mov    $0x0,%ebx
  800436:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800439:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80043c:	39 d3                	cmp    %edx,%ebx
  80043e:	72 05                	jb     800445 <printnum+0x30>
  800440:	39 45 10             	cmp    %eax,0x10(%ebp)
  800443:	77 45                	ja     80048a <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800445:	83 ec 0c             	sub    $0xc,%esp
  800448:	ff 75 18             	pushl  0x18(%ebp)
  80044b:	8b 45 14             	mov    0x14(%ebp),%eax
  80044e:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800451:	53                   	push   %ebx
  800452:	ff 75 10             	pushl  0x10(%ebp)
  800455:	83 ec 08             	sub    $0x8,%esp
  800458:	ff 75 e4             	pushl  -0x1c(%ebp)
  80045b:	ff 75 e0             	pushl  -0x20(%ebp)
  80045e:	ff 75 dc             	pushl  -0x24(%ebp)
  800461:	ff 75 d8             	pushl  -0x28(%ebp)
  800464:	e8 27 08 00 00       	call   800c90 <__udivdi3>
  800469:	83 c4 18             	add    $0x18,%esp
  80046c:	52                   	push   %edx
  80046d:	50                   	push   %eax
  80046e:	89 f2                	mov    %esi,%edx
  800470:	89 f8                	mov    %edi,%eax
  800472:	e8 9e ff ff ff       	call   800415 <printnum>
  800477:	83 c4 20             	add    $0x20,%esp
  80047a:	eb 16                	jmp    800492 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80047c:	83 ec 08             	sub    $0x8,%esp
  80047f:	56                   	push   %esi
  800480:	ff 75 18             	pushl  0x18(%ebp)
  800483:	ff d7                	call   *%edi
  800485:	83 c4 10             	add    $0x10,%esp
  800488:	eb 03                	jmp    80048d <printnum+0x78>
  80048a:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80048d:	4b                   	dec    %ebx
  80048e:	85 db                	test   %ebx,%ebx
  800490:	7f ea                	jg     80047c <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800492:	83 ec 08             	sub    $0x8,%esp
  800495:	56                   	push   %esi
  800496:	83 ec 04             	sub    $0x4,%esp
  800499:	ff 75 e4             	pushl  -0x1c(%ebp)
  80049c:	ff 75 e0             	pushl  -0x20(%ebp)
  80049f:	ff 75 dc             	pushl  -0x24(%ebp)
  8004a2:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a5:	e8 f6 08 00 00       	call   800da0 <__umoddi3>
  8004aa:	83 c4 14             	add    $0x14,%esp
  8004ad:	0f be 80 5e 0f 80 00 	movsbl 0x800f5e(%eax),%eax
  8004b4:	50                   	push   %eax
  8004b5:	ff d7                	call   *%edi
}
  8004b7:	83 c4 10             	add    $0x10,%esp
  8004ba:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004bd:	5b                   	pop    %ebx
  8004be:	5e                   	pop    %esi
  8004bf:	5f                   	pop    %edi
  8004c0:	5d                   	pop    %ebp
  8004c1:	c3                   	ret    

008004c2 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004c2:	55                   	push   %ebp
  8004c3:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004c5:	83 fa 01             	cmp    $0x1,%edx
  8004c8:	7e 0e                	jle    8004d8 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004ca:	8b 10                	mov    (%eax),%edx
  8004cc:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004cf:	89 08                	mov    %ecx,(%eax)
  8004d1:	8b 02                	mov    (%edx),%eax
  8004d3:	8b 52 04             	mov    0x4(%edx),%edx
  8004d6:	eb 22                	jmp    8004fa <getuint+0x38>
	else if (lflag)
  8004d8:	85 d2                	test   %edx,%edx
  8004da:	74 10                	je     8004ec <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004dc:	8b 10                	mov    (%eax),%edx
  8004de:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004e1:	89 08                	mov    %ecx,(%eax)
  8004e3:	8b 02                	mov    (%edx),%eax
  8004e5:	ba 00 00 00 00       	mov    $0x0,%edx
  8004ea:	eb 0e                	jmp    8004fa <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004ec:	8b 10                	mov    (%eax),%edx
  8004ee:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004f1:	89 08                	mov    %ecx,(%eax)
  8004f3:	8b 02                	mov    (%edx),%eax
  8004f5:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8004fa:	5d                   	pop    %ebp
  8004fb:	c3                   	ret    

008004fc <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8004fc:	55                   	push   %ebp
  8004fd:	89 e5                	mov    %esp,%ebp
  8004ff:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800502:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800505:	8b 10                	mov    (%eax),%edx
  800507:	3b 50 04             	cmp    0x4(%eax),%edx
  80050a:	73 0a                	jae    800516 <sprintputch+0x1a>
		*b->buf++ = ch;
  80050c:	8d 4a 01             	lea    0x1(%edx),%ecx
  80050f:	89 08                	mov    %ecx,(%eax)
  800511:	8b 45 08             	mov    0x8(%ebp),%eax
  800514:	88 02                	mov    %al,(%edx)
}
  800516:	5d                   	pop    %ebp
  800517:	c3                   	ret    

00800518 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800518:	55                   	push   %ebp
  800519:	89 e5                	mov    %esp,%ebp
  80051b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80051e:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800521:	50                   	push   %eax
  800522:	ff 75 10             	pushl  0x10(%ebp)
  800525:	ff 75 0c             	pushl  0xc(%ebp)
  800528:	ff 75 08             	pushl  0x8(%ebp)
  80052b:	e8 05 00 00 00       	call   800535 <vprintfmt>
	va_end(ap);
}
  800530:	83 c4 10             	add    $0x10,%esp
  800533:	c9                   	leave  
  800534:	c3                   	ret    

00800535 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800535:	55                   	push   %ebp
  800536:	89 e5                	mov    %esp,%ebp
  800538:	57                   	push   %edi
  800539:	56                   	push   %esi
  80053a:	53                   	push   %ebx
  80053b:	83 ec 2c             	sub    $0x2c,%esp
  80053e:	8b 75 08             	mov    0x8(%ebp),%esi
  800541:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800544:	8b 7d 10             	mov    0x10(%ebp),%edi
  800547:	eb 12                	jmp    80055b <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800549:	85 c0                	test   %eax,%eax
  80054b:	0f 84 68 03 00 00    	je     8008b9 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800551:	83 ec 08             	sub    $0x8,%esp
  800554:	53                   	push   %ebx
  800555:	50                   	push   %eax
  800556:	ff d6                	call   *%esi
  800558:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80055b:	47                   	inc    %edi
  80055c:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800560:	83 f8 25             	cmp    $0x25,%eax
  800563:	75 e4                	jne    800549 <vprintfmt+0x14>
  800565:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800569:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800570:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800577:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80057e:	ba 00 00 00 00       	mov    $0x0,%edx
  800583:	eb 07                	jmp    80058c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800585:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800588:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80058c:	8d 47 01             	lea    0x1(%edi),%eax
  80058f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800592:	0f b6 0f             	movzbl (%edi),%ecx
  800595:	8a 07                	mov    (%edi),%al
  800597:	83 e8 23             	sub    $0x23,%eax
  80059a:	3c 55                	cmp    $0x55,%al
  80059c:	0f 87 fe 02 00 00    	ja     8008a0 <vprintfmt+0x36b>
  8005a2:	0f b6 c0             	movzbl %al,%eax
  8005a5:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8005ac:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005af:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005b3:	eb d7                	jmp    80058c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005b8:	b8 00 00 00 00       	mov    $0x0,%eax
  8005bd:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005c0:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005c3:	01 c0                	add    %eax,%eax
  8005c5:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005c9:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005cc:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005cf:	83 fa 09             	cmp    $0x9,%edx
  8005d2:	77 34                	ja     800608 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005d4:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005d5:	eb e9                	jmp    8005c0 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005d7:	8b 45 14             	mov    0x14(%ebp),%eax
  8005da:	8d 48 04             	lea    0x4(%eax),%ecx
  8005dd:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005e0:	8b 00                	mov    (%eax),%eax
  8005e2:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005e8:	eb 24                	jmp    80060e <vprintfmt+0xd9>
  8005ea:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005ee:	79 07                	jns    8005f7 <vprintfmt+0xc2>
  8005f0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fa:	eb 90                	jmp    80058c <vprintfmt+0x57>
  8005fc:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8005ff:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800606:	eb 84                	jmp    80058c <vprintfmt+0x57>
  800608:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80060b:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80060e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800612:	0f 89 74 ff ff ff    	jns    80058c <vprintfmt+0x57>
				width = precision, precision = -1;
  800618:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80061b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80061e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800625:	e9 62 ff ff ff       	jmp    80058c <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80062a:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80062b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80062e:	e9 59 ff ff ff       	jmp    80058c <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800633:	8b 45 14             	mov    0x14(%ebp),%eax
  800636:	8d 50 04             	lea    0x4(%eax),%edx
  800639:	89 55 14             	mov    %edx,0x14(%ebp)
  80063c:	83 ec 08             	sub    $0x8,%esp
  80063f:	53                   	push   %ebx
  800640:	ff 30                	pushl  (%eax)
  800642:	ff d6                	call   *%esi
			break;
  800644:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800647:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80064a:	e9 0c ff ff ff       	jmp    80055b <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80064f:	8b 45 14             	mov    0x14(%ebp),%eax
  800652:	8d 50 04             	lea    0x4(%eax),%edx
  800655:	89 55 14             	mov    %edx,0x14(%ebp)
  800658:	8b 00                	mov    (%eax),%eax
  80065a:	85 c0                	test   %eax,%eax
  80065c:	79 02                	jns    800660 <vprintfmt+0x12b>
  80065e:	f7 d8                	neg    %eax
  800660:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800662:	83 f8 08             	cmp    $0x8,%eax
  800665:	7f 0b                	jg     800672 <vprintfmt+0x13d>
  800667:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  80066e:	85 c0                	test   %eax,%eax
  800670:	75 18                	jne    80068a <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800672:	52                   	push   %edx
  800673:	68 76 0f 80 00       	push   $0x800f76
  800678:	53                   	push   %ebx
  800679:	56                   	push   %esi
  80067a:	e8 99 fe ff ff       	call   800518 <printfmt>
  80067f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800682:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800685:	e9 d1 fe ff ff       	jmp    80055b <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80068a:	50                   	push   %eax
  80068b:	68 7f 0f 80 00       	push   $0x800f7f
  800690:	53                   	push   %ebx
  800691:	56                   	push   %esi
  800692:	e8 81 fe ff ff       	call   800518 <printfmt>
  800697:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80069a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80069d:	e9 b9 fe ff ff       	jmp    80055b <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006a2:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a5:	8d 50 04             	lea    0x4(%eax),%edx
  8006a8:	89 55 14             	mov    %edx,0x14(%ebp)
  8006ab:	8b 38                	mov    (%eax),%edi
  8006ad:	85 ff                	test   %edi,%edi
  8006af:	75 05                	jne    8006b6 <vprintfmt+0x181>
				p = "(null)";
  8006b1:	bf 6f 0f 80 00       	mov    $0x800f6f,%edi
			if (width > 0 && padc != '-')
  8006b6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006ba:	0f 8e 90 00 00 00    	jle    800750 <vprintfmt+0x21b>
  8006c0:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006c4:	0f 84 8e 00 00 00    	je     800758 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006ca:	83 ec 08             	sub    $0x8,%esp
  8006cd:	ff 75 d0             	pushl  -0x30(%ebp)
  8006d0:	57                   	push   %edi
  8006d1:	e8 70 02 00 00       	call   800946 <strnlen>
  8006d6:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006d9:	29 c1                	sub    %eax,%ecx
  8006db:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006de:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006e1:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006e5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006e8:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006eb:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006ed:	eb 0d                	jmp    8006fc <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006ef:	83 ec 08             	sub    $0x8,%esp
  8006f2:	53                   	push   %ebx
  8006f3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006f6:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f8:	4f                   	dec    %edi
  8006f9:	83 c4 10             	add    $0x10,%esp
  8006fc:	85 ff                	test   %edi,%edi
  8006fe:	7f ef                	jg     8006ef <vprintfmt+0x1ba>
  800700:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800703:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800706:	89 c8                	mov    %ecx,%eax
  800708:	85 c9                	test   %ecx,%ecx
  80070a:	79 05                	jns    800711 <vprintfmt+0x1dc>
  80070c:	b8 00 00 00 00       	mov    $0x0,%eax
  800711:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800714:	29 c1                	sub    %eax,%ecx
  800716:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800719:	89 75 08             	mov    %esi,0x8(%ebp)
  80071c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80071f:	eb 3d                	jmp    80075e <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800721:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800725:	74 19                	je     800740 <vprintfmt+0x20b>
  800727:	0f be c0             	movsbl %al,%eax
  80072a:	83 e8 20             	sub    $0x20,%eax
  80072d:	83 f8 5e             	cmp    $0x5e,%eax
  800730:	76 0e                	jbe    800740 <vprintfmt+0x20b>
					putch('?', putdat);
  800732:	83 ec 08             	sub    $0x8,%esp
  800735:	53                   	push   %ebx
  800736:	6a 3f                	push   $0x3f
  800738:	ff 55 08             	call   *0x8(%ebp)
  80073b:	83 c4 10             	add    $0x10,%esp
  80073e:	eb 0b                	jmp    80074b <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800740:	83 ec 08             	sub    $0x8,%esp
  800743:	53                   	push   %ebx
  800744:	52                   	push   %edx
  800745:	ff 55 08             	call   *0x8(%ebp)
  800748:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80074b:	ff 4d e4             	decl   -0x1c(%ebp)
  80074e:	eb 0e                	jmp    80075e <vprintfmt+0x229>
  800750:	89 75 08             	mov    %esi,0x8(%ebp)
  800753:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800756:	eb 06                	jmp    80075e <vprintfmt+0x229>
  800758:	89 75 08             	mov    %esi,0x8(%ebp)
  80075b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075e:	47                   	inc    %edi
  80075f:	8a 47 ff             	mov    -0x1(%edi),%al
  800762:	0f be d0             	movsbl %al,%edx
  800765:	85 d2                	test   %edx,%edx
  800767:	74 1d                	je     800786 <vprintfmt+0x251>
  800769:	85 f6                	test   %esi,%esi
  80076b:	78 b4                	js     800721 <vprintfmt+0x1ec>
  80076d:	4e                   	dec    %esi
  80076e:	79 b1                	jns    800721 <vprintfmt+0x1ec>
  800770:	8b 75 08             	mov    0x8(%ebp),%esi
  800773:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800776:	eb 14                	jmp    80078c <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800778:	83 ec 08             	sub    $0x8,%esp
  80077b:	53                   	push   %ebx
  80077c:	6a 20                	push   $0x20
  80077e:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800780:	4f                   	dec    %edi
  800781:	83 c4 10             	add    $0x10,%esp
  800784:	eb 06                	jmp    80078c <vprintfmt+0x257>
  800786:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800789:	8b 75 08             	mov    0x8(%ebp),%esi
  80078c:	85 ff                	test   %edi,%edi
  80078e:	7f e8                	jg     800778 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800790:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800793:	e9 c3 fd ff ff       	jmp    80055b <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800798:	83 fa 01             	cmp    $0x1,%edx
  80079b:	7e 16                	jle    8007b3 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80079d:	8b 45 14             	mov    0x14(%ebp),%eax
  8007a0:	8d 50 08             	lea    0x8(%eax),%edx
  8007a3:	89 55 14             	mov    %edx,0x14(%ebp)
  8007a6:	8b 50 04             	mov    0x4(%eax),%edx
  8007a9:	8b 00                	mov    (%eax),%eax
  8007ab:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007ae:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007b1:	eb 32                	jmp    8007e5 <vprintfmt+0x2b0>
	else if (lflag)
  8007b3:	85 d2                	test   %edx,%edx
  8007b5:	74 18                	je     8007cf <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007b7:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ba:	8d 50 04             	lea    0x4(%eax),%edx
  8007bd:	89 55 14             	mov    %edx,0x14(%ebp)
  8007c0:	8b 00                	mov    (%eax),%eax
  8007c2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007c5:	89 c1                	mov    %eax,%ecx
  8007c7:	c1 f9 1f             	sar    $0x1f,%ecx
  8007ca:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007cd:	eb 16                	jmp    8007e5 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007cf:	8b 45 14             	mov    0x14(%ebp),%eax
  8007d2:	8d 50 04             	lea    0x4(%eax),%edx
  8007d5:	89 55 14             	mov    %edx,0x14(%ebp)
  8007d8:	8b 00                	mov    (%eax),%eax
  8007da:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007dd:	89 c1                	mov    %eax,%ecx
  8007df:	c1 f9 1f             	sar    $0x1f,%ecx
  8007e2:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007e5:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007e8:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007eb:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007ef:	79 76                	jns    800867 <vprintfmt+0x332>
				putch('-', putdat);
  8007f1:	83 ec 08             	sub    $0x8,%esp
  8007f4:	53                   	push   %ebx
  8007f5:	6a 2d                	push   $0x2d
  8007f7:	ff d6                	call   *%esi
				num = -(long long) num;
  8007f9:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007fc:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8007ff:	f7 d8                	neg    %eax
  800801:	83 d2 00             	adc    $0x0,%edx
  800804:	f7 da                	neg    %edx
  800806:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800809:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80080e:	eb 5c                	jmp    80086c <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800810:	8d 45 14             	lea    0x14(%ebp),%eax
  800813:	e8 aa fc ff ff       	call   8004c2 <getuint>
			base = 10;
  800818:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80081d:	eb 4d                	jmp    80086c <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80081f:	8d 45 14             	lea    0x14(%ebp),%eax
  800822:	e8 9b fc ff ff       	call   8004c2 <getuint>
			base = 8;
  800827:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80082c:	eb 3e                	jmp    80086c <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80082e:	83 ec 08             	sub    $0x8,%esp
  800831:	53                   	push   %ebx
  800832:	6a 30                	push   $0x30
  800834:	ff d6                	call   *%esi
			putch('x', putdat);
  800836:	83 c4 08             	add    $0x8,%esp
  800839:	53                   	push   %ebx
  80083a:	6a 78                	push   $0x78
  80083c:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80083e:	8b 45 14             	mov    0x14(%ebp),%eax
  800841:	8d 50 04             	lea    0x4(%eax),%edx
  800844:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800847:	8b 00                	mov    (%eax),%eax
  800849:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80084e:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800851:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800856:	eb 14                	jmp    80086c <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800858:	8d 45 14             	lea    0x14(%ebp),%eax
  80085b:	e8 62 fc ff ff       	call   8004c2 <getuint>
			base = 16;
  800860:	b9 10 00 00 00       	mov    $0x10,%ecx
  800865:	eb 05                	jmp    80086c <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800867:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80086c:	83 ec 0c             	sub    $0xc,%esp
  80086f:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800873:	57                   	push   %edi
  800874:	ff 75 e4             	pushl  -0x1c(%ebp)
  800877:	51                   	push   %ecx
  800878:	52                   	push   %edx
  800879:	50                   	push   %eax
  80087a:	89 da                	mov    %ebx,%edx
  80087c:	89 f0                	mov    %esi,%eax
  80087e:	e8 92 fb ff ff       	call   800415 <printnum>
			break;
  800883:	83 c4 20             	add    $0x20,%esp
  800886:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800889:	e9 cd fc ff ff       	jmp    80055b <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80088e:	83 ec 08             	sub    $0x8,%esp
  800891:	53                   	push   %ebx
  800892:	51                   	push   %ecx
  800893:	ff d6                	call   *%esi
			break;
  800895:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800898:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80089b:	e9 bb fc ff ff       	jmp    80055b <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008a0:	83 ec 08             	sub    $0x8,%esp
  8008a3:	53                   	push   %ebx
  8008a4:	6a 25                	push   $0x25
  8008a6:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008a8:	83 c4 10             	add    $0x10,%esp
  8008ab:	eb 01                	jmp    8008ae <vprintfmt+0x379>
  8008ad:	4f                   	dec    %edi
  8008ae:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008b2:	75 f9                	jne    8008ad <vprintfmt+0x378>
  8008b4:	e9 a2 fc ff ff       	jmp    80055b <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008b9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008bc:	5b                   	pop    %ebx
  8008bd:	5e                   	pop    %esi
  8008be:	5f                   	pop    %edi
  8008bf:	5d                   	pop    %ebp
  8008c0:	c3                   	ret    

008008c1 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008c1:	55                   	push   %ebp
  8008c2:	89 e5                	mov    %esp,%ebp
  8008c4:	83 ec 18             	sub    $0x18,%esp
  8008c7:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ca:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008cd:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008d0:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008d4:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008d7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008de:	85 c0                	test   %eax,%eax
  8008e0:	74 26                	je     800908 <vsnprintf+0x47>
  8008e2:	85 d2                	test   %edx,%edx
  8008e4:	7e 29                	jle    80090f <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008e6:	ff 75 14             	pushl  0x14(%ebp)
  8008e9:	ff 75 10             	pushl  0x10(%ebp)
  8008ec:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008ef:	50                   	push   %eax
  8008f0:	68 fc 04 80 00       	push   $0x8004fc
  8008f5:	e8 3b fc ff ff       	call   800535 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8008fa:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8008fd:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800900:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800903:	83 c4 10             	add    $0x10,%esp
  800906:	eb 0c                	jmp    800914 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800908:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80090d:	eb 05                	jmp    800914 <vsnprintf+0x53>
  80090f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800914:	c9                   	leave  
  800915:	c3                   	ret    

00800916 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800916:	55                   	push   %ebp
  800917:	89 e5                	mov    %esp,%ebp
  800919:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80091c:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80091f:	50                   	push   %eax
  800920:	ff 75 10             	pushl  0x10(%ebp)
  800923:	ff 75 0c             	pushl  0xc(%ebp)
  800926:	ff 75 08             	pushl  0x8(%ebp)
  800929:	e8 93 ff ff ff       	call   8008c1 <vsnprintf>
	va_end(ap);

	return rc;
}
  80092e:	c9                   	leave  
  80092f:	c3                   	ret    

00800930 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800930:	55                   	push   %ebp
  800931:	89 e5                	mov    %esp,%ebp
  800933:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800936:	b8 00 00 00 00       	mov    $0x0,%eax
  80093b:	eb 01                	jmp    80093e <strlen+0xe>
		n++;
  80093d:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80093e:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800942:	75 f9                	jne    80093d <strlen+0xd>
		n++;
	return n;
}
  800944:	5d                   	pop    %ebp
  800945:	c3                   	ret    

00800946 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800946:	55                   	push   %ebp
  800947:	89 e5                	mov    %esp,%ebp
  800949:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80094c:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80094f:	ba 00 00 00 00       	mov    $0x0,%edx
  800954:	eb 01                	jmp    800957 <strnlen+0x11>
		n++;
  800956:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800957:	39 c2                	cmp    %eax,%edx
  800959:	74 08                	je     800963 <strnlen+0x1d>
  80095b:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80095f:	75 f5                	jne    800956 <strnlen+0x10>
  800961:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800963:	5d                   	pop    %ebp
  800964:	c3                   	ret    

00800965 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800965:	55                   	push   %ebp
  800966:	89 e5                	mov    %esp,%ebp
  800968:	53                   	push   %ebx
  800969:	8b 45 08             	mov    0x8(%ebp),%eax
  80096c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80096f:	89 c2                	mov    %eax,%edx
  800971:	42                   	inc    %edx
  800972:	41                   	inc    %ecx
  800973:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800976:	88 5a ff             	mov    %bl,-0x1(%edx)
  800979:	84 db                	test   %bl,%bl
  80097b:	75 f4                	jne    800971 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80097d:	5b                   	pop    %ebx
  80097e:	5d                   	pop    %ebp
  80097f:	c3                   	ret    

00800980 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800980:	55                   	push   %ebp
  800981:	89 e5                	mov    %esp,%ebp
  800983:	53                   	push   %ebx
  800984:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800987:	53                   	push   %ebx
  800988:	e8 a3 ff ff ff       	call   800930 <strlen>
  80098d:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800990:	ff 75 0c             	pushl  0xc(%ebp)
  800993:	01 d8                	add    %ebx,%eax
  800995:	50                   	push   %eax
  800996:	e8 ca ff ff ff       	call   800965 <strcpy>
	return dst;
}
  80099b:	89 d8                	mov    %ebx,%eax
  80099d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009a0:	c9                   	leave  
  8009a1:	c3                   	ret    

008009a2 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009a2:	55                   	push   %ebp
  8009a3:	89 e5                	mov    %esp,%ebp
  8009a5:	56                   	push   %esi
  8009a6:	53                   	push   %ebx
  8009a7:	8b 75 08             	mov    0x8(%ebp),%esi
  8009aa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009ad:	89 f3                	mov    %esi,%ebx
  8009af:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009b2:	89 f2                	mov    %esi,%edx
  8009b4:	eb 0c                	jmp    8009c2 <strncpy+0x20>
		*dst++ = *src;
  8009b6:	42                   	inc    %edx
  8009b7:	8a 01                	mov    (%ecx),%al
  8009b9:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009bc:	80 39 01             	cmpb   $0x1,(%ecx)
  8009bf:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009c2:	39 da                	cmp    %ebx,%edx
  8009c4:	75 f0                	jne    8009b6 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009c6:	89 f0                	mov    %esi,%eax
  8009c8:	5b                   	pop    %ebx
  8009c9:	5e                   	pop    %esi
  8009ca:	5d                   	pop    %ebp
  8009cb:	c3                   	ret    

008009cc <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009cc:	55                   	push   %ebp
  8009cd:	89 e5                	mov    %esp,%ebp
  8009cf:	56                   	push   %esi
  8009d0:	53                   	push   %ebx
  8009d1:	8b 75 08             	mov    0x8(%ebp),%esi
  8009d4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009d7:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009da:	85 c0                	test   %eax,%eax
  8009dc:	74 1e                	je     8009fc <strlcpy+0x30>
  8009de:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009e2:	89 f2                	mov    %esi,%edx
  8009e4:	eb 05                	jmp    8009eb <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009e6:	42                   	inc    %edx
  8009e7:	41                   	inc    %ecx
  8009e8:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009eb:	39 c2                	cmp    %eax,%edx
  8009ed:	74 08                	je     8009f7 <strlcpy+0x2b>
  8009ef:	8a 19                	mov    (%ecx),%bl
  8009f1:	84 db                	test   %bl,%bl
  8009f3:	75 f1                	jne    8009e6 <strlcpy+0x1a>
  8009f5:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8009f7:	c6 00 00             	movb   $0x0,(%eax)
  8009fa:	eb 02                	jmp    8009fe <strlcpy+0x32>
  8009fc:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8009fe:	29 f0                	sub    %esi,%eax
}
  800a00:	5b                   	pop    %ebx
  800a01:	5e                   	pop    %esi
  800a02:	5d                   	pop    %ebp
  800a03:	c3                   	ret    

00800a04 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a04:	55                   	push   %ebp
  800a05:	89 e5                	mov    %esp,%ebp
  800a07:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a0a:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a0d:	eb 02                	jmp    800a11 <strcmp+0xd>
		p++, q++;
  800a0f:	41                   	inc    %ecx
  800a10:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a11:	8a 01                	mov    (%ecx),%al
  800a13:	84 c0                	test   %al,%al
  800a15:	74 04                	je     800a1b <strcmp+0x17>
  800a17:	3a 02                	cmp    (%edx),%al
  800a19:	74 f4                	je     800a0f <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a1b:	0f b6 c0             	movzbl %al,%eax
  800a1e:	0f b6 12             	movzbl (%edx),%edx
  800a21:	29 d0                	sub    %edx,%eax
}
  800a23:	5d                   	pop    %ebp
  800a24:	c3                   	ret    

00800a25 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a25:	55                   	push   %ebp
  800a26:	89 e5                	mov    %esp,%ebp
  800a28:	53                   	push   %ebx
  800a29:	8b 45 08             	mov    0x8(%ebp),%eax
  800a2c:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a2f:	89 c3                	mov    %eax,%ebx
  800a31:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a34:	eb 02                	jmp    800a38 <strncmp+0x13>
		n--, p++, q++;
  800a36:	40                   	inc    %eax
  800a37:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a38:	39 d8                	cmp    %ebx,%eax
  800a3a:	74 14                	je     800a50 <strncmp+0x2b>
  800a3c:	8a 08                	mov    (%eax),%cl
  800a3e:	84 c9                	test   %cl,%cl
  800a40:	74 04                	je     800a46 <strncmp+0x21>
  800a42:	3a 0a                	cmp    (%edx),%cl
  800a44:	74 f0                	je     800a36 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a46:	0f b6 00             	movzbl (%eax),%eax
  800a49:	0f b6 12             	movzbl (%edx),%edx
  800a4c:	29 d0                	sub    %edx,%eax
  800a4e:	eb 05                	jmp    800a55 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a50:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a55:	5b                   	pop    %ebx
  800a56:	5d                   	pop    %ebp
  800a57:	c3                   	ret    

00800a58 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a58:	55                   	push   %ebp
  800a59:	89 e5                	mov    %esp,%ebp
  800a5b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a5e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a61:	eb 05                	jmp    800a68 <strchr+0x10>
		if (*s == c)
  800a63:	38 ca                	cmp    %cl,%dl
  800a65:	74 0c                	je     800a73 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a67:	40                   	inc    %eax
  800a68:	8a 10                	mov    (%eax),%dl
  800a6a:	84 d2                	test   %dl,%dl
  800a6c:	75 f5                	jne    800a63 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a6e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a73:	5d                   	pop    %ebp
  800a74:	c3                   	ret    

00800a75 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a75:	55                   	push   %ebp
  800a76:	89 e5                	mov    %esp,%ebp
  800a78:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a7e:	eb 05                	jmp    800a85 <strfind+0x10>
		if (*s == c)
  800a80:	38 ca                	cmp    %cl,%dl
  800a82:	74 07                	je     800a8b <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a84:	40                   	inc    %eax
  800a85:	8a 10                	mov    (%eax),%dl
  800a87:	84 d2                	test   %dl,%dl
  800a89:	75 f5                	jne    800a80 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a8b:	5d                   	pop    %ebp
  800a8c:	c3                   	ret    

00800a8d <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a8d:	55                   	push   %ebp
  800a8e:	89 e5                	mov    %esp,%ebp
  800a90:	57                   	push   %edi
  800a91:	56                   	push   %esi
  800a92:	53                   	push   %ebx
  800a93:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a96:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a99:	85 c9                	test   %ecx,%ecx
  800a9b:	74 36                	je     800ad3 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800a9d:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800aa3:	75 28                	jne    800acd <memset+0x40>
  800aa5:	f6 c1 03             	test   $0x3,%cl
  800aa8:	75 23                	jne    800acd <memset+0x40>
		c &= 0xFF;
  800aaa:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aae:	89 d3                	mov    %edx,%ebx
  800ab0:	c1 e3 08             	shl    $0x8,%ebx
  800ab3:	89 d6                	mov    %edx,%esi
  800ab5:	c1 e6 18             	shl    $0x18,%esi
  800ab8:	89 d0                	mov    %edx,%eax
  800aba:	c1 e0 10             	shl    $0x10,%eax
  800abd:	09 f0                	or     %esi,%eax
  800abf:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800ac1:	89 d8                	mov    %ebx,%eax
  800ac3:	09 d0                	or     %edx,%eax
  800ac5:	c1 e9 02             	shr    $0x2,%ecx
  800ac8:	fc                   	cld    
  800ac9:	f3 ab                	rep stos %eax,%es:(%edi)
  800acb:	eb 06                	jmp    800ad3 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800acd:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ad0:	fc                   	cld    
  800ad1:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800ad3:	89 f8                	mov    %edi,%eax
  800ad5:	5b                   	pop    %ebx
  800ad6:	5e                   	pop    %esi
  800ad7:	5f                   	pop    %edi
  800ad8:	5d                   	pop    %ebp
  800ad9:	c3                   	ret    

00800ada <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ada:	55                   	push   %ebp
  800adb:	89 e5                	mov    %esp,%ebp
  800add:	57                   	push   %edi
  800ade:	56                   	push   %esi
  800adf:	8b 45 08             	mov    0x8(%ebp),%eax
  800ae2:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ae5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ae8:	39 c6                	cmp    %eax,%esi
  800aea:	73 33                	jae    800b1f <memmove+0x45>
  800aec:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800aef:	39 d0                	cmp    %edx,%eax
  800af1:	73 2c                	jae    800b1f <memmove+0x45>
		s += n;
		d += n;
  800af3:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800af6:	89 d6                	mov    %edx,%esi
  800af8:	09 fe                	or     %edi,%esi
  800afa:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b00:	75 13                	jne    800b15 <memmove+0x3b>
  800b02:	f6 c1 03             	test   $0x3,%cl
  800b05:	75 0e                	jne    800b15 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b07:	83 ef 04             	sub    $0x4,%edi
  800b0a:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b0d:	c1 e9 02             	shr    $0x2,%ecx
  800b10:	fd                   	std    
  800b11:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b13:	eb 07                	jmp    800b1c <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b15:	4f                   	dec    %edi
  800b16:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b19:	fd                   	std    
  800b1a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b1c:	fc                   	cld    
  800b1d:	eb 1d                	jmp    800b3c <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b1f:	89 f2                	mov    %esi,%edx
  800b21:	09 c2                	or     %eax,%edx
  800b23:	f6 c2 03             	test   $0x3,%dl
  800b26:	75 0f                	jne    800b37 <memmove+0x5d>
  800b28:	f6 c1 03             	test   $0x3,%cl
  800b2b:	75 0a                	jne    800b37 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b2d:	c1 e9 02             	shr    $0x2,%ecx
  800b30:	89 c7                	mov    %eax,%edi
  800b32:	fc                   	cld    
  800b33:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b35:	eb 05                	jmp    800b3c <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b37:	89 c7                	mov    %eax,%edi
  800b39:	fc                   	cld    
  800b3a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b3c:	5e                   	pop    %esi
  800b3d:	5f                   	pop    %edi
  800b3e:	5d                   	pop    %ebp
  800b3f:	c3                   	ret    

00800b40 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b40:	55                   	push   %ebp
  800b41:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b43:	ff 75 10             	pushl  0x10(%ebp)
  800b46:	ff 75 0c             	pushl  0xc(%ebp)
  800b49:	ff 75 08             	pushl  0x8(%ebp)
  800b4c:	e8 89 ff ff ff       	call   800ada <memmove>
}
  800b51:	c9                   	leave  
  800b52:	c3                   	ret    

00800b53 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b53:	55                   	push   %ebp
  800b54:	89 e5                	mov    %esp,%ebp
  800b56:	56                   	push   %esi
  800b57:	53                   	push   %ebx
  800b58:	8b 45 08             	mov    0x8(%ebp),%eax
  800b5b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b5e:	89 c6                	mov    %eax,%esi
  800b60:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b63:	eb 14                	jmp    800b79 <memcmp+0x26>
		if (*s1 != *s2)
  800b65:	8a 08                	mov    (%eax),%cl
  800b67:	8a 1a                	mov    (%edx),%bl
  800b69:	38 d9                	cmp    %bl,%cl
  800b6b:	74 0a                	je     800b77 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b6d:	0f b6 c1             	movzbl %cl,%eax
  800b70:	0f b6 db             	movzbl %bl,%ebx
  800b73:	29 d8                	sub    %ebx,%eax
  800b75:	eb 0b                	jmp    800b82 <memcmp+0x2f>
		s1++, s2++;
  800b77:	40                   	inc    %eax
  800b78:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b79:	39 f0                	cmp    %esi,%eax
  800b7b:	75 e8                	jne    800b65 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b7d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b82:	5b                   	pop    %ebx
  800b83:	5e                   	pop    %esi
  800b84:	5d                   	pop    %ebp
  800b85:	c3                   	ret    

00800b86 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b86:	55                   	push   %ebp
  800b87:	89 e5                	mov    %esp,%ebp
  800b89:	53                   	push   %ebx
  800b8a:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b8d:	89 c1                	mov    %eax,%ecx
  800b8f:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b92:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b96:	eb 08                	jmp    800ba0 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b98:	0f b6 10             	movzbl (%eax),%edx
  800b9b:	39 da                	cmp    %ebx,%edx
  800b9d:	74 05                	je     800ba4 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b9f:	40                   	inc    %eax
  800ba0:	39 c8                	cmp    %ecx,%eax
  800ba2:	72 f4                	jb     800b98 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800ba4:	5b                   	pop    %ebx
  800ba5:	5d                   	pop    %ebp
  800ba6:	c3                   	ret    

00800ba7 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800ba7:	55                   	push   %ebp
  800ba8:	89 e5                	mov    %esp,%ebp
  800baa:	57                   	push   %edi
  800bab:	56                   	push   %esi
  800bac:	53                   	push   %ebx
  800bad:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb0:	eb 01                	jmp    800bb3 <strtol+0xc>
		s++;
  800bb2:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb3:	8a 01                	mov    (%ecx),%al
  800bb5:	3c 20                	cmp    $0x20,%al
  800bb7:	74 f9                	je     800bb2 <strtol+0xb>
  800bb9:	3c 09                	cmp    $0x9,%al
  800bbb:	74 f5                	je     800bb2 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bbd:	3c 2b                	cmp    $0x2b,%al
  800bbf:	75 08                	jne    800bc9 <strtol+0x22>
		s++;
  800bc1:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bc2:	bf 00 00 00 00       	mov    $0x0,%edi
  800bc7:	eb 11                	jmp    800bda <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bc9:	3c 2d                	cmp    $0x2d,%al
  800bcb:	75 08                	jne    800bd5 <strtol+0x2e>
		s++, neg = 1;
  800bcd:	41                   	inc    %ecx
  800bce:	bf 01 00 00 00       	mov    $0x1,%edi
  800bd3:	eb 05                	jmp    800bda <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bd5:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bda:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bde:	0f 84 87 00 00 00    	je     800c6b <strtol+0xc4>
  800be4:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800be8:	75 27                	jne    800c11 <strtol+0x6a>
  800bea:	80 39 30             	cmpb   $0x30,(%ecx)
  800bed:	75 22                	jne    800c11 <strtol+0x6a>
  800bef:	e9 88 00 00 00       	jmp    800c7c <strtol+0xd5>
		s += 2, base = 16;
  800bf4:	83 c1 02             	add    $0x2,%ecx
  800bf7:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800bfe:	eb 11                	jmp    800c11 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c00:	41                   	inc    %ecx
  800c01:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c08:	eb 07                	jmp    800c11 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c0a:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c11:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c16:	8a 11                	mov    (%ecx),%dl
  800c18:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c1b:	80 fb 09             	cmp    $0x9,%bl
  800c1e:	77 08                	ja     800c28 <strtol+0x81>
			dig = *s - '0';
  800c20:	0f be d2             	movsbl %dl,%edx
  800c23:	83 ea 30             	sub    $0x30,%edx
  800c26:	eb 22                	jmp    800c4a <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c28:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c2b:	89 f3                	mov    %esi,%ebx
  800c2d:	80 fb 19             	cmp    $0x19,%bl
  800c30:	77 08                	ja     800c3a <strtol+0x93>
			dig = *s - 'a' + 10;
  800c32:	0f be d2             	movsbl %dl,%edx
  800c35:	83 ea 57             	sub    $0x57,%edx
  800c38:	eb 10                	jmp    800c4a <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c3a:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c3d:	89 f3                	mov    %esi,%ebx
  800c3f:	80 fb 19             	cmp    $0x19,%bl
  800c42:	77 14                	ja     800c58 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c44:	0f be d2             	movsbl %dl,%edx
  800c47:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c4a:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c4d:	7d 09                	jge    800c58 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c4f:	41                   	inc    %ecx
  800c50:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c54:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c56:	eb be                	jmp    800c16 <strtol+0x6f>

	if (endptr)
  800c58:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c5c:	74 05                	je     800c63 <strtol+0xbc>
		*endptr = (char *) s;
  800c5e:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c61:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c63:	85 ff                	test   %edi,%edi
  800c65:	74 21                	je     800c88 <strtol+0xe1>
  800c67:	f7 d8                	neg    %eax
  800c69:	eb 1d                	jmp    800c88 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c6b:	80 39 30             	cmpb   $0x30,(%ecx)
  800c6e:	75 9a                	jne    800c0a <strtol+0x63>
  800c70:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c74:	0f 84 7a ff ff ff    	je     800bf4 <strtol+0x4d>
  800c7a:	eb 84                	jmp    800c00 <strtol+0x59>
  800c7c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c80:	0f 84 6e ff ff ff    	je     800bf4 <strtol+0x4d>
  800c86:	eb 89                	jmp    800c11 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c88:	5b                   	pop    %ebx
  800c89:	5e                   	pop    %esi
  800c8a:	5f                   	pop    %edi
  800c8b:	5d                   	pop    %ebp
  800c8c:	c3                   	ret    
  800c8d:	66 90                	xchg   %ax,%ax
  800c8f:	90                   	nop

00800c90 <__udivdi3>:
  800c90:	55                   	push   %ebp
  800c91:	57                   	push   %edi
  800c92:	56                   	push   %esi
  800c93:	53                   	push   %ebx
  800c94:	83 ec 1c             	sub    $0x1c,%esp
  800c97:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800c9b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800c9f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ca3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ca7:	89 ca                	mov    %ecx,%edx
  800ca9:	89 f8                	mov    %edi,%eax
  800cab:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800caf:	85 f6                	test   %esi,%esi
  800cb1:	75 2d                	jne    800ce0 <__udivdi3+0x50>
  800cb3:	39 cf                	cmp    %ecx,%edi
  800cb5:	77 65                	ja     800d1c <__udivdi3+0x8c>
  800cb7:	89 fd                	mov    %edi,%ebp
  800cb9:	85 ff                	test   %edi,%edi
  800cbb:	75 0b                	jne    800cc8 <__udivdi3+0x38>
  800cbd:	b8 01 00 00 00       	mov    $0x1,%eax
  800cc2:	31 d2                	xor    %edx,%edx
  800cc4:	f7 f7                	div    %edi
  800cc6:	89 c5                	mov    %eax,%ebp
  800cc8:	31 d2                	xor    %edx,%edx
  800cca:	89 c8                	mov    %ecx,%eax
  800ccc:	f7 f5                	div    %ebp
  800cce:	89 c1                	mov    %eax,%ecx
  800cd0:	89 d8                	mov    %ebx,%eax
  800cd2:	f7 f5                	div    %ebp
  800cd4:	89 cf                	mov    %ecx,%edi
  800cd6:	89 fa                	mov    %edi,%edx
  800cd8:	83 c4 1c             	add    $0x1c,%esp
  800cdb:	5b                   	pop    %ebx
  800cdc:	5e                   	pop    %esi
  800cdd:	5f                   	pop    %edi
  800cde:	5d                   	pop    %ebp
  800cdf:	c3                   	ret    
  800ce0:	39 ce                	cmp    %ecx,%esi
  800ce2:	77 28                	ja     800d0c <__udivdi3+0x7c>
  800ce4:	0f bd fe             	bsr    %esi,%edi
  800ce7:	83 f7 1f             	xor    $0x1f,%edi
  800cea:	75 40                	jne    800d2c <__udivdi3+0x9c>
  800cec:	39 ce                	cmp    %ecx,%esi
  800cee:	72 0a                	jb     800cfa <__udivdi3+0x6a>
  800cf0:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800cf4:	0f 87 9e 00 00 00    	ja     800d98 <__udivdi3+0x108>
  800cfa:	b8 01 00 00 00       	mov    $0x1,%eax
  800cff:	89 fa                	mov    %edi,%edx
  800d01:	83 c4 1c             	add    $0x1c,%esp
  800d04:	5b                   	pop    %ebx
  800d05:	5e                   	pop    %esi
  800d06:	5f                   	pop    %edi
  800d07:	5d                   	pop    %ebp
  800d08:	c3                   	ret    
  800d09:	8d 76 00             	lea    0x0(%esi),%esi
  800d0c:	31 ff                	xor    %edi,%edi
  800d0e:	31 c0                	xor    %eax,%eax
  800d10:	89 fa                	mov    %edi,%edx
  800d12:	83 c4 1c             	add    $0x1c,%esp
  800d15:	5b                   	pop    %ebx
  800d16:	5e                   	pop    %esi
  800d17:	5f                   	pop    %edi
  800d18:	5d                   	pop    %ebp
  800d19:	c3                   	ret    
  800d1a:	66 90                	xchg   %ax,%ax
  800d1c:	89 d8                	mov    %ebx,%eax
  800d1e:	f7 f7                	div    %edi
  800d20:	31 ff                	xor    %edi,%edi
  800d22:	89 fa                	mov    %edi,%edx
  800d24:	83 c4 1c             	add    $0x1c,%esp
  800d27:	5b                   	pop    %ebx
  800d28:	5e                   	pop    %esi
  800d29:	5f                   	pop    %edi
  800d2a:	5d                   	pop    %ebp
  800d2b:	c3                   	ret    
  800d2c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d31:	89 eb                	mov    %ebp,%ebx
  800d33:	29 fb                	sub    %edi,%ebx
  800d35:	89 f9                	mov    %edi,%ecx
  800d37:	d3 e6                	shl    %cl,%esi
  800d39:	89 c5                	mov    %eax,%ebp
  800d3b:	88 d9                	mov    %bl,%cl
  800d3d:	d3 ed                	shr    %cl,%ebp
  800d3f:	89 e9                	mov    %ebp,%ecx
  800d41:	09 f1                	or     %esi,%ecx
  800d43:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d47:	89 f9                	mov    %edi,%ecx
  800d49:	d3 e0                	shl    %cl,%eax
  800d4b:	89 c5                	mov    %eax,%ebp
  800d4d:	89 d6                	mov    %edx,%esi
  800d4f:	88 d9                	mov    %bl,%cl
  800d51:	d3 ee                	shr    %cl,%esi
  800d53:	89 f9                	mov    %edi,%ecx
  800d55:	d3 e2                	shl    %cl,%edx
  800d57:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d5b:	88 d9                	mov    %bl,%cl
  800d5d:	d3 e8                	shr    %cl,%eax
  800d5f:	09 c2                	or     %eax,%edx
  800d61:	89 d0                	mov    %edx,%eax
  800d63:	89 f2                	mov    %esi,%edx
  800d65:	f7 74 24 0c          	divl   0xc(%esp)
  800d69:	89 d6                	mov    %edx,%esi
  800d6b:	89 c3                	mov    %eax,%ebx
  800d6d:	f7 e5                	mul    %ebp
  800d6f:	39 d6                	cmp    %edx,%esi
  800d71:	72 19                	jb     800d8c <__udivdi3+0xfc>
  800d73:	74 0b                	je     800d80 <__udivdi3+0xf0>
  800d75:	89 d8                	mov    %ebx,%eax
  800d77:	31 ff                	xor    %edi,%edi
  800d79:	e9 58 ff ff ff       	jmp    800cd6 <__udivdi3+0x46>
  800d7e:	66 90                	xchg   %ax,%ax
  800d80:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d84:	89 f9                	mov    %edi,%ecx
  800d86:	d3 e2                	shl    %cl,%edx
  800d88:	39 c2                	cmp    %eax,%edx
  800d8a:	73 e9                	jae    800d75 <__udivdi3+0xe5>
  800d8c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d8f:	31 ff                	xor    %edi,%edi
  800d91:	e9 40 ff ff ff       	jmp    800cd6 <__udivdi3+0x46>
  800d96:	66 90                	xchg   %ax,%ax
  800d98:	31 c0                	xor    %eax,%eax
  800d9a:	e9 37 ff ff ff       	jmp    800cd6 <__udivdi3+0x46>
  800d9f:	90                   	nop

00800da0 <__umoddi3>:
  800da0:	55                   	push   %ebp
  800da1:	57                   	push   %edi
  800da2:	56                   	push   %esi
  800da3:	53                   	push   %ebx
  800da4:	83 ec 1c             	sub    $0x1c,%esp
  800da7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800dab:	8b 74 24 34          	mov    0x34(%esp),%esi
  800daf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800db3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800db7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dbb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dbf:	89 f3                	mov    %esi,%ebx
  800dc1:	89 fa                	mov    %edi,%edx
  800dc3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dc7:	89 34 24             	mov    %esi,(%esp)
  800dca:	85 c0                	test   %eax,%eax
  800dcc:	75 1a                	jne    800de8 <__umoddi3+0x48>
  800dce:	39 f7                	cmp    %esi,%edi
  800dd0:	0f 86 a2 00 00 00    	jbe    800e78 <__umoddi3+0xd8>
  800dd6:	89 c8                	mov    %ecx,%eax
  800dd8:	89 f2                	mov    %esi,%edx
  800dda:	f7 f7                	div    %edi
  800ddc:	89 d0                	mov    %edx,%eax
  800dde:	31 d2                	xor    %edx,%edx
  800de0:	83 c4 1c             	add    $0x1c,%esp
  800de3:	5b                   	pop    %ebx
  800de4:	5e                   	pop    %esi
  800de5:	5f                   	pop    %edi
  800de6:	5d                   	pop    %ebp
  800de7:	c3                   	ret    
  800de8:	39 f0                	cmp    %esi,%eax
  800dea:	0f 87 ac 00 00 00    	ja     800e9c <__umoddi3+0xfc>
  800df0:	0f bd e8             	bsr    %eax,%ebp
  800df3:	83 f5 1f             	xor    $0x1f,%ebp
  800df6:	0f 84 ac 00 00 00    	je     800ea8 <__umoddi3+0x108>
  800dfc:	bf 20 00 00 00       	mov    $0x20,%edi
  800e01:	29 ef                	sub    %ebp,%edi
  800e03:	89 fe                	mov    %edi,%esi
  800e05:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e09:	89 e9                	mov    %ebp,%ecx
  800e0b:	d3 e0                	shl    %cl,%eax
  800e0d:	89 d7                	mov    %edx,%edi
  800e0f:	89 f1                	mov    %esi,%ecx
  800e11:	d3 ef                	shr    %cl,%edi
  800e13:	09 c7                	or     %eax,%edi
  800e15:	89 e9                	mov    %ebp,%ecx
  800e17:	d3 e2                	shl    %cl,%edx
  800e19:	89 14 24             	mov    %edx,(%esp)
  800e1c:	89 d8                	mov    %ebx,%eax
  800e1e:	d3 e0                	shl    %cl,%eax
  800e20:	89 c2                	mov    %eax,%edx
  800e22:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e26:	d3 e0                	shl    %cl,%eax
  800e28:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e2c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e30:	89 f1                	mov    %esi,%ecx
  800e32:	d3 e8                	shr    %cl,%eax
  800e34:	09 d0                	or     %edx,%eax
  800e36:	d3 eb                	shr    %cl,%ebx
  800e38:	89 da                	mov    %ebx,%edx
  800e3a:	f7 f7                	div    %edi
  800e3c:	89 d3                	mov    %edx,%ebx
  800e3e:	f7 24 24             	mull   (%esp)
  800e41:	89 c6                	mov    %eax,%esi
  800e43:	89 d1                	mov    %edx,%ecx
  800e45:	39 d3                	cmp    %edx,%ebx
  800e47:	0f 82 87 00 00 00    	jb     800ed4 <__umoddi3+0x134>
  800e4d:	0f 84 91 00 00 00    	je     800ee4 <__umoddi3+0x144>
  800e53:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e57:	29 f2                	sub    %esi,%edx
  800e59:	19 cb                	sbb    %ecx,%ebx
  800e5b:	89 d8                	mov    %ebx,%eax
  800e5d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e61:	d3 e0                	shl    %cl,%eax
  800e63:	89 e9                	mov    %ebp,%ecx
  800e65:	d3 ea                	shr    %cl,%edx
  800e67:	09 d0                	or     %edx,%eax
  800e69:	89 e9                	mov    %ebp,%ecx
  800e6b:	d3 eb                	shr    %cl,%ebx
  800e6d:	89 da                	mov    %ebx,%edx
  800e6f:	83 c4 1c             	add    $0x1c,%esp
  800e72:	5b                   	pop    %ebx
  800e73:	5e                   	pop    %esi
  800e74:	5f                   	pop    %edi
  800e75:	5d                   	pop    %ebp
  800e76:	c3                   	ret    
  800e77:	90                   	nop
  800e78:	89 fd                	mov    %edi,%ebp
  800e7a:	85 ff                	test   %edi,%edi
  800e7c:	75 0b                	jne    800e89 <__umoddi3+0xe9>
  800e7e:	b8 01 00 00 00       	mov    $0x1,%eax
  800e83:	31 d2                	xor    %edx,%edx
  800e85:	f7 f7                	div    %edi
  800e87:	89 c5                	mov    %eax,%ebp
  800e89:	89 f0                	mov    %esi,%eax
  800e8b:	31 d2                	xor    %edx,%edx
  800e8d:	f7 f5                	div    %ebp
  800e8f:	89 c8                	mov    %ecx,%eax
  800e91:	f7 f5                	div    %ebp
  800e93:	89 d0                	mov    %edx,%eax
  800e95:	e9 44 ff ff ff       	jmp    800dde <__umoddi3+0x3e>
  800e9a:	66 90                	xchg   %ax,%ax
  800e9c:	89 c8                	mov    %ecx,%eax
  800e9e:	89 f2                	mov    %esi,%edx
  800ea0:	83 c4 1c             	add    $0x1c,%esp
  800ea3:	5b                   	pop    %ebx
  800ea4:	5e                   	pop    %esi
  800ea5:	5f                   	pop    %edi
  800ea6:	5d                   	pop    %ebp
  800ea7:	c3                   	ret    
  800ea8:	3b 04 24             	cmp    (%esp),%eax
  800eab:	72 06                	jb     800eb3 <__umoddi3+0x113>
  800ead:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800eb1:	77 0f                	ja     800ec2 <__umoddi3+0x122>
  800eb3:	89 f2                	mov    %esi,%edx
  800eb5:	29 f9                	sub    %edi,%ecx
  800eb7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ebb:	89 14 24             	mov    %edx,(%esp)
  800ebe:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ec2:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ec6:	8b 14 24             	mov    (%esp),%edx
  800ec9:	83 c4 1c             	add    $0x1c,%esp
  800ecc:	5b                   	pop    %ebx
  800ecd:	5e                   	pop    %esi
  800ece:	5f                   	pop    %edi
  800ecf:	5d                   	pop    %ebp
  800ed0:	c3                   	ret    
  800ed1:	8d 76 00             	lea    0x0(%esi),%esi
  800ed4:	2b 04 24             	sub    (%esp),%eax
  800ed7:	19 fa                	sbb    %edi,%edx
  800ed9:	89 d1                	mov    %edx,%ecx
  800edb:	89 c6                	mov    %eax,%esi
  800edd:	e9 71 ff ff ff       	jmp    800e53 <__umoddi3+0xb3>
  800ee2:	66 90                	xchg   %ax,%ax
  800ee4:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800ee8:	72 ea                	jb     800ed4 <__umoddi3+0x134>
  800eea:	89 d9                	mov    %ebx,%ecx
  800eec:	e9 62 ff ff ff       	jmp    800e53 <__umoddi3+0xb3>
