
obj/user/faultnostack:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 23 00 00 00       	call   800054 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

void _pgfault_upcall();

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_env_set_pgfault_upcall(0, (void*) _pgfault_upcall);
  800039:	68 3f 03 80 00       	push   $0x80033f
  80003e:	6a 00                	push   $0x0
  800040:	e8 54 02 00 00       	call   800299 <sys_env_set_pgfault_upcall>
	*(int*)0 = 0;
  800045:	c7 05 00 00 00 00 00 	movl   $0x0,0x0
  80004c:	00 00 00 
}
  80004f:	83 c4 10             	add    $0x10,%esp
  800052:	c9                   	leave  
  800053:	c3                   	ret    

00800054 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800054:	55                   	push   %ebp
  800055:	89 e5                	mov    %esp,%ebp
  800057:	56                   	push   %esi
  800058:	53                   	push   %ebx
  800059:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80005c:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80005f:	e8 cf 00 00 00       	call   800133 <sys_getenvid>
  800064:	25 ff 03 00 00       	and    $0x3ff,%eax
  800069:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800070:	c1 e0 07             	shl    $0x7,%eax
  800073:	29 d0                	sub    %edx,%eax
  800075:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80007a:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80007f:	85 db                	test   %ebx,%ebx
  800081:	7e 07                	jle    80008a <libmain+0x36>
		binaryname = argv[0];
  800083:	8b 06                	mov    (%esi),%eax
  800085:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80008a:	83 ec 08             	sub    $0x8,%esp
  80008d:	56                   	push   %esi
  80008e:	53                   	push   %ebx
  80008f:	e8 9f ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800094:	e8 0a 00 00 00       	call   8000a3 <exit>
}
  800099:	83 c4 10             	add    $0x10,%esp
  80009c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80009f:	5b                   	pop    %ebx
  8000a0:	5e                   	pop    %esi
  8000a1:	5d                   	pop    %ebp
  8000a2:	c3                   	ret    

008000a3 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000a3:	55                   	push   %ebp
  8000a4:	89 e5                	mov    %esp,%ebp
  8000a6:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000a9:	6a 00                	push   $0x0
  8000ab:	e8 42 00 00 00       	call   8000f2 <sys_env_destroy>
}
  8000b0:	83 c4 10             	add    $0x10,%esp
  8000b3:	c9                   	leave  
  8000b4:	c3                   	ret    

008000b5 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000b5:	55                   	push   %ebp
  8000b6:	89 e5                	mov    %esp,%ebp
  8000b8:	57                   	push   %edi
  8000b9:	56                   	push   %esi
  8000ba:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000bb:	b8 00 00 00 00       	mov    $0x0,%eax
  8000c0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000c3:	8b 55 08             	mov    0x8(%ebp),%edx
  8000c6:	89 c3                	mov    %eax,%ebx
  8000c8:	89 c7                	mov    %eax,%edi
  8000ca:	89 c6                	mov    %eax,%esi
  8000cc:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000ce:	5b                   	pop    %ebx
  8000cf:	5e                   	pop    %esi
  8000d0:	5f                   	pop    %edi
  8000d1:	5d                   	pop    %ebp
  8000d2:	c3                   	ret    

008000d3 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000d3:	55                   	push   %ebp
  8000d4:	89 e5                	mov    %esp,%ebp
  8000d6:	57                   	push   %edi
  8000d7:	56                   	push   %esi
  8000d8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000d9:	ba 00 00 00 00       	mov    $0x0,%edx
  8000de:	b8 01 00 00 00       	mov    $0x1,%eax
  8000e3:	89 d1                	mov    %edx,%ecx
  8000e5:	89 d3                	mov    %edx,%ebx
  8000e7:	89 d7                	mov    %edx,%edi
  8000e9:	89 d6                	mov    %edx,%esi
  8000eb:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000ed:	5b                   	pop    %ebx
  8000ee:	5e                   	pop    %esi
  8000ef:	5f                   	pop    %edi
  8000f0:	5d                   	pop    %ebp
  8000f1:	c3                   	ret    

008000f2 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000f2:	55                   	push   %ebp
  8000f3:	89 e5                	mov    %esp,%ebp
  8000f5:	57                   	push   %edi
  8000f6:	56                   	push   %esi
  8000f7:	53                   	push   %ebx
  8000f8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000fb:	b9 00 00 00 00       	mov    $0x0,%ecx
  800100:	b8 03 00 00 00       	mov    $0x3,%eax
  800105:	8b 55 08             	mov    0x8(%ebp),%edx
  800108:	89 cb                	mov    %ecx,%ebx
  80010a:	89 cf                	mov    %ecx,%edi
  80010c:	89 ce                	mov    %ecx,%esi
  80010e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800110:	85 c0                	test   %eax,%eax
  800112:	7e 17                	jle    80012b <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800114:	83 ec 0c             	sub    $0xc,%esp
  800117:	50                   	push   %eax
  800118:	6a 03                	push   $0x3
  80011a:	68 aa 0f 80 00       	push   $0x800faa
  80011f:	6a 23                	push   $0x23
  800121:	68 c7 0f 80 00       	push   $0x800fc7
  800126:	e8 3c 02 00 00       	call   800367 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80012b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80012e:	5b                   	pop    %ebx
  80012f:	5e                   	pop    %esi
  800130:	5f                   	pop    %edi
  800131:	5d                   	pop    %ebp
  800132:	c3                   	ret    

00800133 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800133:	55                   	push   %ebp
  800134:	89 e5                	mov    %esp,%ebp
  800136:	57                   	push   %edi
  800137:	56                   	push   %esi
  800138:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800139:	ba 00 00 00 00       	mov    $0x0,%edx
  80013e:	b8 02 00 00 00       	mov    $0x2,%eax
  800143:	89 d1                	mov    %edx,%ecx
  800145:	89 d3                	mov    %edx,%ebx
  800147:	89 d7                	mov    %edx,%edi
  800149:	89 d6                	mov    %edx,%esi
  80014b:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80014d:	5b                   	pop    %ebx
  80014e:	5e                   	pop    %esi
  80014f:	5f                   	pop    %edi
  800150:	5d                   	pop    %ebp
  800151:	c3                   	ret    

00800152 <sys_yield>:

void
sys_yield(void)
{
  800152:	55                   	push   %ebp
  800153:	89 e5                	mov    %esp,%ebp
  800155:	57                   	push   %edi
  800156:	56                   	push   %esi
  800157:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800158:	ba 00 00 00 00       	mov    $0x0,%edx
  80015d:	b8 0a 00 00 00       	mov    $0xa,%eax
  800162:	89 d1                	mov    %edx,%ecx
  800164:	89 d3                	mov    %edx,%ebx
  800166:	89 d7                	mov    %edx,%edi
  800168:	89 d6                	mov    %edx,%esi
  80016a:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80016c:	5b                   	pop    %ebx
  80016d:	5e                   	pop    %esi
  80016e:	5f                   	pop    %edi
  80016f:	5d                   	pop    %ebp
  800170:	c3                   	ret    

00800171 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800171:	55                   	push   %ebp
  800172:	89 e5                	mov    %esp,%ebp
  800174:	57                   	push   %edi
  800175:	56                   	push   %esi
  800176:	53                   	push   %ebx
  800177:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80017a:	be 00 00 00 00       	mov    $0x0,%esi
  80017f:	b8 04 00 00 00       	mov    $0x4,%eax
  800184:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800187:	8b 55 08             	mov    0x8(%ebp),%edx
  80018a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80018d:	89 f7                	mov    %esi,%edi
  80018f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800191:	85 c0                	test   %eax,%eax
  800193:	7e 17                	jle    8001ac <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800195:	83 ec 0c             	sub    $0xc,%esp
  800198:	50                   	push   %eax
  800199:	6a 04                	push   $0x4
  80019b:	68 aa 0f 80 00       	push   $0x800faa
  8001a0:	6a 23                	push   $0x23
  8001a2:	68 c7 0f 80 00       	push   $0x800fc7
  8001a7:	e8 bb 01 00 00       	call   800367 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001ac:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001af:	5b                   	pop    %ebx
  8001b0:	5e                   	pop    %esi
  8001b1:	5f                   	pop    %edi
  8001b2:	5d                   	pop    %ebp
  8001b3:	c3                   	ret    

008001b4 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001b4:	55                   	push   %ebp
  8001b5:	89 e5                	mov    %esp,%ebp
  8001b7:	57                   	push   %edi
  8001b8:	56                   	push   %esi
  8001b9:	53                   	push   %ebx
  8001ba:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001bd:	b8 05 00 00 00       	mov    $0x5,%eax
  8001c2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001c5:	8b 55 08             	mov    0x8(%ebp),%edx
  8001c8:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001cb:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001ce:	8b 75 18             	mov    0x18(%ebp),%esi
  8001d1:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001d3:	85 c0                	test   %eax,%eax
  8001d5:	7e 17                	jle    8001ee <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001d7:	83 ec 0c             	sub    $0xc,%esp
  8001da:	50                   	push   %eax
  8001db:	6a 05                	push   $0x5
  8001dd:	68 aa 0f 80 00       	push   $0x800faa
  8001e2:	6a 23                	push   $0x23
  8001e4:	68 c7 0f 80 00       	push   $0x800fc7
  8001e9:	e8 79 01 00 00       	call   800367 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001ee:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001f1:	5b                   	pop    %ebx
  8001f2:	5e                   	pop    %esi
  8001f3:	5f                   	pop    %edi
  8001f4:	5d                   	pop    %ebp
  8001f5:	c3                   	ret    

008001f6 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001f6:	55                   	push   %ebp
  8001f7:	89 e5                	mov    %esp,%ebp
  8001f9:	57                   	push   %edi
  8001fa:	56                   	push   %esi
  8001fb:	53                   	push   %ebx
  8001fc:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ff:	bb 00 00 00 00       	mov    $0x0,%ebx
  800204:	b8 06 00 00 00       	mov    $0x6,%eax
  800209:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80020c:	8b 55 08             	mov    0x8(%ebp),%edx
  80020f:	89 df                	mov    %ebx,%edi
  800211:	89 de                	mov    %ebx,%esi
  800213:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800215:	85 c0                	test   %eax,%eax
  800217:	7e 17                	jle    800230 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800219:	83 ec 0c             	sub    $0xc,%esp
  80021c:	50                   	push   %eax
  80021d:	6a 06                	push   $0x6
  80021f:	68 aa 0f 80 00       	push   $0x800faa
  800224:	6a 23                	push   $0x23
  800226:	68 c7 0f 80 00       	push   $0x800fc7
  80022b:	e8 37 01 00 00       	call   800367 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800230:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800233:	5b                   	pop    %ebx
  800234:	5e                   	pop    %esi
  800235:	5f                   	pop    %edi
  800236:	5d                   	pop    %ebp
  800237:	c3                   	ret    

00800238 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800238:	55                   	push   %ebp
  800239:	89 e5                	mov    %esp,%ebp
  80023b:	57                   	push   %edi
  80023c:	56                   	push   %esi
  80023d:	53                   	push   %ebx
  80023e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800241:	bb 00 00 00 00       	mov    $0x0,%ebx
  800246:	b8 08 00 00 00       	mov    $0x8,%eax
  80024b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80024e:	8b 55 08             	mov    0x8(%ebp),%edx
  800251:	89 df                	mov    %ebx,%edi
  800253:	89 de                	mov    %ebx,%esi
  800255:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800257:	85 c0                	test   %eax,%eax
  800259:	7e 17                	jle    800272 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80025b:	83 ec 0c             	sub    $0xc,%esp
  80025e:	50                   	push   %eax
  80025f:	6a 08                	push   $0x8
  800261:	68 aa 0f 80 00       	push   $0x800faa
  800266:	6a 23                	push   $0x23
  800268:	68 c7 0f 80 00       	push   $0x800fc7
  80026d:	e8 f5 00 00 00       	call   800367 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800272:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800275:	5b                   	pop    %ebx
  800276:	5e                   	pop    %esi
  800277:	5f                   	pop    %edi
  800278:	5d                   	pop    %ebp
  800279:	c3                   	ret    

0080027a <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80027a:	55                   	push   %ebp
  80027b:	89 e5                	mov    %esp,%ebp
  80027d:	57                   	push   %edi
  80027e:	56                   	push   %esi
  80027f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800280:	ba 00 00 00 00       	mov    $0x0,%edx
  800285:	b8 0b 00 00 00       	mov    $0xb,%eax
  80028a:	89 d1                	mov    %edx,%ecx
  80028c:	89 d3                	mov    %edx,%ebx
  80028e:	89 d7                	mov    %edx,%edi
  800290:	89 d6                	mov    %edx,%esi
  800292:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800294:	5b                   	pop    %ebx
  800295:	5e                   	pop    %esi
  800296:	5f                   	pop    %edi
  800297:	5d                   	pop    %ebp
  800298:	c3                   	ret    

00800299 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800299:	55                   	push   %ebp
  80029a:	89 e5                	mov    %esp,%ebp
  80029c:	57                   	push   %edi
  80029d:	56                   	push   %esi
  80029e:	53                   	push   %ebx
  80029f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002a2:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002a7:	b8 09 00 00 00       	mov    $0x9,%eax
  8002ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002af:	8b 55 08             	mov    0x8(%ebp),%edx
  8002b2:	89 df                	mov    %ebx,%edi
  8002b4:	89 de                	mov    %ebx,%esi
  8002b6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002b8:	85 c0                	test   %eax,%eax
  8002ba:	7e 17                	jle    8002d3 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002bc:	83 ec 0c             	sub    $0xc,%esp
  8002bf:	50                   	push   %eax
  8002c0:	6a 09                	push   $0x9
  8002c2:	68 aa 0f 80 00       	push   $0x800faa
  8002c7:	6a 23                	push   $0x23
  8002c9:	68 c7 0f 80 00       	push   $0x800fc7
  8002ce:	e8 94 00 00 00       	call   800367 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d6:	5b                   	pop    %ebx
  8002d7:	5e                   	pop    %esi
  8002d8:	5f                   	pop    %edi
  8002d9:	5d                   	pop    %ebp
  8002da:	c3                   	ret    

008002db <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002db:	55                   	push   %ebp
  8002dc:	89 e5                	mov    %esp,%ebp
  8002de:	57                   	push   %edi
  8002df:	56                   	push   %esi
  8002e0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002e1:	be 00 00 00 00       	mov    $0x0,%esi
  8002e6:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002eb:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002ee:	8b 55 08             	mov    0x8(%ebp),%edx
  8002f1:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002f4:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002f7:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002f9:	5b                   	pop    %ebx
  8002fa:	5e                   	pop    %esi
  8002fb:	5f                   	pop    %edi
  8002fc:	5d                   	pop    %ebp
  8002fd:	c3                   	ret    

008002fe <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002fe:	55                   	push   %ebp
  8002ff:	89 e5                	mov    %esp,%ebp
  800301:	57                   	push   %edi
  800302:	56                   	push   %esi
  800303:	53                   	push   %ebx
  800304:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800307:	b9 00 00 00 00       	mov    $0x0,%ecx
  80030c:	b8 0d 00 00 00       	mov    $0xd,%eax
  800311:	8b 55 08             	mov    0x8(%ebp),%edx
  800314:	89 cb                	mov    %ecx,%ebx
  800316:	89 cf                	mov    %ecx,%edi
  800318:	89 ce                	mov    %ecx,%esi
  80031a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80031c:	85 c0                	test   %eax,%eax
  80031e:	7e 17                	jle    800337 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800320:	83 ec 0c             	sub    $0xc,%esp
  800323:	50                   	push   %eax
  800324:	6a 0d                	push   $0xd
  800326:	68 aa 0f 80 00       	push   $0x800faa
  80032b:	6a 23                	push   $0x23
  80032d:	68 c7 0f 80 00       	push   $0x800fc7
  800332:	e8 30 00 00 00       	call   800367 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800337:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80033a:	5b                   	pop    %ebx
  80033b:	5e                   	pop    %esi
  80033c:	5f                   	pop    %edi
  80033d:	5d                   	pop    %ebp
  80033e:	c3                   	ret    

0080033f <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  80033f:	54                   	push   %esp
	movl _pgfault_handler, %eax
  800340:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  800345:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  800347:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  80034a:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  80034c:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  800350:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  800354:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  800355:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  800358:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  80035a:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  80035d:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  800360:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  800361:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  800364:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  800365:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  800366:	c3                   	ret    

00800367 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800367:	55                   	push   %ebp
  800368:	89 e5                	mov    %esp,%ebp
  80036a:	56                   	push   %esi
  80036b:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80036c:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80036f:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800375:	e8 b9 fd ff ff       	call   800133 <sys_getenvid>
  80037a:	83 ec 0c             	sub    $0xc,%esp
  80037d:	ff 75 0c             	pushl  0xc(%ebp)
  800380:	ff 75 08             	pushl  0x8(%ebp)
  800383:	56                   	push   %esi
  800384:	50                   	push   %eax
  800385:	68 d8 0f 80 00       	push   $0x800fd8
  80038a:	e8 b0 00 00 00       	call   80043f <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80038f:	83 c4 18             	add    $0x18,%esp
  800392:	53                   	push   %ebx
  800393:	ff 75 10             	pushl  0x10(%ebp)
  800396:	e8 53 00 00 00       	call   8003ee <vcprintf>
	cprintf("\n");
  80039b:	c7 04 24 fb 0f 80 00 	movl   $0x800ffb,(%esp)
  8003a2:	e8 98 00 00 00       	call   80043f <cprintf>
  8003a7:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003aa:	cc                   	int3   
  8003ab:	eb fd                	jmp    8003aa <_panic+0x43>

008003ad <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003ad:	55                   	push   %ebp
  8003ae:	89 e5                	mov    %esp,%ebp
  8003b0:	53                   	push   %ebx
  8003b1:	83 ec 04             	sub    $0x4,%esp
  8003b4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003b7:	8b 13                	mov    (%ebx),%edx
  8003b9:	8d 42 01             	lea    0x1(%edx),%eax
  8003bc:	89 03                	mov    %eax,(%ebx)
  8003be:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003c1:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003c5:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003ca:	75 1a                	jne    8003e6 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003cc:	83 ec 08             	sub    $0x8,%esp
  8003cf:	68 ff 00 00 00       	push   $0xff
  8003d4:	8d 43 08             	lea    0x8(%ebx),%eax
  8003d7:	50                   	push   %eax
  8003d8:	e8 d8 fc ff ff       	call   8000b5 <sys_cputs>
		b->idx = 0;
  8003dd:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003e3:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003e6:	ff 43 04             	incl   0x4(%ebx)
}
  8003e9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003ec:	c9                   	leave  
  8003ed:	c3                   	ret    

008003ee <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003ee:	55                   	push   %ebp
  8003ef:	89 e5                	mov    %esp,%ebp
  8003f1:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003f7:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003fe:	00 00 00 
	b.cnt = 0;
  800401:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800408:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80040b:	ff 75 0c             	pushl  0xc(%ebp)
  80040e:	ff 75 08             	pushl  0x8(%ebp)
  800411:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800417:	50                   	push   %eax
  800418:	68 ad 03 80 00       	push   $0x8003ad
  80041d:	e8 51 01 00 00       	call   800573 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800422:	83 c4 08             	add    $0x8,%esp
  800425:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80042b:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800431:	50                   	push   %eax
  800432:	e8 7e fc ff ff       	call   8000b5 <sys_cputs>

	return b.cnt;
}
  800437:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80043d:	c9                   	leave  
  80043e:	c3                   	ret    

0080043f <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80043f:	55                   	push   %ebp
  800440:	89 e5                	mov    %esp,%ebp
  800442:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800445:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800448:	50                   	push   %eax
  800449:	ff 75 08             	pushl  0x8(%ebp)
  80044c:	e8 9d ff ff ff       	call   8003ee <vcprintf>
	va_end(ap);

	return cnt;
}
  800451:	c9                   	leave  
  800452:	c3                   	ret    

00800453 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800453:	55                   	push   %ebp
  800454:	89 e5                	mov    %esp,%ebp
  800456:	57                   	push   %edi
  800457:	56                   	push   %esi
  800458:	53                   	push   %ebx
  800459:	83 ec 1c             	sub    $0x1c,%esp
  80045c:	89 c7                	mov    %eax,%edi
  80045e:	89 d6                	mov    %edx,%esi
  800460:	8b 45 08             	mov    0x8(%ebp),%eax
  800463:	8b 55 0c             	mov    0xc(%ebp),%edx
  800466:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800469:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80046c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80046f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800474:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800477:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80047a:	39 d3                	cmp    %edx,%ebx
  80047c:	72 05                	jb     800483 <printnum+0x30>
  80047e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800481:	77 45                	ja     8004c8 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800483:	83 ec 0c             	sub    $0xc,%esp
  800486:	ff 75 18             	pushl  0x18(%ebp)
  800489:	8b 45 14             	mov    0x14(%ebp),%eax
  80048c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80048f:	53                   	push   %ebx
  800490:	ff 75 10             	pushl  0x10(%ebp)
  800493:	83 ec 08             	sub    $0x8,%esp
  800496:	ff 75 e4             	pushl  -0x1c(%ebp)
  800499:	ff 75 e0             	pushl  -0x20(%ebp)
  80049c:	ff 75 dc             	pushl  -0x24(%ebp)
  80049f:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a2:	e8 7d 08 00 00       	call   800d24 <__udivdi3>
  8004a7:	83 c4 18             	add    $0x18,%esp
  8004aa:	52                   	push   %edx
  8004ab:	50                   	push   %eax
  8004ac:	89 f2                	mov    %esi,%edx
  8004ae:	89 f8                	mov    %edi,%eax
  8004b0:	e8 9e ff ff ff       	call   800453 <printnum>
  8004b5:	83 c4 20             	add    $0x20,%esp
  8004b8:	eb 16                	jmp    8004d0 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004ba:	83 ec 08             	sub    $0x8,%esp
  8004bd:	56                   	push   %esi
  8004be:	ff 75 18             	pushl  0x18(%ebp)
  8004c1:	ff d7                	call   *%edi
  8004c3:	83 c4 10             	add    $0x10,%esp
  8004c6:	eb 03                	jmp    8004cb <printnum+0x78>
  8004c8:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004cb:	4b                   	dec    %ebx
  8004cc:	85 db                	test   %ebx,%ebx
  8004ce:	7f ea                	jg     8004ba <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004d0:	83 ec 08             	sub    $0x8,%esp
  8004d3:	56                   	push   %esi
  8004d4:	83 ec 04             	sub    $0x4,%esp
  8004d7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004da:	ff 75 e0             	pushl  -0x20(%ebp)
  8004dd:	ff 75 dc             	pushl  -0x24(%ebp)
  8004e0:	ff 75 d8             	pushl  -0x28(%ebp)
  8004e3:	e8 4c 09 00 00       	call   800e34 <__umoddi3>
  8004e8:	83 c4 14             	add    $0x14,%esp
  8004eb:	0f be 80 fd 0f 80 00 	movsbl 0x800ffd(%eax),%eax
  8004f2:	50                   	push   %eax
  8004f3:	ff d7                	call   *%edi
}
  8004f5:	83 c4 10             	add    $0x10,%esp
  8004f8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004fb:	5b                   	pop    %ebx
  8004fc:	5e                   	pop    %esi
  8004fd:	5f                   	pop    %edi
  8004fe:	5d                   	pop    %ebp
  8004ff:	c3                   	ret    

00800500 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800500:	55                   	push   %ebp
  800501:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800503:	83 fa 01             	cmp    $0x1,%edx
  800506:	7e 0e                	jle    800516 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800508:	8b 10                	mov    (%eax),%edx
  80050a:	8d 4a 08             	lea    0x8(%edx),%ecx
  80050d:	89 08                	mov    %ecx,(%eax)
  80050f:	8b 02                	mov    (%edx),%eax
  800511:	8b 52 04             	mov    0x4(%edx),%edx
  800514:	eb 22                	jmp    800538 <getuint+0x38>
	else if (lflag)
  800516:	85 d2                	test   %edx,%edx
  800518:	74 10                	je     80052a <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80051a:	8b 10                	mov    (%eax),%edx
  80051c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80051f:	89 08                	mov    %ecx,(%eax)
  800521:	8b 02                	mov    (%edx),%eax
  800523:	ba 00 00 00 00       	mov    $0x0,%edx
  800528:	eb 0e                	jmp    800538 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80052a:	8b 10                	mov    (%eax),%edx
  80052c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80052f:	89 08                	mov    %ecx,(%eax)
  800531:	8b 02                	mov    (%edx),%eax
  800533:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800538:	5d                   	pop    %ebp
  800539:	c3                   	ret    

0080053a <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80053a:	55                   	push   %ebp
  80053b:	89 e5                	mov    %esp,%ebp
  80053d:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800540:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800543:	8b 10                	mov    (%eax),%edx
  800545:	3b 50 04             	cmp    0x4(%eax),%edx
  800548:	73 0a                	jae    800554 <sprintputch+0x1a>
		*b->buf++ = ch;
  80054a:	8d 4a 01             	lea    0x1(%edx),%ecx
  80054d:	89 08                	mov    %ecx,(%eax)
  80054f:	8b 45 08             	mov    0x8(%ebp),%eax
  800552:	88 02                	mov    %al,(%edx)
}
  800554:	5d                   	pop    %ebp
  800555:	c3                   	ret    

00800556 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800556:	55                   	push   %ebp
  800557:	89 e5                	mov    %esp,%ebp
  800559:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80055c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80055f:	50                   	push   %eax
  800560:	ff 75 10             	pushl  0x10(%ebp)
  800563:	ff 75 0c             	pushl  0xc(%ebp)
  800566:	ff 75 08             	pushl  0x8(%ebp)
  800569:	e8 05 00 00 00       	call   800573 <vprintfmt>
	va_end(ap);
}
  80056e:	83 c4 10             	add    $0x10,%esp
  800571:	c9                   	leave  
  800572:	c3                   	ret    

00800573 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800573:	55                   	push   %ebp
  800574:	89 e5                	mov    %esp,%ebp
  800576:	57                   	push   %edi
  800577:	56                   	push   %esi
  800578:	53                   	push   %ebx
  800579:	83 ec 2c             	sub    $0x2c,%esp
  80057c:	8b 75 08             	mov    0x8(%ebp),%esi
  80057f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800582:	8b 7d 10             	mov    0x10(%ebp),%edi
  800585:	eb 12                	jmp    800599 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800587:	85 c0                	test   %eax,%eax
  800589:	0f 84 68 03 00 00    	je     8008f7 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80058f:	83 ec 08             	sub    $0x8,%esp
  800592:	53                   	push   %ebx
  800593:	50                   	push   %eax
  800594:	ff d6                	call   *%esi
  800596:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800599:	47                   	inc    %edi
  80059a:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80059e:	83 f8 25             	cmp    $0x25,%eax
  8005a1:	75 e4                	jne    800587 <vprintfmt+0x14>
  8005a3:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005a7:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005ae:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005b5:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005bc:	ba 00 00 00 00       	mov    $0x0,%edx
  8005c1:	eb 07                	jmp    8005ca <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c3:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005c6:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ca:	8d 47 01             	lea    0x1(%edi),%eax
  8005cd:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005d0:	0f b6 0f             	movzbl (%edi),%ecx
  8005d3:	8a 07                	mov    (%edi),%al
  8005d5:	83 e8 23             	sub    $0x23,%eax
  8005d8:	3c 55                	cmp    $0x55,%al
  8005da:	0f 87 fe 02 00 00    	ja     8008de <vprintfmt+0x36b>
  8005e0:	0f b6 c0             	movzbl %al,%eax
  8005e3:	ff 24 85 c0 10 80 00 	jmp    *0x8010c0(,%eax,4)
  8005ea:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005ed:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005f1:	eb d7                	jmp    8005ca <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f6:	b8 00 00 00 00       	mov    $0x0,%eax
  8005fb:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005fe:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800601:	01 c0                	add    %eax,%eax
  800603:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800607:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80060a:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80060d:	83 fa 09             	cmp    $0x9,%edx
  800610:	77 34                	ja     800646 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800612:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800613:	eb e9                	jmp    8005fe <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800615:	8b 45 14             	mov    0x14(%ebp),%eax
  800618:	8d 48 04             	lea    0x4(%eax),%ecx
  80061b:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80061e:	8b 00                	mov    (%eax),%eax
  800620:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800623:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800626:	eb 24                	jmp    80064c <vprintfmt+0xd9>
  800628:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80062c:	79 07                	jns    800635 <vprintfmt+0xc2>
  80062e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800635:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800638:	eb 90                	jmp    8005ca <vprintfmt+0x57>
  80063a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80063d:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800644:	eb 84                	jmp    8005ca <vprintfmt+0x57>
  800646:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800649:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80064c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800650:	0f 89 74 ff ff ff    	jns    8005ca <vprintfmt+0x57>
				width = precision, precision = -1;
  800656:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800659:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80065c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800663:	e9 62 ff ff ff       	jmp    8005ca <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800668:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800669:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80066c:	e9 59 ff ff ff       	jmp    8005ca <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800671:	8b 45 14             	mov    0x14(%ebp),%eax
  800674:	8d 50 04             	lea    0x4(%eax),%edx
  800677:	89 55 14             	mov    %edx,0x14(%ebp)
  80067a:	83 ec 08             	sub    $0x8,%esp
  80067d:	53                   	push   %ebx
  80067e:	ff 30                	pushl  (%eax)
  800680:	ff d6                	call   *%esi
			break;
  800682:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800685:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800688:	e9 0c ff ff ff       	jmp    800599 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80068d:	8b 45 14             	mov    0x14(%ebp),%eax
  800690:	8d 50 04             	lea    0x4(%eax),%edx
  800693:	89 55 14             	mov    %edx,0x14(%ebp)
  800696:	8b 00                	mov    (%eax),%eax
  800698:	85 c0                	test   %eax,%eax
  80069a:	79 02                	jns    80069e <vprintfmt+0x12b>
  80069c:	f7 d8                	neg    %eax
  80069e:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006a0:	83 f8 08             	cmp    $0x8,%eax
  8006a3:	7f 0b                	jg     8006b0 <vprintfmt+0x13d>
  8006a5:	8b 04 85 20 12 80 00 	mov    0x801220(,%eax,4),%eax
  8006ac:	85 c0                	test   %eax,%eax
  8006ae:	75 18                	jne    8006c8 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006b0:	52                   	push   %edx
  8006b1:	68 15 10 80 00       	push   $0x801015
  8006b6:	53                   	push   %ebx
  8006b7:	56                   	push   %esi
  8006b8:	e8 99 fe ff ff       	call   800556 <printfmt>
  8006bd:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c0:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006c3:	e9 d1 fe ff ff       	jmp    800599 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006c8:	50                   	push   %eax
  8006c9:	68 1e 10 80 00       	push   $0x80101e
  8006ce:	53                   	push   %ebx
  8006cf:	56                   	push   %esi
  8006d0:	e8 81 fe ff ff       	call   800556 <printfmt>
  8006d5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006d8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006db:	e9 b9 fe ff ff       	jmp    800599 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006e0:	8b 45 14             	mov    0x14(%ebp),%eax
  8006e3:	8d 50 04             	lea    0x4(%eax),%edx
  8006e6:	89 55 14             	mov    %edx,0x14(%ebp)
  8006e9:	8b 38                	mov    (%eax),%edi
  8006eb:	85 ff                	test   %edi,%edi
  8006ed:	75 05                	jne    8006f4 <vprintfmt+0x181>
				p = "(null)";
  8006ef:	bf 0e 10 80 00       	mov    $0x80100e,%edi
			if (width > 0 && padc != '-')
  8006f4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006f8:	0f 8e 90 00 00 00    	jle    80078e <vprintfmt+0x21b>
  8006fe:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800702:	0f 84 8e 00 00 00    	je     800796 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800708:	83 ec 08             	sub    $0x8,%esp
  80070b:	ff 75 d0             	pushl  -0x30(%ebp)
  80070e:	57                   	push   %edi
  80070f:	e8 70 02 00 00       	call   800984 <strnlen>
  800714:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800717:	29 c1                	sub    %eax,%ecx
  800719:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80071c:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80071f:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800723:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800726:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800729:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80072b:	eb 0d                	jmp    80073a <vprintfmt+0x1c7>
					putch(padc, putdat);
  80072d:	83 ec 08             	sub    $0x8,%esp
  800730:	53                   	push   %ebx
  800731:	ff 75 e4             	pushl  -0x1c(%ebp)
  800734:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800736:	4f                   	dec    %edi
  800737:	83 c4 10             	add    $0x10,%esp
  80073a:	85 ff                	test   %edi,%edi
  80073c:	7f ef                	jg     80072d <vprintfmt+0x1ba>
  80073e:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800741:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800744:	89 c8                	mov    %ecx,%eax
  800746:	85 c9                	test   %ecx,%ecx
  800748:	79 05                	jns    80074f <vprintfmt+0x1dc>
  80074a:	b8 00 00 00 00       	mov    $0x0,%eax
  80074f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800752:	29 c1                	sub    %eax,%ecx
  800754:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800757:	89 75 08             	mov    %esi,0x8(%ebp)
  80075a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075d:	eb 3d                	jmp    80079c <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80075f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800763:	74 19                	je     80077e <vprintfmt+0x20b>
  800765:	0f be c0             	movsbl %al,%eax
  800768:	83 e8 20             	sub    $0x20,%eax
  80076b:	83 f8 5e             	cmp    $0x5e,%eax
  80076e:	76 0e                	jbe    80077e <vprintfmt+0x20b>
					putch('?', putdat);
  800770:	83 ec 08             	sub    $0x8,%esp
  800773:	53                   	push   %ebx
  800774:	6a 3f                	push   $0x3f
  800776:	ff 55 08             	call   *0x8(%ebp)
  800779:	83 c4 10             	add    $0x10,%esp
  80077c:	eb 0b                	jmp    800789 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80077e:	83 ec 08             	sub    $0x8,%esp
  800781:	53                   	push   %ebx
  800782:	52                   	push   %edx
  800783:	ff 55 08             	call   *0x8(%ebp)
  800786:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800789:	ff 4d e4             	decl   -0x1c(%ebp)
  80078c:	eb 0e                	jmp    80079c <vprintfmt+0x229>
  80078e:	89 75 08             	mov    %esi,0x8(%ebp)
  800791:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800794:	eb 06                	jmp    80079c <vprintfmt+0x229>
  800796:	89 75 08             	mov    %esi,0x8(%ebp)
  800799:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80079c:	47                   	inc    %edi
  80079d:	8a 47 ff             	mov    -0x1(%edi),%al
  8007a0:	0f be d0             	movsbl %al,%edx
  8007a3:	85 d2                	test   %edx,%edx
  8007a5:	74 1d                	je     8007c4 <vprintfmt+0x251>
  8007a7:	85 f6                	test   %esi,%esi
  8007a9:	78 b4                	js     80075f <vprintfmt+0x1ec>
  8007ab:	4e                   	dec    %esi
  8007ac:	79 b1                	jns    80075f <vprintfmt+0x1ec>
  8007ae:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007b4:	eb 14                	jmp    8007ca <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007b6:	83 ec 08             	sub    $0x8,%esp
  8007b9:	53                   	push   %ebx
  8007ba:	6a 20                	push   $0x20
  8007bc:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007be:	4f                   	dec    %edi
  8007bf:	83 c4 10             	add    $0x10,%esp
  8007c2:	eb 06                	jmp    8007ca <vprintfmt+0x257>
  8007c4:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007c7:	8b 75 08             	mov    0x8(%ebp),%esi
  8007ca:	85 ff                	test   %edi,%edi
  8007cc:	7f e8                	jg     8007b6 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007ce:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007d1:	e9 c3 fd ff ff       	jmp    800599 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007d6:	83 fa 01             	cmp    $0x1,%edx
  8007d9:	7e 16                	jle    8007f1 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007db:	8b 45 14             	mov    0x14(%ebp),%eax
  8007de:	8d 50 08             	lea    0x8(%eax),%edx
  8007e1:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e4:	8b 50 04             	mov    0x4(%eax),%edx
  8007e7:	8b 00                	mov    (%eax),%eax
  8007e9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007ec:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ef:	eb 32                	jmp    800823 <vprintfmt+0x2b0>
	else if (lflag)
  8007f1:	85 d2                	test   %edx,%edx
  8007f3:	74 18                	je     80080d <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007f5:	8b 45 14             	mov    0x14(%ebp),%eax
  8007f8:	8d 50 04             	lea    0x4(%eax),%edx
  8007fb:	89 55 14             	mov    %edx,0x14(%ebp)
  8007fe:	8b 00                	mov    (%eax),%eax
  800800:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800803:	89 c1                	mov    %eax,%ecx
  800805:	c1 f9 1f             	sar    $0x1f,%ecx
  800808:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80080b:	eb 16                	jmp    800823 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80080d:	8b 45 14             	mov    0x14(%ebp),%eax
  800810:	8d 50 04             	lea    0x4(%eax),%edx
  800813:	89 55 14             	mov    %edx,0x14(%ebp)
  800816:	8b 00                	mov    (%eax),%eax
  800818:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80081b:	89 c1                	mov    %eax,%ecx
  80081d:	c1 f9 1f             	sar    $0x1f,%ecx
  800820:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800823:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800826:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800829:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80082d:	79 76                	jns    8008a5 <vprintfmt+0x332>
				putch('-', putdat);
  80082f:	83 ec 08             	sub    $0x8,%esp
  800832:	53                   	push   %ebx
  800833:	6a 2d                	push   $0x2d
  800835:	ff d6                	call   *%esi
				num = -(long long) num;
  800837:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80083a:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80083d:	f7 d8                	neg    %eax
  80083f:	83 d2 00             	adc    $0x0,%edx
  800842:	f7 da                	neg    %edx
  800844:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800847:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80084c:	eb 5c                	jmp    8008aa <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80084e:	8d 45 14             	lea    0x14(%ebp),%eax
  800851:	e8 aa fc ff ff       	call   800500 <getuint>
			base = 10;
  800856:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80085b:	eb 4d                	jmp    8008aa <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80085d:	8d 45 14             	lea    0x14(%ebp),%eax
  800860:	e8 9b fc ff ff       	call   800500 <getuint>
			base = 8;
  800865:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80086a:	eb 3e                	jmp    8008aa <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80086c:	83 ec 08             	sub    $0x8,%esp
  80086f:	53                   	push   %ebx
  800870:	6a 30                	push   $0x30
  800872:	ff d6                	call   *%esi
			putch('x', putdat);
  800874:	83 c4 08             	add    $0x8,%esp
  800877:	53                   	push   %ebx
  800878:	6a 78                	push   $0x78
  80087a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80087c:	8b 45 14             	mov    0x14(%ebp),%eax
  80087f:	8d 50 04             	lea    0x4(%eax),%edx
  800882:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800885:	8b 00                	mov    (%eax),%eax
  800887:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80088c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80088f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800894:	eb 14                	jmp    8008aa <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800896:	8d 45 14             	lea    0x14(%ebp),%eax
  800899:	e8 62 fc ff ff       	call   800500 <getuint>
			base = 16;
  80089e:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008a3:	eb 05                	jmp    8008aa <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008a5:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008aa:	83 ec 0c             	sub    $0xc,%esp
  8008ad:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008b1:	57                   	push   %edi
  8008b2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008b5:	51                   	push   %ecx
  8008b6:	52                   	push   %edx
  8008b7:	50                   	push   %eax
  8008b8:	89 da                	mov    %ebx,%edx
  8008ba:	89 f0                	mov    %esi,%eax
  8008bc:	e8 92 fb ff ff       	call   800453 <printnum>
			break;
  8008c1:	83 c4 20             	add    $0x20,%esp
  8008c4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008c7:	e9 cd fc ff ff       	jmp    800599 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008cc:	83 ec 08             	sub    $0x8,%esp
  8008cf:	53                   	push   %ebx
  8008d0:	51                   	push   %ecx
  8008d1:	ff d6                	call   *%esi
			break;
  8008d3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008d6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008d9:	e9 bb fc ff ff       	jmp    800599 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008de:	83 ec 08             	sub    $0x8,%esp
  8008e1:	53                   	push   %ebx
  8008e2:	6a 25                	push   $0x25
  8008e4:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008e6:	83 c4 10             	add    $0x10,%esp
  8008e9:	eb 01                	jmp    8008ec <vprintfmt+0x379>
  8008eb:	4f                   	dec    %edi
  8008ec:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008f0:	75 f9                	jne    8008eb <vprintfmt+0x378>
  8008f2:	e9 a2 fc ff ff       	jmp    800599 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008fa:	5b                   	pop    %ebx
  8008fb:	5e                   	pop    %esi
  8008fc:	5f                   	pop    %edi
  8008fd:	5d                   	pop    %ebp
  8008fe:	c3                   	ret    

008008ff <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008ff:	55                   	push   %ebp
  800900:	89 e5                	mov    %esp,%ebp
  800902:	83 ec 18             	sub    $0x18,%esp
  800905:	8b 45 08             	mov    0x8(%ebp),%eax
  800908:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80090b:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80090e:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800912:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800915:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80091c:	85 c0                	test   %eax,%eax
  80091e:	74 26                	je     800946 <vsnprintf+0x47>
  800920:	85 d2                	test   %edx,%edx
  800922:	7e 29                	jle    80094d <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800924:	ff 75 14             	pushl  0x14(%ebp)
  800927:	ff 75 10             	pushl  0x10(%ebp)
  80092a:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80092d:	50                   	push   %eax
  80092e:	68 3a 05 80 00       	push   $0x80053a
  800933:	e8 3b fc ff ff       	call   800573 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800938:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80093b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80093e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800941:	83 c4 10             	add    $0x10,%esp
  800944:	eb 0c                	jmp    800952 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800946:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80094b:	eb 05                	jmp    800952 <vsnprintf+0x53>
  80094d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800952:	c9                   	leave  
  800953:	c3                   	ret    

00800954 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800954:	55                   	push   %ebp
  800955:	89 e5                	mov    %esp,%ebp
  800957:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80095a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80095d:	50                   	push   %eax
  80095e:	ff 75 10             	pushl  0x10(%ebp)
  800961:	ff 75 0c             	pushl  0xc(%ebp)
  800964:	ff 75 08             	pushl  0x8(%ebp)
  800967:	e8 93 ff ff ff       	call   8008ff <vsnprintf>
	va_end(ap);

	return rc;
}
  80096c:	c9                   	leave  
  80096d:	c3                   	ret    

0080096e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80096e:	55                   	push   %ebp
  80096f:	89 e5                	mov    %esp,%ebp
  800971:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800974:	b8 00 00 00 00       	mov    $0x0,%eax
  800979:	eb 01                	jmp    80097c <strlen+0xe>
		n++;
  80097b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80097c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800980:	75 f9                	jne    80097b <strlen+0xd>
		n++;
	return n;
}
  800982:	5d                   	pop    %ebp
  800983:	c3                   	ret    

00800984 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800984:	55                   	push   %ebp
  800985:	89 e5                	mov    %esp,%ebp
  800987:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80098a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80098d:	ba 00 00 00 00       	mov    $0x0,%edx
  800992:	eb 01                	jmp    800995 <strnlen+0x11>
		n++;
  800994:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800995:	39 c2                	cmp    %eax,%edx
  800997:	74 08                	je     8009a1 <strnlen+0x1d>
  800999:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80099d:	75 f5                	jne    800994 <strnlen+0x10>
  80099f:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009a1:	5d                   	pop    %ebp
  8009a2:	c3                   	ret    

008009a3 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009a3:	55                   	push   %ebp
  8009a4:	89 e5                	mov    %esp,%ebp
  8009a6:	53                   	push   %ebx
  8009a7:	8b 45 08             	mov    0x8(%ebp),%eax
  8009aa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009ad:	89 c2                	mov    %eax,%edx
  8009af:	42                   	inc    %edx
  8009b0:	41                   	inc    %ecx
  8009b1:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009b4:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009b7:	84 db                	test   %bl,%bl
  8009b9:	75 f4                	jne    8009af <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009bb:	5b                   	pop    %ebx
  8009bc:	5d                   	pop    %ebp
  8009bd:	c3                   	ret    

008009be <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009be:	55                   	push   %ebp
  8009bf:	89 e5                	mov    %esp,%ebp
  8009c1:	53                   	push   %ebx
  8009c2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009c5:	53                   	push   %ebx
  8009c6:	e8 a3 ff ff ff       	call   80096e <strlen>
  8009cb:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009ce:	ff 75 0c             	pushl  0xc(%ebp)
  8009d1:	01 d8                	add    %ebx,%eax
  8009d3:	50                   	push   %eax
  8009d4:	e8 ca ff ff ff       	call   8009a3 <strcpy>
	return dst;
}
  8009d9:	89 d8                	mov    %ebx,%eax
  8009db:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009de:	c9                   	leave  
  8009df:	c3                   	ret    

008009e0 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009e0:	55                   	push   %ebp
  8009e1:	89 e5                	mov    %esp,%ebp
  8009e3:	56                   	push   %esi
  8009e4:	53                   	push   %ebx
  8009e5:	8b 75 08             	mov    0x8(%ebp),%esi
  8009e8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009eb:	89 f3                	mov    %esi,%ebx
  8009ed:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009f0:	89 f2                	mov    %esi,%edx
  8009f2:	eb 0c                	jmp    800a00 <strncpy+0x20>
		*dst++ = *src;
  8009f4:	42                   	inc    %edx
  8009f5:	8a 01                	mov    (%ecx),%al
  8009f7:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009fa:	80 39 01             	cmpb   $0x1,(%ecx)
  8009fd:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a00:	39 da                	cmp    %ebx,%edx
  800a02:	75 f0                	jne    8009f4 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a04:	89 f0                	mov    %esi,%eax
  800a06:	5b                   	pop    %ebx
  800a07:	5e                   	pop    %esi
  800a08:	5d                   	pop    %ebp
  800a09:	c3                   	ret    

00800a0a <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a0a:	55                   	push   %ebp
  800a0b:	89 e5                	mov    %esp,%ebp
  800a0d:	56                   	push   %esi
  800a0e:	53                   	push   %ebx
  800a0f:	8b 75 08             	mov    0x8(%ebp),%esi
  800a12:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a15:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a18:	85 c0                	test   %eax,%eax
  800a1a:	74 1e                	je     800a3a <strlcpy+0x30>
  800a1c:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a20:	89 f2                	mov    %esi,%edx
  800a22:	eb 05                	jmp    800a29 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a24:	42                   	inc    %edx
  800a25:	41                   	inc    %ecx
  800a26:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a29:	39 c2                	cmp    %eax,%edx
  800a2b:	74 08                	je     800a35 <strlcpy+0x2b>
  800a2d:	8a 19                	mov    (%ecx),%bl
  800a2f:	84 db                	test   %bl,%bl
  800a31:	75 f1                	jne    800a24 <strlcpy+0x1a>
  800a33:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a35:	c6 00 00             	movb   $0x0,(%eax)
  800a38:	eb 02                	jmp    800a3c <strlcpy+0x32>
  800a3a:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a3c:	29 f0                	sub    %esi,%eax
}
  800a3e:	5b                   	pop    %ebx
  800a3f:	5e                   	pop    %esi
  800a40:	5d                   	pop    %ebp
  800a41:	c3                   	ret    

00800a42 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a42:	55                   	push   %ebp
  800a43:	89 e5                	mov    %esp,%ebp
  800a45:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a48:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a4b:	eb 02                	jmp    800a4f <strcmp+0xd>
		p++, q++;
  800a4d:	41                   	inc    %ecx
  800a4e:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a4f:	8a 01                	mov    (%ecx),%al
  800a51:	84 c0                	test   %al,%al
  800a53:	74 04                	je     800a59 <strcmp+0x17>
  800a55:	3a 02                	cmp    (%edx),%al
  800a57:	74 f4                	je     800a4d <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a59:	0f b6 c0             	movzbl %al,%eax
  800a5c:	0f b6 12             	movzbl (%edx),%edx
  800a5f:	29 d0                	sub    %edx,%eax
}
  800a61:	5d                   	pop    %ebp
  800a62:	c3                   	ret    

00800a63 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a63:	55                   	push   %ebp
  800a64:	89 e5                	mov    %esp,%ebp
  800a66:	53                   	push   %ebx
  800a67:	8b 45 08             	mov    0x8(%ebp),%eax
  800a6a:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a6d:	89 c3                	mov    %eax,%ebx
  800a6f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a72:	eb 02                	jmp    800a76 <strncmp+0x13>
		n--, p++, q++;
  800a74:	40                   	inc    %eax
  800a75:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a76:	39 d8                	cmp    %ebx,%eax
  800a78:	74 14                	je     800a8e <strncmp+0x2b>
  800a7a:	8a 08                	mov    (%eax),%cl
  800a7c:	84 c9                	test   %cl,%cl
  800a7e:	74 04                	je     800a84 <strncmp+0x21>
  800a80:	3a 0a                	cmp    (%edx),%cl
  800a82:	74 f0                	je     800a74 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a84:	0f b6 00             	movzbl (%eax),%eax
  800a87:	0f b6 12             	movzbl (%edx),%edx
  800a8a:	29 d0                	sub    %edx,%eax
  800a8c:	eb 05                	jmp    800a93 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a8e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a93:	5b                   	pop    %ebx
  800a94:	5d                   	pop    %ebp
  800a95:	c3                   	ret    

00800a96 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a96:	55                   	push   %ebp
  800a97:	89 e5                	mov    %esp,%ebp
  800a99:	8b 45 08             	mov    0x8(%ebp),%eax
  800a9c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a9f:	eb 05                	jmp    800aa6 <strchr+0x10>
		if (*s == c)
  800aa1:	38 ca                	cmp    %cl,%dl
  800aa3:	74 0c                	je     800ab1 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800aa5:	40                   	inc    %eax
  800aa6:	8a 10                	mov    (%eax),%dl
  800aa8:	84 d2                	test   %dl,%dl
  800aaa:	75 f5                	jne    800aa1 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800aac:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ab1:	5d                   	pop    %ebp
  800ab2:	c3                   	ret    

00800ab3 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ab3:	55                   	push   %ebp
  800ab4:	89 e5                	mov    %esp,%ebp
  800ab6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800abc:	eb 05                	jmp    800ac3 <strfind+0x10>
		if (*s == c)
  800abe:	38 ca                	cmp    %cl,%dl
  800ac0:	74 07                	je     800ac9 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800ac2:	40                   	inc    %eax
  800ac3:	8a 10                	mov    (%eax),%dl
  800ac5:	84 d2                	test   %dl,%dl
  800ac7:	75 f5                	jne    800abe <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800ac9:	5d                   	pop    %ebp
  800aca:	c3                   	ret    

00800acb <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800acb:	55                   	push   %ebp
  800acc:	89 e5                	mov    %esp,%ebp
  800ace:	57                   	push   %edi
  800acf:	56                   	push   %esi
  800ad0:	53                   	push   %ebx
  800ad1:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ad4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800ad7:	85 c9                	test   %ecx,%ecx
  800ad9:	74 36                	je     800b11 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800adb:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800ae1:	75 28                	jne    800b0b <memset+0x40>
  800ae3:	f6 c1 03             	test   $0x3,%cl
  800ae6:	75 23                	jne    800b0b <memset+0x40>
		c &= 0xFF;
  800ae8:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aec:	89 d3                	mov    %edx,%ebx
  800aee:	c1 e3 08             	shl    $0x8,%ebx
  800af1:	89 d6                	mov    %edx,%esi
  800af3:	c1 e6 18             	shl    $0x18,%esi
  800af6:	89 d0                	mov    %edx,%eax
  800af8:	c1 e0 10             	shl    $0x10,%eax
  800afb:	09 f0                	or     %esi,%eax
  800afd:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800aff:	89 d8                	mov    %ebx,%eax
  800b01:	09 d0                	or     %edx,%eax
  800b03:	c1 e9 02             	shr    $0x2,%ecx
  800b06:	fc                   	cld    
  800b07:	f3 ab                	rep stos %eax,%es:(%edi)
  800b09:	eb 06                	jmp    800b11 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b0b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b0e:	fc                   	cld    
  800b0f:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b11:	89 f8                	mov    %edi,%eax
  800b13:	5b                   	pop    %ebx
  800b14:	5e                   	pop    %esi
  800b15:	5f                   	pop    %edi
  800b16:	5d                   	pop    %ebp
  800b17:	c3                   	ret    

00800b18 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b18:	55                   	push   %ebp
  800b19:	89 e5                	mov    %esp,%ebp
  800b1b:	57                   	push   %edi
  800b1c:	56                   	push   %esi
  800b1d:	8b 45 08             	mov    0x8(%ebp),%eax
  800b20:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b23:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b26:	39 c6                	cmp    %eax,%esi
  800b28:	73 33                	jae    800b5d <memmove+0x45>
  800b2a:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b2d:	39 d0                	cmp    %edx,%eax
  800b2f:	73 2c                	jae    800b5d <memmove+0x45>
		s += n;
		d += n;
  800b31:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b34:	89 d6                	mov    %edx,%esi
  800b36:	09 fe                	or     %edi,%esi
  800b38:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b3e:	75 13                	jne    800b53 <memmove+0x3b>
  800b40:	f6 c1 03             	test   $0x3,%cl
  800b43:	75 0e                	jne    800b53 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b45:	83 ef 04             	sub    $0x4,%edi
  800b48:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b4b:	c1 e9 02             	shr    $0x2,%ecx
  800b4e:	fd                   	std    
  800b4f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b51:	eb 07                	jmp    800b5a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b53:	4f                   	dec    %edi
  800b54:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b57:	fd                   	std    
  800b58:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b5a:	fc                   	cld    
  800b5b:	eb 1d                	jmp    800b7a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b5d:	89 f2                	mov    %esi,%edx
  800b5f:	09 c2                	or     %eax,%edx
  800b61:	f6 c2 03             	test   $0x3,%dl
  800b64:	75 0f                	jne    800b75 <memmove+0x5d>
  800b66:	f6 c1 03             	test   $0x3,%cl
  800b69:	75 0a                	jne    800b75 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b6b:	c1 e9 02             	shr    $0x2,%ecx
  800b6e:	89 c7                	mov    %eax,%edi
  800b70:	fc                   	cld    
  800b71:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b73:	eb 05                	jmp    800b7a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b75:	89 c7                	mov    %eax,%edi
  800b77:	fc                   	cld    
  800b78:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b7a:	5e                   	pop    %esi
  800b7b:	5f                   	pop    %edi
  800b7c:	5d                   	pop    %ebp
  800b7d:	c3                   	ret    

00800b7e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b7e:	55                   	push   %ebp
  800b7f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b81:	ff 75 10             	pushl  0x10(%ebp)
  800b84:	ff 75 0c             	pushl  0xc(%ebp)
  800b87:	ff 75 08             	pushl  0x8(%ebp)
  800b8a:	e8 89 ff ff ff       	call   800b18 <memmove>
}
  800b8f:	c9                   	leave  
  800b90:	c3                   	ret    

00800b91 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b91:	55                   	push   %ebp
  800b92:	89 e5                	mov    %esp,%ebp
  800b94:	56                   	push   %esi
  800b95:	53                   	push   %ebx
  800b96:	8b 45 08             	mov    0x8(%ebp),%eax
  800b99:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b9c:	89 c6                	mov    %eax,%esi
  800b9e:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ba1:	eb 14                	jmp    800bb7 <memcmp+0x26>
		if (*s1 != *s2)
  800ba3:	8a 08                	mov    (%eax),%cl
  800ba5:	8a 1a                	mov    (%edx),%bl
  800ba7:	38 d9                	cmp    %bl,%cl
  800ba9:	74 0a                	je     800bb5 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bab:	0f b6 c1             	movzbl %cl,%eax
  800bae:	0f b6 db             	movzbl %bl,%ebx
  800bb1:	29 d8                	sub    %ebx,%eax
  800bb3:	eb 0b                	jmp    800bc0 <memcmp+0x2f>
		s1++, s2++;
  800bb5:	40                   	inc    %eax
  800bb6:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bb7:	39 f0                	cmp    %esi,%eax
  800bb9:	75 e8                	jne    800ba3 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bbb:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bc0:	5b                   	pop    %ebx
  800bc1:	5e                   	pop    %esi
  800bc2:	5d                   	pop    %ebp
  800bc3:	c3                   	ret    

00800bc4 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bc4:	55                   	push   %ebp
  800bc5:	89 e5                	mov    %esp,%ebp
  800bc7:	53                   	push   %ebx
  800bc8:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bcb:	89 c1                	mov    %eax,%ecx
  800bcd:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bd0:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bd4:	eb 08                	jmp    800bde <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bd6:	0f b6 10             	movzbl (%eax),%edx
  800bd9:	39 da                	cmp    %ebx,%edx
  800bdb:	74 05                	je     800be2 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bdd:	40                   	inc    %eax
  800bde:	39 c8                	cmp    %ecx,%eax
  800be0:	72 f4                	jb     800bd6 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800be2:	5b                   	pop    %ebx
  800be3:	5d                   	pop    %ebp
  800be4:	c3                   	ret    

00800be5 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800be5:	55                   	push   %ebp
  800be6:	89 e5                	mov    %esp,%ebp
  800be8:	57                   	push   %edi
  800be9:	56                   	push   %esi
  800bea:	53                   	push   %ebx
  800beb:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bee:	eb 01                	jmp    800bf1 <strtol+0xc>
		s++;
  800bf0:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf1:	8a 01                	mov    (%ecx),%al
  800bf3:	3c 20                	cmp    $0x20,%al
  800bf5:	74 f9                	je     800bf0 <strtol+0xb>
  800bf7:	3c 09                	cmp    $0x9,%al
  800bf9:	74 f5                	je     800bf0 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bfb:	3c 2b                	cmp    $0x2b,%al
  800bfd:	75 08                	jne    800c07 <strtol+0x22>
		s++;
  800bff:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c00:	bf 00 00 00 00       	mov    $0x0,%edi
  800c05:	eb 11                	jmp    800c18 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c07:	3c 2d                	cmp    $0x2d,%al
  800c09:	75 08                	jne    800c13 <strtol+0x2e>
		s++, neg = 1;
  800c0b:	41                   	inc    %ecx
  800c0c:	bf 01 00 00 00       	mov    $0x1,%edi
  800c11:	eb 05                	jmp    800c18 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c13:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c18:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c1c:	0f 84 87 00 00 00    	je     800ca9 <strtol+0xc4>
  800c22:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c26:	75 27                	jne    800c4f <strtol+0x6a>
  800c28:	80 39 30             	cmpb   $0x30,(%ecx)
  800c2b:	75 22                	jne    800c4f <strtol+0x6a>
  800c2d:	e9 88 00 00 00       	jmp    800cba <strtol+0xd5>
		s += 2, base = 16;
  800c32:	83 c1 02             	add    $0x2,%ecx
  800c35:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c3c:	eb 11                	jmp    800c4f <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c3e:	41                   	inc    %ecx
  800c3f:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c46:	eb 07                	jmp    800c4f <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c48:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c4f:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c54:	8a 11                	mov    (%ecx),%dl
  800c56:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c59:	80 fb 09             	cmp    $0x9,%bl
  800c5c:	77 08                	ja     800c66 <strtol+0x81>
			dig = *s - '0';
  800c5e:	0f be d2             	movsbl %dl,%edx
  800c61:	83 ea 30             	sub    $0x30,%edx
  800c64:	eb 22                	jmp    800c88 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c66:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c69:	89 f3                	mov    %esi,%ebx
  800c6b:	80 fb 19             	cmp    $0x19,%bl
  800c6e:	77 08                	ja     800c78 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c70:	0f be d2             	movsbl %dl,%edx
  800c73:	83 ea 57             	sub    $0x57,%edx
  800c76:	eb 10                	jmp    800c88 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c78:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c7b:	89 f3                	mov    %esi,%ebx
  800c7d:	80 fb 19             	cmp    $0x19,%bl
  800c80:	77 14                	ja     800c96 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c82:	0f be d2             	movsbl %dl,%edx
  800c85:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c88:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c8b:	7d 09                	jge    800c96 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c8d:	41                   	inc    %ecx
  800c8e:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c92:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c94:	eb be                	jmp    800c54 <strtol+0x6f>

	if (endptr)
  800c96:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c9a:	74 05                	je     800ca1 <strtol+0xbc>
		*endptr = (char *) s;
  800c9c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c9f:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ca1:	85 ff                	test   %edi,%edi
  800ca3:	74 21                	je     800cc6 <strtol+0xe1>
  800ca5:	f7 d8                	neg    %eax
  800ca7:	eb 1d                	jmp    800cc6 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ca9:	80 39 30             	cmpb   $0x30,(%ecx)
  800cac:	75 9a                	jne    800c48 <strtol+0x63>
  800cae:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cb2:	0f 84 7a ff ff ff    	je     800c32 <strtol+0x4d>
  800cb8:	eb 84                	jmp    800c3e <strtol+0x59>
  800cba:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cbe:	0f 84 6e ff ff ff    	je     800c32 <strtol+0x4d>
  800cc4:	eb 89                	jmp    800c4f <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cc6:	5b                   	pop    %ebx
  800cc7:	5e                   	pop    %esi
  800cc8:	5f                   	pop    %edi
  800cc9:	5d                   	pop    %ebp
  800cca:	c3                   	ret    

00800ccb <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  800ccb:	55                   	push   %ebp
  800ccc:	89 e5                	mov    %esp,%ebp
  800cce:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  800cd1:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  800cd8:	75 3e                	jne    800d18 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  800cda:	83 ec 04             	sub    $0x4,%esp
  800cdd:	6a 07                	push   $0x7
  800cdf:	68 00 f0 bf ee       	push   $0xeebff000
  800ce4:	6a 00                	push   $0x0
  800ce6:	e8 86 f4 ff ff       	call   800171 <sys_page_alloc>
  800ceb:	83 c4 10             	add    $0x10,%esp
  800cee:	85 c0                	test   %eax,%eax
  800cf0:	74 14                	je     800d06 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  800cf2:	83 ec 04             	sub    $0x4,%esp
  800cf5:	68 44 12 80 00       	push   $0x801244
  800cfa:	6a 23                	push   $0x23
  800cfc:	68 68 12 80 00       	push   $0x801268
  800d01:	e8 61 f6 ff ff       	call   800367 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  800d06:	83 ec 08             	sub    $0x8,%esp
  800d09:	68 3f 03 80 00       	push   $0x80033f
  800d0e:	6a 00                	push   $0x0
  800d10:	e8 84 f5 ff ff       	call   800299 <sys_env_set_pgfault_upcall>
  800d15:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  800d18:	8b 45 08             	mov    0x8(%ebp),%eax
  800d1b:	a3 08 20 80 00       	mov    %eax,0x802008
}
  800d20:	c9                   	leave  
  800d21:	c3                   	ret    
  800d22:	66 90                	xchg   %ax,%ax

00800d24 <__udivdi3>:
  800d24:	55                   	push   %ebp
  800d25:	57                   	push   %edi
  800d26:	56                   	push   %esi
  800d27:	53                   	push   %ebx
  800d28:	83 ec 1c             	sub    $0x1c,%esp
  800d2b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d2f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d33:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d37:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d3b:	89 ca                	mov    %ecx,%edx
  800d3d:	89 f8                	mov    %edi,%eax
  800d3f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800d43:	85 f6                	test   %esi,%esi
  800d45:	75 2d                	jne    800d74 <__udivdi3+0x50>
  800d47:	39 cf                	cmp    %ecx,%edi
  800d49:	77 65                	ja     800db0 <__udivdi3+0x8c>
  800d4b:	89 fd                	mov    %edi,%ebp
  800d4d:	85 ff                	test   %edi,%edi
  800d4f:	75 0b                	jne    800d5c <__udivdi3+0x38>
  800d51:	b8 01 00 00 00       	mov    $0x1,%eax
  800d56:	31 d2                	xor    %edx,%edx
  800d58:	f7 f7                	div    %edi
  800d5a:	89 c5                	mov    %eax,%ebp
  800d5c:	31 d2                	xor    %edx,%edx
  800d5e:	89 c8                	mov    %ecx,%eax
  800d60:	f7 f5                	div    %ebp
  800d62:	89 c1                	mov    %eax,%ecx
  800d64:	89 d8                	mov    %ebx,%eax
  800d66:	f7 f5                	div    %ebp
  800d68:	89 cf                	mov    %ecx,%edi
  800d6a:	89 fa                	mov    %edi,%edx
  800d6c:	83 c4 1c             	add    $0x1c,%esp
  800d6f:	5b                   	pop    %ebx
  800d70:	5e                   	pop    %esi
  800d71:	5f                   	pop    %edi
  800d72:	5d                   	pop    %ebp
  800d73:	c3                   	ret    
  800d74:	39 ce                	cmp    %ecx,%esi
  800d76:	77 28                	ja     800da0 <__udivdi3+0x7c>
  800d78:	0f bd fe             	bsr    %esi,%edi
  800d7b:	83 f7 1f             	xor    $0x1f,%edi
  800d7e:	75 40                	jne    800dc0 <__udivdi3+0x9c>
  800d80:	39 ce                	cmp    %ecx,%esi
  800d82:	72 0a                	jb     800d8e <__udivdi3+0x6a>
  800d84:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d88:	0f 87 9e 00 00 00    	ja     800e2c <__udivdi3+0x108>
  800d8e:	b8 01 00 00 00       	mov    $0x1,%eax
  800d93:	89 fa                	mov    %edi,%edx
  800d95:	83 c4 1c             	add    $0x1c,%esp
  800d98:	5b                   	pop    %ebx
  800d99:	5e                   	pop    %esi
  800d9a:	5f                   	pop    %edi
  800d9b:	5d                   	pop    %ebp
  800d9c:	c3                   	ret    
  800d9d:	8d 76 00             	lea    0x0(%esi),%esi
  800da0:	31 ff                	xor    %edi,%edi
  800da2:	31 c0                	xor    %eax,%eax
  800da4:	89 fa                	mov    %edi,%edx
  800da6:	83 c4 1c             	add    $0x1c,%esp
  800da9:	5b                   	pop    %ebx
  800daa:	5e                   	pop    %esi
  800dab:	5f                   	pop    %edi
  800dac:	5d                   	pop    %ebp
  800dad:	c3                   	ret    
  800dae:	66 90                	xchg   %ax,%ax
  800db0:	89 d8                	mov    %ebx,%eax
  800db2:	f7 f7                	div    %edi
  800db4:	31 ff                	xor    %edi,%edi
  800db6:	89 fa                	mov    %edi,%edx
  800db8:	83 c4 1c             	add    $0x1c,%esp
  800dbb:	5b                   	pop    %ebx
  800dbc:	5e                   	pop    %esi
  800dbd:	5f                   	pop    %edi
  800dbe:	5d                   	pop    %ebp
  800dbf:	c3                   	ret    
  800dc0:	bd 20 00 00 00       	mov    $0x20,%ebp
  800dc5:	89 eb                	mov    %ebp,%ebx
  800dc7:	29 fb                	sub    %edi,%ebx
  800dc9:	89 f9                	mov    %edi,%ecx
  800dcb:	d3 e6                	shl    %cl,%esi
  800dcd:	89 c5                	mov    %eax,%ebp
  800dcf:	88 d9                	mov    %bl,%cl
  800dd1:	d3 ed                	shr    %cl,%ebp
  800dd3:	89 e9                	mov    %ebp,%ecx
  800dd5:	09 f1                	or     %esi,%ecx
  800dd7:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800ddb:	89 f9                	mov    %edi,%ecx
  800ddd:	d3 e0                	shl    %cl,%eax
  800ddf:	89 c5                	mov    %eax,%ebp
  800de1:	89 d6                	mov    %edx,%esi
  800de3:	88 d9                	mov    %bl,%cl
  800de5:	d3 ee                	shr    %cl,%esi
  800de7:	89 f9                	mov    %edi,%ecx
  800de9:	d3 e2                	shl    %cl,%edx
  800deb:	8b 44 24 08          	mov    0x8(%esp),%eax
  800def:	88 d9                	mov    %bl,%cl
  800df1:	d3 e8                	shr    %cl,%eax
  800df3:	09 c2                	or     %eax,%edx
  800df5:	89 d0                	mov    %edx,%eax
  800df7:	89 f2                	mov    %esi,%edx
  800df9:	f7 74 24 0c          	divl   0xc(%esp)
  800dfd:	89 d6                	mov    %edx,%esi
  800dff:	89 c3                	mov    %eax,%ebx
  800e01:	f7 e5                	mul    %ebp
  800e03:	39 d6                	cmp    %edx,%esi
  800e05:	72 19                	jb     800e20 <__udivdi3+0xfc>
  800e07:	74 0b                	je     800e14 <__udivdi3+0xf0>
  800e09:	89 d8                	mov    %ebx,%eax
  800e0b:	31 ff                	xor    %edi,%edi
  800e0d:	e9 58 ff ff ff       	jmp    800d6a <__udivdi3+0x46>
  800e12:	66 90                	xchg   %ax,%ax
  800e14:	8b 54 24 08          	mov    0x8(%esp),%edx
  800e18:	89 f9                	mov    %edi,%ecx
  800e1a:	d3 e2                	shl    %cl,%edx
  800e1c:	39 c2                	cmp    %eax,%edx
  800e1e:	73 e9                	jae    800e09 <__udivdi3+0xe5>
  800e20:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800e23:	31 ff                	xor    %edi,%edi
  800e25:	e9 40 ff ff ff       	jmp    800d6a <__udivdi3+0x46>
  800e2a:	66 90                	xchg   %ax,%ax
  800e2c:	31 c0                	xor    %eax,%eax
  800e2e:	e9 37 ff ff ff       	jmp    800d6a <__udivdi3+0x46>
  800e33:	90                   	nop

00800e34 <__umoddi3>:
  800e34:	55                   	push   %ebp
  800e35:	57                   	push   %edi
  800e36:	56                   	push   %esi
  800e37:	53                   	push   %ebx
  800e38:	83 ec 1c             	sub    $0x1c,%esp
  800e3b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800e3f:	8b 74 24 34          	mov    0x34(%esp),%esi
  800e43:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e47:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e4b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e4f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e53:	89 f3                	mov    %esi,%ebx
  800e55:	89 fa                	mov    %edi,%edx
  800e57:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e5b:	89 34 24             	mov    %esi,(%esp)
  800e5e:	85 c0                	test   %eax,%eax
  800e60:	75 1a                	jne    800e7c <__umoddi3+0x48>
  800e62:	39 f7                	cmp    %esi,%edi
  800e64:	0f 86 a2 00 00 00    	jbe    800f0c <__umoddi3+0xd8>
  800e6a:	89 c8                	mov    %ecx,%eax
  800e6c:	89 f2                	mov    %esi,%edx
  800e6e:	f7 f7                	div    %edi
  800e70:	89 d0                	mov    %edx,%eax
  800e72:	31 d2                	xor    %edx,%edx
  800e74:	83 c4 1c             	add    $0x1c,%esp
  800e77:	5b                   	pop    %ebx
  800e78:	5e                   	pop    %esi
  800e79:	5f                   	pop    %edi
  800e7a:	5d                   	pop    %ebp
  800e7b:	c3                   	ret    
  800e7c:	39 f0                	cmp    %esi,%eax
  800e7e:	0f 87 ac 00 00 00    	ja     800f30 <__umoddi3+0xfc>
  800e84:	0f bd e8             	bsr    %eax,%ebp
  800e87:	83 f5 1f             	xor    $0x1f,%ebp
  800e8a:	0f 84 ac 00 00 00    	je     800f3c <__umoddi3+0x108>
  800e90:	bf 20 00 00 00       	mov    $0x20,%edi
  800e95:	29 ef                	sub    %ebp,%edi
  800e97:	89 fe                	mov    %edi,%esi
  800e99:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e9d:	89 e9                	mov    %ebp,%ecx
  800e9f:	d3 e0                	shl    %cl,%eax
  800ea1:	89 d7                	mov    %edx,%edi
  800ea3:	89 f1                	mov    %esi,%ecx
  800ea5:	d3 ef                	shr    %cl,%edi
  800ea7:	09 c7                	or     %eax,%edi
  800ea9:	89 e9                	mov    %ebp,%ecx
  800eab:	d3 e2                	shl    %cl,%edx
  800ead:	89 14 24             	mov    %edx,(%esp)
  800eb0:	89 d8                	mov    %ebx,%eax
  800eb2:	d3 e0                	shl    %cl,%eax
  800eb4:	89 c2                	mov    %eax,%edx
  800eb6:	8b 44 24 08          	mov    0x8(%esp),%eax
  800eba:	d3 e0                	shl    %cl,%eax
  800ebc:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ec0:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ec4:	89 f1                	mov    %esi,%ecx
  800ec6:	d3 e8                	shr    %cl,%eax
  800ec8:	09 d0                	or     %edx,%eax
  800eca:	d3 eb                	shr    %cl,%ebx
  800ecc:	89 da                	mov    %ebx,%edx
  800ece:	f7 f7                	div    %edi
  800ed0:	89 d3                	mov    %edx,%ebx
  800ed2:	f7 24 24             	mull   (%esp)
  800ed5:	89 c6                	mov    %eax,%esi
  800ed7:	89 d1                	mov    %edx,%ecx
  800ed9:	39 d3                	cmp    %edx,%ebx
  800edb:	0f 82 87 00 00 00    	jb     800f68 <__umoddi3+0x134>
  800ee1:	0f 84 91 00 00 00    	je     800f78 <__umoddi3+0x144>
  800ee7:	8b 54 24 04          	mov    0x4(%esp),%edx
  800eeb:	29 f2                	sub    %esi,%edx
  800eed:	19 cb                	sbb    %ecx,%ebx
  800eef:	89 d8                	mov    %ebx,%eax
  800ef1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ef5:	d3 e0                	shl    %cl,%eax
  800ef7:	89 e9                	mov    %ebp,%ecx
  800ef9:	d3 ea                	shr    %cl,%edx
  800efb:	09 d0                	or     %edx,%eax
  800efd:	89 e9                	mov    %ebp,%ecx
  800eff:	d3 eb                	shr    %cl,%ebx
  800f01:	89 da                	mov    %ebx,%edx
  800f03:	83 c4 1c             	add    $0x1c,%esp
  800f06:	5b                   	pop    %ebx
  800f07:	5e                   	pop    %esi
  800f08:	5f                   	pop    %edi
  800f09:	5d                   	pop    %ebp
  800f0a:	c3                   	ret    
  800f0b:	90                   	nop
  800f0c:	89 fd                	mov    %edi,%ebp
  800f0e:	85 ff                	test   %edi,%edi
  800f10:	75 0b                	jne    800f1d <__umoddi3+0xe9>
  800f12:	b8 01 00 00 00       	mov    $0x1,%eax
  800f17:	31 d2                	xor    %edx,%edx
  800f19:	f7 f7                	div    %edi
  800f1b:	89 c5                	mov    %eax,%ebp
  800f1d:	89 f0                	mov    %esi,%eax
  800f1f:	31 d2                	xor    %edx,%edx
  800f21:	f7 f5                	div    %ebp
  800f23:	89 c8                	mov    %ecx,%eax
  800f25:	f7 f5                	div    %ebp
  800f27:	89 d0                	mov    %edx,%eax
  800f29:	e9 44 ff ff ff       	jmp    800e72 <__umoddi3+0x3e>
  800f2e:	66 90                	xchg   %ax,%ax
  800f30:	89 c8                	mov    %ecx,%eax
  800f32:	89 f2                	mov    %esi,%edx
  800f34:	83 c4 1c             	add    $0x1c,%esp
  800f37:	5b                   	pop    %ebx
  800f38:	5e                   	pop    %esi
  800f39:	5f                   	pop    %edi
  800f3a:	5d                   	pop    %ebp
  800f3b:	c3                   	ret    
  800f3c:	3b 04 24             	cmp    (%esp),%eax
  800f3f:	72 06                	jb     800f47 <__umoddi3+0x113>
  800f41:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f45:	77 0f                	ja     800f56 <__umoddi3+0x122>
  800f47:	89 f2                	mov    %esi,%edx
  800f49:	29 f9                	sub    %edi,%ecx
  800f4b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f4f:	89 14 24             	mov    %edx,(%esp)
  800f52:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f56:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f5a:	8b 14 24             	mov    (%esp),%edx
  800f5d:	83 c4 1c             	add    $0x1c,%esp
  800f60:	5b                   	pop    %ebx
  800f61:	5e                   	pop    %esi
  800f62:	5f                   	pop    %edi
  800f63:	5d                   	pop    %ebp
  800f64:	c3                   	ret    
  800f65:	8d 76 00             	lea    0x0(%esi),%esi
  800f68:	2b 04 24             	sub    (%esp),%eax
  800f6b:	19 fa                	sbb    %edi,%edx
  800f6d:	89 d1                	mov    %edx,%ecx
  800f6f:	89 c6                	mov    %eax,%esi
  800f71:	e9 71 ff ff ff       	jmp    800ee7 <__umoddi3+0xb3>
  800f76:	66 90                	xchg   %ax,%ax
  800f78:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f7c:	72 ea                	jb     800f68 <__umoddi3+0x134>
  800f7e:	89 d9                	mov    %ebx,%ecx
  800f80:	e9 62 ff ff ff       	jmp    800ee7 <__umoddi3+0xb3>
