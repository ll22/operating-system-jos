
obj/user/buggyhello2:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 1d 00 00 00       	call   80004e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

const char *hello = "hello, world\n";

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs(hello, 1024*1024);
  800039:	68 00 00 10 00       	push   $0x100000
  80003e:	ff 35 00 20 80 00    	pushl  0x802000
  800044:	e8 66 00 00 00       	call   8000af <sys_cputs>
}
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	c9                   	leave  
  80004d:	c3                   	ret    

0080004e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004e:	55                   	push   %ebp
  80004f:	89 e5                	mov    %esp,%ebp
  800051:	56                   	push   %esi
  800052:	53                   	push   %ebx
  800053:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800056:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800059:	e8 cf 00 00 00       	call   80012d <sys_getenvid>
  80005e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800063:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80006a:	c1 e0 07             	shl    $0x7,%eax
  80006d:	29 d0                	sub    %edx,%eax
  80006f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800074:	a3 08 20 80 00       	mov    %eax,0x802008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800079:	85 db                	test   %ebx,%ebx
  80007b:	7e 07                	jle    800084 <libmain+0x36>
		binaryname = argv[0];
  80007d:	8b 06                	mov    (%esi),%eax
  80007f:	a3 04 20 80 00       	mov    %eax,0x802004

	// call user main routine
	umain(argc, argv);
  800084:	83 ec 08             	sub    $0x8,%esp
  800087:	56                   	push   %esi
  800088:	53                   	push   %ebx
  800089:	e8 a5 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008e:	e8 0a 00 00 00       	call   80009d <exit>
}
  800093:	83 c4 10             	add    $0x10,%esp
  800096:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800099:	5b                   	pop    %ebx
  80009a:	5e                   	pop    %esi
  80009b:	5d                   	pop    %ebp
  80009c:	c3                   	ret    

0080009d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80009d:	55                   	push   %ebp
  80009e:	89 e5                	mov    %esp,%ebp
  8000a0:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000a3:	6a 00                	push   $0x0
  8000a5:	e8 42 00 00 00       	call   8000ec <sys_env_destroy>
}
  8000aa:	83 c4 10             	add    $0x10,%esp
  8000ad:	c9                   	leave  
  8000ae:	c3                   	ret    

008000af <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000af:	55                   	push   %ebp
  8000b0:	89 e5                	mov    %esp,%ebp
  8000b2:	57                   	push   %edi
  8000b3:	56                   	push   %esi
  8000b4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000b5:	b8 00 00 00 00       	mov    $0x0,%eax
  8000ba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000bd:	8b 55 08             	mov    0x8(%ebp),%edx
  8000c0:	89 c3                	mov    %eax,%ebx
  8000c2:	89 c7                	mov    %eax,%edi
  8000c4:	89 c6                	mov    %eax,%esi
  8000c6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c8:	5b                   	pop    %ebx
  8000c9:	5e                   	pop    %esi
  8000ca:	5f                   	pop    %edi
  8000cb:	5d                   	pop    %ebp
  8000cc:	c3                   	ret    

008000cd <sys_cgetc>:

int
sys_cgetc(void)
{
  8000cd:	55                   	push   %ebp
  8000ce:	89 e5                	mov    %esp,%ebp
  8000d0:	57                   	push   %edi
  8000d1:	56                   	push   %esi
  8000d2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000d3:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d8:	b8 01 00 00 00       	mov    $0x1,%eax
  8000dd:	89 d1                	mov    %edx,%ecx
  8000df:	89 d3                	mov    %edx,%ebx
  8000e1:	89 d7                	mov    %edx,%edi
  8000e3:	89 d6                	mov    %edx,%esi
  8000e5:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e7:	5b                   	pop    %ebx
  8000e8:	5e                   	pop    %esi
  8000e9:	5f                   	pop    %edi
  8000ea:	5d                   	pop    %ebp
  8000eb:	c3                   	ret    

008000ec <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000ec:	55                   	push   %ebp
  8000ed:	89 e5                	mov    %esp,%ebp
  8000ef:	57                   	push   %edi
  8000f0:	56                   	push   %esi
  8000f1:	53                   	push   %ebx
  8000f2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000f5:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000fa:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ff:	8b 55 08             	mov    0x8(%ebp),%edx
  800102:	89 cb                	mov    %ecx,%ebx
  800104:	89 cf                	mov    %ecx,%edi
  800106:	89 ce                	mov    %ecx,%esi
  800108:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80010a:	85 c0                	test   %eax,%eax
  80010c:	7e 17                	jle    800125 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80010e:	83 ec 0c             	sub    $0xc,%esp
  800111:	50                   	push   %eax
  800112:	6a 03                	push   $0x3
  800114:	68 38 0f 80 00       	push   $0x800f38
  800119:	6a 23                	push   $0x23
  80011b:	68 55 0f 80 00       	push   $0x800f55
  800120:	e8 14 02 00 00       	call   800339 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800125:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800128:	5b                   	pop    %ebx
  800129:	5e                   	pop    %esi
  80012a:	5f                   	pop    %edi
  80012b:	5d                   	pop    %ebp
  80012c:	c3                   	ret    

0080012d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  80012d:	55                   	push   %ebp
  80012e:	89 e5                	mov    %esp,%ebp
  800130:	57                   	push   %edi
  800131:	56                   	push   %esi
  800132:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800133:	ba 00 00 00 00       	mov    $0x0,%edx
  800138:	b8 02 00 00 00       	mov    $0x2,%eax
  80013d:	89 d1                	mov    %edx,%ecx
  80013f:	89 d3                	mov    %edx,%ebx
  800141:	89 d7                	mov    %edx,%edi
  800143:	89 d6                	mov    %edx,%esi
  800145:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800147:	5b                   	pop    %ebx
  800148:	5e                   	pop    %esi
  800149:	5f                   	pop    %edi
  80014a:	5d                   	pop    %ebp
  80014b:	c3                   	ret    

0080014c <sys_yield>:

void
sys_yield(void)
{
  80014c:	55                   	push   %ebp
  80014d:	89 e5                	mov    %esp,%ebp
  80014f:	57                   	push   %edi
  800150:	56                   	push   %esi
  800151:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800152:	ba 00 00 00 00       	mov    $0x0,%edx
  800157:	b8 0a 00 00 00       	mov    $0xa,%eax
  80015c:	89 d1                	mov    %edx,%ecx
  80015e:	89 d3                	mov    %edx,%ebx
  800160:	89 d7                	mov    %edx,%edi
  800162:	89 d6                	mov    %edx,%esi
  800164:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800166:	5b                   	pop    %ebx
  800167:	5e                   	pop    %esi
  800168:	5f                   	pop    %edi
  800169:	5d                   	pop    %ebp
  80016a:	c3                   	ret    

0080016b <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80016b:	55                   	push   %ebp
  80016c:	89 e5                	mov    %esp,%ebp
  80016e:	57                   	push   %edi
  80016f:	56                   	push   %esi
  800170:	53                   	push   %ebx
  800171:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800174:	be 00 00 00 00       	mov    $0x0,%esi
  800179:	b8 04 00 00 00       	mov    $0x4,%eax
  80017e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800181:	8b 55 08             	mov    0x8(%ebp),%edx
  800184:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800187:	89 f7                	mov    %esi,%edi
  800189:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80018b:	85 c0                	test   %eax,%eax
  80018d:	7e 17                	jle    8001a6 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80018f:	83 ec 0c             	sub    $0xc,%esp
  800192:	50                   	push   %eax
  800193:	6a 04                	push   $0x4
  800195:	68 38 0f 80 00       	push   $0x800f38
  80019a:	6a 23                	push   $0x23
  80019c:	68 55 0f 80 00       	push   $0x800f55
  8001a1:	e8 93 01 00 00       	call   800339 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001a6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a9:	5b                   	pop    %ebx
  8001aa:	5e                   	pop    %esi
  8001ab:	5f                   	pop    %edi
  8001ac:	5d                   	pop    %ebp
  8001ad:	c3                   	ret    

008001ae <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001ae:	55                   	push   %ebp
  8001af:	89 e5                	mov    %esp,%ebp
  8001b1:	57                   	push   %edi
  8001b2:	56                   	push   %esi
  8001b3:	53                   	push   %ebx
  8001b4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001b7:	b8 05 00 00 00       	mov    $0x5,%eax
  8001bc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001bf:	8b 55 08             	mov    0x8(%ebp),%edx
  8001c2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001c5:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001c8:	8b 75 18             	mov    0x18(%ebp),%esi
  8001cb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001cd:	85 c0                	test   %eax,%eax
  8001cf:	7e 17                	jle    8001e8 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001d1:	83 ec 0c             	sub    $0xc,%esp
  8001d4:	50                   	push   %eax
  8001d5:	6a 05                	push   $0x5
  8001d7:	68 38 0f 80 00       	push   $0x800f38
  8001dc:	6a 23                	push   $0x23
  8001de:	68 55 0f 80 00       	push   $0x800f55
  8001e3:	e8 51 01 00 00       	call   800339 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001e8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001eb:	5b                   	pop    %ebx
  8001ec:	5e                   	pop    %esi
  8001ed:	5f                   	pop    %edi
  8001ee:	5d                   	pop    %ebp
  8001ef:	c3                   	ret    

008001f0 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001f0:	55                   	push   %ebp
  8001f1:	89 e5                	mov    %esp,%ebp
  8001f3:	57                   	push   %edi
  8001f4:	56                   	push   %esi
  8001f5:	53                   	push   %ebx
  8001f6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001f9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001fe:	b8 06 00 00 00       	mov    $0x6,%eax
  800203:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800206:	8b 55 08             	mov    0x8(%ebp),%edx
  800209:	89 df                	mov    %ebx,%edi
  80020b:	89 de                	mov    %ebx,%esi
  80020d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80020f:	85 c0                	test   %eax,%eax
  800211:	7e 17                	jle    80022a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800213:	83 ec 0c             	sub    $0xc,%esp
  800216:	50                   	push   %eax
  800217:	6a 06                	push   $0x6
  800219:	68 38 0f 80 00       	push   $0x800f38
  80021e:	6a 23                	push   $0x23
  800220:	68 55 0f 80 00       	push   $0x800f55
  800225:	e8 0f 01 00 00       	call   800339 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80022a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80022d:	5b                   	pop    %ebx
  80022e:	5e                   	pop    %esi
  80022f:	5f                   	pop    %edi
  800230:	5d                   	pop    %ebp
  800231:	c3                   	ret    

00800232 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800232:	55                   	push   %ebp
  800233:	89 e5                	mov    %esp,%ebp
  800235:	57                   	push   %edi
  800236:	56                   	push   %esi
  800237:	53                   	push   %ebx
  800238:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80023b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800240:	b8 08 00 00 00       	mov    $0x8,%eax
  800245:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800248:	8b 55 08             	mov    0x8(%ebp),%edx
  80024b:	89 df                	mov    %ebx,%edi
  80024d:	89 de                	mov    %ebx,%esi
  80024f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800251:	85 c0                	test   %eax,%eax
  800253:	7e 17                	jle    80026c <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800255:	83 ec 0c             	sub    $0xc,%esp
  800258:	50                   	push   %eax
  800259:	6a 08                	push   $0x8
  80025b:	68 38 0f 80 00       	push   $0x800f38
  800260:	6a 23                	push   $0x23
  800262:	68 55 0f 80 00       	push   $0x800f55
  800267:	e8 cd 00 00 00       	call   800339 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  80026c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80026f:	5b                   	pop    %ebx
  800270:	5e                   	pop    %esi
  800271:	5f                   	pop    %edi
  800272:	5d                   	pop    %ebp
  800273:	c3                   	ret    

00800274 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800274:	55                   	push   %ebp
  800275:	89 e5                	mov    %esp,%ebp
  800277:	57                   	push   %edi
  800278:	56                   	push   %esi
  800279:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80027a:	ba 00 00 00 00       	mov    $0x0,%edx
  80027f:	b8 0b 00 00 00       	mov    $0xb,%eax
  800284:	89 d1                	mov    %edx,%ecx
  800286:	89 d3                	mov    %edx,%ebx
  800288:	89 d7                	mov    %edx,%edi
  80028a:	89 d6                	mov    %edx,%esi
  80028c:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80028e:	5b                   	pop    %ebx
  80028f:	5e                   	pop    %esi
  800290:	5f                   	pop    %edi
  800291:	5d                   	pop    %ebp
  800292:	c3                   	ret    

00800293 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800293:	55                   	push   %ebp
  800294:	89 e5                	mov    %esp,%ebp
  800296:	57                   	push   %edi
  800297:	56                   	push   %esi
  800298:	53                   	push   %ebx
  800299:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80029c:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002a1:	b8 09 00 00 00       	mov    $0x9,%eax
  8002a6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002a9:	8b 55 08             	mov    0x8(%ebp),%edx
  8002ac:	89 df                	mov    %ebx,%edi
  8002ae:	89 de                	mov    %ebx,%esi
  8002b0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002b2:	85 c0                	test   %eax,%eax
  8002b4:	7e 17                	jle    8002cd <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002b6:	83 ec 0c             	sub    $0xc,%esp
  8002b9:	50                   	push   %eax
  8002ba:	6a 09                	push   $0x9
  8002bc:	68 38 0f 80 00       	push   $0x800f38
  8002c1:	6a 23                	push   $0x23
  8002c3:	68 55 0f 80 00       	push   $0x800f55
  8002c8:	e8 6c 00 00 00       	call   800339 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002cd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d0:	5b                   	pop    %ebx
  8002d1:	5e                   	pop    %esi
  8002d2:	5f                   	pop    %edi
  8002d3:	5d                   	pop    %ebp
  8002d4:	c3                   	ret    

008002d5 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002d5:	55                   	push   %ebp
  8002d6:	89 e5                	mov    %esp,%ebp
  8002d8:	57                   	push   %edi
  8002d9:	56                   	push   %esi
  8002da:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002db:	be 00 00 00 00       	mov    $0x0,%esi
  8002e0:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002e5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002e8:	8b 55 08             	mov    0x8(%ebp),%edx
  8002eb:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002ee:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002f1:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002f3:	5b                   	pop    %ebx
  8002f4:	5e                   	pop    %esi
  8002f5:	5f                   	pop    %edi
  8002f6:	5d                   	pop    %ebp
  8002f7:	c3                   	ret    

008002f8 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002f8:	55                   	push   %ebp
  8002f9:	89 e5                	mov    %esp,%ebp
  8002fb:	57                   	push   %edi
  8002fc:	56                   	push   %esi
  8002fd:	53                   	push   %ebx
  8002fe:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800301:	b9 00 00 00 00       	mov    $0x0,%ecx
  800306:	b8 0d 00 00 00       	mov    $0xd,%eax
  80030b:	8b 55 08             	mov    0x8(%ebp),%edx
  80030e:	89 cb                	mov    %ecx,%ebx
  800310:	89 cf                	mov    %ecx,%edi
  800312:	89 ce                	mov    %ecx,%esi
  800314:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800316:	85 c0                	test   %eax,%eax
  800318:	7e 17                	jle    800331 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80031a:	83 ec 0c             	sub    $0xc,%esp
  80031d:	50                   	push   %eax
  80031e:	6a 0d                	push   $0xd
  800320:	68 38 0f 80 00       	push   $0x800f38
  800325:	6a 23                	push   $0x23
  800327:	68 55 0f 80 00       	push   $0x800f55
  80032c:	e8 08 00 00 00       	call   800339 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800331:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800334:	5b                   	pop    %ebx
  800335:	5e                   	pop    %esi
  800336:	5f                   	pop    %edi
  800337:	5d                   	pop    %ebp
  800338:	c3                   	ret    

00800339 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800339:	55                   	push   %ebp
  80033a:	89 e5                	mov    %esp,%ebp
  80033c:	56                   	push   %esi
  80033d:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80033e:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800341:	8b 35 04 20 80 00    	mov    0x802004,%esi
  800347:	e8 e1 fd ff ff       	call   80012d <sys_getenvid>
  80034c:	83 ec 0c             	sub    $0xc,%esp
  80034f:	ff 75 0c             	pushl  0xc(%ebp)
  800352:	ff 75 08             	pushl  0x8(%ebp)
  800355:	56                   	push   %esi
  800356:	50                   	push   %eax
  800357:	68 64 0f 80 00       	push   $0x800f64
  80035c:	e8 b0 00 00 00       	call   800411 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800361:	83 c4 18             	add    $0x18,%esp
  800364:	53                   	push   %ebx
  800365:	ff 75 10             	pushl  0x10(%ebp)
  800368:	e8 53 00 00 00       	call   8003c0 <vcprintf>
	cprintf("\n");
  80036d:	c7 04 24 2c 0f 80 00 	movl   $0x800f2c,(%esp)
  800374:	e8 98 00 00 00       	call   800411 <cprintf>
  800379:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80037c:	cc                   	int3   
  80037d:	eb fd                	jmp    80037c <_panic+0x43>

0080037f <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80037f:	55                   	push   %ebp
  800380:	89 e5                	mov    %esp,%ebp
  800382:	53                   	push   %ebx
  800383:	83 ec 04             	sub    $0x4,%esp
  800386:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800389:	8b 13                	mov    (%ebx),%edx
  80038b:	8d 42 01             	lea    0x1(%edx),%eax
  80038e:	89 03                	mov    %eax,(%ebx)
  800390:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800393:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800397:	3d ff 00 00 00       	cmp    $0xff,%eax
  80039c:	75 1a                	jne    8003b8 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80039e:	83 ec 08             	sub    $0x8,%esp
  8003a1:	68 ff 00 00 00       	push   $0xff
  8003a6:	8d 43 08             	lea    0x8(%ebx),%eax
  8003a9:	50                   	push   %eax
  8003aa:	e8 00 fd ff ff       	call   8000af <sys_cputs>
		b->idx = 0;
  8003af:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003b5:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003b8:	ff 43 04             	incl   0x4(%ebx)
}
  8003bb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003be:	c9                   	leave  
  8003bf:	c3                   	ret    

008003c0 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003c0:	55                   	push   %ebp
  8003c1:	89 e5                	mov    %esp,%ebp
  8003c3:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003c9:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003d0:	00 00 00 
	b.cnt = 0;
  8003d3:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003da:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003dd:	ff 75 0c             	pushl  0xc(%ebp)
  8003e0:	ff 75 08             	pushl  0x8(%ebp)
  8003e3:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003e9:	50                   	push   %eax
  8003ea:	68 7f 03 80 00       	push   $0x80037f
  8003ef:	e8 51 01 00 00       	call   800545 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003f4:	83 c4 08             	add    $0x8,%esp
  8003f7:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003fd:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800403:	50                   	push   %eax
  800404:	e8 a6 fc ff ff       	call   8000af <sys_cputs>

	return b.cnt;
}
  800409:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80040f:	c9                   	leave  
  800410:	c3                   	ret    

00800411 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800411:	55                   	push   %ebp
  800412:	89 e5                	mov    %esp,%ebp
  800414:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800417:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80041a:	50                   	push   %eax
  80041b:	ff 75 08             	pushl  0x8(%ebp)
  80041e:	e8 9d ff ff ff       	call   8003c0 <vcprintf>
	va_end(ap);

	return cnt;
}
  800423:	c9                   	leave  
  800424:	c3                   	ret    

00800425 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800425:	55                   	push   %ebp
  800426:	89 e5                	mov    %esp,%ebp
  800428:	57                   	push   %edi
  800429:	56                   	push   %esi
  80042a:	53                   	push   %ebx
  80042b:	83 ec 1c             	sub    $0x1c,%esp
  80042e:	89 c7                	mov    %eax,%edi
  800430:	89 d6                	mov    %edx,%esi
  800432:	8b 45 08             	mov    0x8(%ebp),%eax
  800435:	8b 55 0c             	mov    0xc(%ebp),%edx
  800438:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80043b:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80043e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800441:	bb 00 00 00 00       	mov    $0x0,%ebx
  800446:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800449:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80044c:	39 d3                	cmp    %edx,%ebx
  80044e:	72 05                	jb     800455 <printnum+0x30>
  800450:	39 45 10             	cmp    %eax,0x10(%ebp)
  800453:	77 45                	ja     80049a <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800455:	83 ec 0c             	sub    $0xc,%esp
  800458:	ff 75 18             	pushl  0x18(%ebp)
  80045b:	8b 45 14             	mov    0x14(%ebp),%eax
  80045e:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800461:	53                   	push   %ebx
  800462:	ff 75 10             	pushl  0x10(%ebp)
  800465:	83 ec 08             	sub    $0x8,%esp
  800468:	ff 75 e4             	pushl  -0x1c(%ebp)
  80046b:	ff 75 e0             	pushl  -0x20(%ebp)
  80046e:	ff 75 dc             	pushl  -0x24(%ebp)
  800471:	ff 75 d8             	pushl  -0x28(%ebp)
  800474:	e8 27 08 00 00       	call   800ca0 <__udivdi3>
  800479:	83 c4 18             	add    $0x18,%esp
  80047c:	52                   	push   %edx
  80047d:	50                   	push   %eax
  80047e:	89 f2                	mov    %esi,%edx
  800480:	89 f8                	mov    %edi,%eax
  800482:	e8 9e ff ff ff       	call   800425 <printnum>
  800487:	83 c4 20             	add    $0x20,%esp
  80048a:	eb 16                	jmp    8004a2 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80048c:	83 ec 08             	sub    $0x8,%esp
  80048f:	56                   	push   %esi
  800490:	ff 75 18             	pushl  0x18(%ebp)
  800493:	ff d7                	call   *%edi
  800495:	83 c4 10             	add    $0x10,%esp
  800498:	eb 03                	jmp    80049d <printnum+0x78>
  80049a:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80049d:	4b                   	dec    %ebx
  80049e:	85 db                	test   %ebx,%ebx
  8004a0:	7f ea                	jg     80048c <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004a2:	83 ec 08             	sub    $0x8,%esp
  8004a5:	56                   	push   %esi
  8004a6:	83 ec 04             	sub    $0x4,%esp
  8004a9:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004ac:	ff 75 e0             	pushl  -0x20(%ebp)
  8004af:	ff 75 dc             	pushl  -0x24(%ebp)
  8004b2:	ff 75 d8             	pushl  -0x28(%ebp)
  8004b5:	e8 f6 08 00 00       	call   800db0 <__umoddi3>
  8004ba:	83 c4 14             	add    $0x14,%esp
  8004bd:	0f be 80 88 0f 80 00 	movsbl 0x800f88(%eax),%eax
  8004c4:	50                   	push   %eax
  8004c5:	ff d7                	call   *%edi
}
  8004c7:	83 c4 10             	add    $0x10,%esp
  8004ca:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004cd:	5b                   	pop    %ebx
  8004ce:	5e                   	pop    %esi
  8004cf:	5f                   	pop    %edi
  8004d0:	5d                   	pop    %ebp
  8004d1:	c3                   	ret    

008004d2 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004d2:	55                   	push   %ebp
  8004d3:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004d5:	83 fa 01             	cmp    $0x1,%edx
  8004d8:	7e 0e                	jle    8004e8 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004da:	8b 10                	mov    (%eax),%edx
  8004dc:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004df:	89 08                	mov    %ecx,(%eax)
  8004e1:	8b 02                	mov    (%edx),%eax
  8004e3:	8b 52 04             	mov    0x4(%edx),%edx
  8004e6:	eb 22                	jmp    80050a <getuint+0x38>
	else if (lflag)
  8004e8:	85 d2                	test   %edx,%edx
  8004ea:	74 10                	je     8004fc <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004ec:	8b 10                	mov    (%eax),%edx
  8004ee:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004f1:	89 08                	mov    %ecx,(%eax)
  8004f3:	8b 02                	mov    (%edx),%eax
  8004f5:	ba 00 00 00 00       	mov    $0x0,%edx
  8004fa:	eb 0e                	jmp    80050a <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004fc:	8b 10                	mov    (%eax),%edx
  8004fe:	8d 4a 04             	lea    0x4(%edx),%ecx
  800501:	89 08                	mov    %ecx,(%eax)
  800503:	8b 02                	mov    (%edx),%eax
  800505:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80050a:	5d                   	pop    %ebp
  80050b:	c3                   	ret    

0080050c <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80050c:	55                   	push   %ebp
  80050d:	89 e5                	mov    %esp,%ebp
  80050f:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800512:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800515:	8b 10                	mov    (%eax),%edx
  800517:	3b 50 04             	cmp    0x4(%eax),%edx
  80051a:	73 0a                	jae    800526 <sprintputch+0x1a>
		*b->buf++ = ch;
  80051c:	8d 4a 01             	lea    0x1(%edx),%ecx
  80051f:	89 08                	mov    %ecx,(%eax)
  800521:	8b 45 08             	mov    0x8(%ebp),%eax
  800524:	88 02                	mov    %al,(%edx)
}
  800526:	5d                   	pop    %ebp
  800527:	c3                   	ret    

00800528 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800528:	55                   	push   %ebp
  800529:	89 e5                	mov    %esp,%ebp
  80052b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80052e:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800531:	50                   	push   %eax
  800532:	ff 75 10             	pushl  0x10(%ebp)
  800535:	ff 75 0c             	pushl  0xc(%ebp)
  800538:	ff 75 08             	pushl  0x8(%ebp)
  80053b:	e8 05 00 00 00       	call   800545 <vprintfmt>
	va_end(ap);
}
  800540:	83 c4 10             	add    $0x10,%esp
  800543:	c9                   	leave  
  800544:	c3                   	ret    

00800545 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800545:	55                   	push   %ebp
  800546:	89 e5                	mov    %esp,%ebp
  800548:	57                   	push   %edi
  800549:	56                   	push   %esi
  80054a:	53                   	push   %ebx
  80054b:	83 ec 2c             	sub    $0x2c,%esp
  80054e:	8b 75 08             	mov    0x8(%ebp),%esi
  800551:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800554:	8b 7d 10             	mov    0x10(%ebp),%edi
  800557:	eb 12                	jmp    80056b <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800559:	85 c0                	test   %eax,%eax
  80055b:	0f 84 68 03 00 00    	je     8008c9 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800561:	83 ec 08             	sub    $0x8,%esp
  800564:	53                   	push   %ebx
  800565:	50                   	push   %eax
  800566:	ff d6                	call   *%esi
  800568:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80056b:	47                   	inc    %edi
  80056c:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800570:	83 f8 25             	cmp    $0x25,%eax
  800573:	75 e4                	jne    800559 <vprintfmt+0x14>
  800575:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800579:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800580:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800587:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80058e:	ba 00 00 00 00       	mov    $0x0,%edx
  800593:	eb 07                	jmp    80059c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800595:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800598:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059c:	8d 47 01             	lea    0x1(%edi),%eax
  80059f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005a2:	0f b6 0f             	movzbl (%edi),%ecx
  8005a5:	8a 07                	mov    (%edi),%al
  8005a7:	83 e8 23             	sub    $0x23,%eax
  8005aa:	3c 55                	cmp    $0x55,%al
  8005ac:	0f 87 fe 02 00 00    	ja     8008b0 <vprintfmt+0x36b>
  8005b2:	0f b6 c0             	movzbl %al,%eax
  8005b5:	ff 24 85 40 10 80 00 	jmp    *0x801040(,%eax,4)
  8005bc:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005bf:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005c3:	eb d7                	jmp    80059c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005c8:	b8 00 00 00 00       	mov    $0x0,%eax
  8005cd:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005d0:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005d3:	01 c0                	add    %eax,%eax
  8005d5:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005d9:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005dc:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005df:	83 fa 09             	cmp    $0x9,%edx
  8005e2:	77 34                	ja     800618 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005e4:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005e5:	eb e9                	jmp    8005d0 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005e7:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ea:	8d 48 04             	lea    0x4(%eax),%ecx
  8005ed:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005f0:	8b 00                	mov    (%eax),%eax
  8005f2:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005f8:	eb 24                	jmp    80061e <vprintfmt+0xd9>
  8005fa:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005fe:	79 07                	jns    800607 <vprintfmt+0xc2>
  800600:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800607:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80060a:	eb 90                	jmp    80059c <vprintfmt+0x57>
  80060c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80060f:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800616:	eb 84                	jmp    80059c <vprintfmt+0x57>
  800618:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80061b:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80061e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800622:	0f 89 74 ff ff ff    	jns    80059c <vprintfmt+0x57>
				width = precision, precision = -1;
  800628:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80062b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80062e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800635:	e9 62 ff ff ff       	jmp    80059c <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80063a:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80063b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80063e:	e9 59 ff ff ff       	jmp    80059c <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800643:	8b 45 14             	mov    0x14(%ebp),%eax
  800646:	8d 50 04             	lea    0x4(%eax),%edx
  800649:	89 55 14             	mov    %edx,0x14(%ebp)
  80064c:	83 ec 08             	sub    $0x8,%esp
  80064f:	53                   	push   %ebx
  800650:	ff 30                	pushl  (%eax)
  800652:	ff d6                	call   *%esi
			break;
  800654:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800657:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80065a:	e9 0c ff ff ff       	jmp    80056b <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80065f:	8b 45 14             	mov    0x14(%ebp),%eax
  800662:	8d 50 04             	lea    0x4(%eax),%edx
  800665:	89 55 14             	mov    %edx,0x14(%ebp)
  800668:	8b 00                	mov    (%eax),%eax
  80066a:	85 c0                	test   %eax,%eax
  80066c:	79 02                	jns    800670 <vprintfmt+0x12b>
  80066e:	f7 d8                	neg    %eax
  800670:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800672:	83 f8 08             	cmp    $0x8,%eax
  800675:	7f 0b                	jg     800682 <vprintfmt+0x13d>
  800677:	8b 04 85 a0 11 80 00 	mov    0x8011a0(,%eax,4),%eax
  80067e:	85 c0                	test   %eax,%eax
  800680:	75 18                	jne    80069a <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800682:	52                   	push   %edx
  800683:	68 a0 0f 80 00       	push   $0x800fa0
  800688:	53                   	push   %ebx
  800689:	56                   	push   %esi
  80068a:	e8 99 fe ff ff       	call   800528 <printfmt>
  80068f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800692:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800695:	e9 d1 fe ff ff       	jmp    80056b <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80069a:	50                   	push   %eax
  80069b:	68 a9 0f 80 00       	push   $0x800fa9
  8006a0:	53                   	push   %ebx
  8006a1:	56                   	push   %esi
  8006a2:	e8 81 fe ff ff       	call   800528 <printfmt>
  8006a7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006aa:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006ad:	e9 b9 fe ff ff       	jmp    80056b <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006b2:	8b 45 14             	mov    0x14(%ebp),%eax
  8006b5:	8d 50 04             	lea    0x4(%eax),%edx
  8006b8:	89 55 14             	mov    %edx,0x14(%ebp)
  8006bb:	8b 38                	mov    (%eax),%edi
  8006bd:	85 ff                	test   %edi,%edi
  8006bf:	75 05                	jne    8006c6 <vprintfmt+0x181>
				p = "(null)";
  8006c1:	bf 99 0f 80 00       	mov    $0x800f99,%edi
			if (width > 0 && padc != '-')
  8006c6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006ca:	0f 8e 90 00 00 00    	jle    800760 <vprintfmt+0x21b>
  8006d0:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006d4:	0f 84 8e 00 00 00    	je     800768 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006da:	83 ec 08             	sub    $0x8,%esp
  8006dd:	ff 75 d0             	pushl  -0x30(%ebp)
  8006e0:	57                   	push   %edi
  8006e1:	e8 70 02 00 00       	call   800956 <strnlen>
  8006e6:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006e9:	29 c1                	sub    %eax,%ecx
  8006eb:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006ee:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006f1:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006f5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006f8:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006fb:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006fd:	eb 0d                	jmp    80070c <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006ff:	83 ec 08             	sub    $0x8,%esp
  800702:	53                   	push   %ebx
  800703:	ff 75 e4             	pushl  -0x1c(%ebp)
  800706:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800708:	4f                   	dec    %edi
  800709:	83 c4 10             	add    $0x10,%esp
  80070c:	85 ff                	test   %edi,%edi
  80070e:	7f ef                	jg     8006ff <vprintfmt+0x1ba>
  800710:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800713:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800716:	89 c8                	mov    %ecx,%eax
  800718:	85 c9                	test   %ecx,%ecx
  80071a:	79 05                	jns    800721 <vprintfmt+0x1dc>
  80071c:	b8 00 00 00 00       	mov    $0x0,%eax
  800721:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800724:	29 c1                	sub    %eax,%ecx
  800726:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800729:	89 75 08             	mov    %esi,0x8(%ebp)
  80072c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80072f:	eb 3d                	jmp    80076e <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800731:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800735:	74 19                	je     800750 <vprintfmt+0x20b>
  800737:	0f be c0             	movsbl %al,%eax
  80073a:	83 e8 20             	sub    $0x20,%eax
  80073d:	83 f8 5e             	cmp    $0x5e,%eax
  800740:	76 0e                	jbe    800750 <vprintfmt+0x20b>
					putch('?', putdat);
  800742:	83 ec 08             	sub    $0x8,%esp
  800745:	53                   	push   %ebx
  800746:	6a 3f                	push   $0x3f
  800748:	ff 55 08             	call   *0x8(%ebp)
  80074b:	83 c4 10             	add    $0x10,%esp
  80074e:	eb 0b                	jmp    80075b <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800750:	83 ec 08             	sub    $0x8,%esp
  800753:	53                   	push   %ebx
  800754:	52                   	push   %edx
  800755:	ff 55 08             	call   *0x8(%ebp)
  800758:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80075b:	ff 4d e4             	decl   -0x1c(%ebp)
  80075e:	eb 0e                	jmp    80076e <vprintfmt+0x229>
  800760:	89 75 08             	mov    %esi,0x8(%ebp)
  800763:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800766:	eb 06                	jmp    80076e <vprintfmt+0x229>
  800768:	89 75 08             	mov    %esi,0x8(%ebp)
  80076b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80076e:	47                   	inc    %edi
  80076f:	8a 47 ff             	mov    -0x1(%edi),%al
  800772:	0f be d0             	movsbl %al,%edx
  800775:	85 d2                	test   %edx,%edx
  800777:	74 1d                	je     800796 <vprintfmt+0x251>
  800779:	85 f6                	test   %esi,%esi
  80077b:	78 b4                	js     800731 <vprintfmt+0x1ec>
  80077d:	4e                   	dec    %esi
  80077e:	79 b1                	jns    800731 <vprintfmt+0x1ec>
  800780:	8b 75 08             	mov    0x8(%ebp),%esi
  800783:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800786:	eb 14                	jmp    80079c <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800788:	83 ec 08             	sub    $0x8,%esp
  80078b:	53                   	push   %ebx
  80078c:	6a 20                	push   $0x20
  80078e:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800790:	4f                   	dec    %edi
  800791:	83 c4 10             	add    $0x10,%esp
  800794:	eb 06                	jmp    80079c <vprintfmt+0x257>
  800796:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800799:	8b 75 08             	mov    0x8(%ebp),%esi
  80079c:	85 ff                	test   %edi,%edi
  80079e:	7f e8                	jg     800788 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007a0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007a3:	e9 c3 fd ff ff       	jmp    80056b <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007a8:	83 fa 01             	cmp    $0x1,%edx
  8007ab:	7e 16                	jle    8007c3 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007ad:	8b 45 14             	mov    0x14(%ebp),%eax
  8007b0:	8d 50 08             	lea    0x8(%eax),%edx
  8007b3:	89 55 14             	mov    %edx,0x14(%ebp)
  8007b6:	8b 50 04             	mov    0x4(%eax),%edx
  8007b9:	8b 00                	mov    (%eax),%eax
  8007bb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007be:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007c1:	eb 32                	jmp    8007f5 <vprintfmt+0x2b0>
	else if (lflag)
  8007c3:	85 d2                	test   %edx,%edx
  8007c5:	74 18                	je     8007df <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007c7:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ca:	8d 50 04             	lea    0x4(%eax),%edx
  8007cd:	89 55 14             	mov    %edx,0x14(%ebp)
  8007d0:	8b 00                	mov    (%eax),%eax
  8007d2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007d5:	89 c1                	mov    %eax,%ecx
  8007d7:	c1 f9 1f             	sar    $0x1f,%ecx
  8007da:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007dd:	eb 16                	jmp    8007f5 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007df:	8b 45 14             	mov    0x14(%ebp),%eax
  8007e2:	8d 50 04             	lea    0x4(%eax),%edx
  8007e5:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e8:	8b 00                	mov    (%eax),%eax
  8007ea:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007ed:	89 c1                	mov    %eax,%ecx
  8007ef:	c1 f9 1f             	sar    $0x1f,%ecx
  8007f2:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007f5:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007f8:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007fb:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007ff:	79 76                	jns    800877 <vprintfmt+0x332>
				putch('-', putdat);
  800801:	83 ec 08             	sub    $0x8,%esp
  800804:	53                   	push   %ebx
  800805:	6a 2d                	push   $0x2d
  800807:	ff d6                	call   *%esi
				num = -(long long) num;
  800809:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80080c:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80080f:	f7 d8                	neg    %eax
  800811:	83 d2 00             	adc    $0x0,%edx
  800814:	f7 da                	neg    %edx
  800816:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800819:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80081e:	eb 5c                	jmp    80087c <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800820:	8d 45 14             	lea    0x14(%ebp),%eax
  800823:	e8 aa fc ff ff       	call   8004d2 <getuint>
			base = 10;
  800828:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80082d:	eb 4d                	jmp    80087c <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80082f:	8d 45 14             	lea    0x14(%ebp),%eax
  800832:	e8 9b fc ff ff       	call   8004d2 <getuint>
			base = 8;
  800837:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80083c:	eb 3e                	jmp    80087c <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80083e:	83 ec 08             	sub    $0x8,%esp
  800841:	53                   	push   %ebx
  800842:	6a 30                	push   $0x30
  800844:	ff d6                	call   *%esi
			putch('x', putdat);
  800846:	83 c4 08             	add    $0x8,%esp
  800849:	53                   	push   %ebx
  80084a:	6a 78                	push   $0x78
  80084c:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80084e:	8b 45 14             	mov    0x14(%ebp),%eax
  800851:	8d 50 04             	lea    0x4(%eax),%edx
  800854:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800857:	8b 00                	mov    (%eax),%eax
  800859:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80085e:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800861:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800866:	eb 14                	jmp    80087c <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800868:	8d 45 14             	lea    0x14(%ebp),%eax
  80086b:	e8 62 fc ff ff       	call   8004d2 <getuint>
			base = 16;
  800870:	b9 10 00 00 00       	mov    $0x10,%ecx
  800875:	eb 05                	jmp    80087c <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800877:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80087c:	83 ec 0c             	sub    $0xc,%esp
  80087f:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800883:	57                   	push   %edi
  800884:	ff 75 e4             	pushl  -0x1c(%ebp)
  800887:	51                   	push   %ecx
  800888:	52                   	push   %edx
  800889:	50                   	push   %eax
  80088a:	89 da                	mov    %ebx,%edx
  80088c:	89 f0                	mov    %esi,%eax
  80088e:	e8 92 fb ff ff       	call   800425 <printnum>
			break;
  800893:	83 c4 20             	add    $0x20,%esp
  800896:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800899:	e9 cd fc ff ff       	jmp    80056b <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80089e:	83 ec 08             	sub    $0x8,%esp
  8008a1:	53                   	push   %ebx
  8008a2:	51                   	push   %ecx
  8008a3:	ff d6                	call   *%esi
			break;
  8008a5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008a8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008ab:	e9 bb fc ff ff       	jmp    80056b <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008b0:	83 ec 08             	sub    $0x8,%esp
  8008b3:	53                   	push   %ebx
  8008b4:	6a 25                	push   $0x25
  8008b6:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008b8:	83 c4 10             	add    $0x10,%esp
  8008bb:	eb 01                	jmp    8008be <vprintfmt+0x379>
  8008bd:	4f                   	dec    %edi
  8008be:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008c2:	75 f9                	jne    8008bd <vprintfmt+0x378>
  8008c4:	e9 a2 fc ff ff       	jmp    80056b <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008cc:	5b                   	pop    %ebx
  8008cd:	5e                   	pop    %esi
  8008ce:	5f                   	pop    %edi
  8008cf:	5d                   	pop    %ebp
  8008d0:	c3                   	ret    

008008d1 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008d1:	55                   	push   %ebp
  8008d2:	89 e5                	mov    %esp,%ebp
  8008d4:	83 ec 18             	sub    $0x18,%esp
  8008d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8008da:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008dd:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008e0:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008e4:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008e7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008ee:	85 c0                	test   %eax,%eax
  8008f0:	74 26                	je     800918 <vsnprintf+0x47>
  8008f2:	85 d2                	test   %edx,%edx
  8008f4:	7e 29                	jle    80091f <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008f6:	ff 75 14             	pushl  0x14(%ebp)
  8008f9:	ff 75 10             	pushl  0x10(%ebp)
  8008fc:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008ff:	50                   	push   %eax
  800900:	68 0c 05 80 00       	push   $0x80050c
  800905:	e8 3b fc ff ff       	call   800545 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80090a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80090d:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800910:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800913:	83 c4 10             	add    $0x10,%esp
  800916:	eb 0c                	jmp    800924 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800918:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80091d:	eb 05                	jmp    800924 <vsnprintf+0x53>
  80091f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800924:	c9                   	leave  
  800925:	c3                   	ret    

00800926 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800926:	55                   	push   %ebp
  800927:	89 e5                	mov    %esp,%ebp
  800929:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80092c:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80092f:	50                   	push   %eax
  800930:	ff 75 10             	pushl  0x10(%ebp)
  800933:	ff 75 0c             	pushl  0xc(%ebp)
  800936:	ff 75 08             	pushl  0x8(%ebp)
  800939:	e8 93 ff ff ff       	call   8008d1 <vsnprintf>
	va_end(ap);

	return rc;
}
  80093e:	c9                   	leave  
  80093f:	c3                   	ret    

00800940 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800940:	55                   	push   %ebp
  800941:	89 e5                	mov    %esp,%ebp
  800943:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800946:	b8 00 00 00 00       	mov    $0x0,%eax
  80094b:	eb 01                	jmp    80094e <strlen+0xe>
		n++;
  80094d:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80094e:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800952:	75 f9                	jne    80094d <strlen+0xd>
		n++;
	return n;
}
  800954:	5d                   	pop    %ebp
  800955:	c3                   	ret    

00800956 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800956:	55                   	push   %ebp
  800957:	89 e5                	mov    %esp,%ebp
  800959:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80095c:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80095f:	ba 00 00 00 00       	mov    $0x0,%edx
  800964:	eb 01                	jmp    800967 <strnlen+0x11>
		n++;
  800966:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800967:	39 c2                	cmp    %eax,%edx
  800969:	74 08                	je     800973 <strnlen+0x1d>
  80096b:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80096f:	75 f5                	jne    800966 <strnlen+0x10>
  800971:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800973:	5d                   	pop    %ebp
  800974:	c3                   	ret    

00800975 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800975:	55                   	push   %ebp
  800976:	89 e5                	mov    %esp,%ebp
  800978:	53                   	push   %ebx
  800979:	8b 45 08             	mov    0x8(%ebp),%eax
  80097c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80097f:	89 c2                	mov    %eax,%edx
  800981:	42                   	inc    %edx
  800982:	41                   	inc    %ecx
  800983:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800986:	88 5a ff             	mov    %bl,-0x1(%edx)
  800989:	84 db                	test   %bl,%bl
  80098b:	75 f4                	jne    800981 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80098d:	5b                   	pop    %ebx
  80098e:	5d                   	pop    %ebp
  80098f:	c3                   	ret    

00800990 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800990:	55                   	push   %ebp
  800991:	89 e5                	mov    %esp,%ebp
  800993:	53                   	push   %ebx
  800994:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800997:	53                   	push   %ebx
  800998:	e8 a3 ff ff ff       	call   800940 <strlen>
  80099d:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009a0:	ff 75 0c             	pushl  0xc(%ebp)
  8009a3:	01 d8                	add    %ebx,%eax
  8009a5:	50                   	push   %eax
  8009a6:	e8 ca ff ff ff       	call   800975 <strcpy>
	return dst;
}
  8009ab:	89 d8                	mov    %ebx,%eax
  8009ad:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009b0:	c9                   	leave  
  8009b1:	c3                   	ret    

008009b2 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009b2:	55                   	push   %ebp
  8009b3:	89 e5                	mov    %esp,%ebp
  8009b5:	56                   	push   %esi
  8009b6:	53                   	push   %ebx
  8009b7:	8b 75 08             	mov    0x8(%ebp),%esi
  8009ba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009bd:	89 f3                	mov    %esi,%ebx
  8009bf:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009c2:	89 f2                	mov    %esi,%edx
  8009c4:	eb 0c                	jmp    8009d2 <strncpy+0x20>
		*dst++ = *src;
  8009c6:	42                   	inc    %edx
  8009c7:	8a 01                	mov    (%ecx),%al
  8009c9:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009cc:	80 39 01             	cmpb   $0x1,(%ecx)
  8009cf:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009d2:	39 da                	cmp    %ebx,%edx
  8009d4:	75 f0                	jne    8009c6 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009d6:	89 f0                	mov    %esi,%eax
  8009d8:	5b                   	pop    %ebx
  8009d9:	5e                   	pop    %esi
  8009da:	5d                   	pop    %ebp
  8009db:	c3                   	ret    

008009dc <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009dc:	55                   	push   %ebp
  8009dd:	89 e5                	mov    %esp,%ebp
  8009df:	56                   	push   %esi
  8009e0:	53                   	push   %ebx
  8009e1:	8b 75 08             	mov    0x8(%ebp),%esi
  8009e4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009e7:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009ea:	85 c0                	test   %eax,%eax
  8009ec:	74 1e                	je     800a0c <strlcpy+0x30>
  8009ee:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009f2:	89 f2                	mov    %esi,%edx
  8009f4:	eb 05                	jmp    8009fb <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009f6:	42                   	inc    %edx
  8009f7:	41                   	inc    %ecx
  8009f8:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009fb:	39 c2                	cmp    %eax,%edx
  8009fd:	74 08                	je     800a07 <strlcpy+0x2b>
  8009ff:	8a 19                	mov    (%ecx),%bl
  800a01:	84 db                	test   %bl,%bl
  800a03:	75 f1                	jne    8009f6 <strlcpy+0x1a>
  800a05:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a07:	c6 00 00             	movb   $0x0,(%eax)
  800a0a:	eb 02                	jmp    800a0e <strlcpy+0x32>
  800a0c:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a0e:	29 f0                	sub    %esi,%eax
}
  800a10:	5b                   	pop    %ebx
  800a11:	5e                   	pop    %esi
  800a12:	5d                   	pop    %ebp
  800a13:	c3                   	ret    

00800a14 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a14:	55                   	push   %ebp
  800a15:	89 e5                	mov    %esp,%ebp
  800a17:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a1a:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a1d:	eb 02                	jmp    800a21 <strcmp+0xd>
		p++, q++;
  800a1f:	41                   	inc    %ecx
  800a20:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a21:	8a 01                	mov    (%ecx),%al
  800a23:	84 c0                	test   %al,%al
  800a25:	74 04                	je     800a2b <strcmp+0x17>
  800a27:	3a 02                	cmp    (%edx),%al
  800a29:	74 f4                	je     800a1f <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a2b:	0f b6 c0             	movzbl %al,%eax
  800a2e:	0f b6 12             	movzbl (%edx),%edx
  800a31:	29 d0                	sub    %edx,%eax
}
  800a33:	5d                   	pop    %ebp
  800a34:	c3                   	ret    

00800a35 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a35:	55                   	push   %ebp
  800a36:	89 e5                	mov    %esp,%ebp
  800a38:	53                   	push   %ebx
  800a39:	8b 45 08             	mov    0x8(%ebp),%eax
  800a3c:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a3f:	89 c3                	mov    %eax,%ebx
  800a41:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a44:	eb 02                	jmp    800a48 <strncmp+0x13>
		n--, p++, q++;
  800a46:	40                   	inc    %eax
  800a47:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a48:	39 d8                	cmp    %ebx,%eax
  800a4a:	74 14                	je     800a60 <strncmp+0x2b>
  800a4c:	8a 08                	mov    (%eax),%cl
  800a4e:	84 c9                	test   %cl,%cl
  800a50:	74 04                	je     800a56 <strncmp+0x21>
  800a52:	3a 0a                	cmp    (%edx),%cl
  800a54:	74 f0                	je     800a46 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a56:	0f b6 00             	movzbl (%eax),%eax
  800a59:	0f b6 12             	movzbl (%edx),%edx
  800a5c:	29 d0                	sub    %edx,%eax
  800a5e:	eb 05                	jmp    800a65 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a60:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a65:	5b                   	pop    %ebx
  800a66:	5d                   	pop    %ebp
  800a67:	c3                   	ret    

00800a68 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a68:	55                   	push   %ebp
  800a69:	89 e5                	mov    %esp,%ebp
  800a6b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a6e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a71:	eb 05                	jmp    800a78 <strchr+0x10>
		if (*s == c)
  800a73:	38 ca                	cmp    %cl,%dl
  800a75:	74 0c                	je     800a83 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a77:	40                   	inc    %eax
  800a78:	8a 10                	mov    (%eax),%dl
  800a7a:	84 d2                	test   %dl,%dl
  800a7c:	75 f5                	jne    800a73 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a7e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a83:	5d                   	pop    %ebp
  800a84:	c3                   	ret    

00800a85 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a85:	55                   	push   %ebp
  800a86:	89 e5                	mov    %esp,%ebp
  800a88:	8b 45 08             	mov    0x8(%ebp),%eax
  800a8b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a8e:	eb 05                	jmp    800a95 <strfind+0x10>
		if (*s == c)
  800a90:	38 ca                	cmp    %cl,%dl
  800a92:	74 07                	je     800a9b <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a94:	40                   	inc    %eax
  800a95:	8a 10                	mov    (%eax),%dl
  800a97:	84 d2                	test   %dl,%dl
  800a99:	75 f5                	jne    800a90 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a9b:	5d                   	pop    %ebp
  800a9c:	c3                   	ret    

00800a9d <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a9d:	55                   	push   %ebp
  800a9e:	89 e5                	mov    %esp,%ebp
  800aa0:	57                   	push   %edi
  800aa1:	56                   	push   %esi
  800aa2:	53                   	push   %ebx
  800aa3:	8b 7d 08             	mov    0x8(%ebp),%edi
  800aa6:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800aa9:	85 c9                	test   %ecx,%ecx
  800aab:	74 36                	je     800ae3 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800aad:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800ab3:	75 28                	jne    800add <memset+0x40>
  800ab5:	f6 c1 03             	test   $0x3,%cl
  800ab8:	75 23                	jne    800add <memset+0x40>
		c &= 0xFF;
  800aba:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800abe:	89 d3                	mov    %edx,%ebx
  800ac0:	c1 e3 08             	shl    $0x8,%ebx
  800ac3:	89 d6                	mov    %edx,%esi
  800ac5:	c1 e6 18             	shl    $0x18,%esi
  800ac8:	89 d0                	mov    %edx,%eax
  800aca:	c1 e0 10             	shl    $0x10,%eax
  800acd:	09 f0                	or     %esi,%eax
  800acf:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800ad1:	89 d8                	mov    %ebx,%eax
  800ad3:	09 d0                	or     %edx,%eax
  800ad5:	c1 e9 02             	shr    $0x2,%ecx
  800ad8:	fc                   	cld    
  800ad9:	f3 ab                	rep stos %eax,%es:(%edi)
  800adb:	eb 06                	jmp    800ae3 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800add:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ae0:	fc                   	cld    
  800ae1:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800ae3:	89 f8                	mov    %edi,%eax
  800ae5:	5b                   	pop    %ebx
  800ae6:	5e                   	pop    %esi
  800ae7:	5f                   	pop    %edi
  800ae8:	5d                   	pop    %ebp
  800ae9:	c3                   	ret    

00800aea <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800aea:	55                   	push   %ebp
  800aeb:	89 e5                	mov    %esp,%ebp
  800aed:	57                   	push   %edi
  800aee:	56                   	push   %esi
  800aef:	8b 45 08             	mov    0x8(%ebp),%eax
  800af2:	8b 75 0c             	mov    0xc(%ebp),%esi
  800af5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800af8:	39 c6                	cmp    %eax,%esi
  800afa:	73 33                	jae    800b2f <memmove+0x45>
  800afc:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800aff:	39 d0                	cmp    %edx,%eax
  800b01:	73 2c                	jae    800b2f <memmove+0x45>
		s += n;
		d += n;
  800b03:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b06:	89 d6                	mov    %edx,%esi
  800b08:	09 fe                	or     %edi,%esi
  800b0a:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b10:	75 13                	jne    800b25 <memmove+0x3b>
  800b12:	f6 c1 03             	test   $0x3,%cl
  800b15:	75 0e                	jne    800b25 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b17:	83 ef 04             	sub    $0x4,%edi
  800b1a:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b1d:	c1 e9 02             	shr    $0x2,%ecx
  800b20:	fd                   	std    
  800b21:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b23:	eb 07                	jmp    800b2c <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b25:	4f                   	dec    %edi
  800b26:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b29:	fd                   	std    
  800b2a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b2c:	fc                   	cld    
  800b2d:	eb 1d                	jmp    800b4c <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b2f:	89 f2                	mov    %esi,%edx
  800b31:	09 c2                	or     %eax,%edx
  800b33:	f6 c2 03             	test   $0x3,%dl
  800b36:	75 0f                	jne    800b47 <memmove+0x5d>
  800b38:	f6 c1 03             	test   $0x3,%cl
  800b3b:	75 0a                	jne    800b47 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b3d:	c1 e9 02             	shr    $0x2,%ecx
  800b40:	89 c7                	mov    %eax,%edi
  800b42:	fc                   	cld    
  800b43:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b45:	eb 05                	jmp    800b4c <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b47:	89 c7                	mov    %eax,%edi
  800b49:	fc                   	cld    
  800b4a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b4c:	5e                   	pop    %esi
  800b4d:	5f                   	pop    %edi
  800b4e:	5d                   	pop    %ebp
  800b4f:	c3                   	ret    

00800b50 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b50:	55                   	push   %ebp
  800b51:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b53:	ff 75 10             	pushl  0x10(%ebp)
  800b56:	ff 75 0c             	pushl  0xc(%ebp)
  800b59:	ff 75 08             	pushl  0x8(%ebp)
  800b5c:	e8 89 ff ff ff       	call   800aea <memmove>
}
  800b61:	c9                   	leave  
  800b62:	c3                   	ret    

00800b63 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b63:	55                   	push   %ebp
  800b64:	89 e5                	mov    %esp,%ebp
  800b66:	56                   	push   %esi
  800b67:	53                   	push   %ebx
  800b68:	8b 45 08             	mov    0x8(%ebp),%eax
  800b6b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b6e:	89 c6                	mov    %eax,%esi
  800b70:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b73:	eb 14                	jmp    800b89 <memcmp+0x26>
		if (*s1 != *s2)
  800b75:	8a 08                	mov    (%eax),%cl
  800b77:	8a 1a                	mov    (%edx),%bl
  800b79:	38 d9                	cmp    %bl,%cl
  800b7b:	74 0a                	je     800b87 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b7d:	0f b6 c1             	movzbl %cl,%eax
  800b80:	0f b6 db             	movzbl %bl,%ebx
  800b83:	29 d8                	sub    %ebx,%eax
  800b85:	eb 0b                	jmp    800b92 <memcmp+0x2f>
		s1++, s2++;
  800b87:	40                   	inc    %eax
  800b88:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b89:	39 f0                	cmp    %esi,%eax
  800b8b:	75 e8                	jne    800b75 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b8d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b92:	5b                   	pop    %ebx
  800b93:	5e                   	pop    %esi
  800b94:	5d                   	pop    %ebp
  800b95:	c3                   	ret    

00800b96 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b96:	55                   	push   %ebp
  800b97:	89 e5                	mov    %esp,%ebp
  800b99:	53                   	push   %ebx
  800b9a:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b9d:	89 c1                	mov    %eax,%ecx
  800b9f:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800ba2:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ba6:	eb 08                	jmp    800bb0 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800ba8:	0f b6 10             	movzbl (%eax),%edx
  800bab:	39 da                	cmp    %ebx,%edx
  800bad:	74 05                	je     800bb4 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800baf:	40                   	inc    %eax
  800bb0:	39 c8                	cmp    %ecx,%eax
  800bb2:	72 f4                	jb     800ba8 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bb4:	5b                   	pop    %ebx
  800bb5:	5d                   	pop    %ebp
  800bb6:	c3                   	ret    

00800bb7 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bb7:	55                   	push   %ebp
  800bb8:	89 e5                	mov    %esp,%ebp
  800bba:	57                   	push   %edi
  800bbb:	56                   	push   %esi
  800bbc:	53                   	push   %ebx
  800bbd:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bc0:	eb 01                	jmp    800bc3 <strtol+0xc>
		s++;
  800bc2:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bc3:	8a 01                	mov    (%ecx),%al
  800bc5:	3c 20                	cmp    $0x20,%al
  800bc7:	74 f9                	je     800bc2 <strtol+0xb>
  800bc9:	3c 09                	cmp    $0x9,%al
  800bcb:	74 f5                	je     800bc2 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bcd:	3c 2b                	cmp    $0x2b,%al
  800bcf:	75 08                	jne    800bd9 <strtol+0x22>
		s++;
  800bd1:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bd2:	bf 00 00 00 00       	mov    $0x0,%edi
  800bd7:	eb 11                	jmp    800bea <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bd9:	3c 2d                	cmp    $0x2d,%al
  800bdb:	75 08                	jne    800be5 <strtol+0x2e>
		s++, neg = 1;
  800bdd:	41                   	inc    %ecx
  800bde:	bf 01 00 00 00       	mov    $0x1,%edi
  800be3:	eb 05                	jmp    800bea <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800be5:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bea:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bee:	0f 84 87 00 00 00    	je     800c7b <strtol+0xc4>
  800bf4:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bf8:	75 27                	jne    800c21 <strtol+0x6a>
  800bfa:	80 39 30             	cmpb   $0x30,(%ecx)
  800bfd:	75 22                	jne    800c21 <strtol+0x6a>
  800bff:	e9 88 00 00 00       	jmp    800c8c <strtol+0xd5>
		s += 2, base = 16;
  800c04:	83 c1 02             	add    $0x2,%ecx
  800c07:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c0e:	eb 11                	jmp    800c21 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c10:	41                   	inc    %ecx
  800c11:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c18:	eb 07                	jmp    800c21 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c1a:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c21:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c26:	8a 11                	mov    (%ecx),%dl
  800c28:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c2b:	80 fb 09             	cmp    $0x9,%bl
  800c2e:	77 08                	ja     800c38 <strtol+0x81>
			dig = *s - '0';
  800c30:	0f be d2             	movsbl %dl,%edx
  800c33:	83 ea 30             	sub    $0x30,%edx
  800c36:	eb 22                	jmp    800c5a <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c38:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c3b:	89 f3                	mov    %esi,%ebx
  800c3d:	80 fb 19             	cmp    $0x19,%bl
  800c40:	77 08                	ja     800c4a <strtol+0x93>
			dig = *s - 'a' + 10;
  800c42:	0f be d2             	movsbl %dl,%edx
  800c45:	83 ea 57             	sub    $0x57,%edx
  800c48:	eb 10                	jmp    800c5a <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c4a:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c4d:	89 f3                	mov    %esi,%ebx
  800c4f:	80 fb 19             	cmp    $0x19,%bl
  800c52:	77 14                	ja     800c68 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c54:	0f be d2             	movsbl %dl,%edx
  800c57:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c5a:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c5d:	7d 09                	jge    800c68 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c5f:	41                   	inc    %ecx
  800c60:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c64:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c66:	eb be                	jmp    800c26 <strtol+0x6f>

	if (endptr)
  800c68:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c6c:	74 05                	je     800c73 <strtol+0xbc>
		*endptr = (char *) s;
  800c6e:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c71:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c73:	85 ff                	test   %edi,%edi
  800c75:	74 21                	je     800c98 <strtol+0xe1>
  800c77:	f7 d8                	neg    %eax
  800c79:	eb 1d                	jmp    800c98 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c7b:	80 39 30             	cmpb   $0x30,(%ecx)
  800c7e:	75 9a                	jne    800c1a <strtol+0x63>
  800c80:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c84:	0f 84 7a ff ff ff    	je     800c04 <strtol+0x4d>
  800c8a:	eb 84                	jmp    800c10 <strtol+0x59>
  800c8c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c90:	0f 84 6e ff ff ff    	je     800c04 <strtol+0x4d>
  800c96:	eb 89                	jmp    800c21 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c98:	5b                   	pop    %ebx
  800c99:	5e                   	pop    %esi
  800c9a:	5f                   	pop    %edi
  800c9b:	5d                   	pop    %ebp
  800c9c:	c3                   	ret    
  800c9d:	66 90                	xchg   %ax,%ax
  800c9f:	90                   	nop

00800ca0 <__udivdi3>:
  800ca0:	55                   	push   %ebp
  800ca1:	57                   	push   %edi
  800ca2:	56                   	push   %esi
  800ca3:	53                   	push   %ebx
  800ca4:	83 ec 1c             	sub    $0x1c,%esp
  800ca7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cab:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800caf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cb3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cb7:	89 ca                	mov    %ecx,%edx
  800cb9:	89 f8                	mov    %edi,%eax
  800cbb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cbf:	85 f6                	test   %esi,%esi
  800cc1:	75 2d                	jne    800cf0 <__udivdi3+0x50>
  800cc3:	39 cf                	cmp    %ecx,%edi
  800cc5:	77 65                	ja     800d2c <__udivdi3+0x8c>
  800cc7:	89 fd                	mov    %edi,%ebp
  800cc9:	85 ff                	test   %edi,%edi
  800ccb:	75 0b                	jne    800cd8 <__udivdi3+0x38>
  800ccd:	b8 01 00 00 00       	mov    $0x1,%eax
  800cd2:	31 d2                	xor    %edx,%edx
  800cd4:	f7 f7                	div    %edi
  800cd6:	89 c5                	mov    %eax,%ebp
  800cd8:	31 d2                	xor    %edx,%edx
  800cda:	89 c8                	mov    %ecx,%eax
  800cdc:	f7 f5                	div    %ebp
  800cde:	89 c1                	mov    %eax,%ecx
  800ce0:	89 d8                	mov    %ebx,%eax
  800ce2:	f7 f5                	div    %ebp
  800ce4:	89 cf                	mov    %ecx,%edi
  800ce6:	89 fa                	mov    %edi,%edx
  800ce8:	83 c4 1c             	add    $0x1c,%esp
  800ceb:	5b                   	pop    %ebx
  800cec:	5e                   	pop    %esi
  800ced:	5f                   	pop    %edi
  800cee:	5d                   	pop    %ebp
  800cef:	c3                   	ret    
  800cf0:	39 ce                	cmp    %ecx,%esi
  800cf2:	77 28                	ja     800d1c <__udivdi3+0x7c>
  800cf4:	0f bd fe             	bsr    %esi,%edi
  800cf7:	83 f7 1f             	xor    $0x1f,%edi
  800cfa:	75 40                	jne    800d3c <__udivdi3+0x9c>
  800cfc:	39 ce                	cmp    %ecx,%esi
  800cfe:	72 0a                	jb     800d0a <__udivdi3+0x6a>
  800d00:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d04:	0f 87 9e 00 00 00    	ja     800da8 <__udivdi3+0x108>
  800d0a:	b8 01 00 00 00       	mov    $0x1,%eax
  800d0f:	89 fa                	mov    %edi,%edx
  800d11:	83 c4 1c             	add    $0x1c,%esp
  800d14:	5b                   	pop    %ebx
  800d15:	5e                   	pop    %esi
  800d16:	5f                   	pop    %edi
  800d17:	5d                   	pop    %ebp
  800d18:	c3                   	ret    
  800d19:	8d 76 00             	lea    0x0(%esi),%esi
  800d1c:	31 ff                	xor    %edi,%edi
  800d1e:	31 c0                	xor    %eax,%eax
  800d20:	89 fa                	mov    %edi,%edx
  800d22:	83 c4 1c             	add    $0x1c,%esp
  800d25:	5b                   	pop    %ebx
  800d26:	5e                   	pop    %esi
  800d27:	5f                   	pop    %edi
  800d28:	5d                   	pop    %ebp
  800d29:	c3                   	ret    
  800d2a:	66 90                	xchg   %ax,%ax
  800d2c:	89 d8                	mov    %ebx,%eax
  800d2e:	f7 f7                	div    %edi
  800d30:	31 ff                	xor    %edi,%edi
  800d32:	89 fa                	mov    %edi,%edx
  800d34:	83 c4 1c             	add    $0x1c,%esp
  800d37:	5b                   	pop    %ebx
  800d38:	5e                   	pop    %esi
  800d39:	5f                   	pop    %edi
  800d3a:	5d                   	pop    %ebp
  800d3b:	c3                   	ret    
  800d3c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d41:	89 eb                	mov    %ebp,%ebx
  800d43:	29 fb                	sub    %edi,%ebx
  800d45:	89 f9                	mov    %edi,%ecx
  800d47:	d3 e6                	shl    %cl,%esi
  800d49:	89 c5                	mov    %eax,%ebp
  800d4b:	88 d9                	mov    %bl,%cl
  800d4d:	d3 ed                	shr    %cl,%ebp
  800d4f:	89 e9                	mov    %ebp,%ecx
  800d51:	09 f1                	or     %esi,%ecx
  800d53:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d57:	89 f9                	mov    %edi,%ecx
  800d59:	d3 e0                	shl    %cl,%eax
  800d5b:	89 c5                	mov    %eax,%ebp
  800d5d:	89 d6                	mov    %edx,%esi
  800d5f:	88 d9                	mov    %bl,%cl
  800d61:	d3 ee                	shr    %cl,%esi
  800d63:	89 f9                	mov    %edi,%ecx
  800d65:	d3 e2                	shl    %cl,%edx
  800d67:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d6b:	88 d9                	mov    %bl,%cl
  800d6d:	d3 e8                	shr    %cl,%eax
  800d6f:	09 c2                	or     %eax,%edx
  800d71:	89 d0                	mov    %edx,%eax
  800d73:	89 f2                	mov    %esi,%edx
  800d75:	f7 74 24 0c          	divl   0xc(%esp)
  800d79:	89 d6                	mov    %edx,%esi
  800d7b:	89 c3                	mov    %eax,%ebx
  800d7d:	f7 e5                	mul    %ebp
  800d7f:	39 d6                	cmp    %edx,%esi
  800d81:	72 19                	jb     800d9c <__udivdi3+0xfc>
  800d83:	74 0b                	je     800d90 <__udivdi3+0xf0>
  800d85:	89 d8                	mov    %ebx,%eax
  800d87:	31 ff                	xor    %edi,%edi
  800d89:	e9 58 ff ff ff       	jmp    800ce6 <__udivdi3+0x46>
  800d8e:	66 90                	xchg   %ax,%ax
  800d90:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d94:	89 f9                	mov    %edi,%ecx
  800d96:	d3 e2                	shl    %cl,%edx
  800d98:	39 c2                	cmp    %eax,%edx
  800d9a:	73 e9                	jae    800d85 <__udivdi3+0xe5>
  800d9c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d9f:	31 ff                	xor    %edi,%edi
  800da1:	e9 40 ff ff ff       	jmp    800ce6 <__udivdi3+0x46>
  800da6:	66 90                	xchg   %ax,%ax
  800da8:	31 c0                	xor    %eax,%eax
  800daa:	e9 37 ff ff ff       	jmp    800ce6 <__udivdi3+0x46>
  800daf:	90                   	nop

00800db0 <__umoddi3>:
  800db0:	55                   	push   %ebp
  800db1:	57                   	push   %edi
  800db2:	56                   	push   %esi
  800db3:	53                   	push   %ebx
  800db4:	83 ec 1c             	sub    $0x1c,%esp
  800db7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800dbb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dbf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dc3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dc7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dcb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dcf:	89 f3                	mov    %esi,%ebx
  800dd1:	89 fa                	mov    %edi,%edx
  800dd3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dd7:	89 34 24             	mov    %esi,(%esp)
  800dda:	85 c0                	test   %eax,%eax
  800ddc:	75 1a                	jne    800df8 <__umoddi3+0x48>
  800dde:	39 f7                	cmp    %esi,%edi
  800de0:	0f 86 a2 00 00 00    	jbe    800e88 <__umoddi3+0xd8>
  800de6:	89 c8                	mov    %ecx,%eax
  800de8:	89 f2                	mov    %esi,%edx
  800dea:	f7 f7                	div    %edi
  800dec:	89 d0                	mov    %edx,%eax
  800dee:	31 d2                	xor    %edx,%edx
  800df0:	83 c4 1c             	add    $0x1c,%esp
  800df3:	5b                   	pop    %ebx
  800df4:	5e                   	pop    %esi
  800df5:	5f                   	pop    %edi
  800df6:	5d                   	pop    %ebp
  800df7:	c3                   	ret    
  800df8:	39 f0                	cmp    %esi,%eax
  800dfa:	0f 87 ac 00 00 00    	ja     800eac <__umoddi3+0xfc>
  800e00:	0f bd e8             	bsr    %eax,%ebp
  800e03:	83 f5 1f             	xor    $0x1f,%ebp
  800e06:	0f 84 ac 00 00 00    	je     800eb8 <__umoddi3+0x108>
  800e0c:	bf 20 00 00 00       	mov    $0x20,%edi
  800e11:	29 ef                	sub    %ebp,%edi
  800e13:	89 fe                	mov    %edi,%esi
  800e15:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e19:	89 e9                	mov    %ebp,%ecx
  800e1b:	d3 e0                	shl    %cl,%eax
  800e1d:	89 d7                	mov    %edx,%edi
  800e1f:	89 f1                	mov    %esi,%ecx
  800e21:	d3 ef                	shr    %cl,%edi
  800e23:	09 c7                	or     %eax,%edi
  800e25:	89 e9                	mov    %ebp,%ecx
  800e27:	d3 e2                	shl    %cl,%edx
  800e29:	89 14 24             	mov    %edx,(%esp)
  800e2c:	89 d8                	mov    %ebx,%eax
  800e2e:	d3 e0                	shl    %cl,%eax
  800e30:	89 c2                	mov    %eax,%edx
  800e32:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e36:	d3 e0                	shl    %cl,%eax
  800e38:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e3c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e40:	89 f1                	mov    %esi,%ecx
  800e42:	d3 e8                	shr    %cl,%eax
  800e44:	09 d0                	or     %edx,%eax
  800e46:	d3 eb                	shr    %cl,%ebx
  800e48:	89 da                	mov    %ebx,%edx
  800e4a:	f7 f7                	div    %edi
  800e4c:	89 d3                	mov    %edx,%ebx
  800e4e:	f7 24 24             	mull   (%esp)
  800e51:	89 c6                	mov    %eax,%esi
  800e53:	89 d1                	mov    %edx,%ecx
  800e55:	39 d3                	cmp    %edx,%ebx
  800e57:	0f 82 87 00 00 00    	jb     800ee4 <__umoddi3+0x134>
  800e5d:	0f 84 91 00 00 00    	je     800ef4 <__umoddi3+0x144>
  800e63:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e67:	29 f2                	sub    %esi,%edx
  800e69:	19 cb                	sbb    %ecx,%ebx
  800e6b:	89 d8                	mov    %ebx,%eax
  800e6d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e71:	d3 e0                	shl    %cl,%eax
  800e73:	89 e9                	mov    %ebp,%ecx
  800e75:	d3 ea                	shr    %cl,%edx
  800e77:	09 d0                	or     %edx,%eax
  800e79:	89 e9                	mov    %ebp,%ecx
  800e7b:	d3 eb                	shr    %cl,%ebx
  800e7d:	89 da                	mov    %ebx,%edx
  800e7f:	83 c4 1c             	add    $0x1c,%esp
  800e82:	5b                   	pop    %ebx
  800e83:	5e                   	pop    %esi
  800e84:	5f                   	pop    %edi
  800e85:	5d                   	pop    %ebp
  800e86:	c3                   	ret    
  800e87:	90                   	nop
  800e88:	89 fd                	mov    %edi,%ebp
  800e8a:	85 ff                	test   %edi,%edi
  800e8c:	75 0b                	jne    800e99 <__umoddi3+0xe9>
  800e8e:	b8 01 00 00 00       	mov    $0x1,%eax
  800e93:	31 d2                	xor    %edx,%edx
  800e95:	f7 f7                	div    %edi
  800e97:	89 c5                	mov    %eax,%ebp
  800e99:	89 f0                	mov    %esi,%eax
  800e9b:	31 d2                	xor    %edx,%edx
  800e9d:	f7 f5                	div    %ebp
  800e9f:	89 c8                	mov    %ecx,%eax
  800ea1:	f7 f5                	div    %ebp
  800ea3:	89 d0                	mov    %edx,%eax
  800ea5:	e9 44 ff ff ff       	jmp    800dee <__umoddi3+0x3e>
  800eaa:	66 90                	xchg   %ax,%ax
  800eac:	89 c8                	mov    %ecx,%eax
  800eae:	89 f2                	mov    %esi,%edx
  800eb0:	83 c4 1c             	add    $0x1c,%esp
  800eb3:	5b                   	pop    %ebx
  800eb4:	5e                   	pop    %esi
  800eb5:	5f                   	pop    %edi
  800eb6:	5d                   	pop    %ebp
  800eb7:	c3                   	ret    
  800eb8:	3b 04 24             	cmp    (%esp),%eax
  800ebb:	72 06                	jb     800ec3 <__umoddi3+0x113>
  800ebd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ec1:	77 0f                	ja     800ed2 <__umoddi3+0x122>
  800ec3:	89 f2                	mov    %esi,%edx
  800ec5:	29 f9                	sub    %edi,%ecx
  800ec7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ecb:	89 14 24             	mov    %edx,(%esp)
  800ece:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ed2:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ed6:	8b 14 24             	mov    (%esp),%edx
  800ed9:	83 c4 1c             	add    $0x1c,%esp
  800edc:	5b                   	pop    %ebx
  800edd:	5e                   	pop    %esi
  800ede:	5f                   	pop    %edi
  800edf:	5d                   	pop    %ebp
  800ee0:	c3                   	ret    
  800ee1:	8d 76 00             	lea    0x0(%esi),%esi
  800ee4:	2b 04 24             	sub    (%esp),%eax
  800ee7:	19 fa                	sbb    %edi,%edx
  800ee9:	89 d1                	mov    %edx,%ecx
  800eeb:	89 c6                	mov    %eax,%esi
  800eed:	e9 71 ff ff ff       	jmp    800e63 <__umoddi3+0xb3>
  800ef2:	66 90                	xchg   %ax,%ax
  800ef4:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800ef8:	72 ea                	jb     800ee4 <__umoddi3+0x134>
  800efa:	89 d9                	mov    %ebx,%ecx
  800efc:	e9 62 ff ff ff       	jmp    800e63 <__umoddi3+0xb3>
