
obj/user/breakpoint:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 08 00 00 00       	call   800039 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $3"); //break point
  800036:	cc                   	int3   
}
  800037:	5d                   	pop    %ebp
  800038:	c3                   	ret    

00800039 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800039:	55                   	push   %ebp
  80003a:	89 e5                	mov    %esp,%ebp
  80003c:	56                   	push   %esi
  80003d:	53                   	push   %ebx
  80003e:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800041:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800044:	e8 cf 00 00 00       	call   800118 <sys_getenvid>
  800049:	25 ff 03 00 00       	and    $0x3ff,%eax
  80004e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800055:	c1 e0 07             	shl    $0x7,%eax
  800058:	29 d0                	sub    %edx,%eax
  80005a:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80005f:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800064:	85 db                	test   %ebx,%ebx
  800066:	7e 07                	jle    80006f <libmain+0x36>
		binaryname = argv[0];
  800068:	8b 06                	mov    (%esi),%eax
  80006a:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80006f:	83 ec 08             	sub    $0x8,%esp
  800072:	56                   	push   %esi
  800073:	53                   	push   %ebx
  800074:	e8 ba ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800079:	e8 0a 00 00 00       	call   800088 <exit>
}
  80007e:	83 c4 10             	add    $0x10,%esp
  800081:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800084:	5b                   	pop    %ebx
  800085:	5e                   	pop    %esi
  800086:	5d                   	pop    %ebp
  800087:	c3                   	ret    

00800088 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800088:	55                   	push   %ebp
  800089:	89 e5                	mov    %esp,%ebp
  80008b:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80008e:	6a 00                	push   $0x0
  800090:	e8 42 00 00 00       	call   8000d7 <sys_env_destroy>
}
  800095:	83 c4 10             	add    $0x10,%esp
  800098:	c9                   	leave  
  800099:	c3                   	ret    

0080009a <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009a:	55                   	push   %ebp
  80009b:	89 e5                	mov    %esp,%ebp
  80009d:	57                   	push   %edi
  80009e:	56                   	push   %esi
  80009f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a0:	b8 00 00 00 00       	mov    $0x0,%eax
  8000a5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000a8:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ab:	89 c3                	mov    %eax,%ebx
  8000ad:	89 c7                	mov    %eax,%edi
  8000af:	89 c6                	mov    %eax,%esi
  8000b1:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b3:	5b                   	pop    %ebx
  8000b4:	5e                   	pop    %esi
  8000b5:	5f                   	pop    %edi
  8000b6:	5d                   	pop    %ebp
  8000b7:	c3                   	ret    

008000b8 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000b8:	55                   	push   %ebp
  8000b9:	89 e5                	mov    %esp,%ebp
  8000bb:	57                   	push   %edi
  8000bc:	56                   	push   %esi
  8000bd:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000be:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c3:	b8 01 00 00 00       	mov    $0x1,%eax
  8000c8:	89 d1                	mov    %edx,%ecx
  8000ca:	89 d3                	mov    %edx,%ebx
  8000cc:	89 d7                	mov    %edx,%edi
  8000ce:	89 d6                	mov    %edx,%esi
  8000d0:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d2:	5b                   	pop    %ebx
  8000d3:	5e                   	pop    %esi
  8000d4:	5f                   	pop    %edi
  8000d5:	5d                   	pop    %ebp
  8000d6:	c3                   	ret    

008000d7 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000d7:	55                   	push   %ebp
  8000d8:	89 e5                	mov    %esp,%ebp
  8000da:	57                   	push   %edi
  8000db:	56                   	push   %esi
  8000dc:	53                   	push   %ebx
  8000dd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e0:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000e5:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ea:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ed:	89 cb                	mov    %ecx,%ebx
  8000ef:	89 cf                	mov    %ecx,%edi
  8000f1:	89 ce                	mov    %ecx,%esi
  8000f3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000f5:	85 c0                	test   %eax,%eax
  8000f7:	7e 17                	jle    800110 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000f9:	83 ec 0c             	sub    $0xc,%esp
  8000fc:	50                   	push   %eax
  8000fd:	6a 03                	push   $0x3
  8000ff:	68 0a 0f 80 00       	push   $0x800f0a
  800104:	6a 23                	push   $0x23
  800106:	68 27 0f 80 00       	push   $0x800f27
  80010b:	e8 14 02 00 00       	call   800324 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800110:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800113:	5b                   	pop    %ebx
  800114:	5e                   	pop    %esi
  800115:	5f                   	pop    %edi
  800116:	5d                   	pop    %ebp
  800117:	c3                   	ret    

00800118 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800118:	55                   	push   %ebp
  800119:	89 e5                	mov    %esp,%ebp
  80011b:	57                   	push   %edi
  80011c:	56                   	push   %esi
  80011d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80011e:	ba 00 00 00 00       	mov    $0x0,%edx
  800123:	b8 02 00 00 00       	mov    $0x2,%eax
  800128:	89 d1                	mov    %edx,%ecx
  80012a:	89 d3                	mov    %edx,%ebx
  80012c:	89 d7                	mov    %edx,%edi
  80012e:	89 d6                	mov    %edx,%esi
  800130:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800132:	5b                   	pop    %ebx
  800133:	5e                   	pop    %esi
  800134:	5f                   	pop    %edi
  800135:	5d                   	pop    %ebp
  800136:	c3                   	ret    

00800137 <sys_yield>:

void
sys_yield(void)
{
  800137:	55                   	push   %ebp
  800138:	89 e5                	mov    %esp,%ebp
  80013a:	57                   	push   %edi
  80013b:	56                   	push   %esi
  80013c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80013d:	ba 00 00 00 00       	mov    $0x0,%edx
  800142:	b8 0a 00 00 00       	mov    $0xa,%eax
  800147:	89 d1                	mov    %edx,%ecx
  800149:	89 d3                	mov    %edx,%ebx
  80014b:	89 d7                	mov    %edx,%edi
  80014d:	89 d6                	mov    %edx,%esi
  80014f:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800151:	5b                   	pop    %ebx
  800152:	5e                   	pop    %esi
  800153:	5f                   	pop    %edi
  800154:	5d                   	pop    %ebp
  800155:	c3                   	ret    

00800156 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800156:	55                   	push   %ebp
  800157:	89 e5                	mov    %esp,%ebp
  800159:	57                   	push   %edi
  80015a:	56                   	push   %esi
  80015b:	53                   	push   %ebx
  80015c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80015f:	be 00 00 00 00       	mov    $0x0,%esi
  800164:	b8 04 00 00 00       	mov    $0x4,%eax
  800169:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80016c:	8b 55 08             	mov    0x8(%ebp),%edx
  80016f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800172:	89 f7                	mov    %esi,%edi
  800174:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800176:	85 c0                	test   %eax,%eax
  800178:	7e 17                	jle    800191 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80017a:	83 ec 0c             	sub    $0xc,%esp
  80017d:	50                   	push   %eax
  80017e:	6a 04                	push   $0x4
  800180:	68 0a 0f 80 00       	push   $0x800f0a
  800185:	6a 23                	push   $0x23
  800187:	68 27 0f 80 00       	push   $0x800f27
  80018c:	e8 93 01 00 00       	call   800324 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800191:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800194:	5b                   	pop    %ebx
  800195:	5e                   	pop    %esi
  800196:	5f                   	pop    %edi
  800197:	5d                   	pop    %ebp
  800198:	c3                   	ret    

00800199 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800199:	55                   	push   %ebp
  80019a:	89 e5                	mov    %esp,%ebp
  80019c:	57                   	push   %edi
  80019d:	56                   	push   %esi
  80019e:	53                   	push   %ebx
  80019f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001a2:	b8 05 00 00 00       	mov    $0x5,%eax
  8001a7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001aa:	8b 55 08             	mov    0x8(%ebp),%edx
  8001ad:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b0:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001b3:	8b 75 18             	mov    0x18(%ebp),%esi
  8001b6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001b8:	85 c0                	test   %eax,%eax
  8001ba:	7e 17                	jle    8001d3 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001bc:	83 ec 0c             	sub    $0xc,%esp
  8001bf:	50                   	push   %eax
  8001c0:	6a 05                	push   $0x5
  8001c2:	68 0a 0f 80 00       	push   $0x800f0a
  8001c7:	6a 23                	push   $0x23
  8001c9:	68 27 0f 80 00       	push   $0x800f27
  8001ce:	e8 51 01 00 00       	call   800324 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001d6:	5b                   	pop    %ebx
  8001d7:	5e                   	pop    %esi
  8001d8:	5f                   	pop    %edi
  8001d9:	5d                   	pop    %ebp
  8001da:	c3                   	ret    

008001db <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001db:	55                   	push   %ebp
  8001dc:	89 e5                	mov    %esp,%ebp
  8001de:	57                   	push   %edi
  8001df:	56                   	push   %esi
  8001e0:	53                   	push   %ebx
  8001e1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001e4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001e9:	b8 06 00 00 00       	mov    $0x6,%eax
  8001ee:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001f1:	8b 55 08             	mov    0x8(%ebp),%edx
  8001f4:	89 df                	mov    %ebx,%edi
  8001f6:	89 de                	mov    %ebx,%esi
  8001f8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001fa:	85 c0                	test   %eax,%eax
  8001fc:	7e 17                	jle    800215 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001fe:	83 ec 0c             	sub    $0xc,%esp
  800201:	50                   	push   %eax
  800202:	6a 06                	push   $0x6
  800204:	68 0a 0f 80 00       	push   $0x800f0a
  800209:	6a 23                	push   $0x23
  80020b:	68 27 0f 80 00       	push   $0x800f27
  800210:	e8 0f 01 00 00       	call   800324 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800215:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800218:	5b                   	pop    %ebx
  800219:	5e                   	pop    %esi
  80021a:	5f                   	pop    %edi
  80021b:	5d                   	pop    %ebp
  80021c:	c3                   	ret    

0080021d <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80021d:	55                   	push   %ebp
  80021e:	89 e5                	mov    %esp,%ebp
  800220:	57                   	push   %edi
  800221:	56                   	push   %esi
  800222:	53                   	push   %ebx
  800223:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800226:	bb 00 00 00 00       	mov    $0x0,%ebx
  80022b:	b8 08 00 00 00       	mov    $0x8,%eax
  800230:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800233:	8b 55 08             	mov    0x8(%ebp),%edx
  800236:	89 df                	mov    %ebx,%edi
  800238:	89 de                	mov    %ebx,%esi
  80023a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80023c:	85 c0                	test   %eax,%eax
  80023e:	7e 17                	jle    800257 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800240:	83 ec 0c             	sub    $0xc,%esp
  800243:	50                   	push   %eax
  800244:	6a 08                	push   $0x8
  800246:	68 0a 0f 80 00       	push   $0x800f0a
  80024b:	6a 23                	push   $0x23
  80024d:	68 27 0f 80 00       	push   $0x800f27
  800252:	e8 cd 00 00 00       	call   800324 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800257:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80025a:	5b                   	pop    %ebx
  80025b:	5e                   	pop    %esi
  80025c:	5f                   	pop    %edi
  80025d:	5d                   	pop    %ebp
  80025e:	c3                   	ret    

0080025f <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80025f:	55                   	push   %ebp
  800260:	89 e5                	mov    %esp,%ebp
  800262:	57                   	push   %edi
  800263:	56                   	push   %esi
  800264:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800265:	ba 00 00 00 00       	mov    $0x0,%edx
  80026a:	b8 0b 00 00 00       	mov    $0xb,%eax
  80026f:	89 d1                	mov    %edx,%ecx
  800271:	89 d3                	mov    %edx,%ebx
  800273:	89 d7                	mov    %edx,%edi
  800275:	89 d6                	mov    %edx,%esi
  800277:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800279:	5b                   	pop    %ebx
  80027a:	5e                   	pop    %esi
  80027b:	5f                   	pop    %edi
  80027c:	5d                   	pop    %ebp
  80027d:	c3                   	ret    

0080027e <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80027e:	55                   	push   %ebp
  80027f:	89 e5                	mov    %esp,%ebp
  800281:	57                   	push   %edi
  800282:	56                   	push   %esi
  800283:	53                   	push   %ebx
  800284:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800287:	bb 00 00 00 00       	mov    $0x0,%ebx
  80028c:	b8 09 00 00 00       	mov    $0x9,%eax
  800291:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800294:	8b 55 08             	mov    0x8(%ebp),%edx
  800297:	89 df                	mov    %ebx,%edi
  800299:	89 de                	mov    %ebx,%esi
  80029b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80029d:	85 c0                	test   %eax,%eax
  80029f:	7e 17                	jle    8002b8 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002a1:	83 ec 0c             	sub    $0xc,%esp
  8002a4:	50                   	push   %eax
  8002a5:	6a 09                	push   $0x9
  8002a7:	68 0a 0f 80 00       	push   $0x800f0a
  8002ac:	6a 23                	push   $0x23
  8002ae:	68 27 0f 80 00       	push   $0x800f27
  8002b3:	e8 6c 00 00 00       	call   800324 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002b8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002bb:	5b                   	pop    %ebx
  8002bc:	5e                   	pop    %esi
  8002bd:	5f                   	pop    %edi
  8002be:	5d                   	pop    %ebp
  8002bf:	c3                   	ret    

008002c0 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8002c0:	55                   	push   %ebp
  8002c1:	89 e5                	mov    %esp,%ebp
  8002c3:	57                   	push   %edi
  8002c4:	56                   	push   %esi
  8002c5:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002c6:	be 00 00 00 00       	mov    $0x0,%esi
  8002cb:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002d0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002d3:	8b 55 08             	mov    0x8(%ebp),%edx
  8002d6:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8002d9:	8b 7d 14             	mov    0x14(%ebp),%edi
  8002dc:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8002de:	5b                   	pop    %ebx
  8002df:	5e                   	pop    %esi
  8002e0:	5f                   	pop    %edi
  8002e1:	5d                   	pop    %ebp
  8002e2:	c3                   	ret    

008002e3 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8002e3:	55                   	push   %ebp
  8002e4:	89 e5                	mov    %esp,%ebp
  8002e6:	57                   	push   %edi
  8002e7:	56                   	push   %esi
  8002e8:	53                   	push   %ebx
  8002e9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002ec:	b9 00 00 00 00       	mov    $0x0,%ecx
  8002f1:	b8 0d 00 00 00       	mov    $0xd,%eax
  8002f6:	8b 55 08             	mov    0x8(%ebp),%edx
  8002f9:	89 cb                	mov    %ecx,%ebx
  8002fb:	89 cf                	mov    %ecx,%edi
  8002fd:	89 ce                	mov    %ecx,%esi
  8002ff:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800301:	85 c0                	test   %eax,%eax
  800303:	7e 17                	jle    80031c <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800305:	83 ec 0c             	sub    $0xc,%esp
  800308:	50                   	push   %eax
  800309:	6a 0d                	push   $0xd
  80030b:	68 0a 0f 80 00       	push   $0x800f0a
  800310:	6a 23                	push   $0x23
  800312:	68 27 0f 80 00       	push   $0x800f27
  800317:	e8 08 00 00 00       	call   800324 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80031c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80031f:	5b                   	pop    %ebx
  800320:	5e                   	pop    %esi
  800321:	5f                   	pop    %edi
  800322:	5d                   	pop    %ebp
  800323:	c3                   	ret    

00800324 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800324:	55                   	push   %ebp
  800325:	89 e5                	mov    %esp,%ebp
  800327:	56                   	push   %esi
  800328:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800329:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80032c:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800332:	e8 e1 fd ff ff       	call   800118 <sys_getenvid>
  800337:	83 ec 0c             	sub    $0xc,%esp
  80033a:	ff 75 0c             	pushl  0xc(%ebp)
  80033d:	ff 75 08             	pushl  0x8(%ebp)
  800340:	56                   	push   %esi
  800341:	50                   	push   %eax
  800342:	68 38 0f 80 00       	push   $0x800f38
  800347:	e8 b0 00 00 00       	call   8003fc <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80034c:	83 c4 18             	add    $0x18,%esp
  80034f:	53                   	push   %ebx
  800350:	ff 75 10             	pushl  0x10(%ebp)
  800353:	e8 53 00 00 00       	call   8003ab <vcprintf>
	cprintf("\n");
  800358:	c7 04 24 5c 0f 80 00 	movl   $0x800f5c,(%esp)
  80035f:	e8 98 00 00 00       	call   8003fc <cprintf>
  800364:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800367:	cc                   	int3   
  800368:	eb fd                	jmp    800367 <_panic+0x43>

0080036a <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80036a:	55                   	push   %ebp
  80036b:	89 e5                	mov    %esp,%ebp
  80036d:	53                   	push   %ebx
  80036e:	83 ec 04             	sub    $0x4,%esp
  800371:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800374:	8b 13                	mov    (%ebx),%edx
  800376:	8d 42 01             	lea    0x1(%edx),%eax
  800379:	89 03                	mov    %eax,(%ebx)
  80037b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80037e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800382:	3d ff 00 00 00       	cmp    $0xff,%eax
  800387:	75 1a                	jne    8003a3 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800389:	83 ec 08             	sub    $0x8,%esp
  80038c:	68 ff 00 00 00       	push   $0xff
  800391:	8d 43 08             	lea    0x8(%ebx),%eax
  800394:	50                   	push   %eax
  800395:	e8 00 fd ff ff       	call   80009a <sys_cputs>
		b->idx = 0;
  80039a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003a0:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003a3:	ff 43 04             	incl   0x4(%ebx)
}
  8003a6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003a9:	c9                   	leave  
  8003aa:	c3                   	ret    

008003ab <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003ab:	55                   	push   %ebp
  8003ac:	89 e5                	mov    %esp,%ebp
  8003ae:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003b4:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003bb:	00 00 00 
	b.cnt = 0;
  8003be:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003c5:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003c8:	ff 75 0c             	pushl  0xc(%ebp)
  8003cb:	ff 75 08             	pushl  0x8(%ebp)
  8003ce:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003d4:	50                   	push   %eax
  8003d5:	68 6a 03 80 00       	push   $0x80036a
  8003da:	e8 51 01 00 00       	call   800530 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003df:	83 c4 08             	add    $0x8,%esp
  8003e2:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003e8:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003ee:	50                   	push   %eax
  8003ef:	e8 a6 fc ff ff       	call   80009a <sys_cputs>

	return b.cnt;
}
  8003f4:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8003fa:	c9                   	leave  
  8003fb:	c3                   	ret    

008003fc <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8003fc:	55                   	push   %ebp
  8003fd:	89 e5                	mov    %esp,%ebp
  8003ff:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800402:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800405:	50                   	push   %eax
  800406:	ff 75 08             	pushl  0x8(%ebp)
  800409:	e8 9d ff ff ff       	call   8003ab <vcprintf>
	va_end(ap);

	return cnt;
}
  80040e:	c9                   	leave  
  80040f:	c3                   	ret    

00800410 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800410:	55                   	push   %ebp
  800411:	89 e5                	mov    %esp,%ebp
  800413:	57                   	push   %edi
  800414:	56                   	push   %esi
  800415:	53                   	push   %ebx
  800416:	83 ec 1c             	sub    $0x1c,%esp
  800419:	89 c7                	mov    %eax,%edi
  80041b:	89 d6                	mov    %edx,%esi
  80041d:	8b 45 08             	mov    0x8(%ebp),%eax
  800420:	8b 55 0c             	mov    0xc(%ebp),%edx
  800423:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800426:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800429:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80042c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800431:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800434:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800437:	39 d3                	cmp    %edx,%ebx
  800439:	72 05                	jb     800440 <printnum+0x30>
  80043b:	39 45 10             	cmp    %eax,0x10(%ebp)
  80043e:	77 45                	ja     800485 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800440:	83 ec 0c             	sub    $0xc,%esp
  800443:	ff 75 18             	pushl  0x18(%ebp)
  800446:	8b 45 14             	mov    0x14(%ebp),%eax
  800449:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80044c:	53                   	push   %ebx
  80044d:	ff 75 10             	pushl  0x10(%ebp)
  800450:	83 ec 08             	sub    $0x8,%esp
  800453:	ff 75 e4             	pushl  -0x1c(%ebp)
  800456:	ff 75 e0             	pushl  -0x20(%ebp)
  800459:	ff 75 dc             	pushl  -0x24(%ebp)
  80045c:	ff 75 d8             	pushl  -0x28(%ebp)
  80045f:	e8 24 08 00 00       	call   800c88 <__udivdi3>
  800464:	83 c4 18             	add    $0x18,%esp
  800467:	52                   	push   %edx
  800468:	50                   	push   %eax
  800469:	89 f2                	mov    %esi,%edx
  80046b:	89 f8                	mov    %edi,%eax
  80046d:	e8 9e ff ff ff       	call   800410 <printnum>
  800472:	83 c4 20             	add    $0x20,%esp
  800475:	eb 16                	jmp    80048d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800477:	83 ec 08             	sub    $0x8,%esp
  80047a:	56                   	push   %esi
  80047b:	ff 75 18             	pushl  0x18(%ebp)
  80047e:	ff d7                	call   *%edi
  800480:	83 c4 10             	add    $0x10,%esp
  800483:	eb 03                	jmp    800488 <printnum+0x78>
  800485:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800488:	4b                   	dec    %ebx
  800489:	85 db                	test   %ebx,%ebx
  80048b:	7f ea                	jg     800477 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80048d:	83 ec 08             	sub    $0x8,%esp
  800490:	56                   	push   %esi
  800491:	83 ec 04             	sub    $0x4,%esp
  800494:	ff 75 e4             	pushl  -0x1c(%ebp)
  800497:	ff 75 e0             	pushl  -0x20(%ebp)
  80049a:	ff 75 dc             	pushl  -0x24(%ebp)
  80049d:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a0:	e8 f3 08 00 00       	call   800d98 <__umoddi3>
  8004a5:	83 c4 14             	add    $0x14,%esp
  8004a8:	0f be 80 5e 0f 80 00 	movsbl 0x800f5e(%eax),%eax
  8004af:	50                   	push   %eax
  8004b0:	ff d7                	call   *%edi
}
  8004b2:	83 c4 10             	add    $0x10,%esp
  8004b5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004b8:	5b                   	pop    %ebx
  8004b9:	5e                   	pop    %esi
  8004ba:	5f                   	pop    %edi
  8004bb:	5d                   	pop    %ebp
  8004bc:	c3                   	ret    

008004bd <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004bd:	55                   	push   %ebp
  8004be:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004c0:	83 fa 01             	cmp    $0x1,%edx
  8004c3:	7e 0e                	jle    8004d3 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004c5:	8b 10                	mov    (%eax),%edx
  8004c7:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004ca:	89 08                	mov    %ecx,(%eax)
  8004cc:	8b 02                	mov    (%edx),%eax
  8004ce:	8b 52 04             	mov    0x4(%edx),%edx
  8004d1:	eb 22                	jmp    8004f5 <getuint+0x38>
	else if (lflag)
  8004d3:	85 d2                	test   %edx,%edx
  8004d5:	74 10                	je     8004e7 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004d7:	8b 10                	mov    (%eax),%edx
  8004d9:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004dc:	89 08                	mov    %ecx,(%eax)
  8004de:	8b 02                	mov    (%edx),%eax
  8004e0:	ba 00 00 00 00       	mov    $0x0,%edx
  8004e5:	eb 0e                	jmp    8004f5 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004e7:	8b 10                	mov    (%eax),%edx
  8004e9:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004ec:	89 08                	mov    %ecx,(%eax)
  8004ee:	8b 02                	mov    (%edx),%eax
  8004f0:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8004f5:	5d                   	pop    %ebp
  8004f6:	c3                   	ret    

008004f7 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8004f7:	55                   	push   %ebp
  8004f8:	89 e5                	mov    %esp,%ebp
  8004fa:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8004fd:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800500:	8b 10                	mov    (%eax),%edx
  800502:	3b 50 04             	cmp    0x4(%eax),%edx
  800505:	73 0a                	jae    800511 <sprintputch+0x1a>
		*b->buf++ = ch;
  800507:	8d 4a 01             	lea    0x1(%edx),%ecx
  80050a:	89 08                	mov    %ecx,(%eax)
  80050c:	8b 45 08             	mov    0x8(%ebp),%eax
  80050f:	88 02                	mov    %al,(%edx)
}
  800511:	5d                   	pop    %ebp
  800512:	c3                   	ret    

00800513 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800513:	55                   	push   %ebp
  800514:	89 e5                	mov    %esp,%ebp
  800516:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800519:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80051c:	50                   	push   %eax
  80051d:	ff 75 10             	pushl  0x10(%ebp)
  800520:	ff 75 0c             	pushl  0xc(%ebp)
  800523:	ff 75 08             	pushl  0x8(%ebp)
  800526:	e8 05 00 00 00       	call   800530 <vprintfmt>
	va_end(ap);
}
  80052b:	83 c4 10             	add    $0x10,%esp
  80052e:	c9                   	leave  
  80052f:	c3                   	ret    

00800530 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800530:	55                   	push   %ebp
  800531:	89 e5                	mov    %esp,%ebp
  800533:	57                   	push   %edi
  800534:	56                   	push   %esi
  800535:	53                   	push   %ebx
  800536:	83 ec 2c             	sub    $0x2c,%esp
  800539:	8b 75 08             	mov    0x8(%ebp),%esi
  80053c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80053f:	8b 7d 10             	mov    0x10(%ebp),%edi
  800542:	eb 12                	jmp    800556 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800544:	85 c0                	test   %eax,%eax
  800546:	0f 84 68 03 00 00    	je     8008b4 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80054c:	83 ec 08             	sub    $0x8,%esp
  80054f:	53                   	push   %ebx
  800550:	50                   	push   %eax
  800551:	ff d6                	call   *%esi
  800553:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800556:	47                   	inc    %edi
  800557:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80055b:	83 f8 25             	cmp    $0x25,%eax
  80055e:	75 e4                	jne    800544 <vprintfmt+0x14>
  800560:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800564:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80056b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800572:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800579:	ba 00 00 00 00       	mov    $0x0,%edx
  80057e:	eb 07                	jmp    800587 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800580:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800583:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800587:	8d 47 01             	lea    0x1(%edi),%eax
  80058a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80058d:	0f b6 0f             	movzbl (%edi),%ecx
  800590:	8a 07                	mov    (%edi),%al
  800592:	83 e8 23             	sub    $0x23,%eax
  800595:	3c 55                	cmp    $0x55,%al
  800597:	0f 87 fe 02 00 00    	ja     80089b <vprintfmt+0x36b>
  80059d:	0f b6 c0             	movzbl %al,%eax
  8005a0:	ff 24 85 20 10 80 00 	jmp    *0x801020(,%eax,4)
  8005a7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005aa:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005ae:	eb d7                	jmp    800587 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005b3:	b8 00 00 00 00       	mov    $0x0,%eax
  8005b8:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005bb:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005be:	01 c0                	add    %eax,%eax
  8005c0:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005c4:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005c7:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005ca:	83 fa 09             	cmp    $0x9,%edx
  8005cd:	77 34                	ja     800603 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005cf:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005d0:	eb e9                	jmp    8005bb <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005d2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d5:	8d 48 04             	lea    0x4(%eax),%ecx
  8005d8:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005db:	8b 00                	mov    (%eax),%eax
  8005dd:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005e3:	eb 24                	jmp    800609 <vprintfmt+0xd9>
  8005e5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005e9:	79 07                	jns    8005f2 <vprintfmt+0xc2>
  8005eb:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f5:	eb 90                	jmp    800587 <vprintfmt+0x57>
  8005f7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8005fa:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800601:	eb 84                	jmp    800587 <vprintfmt+0x57>
  800603:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800606:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800609:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80060d:	0f 89 74 ff ff ff    	jns    800587 <vprintfmt+0x57>
				width = precision, precision = -1;
  800613:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800616:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800619:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800620:	e9 62 ff ff ff       	jmp    800587 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800625:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800626:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800629:	e9 59 ff ff ff       	jmp    800587 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80062e:	8b 45 14             	mov    0x14(%ebp),%eax
  800631:	8d 50 04             	lea    0x4(%eax),%edx
  800634:	89 55 14             	mov    %edx,0x14(%ebp)
  800637:	83 ec 08             	sub    $0x8,%esp
  80063a:	53                   	push   %ebx
  80063b:	ff 30                	pushl  (%eax)
  80063d:	ff d6                	call   *%esi
			break;
  80063f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800642:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800645:	e9 0c ff ff ff       	jmp    800556 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80064a:	8b 45 14             	mov    0x14(%ebp),%eax
  80064d:	8d 50 04             	lea    0x4(%eax),%edx
  800650:	89 55 14             	mov    %edx,0x14(%ebp)
  800653:	8b 00                	mov    (%eax),%eax
  800655:	85 c0                	test   %eax,%eax
  800657:	79 02                	jns    80065b <vprintfmt+0x12b>
  800659:	f7 d8                	neg    %eax
  80065b:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80065d:	83 f8 08             	cmp    $0x8,%eax
  800660:	7f 0b                	jg     80066d <vprintfmt+0x13d>
  800662:	8b 04 85 80 11 80 00 	mov    0x801180(,%eax,4),%eax
  800669:	85 c0                	test   %eax,%eax
  80066b:	75 18                	jne    800685 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80066d:	52                   	push   %edx
  80066e:	68 76 0f 80 00       	push   $0x800f76
  800673:	53                   	push   %ebx
  800674:	56                   	push   %esi
  800675:	e8 99 fe ff ff       	call   800513 <printfmt>
  80067a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80067d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800680:	e9 d1 fe ff ff       	jmp    800556 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800685:	50                   	push   %eax
  800686:	68 7f 0f 80 00       	push   $0x800f7f
  80068b:	53                   	push   %ebx
  80068c:	56                   	push   %esi
  80068d:	e8 81 fe ff ff       	call   800513 <printfmt>
  800692:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800695:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800698:	e9 b9 fe ff ff       	jmp    800556 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80069d:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a0:	8d 50 04             	lea    0x4(%eax),%edx
  8006a3:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a6:	8b 38                	mov    (%eax),%edi
  8006a8:	85 ff                	test   %edi,%edi
  8006aa:	75 05                	jne    8006b1 <vprintfmt+0x181>
				p = "(null)";
  8006ac:	bf 6f 0f 80 00       	mov    $0x800f6f,%edi
			if (width > 0 && padc != '-')
  8006b1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006b5:	0f 8e 90 00 00 00    	jle    80074b <vprintfmt+0x21b>
  8006bb:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006bf:	0f 84 8e 00 00 00    	je     800753 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006c5:	83 ec 08             	sub    $0x8,%esp
  8006c8:	ff 75 d0             	pushl  -0x30(%ebp)
  8006cb:	57                   	push   %edi
  8006cc:	e8 70 02 00 00       	call   800941 <strnlen>
  8006d1:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006d4:	29 c1                	sub    %eax,%ecx
  8006d6:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006d9:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006dc:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006e0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006e3:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006e6:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006e8:	eb 0d                	jmp    8006f7 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006ea:	83 ec 08             	sub    $0x8,%esp
  8006ed:	53                   	push   %ebx
  8006ee:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006f1:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f3:	4f                   	dec    %edi
  8006f4:	83 c4 10             	add    $0x10,%esp
  8006f7:	85 ff                	test   %edi,%edi
  8006f9:	7f ef                	jg     8006ea <vprintfmt+0x1ba>
  8006fb:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8006fe:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800701:	89 c8                	mov    %ecx,%eax
  800703:	85 c9                	test   %ecx,%ecx
  800705:	79 05                	jns    80070c <vprintfmt+0x1dc>
  800707:	b8 00 00 00 00       	mov    $0x0,%eax
  80070c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80070f:	29 c1                	sub    %eax,%ecx
  800711:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800714:	89 75 08             	mov    %esi,0x8(%ebp)
  800717:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80071a:	eb 3d                	jmp    800759 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80071c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800720:	74 19                	je     80073b <vprintfmt+0x20b>
  800722:	0f be c0             	movsbl %al,%eax
  800725:	83 e8 20             	sub    $0x20,%eax
  800728:	83 f8 5e             	cmp    $0x5e,%eax
  80072b:	76 0e                	jbe    80073b <vprintfmt+0x20b>
					putch('?', putdat);
  80072d:	83 ec 08             	sub    $0x8,%esp
  800730:	53                   	push   %ebx
  800731:	6a 3f                	push   $0x3f
  800733:	ff 55 08             	call   *0x8(%ebp)
  800736:	83 c4 10             	add    $0x10,%esp
  800739:	eb 0b                	jmp    800746 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80073b:	83 ec 08             	sub    $0x8,%esp
  80073e:	53                   	push   %ebx
  80073f:	52                   	push   %edx
  800740:	ff 55 08             	call   *0x8(%ebp)
  800743:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800746:	ff 4d e4             	decl   -0x1c(%ebp)
  800749:	eb 0e                	jmp    800759 <vprintfmt+0x229>
  80074b:	89 75 08             	mov    %esi,0x8(%ebp)
  80074e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800751:	eb 06                	jmp    800759 <vprintfmt+0x229>
  800753:	89 75 08             	mov    %esi,0x8(%ebp)
  800756:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800759:	47                   	inc    %edi
  80075a:	8a 47 ff             	mov    -0x1(%edi),%al
  80075d:	0f be d0             	movsbl %al,%edx
  800760:	85 d2                	test   %edx,%edx
  800762:	74 1d                	je     800781 <vprintfmt+0x251>
  800764:	85 f6                	test   %esi,%esi
  800766:	78 b4                	js     80071c <vprintfmt+0x1ec>
  800768:	4e                   	dec    %esi
  800769:	79 b1                	jns    80071c <vprintfmt+0x1ec>
  80076b:	8b 75 08             	mov    0x8(%ebp),%esi
  80076e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800771:	eb 14                	jmp    800787 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800773:	83 ec 08             	sub    $0x8,%esp
  800776:	53                   	push   %ebx
  800777:	6a 20                	push   $0x20
  800779:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80077b:	4f                   	dec    %edi
  80077c:	83 c4 10             	add    $0x10,%esp
  80077f:	eb 06                	jmp    800787 <vprintfmt+0x257>
  800781:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800784:	8b 75 08             	mov    0x8(%ebp),%esi
  800787:	85 ff                	test   %edi,%edi
  800789:	7f e8                	jg     800773 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80078b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80078e:	e9 c3 fd ff ff       	jmp    800556 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800793:	83 fa 01             	cmp    $0x1,%edx
  800796:	7e 16                	jle    8007ae <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800798:	8b 45 14             	mov    0x14(%ebp),%eax
  80079b:	8d 50 08             	lea    0x8(%eax),%edx
  80079e:	89 55 14             	mov    %edx,0x14(%ebp)
  8007a1:	8b 50 04             	mov    0x4(%eax),%edx
  8007a4:	8b 00                	mov    (%eax),%eax
  8007a6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007a9:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ac:	eb 32                	jmp    8007e0 <vprintfmt+0x2b0>
	else if (lflag)
  8007ae:	85 d2                	test   %edx,%edx
  8007b0:	74 18                	je     8007ca <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007b2:	8b 45 14             	mov    0x14(%ebp),%eax
  8007b5:	8d 50 04             	lea    0x4(%eax),%edx
  8007b8:	89 55 14             	mov    %edx,0x14(%ebp)
  8007bb:	8b 00                	mov    (%eax),%eax
  8007bd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007c0:	89 c1                	mov    %eax,%ecx
  8007c2:	c1 f9 1f             	sar    $0x1f,%ecx
  8007c5:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007c8:	eb 16                	jmp    8007e0 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007ca:	8b 45 14             	mov    0x14(%ebp),%eax
  8007cd:	8d 50 04             	lea    0x4(%eax),%edx
  8007d0:	89 55 14             	mov    %edx,0x14(%ebp)
  8007d3:	8b 00                	mov    (%eax),%eax
  8007d5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007d8:	89 c1                	mov    %eax,%ecx
  8007da:	c1 f9 1f             	sar    $0x1f,%ecx
  8007dd:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007e0:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007e3:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007e6:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007ea:	79 76                	jns    800862 <vprintfmt+0x332>
				putch('-', putdat);
  8007ec:	83 ec 08             	sub    $0x8,%esp
  8007ef:	53                   	push   %ebx
  8007f0:	6a 2d                	push   $0x2d
  8007f2:	ff d6                	call   *%esi
				num = -(long long) num;
  8007f4:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007f7:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8007fa:	f7 d8                	neg    %eax
  8007fc:	83 d2 00             	adc    $0x0,%edx
  8007ff:	f7 da                	neg    %edx
  800801:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800804:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800809:	eb 5c                	jmp    800867 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80080b:	8d 45 14             	lea    0x14(%ebp),%eax
  80080e:	e8 aa fc ff ff       	call   8004bd <getuint>
			base = 10;
  800813:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800818:	eb 4d                	jmp    800867 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80081a:	8d 45 14             	lea    0x14(%ebp),%eax
  80081d:	e8 9b fc ff ff       	call   8004bd <getuint>
			base = 8;
  800822:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800827:	eb 3e                	jmp    800867 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800829:	83 ec 08             	sub    $0x8,%esp
  80082c:	53                   	push   %ebx
  80082d:	6a 30                	push   $0x30
  80082f:	ff d6                	call   *%esi
			putch('x', putdat);
  800831:	83 c4 08             	add    $0x8,%esp
  800834:	53                   	push   %ebx
  800835:	6a 78                	push   $0x78
  800837:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800839:	8b 45 14             	mov    0x14(%ebp),%eax
  80083c:	8d 50 04             	lea    0x4(%eax),%edx
  80083f:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800842:	8b 00                	mov    (%eax),%eax
  800844:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800849:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80084c:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800851:	eb 14                	jmp    800867 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800853:	8d 45 14             	lea    0x14(%ebp),%eax
  800856:	e8 62 fc ff ff       	call   8004bd <getuint>
			base = 16;
  80085b:	b9 10 00 00 00       	mov    $0x10,%ecx
  800860:	eb 05                	jmp    800867 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800862:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800867:	83 ec 0c             	sub    $0xc,%esp
  80086a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80086e:	57                   	push   %edi
  80086f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800872:	51                   	push   %ecx
  800873:	52                   	push   %edx
  800874:	50                   	push   %eax
  800875:	89 da                	mov    %ebx,%edx
  800877:	89 f0                	mov    %esi,%eax
  800879:	e8 92 fb ff ff       	call   800410 <printnum>
			break;
  80087e:	83 c4 20             	add    $0x20,%esp
  800881:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800884:	e9 cd fc ff ff       	jmp    800556 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800889:	83 ec 08             	sub    $0x8,%esp
  80088c:	53                   	push   %ebx
  80088d:	51                   	push   %ecx
  80088e:	ff d6                	call   *%esi
			break;
  800890:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800893:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800896:	e9 bb fc ff ff       	jmp    800556 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80089b:	83 ec 08             	sub    $0x8,%esp
  80089e:	53                   	push   %ebx
  80089f:	6a 25                	push   $0x25
  8008a1:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008a3:	83 c4 10             	add    $0x10,%esp
  8008a6:	eb 01                	jmp    8008a9 <vprintfmt+0x379>
  8008a8:	4f                   	dec    %edi
  8008a9:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008ad:	75 f9                	jne    8008a8 <vprintfmt+0x378>
  8008af:	e9 a2 fc ff ff       	jmp    800556 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008b4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008b7:	5b                   	pop    %ebx
  8008b8:	5e                   	pop    %esi
  8008b9:	5f                   	pop    %edi
  8008ba:	5d                   	pop    %ebp
  8008bb:	c3                   	ret    

008008bc <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008bc:	55                   	push   %ebp
  8008bd:	89 e5                	mov    %esp,%ebp
  8008bf:	83 ec 18             	sub    $0x18,%esp
  8008c2:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c5:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008c8:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008cb:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008cf:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008d2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008d9:	85 c0                	test   %eax,%eax
  8008db:	74 26                	je     800903 <vsnprintf+0x47>
  8008dd:	85 d2                	test   %edx,%edx
  8008df:	7e 29                	jle    80090a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008e1:	ff 75 14             	pushl  0x14(%ebp)
  8008e4:	ff 75 10             	pushl  0x10(%ebp)
  8008e7:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008ea:	50                   	push   %eax
  8008eb:	68 f7 04 80 00       	push   $0x8004f7
  8008f0:	e8 3b fc ff ff       	call   800530 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8008f5:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8008f8:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8008fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8008fe:	83 c4 10             	add    $0x10,%esp
  800901:	eb 0c                	jmp    80090f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800903:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800908:	eb 05                	jmp    80090f <vsnprintf+0x53>
  80090a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80090f:	c9                   	leave  
  800910:	c3                   	ret    

00800911 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800911:	55                   	push   %ebp
  800912:	89 e5                	mov    %esp,%ebp
  800914:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800917:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80091a:	50                   	push   %eax
  80091b:	ff 75 10             	pushl  0x10(%ebp)
  80091e:	ff 75 0c             	pushl  0xc(%ebp)
  800921:	ff 75 08             	pushl  0x8(%ebp)
  800924:	e8 93 ff ff ff       	call   8008bc <vsnprintf>
	va_end(ap);

	return rc;
}
  800929:	c9                   	leave  
  80092a:	c3                   	ret    

0080092b <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80092b:	55                   	push   %ebp
  80092c:	89 e5                	mov    %esp,%ebp
  80092e:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800931:	b8 00 00 00 00       	mov    $0x0,%eax
  800936:	eb 01                	jmp    800939 <strlen+0xe>
		n++;
  800938:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800939:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80093d:	75 f9                	jne    800938 <strlen+0xd>
		n++;
	return n;
}
  80093f:	5d                   	pop    %ebp
  800940:	c3                   	ret    

00800941 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800941:	55                   	push   %ebp
  800942:	89 e5                	mov    %esp,%ebp
  800944:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800947:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80094a:	ba 00 00 00 00       	mov    $0x0,%edx
  80094f:	eb 01                	jmp    800952 <strnlen+0x11>
		n++;
  800951:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800952:	39 c2                	cmp    %eax,%edx
  800954:	74 08                	je     80095e <strnlen+0x1d>
  800956:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80095a:	75 f5                	jne    800951 <strnlen+0x10>
  80095c:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80095e:	5d                   	pop    %ebp
  80095f:	c3                   	ret    

00800960 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800960:	55                   	push   %ebp
  800961:	89 e5                	mov    %esp,%ebp
  800963:	53                   	push   %ebx
  800964:	8b 45 08             	mov    0x8(%ebp),%eax
  800967:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80096a:	89 c2                	mov    %eax,%edx
  80096c:	42                   	inc    %edx
  80096d:	41                   	inc    %ecx
  80096e:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800971:	88 5a ff             	mov    %bl,-0x1(%edx)
  800974:	84 db                	test   %bl,%bl
  800976:	75 f4                	jne    80096c <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800978:	5b                   	pop    %ebx
  800979:	5d                   	pop    %ebp
  80097a:	c3                   	ret    

0080097b <strcat>:

char *
strcat(char *dst, const char *src)
{
  80097b:	55                   	push   %ebp
  80097c:	89 e5                	mov    %esp,%ebp
  80097e:	53                   	push   %ebx
  80097f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800982:	53                   	push   %ebx
  800983:	e8 a3 ff ff ff       	call   80092b <strlen>
  800988:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80098b:	ff 75 0c             	pushl  0xc(%ebp)
  80098e:	01 d8                	add    %ebx,%eax
  800990:	50                   	push   %eax
  800991:	e8 ca ff ff ff       	call   800960 <strcpy>
	return dst;
}
  800996:	89 d8                	mov    %ebx,%eax
  800998:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80099b:	c9                   	leave  
  80099c:	c3                   	ret    

0080099d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80099d:	55                   	push   %ebp
  80099e:	89 e5                	mov    %esp,%ebp
  8009a0:	56                   	push   %esi
  8009a1:	53                   	push   %ebx
  8009a2:	8b 75 08             	mov    0x8(%ebp),%esi
  8009a5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009a8:	89 f3                	mov    %esi,%ebx
  8009aa:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ad:	89 f2                	mov    %esi,%edx
  8009af:	eb 0c                	jmp    8009bd <strncpy+0x20>
		*dst++ = *src;
  8009b1:	42                   	inc    %edx
  8009b2:	8a 01                	mov    (%ecx),%al
  8009b4:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009b7:	80 39 01             	cmpb   $0x1,(%ecx)
  8009ba:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009bd:	39 da                	cmp    %ebx,%edx
  8009bf:	75 f0                	jne    8009b1 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009c1:	89 f0                	mov    %esi,%eax
  8009c3:	5b                   	pop    %ebx
  8009c4:	5e                   	pop    %esi
  8009c5:	5d                   	pop    %ebp
  8009c6:	c3                   	ret    

008009c7 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009c7:	55                   	push   %ebp
  8009c8:	89 e5                	mov    %esp,%ebp
  8009ca:	56                   	push   %esi
  8009cb:	53                   	push   %ebx
  8009cc:	8b 75 08             	mov    0x8(%ebp),%esi
  8009cf:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009d2:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009d5:	85 c0                	test   %eax,%eax
  8009d7:	74 1e                	je     8009f7 <strlcpy+0x30>
  8009d9:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009dd:	89 f2                	mov    %esi,%edx
  8009df:	eb 05                	jmp    8009e6 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009e1:	42                   	inc    %edx
  8009e2:	41                   	inc    %ecx
  8009e3:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009e6:	39 c2                	cmp    %eax,%edx
  8009e8:	74 08                	je     8009f2 <strlcpy+0x2b>
  8009ea:	8a 19                	mov    (%ecx),%bl
  8009ec:	84 db                	test   %bl,%bl
  8009ee:	75 f1                	jne    8009e1 <strlcpy+0x1a>
  8009f0:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8009f2:	c6 00 00             	movb   $0x0,(%eax)
  8009f5:	eb 02                	jmp    8009f9 <strlcpy+0x32>
  8009f7:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8009f9:	29 f0                	sub    %esi,%eax
}
  8009fb:	5b                   	pop    %ebx
  8009fc:	5e                   	pop    %esi
  8009fd:	5d                   	pop    %ebp
  8009fe:	c3                   	ret    

008009ff <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8009ff:	55                   	push   %ebp
  800a00:	89 e5                	mov    %esp,%ebp
  800a02:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a05:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a08:	eb 02                	jmp    800a0c <strcmp+0xd>
		p++, q++;
  800a0a:	41                   	inc    %ecx
  800a0b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a0c:	8a 01                	mov    (%ecx),%al
  800a0e:	84 c0                	test   %al,%al
  800a10:	74 04                	je     800a16 <strcmp+0x17>
  800a12:	3a 02                	cmp    (%edx),%al
  800a14:	74 f4                	je     800a0a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a16:	0f b6 c0             	movzbl %al,%eax
  800a19:	0f b6 12             	movzbl (%edx),%edx
  800a1c:	29 d0                	sub    %edx,%eax
}
  800a1e:	5d                   	pop    %ebp
  800a1f:	c3                   	ret    

00800a20 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a20:	55                   	push   %ebp
  800a21:	89 e5                	mov    %esp,%ebp
  800a23:	53                   	push   %ebx
  800a24:	8b 45 08             	mov    0x8(%ebp),%eax
  800a27:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a2a:	89 c3                	mov    %eax,%ebx
  800a2c:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a2f:	eb 02                	jmp    800a33 <strncmp+0x13>
		n--, p++, q++;
  800a31:	40                   	inc    %eax
  800a32:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a33:	39 d8                	cmp    %ebx,%eax
  800a35:	74 14                	je     800a4b <strncmp+0x2b>
  800a37:	8a 08                	mov    (%eax),%cl
  800a39:	84 c9                	test   %cl,%cl
  800a3b:	74 04                	je     800a41 <strncmp+0x21>
  800a3d:	3a 0a                	cmp    (%edx),%cl
  800a3f:	74 f0                	je     800a31 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a41:	0f b6 00             	movzbl (%eax),%eax
  800a44:	0f b6 12             	movzbl (%edx),%edx
  800a47:	29 d0                	sub    %edx,%eax
  800a49:	eb 05                	jmp    800a50 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a4b:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a50:	5b                   	pop    %ebx
  800a51:	5d                   	pop    %ebp
  800a52:	c3                   	ret    

00800a53 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a53:	55                   	push   %ebp
  800a54:	89 e5                	mov    %esp,%ebp
  800a56:	8b 45 08             	mov    0x8(%ebp),%eax
  800a59:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a5c:	eb 05                	jmp    800a63 <strchr+0x10>
		if (*s == c)
  800a5e:	38 ca                	cmp    %cl,%dl
  800a60:	74 0c                	je     800a6e <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a62:	40                   	inc    %eax
  800a63:	8a 10                	mov    (%eax),%dl
  800a65:	84 d2                	test   %dl,%dl
  800a67:	75 f5                	jne    800a5e <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a69:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a6e:	5d                   	pop    %ebp
  800a6f:	c3                   	ret    

00800a70 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a70:	55                   	push   %ebp
  800a71:	89 e5                	mov    %esp,%ebp
  800a73:	8b 45 08             	mov    0x8(%ebp),%eax
  800a76:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a79:	eb 05                	jmp    800a80 <strfind+0x10>
		if (*s == c)
  800a7b:	38 ca                	cmp    %cl,%dl
  800a7d:	74 07                	je     800a86 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a7f:	40                   	inc    %eax
  800a80:	8a 10                	mov    (%eax),%dl
  800a82:	84 d2                	test   %dl,%dl
  800a84:	75 f5                	jne    800a7b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a86:	5d                   	pop    %ebp
  800a87:	c3                   	ret    

00800a88 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a88:	55                   	push   %ebp
  800a89:	89 e5                	mov    %esp,%ebp
  800a8b:	57                   	push   %edi
  800a8c:	56                   	push   %esi
  800a8d:	53                   	push   %ebx
  800a8e:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a91:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a94:	85 c9                	test   %ecx,%ecx
  800a96:	74 36                	je     800ace <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800a98:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800a9e:	75 28                	jne    800ac8 <memset+0x40>
  800aa0:	f6 c1 03             	test   $0x3,%cl
  800aa3:	75 23                	jne    800ac8 <memset+0x40>
		c &= 0xFF;
  800aa5:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aa9:	89 d3                	mov    %edx,%ebx
  800aab:	c1 e3 08             	shl    $0x8,%ebx
  800aae:	89 d6                	mov    %edx,%esi
  800ab0:	c1 e6 18             	shl    $0x18,%esi
  800ab3:	89 d0                	mov    %edx,%eax
  800ab5:	c1 e0 10             	shl    $0x10,%eax
  800ab8:	09 f0                	or     %esi,%eax
  800aba:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800abc:	89 d8                	mov    %ebx,%eax
  800abe:	09 d0                	or     %edx,%eax
  800ac0:	c1 e9 02             	shr    $0x2,%ecx
  800ac3:	fc                   	cld    
  800ac4:	f3 ab                	rep stos %eax,%es:(%edi)
  800ac6:	eb 06                	jmp    800ace <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ac8:	8b 45 0c             	mov    0xc(%ebp),%eax
  800acb:	fc                   	cld    
  800acc:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800ace:	89 f8                	mov    %edi,%eax
  800ad0:	5b                   	pop    %ebx
  800ad1:	5e                   	pop    %esi
  800ad2:	5f                   	pop    %edi
  800ad3:	5d                   	pop    %ebp
  800ad4:	c3                   	ret    

00800ad5 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ad5:	55                   	push   %ebp
  800ad6:	89 e5                	mov    %esp,%ebp
  800ad8:	57                   	push   %edi
  800ad9:	56                   	push   %esi
  800ada:	8b 45 08             	mov    0x8(%ebp),%eax
  800add:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ae0:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ae3:	39 c6                	cmp    %eax,%esi
  800ae5:	73 33                	jae    800b1a <memmove+0x45>
  800ae7:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800aea:	39 d0                	cmp    %edx,%eax
  800aec:	73 2c                	jae    800b1a <memmove+0x45>
		s += n;
		d += n;
  800aee:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800af1:	89 d6                	mov    %edx,%esi
  800af3:	09 fe                	or     %edi,%esi
  800af5:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800afb:	75 13                	jne    800b10 <memmove+0x3b>
  800afd:	f6 c1 03             	test   $0x3,%cl
  800b00:	75 0e                	jne    800b10 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b02:	83 ef 04             	sub    $0x4,%edi
  800b05:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b08:	c1 e9 02             	shr    $0x2,%ecx
  800b0b:	fd                   	std    
  800b0c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b0e:	eb 07                	jmp    800b17 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b10:	4f                   	dec    %edi
  800b11:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b14:	fd                   	std    
  800b15:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b17:	fc                   	cld    
  800b18:	eb 1d                	jmp    800b37 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b1a:	89 f2                	mov    %esi,%edx
  800b1c:	09 c2                	or     %eax,%edx
  800b1e:	f6 c2 03             	test   $0x3,%dl
  800b21:	75 0f                	jne    800b32 <memmove+0x5d>
  800b23:	f6 c1 03             	test   $0x3,%cl
  800b26:	75 0a                	jne    800b32 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b28:	c1 e9 02             	shr    $0x2,%ecx
  800b2b:	89 c7                	mov    %eax,%edi
  800b2d:	fc                   	cld    
  800b2e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b30:	eb 05                	jmp    800b37 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b32:	89 c7                	mov    %eax,%edi
  800b34:	fc                   	cld    
  800b35:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b37:	5e                   	pop    %esi
  800b38:	5f                   	pop    %edi
  800b39:	5d                   	pop    %ebp
  800b3a:	c3                   	ret    

00800b3b <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b3b:	55                   	push   %ebp
  800b3c:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b3e:	ff 75 10             	pushl  0x10(%ebp)
  800b41:	ff 75 0c             	pushl  0xc(%ebp)
  800b44:	ff 75 08             	pushl  0x8(%ebp)
  800b47:	e8 89 ff ff ff       	call   800ad5 <memmove>
}
  800b4c:	c9                   	leave  
  800b4d:	c3                   	ret    

00800b4e <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b4e:	55                   	push   %ebp
  800b4f:	89 e5                	mov    %esp,%ebp
  800b51:	56                   	push   %esi
  800b52:	53                   	push   %ebx
  800b53:	8b 45 08             	mov    0x8(%ebp),%eax
  800b56:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b59:	89 c6                	mov    %eax,%esi
  800b5b:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b5e:	eb 14                	jmp    800b74 <memcmp+0x26>
		if (*s1 != *s2)
  800b60:	8a 08                	mov    (%eax),%cl
  800b62:	8a 1a                	mov    (%edx),%bl
  800b64:	38 d9                	cmp    %bl,%cl
  800b66:	74 0a                	je     800b72 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b68:	0f b6 c1             	movzbl %cl,%eax
  800b6b:	0f b6 db             	movzbl %bl,%ebx
  800b6e:	29 d8                	sub    %ebx,%eax
  800b70:	eb 0b                	jmp    800b7d <memcmp+0x2f>
		s1++, s2++;
  800b72:	40                   	inc    %eax
  800b73:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b74:	39 f0                	cmp    %esi,%eax
  800b76:	75 e8                	jne    800b60 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b78:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b7d:	5b                   	pop    %ebx
  800b7e:	5e                   	pop    %esi
  800b7f:	5d                   	pop    %ebp
  800b80:	c3                   	ret    

00800b81 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b81:	55                   	push   %ebp
  800b82:	89 e5                	mov    %esp,%ebp
  800b84:	53                   	push   %ebx
  800b85:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b88:	89 c1                	mov    %eax,%ecx
  800b8a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b8d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b91:	eb 08                	jmp    800b9b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b93:	0f b6 10             	movzbl (%eax),%edx
  800b96:	39 da                	cmp    %ebx,%edx
  800b98:	74 05                	je     800b9f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b9a:	40                   	inc    %eax
  800b9b:	39 c8                	cmp    %ecx,%eax
  800b9d:	72 f4                	jb     800b93 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800b9f:	5b                   	pop    %ebx
  800ba0:	5d                   	pop    %ebp
  800ba1:	c3                   	ret    

00800ba2 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800ba2:	55                   	push   %ebp
  800ba3:	89 e5                	mov    %esp,%ebp
  800ba5:	57                   	push   %edi
  800ba6:	56                   	push   %esi
  800ba7:	53                   	push   %ebx
  800ba8:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bab:	eb 01                	jmp    800bae <strtol+0xc>
		s++;
  800bad:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bae:	8a 01                	mov    (%ecx),%al
  800bb0:	3c 20                	cmp    $0x20,%al
  800bb2:	74 f9                	je     800bad <strtol+0xb>
  800bb4:	3c 09                	cmp    $0x9,%al
  800bb6:	74 f5                	je     800bad <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bb8:	3c 2b                	cmp    $0x2b,%al
  800bba:	75 08                	jne    800bc4 <strtol+0x22>
		s++;
  800bbc:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bbd:	bf 00 00 00 00       	mov    $0x0,%edi
  800bc2:	eb 11                	jmp    800bd5 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bc4:	3c 2d                	cmp    $0x2d,%al
  800bc6:	75 08                	jne    800bd0 <strtol+0x2e>
		s++, neg = 1;
  800bc8:	41                   	inc    %ecx
  800bc9:	bf 01 00 00 00       	mov    $0x1,%edi
  800bce:	eb 05                	jmp    800bd5 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bd0:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bd5:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bd9:	0f 84 87 00 00 00    	je     800c66 <strtol+0xc4>
  800bdf:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800be3:	75 27                	jne    800c0c <strtol+0x6a>
  800be5:	80 39 30             	cmpb   $0x30,(%ecx)
  800be8:	75 22                	jne    800c0c <strtol+0x6a>
  800bea:	e9 88 00 00 00       	jmp    800c77 <strtol+0xd5>
		s += 2, base = 16;
  800bef:	83 c1 02             	add    $0x2,%ecx
  800bf2:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800bf9:	eb 11                	jmp    800c0c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800bfb:	41                   	inc    %ecx
  800bfc:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c03:	eb 07                	jmp    800c0c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c05:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c0c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c11:	8a 11                	mov    (%ecx),%dl
  800c13:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c16:	80 fb 09             	cmp    $0x9,%bl
  800c19:	77 08                	ja     800c23 <strtol+0x81>
			dig = *s - '0';
  800c1b:	0f be d2             	movsbl %dl,%edx
  800c1e:	83 ea 30             	sub    $0x30,%edx
  800c21:	eb 22                	jmp    800c45 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c23:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c26:	89 f3                	mov    %esi,%ebx
  800c28:	80 fb 19             	cmp    $0x19,%bl
  800c2b:	77 08                	ja     800c35 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c2d:	0f be d2             	movsbl %dl,%edx
  800c30:	83 ea 57             	sub    $0x57,%edx
  800c33:	eb 10                	jmp    800c45 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c35:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c38:	89 f3                	mov    %esi,%ebx
  800c3a:	80 fb 19             	cmp    $0x19,%bl
  800c3d:	77 14                	ja     800c53 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c3f:	0f be d2             	movsbl %dl,%edx
  800c42:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c45:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c48:	7d 09                	jge    800c53 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c4a:	41                   	inc    %ecx
  800c4b:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c4f:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c51:	eb be                	jmp    800c11 <strtol+0x6f>

	if (endptr)
  800c53:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c57:	74 05                	je     800c5e <strtol+0xbc>
		*endptr = (char *) s;
  800c59:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c5c:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c5e:	85 ff                	test   %edi,%edi
  800c60:	74 21                	je     800c83 <strtol+0xe1>
  800c62:	f7 d8                	neg    %eax
  800c64:	eb 1d                	jmp    800c83 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c66:	80 39 30             	cmpb   $0x30,(%ecx)
  800c69:	75 9a                	jne    800c05 <strtol+0x63>
  800c6b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c6f:	0f 84 7a ff ff ff    	je     800bef <strtol+0x4d>
  800c75:	eb 84                	jmp    800bfb <strtol+0x59>
  800c77:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c7b:	0f 84 6e ff ff ff    	je     800bef <strtol+0x4d>
  800c81:	eb 89                	jmp    800c0c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c83:	5b                   	pop    %ebx
  800c84:	5e                   	pop    %esi
  800c85:	5f                   	pop    %edi
  800c86:	5d                   	pop    %ebp
  800c87:	c3                   	ret    

00800c88 <__udivdi3>:
  800c88:	55                   	push   %ebp
  800c89:	57                   	push   %edi
  800c8a:	56                   	push   %esi
  800c8b:	53                   	push   %ebx
  800c8c:	83 ec 1c             	sub    $0x1c,%esp
  800c8f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800c93:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800c97:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c9b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800c9f:	89 ca                	mov    %ecx,%edx
  800ca1:	89 f8                	mov    %edi,%eax
  800ca3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ca7:	85 f6                	test   %esi,%esi
  800ca9:	75 2d                	jne    800cd8 <__udivdi3+0x50>
  800cab:	39 cf                	cmp    %ecx,%edi
  800cad:	77 65                	ja     800d14 <__udivdi3+0x8c>
  800caf:	89 fd                	mov    %edi,%ebp
  800cb1:	85 ff                	test   %edi,%edi
  800cb3:	75 0b                	jne    800cc0 <__udivdi3+0x38>
  800cb5:	b8 01 00 00 00       	mov    $0x1,%eax
  800cba:	31 d2                	xor    %edx,%edx
  800cbc:	f7 f7                	div    %edi
  800cbe:	89 c5                	mov    %eax,%ebp
  800cc0:	31 d2                	xor    %edx,%edx
  800cc2:	89 c8                	mov    %ecx,%eax
  800cc4:	f7 f5                	div    %ebp
  800cc6:	89 c1                	mov    %eax,%ecx
  800cc8:	89 d8                	mov    %ebx,%eax
  800cca:	f7 f5                	div    %ebp
  800ccc:	89 cf                	mov    %ecx,%edi
  800cce:	89 fa                	mov    %edi,%edx
  800cd0:	83 c4 1c             	add    $0x1c,%esp
  800cd3:	5b                   	pop    %ebx
  800cd4:	5e                   	pop    %esi
  800cd5:	5f                   	pop    %edi
  800cd6:	5d                   	pop    %ebp
  800cd7:	c3                   	ret    
  800cd8:	39 ce                	cmp    %ecx,%esi
  800cda:	77 28                	ja     800d04 <__udivdi3+0x7c>
  800cdc:	0f bd fe             	bsr    %esi,%edi
  800cdf:	83 f7 1f             	xor    $0x1f,%edi
  800ce2:	75 40                	jne    800d24 <__udivdi3+0x9c>
  800ce4:	39 ce                	cmp    %ecx,%esi
  800ce6:	72 0a                	jb     800cf2 <__udivdi3+0x6a>
  800ce8:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800cec:	0f 87 9e 00 00 00    	ja     800d90 <__udivdi3+0x108>
  800cf2:	b8 01 00 00 00       	mov    $0x1,%eax
  800cf7:	89 fa                	mov    %edi,%edx
  800cf9:	83 c4 1c             	add    $0x1c,%esp
  800cfc:	5b                   	pop    %ebx
  800cfd:	5e                   	pop    %esi
  800cfe:	5f                   	pop    %edi
  800cff:	5d                   	pop    %ebp
  800d00:	c3                   	ret    
  800d01:	8d 76 00             	lea    0x0(%esi),%esi
  800d04:	31 ff                	xor    %edi,%edi
  800d06:	31 c0                	xor    %eax,%eax
  800d08:	89 fa                	mov    %edi,%edx
  800d0a:	83 c4 1c             	add    $0x1c,%esp
  800d0d:	5b                   	pop    %ebx
  800d0e:	5e                   	pop    %esi
  800d0f:	5f                   	pop    %edi
  800d10:	5d                   	pop    %ebp
  800d11:	c3                   	ret    
  800d12:	66 90                	xchg   %ax,%ax
  800d14:	89 d8                	mov    %ebx,%eax
  800d16:	f7 f7                	div    %edi
  800d18:	31 ff                	xor    %edi,%edi
  800d1a:	89 fa                	mov    %edi,%edx
  800d1c:	83 c4 1c             	add    $0x1c,%esp
  800d1f:	5b                   	pop    %ebx
  800d20:	5e                   	pop    %esi
  800d21:	5f                   	pop    %edi
  800d22:	5d                   	pop    %ebp
  800d23:	c3                   	ret    
  800d24:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d29:	89 eb                	mov    %ebp,%ebx
  800d2b:	29 fb                	sub    %edi,%ebx
  800d2d:	89 f9                	mov    %edi,%ecx
  800d2f:	d3 e6                	shl    %cl,%esi
  800d31:	89 c5                	mov    %eax,%ebp
  800d33:	88 d9                	mov    %bl,%cl
  800d35:	d3 ed                	shr    %cl,%ebp
  800d37:	89 e9                	mov    %ebp,%ecx
  800d39:	09 f1                	or     %esi,%ecx
  800d3b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d3f:	89 f9                	mov    %edi,%ecx
  800d41:	d3 e0                	shl    %cl,%eax
  800d43:	89 c5                	mov    %eax,%ebp
  800d45:	89 d6                	mov    %edx,%esi
  800d47:	88 d9                	mov    %bl,%cl
  800d49:	d3 ee                	shr    %cl,%esi
  800d4b:	89 f9                	mov    %edi,%ecx
  800d4d:	d3 e2                	shl    %cl,%edx
  800d4f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d53:	88 d9                	mov    %bl,%cl
  800d55:	d3 e8                	shr    %cl,%eax
  800d57:	09 c2                	or     %eax,%edx
  800d59:	89 d0                	mov    %edx,%eax
  800d5b:	89 f2                	mov    %esi,%edx
  800d5d:	f7 74 24 0c          	divl   0xc(%esp)
  800d61:	89 d6                	mov    %edx,%esi
  800d63:	89 c3                	mov    %eax,%ebx
  800d65:	f7 e5                	mul    %ebp
  800d67:	39 d6                	cmp    %edx,%esi
  800d69:	72 19                	jb     800d84 <__udivdi3+0xfc>
  800d6b:	74 0b                	je     800d78 <__udivdi3+0xf0>
  800d6d:	89 d8                	mov    %ebx,%eax
  800d6f:	31 ff                	xor    %edi,%edi
  800d71:	e9 58 ff ff ff       	jmp    800cce <__udivdi3+0x46>
  800d76:	66 90                	xchg   %ax,%ax
  800d78:	8b 54 24 08          	mov    0x8(%esp),%edx
  800d7c:	89 f9                	mov    %edi,%ecx
  800d7e:	d3 e2                	shl    %cl,%edx
  800d80:	39 c2                	cmp    %eax,%edx
  800d82:	73 e9                	jae    800d6d <__udivdi3+0xe5>
  800d84:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800d87:	31 ff                	xor    %edi,%edi
  800d89:	e9 40 ff ff ff       	jmp    800cce <__udivdi3+0x46>
  800d8e:	66 90                	xchg   %ax,%ax
  800d90:	31 c0                	xor    %eax,%eax
  800d92:	e9 37 ff ff ff       	jmp    800cce <__udivdi3+0x46>
  800d97:	90                   	nop

00800d98 <__umoddi3>:
  800d98:	55                   	push   %ebp
  800d99:	57                   	push   %edi
  800d9a:	56                   	push   %esi
  800d9b:	53                   	push   %ebx
  800d9c:	83 ec 1c             	sub    $0x1c,%esp
  800d9f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800da3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800da7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dab:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800daf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800db3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800db7:	89 f3                	mov    %esi,%ebx
  800db9:	89 fa                	mov    %edi,%edx
  800dbb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800dbf:	89 34 24             	mov    %esi,(%esp)
  800dc2:	85 c0                	test   %eax,%eax
  800dc4:	75 1a                	jne    800de0 <__umoddi3+0x48>
  800dc6:	39 f7                	cmp    %esi,%edi
  800dc8:	0f 86 a2 00 00 00    	jbe    800e70 <__umoddi3+0xd8>
  800dce:	89 c8                	mov    %ecx,%eax
  800dd0:	89 f2                	mov    %esi,%edx
  800dd2:	f7 f7                	div    %edi
  800dd4:	89 d0                	mov    %edx,%eax
  800dd6:	31 d2                	xor    %edx,%edx
  800dd8:	83 c4 1c             	add    $0x1c,%esp
  800ddb:	5b                   	pop    %ebx
  800ddc:	5e                   	pop    %esi
  800ddd:	5f                   	pop    %edi
  800dde:	5d                   	pop    %ebp
  800ddf:	c3                   	ret    
  800de0:	39 f0                	cmp    %esi,%eax
  800de2:	0f 87 ac 00 00 00    	ja     800e94 <__umoddi3+0xfc>
  800de8:	0f bd e8             	bsr    %eax,%ebp
  800deb:	83 f5 1f             	xor    $0x1f,%ebp
  800dee:	0f 84 ac 00 00 00    	je     800ea0 <__umoddi3+0x108>
  800df4:	bf 20 00 00 00       	mov    $0x20,%edi
  800df9:	29 ef                	sub    %ebp,%edi
  800dfb:	89 fe                	mov    %edi,%esi
  800dfd:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e01:	89 e9                	mov    %ebp,%ecx
  800e03:	d3 e0                	shl    %cl,%eax
  800e05:	89 d7                	mov    %edx,%edi
  800e07:	89 f1                	mov    %esi,%ecx
  800e09:	d3 ef                	shr    %cl,%edi
  800e0b:	09 c7                	or     %eax,%edi
  800e0d:	89 e9                	mov    %ebp,%ecx
  800e0f:	d3 e2                	shl    %cl,%edx
  800e11:	89 14 24             	mov    %edx,(%esp)
  800e14:	89 d8                	mov    %ebx,%eax
  800e16:	d3 e0                	shl    %cl,%eax
  800e18:	89 c2                	mov    %eax,%edx
  800e1a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e1e:	d3 e0                	shl    %cl,%eax
  800e20:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e24:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e28:	89 f1                	mov    %esi,%ecx
  800e2a:	d3 e8                	shr    %cl,%eax
  800e2c:	09 d0                	or     %edx,%eax
  800e2e:	d3 eb                	shr    %cl,%ebx
  800e30:	89 da                	mov    %ebx,%edx
  800e32:	f7 f7                	div    %edi
  800e34:	89 d3                	mov    %edx,%ebx
  800e36:	f7 24 24             	mull   (%esp)
  800e39:	89 c6                	mov    %eax,%esi
  800e3b:	89 d1                	mov    %edx,%ecx
  800e3d:	39 d3                	cmp    %edx,%ebx
  800e3f:	0f 82 87 00 00 00    	jb     800ecc <__umoddi3+0x134>
  800e45:	0f 84 91 00 00 00    	je     800edc <__umoddi3+0x144>
  800e4b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e4f:	29 f2                	sub    %esi,%edx
  800e51:	19 cb                	sbb    %ecx,%ebx
  800e53:	89 d8                	mov    %ebx,%eax
  800e55:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e59:	d3 e0                	shl    %cl,%eax
  800e5b:	89 e9                	mov    %ebp,%ecx
  800e5d:	d3 ea                	shr    %cl,%edx
  800e5f:	09 d0                	or     %edx,%eax
  800e61:	89 e9                	mov    %ebp,%ecx
  800e63:	d3 eb                	shr    %cl,%ebx
  800e65:	89 da                	mov    %ebx,%edx
  800e67:	83 c4 1c             	add    $0x1c,%esp
  800e6a:	5b                   	pop    %ebx
  800e6b:	5e                   	pop    %esi
  800e6c:	5f                   	pop    %edi
  800e6d:	5d                   	pop    %ebp
  800e6e:	c3                   	ret    
  800e6f:	90                   	nop
  800e70:	89 fd                	mov    %edi,%ebp
  800e72:	85 ff                	test   %edi,%edi
  800e74:	75 0b                	jne    800e81 <__umoddi3+0xe9>
  800e76:	b8 01 00 00 00       	mov    $0x1,%eax
  800e7b:	31 d2                	xor    %edx,%edx
  800e7d:	f7 f7                	div    %edi
  800e7f:	89 c5                	mov    %eax,%ebp
  800e81:	89 f0                	mov    %esi,%eax
  800e83:	31 d2                	xor    %edx,%edx
  800e85:	f7 f5                	div    %ebp
  800e87:	89 c8                	mov    %ecx,%eax
  800e89:	f7 f5                	div    %ebp
  800e8b:	89 d0                	mov    %edx,%eax
  800e8d:	e9 44 ff ff ff       	jmp    800dd6 <__umoddi3+0x3e>
  800e92:	66 90                	xchg   %ax,%ax
  800e94:	89 c8                	mov    %ecx,%eax
  800e96:	89 f2                	mov    %esi,%edx
  800e98:	83 c4 1c             	add    $0x1c,%esp
  800e9b:	5b                   	pop    %ebx
  800e9c:	5e                   	pop    %esi
  800e9d:	5f                   	pop    %edi
  800e9e:	5d                   	pop    %ebp
  800e9f:	c3                   	ret    
  800ea0:	3b 04 24             	cmp    (%esp),%eax
  800ea3:	72 06                	jb     800eab <__umoddi3+0x113>
  800ea5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ea9:	77 0f                	ja     800eba <__umoddi3+0x122>
  800eab:	89 f2                	mov    %esi,%edx
  800ead:	29 f9                	sub    %edi,%ecx
  800eaf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800eb3:	89 14 24             	mov    %edx,(%esp)
  800eb6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800eba:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ebe:	8b 14 24             	mov    (%esp),%edx
  800ec1:	83 c4 1c             	add    $0x1c,%esp
  800ec4:	5b                   	pop    %ebx
  800ec5:	5e                   	pop    %esi
  800ec6:	5f                   	pop    %edi
  800ec7:	5d                   	pop    %ebp
  800ec8:	c3                   	ret    
  800ec9:	8d 76 00             	lea    0x0(%esi),%esi
  800ecc:	2b 04 24             	sub    (%esp),%eax
  800ecf:	19 fa                	sbb    %edi,%edx
  800ed1:	89 d1                	mov    %edx,%ecx
  800ed3:	89 c6                	mov    %eax,%esi
  800ed5:	e9 71 ff ff ff       	jmp    800e4b <__umoddi3+0xb3>
  800eda:	66 90                	xchg   %ax,%ax
  800edc:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800ee0:	72 ea                	jb     800ecc <__umoddi3+0x134>
  800ee2:	89 d9                	mov    %ebx,%ecx
  800ee4:	e9 62 ff ff ff       	jmp    800e4b <__umoddi3+0xb3>
