
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start+0xeffffff4>:
.globl _start
_start = RELOC(entry)

.globl entry
entry:
	movw	$0x1234, 0x472			# warm boot
f0100000:	02 b0 ad 1b 02 00    	add    0x21bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fc                   	cld    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <entry>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 
	# sufficient until we set up our real page table in mem_init
	# in lab 2.

	# Load the physical address of entry_pgdir into cr3.  entry_pgdir
	# is defined in entrypgdir.c.
	movl	$(RELOC(entry_pgdir)), %ecx
f0100015:	b9 00 40 12 00       	mov    $0x124000,%ecx
	movl	%ecx, %cr3
f010001a:	0f 22 d9             	mov    %ecx,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %ecx
f010001d:	0f 20 e1             	mov    %cr4,%ecx
	orl	$(CR4_PSE), %ecx
f0100020:	83 c9 10             	or     $0x10,%ecx
	movl	%ecx, %cr4
f0100023:	0f 22 e1             	mov    %ecx,%cr4
	# Turn on paging.
	movl	%cr0, %ecx
f0100026:	0f 20 c1             	mov    %cr0,%ecx
	orl	$(CR0_PE|CR0_PG|CR0_WP), %ecx
f0100029:	81 c9 01 00 01 80    	or     $0x80010001,%ecx
	movl	%ecx, %cr0
f010002f:	0f 22 c1             	mov    %ecx,%cr0

	# Now paging is enabled, but we're still running at a low EIP
	# (why is this okay?).  Jump up above KERNBASE before entering
	# C code.
	mov	$relocated, %ecx
f0100032:	b9 39 00 10 f0       	mov    $0xf0100039,%ecx
	jmp	*%ecx
f0100037:	ff e1                	jmp    *%ecx

f0100039 <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0, %ebp			# nuke frame pointer
f0100039:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f010003e:	bc 00 40 12 f0       	mov    $0xf0124000,%esp

	# pointer to struct multiboot_info
	pushl	%ebx
f0100043:	53                   	push   %ebx
	# saved magic value
	pushl	%eax
f0100044:	50                   	push   %eax

	# now to C code
	call	i386_init
f0100045:	e8 5c 00 00 00       	call   f01000a6 <i386_init>

f010004a <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010004a:	eb fe                	jmp    f010004a <spin>

f010004c <_panic>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
f010004c:	55                   	push   %ebp
f010004d:	89 e5                	mov    %esp,%ebp
f010004f:	56                   	push   %esi
f0100050:	53                   	push   %ebx
f0100051:	8b 75 10             	mov    0x10(%ebp),%esi
	va_list ap;

	if (panicstr)
f0100054:	83 3d 10 3f 23 f0 00 	cmpl   $0x0,0xf0233f10
f010005b:	75 3a                	jne    f0100097 <_panic+0x4b>
		goto dead;
	panicstr = fmt;
f010005d:	89 35 10 3f 23 f0    	mov    %esi,0xf0233f10

	// Be extra sure that the machine is in as reasonable state
	asm volatile("cli; cld");
f0100063:	fa                   	cli    
f0100064:	fc                   	cld    

	va_start(ap, fmt);
f0100065:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel panic on CPU %d at %s:%d: ", cpunum(), file, line);
f0100068:	e8 c1 66 00 00       	call   f010672e <cpunum>
f010006d:	ff 75 0c             	pushl  0xc(%ebp)
f0100070:	ff 75 08             	pushl  0x8(%ebp)
f0100073:	50                   	push   %eax
f0100074:	68 a0 74 10 f0       	push   $0xf01074a0
f0100079:	e8 93 3b 00 00       	call   f0103c11 <cprintf>
	vcprintf(fmt, ap);
f010007e:	83 c4 08             	add    $0x8,%esp
f0100081:	53                   	push   %ebx
f0100082:	56                   	push   %esi
f0100083:	e8 63 3b 00 00       	call   f0103beb <vcprintf>
	cprintf("\n");
f0100088:	c7 04 24 b2 86 10 f0 	movl   $0xf01086b2,(%esp)
f010008f:	e8 7d 3b 00 00       	call   f0103c11 <cprintf>
	va_end(ap);
f0100094:	83 c4 10             	add    $0x10,%esp

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f0100097:	83 ec 0c             	sub    $0xc,%esp
f010009a:	6a 00                	push   $0x0
f010009c:	e8 d0 09 00 00       	call   f0100a71 <monitor>
f01000a1:	83 c4 10             	add    $0x10,%esp
f01000a4:	eb f1                	jmp    f0100097 <_panic+0x4b>

f01000a6 <i386_init>:
static void boot_aps(void);


void
i386_init(uint32_t magic, uint32_t addr)
{
f01000a6:	55                   	push   %ebp
f01000a7:	89 e5                	mov    %esp,%ebp
f01000a9:	56                   	push   %esi
f01000aa:	53                   	push   %ebx
	extern char edata[], end[];

	// Before doing anything else, complete the ELF loading process.
	// Clear the uninitialized global data (BSS) section of our program.
	// This ensures that all static/global variables start out zero.
	memset(edata, 0, end - edata);
f01000ab:	83 ec 04             	sub    $0x4,%esp
f01000ae:	b8 0c 60 27 f0       	mov    $0xf027600c,%eax
f01000b3:	2d 64 27 23 f0       	sub    $0xf0232764,%eax
f01000b8:	50                   	push   %eax
f01000b9:	6a 00                	push   $0x0
f01000bb:	68 64 27 23 f0       	push   $0xf0232764
f01000c0:	e8 63 5f 00 00       	call   f0106028 <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f01000c5:	e8 3c 06 00 00       	call   f0100706 <cons_init>

	// Must boot from Multiboot.
	assert(magic == MULTIBOOT_BOOTLOADER_MAGIC);
f01000ca:	83 c4 10             	add    $0x10,%esp
f01000cd:	81 7d 08 02 b0 ad 2b 	cmpl   $0x2badb002,0x8(%ebp)
f01000d4:	74 16                	je     f01000ec <i386_init+0x46>
f01000d6:	68 c4 74 10 f0       	push   $0xf01074c4
f01000db:	68 51 75 10 f0       	push   $0xf0107551
f01000e0:	6a 27                	push   $0x27
f01000e2:	68 66 75 10 f0       	push   $0xf0107566
f01000e7:	e8 60 ff ff ff       	call   f010004c <_panic>

	cprintf("451 decimal is %o octal!\n", 451);
f01000ec:	83 ec 08             	sub    $0x8,%esp
f01000ef:	68 c3 01 00 00       	push   $0x1c3
f01000f4:	68 72 75 10 f0       	push   $0xf0107572
f01000f9:	e8 13 3b 00 00       	call   f0103c11 <cprintf>

	// Print CPU information.
	cpuid_print();
f01000fe:	e8 42 56 00 00       	call   f0105745 <cpuid_print>

	// Initialize e820 memory map.
	e820_init(addr);
f0100103:	83 c4 04             	add    $0x4,%esp
f0100106:	ff 75 0c             	pushl  0xc(%ebp)
f0100109:	e8 a7 0a 00 00       	call   f0100bb5 <e820_init>

	// Lab 2 memory management initialization functions
	mem_init();
f010010e:	e8 5c 16 00 00       	call   f010176f <mem_init>

	// Lab 3 user environment initialization functions
	env_init();
f0100113:	e8 f1 32 00 00       	call   f0103409 <env_init>
	cprintf("After env_init\n");
f0100118:	c7 04 24 8c 75 10 f0 	movl   $0xf010758c,(%esp)
f010011f:	e8 ed 3a 00 00       	call   f0103c11 <cprintf>
	trap_init();
f0100124:	e8 bc 3b 00 00       	call   f0103ce5 <trap_init>
	//Advanced Configuration and Power Interface (ACPI) specification provides an open standard
	//that the operating systems can use for computer hardware discovery, configuration, power 
	//management, and monitoring. Internally, ACPI exports the available functionalities by providing 
	//certain instruction lists as part of the system firmware, which the operating system kernel 
	//interprets and executes to perform desired operations, using a form of embedded virtual machine.
	acpi_init();
f0100129:	e8 a8 62 00 00       	call   f01063d6 <acpi_init>
	mp_init(); 
f010012e:	e8 2b 65 00 00       	call   f010665e <mp_init>
	lapic_init(); //The local APIC manages internal (non-I/O) interrupts
f0100133:	e8 51 66 00 00       	call   f0106789 <lapic_init>

	// Lab 4 multitasking initialization functions
	pic_init();
f0100138:	e8 36 3a 00 00       	call   f0103b73 <pic_init>
	ioapic_init(); //The I/O APIC manages hardware interrupts for an SMP system.
f010013d:	e8 c1 68 00 00       	call   f0106a03 <ioapic_init>

	// Enable keyboard & serial interrupts
	ioapic_enable(IRQ_KBD, bootcpu->cpu_apicid);
f0100142:	83 c4 08             	add    $0x8,%esp
f0100145:	0f b6 05 00 50 23 f0 	movzbl 0xf0235000,%eax
f010014c:	50                   	push   %eax
f010014d:	6a 01                	push   $0x1
f010014f:	e8 58 69 00 00       	call   f0106aac <ioapic_enable>
	ioapic_enable(IRQ_SERIAL, bootcpu->cpu_apicid);
f0100154:	83 c4 08             	add    $0x8,%esp
f0100157:	0f b6 05 00 50 23 f0 	movzbl 0xf0235000,%eax
f010015e:	50                   	push   %eax
f010015f:	6a 04                	push   $0x4
f0100161:	e8 46 69 00 00       	call   f0106aac <ioapic_enable>

	// Lab 5 hardware initialization functions
	pci_init();
f0100166:	e8 0d 6f 00 00       	call   f0107078 <pci_init>
extern struct spinlock kernel_lock;

static inline void
lock_kernel(void)
{
	spin_lock(&kernel_lock);
f010016b:	c7 04 24 c0 53 12 f0 	movl   $0xf01253c0,(%esp)
f0100172:	e8 75 69 00 00       	call   f0106aec <spin_lock>
{
	extern unsigned char mpentry_start[], mpentry_end[];
	void *code;
	struct CpuInfo *c;

	if (ncpu <= 1)
f0100177:	83 c4 10             	add    $0x10,%esp
f010017a:	83 3d a0 53 23 f0 01 	cmpl   $0x1,0xf02353a0
f0100181:	0f 8e e1 00 00 00    	jle    f0100268 <i386_init+0x1c2>
		return;
	cprintf("SMP: BSP #%d [apicid %02x]\n", cpunum(), thiscpu->cpu_apicid);
f0100187:	e8 a2 65 00 00       	call   f010672e <cpunum>
f010018c:	6b c0 74             	imul   $0x74,%eax,%eax
f010018f:	0f b6 98 00 50 23 f0 	movzbl -0xfdcb000(%eax),%ebx
f0100196:	e8 93 65 00 00       	call   f010672e <cpunum>
f010019b:	83 ec 04             	sub    $0x4,%esp
f010019e:	53                   	push   %ebx
f010019f:	50                   	push   %eax
f01001a0:	68 9c 75 10 f0       	push   $0xf010759c
f01001a5:	e8 67 3a 00 00       	call   f0103c11 <cprintf>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01001aa:	83 c4 10             	add    $0x10,%esp
f01001ad:	83 3d 24 44 23 f0 07 	cmpl   $0x7,0xf0234424
f01001b4:	77 16                	ja     f01001cc <i386_init+0x126>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01001b6:	68 00 70 00 00       	push   $0x7000
f01001bb:	68 e8 74 10 f0       	push   $0xf01074e8
f01001c0:	6a 7d                	push   $0x7d
f01001c2:	68 66 75 10 f0       	push   $0xf0107566
f01001c7:	e8 80 fe ff ff       	call   f010004c <_panic>

	// Write entry code to unused memory at MPENTRY_PADDR
	code = KADDR(MPENTRY_PADDR);
	memmove(code, mpentry_start, mpentry_end - mpentry_start);
f01001cc:	83 ec 04             	sub    $0x4,%esp
f01001cf:	b8 aa 62 10 f0       	mov    $0xf01062aa,%eax
f01001d4:	2d 28 62 10 f0       	sub    $0xf0106228,%eax
f01001d9:	50                   	push   %eax
f01001da:	68 28 62 10 f0       	push   $0xf0106228
f01001df:	68 00 70 00 f0       	push   $0xf0007000
f01001e4:	e8 8c 5e 00 00       	call   f0106075 <memmove>
f01001e9:	83 c4 10             	add    $0x10,%esp
f01001ec:	be 74 00 00 00       	mov    $0x74,%esi
f01001f1:	eb 52                	jmp    f0100245 <i386_init+0x19f>

	// Boot each AP one at a time
	for (c = cpus + 1; c < cpus + ncpu; c++) {
		// Tell mpentry.S what stack to use 
		mpentry_kstack = percpu_kstacks[c - cpus] + KSTKSIZE;
f01001f3:	89 f0                	mov    %esi,%eax
f01001f5:	c1 f8 02             	sar    $0x2,%eax
f01001f8:	8d 14 80             	lea    (%eax,%eax,4),%edx
f01001fb:	8d 0c d0             	lea    (%eax,%edx,8),%ecx
f01001fe:	89 ca                	mov    %ecx,%edx
f0100200:	c1 e2 05             	shl    $0x5,%edx
f0100203:	29 ca                	sub    %ecx,%edx
f0100205:	8d 0c 90             	lea    (%eax,%edx,4),%ecx
f0100208:	89 ca                	mov    %ecx,%edx
f010020a:	c1 e2 0e             	shl    $0xe,%edx
f010020d:	29 ca                	sub    %ecx,%edx
f010020f:	8d 14 90             	lea    (%eax,%edx,4),%edx
f0100212:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0100215:	c1 e0 0f             	shl    $0xf,%eax
f0100218:	05 00 e0 23 f0       	add    $0xf023e000,%eax
f010021d:	a3 14 3f 23 f0       	mov    %eax,0xf0233f14
		// Start the CPU at mpentry_start
		lapic_startap(c->cpu_apicid, PADDR(code));
f0100222:	83 ec 08             	sub    $0x8,%esp
f0100225:	68 00 70 00 00       	push   $0x7000
f010022a:	0f b6 86 00 50 23 f0 	movzbl -0xfdcb000(%esi),%eax
f0100231:	50                   	push   %eax
f0100232:	e8 e8 66 00 00       	call   f010691f <lapic_startap>
f0100237:	83 c4 10             	add    $0x10,%esp
		// Wait for the CPU to finish some basic setup in mp_main()
		while(c->cpu_status != CPU_STARTED)
f010023a:	8b 43 04             	mov    0x4(%ebx),%eax
f010023d:	83 f8 01             	cmp    $0x1,%eax
f0100240:	75 f8                	jne    f010023a <i386_init+0x194>
f0100242:	83 c6 74             	add    $0x74,%esi
f0100245:	8d 9e 00 50 23 f0    	lea    -0xfdcb000(%esi),%ebx
	// Write entry code to unused memory at MPENTRY_PADDR
	code = KADDR(MPENTRY_PADDR);
	memmove(code, mpentry_start, mpentry_end - mpentry_start);

	// Boot each AP one at a time
	for (c = cpus + 1; c < cpus + ncpu; c++) {
f010024b:	8b 15 a0 53 23 f0    	mov    0xf02353a0,%edx
f0100251:	8d 04 12             	lea    (%edx,%edx,1),%eax
f0100254:	01 d0                	add    %edx,%eax
f0100256:	01 c0                	add    %eax,%eax
f0100258:	01 d0                	add    %edx,%eax
f010025a:	8d 04 82             	lea    (%edx,%eax,4),%eax
f010025d:	8d 04 85 00 50 23 f0 	lea    -0xfdcb000(,%eax,4),%eax
f0100264:	39 c3                	cmp    %eax,%ebx
f0100266:	72 8b                	jb     f01001f3 <i386_init+0x14d>
	//in the real mode. Unlike with the bootloader, we have some control over where the AP will 
	//start executing code; we copy the entry code to 0x7000 (MPENTRY_PADDR)
	boot_aps();

	// Start fs.
	ENV_CREATE(fs_fs, ENV_TYPE_FS);
f0100268:	83 ec 08             	sub    $0x8,%esp
f010026b:	6a 01                	push   $0x1
f010026d:	68 dc 1f 1e f0       	push   $0xf01e1fdc
f0100272:	e8 96 33 00 00       	call   f010360d <env_create>

#if defined(TEST)
	// Don't touch -- used by grading script!
	ENV_CREATE(TEST, ENV_TYPE_USER);
f0100277:	83 c4 08             	add    $0x8,%esp
f010027a:	6a 00                	push   $0x0
f010027c:	68 3c ac 22 f0       	push   $0xf022ac3c
f0100281:	e8 87 33 00 00       	call   f010360d <env_create>
	// Touch all you want.
	ENV_CREATE(user_icode, ENV_TYPE_USER);
#endif // TEST*

	// Should not be necessary - drains keyboard because interrupt has given up.
	kbd_intr();
f0100286:	e8 21 04 00 00       	call   f01006ac <kbd_intr>
	// Schedule and run the first user environment!
	sched_yield();
f010028b:	e8 8f 48 00 00       	call   f0104b1f <sched_yield>

f0100290 <mp_main>:
}

// Setup code for APs
void
mp_main(void)
{
f0100290:	55                   	push   %ebp
f0100291:	89 e5                	mov    %esp,%ebp
f0100293:	53                   	push   %ebx
f0100294:	83 ec 04             	sub    $0x4,%esp
	// We are in high EIP now, safe to switch to kern_pgdir 
	lcr3(PADDR(kern_pgdir));
f0100297:	a1 28 44 23 f0       	mov    0xf0234428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f010029c:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01002a1:	77 15                	ja     f01002b8 <mp_main+0x28>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01002a3:	50                   	push   %eax
f01002a4:	68 0c 75 10 f0       	push   $0xf010750c
f01002a9:	68 91 00 00 00       	push   $0x91
f01002ae:	68 66 75 10 f0       	push   $0xf0107566
f01002b3:	e8 94 fd ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f01002b8:	05 00 00 00 10       	add    $0x10000000,%eax
f01002bd:	0f 22 d8             	mov    %eax,%cr3
	cprintf("  AP #%d [apicid %02x] starting\n", cpunum(), thiscpu->cpu_apicid);
f01002c0:	e8 69 64 00 00       	call   f010672e <cpunum>
f01002c5:	6b c0 74             	imul   $0x74,%eax,%eax
f01002c8:	0f b6 98 00 50 23 f0 	movzbl -0xfdcb000(%eax),%ebx
f01002cf:	e8 5a 64 00 00       	call   f010672e <cpunum>
f01002d4:	83 ec 04             	sub    $0x4,%esp
f01002d7:	53                   	push   %ebx
f01002d8:	50                   	push   %eax
f01002d9:	68 30 75 10 f0       	push   $0xf0107530
f01002de:	e8 2e 39 00 00       	call   f0103c11 <cprintf>

	lapic_init();
f01002e3:	e8 a1 64 00 00       	call   f0106789 <lapic_init>
	env_init_percpu();
f01002e8:	e8 ec 30 00 00       	call   f01033d9 <env_init_percpu>
	trap_init_percpu();
f01002ed:	e8 33 39 00 00       	call   f0103c25 <trap_init_percpu>
	xchg(&thiscpu->cpu_status, CPU_STARTED); // tell boot_aps() we're up
f01002f2:	e8 37 64 00 00       	call   f010672e <cpunum>
f01002f7:	6b d0 74             	imul   $0x74,%eax,%edx
f01002fa:	81 c2 00 50 23 f0    	add    $0xf0235000,%edx
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0100300:	b8 01 00 00 00       	mov    $0x1,%eax
f0100305:	f0 87 42 04          	lock xchg %eax,0x4(%edx)
f0100309:	c7 04 24 c0 53 12 f0 	movl   $0xf01253c0,(%esp)
f0100310:	e8 d7 67 00 00       	call   f0106aec <spin_lock>
	// to start running processes on this CPU.  But make sure that
	// only one CPU can enter the scheduler at a time!
	//
	// Your code here:
	lock_kernel();
    sched_yield();
f0100315:	e8 05 48 00 00       	call   f0104b1f <sched_yield>

f010031a <_warn>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
f010031a:	55                   	push   %ebp
f010031b:	89 e5                	mov    %esp,%ebp
f010031d:	53                   	push   %ebx
f010031e:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f0100321:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f0100324:	ff 75 0c             	pushl  0xc(%ebp)
f0100327:	ff 75 08             	pushl  0x8(%ebp)
f010032a:	68 b8 75 10 f0       	push   $0xf01075b8
f010032f:	e8 dd 38 00 00       	call   f0103c11 <cprintf>
	vcprintf(fmt, ap);
f0100334:	83 c4 08             	add    $0x8,%esp
f0100337:	53                   	push   %ebx
f0100338:	ff 75 10             	pushl  0x10(%ebp)
f010033b:	e8 ab 38 00 00       	call   f0103beb <vcprintf>
	cprintf("\n");
f0100340:	c7 04 24 b2 86 10 f0 	movl   $0xf01086b2,(%esp)
f0100347:	e8 c5 38 00 00       	call   f0103c11 <cprintf>
	va_end(ap);
}
f010034c:	83 c4 10             	add    $0x10,%esp
f010034f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100352:	c9                   	leave  
f0100353:	c3                   	ret    

f0100354 <serial_proc_data>:

static bool serial_exists;

static int
serial_proc_data(void)
{
f0100354:	55                   	push   %ebp
f0100355:	89 e5                	mov    %esp,%ebp

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100357:	ba fd 03 00 00       	mov    $0x3fd,%edx
f010035c:	ec                   	in     (%dx),%al
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f010035d:	a8 01                	test   $0x1,%al
f010035f:	74 0b                	je     f010036c <serial_proc_data+0x18>
f0100361:	ba f8 03 00 00       	mov    $0x3f8,%edx
f0100366:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f0100367:	0f b6 c0             	movzbl %al,%eax
f010036a:	eb 05                	jmp    f0100371 <serial_proc_data+0x1d>

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f010036c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return inb(COM1+COM_RX);
}
f0100371:	5d                   	pop    %ebp
f0100372:	c3                   	ret    

f0100373 <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f0100373:	55                   	push   %ebp
f0100374:	89 e5                	mov    %esp,%ebp
f0100376:	53                   	push   %ebx
f0100377:	83 ec 04             	sub    $0x4,%esp
f010037a:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f010037c:	eb 2b                	jmp    f01003a9 <cons_intr+0x36>
		if (c == 0)
f010037e:	85 c0                	test   %eax,%eax
f0100380:	74 27                	je     f01003a9 <cons_intr+0x36>
			continue;
		cons.buf[cons.wpos++] = c;
f0100382:	8b 0d 24 32 23 f0    	mov    0xf0233224,%ecx
f0100388:	8d 51 01             	lea    0x1(%ecx),%edx
f010038b:	89 15 24 32 23 f0    	mov    %edx,0xf0233224
f0100391:	88 81 20 30 23 f0    	mov    %al,-0xfdccfe0(%ecx)
		if (cons.wpos == CONSBUFSIZE)
f0100397:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f010039d:	75 0a                	jne    f01003a9 <cons_intr+0x36>
			cons.wpos = 0;
f010039f:	c7 05 24 32 23 f0 00 	movl   $0x0,0xf0233224
f01003a6:	00 00 00 
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f01003a9:	ff d3                	call   *%ebx
f01003ab:	83 f8 ff             	cmp    $0xffffffff,%eax
f01003ae:	75 ce                	jne    f010037e <cons_intr+0xb>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01003b0:	83 c4 04             	add    $0x4,%esp
f01003b3:	5b                   	pop    %ebx
f01003b4:	5d                   	pop    %ebp
f01003b5:	c3                   	ret    

f01003b6 <kbd_proc_data>:
f01003b6:	ba 64 00 00 00       	mov    $0x64,%edx
f01003bb:	ec                   	in     (%dx),%al
	int c;
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
f01003bc:	a8 01                	test   $0x1,%al
f01003be:	0f 84 ee 00 00 00    	je     f01004b2 <kbd_proc_data+0xfc>
		return -1;
	// Ignore data from mouse.
	if (stat & KBS_TERR)
f01003c4:	a8 20                	test   $0x20,%al
f01003c6:	0f 85 ec 00 00 00    	jne    f01004b8 <kbd_proc_data+0x102>
f01003cc:	ba 60 00 00 00       	mov    $0x60,%edx
f01003d1:	ec                   	in     (%dx),%al
f01003d2:	88 c2                	mov    %al,%dl
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f01003d4:	3c e0                	cmp    $0xe0,%al
f01003d6:	75 0d                	jne    f01003e5 <kbd_proc_data+0x2f>
		// E0 escape character
		shift |= E0ESC;
f01003d8:	83 0d 00 30 23 f0 40 	orl    $0x40,0xf0233000
		return 0;
f01003df:	b8 00 00 00 00       	mov    $0x0,%eax
f01003e4:	c3                   	ret    
	} else if (data & 0x80) {
f01003e5:	84 c0                	test   %al,%al
f01003e7:	79 2e                	jns    f0100417 <kbd_proc_data+0x61>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f01003e9:	8b 0d 00 30 23 f0    	mov    0xf0233000,%ecx
f01003ef:	f6 c1 40             	test   $0x40,%cl
f01003f2:	75 05                	jne    f01003f9 <kbd_proc_data+0x43>
f01003f4:	83 e0 7f             	and    $0x7f,%eax
f01003f7:	88 c2                	mov    %al,%dl
		shift &= ~(shiftcode[data] | E0ESC);
f01003f9:	0f b6 c2             	movzbl %dl,%eax
f01003fc:	8a 80 20 77 10 f0    	mov    -0xfef88e0(%eax),%al
f0100402:	83 c8 40             	or     $0x40,%eax
f0100405:	0f b6 c0             	movzbl %al,%eax
f0100408:	f7 d0                	not    %eax
f010040a:	21 c8                	and    %ecx,%eax
f010040c:	a3 00 30 23 f0       	mov    %eax,0xf0233000
		return 0;
f0100411:	b8 00 00 00 00       	mov    $0x0,%eax
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f0100416:	c3                   	ret    
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f0100417:	55                   	push   %ebp
f0100418:	89 e5                	mov    %esp,%ebp
f010041a:	53                   	push   %ebx
f010041b:	83 ec 04             	sub    $0x4,%esp
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
f010041e:	8b 0d 00 30 23 f0    	mov    0xf0233000,%ecx
f0100424:	f6 c1 40             	test   $0x40,%cl
f0100427:	74 0e                	je     f0100437 <kbd_proc_data+0x81>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f0100429:	83 c8 80             	or     $0xffffff80,%eax
f010042c:	88 c2                	mov    %al,%dl
		shift &= ~E0ESC;
f010042e:	83 e1 bf             	and    $0xffffffbf,%ecx
f0100431:	89 0d 00 30 23 f0    	mov    %ecx,0xf0233000
	}

	shift |= shiftcode[data];
f0100437:	0f b6 c2             	movzbl %dl,%eax
	shift ^= togglecode[data];
f010043a:	0f b6 90 20 77 10 f0 	movzbl -0xfef88e0(%eax),%edx
f0100441:	0b 15 00 30 23 f0    	or     0xf0233000,%edx
f0100447:	0f b6 88 20 76 10 f0 	movzbl -0xfef89e0(%eax),%ecx
f010044e:	31 ca                	xor    %ecx,%edx
f0100450:	89 15 00 30 23 f0    	mov    %edx,0xf0233000

	c = charcode[shift & (CTL | SHIFT)][data];
f0100456:	89 d1                	mov    %edx,%ecx
f0100458:	83 e1 03             	and    $0x3,%ecx
f010045b:	8b 0c 8d 00 76 10 f0 	mov    -0xfef8a00(,%ecx,4),%ecx
f0100462:	8a 04 01             	mov    (%ecx,%eax,1),%al
f0100465:	0f b6 d8             	movzbl %al,%ebx
	if (shift & CAPSLOCK) {
f0100468:	f6 c2 08             	test   $0x8,%dl
f010046b:	74 1a                	je     f0100487 <kbd_proc_data+0xd1>
		if ('a' <= c && c <= 'z')
f010046d:	89 d8                	mov    %ebx,%eax
f010046f:	8d 4b 9f             	lea    -0x61(%ebx),%ecx
f0100472:	83 f9 19             	cmp    $0x19,%ecx
f0100475:	77 05                	ja     f010047c <kbd_proc_data+0xc6>
			c += 'A' - 'a';
f0100477:	83 eb 20             	sub    $0x20,%ebx
f010047a:	eb 0b                	jmp    f0100487 <kbd_proc_data+0xd1>
		else if ('A' <= c && c <= 'Z')
f010047c:	83 e8 41             	sub    $0x41,%eax
f010047f:	83 f8 19             	cmp    $0x19,%eax
f0100482:	77 03                	ja     f0100487 <kbd_proc_data+0xd1>
			c += 'a' - 'A';
f0100484:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f0100487:	f7 d2                	not    %edx
f0100489:	f6 c2 06             	test   $0x6,%dl
f010048c:	75 30                	jne    f01004be <kbd_proc_data+0x108>
f010048e:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f0100494:	75 2c                	jne    f01004c2 <kbd_proc_data+0x10c>
		cprintf("Rebooting!\n");
f0100496:	83 ec 0c             	sub    $0xc,%esp
f0100499:	68 d2 75 10 f0       	push   $0xf01075d2
f010049e:	e8 6e 37 00 00       	call   f0103c11 <cprintf>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01004a3:	ba 92 00 00 00       	mov    $0x92,%edx
f01004a8:	b0 03                	mov    $0x3,%al
f01004aa:	ee                   	out    %al,(%dx)
f01004ab:	83 c4 10             	add    $0x10,%esp
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01004ae:	89 d8                	mov    %ebx,%eax
f01004b0:	eb 12                	jmp    f01004c4 <kbd_proc_data+0x10e>
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
		return -1;
f01004b2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01004b7:	c3                   	ret    
	// Ignore data from mouse.
	if (stat & KBS_TERR)
		return -1;
f01004b8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01004bd:	c3                   	ret    
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01004be:	89 d8                	mov    %ebx,%eax
f01004c0:	eb 02                	jmp    f01004c4 <kbd_proc_data+0x10e>
f01004c2:	89 d8                	mov    %ebx,%eax
}
f01004c4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01004c7:	c9                   	leave  
f01004c8:	c3                   	ret    

f01004c9 <cons_putc>:
}

// output a character to the console
static void
cons_putc(int c)
{
f01004c9:	55                   	push   %ebp
f01004ca:	89 e5                	mov    %esp,%ebp
f01004cc:	57                   	push   %edi
f01004cd:	56                   	push   %esi
f01004ce:	53                   	push   %ebx
f01004cf:	83 ec 1c             	sub    $0x1c,%esp
f01004d2:	89 c7                	mov    %eax,%edi
f01004d4:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01004d9:	be fd 03 00 00       	mov    $0x3fd,%esi
f01004de:	b9 84 00 00 00       	mov    $0x84,%ecx
f01004e3:	eb 06                	jmp    f01004eb <cons_putc+0x22>
f01004e5:	89 ca                	mov    %ecx,%edx
f01004e7:	ec                   	in     (%dx),%al
f01004e8:	ec                   	in     (%dx),%al
f01004e9:	ec                   	in     (%dx),%al
f01004ea:	ec                   	in     (%dx),%al
f01004eb:	89 f2                	mov    %esi,%edx
f01004ed:	ec                   	in     (%dx),%al
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f01004ee:	a8 20                	test   $0x20,%al
f01004f0:	75 03                	jne    f01004f5 <cons_putc+0x2c>
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
f01004f2:	4b                   	dec    %ebx
f01004f3:	75 f0                	jne    f01004e5 <cons_putc+0x1c>
f01004f5:	89 f8                	mov    %edi,%eax
f01004f7:	88 45 e7             	mov    %al,-0x19(%ebp)
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01004fa:	ba f8 03 00 00       	mov    $0x3f8,%edx
f01004ff:	ee                   	out    %al,(%dx)
f0100500:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100505:	be 79 03 00 00       	mov    $0x379,%esi
f010050a:	b9 84 00 00 00       	mov    $0x84,%ecx
f010050f:	eb 06                	jmp    f0100517 <cons_putc+0x4e>
f0100511:	89 ca                	mov    %ecx,%edx
f0100513:	ec                   	in     (%dx),%al
f0100514:	ec                   	in     (%dx),%al
f0100515:	ec                   	in     (%dx),%al
f0100516:	ec                   	in     (%dx),%al
f0100517:	89 f2                	mov    %esi,%edx
f0100519:	ec                   	in     (%dx),%al
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f010051a:	84 c0                	test   %al,%al
f010051c:	78 03                	js     f0100521 <cons_putc+0x58>
f010051e:	4b                   	dec    %ebx
f010051f:	75 f0                	jne    f0100511 <cons_putc+0x48>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100521:	ba 78 03 00 00       	mov    $0x378,%edx
f0100526:	8a 45 e7             	mov    -0x19(%ebp),%al
f0100529:	ee                   	out    %al,(%dx)
f010052a:	ba 7a 03 00 00       	mov    $0x37a,%edx
f010052f:	b0 0d                	mov    $0xd,%al
f0100531:	ee                   	out    %al,(%dx)
f0100532:	b0 08                	mov    $0x8,%al
f0100534:	ee                   	out    %al,(%dx)

static void
cga_putc(int c)
{
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f0100535:	f7 c7 00 ff ff ff    	test   $0xffffff00,%edi
f010053b:	75 06                	jne    f0100543 <cons_putc+0x7a>
		c |= 0x0500;
f010053d:	81 cf 00 05 00 00    	or     $0x500,%edi

	switch (c & 0xff) {
f0100543:	89 f8                	mov    %edi,%eax
f0100545:	0f b6 c0             	movzbl %al,%eax
f0100548:	83 f8 09             	cmp    $0x9,%eax
f010054b:	74 75                	je     f01005c2 <cons_putc+0xf9>
f010054d:	83 f8 09             	cmp    $0x9,%eax
f0100550:	7f 0a                	jg     f010055c <cons_putc+0x93>
f0100552:	83 f8 08             	cmp    $0x8,%eax
f0100555:	74 14                	je     f010056b <cons_putc+0xa2>
f0100557:	e9 9a 00 00 00       	jmp    f01005f6 <cons_putc+0x12d>
f010055c:	83 f8 0a             	cmp    $0xa,%eax
f010055f:	74 38                	je     f0100599 <cons_putc+0xd0>
f0100561:	83 f8 0d             	cmp    $0xd,%eax
f0100564:	74 3b                	je     f01005a1 <cons_putc+0xd8>
f0100566:	e9 8b 00 00 00       	jmp    f01005f6 <cons_putc+0x12d>
	case '\b':
		if (crt_pos > 0) {
f010056b:	66 a1 28 32 23 f0    	mov    0xf0233228,%ax
f0100571:	66 85 c0             	test   %ax,%ax
f0100574:	0f 84 e7 00 00 00    	je     f0100661 <cons_putc+0x198>
			crt_pos--;
f010057a:	48                   	dec    %eax
f010057b:	66 a3 28 32 23 f0    	mov    %ax,0xf0233228
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f0100581:	0f b7 c0             	movzwl %ax,%eax
f0100584:	81 e7 00 ff ff ff    	and    $0xffffff00,%edi
f010058a:	83 cf 20             	or     $0x20,%edi
f010058d:	8b 15 2c 32 23 f0    	mov    0xf023322c,%edx
f0100593:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
f0100597:	eb 7a                	jmp    f0100613 <cons_putc+0x14a>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f0100599:	66 83 05 28 32 23 f0 	addw   $0x50,0xf0233228
f01005a0:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f01005a1:	66 8b 0d 28 32 23 f0 	mov    0xf0233228,%cx
f01005a8:	bb 50 00 00 00       	mov    $0x50,%ebx
f01005ad:	89 c8                	mov    %ecx,%eax
f01005af:	ba 00 00 00 00       	mov    $0x0,%edx
f01005b4:	66 f7 f3             	div    %bx
f01005b7:	29 d1                	sub    %edx,%ecx
f01005b9:	66 89 0d 28 32 23 f0 	mov    %cx,0xf0233228
f01005c0:	eb 51                	jmp    f0100613 <cons_putc+0x14a>
		break;
	case '\t':
		cons_putc(' ');
f01005c2:	b8 20 00 00 00       	mov    $0x20,%eax
f01005c7:	e8 fd fe ff ff       	call   f01004c9 <cons_putc>
		cons_putc(' ');
f01005cc:	b8 20 00 00 00       	mov    $0x20,%eax
f01005d1:	e8 f3 fe ff ff       	call   f01004c9 <cons_putc>
		cons_putc(' ');
f01005d6:	b8 20 00 00 00       	mov    $0x20,%eax
f01005db:	e8 e9 fe ff ff       	call   f01004c9 <cons_putc>
		cons_putc(' ');
f01005e0:	b8 20 00 00 00       	mov    $0x20,%eax
f01005e5:	e8 df fe ff ff       	call   f01004c9 <cons_putc>
		cons_putc(' ');
f01005ea:	b8 20 00 00 00       	mov    $0x20,%eax
f01005ef:	e8 d5 fe ff ff       	call   f01004c9 <cons_putc>
f01005f4:	eb 1d                	jmp    f0100613 <cons_putc+0x14a>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f01005f6:	66 a1 28 32 23 f0    	mov    0xf0233228,%ax
f01005fc:	8d 50 01             	lea    0x1(%eax),%edx
f01005ff:	66 89 15 28 32 23 f0 	mov    %dx,0xf0233228
f0100606:	0f b7 c0             	movzwl %ax,%eax
f0100609:	8b 15 2c 32 23 f0    	mov    0xf023322c,%edx
f010060f:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f0100613:	66 81 3d 28 32 23 f0 	cmpw   $0x7cf,0xf0233228
f010061a:	cf 07 
f010061c:	76 43                	jbe    f0100661 <cons_putc+0x198>
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f010061e:	a1 2c 32 23 f0       	mov    0xf023322c,%eax
f0100623:	83 ec 04             	sub    $0x4,%esp
f0100626:	68 00 0f 00 00       	push   $0xf00
f010062b:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f0100631:	52                   	push   %edx
f0100632:	50                   	push   %eax
f0100633:	e8 3d 5a 00 00       	call   f0106075 <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f0100638:	8b 15 2c 32 23 f0    	mov    0xf023322c,%edx
f010063e:	8d 82 00 0f 00 00    	lea    0xf00(%edx),%eax
f0100644:	81 c2 a0 0f 00 00    	add    $0xfa0,%edx
f010064a:	83 c4 10             	add    $0x10,%esp
f010064d:	66 c7 00 20 07       	movw   $0x720,(%eax)
f0100652:	83 c0 02             	add    $0x2,%eax
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f0100655:	39 d0                	cmp    %edx,%eax
f0100657:	75 f4                	jne    f010064d <cons_putc+0x184>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f0100659:	66 83 2d 28 32 23 f0 	subw   $0x50,0xf0233228
f0100660:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f0100661:	8b 0d 30 32 23 f0    	mov    0xf0233230,%ecx
f0100667:	b0 0e                	mov    $0xe,%al
f0100669:	89 ca                	mov    %ecx,%edx
f010066b:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f010066c:	8d 59 01             	lea    0x1(%ecx),%ebx
f010066f:	66 a1 28 32 23 f0    	mov    0xf0233228,%ax
f0100675:	66 c1 e8 08          	shr    $0x8,%ax
f0100679:	89 da                	mov    %ebx,%edx
f010067b:	ee                   	out    %al,(%dx)
f010067c:	b0 0f                	mov    $0xf,%al
f010067e:	89 ca                	mov    %ecx,%edx
f0100680:	ee                   	out    %al,(%dx)
f0100681:	a0 28 32 23 f0       	mov    0xf0233228,%al
f0100686:	89 da                	mov    %ebx,%edx
f0100688:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f0100689:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010068c:	5b                   	pop    %ebx
f010068d:	5e                   	pop    %esi
f010068e:	5f                   	pop    %edi
f010068f:	5d                   	pop    %ebp
f0100690:	c3                   	ret    

f0100691 <serial_intr>:
}

void
serial_intr(void)
{
	if (serial_exists)
f0100691:	80 3d 34 32 23 f0 00 	cmpb   $0x0,0xf0233234
f0100698:	74 11                	je     f01006ab <serial_intr+0x1a>
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f010069a:	55                   	push   %ebp
f010069b:	89 e5                	mov    %esp,%ebp
f010069d:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
		cons_intr(serial_proc_data);
f01006a0:	b8 54 03 10 f0       	mov    $0xf0100354,%eax
f01006a5:	e8 c9 fc ff ff       	call   f0100373 <cons_intr>
}
f01006aa:	c9                   	leave  
f01006ab:	c3                   	ret    

f01006ac <kbd_intr>:
	return c;
}

void
kbd_intr(void)
{
f01006ac:	55                   	push   %ebp
f01006ad:	89 e5                	mov    %esp,%ebp
f01006af:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f01006b2:	b8 b6 03 10 f0       	mov    $0xf01003b6,%eax
f01006b7:	e8 b7 fc ff ff       	call   f0100373 <cons_intr>
}
f01006bc:	c9                   	leave  
f01006bd:	c3                   	ret    

f01006be <cons_getc>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f01006be:	55                   	push   %ebp
f01006bf:	89 e5                	mov    %esp,%ebp
f01006c1:	83 ec 08             	sub    $0x8,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f01006c4:	e8 c8 ff ff ff       	call   f0100691 <serial_intr>
	kbd_intr();
f01006c9:	e8 de ff ff ff       	call   f01006ac <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f01006ce:	a1 20 32 23 f0       	mov    0xf0233220,%eax
f01006d3:	3b 05 24 32 23 f0    	cmp    0xf0233224,%eax
f01006d9:	74 24                	je     f01006ff <cons_getc+0x41>
		c = cons.buf[cons.rpos++];
f01006db:	8d 50 01             	lea    0x1(%eax),%edx
f01006de:	89 15 20 32 23 f0    	mov    %edx,0xf0233220
f01006e4:	0f b6 80 20 30 23 f0 	movzbl -0xfdccfe0(%eax),%eax
		if (cons.rpos == CONSBUFSIZE)
f01006eb:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f01006f1:	75 11                	jne    f0100704 <cons_getc+0x46>
			cons.rpos = 0;
f01006f3:	c7 05 20 32 23 f0 00 	movl   $0x0,0xf0233220
f01006fa:	00 00 00 
f01006fd:	eb 05                	jmp    f0100704 <cons_getc+0x46>
		return c;
	}
	return 0;
f01006ff:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100704:	c9                   	leave  
f0100705:	c3                   	ret    

f0100706 <cons_init>:
}

// initialize the console devices
void
cons_init(void)
{
f0100706:	55                   	push   %ebp
f0100707:	89 e5                	mov    %esp,%ebp
f0100709:	56                   	push   %esi
f010070a:	53                   	push   %ebx
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f010070b:	66 8b 15 00 80 0b f0 	mov    0xf00b8000,%dx
	*cp = (uint16_t) 0xA55A;
f0100712:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f0100719:	5a a5 
	if (*cp != 0xA55A) {
f010071b:	66 a1 00 80 0b f0    	mov    0xf00b8000,%ax
f0100721:	66 3d 5a a5          	cmp    $0xa55a,%ax
f0100725:	74 11                	je     f0100738 <cons_init+0x32>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f0100727:	c7 05 30 32 23 f0 b4 	movl   $0x3b4,0xf0233230
f010072e:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f0100731:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f0100736:	eb 16                	jmp    f010074e <cons_init+0x48>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f0100738:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f010073f:	c7 05 30 32 23 f0 d4 	movl   $0x3d4,0xf0233230
f0100746:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f0100749:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
f010074e:	b0 0e                	mov    $0xe,%al
f0100750:	8b 15 30 32 23 f0    	mov    0xf0233230,%edx
f0100756:	ee                   	out    %al,(%dx)
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
f0100757:	8d 5a 01             	lea    0x1(%edx),%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010075a:	89 da                	mov    %ebx,%edx
f010075c:	ec                   	in     (%dx),%al
f010075d:	0f b6 c8             	movzbl %al,%ecx
f0100760:	c1 e1 08             	shl    $0x8,%ecx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100763:	b0 0f                	mov    $0xf,%al
f0100765:	8b 15 30 32 23 f0    	mov    0xf0233230,%edx
f010076b:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010076c:	89 da                	mov    %ebx,%edx
f010076e:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f010076f:	89 35 2c 32 23 f0    	mov    %esi,0xf023322c
	crt_pos = pos;
f0100775:	0f b6 c0             	movzbl %al,%eax
f0100778:	09 c8                	or     %ecx,%eax
f010077a:	66 a3 28 32 23 f0    	mov    %ax,0xf0233228

static void
kbd_init(void)
{
	// Drain the kbd buffer so that QEMU generates interrupts.
	kbd_intr();
f0100780:	e8 27 ff ff ff       	call   f01006ac <kbd_intr>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100785:	be fa 03 00 00       	mov    $0x3fa,%esi
f010078a:	b0 00                	mov    $0x0,%al
f010078c:	89 f2                	mov    %esi,%edx
f010078e:	ee                   	out    %al,(%dx)
f010078f:	ba fb 03 00 00       	mov    $0x3fb,%edx
f0100794:	b0 80                	mov    $0x80,%al
f0100796:	ee                   	out    %al,(%dx)
f0100797:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f010079c:	b0 0c                	mov    $0xc,%al
f010079e:	89 da                	mov    %ebx,%edx
f01007a0:	ee                   	out    %al,(%dx)
f01007a1:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01007a6:	b0 00                	mov    $0x0,%al
f01007a8:	ee                   	out    %al,(%dx)
f01007a9:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01007ae:	b0 03                	mov    $0x3,%al
f01007b0:	ee                   	out    %al,(%dx)
f01007b1:	ba fc 03 00 00       	mov    $0x3fc,%edx
f01007b6:	b0 00                	mov    $0x0,%al
f01007b8:	ee                   	out    %al,(%dx)
f01007b9:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01007be:	b0 01                	mov    $0x1,%al
f01007c0:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01007c1:	ba fd 03 00 00       	mov    $0x3fd,%edx
f01007c6:	ec                   	in     (%dx),%al
f01007c7:	88 c1                	mov    %al,%cl
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f01007c9:	3c ff                	cmp    $0xff,%al
f01007cb:	0f 95 05 34 32 23 f0 	setne  0xf0233234
f01007d2:	89 f2                	mov    %esi,%edx
f01007d4:	ec                   	in     (%dx),%al
f01007d5:	89 da                	mov    %ebx,%edx
f01007d7:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f01007d8:	80 f9 ff             	cmp    $0xff,%cl
f01007db:	75 10                	jne    f01007ed <cons_init+0xe7>
		cprintf("Serial port does not exist!\n");
f01007dd:	83 ec 0c             	sub    $0xc,%esp
f01007e0:	68 de 75 10 f0       	push   $0xf01075de
f01007e5:	e8 27 34 00 00       	call   f0103c11 <cprintf>
f01007ea:	83 c4 10             	add    $0x10,%esp
}
f01007ed:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01007f0:	5b                   	pop    %ebx
f01007f1:	5e                   	pop    %esi
f01007f2:	5d                   	pop    %ebp
f01007f3:	c3                   	ret    

f01007f4 <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f01007f4:	55                   	push   %ebp
f01007f5:	89 e5                	mov    %esp,%ebp
f01007f7:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f01007fa:	8b 45 08             	mov    0x8(%ebp),%eax
f01007fd:	e8 c7 fc ff ff       	call   f01004c9 <cons_putc>
}
f0100802:	c9                   	leave  
f0100803:	c3                   	ret    

f0100804 <getchar>:

int
getchar(void)
{
f0100804:	55                   	push   %ebp
f0100805:	89 e5                	mov    %esp,%ebp
f0100807:	83 ec 08             	sub    $0x8,%esp
	int c;

	while ((c = cons_getc()) == 0)
f010080a:	e8 af fe ff ff       	call   f01006be <cons_getc>
f010080f:	85 c0                	test   %eax,%eax
f0100811:	74 f7                	je     f010080a <getchar+0x6>
		/* do nothing */;
	return c;
}
f0100813:	c9                   	leave  
f0100814:	c3                   	ret    

f0100815 <iscons>:

int
iscons(int fdnum)
{
f0100815:	55                   	push   %ebp
f0100816:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
}
f0100818:	b8 01 00 00 00       	mov    $0x1,%eax
f010081d:	5d                   	pop    %ebp
f010081e:	c3                   	ret    

f010081f <mon_help>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f010081f:	55                   	push   %ebp
f0100820:	89 e5                	mov    %esp,%ebp
f0100822:	83 ec 0c             	sub    $0xc,%esp
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f0100825:	68 20 78 10 f0       	push   $0xf0107820
f010082a:	68 3e 78 10 f0       	push   $0xf010783e
f010082f:	68 43 78 10 f0       	push   $0xf0107843
f0100834:	e8 d8 33 00 00       	call   f0103c11 <cprintf>
f0100839:	83 c4 0c             	add    $0xc,%esp
f010083c:	68 04 79 10 f0       	push   $0xf0107904
f0100841:	68 4c 78 10 f0       	push   $0xf010784c
f0100846:	68 43 78 10 f0       	push   $0xf0107843
f010084b:	e8 c1 33 00 00       	call   f0103c11 <cprintf>
f0100850:	83 c4 0c             	add    $0xc,%esp
f0100853:	68 b3 86 10 f0       	push   $0xf01086b3
f0100858:	68 55 78 10 f0       	push   $0xf0107855
f010085d:	68 43 78 10 f0       	push   $0xf0107843
f0100862:	e8 aa 33 00 00       	call   f0103c11 <cprintf>
	return 0;
}
f0100867:	b8 00 00 00 00       	mov    $0x0,%eax
f010086c:	c9                   	leave  
f010086d:	c3                   	ret    

f010086e <mon_kerninfo>:

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f010086e:	55                   	push   %ebp
f010086f:	89 e5                	mov    %esp,%ebp
f0100871:	83 ec 14             	sub    $0x14,%esp
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f0100874:	68 5f 78 10 f0       	push   $0xf010785f
f0100879:	e8 93 33 00 00       	call   f0103c11 <cprintf>
	cprintf("  _start                  %08x (phys)\n", _start);
f010087e:	83 c4 08             	add    $0x8,%esp
f0100881:	68 0c 00 10 00       	push   $0x10000c
f0100886:	68 2c 79 10 f0       	push   $0xf010792c
f010088b:	e8 81 33 00 00       	call   f0103c11 <cprintf>
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
f0100890:	83 c4 0c             	add    $0xc,%esp
f0100893:	68 0c 00 10 00       	push   $0x10000c
f0100898:	68 0c 00 10 f0       	push   $0xf010000c
f010089d:	68 54 79 10 f0       	push   $0xf0107954
f01008a2:	e8 6a 33 00 00       	call   f0103c11 <cprintf>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f01008a7:	83 c4 0c             	add    $0xc,%esp
f01008aa:	68 99 74 10 00       	push   $0x107499
f01008af:	68 99 74 10 f0       	push   $0xf0107499
f01008b4:	68 78 79 10 f0       	push   $0xf0107978
f01008b9:	e8 53 33 00 00       	call   f0103c11 <cprintf>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f01008be:	83 c4 0c             	add    $0xc,%esp
f01008c1:	68 64 27 23 00       	push   $0x232764
f01008c6:	68 64 27 23 f0       	push   $0xf0232764
f01008cb:	68 9c 79 10 f0       	push   $0xf010799c
f01008d0:	e8 3c 33 00 00       	call   f0103c11 <cprintf>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f01008d5:	83 c4 0c             	add    $0xc,%esp
f01008d8:	68 0c 60 27 00       	push   $0x27600c
f01008dd:	68 0c 60 27 f0       	push   $0xf027600c
f01008e2:	68 c0 79 10 f0       	push   $0xf01079c0
f01008e7:	e8 25 33 00 00       	call   f0103c11 <cprintf>
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
f01008ec:	b8 0b 64 27 f0       	mov    $0xf027640b,%eax
f01008f1:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
f01008f6:	83 c4 08             	add    $0x8,%esp
f01008f9:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f01008fe:	89 c2                	mov    %eax,%edx
f0100900:	85 c0                	test   %eax,%eax
f0100902:	79 06                	jns    f010090a <mon_kerninfo+0x9c>
f0100904:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f010090a:	c1 fa 0a             	sar    $0xa,%edx
f010090d:	52                   	push   %edx
f010090e:	68 e4 79 10 f0       	push   $0xf01079e4
f0100913:	e8 f9 32 00 00       	call   f0103c11 <cprintf>
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
f0100918:	b8 00 00 00 00       	mov    $0x0,%eax
f010091d:	c9                   	leave  
f010091e:	c3                   	ret    

f010091f <print_stack_Info>:
ebp f010ff78  eip f01008ae  args 00000001 f010ff8c 00000000 f0110580 00000000
       kern/monitor.c:143: monitor+106
       (filename, line#, function name+offset)
*/
void print_stack_Info(uint32_t esp, uint32_t* return_Addr)
{
f010091f:	55                   	push   %ebp
f0100920:	89 e5                	mov    %esp,%ebp
f0100922:	57                   	push   %edi
f0100923:	56                   	push   %esi
f0100924:	53                   	push   %ebx
f0100925:	83 ec 30             	sub    $0x30,%esp
f0100928:	8b 75 0c             	mov    0xc(%ebp),%esi

	//if(0==debuginfo_eip((uintptr_t)(*return_Addr), info)){

	//}

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
f010092b:	ff 36                	pushl  (%esi)
f010092d:	ff 75 08             	pushl  0x8(%ebp)
f0100930:	68 78 78 10 f0       	push   $0xf0107878
f0100935:	e8 d7 32 00 00       	call   f0103c11 <cprintf>
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
f010093a:	c7 04 24 8b 78 10 f0 	movl   $0xf010788b,(%esp)
f0100941:	e8 cb 32 00 00       	call   f0103c11 <cprintf>
f0100946:	8d 5e 04             	lea    0x4(%esi),%ebx
f0100949:	8d 7e 18             	lea    0x18(%esi),%edi
f010094c:	83 c4 10             	add    $0x10,%esp
	for(int i=1; i<6; i++){
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
f010094f:	83 ec 08             	sub    $0x8,%esp
f0100952:	ff 33                	pushl  (%ebx)
f0100954:	68 85 78 10 f0       	push   $0xf0107885
f0100959:	e8 b3 32 00 00       	call   f0103c11 <cprintf>
f010095e:	83 c3 04             	add    $0x4,%ebx

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
	for(int i=1; i<6; i++){
f0100961:	83 c4 10             	add    $0x10,%esp
f0100964:	39 fb                	cmp    %edi,%ebx
f0100966:	75 e7                	jne    f010094f <print_stack_Info+0x30>
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
	}
	cprintf("\n");
f0100968:	83 ec 0c             	sub    $0xc,%esp
f010096b:	68 b2 86 10 f0       	push   $0xf01086b2
f0100970:	e8 9c 32 00 00       	call   f0103c11 <cprintf>

	//print more readable info
	//struct Eipdebuginfo* info_Ptr;
	debuginfo_eip((uintptr_t)(*return_Addr), info_Ptr); //get the info anyway
f0100975:	83 c4 08             	add    $0x8,%esp
f0100978:	8d 45 d0             	lea    -0x30(%ebp),%eax
f010097b:	50                   	push   %eax
f010097c:	ff 36                	pushl  (%esi)
f010097e:	e8 ea 4a 00 00       	call   f010546d <debuginfo_eip>
	//print file name
	cprintf("%s", (info_Ptr->eip_file) );
f0100983:	83 c4 08             	add    $0x8,%esp
f0100986:	ff 75 d0             	pushl  -0x30(%ebp)
f0100989:	68 63 75 10 f0       	push   $0xf0107563
f010098e:	e8 7e 32 00 00       	call   f0103c11 <cprintf>
	cprintf(":");
f0100993:	c7 04 24 92 78 10 f0 	movl   $0xf0107892,(%esp)
f010099a:	e8 72 32 00 00       	call   f0103c11 <cprintf>

	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
f010099f:	83 c4 08             	add    $0x8,%esp
f01009a2:	ff 75 d4             	pushl  -0x2c(%ebp)
f01009a5:	68 3e 96 10 f0       	push   $0xf010963e
f01009aa:	e8 62 32 00 00       	call   f0103c11 <cprintf>
	cprintf(": ");
f01009af:	c7 04 24 cf 75 10 f0 	movl   $0xf01075cf,(%esp)
f01009b6:	e8 56 32 00 00       	call   f0103c11 <cprintf>
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01009bb:	83 c4 10             	add    $0x10,%esp
f01009be:	bb 00 00 00 00       	mov    $0x0,%ebx
f01009c3:	eb 19                	jmp    f01009de <print_stack_Info+0xbf>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
f01009c5:	83 ec 08             	sub    $0x8,%esp
f01009c8:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01009cb:	0f be 04 18          	movsbl (%eax,%ebx,1),%eax
f01009cf:	50                   	push   %eax
f01009d0:	68 94 78 10 f0       	push   $0xf0107894
f01009d5:	e8 37 32 00 00       	call   f0103c11 <cprintf>
	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
	cprintf(": ");
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01009da:	43                   	inc    %ebx
f01009db:	83 c4 10             	add    $0x10,%esp
f01009de:	3b 5d dc             	cmp    -0x24(%ebp),%ebx
f01009e1:	7c e2                	jl     f01009c5 <print_stack_Info+0xa6>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
	}
	//print offset number
	cprintf("+");
f01009e3:	83 ec 0c             	sub    $0xc,%esp
f01009e6:	68 97 78 10 f0       	push   $0xf0107897
f01009eb:	e8 21 32 00 00       	call   f0103c11 <cprintf>
	cprintf("%d", ( (int)(*return_Addr) - (int)(info_Ptr->eip_fn_addr) ) );
f01009f0:	83 c4 08             	add    $0x8,%esp
f01009f3:	8b 06                	mov    (%esi),%eax
f01009f5:	2b 45 e0             	sub    -0x20(%ebp),%eax
f01009f8:	50                   	push   %eax
f01009f9:	68 3e 96 10 f0       	push   $0xf010963e
f01009fe:	e8 0e 32 00 00       	call   f0103c11 <cprintf>
	cprintf("\n");
f0100a03:	c7 04 24 b2 86 10 f0 	movl   $0xf01086b2,(%esp)
f0100a0a:	e8 02 32 00 00       	call   f0103c11 <cprintf>
}
f0100a0f:	83 c4 10             	add    $0x10,%esp
f0100a12:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100a15:	5b                   	pop    %ebx
f0100a16:	5e                   	pop    %esi
f0100a17:	5f                   	pop    %edi
f0100a18:	5d                   	pop    %ebp
f0100a19:	c3                   	ret    

f0100a1a <mon_backtrace>:


*/
int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
f0100a1a:	55                   	push   %ebp
f0100a1b:	89 e5                	mov    %esp,%ebp
f0100a1d:	56                   	push   %esi
f0100a1e:	53                   	push   %ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f0100a1f:	89 ee                	mov    %ebp,%esi
	//function prologue
	//f0100751:	55                   	push   %ebp
	//f0100752:	89 e5                	mov    %esp,%ebp
	// Your code here. -.-
	uint32_t esp = read_ebp(); //since mov %esp,%ebp
	uint32_t ebp = *( (uint32_t*)esp );
f0100a21:	8b 1e                	mov    (%esi),%ebx
	uint32_t* return_Addr = (uint32_t*)(esp+4);
	//start print information
	cprintf("Stack backtrace:\n");
f0100a23:	83 ec 0c             	sub    $0xc,%esp
f0100a26:	68 99 78 10 f0       	push   $0xf0107899
f0100a2b:	e8 e1 31 00 00       	call   f0103c11 <cprintf>
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
f0100a30:	83 c4 08             	add    $0x8,%esp
f0100a33:	8d 46 04             	lea    0x4(%esi),%eax
f0100a36:	50                   	push   %eax
f0100a37:	56                   	push   %esi
f0100a38:	e8 e2 fe ff ff       	call   f010091f <print_stack_Info>
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100a3d:	83 c4 10             	add    $0x10,%esp
f0100a40:	eb 12                	jmp    f0100a54 <mon_backtrace+0x3a>
		return_Addr = ((uint32_t*)ebp+1);
		//ebp = *((uint32_t*)ebp); //trace back to the last ebp
		//cprintf("ebp %08x eip %08x\n", ebp, return_Addr);  //the current ebp == esp
		print_stack_Info(ebp, return_Addr);
f0100a42:	83 ec 08             	sub    $0x8,%esp
f0100a45:	8d 43 04             	lea    0x4(%ebx),%eax
f0100a48:	50                   	push   %eax
f0100a49:	53                   	push   %ebx
f0100a4a:	e8 d0 fe ff ff       	call   f010091f <print_stack_Info>
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
f0100a4f:	8b 1b                	mov    (%ebx),%ebx
f0100a51:	83 c4 10             	add    $0x10,%esp
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100a54:	85 db                	test   %ebx,%ebx
f0100a56:	75 ea                	jne    f0100a42 <mon_backtrace+0x28>
		print_stack_Info(ebp, return_Addr);
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
	}

	//Not print the 1st stack address since it is the kernel allocated the beginning of the stack
	cprintf("Stoppp Stack backtrace:\n ");
f0100a58:	83 ec 0c             	sub    $0xc,%esp
f0100a5b:	68 ab 78 10 f0       	push   $0xf01078ab
f0100a60:	e8 ac 31 00 00       	call   f0103c11 <cprintf>

	//cprintf("argc is %08x\n", argc);
	return 0;
}
f0100a65:	b8 00 00 00 00       	mov    $0x0,%eax
f0100a6a:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0100a6d:	5b                   	pop    %ebx
f0100a6e:	5e                   	pop    %esi
f0100a6f:	5d                   	pop    %ebp
f0100a70:	c3                   	ret    

f0100a71 <monitor>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f0100a71:	55                   	push   %ebp
f0100a72:	89 e5                	mov    %esp,%ebp
f0100a74:	57                   	push   %edi
f0100a75:	56                   	push   %esi
f0100a76:	53                   	push   %ebx
f0100a77:	83 ec 58             	sub    $0x58,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f0100a7a:	68 10 7a 10 f0       	push   $0xf0107a10
f0100a7f:	e8 8d 31 00 00       	call   f0103c11 <cprintf>
	cprintf("Type 'help' for a list of commands.\n");
f0100a84:	c7 04 24 34 7a 10 f0 	movl   $0xf0107a34,(%esp)
f0100a8b:	e8 81 31 00 00       	call   f0103c11 <cprintf>

	if (tf != NULL)
f0100a90:	83 c4 10             	add    $0x10,%esp
f0100a93:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0100a97:	74 0e                	je     f0100aa7 <monitor+0x36>
		print_trapframe(tf);
f0100a99:	83 ec 0c             	sub    $0xc,%esp
f0100a9c:	ff 75 08             	pushl  0x8(%ebp)
f0100a9f:	e8 bd 38 00 00       	call   f0104361 <print_trapframe>
f0100aa4:	83 c4 10             	add    $0x10,%esp

	while (1) {
		buf = readline("K> ");
f0100aa7:	83 ec 0c             	sub    $0xc,%esp
f0100aaa:	68 c5 78 10 f0       	push   $0xf01078c5
f0100aaf:	e8 13 53 00 00       	call   f0105dc7 <readline>
f0100ab4:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f0100ab6:	83 c4 10             	add    $0x10,%esp
f0100ab9:	85 c0                	test   %eax,%eax
f0100abb:	74 ea                	je     f0100aa7 <monitor+0x36>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f0100abd:	c7 45 a8 00 00 00 00 	movl   $0x0,-0x58(%ebp)
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f0100ac4:	be 00 00 00 00       	mov    $0x0,%esi
f0100ac9:	eb 0a                	jmp    f0100ad5 <monitor+0x64>
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f0100acb:	c6 03 00             	movb   $0x0,(%ebx)
f0100ace:	89 f7                	mov    %esi,%edi
f0100ad0:	8d 5b 01             	lea    0x1(%ebx),%ebx
f0100ad3:	89 fe                	mov    %edi,%esi
	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f0100ad5:	8a 03                	mov    (%ebx),%al
f0100ad7:	84 c0                	test   %al,%al
f0100ad9:	74 60                	je     f0100b3b <monitor+0xca>
f0100adb:	83 ec 08             	sub    $0x8,%esp
f0100ade:	0f be c0             	movsbl %al,%eax
f0100ae1:	50                   	push   %eax
f0100ae2:	68 c9 78 10 f0       	push   $0xf01078c9
f0100ae7:	e8 07 55 00 00       	call   f0105ff3 <strchr>
f0100aec:	83 c4 10             	add    $0x10,%esp
f0100aef:	85 c0                	test   %eax,%eax
f0100af1:	75 d8                	jne    f0100acb <monitor+0x5a>
			*buf++ = 0;
		if (*buf == 0)
f0100af3:	80 3b 00             	cmpb   $0x0,(%ebx)
f0100af6:	74 43                	je     f0100b3b <monitor+0xca>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f0100af8:	83 fe 0f             	cmp    $0xf,%esi
f0100afb:	75 14                	jne    f0100b11 <monitor+0xa0>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f0100afd:	83 ec 08             	sub    $0x8,%esp
f0100b00:	6a 10                	push   $0x10
f0100b02:	68 ce 78 10 f0       	push   $0xf01078ce
f0100b07:	e8 05 31 00 00       	call   f0103c11 <cprintf>
f0100b0c:	83 c4 10             	add    $0x10,%esp
f0100b0f:	eb 96                	jmp    f0100aa7 <monitor+0x36>
			return 0;
		}
		argv[argc++] = buf;
f0100b11:	8d 7e 01             	lea    0x1(%esi),%edi
f0100b14:	89 5c b5 a8          	mov    %ebx,-0x58(%ebp,%esi,4)
f0100b18:	eb 01                	jmp    f0100b1b <monitor+0xaa>
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f0100b1a:	43                   	inc    %ebx
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
f0100b1b:	8a 03                	mov    (%ebx),%al
f0100b1d:	84 c0                	test   %al,%al
f0100b1f:	74 b2                	je     f0100ad3 <monitor+0x62>
f0100b21:	83 ec 08             	sub    $0x8,%esp
f0100b24:	0f be c0             	movsbl %al,%eax
f0100b27:	50                   	push   %eax
f0100b28:	68 c9 78 10 f0       	push   $0xf01078c9
f0100b2d:	e8 c1 54 00 00       	call   f0105ff3 <strchr>
f0100b32:	83 c4 10             	add    $0x10,%esp
f0100b35:	85 c0                	test   %eax,%eax
f0100b37:	74 e1                	je     f0100b1a <monitor+0xa9>
f0100b39:	eb 98                	jmp    f0100ad3 <monitor+0x62>
			buf++;
	}
	argv[argc] = 0;
f0100b3b:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
f0100b42:	00 

	// Lookup and invoke the command
	if (argc == 0)
f0100b43:	85 f6                	test   %esi,%esi
f0100b45:	0f 84 5c ff ff ff    	je     f0100aa7 <monitor+0x36>
f0100b4b:	bf 60 7a 10 f0       	mov    $0xf0107a60,%edi
f0100b50:	bb 00 00 00 00       	mov    $0x0,%ebx
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f0100b55:	83 ec 08             	sub    $0x8,%esp
f0100b58:	ff 37                	pushl  (%edi)
f0100b5a:	ff 75 a8             	pushl  -0x58(%ebp)
f0100b5d:	e8 3d 54 00 00       	call   f0105f9f <strcmp>
f0100b62:	83 c4 10             	add    $0x10,%esp
f0100b65:	85 c0                	test   %eax,%eax
f0100b67:	75 23                	jne    f0100b8c <monitor+0x11b>
			return commands[i].func(argc, argv, tf);
f0100b69:	83 ec 04             	sub    $0x4,%esp
f0100b6c:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
f0100b6f:	01 c3                	add    %eax,%ebx
f0100b71:	ff 75 08             	pushl  0x8(%ebp)
f0100b74:	8d 45 a8             	lea    -0x58(%ebp),%eax
f0100b77:	50                   	push   %eax
f0100b78:	56                   	push   %esi
f0100b79:	ff 14 9d 68 7a 10 f0 	call   *-0xfef8598(,%ebx,4)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f0100b80:	83 c4 10             	add    $0x10,%esp
f0100b83:	85 c0                	test   %eax,%eax
f0100b85:	78 26                	js     f0100bad <monitor+0x13c>
f0100b87:	e9 1b ff ff ff       	jmp    f0100aa7 <monitor+0x36>
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
f0100b8c:	43                   	inc    %ebx
f0100b8d:	83 c7 0c             	add    $0xc,%edi
f0100b90:	83 fb 03             	cmp    $0x3,%ebx
f0100b93:	75 c0                	jne    f0100b55 <monitor+0xe4>
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f0100b95:	83 ec 08             	sub    $0x8,%esp
f0100b98:	ff 75 a8             	pushl  -0x58(%ebp)
f0100b9b:	68 eb 78 10 f0       	push   $0xf01078eb
f0100ba0:	e8 6c 30 00 00       	call   f0103c11 <cprintf>
f0100ba5:	83 c4 10             	add    $0x10,%esp
f0100ba8:	e9 fa fe ff ff       	jmp    f0100aa7 <monitor+0x36>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f0100bad:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100bb0:	5b                   	pop    %ebx
f0100bb1:	5e                   	pop    %esi
f0100bb2:	5f                   	pop    %edi
f0100bb3:	5d                   	pop    %ebp
f0100bb4:	c3                   	ret    

f0100bb5 <e820_init>:

// This function may ONLY be used during initialization,
// before page_init().
void
e820_init(physaddr_t mbi_pa)
{
f0100bb5:	55                   	push   %ebp
f0100bb6:	89 e5                	mov    %esp,%ebp
f0100bb8:	57                   	push   %edi
f0100bb9:	56                   	push   %esi
f0100bba:	53                   	push   %ebx
f0100bbb:	83 ec 1c             	sub    $0x1c,%esp
f0100bbe:	8b 75 08             	mov    0x8(%ebp),%esi
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
f0100bc1:	f6 06 40             	testb  $0x40,(%esi)
f0100bc4:	75 16                	jne    f0100bdc <e820_init+0x27>
f0100bc6:	68 84 7a 10 f0       	push   $0xf0107a84
f0100bcb:	68 51 75 10 f0       	push   $0xf0107551
f0100bd0:	6a 26                	push   $0x26
f0100bd2:	68 d7 7a 10 f0       	push   $0xf0107ad7
f0100bd7:	e8 70 f4 ff ff       	call   f010004c <_panic>
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);
f0100bdc:	8b 56 30             	mov    0x30(%esi),%edx
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
f0100bdf:	83 ec 04             	sub    $0x4,%esp
f0100be2:	89 d0                	mov    %edx,%eax
f0100be4:	03 46 2c             	add    0x2c(%esi),%eax
f0100be7:	48                   	dec    %eax
f0100be8:	50                   	push   %eax
f0100be9:	52                   	push   %edx
f0100bea:	68 a8 7a 10 f0       	push   $0xf0107aa8
f0100bef:	e8 1d 30 00 00       	call   f0103c11 <cprintf>
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
f0100bf4:	8b 5e 30             	mov    0x30(%esi),%ebx
	addr_end = mbi->mmap_addr + mbi->mmap_length;
f0100bf7:	89 d8                	mov    %ebx,%eax
f0100bf9:	03 46 2c             	add    0x2c(%esi),%eax
f0100bfc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0100bff:	c7 45 dc 24 3f 23 f0 	movl   $0xf0233f24,-0x24(%ebp)
	for (i = 0; addr < addr_end; ++i) {
f0100c06:	83 c4 10             	add    $0x10,%esp
f0100c09:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f0100c10:	e9 b5 00 00 00       	jmp    f0100cca <e820_init+0x115>
		struct multiboot_mmap_entry *e;

		// Print memory mapping.
		assert(addr_end - addr >= sizeof(*e));
f0100c15:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100c18:	29 d8                	sub    %ebx,%eax
f0100c1a:	83 f8 17             	cmp    $0x17,%eax
f0100c1d:	77 16                	ja     f0100c35 <e820_init+0x80>
f0100c1f:	68 e3 7a 10 f0       	push   $0xf0107ae3
f0100c24:	68 51 75 10 f0       	push   $0xf0107551
f0100c29:	6a 30                	push   $0x30
f0100c2b:	68 d7 7a 10 f0       	push   $0xf0107ad7
f0100c30:	e8 17 f4 ff ff       	call   f010004c <_panic>
		e = (struct multiboot_mmap_entry *)addr;
f0100c35:	89 de                	mov    %ebx,%esi
		cprintf("  [mem %08p-%08p] ",
f0100c37:	8b 53 04             	mov    0x4(%ebx),%edx
f0100c3a:	83 ec 04             	sub    $0x4,%esp
f0100c3d:	89 d0                	mov    %edx,%eax
f0100c3f:	03 43 0c             	add    0xc(%ebx),%eax
f0100c42:	48                   	dec    %eax
f0100c43:	50                   	push   %eax
f0100c44:	52                   	push   %edx
f0100c45:	68 01 7b 10 f0       	push   $0xf0107b01
f0100c4a:	e8 c2 2f 00 00       	call   f0103c11 <cprintf>
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
f0100c4f:	8b 43 14             	mov    0x14(%ebx),%eax
	"unusable",
};

static void
print_e820_map_type(uint32_t type) {
	switch (type) {
f0100c52:	83 c4 10             	add    $0x10,%esp
f0100c55:	8d 50 ff             	lea    -0x1(%eax),%edx
f0100c58:	83 fa 04             	cmp    $0x4,%edx
f0100c5b:	77 14                	ja     f0100c71 <e820_init+0xbc>
	case 1 ... 5:
		cprintf(e820_map_types[type - 1]);
f0100c5d:	83 ec 0c             	sub    $0xc,%esp
f0100c60:	ff 34 85 60 7b 10 f0 	pushl  -0xfef84a0(,%eax,4)
f0100c67:	e8 a5 2f 00 00       	call   f0103c11 <cprintf>
f0100c6c:	83 c4 10             	add    $0x10,%esp
f0100c6f:	eb 11                	jmp    f0100c82 <e820_init+0xcd>
		break;
	default:
		cprintf("type %u", type);
f0100c71:	83 ec 08             	sub    $0x8,%esp
f0100c74:	50                   	push   %eax
f0100c75:	68 14 7b 10 f0       	push   $0xf0107b14
f0100c7a:	e8 92 2f 00 00       	call   f0103c11 <cprintf>
f0100c7f:	83 c4 10             	add    $0x10,%esp
		e = (struct multiboot_mmap_entry *)addr;
		cprintf("  [mem %08p-%08p] ",
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
		cprintf("\n");
f0100c82:	83 ec 0c             	sub    $0xc,%esp
f0100c85:	68 b2 86 10 f0       	push   $0xf01086b2
f0100c8a:	e8 82 2f 00 00       	call   f0103c11 <cprintf>

		// Save a copy.
		assert(i < E820_NR_MAX);
f0100c8f:	83 c4 10             	add    $0x10,%esp
f0100c92:	83 7d e0 40          	cmpl   $0x40,-0x20(%ebp)
f0100c96:	75 16                	jne    f0100cae <e820_init+0xf9>
f0100c98:	68 1c 7b 10 f0       	push   $0xf0107b1c
f0100c9d:	68 51 75 10 f0       	push   $0xf0107551
f0100ca2:	6a 39                	push   $0x39
f0100ca4:	68 d7 7a 10 f0       	push   $0xf0107ad7
f0100ca9:	e8 9e f3 ff ff       	call   f010004c <_panic>
		e820_map.entries[i] = e->e820;
f0100cae:	89 f0                	mov    %esi,%eax
f0100cb0:	8d 76 04             	lea    0x4(%esi),%esi
f0100cb3:	b9 05 00 00 00       	mov    $0x5,%ecx
f0100cb8:	8b 7d dc             	mov    -0x24(%ebp),%edi
f0100cbb:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		addr += (e->size + 4);
f0100cbd:	8b 00                	mov    (%eax),%eax
f0100cbf:	8d 5c 03 04          	lea    0x4(%ebx,%eax,1),%ebx
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
	addr_end = mbi->mmap_addr + mbi->mmap_length;
	for (i = 0; addr < addr_end; ++i) {
f0100cc3:	ff 45 e0             	incl   -0x20(%ebp)
f0100cc6:	83 45 dc 14          	addl   $0x14,-0x24(%ebp)
f0100cca:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f0100ccd:	0f 82 42 ff ff ff    	jb     f0100c15 <e820_init+0x60>
		// Save a copy.
		assert(i < E820_NR_MAX);
		e820_map.entries[i] = e->e820;
		addr += (e->size + 4);
	}
	e820_map.nr = i;
f0100cd3:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100cd6:	a3 20 3f 23 f0       	mov    %eax,0xf0233f20
}
f0100cdb:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100cde:	5b                   	pop    %ebx
f0100cdf:	5e                   	pop    %esi
f0100ce0:	5f                   	pop    %edi
f0100ce1:	5d                   	pop    %ebp
f0100ce2:	c3                   	ret    

f0100ce3 <boot_alloc>:
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100ce3:	89 c1                	mov    %eax,%ecx
	// Initialize nextfree if this is the first time.
	// 'end' is a magic symbol automatically generated by the linker,
	// which points to the end of the kernel's bss segment:
	// the first virtual address that the linker did *not* assign
	// to any kernel code or global variables.
	if (!nextfree) {
f0100ce5:	83 3d 38 32 23 f0 00 	cmpl   $0x0,0xf0233238
f0100cec:	75 0f                	jne    f0100cfd <boot_alloc+0x1a>
		extern char end[]; //points to the end of the kernel
		nextfree = ROUNDUP((char *) end, PGSIZE); //set the nextfree pointer
f0100cee:	b8 0b 70 27 f0       	mov    $0xf027700b,%eax
f0100cf3:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100cf8:	a3 38 32 23 f0       	mov    %eax,0xf0233238
	//
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
f0100cfd:	a1 38 32 23 f0       	mov    0xf0233238,%eax
f0100d02:	8b 15 24 44 23 f0    	mov    0xf0234424,%edx
f0100d08:	c1 e2 0c             	shl    $0xc,%edx
f0100d0b:	29 c2                	sub    %eax,%edx
f0100d0d:	39 ca                	cmp    %ecx,%edx
f0100d0f:	73 17                	jae    f0100d28 <boot_alloc+0x45>
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100d11:	55                   	push   %ebp
f0100d12:	89 e5                	mov    %esp,%ebp
f0100d14:	83 ec 0c             	sub    $0xc,%esp
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
		panic("boot_alloc: not enough physical address to allocate\n");
f0100d17:	68 78 7b 10 f0       	push   $0xf0107b78
f0100d1c:	6a 64                	push   $0x64
f0100d1e:	68 e1 84 10 f0       	push   $0xf01084e1
f0100d23:	e8 24 f3 ff ff       	call   f010004c <_panic>
	}
	//nextfree = ROUNDUP( ((char *)n + nextfree), PGSIZE); //update the nextfree pointer
	result = nextfree;
	nextfree = n + nextfree; //update the nextfree pointer
f0100d28:	01 c1                	add    %eax,%ecx
f0100d2a:	89 0d 38 32 23 f0    	mov    %ecx,0xf0233238
	return result;
	//return NULL;
}
f0100d30:	c3                   	ret    

f0100d31 <check_va2pa>:
check_va2pa(pde_t *pgdir, uintptr_t va)
{
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
f0100d31:	89 d1                	mov    %edx,%ecx
f0100d33:	c1 e9 16             	shr    $0x16,%ecx
f0100d36:	8b 04 88             	mov    (%eax,%ecx,4),%eax
f0100d39:	a8 01                	test   $0x1,%al
f0100d3b:	74 47                	je     f0100d84 <check_va2pa+0x53>
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
f0100d3d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100d42:	89 c1                	mov    %eax,%ecx
f0100d44:	c1 e9 0c             	shr    $0xc,%ecx
f0100d47:	3b 0d 24 44 23 f0    	cmp    0xf0234424,%ecx
f0100d4d:	72 1b                	jb     f0100d6a <check_va2pa+0x39>
// this functionality for us!  We define our own version to help check
// the check_kern_pgdir() function; it shouldn't be used elsewhere.

static physaddr_t
check_va2pa(pde_t *pgdir, uintptr_t va)
{
f0100d4f:	55                   	push   %ebp
f0100d50:	89 e5                	mov    %esp,%ebp
f0100d52:	83 ec 08             	sub    $0x8,%esp
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100d55:	50                   	push   %eax
f0100d56:	68 e8 74 10 f0       	push   $0xf01074e8
f0100d5b:	68 8d 04 00 00       	push   $0x48d
f0100d60:	68 e1 84 10 f0       	push   $0xf01084e1
f0100d65:	e8 e2 f2 ff ff       	call   f010004c <_panic>
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
f0100d6a:	c1 ea 0c             	shr    $0xc,%edx
f0100d6d:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f0100d73:	8b 84 90 00 00 00 f0 	mov    -0x10000000(%eax,%edx,4),%eax
f0100d7a:	a8 01                	test   $0x1,%al
f0100d7c:	74 0c                	je     f0100d8a <check_va2pa+0x59>
	}

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
f0100d7e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100d83:	c3                   	ret    
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
f0100d84:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100d89:	c3                   	ret    
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
		//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
		//cprintf("check_va2pa no pte, return fff...\n");
		//cprintf("shit happens\n");
		return ~0;
f0100d8a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
}
f0100d8f:	c3                   	ret    

f0100d90 <check_page_free_list>:
//
// Check that the pages on the page_free_list are reasonable.
//
static void
check_page_free_list(bool only_low_memory)
{
f0100d90:	55                   	push   %ebp
f0100d91:	89 e5                	mov    %esp,%ebp
f0100d93:	57                   	push   %edi
f0100d94:	56                   	push   %esi
f0100d95:	53                   	push   %ebx
f0100d96:	83 ec 2c             	sub    $0x2c,%esp
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100d99:	84 c0                	test   %al,%al
f0100d9b:	0f 85 8b 02 00 00    	jne    f010102c <check_page_free_list+0x29c>
f0100da1:	e9 98 02 00 00       	jmp    f010103e <check_page_free_list+0x2ae>
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
		panic("'page_free_list' is a null pointer!");
f0100da6:	83 ec 04             	sub    $0x4,%esp
f0100da9:	68 b0 7b 10 f0       	push   $0xf0107bb0
f0100dae:	68 bb 03 00 00       	push   $0x3bb
f0100db3:	68 e1 84 10 f0       	push   $0xf01084e1
f0100db8:	e8 8f f2 ff ff       	call   f010004c <_panic>

	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
f0100dbd:	8d 55 d8             	lea    -0x28(%ebp),%edx
f0100dc0:	89 55 e0             	mov    %edx,-0x20(%ebp)
f0100dc3:	8d 55 dc             	lea    -0x24(%ebp),%edx
f0100dc6:	89 55 e4             	mov    %edx,-0x1c(%ebp)
		for (pp = page_free_list; pp; pp = pp->pp_link) {
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
f0100dc9:	89 c2                	mov    %eax,%edx
f0100dcb:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f0100dd1:	f7 c2 00 e0 7f 00    	test   $0x7fe000,%edx
f0100dd7:	0f 95 c2             	setne  %dl
f0100dda:	0f b6 d2             	movzbl %dl,%edx
			*tp[pagetype] = pp;
f0100ddd:	8b 4c 95 e0          	mov    -0x20(%ebp,%edx,4),%ecx
f0100de1:	89 01                	mov    %eax,(%ecx)
			tp[pagetype] = &pp->pp_link;
f0100de3:	89 44 95 e0          	mov    %eax,-0x20(%ebp,%edx,4)
	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
		for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100de7:	8b 00                	mov    (%eax),%eax
f0100de9:	85 c0                	test   %eax,%eax
f0100deb:	75 dc                	jne    f0100dc9 <check_page_free_list+0x39>
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
			*tp[pagetype] = pp;
			tp[pagetype] = &pp->pp_link;
		}
		*tp[1] = 0;
f0100ded:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100df0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		*tp[0] = pp2;
f0100df6:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100df9:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100dfc:	89 10                	mov    %edx,(%eax)
		page_free_list = pp1;
f0100dfe:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100e01:	a3 40 32 23 f0       	mov    %eax,0xf0233240
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100e06:	be 01 00 00 00       	mov    $0x1,%esi
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100e0b:	8b 1d 40 32 23 f0    	mov    0xf0233240,%ebx
f0100e11:	eb 53                	jmp    f0100e66 <check_page_free_list+0xd6>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100e13:	89 d8                	mov    %ebx,%eax
f0100e15:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0100e1b:	c1 f8 03             	sar    $0x3,%eax
f0100e1e:	c1 e0 0c             	shl    $0xc,%eax
		if (PDX(page2pa(pp)) < pdx_limit)
f0100e21:	89 c2                	mov    %eax,%edx
f0100e23:	c1 ea 16             	shr    $0x16,%edx
f0100e26:	39 f2                	cmp    %esi,%edx
f0100e28:	73 3a                	jae    f0100e64 <check_page_free_list+0xd4>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100e2a:	89 c2                	mov    %eax,%edx
f0100e2c:	c1 ea 0c             	shr    $0xc,%edx
f0100e2f:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f0100e35:	72 12                	jb     f0100e49 <check_page_free_list+0xb9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100e37:	50                   	push   %eax
f0100e38:	68 e8 74 10 f0       	push   $0xf01074e8
f0100e3d:	6a 58                	push   $0x58
f0100e3f:	68 ed 84 10 f0       	push   $0xf01084ed
f0100e44:	e8 03 f2 ff ff       	call   f010004c <_panic>
			memset(page2kva(pp), 0x97, 128);
f0100e49:	83 ec 04             	sub    $0x4,%esp
f0100e4c:	68 80 00 00 00       	push   $0x80
f0100e51:	68 97 00 00 00       	push   $0x97
f0100e56:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100e5b:	50                   	push   %eax
f0100e5c:	e8 c7 51 00 00       	call   f0106028 <memset>
f0100e61:	83 c4 10             	add    $0x10,%esp
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100e64:	8b 1b                	mov    (%ebx),%ebx
f0100e66:	85 db                	test   %ebx,%ebx
f0100e68:	75 a9                	jne    f0100e13 <check_page_free_list+0x83>
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
f0100e6a:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e6f:	e8 6f fe ff ff       	call   f0100ce3 <boot_alloc>
f0100e74:	89 45 cc             	mov    %eax,-0x34(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100e77:	8b 15 40 32 23 f0    	mov    0xf0233240,%edx
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100e7d:	8b 0d 2c 44 23 f0    	mov    0xf023442c,%ecx
		assert(pp < pages + npages);
f0100e83:	a1 24 44 23 f0       	mov    0xf0234424,%eax
f0100e88:	89 45 c8             	mov    %eax,-0x38(%ebp)
f0100e8b:	8d 04 c1             	lea    (%ecx,%eax,8),%eax
f0100e8e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100e91:	89 4d d0             	mov    %ecx,-0x30(%ebp)
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
f0100e94:	be 00 00 00 00       	mov    $0x0,%esi
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100e99:	e9 4c 01 00 00       	jmp    f0100fea <check_page_free_list+0x25a>
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100e9e:	39 ca                	cmp    %ecx,%edx
f0100ea0:	73 19                	jae    f0100ebb <check_page_free_list+0x12b>
f0100ea2:	68 fb 84 10 f0       	push   $0xf01084fb
f0100ea7:	68 51 75 10 f0       	push   $0xf0107551
f0100eac:	68 d5 03 00 00       	push   $0x3d5
f0100eb1:	68 e1 84 10 f0       	push   $0xf01084e1
f0100eb6:	e8 91 f1 ff ff       	call   f010004c <_panic>
		assert(pp < pages + npages);
f0100ebb:	3b 55 d4             	cmp    -0x2c(%ebp),%edx
f0100ebe:	72 19                	jb     f0100ed9 <check_page_free_list+0x149>
f0100ec0:	68 07 85 10 f0       	push   $0xf0108507
f0100ec5:	68 51 75 10 f0       	push   $0xf0107551
f0100eca:	68 d6 03 00 00       	push   $0x3d6
f0100ecf:	68 e1 84 10 f0       	push   $0xf01084e1
f0100ed4:	e8 73 f1 ff ff       	call   f010004c <_panic>
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100ed9:	89 d0                	mov    %edx,%eax
f0100edb:	2b 45 d0             	sub    -0x30(%ebp),%eax
f0100ede:	a8 07                	test   $0x7,%al
f0100ee0:	74 19                	je     f0100efb <check_page_free_list+0x16b>
f0100ee2:	68 d4 7b 10 f0       	push   $0xf0107bd4
f0100ee7:	68 51 75 10 f0       	push   $0xf0107551
f0100eec:	68 d7 03 00 00       	push   $0x3d7
f0100ef1:	68 e1 84 10 f0       	push   $0xf01084e1
f0100ef6:	e8 51 f1 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100efb:	c1 f8 03             	sar    $0x3,%eax

		// check a few pages that shouldn't be on the free list
		assert(page2pa(pp) != 0);
f0100efe:	c1 e0 0c             	shl    $0xc,%eax
f0100f01:	75 19                	jne    f0100f1c <check_page_free_list+0x18c>
f0100f03:	68 1b 85 10 f0       	push   $0xf010851b
f0100f08:	68 51 75 10 f0       	push   $0xf0107551
f0100f0d:	68 da 03 00 00       	push   $0x3da
f0100f12:	68 e1 84 10 f0       	push   $0xf01084e1
f0100f17:	e8 30 f1 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != IOPHYSMEM);
f0100f1c:	3d 00 00 0a 00       	cmp    $0xa0000,%eax
f0100f21:	75 19                	jne    f0100f3c <check_page_free_list+0x1ac>
f0100f23:	68 2c 85 10 f0       	push   $0xf010852c
f0100f28:	68 51 75 10 f0       	push   $0xf0107551
f0100f2d:	68 db 03 00 00       	push   $0x3db
f0100f32:	68 e1 84 10 f0       	push   $0xf01084e1
f0100f37:	e8 10 f1 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM - PGSIZE);
f0100f3c:	3d 00 f0 0f 00       	cmp    $0xff000,%eax
f0100f41:	75 19                	jne    f0100f5c <check_page_free_list+0x1cc>
f0100f43:	68 08 7c 10 f0       	push   $0xf0107c08
f0100f48:	68 51 75 10 f0       	push   $0xf0107551
f0100f4d:	68 dc 03 00 00       	push   $0x3dc
f0100f52:	68 e1 84 10 f0       	push   $0xf01084e1
f0100f57:	e8 f0 f0 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM);
f0100f5c:	3d 00 00 10 00       	cmp    $0x100000,%eax
f0100f61:	75 19                	jne    f0100f7c <check_page_free_list+0x1ec>
f0100f63:	68 45 85 10 f0       	push   $0xf0108545
f0100f68:	68 51 75 10 f0       	push   $0xf0107551
f0100f6d:	68 dd 03 00 00       	push   $0x3dd
f0100f72:	68 e1 84 10 f0       	push   $0xf01084e1
f0100f77:	e8 d0 f0 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) < EXTPHYSMEM || (char *) page2kva(pp) >= first_free_page);
f0100f7c:	3d ff ff 0f 00       	cmp    $0xfffff,%eax
f0100f81:	0f 86 da 00 00 00    	jbe    f0101061 <check_page_free_list+0x2d1>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100f87:	89 c7                	mov    %eax,%edi
f0100f89:	c1 ef 0c             	shr    $0xc,%edi
f0100f8c:	39 7d c8             	cmp    %edi,-0x38(%ebp)
f0100f8f:	77 12                	ja     f0100fa3 <check_page_free_list+0x213>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100f91:	50                   	push   %eax
f0100f92:	68 e8 74 10 f0       	push   $0xf01074e8
f0100f97:	6a 58                	push   $0x58
f0100f99:	68 ed 84 10 f0       	push   $0xf01084ed
f0100f9e:	e8 a9 f0 ff ff       	call   f010004c <_panic>
f0100fa3:	8d b8 00 00 00 f0    	lea    -0x10000000(%eax),%edi
f0100fa9:	39 7d cc             	cmp    %edi,-0x34(%ebp)
f0100fac:	0f 86 a3 00 00 00    	jbe    f0101055 <check_page_free_list+0x2c5>
f0100fb2:	68 2c 7c 10 f0       	push   $0xf0107c2c
f0100fb7:	68 51 75 10 f0       	push   $0xf0107551
f0100fbc:	68 de 03 00 00       	push   $0x3de
f0100fc1:	68 e1 84 10 f0       	push   $0xf01084e1
f0100fc6:	e8 81 f0 ff ff       	call   f010004c <_panic>
		// (new test for lab 4)
		assert(page2pa(pp) != MPENTRY_PADDR);
f0100fcb:	68 5f 85 10 f0       	push   $0xf010855f
f0100fd0:	68 51 75 10 f0       	push   $0xf0107551
f0100fd5:	68 e0 03 00 00       	push   $0x3e0
f0100fda:	68 e1 84 10 f0       	push   $0xf01084e1
f0100fdf:	e8 68 f0 ff ff       	call   f010004c <_panic>

		if (page2pa(pp) < EXTPHYSMEM)
			++nfree_basemem;
f0100fe4:	46                   	inc    %esi
f0100fe5:	eb 01                	jmp    f0100fe8 <check_page_free_list+0x258>
		else
			++nfree_extmem;
f0100fe7:	43                   	inc    %ebx
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100fe8:	8b 12                	mov    (%edx),%edx
f0100fea:	85 d2                	test   %edx,%edx
f0100fec:	0f 85 ac fe ff ff    	jne    f0100e9e <check_page_free_list+0x10e>
			++nfree_basemem;
		else
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
f0100ff2:	85 f6                	test   %esi,%esi
f0100ff4:	7f 19                	jg     f010100f <check_page_free_list+0x27f>
f0100ff6:	68 7c 85 10 f0       	push   $0xf010857c
f0100ffb:	68 51 75 10 f0       	push   $0xf0107551
f0101000:	68 e8 03 00 00       	push   $0x3e8
f0101005:	68 e1 84 10 f0       	push   $0xf01084e1
f010100a:	e8 3d f0 ff ff       	call   f010004c <_panic>
	assert(nfree_extmem > 0);
f010100f:	85 db                	test   %ebx,%ebx
f0101011:	7f 5e                	jg     f0101071 <check_page_free_list+0x2e1>
f0101013:	68 8e 85 10 f0       	push   $0xf010858e
f0101018:	68 51 75 10 f0       	push   $0xf0107551
f010101d:	68 e9 03 00 00       	push   $0x3e9
f0101022:	68 e1 84 10 f0       	push   $0xf01084e1
f0101027:	e8 20 f0 ff ff       	call   f010004c <_panic>
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
f010102c:	a1 40 32 23 f0       	mov    0xf0233240,%eax
f0101031:	85 c0                	test   %eax,%eax
f0101033:	0f 85 84 fd ff ff    	jne    f0100dbd <check_page_free_list+0x2d>
f0101039:	e9 68 fd ff ff       	jmp    f0100da6 <check_page_free_list+0x16>
f010103e:	83 3d 40 32 23 f0 00 	cmpl   $0x0,0xf0233240
f0101045:	0f 84 5b fd ff ff    	je     f0100da6 <check_page_free_list+0x16>
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f010104b:	be 00 04 00 00       	mov    $0x400,%esi
f0101050:	e9 b6 fd ff ff       	jmp    f0100e0b <check_page_free_list+0x7b>
		assert(page2pa(pp) != IOPHYSMEM);
		assert(page2pa(pp) != EXTPHYSMEM - PGSIZE);
		assert(page2pa(pp) != EXTPHYSMEM);
		assert(page2pa(pp) < EXTPHYSMEM || (char *) page2kva(pp) >= first_free_page);
		// (new test for lab 4)
		assert(page2pa(pp) != MPENTRY_PADDR);
f0101055:	3d 00 70 00 00       	cmp    $0x7000,%eax
f010105a:	75 8b                	jne    f0100fe7 <check_page_free_list+0x257>
f010105c:	e9 6a ff ff ff       	jmp    f0100fcb <check_page_free_list+0x23b>
f0101061:	3d 00 70 00 00       	cmp    $0x7000,%eax
f0101066:	0f 85 78 ff ff ff    	jne    f0100fe4 <check_page_free_list+0x254>
f010106c:	e9 5a ff ff ff       	jmp    f0100fcb <check_page_free_list+0x23b>
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
	assert(nfree_extmem > 0);
}
f0101071:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0101074:	5b                   	pop    %ebx
f0101075:	5e                   	pop    %esi
f0101076:	5f                   	pop    %edi
f0101077:	5d                   	pop    %ebp
f0101078:	c3                   	ret    

f0101079 <page_init>:
// allocator functions below to allocate and deallocate physical
// memory via the page_free_list.
//
void
page_init(void)
{
f0101079:	55                   	push   %ebp
f010107a:	89 e5                	mov    %esp,%ebp
f010107c:	56                   	push   %esi
f010107d:	53                   	push   %ebx

	//page_free_list -> Pn -> Pn-1 -> ... -> P2 -> P1
	//physical page 0 is in use.
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
f010107e:	83 ec 08             	sub    $0x8,%esp
f0101081:	ff 35 24 44 23 f0    	pushl  0xf0234424
f0101087:	68 9f 85 10 f0       	push   $0xf010859f
f010108c:	e8 80 2b 00 00       	call   f0103c11 <cprintf>
	assert(PGNUM(0x0009fbff) <= npages);
f0101091:	83 c4 10             	add    $0x10,%esp
f0101094:	81 3d 24 44 23 f0 9e 	cmpl   $0x9e,0xf0234424
f010109b:	00 00 00 
f010109e:	76 0f                	jbe    f01010af <page_init+0x36>
f01010a0:	8b 1d 40 32 23 f0    	mov    0xf0233240,%ebx
f01010a6:	b1 00                	mov    $0x0,%cl
f01010a8:	b8 01 00 00 00       	mov    $0x1,%eax
f01010ad:	eb 49                	jmp    f01010f8 <page_init+0x7f>
f01010af:	68 ab 85 10 f0       	push   $0xf01085ab
f01010b4:	68 51 75 10 f0       	push   $0xf0107551
f01010b9:	68 7b 01 00 00       	push   $0x17b
f01010be:	68 e1 84 10 f0       	push   $0xf01084e1
f01010c3:	e8 84 ef ff ff       	call   f010004c <_panic>
	size_t mpentry_index = PGNUM(MPENTRY_PADDR);
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
		//skip the 
		if (i == mpentry_index){
f01010c8:	83 f8 07             	cmp    $0x7,%eax
f01010cb:	75 0f                	jne    f01010dc <page_init+0x63>
            pages[i].pp_ref = 1;
f01010cd:	66 c7 42 3c 01 00    	movw   $0x1,0x3c(%edx)
            pages[i].pp_link = NULL;
f01010d3:	c7 42 38 00 00 00 00 	movl   $0x0,0x38(%edx)
            continue;
f01010da:	eb 1b                	jmp    f01010f7 <page_init+0x7e>
f01010dc:	8d 0c c5 00 00 00 00 	lea    0x0(,%eax,8),%ecx
        }

		pages[i].pp_ref = 0;
f01010e3:	01 ca                	add    %ecx,%edx
f01010e5:	66 c7 42 04 00 00    	movw   $0x0,0x4(%edx)
		pages[i].pp_link = page_free_list;
f01010eb:	89 1a                	mov    %ebx,(%edx)
		page_free_list = &pages[i];
f01010ed:	89 cb                	mov    %ecx,%ebx
f01010ef:	03 1d 2c 44 23 f0    	add    0xf023442c,%ebx
f01010f5:	b1 01                	mov    $0x1,%cl
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
	assert(PGNUM(0x0009fbff) <= npages);
	size_t mpentry_index = PGNUM(MPENTRY_PADDR);
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
f01010f7:	40                   	inc    %eax
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01010f8:	81 3d 24 44 23 f0 9f 	cmpl   $0x9f,0xf0234424
f01010ff:	00 00 00 
f0101102:	77 1e                	ja     f0101122 <page_init+0xa9>
f0101104:	84 c9                	test   %cl,%cl
f0101106:	74 06                	je     f010110e <page_init+0x95>
f0101108:	89 1d 40 32 23 f0    	mov    %ebx,0xf0233240
		panic("pa2page called with invalid pa");
f010110e:	83 ec 04             	sub    $0x4,%esp
f0101111:	68 74 7c 10 f0       	push   $0xf0107c74
f0101116:	6a 51                	push   $0x51
f0101118:	68 ed 84 10 f0       	push   $0xf01084ed
f010111d:	e8 2a ef ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0101122:	8b 15 2c 44 23 f0    	mov    0xf023442c,%edx
f0101128:	3d 9f 00 00 00       	cmp    $0x9f,%eax
f010112d:	75 99                	jne    f01010c8 <page_init+0x4f>
f010112f:	84 c9                	test   %cl,%cl
f0101131:	74 06                	je     f0101139 <page_init+0xc0>
f0101133:	89 1d 40 32 23 f0    	mov    %ebx,0xf0233240
	//     0x00100000-0x????????  in use for kernel
    //[mem 0x00100000-0x07fdffff] available
	//physaddr_t start_PA = PADDR(boot_alloc(0)); //physical address of the end of the kernel plus previously allocated stuff
	//physaddr_t end_PA = PADDR(0x07fdffff);

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
f0101139:	b8 00 00 00 00       	mov    $0x0,%eax
f010113e:	e8 a0 fb ff ff       	call   f0100ce3 <boot_alloc>
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
f0101143:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101149:	c1 ea 0c             	shr    $0xc,%edx
f010114c:	a1 24 44 23 f0       	mov    0xf0234424,%eax
f0101151:	39 c2                	cmp    %eax,%edx
f0101153:	76 19                	jbe    f010116e <page_init+0xf5>
f0101155:	68 c7 85 10 f0       	push   $0xf01085c7
f010115a:	68 51 75 10 f0       	push   $0xf0107551
f010115f:	68 91 01 00 00       	push   $0x191
f0101164:	68 e1 84 10 f0       	push   $0xf01084e1
f0101169:	e8 de ee ff ff       	call   f010004c <_panic>
	assert(PGNUM(end_PA) <= npages);
f010116e:	3d de 7f 00 00       	cmp    $0x7fde,%eax
f0101173:	77 19                	ja     f010118e <page_init+0x115>
f0101175:	68 e1 85 10 f0       	push   $0xf01085e1
f010117a:	68 51 75 10 f0       	push   $0xf0107551
f010117f:	68 92 01 00 00       	push   $0x192
f0101184:	68 e1 84 10 f0       	push   $0xf01084e1
f0101189:	e8 be ee ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010118e:	39 c2                	cmp    %eax,%edx
f0101190:	72 14                	jb     f01011a6 <page_init+0x12d>
		panic("pa2page called with invalid pa");
f0101192:	83 ec 04             	sub    $0x4,%esp
f0101195:	68 74 7c 10 f0       	push   $0xf0107c74
f010119a:	6a 51                	push   $0x51
f010119c:	68 ed 84 10 f0       	push   $0xf01084ed
f01011a1:	e8 a6 ee ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f01011a6:	c1 e2 03             	shl    $0x3,%edx
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f01011a9:	89 d1                	mov    %edx,%ecx
f01011ab:	c1 f9 03             	sar    $0x3,%ecx
f01011ae:	8b 1d 40 32 23 f0    	mov    0xf0233240,%ebx
f01011b4:	be 00 00 00 00       	mov    $0x0,%esi
f01011b9:	eb 1b                	jmp    f01011d6 <page_init+0x15d>
		pages[i].pp_ref = 0;
f01011bb:	01 d0                	add    %edx,%eax
f01011bd:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
		pages[i].pp_link = page_free_list;
f01011c3:	89 18                	mov    %ebx,(%eax)
		page_free_list = &pages[i];
f01011c5:	89 d3                	mov    %edx,%ebx
f01011c7:	03 1d 2c 44 23 f0    	add    0xf023442c,%ebx

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
	assert(PGNUM(end_PA) <= npages);
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f01011cd:	41                   	inc    %ecx
f01011ce:	83 c2 08             	add    $0x8,%edx
f01011d1:	be 01 00 00 00       	mov    $0x1,%esi
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01011d6:	81 3d 24 44 23 f0 df 	cmpl   $0x7fdf,0xf0234424
f01011dd:	7f 00 00 
f01011e0:	77 20                	ja     f0101202 <page_init+0x189>
f01011e2:	89 f0                	mov    %esi,%eax
f01011e4:	84 c0                	test   %al,%al
f01011e6:	74 06                	je     f01011ee <page_init+0x175>
f01011e8:	89 1d 40 32 23 f0    	mov    %ebx,0xf0233240
		panic("pa2page called with invalid pa");
f01011ee:	83 ec 04             	sub    $0x4,%esp
f01011f1:	68 74 7c 10 f0       	push   $0xf0107c74
f01011f6:	6a 51                	push   $0x51
f01011f8:	68 ed 84 10 f0       	push   $0xf01084ed
f01011fd:	e8 4a ee ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0101202:	a1 2c 44 23 f0       	mov    0xf023442c,%eax
f0101207:	81 f9 de 7f 00 00    	cmp    $0x7fde,%ecx
f010120d:	7e ac                	jle    f01011bb <page_init+0x142>
f010120f:	89 f0                	mov    %esi,%eax
f0101211:	84 c0                	test   %al,%al
f0101213:	74 06                	je     f010121b <page_init+0x1a2>
f0101215:	89 1d 40 32 23 f0    	mov    %ebx,0xf0233240
		page_free_list = &pages[i];
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//cprintf("Got here1\n");
}
f010121b:	8d 65 f8             	lea    -0x8(%ebp),%esp
f010121e:	5b                   	pop    %ebx
f010121f:	5e                   	pop    %esi
f0101220:	5d                   	pop    %ebp
f0101221:	c3                   	ret    

f0101222 <page_alloc>:
// Returns NULL if out of free memory.
//
// Hint: use page2kva and memset
struct PageInfo *
page_alloc(int alloc_flags)
{
f0101222:	55                   	push   %ebp
f0101223:	89 e5                	mov    %esp,%ebp
f0101225:	53                   	push   %ebx
f0101226:	83 ec 04             	sub    $0x4,%esp
	
	// Fill this function in
	if(page_free_list == NULL){ //check if out of memory
f0101229:	8b 1d 40 32 23 f0    	mov    0xf0233240,%ebx
f010122f:	85 db                	test   %ebx,%ebx
f0101231:	74 5e                	je     f0101291 <page_alloc+0x6f>
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//page_free_list->pp_link = NULL;
	struct PageInfo *page = page_free_list;
	page_free_list = page_free_list->pp_link;
f0101233:	8b 03                	mov    (%ebx),%eax
f0101235:	a3 40 32 23 f0       	mov    %eax,0xf0233240
	page->pp_link = NULL;
f010123a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
	page->pp_ref = 0;
f0101240:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)
	if(alloc_flags & ALLOC_ZERO){  //fills the entire returned physical page with '\0' bytes
f0101246:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
f010124a:	74 45                	je     f0101291 <page_alloc+0x6f>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010124c:	89 d8                	mov    %ebx,%eax
f010124e:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0101254:	c1 f8 03             	sar    $0x3,%eax
f0101257:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010125a:	89 c2                	mov    %eax,%edx
f010125c:	c1 ea 0c             	shr    $0xc,%edx
f010125f:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f0101265:	72 12                	jb     f0101279 <page_alloc+0x57>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101267:	50                   	push   %eax
f0101268:	68 e8 74 10 f0       	push   $0xf01074e8
f010126d:	6a 58                	push   $0x58
f010126f:	68 ed 84 10 f0       	push   $0xf01084ed
f0101274:	e8 d3 ed ff ff       	call   f010004c <_panic>
		memset(page2kva(page), '\0', PGSIZE);
f0101279:	83 ec 04             	sub    $0x4,%esp
f010127c:	68 00 10 00 00       	push   $0x1000
f0101281:	6a 00                	push   $0x0
f0101283:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101288:	50                   	push   %eax
f0101289:	e8 9a 4d 00 00       	call   f0106028 <memset>
f010128e:	83 c4 10             	add    $0x10,%esp
		char *kva = page2kva(result);
		memset(kva, '\0', PGSIZE);
	}
	
	return result;*/
}
f0101291:	89 d8                	mov    %ebx,%eax
f0101293:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101296:	c9                   	leave  
f0101297:	c3                   	ret    

f0101298 <page_free>:
// Return a page to the free list.
// (This function should only be called when pp->pp_ref reaches 0.)
//
void
page_free(struct PageInfo *pp)
{
f0101298:	55                   	push   %ebp
f0101299:	89 e5                	mov    %esp,%ebp
f010129b:	83 ec 08             	sub    $0x8,%esp
f010129e:	8b 45 08             	mov    0x8(%ebp),%eax
	// Fill this function in
	// Hint: You may want to panic if pp->pp_ref is nonzero or
	// pp->pp_link is not NULL.
	if(pp->pp_ref != 0){
f01012a1:	66 83 78 04 00       	cmpw   $0x0,0x4(%eax)
f01012a6:	74 17                	je     f01012bf <page_free+0x27>
		panic("pp->pp_ref is nonzero");
f01012a8:	83 ec 04             	sub    $0x4,%esp
f01012ab:	68 f9 85 10 f0       	push   $0xf01085f9
f01012b0:	68 de 01 00 00       	push   $0x1de
f01012b5:	68 e1 84 10 f0       	push   $0xf01084e1
f01012ba:	e8 8d ed ff ff       	call   f010004c <_panic>
	}

	if(pp->pp_link != NULL){
f01012bf:	83 38 00             	cmpl   $0x0,(%eax)
f01012c2:	74 17                	je     f01012db <page_free+0x43>
		panic("pp->pp_link is not NULL");
f01012c4:	83 ec 04             	sub    $0x4,%esp
f01012c7:	68 0f 86 10 f0       	push   $0xf010860f
f01012cc:	68 e2 01 00 00       	push   $0x1e2
f01012d1:	68 e1 84 10 f0       	push   $0xf01084e1
f01012d6:	e8 71 ed ff ff       	call   f010004c <_panic>
	}

	if(page_free_list == NULL){
f01012db:	8b 15 40 32 23 f0    	mov    0xf0233240,%edx
f01012e1:	85 d2                	test   %edx,%edx
f01012e3:	75 07                	jne    f01012ec <page_free+0x54>
		page_free_list = pp;
f01012e5:	a3 40 32 23 f0       	mov    %eax,0xf0233240
f01012ea:	eb 07                	jmp    f01012f3 <page_free+0x5b>
	}else{
		pp->pp_link = page_free_list;
f01012ec:	89 10                	mov    %edx,(%eax)
		page_free_list = pp;
f01012ee:	a3 40 32 23 f0       	mov    %eax,0xf0233240
	}
	//cprintf("Free-ed %x\n", pp);
}
f01012f3:	c9                   	leave  
f01012f4:	c3                   	ret    

f01012f5 <page_decref>:
// Decrement the reference count on a page,
// freeing it if there are no more refs.
//
void
page_decref(struct PageInfo* pp)
{
f01012f5:	55                   	push   %ebp
f01012f6:	89 e5                	mov    %esp,%ebp
f01012f8:	83 ec 08             	sub    $0x8,%esp
f01012fb:	8b 55 08             	mov    0x8(%ebp),%edx
	if (--pp->pp_ref == 0)
f01012fe:	8b 42 04             	mov    0x4(%edx),%eax
f0101301:	48                   	dec    %eax
f0101302:	66 89 42 04          	mov    %ax,0x4(%edx)
f0101306:	66 85 c0             	test   %ax,%ax
f0101309:	75 0c                	jne    f0101317 <page_decref+0x22>
		page_free(pp);
f010130b:	83 ec 0c             	sub    $0xc,%esp
f010130e:	52                   	push   %edx
f010130f:	e8 84 ff ff ff       	call   f0101298 <page_free>
f0101314:	83 c4 10             	add    $0x10,%esp
}
f0101317:	c9                   	leave  
f0101318:	c3                   	ret    

f0101319 <pgdir_walk>:
// Hint 3: look at inc/mmu.h for useful macros that mainipulate page
// table and page directory entries.
//
pte_t *
pgdir_walk(pde_t *pgdir, const void *va, int create)
{
f0101319:	55                   	push   %ebp
f010131a:	89 e5                	mov    %esp,%ebp
f010131c:	53                   	push   %ebx
f010131d:	83 ec 04             	sub    $0x4,%esp
f0101320:	8b 45 08             	mov    0x8(%ebp),%eax
	//pte_t* pte_Ptr = (pte_t*)(KADDR(pde) + PTX(va));
	return pte_Ptr;
	*/

	//reference code:
	assert(pgdir);
f0101323:	85 c0                	test   %eax,%eax
f0101325:	75 19                	jne    f0101340 <pgdir_walk+0x27>
f0101327:	68 27 86 10 f0       	push   $0xf0108627
f010132c:	68 51 75 10 f0       	push   $0xf0107551
f0101331:	68 37 02 00 00       	push   $0x237
f0101336:	68 e1 84 10 f0       	push   $0xf01084e1
f010133b:	e8 0c ed ff ff       	call   f010004c <_panic>

	// Page directory index.
	size_t pdx = PDX(va);

	// Page directory entry.
	pde_t pde = pgdir[pdx];
f0101340:	8b 55 0c             	mov    0xc(%ebp),%edx
f0101343:	c1 ea 16             	shr    $0x16,%edx
f0101346:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
f0101349:	8b 03                	mov    (%ebx),%eax

	// Page table present?
	// pt is a (kernel) virtual address.
	if (pde & PTE_P) {
f010134b:	a8 01                	test   $0x1,%al
f010134d:	74 2f                	je     f010137e <pgdir_walk+0x65>
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
f010134f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101354:	89 c2                	mov    %eax,%edx
f0101356:	c1 ea 0c             	shr    $0xc,%edx
f0101359:	39 15 24 44 23 f0    	cmp    %edx,0xf0234424
f010135f:	77 15                	ja     f0101376 <pgdir_walk+0x5d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101361:	50                   	push   %eax
f0101362:	68 e8 74 10 f0       	push   $0xf01074e8
f0101367:	68 49 02 00 00       	push   $0x249
f010136c:	68 e1 84 10 f0       	push   $0xf01084e1
f0101371:	e8 d6 ec ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0101376:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx
f010137c:	eb 5f                	jmp    f01013dd <pgdir_walk+0xc4>
	} else {
		if (!create)
f010137e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0101382:	74 68                	je     f01013ec <pgdir_walk+0xd3>
			return NULL;
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
f0101384:	83 ec 0c             	sub    $0xc,%esp
f0101387:	6a 01                	push   $0x1
f0101389:	e8 94 fe ff ff       	call   f0101222 <page_alloc>
		if (!page)
f010138e:	83 c4 10             	add    $0x10,%esp
f0101391:	85 c0                	test   %eax,%eax
f0101393:	74 5e                	je     f01013f3 <pgdir_walk+0xda>
			return NULL;

		// The new page has these permissions for convenience.
		pgdir[pdx] = page2pa(page) | PTE_U | PTE_W | PTE_P;
f0101395:	89 c2                	mov    %eax,%edx
f0101397:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f010139d:	c1 fa 03             	sar    $0x3,%edx
f01013a0:	c1 e2 0c             	shl    $0xc,%edx
f01013a3:	83 ca 07             	or     $0x7,%edx
f01013a6:	89 13                	mov    %edx,(%ebx)

		// It appeared in page directory, so increment refcount.
		page->pp_ref++;
f01013a8:	66 ff 40 04          	incw   0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01013ac:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f01013b2:	c1 f8 03             	sar    $0x3,%eax
f01013b5:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01013b8:	89 c2                	mov    %eax,%edx
f01013ba:	c1 ea 0c             	shr    $0xc,%edx
f01013bd:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f01013c3:	72 12                	jb     f01013d7 <pgdir_walk+0xbe>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01013c5:	50                   	push   %eax
f01013c6:	68 e8 74 10 f0       	push   $0xf01074e8
f01013cb:	6a 58                	push   $0x58
f01013cd:	68 ed 84 10 f0       	push   $0xf01084ed
f01013d2:	e8 75 ec ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01013d7:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx

	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
f01013dd:	8b 45 0c             	mov    0xc(%ebp),%eax
f01013e0:	c1 e8 0a             	shr    $0xa,%eax
f01013e3:	25 fc 0f 00 00       	and    $0xffc,%eax
f01013e8:	01 d0                	add    %edx,%eax
f01013ea:	eb 0c                	jmp    f01013f8 <pgdir_walk+0xdf>
	if (pde & PTE_P) {
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
	} else {
		if (!create)
			return NULL;
f01013ec:	b8 00 00 00 00       	mov    $0x0,%eax
f01013f1:	eb 05                	jmp    f01013f8 <pgdir_walk+0xdf>
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
		if (!page)
			return NULL;
f01013f3:	b8 00 00 00 00       	mov    $0x0,%eax
	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
}
f01013f8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01013fb:	c9                   	leave  
f01013fc:	c3                   	ret    

f01013fd <boot_map_region>:
// mapped pages.
//
// Hint: the TA solution uses pgdir_walk
static void
boot_map_region(pde_t *pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm)
{
f01013fd:	55                   	push   %ebp
f01013fe:	89 e5                	mov    %esp,%ebp
f0101400:	57                   	push   %edi
f0101401:	56                   	push   %esi
f0101402:	53                   	push   %ebx
f0101403:	83 ec 1c             	sub    $0x1c,%esp
f0101406:	89 c3                	mov    %eax,%ebx
f0101408:	89 45 e0             	mov    %eax,-0x20(%ebp)
f010140b:	89 cf                	mov    %ecx,%edi
f010140d:	8b 45 08             	mov    0x8(%ebp),%eax
		//cprintf("pte boot_map_region get is: %x\n", (*pte) );
		//page_insert(pgdir, pa2page(pa+i*PGSIZE), (void*)(va+i*PGSIZE), perm);
//	}
	
	//reference code:
	assert(pgdir);
f0101410:	85 db                	test   %ebx,%ebx
f0101412:	75 19                	jne    f010142d <boot_map_region+0x30>
f0101414:	68 27 86 10 f0       	push   $0xf0108627
f0101419:	68 51 75 10 f0       	push   $0xf0107551
f010141e:	68 88 02 00 00       	push   $0x288
f0101423:	68 e1 84 10 f0       	push   $0xf01084e1
f0101428:	e8 1f ec ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(va, PGSIZE) == va);
f010142d:	f7 c2 ff 0f 00 00    	test   $0xfff,%edx
f0101433:	74 19                	je     f010144e <boot_map_region+0x51>
f0101435:	68 2d 86 10 f0       	push   $0xf010862d
f010143a:	68 51 75 10 f0       	push   $0xf0107551
f010143f:	68 89 02 00 00       	push   $0x289
f0101444:	68 e1 84 10 f0       	push   $0xf01084e1
f0101449:	e8 fe eb ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(size, PGSIZE) == size);
f010144e:	f7 c7 ff 0f 00 00    	test   $0xfff,%edi
f0101454:	74 19                	je     f010146f <boot_map_region+0x72>
f0101456:	68 94 7c 10 f0       	push   $0xf0107c94
f010145b:	68 51 75 10 f0       	push   $0xf0107551
f0101460:	68 8a 02 00 00       	push   $0x28a
f0101465:	68 e1 84 10 f0       	push   $0xf01084e1
f010146a:	e8 dd eb ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(pa, PGSIZE) == pa);
f010146f:	a9 ff 0f 00 00       	test   $0xfff,%eax
f0101474:	74 3d                	je     f01014b3 <boot_map_region+0xb6>
f0101476:	68 49 86 10 f0       	push   $0xf0108649
f010147b:	68 51 75 10 f0       	push   $0xf0107551
f0101480:	68 8b 02 00 00       	push   $0x28b
f0101485:	68 e1 84 10 f0       	push   $0xf01084e1
f010148a:	e8 bd eb ff ff       	call   f010004c <_panic>

	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);
f010148f:	83 ec 04             	sub    $0x4,%esp
f0101492:	6a 01                	push   $0x1
f0101494:	53                   	push   %ebx
f0101495:	ff 75 e0             	pushl  -0x20(%ebp)
f0101498:	e8 7c fe ff ff       	call   f0101319 <pgdir_walk>

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P | PTE_W;
f010149d:	0b 75 dc             	or     -0x24(%ebp),%esi
f01014a0:	89 30                	mov    %esi,(%eax)

		// Next page.
		va += PGSIZE;
f01014a2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		pa += PGSIZE;
		size -= PGSIZE;
f01014a8:	81 ef 00 10 00 00    	sub    $0x1000,%edi
f01014ae:	83 c4 10             	add    $0x10,%esp
f01014b1:	eb 10                	jmp    f01014c3 <boot_map_region+0xc6>
f01014b3:	89 d3                	mov    %edx,%ebx
f01014b5:	29 d0                	sub    %edx,%eax
f01014b7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P | PTE_W;
f01014ba:	8b 45 0c             	mov    0xc(%ebp),%eax
f01014bd:	83 c8 03             	or     $0x3,%eax
f01014c0:	89 45 dc             	mov    %eax,-0x24(%ebp)
f01014c3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01014c6:	8d 34 18             	lea    (%eax,%ebx,1),%esi
	assert(pgdir);
	assert(ROUNDDOWN(va, PGSIZE) == va);
	assert(ROUNDDOWN(size, PGSIZE) == size);
	assert(ROUNDDOWN(pa, PGSIZE) == pa);

	while (size >= PGSIZE) {
f01014c9:	81 ff ff 0f 00 00    	cmp    $0xfff,%edi
f01014cf:	77 be                	ja     f010148f <boot_map_region+0x92>
		// Next page.
		va += PGSIZE;
		pa += PGSIZE;
		size -= PGSIZE;
	}
}
f01014d1:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01014d4:	5b                   	pop    %ebx
f01014d5:	5e                   	pop    %esi
f01014d6:	5f                   	pop    %edi
f01014d7:	5d                   	pop    %ebp
f01014d8:	c3                   	ret    

f01014d9 <page_lookup>:
//
// Hint: the TA solution uses pgdir_walk and pa2page.
//
struct PageInfo *
page_lookup(pde_t *pgdir, void *va, pte_t **pte_store)
{
f01014d9:	55                   	push   %ebp
f01014da:	89 e5                	mov    %esp,%ebp
f01014dc:	53                   	push   %ebx
f01014dd:	83 ec 08             	sub    $0x8,%esp
f01014e0:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f01014e3:	6a 00                	push   $0x0
f01014e5:	ff 75 0c             	pushl  0xc(%ebp)
f01014e8:	ff 75 08             	pushl  0x8(%ebp)
f01014eb:	e8 29 fe ff ff       	call   f0101319 <pgdir_walk>
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
f01014f0:	83 c4 10             	add    $0x10,%esp
f01014f3:	85 c0                	test   %eax,%eax
f01014f5:	74 5a                	je     f0101551 <page_lookup+0x78>
		return NULL; //no page mapped at va (not even a table)
	}

	if(pte_store != 0){
f01014f7:	85 db                	test   %ebx,%ebx
f01014f9:	74 02                	je     f01014fd <page_lookup+0x24>
		*pte_store = pte_Ptr;
f01014fb:	89 03                	mov    %eax,(%ebx)
	}

	pte_t pte = *pte_Ptr;
f01014fd:	8b 00                	mov    (%eax),%eax
	if(pte & PTE_P){ //if present
f01014ff:	a8 01                	test   $0x1,%al
f0101501:	74 55                	je     f0101558 <page_lookup+0x7f>
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
f0101503:	8b 15 24 44 23 f0    	mov    0xf0234424,%edx
f0101509:	89 c1                	mov    %eax,%ecx
f010150b:	c1 e9 0c             	shr    $0xc,%ecx
f010150e:	39 d1                	cmp    %edx,%ecx
f0101510:	72 19                	jb     f010152b <page_lookup+0x52>
f0101512:	68 b4 7c 10 f0       	push   $0xf0107cb4
f0101517:	68 51 75 10 f0       	push   $0xf0107551
f010151c:	68 e3 02 00 00       	push   $0x2e3
f0101521:	68 e1 84 10 f0       	push   $0xf01084e1
f0101526:	e8 21 eb ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010152b:	c1 e8 0c             	shr    $0xc,%eax
f010152e:	39 c2                	cmp    %eax,%edx
f0101530:	77 14                	ja     f0101546 <page_lookup+0x6d>
		panic("pa2page called with invalid pa");
f0101532:	83 ec 04             	sub    $0x4,%esp
f0101535:	68 74 7c 10 f0       	push   $0xf0107c74
f010153a:	6a 51                	push   $0x51
f010153c:	68 ed 84 10 f0       	push   $0xf01084ed
f0101541:	e8 06 eb ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0101546:	8b 15 2c 44 23 f0    	mov    0xf023442c,%edx
f010154c:	8d 04 c2             	lea    (%edx,%eax,8),%eax
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
f010154f:	eb 0c                	jmp    f010155d <page_lookup+0x84>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
		return NULL; //no page mapped at va (not even a table)
f0101551:	b8 00 00 00 00       	mov    $0x0,%eax
f0101556:	eb 05                	jmp    f010155d <page_lookup+0x84>
	pte_t pte = *pte_Ptr;
	if(pte & PTE_P){ //if present
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
	}else{
		return NULL;
f0101558:	b8 00 00 00 00       	mov    $0x0,%eax
	}
}
f010155d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101560:	c9                   	leave  
f0101561:	c3                   	ret    

f0101562 <print_fl>:

}

void
print_fl()
{	
f0101562:	55                   	push   %ebp
f0101563:	89 e5                	mov    %esp,%ebp
f0101565:	53                   	push   %ebx
f0101566:	83 ec 10             	sub    $0x10,%esp
	struct PageInfo *pp = page_free_list;
f0101569:	8b 1d 40 32 23 f0    	mov    0xf0233240,%ebx
	cprintf("Freelist is: ");
f010156f:	68 65 86 10 f0       	push   $0xf0108665
f0101574:	e8 98 26 00 00       	call   f0103c11 <cprintf>
	while(pp != NULL){
f0101579:	83 c4 10             	add    $0x10,%esp
f010157c:	eb 1e                	jmp    f010159c <print_fl+0x3a>
		cprintf("-> p%x ", (pp-pages));
f010157e:	83 ec 08             	sub    $0x8,%esp
f0101581:	89 d8                	mov    %ebx,%eax
f0101583:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0101589:	c1 f8 03             	sar    $0x3,%eax
f010158c:	50                   	push   %eax
f010158d:	68 73 86 10 f0       	push   $0xf0108673
f0101592:	e8 7a 26 00 00       	call   f0103c11 <cprintf>
		pp = pp->pp_link;
f0101597:	8b 1b                	mov    (%ebx),%ebx
f0101599:	83 c4 10             	add    $0x10,%esp
void
print_fl()
{	
	struct PageInfo *pp = page_free_list;
	cprintf("Freelist is: ");
	while(pp != NULL){
f010159c:	85 db                	test   %ebx,%ebx
f010159e:	75 de                	jne    f010157e <print_fl+0x1c>
		cprintf("-> p%x ", (pp-pages));
		pp = pp->pp_link;
	}
	cprintf("->end\n");
f01015a0:	83 ec 0c             	sub    $0xc,%esp
f01015a3:	68 7b 86 10 f0       	push   $0xf010867b
f01015a8:	e8 64 26 00 00       	call   f0103c11 <cprintf>
}
f01015ad:	83 c4 10             	add    $0x10,%esp
f01015b0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01015b3:	c9                   	leave  
f01015b4:	c3                   	ret    

f01015b5 <tlb_invalidate>:
// Invalidate a TLB entry, but only if the page tables being
// edited are the ones currently in use by the processor.
//
void
tlb_invalidate(pde_t *pgdir, void *va)
{
f01015b5:	55                   	push   %ebp
f01015b6:	89 e5                	mov    %esp,%ebp
f01015b8:	83 ec 08             	sub    $0x8,%esp
	// Flush the entry only if we're modifying the current address space.
	if (!curenv || curenv->env_pgdir == pgdir)
f01015bb:	e8 6e 51 00 00       	call   f010672e <cpunum>
f01015c0:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01015c3:	01 c2                	add    %eax,%edx
f01015c5:	01 d2                	add    %edx,%edx
f01015c7:	01 c2                	add    %eax,%edx
f01015c9:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01015cc:	83 3c 85 08 50 23 f0 	cmpl   $0x0,-0xfdcaff8(,%eax,4)
f01015d3:	00 
f01015d4:	74 20                	je     f01015f6 <tlb_invalidate+0x41>
f01015d6:	e8 53 51 00 00       	call   f010672e <cpunum>
f01015db:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01015de:	01 c2                	add    %eax,%edx
f01015e0:	01 d2                	add    %edx,%edx
f01015e2:	01 c2                	add    %eax,%edx
f01015e4:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01015e7:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f01015ee:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01015f1:	39 48 60             	cmp    %ecx,0x60(%eax)
f01015f4:	75 06                	jne    f01015fc <tlb_invalidate+0x47>
}

static inline void
invlpg(void *addr)
{
	asm volatile("invlpg (%0)" : : "r" (addr) : "memory");
f01015f6:	8b 45 0c             	mov    0xc(%ebp),%eax
f01015f9:	0f 01 38             	invlpg (%eax)
		invlpg(va);
}
f01015fc:	c9                   	leave  
f01015fd:	c3                   	ret    

f01015fe <page_remove>:
// Hint: The TA solution is implemented using page_lookup,
// 	tlb_invalidate, and page_decref.
//
void
page_remove(pde_t *pgdir, void *va)
{
f01015fe:	55                   	push   %ebp
f01015ff:	89 e5                	mov    %esp,%ebp
f0101601:	56                   	push   %esi
f0101602:	53                   	push   %ebx
f0101603:	8b 5d 08             	mov    0x8(%ebp),%ebx
f0101606:	8b 75 0c             	mov    0xc(%ebp),%esi
	// Fill this function in
	//pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	if( page_lookup(pgdir, va, 0) == NULL){
f0101609:	83 ec 04             	sub    $0x4,%esp
f010160c:	6a 00                	push   $0x0
f010160e:	56                   	push   %esi
f010160f:	53                   	push   %ebx
f0101610:	e8 c4 fe ff ff       	call   f01014d9 <page_lookup>
f0101615:	83 c4 10             	add    $0x10,%esp
f0101618:	85 c0                	test   %eax,%eax
f010161a:	74 7b                	je     f0101697 <page_remove+0x99>
		return;
	}
	tlb_invalidate(pgdir, va);
f010161c:	83 ec 08             	sub    $0x8,%esp
f010161f:	56                   	push   %esi
f0101620:	53                   	push   %ebx
f0101621:	e8 8f ff ff ff       	call   f01015b5 <tlb_invalidate>
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f0101626:	83 c4 0c             	add    $0xc,%esp
f0101629:	6a 00                	push   $0x0
f010162b:	56                   	push   %esi
f010162c:	53                   	push   %ebx
f010162d:	e8 e7 fc ff ff       	call   f0101319 <pgdir_walk>
	pte_t pte = *pte_Ptr;
f0101632:	8b 10                	mov    (%eax),%edx
	*pte_Ptr = 0;
f0101634:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	assert(PGNUM(PTE_ADDR(pte)) < npages);
f010163a:	8b 0d 24 44 23 f0    	mov    0xf0234424,%ecx
f0101640:	89 d0                	mov    %edx,%eax
f0101642:	c1 e8 0c             	shr    $0xc,%eax
f0101645:	83 c4 10             	add    $0x10,%esp
f0101648:	39 c8                	cmp    %ecx,%eax
f010164a:	72 19                	jb     f0101665 <page_remove+0x67>
f010164c:	68 82 86 10 f0       	push   $0xf0108682
f0101651:	68 51 75 10 f0       	push   $0xf0107551
f0101656:	68 05 03 00 00       	push   $0x305
f010165b:	68 e1 84 10 f0       	push   $0xf01084e1
f0101660:	e8 e7 e9 ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101665:	89 d0                	mov    %edx,%eax
f0101667:	c1 e8 0c             	shr    $0xc,%eax
f010166a:	39 c1                	cmp    %eax,%ecx
f010166c:	77 14                	ja     f0101682 <page_remove+0x84>
		panic("pa2page called with invalid pa");
f010166e:	83 ec 04             	sub    $0x4,%esp
f0101671:	68 74 7c 10 f0       	push   $0xf0107c74
f0101676:	6a 51                	push   $0x51
f0101678:	68 ed 84 10 f0       	push   $0xf01084ed
f010167d:	e8 ca e9 ff ff       	call   f010004c <_panic>
	struct PageInfo* pp = pa2page( PTE_ADDR(pte));
	page_decref(pp);
f0101682:	83 ec 0c             	sub    $0xc,%esp
f0101685:	8b 15 2c 44 23 f0    	mov    0xf023442c,%edx
f010168b:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f010168e:	50                   	push   %eax
f010168f:	e8 61 fc ff ff       	call   f01012f5 <page_decref>
f0101694:	83 c4 10             	add    $0x10,%esp

}
f0101697:	8d 65 f8             	lea    -0x8(%ebp),%esp
f010169a:	5b                   	pop    %ebx
f010169b:	5e                   	pop    %esi
f010169c:	5d                   	pop    %ebp
f010169d:	c3                   	ret    

f010169e <page_insert>:
// Hint: The TA solution is implemented using pgdir_walk, page_remove,
// and page2pa.
//
int
page_insert(pde_t *pgdir, struct PageInfo *pp, void *va, int perm)
{
f010169e:	55                   	push   %ebp
f010169f:	89 e5                	mov    %esp,%ebp
f01016a1:	57                   	push   %edi
f01016a2:	56                   	push   %esi
f01016a3:	53                   	push   %ebx
f01016a4:	83 ec 10             	sub    $0x10,%esp
f01016a7:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01016aa:	8b 7d 10             	mov    0x10(%ebp),%edi
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
f01016ad:	6a 01                	push   $0x1
f01016af:	57                   	push   %edi
f01016b0:	ff 75 08             	pushl  0x8(%ebp)
f01016b3:	e8 61 fc ff ff       	call   f0101319 <pgdir_walk>
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
f01016b8:	83 c4 10             	add    $0x10,%esp
f01016bb:	85 c0                	test   %eax,%eax
f01016bd:	74 38                	je     f01016f7 <page_insert+0x59>
f01016bf:	89 c6                	mov    %eax,%esi
		return -E_NO_MEM; //page table couldn't be allocated
	}
	pte_t pte = *pte_Ptr;
f01016c1:	8b 00                	mov    (%eax),%eax
	pp->pp_ref++;
f01016c3:	66 ff 43 04          	incw   0x4(%ebx)
	if(pte & PTE_P){ //check if there is already a page mapped at 'va'
f01016c7:	a8 01                	test   $0x1,%al
f01016c9:	74 0f                	je     f01016da <page_insert+0x3c>
		page_remove(pgdir, va); //remove the exising page
f01016cb:	83 ec 08             	sub    $0x8,%esp
f01016ce:	57                   	push   %edi
f01016cf:	ff 75 08             	pushl  0x8(%ebp)
f01016d2:	e8 27 ff ff ff       	call   f01015fe <page_remove>
f01016d7:	83 c4 10             	add    $0x10,%esp
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
f01016da:	2b 1d 2c 44 23 f0    	sub    0xf023442c,%ebx
f01016e0:	c1 fb 03             	sar    $0x3,%ebx
f01016e3:	c1 e3 0c             	shl    $0xc,%ebx
f01016e6:	8b 45 14             	mov    0x14(%ebp),%eax
f01016e9:	83 c8 01             	or     $0x1,%eax
f01016ec:	09 c3                	or     %eax,%ebx
f01016ee:	89 1e                	mov    %ebx,(%esi)
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
f01016f0:	b8 00 00 00 00       	mov    $0x0,%eax
f01016f5:	eb 05                	jmp    f01016fc <page_insert+0x5e>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
		return -E_NO_MEM; //page table couldn't be allocated
f01016f7:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
}
f01016fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01016ff:	5b                   	pop    %ebx
f0101700:	5e                   	pop    %esi
f0101701:	5f                   	pop    %edi
f0101702:	5d                   	pop    %ebp
f0101703:	c3                   	ret    

f0101704 <mmio_map_region>:
// location.  Return the base of the reserved region.  size does *not*
// have to be multiple of PGSIZE.
//
void *
mmio_map_region(physaddr_t pa, size_t size)
{
f0101704:	55                   	push   %ebp
f0101705:	89 e5                	mov    %esp,%ebp
f0101707:	53                   	push   %ebx
f0101708:	83 ec 04             	sub    $0x4,%esp
f010170b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//
	// Your code here:

	//round up the size
	//size_t round_Up_Size;
	if(size % PGSIZE != 0){
f010170e:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
f0101714:	74 0c                	je     f0101722 <mmio_map_region+0x1e>
		size = ((size / PGSIZE) + 1)*PGSIZE;
f0101716:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
f010171c:	8d 9b 00 10 00 00    	lea    0x1000(%ebx),%ebx
	}//else{
	//cprintf("In mmio_map_region, the rounded size is: %d\n\n\n", size);
	//	round_Up_Size = size;
	//}
	//check overflow
	if(base + size > MMIOLIM){
f0101722:	8b 15 00 53 12 f0    	mov    0xf0125300,%edx
f0101728:	8d 04 13             	lea    (%ebx,%edx,1),%eax
f010172b:	3d 00 00 c0 ef       	cmp    $0xefc00000,%eax
f0101730:	76 17                	jbe    f0101749 <mmio_map_region+0x45>
		panic("MMIOLIM overflow!!\n");
f0101732:	83 ec 04             	sub    $0x4,%esp
f0101735:	68 a0 86 10 f0       	push   $0xf01086a0
f010173a:	68 4d 03 00 00       	push   $0x34d
f010173f:	68 e1 84 10 f0       	push   $0xf01084e1
f0101744:	e8 03 e9 ff ff       	call   f010004c <_panic>
	}
	//boot_map_region(pde_t *pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm)
	boot_map_region(kern_pgdir, base, size, pa, PTE_PCD|PTE_PWT|PTE_P|PTE_W);
f0101749:	83 ec 08             	sub    $0x8,%esp
f010174c:	6a 1b                	push   $0x1b
f010174e:	ff 75 08             	pushl  0x8(%ebp)
f0101751:	89 d9                	mov    %ebx,%ecx
f0101753:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0101758:	e8 a0 fc ff ff       	call   f01013fd <boot_map_region>
	ret = base;
f010175d:	a1 00 53 12 f0       	mov    0xf0125300,%eax
	base += size;
f0101762:	01 c3                	add    %eax,%ebx
f0101764:	89 1d 00 53 12 f0    	mov    %ebx,0xf0125300
	return ( (void*)ret );
	//panic("mmio_map_region not implemented");
}
f010176a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010176d:	c9                   	leave  
f010176e:	c3                   	ret    

f010176f <mem_init>:
//
// From UTOP to ULIM, the user is allowed to read but not write.
// Above ULIM the user cannot read or write.
void
mem_init(void)
{
f010176f:	55                   	push   %ebp
f0101770:	89 e5                	mov    %esp,%ebp
f0101772:	57                   	push   %edi
f0101773:	56                   	push   %esi
f0101774:	53                   	push   %ebx
f0101775:	83 ec 3c             	sub    $0x3c,%esp
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101778:	8b 1d 20 3f 23 f0    	mov    0xf0233f20,%ebx
static void
detect_memory(void)
{
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;
f010177e:	be 00 00 00 00       	mov    $0x0,%esi

	e = e820_map.entries;
f0101783:	b8 24 3f 23 f0       	mov    $0xf0233f24,%eax
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101788:	b9 00 00 00 00       	mov    $0x0,%ecx
f010178d:	eb 1d                	jmp    f01017ac <mem_init+0x3d>
		if (e->addr >= mem_max)
f010178f:	8b 10                	mov    (%eax),%edx
f0101791:	83 78 04 00          	cmpl   $0x0,0x4(%eax)
f0101795:	75 11                	jne    f01017a8 <mem_init+0x39>
f0101797:	81 fa ff ff ff 0f    	cmp    $0xfffffff,%edx
f010179d:	77 09                	ja     f01017a8 <mem_init+0x39>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
f010179f:	03 50 08             	add    0x8(%eax),%edx
f01017a2:	39 d6                	cmp    %edx,%esi
f01017a4:	73 02                	jae    f01017a8 <mem_init+0x39>
f01017a6:	89 d6                	mov    %edx,%esi
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f01017a8:	41                   	inc    %ecx
f01017a9:	83 c0 14             	add    $0x14,%eax
f01017ac:	39 d9                	cmp    %ebx,%ecx
f01017ae:	75 df                	jne    f010178f <mem_init+0x20>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
	}

	// Limit memory to 256MB.
	mem = MIN(mem, mem_max);
f01017b0:	89 f0                	mov    %esi,%eax
f01017b2:	81 fe 00 00 00 10    	cmp    $0x10000000,%esi
f01017b8:	76 05                	jbe    f01017bf <mem_init+0x50>
f01017ba:	b8 00 00 00 10       	mov    $0x10000000,%eax
	npages = mem / PGSIZE;
f01017bf:	89 c2                	mov    %eax,%edx
f01017c1:	c1 ea 0c             	shr    $0xc,%edx
f01017c4:	89 15 24 44 23 f0    	mov    %edx,0xf0234424
	cprintf("E820: physical memory %uMB\n", mem / 1024 / 1024);
f01017ca:	83 ec 08             	sub    $0x8,%esp
f01017cd:	c1 e8 14             	shr    $0x14,%eax
f01017d0:	50                   	push   %eax
f01017d1:	68 b4 86 10 f0       	push   $0xf01086b4
f01017d6:	e8 36 24 00 00       	call   f0103c11 <cprintf>
	// Remove this line when you're ready to test this function.
	//panic("mem_init: This function is not finished\n");

	//////////////////////////////////////////////////////////////////////
	// create initial page directory.
	kern_pgdir = (pde_t *) boot_alloc(PGSIZE);
f01017db:	b8 00 10 00 00       	mov    $0x1000,%eax
f01017e0:	e8 fe f4 ff ff       	call   f0100ce3 <boot_alloc>
f01017e5:	a3 28 44 23 f0       	mov    %eax,0xf0234428
	memset(kern_pgdir, 0, PGSIZE);
f01017ea:	83 c4 0c             	add    $0xc,%esp
f01017ed:	68 00 10 00 00       	push   $0x1000
f01017f2:	6a 00                	push   $0x0
f01017f4:	50                   	push   %eax
f01017f5:	e8 2e 48 00 00       	call   f0106028 <memset>
	// a virtual page table at virtual address UVPT.
	// (For now, you don't have understand the greater purpose of the
	// following line.)

	// Permissions: kernel R, user R
	kern_pgdir[PDX(UVPT)] = PADDR(kern_pgdir) | PTE_U | PTE_P;
f01017fa:	a1 28 44 23 f0       	mov    0xf0234428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01017ff:	83 c4 10             	add    $0x10,%esp
f0101802:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0101807:	77 15                	ja     f010181e <mem_init+0xaf>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0101809:	50                   	push   %eax
f010180a:	68 0c 75 10 f0       	push   $0xf010750c
f010180f:	68 8e 00 00 00       	push   $0x8e
f0101814:	68 e1 84 10 f0       	push   $0xf01084e1
f0101819:	e8 2e e8 ff ff       	call   f010004c <_panic>
f010181e:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101824:	83 ca 05             	or     $0x5,%edx
f0101827:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)
	// The kernel uses this array to keep track of physical pages: for
	// each physical page, there is a corresponding struct PageInfo in this
	// array.  'npages' is the number of physical pages in memory.  Use memset
	// to initialize all fields of each struct PageInfo to 0.
	// Your code goes here:
	pages = boot_alloc(npages * sizeof(struct PageInfo)); 
f010182d:	a1 24 44 23 f0       	mov    0xf0234424,%eax
f0101832:	c1 e0 03             	shl    $0x3,%eax
f0101835:	e8 a9 f4 ff ff       	call   f0100ce3 <boot_alloc>
f010183a:	a3 2c 44 23 f0       	mov    %eax,0xf023442c

	//Lab3 Exercise 1: allocate and map the envs array
	//pages = boot_alloc(npages * sizeof(struct PageInfo)); 
	envs = (struct Env *)boot_alloc(NENV * sizeof(struct Env));
f010183f:	b8 00 f0 01 00       	mov    $0x1f000,%eax
f0101844:	e8 9a f4 ff ff       	call   f0100ce3 <boot_alloc>
f0101849:	a3 44 32 23 f0       	mov    %eax,0xf0233244

	//initialize all fields of each struct PageInfo to 0.
	memset(pages, 0, npages * sizeof(struct PageInfo)); //npages * 8 is the total number of bytes
f010184e:	83 ec 04             	sub    $0x4,%esp
f0101851:	a1 24 44 23 f0       	mov    0xf0234424,%eax
f0101856:	c1 e0 03             	shl    $0x3,%eax
f0101859:	50                   	push   %eax
f010185a:	6a 00                	push   $0x0
f010185c:	ff 35 2c 44 23 f0    	pushl  0xf023442c
f0101862:	e8 c1 47 00 00       	call   f0106028 <memset>
	// Now that we've allocated the initial kernel data structures, we set
	// up the list of free physical pages. Once we've done so, all further
	// memory management will go through the page_* functions. In
	// particular, we can now map memory using boot_map_region
	// or page_insert
	page_init();
f0101867:	e8 0d f8 ff ff       	call   f0101079 <page_init>

	check_page_free_list(1);
f010186c:	b8 01 00 00 00       	mov    $0x1,%eax
f0101871:	e8 1a f5 ff ff       	call   f0100d90 <check_page_free_list>
	int nfree;
	struct PageInfo *fl;
	char *c;
	int i;

	if (!pages)
f0101876:	83 c4 10             	add    $0x10,%esp
f0101879:	83 3d 2c 44 23 f0 00 	cmpl   $0x0,0xf023442c
f0101880:	75 17                	jne    f0101899 <mem_init+0x12a>
		panic("'pages' is a null pointer!");
f0101882:	83 ec 04             	sub    $0x4,%esp
f0101885:	68 d0 86 10 f0       	push   $0xf01086d0
f010188a:	68 fa 03 00 00       	push   $0x3fa
f010188f:	68 e1 84 10 f0       	push   $0xf01084e1
f0101894:	e8 b3 e7 ff ff       	call   f010004c <_panic>

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f0101899:	a1 40 32 23 f0       	mov    0xf0233240,%eax
f010189e:	bb 00 00 00 00       	mov    $0x0,%ebx
f01018a3:	eb 03                	jmp    f01018a8 <mem_init+0x139>
		++nfree;
f01018a5:	43                   	inc    %ebx

	if (!pages)
		panic("'pages' is a null pointer!");

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f01018a6:	8b 00                	mov    (%eax),%eax
f01018a8:	85 c0                	test   %eax,%eax
f01018aa:	75 f9                	jne    f01018a5 <mem_init+0x136>
		++nfree;

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f01018ac:	83 ec 0c             	sub    $0xc,%esp
f01018af:	6a 00                	push   $0x0
f01018b1:	e8 6c f9 ff ff       	call   f0101222 <page_alloc>
f01018b6:	89 c7                	mov    %eax,%edi
f01018b8:	83 c4 10             	add    $0x10,%esp
f01018bb:	85 c0                	test   %eax,%eax
f01018bd:	75 19                	jne    f01018d8 <mem_init+0x169>
f01018bf:	68 eb 86 10 f0       	push   $0xf01086eb
f01018c4:	68 51 75 10 f0       	push   $0xf0107551
f01018c9:	68 02 04 00 00       	push   $0x402
f01018ce:	68 e1 84 10 f0       	push   $0xf01084e1
f01018d3:	e8 74 e7 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f01018d8:	83 ec 0c             	sub    $0xc,%esp
f01018db:	6a 00                	push   $0x0
f01018dd:	e8 40 f9 ff ff       	call   f0101222 <page_alloc>
f01018e2:	89 c6                	mov    %eax,%esi
f01018e4:	83 c4 10             	add    $0x10,%esp
f01018e7:	85 c0                	test   %eax,%eax
f01018e9:	75 19                	jne    f0101904 <mem_init+0x195>
f01018eb:	68 01 87 10 f0       	push   $0xf0108701
f01018f0:	68 51 75 10 f0       	push   $0xf0107551
f01018f5:	68 03 04 00 00       	push   $0x403
f01018fa:	68 e1 84 10 f0       	push   $0xf01084e1
f01018ff:	e8 48 e7 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0101904:	83 ec 0c             	sub    $0xc,%esp
f0101907:	6a 00                	push   $0x0
f0101909:	e8 14 f9 ff ff       	call   f0101222 <page_alloc>
f010190e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101911:	83 c4 10             	add    $0x10,%esp
f0101914:	85 c0                	test   %eax,%eax
f0101916:	75 19                	jne    f0101931 <mem_init+0x1c2>
f0101918:	68 17 87 10 f0       	push   $0xf0108717
f010191d:	68 51 75 10 f0       	push   $0xf0107551
f0101922:	68 04 04 00 00       	push   $0x404
f0101927:	68 e1 84 10 f0       	push   $0xf01084e1
f010192c:	e8 1b e7 ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101931:	39 f7                	cmp    %esi,%edi
f0101933:	75 19                	jne    f010194e <mem_init+0x1df>
f0101935:	68 2d 87 10 f0       	push   $0xf010872d
f010193a:	68 51 75 10 f0       	push   $0xf0107551
f010193f:	68 07 04 00 00       	push   $0x407
f0101944:	68 e1 84 10 f0       	push   $0xf01084e1
f0101949:	e8 fe e6 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f010194e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101951:	39 c6                	cmp    %eax,%esi
f0101953:	74 04                	je     f0101959 <mem_init+0x1ea>
f0101955:	39 c7                	cmp    %eax,%edi
f0101957:	75 19                	jne    f0101972 <mem_init+0x203>
f0101959:	68 d4 7c 10 f0       	push   $0xf0107cd4
f010195e:	68 51 75 10 f0       	push   $0xf0107551
f0101963:	68 08 04 00 00       	push   $0x408
f0101968:	68 e1 84 10 f0       	push   $0xf01084e1
f010196d:	e8 da e6 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101972:	8b 0d 2c 44 23 f0    	mov    0xf023442c,%ecx
	assert(page2pa(pp0) < npages*PGSIZE);
f0101978:	8b 15 24 44 23 f0    	mov    0xf0234424,%edx
f010197e:	c1 e2 0c             	shl    $0xc,%edx
f0101981:	89 f8                	mov    %edi,%eax
f0101983:	29 c8                	sub    %ecx,%eax
f0101985:	c1 f8 03             	sar    $0x3,%eax
f0101988:	c1 e0 0c             	shl    $0xc,%eax
f010198b:	39 d0                	cmp    %edx,%eax
f010198d:	72 19                	jb     f01019a8 <mem_init+0x239>
f010198f:	68 3f 87 10 f0       	push   $0xf010873f
f0101994:	68 51 75 10 f0       	push   $0xf0107551
f0101999:	68 09 04 00 00       	push   $0x409
f010199e:	68 e1 84 10 f0       	push   $0xf01084e1
f01019a3:	e8 a4 e6 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp1) < npages*PGSIZE);
f01019a8:	89 f0                	mov    %esi,%eax
f01019aa:	29 c8                	sub    %ecx,%eax
f01019ac:	c1 f8 03             	sar    $0x3,%eax
f01019af:	c1 e0 0c             	shl    $0xc,%eax
f01019b2:	39 c2                	cmp    %eax,%edx
f01019b4:	77 19                	ja     f01019cf <mem_init+0x260>
f01019b6:	68 5c 87 10 f0       	push   $0xf010875c
f01019bb:	68 51 75 10 f0       	push   $0xf0107551
f01019c0:	68 0a 04 00 00       	push   $0x40a
f01019c5:	68 e1 84 10 f0       	push   $0xf01084e1
f01019ca:	e8 7d e6 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp2) < npages*PGSIZE);
f01019cf:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01019d2:	29 c8                	sub    %ecx,%eax
f01019d4:	c1 f8 03             	sar    $0x3,%eax
f01019d7:	c1 e0 0c             	shl    $0xc,%eax
f01019da:	39 c2                	cmp    %eax,%edx
f01019dc:	77 19                	ja     f01019f7 <mem_init+0x288>
f01019de:	68 79 87 10 f0       	push   $0xf0108779
f01019e3:	68 51 75 10 f0       	push   $0xf0107551
f01019e8:	68 0b 04 00 00       	push   $0x40b
f01019ed:	68 e1 84 10 f0       	push   $0xf01084e1
f01019f2:	e8 55 e6 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f01019f7:	a1 40 32 23 f0       	mov    0xf0233240,%eax
f01019fc:	89 45 d0             	mov    %eax,-0x30(%ebp)
	page_free_list = 0;
f01019ff:	c7 05 40 32 23 f0 00 	movl   $0x0,0xf0233240
f0101a06:	00 00 00 

	// should be no free memory
	assert(!page_alloc(0));
f0101a09:	83 ec 0c             	sub    $0xc,%esp
f0101a0c:	6a 00                	push   $0x0
f0101a0e:	e8 0f f8 ff ff       	call   f0101222 <page_alloc>
f0101a13:	83 c4 10             	add    $0x10,%esp
f0101a16:	85 c0                	test   %eax,%eax
f0101a18:	74 19                	je     f0101a33 <mem_init+0x2c4>
f0101a1a:	68 96 87 10 f0       	push   $0xf0108796
f0101a1f:	68 51 75 10 f0       	push   $0xf0107551
f0101a24:	68 12 04 00 00       	push   $0x412
f0101a29:	68 e1 84 10 f0       	push   $0xf01084e1
f0101a2e:	e8 19 e6 ff ff       	call   f010004c <_panic>

	// free and re-allocate?
	page_free(pp0);
f0101a33:	83 ec 0c             	sub    $0xc,%esp
f0101a36:	57                   	push   %edi
f0101a37:	e8 5c f8 ff ff       	call   f0101298 <page_free>
	page_free(pp1);
f0101a3c:	89 34 24             	mov    %esi,(%esp)
f0101a3f:	e8 54 f8 ff ff       	call   f0101298 <page_free>
	page_free(pp2);
f0101a44:	83 c4 04             	add    $0x4,%esp
f0101a47:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101a4a:	e8 49 f8 ff ff       	call   f0101298 <page_free>
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101a4f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101a56:	e8 c7 f7 ff ff       	call   f0101222 <page_alloc>
f0101a5b:	89 c6                	mov    %eax,%esi
f0101a5d:	83 c4 10             	add    $0x10,%esp
f0101a60:	85 c0                	test   %eax,%eax
f0101a62:	75 19                	jne    f0101a7d <mem_init+0x30e>
f0101a64:	68 eb 86 10 f0       	push   $0xf01086eb
f0101a69:	68 51 75 10 f0       	push   $0xf0107551
f0101a6e:	68 19 04 00 00       	push   $0x419
f0101a73:	68 e1 84 10 f0       	push   $0xf01084e1
f0101a78:	e8 cf e5 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0101a7d:	83 ec 0c             	sub    $0xc,%esp
f0101a80:	6a 00                	push   $0x0
f0101a82:	e8 9b f7 ff ff       	call   f0101222 <page_alloc>
f0101a87:	89 c7                	mov    %eax,%edi
f0101a89:	83 c4 10             	add    $0x10,%esp
f0101a8c:	85 c0                	test   %eax,%eax
f0101a8e:	75 19                	jne    f0101aa9 <mem_init+0x33a>
f0101a90:	68 01 87 10 f0       	push   $0xf0108701
f0101a95:	68 51 75 10 f0       	push   $0xf0107551
f0101a9a:	68 1a 04 00 00       	push   $0x41a
f0101a9f:	68 e1 84 10 f0       	push   $0xf01084e1
f0101aa4:	e8 a3 e5 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0101aa9:	83 ec 0c             	sub    $0xc,%esp
f0101aac:	6a 00                	push   $0x0
f0101aae:	e8 6f f7 ff ff       	call   f0101222 <page_alloc>
f0101ab3:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101ab6:	83 c4 10             	add    $0x10,%esp
f0101ab9:	85 c0                	test   %eax,%eax
f0101abb:	75 19                	jne    f0101ad6 <mem_init+0x367>
f0101abd:	68 17 87 10 f0       	push   $0xf0108717
f0101ac2:	68 51 75 10 f0       	push   $0xf0107551
f0101ac7:	68 1b 04 00 00       	push   $0x41b
f0101acc:	68 e1 84 10 f0       	push   $0xf01084e1
f0101ad1:	e8 76 e5 ff ff       	call   f010004c <_panic>
	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101ad6:	39 fe                	cmp    %edi,%esi
f0101ad8:	75 19                	jne    f0101af3 <mem_init+0x384>
f0101ada:	68 2d 87 10 f0       	push   $0xf010872d
f0101adf:	68 51 75 10 f0       	push   $0xf0107551
f0101ae4:	68 1d 04 00 00       	push   $0x41d
f0101ae9:	68 e1 84 10 f0       	push   $0xf01084e1
f0101aee:	e8 59 e5 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101af3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101af6:	39 c7                	cmp    %eax,%edi
f0101af8:	74 04                	je     f0101afe <mem_init+0x38f>
f0101afa:	39 c6                	cmp    %eax,%esi
f0101afc:	75 19                	jne    f0101b17 <mem_init+0x3a8>
f0101afe:	68 d4 7c 10 f0       	push   $0xf0107cd4
f0101b03:	68 51 75 10 f0       	push   $0xf0107551
f0101b08:	68 1e 04 00 00       	push   $0x41e
f0101b0d:	68 e1 84 10 f0       	push   $0xf01084e1
f0101b12:	e8 35 e5 ff ff       	call   f010004c <_panic>
	assert(!page_alloc(0));
f0101b17:	83 ec 0c             	sub    $0xc,%esp
f0101b1a:	6a 00                	push   $0x0
f0101b1c:	e8 01 f7 ff ff       	call   f0101222 <page_alloc>
f0101b21:	83 c4 10             	add    $0x10,%esp
f0101b24:	85 c0                	test   %eax,%eax
f0101b26:	74 19                	je     f0101b41 <mem_init+0x3d2>
f0101b28:	68 96 87 10 f0       	push   $0xf0108796
f0101b2d:	68 51 75 10 f0       	push   $0xf0107551
f0101b32:	68 1f 04 00 00       	push   $0x41f
f0101b37:	68 e1 84 10 f0       	push   $0xf01084e1
f0101b3c:	e8 0b e5 ff ff       	call   f010004c <_panic>
f0101b41:	89 f0                	mov    %esi,%eax
f0101b43:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0101b49:	c1 f8 03             	sar    $0x3,%eax
f0101b4c:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101b4f:	89 c2                	mov    %eax,%edx
f0101b51:	c1 ea 0c             	shr    $0xc,%edx
f0101b54:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f0101b5a:	72 12                	jb     f0101b6e <mem_init+0x3ff>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101b5c:	50                   	push   %eax
f0101b5d:	68 e8 74 10 f0       	push   $0xf01074e8
f0101b62:	6a 58                	push   $0x58
f0101b64:	68 ed 84 10 f0       	push   $0xf01084ed
f0101b69:	e8 de e4 ff ff       	call   f010004c <_panic>

	// test flags
	memset(page2kva(pp0), 1, PGSIZE);
f0101b6e:	83 ec 04             	sub    $0x4,%esp
f0101b71:	68 00 10 00 00       	push   $0x1000
f0101b76:	6a 01                	push   $0x1
f0101b78:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101b7d:	50                   	push   %eax
f0101b7e:	e8 a5 44 00 00       	call   f0106028 <memset>
	page_free(pp0);
f0101b83:	89 34 24             	mov    %esi,(%esp)
f0101b86:	e8 0d f7 ff ff       	call   f0101298 <page_free>
	assert((pp = page_alloc(ALLOC_ZERO)));
f0101b8b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
f0101b92:	e8 8b f6 ff ff       	call   f0101222 <page_alloc>
f0101b97:	83 c4 10             	add    $0x10,%esp
f0101b9a:	85 c0                	test   %eax,%eax
f0101b9c:	75 19                	jne    f0101bb7 <mem_init+0x448>
f0101b9e:	68 a5 87 10 f0       	push   $0xf01087a5
f0101ba3:	68 51 75 10 f0       	push   $0xf0107551
f0101ba8:	68 24 04 00 00       	push   $0x424
f0101bad:	68 e1 84 10 f0       	push   $0xf01084e1
f0101bb2:	e8 95 e4 ff ff       	call   f010004c <_panic>
	assert(pp && pp0 == pp);
f0101bb7:	39 c6                	cmp    %eax,%esi
f0101bb9:	74 19                	je     f0101bd4 <mem_init+0x465>
f0101bbb:	68 c3 87 10 f0       	push   $0xf01087c3
f0101bc0:	68 51 75 10 f0       	push   $0xf0107551
f0101bc5:	68 25 04 00 00       	push   $0x425
f0101bca:	68 e1 84 10 f0       	push   $0xf01084e1
f0101bcf:	e8 78 e4 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101bd4:	89 f0                	mov    %esi,%eax
f0101bd6:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0101bdc:	c1 f8 03             	sar    $0x3,%eax
f0101bdf:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101be2:	89 c2                	mov    %eax,%edx
f0101be4:	c1 ea 0c             	shr    $0xc,%edx
f0101be7:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f0101bed:	72 12                	jb     f0101c01 <mem_init+0x492>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101bef:	50                   	push   %eax
f0101bf0:	68 e8 74 10 f0       	push   $0xf01074e8
f0101bf5:	6a 58                	push   $0x58
f0101bf7:	68 ed 84 10 f0       	push   $0xf01084ed
f0101bfc:	e8 4b e4 ff ff       	call   f010004c <_panic>
f0101c01:	8d 90 00 10 00 f0    	lea    -0xffff000(%eax),%edx
	return (void *)(pa + KERNBASE);
f0101c07:	8d 80 00 00 00 f0    	lea    -0x10000000(%eax),%eax
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
		assert(c[i] == 0);
f0101c0d:	80 38 00             	cmpb   $0x0,(%eax)
f0101c10:	74 19                	je     f0101c2b <mem_init+0x4bc>
f0101c12:	68 d3 87 10 f0       	push   $0xf01087d3
f0101c17:	68 51 75 10 f0       	push   $0xf0107551
f0101c1c:	68 28 04 00 00       	push   $0x428
f0101c21:	68 e1 84 10 f0       	push   $0xf01084e1
f0101c26:	e8 21 e4 ff ff       	call   f010004c <_panic>
f0101c2b:	40                   	inc    %eax
	memset(page2kva(pp0), 1, PGSIZE);
	page_free(pp0);
	assert((pp = page_alloc(ALLOC_ZERO)));
	assert(pp && pp0 == pp);
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
f0101c2c:	39 d0                	cmp    %edx,%eax
f0101c2e:	75 dd                	jne    f0101c0d <mem_init+0x49e>
		assert(c[i] == 0);

	// give free list back
	page_free_list = fl;
f0101c30:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0101c33:	a3 40 32 23 f0       	mov    %eax,0xf0233240

	// free the pages we took
	page_free(pp0);
f0101c38:	83 ec 0c             	sub    $0xc,%esp
f0101c3b:	56                   	push   %esi
f0101c3c:	e8 57 f6 ff ff       	call   f0101298 <page_free>
	page_free(pp1);
f0101c41:	89 3c 24             	mov    %edi,(%esp)
f0101c44:	e8 4f f6 ff ff       	call   f0101298 <page_free>
	page_free(pp2);
f0101c49:	83 c4 04             	add    $0x4,%esp
f0101c4c:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101c4f:	e8 44 f6 ff ff       	call   f0101298 <page_free>

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101c54:	a1 40 32 23 f0       	mov    0xf0233240,%eax
f0101c59:	83 c4 10             	add    $0x10,%esp
f0101c5c:	eb 03                	jmp    f0101c61 <mem_init+0x4f2>
		--nfree;
f0101c5e:	4b                   	dec    %ebx
	page_free(pp0);
	page_free(pp1);
	page_free(pp2);

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101c5f:	8b 00                	mov    (%eax),%eax
f0101c61:	85 c0                	test   %eax,%eax
f0101c63:	75 f9                	jne    f0101c5e <mem_init+0x4ef>
		--nfree;
	assert(nfree == 0);
f0101c65:	85 db                	test   %ebx,%ebx
f0101c67:	74 19                	je     f0101c82 <mem_init+0x513>
f0101c69:	68 dd 87 10 f0       	push   $0xf01087dd
f0101c6e:	68 51 75 10 f0       	push   $0xf0107551
f0101c73:	68 35 04 00 00       	push   $0x435
f0101c78:	68 e1 84 10 f0       	push   $0xf01084e1
f0101c7d:	e8 ca e3 ff ff       	call   f010004c <_panic>

	cprintf("check_page_alloc() succeeded!\n");
f0101c82:	83 ec 0c             	sub    $0xc,%esp
f0101c85:	68 f4 7c 10 f0       	push   $0xf0107cf4
f0101c8a:	e8 82 1f 00 00       	call   f0103c11 <cprintf>
	int i;
	extern pde_t entry_pgdir[];

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101c8f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101c96:	e8 87 f5 ff ff       	call   f0101222 <page_alloc>
f0101c9b:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101c9e:	83 c4 10             	add    $0x10,%esp
f0101ca1:	85 c0                	test   %eax,%eax
f0101ca3:	75 19                	jne    f0101cbe <mem_init+0x54f>
f0101ca5:	68 eb 86 10 f0       	push   $0xf01086eb
f0101caa:	68 51 75 10 f0       	push   $0xf0107551
f0101caf:	68 ac 04 00 00       	push   $0x4ac
f0101cb4:	68 e1 84 10 f0       	push   $0xf01084e1
f0101cb9:	e8 8e e3 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0101cbe:	83 ec 0c             	sub    $0xc,%esp
f0101cc1:	6a 00                	push   $0x0
f0101cc3:	e8 5a f5 ff ff       	call   f0101222 <page_alloc>
f0101cc8:	89 c6                	mov    %eax,%esi
f0101cca:	83 c4 10             	add    $0x10,%esp
f0101ccd:	85 c0                	test   %eax,%eax
f0101ccf:	75 19                	jne    f0101cea <mem_init+0x57b>
f0101cd1:	68 01 87 10 f0       	push   $0xf0108701
f0101cd6:	68 51 75 10 f0       	push   $0xf0107551
f0101cdb:	68 ad 04 00 00       	push   $0x4ad
f0101ce0:	68 e1 84 10 f0       	push   $0xf01084e1
f0101ce5:	e8 62 e3 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0101cea:	83 ec 0c             	sub    $0xc,%esp
f0101ced:	6a 00                	push   $0x0
f0101cef:	e8 2e f5 ff ff       	call   f0101222 <page_alloc>
f0101cf4:	89 c3                	mov    %eax,%ebx
f0101cf6:	83 c4 10             	add    $0x10,%esp
f0101cf9:	85 c0                	test   %eax,%eax
f0101cfb:	75 19                	jne    f0101d16 <mem_init+0x5a7>
f0101cfd:	68 17 87 10 f0       	push   $0xf0108717
f0101d02:	68 51 75 10 f0       	push   $0xf0107551
f0101d07:	68 ae 04 00 00       	push   $0x4ae
f0101d0c:	68 e1 84 10 f0       	push   $0xf01084e1
f0101d11:	e8 36 e3 ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101d16:	39 75 d4             	cmp    %esi,-0x2c(%ebp)
f0101d19:	75 19                	jne    f0101d34 <mem_init+0x5c5>
f0101d1b:	68 2d 87 10 f0       	push   $0xf010872d
f0101d20:	68 51 75 10 f0       	push   $0xf0107551
f0101d25:	68 b1 04 00 00       	push   $0x4b1
f0101d2a:	68 e1 84 10 f0       	push   $0xf01084e1
f0101d2f:	e8 18 e3 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101d34:	39 c6                	cmp    %eax,%esi
f0101d36:	74 05                	je     f0101d3d <mem_init+0x5ce>
f0101d38:	39 45 d4             	cmp    %eax,-0x2c(%ebp)
f0101d3b:	75 19                	jne    f0101d56 <mem_init+0x5e7>
f0101d3d:	68 d4 7c 10 f0       	push   $0xf0107cd4
f0101d42:	68 51 75 10 f0       	push   $0xf0107551
f0101d47:	68 b2 04 00 00       	push   $0x4b2
f0101d4c:	68 e1 84 10 f0       	push   $0xf01084e1
f0101d51:	e8 f6 e2 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f0101d56:	a1 40 32 23 f0       	mov    0xf0233240,%eax
f0101d5b:	89 45 d0             	mov    %eax,-0x30(%ebp)

	page_free_list = 0;
f0101d5e:	c7 05 40 32 23 f0 00 	movl   $0x0,0xf0233240
f0101d65:	00 00 00 
	//print_fl();
	// should be no free memory
	assert(!page_alloc(0));
f0101d68:	83 ec 0c             	sub    $0xc,%esp
f0101d6b:	6a 00                	push   $0x0
f0101d6d:	e8 b0 f4 ff ff       	call   f0101222 <page_alloc>
f0101d72:	83 c4 10             	add    $0x10,%esp
f0101d75:	85 c0                	test   %eax,%eax
f0101d77:	74 19                	je     f0101d92 <mem_init+0x623>
f0101d79:	68 96 87 10 f0       	push   $0xf0108796
f0101d7e:	68 51 75 10 f0       	push   $0xf0107551
f0101d83:	68 ba 04 00 00       	push   $0x4ba
f0101d88:	68 e1 84 10 f0       	push   $0xf01084e1
f0101d8d:	e8 ba e2 ff ff       	call   f010004c <_panic>
	//print_fl();
	// there is no page allocated at address 0
	assert(page_lookup(kern_pgdir, (void *) 0x0, &ptep) == NULL);
f0101d92:	83 ec 04             	sub    $0x4,%esp
f0101d95:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0101d98:	50                   	push   %eax
f0101d99:	6a 00                	push   $0x0
f0101d9b:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0101da1:	e8 33 f7 ff ff       	call   f01014d9 <page_lookup>
f0101da6:	83 c4 10             	add    $0x10,%esp
f0101da9:	85 c0                	test   %eax,%eax
f0101dab:	74 19                	je     f0101dc6 <mem_init+0x657>
f0101dad:	68 14 7d 10 f0       	push   $0xf0107d14
f0101db2:	68 51 75 10 f0       	push   $0xf0107551
f0101db7:	68 bd 04 00 00       	push   $0x4bd
f0101dbc:	68 e1 84 10 f0       	push   $0xf01084e1
f0101dc1:	e8 86 e2 ff ff       	call   f010004c <_panic>
	//cprintf("H1\n");
	// there is no free memory, so we can't allocate a page table
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) < 0); //insert fail
f0101dc6:	6a 02                	push   $0x2
f0101dc8:	6a 00                	push   $0x0
f0101dca:	56                   	push   %esi
f0101dcb:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0101dd1:	e8 c8 f8 ff ff       	call   f010169e <page_insert>
f0101dd6:	83 c4 10             	add    $0x10,%esp
f0101dd9:	85 c0                	test   %eax,%eax
f0101ddb:	78 19                	js     f0101df6 <mem_init+0x687>
f0101ddd:	68 4c 7d 10 f0       	push   $0xf0107d4c
f0101de2:	68 51 75 10 f0       	push   $0xf0107551
f0101de7:	68 c0 04 00 00       	push   $0x4c0
f0101dec:	68 e1 84 10 f0       	push   $0xf01084e1
f0101df1:	e8 56 e2 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H2\n");
	// free pp0 and try again: pp0 should be used for page table
	page_free(pp0);
f0101df6:	83 ec 0c             	sub    $0xc,%esp
f0101df9:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101dfc:	e8 97 f4 ff ff       	call   f0101298 <page_free>
	//cprintf("H3\n");
	
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) == 0); //insert success
f0101e01:	6a 02                	push   $0x2
f0101e03:	6a 00                	push   $0x0
f0101e05:	56                   	push   %esi
f0101e06:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0101e0c:	e8 8d f8 ff ff       	call   f010169e <page_insert>
f0101e11:	83 c4 20             	add    $0x20,%esp
f0101e14:	85 c0                	test   %eax,%eax
f0101e16:	74 19                	je     f0101e31 <mem_init+0x6c2>
f0101e18:	68 7c 7d 10 f0       	push   $0xf0107d7c
f0101e1d:	68 51 75 10 f0       	push   $0xf0107551
f0101e22:	68 c7 04 00 00       	push   $0x4c7
f0101e27:	68 e1 84 10 f0       	push   $0xf01084e1
f0101e2c:	e8 1b e2 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H3\n");
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0)); //problem
f0101e31:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101e37:	a1 2c 44 23 f0       	mov    0xf023442c,%eax
f0101e3c:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0101e3f:	8b 17                	mov    (%edi),%edx
f0101e41:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0101e47:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
f0101e4a:	29 c1                	sub    %eax,%ecx
f0101e4c:	89 c8                	mov    %ecx,%eax
f0101e4e:	c1 f8 03             	sar    $0x3,%eax
f0101e51:	c1 e0 0c             	shl    $0xc,%eax
f0101e54:	39 c2                	cmp    %eax,%edx
f0101e56:	74 19                	je     f0101e71 <mem_init+0x702>
f0101e58:	68 ac 7d 10 f0       	push   $0xf0107dac
f0101e5d:	68 51 75 10 f0       	push   $0xf0107551
f0101e62:	68 ca 04 00 00       	push   $0x4ca
f0101e67:	68 e1 84 10 f0       	push   $0xf01084e1
f0101e6c:	e8 db e1 ff ff       	call   f010004c <_panic>
	//cprintf("H4\n");
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, 0x0));
	assert(check_va2pa(kern_pgdir, 0x0) == page2pa(pp1)); //problem
f0101e71:	ba 00 00 00 00       	mov    $0x0,%edx
f0101e76:	89 f8                	mov    %edi,%eax
f0101e78:	e8 b4 ee ff ff       	call   f0100d31 <check_va2pa>
f0101e7d:	89 f2                	mov    %esi,%edx
f0101e7f:	2b 55 cc             	sub    -0x34(%ebp),%edx
f0101e82:	c1 fa 03             	sar    $0x3,%edx
f0101e85:	c1 e2 0c             	shl    $0xc,%edx
f0101e88:	39 d0                	cmp    %edx,%eax
f0101e8a:	74 19                	je     f0101ea5 <mem_init+0x736>
f0101e8c:	68 d4 7d 10 f0       	push   $0xf0107dd4
f0101e91:	68 51 75 10 f0       	push   $0xf0107551
f0101e96:	68 cd 04 00 00       	push   $0x4cd
f0101e9b:	68 e1 84 10 f0       	push   $0xf01084e1
f0101ea0:	e8 a7 e1 ff ff       	call   f010004c <_panic>
	//cprintf("H5\n");
	assert(pp1->pp_ref == 1);
f0101ea5:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0101eaa:	74 19                	je     f0101ec5 <mem_init+0x756>
f0101eac:	68 e8 87 10 f0       	push   $0xf01087e8
f0101eb1:	68 51 75 10 f0       	push   $0xf0107551
f0101eb6:	68 cf 04 00 00       	push   $0x4cf
f0101ebb:	68 e1 84 10 f0       	push   $0xf01084e1
f0101ec0:	e8 87 e1 ff ff       	call   f010004c <_panic>
	assert(pp0->pp_ref == 1);
f0101ec5:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101ec8:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f0101ecd:	74 19                	je     f0101ee8 <mem_init+0x779>
f0101ecf:	68 f9 87 10 f0       	push   $0xf01087f9
f0101ed4:	68 51 75 10 f0       	push   $0xf0107551
f0101ed9:	68 d0 04 00 00       	push   $0x4d0
f0101ede:	68 e1 84 10 f0       	push   $0xf01084e1
f0101ee3:	e8 64 e1 ff ff       	call   f010004c <_panic>

	//print_fl();
	// should be able to map pp2 at PGSIZE because pp0 is already allocated for page table
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101ee8:	6a 02                	push   $0x2
f0101eea:	68 00 10 00 00       	push   $0x1000
f0101eef:	53                   	push   %ebx
f0101ef0:	57                   	push   %edi
f0101ef1:	e8 a8 f7 ff ff       	call   f010169e <page_insert>
f0101ef6:	83 c4 10             	add    $0x10,%esp
f0101ef9:	85 c0                	test   %eax,%eax
f0101efb:	74 19                	je     f0101f16 <mem_init+0x7a7>
f0101efd:	68 04 7e 10 f0       	push   $0xf0107e04
f0101f02:	68 51 75 10 f0       	push   $0xf0107551
f0101f07:	68 d4 04 00 00       	push   $0x4d4
f0101f0c:	68 e1 84 10 f0       	push   $0xf01084e1
f0101f11:	e8 36 e1 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, PGSIZE));
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101f16:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101f1b:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0101f20:	e8 0c ee ff ff       	call   f0100d31 <check_va2pa>
f0101f25:	89 da                	mov    %ebx,%edx
f0101f27:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f0101f2d:	c1 fa 03             	sar    $0x3,%edx
f0101f30:	c1 e2 0c             	shl    $0xc,%edx
f0101f33:	39 d0                	cmp    %edx,%eax
f0101f35:	74 19                	je     f0101f50 <mem_init+0x7e1>
f0101f37:	68 40 7e 10 f0       	push   $0xf0107e40
f0101f3c:	68 51 75 10 f0       	push   $0xf0107551
f0101f41:	68 d7 04 00 00       	push   $0x4d7
f0101f46:	68 e1 84 10 f0       	push   $0xf01084e1
f0101f4b:	e8 fc e0 ff ff       	call   f010004c <_panic>
	//cprintf("H6\n");
	assert(pp2->pp_ref == 1);
f0101f50:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101f55:	74 19                	je     f0101f70 <mem_init+0x801>
f0101f57:	68 0a 88 10 f0       	push   $0xf010880a
f0101f5c:	68 51 75 10 f0       	push   $0xf0107551
f0101f61:	68 d9 04 00 00       	push   $0x4d9
f0101f66:	68 e1 84 10 f0       	push   $0xf01084e1
f0101f6b:	e8 dc e0 ff ff       	call   f010004c <_panic>

	// should be no free memory
	//print_fl();
	assert(!page_alloc(0));
f0101f70:	83 ec 0c             	sub    $0xc,%esp
f0101f73:	6a 00                	push   $0x0
f0101f75:	e8 a8 f2 ff ff       	call   f0101222 <page_alloc>
f0101f7a:	83 c4 10             	add    $0x10,%esp
f0101f7d:	85 c0                	test   %eax,%eax
f0101f7f:	74 19                	je     f0101f9a <mem_init+0x82b>
f0101f81:	68 96 87 10 f0       	push   $0xf0108796
f0101f86:	68 51 75 10 f0       	push   $0xf0107551
f0101f8b:	68 dd 04 00 00       	push   $0x4dd
f0101f90:	68 e1 84 10 f0       	push   $0xf01084e1
f0101f95:	e8 b2 e0 ff ff       	call   f010004c <_panic>
	//print_fl();
	// should be able to map pp2 at PGSIZE because it's already there
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101f9a:	6a 02                	push   $0x2
f0101f9c:	68 00 10 00 00       	push   $0x1000
f0101fa1:	53                   	push   %ebx
f0101fa2:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0101fa8:	e8 f1 f6 ff ff       	call   f010169e <page_insert>
f0101fad:	83 c4 10             	add    $0x10,%esp
f0101fb0:	85 c0                	test   %eax,%eax
f0101fb2:	74 19                	je     f0101fcd <mem_init+0x85e>
f0101fb4:	68 04 7e 10 f0       	push   $0xf0107e04
f0101fb9:	68 51 75 10 f0       	push   $0xf0107551
f0101fbe:	68 e0 04 00 00       	push   $0x4e0
f0101fc3:	68 e1 84 10 f0       	push   $0xf01084e1
f0101fc8:	e8 7f e0 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101fcd:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101fd2:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0101fd7:	e8 55 ed ff ff       	call   f0100d31 <check_va2pa>
f0101fdc:	89 da                	mov    %ebx,%edx
f0101fde:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f0101fe4:	c1 fa 03             	sar    $0x3,%edx
f0101fe7:	c1 e2 0c             	shl    $0xc,%edx
f0101fea:	39 d0                	cmp    %edx,%eax
f0101fec:	74 19                	je     f0102007 <mem_init+0x898>
f0101fee:	68 40 7e 10 f0       	push   $0xf0107e40
f0101ff3:	68 51 75 10 f0       	push   $0xf0107551
f0101ff8:	68 e1 04 00 00       	push   $0x4e1
f0101ffd:	68 e1 84 10 f0       	push   $0xf01084e1
f0102002:	e8 45 e0 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0102007:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f010200c:	74 19                	je     f0102027 <mem_init+0x8b8>
f010200e:	68 0a 88 10 f0       	push   $0xf010880a
f0102013:	68 51 75 10 f0       	push   $0xf0107551
f0102018:	68 e2 04 00 00       	push   $0x4e2
f010201d:	68 e1 84 10 f0       	push   $0xf01084e1
f0102022:	e8 25 e0 ff ff       	call   f010004c <_panic>
	//print_fl();
	// pp2 should NOT be on the free list
	// could happen in ref counts are handled sloppily in page_insert
	assert(!page_alloc(0));
f0102027:	83 ec 0c             	sub    $0xc,%esp
f010202a:	6a 00                	push   $0x0
f010202c:	e8 f1 f1 ff ff       	call   f0101222 <page_alloc>
f0102031:	83 c4 10             	add    $0x10,%esp
f0102034:	85 c0                	test   %eax,%eax
f0102036:	74 19                	je     f0102051 <mem_init+0x8e2>
f0102038:	68 96 87 10 f0       	push   $0xf0108796
f010203d:	68 51 75 10 f0       	push   $0xf0107551
f0102042:	68 e6 04 00 00       	push   $0x4e6
f0102047:	68 e1 84 10 f0       	push   $0xf01084e1
f010204c:	e8 fb df ff ff       	call   f010004c <_panic>

	// check that pgdir_walk returns a pointer to the pte
	ptep = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(PGSIZE)]));
f0102051:	8b 15 28 44 23 f0    	mov    0xf0234428,%edx
f0102057:	8b 02                	mov    (%edx),%eax
f0102059:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010205e:	89 c1                	mov    %eax,%ecx
f0102060:	c1 e9 0c             	shr    $0xc,%ecx
f0102063:	3b 0d 24 44 23 f0    	cmp    0xf0234424,%ecx
f0102069:	72 15                	jb     f0102080 <mem_init+0x911>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010206b:	50                   	push   %eax
f010206c:	68 e8 74 10 f0       	push   $0xf01074e8
f0102071:	68 e9 04 00 00       	push   $0x4e9
f0102076:	68 e1 84 10 f0       	push   $0xf01084e1
f010207b:	e8 cc df ff ff       	call   f010004c <_panic>
f0102080:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102085:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	assert(pgdir_walk(kern_pgdir, (void*)PGSIZE, 0) == ptep+PTX(PGSIZE));
f0102088:	83 ec 04             	sub    $0x4,%esp
f010208b:	6a 00                	push   $0x0
f010208d:	68 00 10 00 00       	push   $0x1000
f0102092:	52                   	push   %edx
f0102093:	e8 81 f2 ff ff       	call   f0101319 <pgdir_walk>
f0102098:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f010209b:	8d 57 04             	lea    0x4(%edi),%edx
f010209e:	83 c4 10             	add    $0x10,%esp
f01020a1:	39 d0                	cmp    %edx,%eax
f01020a3:	74 19                	je     f01020be <mem_init+0x94f>
f01020a5:	68 70 7e 10 f0       	push   $0xf0107e70
f01020aa:	68 51 75 10 f0       	push   $0xf0107551
f01020af:	68 ea 04 00 00       	push   $0x4ea
f01020b4:	68 e1 84 10 f0       	push   $0xf01084e1
f01020b9:	e8 8e df ff ff       	call   f010004c <_panic>

	// should be able to change permissions too.
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W|PTE_U) == 0);
f01020be:	6a 06                	push   $0x6
f01020c0:	68 00 10 00 00       	push   $0x1000
f01020c5:	53                   	push   %ebx
f01020c6:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01020cc:	e8 cd f5 ff ff       	call   f010169e <page_insert>
f01020d1:	83 c4 10             	add    $0x10,%esp
f01020d4:	85 c0                	test   %eax,%eax
f01020d6:	74 19                	je     f01020f1 <mem_init+0x982>
f01020d8:	68 b0 7e 10 f0       	push   $0xf0107eb0
f01020dd:	68 51 75 10 f0       	push   $0xf0107551
f01020e2:	68 ed 04 00 00       	push   $0x4ed
f01020e7:	68 e1 84 10 f0       	push   $0xf01084e1
f01020ec:	e8 5b df ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f01020f1:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi
f01020f7:	ba 00 10 00 00       	mov    $0x1000,%edx
f01020fc:	89 f8                	mov    %edi,%eax
f01020fe:	e8 2e ec ff ff       	call   f0100d31 <check_va2pa>
f0102103:	89 da                	mov    %ebx,%edx
f0102105:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f010210b:	c1 fa 03             	sar    $0x3,%edx
f010210e:	c1 e2 0c             	shl    $0xc,%edx
f0102111:	39 d0                	cmp    %edx,%eax
f0102113:	74 19                	je     f010212e <mem_init+0x9bf>
f0102115:	68 40 7e 10 f0       	push   $0xf0107e40
f010211a:	68 51 75 10 f0       	push   $0xf0107551
f010211f:	68 ee 04 00 00       	push   $0x4ee
f0102124:	68 e1 84 10 f0       	push   $0xf01084e1
f0102129:	e8 1e df ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f010212e:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0102133:	74 19                	je     f010214e <mem_init+0x9df>
f0102135:	68 0a 88 10 f0       	push   $0xf010880a
f010213a:	68 51 75 10 f0       	push   $0xf0107551
f010213f:	68 ef 04 00 00       	push   $0x4ef
f0102144:	68 e1 84 10 f0       	push   $0xf01084e1
f0102149:	e8 fe de ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U);
f010214e:	83 ec 04             	sub    $0x4,%esp
f0102151:	6a 00                	push   $0x0
f0102153:	68 00 10 00 00       	push   $0x1000
f0102158:	57                   	push   %edi
f0102159:	e8 bb f1 ff ff       	call   f0101319 <pgdir_walk>
f010215e:	83 c4 10             	add    $0x10,%esp
f0102161:	f6 00 04             	testb  $0x4,(%eax)
f0102164:	75 19                	jne    f010217f <mem_init+0xa10>
f0102166:	68 f0 7e 10 f0       	push   $0xf0107ef0
f010216b:	68 51 75 10 f0       	push   $0xf0107551
f0102170:	68 f0 04 00 00       	push   $0x4f0
f0102175:	68 e1 84 10 f0       	push   $0xf01084e1
f010217a:	e8 cd de ff ff       	call   f010004c <_panic>
	assert(kern_pgdir[0] & PTE_U);
f010217f:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0102184:	f6 00 04             	testb  $0x4,(%eax)
f0102187:	75 19                	jne    f01021a2 <mem_init+0xa33>
f0102189:	68 1b 88 10 f0       	push   $0xf010881b
f010218e:	68 51 75 10 f0       	push   $0xf0107551
f0102193:	68 f1 04 00 00       	push   $0x4f1
f0102198:	68 e1 84 10 f0       	push   $0xf01084e1
f010219d:	e8 aa de ff ff       	call   f010004c <_panic>

	// should be able to remap with fewer permissions
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f01021a2:	6a 02                	push   $0x2
f01021a4:	68 00 10 00 00       	push   $0x1000
f01021a9:	53                   	push   %ebx
f01021aa:	50                   	push   %eax
f01021ab:	e8 ee f4 ff ff       	call   f010169e <page_insert>
f01021b0:	83 c4 10             	add    $0x10,%esp
f01021b3:	85 c0                	test   %eax,%eax
f01021b5:	74 19                	je     f01021d0 <mem_init+0xa61>
f01021b7:	68 04 7e 10 f0       	push   $0xf0107e04
f01021bc:	68 51 75 10 f0       	push   $0xf0107551
f01021c1:	68 f4 04 00 00       	push   $0x4f4
f01021c6:	68 e1 84 10 f0       	push   $0xf01084e1
f01021cb:	e8 7c de ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_W);
f01021d0:	83 ec 04             	sub    $0x4,%esp
f01021d3:	6a 00                	push   $0x0
f01021d5:	68 00 10 00 00       	push   $0x1000
f01021da:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01021e0:	e8 34 f1 ff ff       	call   f0101319 <pgdir_walk>
f01021e5:	83 c4 10             	add    $0x10,%esp
f01021e8:	f6 00 02             	testb  $0x2,(%eax)
f01021eb:	75 19                	jne    f0102206 <mem_init+0xa97>
f01021ed:	68 24 7f 10 f0       	push   $0xf0107f24
f01021f2:	68 51 75 10 f0       	push   $0xf0107551
f01021f7:	68 f5 04 00 00       	push   $0x4f5
f01021fc:	68 e1 84 10 f0       	push   $0xf01084e1
f0102201:	e8 46 de ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f0102206:	83 ec 04             	sub    $0x4,%esp
f0102209:	6a 00                	push   $0x0
f010220b:	68 00 10 00 00       	push   $0x1000
f0102210:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0102216:	e8 fe f0 ff ff       	call   f0101319 <pgdir_walk>
f010221b:	83 c4 10             	add    $0x10,%esp
f010221e:	f6 00 04             	testb  $0x4,(%eax)
f0102221:	74 19                	je     f010223c <mem_init+0xacd>
f0102223:	68 58 7f 10 f0       	push   $0xf0107f58
f0102228:	68 51 75 10 f0       	push   $0xf0107551
f010222d:	68 f6 04 00 00       	push   $0x4f6
f0102232:	68 e1 84 10 f0       	push   $0xf01084e1
f0102237:	e8 10 de ff ff       	call   f010004c <_panic>

	// should not be able to map at PTSIZE because need free page for page table
	assert(page_insert(kern_pgdir, pp0, (void*) PTSIZE, PTE_W) < 0);
f010223c:	6a 02                	push   $0x2
f010223e:	68 00 00 40 00       	push   $0x400000
f0102243:	ff 75 d4             	pushl  -0x2c(%ebp)
f0102246:	ff 35 28 44 23 f0    	pushl  0xf0234428
f010224c:	e8 4d f4 ff ff       	call   f010169e <page_insert>
f0102251:	83 c4 10             	add    $0x10,%esp
f0102254:	85 c0                	test   %eax,%eax
f0102256:	78 19                	js     f0102271 <mem_init+0xb02>
f0102258:	68 90 7f 10 f0       	push   $0xf0107f90
f010225d:	68 51 75 10 f0       	push   $0xf0107551
f0102262:	68 f9 04 00 00       	push   $0x4f9
f0102267:	68 e1 84 10 f0       	push   $0xf01084e1
f010226c:	e8 db dd ff ff       	call   f010004c <_panic>

	// insert pp1 at PGSIZE (replacing pp2)
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W) == 0);
f0102271:	6a 02                	push   $0x2
f0102273:	68 00 10 00 00       	push   $0x1000
f0102278:	56                   	push   %esi
f0102279:	ff 35 28 44 23 f0    	pushl  0xf0234428
f010227f:	e8 1a f4 ff ff       	call   f010169e <page_insert>
f0102284:	83 c4 10             	add    $0x10,%esp
f0102287:	85 c0                	test   %eax,%eax
f0102289:	74 19                	je     f01022a4 <mem_init+0xb35>
f010228b:	68 c8 7f 10 f0       	push   $0xf0107fc8
f0102290:	68 51 75 10 f0       	push   $0xf0107551
f0102295:	68 fc 04 00 00       	push   $0x4fc
f010229a:	68 e1 84 10 f0       	push   $0xf01084e1
f010229f:	e8 a8 dd ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f01022a4:	83 ec 04             	sub    $0x4,%esp
f01022a7:	6a 00                	push   $0x0
f01022a9:	68 00 10 00 00       	push   $0x1000
f01022ae:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01022b4:	e8 60 f0 ff ff       	call   f0101319 <pgdir_walk>
f01022b9:	83 c4 10             	add    $0x10,%esp
f01022bc:	f6 00 04             	testb  $0x4,(%eax)
f01022bf:	74 19                	je     f01022da <mem_init+0xb6b>
f01022c1:	68 58 7f 10 f0       	push   $0xf0107f58
f01022c6:	68 51 75 10 f0       	push   $0xf0107551
f01022cb:	68 fd 04 00 00       	push   $0x4fd
f01022d0:	68 e1 84 10 f0       	push   $0xf01084e1
f01022d5:	e8 72 dd ff ff       	call   f010004c <_panic>

	// should have pp1 at both 0 and PGSIZE, pp2 nowhere, ...
	assert(check_va2pa(kern_pgdir, 0) == page2pa(pp1));
f01022da:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi
f01022e0:	ba 00 00 00 00       	mov    $0x0,%edx
f01022e5:	89 f8                	mov    %edi,%eax
f01022e7:	e8 45 ea ff ff       	call   f0100d31 <check_va2pa>
f01022ec:	89 c1                	mov    %eax,%ecx
f01022ee:	89 45 cc             	mov    %eax,-0x34(%ebp)
f01022f1:	89 f0                	mov    %esi,%eax
f01022f3:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f01022f9:	c1 f8 03             	sar    $0x3,%eax
f01022fc:	c1 e0 0c             	shl    $0xc,%eax
f01022ff:	39 c1                	cmp    %eax,%ecx
f0102301:	74 19                	je     f010231c <mem_init+0xbad>
f0102303:	68 04 80 10 f0       	push   $0xf0108004
f0102308:	68 51 75 10 f0       	push   $0xf0107551
f010230d:	68 00 05 00 00       	push   $0x500
f0102312:	68 e1 84 10 f0       	push   $0xf01084e1
f0102317:	e8 30 dd ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f010231c:	ba 00 10 00 00       	mov    $0x1000,%edx
f0102321:	89 f8                	mov    %edi,%eax
f0102323:	e8 09 ea ff ff       	call   f0100d31 <check_va2pa>
f0102328:	39 45 cc             	cmp    %eax,-0x34(%ebp)
f010232b:	74 19                	je     f0102346 <mem_init+0xbd7>
f010232d:	68 30 80 10 f0       	push   $0xf0108030
f0102332:	68 51 75 10 f0       	push   $0xf0107551
f0102337:	68 01 05 00 00       	push   $0x501
f010233c:	68 e1 84 10 f0       	push   $0xf01084e1
f0102341:	e8 06 dd ff ff       	call   f010004c <_panic>
	// ... and ref counts should reflect this
	assert(pp1->pp_ref == 2);
f0102346:	66 83 7e 04 02       	cmpw   $0x2,0x4(%esi)
f010234b:	74 19                	je     f0102366 <mem_init+0xbf7>
f010234d:	68 31 88 10 f0       	push   $0xf0108831
f0102352:	68 51 75 10 f0       	push   $0xf0107551
f0102357:	68 03 05 00 00       	push   $0x503
f010235c:	68 e1 84 10 f0       	push   $0xf01084e1
f0102361:	e8 e6 dc ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f0102366:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f010236b:	74 19                	je     f0102386 <mem_init+0xc17>
f010236d:	68 42 88 10 f0       	push   $0xf0108842
f0102372:	68 51 75 10 f0       	push   $0xf0107551
f0102377:	68 04 05 00 00       	push   $0x504
f010237c:	68 e1 84 10 f0       	push   $0xf01084e1
f0102381:	e8 c6 dc ff ff       	call   f010004c <_panic>

	// pp2 should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp2);
f0102386:	83 ec 0c             	sub    $0xc,%esp
f0102389:	6a 00                	push   $0x0
f010238b:	e8 92 ee ff ff       	call   f0101222 <page_alloc>
f0102390:	83 c4 10             	add    $0x10,%esp
f0102393:	85 c0                	test   %eax,%eax
f0102395:	74 04                	je     f010239b <mem_init+0xc2c>
f0102397:	39 c3                	cmp    %eax,%ebx
f0102399:	74 19                	je     f01023b4 <mem_init+0xc45>
f010239b:	68 60 80 10 f0       	push   $0xf0108060
f01023a0:	68 51 75 10 f0       	push   $0xf0107551
f01023a5:	68 07 05 00 00       	push   $0x507
f01023aa:	68 e1 84 10 f0       	push   $0xf01084e1
f01023af:	e8 98 dc ff ff       	call   f010004c <_panic>

	// unmapping pp1 at 0 should keep pp1 at PGSIZE
	page_remove(kern_pgdir, 0x0);
f01023b4:	83 ec 08             	sub    $0x8,%esp
f01023b7:	6a 00                	push   $0x0
f01023b9:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01023bf:	e8 3a f2 ff ff       	call   f01015fe <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f01023c4:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi
f01023ca:	ba 00 00 00 00       	mov    $0x0,%edx
f01023cf:	89 f8                	mov    %edi,%eax
f01023d1:	e8 5b e9 ff ff       	call   f0100d31 <check_va2pa>
f01023d6:	83 c4 10             	add    $0x10,%esp
f01023d9:	83 f8 ff             	cmp    $0xffffffff,%eax
f01023dc:	74 19                	je     f01023f7 <mem_init+0xc88>
f01023de:	68 84 80 10 f0       	push   $0xf0108084
f01023e3:	68 51 75 10 f0       	push   $0xf0107551
f01023e8:	68 0b 05 00 00       	push   $0x50b
f01023ed:	68 e1 84 10 f0       	push   $0xf01084e1
f01023f2:	e8 55 dc ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f01023f7:	ba 00 10 00 00       	mov    $0x1000,%edx
f01023fc:	89 f8                	mov    %edi,%eax
f01023fe:	e8 2e e9 ff ff       	call   f0100d31 <check_va2pa>
f0102403:	89 f2                	mov    %esi,%edx
f0102405:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f010240b:	c1 fa 03             	sar    $0x3,%edx
f010240e:	c1 e2 0c             	shl    $0xc,%edx
f0102411:	39 d0                	cmp    %edx,%eax
f0102413:	74 19                	je     f010242e <mem_init+0xcbf>
f0102415:	68 30 80 10 f0       	push   $0xf0108030
f010241a:	68 51 75 10 f0       	push   $0xf0107551
f010241f:	68 0c 05 00 00       	push   $0x50c
f0102424:	68 e1 84 10 f0       	push   $0xf01084e1
f0102429:	e8 1e dc ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 1);
f010242e:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0102433:	74 19                	je     f010244e <mem_init+0xcdf>
f0102435:	68 e8 87 10 f0       	push   $0xf01087e8
f010243a:	68 51 75 10 f0       	push   $0xf0107551
f010243f:	68 0d 05 00 00       	push   $0x50d
f0102444:	68 e1 84 10 f0       	push   $0xf01084e1
f0102449:	e8 fe db ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f010244e:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102453:	74 19                	je     f010246e <mem_init+0xcff>
f0102455:	68 42 88 10 f0       	push   $0xf0108842
f010245a:	68 51 75 10 f0       	push   $0xf0107551
f010245f:	68 0e 05 00 00       	push   $0x50e
f0102464:	68 e1 84 10 f0       	push   $0xf01084e1
f0102469:	e8 de db ff ff       	call   f010004c <_panic>

	// test re-inserting pp1 at PGSIZE
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, 0) == 0);
f010246e:	6a 00                	push   $0x0
f0102470:	68 00 10 00 00       	push   $0x1000
f0102475:	56                   	push   %esi
f0102476:	57                   	push   %edi
f0102477:	e8 22 f2 ff ff       	call   f010169e <page_insert>
f010247c:	83 c4 10             	add    $0x10,%esp
f010247f:	85 c0                	test   %eax,%eax
f0102481:	74 19                	je     f010249c <mem_init+0xd2d>
f0102483:	68 a8 80 10 f0       	push   $0xf01080a8
f0102488:	68 51 75 10 f0       	push   $0xf0107551
f010248d:	68 11 05 00 00       	push   $0x511
f0102492:	68 e1 84 10 f0       	push   $0xf01084e1
f0102497:	e8 b0 db ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref);
f010249c:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f01024a1:	75 19                	jne    f01024bc <mem_init+0xd4d>
f01024a3:	68 53 88 10 f0       	push   $0xf0108853
f01024a8:	68 51 75 10 f0       	push   $0xf0107551
f01024ad:	68 12 05 00 00       	push   $0x512
f01024b2:	68 e1 84 10 f0       	push   $0xf01084e1
f01024b7:	e8 90 db ff ff       	call   f010004c <_panic>
	assert(pp1->pp_link == NULL);
f01024bc:	83 3e 00             	cmpl   $0x0,(%esi)
f01024bf:	74 19                	je     f01024da <mem_init+0xd6b>
f01024c1:	68 5f 88 10 f0       	push   $0xf010885f
f01024c6:	68 51 75 10 f0       	push   $0xf0107551
f01024cb:	68 13 05 00 00       	push   $0x513
f01024d0:	68 e1 84 10 f0       	push   $0xf01084e1
f01024d5:	e8 72 db ff ff       	call   f010004c <_panic>

	// unmapping pp1 at PGSIZE should free it
	page_remove(kern_pgdir, (void*) PGSIZE);
f01024da:	83 ec 08             	sub    $0x8,%esp
f01024dd:	68 00 10 00 00       	push   $0x1000
f01024e2:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01024e8:	e8 11 f1 ff ff       	call   f01015fe <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f01024ed:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi
f01024f3:	ba 00 00 00 00       	mov    $0x0,%edx
f01024f8:	89 f8                	mov    %edi,%eax
f01024fa:	e8 32 e8 ff ff       	call   f0100d31 <check_va2pa>
f01024ff:	83 c4 10             	add    $0x10,%esp
f0102502:	83 f8 ff             	cmp    $0xffffffff,%eax
f0102505:	74 19                	je     f0102520 <mem_init+0xdb1>
f0102507:	68 84 80 10 f0       	push   $0xf0108084
f010250c:	68 51 75 10 f0       	push   $0xf0107551
f0102511:	68 17 05 00 00       	push   $0x517
f0102516:	68 e1 84 10 f0       	push   $0xf01084e1
f010251b:	e8 2c db ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == ~0);
f0102520:	ba 00 10 00 00       	mov    $0x1000,%edx
f0102525:	89 f8                	mov    %edi,%eax
f0102527:	e8 05 e8 ff ff       	call   f0100d31 <check_va2pa>
f010252c:	83 f8 ff             	cmp    $0xffffffff,%eax
f010252f:	74 19                	je     f010254a <mem_init+0xddb>
f0102531:	68 e0 80 10 f0       	push   $0xf01080e0
f0102536:	68 51 75 10 f0       	push   $0xf0107551
f010253b:	68 18 05 00 00       	push   $0x518
f0102540:	68 e1 84 10 f0       	push   $0xf01084e1
f0102545:	e8 02 db ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f010254a:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f010254f:	74 19                	je     f010256a <mem_init+0xdfb>
f0102551:	68 74 88 10 f0       	push   $0xf0108874
f0102556:	68 51 75 10 f0       	push   $0xf0107551
f010255b:	68 19 05 00 00       	push   $0x519
f0102560:	68 e1 84 10 f0       	push   $0xf01084e1
f0102565:	e8 e2 da ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f010256a:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f010256f:	74 19                	je     f010258a <mem_init+0xe1b>
f0102571:	68 42 88 10 f0       	push   $0xf0108842
f0102576:	68 51 75 10 f0       	push   $0xf0107551
f010257b:	68 1a 05 00 00       	push   $0x51a
f0102580:	68 e1 84 10 f0       	push   $0xf01084e1
f0102585:	e8 c2 da ff ff       	call   f010004c <_panic>

	// so it should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp1);
f010258a:	83 ec 0c             	sub    $0xc,%esp
f010258d:	6a 00                	push   $0x0
f010258f:	e8 8e ec ff ff       	call   f0101222 <page_alloc>
f0102594:	83 c4 10             	add    $0x10,%esp
f0102597:	85 c0                	test   %eax,%eax
f0102599:	74 04                	je     f010259f <mem_init+0xe30>
f010259b:	39 c6                	cmp    %eax,%esi
f010259d:	74 19                	je     f01025b8 <mem_init+0xe49>
f010259f:	68 08 81 10 f0       	push   $0xf0108108
f01025a4:	68 51 75 10 f0       	push   $0xf0107551
f01025a9:	68 1d 05 00 00       	push   $0x51d
f01025ae:	68 e1 84 10 f0       	push   $0xf01084e1
f01025b3:	e8 94 da ff ff       	call   f010004c <_panic>

	// should be no free memory
	assert(!page_alloc(0));
f01025b8:	83 ec 0c             	sub    $0xc,%esp
f01025bb:	6a 00                	push   $0x0
f01025bd:	e8 60 ec ff ff       	call   f0101222 <page_alloc>
f01025c2:	83 c4 10             	add    $0x10,%esp
f01025c5:	85 c0                	test   %eax,%eax
f01025c7:	74 19                	je     f01025e2 <mem_init+0xe73>
f01025c9:	68 96 87 10 f0       	push   $0xf0108796
f01025ce:	68 51 75 10 f0       	push   $0xf0107551
f01025d3:	68 20 05 00 00       	push   $0x520
f01025d8:	68 e1 84 10 f0       	push   $0xf01084e1
f01025dd:	e8 6a da ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f01025e2:	8b 0d 28 44 23 f0    	mov    0xf0234428,%ecx
f01025e8:	8b 11                	mov    (%ecx),%edx
f01025ea:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f01025f0:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01025f3:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f01025f9:	c1 f8 03             	sar    $0x3,%eax
f01025fc:	c1 e0 0c             	shl    $0xc,%eax
f01025ff:	39 c2                	cmp    %eax,%edx
f0102601:	74 19                	je     f010261c <mem_init+0xead>
f0102603:	68 ac 7d 10 f0       	push   $0xf0107dac
f0102608:	68 51 75 10 f0       	push   $0xf0107551
f010260d:	68 23 05 00 00       	push   $0x523
f0102612:	68 e1 84 10 f0       	push   $0xf01084e1
f0102617:	e8 30 da ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f010261c:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f0102622:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102625:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f010262a:	74 19                	je     f0102645 <mem_init+0xed6>
f010262c:	68 f9 87 10 f0       	push   $0xf01087f9
f0102631:	68 51 75 10 f0       	push   $0xf0107551
f0102636:	68 25 05 00 00       	push   $0x525
f010263b:	68 e1 84 10 f0       	push   $0xf01084e1
f0102640:	e8 07 da ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f0102645:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102648:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// check pointer arithmetic in pgdir_walk
	page_free(pp0);
f010264e:	83 ec 0c             	sub    $0xc,%esp
f0102651:	50                   	push   %eax
f0102652:	e8 41 ec ff ff       	call   f0101298 <page_free>
	va = (void*)(PGSIZE * NPDENTRIES + PGSIZE);
	ptep = pgdir_walk(kern_pgdir, va, 1);
f0102657:	83 c4 0c             	add    $0xc,%esp
f010265a:	6a 01                	push   $0x1
f010265c:	68 00 10 40 00       	push   $0x401000
f0102661:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0102667:	e8 ad ec ff ff       	call   f0101319 <pgdir_walk>
f010266c:	89 45 cc             	mov    %eax,-0x34(%ebp)
f010266f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	ptep1 = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(va)]));
f0102672:	8b 0d 28 44 23 f0    	mov    0xf0234428,%ecx
f0102678:	8b 51 04             	mov    0x4(%ecx),%edx
f010267b:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102681:	8b 3d 24 44 23 f0    	mov    0xf0234424,%edi
f0102687:	89 d0                	mov    %edx,%eax
f0102689:	c1 e8 0c             	shr    $0xc,%eax
f010268c:	83 c4 10             	add    $0x10,%esp
f010268f:	39 f8                	cmp    %edi,%eax
f0102691:	72 15                	jb     f01026a8 <mem_init+0xf39>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102693:	52                   	push   %edx
f0102694:	68 e8 74 10 f0       	push   $0xf01074e8
f0102699:	68 2c 05 00 00       	push   $0x52c
f010269e:	68 e1 84 10 f0       	push   $0xf01084e1
f01026a3:	e8 a4 d9 ff ff       	call   f010004c <_panic>
	assert(ptep == ptep1 + PTX(va));
f01026a8:	81 ea fc ff ff 0f    	sub    $0xffffffc,%edx
f01026ae:	39 55 cc             	cmp    %edx,-0x34(%ebp)
f01026b1:	74 19                	je     f01026cc <mem_init+0xf5d>
f01026b3:	68 85 88 10 f0       	push   $0xf0108885
f01026b8:	68 51 75 10 f0       	push   $0xf0107551
f01026bd:	68 2d 05 00 00       	push   $0x52d
f01026c2:	68 e1 84 10 f0       	push   $0xf01084e1
f01026c7:	e8 80 d9 ff ff       	call   f010004c <_panic>
	kern_pgdir[PDX(va)] = 0;
f01026cc:	c7 41 04 00 00 00 00 	movl   $0x0,0x4(%ecx)
	pp0->pp_ref = 0;
f01026d3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01026d6:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01026dc:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f01026e2:	c1 f8 03             	sar    $0x3,%eax
f01026e5:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01026e8:	89 c2                	mov    %eax,%edx
f01026ea:	c1 ea 0c             	shr    $0xc,%edx
f01026ed:	39 d7                	cmp    %edx,%edi
f01026ef:	77 12                	ja     f0102703 <mem_init+0xf94>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01026f1:	50                   	push   %eax
f01026f2:	68 e8 74 10 f0       	push   $0xf01074e8
f01026f7:	6a 58                	push   $0x58
f01026f9:	68 ed 84 10 f0       	push   $0xf01084ed
f01026fe:	e8 49 d9 ff ff       	call   f010004c <_panic>

	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
f0102703:	83 ec 04             	sub    $0x4,%esp
f0102706:	68 00 10 00 00       	push   $0x1000
f010270b:	68 ff 00 00 00       	push   $0xff
f0102710:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102715:	50                   	push   %eax
f0102716:	e8 0d 39 00 00       	call   f0106028 <memset>
	page_free(pp0);
f010271b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f010271e:	89 3c 24             	mov    %edi,(%esp)
f0102721:	e8 72 eb ff ff       	call   f0101298 <page_free>
	pgdir_walk(kern_pgdir, 0x0, 1);
f0102726:	83 c4 0c             	add    $0xc,%esp
f0102729:	6a 01                	push   $0x1
f010272b:	6a 00                	push   $0x0
f010272d:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0102733:	e8 e1 eb ff ff       	call   f0101319 <pgdir_walk>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102738:	89 fa                	mov    %edi,%edx
f010273a:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f0102740:	c1 fa 03             	sar    $0x3,%edx
f0102743:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102746:	89 d0                	mov    %edx,%eax
f0102748:	c1 e8 0c             	shr    $0xc,%eax
f010274b:	83 c4 10             	add    $0x10,%esp
f010274e:	3b 05 24 44 23 f0    	cmp    0xf0234424,%eax
f0102754:	72 12                	jb     f0102768 <mem_init+0xff9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102756:	52                   	push   %edx
f0102757:	68 e8 74 10 f0       	push   $0xf01074e8
f010275c:	6a 58                	push   $0x58
f010275e:	68 ed 84 10 f0       	push   $0xf01084ed
f0102763:	e8 e4 d8 ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0102768:	8d 82 00 00 00 f0    	lea    -0x10000000(%edx),%eax
	ptep = (pte_t *) page2kva(pp0);
f010276e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0102771:	81 ea 00 f0 ff 0f    	sub    $0xffff000,%edx
	for(i=0; i<NPTENTRIES; i++)
		assert((ptep[i] & PTE_P) == 0);
f0102777:	f6 00 01             	testb  $0x1,(%eax)
f010277a:	74 19                	je     f0102795 <mem_init+0x1026>
f010277c:	68 9d 88 10 f0       	push   $0xf010889d
f0102781:	68 51 75 10 f0       	push   $0xf0107551
f0102786:	68 37 05 00 00       	push   $0x537
f010278b:	68 e1 84 10 f0       	push   $0xf01084e1
f0102790:	e8 b7 d8 ff ff       	call   f010004c <_panic>
f0102795:	83 c0 04             	add    $0x4,%eax
	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
	page_free(pp0);
	pgdir_walk(kern_pgdir, 0x0, 1);
	ptep = (pte_t *) page2kva(pp0);
	for(i=0; i<NPTENTRIES; i++)
f0102798:	39 d0                	cmp    %edx,%eax
f010279a:	75 db                	jne    f0102777 <mem_init+0x1008>
		assert((ptep[i] & PTE_P) == 0);
	kern_pgdir[0] = 0;
f010279c:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f01027a1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	pp0->pp_ref = 0;
f01027a7:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01027aa:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// give free list back
	page_free_list = fl;
f01027b0:	8b 7d d0             	mov    -0x30(%ebp),%edi
f01027b3:	89 3d 40 32 23 f0    	mov    %edi,0xf0233240

	// free the pages we took
	page_free(pp0);
f01027b9:	83 ec 0c             	sub    $0xc,%esp
f01027bc:	50                   	push   %eax
f01027bd:	e8 d6 ea ff ff       	call   f0101298 <page_free>
	page_free(pp1);
f01027c2:	89 34 24             	mov    %esi,(%esp)
f01027c5:	e8 ce ea ff ff       	call   f0101298 <page_free>
	page_free(pp2);
f01027ca:	89 1c 24             	mov    %ebx,(%esp)
f01027cd:	e8 c6 ea ff ff       	call   f0101298 <page_free>

	// test mmio_map_region
	//mmio_map_region(physaddr_t pa, size_t size)
	mm1 = (uintptr_t) mmio_map_region(0, 4097);
f01027d2:	83 c4 08             	add    $0x8,%esp
f01027d5:	68 01 10 00 00       	push   $0x1001
f01027da:	6a 00                	push   $0x0
f01027dc:	e8 23 ef ff ff       	call   f0101704 <mmio_map_region>
f01027e1:	89 c3                	mov    %eax,%ebx
	mm2 = (uintptr_t) mmio_map_region(0, 4096);
f01027e3:	83 c4 08             	add    $0x8,%esp
f01027e6:	68 00 10 00 00       	push   $0x1000
f01027eb:	6a 00                	push   $0x0
f01027ed:	e8 12 ef ff ff       	call   f0101704 <mmio_map_region>
f01027f2:	89 c6                	mov    %eax,%esi
	// check that they're in the right region
	assert(mm1 >= MMIOBASE && mm1 + 8096 < MMIOLIM);
f01027f4:	83 c4 10             	add    $0x10,%esp
f01027f7:	81 fb ff ff 7f ef    	cmp    $0xef7fffff,%ebx
f01027fd:	76 0e                	jbe    f010280d <mem_init+0x109e>
f01027ff:	8d bb a0 1f 00 00    	lea    0x1fa0(%ebx),%edi
f0102805:	81 ff ff ff bf ef    	cmp    $0xefbfffff,%edi
f010280b:	76 19                	jbe    f0102826 <mem_init+0x10b7>
f010280d:	68 2c 81 10 f0       	push   $0xf010812c
f0102812:	68 51 75 10 f0       	push   $0xf0107551
f0102817:	68 48 05 00 00       	push   $0x548
f010281c:	68 e1 84 10 f0       	push   $0xf01084e1
f0102821:	e8 26 d8 ff ff       	call   f010004c <_panic>
	assert(mm2 >= MMIOBASE && mm2 + 8096 < MMIOLIM);
f0102826:	3d ff ff 7f ef       	cmp    $0xef7fffff,%eax
f010282b:	76 0d                	jbe    f010283a <mem_init+0x10cb>
f010282d:	8d 80 a0 1f 00 00    	lea    0x1fa0(%eax),%eax
f0102833:	3d ff ff bf ef       	cmp    $0xefbfffff,%eax
f0102838:	76 19                	jbe    f0102853 <mem_init+0x10e4>
f010283a:	68 54 81 10 f0       	push   $0xf0108154
f010283f:	68 51 75 10 f0       	push   $0xf0107551
f0102844:	68 49 05 00 00       	push   $0x549
f0102849:	68 e1 84 10 f0       	push   $0xf01084e1
f010284e:	e8 f9 d7 ff ff       	call   f010004c <_panic>
	// check that they're page-aligned
	assert(mm1 % PGSIZE == 0 && mm2 % PGSIZE == 0);
f0102853:	89 d8                	mov    %ebx,%eax
f0102855:	09 f0                	or     %esi,%eax
f0102857:	a9 ff 0f 00 00       	test   $0xfff,%eax
f010285c:	74 19                	je     f0102877 <mem_init+0x1108>
f010285e:	68 7c 81 10 f0       	push   $0xf010817c
f0102863:	68 51 75 10 f0       	push   $0xf0107551
f0102868:	68 4b 05 00 00       	push   $0x54b
f010286d:	68 e1 84 10 f0       	push   $0xf01084e1
f0102872:	e8 d5 d7 ff ff       	call   f010004c <_panic>
	// check that they don't overlap
	cprintf("mm1 is: %p\n", mm1);
f0102877:	83 ec 08             	sub    $0x8,%esp
f010287a:	53                   	push   %ebx
f010287b:	68 b4 88 10 f0       	push   $0xf01088b4
f0102880:	e8 8c 13 00 00       	call   f0103c11 <cprintf>
	cprintf("mm2 is: %p\n", mm2);
f0102885:	83 c4 08             	add    $0x8,%esp
f0102888:	56                   	push   %esi
f0102889:	68 c0 88 10 f0       	push   $0xf01088c0
f010288e:	e8 7e 13 00 00       	call   f0103c11 <cprintf>
	assert(mm1 + 8096 <= mm2);
f0102893:	83 c4 10             	add    $0x10,%esp
f0102896:	39 fe                	cmp    %edi,%esi
f0102898:	73 19                	jae    f01028b3 <mem_init+0x1144>
f010289a:	68 cc 88 10 f0       	push   $0xf01088cc
f010289f:	68 51 75 10 f0       	push   $0xf0107551
f01028a4:	68 4f 05 00 00       	push   $0x54f
f01028a9:	68 e1 84 10 f0       	push   $0xf01084e1
f01028ae:	e8 99 d7 ff ff       	call   f010004c <_panic>
	// check page mappings
	assert(check_va2pa(kern_pgdir, mm1) == 0);
f01028b3:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi
f01028b9:	89 da                	mov    %ebx,%edx
f01028bb:	89 f8                	mov    %edi,%eax
f01028bd:	e8 6f e4 ff ff       	call   f0100d31 <check_va2pa>
f01028c2:	85 c0                	test   %eax,%eax
f01028c4:	74 19                	je     f01028df <mem_init+0x1170>
f01028c6:	68 a4 81 10 f0       	push   $0xf01081a4
f01028cb:	68 51 75 10 f0       	push   $0xf0107551
f01028d0:	68 51 05 00 00       	push   $0x551
f01028d5:	68 e1 84 10 f0       	push   $0xf01084e1
f01028da:	e8 6d d7 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, mm1+PGSIZE) == PGSIZE);
f01028df:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
f01028e5:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01028e8:	89 c2                	mov    %eax,%edx
f01028ea:	89 f8                	mov    %edi,%eax
f01028ec:	e8 40 e4 ff ff       	call   f0100d31 <check_va2pa>
f01028f1:	3d 00 10 00 00       	cmp    $0x1000,%eax
f01028f6:	74 19                	je     f0102911 <mem_init+0x11a2>
f01028f8:	68 c8 81 10 f0       	push   $0xf01081c8
f01028fd:	68 51 75 10 f0       	push   $0xf0107551
f0102902:	68 52 05 00 00       	push   $0x552
f0102907:	68 e1 84 10 f0       	push   $0xf01084e1
f010290c:	e8 3b d7 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, mm2) == 0);
f0102911:	89 f2                	mov    %esi,%edx
f0102913:	89 f8                	mov    %edi,%eax
f0102915:	e8 17 e4 ff ff       	call   f0100d31 <check_va2pa>
f010291a:	85 c0                	test   %eax,%eax
f010291c:	74 19                	je     f0102937 <mem_init+0x11c8>
f010291e:	68 f8 81 10 f0       	push   $0xf01081f8
f0102923:	68 51 75 10 f0       	push   $0xf0107551
f0102928:	68 53 05 00 00       	push   $0x553
f010292d:	68 e1 84 10 f0       	push   $0xf01084e1
f0102932:	e8 15 d7 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, mm2+PGSIZE) == ~0);
f0102937:	8d 96 00 10 00 00    	lea    0x1000(%esi),%edx
f010293d:	89 f8                	mov    %edi,%eax
f010293f:	e8 ed e3 ff ff       	call   f0100d31 <check_va2pa>
f0102944:	83 f8 ff             	cmp    $0xffffffff,%eax
f0102947:	74 19                	je     f0102962 <mem_init+0x11f3>
f0102949:	68 1c 82 10 f0       	push   $0xf010821c
f010294e:	68 51 75 10 f0       	push   $0xf0107551
f0102953:	68 54 05 00 00       	push   $0x554
f0102958:	68 e1 84 10 f0       	push   $0xf01084e1
f010295d:	e8 ea d6 ff ff       	call   f010004c <_panic>
	// check permissions
	assert(*pgdir_walk(kern_pgdir, (void*) mm1, 0) & (PTE_W|PTE_PWT|PTE_PCD));
f0102962:	83 ec 04             	sub    $0x4,%esp
f0102965:	6a 00                	push   $0x0
f0102967:	53                   	push   %ebx
f0102968:	57                   	push   %edi
f0102969:	e8 ab e9 ff ff       	call   f0101319 <pgdir_walk>
f010296e:	83 c4 10             	add    $0x10,%esp
f0102971:	f6 00 1a             	testb  $0x1a,(%eax)
f0102974:	75 19                	jne    f010298f <mem_init+0x1220>
f0102976:	68 48 82 10 f0       	push   $0xf0108248
f010297b:	68 51 75 10 f0       	push   $0xf0107551
f0102980:	68 56 05 00 00       	push   $0x556
f0102985:	68 e1 84 10 f0       	push   $0xf01084e1
f010298a:	e8 bd d6 ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) mm1, 0) & PTE_U));
f010298f:	83 ec 04             	sub    $0x4,%esp
f0102992:	6a 00                	push   $0x0
f0102994:	53                   	push   %ebx
f0102995:	ff 35 28 44 23 f0    	pushl  0xf0234428
f010299b:	e8 79 e9 ff ff       	call   f0101319 <pgdir_walk>
f01029a0:	8b 00                	mov    (%eax),%eax
f01029a2:	83 c4 10             	add    $0x10,%esp
f01029a5:	83 e0 04             	and    $0x4,%eax
f01029a8:	89 45 c8             	mov    %eax,-0x38(%ebp)
f01029ab:	74 19                	je     f01029c6 <mem_init+0x1257>
f01029ad:	68 8c 82 10 f0       	push   $0xf010828c
f01029b2:	68 51 75 10 f0       	push   $0xf0107551
f01029b7:	68 57 05 00 00       	push   $0x557
f01029bc:	68 e1 84 10 f0       	push   $0xf01084e1
f01029c1:	e8 86 d6 ff ff       	call   f010004c <_panic>
	// clear the mappings
	*pgdir_walk(kern_pgdir, (void*) mm1, 0) = 0;
f01029c6:	83 ec 04             	sub    $0x4,%esp
f01029c9:	6a 00                	push   $0x0
f01029cb:	53                   	push   %ebx
f01029cc:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01029d2:	e8 42 e9 ff ff       	call   f0101319 <pgdir_walk>
f01029d7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	*pgdir_walk(kern_pgdir, (void*) mm1 + PGSIZE, 0) = 0;
f01029dd:	83 c4 0c             	add    $0xc,%esp
f01029e0:	6a 00                	push   $0x0
f01029e2:	ff 75 d4             	pushl  -0x2c(%ebp)
f01029e5:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01029eb:	e8 29 e9 ff ff       	call   f0101319 <pgdir_walk>
f01029f0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	*pgdir_walk(kern_pgdir, (void*) mm2, 0) = 0;
f01029f6:	83 c4 0c             	add    $0xc,%esp
f01029f9:	6a 00                	push   $0x0
f01029fb:	56                   	push   %esi
f01029fc:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0102a02:	e8 12 e9 ff ff       	call   f0101319 <pgdir_walk>
f0102a07:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

	cprintf("check_page() succeeded!\n");
f0102a0d:	c7 04 24 de 88 10 f0 	movl   $0xf01088de,(%esp)
f0102a14:	e8 f8 11 00 00       	call   f0103c11 <cprintf>
	//    - the new image at UPAGES -- kernel R, user R
	//      (ie. perm = PTE_U | PTE_P)
	//    - pages itself -- kernel RW, user NONE
	// Your code goes here:
	size_t pags_Size = npages * sizeof(struct PageInfo);
	physaddr_t pages_PA = PADDR(pages);//page2pa(pages);
f0102a19:	a1 2c 44 23 f0       	mov    0xf023442c,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102a1e:	83 c4 10             	add    $0x10,%esp
f0102a21:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102a26:	77 15                	ja     f0102a3d <mem_init+0x12ce>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102a28:	50                   	push   %eax
f0102a29:	68 0c 75 10 f0       	push   $0xf010750c
f0102a2e:	68 cd 00 00 00       	push   $0xcd
f0102a33:	68 e1 84 10 f0       	push   $0xf01084e1
f0102a38:	e8 0f d6 ff ff       	call   f010004c <_panic>
//boot_map_region(kern_pgdir, UPAGES, PTSIZE, pages_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U);
f0102a3d:	83 ec 08             	sub    $0x8,%esp
f0102a40:	6a 04                	push   $0x4
f0102a42:	05 00 00 00 10       	add    $0x10000000,%eax
f0102a47:	50                   	push   %eax
f0102a48:	b9 00 00 40 00       	mov    $0x400000,%ecx
f0102a4d:	ba 00 00 00 ef       	mov    $0xef000000,%edx
f0102a52:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0102a57:	e8 a1 e9 ff ff       	call   f01013fd <boot_map_region>

	size_t envs_Size = NENV * sizeof(struct Env);
	physaddr_t envs_PA = PADDR(envs);
f0102a5c:	a1 44 32 23 f0       	mov    0xf0233244,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102a61:	83 c4 10             	add    $0x10,%esp
f0102a64:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102a69:	77 15                	ja     f0102a80 <mem_init+0x1311>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102a6b:	50                   	push   %eax
f0102a6c:	68 0c 75 10 f0       	push   $0xf010750c
f0102a71:	68 d3 00 00 00       	push   $0xd3
f0102a76:	68 e1 84 10 f0       	push   $0xf01084e1
f0102a7b:	e8 cc d5 ff ff       	call   f010004c <_panic>
	//the memory backing envs should also be mapped user read-only at UENVS
	//boot_map_region(kern_pgdir, UENVS, envs_Size, envs_PA, PTE_U | PTE_P);
//PTSIZE
//boot_map_region(kern_pgdir, UENVS, PTSIZE, envs_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UENVS, PTSIZE, PADDR(envs), PTE_U);
f0102a80:	83 ec 08             	sub    $0x8,%esp
f0102a83:	6a 04                	push   $0x4
f0102a85:	05 00 00 00 10       	add    $0x10000000,%eax
f0102a8a:	50                   	push   %eax
f0102a8b:	b9 00 00 40 00       	mov    $0x400000,%ecx
f0102a90:	ba 00 00 c0 ee       	mov    $0xeec00000,%edx
f0102a95:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0102a9a:	e8 5e e9 ff ff       	call   f01013fd <boot_map_region>
	cprintf("In mem_init(), UENVS is %p, and envs is: %p\n\n\n\n", UENVS, envs);
f0102a9f:	83 c4 0c             	add    $0xc,%esp
f0102aa2:	ff 35 44 32 23 f0    	pushl  0xf0233244
f0102aa8:	68 00 00 c0 ee       	push   $0xeec00000
f0102aad:	68 c0 82 10 f0       	push   $0xf01082c0
f0102ab2:	e8 5a 11 00 00       	call   f0103c11 <cprintf>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102ab7:	83 c4 10             	add    $0x10,%esp
f0102aba:	b8 00 c0 11 f0       	mov    $0xf011c000,%eax
f0102abf:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102ac4:	77 15                	ja     f0102adb <mem_init+0x136c>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102ac6:	50                   	push   %eax
f0102ac7:	68 0c 75 10 f0       	push   $0xf010750c
f0102acc:	68 f8 00 00 00       	push   $0xf8
f0102ad1:	68 e1 84 10 f0       	push   $0xf01084e1
f0102ad6:	e8 71 d5 ff ff       	call   f010004c <_panic>
	  #### (unmapped)
	  KSTACKTOP - PTSIZE
	*/
//boot_map_region(kern_pgdir, (KSTACKTOP-KSTKSIZE), KSTKSIZE, ((physaddr_t)PADDR(bootstack)), PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KSTACKTOP - KSTKSIZE, KSTKSIZE, PADDR(bootstack), PTE_W);  
f0102adb:	83 ec 08             	sub    $0x8,%esp
f0102ade:	6a 02                	push   $0x2
f0102ae0:	68 00 c0 11 00       	push   $0x11c000
f0102ae5:	b9 00 80 00 00       	mov    $0x8000,%ecx
f0102aea:	ba 00 80 ff ef       	mov    $0xefff8000,%edx
f0102aef:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0102af4:	e8 04 e9 ff ff       	call   f01013fd <boot_map_region>
	// we just set up the mapping anyway.
	// Permissions: kernel RW, user NONE
	// Your code goes here:
//boot_map_region(kern_pgdir, KERNBASE, (0xFFFFFFFF - KERNBASE), 0, PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KERNBASE, -KERNBASE, 0, PTE_W); // -KERNBASE <==> ~KERNBASE + 1 
f0102af9:	83 c4 08             	add    $0x8,%esp
f0102afc:	6a 02                	push   $0x2
f0102afe:	6a 00                	push   $0x0
f0102b00:	b9 00 00 00 10       	mov    $0x10000000,%ecx
f0102b05:	ba 00 00 00 f0       	mov    $0xf0000000,%edx
f0102b0a:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0102b0f:	e8 e9 e8 ff ff       	call   f01013fd <boot_map_region>
f0102b14:	c7 45 bc 00 60 23 f0 	movl   $0xf0236000,-0x44(%ebp)
f0102b1b:	83 c4 10             	add    $0x10,%esp
f0102b1e:	bb 00 60 23 f0       	mov    $0xf0236000,%ebx
	//             it will fault rather than overwrite another CPU's stack.
	//             Known as a "guard page".
	//     Permissions: kernel RW, user NONE
	//
	// LAB 4: Your code here:
	uintptr_t kernel_Stack_top = KSTACKTOP - KSTKSIZE;
f0102b23:	be 00 80 ff ef       	mov    $0xefff8000,%esi
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102b28:	81 fb ff ff ff ef    	cmp    $0xefffffff,%ebx
f0102b2e:	77 15                	ja     f0102b45 <mem_init+0x13d6>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102b30:	53                   	push   %ebx
f0102b31:	68 0c 75 10 f0       	push   $0xf010750c
f0102b36:	68 44 01 00 00       	push   $0x144
f0102b3b:	68 e1 84 10 f0       	push   $0xf01084e1
f0102b40:	e8 07 d5 ff ff       	call   f010004c <_panic>

    int i;
    for (i = 0; i < NCPU; i++) {
        boot_map_region(kern_pgdir, kernel_Stack_top, KSTKSIZE, PADDR(percpu_kstacks[i]), PTE_W);
f0102b45:	83 ec 08             	sub    $0x8,%esp
f0102b48:	6a 02                	push   $0x2
f0102b4a:	8d 83 00 00 00 10    	lea    0x10000000(%ebx),%eax
f0102b50:	50                   	push   %eax
f0102b51:	b9 00 80 00 00       	mov    $0x8000,%ecx
f0102b56:	89 f2                	mov    %esi,%edx
f0102b58:	a1 28 44 23 f0       	mov    0xf0234428,%eax
f0102b5d:	e8 9b e8 ff ff       	call   f01013fd <boot_map_region>
        kernel_Stack_top -= (KSTKSIZE + KSTKGAP);
f0102b62:	81 ee 00 00 01 00    	sub    $0x10000,%esi
f0102b68:	81 c3 00 80 00 00    	add    $0x8000,%ebx
	//
	// LAB 4: Your code here:
	uintptr_t kernel_Stack_top = KSTACKTOP - KSTKSIZE;

    int i;
    for (i = 0; i < NCPU; i++) {
f0102b6e:	83 c4 10             	add    $0x10,%esp
f0102b71:	81 fe 00 80 f7 ef    	cmp    $0xeff78000,%esi
f0102b77:	75 af                	jne    f0102b28 <mem_init+0x13b9>
check_kern_pgdir(void)
{
	uint32_t i, n;
	pde_t *pgdir;

	pgdir = kern_pgdir;
f0102b79:	8b 3d 28 44 23 f0    	mov    0xf0234428,%edi

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
f0102b7f:	a1 24 44 23 f0       	mov    0xf0234424,%eax
f0102b84:	89 45 c4             	mov    %eax,-0x3c(%ebp)
f0102b87:	8d 04 c5 ff 0f 00 00 	lea    0xfff(,%eax,8),%eax
f0102b8e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
	for (i = 0; i < n; i += PGSIZE){
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f0102b93:	8b 35 2c 44 23 f0    	mov    0xf023442c,%esi
f0102b99:	89 75 c0             	mov    %esi,-0x40(%ebp)
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102b9c:	89 75 d0             	mov    %esi,-0x30(%ebp)
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
	return (physaddr_t)kva - KERNBASE;
f0102b9f:	81 c6 00 00 00 10    	add    $0x10000000,%esi
f0102ba5:	89 75 cc             	mov    %esi,-0x34(%ebp)
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f0102ba8:	8b 35 44 32 23 f0    	mov    0xf0233244,%esi
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102bae:	89 75 d4             	mov    %esi,-0x2c(%ebp)

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f0102bb1:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102bb6:	e9 b8 00 00 00       	jmp    f0102c73 <mem_init+0x1504>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f0102bbb:	8d 93 00 00 00 ef    	lea    -0x11000000(%ebx),%edx
f0102bc1:	89 f8                	mov    %edi,%eax
f0102bc3:	e8 69 e1 ff ff       	call   f0100d31 <check_va2pa>
f0102bc8:	81 7d d0 ff ff ff ef 	cmpl   $0xefffffff,-0x30(%ebp)
f0102bcf:	77 17                	ja     f0102be8 <mem_init+0x1479>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102bd1:	ff 75 c0             	pushl  -0x40(%ebp)
f0102bd4:	68 0c 75 10 f0       	push   $0xf010750c
f0102bd9:	68 4d 04 00 00       	push   $0x44d
f0102bde:	68 e1 84 10 f0       	push   $0xf01084e1
f0102be3:	e8 64 d4 ff ff       	call   f010004c <_panic>
f0102be8:	03 5d cc             	add    -0x34(%ebp),%ebx
f0102beb:	39 d8                	cmp    %ebx,%eax
f0102bed:	74 19                	je     f0102c08 <mem_init+0x1499>
f0102bef:	68 f0 82 10 f0       	push   $0xf01082f0
f0102bf4:	68 51 75 10 f0       	push   $0xf0107551
f0102bf9:	68 4d 04 00 00       	push   $0x44d
f0102bfe:	68 e1 84 10 f0       	push   $0xf01084e1
f0102c03:	e8 44 d4 ff ff       	call   f010004c <_panic>
f0102c08:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102c0d:	eb 02                	jmp    f0102c11 <mem_init+0x14a2>
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102c0f:	89 c3                	mov    %eax,%ebx
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f0102c11:	8d 93 00 00 c0 ee    	lea    -0x11400000(%ebx),%edx
f0102c17:	89 f8                	mov    %edi,%eax
f0102c19:	e8 13 e1 ff ff       	call   f0100d31 <check_va2pa>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102c1e:	81 7d d4 ff ff ff ef 	cmpl   $0xefffffff,-0x2c(%ebp)
f0102c25:	77 15                	ja     f0102c3c <mem_init+0x14cd>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102c27:	56                   	push   %esi
f0102c28:	68 0c 75 10 f0       	push   $0xf010750c
f0102c2d:	68 53 04 00 00       	push   $0x453
f0102c32:	68 e1 84 10 f0       	push   $0xf01084e1
f0102c37:	e8 10 d4 ff ff       	call   f010004c <_panic>
f0102c3c:	8d 94 1e 00 00 00 10 	lea    0x10000000(%esi,%ebx,1),%edx
f0102c43:	39 d0                	cmp    %edx,%eax
f0102c45:	74 19                	je     f0102c60 <mem_init+0x14f1>
f0102c47:	68 24 83 10 f0       	push   $0xf0108324
f0102c4c:	68 51 75 10 f0       	push   $0xf0107551
f0102c51:	68 53 04 00 00       	push   $0x453
f0102c56:	68 e1 84 10 f0       	push   $0xf01084e1
f0102c5b:	e8 ec d3 ff ff       	call   f010004c <_panic>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102c60:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
f0102c66:	3d 00 f0 01 00       	cmp    $0x1f000,%eax
f0102c6b:	75 a2                	jne    f0102c0f <mem_init+0x14a0>

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f0102c6d:	81 c3 00 20 00 00    	add    $0x2000,%ebx
f0102c73:	39 c3                	cmp    %eax,%ebx
f0102c75:	0f 82 40 ff ff ff    	jb     f0102bbb <mem_init+0x144c>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f0102c7b:	8b 75 c4             	mov    -0x3c(%ebp),%esi
f0102c7e:	c1 e6 0c             	shl    $0xc,%esi
f0102c81:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102c86:	eb 30                	jmp    f0102cb8 <mem_init+0x1549>
		assert(check_va2pa(pgdir, KERNBASE + i) == i);
f0102c88:	8d 93 00 00 00 f0    	lea    -0x10000000(%ebx),%edx
f0102c8e:	89 f8                	mov    %edi,%eax
f0102c90:	e8 9c e0 ff ff       	call   f0100d31 <check_va2pa>
f0102c95:	39 c3                	cmp    %eax,%ebx
f0102c97:	74 19                	je     f0102cb2 <mem_init+0x1543>
f0102c99:	68 58 83 10 f0       	push   $0xf0108358
f0102c9e:	68 51 75 10 f0       	push   $0xf0107551
f0102ca3:	68 5a 04 00 00       	push   $0x45a
f0102ca8:	68 e1 84 10 f0       	push   $0xf01084e1
f0102cad:	e8 9a d3 ff ff       	call   f010004c <_panic>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f0102cb2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f0102cb8:	39 f3                	cmp    %esi,%ebx
f0102cba:	72 cc                	jb     f0102c88 <mem_init+0x1519>
f0102cbc:	c7 45 cc 00 80 ff ef 	movl   $0xefff8000,-0x34(%ebp)
f0102cc3:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f0102cc6:	8b 7d bc             	mov    -0x44(%ebp),%edi
f0102cc9:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0102ccc:	89 c3                	mov    %eax,%ebx
f0102cce:	05 00 80 00 00       	add    $0x8000,%eax
f0102cd3:	89 45 d0             	mov    %eax,-0x30(%ebp)
	// check kernel stack
	// (updated in lab 4 to check per-CPU kernel stacks)
	for (n = 0; n < NCPU; n++) {
		uint32_t base = KSTACKTOP - (KSTKSIZE + KSTKGAP) * (n + 1);
		for (i = 0; i < KSTKSIZE; i += PGSIZE)
			assert(check_va2pa(pgdir, base + KSTKGAP + i)
f0102cd6:	8b 45 c8             	mov    -0x38(%ebp),%eax
f0102cd9:	8d b0 00 80 00 20    	lea    0x20008000(%eax),%esi
f0102cdf:	89 da                	mov    %ebx,%edx
f0102ce1:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102ce4:	e8 48 e0 ff ff       	call   f0100d31 <check_va2pa>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102ce9:	81 ff ff ff ff ef    	cmp    $0xefffffff,%edi
f0102cef:	77 15                	ja     f0102d06 <mem_init+0x1597>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102cf1:	57                   	push   %edi
f0102cf2:	68 0c 75 10 f0       	push   $0xf010750c
f0102cf7:	68 62 04 00 00       	push   $0x462
f0102cfc:	68 e1 84 10 f0       	push   $0xf01084e1
f0102d01:	e8 46 d3 ff ff       	call   f010004c <_panic>
f0102d06:	8d 94 33 00 60 23 f0 	lea    -0xfdca000(%ebx,%esi,1),%edx
f0102d0d:	39 d0                	cmp    %edx,%eax
f0102d0f:	74 19                	je     f0102d2a <mem_init+0x15bb>
f0102d11:	68 80 83 10 f0       	push   $0xf0108380
f0102d16:	68 51 75 10 f0       	push   $0xf0107551
f0102d1b:	68 62 04 00 00       	push   $0x462
f0102d20:	68 e1 84 10 f0       	push   $0xf01084e1
f0102d25:	e8 22 d3 ff ff       	call   f010004c <_panic>
f0102d2a:	81 c3 00 10 00 00    	add    $0x1000,%ebx

	// check kernel stack
	// (updated in lab 4 to check per-CPU kernel stacks)
	for (n = 0; n < NCPU; n++) {
		uint32_t base = KSTACKTOP - (KSTKSIZE + KSTKGAP) * (n + 1);
		for (i = 0; i < KSTKSIZE; i += PGSIZE)
f0102d30:	3b 5d d0             	cmp    -0x30(%ebp),%ebx
f0102d33:	75 aa                	jne    f0102cdf <mem_init+0x1570>
f0102d35:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0102d38:	8d 98 00 80 ff ff    	lea    -0x8000(%eax),%ebx
f0102d3e:	89 c6                	mov    %eax,%esi
f0102d40:	89 7d d0             	mov    %edi,-0x30(%ebp)
f0102d43:	8b 7d d4             	mov    -0x2c(%ebp),%edi
			assert(check_va2pa(pgdir, base + KSTKGAP + i)
				== PADDR(percpu_kstacks[n]) + i);
		for (i = 0; i < KSTKGAP; i += PGSIZE)
			assert(check_va2pa(pgdir, base + i) == ~0);
f0102d46:	89 da                	mov    %ebx,%edx
f0102d48:	89 f8                	mov    %edi,%eax
f0102d4a:	e8 e2 df ff ff       	call   f0100d31 <check_va2pa>
f0102d4f:	83 f8 ff             	cmp    $0xffffffff,%eax
f0102d52:	74 19                	je     f0102d6d <mem_init+0x15fe>
f0102d54:	68 c8 83 10 f0       	push   $0xf01083c8
f0102d59:	68 51 75 10 f0       	push   $0xf0107551
f0102d5e:	68 64 04 00 00       	push   $0x464
f0102d63:	68 e1 84 10 f0       	push   $0xf01084e1
f0102d68:	e8 df d2 ff ff       	call   f010004c <_panic>
f0102d6d:	81 c3 00 10 00 00    	add    $0x1000,%ebx
	for (n = 0; n < NCPU; n++) {
		uint32_t base = KSTACKTOP - (KSTKSIZE + KSTKGAP) * (n + 1);
		for (i = 0; i < KSTKSIZE; i += PGSIZE)
			assert(check_va2pa(pgdir, base + KSTKGAP + i)
				== PADDR(percpu_kstacks[n]) + i);
		for (i = 0; i < KSTKGAP; i += PGSIZE)
f0102d73:	39 f3                	cmp    %esi,%ebx
f0102d75:	75 cf                	jne    f0102d46 <mem_init+0x15d7>
f0102d77:	8b 7d d0             	mov    -0x30(%ebp),%edi
f0102d7a:	81 6d cc 00 00 01 00 	subl   $0x10000,-0x34(%ebp)
f0102d81:	81 45 c8 00 80 01 00 	addl   $0x18000,-0x38(%ebp)
f0102d88:	81 c7 00 80 00 00    	add    $0x8000,%edi
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	// (updated in lab 4 to check per-CPU kernel stacks)
	for (n = 0; n < NCPU; n++) {
f0102d8e:	81 ff 00 60 27 f0    	cmp    $0xf0276000,%edi
f0102d94:	0f 85 2f ff ff ff    	jne    f0102cc9 <mem_init+0x155a>
f0102d9a:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0102d9d:	b8 00 00 00 00       	mov    $0x0,%eax
f0102da2:	eb 2a                	jmp    f0102dce <mem_init+0x165f>
			assert(check_va2pa(pgdir, base + i) == ~0);
	}

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
		switch (i) {
f0102da4:	8d 90 45 fc ff ff    	lea    -0x3bb(%eax),%edx
f0102daa:	83 fa 04             	cmp    $0x4,%edx
f0102dad:	77 1f                	ja     f0102dce <mem_init+0x165f>
		case PDX(UVPT):
		case PDX(KSTACKTOP-1):
		case PDX(UPAGES):
		case PDX(UENVS):
		case PDX(MMIOBASE):
			assert(pgdir[i] & PTE_P);
f0102daf:	f6 04 87 01          	testb  $0x1,(%edi,%eax,4)
f0102db3:	75 7e                	jne    f0102e33 <mem_init+0x16c4>
f0102db5:	68 f7 88 10 f0       	push   $0xf01088f7
f0102dba:	68 51 75 10 f0       	push   $0xf0107551
f0102dbf:	68 6f 04 00 00       	push   $0x46f
f0102dc4:	68 e1 84 10 f0       	push   $0xf01084e1
f0102dc9:	e8 7e d2 ff ff       	call   f010004c <_panic>
			break;
		default:
			if (i >= PDX(KERNBASE)) {
f0102dce:	3d bf 03 00 00       	cmp    $0x3bf,%eax
f0102dd3:	76 3f                	jbe    f0102e14 <mem_init+0x16a5>
				assert(pgdir[i] & PTE_P);
f0102dd5:	8b 14 87             	mov    (%edi,%eax,4),%edx
f0102dd8:	f6 c2 01             	test   $0x1,%dl
f0102ddb:	75 19                	jne    f0102df6 <mem_init+0x1687>
f0102ddd:	68 f7 88 10 f0       	push   $0xf01088f7
f0102de2:	68 51 75 10 f0       	push   $0xf0107551
f0102de7:	68 73 04 00 00       	push   $0x473
f0102dec:	68 e1 84 10 f0       	push   $0xf01084e1
f0102df1:	e8 56 d2 ff ff       	call   f010004c <_panic>
				assert(pgdir[i] & PTE_W);
f0102df6:	f6 c2 02             	test   $0x2,%dl
f0102df9:	75 38                	jne    f0102e33 <mem_init+0x16c4>
f0102dfb:	68 08 89 10 f0       	push   $0xf0108908
f0102e00:	68 51 75 10 f0       	push   $0xf0107551
f0102e05:	68 74 04 00 00       	push   $0x474
f0102e0a:	68 e1 84 10 f0       	push   $0xf01084e1
f0102e0f:	e8 38 d2 ff ff       	call   f010004c <_panic>
			} else
				assert(pgdir[i] == 0);
f0102e14:	83 3c 87 00          	cmpl   $0x0,(%edi,%eax,4)
f0102e18:	74 19                	je     f0102e33 <mem_init+0x16c4>
f0102e1a:	68 19 89 10 f0       	push   $0xf0108919
f0102e1f:	68 51 75 10 f0       	push   $0xf0107551
f0102e24:	68 76 04 00 00       	push   $0x476
f0102e29:	68 e1 84 10 f0       	push   $0xf01084e1
f0102e2e:	e8 19 d2 ff ff       	call   f010004c <_panic>
		for (i = 0; i < KSTKGAP; i += PGSIZE)
			assert(check_va2pa(pgdir, base + i) == ~0);
	}

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
f0102e33:	40                   	inc    %eax
f0102e34:	3d ff 03 00 00       	cmp    $0x3ff,%eax
f0102e39:	0f 86 65 ff ff ff    	jbe    f0102da4 <mem_init+0x1635>
			} else
				assert(pgdir[i] == 0);
			break;
		}
	}
	cprintf("check_kern_pgdir() succeeded!\n");
f0102e3f:	83 ec 0c             	sub    $0xc,%esp
f0102e42:	68 ec 83 10 f0       	push   $0xf01083ec
f0102e47:	e8 c5 0d 00 00       	call   f0103c11 <cprintf>
	// somewhere between KERNBASE and KERNBASE+4MB right now, which is
	// mapped the same way by both page tables.
	//
	// If the machine reboots at this point, you've probably set up your
	// kern_pgdir wrong.
	lcr3(PADDR(kern_pgdir));
f0102e4c:	a1 28 44 23 f0       	mov    0xf0234428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102e51:	83 c4 10             	add    $0x10,%esp
f0102e54:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102e59:	77 15                	ja     f0102e70 <mem_init+0x1701>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102e5b:	50                   	push   %eax
f0102e5c:	68 0c 75 10 f0       	push   $0xf010750c
f0102e61:	68 16 01 00 00       	push   $0x116
f0102e66:	68 e1 84 10 f0       	push   $0xf01084e1
f0102e6b:	e8 dc d1 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0102e70:	05 00 00 00 10       	add    $0x10000000,%eax
f0102e75:	0f 22 d8             	mov    %eax,%cr3

	check_page_free_list(0);
f0102e78:	b8 00 00 00 00       	mov    $0x0,%eax
f0102e7d:	e8 0e df ff ff       	call   f0100d90 <check_page_free_list>

static inline uint32_t
rcr0(void)
{
	uint32_t val;
	asm volatile("movl %%cr0,%0" : "=r" (val));
f0102e82:	0f 20 c0             	mov    %cr0,%eax
f0102e85:	83 e0 f3             	and    $0xfffffff3,%eax
}

static inline void
lcr0(uint32_t val)
{
	asm volatile("movl %0,%%cr0" : : "r" (val));
f0102e88:	0d 23 00 05 80       	or     $0x80050023,%eax
f0102e8d:	0f 22 c0             	mov    %eax,%cr0
	uintptr_t va;
	int i;

	// check that we can read and write installed pages
	pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0102e90:	83 ec 0c             	sub    $0xc,%esp
f0102e93:	6a 00                	push   $0x0
f0102e95:	e8 88 e3 ff ff       	call   f0101222 <page_alloc>
f0102e9a:	89 c3                	mov    %eax,%ebx
f0102e9c:	83 c4 10             	add    $0x10,%esp
f0102e9f:	85 c0                	test   %eax,%eax
f0102ea1:	75 19                	jne    f0102ebc <mem_init+0x174d>
f0102ea3:	68 eb 86 10 f0       	push   $0xf01086eb
f0102ea8:	68 51 75 10 f0       	push   $0xf0107551
f0102ead:	68 6c 05 00 00       	push   $0x56c
f0102eb2:	68 e1 84 10 f0       	push   $0xf01084e1
f0102eb7:	e8 90 d1 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0102ebc:	83 ec 0c             	sub    $0xc,%esp
f0102ebf:	6a 00                	push   $0x0
f0102ec1:	e8 5c e3 ff ff       	call   f0101222 <page_alloc>
f0102ec6:	89 c7                	mov    %eax,%edi
f0102ec8:	83 c4 10             	add    $0x10,%esp
f0102ecb:	85 c0                	test   %eax,%eax
f0102ecd:	75 19                	jne    f0102ee8 <mem_init+0x1779>
f0102ecf:	68 01 87 10 f0       	push   $0xf0108701
f0102ed4:	68 51 75 10 f0       	push   $0xf0107551
f0102ed9:	68 6d 05 00 00       	push   $0x56d
f0102ede:	68 e1 84 10 f0       	push   $0xf01084e1
f0102ee3:	e8 64 d1 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0102ee8:	83 ec 0c             	sub    $0xc,%esp
f0102eeb:	6a 00                	push   $0x0
f0102eed:	e8 30 e3 ff ff       	call   f0101222 <page_alloc>
f0102ef2:	89 c6                	mov    %eax,%esi
f0102ef4:	83 c4 10             	add    $0x10,%esp
f0102ef7:	85 c0                	test   %eax,%eax
f0102ef9:	75 19                	jne    f0102f14 <mem_init+0x17a5>
f0102efb:	68 17 87 10 f0       	push   $0xf0108717
f0102f00:	68 51 75 10 f0       	push   $0xf0107551
f0102f05:	68 6e 05 00 00       	push   $0x56e
f0102f0a:	68 e1 84 10 f0       	push   $0xf01084e1
f0102f0f:	e8 38 d1 ff ff       	call   f010004c <_panic>
	page_free(pp0);
f0102f14:	83 ec 0c             	sub    $0xc,%esp
f0102f17:	53                   	push   %ebx
f0102f18:	e8 7b e3 ff ff       	call   f0101298 <page_free>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102f1d:	89 f8                	mov    %edi,%eax
f0102f1f:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0102f25:	c1 f8 03             	sar    $0x3,%eax
f0102f28:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102f2b:	89 c2                	mov    %eax,%edx
f0102f2d:	c1 ea 0c             	shr    $0xc,%edx
f0102f30:	83 c4 10             	add    $0x10,%esp
f0102f33:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f0102f39:	72 12                	jb     f0102f4d <mem_init+0x17de>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102f3b:	50                   	push   %eax
f0102f3c:	68 e8 74 10 f0       	push   $0xf01074e8
f0102f41:	6a 58                	push   $0x58
f0102f43:	68 ed 84 10 f0       	push   $0xf01084ed
f0102f48:	e8 ff d0 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp1), 1, PGSIZE);
f0102f4d:	83 ec 04             	sub    $0x4,%esp
f0102f50:	68 00 10 00 00       	push   $0x1000
f0102f55:	6a 01                	push   $0x1
f0102f57:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102f5c:	50                   	push   %eax
f0102f5d:	e8 c6 30 00 00       	call   f0106028 <memset>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102f62:	89 f0                	mov    %esi,%eax
f0102f64:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0102f6a:	c1 f8 03             	sar    $0x3,%eax
f0102f6d:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102f70:	89 c2                	mov    %eax,%edx
f0102f72:	c1 ea 0c             	shr    $0xc,%edx
f0102f75:	83 c4 10             	add    $0x10,%esp
f0102f78:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f0102f7e:	72 12                	jb     f0102f92 <mem_init+0x1823>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102f80:	50                   	push   %eax
f0102f81:	68 e8 74 10 f0       	push   $0xf01074e8
f0102f86:	6a 58                	push   $0x58
f0102f88:	68 ed 84 10 f0       	push   $0xf01084ed
f0102f8d:	e8 ba d0 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp2), 2, PGSIZE);
f0102f92:	83 ec 04             	sub    $0x4,%esp
f0102f95:	68 00 10 00 00       	push   $0x1000
f0102f9a:	6a 02                	push   $0x2
f0102f9c:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102fa1:	50                   	push   %eax
f0102fa2:	e8 81 30 00 00       	call   f0106028 <memset>
	page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W);
f0102fa7:	6a 02                	push   $0x2
f0102fa9:	68 00 10 00 00       	push   $0x1000
f0102fae:	57                   	push   %edi
f0102faf:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0102fb5:	e8 e4 e6 ff ff       	call   f010169e <page_insert>
	assert(pp1->pp_ref == 1);
f0102fba:	83 c4 20             	add    $0x20,%esp
f0102fbd:	66 83 7f 04 01       	cmpw   $0x1,0x4(%edi)
f0102fc2:	74 19                	je     f0102fdd <mem_init+0x186e>
f0102fc4:	68 e8 87 10 f0       	push   $0xf01087e8
f0102fc9:	68 51 75 10 f0       	push   $0xf0107551
f0102fce:	68 73 05 00 00       	push   $0x573
f0102fd3:	68 e1 84 10 f0       	push   $0xf01084e1
f0102fd8:	e8 6f d0 ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)PGSIZE == 0x01010101U);
f0102fdd:	81 3d 00 10 00 00 01 	cmpl   $0x1010101,0x1000
f0102fe4:	01 01 01 
f0102fe7:	74 19                	je     f0103002 <mem_init+0x1893>
f0102fe9:	68 0c 84 10 f0       	push   $0xf010840c
f0102fee:	68 51 75 10 f0       	push   $0xf0107551
f0102ff3:	68 74 05 00 00       	push   $0x574
f0102ff8:	68 e1 84 10 f0       	push   $0xf01084e1
f0102ffd:	e8 4a d0 ff ff       	call   f010004c <_panic>
	page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W);
f0103002:	6a 02                	push   $0x2
f0103004:	68 00 10 00 00       	push   $0x1000
f0103009:	56                   	push   %esi
f010300a:	ff 35 28 44 23 f0    	pushl  0xf0234428
f0103010:	e8 89 e6 ff ff       	call   f010169e <page_insert>
	assert(*(uint32_t *)PGSIZE == 0x02020202U);
f0103015:	83 c4 10             	add    $0x10,%esp
f0103018:	81 3d 00 10 00 00 02 	cmpl   $0x2020202,0x1000
f010301f:	02 02 02 
f0103022:	74 19                	je     f010303d <mem_init+0x18ce>
f0103024:	68 30 84 10 f0       	push   $0xf0108430
f0103029:	68 51 75 10 f0       	push   $0xf0107551
f010302e:	68 76 05 00 00       	push   $0x576
f0103033:	68 e1 84 10 f0       	push   $0xf01084e1
f0103038:	e8 0f d0 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f010303d:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0103042:	74 19                	je     f010305d <mem_init+0x18ee>
f0103044:	68 0a 88 10 f0       	push   $0xf010880a
f0103049:	68 51 75 10 f0       	push   $0xf0107551
f010304e:	68 77 05 00 00       	push   $0x577
f0103053:	68 e1 84 10 f0       	push   $0xf01084e1
f0103058:	e8 ef cf ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f010305d:	66 83 7f 04 00       	cmpw   $0x0,0x4(%edi)
f0103062:	74 19                	je     f010307d <mem_init+0x190e>
f0103064:	68 74 88 10 f0       	push   $0xf0108874
f0103069:	68 51 75 10 f0       	push   $0xf0107551
f010306e:	68 78 05 00 00       	push   $0x578
f0103073:	68 e1 84 10 f0       	push   $0xf01084e1
f0103078:	e8 cf cf ff ff       	call   f010004c <_panic>
	*(uint32_t *)PGSIZE = 0x03030303U;
f010307d:	c7 05 00 10 00 00 03 	movl   $0x3030303,0x1000
f0103084:	03 03 03 
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0103087:	89 f0                	mov    %esi,%eax
f0103089:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f010308f:	c1 f8 03             	sar    $0x3,%eax
f0103092:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103095:	89 c2                	mov    %eax,%edx
f0103097:	c1 ea 0c             	shr    $0xc,%edx
f010309a:	3b 15 24 44 23 f0    	cmp    0xf0234424,%edx
f01030a0:	72 12                	jb     f01030b4 <mem_init+0x1945>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01030a2:	50                   	push   %eax
f01030a3:	68 e8 74 10 f0       	push   $0xf01074e8
f01030a8:	6a 58                	push   $0x58
f01030aa:	68 ed 84 10 f0       	push   $0xf01084ed
f01030af:	e8 98 cf ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)page2kva(pp2) == 0x03030303U);
f01030b4:	81 b8 00 00 00 f0 03 	cmpl   $0x3030303,-0x10000000(%eax)
f01030bb:	03 03 03 
f01030be:	74 19                	je     f01030d9 <mem_init+0x196a>
f01030c0:	68 54 84 10 f0       	push   $0xf0108454
f01030c5:	68 51 75 10 f0       	push   $0xf0107551
f01030ca:	68 7a 05 00 00       	push   $0x57a
f01030cf:	68 e1 84 10 f0       	push   $0xf01084e1
f01030d4:	e8 73 cf ff ff       	call   f010004c <_panic>
	page_remove(kern_pgdir, (void*) PGSIZE);
f01030d9:	83 ec 08             	sub    $0x8,%esp
f01030dc:	68 00 10 00 00       	push   $0x1000
f01030e1:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01030e7:	e8 12 e5 ff ff       	call   f01015fe <page_remove>
	assert(pp2->pp_ref == 0);
f01030ec:	83 c4 10             	add    $0x10,%esp
f01030ef:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f01030f4:	74 19                	je     f010310f <mem_init+0x19a0>
f01030f6:	68 42 88 10 f0       	push   $0xf0108842
f01030fb:	68 51 75 10 f0       	push   $0xf0107551
f0103100:	68 7c 05 00 00       	push   $0x57c
f0103105:	68 e1 84 10 f0       	push   $0xf01084e1
f010310a:	e8 3d cf ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f010310f:	8b 0d 28 44 23 f0    	mov    0xf0234428,%ecx
f0103115:	8b 11                	mov    (%ecx),%edx
f0103117:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f010311d:	89 d8                	mov    %ebx,%eax
f010311f:	2b 05 2c 44 23 f0    	sub    0xf023442c,%eax
f0103125:	c1 f8 03             	sar    $0x3,%eax
f0103128:	c1 e0 0c             	shl    $0xc,%eax
f010312b:	39 c2                	cmp    %eax,%edx
f010312d:	74 19                	je     f0103148 <mem_init+0x19d9>
f010312f:	68 ac 7d 10 f0       	push   $0xf0107dac
f0103134:	68 51 75 10 f0       	push   $0xf0107551
f0103139:	68 7f 05 00 00       	push   $0x57f
f010313e:	68 e1 84 10 f0       	push   $0xf01084e1
f0103143:	e8 04 cf ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f0103148:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f010314e:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0103153:	74 19                	je     f010316e <mem_init+0x19ff>
f0103155:	68 f9 87 10 f0       	push   $0xf01087f9
f010315a:	68 51 75 10 f0       	push   $0xf0107551
f010315f:	68 81 05 00 00       	push   $0x581
f0103164:	68 e1 84 10 f0       	push   $0xf01084e1
f0103169:	e8 de ce ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f010316e:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)

	// free the pages we took
	page_free(pp0);
f0103174:	83 ec 0c             	sub    $0xc,%esp
f0103177:	53                   	push   %ebx
f0103178:	e8 1b e1 ff ff       	call   f0101298 <page_free>

	cprintf("check_page_installed_pgdir() succeeded!\n");
f010317d:	c7 04 24 80 84 10 f0 	movl   $0xf0108480,(%esp)
f0103184:	e8 88 0a 00 00       	call   f0103c11 <cprintf>
	cr0 &= ~(CR0_TS|CR0_EM);
	lcr0(cr0);

	// Some more checks, only possible after kern_pgdir is installed.
	check_page_installed_pgdir();
}
f0103189:	83 c4 10             	add    $0x10,%esp
f010318c:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010318f:	5b                   	pop    %ebx
f0103190:	5e                   	pop    %esi
f0103191:	5f                   	pop    %edi
f0103192:	5d                   	pop    %ebp
f0103193:	c3                   	ret    

f0103194 <user_mem_check>:
// Returns 0 if the user program can access this range of addresses,
// and -E_FAULT otherwise.
//
int
user_mem_check(struct Env *env, const void *va, size_t len, int perm)
{
f0103194:	55                   	push   %ebp
f0103195:	89 e5                	mov    %esp,%ebp
f0103197:	57                   	push   %edi
f0103198:	56                   	push   %esi
f0103199:	53                   	push   %ebx
f010319a:	83 ec 1c             	sub    $0x1c,%esp
f010319d:	8b 7d 08             	mov    0x8(%ebp),%edi
	num_Page = (end_Va - va_temp)/PGSIZE;

	//PGSIZE
	return 0;*/
		// LAB 3: Your code here.
    perm |= PTE_P;
f01031a0:	8b 75 14             	mov    0x14(%ebp),%esi
f01031a3:	83 ce 01             	or     $0x1,%esi

	uintptr_t addr = ROUNDDOWN((uint32_t)va, PGSIZE);
f01031a6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01031a9:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
	uintptr_t end = ROUNDUP((uint32_t)va + len, PGSIZE);
f01031af:	8b 45 10             	mov    0x10(%ebp),%eax
f01031b2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f01031b5:	8d 84 01 ff 0f 00 00 	lea    0xfff(%ecx,%eax,1),%eax
f01031bc:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f01031c1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (addr < end) {
f01031c4:	eb 2b                	jmp    f01031f1 <user_mem_check+0x5d>
		if (addr >= ULIM)
f01031c6:	81 fb ff ff 7f ef    	cmp    $0xef7fffff,%ebx
f01031cc:	77 2f                	ja     f01031fd <user_mem_check+0x69>
            goto bad;
        
        pte_t *ppte = pgdir_walk(env->env_pgdir, (void *)addr, 0);
f01031ce:	83 ec 04             	sub    $0x4,%esp
f01031d1:	6a 00                	push   $0x0
f01031d3:	53                   	push   %ebx
f01031d4:	ff 77 60             	pushl  0x60(%edi)
f01031d7:	e8 3d e1 ff ff       	call   f0101319 <pgdir_walk>
        if (!ppte)
f01031dc:	83 c4 10             	add    $0x10,%esp
f01031df:	85 c0                	test   %eax,%eax
f01031e1:	74 1a                	je     f01031fd <user_mem_check+0x69>
            goto bad;
        
        if ((*ppte & perm) != perm)
f01031e3:	89 f2                	mov    %esi,%edx
f01031e5:	23 10                	and    (%eax),%edx
f01031e7:	39 d6                	cmp    %edx,%esi
f01031e9:	75 12                	jne    f01031fd <user_mem_check+0x69>
            goto bad;

        addr += PGSIZE;
f01031eb:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		// LAB 3: Your code here.
    perm |= PTE_P;

	uintptr_t addr = ROUNDDOWN((uint32_t)va, PGSIZE);
	uintptr_t end = ROUNDUP((uint32_t)va + len, PGSIZE);
	while (addr < end) {
f01031f1:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f01031f4:	72 d0                	jb     f01031c6 <user_mem_check+0x32>
            goto bad;

        addr += PGSIZE;
	}

	return 0;
f01031f6:	b8 00 00 00 00       	mov    $0x0,%eax
f01031fb:	eb 1f                	jmp    f010321c <user_mem_check+0x88>

bad:
    user_mem_check_addr = (uintptr_t)va;
    if (user_mem_check_addr < addr)
f01031fd:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
f0103200:	77 0f                	ja     f0103211 <user_mem_check+0x7d>
	}

	return 0;

bad:
    user_mem_check_addr = (uintptr_t)va;
f0103202:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103205:	a3 3c 32 23 f0       	mov    %eax,0xf023323c
    if (user_mem_check_addr < addr)
        user_mem_check_addr = addr;
    return -E_FAULT;
f010320a:	b8 fa ff ff ff       	mov    $0xfffffffa,%eax
f010320f:	eb 0b                	jmp    f010321c <user_mem_check+0x88>
	return 0;

bad:
    user_mem_check_addr = (uintptr_t)va;
    if (user_mem_check_addr < addr)
        user_mem_check_addr = addr;
f0103211:	89 1d 3c 32 23 f0    	mov    %ebx,0xf023323c
    return -E_FAULT;
f0103217:	b8 fa ff ff ff       	mov    $0xfffffffa,%eax
}
f010321c:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010321f:	5b                   	pop    %ebx
f0103220:	5e                   	pop    %esi
f0103221:	5f                   	pop    %edi
f0103222:	5d                   	pop    %ebp
f0103223:	c3                   	ret    

f0103224 <user_mem_assert>:
// If it cannot, 'env' is destroyed and, if env is the current
// environment, this function will not return.
//
void
user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
{
f0103224:	55                   	push   %ebp
f0103225:	89 e5                	mov    %esp,%ebp
f0103227:	53                   	push   %ebx
f0103228:	83 ec 04             	sub    $0x4,%esp
f010322b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (user_mem_check(env, va, len, perm | PTE_U) < 0) {
f010322e:	8b 45 14             	mov    0x14(%ebp),%eax
f0103231:	83 c8 04             	or     $0x4,%eax
f0103234:	50                   	push   %eax
f0103235:	ff 75 10             	pushl  0x10(%ebp)
f0103238:	ff 75 0c             	pushl  0xc(%ebp)
f010323b:	53                   	push   %ebx
f010323c:	e8 53 ff ff ff       	call   f0103194 <user_mem_check>
f0103241:	83 c4 10             	add    $0x10,%esp
f0103244:	85 c0                	test   %eax,%eax
f0103246:	79 21                	jns    f0103269 <user_mem_assert+0x45>
		cprintf("[%08x] user_mem_check assertion failure for "
f0103248:	83 ec 04             	sub    $0x4,%esp
f010324b:	ff 35 3c 32 23 f0    	pushl  0xf023323c
f0103251:	ff 73 48             	pushl  0x48(%ebx)
f0103254:	68 ac 84 10 f0       	push   $0xf01084ac
f0103259:	e8 b3 09 00 00       	call   f0103c11 <cprintf>
			"va %08x\n", env->env_id, user_mem_check_addr);
		env_destroy(env);	// may not return
f010325e:	89 1c 24             	mov    %ebx,(%esp)
f0103261:	e8 09 07 00 00       	call   f010396f <env_destroy>
f0103266:	83 c4 10             	add    $0x10,%esp
	}
}
f0103269:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010326c:	c9                   	leave  
f010326d:	c3                   	ret    

f010326e <region_alloc>:
{
	// LAB 3: Your code here.
	// (But only if you need it for load_icode.)
	//
	int num_Page;
	if(len == 0){
f010326e:	85 c9                	test   %ecx,%ecx
f0103270:	0f 84 9b 00 00 00    	je     f0103311 <region_alloc+0xa3>
// Pages should be writable by user and kernel.
// Panic if any allocation attempt fails.
//
static void
region_alloc(struct Env *e, void *va, size_t len)
{
f0103276:	55                   	push   %ebp
f0103277:	89 e5                	mov    %esp,%ebp
f0103279:	57                   	push   %edi
f010327a:	56                   	push   %esi
f010327b:	53                   	push   %ebx
f010327c:	83 ec 1c             	sub    $0x1c,%esp
	//
	int num_Page;
	if(len == 0){
		return;
	}
	void* end_Va = va + len;
f010327f:	01 d1                	add    %edx,%ecx
	void* start_Va = (void*)PTE_ADDR(va); //va & ~0xFFF; //round down va
f0103281:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0103287:	89 d3                	mov    %edx,%ebx
	//figure out how many pages needs to be allocated
	if(((int)end_Va % PGSIZE) == 0){ //already aligned 
f0103289:	f7 c1 ff 0f 00 00    	test   $0xfff,%ecx
f010328f:	75 16                	jne    f01032a7 <region_alloc+0x39>
		num_Page = ((end_Va - start_Va) / PGSIZE);
f0103291:	29 d1                	sub    %edx,%ecx
f0103293:	89 ca                	mov    %ecx,%edx
f0103295:	85 c9                	test   %ecx,%ecx
f0103297:	79 06                	jns    f010329f <region_alloc+0x31>
f0103299:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f010329f:	c1 fa 0c             	sar    $0xc,%edx
f01032a2:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f01032a5:	eb 17                	jmp    f01032be <region_alloc+0x50>
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
f01032a7:	29 d1                	sub    %edx,%ecx
f01032a9:	89 ca                	mov    %ecx,%edx
f01032ab:	85 c9                	test   %ecx,%ecx
f01032ad:	79 06                	jns    f01032b5 <region_alloc+0x47>
f01032af:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f01032b5:	c1 fa 0c             	sar    $0xc,%edx
f01032b8:	8d 4a 01             	lea    0x1(%edx),%ecx
f01032bb:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f01032be:	89 c7                	mov    %eax,%edi
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f01032c0:	be 00 00 00 00       	mov    $0x0,%esi
f01032c5:	eb 3e                	jmp    f0103305 <region_alloc+0x97>
		struct PageInfo* pp = page_alloc((!ALLOC_ZERO)); //Does not zero
f01032c7:	83 ec 0c             	sub    $0xc,%esp
f01032ca:	6a 00                	push   $0x0
f01032cc:	e8 51 df ff ff       	call   f0101222 <page_alloc>
		if(pp == NULL){
f01032d1:	83 c4 10             	add    $0x10,%esp
f01032d4:	85 c0                	test   %eax,%eax
f01032d6:	75 17                	jne    f01032ef <region_alloc+0x81>
			panic("allocation attempt fails in region_alloc");
f01032d8:	83 ec 04             	sub    $0x4,%esp
f01032db:	68 28 89 10 f0       	push   $0xf0108928
f01032e0:	68 40 01 00 00       	push   $0x140
f01032e5:	68 73 89 10 f0       	push   $0xf0108973
f01032ea:	e8 5d cd ff ff       	call   f010004c <_panic>
			//return -E_NO_MEM; //page table couldn't be allocated
		}
		page_insert(e->env_pgdir, pp, start_Va+i*PGSIZE, PTE_W|PTE_U|PTE_P); //user read and write
f01032ef:	6a 07                	push   $0x7
f01032f1:	53                   	push   %ebx
f01032f2:	50                   	push   %eax
f01032f3:	ff 77 60             	pushl  0x60(%edi)
f01032f6:	e8 a3 e3 ff ff       	call   f010169e <page_insert>
		num_Page = ((end_Va - start_Va) / PGSIZE);
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f01032fb:	46                   	inc    %esi
f01032fc:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f0103302:	83 c4 10             	add    $0x10,%esp
f0103305:	39 75 e4             	cmp    %esi,-0x1c(%ebp)
f0103308:	7f bd                	jg     f01032c7 <region_alloc+0x59>

	// Hint: It is easier to use region_alloc if the caller can pass
	//   'va' and 'len' values that are not page-aligned.
	//   You should round va down, and round (va + len) up.
	//   (Watch out for corner-cases!)
}
f010330a:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010330d:	5b                   	pop    %ebx
f010330e:	5e                   	pop    %esi
f010330f:	5f                   	pop    %edi
f0103310:	5d                   	pop    %ebp
f0103311:	c3                   	ret    

f0103312 <envid2env>:
//   On success, sets *env_store to the environment.
//   On error, sets *env_store to NULL.
//
int
envid2env(envid_t envid, struct Env **env_store, bool checkperm)
{
f0103312:	55                   	push   %ebp
f0103313:	89 e5                	mov    %esp,%ebp
f0103315:	56                   	push   %esi
f0103316:	53                   	push   %ebx
f0103317:	8b 45 08             	mov    0x8(%ebp),%eax
f010331a:	8b 55 10             	mov    0x10(%ebp),%edx
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
f010331d:	85 c0                	test   %eax,%eax
f010331f:	75 27                	jne    f0103348 <envid2env+0x36>
		*env_store = curenv;
f0103321:	e8 08 34 00 00       	call   f010672e <cpunum>
f0103326:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103329:	01 c2                	add    %eax,%edx
f010332b:	01 d2                	add    %edx,%edx
f010332d:	01 c2                	add    %eax,%edx
f010332f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103332:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0103339:	8b 75 0c             	mov    0xc(%ebp),%esi
f010333c:	89 06                	mov    %eax,(%esi)
		return 0;
f010333e:	b8 00 00 00 00       	mov    $0x0,%eax
f0103343:	e9 8d 00 00 00       	jmp    f01033d5 <envid2env+0xc3>
	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
f0103348:	89 c3                	mov    %eax,%ebx
f010334a:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
f0103350:	8d 0c 9d 00 00 00 00 	lea    0x0(,%ebx,4),%ecx
f0103357:	c1 e3 07             	shl    $0x7,%ebx
f010335a:	29 cb                	sub    %ecx,%ebx
f010335c:	03 1d 44 32 23 f0    	add    0xf0233244,%ebx
	if (e->env_status == ENV_FREE || e->env_id != envid) {
f0103362:	83 7b 54 00          	cmpl   $0x0,0x54(%ebx)
f0103366:	74 05                	je     f010336d <envid2env+0x5b>
f0103368:	3b 43 48             	cmp    0x48(%ebx),%eax
f010336b:	74 10                	je     f010337d <envid2env+0x6b>
		*env_store = 0;
f010336d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103370:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0103376:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f010337b:	eb 58                	jmp    f01033d5 <envid2env+0xc3>
	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
f010337d:	84 d2                	test   %dl,%dl
f010337f:	74 4a                	je     f01033cb <envid2env+0xb9>
f0103381:	e8 a8 33 00 00       	call   f010672e <cpunum>
f0103386:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103389:	01 c2                	add    %eax,%edx
f010338b:	01 d2                	add    %edx,%edx
f010338d:	01 c2                	add    %eax,%edx
f010338f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103392:	3b 1c 85 08 50 23 f0 	cmp    -0xfdcaff8(,%eax,4),%ebx
f0103399:	74 30                	je     f01033cb <envid2env+0xb9>
f010339b:	8b 73 4c             	mov    0x4c(%ebx),%esi
f010339e:	e8 8b 33 00 00       	call   f010672e <cpunum>
f01033a3:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01033a6:	01 c2                	add    %eax,%edx
f01033a8:	01 d2                	add    %edx,%edx
f01033aa:	01 c2                	add    %eax,%edx
f01033ac:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01033af:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f01033b6:	3b 70 48             	cmp    0x48(%eax),%esi
f01033b9:	74 10                	je     f01033cb <envid2env+0xb9>
		*env_store = 0;
f01033bb:	8b 45 0c             	mov    0xc(%ebp),%eax
f01033be:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f01033c4:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f01033c9:	eb 0a                	jmp    f01033d5 <envid2env+0xc3>
	}

	*env_store = e;
f01033cb:	8b 45 0c             	mov    0xc(%ebp),%eax
f01033ce:	89 18                	mov    %ebx,(%eax)
	return 0;
f01033d0:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01033d5:	5b                   	pop    %ebx
f01033d6:	5e                   	pop    %esi
f01033d7:	5d                   	pop    %ebp
f01033d8:	c3                   	ret    

f01033d9 <env_init_percpu>:
}

// Load GDT and segment descriptors.
void
env_init_percpu(void)
{
f01033d9:	55                   	push   %ebp
f01033da:	89 e5                	mov    %esp,%ebp
}

static inline void
lgdt(void *p)
{
	asm volatile("lgdt (%0)" : : "r" (p));
f01033dc:	b8 20 53 12 f0       	mov    $0xf0125320,%eax
f01033e1:	0f 01 10             	lgdtl  (%eax)
	lgdt(&gdt_pd);
	// The kernel never uses GS or FS, so we leave those set to
	// the user data segment.
	asm volatile("movw %%ax,%%gs" : : "a" (GD_UD|3));
f01033e4:	b8 23 00 00 00       	mov    $0x23,%eax
f01033e9:	8e e8                	mov    %eax,%gs
	asm volatile("movw %%ax,%%fs" : : "a" (GD_UD|3));
f01033eb:	8e e0                	mov    %eax,%fs
	// The kernel does use ES, DS, and SS.  We'll change between
	// the kernel and user data segments as needed.
	asm volatile("movw %%ax,%%es" : : "a" (GD_KD));
f01033ed:	b8 10 00 00 00       	mov    $0x10,%eax
f01033f2:	8e c0                	mov    %eax,%es
	asm volatile("movw %%ax,%%ds" : : "a" (GD_KD));
f01033f4:	8e d8                	mov    %eax,%ds
	asm volatile("movw %%ax,%%ss" : : "a" (GD_KD));
f01033f6:	8e d0                	mov    %eax,%ss
	// Load the kernel text segment into CS.
	asm volatile("ljmp %0,$1f\n 1:\n" : : "i" (GD_KT));
f01033f8:	ea ff 33 10 f0 08 00 	ljmp   $0x8,$0xf01033ff
}

static inline void
lldt(uint16_t sel)
{
	asm volatile("lldt %0" : : "r" (sel));
f01033ff:	b8 00 00 00 00       	mov    $0x0,%eax
f0103404:	0f 00 d0             	lldt   %ax
	// For good measure, clear the local descriptor table (LDT),
	// since we don't use it.
	lldt(0);
}
f0103407:	5d                   	pop    %ebp
f0103408:	c3                   	ret    

f0103409 <env_init>:
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
f0103409:	8b 15 44 32 23 f0    	mov    0xf0233244,%edx
f010340f:	8d 42 7c             	lea    0x7c(%edx),%eax
f0103412:	81 c2 00 f0 01 00    	add    $0x1f000,%edx
f0103418:	c7 40 cc 00 00 00 00 	movl   $0x0,-0x34(%eax)
		envs[i].env_link = &envs[i + 1];
f010341f:	89 40 c8             	mov    %eax,-0x38(%eax)
f0103422:	83 c0 7c             	add    $0x7c,%eax
	// Per-CPU part of the initialization
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
f0103425:	39 d0                	cmp    %edx,%eax
f0103427:	75 ef                	jne    f0103418 <env_init+0xf>
// they are in the envs array (i.e., so that the first call to
// env_alloc() returns envs[0]).
//
void
env_init(void)
{
f0103429:	55                   	push   %ebp
f010342a:	89 e5                	mov    %esp,%ebp
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
		envs[i].env_link = &envs[i + 1];
	}
	envs[NENV - 1].env_link = NULL;
f010342c:	a1 44 32 23 f0       	mov    0xf0233244,%eax
f0103431:	c7 80 c8 ef 01 00 00 	movl   $0x0,0x1efc8(%eax)
f0103438:	00 00 00 
	env_free_list = &envs[0];
f010343b:	a3 48 32 23 f0       	mov    %eax,0xf0233248

	// Per-CPU part of the initialization
	env_init_percpu();
f0103440:	e8 94 ff ff ff       	call   f01033d9 <env_init_percpu>
}
f0103445:	5d                   	pop    %ebp
f0103446:	c3                   	ret    

f0103447 <env_alloc>:
//	-E_NO_FREE_ENV if all NENVS environments are allocated
//	-E_NO_MEM on memory exhaustion
//
int
env_alloc(struct Env **newenv_store, envid_t parent_id)
{
f0103447:	55                   	push   %ebp
f0103448:	89 e5                	mov    %esp,%ebp
f010344a:	56                   	push   %esi
f010344b:	53                   	push   %ebx
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
f010344c:	8b 1d 48 32 23 f0    	mov    0xf0233248,%ebx
f0103452:	85 db                	test   %ebx,%ebx
f0103454:	0f 84 a0 01 00 00    	je     f01035fa <env_alloc+0x1b3>
{
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
f010345a:	83 ec 0c             	sub    $0xc,%esp
f010345d:	6a 01                	push   $0x1
f010345f:	e8 be dd ff ff       	call   f0101222 <page_alloc>
f0103464:	83 c4 10             	add    $0x10,%esp
f0103467:	85 c0                	test   %eax,%eax
f0103469:	0f 84 92 01 00 00    	je     f0103601 <env_alloc+0x1ba>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010346f:	89 c2                	mov    %eax,%edx
f0103471:	2b 15 2c 44 23 f0    	sub    0xf023442c,%edx
f0103477:	c1 fa 03             	sar    $0x3,%edx
f010347a:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010347d:	89 d1                	mov    %edx,%ecx
f010347f:	c1 e9 0c             	shr    $0xc,%ecx
f0103482:	3b 0d 24 44 23 f0    	cmp    0xf0234424,%ecx
f0103488:	72 12                	jb     f010349c <env_alloc+0x55>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010348a:	52                   	push   %edx
f010348b:	68 e8 74 10 f0       	push   $0xf01074e8
f0103490:	6a 58                	push   $0x58
f0103492:	68 ed 84 10 f0       	push   $0xf01084ed
f0103497:	e8 b0 cb ff ff       	call   f010004c <_panic>
	//	is an exception -- you need to increment env_pgdir's
	//	pp_ref for env_free to work correctly.
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
f010349c:	81 ea 00 00 00 10    	sub    $0x10000000,%edx
f01034a2:	89 53 60             	mov    %edx,0x60(%ebx)
f01034a5:	ba ec 0e 00 00       	mov    $0xeec,%edx
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
		e->env_pgdir[i] = kern_pgdir[i];
f01034aa:	8b 0d 28 44 23 f0    	mov    0xf0234428,%ecx
f01034b0:	8b 34 11             	mov    (%ecx,%edx,1),%esi
f01034b3:	8b 4b 60             	mov    0x60(%ebx),%ecx
f01034b6:	89 34 11             	mov    %esi,(%ecx,%edx,1)
f01034b9:	83 c2 04             	add    $0x4,%edx
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
f01034bc:	81 fa 00 10 00 00    	cmp    $0x1000,%edx
f01034c2:	75 e6                	jne    f01034aa <env_alloc+0x63>
		e->env_pgdir[i] = kern_pgdir[i];
	}
	p->pp_ref++;
f01034c4:	66 ff 40 04          	incw   0x4(%eax)
	// UVPT maps the env's own page table read-only.
	// Permissions: kernel R, user R
	e->env_pgdir[PDX(UVPT)] = PADDR(e->env_pgdir) | PTE_P | PTE_U; 
f01034c8:	8b 43 60             	mov    0x60(%ebx),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01034cb:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01034d0:	77 15                	ja     f01034e7 <env_alloc+0xa0>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01034d2:	50                   	push   %eax
f01034d3:	68 0c 75 10 f0       	push   $0xf010750c
f01034d8:	68 d3 00 00 00       	push   $0xd3
f01034dd:	68 73 89 10 f0       	push   $0xf0108973
f01034e2:	e8 65 cb ff ff       	call   f010004c <_panic>
f01034e7:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f01034ed:	83 ca 05             	or     $0x5,%edx
f01034f0:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)
	if ((r = env_setup_vm(e)) < 0){
		//cprintf("In env_alloc(), Allocate and set up the page directory for this environment");
		return r;
	}
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1);
f01034f6:	8b 43 48             	mov    0x48(%ebx),%eax
f01034f9:	8d 90 00 10 00 00    	lea    0x1000(%eax),%edx
	if (generation <= 0)	// Don't create a negative env_id.
f01034ff:	81 e2 00 fc ff ff    	and    $0xfffffc00,%edx
f0103505:	7f 05                	jg     f010350c <env_alloc+0xc5>
		generation = 1 << ENVGENSHIFT;
f0103507:	ba 00 10 00 00       	mov    $0x1000,%edx
	e->env_id = generation | (e - envs);
f010350c:	89 d8                	mov    %ebx,%eax
f010350e:	2b 05 44 32 23 f0    	sub    0xf0233244,%eax
f0103514:	c1 f8 02             	sar    $0x2,%eax
f0103517:	89 c6                	mov    %eax,%esi
f0103519:	c1 e6 05             	shl    $0x5,%esi
f010351c:	89 c1                	mov    %eax,%ecx
f010351e:	c1 e1 0a             	shl    $0xa,%ecx
f0103521:	01 f1                	add    %esi,%ecx
f0103523:	01 c1                	add    %eax,%ecx
f0103525:	89 ce                	mov    %ecx,%esi
f0103527:	c1 e6 0f             	shl    $0xf,%esi
f010352a:	01 f1                	add    %esi,%ecx
f010352c:	c1 e1 05             	shl    $0x5,%ecx
f010352f:	01 c8                	add    %ecx,%eax
f0103531:	f7 d8                	neg    %eax
f0103533:	09 d0                	or     %edx,%eax
f0103535:	89 43 48             	mov    %eax,0x48(%ebx)
	//cprintf("New env_id is: %d\n", (generation | (e - envs)) );
	// Set the basic status variables.
	e->env_parent_id = parent_id;
f0103538:	8b 45 0c             	mov    0xc(%ebp),%eax
f010353b:	89 43 4c             	mov    %eax,0x4c(%ebx)
	e->env_type = ENV_TYPE_USER;
f010353e:	c7 43 50 00 00 00 00 	movl   $0x0,0x50(%ebx)
	e->env_status = ENV_RUNNABLE;
f0103545:	c7 43 54 02 00 00 00 	movl   $0x2,0x54(%ebx)
	e->env_runs = 0;
f010354c:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf));
f0103553:	83 ec 04             	sub    $0x4,%esp
f0103556:	6a 44                	push   $0x44
f0103558:	6a 00                	push   $0x0
f010355a:	53                   	push   %ebx
f010355b:	e8 c8 2a 00 00       	call   f0106028 <memset>
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.  When
	// we switch privilege levels, the hardware does various
	// checks involving the RPL and the Descriptor Privilege Level
	// (DPL) stored in the descriptors themselves.
	e->env_tf.tf_ds = GD_UD | 3;
f0103560:	66 c7 43 24 23 00    	movw   $0x23,0x24(%ebx)
	e->env_tf.tf_es = GD_UD | 3;
f0103566:	66 c7 43 20 23 00    	movw   $0x23,0x20(%ebx)
	e->env_tf.tf_ss = GD_UD | 3;
f010356c:	66 c7 43 40 23 00    	movw   $0x23,0x40(%ebx)
	e->env_tf.tf_esp = USTACKTOP;
f0103572:	c7 43 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%ebx)
	e->env_tf.tf_cs = GD_UT | 3;
f0103579:	66 c7 43 34 1b 00    	movw   $0x1b,0x34(%ebx)
	// You will set e->env_tf.tf_eip later.

	// Enable interrupts while in user mode.
	// LAB 4: Your code here.
	e->env_tf.tf_eflags |= FL_IF; // Enable interrupts
f010357f:	81 4b 38 00 02 00 00 	orl    $0x200,0x38(%ebx)

	// Clear the page fault handler until user installs one.
	e->env_pgfault_upcall = 0;
f0103586:	c7 43 64 00 00 00 00 	movl   $0x0,0x64(%ebx)

	// Also clear the IPC receiving flag.
	e->env_ipc_recving = 0;
f010358d:	c6 43 68 00          	movb   $0x0,0x68(%ebx)

	// commit the allocation
	env_free_list = e->env_link;
f0103591:	8b 43 44             	mov    0x44(%ebx),%eax
f0103594:	a3 48 32 23 f0       	mov    %eax,0xf0233248
	*newenv_store = e;
f0103599:	8b 45 08             	mov    0x8(%ebp),%eax
f010359c:	89 18                	mov    %ebx,(%eax)

	//cprintf("In env_alloc(), current env is: %x, parent env is: %x\n", curenv ? curenv->env_id: 0, parent_id);
	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f010359e:	8b 5b 48             	mov    0x48(%ebx),%ebx
f01035a1:	e8 88 31 00 00       	call   f010672e <cpunum>
f01035a6:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01035a9:	01 c2                	add    %eax,%edx
f01035ab:	01 d2                	add    %edx,%edx
f01035ad:	01 c2                	add    %eax,%edx
f01035af:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01035b2:	83 c4 10             	add    $0x10,%esp
f01035b5:	83 3c 85 08 50 23 f0 	cmpl   $0x0,-0xfdcaff8(,%eax,4)
f01035bc:	00 
f01035bd:	74 1d                	je     f01035dc <env_alloc+0x195>
f01035bf:	e8 6a 31 00 00       	call   f010672e <cpunum>
f01035c4:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01035c7:	01 c2                	add    %eax,%edx
f01035c9:	01 d2                	add    %edx,%edx
f01035cb:	01 c2                	add    %eax,%edx
f01035cd:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01035d0:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f01035d7:	8b 40 48             	mov    0x48(%eax),%eax
f01035da:	eb 05                	jmp    f01035e1 <env_alloc+0x19a>
f01035dc:	b8 00 00 00 00       	mov    $0x0,%eax
f01035e1:	83 ec 04             	sub    $0x4,%esp
f01035e4:	53                   	push   %ebx
f01035e5:	50                   	push   %eax
f01035e6:	68 7e 89 10 f0       	push   $0xf010897e
f01035eb:	e8 21 06 00 00       	call   f0103c11 <cprintf>
	return 0;
f01035f0:	83 c4 10             	add    $0x10,%esp
f01035f3:	b8 00 00 00 00       	mov    $0x0,%eax
f01035f8:	eb 0c                	jmp    f0103606 <env_alloc+0x1bf>
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
		return -E_NO_FREE_ENV;
f01035fa:	b8 fb ff ff ff       	mov    $0xfffffffb,%eax
f01035ff:	eb 05                	jmp    f0103606 <env_alloc+0x1bf>
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
		return -E_NO_MEM;
f0103601:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	*newenv_store = e;

	//cprintf("In env_alloc(), current env is: %x, parent env is: %x\n", curenv ? curenv->env_id: 0, parent_id);
	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
	return 0;
}
f0103606:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0103609:	5b                   	pop    %ebx
f010360a:	5e                   	pop    %esi
f010360b:	5d                   	pop    %ebp
f010360c:	c3                   	ret    

f010360d <env_create>:
// before running the first user-mode environment.
// The new env's parent ID is set to 0.
//
void
env_create(uint8_t *binary, enum EnvType type)
{
f010360d:	55                   	push   %ebp
f010360e:	89 e5                	mov    %esp,%ebp
f0103610:	57                   	push   %edi
f0103611:	56                   	push   %esi
f0103612:	53                   	push   %ebx
f0103613:	83 ec 2c             	sub    $0x2c,%esp
f0103616:	8b 7d 08             	mov    0x8(%ebp),%edi
f0103619:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Env *newenv;
	int env_alloc_Result;
	//cprintf("In env_create(), about to call env_alloc()\n");
	//env_alloc_Result = env_alloc(&newenv, 0); //new env's parent ID is set to 0.
	//curenv ? curenv->env_id: 0
	env_alloc_Result = env_alloc(&newenv, curenv ? curenv->env_id:0); //new env's parent ID is set to 0.
f010361c:	e8 0d 31 00 00       	call   f010672e <cpunum>
f0103621:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103624:	01 c2                	add    %eax,%edx
f0103626:	01 d2                	add    %edx,%edx
f0103628:	01 c2                	add    %eax,%edx
f010362a:	8d 04 90             	lea    (%eax,%edx,4),%eax
f010362d:	83 3c 85 08 50 23 f0 	cmpl   $0x0,-0xfdcaff8(,%eax,4)
f0103634:	00 
f0103635:	74 1d                	je     f0103654 <env_create+0x47>
f0103637:	e8 f2 30 00 00       	call   f010672e <cpunum>
f010363c:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010363f:	01 c2                	add    %eax,%edx
f0103641:	01 d2                	add    %edx,%edx
f0103643:	01 c2                	add    %eax,%edx
f0103645:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103648:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f010364f:	8b 40 48             	mov    0x48(%eax),%eax
f0103652:	eb 05                	jmp    f0103659 <env_create+0x4c>
f0103654:	b8 00 00 00 00       	mov    $0x0,%eax
f0103659:	83 ec 08             	sub    $0x8,%esp
f010365c:	50                   	push   %eax
f010365d:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0103660:	50                   	push   %eax
f0103661:	e8 e1 fd ff ff       	call   f0103447 <env_alloc>
	if(env_alloc_Result == -E_NO_FREE_ENV){
f0103666:	83 c4 10             	add    $0x10,%esp
f0103669:	83 f8 fb             	cmp    $0xfffffffb,%eax
f010366c:	75 16                	jne    f0103684 <env_create+0x77>
		panic("env_create: %e", env_alloc_Result);
f010366e:	6a fb                	push   $0xfffffffb
f0103670:	68 93 89 10 f0       	push   $0xf0108993
f0103675:	68 b3 01 00 00       	push   $0x1b3
f010367a:	68 73 89 10 f0       	push   $0xf0108973
f010367f:	e8 c8 c9 ff ff       	call   f010004c <_panic>
	}
	if(env_alloc_Result == -E_NO_MEM){
f0103684:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0103687:	75 16                	jne    f010369f <env_create+0x92>
		panic("env_create: %e", env_alloc_Result);
f0103689:	6a fc                	push   $0xfffffffc
f010368b:	68 93 89 10 f0       	push   $0xf0108993
f0103690:	68 b6 01 00 00       	push   $0x1b6
f0103695:	68 73 89 10 f0       	push   $0xf0108973
f010369a:	e8 ad c9 ff ff       	call   f010004c <_panic>
	//   - Think carefully about the permissions.
	//
	// LAB 5: Your code here.
	//function prototype:
	//#define UMMIOBASE	0xE0000000
	newenv->env_type = type;
f010369f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01036a2:	89 58 50             	mov    %ebx,0x50(%eax)
	if(type == ENV_TYPE_FS){
f01036a5:	83 fb 01             	cmp    $0x1,%ebx
f01036a8:	75 37                	jne    f01036e1 <env_create+0xd4>
		// Find PTE of va.
		//physaddr_t ahci_va=0xfebf1000;
		extern void* ahci_va;
		pte_t *pa_Ptr1 = pgdir_walk(kern_pgdir, (void *)ahci_va, 0);
f01036aa:	83 ec 04             	sub    $0x4,%esp
f01036ad:	6a 00                	push   $0x0
f01036af:	ff 35 08 60 27 f0    	pushl  0xf0276008
f01036b5:	ff 35 28 44 23 f0    	pushl  0xf0234428
f01036bb:	e8 59 dc ff ff       	call   f0101319 <pgdir_walk>
f01036c0:	89 c3                	mov    %eax,%ebx
		pte_t *pa_Ptr2 = pgdir_walk(newenv->env_pgdir, (void *)UMMIOAHCI, 1);
f01036c2:	83 c4 0c             	add    $0xc,%esp
f01036c5:	6a 01                	push   $0x1
f01036c7:	68 00 00 00 e0       	push   $0xe0000000
f01036cc:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01036cf:	ff 70 60             	pushl  0x60(%eax)
f01036d2:	e8 42 dc ff ff       	call   f0101319 <pgdir_walk>

		*pa_Ptr2 = *pa_Ptr1 | PTE_U;
f01036d7:	8b 13                	mov    (%ebx),%edx
f01036d9:	83 ca 04             	or     $0x4,%edx
f01036dc:	89 10                	mov    %edx,(%eax)
f01036de:	83 c4 10             	add    $0x10,%esp
	}

	//cprintf("Before load_icode.\n\n");
	load_icode(newenv, binary);
f01036e1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01036e4:	89 45 d4             	mov    %eax,-0x2c(%ebp)

static inline uint32_t
rcr3(void)
{
	uint32_t val;
	asm volatile("movl %%cr3,%0" : "=r" (val));
f01036e7:	0f 20 d8             	mov    %cr3,%eax
f01036ea:	89 45 d0             	mov    %eax,-0x30(%ebp)
	// LAB 3: Your code here.
	//cprintf("load_icode starts:\n");
	physaddr_t  old_cr3 = rcr3(); 

	struct Proghdr *ph, *eph;
	if(((struct Elf *)binary)->e_magic != ELF_MAGIC) {
f01036ed:	81 3f 7f 45 4c 46    	cmpl   $0x464c457f,(%edi)
f01036f3:	74 17                	je     f010370c <env_create+0xff>
		panic("Invalid binary in load_icode.\n");
f01036f5:	83 ec 04             	sub    $0x4,%esp
f01036f8:	68 54 89 10 f0       	push   $0xf0108954
f01036fd:	68 87 01 00 00       	push   $0x187
f0103702:	68 73 89 10 f0       	push   $0xf0108973
f0103707:	e8 40 c9 ff ff       	call   f010004c <_panic>
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
f010370c:	89 fb                	mov    %edi,%ebx
f010370e:	03 5f 1c             	add    0x1c(%edi),%ebx
	eph = ph + ((struct Elf *)binary)->e_phnum;
f0103711:	0f b7 77 2c          	movzwl 0x2c(%edi),%esi
f0103715:	c1 e6 05             	shl    $0x5,%esi
f0103718:	01 de                	add    %ebx,%esi
	lcr3( PADDR(e->env_pgdir) );
f010371a:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010371d:	8b 40 60             	mov    0x60(%eax),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103720:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0103725:	77 15                	ja     f010373c <env_create+0x12f>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103727:	50                   	push   %eax
f0103728:	68 0c 75 10 f0       	push   $0xf010750c
f010372d:	68 8b 01 00 00       	push   $0x18b
f0103732:	68 73 89 10 f0       	push   $0xf0108973
f0103737:	e8 10 c9 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f010373c:	05 00 00 00 10       	add    $0x10000000,%eax
f0103741:	0f 22 d8             	mov    %eax,%cr3
f0103744:	eb 44                	jmp    f010378a <env_create+0x17d>

	for (; ph < eph; ph++){
		if(ph->p_type == ELF_PROG_LOAD){
f0103746:	83 3b 01             	cmpl   $0x1,(%ebx)
f0103749:	75 3c                	jne    f0103787 <env_create+0x17a>
			region_alloc(e, (void *)ph->p_va, ph->p_memsz);
f010374b:	8b 4b 14             	mov    0x14(%ebx),%ecx
f010374e:	8b 53 08             	mov    0x8(%ebx),%edx
f0103751:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103754:	e8 15 fb ff ff       	call   f010326e <region_alloc>
			memmove((void *)ph->p_va,(binary  + ph->p_offset), ph->p_filesz);
f0103759:	83 ec 04             	sub    $0x4,%esp
f010375c:	ff 73 10             	pushl  0x10(%ebx)
f010375f:	89 f8                	mov    %edi,%eax
f0103761:	03 43 04             	add    0x4(%ebx),%eax
f0103764:	50                   	push   %eax
f0103765:	ff 73 08             	pushl  0x8(%ebx)
f0103768:	e8 08 29 00 00       	call   f0106075 <memmove>
			memset((void *)ph->p_va+ph->p_filesz, 0, ph->p_memsz-ph->p_filesz);
f010376d:	8b 43 10             	mov    0x10(%ebx),%eax
f0103770:	83 c4 0c             	add    $0xc,%esp
f0103773:	8b 53 14             	mov    0x14(%ebx),%edx
f0103776:	29 c2                	sub    %eax,%edx
f0103778:	52                   	push   %edx
f0103779:	6a 00                	push   $0x0
f010377b:	03 43 08             	add    0x8(%ebx),%eax
f010377e:	50                   	push   %eax
f010377f:	e8 a4 28 00 00       	call   f0106028 <memset>
f0103784:	83 c4 10             	add    $0x10,%esp
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
	eph = ph + ((struct Elf *)binary)->e_phnum;
	lcr3( PADDR(e->env_pgdir) );

	for (; ph < eph; ph++){
f0103787:	83 c3 20             	add    $0x20,%ebx
f010378a:	39 de                	cmp    %ebx,%esi
f010378c:	77 b8                	ja     f0103746 <env_create+0x139>
f010378e:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103791:	0f 22 d8             	mov    %eax,%cr3
	}

	lcr3(old_cr3);
	// Now map one page for the program's initial stack
	// at virtual address USTACKTOP - PGSIZE.
	(e->env_tf).tf_eip = ((struct Elf *)binary)->e_entry;
f0103794:	8b 47 18             	mov    0x18(%edi),%eax
f0103797:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f010379a:	89 47 30             	mov    %eax,0x30(%edi)
	region_alloc(e, (void*)(USTACKTOP - PGSIZE), PGSIZE);
f010379d:	b9 00 10 00 00       	mov    $0x1000,%ecx
f01037a2:	ba 00 d0 bf ee       	mov    $0xeebfd000,%edx
f01037a7:	89 f8                	mov    %edi,%eax
f01037a9:	e8 c0 fa ff ff       	call   f010326e <region_alloc>
		*pa_Ptr2 = *pa_Ptr1 | PTE_U;
	}

	//cprintf("Before load_icode.\n\n");
	load_icode(newenv, binary);
}
f01037ae:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01037b1:	5b                   	pop    %ebx
f01037b2:	5e                   	pop    %esi
f01037b3:	5f                   	pop    %edi
f01037b4:	5d                   	pop    %ebp
f01037b5:	c3                   	ret    

f01037b6 <env_free>:
//
// Frees env e and all memory it uses.
//
void
env_free(struct Env *e)
{
f01037b6:	55                   	push   %ebp
f01037b7:	89 e5                	mov    %esp,%ebp
f01037b9:	57                   	push   %edi
f01037ba:	56                   	push   %esi
f01037bb:	53                   	push   %ebx
f01037bc:	83 ec 1c             	sub    $0x1c,%esp
f01037bf:	8b 7d 08             	mov    0x8(%ebp),%edi
	physaddr_t pa;

	// If freeing the current environment, switch to kern_pgdir
	// before freeing the page directory, just in case the page
	// gets reused.
	if (e == curenv)
f01037c2:	e8 67 2f 00 00       	call   f010672e <cpunum>
f01037c7:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01037ca:	01 c2                	add    %eax,%edx
f01037cc:	01 d2                	add    %edx,%edx
f01037ce:	01 c2                	add    %eax,%edx
f01037d0:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01037d3:	39 3c 85 08 50 23 f0 	cmp    %edi,-0xfdcaff8(,%eax,4)
f01037da:	75 39                	jne    f0103815 <env_free+0x5f>
		lcr3(PADDR(kern_pgdir));
f01037dc:	a1 28 44 23 f0       	mov    0xf0234428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01037e1:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01037e6:	77 15                	ja     f01037fd <env_free+0x47>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01037e8:	50                   	push   %eax
f01037e9:	68 0c 75 10 f0       	push   $0xf010750c
f01037ee:	68 e3 01 00 00       	push   $0x1e3
f01037f3:	68 73 89 10 f0       	push   $0xf0108973
f01037f8:	e8 4f c8 ff ff       	call   f010004c <_panic>
f01037fd:	05 00 00 00 10       	add    $0x10000000,%eax
f0103802:	0f 22 d8             	mov    %eax,%cr3
f0103805:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f010380c:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
f0103813:	eb 0e                	jmp    f0103823 <env_free+0x6d>
f0103815:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f010381c:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {

		// only look at mapped page tables
		if (!(e->env_pgdir[pdeno] & PTE_P))
f0103823:	8b 47 60             	mov    0x60(%edi),%eax
f0103826:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0103829:	8b 34 10             	mov    (%eax,%edx,1),%esi
f010382c:	f7 c6 01 00 00 00    	test   $0x1,%esi
f0103832:	0f 84 a6 00 00 00    	je     f01038de <env_free+0x128>
			continue;

		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
f0103838:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010383e:	89 f0                	mov    %esi,%eax
f0103840:	c1 e8 0c             	shr    $0xc,%eax
f0103843:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0103846:	39 05 24 44 23 f0    	cmp    %eax,0xf0234424
f010384c:	77 15                	ja     f0103863 <env_free+0xad>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010384e:	56                   	push   %esi
f010384f:	68 e8 74 10 f0       	push   $0xf01074e8
f0103854:	68 f2 01 00 00       	push   $0x1f2
f0103859:	68 73 89 10 f0       	push   $0xf0108973
f010385e:	e8 e9 c7 ff ff       	call   f010004c <_panic>
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f0103863:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0103866:	c1 e0 16             	shl    $0x16,%eax
f0103869:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f010386c:	bb 00 00 00 00       	mov    $0x0,%ebx
			if (pt[pteno] & PTE_P)
f0103871:	f6 84 9e 00 00 00 f0 	testb  $0x1,-0x10000000(%esi,%ebx,4)
f0103878:	01 
f0103879:	74 17                	je     f0103892 <env_free+0xdc>
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f010387b:	83 ec 08             	sub    $0x8,%esp
f010387e:	89 d8                	mov    %ebx,%eax
f0103880:	c1 e0 0c             	shl    $0xc,%eax
f0103883:	0b 45 e4             	or     -0x1c(%ebp),%eax
f0103886:	50                   	push   %eax
f0103887:	ff 77 60             	pushl  0x60(%edi)
f010388a:	e8 6f dd ff ff       	call   f01015fe <page_remove>
f010388f:	83 c4 10             	add    $0x10,%esp
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f0103892:	43                   	inc    %ebx
f0103893:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
f0103899:	75 d6                	jne    f0103871 <env_free+0xbb>
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
		}

		// free the page table itself
		e->env_pgdir[pdeno] = 0;
f010389b:	8b 47 60             	mov    0x60(%edi),%eax
f010389e:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01038a1:	c7 04 10 00 00 00 00 	movl   $0x0,(%eax,%edx,1)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01038a8:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01038ab:	3b 05 24 44 23 f0    	cmp    0xf0234424,%eax
f01038b1:	72 14                	jb     f01038c7 <env_free+0x111>
		panic("pa2page called with invalid pa");
f01038b3:	83 ec 04             	sub    $0x4,%esp
f01038b6:	68 74 7c 10 f0       	push   $0xf0107c74
f01038bb:	6a 51                	push   $0x51
f01038bd:	68 ed 84 10 f0       	push   $0xf01084ed
f01038c2:	e8 85 c7 ff ff       	call   f010004c <_panic>
		page_decref(pa2page(pa));
f01038c7:	83 ec 0c             	sub    $0xc,%esp
f01038ca:	a1 2c 44 23 f0       	mov    0xf023442c,%eax
f01038cf:	8b 55 d8             	mov    -0x28(%ebp),%edx
f01038d2:	8d 04 d0             	lea    (%eax,%edx,8),%eax
f01038d5:	50                   	push   %eax
f01038d6:	e8 1a da ff ff       	call   f01012f5 <page_decref>
f01038db:	83 c4 10             	add    $0x10,%esp
	// Note the environment's demise.
	// cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f01038de:	ff 45 dc             	incl   -0x24(%ebp)
f01038e1:	83 45 e0 04          	addl   $0x4,-0x20(%ebp)
f01038e5:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01038e8:	3d ec 0e 00 00       	cmp    $0xeec,%eax
f01038ed:	0f 85 30 ff ff ff    	jne    f0103823 <env_free+0x6d>
		e->env_pgdir[pdeno] = 0;
		page_decref(pa2page(pa));
	}

	// free the page directory
	pa = PADDR(e->env_pgdir);
f01038f3:	8b 47 60             	mov    0x60(%edi),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01038f6:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01038fb:	77 15                	ja     f0103912 <env_free+0x15c>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01038fd:	50                   	push   %eax
f01038fe:	68 0c 75 10 f0       	push   $0xf010750c
f0103903:	68 00 02 00 00       	push   $0x200
f0103908:	68 73 89 10 f0       	push   $0xf0108973
f010390d:	e8 3a c7 ff ff       	call   f010004c <_panic>
	e->env_pgdir = 0;
f0103912:	c7 47 60 00 00 00 00 	movl   $0x0,0x60(%edi)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103919:	05 00 00 00 10       	add    $0x10000000,%eax
f010391e:	c1 e8 0c             	shr    $0xc,%eax
f0103921:	3b 05 24 44 23 f0    	cmp    0xf0234424,%eax
f0103927:	72 14                	jb     f010393d <env_free+0x187>
		panic("pa2page called with invalid pa");
f0103929:	83 ec 04             	sub    $0x4,%esp
f010392c:	68 74 7c 10 f0       	push   $0xf0107c74
f0103931:	6a 51                	push   $0x51
f0103933:	68 ed 84 10 f0       	push   $0xf01084ed
f0103938:	e8 0f c7 ff ff       	call   f010004c <_panic>
	page_decref(pa2page(pa));
f010393d:	83 ec 0c             	sub    $0xc,%esp
f0103940:	8b 15 2c 44 23 f0    	mov    0xf023442c,%edx
f0103946:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f0103949:	50                   	push   %eax
f010394a:	e8 a6 d9 ff ff       	call   f01012f5 <page_decref>

	// return the environment to the free list
	e->env_status = ENV_FREE;
f010394f:	c7 47 54 00 00 00 00 	movl   $0x0,0x54(%edi)
	e->env_link = env_free_list;
f0103956:	a1 48 32 23 f0       	mov    0xf0233248,%eax
f010395b:	89 47 44             	mov    %eax,0x44(%edi)
	env_free_list = e;
f010395e:	89 3d 48 32 23 f0    	mov    %edi,0xf0233248
	//cprintf("End of env_free()\n");
}
f0103964:	83 c4 10             	add    $0x10,%esp
f0103967:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010396a:	5b                   	pop    %ebx
f010396b:	5e                   	pop    %esi
f010396c:	5f                   	pop    %edi
f010396d:	5d                   	pop    %ebp
f010396e:	c3                   	ret    

f010396f <env_destroy>:
// If e was the current env, then runs a new environment (and does not return
// to the caller).
//
void
env_destroy(struct Env *e)
{
f010396f:	55                   	push   %ebp
f0103970:	89 e5                	mov    %esp,%ebp
f0103972:	53                   	push   %ebx
f0103973:	83 ec 04             	sub    $0x4,%esp
f0103976:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// If e is currently running on other CPUs, we change its state to
	// ENV_DYING. A zombie environment will be freed the next time
	// it traps to the kernel.
	if (e->env_status == ENV_RUNNING && curenv != e) {
f0103979:	83 7b 54 03          	cmpl   $0x3,0x54(%ebx)
f010397d:	75 23                	jne    f01039a2 <env_destroy+0x33>
f010397f:	e8 aa 2d 00 00       	call   f010672e <cpunum>
f0103984:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103987:	01 c2                	add    %eax,%edx
f0103989:	01 d2                	add    %edx,%edx
f010398b:	01 c2                	add    %eax,%edx
f010398d:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103990:	3b 1c 85 08 50 23 f0 	cmp    -0xfdcaff8(,%eax,4),%ebx
f0103997:	74 09                	je     f01039a2 <env_destroy+0x33>
		e->env_status = ENV_DYING;
f0103999:	c7 43 54 01 00 00 00 	movl   $0x1,0x54(%ebx)
		return;
f01039a0:	eb 5a                	jmp    f01039fc <env_destroy+0x8d>
	}

	env_free(e);
f01039a2:	83 ec 0c             	sub    $0xc,%esp
f01039a5:	53                   	push   %ebx
f01039a6:	e8 0b fe ff ff       	call   f01037b6 <env_free>

	if (curenv == e) {
f01039ab:	e8 7e 2d 00 00       	call   f010672e <cpunum>
f01039b0:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01039b3:	01 c2                	add    %eax,%edx
f01039b5:	01 d2                	add    %edx,%edx
f01039b7:	01 c2                	add    %eax,%edx
f01039b9:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01039bc:	83 c4 10             	add    $0x10,%esp
f01039bf:	3b 1c 85 08 50 23 f0 	cmp    -0xfdcaff8(,%eax,4),%ebx
f01039c6:	75 24                	jne    f01039ec <env_destroy+0x7d>
		cprintf("Destroy courrent env\n");
f01039c8:	83 ec 0c             	sub    $0xc,%esp
f01039cb:	68 a2 89 10 f0       	push   $0xf01089a2
f01039d0:	e8 3c 02 00 00       	call   f0103c11 <cprintf>
		curenv = NULL;
f01039d5:	e8 54 2d 00 00       	call   f010672e <cpunum>
f01039da:	6b c0 74             	imul   $0x74,%eax,%eax
f01039dd:	c7 80 08 50 23 f0 00 	movl   $0x0,-0xfdcaff8(%eax)
f01039e4:	00 00 00 
		sched_yield();
f01039e7:	e8 33 11 00 00       	call   f0104b1f <sched_yield>
	}
	cprintf("end of env_destroy()\n");
f01039ec:	83 ec 0c             	sub    $0xc,%esp
f01039ef:	68 b8 89 10 f0       	push   $0xf01089b8
f01039f4:	e8 18 02 00 00       	call   f0103c11 <cprintf>
f01039f9:	83 c4 10             	add    $0x10,%esp
}
f01039fc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01039ff:	c9                   	leave  
f0103a00:	c3                   	ret    

f0103a01 <env_pop_tf>:
//
// This function does not return.
//
void
env_pop_tf(struct Trapframe *tf)
{
f0103a01:	55                   	push   %ebp
f0103a02:	89 e5                	mov    %esp,%ebp
f0103a04:	53                   	push   %ebx
f0103a05:	83 ec 04             	sub    $0x4,%esp
	// Record the CPU we are running on for user-space debugging
	curenv->env_cpunum = cpunum();
f0103a08:	e8 21 2d 00 00       	call   f010672e <cpunum>
f0103a0d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103a10:	01 c2                	add    %eax,%edx
f0103a12:	01 d2                	add    %edx,%edx
f0103a14:	01 c2                	add    %eax,%edx
f0103a16:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103a19:	8b 1c 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%ebx
f0103a20:	e8 09 2d 00 00       	call   f010672e <cpunum>
f0103a25:	89 43 5c             	mov    %eax,0x5c(%ebx)

	asm volatile(
f0103a28:	8b 65 08             	mov    0x8(%ebp),%esp
f0103a2b:	61                   	popa   
f0103a2c:	07                   	pop    %es
f0103a2d:	1f                   	pop    %ds
f0103a2e:	83 c4 08             	add    $0x8,%esp
f0103a31:	cf                   	iret   
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret\n"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
f0103a32:	83 ec 04             	sub    $0x4,%esp
f0103a35:	68 ce 89 10 f0       	push   $0xf01089ce
f0103a3a:	68 3a 02 00 00       	push   $0x23a
f0103a3f:	68 73 89 10 f0       	push   $0xf0108973
f0103a44:	e8 03 c6 ff ff       	call   f010004c <_panic>

f0103a49 <env_run>:
//
// This function does not return.
//
void
env_run(struct Env *e)
{
f0103a49:	55                   	push   %ebp
f0103a4a:	89 e5                	mov    %esp,%ebp
f0103a4c:	53                   	push   %ebx
f0103a4d:	83 ec 04             	sub    $0x4,%esp
f0103a50:	8b 5d 08             	mov    0x8(%ebp),%ebx
	//if(curenv == NULL && (curenv->env_id == e->env_id) ){
	//	goto end;
	//}
	//if(curenv != NULL && (curenv->env_id != e->env_id)){ //check if it's a context switch
	
	if(curenv != NULL && (curenv->env_id != e->env_id)){
f0103a53:	e8 d6 2c 00 00       	call   f010672e <cpunum>
f0103a58:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103a5b:	01 c2                	add    %eax,%edx
f0103a5d:	01 d2                	add    %edx,%edx
f0103a5f:	01 c2                	add    %eax,%edx
f0103a61:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103a64:	83 3c 85 08 50 23 f0 	cmpl   $0x0,-0xfdcaff8(,%eax,4)
f0103a6b:	00 
f0103a6c:	74 59                	je     f0103ac7 <env_run+0x7e>
f0103a6e:	e8 bb 2c 00 00       	call   f010672e <cpunum>
f0103a73:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103a76:	01 c2                	add    %eax,%edx
f0103a78:	01 d2                	add    %edx,%edx
f0103a7a:	01 c2                	add    %eax,%edx
f0103a7c:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103a7f:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0103a86:	8b 4b 48             	mov    0x48(%ebx),%ecx
f0103a89:	39 48 48             	cmp    %ecx,0x48(%eax)
f0103a8c:	74 39                	je     f0103ac7 <env_run+0x7e>
		if(curenv->env_ipc_recving != 1){
f0103a8e:	e8 9b 2c 00 00       	call   f010672e <cpunum>
f0103a93:	6b c0 74             	imul   $0x74,%eax,%eax
f0103a96:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f0103a9c:	80 78 68 00          	cmpb   $0x0,0x68(%eax)
f0103aa0:	75 15                	jne    f0103ab7 <env_run+0x6e>
			curenv->env_status = ENV_RUNNABLE;
f0103aa2:	e8 87 2c 00 00       	call   f010672e <cpunum>
f0103aa7:	6b c0 74             	imul   $0x74,%eax,%eax
f0103aaa:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f0103ab0:	c7 40 54 02 00 00 00 	movl   $0x2,0x54(%eax)
		}
		curenv = e;
f0103ab7:	e8 72 2c 00 00       	call   f010672e <cpunum>
f0103abc:	6b c0 74             	imul   $0x74,%eax,%eax
f0103abf:	89 98 08 50 23 f0    	mov    %ebx,-0xfdcaff8(%eax)
f0103ac5:	eb 0e                	jmp    f0103ad5 <env_run+0x8c>
	}else{
		curenv = e;
f0103ac7:	e8 62 2c 00 00       	call   f010672e <cpunum>
f0103acc:	6b c0 74             	imul   $0x74,%eax,%eax
f0103acf:	89 98 08 50 23 f0    	mov    %ebx,-0xfdcaff8(%eax)
	}
		//cprintf("In env_run()\n");
		curenv->env_status = ENV_RUNNING;
f0103ad5:	e8 54 2c 00 00       	call   f010672e <cpunum>
f0103ada:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103add:	01 c2                	add    %eax,%edx
f0103adf:	01 d2                	add    %edx,%edx
f0103ae1:	01 c2                	add    %eax,%edx
f0103ae3:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103ae6:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0103aed:	c7 40 54 03 00 00 00 	movl   $0x3,0x54(%eax)
		(curenv->env_runs)++;
f0103af4:	e8 35 2c 00 00       	call   f010672e <cpunum>
f0103af9:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103afc:	01 c2                	add    %eax,%edx
f0103afe:	01 d2                	add    %edx,%edx
f0103b00:	01 c2                	add    %eax,%edx
f0103b02:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103b05:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0103b0c:	ff 40 58             	incl   0x58(%eax)
		lcr3(PADDR(curenv->env_pgdir));
f0103b0f:	e8 1a 2c 00 00       	call   f010672e <cpunum>
f0103b14:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103b17:	01 c2                	add    %eax,%edx
f0103b19:	01 d2                	add    %edx,%edx
f0103b1b:	01 c2                	add    %eax,%edx
f0103b1d:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0103b20:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0103b27:	8b 40 60             	mov    0x60(%eax),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103b2a:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0103b2f:	77 15                	ja     f0103b46 <env_run+0xfd>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103b31:	50                   	push   %eax
f0103b32:	68 0c 75 10 f0       	push   $0xf010750c
f0103b37:	68 68 02 00 00       	push   $0x268
f0103b3c:	68 73 89 10 f0       	push   $0xf0108973
f0103b41:	e8 06 c5 ff ff       	call   f010004c <_panic>
f0103b46:	05 00 00 00 10       	add    $0x10000000,%eax
f0103b4b:	0f 22 d8             	mov    %eax,%cr3
}

static inline void
unlock_kernel(void)
{
	spin_unlock(&kernel_lock);
f0103b4e:	83 ec 0c             	sub    $0xc,%esp
f0103b51:	68 c0 53 12 f0       	push   $0xf01253c0
f0103b56:	e8 3e 30 00 00       	call   f0106b99 <spin_unlock>

	// Normally we wouldn't need to do this, but QEMU only runs
	// one CPU at a time and has a long time-slice.  Without the
	// pause, this CPU is likely to reacquire the lock before
	// another CPU has even been given a chance to acquire it.
	asm volatile("pause");
f0103b5b:	f3 90                	pause  

		unlock_kernel();
		env_pop_tf(&(curenv->env_tf));
f0103b5d:	e8 cc 2b 00 00       	call   f010672e <cpunum>
f0103b62:	83 c4 04             	add    $0x4,%esp
f0103b65:	6b c0 74             	imul   $0x74,%eax,%eax
f0103b68:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f0103b6e:	e8 8e fe ff ff       	call   f0103a01 <env_pop_tf>

f0103b73 <pic_init>:
}

/* Initialize the 8259A interrupt controllers. */
void
pic_init(void)
{
f0103b73:	55                   	push   %ebp
f0103b74:	89 e5                	mov    %esp,%ebp
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0103b76:	ba 21 00 00 00       	mov    $0x21,%edx
f0103b7b:	b0 ff                	mov    $0xff,%al
f0103b7d:	ee                   	out    %al,(%dx)
f0103b7e:	ba a1 00 00 00       	mov    $0xa1,%edx
f0103b83:	ee                   	out    %al,(%dx)
f0103b84:	ba 20 00 00 00       	mov    $0x20,%edx
f0103b89:	b0 11                	mov    $0x11,%al
f0103b8b:	ee                   	out    %al,(%dx)
f0103b8c:	ba 21 00 00 00       	mov    $0x21,%edx
f0103b91:	b0 20                	mov    $0x20,%al
f0103b93:	ee                   	out    %al,(%dx)
f0103b94:	b0 04                	mov    $0x4,%al
f0103b96:	ee                   	out    %al,(%dx)
f0103b97:	b0 03                	mov    $0x3,%al
f0103b99:	ee                   	out    %al,(%dx)
f0103b9a:	ba a0 00 00 00       	mov    $0xa0,%edx
f0103b9f:	b0 11                	mov    $0x11,%al
f0103ba1:	ee                   	out    %al,(%dx)
f0103ba2:	ba a1 00 00 00       	mov    $0xa1,%edx
f0103ba7:	b0 28                	mov    $0x28,%al
f0103ba9:	ee                   	out    %al,(%dx)
f0103baa:	b0 02                	mov    $0x2,%al
f0103bac:	ee                   	out    %al,(%dx)
f0103bad:	b0 01                	mov    $0x1,%al
f0103baf:	ee                   	out    %al,(%dx)
f0103bb0:	ba 20 00 00 00       	mov    $0x20,%edx
f0103bb5:	b0 68                	mov    $0x68,%al
f0103bb7:	ee                   	out    %al,(%dx)
f0103bb8:	b0 0a                	mov    $0xa,%al
f0103bba:	ee                   	out    %al,(%dx)
f0103bbb:	ba a0 00 00 00       	mov    $0xa0,%edx
f0103bc0:	b0 68                	mov    $0x68,%al
f0103bc2:	ee                   	out    %al,(%dx)
f0103bc3:	b0 0a                	mov    $0xa,%al
f0103bc5:	ee                   	out    %al,(%dx)
f0103bc6:	ba 21 00 00 00       	mov    $0x21,%edx
f0103bcb:	b0 fb                	mov    $0xfb,%al
f0103bcd:	ee                   	out    %al,(%dx)
f0103bce:	ba a1 00 00 00       	mov    $0xa1,%edx
f0103bd3:	b0 ff                	mov    $0xff,%al
f0103bd5:	ee                   	out    %al,(%dx)
	outb(IO_PIC2, 0x68);               /* OCW3 */
	outb(IO_PIC2, 0x0a);               /* OCW3 */

	// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
	irq_setmask_8259A(0xffff & ~(BIT(IRQ_SLAVE)));
}
f0103bd6:	5d                   	pop    %ebp
f0103bd7:	c3                   	ret    

f0103bd8 <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0103bd8:	55                   	push   %ebp
f0103bd9:	89 e5                	mov    %esp,%ebp
f0103bdb:	83 ec 14             	sub    $0x14,%esp
	cputchar(ch);
f0103bde:	ff 75 08             	pushl  0x8(%ebp)
f0103be1:	e8 0e cc ff ff       	call   f01007f4 <cputchar>
	*cnt++;
}
f0103be6:	83 c4 10             	add    $0x10,%esp
f0103be9:	c9                   	leave  
f0103bea:	c3                   	ret    

f0103beb <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f0103beb:	55                   	push   %ebp
f0103bec:	89 e5                	mov    %esp,%ebp
f0103bee:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f0103bf1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0103bf8:	ff 75 0c             	pushl  0xc(%ebp)
f0103bfb:	ff 75 08             	pushl  0x8(%ebp)
f0103bfe:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0103c01:	50                   	push   %eax
f0103c02:	68 d8 3b 10 f0       	push   $0xf0103bd8
f0103c07:	e8 c0 1d 00 00       	call   f01059cc <vprintfmt>
	return cnt;
}
f0103c0c:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103c0f:	c9                   	leave  
f0103c10:	c3                   	ret    

f0103c11 <cprintf>:

int
cprintf(const char *fmt, ...)
{
f0103c11:	55                   	push   %ebp
f0103c12:	89 e5                	mov    %esp,%ebp
f0103c14:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0103c17:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
f0103c1a:	50                   	push   %eax
f0103c1b:	ff 75 08             	pushl  0x8(%ebp)
f0103c1e:	e8 c8 ff ff ff       	call   f0103beb <vcprintf>
	va_end(ap);

	return cnt;
}
f0103c23:	c9                   	leave  
f0103c24:	c3                   	ret    

f0103c25 <trap_init_percpu>:
}

// Initialize and load the per-CPU TSS and IDT
void
trap_init_percpu(void)
{
f0103c25:	55                   	push   %ebp
f0103c26:	89 e5                	mov    %esp,%ebp
f0103c28:	57                   	push   %edi
f0103c29:	56                   	push   %esi
f0103c2a:	53                   	push   %ebx
f0103c2b:	83 ec 1c             	sub    $0x1c,%esp
	lidt(&idt_pd);
	*/

	//reference code
	// LAB 4: Your code here:
    uint8_t cpu_id = cpunum();//thiscpu->cpu_id;
f0103c2e:	e8 fb 2a 00 00       	call   f010672e <cpunum>
f0103c33:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0103c36:	0f b6 5d e4          	movzbl -0x1c(%ebp),%ebx
    cprintf("Print CPU_id: %d\n", cpu_id);
f0103c3a:	83 ec 08             	sub    $0x8,%esp
f0103c3d:	53                   	push   %ebx
f0103c3e:	68 da 89 10 f0       	push   $0xf01089da
f0103c43:	e8 c9 ff ff ff       	call   f0103c11 <cprintf>
    struct Taskstate *this_TS = &thiscpu->cpu_ts;
f0103c48:	e8 e1 2a 00 00       	call   f010672e <cpunum>
f0103c4d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103c50:	01 c2                	add    %eax,%edx
f0103c52:	01 d2                	add    %edx,%edx
f0103c54:	01 c2                	add    %eax,%edx
f0103c56:	c1 e2 02             	shl    $0x2,%edx
f0103c59:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
f0103c5c:	8d 3c 8d 00 50 23 f0 	lea    -0xfdcb000(,%ecx,4),%edi
f0103c63:	8d 4f 0c             	lea    0xc(%edi),%ecx

    this_TS->ts_esp0 = KSTACKTOP - (KSTKSIZE + KSTKGAP) * cpu_id;
f0103c66:	89 de                	mov    %ebx,%esi
f0103c68:	f7 de                	neg    %esi
f0103c6a:	c1 e6 10             	shl    $0x10,%esi
f0103c6d:	81 ee 00 00 00 10    	sub    $0x10000000,%esi
f0103c73:	89 77 10             	mov    %esi,0x10(%edi)
    this_TS->ts_ss0 = GD_KD;
f0103c76:	01 d0                	add    %edx,%eax
f0103c78:	66 c7 04 85 14 50 23 	movw   $0x10,-0xfdcafec(,%eax,4)
f0103c7f:	f0 10 00 
f0103c82:	8d 04 dd 28 00 00 00 	lea    0x28(,%ebx,8),%eax
f0103c89:	c1 e8 03             	shr    $0x3,%eax
    uint16_t gdt_offset = GD_TSS0 + (cpu_id << 3);
    uint16_t gdt_index = (GD_TSS0 + (cpu_id << 3)) >> 3;

    // selector
    //#define SEG16(type, base, lim, dpl)
    gdt[gdt_index] = SEG16(STS_T32A, (uint32_t) (this_TS), sizeof(struct Taskstate) - 1, 0);
f0103c8c:	66 c7 04 c5 40 53 12 	movw   $0x67,-0xfedacc0(,%eax,8)
f0103c93:	f0 67 00 
f0103c96:	66 89 0c c5 42 53 12 	mov    %cx,-0xfedacbe(,%eax,8)
f0103c9d:	f0 
f0103c9e:	89 ca                	mov    %ecx,%edx
f0103ca0:	c1 ea 10             	shr    $0x10,%edx
f0103ca3:	88 14 c5 44 53 12 f0 	mov    %dl,-0xfedacbc(,%eax,8)
f0103caa:	c6 04 c5 46 53 12 f0 	movb   $0x40,-0xfedacba(,%eax,8)
f0103cb1:	40 
f0103cb2:	c1 e9 18             	shr    $0x18,%ecx
f0103cb5:	88 0c c5 47 53 12 f0 	mov    %cl,-0xfedacb9(,%eax,8)
    gdt[gdt_index].sd_s = 0;
f0103cbc:	c6 04 c5 45 53 12 f0 	movb   $0x89,-0xfedacbb(,%eax,8)
f0103cc3:	89 
}

static inline void
ltr(uint16_t sel)
{
	asm volatile("ltr %0" : : "r" (sel));
f0103cc4:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
f0103cc8:	8d 04 c5 28 00 00 00 	lea    0x28(,%eax,8),%eax
f0103ccf:	0f 00 d8             	ltr    %ax
}

static inline void
lidt(void *p)
{
	asm volatile("lidt (%0)" : : "r" (p));
f0103cd2:	b8 ac 53 12 f0       	mov    $0xf01253ac,%eax
f0103cd7:	0f 01 18             	lidtl  (%eax)

    ltr(gdt_offset);

	lidt(&idt_pd);
}
f0103cda:	83 c4 10             	add    $0x10,%esp
f0103cdd:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103ce0:	5b                   	pop    %ebx
f0103ce1:	5e                   	pop    %esi
f0103ce2:	5f                   	pop    %edi
f0103ce3:	5d                   	pop    %ebp
f0103ce4:	c3                   	ret    

f0103ce5 <trap_init>:
    } while (0)

//GD_KT is "kernel text"
void
trap_init(void)
{
f0103ce5:	55                   	push   %ebp
f0103ce6:	89 e5                	mov    %esp,%ebp
f0103ce8:	83 ec 08             	sub    $0x8,%esp
	//SETGATE(idt[0], 0, GD_KT, );

	// Note that ISTRAP must be zero for all gates
    // because JOS says when it enters kernel,
    // all interrupts are masked.
	SG(0, 0, 0);
f0103ceb:	b8 d0 48 10 f0       	mov    $0xf01048d0,%eax
f0103cf0:	66 a3 60 32 23 f0    	mov    %ax,0xf0233260
f0103cf6:	66 c7 05 62 32 23 f0 	movw   $0x8,0xf0233262
f0103cfd:	08 00 
f0103cff:	c6 05 64 32 23 f0 00 	movb   $0x0,0xf0233264
f0103d06:	c6 05 65 32 23 f0 8e 	movb   $0x8e,0xf0233265
f0103d0d:	c1 e8 10             	shr    $0x10,%eax
f0103d10:	66 a3 66 32 23 f0    	mov    %ax,0xf0233266
	SG(1, 0, 0);
f0103d16:	b8 da 48 10 f0       	mov    $0xf01048da,%eax
f0103d1b:	66 a3 68 32 23 f0    	mov    %ax,0xf0233268
f0103d21:	66 c7 05 6a 32 23 f0 	movw   $0x8,0xf023326a
f0103d28:	08 00 
f0103d2a:	c6 05 6c 32 23 f0 00 	movb   $0x0,0xf023326c
f0103d31:	c6 05 6d 32 23 f0 8e 	movb   $0x8e,0xf023326d
f0103d38:	c1 e8 10             	shr    $0x10,%eax
f0103d3b:	66 a3 6e 32 23 f0    	mov    %ax,0xf023326e
	SG(2, 0, 0);
f0103d41:	b8 e4 48 10 f0       	mov    $0xf01048e4,%eax
f0103d46:	66 a3 70 32 23 f0    	mov    %ax,0xf0233270
f0103d4c:	66 c7 05 72 32 23 f0 	movw   $0x8,0xf0233272
f0103d53:	08 00 
f0103d55:	c6 05 74 32 23 f0 00 	movb   $0x0,0xf0233274
f0103d5c:	c6 05 75 32 23 f0 8e 	movb   $0x8e,0xf0233275
f0103d63:	c1 e8 10             	shr    $0x10,%eax
f0103d66:	66 a3 76 32 23 f0    	mov    %ax,0xf0233276
	SG(3, 0, 3);
f0103d6c:	b8 ee 48 10 f0       	mov    $0xf01048ee,%eax
f0103d71:	66 a3 78 32 23 f0    	mov    %ax,0xf0233278
f0103d77:	66 c7 05 7a 32 23 f0 	movw   $0x8,0xf023327a
f0103d7e:	08 00 
f0103d80:	c6 05 7c 32 23 f0 00 	movb   $0x0,0xf023327c
f0103d87:	c6 05 7d 32 23 f0 ee 	movb   $0xee,0xf023327d
f0103d8e:	c1 e8 10             	shr    $0x10,%eax
f0103d91:	66 a3 7e 32 23 f0    	mov    %ax,0xf023327e
	SG(4, 0, 0);
f0103d97:	b8 f8 48 10 f0       	mov    $0xf01048f8,%eax
f0103d9c:	66 a3 80 32 23 f0    	mov    %ax,0xf0233280
f0103da2:	66 c7 05 82 32 23 f0 	movw   $0x8,0xf0233282
f0103da9:	08 00 
f0103dab:	c6 05 84 32 23 f0 00 	movb   $0x0,0xf0233284
f0103db2:	c6 05 85 32 23 f0 8e 	movb   $0x8e,0xf0233285
f0103db9:	c1 e8 10             	shr    $0x10,%eax
f0103dbc:	66 a3 86 32 23 f0    	mov    %ax,0xf0233286
	SG(5, 0, 0);
f0103dc2:	b8 02 49 10 f0       	mov    $0xf0104902,%eax
f0103dc7:	66 a3 88 32 23 f0    	mov    %ax,0xf0233288
f0103dcd:	66 c7 05 8a 32 23 f0 	movw   $0x8,0xf023328a
f0103dd4:	08 00 
f0103dd6:	c6 05 8c 32 23 f0 00 	movb   $0x0,0xf023328c
f0103ddd:	c6 05 8d 32 23 f0 8e 	movb   $0x8e,0xf023328d
f0103de4:	c1 e8 10             	shr    $0x10,%eax
f0103de7:	66 a3 8e 32 23 f0    	mov    %ax,0xf023328e
	SG(6, 0, 0);
f0103ded:	b8 0c 49 10 f0       	mov    $0xf010490c,%eax
f0103df2:	66 a3 90 32 23 f0    	mov    %ax,0xf0233290
f0103df8:	66 c7 05 92 32 23 f0 	movw   $0x8,0xf0233292
f0103dff:	08 00 
f0103e01:	c6 05 94 32 23 f0 00 	movb   $0x0,0xf0233294
f0103e08:	c6 05 95 32 23 f0 8e 	movb   $0x8e,0xf0233295
f0103e0f:	c1 e8 10             	shr    $0x10,%eax
f0103e12:	66 a3 96 32 23 f0    	mov    %ax,0xf0233296
	SG(7, 0, 0);
f0103e18:	b8 16 49 10 f0       	mov    $0xf0104916,%eax
f0103e1d:	66 a3 98 32 23 f0    	mov    %ax,0xf0233298
f0103e23:	66 c7 05 9a 32 23 f0 	movw   $0x8,0xf023329a
f0103e2a:	08 00 
f0103e2c:	c6 05 9c 32 23 f0 00 	movb   $0x0,0xf023329c
f0103e33:	c6 05 9d 32 23 f0 8e 	movb   $0x8e,0xf023329d
f0103e3a:	c1 e8 10             	shr    $0x10,%eax
f0103e3d:	66 a3 9e 32 23 f0    	mov    %ax,0xf023329e
	SG(8, 0, 0);
f0103e43:	b8 20 49 10 f0       	mov    $0xf0104920,%eax
f0103e48:	66 a3 a0 32 23 f0    	mov    %ax,0xf02332a0
f0103e4e:	66 c7 05 a2 32 23 f0 	movw   $0x8,0xf02332a2
f0103e55:	08 00 
f0103e57:	c6 05 a4 32 23 f0 00 	movb   $0x0,0xf02332a4
f0103e5e:	c6 05 a5 32 23 f0 8e 	movb   $0x8e,0xf02332a5
f0103e65:	c1 e8 10             	shr    $0x10,%eax
f0103e68:	66 a3 a6 32 23 f0    	mov    %ax,0xf02332a6
	SG(10, 0, 0);
f0103e6e:	b8 28 49 10 f0       	mov    $0xf0104928,%eax
f0103e73:	66 a3 b0 32 23 f0    	mov    %ax,0xf02332b0
f0103e79:	66 c7 05 b2 32 23 f0 	movw   $0x8,0xf02332b2
f0103e80:	08 00 
f0103e82:	c6 05 b4 32 23 f0 00 	movb   $0x0,0xf02332b4
f0103e89:	c6 05 b5 32 23 f0 8e 	movb   $0x8e,0xf02332b5
f0103e90:	c1 e8 10             	shr    $0x10,%eax
f0103e93:	66 a3 b6 32 23 f0    	mov    %ax,0xf02332b6
	SG(11, 0, 0);
f0103e99:	b8 30 49 10 f0       	mov    $0xf0104930,%eax
f0103e9e:	66 a3 b8 32 23 f0    	mov    %ax,0xf02332b8
f0103ea4:	66 c7 05 ba 32 23 f0 	movw   $0x8,0xf02332ba
f0103eab:	08 00 
f0103ead:	c6 05 bc 32 23 f0 00 	movb   $0x0,0xf02332bc
f0103eb4:	c6 05 bd 32 23 f0 8e 	movb   $0x8e,0xf02332bd
f0103ebb:	c1 e8 10             	shr    $0x10,%eax
f0103ebe:	66 a3 be 32 23 f0    	mov    %ax,0xf02332be
	SG(12, 0, 0);
f0103ec4:	b8 38 49 10 f0       	mov    $0xf0104938,%eax
f0103ec9:	66 a3 c0 32 23 f0    	mov    %ax,0xf02332c0
f0103ecf:	66 c7 05 c2 32 23 f0 	movw   $0x8,0xf02332c2
f0103ed6:	08 00 
f0103ed8:	c6 05 c4 32 23 f0 00 	movb   $0x0,0xf02332c4
f0103edf:	c6 05 c5 32 23 f0 8e 	movb   $0x8e,0xf02332c5
f0103ee6:	c1 e8 10             	shr    $0x10,%eax
f0103ee9:	66 a3 c6 32 23 f0    	mov    %ax,0xf02332c6
	SG(13, 0, 0);
f0103eef:	b8 40 49 10 f0       	mov    $0xf0104940,%eax
f0103ef4:	66 a3 c8 32 23 f0    	mov    %ax,0xf02332c8
f0103efa:	66 c7 05 ca 32 23 f0 	movw   $0x8,0xf02332ca
f0103f01:	08 00 
f0103f03:	c6 05 cc 32 23 f0 00 	movb   $0x0,0xf02332cc
f0103f0a:	c6 05 cd 32 23 f0 8e 	movb   $0x8e,0xf02332cd
f0103f11:	c1 e8 10             	shr    $0x10,%eax
f0103f14:	66 a3 ce 32 23 f0    	mov    %ax,0xf02332ce
	SG(14, 0, 0);
f0103f1a:	b8 48 49 10 f0       	mov    $0xf0104948,%eax
f0103f1f:	66 a3 d0 32 23 f0    	mov    %ax,0xf02332d0
f0103f25:	66 c7 05 d2 32 23 f0 	movw   $0x8,0xf02332d2
f0103f2c:	08 00 
f0103f2e:	c6 05 d4 32 23 f0 00 	movb   $0x0,0xf02332d4
f0103f35:	c6 05 d5 32 23 f0 8e 	movb   $0x8e,0xf02332d5
f0103f3c:	c1 e8 10             	shr    $0x10,%eax
f0103f3f:	66 a3 d6 32 23 f0    	mov    %ax,0xf02332d6
	SG(16, 0, 0);
f0103f45:	b8 50 49 10 f0       	mov    $0xf0104950,%eax
f0103f4a:	66 a3 e0 32 23 f0    	mov    %ax,0xf02332e0
f0103f50:	66 c7 05 e2 32 23 f0 	movw   $0x8,0xf02332e2
f0103f57:	08 00 
f0103f59:	c6 05 e4 32 23 f0 00 	movb   $0x0,0xf02332e4
f0103f60:	c6 05 e5 32 23 f0 8e 	movb   $0x8e,0xf02332e5
f0103f67:	c1 e8 10             	shr    $0x10,%eax
f0103f6a:	66 a3 e6 32 23 f0    	mov    %ax,0xf02332e6
	SG(17, 0, 0);
f0103f70:	b8 5a 49 10 f0       	mov    $0xf010495a,%eax
f0103f75:	66 a3 e8 32 23 f0    	mov    %ax,0xf02332e8
f0103f7b:	66 c7 05 ea 32 23 f0 	movw   $0x8,0xf02332ea
f0103f82:	08 00 
f0103f84:	c6 05 ec 32 23 f0 00 	movb   $0x0,0xf02332ec
f0103f8b:	c6 05 ed 32 23 f0 8e 	movb   $0x8e,0xf02332ed
f0103f92:	c1 e8 10             	shr    $0x10,%eax
f0103f95:	66 a3 ee 32 23 f0    	mov    %ax,0xf02332ee
	SG(18, 0, 0);
f0103f9b:	b8 62 49 10 f0       	mov    $0xf0104962,%eax
f0103fa0:	66 a3 f0 32 23 f0    	mov    %ax,0xf02332f0
f0103fa6:	66 c7 05 f2 32 23 f0 	movw   $0x8,0xf02332f2
f0103fad:	08 00 
f0103faf:	c6 05 f4 32 23 f0 00 	movb   $0x0,0xf02332f4
f0103fb6:	c6 05 f5 32 23 f0 8e 	movb   $0x8e,0xf02332f5
f0103fbd:	c1 e8 10             	shr    $0x10,%eax
f0103fc0:	66 a3 f6 32 23 f0    	mov    %ax,0xf02332f6
	SG(19, 0, 0);
f0103fc6:	b8 6c 49 10 f0       	mov    $0xf010496c,%eax
f0103fcb:	66 a3 f8 32 23 f0    	mov    %ax,0xf02332f8
f0103fd1:	66 c7 05 fa 32 23 f0 	movw   $0x8,0xf02332fa
f0103fd8:	08 00 
f0103fda:	c6 05 fc 32 23 f0 00 	movb   $0x0,0xf02332fc
f0103fe1:	c6 05 fd 32 23 f0 8e 	movb   $0x8e,0xf02332fd
f0103fe8:	c1 e8 10             	shr    $0x10,%eax
f0103feb:	66 a3 fe 32 23 f0    	mov    %ax,0xf02332fe

	// 16 IRQs from reference code
	SG(32, 0, 0);
f0103ff1:	b8 76 49 10 f0       	mov    $0xf0104976,%eax
f0103ff6:	66 a3 60 33 23 f0    	mov    %ax,0xf0233360
f0103ffc:	66 c7 05 62 33 23 f0 	movw   $0x8,0xf0233362
f0104003:	08 00 
f0104005:	c6 05 64 33 23 f0 00 	movb   $0x0,0xf0233364
f010400c:	c6 05 65 33 23 f0 8e 	movb   $0x8e,0xf0233365
f0104013:	c1 e8 10             	shr    $0x10,%eax
f0104016:	66 a3 66 33 23 f0    	mov    %ax,0xf0233366
	SG(33, 0, 0);
f010401c:	b8 80 49 10 f0       	mov    $0xf0104980,%eax
f0104021:	66 a3 68 33 23 f0    	mov    %ax,0xf0233368
f0104027:	66 c7 05 6a 33 23 f0 	movw   $0x8,0xf023336a
f010402e:	08 00 
f0104030:	c6 05 6c 33 23 f0 00 	movb   $0x0,0xf023336c
f0104037:	c6 05 6d 33 23 f0 8e 	movb   $0x8e,0xf023336d
f010403e:	c1 e8 10             	shr    $0x10,%eax
f0104041:	66 a3 6e 33 23 f0    	mov    %ax,0xf023336e
	SG(34, 0, 0);
f0104047:	b8 8a 49 10 f0       	mov    $0xf010498a,%eax
f010404c:	66 a3 70 33 23 f0    	mov    %ax,0xf0233370
f0104052:	66 c7 05 72 33 23 f0 	movw   $0x8,0xf0233372
f0104059:	08 00 
f010405b:	c6 05 74 33 23 f0 00 	movb   $0x0,0xf0233374
f0104062:	c6 05 75 33 23 f0 8e 	movb   $0x8e,0xf0233375
f0104069:	c1 e8 10             	shr    $0x10,%eax
f010406c:	66 a3 76 33 23 f0    	mov    %ax,0xf0233376
	SG(35, 0, 0);
f0104072:	b8 94 49 10 f0       	mov    $0xf0104994,%eax
f0104077:	66 a3 78 33 23 f0    	mov    %ax,0xf0233378
f010407d:	66 c7 05 7a 33 23 f0 	movw   $0x8,0xf023337a
f0104084:	08 00 
f0104086:	c6 05 7c 33 23 f0 00 	movb   $0x0,0xf023337c
f010408d:	c6 05 7d 33 23 f0 8e 	movb   $0x8e,0xf023337d
f0104094:	c1 e8 10             	shr    $0x10,%eax
f0104097:	66 a3 7e 33 23 f0    	mov    %ax,0xf023337e
	SG(36, 0, 0);
f010409d:	b8 9e 49 10 f0       	mov    $0xf010499e,%eax
f01040a2:	66 a3 80 33 23 f0    	mov    %ax,0xf0233380
f01040a8:	66 c7 05 82 33 23 f0 	movw   $0x8,0xf0233382
f01040af:	08 00 
f01040b1:	c6 05 84 33 23 f0 00 	movb   $0x0,0xf0233384
f01040b8:	c6 05 85 33 23 f0 8e 	movb   $0x8e,0xf0233385
f01040bf:	c1 e8 10             	shr    $0x10,%eax
f01040c2:	66 a3 86 33 23 f0    	mov    %ax,0xf0233386
	SG(37, 0, 0);
f01040c8:	b8 a8 49 10 f0       	mov    $0xf01049a8,%eax
f01040cd:	66 a3 88 33 23 f0    	mov    %ax,0xf0233388
f01040d3:	66 c7 05 8a 33 23 f0 	movw   $0x8,0xf023338a
f01040da:	08 00 
f01040dc:	c6 05 8c 33 23 f0 00 	movb   $0x0,0xf023338c
f01040e3:	c6 05 8d 33 23 f0 8e 	movb   $0x8e,0xf023338d
f01040ea:	c1 e8 10             	shr    $0x10,%eax
f01040ed:	66 a3 8e 33 23 f0    	mov    %ax,0xf023338e
	SG(38, 0, 0);
f01040f3:	b8 b2 49 10 f0       	mov    $0xf01049b2,%eax
f01040f8:	66 a3 90 33 23 f0    	mov    %ax,0xf0233390
f01040fe:	66 c7 05 92 33 23 f0 	movw   $0x8,0xf0233392
f0104105:	08 00 
f0104107:	c6 05 94 33 23 f0 00 	movb   $0x0,0xf0233394
f010410e:	c6 05 95 33 23 f0 8e 	movb   $0x8e,0xf0233395
f0104115:	c1 e8 10             	shr    $0x10,%eax
f0104118:	66 a3 96 33 23 f0    	mov    %ax,0xf0233396
	SG(39, 0, 0);
f010411e:	b8 bc 49 10 f0       	mov    $0xf01049bc,%eax
f0104123:	66 a3 98 33 23 f0    	mov    %ax,0xf0233398
f0104129:	66 c7 05 9a 33 23 f0 	movw   $0x8,0xf023339a
f0104130:	08 00 
f0104132:	c6 05 9c 33 23 f0 00 	movb   $0x0,0xf023339c
f0104139:	c6 05 9d 33 23 f0 8e 	movb   $0x8e,0xf023339d
f0104140:	c1 e8 10             	shr    $0x10,%eax
f0104143:	66 a3 9e 33 23 f0    	mov    %ax,0xf023339e
	SG(40, 0, 0);
f0104149:	b8 c6 49 10 f0       	mov    $0xf01049c6,%eax
f010414e:	66 a3 a0 33 23 f0    	mov    %ax,0xf02333a0
f0104154:	66 c7 05 a2 33 23 f0 	movw   $0x8,0xf02333a2
f010415b:	08 00 
f010415d:	c6 05 a4 33 23 f0 00 	movb   $0x0,0xf02333a4
f0104164:	c6 05 a5 33 23 f0 8e 	movb   $0x8e,0xf02333a5
f010416b:	c1 e8 10             	shr    $0x10,%eax
f010416e:	66 a3 a6 33 23 f0    	mov    %ax,0xf02333a6
	SG(41, 0, 0);
f0104174:	b8 d0 49 10 f0       	mov    $0xf01049d0,%eax
f0104179:	66 a3 a8 33 23 f0    	mov    %ax,0xf02333a8
f010417f:	66 c7 05 aa 33 23 f0 	movw   $0x8,0xf02333aa
f0104186:	08 00 
f0104188:	c6 05 ac 33 23 f0 00 	movb   $0x0,0xf02333ac
f010418f:	c6 05 ad 33 23 f0 8e 	movb   $0x8e,0xf02333ad
f0104196:	c1 e8 10             	shr    $0x10,%eax
f0104199:	66 a3 ae 33 23 f0    	mov    %ax,0xf02333ae
	SG(42, 0, 0);
f010419f:	b8 da 49 10 f0       	mov    $0xf01049da,%eax
f01041a4:	66 a3 b0 33 23 f0    	mov    %ax,0xf02333b0
f01041aa:	66 c7 05 b2 33 23 f0 	movw   $0x8,0xf02333b2
f01041b1:	08 00 
f01041b3:	c6 05 b4 33 23 f0 00 	movb   $0x0,0xf02333b4
f01041ba:	c6 05 b5 33 23 f0 8e 	movb   $0x8e,0xf02333b5
f01041c1:	c1 e8 10             	shr    $0x10,%eax
f01041c4:	66 a3 b6 33 23 f0    	mov    %ax,0xf02333b6
	SG(43, 0, 0);
f01041ca:	b8 e4 49 10 f0       	mov    $0xf01049e4,%eax
f01041cf:	66 a3 b8 33 23 f0    	mov    %ax,0xf02333b8
f01041d5:	66 c7 05 ba 33 23 f0 	movw   $0x8,0xf02333ba
f01041dc:	08 00 
f01041de:	c6 05 bc 33 23 f0 00 	movb   $0x0,0xf02333bc
f01041e5:	c6 05 bd 33 23 f0 8e 	movb   $0x8e,0xf02333bd
f01041ec:	c1 e8 10             	shr    $0x10,%eax
f01041ef:	66 a3 be 33 23 f0    	mov    %ax,0xf02333be
	SG(44, 0, 0);
f01041f5:	b8 ee 49 10 f0       	mov    $0xf01049ee,%eax
f01041fa:	66 a3 c0 33 23 f0    	mov    %ax,0xf02333c0
f0104200:	66 c7 05 c2 33 23 f0 	movw   $0x8,0xf02333c2
f0104207:	08 00 
f0104209:	c6 05 c4 33 23 f0 00 	movb   $0x0,0xf02333c4
f0104210:	c6 05 c5 33 23 f0 8e 	movb   $0x8e,0xf02333c5
f0104217:	c1 e8 10             	shr    $0x10,%eax
f010421a:	66 a3 c6 33 23 f0    	mov    %ax,0xf02333c6
	SG(45, 0, 0);
f0104220:	b8 f8 49 10 f0       	mov    $0xf01049f8,%eax
f0104225:	66 a3 c8 33 23 f0    	mov    %ax,0xf02333c8
f010422b:	66 c7 05 ca 33 23 f0 	movw   $0x8,0xf02333ca
f0104232:	08 00 
f0104234:	c6 05 cc 33 23 f0 00 	movb   $0x0,0xf02333cc
f010423b:	c6 05 cd 33 23 f0 8e 	movb   $0x8e,0xf02333cd
f0104242:	c1 e8 10             	shr    $0x10,%eax
f0104245:	66 a3 ce 33 23 f0    	mov    %ax,0xf02333ce
	SG(46, 0, 0);
f010424b:	b8 02 4a 10 f0       	mov    $0xf0104a02,%eax
f0104250:	66 a3 d0 33 23 f0    	mov    %ax,0xf02333d0
f0104256:	66 c7 05 d2 33 23 f0 	movw   $0x8,0xf02333d2
f010425d:	08 00 
f010425f:	c6 05 d4 33 23 f0 00 	movb   $0x0,0xf02333d4
f0104266:	c6 05 d5 33 23 f0 8e 	movb   $0x8e,0xf02333d5
f010426d:	c1 e8 10             	shr    $0x10,%eax
f0104270:	66 a3 d6 33 23 f0    	mov    %ax,0xf02333d6
	SG(47, 0, 0);
f0104276:	b8 0c 4a 10 f0       	mov    $0xf0104a0c,%eax
f010427b:	66 a3 d8 33 23 f0    	mov    %ax,0xf02333d8
f0104281:	66 c7 05 da 33 23 f0 	movw   $0x8,0xf02333da
f0104288:	08 00 
f010428a:	c6 05 dc 33 23 f0 00 	movb   $0x0,0xf02333dc
f0104291:	c6 05 dd 33 23 f0 8e 	movb   $0x8e,0xf02333dd
f0104298:	c1 e8 10             	shr    $0x10,%eax
f010429b:	66 a3 de 33 23 f0    	mov    %ax,0xf02333de
 
	SG(48, 0, 3); //for interrupt
f01042a1:	b8 16 4a 10 f0       	mov    $0xf0104a16,%eax
f01042a6:	66 a3 e0 33 23 f0    	mov    %ax,0xf02333e0
f01042ac:	66 c7 05 e2 33 23 f0 	movw   $0x8,0xf02333e2
f01042b3:	08 00 
f01042b5:	c6 05 e4 33 23 f0 00 	movb   $0x0,0xf02333e4
f01042bc:	c6 05 e5 33 23 f0 ee 	movb   $0xee,0xf02333e5
f01042c3:	c1 e8 10             	shr    $0x10,%eax
f01042c6:	66 a3 e6 33 23 f0    	mov    %ax,0xf02333e6
	// warn("trap_init: treat system call as not trap");

	// Per-CPU setup 
	trap_init_percpu();
f01042cc:	e8 54 f9 ff ff       	call   f0103c25 <trap_init_percpu>
}
f01042d1:	c9                   	leave  
f01042d2:	c3                   	ret    

f01042d3 <print_regs>:
	}
}

void
print_regs(struct PushRegs *regs)
{
f01042d3:	55                   	push   %ebp
f01042d4:	89 e5                	mov    %esp,%ebp
f01042d6:	53                   	push   %ebx
f01042d7:	83 ec 0c             	sub    $0xc,%esp
f01042da:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("  edi  0x%08x\n", regs->reg_edi);
f01042dd:	ff 33                	pushl  (%ebx)
f01042df:	68 ec 89 10 f0       	push   $0xf01089ec
f01042e4:	e8 28 f9 ff ff       	call   f0103c11 <cprintf>
	cprintf("  esi  0x%08x\n", regs->reg_esi);
f01042e9:	83 c4 08             	add    $0x8,%esp
f01042ec:	ff 73 04             	pushl  0x4(%ebx)
f01042ef:	68 fb 89 10 f0       	push   $0xf01089fb
f01042f4:	e8 18 f9 ff ff       	call   f0103c11 <cprintf>
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
f01042f9:	83 c4 08             	add    $0x8,%esp
f01042fc:	ff 73 08             	pushl  0x8(%ebx)
f01042ff:	68 0a 8a 10 f0       	push   $0xf0108a0a
f0104304:	e8 08 f9 ff ff       	call   f0103c11 <cprintf>
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
f0104309:	83 c4 08             	add    $0x8,%esp
f010430c:	ff 73 0c             	pushl  0xc(%ebx)
f010430f:	68 19 8a 10 f0       	push   $0xf0108a19
f0104314:	e8 f8 f8 ff ff       	call   f0103c11 <cprintf>
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
f0104319:	83 c4 08             	add    $0x8,%esp
f010431c:	ff 73 10             	pushl  0x10(%ebx)
f010431f:	68 28 8a 10 f0       	push   $0xf0108a28
f0104324:	e8 e8 f8 ff ff       	call   f0103c11 <cprintf>
	cprintf("  edx  0x%08x\n", regs->reg_edx);
f0104329:	83 c4 08             	add    $0x8,%esp
f010432c:	ff 73 14             	pushl  0x14(%ebx)
f010432f:	68 37 8a 10 f0       	push   $0xf0108a37
f0104334:	e8 d8 f8 ff ff       	call   f0103c11 <cprintf>
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
f0104339:	83 c4 08             	add    $0x8,%esp
f010433c:	ff 73 18             	pushl  0x18(%ebx)
f010433f:	68 46 8a 10 f0       	push   $0xf0108a46
f0104344:	e8 c8 f8 ff ff       	call   f0103c11 <cprintf>
	cprintf("  eax  0x%08x\n", regs->reg_eax);
f0104349:	83 c4 08             	add    $0x8,%esp
f010434c:	ff 73 1c             	pushl  0x1c(%ebx)
f010434f:	68 55 8a 10 f0       	push   $0xf0108a55
f0104354:	e8 b8 f8 ff ff       	call   f0103c11 <cprintf>
}
f0104359:	83 c4 10             	add    $0x10,%esp
f010435c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010435f:	c9                   	leave  
f0104360:	c3                   	ret    

f0104361 <print_trapframe>:
	lidt(&idt_pd);
}

void
print_trapframe(struct Trapframe *tf)
{
f0104361:	55                   	push   %ebp
f0104362:	89 e5                	mov    %esp,%ebp
f0104364:	53                   	push   %ebx
f0104365:	83 ec 04             	sub    $0x4,%esp
f0104368:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("TRAP frame at %p from CPU %d\n", tf, cpunum());
f010436b:	e8 be 23 00 00       	call   f010672e <cpunum>
f0104370:	83 ec 04             	sub    $0x4,%esp
f0104373:	50                   	push   %eax
f0104374:	53                   	push   %ebx
f0104375:	68 b9 8a 10 f0       	push   $0xf0108ab9
f010437a:	e8 92 f8 ff ff       	call   f0103c11 <cprintf>
	print_regs(&tf->tf_regs);
f010437f:	89 1c 24             	mov    %ebx,(%esp)
f0104382:	e8 4c ff ff ff       	call   f01042d3 <print_regs>
	cprintf("  es   0x----%04x\n", tf->tf_es);
f0104387:	83 c4 08             	add    $0x8,%esp
f010438a:	0f b7 43 20          	movzwl 0x20(%ebx),%eax
f010438e:	50                   	push   %eax
f010438f:	68 d7 8a 10 f0       	push   $0xf0108ad7
f0104394:	e8 78 f8 ff ff       	call   f0103c11 <cprintf>
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
f0104399:	83 c4 08             	add    $0x8,%esp
f010439c:	0f b7 43 24          	movzwl 0x24(%ebx),%eax
f01043a0:	50                   	push   %eax
f01043a1:	68 ea 8a 10 f0       	push   $0xf0108aea
f01043a6:	e8 66 f8 ff ff       	call   f0103c11 <cprintf>
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f01043ab:	8b 43 28             	mov    0x28(%ebx),%eax
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < ARRAY_SIZE(excnames))
f01043ae:	83 c4 10             	add    $0x10,%esp
f01043b1:	83 f8 13             	cmp    $0x13,%eax
f01043b4:	77 09                	ja     f01043bf <print_trapframe+0x5e>
		return excnames[trapno];
f01043b6:	8b 14 85 80 8d 10 f0 	mov    -0xfef7280(,%eax,4),%edx
f01043bd:	eb 20                	jmp    f01043df <print_trapframe+0x7e>
	if (trapno == T_SYSCALL)
f01043bf:	83 f8 30             	cmp    $0x30,%eax
f01043c2:	74 0f                	je     f01043d3 <print_trapframe+0x72>
		return "System call";
	if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
f01043c4:	8d 50 e0             	lea    -0x20(%eax),%edx
f01043c7:	83 fa 0f             	cmp    $0xf,%edx
f01043ca:	76 0e                	jbe    f01043da <print_trapframe+0x79>
		return "Hardware Interrupt";
	return "(unknown trap)";
f01043cc:	ba 83 8a 10 f0       	mov    $0xf0108a83,%edx
f01043d1:	eb 0c                	jmp    f01043df <print_trapframe+0x7e>
	};

	if (trapno < ARRAY_SIZE(excnames))
		return excnames[trapno];
	if (trapno == T_SYSCALL)
		return "System call";
f01043d3:	ba 64 8a 10 f0       	mov    $0xf0108a64,%edx
f01043d8:	eb 05                	jmp    f01043df <print_trapframe+0x7e>
	if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
		return "Hardware Interrupt";
f01043da:	ba 70 8a 10 f0       	mov    $0xf0108a70,%edx
{
	cprintf("TRAP frame at %p from CPU %d\n", tf, cpunum());
	print_regs(&tf->tf_regs);
	cprintf("  es   0x----%04x\n", tf->tf_es);
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f01043df:	83 ec 04             	sub    $0x4,%esp
f01043e2:	52                   	push   %edx
f01043e3:	50                   	push   %eax
f01043e4:	68 fd 8a 10 f0       	push   $0xf0108afd
f01043e9:	e8 23 f8 ff ff       	call   f0103c11 <cprintf>
	// If this trap was a page fault that just happened
	// (so %cr2 is meaningful), print the faulting linear address.
	if (tf == last_tf && tf->tf_trapno == T_PGFLT)
f01043ee:	83 c4 10             	add    $0x10,%esp
f01043f1:	3b 1d 60 3a 23 f0    	cmp    0xf0233a60,%ebx
f01043f7:	75 1a                	jne    f0104413 <print_trapframe+0xb2>
f01043f9:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f01043fd:	75 14                	jne    f0104413 <print_trapframe+0xb2>

static inline uint32_t
rcr2(void)
{
	uint32_t val;
	asm volatile("movl %%cr2,%0" : "=r" (val));
f01043ff:	0f 20 d0             	mov    %cr2,%eax
		cprintf("  cr2  0x%08x\n", rcr2());
f0104402:	83 ec 08             	sub    $0x8,%esp
f0104405:	50                   	push   %eax
f0104406:	68 0f 8b 10 f0       	push   $0xf0108b0f
f010440b:	e8 01 f8 ff ff       	call   f0103c11 <cprintf>
f0104410:	83 c4 10             	add    $0x10,%esp
	cprintf("  err  0x%08x", tf->tf_err);
f0104413:	83 ec 08             	sub    $0x8,%esp
f0104416:	ff 73 2c             	pushl  0x2c(%ebx)
f0104419:	68 1e 8b 10 f0       	push   $0xf0108b1e
f010441e:	e8 ee f7 ff ff       	call   f0103c11 <cprintf>
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
f0104423:	83 c4 10             	add    $0x10,%esp
f0104426:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f010442a:	75 45                	jne    f0104471 <print_trapframe+0x110>
		cprintf(" [%s, %s, %s]\n",
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
f010442c:	8b 43 2c             	mov    0x2c(%ebx),%eax
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
		cprintf(" [%s, %s, %s]\n",
f010442f:	a8 01                	test   $0x1,%al
f0104431:	75 07                	jne    f010443a <print_trapframe+0xd9>
f0104433:	b9 9d 8a 10 f0       	mov    $0xf0108a9d,%ecx
f0104438:	eb 05                	jmp    f010443f <print_trapframe+0xde>
f010443a:	b9 92 8a 10 f0       	mov    $0xf0108a92,%ecx
f010443f:	a8 02                	test   $0x2,%al
f0104441:	75 07                	jne    f010444a <print_trapframe+0xe9>
f0104443:	ba af 8a 10 f0       	mov    $0xf0108aaf,%edx
f0104448:	eb 05                	jmp    f010444f <print_trapframe+0xee>
f010444a:	ba a9 8a 10 f0       	mov    $0xf0108aa9,%edx
f010444f:	a8 04                	test   $0x4,%al
f0104451:	75 07                	jne    f010445a <print_trapframe+0xf9>
f0104453:	b8 04 8c 10 f0       	mov    $0xf0108c04,%eax
f0104458:	eb 05                	jmp    f010445f <print_trapframe+0xfe>
f010445a:	b8 b4 8a 10 f0       	mov    $0xf0108ab4,%eax
f010445f:	51                   	push   %ecx
f0104460:	52                   	push   %edx
f0104461:	50                   	push   %eax
f0104462:	68 2c 8b 10 f0       	push   $0xf0108b2c
f0104467:	e8 a5 f7 ff ff       	call   f0103c11 <cprintf>
f010446c:	83 c4 10             	add    $0x10,%esp
f010446f:	eb 10                	jmp    f0104481 <print_trapframe+0x120>
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
	else
		cprintf("\n");
f0104471:	83 ec 0c             	sub    $0xc,%esp
f0104474:	68 b2 86 10 f0       	push   $0xf01086b2
f0104479:	e8 93 f7 ff ff       	call   f0103c11 <cprintf>
f010447e:	83 c4 10             	add    $0x10,%esp
	cprintf("  eip  0x%08x\n", tf->tf_eip);
f0104481:	83 ec 08             	sub    $0x8,%esp
f0104484:	ff 73 30             	pushl  0x30(%ebx)
f0104487:	68 3b 8b 10 f0       	push   $0xf0108b3b
f010448c:	e8 80 f7 ff ff       	call   f0103c11 <cprintf>
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
f0104491:	83 c4 08             	add    $0x8,%esp
f0104494:	0f b7 43 34          	movzwl 0x34(%ebx),%eax
f0104498:	50                   	push   %eax
f0104499:	68 4a 8b 10 f0       	push   $0xf0108b4a
f010449e:	e8 6e f7 ff ff       	call   f0103c11 <cprintf>
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
f01044a3:	83 c4 08             	add    $0x8,%esp
f01044a6:	ff 73 38             	pushl  0x38(%ebx)
f01044a9:	68 5d 8b 10 f0       	push   $0xf0108b5d
f01044ae:	e8 5e f7 ff ff       	call   f0103c11 <cprintf>
	if ((tf->tf_cs & 3) != 0) {
f01044b3:	83 c4 10             	add    $0x10,%esp
f01044b6:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f01044ba:	74 25                	je     f01044e1 <print_trapframe+0x180>
		cprintf("  esp  0x%08x\n", tf->tf_esp);
f01044bc:	83 ec 08             	sub    $0x8,%esp
f01044bf:	ff 73 3c             	pushl  0x3c(%ebx)
f01044c2:	68 6c 8b 10 f0       	push   $0xf0108b6c
f01044c7:	e8 45 f7 ff ff       	call   f0103c11 <cprintf>
		cprintf("  ss   0x----%04x\n", tf->tf_ss);
f01044cc:	83 c4 08             	add    $0x8,%esp
f01044cf:	0f b7 43 40          	movzwl 0x40(%ebx),%eax
f01044d3:	50                   	push   %eax
f01044d4:	68 7b 8b 10 f0       	push   $0xf0108b7b
f01044d9:	e8 33 f7 ff ff       	call   f0103c11 <cprintf>
f01044de:	83 c4 10             	add    $0x10,%esp
	}
}
f01044e1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01044e4:	c9                   	leave  
f01044e5:	c3                   	ret    

f01044e6 <page_fault_handler>:
}

//special function to handle page fault
void
page_fault_handler(struct Trapframe *tf)
{
f01044e6:	55                   	push   %ebp
f01044e7:	89 e5                	mov    %esp,%ebp
f01044e9:	57                   	push   %edi
f01044ea:	56                   	push   %esi
f01044eb:	53                   	push   %ebx
f01044ec:	83 ec 1c             	sub    $0x1c,%esp
f01044ef:	8b 5d 08             	mov    0x8(%ebp),%ebx
f01044f2:	0f 20 d6             	mov    %cr2,%esi

	// Handle kernel-mode page faults.

	// LAB 3: Your code here.
//------------
	if ((tf->tf_cs & 0x3) == 0) { //if page fault happens in kernel
f01044f5:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f01044f9:	75 20                	jne    f010451b <page_fault_handler+0x35>
        print_trapframe(tf);
f01044fb:	83 ec 0c             	sub    $0xc,%esp
f01044fe:	53                   	push   %ebx
f01044ff:	e8 5d fe ff ff       	call   f0104361 <print_trapframe>
		panic("page fault in kernel space");
f0104504:	83 c4 0c             	add    $0xc,%esp
f0104507:	68 8e 8b 10 f0       	push   $0xf0108b8e
f010450c:	68 93 01 00 00       	push   $0x193
f0104511:	68 a9 8b 10 f0       	push   $0xf0108ba9
f0104516:	e8 31 bb ff ff       	call   f010004c <_panic>

	// LAB 4: Your code here.
    // If no pgfault_upcall, fall to destruction.
    // If page faults take place in the region [USTACKTOP, UXSTACKTOP - PGSIZE),
    // it means the exception stack is exhausted.
    if (curenv->env_pgfault_upcall &&
f010451b:	e8 0e 22 00 00       	call   f010672e <cpunum>
f0104520:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104523:	01 c2                	add    %eax,%edx
f0104525:	01 d2                	add    %edx,%edx
f0104527:	01 c2                	add    %eax,%edx
f0104529:	8d 04 90             	lea    (%eax,%edx,4),%eax
f010452c:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0104533:	83 78 64 00          	cmpl   $0x0,0x64(%eax)
f0104537:	0f 84 df 00 00 00    	je     f010461c <page_fault_handler+0x136>
        (tf->tf_esp < USTACKTOP ||
f010453d:	8b 43 3c             	mov    0x3c(%ebx),%eax

	// LAB 4: Your code here.
    // If no pgfault_upcall, fall to destruction.
    // If page faults take place in the region [USTACKTOP, UXSTACKTOP - PGSIZE),
    // it means the exception stack is exhausted.
    if (curenv->env_pgfault_upcall &&
f0104540:	8d 90 00 20 40 11    	lea    0x11402000(%eax),%edx
f0104546:	81 fa ff 0f 00 00    	cmp    $0xfff,%edx
f010454c:	0f 86 ca 00 00 00    	jbe    f010461c <page_fault_handler+0x136>
        (tf->tf_esp < USTACKTOP ||
         tf->tf_esp >= UXSTACKTOP - PGSIZE)) {
    //if(curenv->env_pgfault_upcall){
        // Determine the starting address of the stack frame.
        uint32_t xtop;
        if (tf->tf_esp >= UXSTACKTOP - PGSIZE &&
f0104552:	8d 90 00 10 40 11    	lea    0x11401000(%eax),%edx
            tf->tf_esp < UXSTACKTOP)
            xtop = tf->tf_esp - sizeof(struct UTrapframe) - 4/* an required empty word */;
        else
            xtop = UXSTACKTOP - sizeof(struct UTrapframe);
f0104558:	c7 45 e4 cc ff bf ee 	movl   $0xeebfffcc,-0x1c(%ebp)
        (tf->tf_esp < USTACKTOP ||
         tf->tf_esp >= UXSTACKTOP - PGSIZE)) {
    //if(curenv->env_pgfault_upcall){
        // Determine the starting address of the stack frame.
        uint32_t xtop;
        if (tf->tf_esp >= UXSTACKTOP - PGSIZE &&
f010455f:	81 fa ff 0f 00 00    	cmp    $0xfff,%edx
f0104565:	77 06                	ja     f010456d <page_fault_handler+0x87>
            tf->tf_esp < UXSTACKTOP)
            xtop = tf->tf_esp - sizeof(struct UTrapframe) - 4/* an required empty word */;
f0104567:	83 e8 38             	sub    $0x38,%eax
f010456a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        else
            xtop = UXSTACKTOP - sizeof(struct UTrapframe);
        
        // Check permission for the whole exception stack, but for only once.
        static int is_xstack_checked = 1;
        if (!is_xstack_checked) {
f010456d:	83 3d a8 53 12 f0 00 	cmpl   $0x0,0xf01253a8
f0104574:	75 2c                	jne    f01045a2 <page_fault_handler+0xbc>
            user_mem_assert(curenv, (void *)(UXSTACKTOP - PGSIZE), PGSIZE, PTE_W | PTE_U);
f0104576:	e8 b3 21 00 00       	call   f010672e <cpunum>
f010457b:	6a 06                	push   $0x6
f010457d:	68 00 10 00 00       	push   $0x1000
f0104582:	68 00 f0 bf ee       	push   $0xeebff000
f0104587:	6b c0 74             	imul   $0x74,%eax,%eax
f010458a:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f0104590:	e8 8f ec ff ff       	call   f0103224 <user_mem_assert>
            is_xstack_checked = 1;
f0104595:	c7 05 a8 53 12 f0 01 	movl   $0x1,0xf01253a8
f010459c:	00 00 00 
f010459f:	83 c4 10             	add    $0x10,%esp
        }
        
        // For the damn test to be OK.
        user_mem_assert(curenv, (void *)xtop, UXSTACKTOP - xtop, PTE_W | PTE_U);
f01045a2:	e8 87 21 00 00       	call   f010672e <cpunum>
f01045a7:	6a 06                	push   $0x6
f01045a9:	ba 00 00 c0 ee       	mov    $0xeec00000,%edx
f01045ae:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f01045b1:	29 fa                	sub    %edi,%edx
f01045b3:	52                   	push   %edx
f01045b4:	57                   	push   %edi
f01045b5:	6b c0 74             	imul   $0x74,%eax,%eax
f01045b8:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f01045be:	e8 61 ec ff ff       	call   f0103224 <user_mem_assert>

        // Push the struct UTrapframe onto the stack.
        struct UTrapframe *utf = (struct UTrapframe *)xtop;
        utf->utf_eflags = tf->tf_eflags;
f01045c3:	8b 43 38             	mov    0x38(%ebx),%eax
f01045c6:	89 47 2c             	mov    %eax,0x2c(%edi)
        utf->utf_eip = tf->tf_eip;
f01045c9:	8b 43 30             	mov    0x30(%ebx),%eax
f01045cc:	89 47 28             	mov    %eax,0x28(%edi)
        utf->utf_err = tf->tf_err;
f01045cf:	8b 43 2c             	mov    0x2c(%ebx),%eax
f01045d2:	89 47 04             	mov    %eax,0x4(%edi)
        utf->utf_esp = tf->tf_esp;
f01045d5:	8b 43 3c             	mov    0x3c(%ebx),%eax
f01045d8:	89 47 30             	mov    %eax,0x30(%edi)
        utf->utf_fault_va = fault_va;
f01045db:	89 37                	mov    %esi,(%edi)
        utf->utf_regs = tf->tf_regs;
f01045dd:	89 7d e4             	mov    %edi,-0x1c(%ebp)
f01045e0:	8d 7f 08             	lea    0x8(%edi),%edi
f01045e3:	b9 08 00 00 00       	mov    $0x8,%ecx
f01045e8:	89 de                	mov    %ebx,%esi
f01045ea:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
        
        // Run current env with user-page fault handler.
        tf->tf_eip = (uint32_t)curenv->env_pgfault_upcall;
f01045ec:	e8 3d 21 00 00       	call   f010672e <cpunum>
f01045f1:	6b c0 74             	imul   $0x74,%eax,%eax
f01045f4:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f01045fa:	8b 40 64             	mov    0x64(%eax),%eax
f01045fd:	89 43 30             	mov    %eax,0x30(%ebx)
        tf->tf_esp = xtop;
f0104600:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f0104603:	89 4b 3c             	mov    %ecx,0x3c(%ebx)
        env_run(curenv);
f0104606:	e8 23 21 00 00       	call   f010672e <cpunum>
f010460b:	83 c4 04             	add    $0x4,%esp
f010460e:	6b c0 74             	imul   $0x74,%eax,%eax
f0104611:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f0104617:	e8 2d f4 ff ff       	call   f0103a49 <env_run>

        // Never reach here.   
    }
	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f010461c:	8b 7b 30             	mov    0x30(%ebx),%edi
		curenv->env_id, fault_va, tf->tf_eip);
f010461f:	e8 0a 21 00 00       	call   f010672e <cpunum>
        env_run(curenv);

        // Never reach here.   
    }
	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0104624:	57                   	push   %edi
f0104625:	56                   	push   %esi
		curenv->env_id, fault_va, tf->tf_eip);
f0104626:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104629:	01 c2                	add    %eax,%edx
f010462b:	01 d2                	add    %edx,%edx
f010462d:	01 c2                	add    %eax,%edx
f010462f:	8d 04 90             	lea    (%eax,%edx,4),%eax
        env_run(curenv);

        // Never reach here.   
    }
	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0104632:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0104639:	ff 70 48             	pushl  0x48(%eax)
f010463c:	68 50 8d 10 f0       	push   $0xf0108d50
f0104641:	e8 cb f5 ff ff       	call   f0103c11 <cprintf>
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
f0104646:	89 1c 24             	mov    %ebx,(%esp)
f0104649:	e8 13 fd ff ff       	call   f0104361 <print_trapframe>
	env_destroy(curenv);
f010464e:	e8 db 20 00 00       	call   f010672e <cpunum>
f0104653:	83 c4 04             	add    $0x4,%esp
f0104656:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104659:	01 c2                	add    %eax,%edx
f010465b:	01 d2                	add    %edx,%edx
f010465d:	01 c2                	add    %eax,%edx
f010465f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104662:	ff 34 85 08 50 23 f0 	pushl  -0xfdcaff8(,%eax,4)
f0104669:	e8 01 f3 ff ff       	call   f010396f <env_destroy>
}
f010466e:	83 c4 10             	add    $0x10,%esp
f0104671:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104674:	5b                   	pop    %ebx
f0104675:	5e                   	pop    %esi
f0104676:	5f                   	pop    %edi
f0104677:	5d                   	pop    %ebp
f0104678:	c3                   	ret    

f0104679 <trap>:
	}
}

void
trap(struct Trapframe *tf)
{
f0104679:	55                   	push   %ebp
f010467a:	89 e5                	mov    %esp,%ebp
f010467c:	57                   	push   %edi
f010467d:	56                   	push   %esi
f010467e:	8b 75 08             	mov    0x8(%ebp),%esi
	// The environment may have set DF and some versions
	// of GCC rely on DF being clear
	asm volatile("cld" ::: "cc");
f0104681:	fc                   	cld    

	// Halt the CPU if some other CPU has called panic()
	extern char *panicstr;
	if (panicstr)
f0104682:	83 3d 10 3f 23 f0 00 	cmpl   $0x0,0xf0233f10
f0104689:	74 01                	je     f010468c <trap+0x13>
		asm volatile("hlt");
f010468b:	f4                   	hlt    

	// Re-acqurie the big kernel lock if we were halted in
	// sched_yield()
	if (xchg(&thiscpu->cpu_status, CPU_STARTED) == CPU_HALTED)
f010468c:	e8 9d 20 00 00       	call   f010672e <cpunum>
f0104691:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104694:	01 c2                	add    %eax,%edx
f0104696:	01 d2                	add    %edx,%edx
f0104698:	01 c2                	add    %eax,%edx
f010469a:	8d 04 90             	lea    (%eax,%edx,4),%eax
f010469d:	8d 14 85 00 50 23 f0 	lea    -0xfdcb000(,%eax,4),%edx
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f01046a4:	b8 01 00 00 00       	mov    $0x1,%eax
f01046a9:	f0 87 42 04          	lock xchg %eax,0x4(%edx)
f01046ad:	83 f8 02             	cmp    $0x2,%eax
f01046b0:	75 10                	jne    f01046c2 <trap+0x49>
extern struct spinlock kernel_lock;

static inline void
lock_kernel(void)
{
	spin_lock(&kernel_lock);
f01046b2:	83 ec 0c             	sub    $0xc,%esp
f01046b5:	68 c0 53 12 f0       	push   $0xf01253c0
f01046ba:	e8 2d 24 00 00       	call   f0106aec <spin_lock>
f01046bf:	83 c4 10             	add    $0x10,%esp

static inline uint32_t
read_eflags(void)
{
	uint32_t eflags;
	asm volatile("pushfl; popl %0" : "=r" (eflags));
f01046c2:	9c                   	pushf  
f01046c3:	58                   	pop    %eax
		lock_kernel();
	// Check that interrupts are disabled.  If this assertion
	// fails, DO NOT be tempted to fix it by inserting a "cli" in
	// the interrupt path.
	assert(!(read_eflags() & FL_IF));
f01046c4:	f6 c4 02             	test   $0x2,%ah
f01046c7:	74 19                	je     f01046e2 <trap+0x69>
f01046c9:	68 b5 8b 10 f0       	push   $0xf0108bb5
f01046ce:	68 51 75 10 f0       	push   $0xf0107551
f01046d3:	68 52 01 00 00       	push   $0x152
f01046d8:	68 a9 8b 10 f0       	push   $0xf0108ba9
f01046dd:	e8 6a b9 ff ff       	call   f010004c <_panic>

	if ((tf->tf_cs & 3) == 3) {
f01046e2:	66 8b 46 34          	mov    0x34(%esi),%ax
f01046e6:	83 e0 03             	and    $0x3,%eax
f01046e9:	66 83 f8 03          	cmp    $0x3,%ax
f01046ed:	0f 85 a0 00 00 00    	jne    f0104793 <trap+0x11a>
		// Trapped from user mode.
		// Acquire the big kernel lock before doing any
		// serious kernel work.
		// LAB 4: Your code here.
		assert(curenv);
f01046f3:	e8 36 20 00 00       	call   f010672e <cpunum>
f01046f8:	6b c0 74             	imul   $0x74,%eax,%eax
f01046fb:	83 b8 08 50 23 f0 00 	cmpl   $0x0,-0xfdcaff8(%eax)
f0104702:	75 19                	jne    f010471d <trap+0xa4>
f0104704:	68 ce 8b 10 f0       	push   $0xf0108bce
f0104709:	68 51 75 10 f0       	push   $0xf0107551
f010470e:	68 59 01 00 00       	push   $0x159
f0104713:	68 a9 8b 10 f0       	push   $0xf0108ba9
f0104718:	e8 2f b9 ff ff       	call   f010004c <_panic>
f010471d:	83 ec 0c             	sub    $0xc,%esp
f0104720:	68 c0 53 12 f0       	push   $0xf01253c0
f0104725:	e8 c2 23 00 00       	call   f0106aec <spin_lock>
		lock_kernel();
		// Garbage collect if current enviroment is a zombie
		if (curenv->env_status == ENV_DYING) {
f010472a:	e8 ff 1f 00 00       	call   f010672e <cpunum>
f010472f:	6b c0 74             	imul   $0x74,%eax,%eax
f0104732:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f0104738:	83 c4 10             	add    $0x10,%esp
f010473b:	83 78 54 01          	cmpl   $0x1,0x54(%eax)
f010473f:	75 2d                	jne    f010476e <trap+0xf5>
			env_free(curenv);
f0104741:	e8 e8 1f 00 00       	call   f010672e <cpunum>
f0104746:	83 ec 0c             	sub    $0xc,%esp
f0104749:	6b c0 74             	imul   $0x74,%eax,%eax
f010474c:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f0104752:	e8 5f f0 ff ff       	call   f01037b6 <env_free>
			curenv = NULL;
f0104757:	e8 d2 1f 00 00       	call   f010672e <cpunum>
f010475c:	6b c0 74             	imul   $0x74,%eax,%eax
f010475f:	c7 80 08 50 23 f0 00 	movl   $0x0,-0xfdcaff8(%eax)
f0104766:	00 00 00 
			sched_yield();
f0104769:	e8 b1 03 00 00       	call   f0104b1f <sched_yield>
		}

		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		curenv->env_tf = *tf;
f010476e:	e8 bb 1f 00 00       	call   f010672e <cpunum>
f0104773:	6b c0 74             	imul   $0x74,%eax,%eax
f0104776:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f010477c:	b9 11 00 00 00       	mov    $0x11,%ecx
f0104781:	89 c7                	mov    %eax,%edi
f0104783:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
f0104785:	e8 a4 1f 00 00       	call   f010672e <cpunum>
f010478a:	6b c0 74             	imul   $0x74,%eax,%eax
f010478d:	8b b0 08 50 23 f0    	mov    -0xfdcaff8(%eax),%esi
	}

	// Record that tf is the last real trapframe so
	// print_trapframe can print some additional information.
	last_tf = tf;
f0104793:	89 35 60 3a 23 f0    	mov    %esi,0xf0233a60
	// LAB 3: Your code here.

	// Handle spurious interrupts
	// The hardware sometimes raises these because of noise on the
	// IRQ line or other reasons. We don't care.
	if (tf->tf_trapno == IRQ_OFFSET + IRQ_SPURIOUS) {
f0104799:	8b 46 28             	mov    0x28(%esi),%eax
f010479c:	83 f8 27             	cmp    $0x27,%eax
f010479f:	75 1d                	jne    f01047be <trap+0x145>
		cprintf("Spurious interrupt on irq 7\n");
f01047a1:	83 ec 0c             	sub    $0xc,%esp
f01047a4:	68 d5 8b 10 f0       	push   $0xf0108bd5
f01047a9:	e8 63 f4 ff ff       	call   f0103c11 <cprintf>
		print_trapframe(tf);
f01047ae:	89 34 24             	mov    %esi,(%esp)
f01047b1:	e8 ab fb ff ff       	call   f0104361 <print_trapframe>
f01047b6:	83 c4 10             	add    $0x10,%esp
f01047b9:	e9 d2 00 00 00       	jmp    f0104890 <trap+0x217>


	// Handle keyboard and serial interrupts.
	// LAB 5: Your code here.

	switch (tf->tf_trapno) {
f01047be:	83 f8 20             	cmp    $0x20,%eax
f01047c1:	74 6d                	je     f0104830 <trap+0x1b7>
f01047c3:	83 f8 20             	cmp    $0x20,%eax
f01047c6:	77 0c                	ja     f01047d4 <trap+0x15b>
f01047c8:	83 f8 03             	cmp    $0x3,%eax
f01047cb:	74 29                	je     f01047f6 <trap+0x17d>
f01047cd:	83 f8 0e             	cmp    $0xe,%eax
f01047d0:	74 13                	je     f01047e5 <trap+0x16c>
f01047d2:	eb 79                	jmp    f010484d <trap+0x1d4>
f01047d4:	83 f8 24             	cmp    $0x24,%eax
f01047d7:	74 6d                	je     f0104846 <trap+0x1cd>
f01047d9:	83 f8 30             	cmp    $0x30,%eax
f01047dc:	74 31                	je     f010480f <trap+0x196>
f01047de:	83 f8 21             	cmp    $0x21,%eax
f01047e1:	75 6a                	jne    f010484d <trap+0x1d4>
f01047e3:	eb 5a                	jmp    f010483f <trap+0x1c6>
		case T_PGFLT:
			page_fault_handler(tf);
f01047e5:	83 ec 0c             	sub    $0xc,%esp
f01047e8:	56                   	push   %esi
f01047e9:	e8 f8 fc ff ff       	call   f01044e6 <page_fault_handler>
f01047ee:	83 c4 10             	add    $0x10,%esp
f01047f1:	e9 9a 00 00 00       	jmp    f0104890 <trap+0x217>
			break;
		case T_BRKPT:
			print_trapframe(tf);
f01047f6:	83 ec 0c             	sub    $0xc,%esp
f01047f9:	56                   	push   %esi
f01047fa:	e8 62 fb ff ff       	call   f0104361 <print_trapframe>
			monitor(tf); //invoke the kernel monitor
f01047ff:	89 34 24             	mov    %esi,(%esp)
f0104802:	e8 6a c2 ff ff       	call   f0100a71 <monitor>
f0104807:	83 c4 10             	add    $0x10,%esp
f010480a:	e9 81 00 00 00       	jmp    f0104890 <trap+0x217>

		//case IRQ_OFFSET+IRQ_TIMER:
		//	break;
		case T_SYSCALL:
			//syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
			tf->tf_regs.reg_eax = syscall(
f010480f:	83 ec 08             	sub    $0x8,%esp
f0104812:	ff 76 04             	pushl  0x4(%esi)
f0104815:	ff 36                	pushl  (%esi)
f0104817:	ff 76 10             	pushl  0x10(%esi)
f010481a:	ff 76 18             	pushl  0x18(%esi)
f010481d:	ff 76 14             	pushl  0x14(%esi)
f0104820:	ff 76 1c             	pushl  0x1c(%esi)
f0104823:	e8 8b 03 00 00       	call   f0104bb3 <syscall>
f0104828:	89 46 1c             	mov    %eax,0x1c(%esi)
f010482b:	83 c4 20             	add    $0x20,%esp
f010482e:	eb 60                	jmp    f0104890 <trap+0x217>
            tf->tf_regs.reg_ebx,
            tf->tf_regs.reg_edi,
            tf->tf_regs.reg_esi);
            break;
         case IRQ_OFFSET + IRQ_TIMER:
         	time_tick();
f0104830:	e8 67 24 00 00       	call   f0106c9c <time_tick>
         	lapic_eoi();
f0104835:	e8 c8 20 00 00       	call   f0106902 <lapic_eoi>
         	sched_yield();
f010483a:	e8 e0 02 00 00       	call   f0104b1f <sched_yield>
         	break;

         case IRQ_OFFSET + IRQ_KBD:
		 	kbd_intr();
f010483f:	e8 68 be ff ff       	call   f01006ac <kbd_intr>
f0104844:	eb 4a                	jmp    f0104890 <trap+0x217>
		 	return;
		 case IRQ_OFFSET + IRQ_SERIAL:
		 	serial_intr();
f0104846:	e8 46 be ff ff       	call   f0100691 <serial_intr>
f010484b:	eb 43                	jmp    f0104890 <trap+0x217>
		 	return;
		default:
			// Unexpected trap: The user process or the kernel has a bug.
			print_trapframe(tf);
f010484d:	83 ec 0c             	sub    $0xc,%esp
f0104850:	56                   	push   %esi
f0104851:	e8 0b fb ff ff       	call   f0104361 <print_trapframe>
			if (tf->tf_cs == GD_KT)
f0104856:	83 c4 10             	add    $0x10,%esp
f0104859:	66 83 7e 34 08       	cmpw   $0x8,0x34(%esi)
f010485e:	75 17                	jne    f0104877 <trap+0x1fe>
				panic("unhandled trap in kernel");
f0104860:	83 ec 04             	sub    $0x4,%esp
f0104863:	68 f2 8b 10 f0       	push   $0xf0108bf2
f0104868:	68 37 01 00 00       	push   $0x137
f010486d:	68 a9 8b 10 f0       	push   $0xf0108ba9
f0104872:	e8 d5 b7 ff ff       	call   f010004c <_panic>
			else {
				env_destroy(curenv);
f0104877:	e8 b2 1e 00 00       	call   f010672e <cpunum>
f010487c:	83 ec 0c             	sub    $0xc,%esp
f010487f:	6b c0 74             	imul   $0x74,%eax,%eax
f0104882:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f0104888:	e8 e2 f0 ff ff       	call   f010396f <env_destroy>
f010488d:	83 c4 10             	add    $0x10,%esp

//<<<<<<< HEAD
	// If we made it to this point, then no other environment was
	// scheduled, so we should return to the current environment
	// if doing so makes sense.
	if (curenv && curenv->env_status == ENV_RUNNING)
f0104890:	e8 99 1e 00 00       	call   f010672e <cpunum>
f0104895:	6b c0 74             	imul   $0x74,%eax,%eax
f0104898:	83 b8 08 50 23 f0 00 	cmpl   $0x0,-0xfdcaff8(%eax)
f010489f:	74 2a                	je     f01048cb <trap+0x252>
f01048a1:	e8 88 1e 00 00       	call   f010672e <cpunum>
f01048a6:	6b c0 74             	imul   $0x74,%eax,%eax
f01048a9:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f01048af:	83 78 54 03          	cmpl   $0x3,0x54(%eax)
f01048b3:	75 16                	jne    f01048cb <trap+0x252>
		
		env_run(curenv);
f01048b5:	e8 74 1e 00 00       	call   f010672e <cpunum>
f01048ba:	83 ec 0c             	sub    $0xc,%esp
f01048bd:	6b c0 74             	imul   $0x74,%eax,%eax
f01048c0:	ff b0 08 50 23 f0    	pushl  -0xfdcaff8(%eax)
f01048c6:	e8 7e f1 ff ff       	call   f0103a49 <env_run>
		//panic("should we get to the end of trap()???\n");
	else
		sched_yield();
f01048cb:	e8 4f 02 00 00       	call   f0104b1f <sched_yield>

f01048d0 <handler0>:
#define TY(N) TRAPHANDLER(H(N), N)
#define TN(N) TRAPHANDLER_NOEC(H(N), N)
#define L(N) .long H(N)
#define S(N) .long 0 /* skip */

TN(0)
f01048d0:	6a 00                	push   $0x0
f01048d2:	6a 00                	push   $0x0
f01048d4:	e9 46 01 00 00       	jmp    f0104a1f <_alltraps>
f01048d9:	90                   	nop

f01048da <handler1>:
TN(1)
f01048da:	6a 00                	push   $0x0
f01048dc:	6a 01                	push   $0x1
f01048de:	e9 3c 01 00 00       	jmp    f0104a1f <_alltraps>
f01048e3:	90                   	nop

f01048e4 <handler2>:
TN(2)
f01048e4:	6a 00                	push   $0x0
f01048e6:	6a 02                	push   $0x2
f01048e8:	e9 32 01 00 00       	jmp    f0104a1f <_alltraps>
f01048ed:	90                   	nop

f01048ee <handler3>:
TN(3)
f01048ee:	6a 00                	push   $0x0
f01048f0:	6a 03                	push   $0x3
f01048f2:	e9 28 01 00 00       	jmp    f0104a1f <_alltraps>
f01048f7:	90                   	nop

f01048f8 <handler4>:
TN(4)
f01048f8:	6a 00                	push   $0x0
f01048fa:	6a 04                	push   $0x4
f01048fc:	e9 1e 01 00 00       	jmp    f0104a1f <_alltraps>
f0104901:	90                   	nop

f0104902 <handler5>:
TN(5)
f0104902:	6a 00                	push   $0x0
f0104904:	6a 05                	push   $0x5
f0104906:	e9 14 01 00 00       	jmp    f0104a1f <_alltraps>
f010490b:	90                   	nop

f010490c <handler6>:
TN(6)
f010490c:	6a 00                	push   $0x0
f010490e:	6a 06                	push   $0x6
f0104910:	e9 0a 01 00 00       	jmp    f0104a1f <_alltraps>
f0104915:	90                   	nop

f0104916 <handler7>:
TN(7)
f0104916:	6a 00                	push   $0x0
f0104918:	6a 07                	push   $0x7
f010491a:	e9 00 01 00 00       	jmp    f0104a1f <_alltraps>
f010491f:	90                   	nop

f0104920 <handler8>:
TY(8)
f0104920:	6a 08                	push   $0x8
f0104922:	e9 f8 00 00 00       	jmp    f0104a1f <_alltraps>
f0104927:	90                   	nop

f0104928 <handler10>:
// TN(9) /* reserved */
TY(10)
f0104928:	6a 0a                	push   $0xa
f010492a:	e9 f0 00 00 00       	jmp    f0104a1f <_alltraps>
f010492f:	90                   	nop

f0104930 <handler11>:
TY(11)
f0104930:	6a 0b                	push   $0xb
f0104932:	e9 e8 00 00 00       	jmp    f0104a1f <_alltraps>
f0104937:	90                   	nop

f0104938 <handler12>:
TY(12)
f0104938:	6a 0c                	push   $0xc
f010493a:	e9 e0 00 00 00       	jmp    f0104a1f <_alltraps>
f010493f:	90                   	nop

f0104940 <handler13>:
TY(13)
f0104940:	6a 0d                	push   $0xd
f0104942:	e9 d8 00 00 00       	jmp    f0104a1f <_alltraps>
f0104947:	90                   	nop

f0104948 <handler14>:
TY(14)
f0104948:	6a 0e                	push   $0xe
f010494a:	e9 d0 00 00 00       	jmp    f0104a1f <_alltraps>
f010494f:	90                   	nop

f0104950 <handler16>:
// TN(15) /* reserved */
TN(16)
f0104950:	6a 00                	push   $0x0
f0104952:	6a 10                	push   $0x10
f0104954:	e9 c6 00 00 00       	jmp    f0104a1f <_alltraps>
f0104959:	90                   	nop

f010495a <handler17>:
TY(17)
f010495a:	6a 11                	push   $0x11
f010495c:	e9 be 00 00 00       	jmp    f0104a1f <_alltraps>
f0104961:	90                   	nop

f0104962 <handler18>:
TN(18)
f0104962:	6a 00                	push   $0x0
f0104964:	6a 12                	push   $0x12
f0104966:	e9 b4 00 00 00       	jmp    f0104a1f <_alltraps>
f010496b:	90                   	nop

f010496c <handler19>:
TN(19)
f010496c:	6a 00                	push   $0x0
f010496e:	6a 13                	push   $0x13
f0104970:	e9 aa 00 00 00       	jmp    f0104a1f <_alltraps>
f0104975:	90                   	nop

f0104976 <handler32>:

/* 16 IRQs */
TN(32)
f0104976:	6a 00                	push   $0x0
f0104978:	6a 20                	push   $0x20
f010497a:	e9 a0 00 00 00       	jmp    f0104a1f <_alltraps>
f010497f:	90                   	nop

f0104980 <handler33>:
TN(33)
f0104980:	6a 00                	push   $0x0
f0104982:	6a 21                	push   $0x21
f0104984:	e9 96 00 00 00       	jmp    f0104a1f <_alltraps>
f0104989:	90                   	nop

f010498a <handler34>:
TN(34)
f010498a:	6a 00                	push   $0x0
f010498c:	6a 22                	push   $0x22
f010498e:	e9 8c 00 00 00       	jmp    f0104a1f <_alltraps>
f0104993:	90                   	nop

f0104994 <handler35>:
TN(35)
f0104994:	6a 00                	push   $0x0
f0104996:	6a 23                	push   $0x23
f0104998:	e9 82 00 00 00       	jmp    f0104a1f <_alltraps>
f010499d:	90                   	nop

f010499e <handler36>:
TN(36)
f010499e:	6a 00                	push   $0x0
f01049a0:	6a 24                	push   $0x24
f01049a2:	e9 78 00 00 00       	jmp    f0104a1f <_alltraps>
f01049a7:	90                   	nop

f01049a8 <handler37>:
TN(37)
f01049a8:	6a 00                	push   $0x0
f01049aa:	6a 25                	push   $0x25
f01049ac:	e9 6e 00 00 00       	jmp    f0104a1f <_alltraps>
f01049b1:	90                   	nop

f01049b2 <handler38>:
TN(38)
f01049b2:	6a 00                	push   $0x0
f01049b4:	6a 26                	push   $0x26
f01049b6:	e9 64 00 00 00       	jmp    f0104a1f <_alltraps>
f01049bb:	90                   	nop

f01049bc <handler39>:
TN(39)
f01049bc:	6a 00                	push   $0x0
f01049be:	6a 27                	push   $0x27
f01049c0:	e9 5a 00 00 00       	jmp    f0104a1f <_alltraps>
f01049c5:	90                   	nop

f01049c6 <handler40>:
TN(40)
f01049c6:	6a 00                	push   $0x0
f01049c8:	6a 28                	push   $0x28
f01049ca:	e9 50 00 00 00       	jmp    f0104a1f <_alltraps>
f01049cf:	90                   	nop

f01049d0 <handler41>:
TN(41)
f01049d0:	6a 00                	push   $0x0
f01049d2:	6a 29                	push   $0x29
f01049d4:	e9 46 00 00 00       	jmp    f0104a1f <_alltraps>
f01049d9:	90                   	nop

f01049da <handler42>:
TN(42)
f01049da:	6a 00                	push   $0x0
f01049dc:	6a 2a                	push   $0x2a
f01049de:	e9 3c 00 00 00       	jmp    f0104a1f <_alltraps>
f01049e3:	90                   	nop

f01049e4 <handler43>:
TN(43)
f01049e4:	6a 00                	push   $0x0
f01049e6:	6a 2b                	push   $0x2b
f01049e8:	e9 32 00 00 00       	jmp    f0104a1f <_alltraps>
f01049ed:	90                   	nop

f01049ee <handler44>:
TN(44)
f01049ee:	6a 00                	push   $0x0
f01049f0:	6a 2c                	push   $0x2c
f01049f2:	e9 28 00 00 00       	jmp    f0104a1f <_alltraps>
f01049f7:	90                   	nop

f01049f8 <handler45>:
TN(45)
f01049f8:	6a 00                	push   $0x0
f01049fa:	6a 2d                	push   $0x2d
f01049fc:	e9 1e 00 00 00       	jmp    f0104a1f <_alltraps>
f0104a01:	90                   	nop

f0104a02 <handler46>:
TN(46)
f0104a02:	6a 00                	push   $0x0
f0104a04:	6a 2e                	push   $0x2e
f0104a06:	e9 14 00 00 00       	jmp    f0104a1f <_alltraps>
f0104a0b:	90                   	nop

f0104a0c <handler47>:
TN(47)
f0104a0c:	6a 00                	push   $0x0
f0104a0e:	6a 2f                	push   $0x2f
f0104a10:	e9 0a 00 00 00       	jmp    f0104a1f <_alltraps>
f0104a15:	90                   	nop

f0104a16 <handler48>:
	
TN(48)
f0104a16:	6a 00                	push   $0x0
f0104a18:	6a 30                	push   $0x30
f0104a1a:	e9 00 00 00 00       	jmp    f0104a1f <_alltraps>

f0104a1f <_alltraps>:
 */

.global _alltraps
_alltraps:
	// Build struct Trapframe
	pushl %ds
f0104a1f:	1e                   	push   %ds
	pushl %es
f0104a20:	06                   	push   %es
	pushal
f0104a21:	60                   	pusha  

	movl $GD_KD, %eax
f0104a22:	b8 10 00 00 00       	mov    $0x10,%eax
	movw %ax, %ds
f0104a27:	8e d8                	mov    %eax,%ds
	movw %ax, %es
f0104a29:	8e c0                	mov    %eax,%es

	pushl %esp /* struct Trapframe * as argument */
f0104a2b:	54                   	push   %esp
	call trap
f0104a2c:	e8 48 fc ff ff       	call   f0104679 <trap>

f0104a31 <sched_halt>:
// Halt this CPU when there is nothing to do. Wait until the
// timer interrupt wakes it up. This function never returns.
//
void
sched_halt(void)
{
f0104a31:	55                   	push   %ebp
f0104a32:	89 e5                	mov    %esp,%ebp
f0104a34:	83 ec 08             	sub    $0x8,%esp
f0104a37:	a1 44 32 23 f0       	mov    0xf0233244,%eax
f0104a3c:	8d 50 54             	lea    0x54(%eax),%edx
	int i;

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
f0104a3f:	b9 00 00 00 00       	mov    $0x0,%ecx
		if ((envs[i].env_status == ENV_RUNNABLE ||
f0104a44:	8b 02                	mov    (%edx),%eax
f0104a46:	48                   	dec    %eax
f0104a47:	83 f8 02             	cmp    $0x2,%eax
f0104a4a:	76 0e                	jbe    f0104a5a <sched_halt+0x29>
{
	int i;

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
f0104a4c:	41                   	inc    %ecx
f0104a4d:	83 c2 7c             	add    $0x7c,%edx
f0104a50:	81 f9 00 04 00 00    	cmp    $0x400,%ecx
f0104a56:	75 ec                	jne    f0104a44 <sched_halt+0x13>
f0104a58:	eb 08                	jmp    f0104a62 <sched_halt+0x31>
		if ((envs[i].env_status == ENV_RUNNABLE ||
		     envs[i].env_status == ENV_RUNNING ||
		     envs[i].env_status == ENV_DYING))
			break;
	}
	if (i == NENV) {
f0104a5a:	81 f9 00 04 00 00    	cmp    $0x400,%ecx
f0104a60:	75 1f                	jne    f0104a81 <sched_halt+0x50>
		cprintf("No runnable environments in the system!\n");
f0104a62:	83 ec 0c             	sub    $0xc,%esp
f0104a65:	68 d0 8d 10 f0       	push   $0xf0108dd0
f0104a6a:	e8 a2 f1 ff ff       	call   f0103c11 <cprintf>
f0104a6f:	83 c4 10             	add    $0x10,%esp
		while (1)
			monitor(NULL);
f0104a72:	83 ec 0c             	sub    $0xc,%esp
f0104a75:	6a 00                	push   $0x0
f0104a77:	e8 f5 bf ff ff       	call   f0100a71 <monitor>
f0104a7c:	83 c4 10             	add    $0x10,%esp
f0104a7f:	eb f1                	jmp    f0104a72 <sched_halt+0x41>
	}

	// Mark that no environment is running on this CPU
	curenv = NULL;
f0104a81:	e8 a8 1c 00 00       	call   f010672e <cpunum>
f0104a86:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104a89:	01 c2                	add    %eax,%edx
f0104a8b:	01 d2                	add    %edx,%edx
f0104a8d:	01 c2                	add    %eax,%edx
f0104a8f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104a92:	c7 04 85 08 50 23 f0 	movl   $0x0,-0xfdcaff8(,%eax,4)
f0104a99:	00 00 00 00 
	lcr3(PADDR(kern_pgdir));
f0104a9d:	a1 28 44 23 f0       	mov    0xf0234428,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0104aa2:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0104aa7:	77 12                	ja     f0104abb <sched_halt+0x8a>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0104aa9:	50                   	push   %eax
f0104aaa:	68 0c 75 10 f0       	push   $0xf010750c
f0104aaf:	6a 55                	push   $0x55
f0104ab1:	68 f9 8d 10 f0       	push   $0xf0108df9
f0104ab6:	e8 91 b5 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0104abb:	05 00 00 00 10       	add    $0x10000000,%eax
f0104ac0:	0f 22 d8             	mov    %eax,%cr3

	// Mark that this CPU is in the HALT state, so that when
	// timer interupts come in, we know we should re-acquire the
	// big kernel lock
	xchg(&thiscpu->cpu_status, CPU_HALTED);
f0104ac3:	e8 66 1c 00 00       	call   f010672e <cpunum>
f0104ac8:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104acb:	01 c2                	add    %eax,%edx
f0104acd:	01 d2                	add    %edx,%edx
f0104acf:	01 c2                	add    %eax,%edx
f0104ad1:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104ad4:	8d 14 85 00 50 23 f0 	lea    -0xfdcb000(,%eax,4),%edx
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0104adb:	b8 02 00 00 00       	mov    $0x2,%eax
f0104ae0:	f0 87 42 04          	lock xchg %eax,0x4(%edx)
}

static inline void
unlock_kernel(void)
{
	spin_unlock(&kernel_lock);
f0104ae4:	83 ec 0c             	sub    $0xc,%esp
f0104ae7:	68 c0 53 12 f0       	push   $0xf01253c0
f0104aec:	e8 a8 20 00 00       	call   f0106b99 <spin_unlock>

	// Normally we wouldn't need to do this, but QEMU only runs
	// one CPU at a time and has a long time-slice.  Without the
	// pause, this CPU is likely to reacquire the lock before
	// another CPU has even been given a chance to acquire it.
	asm volatile("pause");
f0104af1:	f3 90                	pause  
		"pushl $0\n"
		"sti\n"
		"1:\n"
		"hlt\n"
		"jmp 1b\n"
	: : "a" (thiscpu->cpu_ts.ts_esp0));
f0104af3:	e8 36 1c 00 00       	call   f010672e <cpunum>
f0104af8:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104afb:	01 c2                	add    %eax,%edx
f0104afd:	01 d2                	add    %edx,%edx
f0104aff:	01 c2                	add    %eax,%edx
f0104b01:	8d 04 90             	lea    (%eax,%edx,4),%eax

	// Release the big kernel lock as if we were "leaving" the kernel
	unlock_kernel();

	// Reset stack pointer, enable interrupts and then halt.
	asm volatile (
f0104b04:	8b 04 85 10 50 23 f0 	mov    -0xfdcaff0(,%eax,4),%eax
f0104b0b:	bd 00 00 00 00       	mov    $0x0,%ebp
f0104b10:	89 c4                	mov    %eax,%esp
f0104b12:	6a 00                	push   $0x0
f0104b14:	6a 00                	push   $0x0
f0104b16:	fb                   	sti    
f0104b17:	f4                   	hlt    
f0104b18:	eb fd                	jmp    f0104b17 <sched_halt+0xe6>
		"sti\n"
		"1:\n"
		"hlt\n"
		"jmp 1b\n"
	: : "a" (thiscpu->cpu_ts.ts_esp0));
}
f0104b1a:	83 c4 10             	add    $0x10,%esp
f0104b1d:	c9                   	leave  
f0104b1e:	c3                   	ret    

f0104b1f <sched_yield>:
void sched_halt(void);

// Choose a user environment to run and run it.
void
sched_yield(void)
{
f0104b1f:	55                   	push   %ebp
f0104b20:	89 e5                	mov    %esp,%ebp
f0104b22:	57                   	push   %edi
f0104b23:	56                   	push   %esi
f0104b24:	53                   	push   %ebx
f0104b25:	83 ec 0c             	sub    $0xc,%esp
	// no runnable environments, simply drop through to the code
	// below to halt the cpu.

	// LAB 4: Your code here.
	// Find the environment this CPU was last running.
    struct Env *curr = thiscpu->cpu_env;
f0104b28:	e8 01 1c 00 00       	call   f010672e <cpunum>
f0104b2d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104b30:	01 c2                	add    %eax,%edx
f0104b32:	01 d2                	add    %edx,%edx
f0104b34:	01 c2                	add    %eax,%edx
f0104b36:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104b39:	8b 34 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%esi
    	indx = ENVX(curr->env_id);
    }else{
    	indx = 0;
    }*/

    indx = curr ? ENVX(curr->env_id) % NENV : 0; //if there is no environment running, starting from index 0
f0104b40:	85 f6                	test   %esi,%esi
f0104b42:	74 0b                	je     f0104b4f <sched_yield+0x30>
f0104b44:	8b 7e 48             	mov    0x48(%esi),%edi
f0104b47:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
f0104b4d:	eb 05                	jmp    f0104b54 <sched_yield+0x35>
f0104b4f:	bf 00 00 00 00       	mov    $0x0,%edi
	//cprintf("indx is %d\n", indx);
	for (i = 0; i < NENV; i++) {
        indx = (indx + 1) % NENV;
        
        if (envs[indx].env_status == ENV_RUNNABLE){
f0104b54:	8b 1d 44 32 23 f0    	mov    0xf0233244,%ebx
f0104b5a:	b9 00 04 00 00       	mov    $0x400,%ecx
    }*/

    indx = curr ? ENVX(curr->env_id) % NENV : 0; //if there is no environment running, starting from index 0
	//cprintf("indx is %d\n", indx);
	for (i = 0; i < NENV; i++) {
        indx = (indx + 1) % NENV;
f0104b5f:	8d 47 01             	lea    0x1(%edi),%eax
f0104b62:	25 ff 03 00 80       	and    $0x800003ff,%eax
f0104b67:	79 07                	jns    f0104b70 <sched_yield+0x51>
f0104b69:	48                   	dec    %eax
f0104b6a:	0d 00 fc ff ff       	or     $0xfffffc00,%eax
f0104b6f:	40                   	inc    %eax
f0104b70:	89 c7                	mov    %eax,%edi
        
        if (envs[indx].env_status == ENV_RUNNABLE){
f0104b72:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104b79:	c1 e0 07             	shl    $0x7,%eax
f0104b7c:	29 d0                	sub    %edx,%eax
f0104b7e:	8d 14 03             	lea    (%ebx,%eax,1),%edx
f0104b81:	83 7a 54 02          	cmpl   $0x2,0x54(%edx)
f0104b85:	75 09                	jne    f0104b90 <sched_yield+0x71>
        	//cprintf("About to run env: %d\n", envs[indx].env_id);
            env_run(&envs[indx]);
f0104b87:	83 ec 0c             	sub    $0xc,%esp
f0104b8a:	52                   	push   %edx
f0104b8b:	e8 b9 ee ff ff       	call   f0103a49 <env_run>
    	indx = 0;
    }*/

    indx = curr ? ENVX(curr->env_id) % NENV : 0; //if there is no environment running, starting from index 0
	//cprintf("indx is %d\n", indx);
	for (i = 0; i < NENV; i++) {
f0104b90:	49                   	dec    %ecx
f0104b91:	75 cc                	jne    f0104b5f <sched_yield+0x40>
            env_run(&envs[indx]);

        }
	}

    if (curr && curr->env_status == ENV_RUNNING) {
f0104b93:	85 f6                	test   %esi,%esi
f0104b95:	74 0f                	je     f0104ba6 <sched_yield+0x87>
f0104b97:	83 7e 54 03          	cmpl   $0x3,0x54(%esi)
f0104b9b:	75 09                	jne    f0104ba6 <sched_yield+0x87>
        // If not found and current environment is running, then continue running.
        env_run(curr);
f0104b9d:	83 ec 0c             	sub    $0xc,%esp
f0104ba0:	56                   	push   %esi
f0104ba1:	e8 a3 ee ff ff       	call   f0103a49 <env_run>
    }
	// sched_halt never returns
	sched_halt();
f0104ba6:	e8 86 fe ff ff       	call   f0104a31 <sched_halt>
}
f0104bab:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104bae:	5b                   	pop    %ebx
f0104baf:	5e                   	pop    %esi
f0104bb0:	5f                   	pop    %edi
f0104bb1:	5d                   	pop    %ebp
f0104bb2:	c3                   	ret    

f0104bb3 <syscall>:
}

// Dispatches to the correct kernel function, passing the arguments.
int32_t
syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
f0104bb3:	55                   	push   %ebp
f0104bb4:	89 e5                	mov    %esp,%ebp
f0104bb6:	57                   	push   %edi
f0104bb7:	56                   	push   %esi
f0104bb8:	53                   	push   %ebx
f0104bb9:	83 ec 1c             	sub    $0x1c,%esp
f0104bbc:	8b 45 08             	mov    0x8(%ebp),%eax
	// Return any appropriate return value.
	// LAB 3: Your code here.

	//panic("syscall not implemented");

	switch (syscallno) {
f0104bbf:	83 f8 0e             	cmp    $0xe,%eax
f0104bc2:	0f 87 91 07 00 00    	ja     f0105359 <syscall+0x7a6>
f0104bc8:	ff 24 85 d4 90 10 f0 	jmp    *-0xfef6f2c(,%eax,4)
	// Check that the user has permission to read memory [s, s+len).
	// Destroy the environment if not.

	// LAB 3: Your code here.
	//user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
	user_mem_assert(curenv, (void*)s, len, 0);
f0104bcf:	e8 5a 1b 00 00       	call   f010672e <cpunum>
f0104bd4:	6a 00                	push   $0x0
f0104bd6:	ff 75 10             	pushl  0x10(%ebp)
f0104bd9:	ff 75 0c             	pushl  0xc(%ebp)
f0104bdc:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104bdf:	01 c2                	add    %eax,%edx
f0104be1:	01 d2                	add    %edx,%edx
f0104be3:	01 c2                	add    %eax,%edx
f0104be5:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104be8:	ff 34 85 08 50 23 f0 	pushl  -0xfdcaff8(,%eax,4)
f0104bef:	e8 30 e6 ff ff       	call   f0103224 <user_mem_assert>
	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
f0104bf4:	83 c4 0c             	add    $0xc,%esp
f0104bf7:	ff 75 0c             	pushl  0xc(%ebp)
f0104bfa:	ff 75 10             	pushl  0x10(%ebp)
f0104bfd:	68 06 8e 10 f0       	push   $0xf0108e06
f0104c02:	e8 0a f0 ff ff       	call   f0103c11 <cprintf>
f0104c07:	83 c4 10             	add    $0x10,%esp
	//panic("syscall not implemented");

	switch (syscallno) {
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;
f0104c0a:	bb 00 00 00 00       	mov    $0x0,%ebx
f0104c0f:	e9 5c 07 00 00       	jmp    f0105370 <syscall+0x7bd>
// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
static int
sys_cgetc(void)
{
	return cons_getc();
f0104c14:	e8 a5 ba ff ff       	call   f01006be <cons_getc>
f0104c19:	89 c3                	mov    %eax,%ebx
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;

		case SYS_cgetc:
			return sys_cgetc(); //Read a character from the system console without blocking.
f0104c1b:	e9 50 07 00 00       	jmp    f0105370 <syscall+0x7bd>
sys_env_destroy(envid_t envid_In)
{
	int r;
    envid_t envid = envid_In;
	struct Env *e;
    cprintf("envid in sys_env_destroy() is:%d\n", envid);
f0104c20:	83 ec 08             	sub    $0x8,%esp
f0104c23:	ff 75 0c             	pushl  0xc(%ebp)
f0104c26:	68 e4 8f 10 f0       	push   $0xf0108fe4
f0104c2b:	e8 e1 ef ff ff       	call   f0103c11 <cprintf>
	if ((r = envid2env(envid, &e, 1)) < 0)
f0104c30:	83 c4 0c             	add    $0xc,%esp
f0104c33:	6a 01                	push   $0x1
f0104c35:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104c38:	50                   	push   %eax
f0104c39:	ff 75 0c             	pushl  0xc(%ebp)
f0104c3c:	e8 d1 e6 ff ff       	call   f0103312 <envid2env>
f0104c41:	83 c4 10             	add    $0x10,%esp
f0104c44:	85 c0                	test   %eax,%eax
f0104c46:	78 18                	js     f0104c60 <syscall+0xad>
		return r;
	env_destroy(e);
f0104c48:	83 ec 0c             	sub    $0xc,%esp
f0104c4b:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104c4e:	e8 1c ed ff ff       	call   f010396f <env_destroy>
f0104c53:	83 c4 10             	add    $0x10,%esp
	return 0;
f0104c56:	bb 00 00 00 00       	mov    $0x0,%ebx
f0104c5b:	e9 10 07 00 00       	jmp    f0105370 <syscall+0x7bd>
	int r;
    envid_t envid = envid_In;
	struct Env *e;
    cprintf("envid in sys_env_destroy() is:%d\n", envid);
	if ((r = envid2env(envid, &e, 1)) < 0)
		return r;
f0104c60:	89 c3                	mov    %eax,%ebx
		case SYS_cgetc:
			return sys_cgetc(); //Read a character from the system console without blocking.

		case SYS_env_destroy:
			//return sys_env_destroy(a2); //Destroy a given environment (possibly the currently running environment).
            return sys_env_destroy(a1);
f0104c62:	e9 09 07 00 00       	jmp    f0105370 <syscall+0x7bd>

// Returns the current environment's envid.
static envid_t
sys_getenvid(void)
{
	return curenv->env_id;
f0104c67:	e8 c2 1a 00 00       	call   f010672e <cpunum>
f0104c6c:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104c6f:	01 c2                	add    %eax,%edx
f0104c71:	01 d2                	add    %edx,%edx
f0104c73:	01 c2                	add    %eax,%edx
f0104c75:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104c78:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0104c7f:	8b 58 48             	mov    0x48(%eax),%ebx

		case SYS_env_destroy:
			//return sys_env_destroy(a2); //Destroy a given environment (possibly the currently running environment).
            return sys_env_destroy(a1);
		case SYS_getenvid:
			return sys_getenvid(); //get the current environment's envid.
f0104c82:	e9 e9 06 00 00       	jmp    f0105370 <syscall+0x7bd>
// Deschedule current environment and pick a different one to run.
static void
sys_yield(void)
{   
    //cprintf("timer interrupted!\n");
	sched_yield();
f0104c87:	e8 93 fe ff ff       	call   f0104b1f <sched_yield>
	// LAB 4: Your code here.
	//cprintf("sys_exofork() starts\n\n");
	struct Env *env;
    //cprintf("In sys_exofork(), the envid to env_alloc() is: %d\n\n\n", thiscpu->cpu_env->env_id);
    //env_alloc(struct Env **newenv_store, envid_t parent_id)
    int result = env_alloc(&env, thiscpu->cpu_env->env_id);
f0104c8c:	e8 9d 1a 00 00       	call   f010672e <cpunum>
f0104c91:	83 ec 08             	sub    $0x8,%esp
f0104c94:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104c97:	01 c2                	add    %eax,%edx
f0104c99:	01 d2                	add    %edx,%edx
f0104c9b:	01 c2                	add    %eax,%edx
f0104c9d:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104ca0:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0104ca7:	ff 70 48             	pushl  0x48(%eax)
f0104caa:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104cad:	50                   	push   %eax
f0104cae:	e8 94 e7 ff ff       	call   f0103447 <env_alloc>
    //cprintf("In sys_exofork(), result from env_alloc() is: %d\n\n\n", result);
    if (result) {
f0104cb3:	83 c4 10             	add    $0x10,%esp
f0104cb6:	85 c0                	test   %eax,%eax
f0104cb8:	75 3d                	jne    f0104cf7 <syscall+0x144>
        //cprintf("sys_exofork: no free env");
        return -E_NO_FREE_ENV;
    }

    env->env_status = ENV_NOT_RUNNABLE;
f0104cba:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f0104cbd:	c7 43 54 04 00 00 00 	movl   $0x4,0x54(%ebx)
    env->env_tf = thiscpu->cpu_env->env_tf;
f0104cc4:	e8 65 1a 00 00       	call   f010672e <cpunum>
f0104cc9:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104ccc:	01 c2                	add    %eax,%edx
f0104cce:	01 d2                	add    %edx,%edx
f0104cd0:	01 c2                	add    %eax,%edx
f0104cd2:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0104cd5:	8b 34 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%esi
f0104cdc:	b9 11 00 00 00       	mov    $0x11,%ecx
f0104ce1:	89 df                	mov    %ebx,%edi
f0104ce3:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
    
    // Make the new environment return zero.
    env->env_tf.tf_regs.reg_eax = 0;
f0104ce5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104ce8:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

    return env->env_id;
f0104cef:	8b 58 48             	mov    0x48(%eax),%ebx
f0104cf2:	e9 79 06 00 00       	jmp    f0105370 <syscall+0x7bd>
    //env_alloc(struct Env **newenv_store, envid_t parent_id)
    int result = env_alloc(&env, thiscpu->cpu_env->env_id);
    //cprintf("In sys_exofork(), result from env_alloc() is: %d\n\n\n", result);
    if (result) {
        //cprintf("sys_exofork: no free env");
        return -E_NO_FREE_ENV;
f0104cf7:	bb fb ff ff ff       	mov    $0xfffffffb,%ebx

		case SYS_yield:
        	sys_yield();
        	return 0;
        case SYS_exofork:
        	return sys_exofork();
f0104cfc:	e9 6f 06 00 00       	jmp    f0105370 <syscall+0x7bd>
	// check whether the current environment has permission to set
	// envid's status.

	// LAB 4: Your code here.
    // check status.
    if (status != ENV_RUNNABLE &&
f0104d01:	83 7d 10 02          	cmpl   $0x2,0x10(%ebp)
f0104d05:	74 20                	je     f0104d27 <syscall+0x174>
f0104d07:	83 7d 10 04          	cmpl   $0x4,0x10(%ebp)
f0104d0b:	74 1a                	je     f0104d27 <syscall+0x174>
        status != ENV_NOT_RUNNABLE) {
        cprintf("sys_env_set_status: bad status");
f0104d0d:	83 ec 0c             	sub    $0xc,%esp
f0104d10:	68 08 90 10 f0       	push   $0xf0109008
f0104d15:	e8 f7 ee ff ff       	call   f0103c11 <cprintf>
f0104d1a:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104d1d:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104d22:	e9 49 06 00 00       	jmp    f0105370 <syscall+0x7bd>
    }
    
    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f0104d27:	83 ec 04             	sub    $0x4,%esp
f0104d2a:	6a 01                	push   $0x1
f0104d2c:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104d2f:	50                   	push   %eax
f0104d30:	ff 75 0c             	pushl  0xc(%ebp)
f0104d33:	e8 da e5 ff ff       	call   f0103312 <envid2env>
f0104d38:	89 c3                	mov    %eax,%ebx
f0104d3a:	83 c4 10             	add    $0x10,%esp
f0104d3d:	85 c0                	test   %eax,%eax
f0104d3f:	74 1a                	je     f0104d5b <syscall+0x1a8>
        cprintf("sys_env_set_status: bad env");
f0104d41:	83 ec 0c             	sub    $0xc,%esp
f0104d44:	68 0b 8e 10 f0       	push   $0xf0108e0b
f0104d49:	e8 c3 ee ff ff       	call   f0103c11 <cprintf>
f0104d4e:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104d51:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104d56:	e9 15 06 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // change status.
    env->env_status = status;
f0104d5b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104d5e:	8b 4d 10             	mov    0x10(%ebp),%ecx
f0104d61:	89 48 54             	mov    %ecx,0x54(%eax)
        	sys_yield();
        	return 0;
        case SYS_exofork:
        	return sys_exofork();
    	case SYS_env_set_status:
        	return sys_env_set_status(a1, a2);
f0104d64:	e9 07 06 00 00       	jmp    f0105370 <syscall+0x7bd>

	// LAB 4: Your code here.
#define PGMASK 0xfff

    // check address.
    if ((uint32_t)va >= UTOP || (uint32_t)va & PGMASK) {
f0104d69:	81 7d 10 ff ff bf ee 	cmpl   $0xeebfffff,0x10(%ebp)
f0104d70:	77 09                	ja     f0104d7b <syscall+0x1c8>
f0104d72:	f7 45 10 ff 0f 00 00 	testl  $0xfff,0x10(%ebp)
f0104d79:	74 1a                	je     f0104d95 <syscall+0x1e2>
        cprintf("sys_page_alloc: bad va");
f0104d7b:	83 ec 0c             	sub    $0xc,%esp
f0104d7e:	68 27 8e 10 f0       	push   $0xf0108e27
f0104d83:	e8 89 ee ff ff       	call   f0103c11 <cprintf>
f0104d88:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104d8b:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104d90:	e9 db 05 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // check permission.
    if (!(perm & PTE_U) || !(perm & PTE_P) || (perm & ~PTE_SYSCALL)) {
f0104d95:	8b 45 14             	mov    0x14(%ebp),%eax
f0104d98:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f0104d9d:	83 f8 05             	cmp    $0x5,%eax
f0104da0:	74 1a                	je     f0104dbc <syscall+0x209>
        cprintf("sys_page_alloc: bad perm");
f0104da2:	83 ec 0c             	sub    $0xc,%esp
f0104da5:	68 3e 8e 10 f0       	push   $0xf0108e3e
f0104daa:	e8 62 ee ff ff       	call   f0103c11 <cprintf>
f0104daf:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104db2:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104db7:	e9 b4 05 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1) < 0) {
f0104dbc:	83 ec 04             	sub    $0x4,%esp
f0104dbf:	6a 01                	push   $0x1
f0104dc1:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104dc4:	50                   	push   %eax
f0104dc5:	ff 75 0c             	pushl  0xc(%ebp)
f0104dc8:	e8 45 e5 ff ff       	call   f0103312 <envid2env>
f0104dcd:	83 c4 10             	add    $0x10,%esp
f0104dd0:	85 c0                	test   %eax,%eax
f0104dd2:	79 1a                	jns    f0104dee <syscall+0x23b>
        cprintf("sys_page_alloc: bad env");
f0104dd4:	83 ec 0c             	sub    $0xc,%esp
f0104dd7:	68 57 8e 10 f0       	push   $0xf0108e57
f0104ddc:	e8 30 ee ff ff       	call   f0103c11 <cprintf>
f0104de1:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104de4:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104de9:	e9 82 05 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    struct PageInfo *page;
    page = page_alloc(ALLOC_ZERO);
f0104dee:	83 ec 0c             	sub    $0xc,%esp
f0104df1:	6a 01                	push   $0x1
f0104df3:	e8 2a c4 ff ff       	call   f0101222 <page_alloc>
f0104df8:	89 c6                	mov    %eax,%esi
    if (!page) {
f0104dfa:	83 c4 10             	add    $0x10,%esp
f0104dfd:	85 c0                	test   %eax,%eax
f0104dff:	75 1a                	jne    f0104e1b <syscall+0x268>
        cprintf("sys_page_alloc: no mem");
f0104e01:	83 ec 0c             	sub    $0xc,%esp
f0104e04:	68 6f 8e 10 f0       	push   $0xf0108e6f
f0104e09:	e8 03 ee ff ff       	call   f0103c11 <cprintf>
f0104e0e:	83 c4 10             	add    $0x10,%esp
        return -E_NO_MEM;
f0104e11:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
f0104e16:	e9 55 05 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // Side effect is within page_insert.
    if (page_insert(env->env_pgdir, page, va, perm)) {
f0104e1b:	ff 75 14             	pushl  0x14(%ebp)
f0104e1e:	ff 75 10             	pushl  0x10(%ebp)
f0104e21:	50                   	push   %eax
f0104e22:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104e25:	ff 70 60             	pushl  0x60(%eax)
f0104e28:	e8 71 c8 ff ff       	call   f010169e <page_insert>
f0104e2d:	89 c3                	mov    %eax,%ebx
f0104e2f:	83 c4 10             	add    $0x10,%esp
f0104e32:	85 c0                	test   %eax,%eax
f0104e34:	0f 84 36 05 00 00    	je     f0105370 <syscall+0x7bd>
        cprintf("sys_page_alloc: no mem");
f0104e3a:	83 ec 0c             	sub    $0xc,%esp
f0104e3d:	68 6f 8e 10 f0       	push   $0xf0108e6f
f0104e42:	e8 ca ed ff ff       	call   f0103c11 <cprintf>
        page_free(page);
f0104e47:	89 34 24             	mov    %esi,(%esp)
f0104e4a:	e8 49 c4 ff ff       	call   f0101298 <page_free>
f0104e4f:	83 c4 10             	add    $0x10,%esp
        return -E_NO_MEM;
f0104e52:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
        case SYS_exofork:
        	return sys_exofork();
    	case SYS_env_set_status:
        	return sys_env_set_status(a1, a2);
    	case SYS_page_alloc:
        	return sys_page_alloc(a1, (void *)a2, a3);
f0104e57:	e9 14 05 00 00       	jmp    f0105370 <syscall+0x7bd>
	//   check the current permissions on the page.

	// LAB 4: Your code here.
    
    // check address.
    if ((uint32_t)srcva >= UTOP ||
f0104e5c:	81 7d 10 ff ff bf ee 	cmpl   $0xeebfffff,0x10(%ebp)
f0104e63:	77 1b                	ja     f0104e80 <syscall+0x2cd>
f0104e65:	f7 45 10 ff 0f 00 00 	testl  $0xfff,0x10(%ebp)
f0104e6c:	75 12                	jne    f0104e80 <syscall+0x2cd>
        ((uint32_t)srcva & PGMASK) ||
f0104e6e:	81 7d 18 ff ff bf ee 	cmpl   $0xeebfffff,0x18(%ebp)
f0104e75:	77 09                	ja     f0104e80 <syscall+0x2cd>
        (uint32_t)dstva >= UTOP ||
f0104e77:	f7 45 18 ff 0f 00 00 	testl  $0xfff,0x18(%ebp)
f0104e7e:	74 1a                	je     f0104e9a <syscall+0x2e7>
        ((uint32_t)dstva & PGMASK)) {
        cprintf("sys_page_map: bad va");
f0104e80:	83 ec 0c             	sub    $0xc,%esp
f0104e83:	68 86 8e 10 f0       	push   $0xf0108e86
f0104e88:	e8 84 ed ff ff       	call   f0103c11 <cprintf>
f0104e8d:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104e90:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104e95:	e9 d6 04 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // check permission.
    if (!(perm & PTE_U) || !(perm & PTE_P) || (perm & ~PTE_SYSCALL)) {
f0104e9a:	8b 45 1c             	mov    0x1c(%ebp),%eax
f0104e9d:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f0104ea2:	83 f8 05             	cmp    $0x5,%eax
f0104ea5:	74 1a                	je     f0104ec1 <syscall+0x30e>
        cprintf("sys_page_map: bad perm");
f0104ea7:	83 ec 0c             	sub    $0xc,%esp
f0104eaa:	68 9b 8e 10 f0       	push   $0xf0108e9b
f0104eaf:	e8 5d ed ff ff       	call   f0103c11 <cprintf>
f0104eb4:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104eb7:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104ebc:	e9 af 04 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // find environments.
    struct Env *srcenv, *dstenv;
    if (envid2env(srcenvid, &srcenv, 1) ||
f0104ec1:	83 ec 04             	sub    $0x4,%esp
f0104ec4:	6a 01                	push   $0x1
f0104ec6:	8d 45 dc             	lea    -0x24(%ebp),%eax
f0104ec9:	50                   	push   %eax
f0104eca:	ff 75 0c             	pushl  0xc(%ebp)
f0104ecd:	e8 40 e4 ff ff       	call   f0103312 <envid2env>
f0104ed2:	83 c4 10             	add    $0x10,%esp
f0104ed5:	85 c0                	test   %eax,%eax
f0104ed7:	75 18                	jne    f0104ef1 <syscall+0x33e>
        envid2env(dstenvid, &dstenv, 1)) {
f0104ed9:	83 ec 04             	sub    $0x4,%esp
f0104edc:	6a 01                	push   $0x1
f0104ede:	8d 45 e0             	lea    -0x20(%ebp),%eax
f0104ee1:	50                   	push   %eax
f0104ee2:	ff 75 14             	pushl  0x14(%ebp)
f0104ee5:	e8 28 e4 ff ff       	call   f0103312 <envid2env>
        return -E_INVAL;
    }

    // find environments.
    struct Env *srcenv, *dstenv;
    if (envid2env(srcenvid, &srcenv, 1) ||
f0104eea:	83 c4 10             	add    $0x10,%esp
f0104eed:	85 c0                	test   %eax,%eax
f0104eef:	74 1a                	je     f0104f0b <syscall+0x358>
        envid2env(dstenvid, &dstenv, 1)) {
        cprintf("sys_page_map: bad env");
f0104ef1:	83 ec 0c             	sub    $0xc,%esp
f0104ef4:	68 b2 8e 10 f0       	push   $0xf0108eb2
f0104ef9:	e8 13 ed ff ff       	call   f0103c11 <cprintf>
f0104efe:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104f01:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104f06:	e9 65 04 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // is source mapped?
    pte_t *srcpte;
    struct PageInfo *page = page_lookup(srcenv->env_pgdir, srcva, &srcpte);
f0104f0b:	83 ec 04             	sub    $0x4,%esp
f0104f0e:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104f11:	50                   	push   %eax
f0104f12:	ff 75 10             	pushl  0x10(%ebp)
f0104f15:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0104f18:	ff 70 60             	pushl  0x60(%eax)
f0104f1b:	e8 b9 c5 ff ff       	call   f01014d9 <page_lookup>
    if (!page) {
f0104f20:	83 c4 10             	add    $0x10,%esp
f0104f23:	85 c0                	test   %eax,%eax
f0104f25:	75 1a                	jne    f0104f41 <syscall+0x38e>
        cprintf("sys_page_map: src not mapped");
f0104f27:	83 ec 0c             	sub    $0xc,%esp
f0104f2a:	68 c8 8e 10 f0       	push   $0xf0108ec8
f0104f2f:	e8 dd ec ff ff       	call   f0103c11 <cprintf>
f0104f34:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104f37:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104f3c:	e9 2f 04 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // is source read-only but dest writable?
    if ((perm & PTE_W) && !(*srcpte & PTE_W)) {
f0104f41:	f6 45 1c 02          	testb  $0x2,0x1c(%ebp)
f0104f45:	74 22                	je     f0104f69 <syscall+0x3b6>
f0104f47:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0104f4a:	f6 02 02             	testb  $0x2,(%edx)
f0104f4d:	75 1a                	jne    f0104f69 <syscall+0x3b6>
        cprintf("sys_page_map: src ro but dst w");
f0104f4f:	83 ec 0c             	sub    $0xc,%esp
f0104f52:	68 28 90 10 f0       	push   $0xf0109028
f0104f57:	e8 b5 ec ff ff       	call   f0103c11 <cprintf>
f0104f5c:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104f5f:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104f64:	e9 07 04 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // try mapping
    if (page_insert(dstenv->env_pgdir, page, dstva, perm)) {
f0104f69:	ff 75 1c             	pushl  0x1c(%ebp)
f0104f6c:	ff 75 18             	pushl  0x18(%ebp)
f0104f6f:	50                   	push   %eax
f0104f70:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0104f73:	ff 70 60             	pushl  0x60(%eax)
f0104f76:	e8 23 c7 ff ff       	call   f010169e <page_insert>
f0104f7b:	89 c3                	mov    %eax,%ebx
f0104f7d:	83 c4 10             	add    $0x10,%esp
f0104f80:	85 c0                	test   %eax,%eax
f0104f82:	0f 84 e8 03 00 00    	je     f0105370 <syscall+0x7bd>
        cprintf("sys_page_map: no mem");
f0104f88:	83 ec 0c             	sub    $0xc,%esp
f0104f8b:	68 e5 8e 10 f0       	push   $0xf0108ee5
f0104f90:	e8 7c ec ff ff       	call   f0103c11 <cprintf>
f0104f95:	83 c4 10             	add    $0x10,%esp
        return -E_NO_MEM;
f0104f98:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
    	case SYS_env_set_status:
        	return sys_env_set_status(a1, a2);
    	case SYS_page_alloc:
        	return sys_page_alloc(a1, (void *)a2, a3);
    	case SYS_page_map:
        	return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
f0104f9d:	e9 ce 03 00 00       	jmp    f0105370 <syscall+0x7bd>
{
	// Hint: This function is a wrapper around page_remove().

	// LAB 4: Your code here.
    // check va.
    if ((uint32_t)va >= UTOP ||
f0104fa2:	81 7d 10 ff ff bf ee 	cmpl   $0xeebfffff,0x10(%ebp)
f0104fa9:	77 09                	ja     f0104fb4 <syscall+0x401>
f0104fab:	f7 45 10 ff 0f 00 00 	testl  $0xfff,0x10(%ebp)
f0104fb2:	74 1a                	je     f0104fce <syscall+0x41b>
        ((uint32_t)va & PGMASK)) {
        cprintf("sys_page_unmap: bad va");
f0104fb4:	83 ec 0c             	sub    $0xc,%esp
f0104fb7:	68 fa 8e 10 f0       	push   $0xf0108efa
f0104fbc:	e8 50 ec ff ff       	call   f0103c11 <cprintf>
f0104fc1:	83 c4 10             	add    $0x10,%esp
        return -E_INVAL;
f0104fc4:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0104fc9:	e9 a2 03 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f0104fce:	83 ec 04             	sub    $0x4,%esp
f0104fd1:	6a 01                	push   $0x1
f0104fd3:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0104fd6:	50                   	push   %eax
f0104fd7:	ff 75 0c             	pushl  0xc(%ebp)
f0104fda:	e8 33 e3 ff ff       	call   f0103312 <envid2env>
f0104fdf:	89 c3                	mov    %eax,%ebx
f0104fe1:	83 c4 10             	add    $0x10,%esp
f0104fe4:	85 c0                	test   %eax,%eax
f0104fe6:	74 1a                	je     f0105002 <syscall+0x44f>
        cprintf("sys_page_unmap: bad env");
f0104fe8:	83 ec 0c             	sub    $0xc,%esp
f0104feb:	68 11 8f 10 f0       	push   $0xf0108f11
f0104ff0:	e8 1c ec ff ff       	call   f0103c11 <cprintf>
f0104ff5:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0104ff8:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0104ffd:	e9 6e 03 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    page_remove(env->env_pgdir, va);
f0105002:	83 ec 08             	sub    $0x8,%esp
f0105005:	ff 75 10             	pushl  0x10(%ebp)
f0105008:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010500b:	ff 70 60             	pushl  0x60(%eax)
f010500e:	e8 eb c5 ff ff       	call   f01015fe <page_remove>
f0105013:	83 c4 10             	add    $0x10,%esp
    	case SYS_page_alloc:
        	return sys_page_alloc(a1, (void *)a2, a3);
    	case SYS_page_map:
        	return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
	    case SYS_page_unmap:
	        return sys_page_unmap(a1, (void *)a2);
f0105016:	e9 55 03 00 00       	jmp    f0105370 <syscall+0x7bd>
static int
sys_env_set_pgfault_upcall(envid_t envid, void *func)
{
	// LAB 4: Your code here.
	//panic("sys_env_set_pgfault_upcall not implemented");
    assert(func != NULL);
f010501b:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f010501f:	75 19                	jne    f010503a <syscall+0x487>
f0105021:	68 29 8f 10 f0       	push   $0xf0108f29
f0105026:	68 51 75 10 f0       	push   $0xf0107551
f010502b:	68 bc 00 00 00       	push   $0xbc
f0105030:	68 36 8f 10 f0       	push   $0xf0108f36
f0105035:	e8 12 b0 ff ff       	call   f010004c <_panic>
    
    // find environment.
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f010503a:	83 ec 04             	sub    $0x4,%esp
f010503d:	6a 01                	push   $0x1
f010503f:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0105042:	50                   	push   %eax
f0105043:	ff 75 0c             	pushl  0xc(%ebp)
f0105046:	e8 c7 e2 ff ff       	call   f0103312 <envid2env>
f010504b:	89 c3                	mov    %eax,%ebx
f010504d:	83 c4 10             	add    $0x10,%esp
f0105050:	85 c0                	test   %eax,%eax
f0105052:	74 1a                	je     f010506e <syscall+0x4bb>
        cprintf("sys_env_set_pgfault_upcall: bad env");
f0105054:	83 ec 0c             	sub    $0xc,%esp
f0105057:	68 48 90 10 f0       	push   $0xf0109048
f010505c:	e8 b0 eb ff ff       	call   f0103c11 <cprintf>
f0105061:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0105064:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f0105069:	e9 02 03 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    env->env_pgfault_upcall = func;
f010506e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0105071:	8b 7d 10             	mov    0x10(%ebp),%edi
f0105074:	89 78 64             	mov    %edi,0x64(%eax)
    	case SYS_page_map:
        	return sys_page_map(a1, (void *)a2, a3, (void *)a4, a5);
	    case SYS_page_unmap:
	        return sys_page_unmap(a1, (void *)a2);
	    case SYS_env_set_pgfault_upcall:
	        return sys_env_set_pgfault_upcall(a1, (void *)a2);
f0105077:	e9 f4 02 00 00       	jmp    f0105370 <syscall+0x7bd>
static int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm)
{
	// LAB 4: Your code here.
    struct Env *env;
    if (envid2env(envid, &env, 0)) {
f010507c:	83 ec 04             	sub    $0x4,%esp
f010507f:	6a 00                	push   $0x0
f0105081:	8d 45 e0             	lea    -0x20(%ebp),%eax
f0105084:	50                   	push   %eax
f0105085:	ff 75 0c             	pushl  0xc(%ebp)
f0105088:	e8 85 e2 ff ff       	call   f0103312 <envid2env>
f010508d:	89 c3                	mov    %eax,%ebx
f010508f:	83 c4 10             	add    $0x10,%esp
f0105092:	85 c0                	test   %eax,%eax
f0105094:	74 24                	je     f01050ba <syscall+0x507>
        warn("sys_ipc_try_send: bad env");
f0105096:	83 ec 04             	sub    $0x4,%esp
f0105099:	68 45 8f 10 f0       	push   $0xf0108f45
f010509e:	68 b2 01 00 00       	push   $0x1b2
f01050a3:	68 36 8f 10 f0       	push   $0xf0108f36
f01050a8:	e8 6d b2 ff ff       	call   f010031a <_warn>
f01050ad:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f01050b0:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f01050b5:	e9 b6 02 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    if (!env->env_ipc_recving) {
f01050ba:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01050bd:	80 78 68 00          	cmpb   $0x0,0x68(%eax)
f01050c1:	0f 84 84 01 00 00    	je     f010524b <syscall+0x698>
        // If uncomment this, user/primes will make the screen a big mass.
        // warn("sys_ipc_try_send: not recv");
        return -E_IPC_NOT_RECV;
    }

    if ((uintptr_t)srcva < UTOP) {
f01050c7:	81 7d 14 ff ff bf ee 	cmpl   $0xeebfffff,0x14(%ebp)
f01050ce:	0f 87 36 01 00 00    	ja     f010520a <syscall+0x657>
        if ((uintptr_t)srcva & 0xfff) {
f01050d4:	f7 45 14 ff 0f 00 00 	testl  $0xfff,0x14(%ebp)
f01050db:	74 24                	je     f0105101 <syscall+0x54e>
            warn("sys_ipc_try_send: not aligned");
f01050dd:	83 ec 04             	sub    $0x4,%esp
f01050e0:	68 5f 8f 10 f0       	push   $0xf0108f5f
f01050e5:	68 be 01 00 00       	push   $0x1be
f01050ea:	68 36 8f 10 f0       	push   $0xf0108f36
f01050ef:	e8 26 b2 ff ff       	call   f010031a <_warn>
f01050f4:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f01050f7:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f01050fc:	e9 6f 02 00 00       	jmp    f0105370 <syscall+0x7bd>
        }

        if (!(perm & PTE_U) ||
f0105101:	8b 45 18             	mov    0x18(%ebp),%eax
f0105104:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f0105109:	83 f8 05             	cmp    $0x5,%eax
f010510c:	74 24                	je     f0105132 <syscall+0x57f>
            !(perm & PTE_P) ||
            (perm & ~PTE_SYSCALL)) {
            warn("sys_ipc_try_send: bad perm");
f010510e:	83 ec 04             	sub    $0x4,%esp
f0105111:	68 7d 8f 10 f0       	push   $0xf0108f7d
f0105116:	68 c5 01 00 00       	push   $0x1c5
f010511b:	68 36 8f 10 f0       	push   $0xf0108f36
f0105120:	e8 f5 b1 ff ff       	call   f010031a <_warn>
f0105125:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f0105128:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f010512d:	e9 3e 02 00 00       	jmp    f0105370 <syscall+0x7bd>
        }

        pte_t *pte;
        struct PageInfo *page = page_lookup(curenv->env_pgdir, srcva, &pte);
f0105132:	e8 f7 15 00 00       	call   f010672e <cpunum>
f0105137:	83 ec 04             	sub    $0x4,%esp
f010513a:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f010513d:	52                   	push   %edx
f010513e:	ff 75 14             	pushl  0x14(%ebp)
f0105141:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105144:	01 c2                	add    %eax,%edx
f0105146:	01 d2                	add    %edx,%edx
f0105148:	01 c2                	add    %eax,%edx
f010514a:	8d 04 90             	lea    (%eax,%edx,4),%eax
f010514d:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0105154:	ff 70 60             	pushl  0x60(%eax)
f0105157:	e8 7d c3 ff ff       	call   f01014d9 <page_lookup>
        if (!page) {
f010515c:	83 c4 10             	add    $0x10,%esp
f010515f:	85 c0                	test   %eax,%eax
f0105161:	75 24                	jne    f0105187 <syscall+0x5d4>
            warn("sys_ipc_try_send: src not mapped");
f0105163:	83 ec 04             	sub    $0x4,%esp
f0105166:	68 6c 90 10 f0       	push   $0xf010906c
f010516b:	68 cc 01 00 00       	push   $0x1cc
f0105170:	68 36 8f 10 f0       	push   $0xf0108f36
f0105175:	e8 a0 b1 ff ff       	call   f010031a <_warn>
f010517a:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f010517d:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0105182:	e9 e9 01 00 00       	jmp    f0105370 <syscall+0x7bd>
        }

        if ((perm & PTE_W) && !(*pte & PTE_W)) {
f0105187:	f6 45 18 02          	testb  $0x2,0x18(%ebp)
f010518b:	74 2c                	je     f01051b9 <syscall+0x606>
f010518d:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0105190:	f6 02 02             	testb  $0x2,(%edx)
f0105193:	75 24                	jne    f01051b9 <syscall+0x606>
            warn("sys_ipc_try_send: src not writable");
f0105195:	83 ec 04             	sub    $0x4,%esp
f0105198:	68 90 90 10 f0       	push   $0xf0109090
f010519d:	68 d1 01 00 00       	push   $0x1d1
f01051a2:	68 36 8f 10 f0       	push   $0xf0108f36
f01051a7:	e8 6e b1 ff ff       	call   f010031a <_warn>
f01051ac:	83 c4 10             	add    $0x10,%esp
            return -E_INVAL;
f01051af:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f01051b4:	e9 b7 01 00 00       	jmp    f0105370 <syscall+0x7bd>
        }

        if ((uintptr_t)env->env_ipc_dstva < UTOP &&
f01051b9:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01051bc:	8b 4a 6c             	mov    0x6c(%edx),%ecx
f01051bf:	81 f9 ff ff bf ee    	cmp    $0xeebfffff,%ecx
f01051c5:	77 38                	ja     f01051ff <syscall+0x64c>
            page_insert(env->env_pgdir, page, env->env_ipc_dstva, perm)) {
f01051c7:	ff 75 18             	pushl  0x18(%ebp)
f01051ca:	51                   	push   %ecx
f01051cb:	50                   	push   %eax
f01051cc:	ff 72 60             	pushl  0x60(%edx)
f01051cf:	e8 ca c4 ff ff       	call   f010169e <page_insert>
        if ((perm & PTE_W) && !(*pte & PTE_W)) {
            warn("sys_ipc_try_send: src not writable");
            return -E_INVAL;
        }

        if ((uintptr_t)env->env_ipc_dstva < UTOP &&
f01051d4:	83 c4 10             	add    $0x10,%esp
f01051d7:	85 c0                	test   %eax,%eax
f01051d9:	74 24                	je     f01051ff <syscall+0x64c>
            page_insert(env->env_pgdir, page, env->env_ipc_dstva, perm)) {
            warn("sys_ipc_try_send: no mem");
f01051db:	83 ec 04             	sub    $0x4,%esp
f01051de:	68 98 8f 10 f0       	push   $0xf0108f98
f01051e3:	68 d7 01 00 00       	push   $0x1d7
f01051e8:	68 36 8f 10 f0       	push   $0xf0108f36
f01051ed:	e8 28 b1 ff ff       	call   f010031a <_warn>
f01051f2:	83 c4 10             	add    $0x10,%esp
            return -E_NO_MEM;
f01051f5:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
f01051fa:	e9 71 01 00 00       	jmp    f0105370 <syscall+0x7bd>
        }
        env->env_ipc_perm = perm;
f01051ff:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0105202:	8b 4d 18             	mov    0x18(%ebp),%ecx
f0105205:	89 48 78             	mov    %ecx,0x78(%eax)
f0105208:	eb 07                	jmp    f0105211 <syscall+0x65e>
    } else {
        env->env_ipc_perm = 0;
f010520a:	c7 40 78 00 00 00 00 	movl   $0x0,0x78(%eax)
    }
    //cprintf("val in sys_ipc_try_send() is: %d\n", value);
    env->env_ipc_recving = 0;
f0105211:	8b 75 e0             	mov    -0x20(%ebp),%esi
f0105214:	c6 46 68 00          	movb   $0x0,0x68(%esi)
    env->env_ipc_from = curenv->env_id;
f0105218:	e8 11 15 00 00       	call   f010672e <cpunum>
f010521d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105220:	01 c2                	add    %eax,%edx
f0105222:	01 d2                	add    %edx,%edx
f0105224:	01 c2                	add    %eax,%edx
f0105226:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105229:	8b 04 85 08 50 23 f0 	mov    -0xfdcaff8(,%eax,4),%eax
f0105230:	8b 40 48             	mov    0x48(%eax),%eax
f0105233:	89 46 74             	mov    %eax,0x74(%esi)
    env->env_ipc_value = value;
f0105236:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0105239:	8b 7d 10             	mov    0x10(%ebp),%edi
f010523c:	89 78 70             	mov    %edi,0x70(%eax)
    env->env_status = ENV_RUNNABLE;
f010523f:	c7 40 54 02 00 00 00 	movl   $0x2,0x54(%eax)
f0105246:	e9 25 01 00 00       	jmp    f0105370 <syscall+0x7bd>
    }

    if (!env->env_ipc_recving) {
        // If uncomment this, user/primes will make the screen a big mass.
        // warn("sys_ipc_try_send: not recv");
        return -E_IPC_NOT_RECV;
f010524b:	bb f9 ff ff ff       	mov    $0xfffffff9,%ebx
	    case SYS_page_unmap:
	        return sys_page_unmap(a1, (void *)a2);
	    case SYS_env_set_pgfault_upcall:
	        return sys_env_set_pgfault_upcall(a1, (void *)a2);
	    case SYS_ipc_try_send: //lab4 last part 
	        return sys_ipc_try_send(a1, a2, (void *)a3, a4);
f0105250:	e9 1b 01 00 00       	jmp    f0105370 <syscall+0x7bd>
//	-E_INVAL if dstva < UTOP but dstva is not page-aligned.
static int
sys_ipc_recv(void *dstva)
{
	// LAB 4: Your code here.
    if ((uintptr_t)dstva < UTOP) {
f0105255:	81 7d 0c ff ff bf ee 	cmpl   $0xeebfffff,0xc(%ebp)
f010525c:	77 43                	ja     f01052a1 <syscall+0x6ee>
        if ((uintptr_t)dstva & 0xfff) {
f010525e:	f7 45 0c ff 0f 00 00 	testl  $0xfff,0xc(%ebp)
f0105265:	74 24                	je     f010528b <syscall+0x6d8>
            warn("sys_ipc_recv: not aligned");
f0105267:	83 ec 04             	sub    $0x4,%esp
f010526a:	68 b1 8f 10 f0       	push   $0xf0108fb1
f010526f:	68 f9 01 00 00       	push   $0x1f9
f0105274:	68 36 8f 10 f0       	push   $0xf0108f36
f0105279:	e8 9c b0 ff ff       	call   f010031a <_warn>
	    case SYS_env_set_pgfault_upcall:
	        return sys_env_set_pgfault_upcall(a1, (void *)a2);
	    case SYS_ipc_try_send: //lab4 last part 
	        return sys_ipc_try_send(a1, a2, (void *)a3, a4);
	    case SYS_ipc_recv: //lab4 last part 
	        return sys_ipc_recv((void *)a1);
f010527e:	83 c4 10             	add    $0x10,%esp
f0105281:	bb fd ff ff ff       	mov    $0xfffffffd,%ebx
f0105286:	e9 e5 00 00 00       	jmp    f0105370 <syscall+0x7bd>
        if ((uintptr_t)dstva & 0xfff) {
            warn("sys_ipc_recv: not aligned");
            return -E_INVAL;
        }

        curenv->env_ipc_dstva = dstva;
f010528b:	e8 9e 14 00 00       	call   f010672e <cpunum>
f0105290:	6b c0 74             	imul   $0x74,%eax,%eax
f0105293:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f0105299:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f010529c:	89 48 6c             	mov    %ecx,0x6c(%eax)
f010529f:	eb 15                	jmp    f01052b6 <syscall+0x703>
    } else {
        // in case it's less than UTOP,
        // which would result in error.
        curenv->env_ipc_dstva = (void *)UTOP;
f01052a1:	e8 88 14 00 00       	call   f010672e <cpunum>
f01052a6:	6b c0 74             	imul   $0x74,%eax,%eax
f01052a9:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f01052af:	c7 40 6c 00 00 c0 ee 	movl   $0xeec00000,0x6c(%eax)
    }
    //if(curenv->env_ipc_recving != 0){
        curenv->env_ipc_recving = 1;
f01052b6:	e8 73 14 00 00       	call   f010672e <cpunum>
f01052bb:	6b c0 74             	imul   $0x74,%eax,%eax
f01052be:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f01052c4:	c6 40 68 01          	movb   $0x1,0x68(%eax)
        curenv->env_status = ENV_NOT_RUNNABLE;
f01052c8:	e8 61 14 00 00       	call   f010672e <cpunum>
f01052cd:	6b c0 74             	imul   $0x74,%eax,%eax
f01052d0:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f01052d6:	c7 40 54 04 00 00 00 	movl   $0x4,0x54(%eax)
        //cprintf("In sys_ipc_recv(), env %d set itself not runable\n", curenv->env_id);
        // The call will never return on success,
        // but the system call needs to return 0.
        curenv->env_tf.tf_regs.reg_eax = 0;
f01052dd:	e8 4c 14 00 00       	call   f010672e <cpunum>
f01052e2:	6b c0 74             	imul   $0x74,%eax,%eax
f01052e5:	8b 80 08 50 23 f0    	mov    -0xfdcaff8(%eax),%eax
f01052eb:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
// Deschedule current environment and pick a different one to run.
static void
sys_yield(void)
{   
    //cprintf("timer interrupted!\n");
	sched_yield();
f01052f2:	e8 28 f8 ff ff       	call   f0104b1f <sched_yield>
// Return the current time.
static int
sys_time_msec(void)
{
	// LAB 4: Your code here.
    return (time_msec());
f01052f7:	e8 cc 19 00 00       	call   f0106cc8 <time_msec>
f01052fc:	89 c3                	mov    %eax,%ebx
	    case SYS_ipc_try_send: //lab4 last part 
	        return sys_ipc_try_send(a1, a2, (void *)a3, a4);
	    case SYS_ipc_recv: //lab4 last part 
	        return sys_ipc_recv((void *)a1);
        case SYS_time_msec:
            return sys_time_msec();
f01052fe:	eb 70                	jmp    f0105370 <syscall+0x7bd>
		case SYS_env_set_trapframe:
	        return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
f0105300:	8b 75 10             	mov    0x10(%ebp),%esi
{
	// LAB 5: Your code here.
	// Remember to check whether the user has supplied us with a good
	// address!
    struct Env *env;
    if (envid2env(envid, &env, 1)) {
f0105303:	83 ec 04             	sub    $0x4,%esp
f0105306:	6a 01                	push   $0x1
f0105308:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f010530b:	50                   	push   %eax
f010530c:	ff 75 0c             	pushl  0xc(%ebp)
f010530f:	e8 fe df ff ff       	call   f0103312 <envid2env>
f0105314:	89 c3                	mov    %eax,%ebx
f0105316:	83 c4 10             	add    $0x10,%esp
f0105319:	85 c0                	test   %eax,%eax
f010531b:	74 21                	je     f010533e <syscall+0x78b>
        warn("sys_env_set_trapframe: bad env");
f010531d:	83 ec 04             	sub    $0x4,%esp
f0105320:	68 b4 90 10 f0       	push   $0xf01090b4
f0105325:	68 a1 00 00 00       	push   $0xa1
f010532a:	68 36 8f 10 f0       	push   $0xf0108f36
f010532f:	e8 e6 af ff ff       	call   f010031a <_warn>
f0105334:	83 c4 10             	add    $0x10,%esp
        return -E_BAD_ENV;
f0105337:	bb fe ff ff ff       	mov    $0xfffffffe,%ebx
f010533c:	eb 32                	jmp    f0105370 <syscall+0x7bd>
    }

    env->env_tf = *tf;
f010533e:	b9 11 00 00 00       	mov    $0x11,%ecx
f0105343:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105346:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

    // Make sure CPL = 3, interrupts enabled.
    env->env_tf.tf_cs |= 3;
f0105348:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010534b:	66 83 48 34 03       	orw    $0x3,0x34(%eax)
    env->env_tf.tf_eflags |= FL_IF;
f0105350:	81 48 38 00 02 00 00 	orl    $0x200,0x38(%eax)
	    case SYS_ipc_recv: //lab4 last part 
	        return sys_ipc_recv((void *)a1);
        case SYS_time_msec:
            return sys_time_msec();
		case SYS_env_set_trapframe:
	        return sys_env_set_trapframe(a1, (struct Trapframe *)a2);
f0105357:	eb 17                	jmp    f0105370 <syscall+0x7bd>
		default:
			panic("syscall not implemented");
f0105359:	83 ec 04             	sub    $0x4,%esp
f010535c:	68 cb 8f 10 f0       	push   $0xf0108fcb
f0105361:	68 44 02 00 00       	push   $0x244
f0105366:	68 36 8f 10 f0       	push   $0xf0108f36
f010536b:	e8 dc ac ff ff       	call   f010004c <_panic>
			return -E_INVAL;
	}
}
f0105370:	89 d8                	mov    %ebx,%eax
f0105372:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105375:	5b                   	pop    %ebx
f0105376:	5e                   	pop    %esi
f0105377:	5f                   	pop    %edi
f0105378:	5d                   	pop    %ebp
f0105379:	c3                   	ret    

f010537a <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f010537a:	55                   	push   %ebp
f010537b:	89 e5                	mov    %esp,%ebp
f010537d:	57                   	push   %edi
f010537e:	56                   	push   %esi
f010537f:	53                   	push   %ebx
f0105380:	83 ec 14             	sub    $0x14,%esp
f0105383:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0105386:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0105389:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f010538c:	8b 7d 08             	mov    0x8(%ebp),%edi
	int l = *region_left, r = *region_right, any_matches = 0;
f010538f:	8b 1a                	mov    (%edx),%ebx
f0105391:	8b 01                	mov    (%ecx),%eax
f0105393:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0105396:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	while (l <= r) {
f010539d:	eb 7e                	jmp    f010541d <stab_binsearch+0xa3>
		int true_m = (l + r) / 2, m = true_m;
f010539f:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01053a2:	01 d8                	add    %ebx,%eax
f01053a4:	89 c6                	mov    %eax,%esi
f01053a6:	c1 ee 1f             	shr    $0x1f,%esi
f01053a9:	01 c6                	add    %eax,%esi
f01053ab:	d1 fe                	sar    %esi
f01053ad:	8d 04 36             	lea    (%esi,%esi,1),%eax
f01053b0:	01 f0                	add    %esi,%eax
f01053b2:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f01053b5:	8d 54 81 04          	lea    0x4(%ecx,%eax,4),%edx
f01053b9:	89 f0                	mov    %esi,%eax

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f01053bb:	eb 01                	jmp    f01053be <stab_binsearch+0x44>
			m--;
f01053bd:	48                   	dec    %eax

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f01053be:	39 c3                	cmp    %eax,%ebx
f01053c0:	7f 0c                	jg     f01053ce <stab_binsearch+0x54>
f01053c2:	0f b6 0a             	movzbl (%edx),%ecx
f01053c5:	83 ea 0c             	sub    $0xc,%edx
f01053c8:	39 f9                	cmp    %edi,%ecx
f01053ca:	75 f1                	jne    f01053bd <stab_binsearch+0x43>
f01053cc:	eb 05                	jmp    f01053d3 <stab_binsearch+0x59>
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f01053ce:	8d 5e 01             	lea    0x1(%esi),%ebx
			continue;
f01053d1:	eb 4a                	jmp    f010541d <stab_binsearch+0xa3>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f01053d3:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01053d6:	01 c2                	add    %eax,%edx
f01053d8:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f01053db:	8b 54 91 08          	mov    0x8(%ecx,%edx,4),%edx
f01053df:	39 55 0c             	cmp    %edx,0xc(%ebp)
f01053e2:	76 11                	jbe    f01053f5 <stab_binsearch+0x7b>
			*region_left = m;
f01053e4:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f01053e7:	89 03                	mov    %eax,(%ebx)
			l = true_m + 1;
f01053e9:	8d 5e 01             	lea    0x1(%esi),%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f01053ec:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f01053f3:	eb 28                	jmp    f010541d <stab_binsearch+0xa3>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f01053f5:	39 55 0c             	cmp    %edx,0xc(%ebp)
f01053f8:	73 12                	jae    f010540c <stab_binsearch+0x92>
			*region_right = m - 1;
f01053fa:	48                   	dec    %eax
f01053fb:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01053fe:	8b 75 e0             	mov    -0x20(%ebp),%esi
f0105401:	89 06                	mov    %eax,(%esi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0105403:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f010540a:	eb 11                	jmp    f010541d <stab_binsearch+0xa3>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f010540c:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f010540f:	89 06                	mov    %eax,(%esi)
			l = m;
			addr++;
f0105411:	ff 45 0c             	incl   0xc(%ebp)
f0105414:	89 c3                	mov    %eax,%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0105416:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f010541d:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
f0105420:	0f 8e 79 ff ff ff    	jle    f010539f <stab_binsearch+0x25>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0105426:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
f010542a:	75 0d                	jne    f0105439 <stab_binsearch+0xbf>
		*region_right = *region_left - 1;
f010542c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010542f:	8b 00                	mov    (%eax),%eax
f0105431:	48                   	dec    %eax
f0105432:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105435:	89 07                	mov    %eax,(%edi)
f0105437:	eb 2c                	jmp    f0105465 <stab_binsearch+0xeb>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0105439:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010543c:	8b 00                	mov    (%eax),%eax
		     l > *region_left && stabs[l].n_type != type;
f010543e:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0105441:	8b 0e                	mov    (%esi),%ecx
f0105443:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105446:	01 c2                	add    %eax,%edx
f0105448:	8b 75 ec             	mov    -0x14(%ebp),%esi
f010544b:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f010544f:	eb 01                	jmp    f0105452 <stab_binsearch+0xd8>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0105451:	48                   	dec    %eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0105452:	39 c8                	cmp    %ecx,%eax
f0105454:	7e 0a                	jle    f0105460 <stab_binsearch+0xe6>
		     l > *region_left && stabs[l].n_type != type;
f0105456:	0f b6 1a             	movzbl (%edx),%ebx
f0105459:	83 ea 0c             	sub    $0xc,%edx
f010545c:	39 df                	cmp    %ebx,%edi
f010545e:	75 f1                	jne    f0105451 <stab_binsearch+0xd7>
		     l--)
			/* do nothing */;
		*region_left = l;
f0105460:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105463:	89 07                	mov    %eax,(%edi)
	}
}
f0105465:	83 c4 14             	add    $0x14,%esp
f0105468:	5b                   	pop    %ebx
f0105469:	5e                   	pop    %esi
f010546a:	5f                   	pop    %edi
f010546b:	5d                   	pop    %ebp
f010546c:	c3                   	ret    

f010546d <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f010546d:	55                   	push   %ebp
f010546e:	89 e5                	mov    %esp,%ebp
f0105470:	57                   	push   %edi
f0105471:	56                   	push   %esi
f0105472:	53                   	push   %ebx
f0105473:	83 ec 3c             	sub    $0x3c,%esp
f0105476:	8b 7d 0c             	mov    0xc(%ebp),%edi
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0105479:	c7 07 10 91 10 f0    	movl   $0xf0109110,(%edi)
	info->eip_line = 0;
f010547f:	c7 47 04 00 00 00 00 	movl   $0x0,0x4(%edi)
	info->eip_fn_name = "<unknown>";
f0105486:	c7 47 08 10 91 10 f0 	movl   $0xf0109110,0x8(%edi)
	info->eip_fn_namelen = 9;
f010548d:	c7 47 0c 09 00 00 00 	movl   $0x9,0xc(%edi)
	info->eip_fn_addr = addr;
f0105494:	8b 45 08             	mov    0x8(%ebp),%eax
f0105497:	89 47 10             	mov    %eax,0x10(%edi)
	info->eip_fn_narg = 0;
f010549a:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f01054a1:	3d ff ff 7f ef       	cmp    $0xef7fffff,%eax
f01054a6:	0f 87 a7 00 00 00    	ja     f0105553 <debuginfo_eip+0xe6>

		// Make sure this memory is valid.
		// Return -1 if it is not.  Hint: Call user_mem_check.
		// LAB 3: Your code here.

		stabs = usd->stabs;
f01054ac:	a1 00 00 20 00       	mov    0x200000,%eax
f01054b1:	89 c3                	mov    %eax,%ebx
f01054b3:	89 45 c0             	mov    %eax,-0x40(%ebp)
		stab_end = usd->stab_end;
f01054b6:	8b 35 04 00 20 00    	mov    0x200004,%esi
		stabstr = usd->stabstr;
f01054bc:	a1 08 00 20 00       	mov    0x200008,%eax
f01054c1:	89 45 bc             	mov    %eax,-0x44(%ebp)
		stabstr_end = usd->stabstr_end;
f01054c4:	8b 0d 0c 00 20 00    	mov    0x20000c,%ecx
f01054ca:	89 4d b8             	mov    %ecx,-0x48(%ebp)

		// Make sure the STABS and string table memory is valid.
		// LAB 3: Your code here.
		user_mem_assert(curenv, usd, sizeof(struct UserStabData), PTE_U);
f01054cd:	e8 5c 12 00 00       	call   f010672e <cpunum>
f01054d2:	6a 04                	push   $0x4
f01054d4:	6a 10                	push   $0x10
f01054d6:	68 00 00 20 00       	push   $0x200000
f01054db:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01054de:	01 c2                	add    %eax,%edx
f01054e0:	01 d2                	add    %edx,%edx
f01054e2:	01 c2                	add    %eax,%edx
f01054e4:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01054e7:	ff 34 85 08 50 23 f0 	pushl  -0xfdcaff8(,%eax,4)
f01054ee:	e8 31 dd ff ff       	call   f0103224 <user_mem_assert>
		user_mem_assert(curenv, stabs, (uintptr_t)stab_end - (uintptr_t)stabs, PTE_U);
f01054f3:	89 f2                	mov    %esi,%edx
f01054f5:	29 da                	sub    %ebx,%edx
f01054f7:	89 55 c4             	mov    %edx,-0x3c(%ebp)
f01054fa:	e8 2f 12 00 00       	call   f010672e <cpunum>
f01054ff:	6a 04                	push   $0x4
f0105501:	ff 75 c4             	pushl  -0x3c(%ebp)
f0105504:	53                   	push   %ebx
f0105505:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105508:	01 c2                	add    %eax,%edx
f010550a:	01 d2                	add    %edx,%edx
f010550c:	01 c2                	add    %eax,%edx
f010550e:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105511:	ff 34 85 08 50 23 f0 	pushl  -0xfdcaff8(,%eax,4)
f0105518:	e8 07 dd ff ff       	call   f0103224 <user_mem_assert>
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
f010551d:	8b 4d b8             	mov    -0x48(%ebp),%ecx
f0105520:	8b 5d bc             	mov    -0x44(%ebp),%ebx
f0105523:	29 d9                	sub    %ebx,%ecx
f0105525:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
f0105528:	83 c4 20             	add    $0x20,%esp
f010552b:	e8 fe 11 00 00       	call   f010672e <cpunum>
f0105530:	6a 04                	push   $0x4
f0105532:	ff 75 c4             	pushl  -0x3c(%ebp)
f0105535:	53                   	push   %ebx
f0105536:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0105539:	01 c2                	add    %eax,%edx
f010553b:	01 d2                	add    %edx,%edx
f010553d:	01 c2                	add    %eax,%edx
f010553f:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0105542:	ff 34 85 08 50 23 f0 	pushl  -0xfdcaff8(,%eax,4)
f0105549:	e8 d6 dc ff ff       	call   f0103224 <user_mem_assert>
f010554e:	83 c4 10             	add    $0x10,%esp
f0105551:	eb 1a                	jmp    f010556d <debuginfo_eip+0x100>
	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
f0105553:	c7 45 b8 90 b6 11 f0 	movl   $0xf011b690,-0x48(%ebp)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
f010555a:	c7 45 bc 95 5f 11 f0 	movl   $0xf0115f95,-0x44(%ebp)
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
f0105561:	be 94 5f 11 f0       	mov    $0xf0115f94,%esi
	info->eip_fn_addr = addr;
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
f0105566:	c7 45 c0 38 9e 10 f0 	movl   $0xf0109e38,-0x40(%ebp)
		user_mem_assert(curenv, stabs, (uintptr_t)stab_end - (uintptr_t)stabs, PTE_U);
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f010556d:	8b 45 b8             	mov    -0x48(%ebp),%eax
f0105570:	39 45 bc             	cmp    %eax,-0x44(%ebp)
f0105573:	0f 83 a3 01 00 00    	jae    f010571c <debuginfo_eip+0x2af>
f0105579:	89 c3                	mov    %eax,%ebx
f010557b:	80 78 ff 00          	cmpb   $0x0,-0x1(%eax)
f010557f:	0f 85 9e 01 00 00    	jne    f0105723 <debuginfo_eip+0x2b6>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0105585:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	rfile = (stab_end - stabs) - 1;
f010558c:	2b 75 c0             	sub    -0x40(%ebp),%esi
f010558f:	c1 fe 02             	sar    $0x2,%esi
f0105592:	8d 04 b6             	lea    (%esi,%esi,4),%eax
f0105595:	8d 04 86             	lea    (%esi,%eax,4),%eax
f0105598:	8d 14 86             	lea    (%esi,%eax,4),%edx
f010559b:	89 d0                	mov    %edx,%eax
f010559d:	c1 e0 08             	shl    $0x8,%eax
f01055a0:	01 c2                	add    %eax,%edx
f01055a2:	89 d0                	mov    %edx,%eax
f01055a4:	c1 e0 10             	shl    $0x10,%eax
f01055a7:	01 d0                	add    %edx,%eax
f01055a9:	01 c0                	add    %eax,%eax
f01055ab:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f01055af:	89 45 e0             	mov    %eax,-0x20(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f01055b2:	83 ec 08             	sub    $0x8,%esp
f01055b5:	ff 75 08             	pushl  0x8(%ebp)
f01055b8:	6a 64                	push   $0x64
f01055ba:	8d 4d e0             	lea    -0x20(%ebp),%ecx
f01055bd:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f01055c0:	8b 75 c0             	mov    -0x40(%ebp),%esi
f01055c3:	89 f0                	mov    %esi,%eax
f01055c5:	e8 b0 fd ff ff       	call   f010537a <stab_binsearch>
	if (lfile == 0)
f01055ca:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01055cd:	83 c4 10             	add    $0x10,%esp
f01055d0:	85 c0                	test   %eax,%eax
f01055d2:	0f 84 52 01 00 00    	je     f010572a <debuginfo_eip+0x2bd>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f01055d8:	89 45 dc             	mov    %eax,-0x24(%ebp)
	rfun = rfile;
f01055db:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01055de:	89 45 d8             	mov    %eax,-0x28(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f01055e1:	83 ec 08             	sub    $0x8,%esp
f01055e4:	ff 75 08             	pushl  0x8(%ebp)
f01055e7:	6a 24                	push   $0x24
f01055e9:	8d 4d d8             	lea    -0x28(%ebp),%ecx
f01055ec:	8d 55 dc             	lea    -0x24(%ebp),%edx
f01055ef:	89 f0                	mov    %esi,%eax
f01055f1:	e8 84 fd ff ff       	call   f010537a <stab_binsearch>

	if (lfun <= rfun) {
f01055f6:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01055f9:	8b 55 d8             	mov    -0x28(%ebp),%edx
f01055fc:	83 c4 10             	add    $0x10,%esp
f01055ff:	39 d0                	cmp    %edx,%eax
f0105601:	7f 28                	jg     f010562b <debuginfo_eip+0x1be>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0105603:	8d 0c 00             	lea    (%eax,%eax,1),%ecx
f0105606:	01 c1                	add    %eax,%ecx
f0105608:	8d 34 8e             	lea    (%esi,%ecx,4),%esi
f010560b:	8b 0e                	mov    (%esi),%ecx
f010560d:	2b 5d bc             	sub    -0x44(%ebp),%ebx
f0105610:	39 d9                	cmp    %ebx,%ecx
f0105612:	73 06                	jae    f010561a <debuginfo_eip+0x1ad>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0105614:	03 4d bc             	add    -0x44(%ebp),%ecx
f0105617:	89 4f 08             	mov    %ecx,0x8(%edi)
		info->eip_fn_addr = stabs[lfun].n_value;
f010561a:	8b 4e 08             	mov    0x8(%esi),%ecx
f010561d:	89 4f 10             	mov    %ecx,0x10(%edi)
		addr -= info->eip_fn_addr;
f0105620:	29 4d 08             	sub    %ecx,0x8(%ebp)
		// Search within the function definition for the line number.
		lline = lfun;
f0105623:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfun;
f0105626:	89 55 d0             	mov    %edx,-0x30(%ebp)
f0105629:	eb 12                	jmp    f010563d <debuginfo_eip+0x1d0>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f010562b:	8b 45 08             	mov    0x8(%ebp),%eax
f010562e:	89 47 10             	mov    %eax,0x10(%edi)
		lline = lfile;
f0105631:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0105634:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfile;
f0105637:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010563a:	89 45 d0             	mov    %eax,-0x30(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f010563d:	83 ec 08             	sub    $0x8,%esp
f0105640:	6a 3a                	push   $0x3a
f0105642:	ff 77 08             	pushl  0x8(%edi)
f0105645:	e8 c6 09 00 00       	call   f0106010 <strfind>
f010564a:	2b 47 08             	sub    0x8(%edi),%eax
f010564d:	89 47 0c             	mov    %eax,0xc(%edi)
	// Hint:
	//	There's a particular stabs type used for line numbers.
	//	Look at the STABS documentation and <inc/stab.h> to find
	//	which one.
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
f0105650:	83 c4 08             	add    $0x8,%esp
f0105653:	ff 75 08             	pushl  0x8(%ebp)
f0105656:	6a 44                	push   $0x44
f0105658:	8d 4d d0             	lea    -0x30(%ebp),%ecx
f010565b:	8d 55 d4             	lea    -0x2c(%ebp),%edx
f010565e:	8b 5d c0             	mov    -0x40(%ebp),%ebx
f0105661:	89 d8                	mov    %ebx,%eax
f0105663:	e8 12 fd ff ff       	call   f010537a <stab_binsearch>
	info->eip_line = stabs[lline].n_value;
f0105668:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010566b:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010566e:	01 c2                	add    %eax,%edx
f0105670:	c1 e2 02             	shl    $0x2,%edx
f0105673:	8b 4c 13 08          	mov    0x8(%ebx,%edx,1),%ecx
f0105677:	89 4f 04             	mov    %ecx,0x4(%edi)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f010567a:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f010567d:	8d 54 13 08          	lea    0x8(%ebx,%edx,1),%edx
f0105681:	83 c4 10             	add    $0x10,%esp
f0105684:	c6 45 c4 00          	movb   $0x0,-0x3c(%ebp)
f0105688:	89 7d 0c             	mov    %edi,0xc(%ebp)
f010568b:	eb 08                	jmp    f0105695 <debuginfo_eip+0x228>
f010568d:	48                   	dec    %eax
f010568e:	83 ea 0c             	sub    $0xc,%edx
f0105691:	c6 45 c4 01          	movb   $0x1,-0x3c(%ebp)
f0105695:	39 c6                	cmp    %eax,%esi
f0105697:	7e 05                	jle    f010569e <debuginfo_eip+0x231>
f0105699:	8b 7d 0c             	mov    0xc(%ebp),%edi
f010569c:	eb 47                	jmp    f01056e5 <debuginfo_eip+0x278>
	       && stabs[lline].n_type != N_SOL
f010569e:	8a 4a fc             	mov    -0x4(%edx),%cl
f01056a1:	80 f9 84             	cmp    $0x84,%cl
f01056a4:	75 0e                	jne    f01056b4 <debuginfo_eip+0x247>
f01056a6:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01056a9:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01056ad:	74 1b                	je     f01056ca <debuginfo_eip+0x25d>
f01056af:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01056b2:	eb 16                	jmp    f01056ca <debuginfo_eip+0x25d>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f01056b4:	80 f9 64             	cmp    $0x64,%cl
f01056b7:	75 d4                	jne    f010568d <debuginfo_eip+0x220>
f01056b9:	83 3a 00             	cmpl   $0x0,(%edx)
f01056bc:	74 cf                	je     f010568d <debuginfo_eip+0x220>
f01056be:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01056c1:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01056c5:	74 03                	je     f01056ca <debuginfo_eip+0x25d>
f01056c7:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f01056ca:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01056cd:	01 d0                	add    %edx,%eax
f01056cf:	8b 75 c0             	mov    -0x40(%ebp),%esi
f01056d2:	8b 14 86             	mov    (%esi,%eax,4),%edx
f01056d5:	8b 45 b8             	mov    -0x48(%ebp),%eax
f01056d8:	8b 75 bc             	mov    -0x44(%ebp),%esi
f01056db:	29 f0                	sub    %esi,%eax
f01056dd:	39 c2                	cmp    %eax,%edx
f01056df:	73 04                	jae    f01056e5 <debuginfo_eip+0x278>
		info->eip_file = stabstr + stabs[lline].n_strx;
f01056e1:	01 f2                	add    %esi,%edx
f01056e3:	89 17                	mov    %edx,(%edi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f01056e5:	8b 55 dc             	mov    -0x24(%ebp),%edx
f01056e8:	8b 5d d8             	mov    -0x28(%ebp),%ebx
f01056eb:	39 da                	cmp    %ebx,%edx
f01056ed:	7d 42                	jge    f0105731 <debuginfo_eip+0x2c4>
		for (lline = lfun + 1;
f01056ef:	42                   	inc    %edx
f01056f0:	89 55 d4             	mov    %edx,-0x2c(%ebp)
f01056f3:	89 d0                	mov    %edx,%eax
f01056f5:	8d 0c 12             	lea    (%edx,%edx,1),%ecx
f01056f8:	01 ca                	add    %ecx,%edx
f01056fa:	8b 75 c0             	mov    -0x40(%ebp),%esi
f01056fd:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx
f0105701:	eb 03                	jmp    f0105706 <debuginfo_eip+0x299>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;
f0105703:	ff 47 14             	incl   0x14(%edi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
		for (lline = lfun + 1;
f0105706:	39 c3                	cmp    %eax,%ebx
f0105708:	7e 2e                	jle    f0105738 <debuginfo_eip+0x2cb>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
f010570a:	8a 0a                	mov    (%edx),%cl
f010570c:	40                   	inc    %eax
f010570d:	83 c2 0c             	add    $0xc,%edx
f0105710:	80 f9 a0             	cmp    $0xa0,%cl
f0105713:	74 ee                	je     f0105703 <debuginfo_eip+0x296>
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0105715:	b8 00 00 00 00       	mov    $0x0,%eax
f010571a:	eb 21                	jmp    f010573d <debuginfo_eip+0x2d0>
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f010571c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0105721:	eb 1a                	jmp    f010573d <debuginfo_eip+0x2d0>
f0105723:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0105728:	eb 13                	jmp    f010573d <debuginfo_eip+0x2d0>
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
		return -1;
f010572a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f010572f:	eb 0c                	jmp    f010573d <debuginfo_eip+0x2d0>
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0105731:	b8 00 00 00 00       	mov    $0x0,%eax
f0105736:	eb 05                	jmp    f010573d <debuginfo_eip+0x2d0>
f0105738:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010573d:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105740:	5b                   	pop    %ebx
f0105741:	5e                   	pop    %esi
f0105742:	5f                   	pop    %edi
f0105743:	5d                   	pop    %ebp
f0105744:	c3                   	ret    

f0105745 <cpuid_print>:
	return feature[bit / 32] & BIT(bit % 32);
}

void
cpuid_print(void)
{
f0105745:	55                   	push   %ebp
f0105746:	89 e5                	mov    %esp,%ebp
f0105748:	57                   	push   %edi
f0105749:	56                   	push   %esi
f010574a:	53                   	push   %ebx
f010574b:	83 ec 6c             	sub    $0x6c,%esp
	uint32_t eax, brand[12], feature[CPUID_NR_FLAGS] = {0};
f010574e:	8d 7d a4             	lea    -0x5c(%ebp),%edi
f0105751:	b9 05 00 00 00       	mov    $0x5,%ecx
f0105756:	b8 00 00 00 00       	mov    $0x0,%eax
f010575b:	f3 ab                	rep stos %eax,%es:(%edi)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f010575d:	b8 00 00 00 80       	mov    $0x80000000,%eax
f0105762:	0f a2                	cpuid  

	cpuid(0x80000000, &eax, NULL, NULL, NULL);
	if (eax < 0x80000004)
f0105764:	3d 03 00 00 80       	cmp    $0x80000003,%eax
f0105769:	77 17                	ja     f0105782 <cpuid_print+0x3d>
		panic("CPU too old!");
f010576b:	83 ec 04             	sub    $0x4,%esp
f010576e:	68 1a 91 10 f0       	push   $0xf010911a
f0105773:	68 8d 00 00 00       	push   $0x8d
f0105778:	68 27 91 10 f0       	push   $0xf0109127
f010577d:	e8 ca a8 ff ff       	call   f010004c <_panic>
f0105782:	b8 02 00 00 80       	mov    $0x80000002,%eax
f0105787:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f0105789:	89 45 b8             	mov    %eax,-0x48(%ebp)
	if (ebxp)
		*ebxp = ebx;
f010578c:	89 5d bc             	mov    %ebx,-0x44(%ebp)
	if (ecxp)
		*ecxp = ecx;
f010578f:	89 4d c0             	mov    %ecx,-0x40(%ebp)
	if (edxp)
		*edxp = edx;
f0105792:	89 55 c4             	mov    %edx,-0x3c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0105795:	b8 03 00 00 80       	mov    $0x80000003,%eax
f010579a:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f010579c:	89 45 c8             	mov    %eax,-0x38(%ebp)
	if (ebxp)
		*ebxp = ebx;
f010579f:	89 5d cc             	mov    %ebx,-0x34(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01057a2:	89 4d d0             	mov    %ecx,-0x30(%ebp)
	if (edxp)
		*edxp = edx;
f01057a5:	89 55 d4             	mov    %edx,-0x2c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01057a8:	b8 04 00 00 80       	mov    $0x80000004,%eax
f01057ad:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f01057af:	89 45 d8             	mov    %eax,-0x28(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01057b2:	89 5d dc             	mov    %ebx,-0x24(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01057b5:	89 4d e0             	mov    %ecx,-0x20(%ebp)
	if (edxp)
		*edxp = edx;
f01057b8:	89 55 e4             	mov    %edx,-0x1c(%ebp)

	cpuid(0x80000002, &brand[0], &brand[1], &brand[2], &brand[3]);
	cpuid(0x80000003, &brand[4], &brand[5], &brand[6], &brand[7]);
	cpuid(0x80000004, &brand[8], &brand[9], &brand[10], &brand[11]);
	cprintf("CPU: %.48s\n", brand);
f01057bb:	83 ec 08             	sub    $0x8,%esp
f01057be:	8d 45 b8             	lea    -0x48(%ebp),%eax
f01057c1:	50                   	push   %eax
f01057c2:	68 33 91 10 f0       	push   $0xf0109133
f01057c7:	e8 45 e4 ff ff       	call   f0103c11 <cprintf>

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01057cc:	b8 01 00 00 00       	mov    $0x1,%eax
f01057d1:	0f a2                	cpuid  
f01057d3:	89 55 90             	mov    %edx,-0x70(%ebp)
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f01057d6:	89 4d a8             	mov    %ecx,-0x58(%ebp)
	if (edxp)
		*edxp = edx;
f01057d9:	89 55 a4             	mov    %edx,-0x5c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01057dc:	b8 01 00 00 80       	mov    $0x80000001,%eax
f01057e1:	0f a2                	cpuid  
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f01057e3:	89 4d b4             	mov    %ecx,-0x4c(%ebp)
	if (edxp)
		*edxp = edx;
f01057e6:	89 55 b0             	mov    %edx,-0x50(%ebp)
f01057e9:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f01057ec:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
		if (!feature[i])
f01057f3:	8b 45 94             	mov    -0x6c(%ebp),%eax
f01057f6:	8b 74 85 a4          	mov    -0x5c(%ebp,%eax,4),%esi
f01057fa:	85 f6                	test   %esi,%esi
f01057fc:	74 5a                	je     f0105858 <cpuid_print+0x113>
			continue;
		cprintf(" ");
f01057fe:	83 ec 0c             	sub    $0xc,%esp
f0105801:	68 cc 78 10 f0       	push   $0xf01078cc
f0105806:	e8 06 e4 ff ff       	call   f0103c11 <cprintf>
f010580b:	8b 7d 94             	mov    -0x6c(%ebp),%edi
f010580e:	c1 e7 05             	shl    $0x5,%edi
f0105811:	83 c4 10             	add    $0x10,%esp
		for (j = 0; j < 32; ++j) {
f0105814:	bb 00 00 00 00       	mov    $0x0,%ebx
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
f0105819:	89 f0                	mov    %esi,%eax
f010581b:	88 d9                	mov    %bl,%cl
f010581d:	d3 e8                	shr    %cl,%eax
f010581f:	a8 01                	test   $0x1,%al
f0105821:	74 1f                	je     f0105842 <cpuid_print+0xfd>
	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
			const char *name = names[CPUID_BIT(i, j)];
f0105823:	8d 04 3b             	lea    (%ebx,%edi,1),%eax
f0105826:	8b 04 85 a0 93 10 f0 	mov    -0xfef6c60(,%eax,4),%eax

			if ((feature[i] & BIT(j)) && name)
f010582d:	85 c0                	test   %eax,%eax
f010582f:	74 11                	je     f0105842 <cpuid_print+0xfd>
				cprintf(" %s", name);
f0105831:	83 ec 08             	sub    $0x8,%esp
f0105834:	50                   	push   %eax
f0105835:	68 62 75 10 f0       	push   $0xf0107562
f010583a:	e8 d2 e3 ff ff       	call   f0103c11 <cprintf>
f010583f:	83 c4 10             	add    $0x10,%esp

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
f0105842:	43                   	inc    %ebx
f0105843:	83 fb 20             	cmp    $0x20,%ebx
f0105846:	75 d1                	jne    f0105819 <cpuid_print+0xd4>
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
				cprintf(" %s", name);
		}
		cprintf("\n");
f0105848:	83 ec 0c             	sub    $0xc,%esp
f010584b:	68 b2 86 10 f0       	push   $0xf01086b2
f0105850:	e8 bc e3 ff ff       	call   f0103c11 <cprintf>
f0105855:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f0105858:	ff 45 94             	incl   -0x6c(%ebp)
f010585b:	8b 45 94             	mov    -0x6c(%ebp),%eax
f010585e:	83 f8 05             	cmp    $0x5,%eax
f0105861:	75 90                	jne    f01057f3 <cpuid_print+0xae>
	      &feature[CPUID_1_ECX], &feature[CPUID_1_EDX]);
	cpuid(0x80000001, NULL, NULL,
	      &feature[CPUID_80000001_ECX], &feature[CPUID_80000001_EDX]);
	print_feature(feature);
	// Check feature bits.
	assert(cpuid_has(feature, CPUID_FEATURE_PSE));
f0105863:	f6 45 90 08          	testb  $0x8,-0x70(%ebp)
f0105867:	75 19                	jne    f0105882 <cpuid_print+0x13d>
f0105869:	68 38 93 10 f0       	push   $0xf0109338
f010586e:	68 51 75 10 f0       	push   $0xf0107551
f0105873:	68 9a 00 00 00       	push   $0x9a
f0105878:	68 27 91 10 f0       	push   $0xf0109127
f010587d:	e8 ca a7 ff ff       	call   f010004c <_panic>
	assert(cpuid_has(feature, CPUID_FEATURE_APIC));
f0105882:	f7 45 90 00 02 00 00 	testl  $0x200,-0x70(%ebp)
f0105889:	75 19                	jne    f01058a4 <cpuid_print+0x15f>
f010588b:	68 60 93 10 f0       	push   $0xf0109360
f0105890:	68 51 75 10 f0       	push   $0xf0107551
f0105895:	68 9b 00 00 00       	push   $0x9b
f010589a:	68 27 91 10 f0       	push   $0xf0109127
f010589f:	e8 a8 a7 ff ff       	call   f010004c <_panic>
}
f01058a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01058a7:	5b                   	pop    %ebx
f01058a8:	5e                   	pop    %esi
f01058a9:	5f                   	pop    %edi
f01058aa:	5d                   	pop    %ebp
f01058ab:	c3                   	ret    

f01058ac <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f01058ac:	55                   	push   %ebp
f01058ad:	89 e5                	mov    %esp,%ebp
f01058af:	57                   	push   %edi
f01058b0:	56                   	push   %esi
f01058b1:	53                   	push   %ebx
f01058b2:	83 ec 1c             	sub    $0x1c,%esp
f01058b5:	89 c7                	mov    %eax,%edi
f01058b7:	89 d6                	mov    %edx,%esi
f01058b9:	8b 45 08             	mov    0x8(%ebp),%eax
f01058bc:	8b 55 0c             	mov    0xc(%ebp),%edx
f01058bf:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01058c2:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f01058c5:	8b 4d 10             	mov    0x10(%ebp),%ecx
f01058c8:	bb 00 00 00 00       	mov    $0x0,%ebx
f01058cd:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f01058d0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
f01058d3:	39 d3                	cmp    %edx,%ebx
f01058d5:	72 05                	jb     f01058dc <printnum+0x30>
f01058d7:	39 45 10             	cmp    %eax,0x10(%ebp)
f01058da:	77 45                	ja     f0105921 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f01058dc:	83 ec 0c             	sub    $0xc,%esp
f01058df:	ff 75 18             	pushl  0x18(%ebp)
f01058e2:	8b 45 14             	mov    0x14(%ebp),%eax
f01058e5:	8d 58 ff             	lea    -0x1(%eax),%ebx
f01058e8:	53                   	push   %ebx
f01058e9:	ff 75 10             	pushl  0x10(%ebp)
f01058ec:	83 ec 08             	sub    $0x8,%esp
f01058ef:	ff 75 e4             	pushl  -0x1c(%ebp)
f01058f2:	ff 75 e0             	pushl  -0x20(%ebp)
f01058f5:	ff 75 dc             	pushl  -0x24(%ebp)
f01058f8:	ff 75 d8             	pushl  -0x28(%ebp)
f01058fb:	e8 38 19 00 00       	call   f0107238 <__udivdi3>
f0105900:	83 c4 18             	add    $0x18,%esp
f0105903:	52                   	push   %edx
f0105904:	50                   	push   %eax
f0105905:	89 f2                	mov    %esi,%edx
f0105907:	89 f8                	mov    %edi,%eax
f0105909:	e8 9e ff ff ff       	call   f01058ac <printnum>
f010590e:	83 c4 20             	add    $0x20,%esp
f0105911:	eb 16                	jmp    f0105929 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0105913:	83 ec 08             	sub    $0x8,%esp
f0105916:	56                   	push   %esi
f0105917:	ff 75 18             	pushl  0x18(%ebp)
f010591a:	ff d7                	call   *%edi
f010591c:	83 c4 10             	add    $0x10,%esp
f010591f:	eb 03                	jmp    f0105924 <printnum+0x78>
f0105921:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0105924:	4b                   	dec    %ebx
f0105925:	85 db                	test   %ebx,%ebx
f0105927:	7f ea                	jg     f0105913 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0105929:	83 ec 08             	sub    $0x8,%esp
f010592c:	56                   	push   %esi
f010592d:	83 ec 04             	sub    $0x4,%esp
f0105930:	ff 75 e4             	pushl  -0x1c(%ebp)
f0105933:	ff 75 e0             	pushl  -0x20(%ebp)
f0105936:	ff 75 dc             	pushl  -0x24(%ebp)
f0105939:	ff 75 d8             	pushl  -0x28(%ebp)
f010593c:	e8 07 1a 00 00       	call   f0107348 <__umoddi3>
f0105941:	83 c4 14             	add    $0x14,%esp
f0105944:	0f be 80 20 96 10 f0 	movsbl -0xfef69e0(%eax),%eax
f010594b:	50                   	push   %eax
f010594c:	ff d7                	call   *%edi
}
f010594e:	83 c4 10             	add    $0x10,%esp
f0105951:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105954:	5b                   	pop    %ebx
f0105955:	5e                   	pop    %esi
f0105956:	5f                   	pop    %edi
f0105957:	5d                   	pop    %ebp
f0105958:	c3                   	ret    

f0105959 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0105959:	55                   	push   %ebp
f010595a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f010595c:	83 fa 01             	cmp    $0x1,%edx
f010595f:	7e 0e                	jle    f010596f <getuint+0x16>
		return va_arg(*ap, unsigned long long);
f0105961:	8b 10                	mov    (%eax),%edx
f0105963:	8d 4a 08             	lea    0x8(%edx),%ecx
f0105966:	89 08                	mov    %ecx,(%eax)
f0105968:	8b 02                	mov    (%edx),%eax
f010596a:	8b 52 04             	mov    0x4(%edx),%edx
f010596d:	eb 22                	jmp    f0105991 <getuint+0x38>
	else if (lflag)
f010596f:	85 d2                	test   %edx,%edx
f0105971:	74 10                	je     f0105983 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
f0105973:	8b 10                	mov    (%eax),%edx
f0105975:	8d 4a 04             	lea    0x4(%edx),%ecx
f0105978:	89 08                	mov    %ecx,(%eax)
f010597a:	8b 02                	mov    (%edx),%eax
f010597c:	ba 00 00 00 00       	mov    $0x0,%edx
f0105981:	eb 0e                	jmp    f0105991 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
f0105983:	8b 10                	mov    (%eax),%edx
f0105985:	8d 4a 04             	lea    0x4(%edx),%ecx
f0105988:	89 08                	mov    %ecx,(%eax)
f010598a:	8b 02                	mov    (%edx),%eax
f010598c:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0105991:	5d                   	pop    %ebp
f0105992:	c3                   	ret    

f0105993 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f0105993:	55                   	push   %ebp
f0105994:	89 e5                	mov    %esp,%ebp
f0105996:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
f0105999:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
f010599c:	8b 10                	mov    (%eax),%edx
f010599e:	3b 50 04             	cmp    0x4(%eax),%edx
f01059a1:	73 0a                	jae    f01059ad <sprintputch+0x1a>
		*b->buf++ = ch;
f01059a3:	8d 4a 01             	lea    0x1(%edx),%ecx
f01059a6:	89 08                	mov    %ecx,(%eax)
f01059a8:	8b 45 08             	mov    0x8(%ebp),%eax
f01059ab:	88 02                	mov    %al,(%edx)
}
f01059ad:	5d                   	pop    %ebp
f01059ae:	c3                   	ret    

f01059af <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f01059af:	55                   	push   %ebp
f01059b0:	89 e5                	mov    %esp,%ebp
f01059b2:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f01059b5:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f01059b8:	50                   	push   %eax
f01059b9:	ff 75 10             	pushl  0x10(%ebp)
f01059bc:	ff 75 0c             	pushl  0xc(%ebp)
f01059bf:	ff 75 08             	pushl  0x8(%ebp)
f01059c2:	e8 05 00 00 00       	call   f01059cc <vprintfmt>
	va_end(ap);
}
f01059c7:	83 c4 10             	add    $0x10,%esp
f01059ca:	c9                   	leave  
f01059cb:	c3                   	ret    

f01059cc <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f01059cc:	55                   	push   %ebp
f01059cd:	89 e5                	mov    %esp,%ebp
f01059cf:	57                   	push   %edi
f01059d0:	56                   	push   %esi
f01059d1:	53                   	push   %ebx
f01059d2:	83 ec 2c             	sub    $0x2c,%esp
f01059d5:	8b 75 08             	mov    0x8(%ebp),%esi
f01059d8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01059db:	8b 7d 10             	mov    0x10(%ebp),%edi
f01059de:	eb 12                	jmp    f01059f2 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
f01059e0:	85 c0                	test   %eax,%eax
f01059e2:	0f 84 68 03 00 00    	je     f0105d50 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
f01059e8:	83 ec 08             	sub    $0x8,%esp
f01059eb:	53                   	push   %ebx
f01059ec:	50                   	push   %eax
f01059ed:	ff d6                	call   *%esi
f01059ef:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f01059f2:	47                   	inc    %edi
f01059f3:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f01059f7:	83 f8 25             	cmp    $0x25,%eax
f01059fa:	75 e4                	jne    f01059e0 <vprintfmt+0x14>
f01059fc:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
f0105a00:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f0105a07:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0105a0e:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
f0105a15:	ba 00 00 00 00       	mov    $0x0,%edx
f0105a1a:	eb 07                	jmp    f0105a23 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a1c:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
f0105a1f:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a23:	8d 47 01             	lea    0x1(%edi),%eax
f0105a26:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0105a29:	0f b6 0f             	movzbl (%edi),%ecx
f0105a2c:	8a 07                	mov    (%edi),%al
f0105a2e:	83 e8 23             	sub    $0x23,%eax
f0105a31:	3c 55                	cmp    $0x55,%al
f0105a33:	0f 87 fe 02 00 00    	ja     f0105d37 <vprintfmt+0x36b>
f0105a39:	0f b6 c0             	movzbl %al,%eax
f0105a3c:	ff 24 85 60 97 10 f0 	jmp    *-0xfef68a0(,%eax,4)
f0105a43:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f0105a46:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
f0105a4a:	eb d7                	jmp    f0105a23 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a4c:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105a4f:	b8 00 00 00 00       	mov    $0x0,%eax
f0105a54:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f0105a57:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0105a5a:	01 c0                	add    %eax,%eax
f0105a5c:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
f0105a60:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
f0105a63:	8d 51 d0             	lea    -0x30(%ecx),%edx
f0105a66:	83 fa 09             	cmp    $0x9,%edx
f0105a69:	77 34                	ja     f0105a9f <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0105a6b:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f0105a6c:	eb e9                	jmp    f0105a57 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f0105a6e:	8b 45 14             	mov    0x14(%ebp),%eax
f0105a71:	8d 48 04             	lea    0x4(%eax),%ecx
f0105a74:	89 4d 14             	mov    %ecx,0x14(%ebp)
f0105a77:	8b 00                	mov    (%eax),%eax
f0105a79:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a7c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f0105a7f:	eb 24                	jmp    f0105aa5 <vprintfmt+0xd9>
f0105a81:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0105a85:	79 07                	jns    f0105a8e <vprintfmt+0xc2>
f0105a87:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105a8e:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105a91:	eb 90                	jmp    f0105a23 <vprintfmt+0x57>
f0105a93:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f0105a96:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
f0105a9d:	eb 84                	jmp    f0105a23 <vprintfmt+0x57>
f0105a9f:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0105aa2:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
f0105aa5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0105aa9:	0f 89 74 ff ff ff    	jns    f0105a23 <vprintfmt+0x57>
				width = precision, precision = -1;
f0105aaf:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0105ab2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0105ab5:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0105abc:	e9 62 ff ff ff       	jmp    f0105a23 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f0105ac1:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105ac2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f0105ac5:	e9 59 ff ff ff       	jmp    f0105a23 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f0105aca:	8b 45 14             	mov    0x14(%ebp),%eax
f0105acd:	8d 50 04             	lea    0x4(%eax),%edx
f0105ad0:	89 55 14             	mov    %edx,0x14(%ebp)
f0105ad3:	83 ec 08             	sub    $0x8,%esp
f0105ad6:	53                   	push   %ebx
f0105ad7:	ff 30                	pushl  (%eax)
f0105ad9:	ff d6                	call   *%esi
			break;
f0105adb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105ade:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
f0105ae1:	e9 0c ff ff ff       	jmp    f01059f2 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
f0105ae6:	8b 45 14             	mov    0x14(%ebp),%eax
f0105ae9:	8d 50 04             	lea    0x4(%eax),%edx
f0105aec:	89 55 14             	mov    %edx,0x14(%ebp)
f0105aef:	8b 00                	mov    (%eax),%eax
f0105af1:	85 c0                	test   %eax,%eax
f0105af3:	79 02                	jns    f0105af7 <vprintfmt+0x12b>
f0105af5:	f7 d8                	neg    %eax
f0105af7:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
f0105af9:	83 f8 0f             	cmp    $0xf,%eax
f0105afc:	7f 0b                	jg     f0105b09 <vprintfmt+0x13d>
f0105afe:	8b 04 85 c0 98 10 f0 	mov    -0xfef6740(,%eax,4),%eax
f0105b05:	85 c0                	test   %eax,%eax
f0105b07:	75 18                	jne    f0105b21 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
f0105b09:	52                   	push   %edx
f0105b0a:	68 38 96 10 f0       	push   $0xf0109638
f0105b0f:	53                   	push   %ebx
f0105b10:	56                   	push   %esi
f0105b11:	e8 99 fe ff ff       	call   f01059af <printfmt>
f0105b16:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105b19:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
f0105b1c:	e9 d1 fe ff ff       	jmp    f01059f2 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
f0105b21:	50                   	push   %eax
f0105b22:	68 63 75 10 f0       	push   $0xf0107563
f0105b27:	53                   	push   %ebx
f0105b28:	56                   	push   %esi
f0105b29:	e8 81 fe ff ff       	call   f01059af <printfmt>
f0105b2e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105b31:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105b34:	e9 b9 fe ff ff       	jmp    f01059f2 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0105b39:	8b 45 14             	mov    0x14(%ebp),%eax
f0105b3c:	8d 50 04             	lea    0x4(%eax),%edx
f0105b3f:	89 55 14             	mov    %edx,0x14(%ebp)
f0105b42:	8b 38                	mov    (%eax),%edi
f0105b44:	85 ff                	test   %edi,%edi
f0105b46:	75 05                	jne    f0105b4d <vprintfmt+0x181>
				p = "(null)";
f0105b48:	bf 31 96 10 f0       	mov    $0xf0109631,%edi
			if (width > 0 && padc != '-')
f0105b4d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0105b51:	0f 8e 90 00 00 00    	jle    f0105be7 <vprintfmt+0x21b>
f0105b57:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
f0105b5b:	0f 84 8e 00 00 00    	je     f0105bef <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
f0105b61:	83 ec 08             	sub    $0x8,%esp
f0105b64:	ff 75 d0             	pushl  -0x30(%ebp)
f0105b67:	57                   	push   %edi
f0105b68:	e8 74 03 00 00       	call   f0105ee1 <strnlen>
f0105b6d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f0105b70:	29 c1                	sub    %eax,%ecx
f0105b72:	89 4d cc             	mov    %ecx,-0x34(%ebp)
f0105b75:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
f0105b78:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
f0105b7c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0105b7f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f0105b82:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0105b84:	eb 0d                	jmp    f0105b93 <vprintfmt+0x1c7>
					putch(padc, putdat);
f0105b86:	83 ec 08             	sub    $0x8,%esp
f0105b89:	53                   	push   %ebx
f0105b8a:	ff 75 e4             	pushl  -0x1c(%ebp)
f0105b8d:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0105b8f:	4f                   	dec    %edi
f0105b90:	83 c4 10             	add    $0x10,%esp
f0105b93:	85 ff                	test   %edi,%edi
f0105b95:	7f ef                	jg     f0105b86 <vprintfmt+0x1ba>
f0105b97:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0105b9a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f0105b9d:	89 c8                	mov    %ecx,%eax
f0105b9f:	85 c9                	test   %ecx,%ecx
f0105ba1:	79 05                	jns    f0105ba8 <vprintfmt+0x1dc>
f0105ba3:	b8 00 00 00 00       	mov    $0x0,%eax
f0105ba8:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f0105bab:	29 c1                	sub    %eax,%ecx
f0105bad:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f0105bb0:	89 75 08             	mov    %esi,0x8(%ebp)
f0105bb3:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0105bb6:	eb 3d                	jmp    f0105bf5 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f0105bb8:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
f0105bbc:	74 19                	je     f0105bd7 <vprintfmt+0x20b>
f0105bbe:	0f be c0             	movsbl %al,%eax
f0105bc1:	83 e8 20             	sub    $0x20,%eax
f0105bc4:	83 f8 5e             	cmp    $0x5e,%eax
f0105bc7:	76 0e                	jbe    f0105bd7 <vprintfmt+0x20b>
					putch('?', putdat);
f0105bc9:	83 ec 08             	sub    $0x8,%esp
f0105bcc:	53                   	push   %ebx
f0105bcd:	6a 3f                	push   $0x3f
f0105bcf:	ff 55 08             	call   *0x8(%ebp)
f0105bd2:	83 c4 10             	add    $0x10,%esp
f0105bd5:	eb 0b                	jmp    f0105be2 <vprintfmt+0x216>
				else
					putch(ch, putdat);
f0105bd7:	83 ec 08             	sub    $0x8,%esp
f0105bda:	53                   	push   %ebx
f0105bdb:	52                   	push   %edx
f0105bdc:	ff 55 08             	call   *0x8(%ebp)
f0105bdf:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f0105be2:	ff 4d e4             	decl   -0x1c(%ebp)
f0105be5:	eb 0e                	jmp    f0105bf5 <vprintfmt+0x229>
f0105be7:	89 75 08             	mov    %esi,0x8(%ebp)
f0105bea:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0105bed:	eb 06                	jmp    f0105bf5 <vprintfmt+0x229>
f0105bef:	89 75 08             	mov    %esi,0x8(%ebp)
f0105bf2:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0105bf5:	47                   	inc    %edi
f0105bf6:	8a 47 ff             	mov    -0x1(%edi),%al
f0105bf9:	0f be d0             	movsbl %al,%edx
f0105bfc:	85 d2                	test   %edx,%edx
f0105bfe:	74 1d                	je     f0105c1d <vprintfmt+0x251>
f0105c00:	85 f6                	test   %esi,%esi
f0105c02:	78 b4                	js     f0105bb8 <vprintfmt+0x1ec>
f0105c04:	4e                   	dec    %esi
f0105c05:	79 b1                	jns    f0105bb8 <vprintfmt+0x1ec>
f0105c07:	8b 75 08             	mov    0x8(%ebp),%esi
f0105c0a:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105c0d:	eb 14                	jmp    f0105c23 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
f0105c0f:	83 ec 08             	sub    $0x8,%esp
f0105c12:	53                   	push   %ebx
f0105c13:	6a 20                	push   $0x20
f0105c15:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f0105c17:	4f                   	dec    %edi
f0105c18:	83 c4 10             	add    $0x10,%esp
f0105c1b:	eb 06                	jmp    f0105c23 <vprintfmt+0x257>
f0105c1d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0105c20:	8b 75 08             	mov    0x8(%ebp),%esi
f0105c23:	85 ff                	test   %edi,%edi
f0105c25:	7f e8                	jg     f0105c0f <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105c27:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105c2a:	e9 c3 fd ff ff       	jmp    f01059f2 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0105c2f:	83 fa 01             	cmp    $0x1,%edx
f0105c32:	7e 16                	jle    f0105c4a <vprintfmt+0x27e>
		return va_arg(*ap, long long);
f0105c34:	8b 45 14             	mov    0x14(%ebp),%eax
f0105c37:	8d 50 08             	lea    0x8(%eax),%edx
f0105c3a:	89 55 14             	mov    %edx,0x14(%ebp)
f0105c3d:	8b 50 04             	mov    0x4(%eax),%edx
f0105c40:	8b 00                	mov    (%eax),%eax
f0105c42:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0105c45:	89 55 dc             	mov    %edx,-0x24(%ebp)
f0105c48:	eb 32                	jmp    f0105c7c <vprintfmt+0x2b0>
	else if (lflag)
f0105c4a:	85 d2                	test   %edx,%edx
f0105c4c:	74 18                	je     f0105c66 <vprintfmt+0x29a>
		return va_arg(*ap, long);
f0105c4e:	8b 45 14             	mov    0x14(%ebp),%eax
f0105c51:	8d 50 04             	lea    0x4(%eax),%edx
f0105c54:	89 55 14             	mov    %edx,0x14(%ebp)
f0105c57:	8b 00                	mov    (%eax),%eax
f0105c59:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0105c5c:	89 c1                	mov    %eax,%ecx
f0105c5e:	c1 f9 1f             	sar    $0x1f,%ecx
f0105c61:	89 4d dc             	mov    %ecx,-0x24(%ebp)
f0105c64:	eb 16                	jmp    f0105c7c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
f0105c66:	8b 45 14             	mov    0x14(%ebp),%eax
f0105c69:	8d 50 04             	lea    0x4(%eax),%edx
f0105c6c:	89 55 14             	mov    %edx,0x14(%ebp)
f0105c6f:	8b 00                	mov    (%eax),%eax
f0105c71:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0105c74:	89 c1                	mov    %eax,%ecx
f0105c76:	c1 f9 1f             	sar    $0x1f,%ecx
f0105c79:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f0105c7c:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0105c7f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
f0105c82:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f0105c86:	79 76                	jns    f0105cfe <vprintfmt+0x332>
				putch('-', putdat);
f0105c88:	83 ec 08             	sub    $0x8,%esp
f0105c8b:	53                   	push   %ebx
f0105c8c:	6a 2d                	push   $0x2d
f0105c8e:	ff d6                	call   *%esi
				num = -(long long) num;
f0105c90:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0105c93:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0105c96:	f7 d8                	neg    %eax
f0105c98:	83 d2 00             	adc    $0x0,%edx
f0105c9b:	f7 da                	neg    %edx
f0105c9d:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
f0105ca0:	b9 0a 00 00 00       	mov    $0xa,%ecx
f0105ca5:	eb 5c                	jmp    f0105d03 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f0105ca7:	8d 45 14             	lea    0x14(%ebp),%eax
f0105caa:	e8 aa fc ff ff       	call   f0105959 <getuint>
			base = 10;
f0105caf:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
f0105cb4:	eb 4d                	jmp    f0105d03 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
f0105cb6:	8d 45 14             	lea    0x14(%ebp),%eax
f0105cb9:	e8 9b fc ff ff       	call   f0105959 <getuint>
			base = 8;
f0105cbe:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
f0105cc3:	eb 3e                	jmp    f0105d03 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
f0105cc5:	83 ec 08             	sub    $0x8,%esp
f0105cc8:	53                   	push   %ebx
f0105cc9:	6a 30                	push   $0x30
f0105ccb:	ff d6                	call   *%esi
			putch('x', putdat);
f0105ccd:	83 c4 08             	add    $0x8,%esp
f0105cd0:	53                   	push   %ebx
f0105cd1:	6a 78                	push   $0x78
f0105cd3:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f0105cd5:	8b 45 14             	mov    0x14(%ebp),%eax
f0105cd8:	8d 50 04             	lea    0x4(%eax),%edx
f0105cdb:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f0105cde:	8b 00                	mov    (%eax),%eax
f0105ce0:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
f0105ce5:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
f0105ce8:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
f0105ced:	eb 14                	jmp    f0105d03 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f0105cef:	8d 45 14             	lea    0x14(%ebp),%eax
f0105cf2:	e8 62 fc ff ff       	call   f0105959 <getuint>
			base = 16;
f0105cf7:	b9 10 00 00 00       	mov    $0x10,%ecx
f0105cfc:	eb 05                	jmp    f0105d03 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f0105cfe:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
f0105d03:	83 ec 0c             	sub    $0xc,%esp
f0105d06:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
f0105d0a:	57                   	push   %edi
f0105d0b:	ff 75 e4             	pushl  -0x1c(%ebp)
f0105d0e:	51                   	push   %ecx
f0105d0f:	52                   	push   %edx
f0105d10:	50                   	push   %eax
f0105d11:	89 da                	mov    %ebx,%edx
f0105d13:	89 f0                	mov    %esi,%eax
f0105d15:	e8 92 fb ff ff       	call   f01058ac <printnum>
			break;
f0105d1a:	83 c4 20             	add    $0x20,%esp
f0105d1d:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0105d20:	e9 cd fc ff ff       	jmp    f01059f2 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0105d25:	83 ec 08             	sub    $0x8,%esp
f0105d28:	53                   	push   %ebx
f0105d29:	51                   	push   %ecx
f0105d2a:	ff d6                	call   *%esi
			break;
f0105d2c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0105d2f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
f0105d32:	e9 bb fc ff ff       	jmp    f01059f2 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0105d37:	83 ec 08             	sub    $0x8,%esp
f0105d3a:	53                   	push   %ebx
f0105d3b:	6a 25                	push   $0x25
f0105d3d:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
f0105d3f:	83 c4 10             	add    $0x10,%esp
f0105d42:	eb 01                	jmp    f0105d45 <vprintfmt+0x379>
f0105d44:	4f                   	dec    %edi
f0105d45:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
f0105d49:	75 f9                	jne    f0105d44 <vprintfmt+0x378>
f0105d4b:	e9 a2 fc ff ff       	jmp    f01059f2 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
f0105d50:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105d53:	5b                   	pop    %ebx
f0105d54:	5e                   	pop    %esi
f0105d55:	5f                   	pop    %edi
f0105d56:	5d                   	pop    %ebp
f0105d57:	c3                   	ret    

f0105d58 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f0105d58:	55                   	push   %ebp
f0105d59:	89 e5                	mov    %esp,%ebp
f0105d5b:	83 ec 18             	sub    $0x18,%esp
f0105d5e:	8b 45 08             	mov    0x8(%ebp),%eax
f0105d61:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f0105d64:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0105d67:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f0105d6b:	89 4d f0             	mov    %ecx,-0x10(%ebp)
f0105d6e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f0105d75:	85 c0                	test   %eax,%eax
f0105d77:	74 26                	je     f0105d9f <vsnprintf+0x47>
f0105d79:	85 d2                	test   %edx,%edx
f0105d7b:	7e 29                	jle    f0105da6 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f0105d7d:	ff 75 14             	pushl  0x14(%ebp)
f0105d80:	ff 75 10             	pushl  0x10(%ebp)
f0105d83:	8d 45 ec             	lea    -0x14(%ebp),%eax
f0105d86:	50                   	push   %eax
f0105d87:	68 93 59 10 f0       	push   $0xf0105993
f0105d8c:	e8 3b fc ff ff       	call   f01059cc <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f0105d91:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0105d94:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f0105d97:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0105d9a:	83 c4 10             	add    $0x10,%esp
f0105d9d:	eb 0c                	jmp    f0105dab <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f0105d9f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f0105da4:	eb 05                	jmp    f0105dab <vsnprintf+0x53>
f0105da6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f0105dab:	c9                   	leave  
f0105dac:	c3                   	ret    

f0105dad <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f0105dad:	55                   	push   %ebp
f0105dae:	89 e5                	mov    %esp,%ebp
f0105db0:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f0105db3:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f0105db6:	50                   	push   %eax
f0105db7:	ff 75 10             	pushl  0x10(%ebp)
f0105dba:	ff 75 0c             	pushl  0xc(%ebp)
f0105dbd:	ff 75 08             	pushl  0x8(%ebp)
f0105dc0:	e8 93 ff ff ff       	call   f0105d58 <vsnprintf>
	va_end(ap);

	return rc;
}
f0105dc5:	c9                   	leave  
f0105dc6:	c3                   	ret    

f0105dc7 <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f0105dc7:	55                   	push   %ebp
f0105dc8:	89 e5                	mov    %esp,%ebp
f0105dca:	57                   	push   %edi
f0105dcb:	56                   	push   %esi
f0105dcc:	53                   	push   %ebx
f0105dcd:	83 ec 0c             	sub    $0xc,%esp
f0105dd0:	8b 45 08             	mov    0x8(%ebp),%eax
	int i, c, echoing;

#if JOS_KERNEL
	if (prompt != NULL)
f0105dd3:	85 c0                	test   %eax,%eax
f0105dd5:	74 11                	je     f0105de8 <readline+0x21>
		cprintf("%s", prompt);
f0105dd7:	83 ec 08             	sub    $0x8,%esp
f0105dda:	50                   	push   %eax
f0105ddb:	68 63 75 10 f0       	push   $0xf0107563
f0105de0:	e8 2c de ff ff       	call   f0103c11 <cprintf>
f0105de5:	83 c4 10             	add    $0x10,%esp
	if (prompt != NULL)
		fprintf(1, "%s", prompt);
#endif

	i = 0;
	echoing = iscons(0);
f0105de8:	83 ec 0c             	sub    $0xc,%esp
f0105deb:	6a 00                	push   $0x0
f0105ded:	e8 23 aa ff ff       	call   f0100815 <iscons>
f0105df2:	89 c7                	mov    %eax,%edi
f0105df4:	83 c4 10             	add    $0x10,%esp
#else
	if (prompt != NULL)
		fprintf(1, "%s", prompt);
#endif

	i = 0;
f0105df7:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
f0105dfc:	e8 03 aa ff ff       	call   f0100804 <getchar>
f0105e01:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f0105e03:	85 c0                	test   %eax,%eax
f0105e05:	79 24                	jns    f0105e2b <readline+0x64>
			if (c != -E_EOF)
f0105e07:	83 f8 f8             	cmp    $0xfffffff8,%eax
f0105e0a:	0f 84 90 00 00 00    	je     f0105ea0 <readline+0xd9>
				cprintf("read error: %e\n", c);
f0105e10:	83 ec 08             	sub    $0x8,%esp
f0105e13:	50                   	push   %eax
f0105e14:	68 1f 99 10 f0       	push   $0xf010991f
f0105e19:	e8 f3 dd ff ff       	call   f0103c11 <cprintf>
f0105e1e:	83 c4 10             	add    $0x10,%esp
			return NULL;
f0105e21:	b8 00 00 00 00       	mov    $0x0,%eax
f0105e26:	e9 98 00 00 00       	jmp    f0105ec3 <readline+0xfc>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0105e2b:	83 f8 08             	cmp    $0x8,%eax
f0105e2e:	74 7d                	je     f0105ead <readline+0xe6>
f0105e30:	83 f8 7f             	cmp    $0x7f,%eax
f0105e33:	75 16                	jne    f0105e4b <readline+0x84>
f0105e35:	eb 70                	jmp    f0105ea7 <readline+0xe0>
			if (echoing)
f0105e37:	85 ff                	test   %edi,%edi
f0105e39:	74 0d                	je     f0105e48 <readline+0x81>
				cputchar('\b');
f0105e3b:	83 ec 0c             	sub    $0xc,%esp
f0105e3e:	6a 08                	push   $0x8
f0105e40:	e8 af a9 ff ff       	call   f01007f4 <cputchar>
f0105e45:	83 c4 10             	add    $0x10,%esp
			i--;
f0105e48:	4e                   	dec    %esi
f0105e49:	eb b1                	jmp    f0105dfc <readline+0x35>
		} else if (c >= ' ' && i < BUFLEN-1) {
f0105e4b:	83 f8 1f             	cmp    $0x1f,%eax
f0105e4e:	7e 23                	jle    f0105e73 <readline+0xac>
f0105e50:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0105e56:	7f 1b                	jg     f0105e73 <readline+0xac>
			if (echoing)
f0105e58:	85 ff                	test   %edi,%edi
f0105e5a:	74 0c                	je     f0105e68 <readline+0xa1>
				cputchar(c);
f0105e5c:	83 ec 0c             	sub    $0xc,%esp
f0105e5f:	53                   	push   %ebx
f0105e60:	e8 8f a9 ff ff       	call   f01007f4 <cputchar>
f0105e65:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f0105e68:	88 9e 80 3a 23 f0    	mov    %bl,-0xfdcc580(%esi)
f0105e6e:	8d 76 01             	lea    0x1(%esi),%esi
f0105e71:	eb 89                	jmp    f0105dfc <readline+0x35>
		} else if (c == '\n' || c == '\r') {
f0105e73:	83 fb 0a             	cmp    $0xa,%ebx
f0105e76:	74 09                	je     f0105e81 <readline+0xba>
f0105e78:	83 fb 0d             	cmp    $0xd,%ebx
f0105e7b:	0f 85 7b ff ff ff    	jne    f0105dfc <readline+0x35>
			if (echoing)
f0105e81:	85 ff                	test   %edi,%edi
f0105e83:	74 0d                	je     f0105e92 <readline+0xcb>
				cputchar('\n');
f0105e85:	83 ec 0c             	sub    $0xc,%esp
f0105e88:	6a 0a                	push   $0xa
f0105e8a:	e8 65 a9 ff ff       	call   f01007f4 <cputchar>
f0105e8f:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
f0105e92:	c6 86 80 3a 23 f0 00 	movb   $0x0,-0xfdcc580(%esi)
			return buf;
f0105e99:	b8 80 3a 23 f0       	mov    $0xf0233a80,%eax
f0105e9e:	eb 23                	jmp    f0105ec3 <readline+0xfc>
	while (1) {
		c = getchar();
		if (c < 0) {
			if (c != -E_EOF)
				cprintf("read error: %e\n", c);
			return NULL;
f0105ea0:	b8 00 00 00 00       	mov    $0x0,%eax
f0105ea5:	eb 1c                	jmp    f0105ec3 <readline+0xfc>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0105ea7:	85 f6                	test   %esi,%esi
f0105ea9:	7f 8c                	jg     f0105e37 <readline+0x70>
f0105eab:	eb 09                	jmp    f0105eb6 <readline+0xef>
f0105ead:	85 f6                	test   %esi,%esi
f0105eaf:	7f 86                	jg     f0105e37 <readline+0x70>
f0105eb1:	e9 46 ff ff ff       	jmp    f0105dfc <readline+0x35>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
f0105eb6:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0105ebc:	7e 9a                	jle    f0105e58 <readline+0x91>
f0105ebe:	e9 39 ff ff ff       	jmp    f0105dfc <readline+0x35>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
f0105ec3:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0105ec6:	5b                   	pop    %ebx
f0105ec7:	5e                   	pop    %esi
f0105ec8:	5f                   	pop    %edi
f0105ec9:	5d                   	pop    %ebp
f0105eca:	c3                   	ret    

f0105ecb <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f0105ecb:	55                   	push   %ebp
f0105ecc:	89 e5                	mov    %esp,%ebp
f0105ece:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f0105ed1:	b8 00 00 00 00       	mov    $0x0,%eax
f0105ed6:	eb 01                	jmp    f0105ed9 <strlen+0xe>
		n++;
f0105ed8:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f0105ed9:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f0105edd:	75 f9                	jne    f0105ed8 <strlen+0xd>
		n++;
	return n;
}
f0105edf:	5d                   	pop    %ebp
f0105ee0:	c3                   	ret    

f0105ee1 <strnlen>:

int
strnlen(const char *s, size_t size)
{
f0105ee1:	55                   	push   %ebp
f0105ee2:	89 e5                	mov    %esp,%ebp
f0105ee4:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0105ee7:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0105eea:	ba 00 00 00 00       	mov    $0x0,%edx
f0105eef:	eb 01                	jmp    f0105ef2 <strnlen+0x11>
		n++;
f0105ef1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0105ef2:	39 c2                	cmp    %eax,%edx
f0105ef4:	74 08                	je     f0105efe <strnlen+0x1d>
f0105ef6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
f0105efa:	75 f5                	jne    f0105ef1 <strnlen+0x10>
f0105efc:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
f0105efe:	5d                   	pop    %ebp
f0105eff:	c3                   	ret    

f0105f00 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f0105f00:	55                   	push   %ebp
f0105f01:	89 e5                	mov    %esp,%ebp
f0105f03:	53                   	push   %ebx
f0105f04:	8b 45 08             	mov    0x8(%ebp),%eax
f0105f07:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
f0105f0a:	89 c2                	mov    %eax,%edx
f0105f0c:	42                   	inc    %edx
f0105f0d:	41                   	inc    %ecx
f0105f0e:	8a 59 ff             	mov    -0x1(%ecx),%bl
f0105f11:	88 5a ff             	mov    %bl,-0x1(%edx)
f0105f14:	84 db                	test   %bl,%bl
f0105f16:	75 f4                	jne    f0105f0c <strcpy+0xc>
		/* do nothing */;
	return ret;
}
f0105f18:	5b                   	pop    %ebx
f0105f19:	5d                   	pop    %ebp
f0105f1a:	c3                   	ret    

f0105f1b <strcat>:

char *
strcat(char *dst, const char *src)
{
f0105f1b:	55                   	push   %ebp
f0105f1c:	89 e5                	mov    %esp,%ebp
f0105f1e:	53                   	push   %ebx
f0105f1f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
f0105f22:	53                   	push   %ebx
f0105f23:	e8 a3 ff ff ff       	call   f0105ecb <strlen>
f0105f28:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
f0105f2b:	ff 75 0c             	pushl  0xc(%ebp)
f0105f2e:	01 d8                	add    %ebx,%eax
f0105f30:	50                   	push   %eax
f0105f31:	e8 ca ff ff ff       	call   f0105f00 <strcpy>
	return dst;
}
f0105f36:	89 d8                	mov    %ebx,%eax
f0105f38:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0105f3b:	c9                   	leave  
f0105f3c:	c3                   	ret    

f0105f3d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
f0105f3d:	55                   	push   %ebp
f0105f3e:	89 e5                	mov    %esp,%ebp
f0105f40:	56                   	push   %esi
f0105f41:	53                   	push   %ebx
f0105f42:	8b 75 08             	mov    0x8(%ebp),%esi
f0105f45:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0105f48:	89 f3                	mov    %esi,%ebx
f0105f4a:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0105f4d:	89 f2                	mov    %esi,%edx
f0105f4f:	eb 0c                	jmp    f0105f5d <strncpy+0x20>
		*dst++ = *src;
f0105f51:	42                   	inc    %edx
f0105f52:	8a 01                	mov    (%ecx),%al
f0105f54:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f0105f57:	80 39 01             	cmpb   $0x1,(%ecx)
f0105f5a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0105f5d:	39 da                	cmp    %ebx,%edx
f0105f5f:	75 f0                	jne    f0105f51 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f0105f61:	89 f0                	mov    %esi,%eax
f0105f63:	5b                   	pop    %ebx
f0105f64:	5e                   	pop    %esi
f0105f65:	5d                   	pop    %ebp
f0105f66:	c3                   	ret    

f0105f67 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f0105f67:	55                   	push   %ebp
f0105f68:	89 e5                	mov    %esp,%ebp
f0105f6a:	56                   	push   %esi
f0105f6b:	53                   	push   %ebx
f0105f6c:	8b 75 08             	mov    0x8(%ebp),%esi
f0105f6f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0105f72:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
f0105f75:	85 c0                	test   %eax,%eax
f0105f77:	74 1e                	je     f0105f97 <strlcpy+0x30>
f0105f79:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f0105f7d:	89 f2                	mov    %esi,%edx
f0105f7f:	eb 05                	jmp    f0105f86 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
f0105f81:	42                   	inc    %edx
f0105f82:	41                   	inc    %ecx
f0105f83:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f0105f86:	39 c2                	cmp    %eax,%edx
f0105f88:	74 08                	je     f0105f92 <strlcpy+0x2b>
f0105f8a:	8a 19                	mov    (%ecx),%bl
f0105f8c:	84 db                	test   %bl,%bl
f0105f8e:	75 f1                	jne    f0105f81 <strlcpy+0x1a>
f0105f90:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
f0105f92:	c6 00 00             	movb   $0x0,(%eax)
f0105f95:	eb 02                	jmp    f0105f99 <strlcpy+0x32>
f0105f97:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
f0105f99:	29 f0                	sub    %esi,%eax
}
f0105f9b:	5b                   	pop    %ebx
f0105f9c:	5e                   	pop    %esi
f0105f9d:	5d                   	pop    %ebp
f0105f9e:	c3                   	ret    

f0105f9f <strcmp>:

int
strcmp(const char *p, const char *q)
{
f0105f9f:	55                   	push   %ebp
f0105fa0:	89 e5                	mov    %esp,%ebp
f0105fa2:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0105fa5:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
f0105fa8:	eb 02                	jmp    f0105fac <strcmp+0xd>
		p++, q++;
f0105faa:	41                   	inc    %ecx
f0105fab:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f0105fac:	8a 01                	mov    (%ecx),%al
f0105fae:	84 c0                	test   %al,%al
f0105fb0:	74 04                	je     f0105fb6 <strcmp+0x17>
f0105fb2:	3a 02                	cmp    (%edx),%al
f0105fb4:	74 f4                	je     f0105faa <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f0105fb6:	0f b6 c0             	movzbl %al,%eax
f0105fb9:	0f b6 12             	movzbl (%edx),%edx
f0105fbc:	29 d0                	sub    %edx,%eax
}
f0105fbe:	5d                   	pop    %ebp
f0105fbf:	c3                   	ret    

f0105fc0 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
f0105fc0:	55                   	push   %ebp
f0105fc1:	89 e5                	mov    %esp,%ebp
f0105fc3:	53                   	push   %ebx
f0105fc4:	8b 45 08             	mov    0x8(%ebp),%eax
f0105fc7:	8b 55 0c             	mov    0xc(%ebp),%edx
f0105fca:	89 c3                	mov    %eax,%ebx
f0105fcc:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
f0105fcf:	eb 02                	jmp    f0105fd3 <strncmp+0x13>
		n--, p++, q++;
f0105fd1:	40                   	inc    %eax
f0105fd2:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
f0105fd3:	39 d8                	cmp    %ebx,%eax
f0105fd5:	74 14                	je     f0105feb <strncmp+0x2b>
f0105fd7:	8a 08                	mov    (%eax),%cl
f0105fd9:	84 c9                	test   %cl,%cl
f0105fdb:	74 04                	je     f0105fe1 <strncmp+0x21>
f0105fdd:	3a 0a                	cmp    (%edx),%cl
f0105fdf:	74 f0                	je     f0105fd1 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0105fe1:	0f b6 00             	movzbl (%eax),%eax
f0105fe4:	0f b6 12             	movzbl (%edx),%edx
f0105fe7:	29 d0                	sub    %edx,%eax
f0105fe9:	eb 05                	jmp    f0105ff0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
f0105feb:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
f0105ff0:	5b                   	pop    %ebx
f0105ff1:	5d                   	pop    %ebp
f0105ff2:	c3                   	ret    

f0105ff3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f0105ff3:	55                   	push   %ebp
f0105ff4:	89 e5                	mov    %esp,%ebp
f0105ff6:	8b 45 08             	mov    0x8(%ebp),%eax
f0105ff9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0105ffc:	eb 05                	jmp    f0106003 <strchr+0x10>
		if (*s == c)
f0105ffe:	38 ca                	cmp    %cl,%dl
f0106000:	74 0c                	je     f010600e <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f0106002:	40                   	inc    %eax
f0106003:	8a 10                	mov    (%eax),%dl
f0106005:	84 d2                	test   %dl,%dl
f0106007:	75 f5                	jne    f0105ffe <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
f0106009:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010600e:	5d                   	pop    %ebp
f010600f:	c3                   	ret    

f0106010 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f0106010:	55                   	push   %ebp
f0106011:	89 e5                	mov    %esp,%ebp
f0106013:	8b 45 08             	mov    0x8(%ebp),%eax
f0106016:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0106019:	eb 05                	jmp    f0106020 <strfind+0x10>
		if (*s == c)
f010601b:	38 ca                	cmp    %cl,%dl
f010601d:	74 07                	je     f0106026 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f010601f:	40                   	inc    %eax
f0106020:	8a 10                	mov    (%eax),%dl
f0106022:	84 d2                	test   %dl,%dl
f0106024:	75 f5                	jne    f010601b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
f0106026:	5d                   	pop    %ebp
f0106027:	c3                   	ret    

f0106028 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
f0106028:	55                   	push   %ebp
f0106029:	89 e5                	mov    %esp,%ebp
f010602b:	57                   	push   %edi
f010602c:	56                   	push   %esi
f010602d:	53                   	push   %ebx
f010602e:	8b 7d 08             	mov    0x8(%ebp),%edi
f0106031:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
f0106034:	85 c9                	test   %ecx,%ecx
f0106036:	74 36                	je     f010606e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
f0106038:	f7 c7 03 00 00 00    	test   $0x3,%edi
f010603e:	75 28                	jne    f0106068 <memset+0x40>
f0106040:	f6 c1 03             	test   $0x3,%cl
f0106043:	75 23                	jne    f0106068 <memset+0x40>
		c &= 0xFF;
f0106045:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f0106049:	89 d3                	mov    %edx,%ebx
f010604b:	c1 e3 08             	shl    $0x8,%ebx
f010604e:	89 d6                	mov    %edx,%esi
f0106050:	c1 e6 18             	shl    $0x18,%esi
f0106053:	89 d0                	mov    %edx,%eax
f0106055:	c1 e0 10             	shl    $0x10,%eax
f0106058:	09 f0                	or     %esi,%eax
f010605a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
f010605c:	89 d8                	mov    %ebx,%eax
f010605e:	09 d0                	or     %edx,%eax
f0106060:	c1 e9 02             	shr    $0x2,%ecx
f0106063:	fc                   	cld    
f0106064:	f3 ab                	rep stos %eax,%es:(%edi)
f0106066:	eb 06                	jmp    f010606e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
f0106068:	8b 45 0c             	mov    0xc(%ebp),%eax
f010606b:	fc                   	cld    
f010606c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
f010606e:	89 f8                	mov    %edi,%eax
f0106070:	5b                   	pop    %ebx
f0106071:	5e                   	pop    %esi
f0106072:	5f                   	pop    %edi
f0106073:	5d                   	pop    %ebp
f0106074:	c3                   	ret    

f0106075 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
f0106075:	55                   	push   %ebp
f0106076:	89 e5                	mov    %esp,%ebp
f0106078:	57                   	push   %edi
f0106079:	56                   	push   %esi
f010607a:	8b 45 08             	mov    0x8(%ebp),%eax
f010607d:	8b 75 0c             	mov    0xc(%ebp),%esi
f0106080:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
f0106083:	39 c6                	cmp    %eax,%esi
f0106085:	73 33                	jae    f01060ba <memmove+0x45>
f0106087:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f010608a:	39 d0                	cmp    %edx,%eax
f010608c:	73 2c                	jae    f01060ba <memmove+0x45>
		s += n;
		d += n;
f010608e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f0106091:	89 d6                	mov    %edx,%esi
f0106093:	09 fe                	or     %edi,%esi
f0106095:	f7 c6 03 00 00 00    	test   $0x3,%esi
f010609b:	75 13                	jne    f01060b0 <memmove+0x3b>
f010609d:	f6 c1 03             	test   $0x3,%cl
f01060a0:	75 0e                	jne    f01060b0 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
f01060a2:	83 ef 04             	sub    $0x4,%edi
f01060a5:	8d 72 fc             	lea    -0x4(%edx),%esi
f01060a8:	c1 e9 02             	shr    $0x2,%ecx
f01060ab:	fd                   	std    
f01060ac:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01060ae:	eb 07                	jmp    f01060b7 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
f01060b0:	4f                   	dec    %edi
f01060b1:	8d 72 ff             	lea    -0x1(%edx),%esi
f01060b4:	fd                   	std    
f01060b5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f01060b7:	fc                   	cld    
f01060b8:	eb 1d                	jmp    f01060d7 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f01060ba:	89 f2                	mov    %esi,%edx
f01060bc:	09 c2                	or     %eax,%edx
f01060be:	f6 c2 03             	test   $0x3,%dl
f01060c1:	75 0f                	jne    f01060d2 <memmove+0x5d>
f01060c3:	f6 c1 03             	test   $0x3,%cl
f01060c6:	75 0a                	jne    f01060d2 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
f01060c8:	c1 e9 02             	shr    $0x2,%ecx
f01060cb:	89 c7                	mov    %eax,%edi
f01060cd:	fc                   	cld    
f01060ce:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01060d0:	eb 05                	jmp    f01060d7 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
f01060d2:	89 c7                	mov    %eax,%edi
f01060d4:	fc                   	cld    
f01060d5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
f01060d7:	5e                   	pop    %esi
f01060d8:	5f                   	pop    %edi
f01060d9:	5d                   	pop    %ebp
f01060da:	c3                   	ret    

f01060db <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
f01060db:	55                   	push   %ebp
f01060dc:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
f01060de:	ff 75 10             	pushl  0x10(%ebp)
f01060e1:	ff 75 0c             	pushl  0xc(%ebp)
f01060e4:	ff 75 08             	pushl  0x8(%ebp)
f01060e7:	e8 89 ff ff ff       	call   f0106075 <memmove>
}
f01060ec:	c9                   	leave  
f01060ed:	c3                   	ret    

f01060ee <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
f01060ee:	55                   	push   %ebp
f01060ef:	89 e5                	mov    %esp,%ebp
f01060f1:	56                   	push   %esi
f01060f2:	53                   	push   %ebx
f01060f3:	8b 45 08             	mov    0x8(%ebp),%eax
f01060f6:	8b 55 0c             	mov    0xc(%ebp),%edx
f01060f9:	89 c6                	mov    %eax,%esi
f01060fb:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f01060fe:	eb 14                	jmp    f0106114 <memcmp+0x26>
		if (*s1 != *s2)
f0106100:	8a 08                	mov    (%eax),%cl
f0106102:	8a 1a                	mov    (%edx),%bl
f0106104:	38 d9                	cmp    %bl,%cl
f0106106:	74 0a                	je     f0106112 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
f0106108:	0f b6 c1             	movzbl %cl,%eax
f010610b:	0f b6 db             	movzbl %bl,%ebx
f010610e:	29 d8                	sub    %ebx,%eax
f0106110:	eb 0b                	jmp    f010611d <memcmp+0x2f>
		s1++, s2++;
f0106112:	40                   	inc    %eax
f0106113:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0106114:	39 f0                	cmp    %esi,%eax
f0106116:	75 e8                	jne    f0106100 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f0106118:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010611d:	5b                   	pop    %ebx
f010611e:	5e                   	pop    %esi
f010611f:	5d                   	pop    %ebp
f0106120:	c3                   	ret    

f0106121 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
f0106121:	55                   	push   %ebp
f0106122:	89 e5                	mov    %esp,%ebp
f0106124:	53                   	push   %ebx
f0106125:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
f0106128:	89 c1                	mov    %eax,%ecx
f010612a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
f010612d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0106131:	eb 08                	jmp    f010613b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
f0106133:	0f b6 10             	movzbl (%eax),%edx
f0106136:	39 da                	cmp    %ebx,%edx
f0106138:	74 05                	je     f010613f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f010613a:	40                   	inc    %eax
f010613b:	39 c8                	cmp    %ecx,%eax
f010613d:	72 f4                	jb     f0106133 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
f010613f:	5b                   	pop    %ebx
f0106140:	5d                   	pop    %ebp
f0106141:	c3                   	ret    

f0106142 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f0106142:	55                   	push   %ebp
f0106143:	89 e5                	mov    %esp,%ebp
f0106145:	57                   	push   %edi
f0106146:	56                   	push   %esi
f0106147:	53                   	push   %ebx
f0106148:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f010614b:	eb 01                	jmp    f010614e <strtol+0xc>
		s++;
f010614d:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f010614e:	8a 01                	mov    (%ecx),%al
f0106150:	3c 20                	cmp    $0x20,%al
f0106152:	74 f9                	je     f010614d <strtol+0xb>
f0106154:	3c 09                	cmp    $0x9,%al
f0106156:	74 f5                	je     f010614d <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
f0106158:	3c 2b                	cmp    $0x2b,%al
f010615a:	75 08                	jne    f0106164 <strtol+0x22>
		s++;
f010615c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f010615d:	bf 00 00 00 00       	mov    $0x0,%edi
f0106162:	eb 11                	jmp    f0106175 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f0106164:	3c 2d                	cmp    $0x2d,%al
f0106166:	75 08                	jne    f0106170 <strtol+0x2e>
		s++, neg = 1;
f0106168:	41                   	inc    %ecx
f0106169:	bf 01 00 00 00       	mov    $0x1,%edi
f010616e:	eb 05                	jmp    f0106175 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0106170:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0106175:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0106179:	0f 84 87 00 00 00    	je     f0106206 <strtol+0xc4>
f010617f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f0106183:	75 27                	jne    f01061ac <strtol+0x6a>
f0106185:	80 39 30             	cmpb   $0x30,(%ecx)
f0106188:	75 22                	jne    f01061ac <strtol+0x6a>
f010618a:	e9 88 00 00 00       	jmp    f0106217 <strtol+0xd5>
		s += 2, base = 16;
f010618f:	83 c1 02             	add    $0x2,%ecx
f0106192:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f0106199:	eb 11                	jmp    f01061ac <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
f010619b:	41                   	inc    %ecx
f010619c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f01061a3:	eb 07                	jmp    f01061ac <strtol+0x6a>
	else if (base == 0)
		base = 10;
f01061a5:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
f01061ac:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f01061b1:	8a 11                	mov    (%ecx),%dl
f01061b3:	8d 5a d0             	lea    -0x30(%edx),%ebx
f01061b6:	80 fb 09             	cmp    $0x9,%bl
f01061b9:	77 08                	ja     f01061c3 <strtol+0x81>
			dig = *s - '0';
f01061bb:	0f be d2             	movsbl %dl,%edx
f01061be:	83 ea 30             	sub    $0x30,%edx
f01061c1:	eb 22                	jmp    f01061e5 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
f01061c3:	8d 72 9f             	lea    -0x61(%edx),%esi
f01061c6:	89 f3                	mov    %esi,%ebx
f01061c8:	80 fb 19             	cmp    $0x19,%bl
f01061cb:	77 08                	ja     f01061d5 <strtol+0x93>
			dig = *s - 'a' + 10;
f01061cd:	0f be d2             	movsbl %dl,%edx
f01061d0:	83 ea 57             	sub    $0x57,%edx
f01061d3:	eb 10                	jmp    f01061e5 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
f01061d5:	8d 72 bf             	lea    -0x41(%edx),%esi
f01061d8:	89 f3                	mov    %esi,%ebx
f01061da:	80 fb 19             	cmp    $0x19,%bl
f01061dd:	77 14                	ja     f01061f3 <strtol+0xb1>
			dig = *s - 'A' + 10;
f01061df:	0f be d2             	movsbl %dl,%edx
f01061e2:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
f01061e5:	3b 55 10             	cmp    0x10(%ebp),%edx
f01061e8:	7d 09                	jge    f01061f3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
f01061ea:	41                   	inc    %ecx
f01061eb:	0f af 45 10          	imul   0x10(%ebp),%eax
f01061ef:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
f01061f1:	eb be                	jmp    f01061b1 <strtol+0x6f>

	if (endptr)
f01061f3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f01061f7:	74 05                	je     f01061fe <strtol+0xbc>
		*endptr = (char *) s;
f01061f9:	8b 75 0c             	mov    0xc(%ebp),%esi
f01061fc:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
f01061fe:	85 ff                	test   %edi,%edi
f0106200:	74 21                	je     f0106223 <strtol+0xe1>
f0106202:	f7 d8                	neg    %eax
f0106204:	eb 1d                	jmp    f0106223 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0106206:	80 39 30             	cmpb   $0x30,(%ecx)
f0106209:	75 9a                	jne    f01061a5 <strtol+0x63>
f010620b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f010620f:	0f 84 7a ff ff ff    	je     f010618f <strtol+0x4d>
f0106215:	eb 84                	jmp    f010619b <strtol+0x59>
f0106217:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f010621b:	0f 84 6e ff ff ff    	je     f010618f <strtol+0x4d>
f0106221:	eb 89                	jmp    f01061ac <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
f0106223:	5b                   	pop    %ebx
f0106224:	5e                   	pop    %esi
f0106225:	5f                   	pop    %edi
f0106226:	5d                   	pop    %ebp
f0106227:	c3                   	ret    

f0106228 <mpentry_start>:
.set PROT_MODE_DSEG, 0x10			# kernel data segment selector

.code16
.globl mpentry_start
mpentry_start:
	cli
f0106228:	fa                   	cli    

	xorw	%ax, %ax
f0106229:	31 c0                	xor    %eax,%eax
	movw	%ax, %ds
f010622b:	8e d8                	mov    %eax,%ds
	movw	%ax, %es
f010622d:	8e c0                	mov    %eax,%es
	movw	%ax, %ss
f010622f:	8e d0                	mov    %eax,%ss

	lgdt	MPBOOTPHYS(gdtdesc) # load the GDTR
f0106231:	0f 01 16             	lgdtl  (%esi)
f0106234:	7c 70                	jl     f01062a6 <gdtdesc+0x2>
	movl	%cr0, %eax
f0106236:	0f 20 c0             	mov    %cr0,%eax
	orl	$CR0_PE, %eax
f0106239:	66 83 c8 01          	or     $0x1,%ax
	movl	%eax, %cr0
f010623d:	0f 22 c0             	mov    %eax,%cr0

	ljmpl	$(PROT_MODE_CSEG), $(MPBOOTPHYS(start32))
f0106240:	66 ea 20 70 00 00    	ljmpw  $0x0,$0x7020
f0106246:	08 00                	or     %al,(%eax)

f0106248 <start32>:

.code32
start32:
	movw	$(PROT_MODE_DSEG), %ax
f0106248:	66 b8 10 00          	mov    $0x10,%ax
	movw	%ax, %ds
f010624c:	8e d8                	mov    %eax,%ds
	movw	%ax, %es
f010624e:	8e c0                	mov    %eax,%es
	movw	%ax, %ss
f0106250:	8e d0                	mov    %eax,%ss
	movw	$0, %ax
f0106252:	66 b8 00 00          	mov    $0x0,%ax
	movw	%ax, %fs
f0106256:	8e e0                	mov    %eax,%fs
	movw	%ax, %gs
f0106258:	8e e8                	mov    %eax,%gs

	# Set up initial page table. We cannot use kern_pgdir yet because
	# we are still running at a low EIP.
	movl	$(RELOC(entry_pgdir)), %eax
f010625a:	b8 00 40 12 00       	mov    $0x124000,%eax
	movl	%eax, %cr3
f010625f:	0f 22 d8             	mov    %eax,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %eax
f0106262:	0f 20 e0             	mov    %cr4,%eax
	orl	$(CR4_PSE), %eax
f0106265:	83 c8 10             	or     $0x10,%eax
	movl	%eax, %cr4
f0106268:	0f 22 e0             	mov    %eax,%cr4
	# Turn on paging.
	movl	%cr0, %eax
f010626b:	0f 20 c0             	mov    %cr0,%eax
	orl	$(CR0_PE|CR0_PG|CR0_WP), %eax
f010626e:	0d 01 00 01 80       	or     $0x80010001,%eax
	movl	%eax, %cr0
f0106273:	0f 22 c0             	mov    %eax,%cr0

	# Switch to the per-cpu stack allocated in boot_aps()
	movl	mpentry_kstack, %esp
f0106276:	8b 25 14 3f 23 f0    	mov    0xf0233f14,%esp
	movl	$0x0, %ebp			# nuke frame pointer
f010627c:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Call mp_main().  (Exercise for the reader: why the indirect call?)
	movl	$mp_main, %eax
f0106281:	b8 90 02 10 f0       	mov    $0xf0100290,%eax
	call	*%eax
f0106286:	ff d0                	call   *%eax

f0106288 <spin>:

	# If mp_main returns (it shouldn't), loop.
spin:
	jmp	spin
f0106288:	eb fe                	jmp    f0106288 <spin>
f010628a:	66 90                	xchg   %ax,%ax

f010628c <gdt>:
	...
f0106294:	ff                   	(bad)  
f0106295:	ff 00                	incl   (%eax)
f0106297:	00 00                	add    %al,(%eax)
f0106299:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf
f01062a0:	00 92 cf 00 17 00    	add    %dl,0x1700cf(%edx)

f01062a4 <gdtdesc>:
f01062a4:	17                   	pop    %ss
f01062a5:	00 64 70 00          	add    %ah,0x0(%eax,%esi,2)
	...

f01062aa <mpentry_end>:
	.word	0x17				# sizeof(gdt) - 1
	.long	MPBOOTPHYS(gdt)			# address gdt

.globl mpentry_end
mpentry_end:
	nop
f01062aa:	90                   	nop

f01062ab <print_table_header>:
		rsdp->revision, rsdp->oem_id);
}

static void
print_table_header(struct acpi_table_header *hdr)
{
f01062ab:	55                   	push   %ebp
f01062ac:	89 e5                	mov    %esp,%ebp
f01062ae:	57                   	push   %edi
f01062af:	56                   	push   %esi
f01062b0:	53                   	push   %ebx
f01062b1:	83 ec 1c             	sub    $0x1c,%esp
f01062b4:	89 c2                	mov    %eax,%edx
	cprintf("ACPI: %.4s %08p %06x v%02d %.6s %.8s %02d %.4s %02d\n",
f01062b6:	8b 78 20             	mov    0x20(%eax),%edi
		hdr->signature, PADDR(hdr), hdr->length, hdr->revision,
		hdr->oem_id, hdr->oem_table_id, hdr->oem_revision,
		hdr->asl_compiler_id, hdr->asl_compiler_revision);
f01062b9:	8d 70 1c             	lea    0x1c(%eax),%esi
}

static void
print_table_header(struct acpi_table_header *hdr)
{
	cprintf("ACPI: %.4s %08p %06x v%02d %.6s %.8s %02d %.4s %02d\n",
f01062bc:	8b 58 18             	mov    0x18(%eax),%ebx
		hdr->signature, PADDR(hdr), hdr->length, hdr->revision,
		hdr->oem_id, hdr->oem_table_id, hdr->oem_revision,
f01062bf:	8d 48 10             	lea    0x10(%eax),%ecx
f01062c2:	8d 40 0a             	lea    0xa(%eax),%eax
f01062c5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
}

static void
print_table_header(struct acpi_table_header *hdr)
{
	cprintf("ACPI: %.4s %08p %06x v%02d %.6s %.8s %02d %.4s %02d\n",
f01062c8:	0f b6 42 08          	movzbl 0x8(%edx),%eax
f01062cc:	89 45 e0             	mov    %eax,-0x20(%ebp)
f01062cf:	8b 42 04             	mov    0x4(%edx),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01062d2:	81 fa ff ff ff ef    	cmp    $0xefffffff,%edx
f01062d8:	77 12                	ja     f01062ec <print_table_header+0x41>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01062da:	52                   	push   %edx
f01062db:	68 0c 75 10 f0       	push   $0xf010750c
f01062e0:	6a 1e                	push   $0x1e
f01062e2:	68 89 99 10 f0       	push   $0xf0109989
f01062e7:	e8 60 9d ff ff       	call   f010004c <_panic>
f01062ec:	83 ec 08             	sub    $0x8,%esp
f01062ef:	57                   	push   %edi
f01062f0:	56                   	push   %esi
f01062f1:	53                   	push   %ebx
f01062f2:	51                   	push   %ecx
f01062f3:	ff 75 e4             	pushl  -0x1c(%ebp)
f01062f6:	ff 75 e0             	pushl  -0x20(%ebp)
f01062f9:	50                   	push   %eax
f01062fa:	8d 82 00 00 00 10    	lea    0x10000000(%edx),%eax
f0106300:	50                   	push   %eax
f0106301:	52                   	push   %edx
f0106302:	68 30 99 10 f0       	push   $0xf0109930
f0106307:	e8 05 d9 ff ff       	call   f0103c11 <cprintf>
		hdr->signature, PADDR(hdr), hdr->length, hdr->revision,
		hdr->oem_id, hdr->oem_table_id, hdr->oem_revision,
		hdr->asl_compiler_id, hdr->asl_compiler_revision);
}
f010630c:	83 c4 30             	add    $0x30,%esp
f010630f:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106312:	5b                   	pop    %ebx
f0106313:	5e                   	pop    %esi
f0106314:	5f                   	pop    %edi
f0106315:	5d                   	pop    %ebp
f0106316:	c3                   	ret    

f0106317 <rsdp_search1>:
}

// Look for the RSDP in the len bytes at physical address addr.
static struct acpi_table_rsdp *
rsdp_search1(physaddr_t a, int len)
{
f0106317:	55                   	push   %ebp
f0106318:	89 e5                	mov    %esp,%ebp
f010631a:	57                   	push   %edi
f010631b:	56                   	push   %esi
f010631c:	53                   	push   %ebx
f010631d:	83 ec 0c             	sub    $0xc,%esp
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106320:	8b 0d 24 44 23 f0    	mov    0xf0234424,%ecx
f0106326:	89 c3                	mov    %eax,%ebx
f0106328:	c1 eb 0c             	shr    $0xc,%ebx
f010632b:	39 cb                	cmp    %ecx,%ebx
f010632d:	72 12                	jb     f0106341 <rsdp_search1+0x2a>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010632f:	50                   	push   %eax
f0106330:	68 e8 74 10 f0       	push   $0xf01074e8
f0106335:	6a 32                	push   $0x32
f0106337:	68 89 99 10 f0       	push   $0xf0109989
f010633c:	e8 0b 9d ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0106341:	8d 98 00 00 00 f0    	lea    -0x10000000(%eax),%ebx
	void *p = KADDR(a), *e = KADDR(a + len);
f0106347:	01 d0                	add    %edx,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106349:	89 c2                	mov    %eax,%edx
f010634b:	c1 ea 0c             	shr    $0xc,%edx
f010634e:	39 ca                	cmp    %ecx,%edx
f0106350:	72 12                	jb     f0106364 <rsdp_search1+0x4d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0106352:	50                   	push   %eax
f0106353:	68 e8 74 10 f0       	push   $0xf01074e8
f0106358:	6a 32                	push   $0x32
f010635a:	68 89 99 10 f0       	push   $0xf0109989
f010635f:	e8 e8 9c ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0106364:	8d b0 00 00 00 f0    	lea    -0x10000000(%eax),%esi

	// The signature is on a 16-byte boundary.
	for (; p < e; p += 16) {
f010636a:	eb 51                	jmp    f01063bd <rsdp_search1+0xa6>
		struct acpi_table_rsdp *rsdp = p;

		if (memcmp(rsdp->signature, ACPI_SIG_RSDP, 8) ||
f010636c:	83 ec 04             	sub    $0x4,%esp
f010636f:	6a 08                	push   $0x8
f0106371:	68 95 99 10 f0       	push   $0xf0109995
f0106376:	53                   	push   %ebx
f0106377:	e8 72 fd ff ff       	call   f01060ee <memcmp>
f010637c:	83 c4 10             	add    $0x10,%esp
f010637f:	85 c0                	test   %eax,%eax
f0106381:	75 37                	jne    f01063ba <rsdp_search1+0xa3>
f0106383:	89 da                	mov    %ebx,%edx
f0106385:	8d 7b 14             	lea    0x14(%ebx),%edi
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
		sum += ((uint8_t *)addr)[i];
f0106388:	0f b6 0a             	movzbl (%edx),%ecx
f010638b:	01 c8                	add    %ecx,%eax
f010638d:	42                   	inc    %edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f010638e:	39 d7                	cmp    %edx,%edi
f0106390:	75 f6                	jne    f0106388 <rsdp_search1+0x71>

	// The signature is on a 16-byte boundary.
	for (; p < e; p += 16) {
		struct acpi_table_rsdp *rsdp = p;

		if (memcmp(rsdp->signature, ACPI_SIG_RSDP, 8) ||
f0106392:	84 c0                	test   %al,%al
f0106394:	75 24                	jne    f01063ba <rsdp_search1+0xa3>
		    sum(rsdp, 20))
			continue;
		// ACPI 2.0+
		if (rsdp->revision && sum(rsdp, rsdp->length))
f0106396:	80 7b 0f 00          	cmpb   $0x0,0xf(%ebx)
f010639a:	74 2c                	je     f01063c8 <rsdp_search1+0xb1>
f010639c:	8b 7b 14             	mov    0x14(%ebx),%edi
static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
f010639f:	ba 00 00 00 00       	mov    $0x0,%edx
	for (i = 0; i < len; i++)
f01063a4:	b8 00 00 00 00       	mov    $0x0,%eax
f01063a9:	eb 07                	jmp    f01063b2 <rsdp_search1+0x9b>
		sum += ((uint8_t *)addr)[i];
f01063ab:	0f b6 0c 03          	movzbl (%ebx,%eax,1),%ecx
f01063af:	01 ca                	add    %ecx,%edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f01063b1:	40                   	inc    %eax
f01063b2:	39 c7                	cmp    %eax,%edi
f01063b4:	7f f5                	jg     f01063ab <rsdp_search1+0x94>

		if (memcmp(rsdp->signature, ACPI_SIG_RSDP, 8) ||
		    sum(rsdp, 20))
			continue;
		// ACPI 2.0+
		if (rsdp->revision && sum(rsdp, rsdp->length))
f01063b6:	84 d2                	test   %dl,%dl
f01063b8:	74 12                	je     f01063cc <rsdp_search1+0xb5>
rsdp_search1(physaddr_t a, int len)
{
	void *p = KADDR(a), *e = KADDR(a + len);

	// The signature is on a 16-byte boundary.
	for (; p < e; p += 16) {
f01063ba:	83 c3 10             	add    $0x10,%ebx
f01063bd:	39 f3                	cmp    %esi,%ebx
f01063bf:	72 ab                	jb     f010636c <rsdp_search1+0x55>
		// ACPI 2.0+
		if (rsdp->revision && sum(rsdp, rsdp->length))
			continue;
		return rsdp;
	}
	return NULL;
f01063c1:	b8 00 00 00 00       	mov    $0x0,%eax
f01063c6:	eb 06                	jmp    f01063ce <rsdp_search1+0xb7>
f01063c8:	89 d8                	mov    %ebx,%eax
f01063ca:	eb 02                	jmp    f01063ce <rsdp_search1+0xb7>
f01063cc:	89 d8                	mov    %ebx,%eax
}
f01063ce:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01063d1:	5b                   	pop    %ebx
f01063d2:	5e                   	pop    %esi
f01063d3:	5f                   	pop    %edi
f01063d4:	5d                   	pop    %ebp
f01063d5:	c3                   	ret    

f01063d6 <acpi_init>:
	return rsdp_search1(0xE0000, 0x20000);
}

void
acpi_init(void)
{
f01063d6:	55                   	push   %ebp
f01063d7:	89 e5                	mov    %esp,%ebp
f01063d9:	57                   	push   %edi
f01063da:	56                   	push   %esi
f01063db:	53                   	push   %ebx
f01063dc:	83 ec 1c             	sub    $0x1c,%esp
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01063df:	83 3d 24 44 23 f0 00 	cmpl   $0x0,0xf0234424
f01063e6:	75 16                	jne    f01063fe <acpi_init+0x28>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01063e8:	68 0e 04 00 00       	push   $0x40e
f01063ed:	68 e8 74 10 f0       	push   $0xf01074e8
f01063f2:	6a 4d                	push   $0x4d
f01063f4:	68 89 99 10 f0       	push   $0xf0109989
f01063f9:	e8 4e 9c ff ff       	call   f010004c <_panic>
{
	physaddr_t ebda;
	struct acpi_table_rsdp *rsdp;

	// The 16-bit segment of the EBDA is in the two byte at 0x40:0x0E.
	ebda = *(uint16_t *) KADDR(0x40E);
f01063fe:	0f b7 05 0e 04 00 f0 	movzwl 0xf000040e,%eax
	ebda <<= 4;
	if ((rsdp = rsdp_search1(ebda, 1024)))
f0106405:	c1 e0 04             	shl    $0x4,%eax
f0106408:	ba 00 04 00 00       	mov    $0x400,%edx
f010640d:	e8 05 ff ff ff       	call   f0106317 <rsdp_search1>
		return rsdp;
f0106412:	89 c3                	mov    %eax,%ebx
	struct acpi_table_rsdp *rsdp;

	// The 16-bit segment of the EBDA is in the two byte at 0x40:0x0E.
	ebda = *(uint16_t *) KADDR(0x40E);
	ebda <<= 4;
	if ((rsdp = rsdp_search1(ebda, 1024)))
f0106414:	85 c0                	test   %eax,%eax
f0106416:	75 29                	jne    f0106441 <acpi_init+0x6b>
		return rsdp;
	return rsdp_search1(0xE0000, 0x20000);
f0106418:	ba 00 00 02 00       	mov    $0x20000,%edx
f010641d:	b8 00 00 0e 00       	mov    $0xe0000,%eax
f0106422:	e8 f0 fe ff ff       	call   f0106317 <rsdp_search1>
f0106427:	89 c3                	mov    %eax,%ebx
	size_t entry_size;
	void *p, *e;
	uint32_t i;

	rsdp = rsdp_search();
	if (!rsdp)
f0106429:	85 c0                	test   %eax,%eax
f010642b:	75 14                	jne    f0106441 <acpi_init+0x6b>
		panic("ACPI: No RSDP found");
f010642d:	83 ec 04             	sub    $0x4,%esp
f0106430:	68 a8 99 10 f0       	push   $0xf01099a8
f0106435:	6a 60                	push   $0x60
f0106437:	68 89 99 10 f0       	push   $0xf0109989
f010643c:	e8 0b 9c ff ff       	call   f010004c <_panic>
static void
print_table_rsdp(struct acpi_table_rsdp *rsdp)
{
	cprintf("ACPI: RSDP %08p %06x v%02d %.6s\n",
		PADDR(rsdp), (rsdp->revision) ? rsdp->length : 20,
		rsdp->revision, rsdp->oem_id);
f0106441:	8d 4b 09             	lea    0x9(%ebx),%ecx
static struct acpi_tables acpi_tables;

static void
print_table_rsdp(struct acpi_table_rsdp *rsdp)
{
	cprintf("ACPI: RSDP %08p %06x v%02d %.6s\n",
f0106444:	0f b6 53 0f          	movzbl 0xf(%ebx),%edx
f0106448:	80 7b 0f 00          	cmpb   $0x0,0xf(%ebx)
f010644c:	74 05                	je     f0106453 <acpi_init+0x7d>
f010644e:	8b 43 14             	mov    0x14(%ebx),%eax
f0106451:	eb 05                	jmp    f0106458 <acpi_init+0x82>
f0106453:	b8 14 00 00 00       	mov    $0x14,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0106458:	81 fb ff ff ff ef    	cmp    $0xefffffff,%ebx
f010645e:	77 12                	ja     f0106472 <acpi_init+0x9c>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0106460:	53                   	push   %ebx
f0106461:	68 0c 75 10 f0       	push   $0xf010750c
f0106466:	6a 16                	push   $0x16
f0106468:	68 89 99 10 f0       	push   $0xf0109989
f010646d:	e8 da 9b ff ff       	call   f010004c <_panic>
f0106472:	83 ec 0c             	sub    $0xc,%esp
f0106475:	51                   	push   %ecx
f0106476:	52                   	push   %edx
f0106477:	50                   	push   %eax
f0106478:	8d 83 00 00 00 10    	lea    0x10000000(%ebx),%eax
f010647e:	50                   	push   %eax
f010647f:	68 68 99 10 f0       	push   $0xf0109968
f0106484:	e8 88 d7 ff ff       	call   f0103c11 <cprintf>
	rsdp = rsdp_search();
	if (!rsdp)
		panic("ACPI: No RSDP found");
	print_table_rsdp(rsdp);

	if (rsdp->revision) {
f0106489:	83 c4 20             	add    $0x20,%esp
f010648c:	80 7b 0f 00          	cmpb   $0x0,0xf(%ebx)
f0106490:	74 36                	je     f01064c8 <acpi_init+0xf2>
		hdr = KADDR(rsdp->xsdt_physical_address);
f0106492:	8b 5b 18             	mov    0x18(%ebx),%ebx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106495:	89 d8                	mov    %ebx,%eax
f0106497:	c1 e8 0c             	shr    $0xc,%eax
f010649a:	39 05 24 44 23 f0    	cmp    %eax,0xf0234424
f01064a0:	77 12                	ja     f01064b4 <acpi_init+0xde>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01064a2:	53                   	push   %ebx
f01064a3:	68 e8 74 10 f0       	push   $0xf01074e8
f01064a8:	6a 64                	push   $0x64
f01064aa:	68 89 99 10 f0       	push   $0xf0109989
f01064af:	e8 98 9b ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01064b4:	81 eb 00 00 00 10    	sub    $0x10000000,%ebx
		sig = ACPI_SIG_XSDT;
		entry_size = 8;
f01064ba:	c7 45 dc 08 00 00 00 	movl   $0x8,-0x24(%ebp)
		panic("ACPI: No RSDP found");
	print_table_rsdp(rsdp);

	if (rsdp->revision) {
		hdr = KADDR(rsdp->xsdt_physical_address);
		sig = ACPI_SIG_XSDT;
f01064c1:	bf 9e 99 10 f0       	mov    $0xf010999e,%edi
f01064c6:	eb 34                	jmp    f01064fc <acpi_init+0x126>
		entry_size = 8;
	} else {
		hdr = KADDR(rsdp->rsdt_physical_address);
f01064c8:	8b 5b 10             	mov    0x10(%ebx),%ebx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01064cb:	89 d8                	mov    %ebx,%eax
f01064cd:	c1 e8 0c             	shr    $0xc,%eax
f01064d0:	3b 05 24 44 23 f0    	cmp    0xf0234424,%eax
f01064d6:	72 12                	jb     f01064ea <acpi_init+0x114>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01064d8:	53                   	push   %ebx
f01064d9:	68 e8 74 10 f0       	push   $0xf01074e8
f01064de:	6a 68                	push   $0x68
f01064e0:	68 89 99 10 f0       	push   $0xf0109989
f01064e5:	e8 62 9b ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01064ea:	81 eb 00 00 00 10    	sub    $0x10000000,%ebx
		sig = ACPI_SIG_RSDT;
		entry_size = 4;
f01064f0:	c7 45 dc 04 00 00 00 	movl   $0x4,-0x24(%ebp)
		hdr = KADDR(rsdp->xsdt_physical_address);
		sig = ACPI_SIG_XSDT;
		entry_size = 8;
	} else {
		hdr = KADDR(rsdp->rsdt_physical_address);
		sig = ACPI_SIG_RSDT;
f01064f7:	bf a3 99 10 f0       	mov    $0xf01099a3,%edi
		entry_size = 4;
	}

	if (memcmp(hdr->signature, sig, 4))
f01064fc:	83 ec 04             	sub    $0x4,%esp
f01064ff:	6a 04                	push   $0x4
f0106501:	57                   	push   %edi
f0106502:	53                   	push   %ebx
f0106503:	e8 e6 fb ff ff       	call   f01060ee <memcmp>
f0106508:	83 c4 10             	add    $0x10,%esp
f010650b:	85 c0                	test   %eax,%eax
f010650d:	74 12                	je     f0106521 <acpi_init+0x14b>
		panic("ACPI: Incorrect %s signature", sig);
f010650f:	57                   	push   %edi
f0106510:	68 bc 99 10 f0       	push   $0xf01099bc
f0106515:	6a 6e                	push   $0x6e
f0106517:	68 89 99 10 f0       	push   $0xf0109989
f010651c:	e8 2b 9b ff ff       	call   f010004c <_panic>
	if (sum(hdr, hdr->length))
f0106521:	8b 73 04             	mov    0x4(%ebx),%esi
static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
f0106524:	ba 00 00 00 00       	mov    $0x0,%edx
	for (i = 0; i < len; i++)
f0106529:	b8 00 00 00 00       	mov    $0x0,%eax
f010652e:	eb 07                	jmp    f0106537 <acpi_init+0x161>
		sum += ((uint8_t *)addr)[i];
f0106530:	0f b6 0c 03          	movzbl (%ebx,%eax,1),%ecx
f0106534:	01 ca                	add    %ecx,%edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f0106536:	40                   	inc    %eax
f0106537:	39 f0                	cmp    %esi,%eax
f0106539:	7c f5                	jl     f0106530 <acpi_init+0x15a>
		entry_size = 4;
	}

	if (memcmp(hdr->signature, sig, 4))
		panic("ACPI: Incorrect %s signature", sig);
	if (sum(hdr, hdr->length))
f010653b:	84 d2                	test   %dl,%dl
f010653d:	74 12                	je     f0106551 <acpi_init+0x17b>
		panic("ACPI: Bad %s checksum", sig);
f010653f:	57                   	push   %edi
f0106540:	68 d9 99 10 f0       	push   $0xf01099d9
f0106545:	6a 70                	push   $0x70
f0106547:	68 89 99 10 f0       	push   $0xf0109989
f010654c:	e8 fb 9a ff ff       	call   f010004c <_panic>
	print_table_header(hdr);
f0106551:	89 d8                	mov    %ebx,%eax
f0106553:	e8 53 fd ff ff       	call   f01062ab <print_table_header>

	p = hdr + 1;
f0106558:	8d 43 24             	lea    0x24(%ebx),%eax
f010655b:	89 c7                	mov    %eax,%edi
	e = (void *)hdr + hdr->length;
f010655d:	89 d8                	mov    %ebx,%eax
f010655f:	03 43 04             	add    0x4(%ebx),%eax
f0106562:	89 45 e0             	mov    %eax,-0x20(%ebp)
	for (i = 0; p < e; p += entry_size) {
f0106565:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f010656c:	89 fb                	mov    %edi,%ebx
f010656e:	e9 89 00 00 00       	jmp    f01065fc <acpi_init+0x226>
		hdr = KADDR(*(uint32_t *)p);
f0106573:	8b 33                	mov    (%ebx),%esi
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0106575:	89 f0                	mov    %esi,%eax
f0106577:	c1 e8 0c             	shr    $0xc,%eax
f010657a:	3b 05 24 44 23 f0    	cmp    0xf0234424,%eax
f0106580:	72 12                	jb     f0106594 <acpi_init+0x1be>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0106582:	56                   	push   %esi
f0106583:	68 e8 74 10 f0       	push   $0xf01074e8
f0106588:	6a 76                	push   $0x76
f010658a:	68 89 99 10 f0       	push   $0xf0109989
f010658f:	e8 b8 9a ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0106594:	8d 86 00 00 00 f0    	lea    -0x10000000(%esi),%eax
f010659a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		if (sum(hdr, hdr->length))
f010659d:	8b be 04 00 00 f0    	mov    -0xffffffc(%esi),%edi
static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
f01065a3:	ba 00 00 00 00       	mov    $0x0,%edx
	for (i = 0; i < len; i++)
f01065a8:	b8 00 00 00 00       	mov    $0x0,%eax
f01065ad:	eb 0b                	jmp    f01065ba <acpi_init+0x1e4>
		sum += ((uint8_t *)addr)[i];
f01065af:	0f b6 8c 30 00 00 00 	movzbl -0x10000000(%eax,%esi,1),%ecx
f01065b6:	f0 
f01065b7:	01 ca                	add    %ecx,%edx
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
f01065b9:	40                   	inc    %eax
f01065ba:	39 c7                	cmp    %eax,%edi
f01065bc:	7f f1                	jg     f01065af <acpi_init+0x1d9>

	p = hdr + 1;
	e = (void *)hdr + hdr->length;
	for (i = 0; p < e; p += entry_size) {
		hdr = KADDR(*(uint32_t *)p);
		if (sum(hdr, hdr->length))
f01065be:	84 d2                	test   %dl,%dl
f01065c0:	75 37                	jne    f01065f9 <acpi_init+0x223>
			continue;
		print_table_header(hdr);
f01065c2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01065c5:	e8 e1 fc ff ff       	call   f01062ab <print_table_header>
		assert(i < ACPI_NR_MAX);
f01065ca:	83 7d d8 1f          	cmpl   $0x1f,-0x28(%ebp)
f01065ce:	76 16                	jbe    f01065e6 <acpi_init+0x210>
f01065d0:	68 ef 99 10 f0       	push   $0xf01099ef
f01065d5:	68 51 75 10 f0       	push   $0xf0107551
f01065da:	6a 7a                	push   $0x7a
f01065dc:	68 89 99 10 f0       	push   $0xf0109989
f01065e1:	e8 66 9a ff ff       	call   f010004c <_panic>
		acpi_tables.entries[i++] = hdr;
f01065e6:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01065e9:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f01065ec:	89 3c 85 84 3e 23 f0 	mov    %edi,-0xfdcc17c(,%eax,4)
f01065f3:	8d 40 01             	lea    0x1(%eax),%eax
f01065f6:	89 45 d8             	mov    %eax,-0x28(%ebp)
		panic("ACPI: Bad %s checksum", sig);
	print_table_header(hdr);

	p = hdr + 1;
	e = (void *)hdr + hdr->length;
	for (i = 0; p < e; p += entry_size) {
f01065f9:	03 5d dc             	add    -0x24(%ebp),%ebx
f01065fc:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
f01065ff:	0f 82 6e ff ff ff    	jb     f0106573 <acpi_init+0x19d>
			continue;
		print_table_header(hdr);
		assert(i < ACPI_NR_MAX);
		acpi_tables.entries[i++] = hdr;
	}
	acpi_tables.nr = i;
f0106605:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0106608:	a3 80 3e 23 f0       	mov    %eax,0xf0233e80
}
f010660d:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106610:	5b                   	pop    %ebx
f0106611:	5e                   	pop    %esi
f0106612:	5f                   	pop    %edi
f0106613:	5d                   	pop    %ebp
f0106614:	c3                   	ret    

f0106615 <acpi_get_table>:

void *
acpi_get_table(const char *signature)
{
f0106615:	55                   	push   %ebp
f0106616:	89 e5                	mov    %esp,%ebp
f0106618:	57                   	push   %edi
f0106619:	56                   	push   %esi
f010661a:	53                   	push   %ebx
f010661b:	83 ec 0c             	sub    $0xc,%esp
f010661e:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint32_t i;
	struct acpi_table_header **phdr = acpi_tables.entries;
f0106621:	be 84 3e 23 f0       	mov    $0xf0233e84,%esi

	for (i = 0; i < acpi_tables.nr; ++i, ++phdr) {
f0106626:	bb 00 00 00 00       	mov    $0x0,%ebx
f010662b:	eb 1c                	jmp    f0106649 <acpi_get_table+0x34>
		if (!memcmp((*phdr)->signature, signature, 4))
f010662d:	83 ec 04             	sub    $0x4,%esp
f0106630:	6a 04                	push   $0x4
f0106632:	57                   	push   %edi
f0106633:	ff 36                	pushl  (%esi)
f0106635:	e8 b4 fa ff ff       	call   f01060ee <memcmp>
f010663a:	83 c4 10             	add    $0x10,%esp
f010663d:	85 c0                	test   %eax,%eax
f010663f:	75 04                	jne    f0106645 <acpi_get_table+0x30>
			return *phdr;
f0106641:	8b 06                	mov    (%esi),%eax
f0106643:	eb 11                	jmp    f0106656 <acpi_get_table+0x41>
acpi_get_table(const char *signature)
{
	uint32_t i;
	struct acpi_table_header **phdr = acpi_tables.entries;

	for (i = 0; i < acpi_tables.nr; ++i, ++phdr) {
f0106645:	43                   	inc    %ebx
f0106646:	83 c6 04             	add    $0x4,%esi
f0106649:	3b 1d 80 3e 23 f0    	cmp    0xf0233e80,%ebx
f010664f:	72 dc                	jb     f010662d <acpi_get_table+0x18>
		if (!memcmp((*phdr)->signature, signature, 4))
			return *phdr;
	}
	return NULL;
f0106651:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0106656:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106659:	5b                   	pop    %ebx
f010665a:	5e                   	pop    %esi
f010665b:	5f                   	pop    %edi
f010665c:	5d                   	pop    %ebp
f010665d:	c3                   	ret    

f010665e <mp_init>:
unsigned char percpu_kstacks[NCPU][KSTKSIZE]
__attribute__ ((aligned(PGSIZE)));

void
mp_init(void)
{
f010665e:	55                   	push   %ebp
f010665f:	89 e5                	mov    %esp,%ebp
f0106661:	56                   	push   %esi
f0106662:	53                   	push   %ebx
	struct acpi_subtable_header *hdr, *end;

	// 5.2.12.1 MADT Processor Local APIC / SAPIC Structure Entry Order
	// * initialize processors in the order that they appear in MADT;
	// * the boot processor is the first processor entry.
	bootcpu->cpu_status = CPU_STARTED;
f0106663:	c7 05 04 50 23 f0 01 	movl   $0x1,0xf0235004
f010666a:	00 00 00 

	madt = acpi_get_table(ACPI_SIG_MADT);
f010666d:	83 ec 0c             	sub    $0xc,%esp
f0106670:	68 ff 99 10 f0       	push   $0xf01099ff
f0106675:	e8 9b ff ff ff       	call   f0106615 <acpi_get_table>
	if (!madt)
f010667a:	83 c4 10             	add    $0x10,%esp
f010667d:	85 c0                	test   %eax,%eax
f010667f:	75 14                	jne    f0106695 <mp_init+0x37>
		panic("ACPI: No MADT found");
f0106681:	83 ec 04             	sub    $0x4,%esp
f0106684:	68 04 9a 10 f0       	push   $0xf0109a04
f0106689:	6a 1c                	push   $0x1c
f010668b:	68 18 9a 10 f0       	push   $0xf0109a18
f0106690:	e8 b7 99 ff ff       	call   f010004c <_panic>

	lapic_addr = madt->address;
f0106695:	8b 50 24             	mov    0x24(%eax),%edx
f0106698:	89 15 00 60 27 f0    	mov    %edx,0xf0276000

	hdr = (void *)madt + sizeof(*madt);
f010669e:	8d 50 2c             	lea    0x2c(%eax),%edx
	end = (void *)madt + madt->header.length;
f01066a1:	03 40 04             	add    0x4(%eax),%eax
	for (; hdr < end; hdr = (void *)hdr + hdr->length) {
f01066a4:	eb 54                	jmp    f01066fa <mp_init+0x9c>
		switch (hdr->type) {
f01066a6:	8a 0a                	mov    (%edx),%cl
f01066a8:	84 c9                	test   %cl,%cl
f01066aa:	74 07                	je     f01066b3 <mp_init+0x55>
f01066ac:	80 f9 01             	cmp    $0x1,%cl
f01066af:	74 34                	je     f01066e5 <mp_init+0x87>
f01066b1:	eb 41                	jmp    f01066f4 <mp_init+0x96>
		case ACPI_MADT_TYPE_LOCAL_APIC: {
			struct acpi_madt_local_apic *p = (void *)hdr;
			bool enabled = p->lapic_flags & BIT(0);

			if (ncpu < NCPU && enabled) {
f01066b3:	8b 35 a0 53 23 f0    	mov    0xf02353a0,%esi
f01066b9:	83 fe 07             	cmp    $0x7,%esi
f01066bc:	7f 36                	jg     f01066f4 <mp_init+0x96>
f01066be:	f6 42 04 01          	testb  $0x1,0x4(%edx)
f01066c2:	74 30                	je     f01066f4 <mp_init+0x96>
				// Be careful: cpu_apicid may differ from cpus index
				cpus[ncpu].cpu_apicid = p->id;
f01066c4:	8d 1c 36             	lea    (%esi,%esi,1),%ebx
f01066c7:	01 f3                	add    %esi,%ebx
f01066c9:	01 db                	add    %ebx,%ebx
f01066cb:	01 f3                	add    %esi,%ebx
f01066cd:	8d 1c 9e             	lea    (%esi,%ebx,4),%ebx
f01066d0:	8a 4a 03             	mov    0x3(%edx),%cl
f01066d3:	88 0c 9d 00 50 23 f0 	mov    %cl,-0xfdcb000(,%ebx,4)
				ncpu++;
f01066da:	8d 4e 01             	lea    0x1(%esi),%ecx
f01066dd:	89 0d a0 53 23 f0    	mov    %ecx,0xf02353a0
f01066e3:	eb 0f                	jmp    f01066f4 <mp_init+0x96>
		}
		case ACPI_MADT_TYPE_IO_APIC: {
			struct acpi_madt_io_apic *p = (void *)hdr;

			// We use one IOAPIC.
			if (p->global_irq_base == 0)
f01066e5:	83 7a 08 00          	cmpl   $0x0,0x8(%edx)
f01066e9:	75 09                	jne    f01066f4 <mp_init+0x96>
				ioapic_addr = p->address;
f01066eb:	8b 4a 04             	mov    0x4(%edx),%ecx
f01066ee:	89 0d 04 60 27 f0    	mov    %ecx,0xf0276004

	lapic_addr = madt->address;

	hdr = (void *)madt + sizeof(*madt);
	end = (void *)madt + madt->header.length;
	for (; hdr < end; hdr = (void *)hdr + hdr->length) {
f01066f4:	0f b6 4a 01          	movzbl 0x1(%edx),%ecx
f01066f8:	01 ca                	add    %ecx,%edx
f01066fa:	39 c2                	cmp    %eax,%edx
f01066fc:	72 a8                	jb     f01066a6 <mp_init+0x48>
		default:
			break;
		}
	}

	cprintf("SMP: %d CPU(s)\n", ncpu);
f01066fe:	83 ec 08             	sub    $0x8,%esp
f0106701:	ff 35 a0 53 23 f0    	pushl  0xf02353a0
f0106707:	68 28 9a 10 f0       	push   $0xf0109a28
f010670c:	e8 00 d5 ff ff       	call   f0103c11 <cprintf>
}
f0106711:	83 c4 10             	add    $0x10,%esp
f0106714:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106717:	5b                   	pop    %ebx
f0106718:	5e                   	pop    %esi
f0106719:	5d                   	pop    %ebp
f010671a:	c3                   	ret    

f010671b <lapic_write>:
	return lapic[index];
}

static void
lapic_write(uint32_t index, uint32_t value)
{
f010671b:	55                   	push   %ebp
f010671c:	89 e5                	mov    %esp,%ebp
	lapic[index] = value;
f010671e:	8b 0d 04 3f 23 f0    	mov    0xf0233f04,%ecx
f0106724:	8d 04 81             	lea    (%ecx,%eax,4),%eax
f0106727:	89 10                	mov    %edx,(%eax)
	lapic[ID];  // wait for write to finish, by reading
f0106729:	8b 41 20             	mov    0x20(%ecx),%eax
}
f010672c:	5d                   	pop    %ebp
f010672d:	c3                   	ret    

f010672e <cpunum>:
int
cpunum(void)
{
	int apicid, i;

	if (!lapic)
f010672e:	a1 04 3f 23 f0       	mov    0xf0233f04,%eax
f0106733:	85 c0                	test   %eax,%eax
f0106735:	74 45                	je     f010677c <cpunum+0x4e>
	lapic_write(TPR, 0);
}

int
cpunum(void)
{
f0106737:	55                   	push   %ebp
f0106738:	89 e5                	mov    %esp,%ebp
f010673a:	56                   	push   %esi
f010673b:	53                   	push   %ebx
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f010673c:	8b 58 20             	mov    0x20(%eax),%ebx
{
	int apicid, i;

	if (!lapic)
		return 0;
	apicid = lapic_read(ID) >> 24;
f010673f:	c1 eb 18             	shr    $0x18,%ebx
	for (i = 0; i < ncpu; ++i) {
f0106742:	8b 35 a0 53 23 f0    	mov    0xf02353a0,%esi
f0106748:	ba 00 50 23 f0       	mov    $0xf0235000,%edx
f010674d:	b8 00 00 00 00       	mov    $0x0,%eax
f0106752:	eb 0b                	jmp    f010675f <cpunum+0x31>
		if (cpus[i].cpu_apicid == apicid)
f0106754:	0f b6 0a             	movzbl (%edx),%ecx
f0106757:	83 c2 74             	add    $0x74,%edx
f010675a:	39 cb                	cmp    %ecx,%ebx
f010675c:	74 24                	je     f0106782 <cpunum+0x54>
	int apicid, i;

	if (!lapic)
		return 0;
	apicid = lapic_read(ID) >> 24;
	for (i = 0; i < ncpu; ++i) {
f010675e:	40                   	inc    %eax
f010675f:	39 f0                	cmp    %esi,%eax
f0106761:	7c f1                	jl     f0106754 <cpunum+0x26>
		if (cpus[i].cpu_apicid == apicid)
			return i;
	}
	assert(0);
f0106763:	68 2a 85 10 f0       	push   $0xf010852a
f0106768:	68 51 75 10 f0       	push   $0xf0107551
f010676d:	68 86 00 00 00       	push   $0x86
f0106772:	68 38 9a 10 f0       	push   $0xf0109a38
f0106777:	e8 d0 98 ff ff       	call   f010004c <_panic>
cpunum(void)
{
	int apicid, i;

	if (!lapic)
		return 0;
f010677c:	b8 00 00 00 00       	mov    $0x0,%eax
f0106781:	c3                   	ret    
	for (i = 0; i < ncpu; ++i) {
		if (cpus[i].cpu_apicid == apicid)
			return i;
	}
	assert(0);
}
f0106782:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106785:	5b                   	pop    %ebx
f0106786:	5e                   	pop    %esi
f0106787:	5d                   	pop    %ebp
f0106788:	c3                   	ret    

f0106789 <lapic_init>:
	lapic[ID];  // wait for write to finish, by reading
}

void
lapic_init(void)
{
f0106789:	55                   	push   %ebp
f010678a:	89 e5                	mov    %esp,%ebp
f010678c:	53                   	push   %ebx
f010678d:	83 ec 04             	sub    $0x4,%esp
	assert(lapic_addr);
f0106790:	a1 00 60 27 f0       	mov    0xf0276000,%eax
f0106795:	85 c0                	test   %eax,%eax
f0106797:	75 16                	jne    f01067af <lapic_init+0x26>
f0106799:	68 45 9a 10 f0       	push   $0xf0109a45
f010679e:	68 51 75 10 f0       	push   $0xf0107551
f01067a3:	6a 40                	push   $0x40
f01067a5:	68 38 9a 10 f0       	push   $0xf0109a38
f01067aa:	e8 9d 98 ff ff       	call   f010004c <_panic>

	// lapic_addr is the physical address of the LAPIC's 4K MMIO
	// region.  Map it in to virtual memory so we can access it.
	lapic = mmio_map_region(lapic_addr, 4096);
f01067af:	83 ec 08             	sub    $0x8,%esp
f01067b2:	68 00 10 00 00       	push   $0x1000
f01067b7:	50                   	push   %eax
f01067b8:	e8 47 af ff ff       	call   f0101704 <mmio_map_region>
f01067bd:	89 c3                	mov    %eax,%ebx
f01067bf:	a3 04 3f 23 f0       	mov    %eax,0xf0233f04

	if (thiscpu == bootcpu)
f01067c4:	e8 65 ff ff ff       	call   f010672e <cpunum>
f01067c9:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01067cc:	01 c2                	add    %eax,%edx
f01067ce:	01 d2                	add    %edx,%edx
f01067d0:	01 c2                	add    %eax,%edx
f01067d2:	8d 04 90             	lea    (%eax,%edx,4),%eax
f01067d5:	83 c4 10             	add    $0x10,%esp
f01067d8:	c1 e0 02             	shl    $0x2,%eax
f01067db:	75 1d                	jne    f01067fa <lapic_init+0x71>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f01067dd:	8b 43 30             	mov    0x30(%ebx),%eax
	// lapic_addr is the physical address of the LAPIC's 4K MMIO
	// region.  Map it in to virtual memory so we can access it.
	lapic = mmio_map_region(lapic_addr, 4096);

	if (thiscpu == bootcpu)
		cprintf("SMP: LAPIC %08p v%02x\n", lapic_addr, lapic_read(VER) & 0xFF);
f01067e0:	83 ec 04             	sub    $0x4,%esp
f01067e3:	0f b6 c0             	movzbl %al,%eax
f01067e6:	50                   	push   %eax
f01067e7:	ff 35 00 60 27 f0    	pushl  0xf0276000
f01067ed:	68 50 9a 10 f0       	push   $0xf0109a50
f01067f2:	e8 1a d4 ff ff       	call   f0103c11 <cprintf>
f01067f7:	83 c4 10             	add    $0x10,%esp

	// Enable local APIC; set spurious interrupt vector.
	lapic_write(SVR, ENABLE | (IRQ_OFFSET + IRQ_SPURIOUS));
f01067fa:	ba 27 01 00 00       	mov    $0x127,%edx
f01067ff:	b8 3c 00 00 00       	mov    $0x3c,%eax
f0106804:	e8 12 ff ff ff       	call   f010671b <lapic_write>

	// The timer repeatedly counts down at bus frequency
	// from lapic[TICR] and then issues an interrupt.
	// If we cared more about precise timekeeping,
	// TICR would be calibrated using an external time source.
	lapic_write(TDCR, X1);
f0106809:	ba 0b 00 00 00       	mov    $0xb,%edx
f010680e:	b8 f8 00 00 00       	mov    $0xf8,%eax
f0106813:	e8 03 ff ff ff       	call   f010671b <lapic_write>
	lapic_write(TIMER, PERIODIC | (IRQ_OFFSET + IRQ_TIMER));
f0106818:	ba 20 00 02 00       	mov    $0x20020,%edx
f010681d:	b8 c8 00 00 00       	mov    $0xc8,%eax
f0106822:	e8 f4 fe ff ff       	call   f010671b <lapic_write>
	lapic_write(TICR, 10000000);
f0106827:	ba 80 96 98 00       	mov    $0x989680,%edx
f010682c:	b8 e0 00 00 00       	mov    $0xe0,%eax
f0106831:	e8 e5 fe ff ff       	call   f010671b <lapic_write>
	//
	// According to Intel MP Specification, the BIOS should initialize
	// BSP's local APIC in Virtual Wire Mode, in which 8259A's
	// INTR is virtually connected to BSP's LINTIN0. In this mode,
	// we do not need to program the IOAPIC.
	if (thiscpu != bootcpu)
f0106836:	e8 f3 fe ff ff       	call   f010672e <cpunum>
f010683b:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010683e:	01 c2                	add    %eax,%edx
f0106840:	01 d2                	add    %edx,%edx
f0106842:	01 c2                	add    %eax,%edx
f0106844:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106847:	c1 e0 02             	shl    $0x2,%eax
f010684a:	74 0f                	je     f010685b <lapic_init+0xd2>
		lapic_write(LINT0, MASKED);
f010684c:	ba 00 00 01 00       	mov    $0x10000,%edx
f0106851:	b8 d4 00 00 00       	mov    $0xd4,%eax
f0106856:	e8 c0 fe ff ff       	call   f010671b <lapic_write>

	// Disable NMI (LINT1) on all CPUs
	lapic_write(LINT1, MASKED);
f010685b:	ba 00 00 01 00       	mov    $0x10000,%edx
f0106860:	b8 d8 00 00 00       	mov    $0xd8,%eax
f0106865:	e8 b1 fe ff ff       	call   f010671b <lapic_write>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f010686a:	8b 1d 04 3f 23 f0    	mov    0xf0233f04,%ebx
f0106870:	8b 43 30             	mov    0x30(%ebx),%eax
	// Disable NMI (LINT1) on all CPUs
	lapic_write(LINT1, MASKED);

	// Disable performance counter overflow interrupts
	// on machines that provide that interrupt entry.
	if (((lapic_read(VER)>>16) & 0xFF) >= 4)
f0106873:	c1 e8 10             	shr    $0x10,%eax
f0106876:	3c 03                	cmp    $0x3,%al
f0106878:	76 0f                	jbe    f0106889 <lapic_init+0x100>
		lapic_write(PCINT, MASKED);
f010687a:	ba 00 00 01 00       	mov    $0x10000,%edx
f010687f:	b8 d0 00 00 00       	mov    $0xd0,%eax
f0106884:	e8 92 fe ff ff       	call   f010671b <lapic_write>

	// Map error interrupt to IRQ_ERROR.
	lapic_write(ERROR, IRQ_OFFSET + IRQ_ERROR);
f0106889:	ba 33 00 00 00       	mov    $0x33,%edx
f010688e:	b8 dc 00 00 00       	mov    $0xdc,%eax
f0106893:	e8 83 fe ff ff       	call   f010671b <lapic_write>

	// Clear error status register (requires back-to-back writes).
	lapic_write(ESR, 0);
f0106898:	ba 00 00 00 00       	mov    $0x0,%edx
f010689d:	b8 a0 00 00 00       	mov    $0xa0,%eax
f01068a2:	e8 74 fe ff ff       	call   f010671b <lapic_write>
	lapic_write(ESR, 0);
f01068a7:	ba 00 00 00 00       	mov    $0x0,%edx
f01068ac:	b8 a0 00 00 00       	mov    $0xa0,%eax
f01068b1:	e8 65 fe ff ff       	call   f010671b <lapic_write>

	// Ack any outstanding interrupts.
	lapic_write(EOI, 0);
f01068b6:	ba 00 00 00 00       	mov    $0x0,%edx
f01068bb:	b8 2c 00 00 00       	mov    $0x2c,%eax
f01068c0:	e8 56 fe ff ff       	call   f010671b <lapic_write>

	// Send an Init Level De-Assert to synchronize arbitration ID's.
	lapic_write(ICRHI, 0);
f01068c5:	ba 00 00 00 00       	mov    $0x0,%edx
f01068ca:	b8 c4 00 00 00       	mov    $0xc4,%eax
f01068cf:	e8 47 fe ff ff       	call   f010671b <lapic_write>
	lapic_write(ICRLO, BCAST | INIT | LEVEL);
f01068d4:	ba 00 85 08 00       	mov    $0x88500,%edx
f01068d9:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01068de:	e8 38 fe ff ff       	call   f010671b <lapic_write>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f01068e3:	8b 83 00 03 00 00    	mov    0x300(%ebx),%eax
	lapic_write(EOI, 0);

	// Send an Init Level De-Assert to synchronize arbitration ID's.
	lapic_write(ICRHI, 0);
	lapic_write(ICRLO, BCAST | INIT | LEVEL);
	while(lapic_read(ICRLO) & DELIVS)
f01068e9:	f6 c4 10             	test   $0x10,%ah
f01068ec:	75 f5                	jne    f01068e3 <lapic_init+0x15a>
		;

	// Enable interrupts on the APIC (but not on the processor).
	lapic_write(TPR, 0);
f01068ee:	ba 00 00 00 00       	mov    $0x0,%edx
f01068f3:	b8 20 00 00 00       	mov    $0x20,%eax
f01068f8:	e8 1e fe ff ff       	call   f010671b <lapic_write>
}
f01068fd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0106900:	c9                   	leave  
f0106901:	c3                   	ret    

f0106902 <lapic_eoi>:

// Acknowledge interrupt.
void
lapic_eoi(void)
{
	if (lapic)
f0106902:	83 3d 04 3f 23 f0 00 	cmpl   $0x0,0xf0233f04
f0106909:	74 13                	je     f010691e <lapic_eoi+0x1c>
}

// Acknowledge interrupt.
void
lapic_eoi(void)
{
f010690b:	55                   	push   %ebp
f010690c:	89 e5                	mov    %esp,%ebp
	if (lapic)
		lapic_write(EOI, 0);
f010690e:	ba 00 00 00 00       	mov    $0x0,%edx
f0106913:	b8 2c 00 00 00       	mov    $0x2c,%eax
f0106918:	e8 fe fd ff ff       	call   f010671b <lapic_write>
}
f010691d:	5d                   	pop    %ebp
f010691e:	c3                   	ret    

f010691f <lapic_startap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapic_startap(uint8_t apicid, uint32_t addr)
{
f010691f:	55                   	push   %ebp
f0106920:	89 e5                	mov    %esp,%ebp
f0106922:	56                   	push   %esi
f0106923:	53                   	push   %ebx
f0106924:	8b 75 08             	mov    0x8(%ebp),%esi
f0106927:	8b 5d 0c             	mov    0xc(%ebp),%ebx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010692a:	ba 70 00 00 00       	mov    $0x70,%edx
f010692f:	b0 0f                	mov    $0xf,%al
f0106931:	ee                   	out    %al,(%dx)
f0106932:	ba 71 00 00 00       	mov    $0x71,%edx
f0106937:	b0 0a                	mov    $0xa,%al
f0106939:	ee                   	out    %al,(%dx)
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010693a:	83 3d 24 44 23 f0 00 	cmpl   $0x0,0xf0234424
f0106941:	75 19                	jne    f010695c <lapic_startap+0x3d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0106943:	68 67 04 00 00       	push   $0x467
f0106948:	68 e8 74 10 f0       	push   $0xf01074e8
f010694d:	68 a7 00 00 00       	push   $0xa7
f0106952:	68 38 9a 10 f0       	push   $0xf0109a38
f0106957:	e8 f0 96 ff ff       	call   f010004c <_panic>
	// and the warm reset vector (DWORD based at 40:67) to point at
	// the AP startup code prior to the [universal startup algorithm]."
	outb(IO_RTC, 0xF);  // offset 0xF is shutdown code
	outb(IO_RTC+1, 0x0A);
	wrv = (uint16_t *)KADDR((0x40 << 4 | 0x67));  // Warm reset vector
	wrv[0] = 0;
f010695c:	66 c7 05 67 04 00 f0 	movw   $0x0,0xf0000467
f0106963:	00 00 
	wrv[1] = addr >> 4;
f0106965:	89 d8                	mov    %ebx,%eax
f0106967:	c1 e8 04             	shr    $0x4,%eax
f010696a:	66 a3 69 04 00 f0    	mov    %ax,0xf0000469

	// "Universal startup algorithm."
	// Send INIT (level-triggered) interrupt to reset other CPU.
	lapic_write(ICRHI, apicid << 24);
f0106970:	c1 e6 18             	shl    $0x18,%esi
f0106973:	89 f2                	mov    %esi,%edx
f0106975:	b8 c4 00 00 00       	mov    $0xc4,%eax
f010697a:	e8 9c fd ff ff       	call   f010671b <lapic_write>
	lapic_write(ICRLO, INIT | LEVEL | ASSERT);
f010697f:	ba 00 c5 00 00       	mov    $0xc500,%edx
f0106984:	b8 c0 00 00 00       	mov    $0xc0,%eax
f0106989:	e8 8d fd ff ff       	call   f010671b <lapic_write>
	microdelay(200);
	lapic_write(ICRLO, INIT | LEVEL);
f010698e:	ba 00 85 00 00       	mov    $0x8500,%edx
f0106993:	b8 c0 00 00 00       	mov    $0xc0,%eax
f0106998:	e8 7e fd ff ff       	call   f010671b <lapic_write>
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapic_write(ICRHI, apicid << 24);
		lapic_write(ICRLO, STARTUP | (addr >> 12));
f010699d:	c1 eb 0c             	shr    $0xc,%ebx
f01069a0:	80 cf 06             	or     $0x6,%bh
	// Regular hardware is supposed to only accept a STARTUP
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapic_write(ICRHI, apicid << 24);
f01069a3:	89 f2                	mov    %esi,%edx
f01069a5:	b8 c4 00 00 00       	mov    $0xc4,%eax
f01069aa:	e8 6c fd ff ff       	call   f010671b <lapic_write>
		lapic_write(ICRLO, STARTUP | (addr >> 12));
f01069af:	89 da                	mov    %ebx,%edx
f01069b1:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01069b6:	e8 60 fd ff ff       	call   f010671b <lapic_write>
	// Regular hardware is supposed to only accept a STARTUP
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapic_write(ICRHI, apicid << 24);
f01069bb:	89 f2                	mov    %esi,%edx
f01069bd:	b8 c4 00 00 00       	mov    $0xc4,%eax
f01069c2:	e8 54 fd ff ff       	call   f010671b <lapic_write>
		lapic_write(ICRLO, STARTUP | (addr >> 12));
f01069c7:	89 da                	mov    %ebx,%edx
f01069c9:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01069ce:	e8 48 fd ff ff       	call   f010671b <lapic_write>
		microdelay(200);
	}
}
f01069d3:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01069d6:	5b                   	pop    %ebx
f01069d7:	5e                   	pop    %esi
f01069d8:	5d                   	pop    %ebp
f01069d9:	c3                   	ret    

f01069da <lapic_ipi>:

void
lapic_ipi(int vector)
{
f01069da:	55                   	push   %ebp
f01069db:	89 e5                	mov    %esp,%ebp
	lapic_write(ICRLO, OTHERS | FIXED | vector);
f01069dd:	8b 55 08             	mov    0x8(%ebp),%edx
f01069e0:	81 ca 00 00 0c 00    	or     $0xc0000,%edx
f01069e6:	b8 c0 00 00 00       	mov    $0xc0,%eax
f01069eb:	e8 2b fd ff ff       	call   f010671b <lapic_write>
static volatile uint32_t *lapic;

static uint32_t
lapic_read(uint32_t index)
{
	return lapic[index];
f01069f0:	8b 15 04 3f 23 f0    	mov    0xf0233f04,%edx
f01069f6:	8b 82 00 03 00 00    	mov    0x300(%edx),%eax

void
lapic_ipi(int vector)
{
	lapic_write(ICRLO, OTHERS | FIXED | vector);
	while (lapic_read(ICRLO) & DELIVS)
f01069fc:	f6 c4 10             	test   $0x10,%ah
f01069ff:	75 f5                	jne    f01069f6 <lapic_ipi+0x1c>
		;
}
f0106a01:	5d                   	pop    %ebp
f0106a02:	c3                   	ret    

f0106a03 <ioapic_init>:
	*(ioapic + IOWIN) = data;
}

void
ioapic_init(void)
{
f0106a03:	55                   	push   %ebp
f0106a04:	89 e5                	mov    %esp,%ebp
f0106a06:	56                   	push   %esi
f0106a07:	53                   	push   %ebx
	int i, reg_ver, ver, pins;

	// Default physical address.
	assert(ioapic_addr == 0xfec00000);
f0106a08:	81 3d 04 60 27 f0 00 	cmpl   $0xfec00000,0xf0276004
f0106a0f:	00 c0 fe 
f0106a12:	74 16                	je     f0106a2a <ioapic_init+0x27>
f0106a14:	68 67 9a 10 f0       	push   $0xf0109a67
f0106a19:	68 51 75 10 f0       	push   $0xf0107551
f0106a1e:	6a 33                	push   $0x33
f0106a20:	68 81 9a 10 f0       	push   $0xf0109a81
f0106a25:	e8 22 96 ff ff       	call   f010004c <_panic>

	// IOAPIC is the default physical address.  Map it in to
	// virtual memory so we can access it.
	ioapic = mmio_map_region(ioapic_addr, 4096);
f0106a2a:	83 ec 08             	sub    $0x8,%esp
f0106a2d:	68 00 10 00 00       	push   $0x1000
f0106a32:	68 00 00 c0 fe       	push   $0xfec00000
f0106a37:	e8 c8 ac ff ff       	call   f0101704 <mmio_map_region>
f0106a3c:	a3 08 3f 23 f0       	mov    %eax,0xf0233f08
static volatile uint32_t *ioapic;

static uint32_t
ioapic_read(int reg)
{
	*(ioapic + IOREGSEL) = reg;
f0106a41:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
	return *(ioapic + IOWIN);
f0106a47:	8b 40 10             	mov    0x10(%eax),%eax
	// virtual memory so we can access it.
	ioapic = mmio_map_region(ioapic_addr, 4096);

	reg_ver = ioapic_read(IOAPICVER);
	ver = reg_ver & 0xff;
	pins = ((reg_ver >> 16) & 0xff) + 1;
f0106a4a:	89 c2                	mov    %eax,%edx
f0106a4c:	c1 fa 10             	sar    $0x10,%edx
f0106a4f:	0f b6 d2             	movzbl %dl,%edx
f0106a52:	8d 5a 01             	lea    0x1(%edx),%ebx
	cprintf("SMP: IOAPIC %08p v%02x [global_irq %02d-%02d]\n",
f0106a55:	89 14 24             	mov    %edx,(%esp)
f0106a58:	6a 00                	push   $0x0
f0106a5a:	0f b6 c0             	movzbl %al,%eax
f0106a5d:	50                   	push   %eax
f0106a5e:	ff 35 04 60 27 f0    	pushl  0xf0276004
f0106a64:	68 90 9a 10 f0       	push   $0xf0109a90
f0106a69:	e8 a3 d1 ff ff       	call   f0103c11 <cprintf>
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a6e:	8b 0d 08 3f 23 f0    	mov    0xf0233f08,%ecx
	cprintf("SMP: IOAPIC %08p v%02x [global_irq %02d-%02d]\n",
		ioapic_addr, ver, 0, pins - 1);

	// Mark all interrupts edge-triggered, active high, disabled,
	// and not routed to any CPUs.
	for (i = 0; i < pins; ++i) {
f0106a74:	83 c4 20             	add    $0x20,%esp
f0106a77:	ba 10 00 00 00       	mov    $0x10,%edx
f0106a7c:	b8 00 00 00 00       	mov    $0x0,%eax
f0106a81:	eb 1e                	jmp    f0106aa1 <ioapic_init+0x9e>
		ioapic_write(IOREDTBL+2*i, INT_DISABLED | (IRQ_OFFSET + i));
f0106a83:	8d 70 20             	lea    0x20(%eax),%esi
f0106a86:	81 ce 00 00 01 00    	or     $0x10000,%esi
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a8c:	89 11                	mov    %edx,(%ecx)
	*(ioapic + IOWIN) = data;
f0106a8e:	89 71 10             	mov    %esi,0x10(%ecx)
f0106a91:	8d 72 01             	lea    0x1(%edx),%esi
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106a94:	89 31                	mov    %esi,(%ecx)
	*(ioapic + IOWIN) = data;
f0106a96:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
	cprintf("SMP: IOAPIC %08p v%02x [global_irq %02d-%02d]\n",
		ioapic_addr, ver, 0, pins - 1);

	// Mark all interrupts edge-triggered, active high, disabled,
	// and not routed to any CPUs.
	for (i = 0; i < pins; ++i) {
f0106a9d:	40                   	inc    %eax
f0106a9e:	83 c2 02             	add    $0x2,%edx
f0106aa1:	39 d8                	cmp    %ebx,%eax
f0106aa3:	7c de                	jl     f0106a83 <ioapic_init+0x80>
		ioapic_write(IOREDTBL+2*i, INT_DISABLED | (IRQ_OFFSET + i));
		ioapic_write(IOREDTBL+2*i+1, 0);
	}
}
f0106aa5:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106aa8:	5b                   	pop    %ebx
f0106aa9:	5e                   	pop    %esi
f0106aaa:	5d                   	pop    %ebp
f0106aab:	c3                   	ret    

f0106aac <ioapic_enable>:

void
ioapic_enable(int irq, int apicid)
{
f0106aac:	55                   	push   %ebp
f0106aad:	89 e5                	mov    %esp,%ebp
f0106aaf:	8b 45 08             	mov    0x8(%ebp),%eax
	// Mark interrupt edge-triggered, active high, enabled,
	// and routed to the given cpu's APIC ID.
	ioapic_write(IOREDTBL+2*irq, IRQ_OFFSET + irq);
f0106ab2:	8d 48 20             	lea    0x20(%eax),%ecx
f0106ab5:	8d 54 00 10          	lea    0x10(%eax,%eax,1),%edx
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106ab9:	a1 08 3f 23 f0       	mov    0xf0233f08,%eax
f0106abe:	89 10                	mov    %edx,(%eax)
	*(ioapic + IOWIN) = data;
f0106ac0:	89 48 10             	mov    %ecx,0x10(%eax)
ioapic_enable(int irq, int apicid)
{
	// Mark interrupt edge-triggered, active high, enabled,
	// and routed to the given cpu's APIC ID.
	ioapic_write(IOREDTBL+2*irq, IRQ_OFFSET + irq);
	ioapic_write(IOREDTBL+2*irq+1, apicid << 24);
f0106ac3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0106ac6:	c1 e1 18             	shl    $0x18,%ecx
}

static void
ioapic_write(int reg, uint32_t data)
{
	*(ioapic + IOREGSEL) = reg;
f0106ac9:	42                   	inc    %edx
f0106aca:	89 10                	mov    %edx,(%eax)
	*(ioapic + IOWIN) = data;
f0106acc:	89 48 10             	mov    %ecx,0x10(%eax)
{
	// Mark interrupt edge-triggered, active high, enabled,
	// and routed to the given cpu's APIC ID.
	ioapic_write(IOREDTBL+2*irq, IRQ_OFFSET + irq);
	ioapic_write(IOREDTBL+2*irq+1, apicid << 24);
}
f0106acf:	5d                   	pop    %ebp
f0106ad0:	c3                   	ret    

f0106ad1 <__spin_initlock>:
}
#endif

void
__spin_initlock(struct spinlock *lk, char *name)
{
f0106ad1:	55                   	push   %ebp
f0106ad2:	89 e5                	mov    %esp,%ebp
f0106ad4:	8b 45 08             	mov    0x8(%ebp),%eax
	lk->locked = 0;
f0106ad7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
#ifdef DEBUG_SPINLOCK
	lk->name = name;
f0106add:	8b 55 0c             	mov    0xc(%ebp),%edx
f0106ae0:	89 50 04             	mov    %edx,0x4(%eax)
	lk->cpu = 0;
f0106ae3:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
#endif
}
f0106aea:	5d                   	pop    %ebp
f0106aeb:	c3                   	ret    

f0106aec <spin_lock>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
spin_lock(struct spinlock *lk)
{
f0106aec:	55                   	push   %ebp
f0106aed:	89 e5                	mov    %esp,%ebp
f0106aef:	56                   	push   %esi
f0106af0:	53                   	push   %ebx
f0106af1:	8b 5d 08             	mov    0x8(%ebp),%ebx

// Check whether this CPU is holding the lock.
static int
holding(struct spinlock *lock)
{
	return lock->locked && lock->cpu == thiscpu;
f0106af4:	83 3b 00             	cmpl   $0x0,(%ebx)
f0106af7:	74 1f                	je     f0106b18 <spin_lock+0x2c>
f0106af9:	8b 73 08             	mov    0x8(%ebx),%esi
f0106afc:	e8 2d fc ff ff       	call   f010672e <cpunum>
f0106b01:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0106b04:	01 c2                	add    %eax,%edx
f0106b06:	01 d2                	add    %edx,%edx
f0106b08:	01 c2                	add    %eax,%edx
f0106b0a:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106b0d:	8d 04 85 00 50 23 f0 	lea    -0xfdcb000(,%eax,4),%eax
// other CPUs to waste time spinning to acquire it.
void
spin_lock(struct spinlock *lk)
{
#ifdef DEBUG_SPINLOCK
	if (holding(lk))
f0106b14:	39 c6                	cmp    %eax,%esi
f0106b16:	74 07                	je     f0106b1f <spin_lock+0x33>
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0106b18:	ba 01 00 00 00       	mov    $0x1,%edx
f0106b1d:	eb 20                	jmp    f0106b3f <spin_lock+0x53>
		panic("CPU %d cannot acquire %s: already holding", cpunum(), lk->name);
f0106b1f:	8b 5b 04             	mov    0x4(%ebx),%ebx
f0106b22:	e8 07 fc ff ff       	call   f010672e <cpunum>
f0106b27:	83 ec 0c             	sub    $0xc,%esp
f0106b2a:	53                   	push   %ebx
f0106b2b:	50                   	push   %eax
f0106b2c:	68 c0 9a 10 f0       	push   $0xf0109ac0
f0106b31:	6a 41                	push   $0x41
f0106b33:	68 22 9b 10 f0       	push   $0xf0109b22
f0106b38:	e8 0f 95 ff ff       	call   f010004c <_panic>

	// The xchg is atomic.
	// It also serializes, so that reads after acquire are not
	// reordered before it. 
	while (xchg(&lk->locked, 1) != 0)
		asm volatile ("pause");
f0106b3d:	f3 90                	pause  
f0106b3f:	89 d0                	mov    %edx,%eax
f0106b41:	f0 87 03             	lock xchg %eax,(%ebx)
#endif

	// The xchg is atomic.
	// It also serializes, so that reads after acquire are not
	// reordered before it. 
	while (xchg(&lk->locked, 1) != 0)
f0106b44:	85 c0                	test   %eax,%eax
f0106b46:	75 f5                	jne    f0106b3d <spin_lock+0x51>
		asm volatile ("pause");

	// Record info about lock acquisition for debugging.
#ifdef DEBUG_SPINLOCK
	lk->cpu = thiscpu;
f0106b48:	e8 e1 fb ff ff       	call   f010672e <cpunum>
f0106b4d:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0106b50:	01 c2                	add    %eax,%edx
f0106b52:	01 d2                	add    %edx,%edx
f0106b54:	01 c2                	add    %eax,%edx
f0106b56:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106b59:	8d 04 85 00 50 23 f0 	lea    -0xfdcb000(,%eax,4),%eax
f0106b60:	89 43 08             	mov    %eax,0x8(%ebx)
	get_caller_pcs(lk->pcs);
f0106b63:	83 c3 0c             	add    $0xc,%ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f0106b66:	89 ea                	mov    %ebp,%edx
{
	uint32_t *ebp;
	int i;

	ebp = (uint32_t *)read_ebp();
	for (i = 0; i < 10; i++){
f0106b68:	b8 00 00 00 00       	mov    $0x0,%eax
		if (ebp == 0 || ebp < (uint32_t *)ULIM)
f0106b6d:	81 fa ff ff 7f ef    	cmp    $0xef7fffff,%edx
f0106b73:	76 18                	jbe    f0106b8d <spin_lock+0xa1>
			break;
		pcs[i] = ebp[1];          // saved %eip
f0106b75:	8b 4a 04             	mov    0x4(%edx),%ecx
f0106b78:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
		ebp = (uint32_t *)ebp[0]; // saved %ebp
f0106b7b:	8b 12                	mov    (%edx),%edx
{
	uint32_t *ebp;
	int i;

	ebp = (uint32_t *)read_ebp();
	for (i = 0; i < 10; i++){
f0106b7d:	40                   	inc    %eax
f0106b7e:	83 f8 0a             	cmp    $0xa,%eax
f0106b81:	75 ea                	jne    f0106b6d <spin_lock+0x81>
f0106b83:	eb 0d                	jmp    f0106b92 <spin_lock+0xa6>
			break;
		pcs[i] = ebp[1];          // saved %eip
		ebp = (uint32_t *)ebp[0]; // saved %ebp
	}
	for (; i < 10; i++)
		pcs[i] = 0;
f0106b85:	c7 04 83 00 00 00 00 	movl   $0x0,(%ebx,%eax,4)
		if (ebp == 0 || ebp < (uint32_t *)ULIM)
			break;
		pcs[i] = ebp[1];          // saved %eip
		ebp = (uint32_t *)ebp[0]; // saved %ebp
	}
	for (; i < 10; i++)
f0106b8c:	40                   	inc    %eax
f0106b8d:	83 f8 09             	cmp    $0x9,%eax
f0106b90:	7e f3                	jle    f0106b85 <spin_lock+0x99>
	// Record info about lock acquisition for debugging.
#ifdef DEBUG_SPINLOCK
	lk->cpu = thiscpu;
	get_caller_pcs(lk->pcs);
#endif
}
f0106b92:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106b95:	5b                   	pop    %ebx
f0106b96:	5e                   	pop    %esi
f0106b97:	5d                   	pop    %ebp
f0106b98:	c3                   	ret    

f0106b99 <spin_unlock>:

// Release the lock.
void
spin_unlock(struct spinlock *lk)
{
f0106b99:	55                   	push   %ebp
f0106b9a:	89 e5                	mov    %esp,%ebp
f0106b9c:	57                   	push   %edi
f0106b9d:	56                   	push   %esi
f0106b9e:	53                   	push   %ebx
f0106b9f:	83 ec 4c             	sub    $0x4c,%esp
f0106ba2:	8b 75 08             	mov    0x8(%ebp),%esi

// Check whether this CPU is holding the lock.
static int
holding(struct spinlock *lock)
{
	return lock->locked && lock->cpu == thiscpu;
f0106ba5:	83 3e 00             	cmpl   $0x0,(%esi)
f0106ba8:	74 23                	je     f0106bcd <spin_unlock+0x34>
f0106baa:	8b 5e 08             	mov    0x8(%esi),%ebx
f0106bad:	e8 7c fb ff ff       	call   f010672e <cpunum>
f0106bb2:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0106bb5:	01 c2                	add    %eax,%edx
f0106bb7:	01 d2                	add    %edx,%edx
f0106bb9:	01 c2                	add    %eax,%edx
f0106bbb:	8d 04 90             	lea    (%eax,%edx,4),%eax
f0106bbe:	8d 04 85 00 50 23 f0 	lea    -0xfdcb000(,%eax,4),%eax
// Release the lock.
void
spin_unlock(struct spinlock *lk)
{
#ifdef DEBUG_SPINLOCK
	if (!holding(lk)) {
f0106bc5:	39 c3                	cmp    %eax,%ebx
f0106bc7:	0f 84 b1 00 00 00    	je     f0106c7e <spin_unlock+0xe5>
		int i;
		uint32_t pcs[10];
		// Nab the acquiring EIP chain before it gets released
		memmove(pcs, lk->pcs, sizeof pcs);
f0106bcd:	83 ec 04             	sub    $0x4,%esp
f0106bd0:	6a 28                	push   $0x28
f0106bd2:	8d 46 0c             	lea    0xc(%esi),%eax
f0106bd5:	50                   	push   %eax
f0106bd6:	8d 5d c0             	lea    -0x40(%ebp),%ebx
f0106bd9:	53                   	push   %ebx
f0106bda:	e8 96 f4 ff ff       	call   f0106075 <memmove>
		cprintf("CPU %d cannot release %s: held by CPU %d\nAcquired at:", 
f0106bdf:	8b 7e 08             	mov    0x8(%esi),%edi
f0106be2:	81 ef 00 50 23 f0    	sub    $0xf0235000,%edi
f0106be8:	c1 ff 02             	sar    $0x2,%edi
f0106beb:	69 ff 35 c2 72 4f    	imul   $0x4f72c235,%edi,%edi
f0106bf1:	8b 76 04             	mov    0x4(%esi),%esi
f0106bf4:	e8 35 fb ff ff       	call   f010672e <cpunum>
f0106bf9:	57                   	push   %edi
f0106bfa:	56                   	push   %esi
f0106bfb:	50                   	push   %eax
f0106bfc:	68 ec 9a 10 f0       	push   $0xf0109aec
f0106c01:	e8 0b d0 ff ff       	call   f0103c11 <cprintf>
f0106c06:	83 c4 20             	add    $0x20,%esp
			cpunum(), lk->name, lk->cpu - cpus);
		for (i = 0; i < 10 && pcs[i]; i++) {
			struct Eipdebuginfo info;
			if (debuginfo_eip(pcs[i], &info) >= 0)
f0106c09:	8d 7d a8             	lea    -0x58(%ebp),%edi
f0106c0c:	eb 54                	jmp    f0106c62 <spin_unlock+0xc9>
f0106c0e:	83 ec 08             	sub    $0x8,%esp
f0106c11:	57                   	push   %edi
f0106c12:	50                   	push   %eax
f0106c13:	e8 55 e8 ff ff       	call   f010546d <debuginfo_eip>
f0106c18:	83 c4 10             	add    $0x10,%esp
f0106c1b:	85 c0                	test   %eax,%eax
f0106c1d:	78 27                	js     f0106c46 <spin_unlock+0xad>
				cprintf("  %08x %s:%d: %.*s+%x\n", pcs[i],
					info.eip_file, info.eip_line,
					info.eip_fn_namelen, info.eip_fn_name,
					pcs[i] - info.eip_fn_addr);
f0106c1f:	8b 06                	mov    (%esi),%eax
		cprintf("CPU %d cannot release %s: held by CPU %d\nAcquired at:", 
			cpunum(), lk->name, lk->cpu - cpus);
		for (i = 0; i < 10 && pcs[i]; i++) {
			struct Eipdebuginfo info;
			if (debuginfo_eip(pcs[i], &info) >= 0)
				cprintf("  %08x %s:%d: %.*s+%x\n", pcs[i],
f0106c21:	83 ec 04             	sub    $0x4,%esp
f0106c24:	89 c2                	mov    %eax,%edx
f0106c26:	2b 55 b8             	sub    -0x48(%ebp),%edx
f0106c29:	52                   	push   %edx
f0106c2a:	ff 75 b0             	pushl  -0x50(%ebp)
f0106c2d:	ff 75 b4             	pushl  -0x4c(%ebp)
f0106c30:	ff 75 ac             	pushl  -0x54(%ebp)
f0106c33:	ff 75 a8             	pushl  -0x58(%ebp)
f0106c36:	50                   	push   %eax
f0106c37:	68 32 9b 10 f0       	push   $0xf0109b32
f0106c3c:	e8 d0 cf ff ff       	call   f0103c11 <cprintf>
f0106c41:	83 c4 20             	add    $0x20,%esp
f0106c44:	eb 12                	jmp    f0106c58 <spin_unlock+0xbf>
					info.eip_file, info.eip_line,
					info.eip_fn_namelen, info.eip_fn_name,
					pcs[i] - info.eip_fn_addr);
			else
				cprintf("  %08x\n", pcs[i]);
f0106c46:	83 ec 08             	sub    $0x8,%esp
f0106c49:	ff 36                	pushl  (%esi)
f0106c4b:	68 49 9b 10 f0       	push   $0xf0109b49
f0106c50:	e8 bc cf ff ff       	call   f0103c11 <cprintf>
f0106c55:	83 c4 10             	add    $0x10,%esp
f0106c58:	83 c3 04             	add    $0x4,%ebx
		uint32_t pcs[10];
		// Nab the acquiring EIP chain before it gets released
		memmove(pcs, lk->pcs, sizeof pcs);
		cprintf("CPU %d cannot release %s: held by CPU %d\nAcquired at:", 
			cpunum(), lk->name, lk->cpu - cpus);
		for (i = 0; i < 10 && pcs[i]; i++) {
f0106c5b:	8d 45 e8             	lea    -0x18(%ebp),%eax
f0106c5e:	39 c3                	cmp    %eax,%ebx
f0106c60:	74 08                	je     f0106c6a <spin_unlock+0xd1>
f0106c62:	89 de                	mov    %ebx,%esi
f0106c64:	8b 03                	mov    (%ebx),%eax
f0106c66:	85 c0                	test   %eax,%eax
f0106c68:	75 a4                	jne    f0106c0e <spin_unlock+0x75>
					info.eip_fn_namelen, info.eip_fn_name,
					pcs[i] - info.eip_fn_addr);
			else
				cprintf("  %08x\n", pcs[i]);
		}
		panic("spin_unlock");
f0106c6a:	83 ec 04             	sub    $0x4,%esp
f0106c6d:	68 51 9b 10 f0       	push   $0xf0109b51
f0106c72:	6a 67                	push   $0x67
f0106c74:	68 22 9b 10 f0       	push   $0xf0109b22
f0106c79:	e8 ce 93 ff ff       	call   f010004c <_panic>
	}

	lk->pcs[0] = 0;
f0106c7e:	c7 46 0c 00 00 00 00 	movl   $0x0,0xc(%esi)
	lk->cpu = 0;
f0106c85:	c7 46 08 00 00 00 00 	movl   $0x0,0x8(%esi)
xchg(volatile uint32_t *addr, uint32_t newval)
{
	uint32_t result;

	// The + in "+m" denotes a read-modify-write operand.
	asm volatile("lock; xchgl %0, %1"
f0106c8c:	b8 00 00 00 00       	mov    $0x0,%eax
f0106c91:	f0 87 06             	lock xchg %eax,(%esi)
	// Paper says that Intel 64 and IA-32 will not move a load
	// after a store. So lock->locked = 0 would work here.
	// The xchg being asm volatile ensures gcc emits it after
	// the above assignments (and after the critical section).
	xchg(&lk->locked, 0);
}
f0106c94:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106c97:	5b                   	pop    %ebx
f0106c98:	5e                   	pop    %esi
f0106c99:	5f                   	pop    %edi
f0106c9a:	5d                   	pop    %ebp
f0106c9b:	c3                   	ret    

f0106c9c <time_tick>:
// This should be called once per timer interrupt.  A timer interrupt
// fires every 10 ms.
void
time_tick(void)
{
	ticks++;
f0106c9c:	a1 0c 3f 23 f0       	mov    0xf0233f0c,%eax
f0106ca1:	40                   	inc    %eax
f0106ca2:	a3 0c 3f 23 f0       	mov    %eax,0xf0233f0c
	if (ticks * 10 < ticks)
f0106ca7:	8d 14 80             	lea    (%eax,%eax,4),%edx
f0106caa:	01 d2                	add    %edx,%edx
f0106cac:	39 d0                	cmp    %edx,%eax
f0106cae:	76 17                	jbe    f0106cc7 <time_tick+0x2b>

// This should be called once per timer interrupt.  A timer interrupt
// fires every 10 ms.
void
time_tick(void)
{
f0106cb0:	55                   	push   %ebp
f0106cb1:	89 e5                	mov    %esp,%ebp
f0106cb3:	83 ec 0c             	sub    $0xc,%esp
	ticks++;
	if (ticks * 10 < ticks)
		panic("time_tick: time overflowed");
f0106cb6:	68 69 9b 10 f0       	push   $0xf0109b69
f0106cbb:	6a 0d                	push   $0xd
f0106cbd:	68 84 9b 10 f0       	push   $0xf0109b84
f0106cc2:	e8 85 93 ff ff       	call   f010004c <_panic>
f0106cc7:	c3                   	ret    

f0106cc8 <time_msec>:
}

unsigned int
time_msec(void)
{
f0106cc8:	55                   	push   %ebp
f0106cc9:	89 e5                	mov    %esp,%ebp
	return ticks * 10;
f0106ccb:	a1 0c 3f 23 f0       	mov    0xf0233f0c,%eax
f0106cd0:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0106cd3:	01 c0                	add    %eax,%eax
}
f0106cd5:	5d                   	pop    %ebp
f0106cd6:	c3                   	ret    

f0106cd7 <pci_conf1_set_addr>:
static void
pci_conf1_set_addr(uint32_t bus,
		   uint32_t dev,
		   uint32_t func,
		   uint32_t offset)
{
f0106cd7:	55                   	push   %ebp
f0106cd8:	89 e5                	mov    %esp,%ebp
f0106cda:	53                   	push   %ebx
f0106cdb:	83 ec 04             	sub    $0x4,%esp
f0106cde:	8b 5d 08             	mov    0x8(%ebp),%ebx
	assert(bus < 256);
f0106ce1:	3d ff 00 00 00       	cmp    $0xff,%eax
f0106ce6:	76 16                	jbe    f0106cfe <pci_conf1_set_addr+0x27>
f0106ce8:	68 90 9b 10 f0       	push   $0xf0109b90
f0106ced:	68 51 75 10 f0       	push   $0xf0107551
f0106cf2:	6a 3e                	push   $0x3e
f0106cf4:	68 9a 9b 10 f0       	push   $0xf0109b9a
f0106cf9:	e8 4e 93 ff ff       	call   f010004c <_panic>
	assert(dev < 32);
f0106cfe:	83 fa 1f             	cmp    $0x1f,%edx
f0106d01:	76 16                	jbe    f0106d19 <pci_conf1_set_addr+0x42>
f0106d03:	68 a5 9b 10 f0       	push   $0xf0109ba5
f0106d08:	68 51 75 10 f0       	push   $0xf0107551
f0106d0d:	6a 3f                	push   $0x3f
f0106d0f:	68 9a 9b 10 f0       	push   $0xf0109b9a
f0106d14:	e8 33 93 ff ff       	call   f010004c <_panic>
	assert(func < 8);
f0106d19:	83 f9 07             	cmp    $0x7,%ecx
f0106d1c:	76 16                	jbe    f0106d34 <pci_conf1_set_addr+0x5d>
f0106d1e:	68 ae 9b 10 f0       	push   $0xf0109bae
f0106d23:	68 51 75 10 f0       	push   $0xf0107551
f0106d28:	6a 40                	push   $0x40
f0106d2a:	68 9a 9b 10 f0       	push   $0xf0109b9a
f0106d2f:	e8 18 93 ff ff       	call   f010004c <_panic>
	assert(offset < 256);
f0106d34:	81 fb ff 00 00 00    	cmp    $0xff,%ebx
f0106d3a:	76 16                	jbe    f0106d52 <pci_conf1_set_addr+0x7b>
f0106d3c:	68 b7 9b 10 f0       	push   $0xf0109bb7
f0106d41:	68 51 75 10 f0       	push   $0xf0107551
f0106d46:	6a 41                	push   $0x41
f0106d48:	68 9a 9b 10 f0       	push   $0xf0109b9a
f0106d4d:	e8 fa 92 ff ff       	call   f010004c <_panic>
	assert((offset & 0x3) == 0);
f0106d52:	f6 c3 03             	test   $0x3,%bl
f0106d55:	74 16                	je     f0106d6d <pci_conf1_set_addr+0x96>
f0106d57:	68 c4 9b 10 f0       	push   $0xf0109bc4
f0106d5c:	68 51 75 10 f0       	push   $0xf0107551
f0106d61:	6a 42                	push   $0x42
f0106d63:	68 9a 9b 10 f0       	push   $0xf0109b9a
f0106d68:	e8 df 92 ff ff       	call   f010004c <_panic>
}

static inline void
outl(int port, uint32_t data)
{
	asm volatile("outl %0,%w1" : : "a" (data), "d" (port));
f0106d6d:	c1 e1 08             	shl    $0x8,%ecx
f0106d70:	81 cb 00 00 00 80    	or     $0x80000000,%ebx
f0106d76:	09 cb                	or     %ecx,%ebx
f0106d78:	c1 e2 0b             	shl    $0xb,%edx
f0106d7b:	09 d3                	or     %edx,%ebx
f0106d7d:	c1 e0 10             	shl    $0x10,%eax
f0106d80:	09 d8                	or     %ebx,%eax
f0106d82:	ba f8 0c 00 00       	mov    $0xcf8,%edx
f0106d87:	ef                   	out    %eax,(%dx)

	uint32_t v = BIT(31) |		// config-space
		(bus << 16) | (dev << 11) | (func << 8) | (offset);
	outl(pci_conf1_addr_ioport, v);
}
f0106d88:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0106d8b:	c9                   	leave  
f0106d8c:	c3                   	ret    

f0106d8d <pci_conf_read>:

static uint32_t
pci_conf_read(struct pci_func *f, uint32_t off)
{
f0106d8d:	55                   	push   %ebp
f0106d8e:	89 e5                	mov    %esp,%ebp
f0106d90:	53                   	push   %ebx
f0106d91:	83 ec 10             	sub    $0x10,%esp
	pci_conf1_set_addr(f->bus->busno, f->dev, f->func, off);
f0106d94:	8b 48 08             	mov    0x8(%eax),%ecx
f0106d97:	8b 58 04             	mov    0x4(%eax),%ebx
f0106d9a:	8b 00                	mov    (%eax),%eax
f0106d9c:	8b 40 04             	mov    0x4(%eax),%eax
f0106d9f:	52                   	push   %edx
f0106da0:	89 da                	mov    %ebx,%edx
f0106da2:	e8 30 ff ff ff       	call   f0106cd7 <pci_conf1_set_addr>

static inline uint32_t
inl(int port)
{
	uint32_t data;
	asm volatile("inl %w1,%0" : "=a" (data) : "d" (port));
f0106da7:	ba fc 0c 00 00       	mov    $0xcfc,%edx
f0106dac:	ed                   	in     (%dx),%eax
	return inl(pci_conf1_data_ioport);
}
f0106dad:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0106db0:	c9                   	leave  
f0106db1:	c3                   	ret    

f0106db2 <pci_conf_write>:

static void
pci_conf_write(struct pci_func *f, uint32_t off, uint32_t v)
{
f0106db2:	55                   	push   %ebp
f0106db3:	89 e5                	mov    %esp,%ebp
f0106db5:	56                   	push   %esi
f0106db6:	53                   	push   %ebx
f0106db7:	89 cb                	mov    %ecx,%ebx
	pci_conf1_set_addr(f->bus->busno, f->dev, f->func, off);
f0106db9:	83 ec 0c             	sub    $0xc,%esp
f0106dbc:	8b 48 08             	mov    0x8(%eax),%ecx
f0106dbf:	8b 70 04             	mov    0x4(%eax),%esi
f0106dc2:	8b 00                	mov    (%eax),%eax
f0106dc4:	8b 40 04             	mov    0x4(%eax),%eax
f0106dc7:	52                   	push   %edx
f0106dc8:	89 f2                	mov    %esi,%edx
f0106dca:	e8 08 ff ff ff       	call   f0106cd7 <pci_conf1_set_addr>
}

static inline void
outl(int port, uint32_t data)
{
	asm volatile("outl %0,%w1" : : "a" (data), "d" (port));
f0106dcf:	ba fc 0c 00 00       	mov    $0xcfc,%edx
f0106dd4:	89 d8                	mov    %ebx,%eax
f0106dd6:	ef                   	out    %eax,(%dx)
	outl(pci_conf1_data_ioport, v);
}
f0106dd7:	83 c4 10             	add    $0x10,%esp
f0106dda:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0106ddd:	5b                   	pop    %ebx
f0106dde:	5e                   	pop    %esi
f0106ddf:	5d                   	pop    %ebp
f0106de0:	c3                   	ret    

f0106de1 <pci_attach_match>:

static int __attribute__((warn_unused_result))
pci_attach_match(uint32_t key1, uint32_t key2,
		 struct pci_driver *list, struct pci_func *pcif)
{
f0106de1:	55                   	push   %ebp
f0106de2:	89 e5                	mov    %esp,%ebp
f0106de4:	57                   	push   %edi
f0106de5:	56                   	push   %esi
f0106de6:	53                   	push   %ebx
f0106de7:	83 ec 0c             	sub    $0xc,%esp
f0106dea:	8b 7d 08             	mov    0x8(%ebp),%edi
f0106ded:	8b 45 10             	mov    0x10(%ebp),%eax
f0106df0:	8d 58 08             	lea    0x8(%eax),%ebx
	uint32_t i;

	for (i = 0; list[i].attachfn; i++) {
f0106df3:	eb 3a                	jmp    f0106e2f <pci_attach_match+0x4e>
		if (list[i].key1 == key1 && list[i].key2 == key2) {
f0106df5:	39 7b f8             	cmp    %edi,-0x8(%ebx)
f0106df8:	75 32                	jne    f0106e2c <pci_attach_match+0x4b>
f0106dfa:	8b 55 0c             	mov    0xc(%ebp),%edx
f0106dfd:	39 56 fc             	cmp    %edx,-0x4(%esi)
f0106e00:	75 2a                	jne    f0106e2c <pci_attach_match+0x4b>
			int r = list[i].attachfn(pcif);
f0106e02:	83 ec 0c             	sub    $0xc,%esp
f0106e05:	ff 75 14             	pushl  0x14(%ebp)
f0106e08:	ff d0                	call   *%eax
			if (r > 0)
f0106e0a:	83 c4 10             	add    $0x10,%esp
f0106e0d:	85 c0                	test   %eax,%eax
f0106e0f:	7f 26                	jg     f0106e37 <pci_attach_match+0x56>
				return r;
			if (r < 0)
f0106e11:	85 c0                	test   %eax,%eax
f0106e13:	79 17                	jns    f0106e2c <pci_attach_match+0x4b>
				cprintf("pci_attach_match: attaching "
f0106e15:	83 ec 0c             	sub    $0xc,%esp
f0106e18:	50                   	push   %eax
f0106e19:	ff 36                	pushl  (%esi)
f0106e1b:	ff 75 0c             	pushl  0xc(%ebp)
f0106e1e:	57                   	push   %edi
f0106e1f:	68 dc 9c 10 f0       	push   $0xf0109cdc
f0106e24:	e8 e8 cd ff ff       	call   f0103c11 <cprintf>
f0106e29:	83 c4 20             	add    $0x20,%esp
f0106e2c:	83 c3 0c             	add    $0xc,%ebx
f0106e2f:	89 de                	mov    %ebx,%esi
pci_attach_match(uint32_t key1, uint32_t key2,
		 struct pci_driver *list, struct pci_func *pcif)
{
	uint32_t i;

	for (i = 0; list[i].attachfn; i++) {
f0106e31:	8b 03                	mov    (%ebx),%eax
f0106e33:	85 c0                	test   %eax,%eax
f0106e35:	75 be                	jne    f0106df5 <pci_attach_match+0x14>
					"%x.%x (%p): e\n",
					key1, key2, list[i].attachfn, r);
		}
	}
	return 0;
}
f0106e37:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106e3a:	5b                   	pop    %ebx
f0106e3b:	5e                   	pop    %esi
f0106e3c:	5f                   	pop    %edi
f0106e3d:	5d                   	pop    %ebp
f0106e3e:	c3                   	ret    

f0106e3f <pci_scan_bus>:
		PCI_INTERFACE(f->dev_class), desc);
}

static int
pci_scan_bus(struct pci_bus *bus)
{
f0106e3f:	55                   	push   %ebp
f0106e40:	89 e5                	mov    %esp,%ebp
f0106e42:	57                   	push   %edi
f0106e43:	56                   	push   %esi
f0106e44:	53                   	push   %ebx
f0106e45:	81 ec fc 00 00 00    	sub    $0xfc,%esp
f0106e4b:	89 c2                	mov    %eax,%edx
	int totaldev = 0;
	struct pci_func df = { .bus = bus };
f0106e4d:	8d 7d a0             	lea    -0x60(%ebp),%edi
f0106e50:	b9 12 00 00 00       	mov    $0x12,%ecx
f0106e55:	b8 00 00 00 00       	mov    $0x0,%eax
f0106e5a:	f3 ab                	rep stos %eax,%es:(%edi)
f0106e5c:	89 55 a0             	mov    %edx,-0x60(%ebp)
}

static int
pci_scan_bus(struct pci_bus *bus)
{
	int totaldev = 0;
f0106e5f:	c7 85 00 ff ff ff 00 	movl   $0x0,-0x100(%ebp)
f0106e66:	00 00 00 
	struct pci_func df = { .bus = bus };

	for (df.dev = 0; df.dev < 32; df.dev++) {
f0106e69:	e9 71 01 00 00       	jmp    f0106fdf <pci_scan_bus+0x1a0>
		uint32_t bhlc = pci_conf_read(&df, PCI_BHLC_REG);
f0106e6e:	ba 0c 00 00 00       	mov    $0xc,%edx
f0106e73:	8d 45 a0             	lea    -0x60(%ebp),%eax
f0106e76:	e8 12 ff ff ff       	call   f0106d8d <pci_conf_read>
		if (PCI_HDRTYPE_TYPE(bhlc) > 1)	    // Unsupported or no device
f0106e7b:	89 c2                	mov    %eax,%edx
f0106e7d:	c1 ea 10             	shr    $0x10,%edx
f0106e80:	83 e2 7f             	and    $0x7f,%edx
f0106e83:	83 fa 01             	cmp    $0x1,%edx
f0106e86:	0f 87 50 01 00 00    	ja     f0106fdc <pci_scan_bus+0x19d>
			continue;

		totaldev++;
f0106e8c:	ff 85 00 ff ff ff    	incl   -0x100(%ebp)

		struct pci_func f = df;
f0106e92:	8d bd 10 ff ff ff    	lea    -0xf0(%ebp),%edi
f0106e98:	8d 75 a0             	lea    -0x60(%ebp),%esi
f0106e9b:	b9 12 00 00 00       	mov    $0x12,%ecx
f0106ea0:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		for (f.func = 0; f.func < (PCI_HDRTYPE_MULTIFN(bhlc) ? 8 : 1);
f0106ea2:	c7 85 18 ff ff ff 00 	movl   $0x0,-0xe8(%ebp)
f0106ea9:	00 00 00 
f0106eac:	25 00 00 80 00       	and    $0x800000,%eax
f0106eb1:	83 f8 01             	cmp    $0x1,%eax
f0106eb4:	19 c0                	sbb    %eax,%eax
f0106eb6:	83 e0 f9             	and    $0xfffffff9,%eax
f0106eb9:	83 c0 08             	add    $0x8,%eax
f0106ebc:	89 85 04 ff ff ff    	mov    %eax,-0xfc(%ebp)

			af.dev_id = pci_conf_read(&f, PCI_ID_REG);
			if (PCI_VENDOR(af.dev_id) == 0xffff)
				continue;

			uint32_t intr = pci_conf_read(&af, PCI_INTERRUPT_REG);
f0106ec2:	8d 9d 58 ff ff ff    	lea    -0xa8(%ebp),%ebx
			continue;

		totaldev++;

		struct pci_func f = df;
		for (f.func = 0; f.func < (PCI_HDRTYPE_MULTIFN(bhlc) ? 8 : 1);
f0106ec8:	e9 fd 00 00 00       	jmp    f0106fca <pci_scan_bus+0x18b>
		     f.func++) {
			struct pci_func af = f;
f0106ecd:	8d bd 58 ff ff ff    	lea    -0xa8(%ebp),%edi
f0106ed3:	8d b5 10 ff ff ff    	lea    -0xf0(%ebp),%esi
f0106ed9:	b9 12 00 00 00       	mov    $0x12,%ecx
f0106ede:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

			af.dev_id = pci_conf_read(&f, PCI_ID_REG);
f0106ee0:	ba 00 00 00 00       	mov    $0x0,%edx
f0106ee5:	8d 85 10 ff ff ff    	lea    -0xf0(%ebp),%eax
f0106eeb:	e8 9d fe ff ff       	call   f0106d8d <pci_conf_read>
f0106ef0:	89 85 64 ff ff ff    	mov    %eax,-0x9c(%ebp)
			if (PCI_VENDOR(af.dev_id) == 0xffff)
f0106ef6:	66 83 f8 ff          	cmp    $0xffff,%ax
f0106efa:	0f 84 c4 00 00 00    	je     f0106fc4 <pci_scan_bus+0x185>
				continue;

			uint32_t intr = pci_conf_read(&af, PCI_INTERRUPT_REG);
f0106f00:	ba 3c 00 00 00       	mov    $0x3c,%edx
f0106f05:	89 d8                	mov    %ebx,%eax
f0106f07:	e8 81 fe ff ff       	call   f0106d8d <pci_conf_read>
			af.irq_line = PCI_INTERRUPT_LINE(intr);
f0106f0c:	88 45 9c             	mov    %al,-0x64(%ebp)

			af.dev_class = pci_conf_read(&af, PCI_CLASS_REG);
f0106f0f:	ba 08 00 00 00       	mov    $0x8,%edx
f0106f14:	89 d8                	mov    %ebx,%eax
f0106f16:	e8 72 fe ff ff       	call   f0106d8d <pci_conf_read>
f0106f1b:	89 85 68 ff ff ff    	mov    %eax,-0x98(%ebp)
static void
pci_print_func(struct pci_func *f)
{
	const char *desc = NULL;

	if (PCI_CLASS(f->dev_class) < ARRAY_SIZE(pci_class))
f0106f21:	89 c1                	mov    %eax,%ecx
f0106f23:	c1 e9 18             	shr    $0x18,%ecx
f0106f26:	83 f9 0d             	cmp    $0xd,%ecx
f0106f29:	77 12                	ja     f0106f3d <pci_scan_bus+0xfe>
		desc = pci_class[PCI_CLASS(f->dev_class)];
f0106f2b:	8b 34 8d 00 9e 10 f0 	mov    -0xfef6200(,%ecx,4),%esi
	if (!desc)
f0106f32:	85 f6                	test   %esi,%esi
f0106f34:	75 0c                	jne    f0106f42 <pci_scan_bus+0x103>
		desc = "Unknown";
f0106f36:	be d8 9b 10 f0       	mov    $0xf0109bd8,%esi
f0106f3b:	eb 05                	jmp    f0106f42 <pci_scan_bus+0x103>
f0106f3d:	be d8 9b 10 f0       	mov    $0xf0109bd8,%esi

	cprintf("PCI: %02x:%02x.%d %04x:%04x %02x.%02x.%02x %s\n",
		f->bus->busno, f->dev, f->func,
		PCI_VENDOR(f->dev_id), PCI_PRODUCT(f->dev_id),
f0106f42:	8b 95 64 ff ff ff    	mov    -0x9c(%ebp),%edx
	if (PCI_CLASS(f->dev_class) < ARRAY_SIZE(pci_class))
		desc = pci_class[PCI_CLASS(f->dev_class)];
	if (!desc)
		desc = "Unknown";

	cprintf("PCI: %02x:%02x.%d %04x:%04x %02x.%02x.%02x %s\n",
f0106f48:	83 ec 08             	sub    $0x8,%esp
f0106f4b:	56                   	push   %esi
f0106f4c:	0f b6 f4             	movzbl %ah,%esi
f0106f4f:	56                   	push   %esi
f0106f50:	c1 e8 10             	shr    $0x10,%eax
f0106f53:	0f b6 c0             	movzbl %al,%eax
f0106f56:	50                   	push   %eax
f0106f57:	51                   	push   %ecx
f0106f58:	89 d0                	mov    %edx,%eax
f0106f5a:	c1 e8 10             	shr    $0x10,%eax
f0106f5d:	50                   	push   %eax
f0106f5e:	0f b7 d2             	movzwl %dx,%edx
f0106f61:	52                   	push   %edx
f0106f62:	ff b5 60 ff ff ff    	pushl  -0xa0(%ebp)
f0106f68:	ff b5 5c ff ff ff    	pushl  -0xa4(%ebp)
f0106f6e:	8b 85 58 ff ff ff    	mov    -0xa8(%ebp),%eax
f0106f74:	ff 70 04             	pushl  0x4(%eax)
f0106f77:	68 08 9d 10 f0       	push   $0xf0109d08
f0106f7c:	e8 90 cc ff ff       	call   f0103c11 <cprintf>

static int
pci_attach(struct pci_func *f)
{
	return
		pci_attach_match(PCI_CLASS(f->dev_class),
f0106f81:	83 c4 30             	add    $0x30,%esp
f0106f84:	53                   	push   %ebx
f0106f85:	68 20 54 12 f0       	push   $0xf0125420
f0106f8a:	0f b6 85 6a ff ff ff 	movzbl -0x96(%ebp),%eax
f0106f91:	50                   	push   %eax
f0106f92:	0f b6 85 6b ff ff ff 	movzbl -0x95(%ebp),%eax
f0106f99:	50                   	push   %eax
f0106f9a:	e8 42 fe ff ff       	call   f0106de1 <pci_attach_match>
				 PCI_SUBCLASS(f->dev_class),
				 &pci_attach_class[0], f) ||
f0106f9f:	83 c4 10             	add    $0x10,%esp
f0106fa2:	85 c0                	test   %eax,%eax
f0106fa4:	75 1e                	jne    f0106fc4 <pci_scan_bus+0x185>
		pci_attach_match(PCI_VENDOR(f->dev_id),
				 PCI_PRODUCT(f->dev_id),
f0106fa6:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
{
	return
		pci_attach_match(PCI_CLASS(f->dev_class),
				 PCI_SUBCLASS(f->dev_class),
				 &pci_attach_class[0], f) ||
		pci_attach_match(PCI_VENDOR(f->dev_id),
f0106fac:	53                   	push   %ebx
f0106fad:	68 00 54 12 f0       	push   $0xf0125400
f0106fb2:	89 c2                	mov    %eax,%edx
f0106fb4:	c1 ea 10             	shr    $0x10,%edx
f0106fb7:	52                   	push   %edx
f0106fb8:	0f b7 c0             	movzwl %ax,%eax
f0106fbb:	50                   	push   %eax
f0106fbc:	e8 20 fe ff ff       	call   f0106de1 <pci_attach_match>
f0106fc1:	83 c4 10             	add    $0x10,%esp

		totaldev++;

		struct pci_func f = df;
		for (f.func = 0; f.func < (PCI_HDRTYPE_MULTIFN(bhlc) ? 8 : 1);
		     f.func++) {
f0106fc4:	ff 85 18 ff ff ff    	incl   -0xe8(%ebp)
			continue;

		totaldev++;

		struct pci_func f = df;
		for (f.func = 0; f.func < (PCI_HDRTYPE_MULTIFN(bhlc) ? 8 : 1);
f0106fca:	8b 85 04 ff ff ff    	mov    -0xfc(%ebp),%eax
f0106fd0:	3b 85 18 ff ff ff    	cmp    -0xe8(%ebp),%eax
f0106fd6:	0f 87 f1 fe ff ff    	ja     f0106ecd <pci_scan_bus+0x8e>
pci_scan_bus(struct pci_bus *bus)
{
	int totaldev = 0;
	struct pci_func df = { .bus = bus };

	for (df.dev = 0; df.dev < 32; df.dev++) {
f0106fdc:	ff 45 a4             	incl   -0x5c(%ebp)
f0106fdf:	83 7d a4 1f          	cmpl   $0x1f,-0x5c(%ebp)
f0106fe3:	0f 86 85 fe ff ff    	jbe    f0106e6e <pci_scan_bus+0x2f>
			pci_attach(&af);
		}
	}

	return totaldev;
}
f0106fe9:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
f0106fef:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0106ff2:	5b                   	pop    %ebx
f0106ff3:	5e                   	pop    %esi
f0106ff4:	5f                   	pop    %edi
f0106ff5:	5d                   	pop    %ebp
f0106ff6:	c3                   	ret    

f0106ff7 <pci_bridge_attach>:
		PCI_VENDOR(f->dev_id), PCI_PRODUCT(f->dev_id));
}

static int
pci_bridge_attach(struct pci_func *pcif)
{
f0106ff7:	55                   	push   %ebp
f0106ff8:	89 e5                	mov    %esp,%ebp
f0106ffa:	56                   	push   %esi
f0106ffb:	53                   	push   %ebx
f0106ffc:	83 ec 10             	sub    $0x10,%esp
f0106fff:	8b 5d 08             	mov    0x8(%ebp),%ebx
	uint32_t ioreg  = pci_conf_read(pcif, PCI_BRIDGE_STATIO_REG);
f0107002:	ba 1c 00 00 00       	mov    $0x1c,%edx
f0107007:	89 d8                	mov    %ebx,%eax
f0107009:	e8 7f fd ff ff       	call   f0106d8d <pci_conf_read>
f010700e:	89 c6                	mov    %eax,%esi
	uint32_t busreg = pci_conf_read(pcif, PCI_BRIDGE_BUS_REG);
f0107010:	ba 18 00 00 00       	mov    $0x18,%edx
f0107015:	89 d8                	mov    %ebx,%eax
f0107017:	e8 71 fd ff ff       	call   f0106d8d <pci_conf_read>

	if (PCI_BRIDGE_IO_32BITS(ioreg)) {
f010701c:	83 e6 0f             	and    $0xf,%esi
f010701f:	83 fe 01             	cmp    $0x1,%esi
f0107022:	75 1f                	jne    f0107043 <pci_bridge_attach+0x4c>
		cprintf("PCI: %02x:%02x.%d: 32-bit bridge IO not supported.\n",
f0107024:	ff 73 08             	pushl  0x8(%ebx)
f0107027:	ff 73 04             	pushl  0x4(%ebx)
f010702a:	8b 03                	mov    (%ebx),%eax
f010702c:	ff 70 04             	pushl  0x4(%eax)
f010702f:	68 38 9d 10 f0       	push   $0xf0109d38
f0107034:	e8 d8 cb ff ff       	call   f0103c11 <cprintf>
			pcif->bus->busno, pcif->dev, pcif->func);
		return 0;
f0107039:	83 c4 10             	add    $0x10,%esp
f010703c:	b8 00 00 00 00       	mov    $0x0,%eax
f0107041:	eb 2e                	jmp    f0107071 <pci_bridge_attach+0x7a>
	}

	struct pci_bus nbus = {
f0107043:	89 5d f0             	mov    %ebx,-0x10(%ebp)
		.parent_bridge = pcif,
		.busno = (busreg >> PCI_BRIDGE_BUS_SECONDARY_SHIFT) & 0xff,
f0107046:	0f b6 d4             	movzbl %ah,%edx
		cprintf("PCI: %02x:%02x.%d: 32-bit bridge IO not supported.\n",
			pcif->bus->busno, pcif->dev, pcif->func);
		return 0;
	}

	struct pci_bus nbus = {
f0107049:	89 55 f4             	mov    %edx,-0xc(%ebp)
		.parent_bridge = pcif,
		.busno = (busreg >> PCI_BRIDGE_BUS_SECONDARY_SHIFT) & 0xff,
	};

	if (pci_show_devs)
		cprintf("  bridge to [bus %02x-%02x]\n",
f010704c:	83 ec 04             	sub    $0x4,%esp
f010704f:	c1 e8 10             	shr    $0x10,%eax
f0107052:	0f b6 c0             	movzbl %al,%eax
f0107055:	50                   	push   %eax
f0107056:	52                   	push   %edx
f0107057:	68 e0 9b 10 f0       	push   $0xf0109be0
f010705c:	e8 b0 cb ff ff       	call   f0103c11 <cprintf>
			nbus.busno, (busreg >> PCI_BRIDGE_BUS_SUBORDINATE_SHIFT) & 0xff);

	pci_scan_bus(&nbus);
f0107061:	8d 45 f0             	lea    -0x10(%ebp),%eax
f0107064:	e8 d6 fd ff ff       	call   f0106e3f <pci_scan_bus>
	return 1;
f0107069:	83 c4 10             	add    $0x10,%esp
f010706c:	b8 01 00 00 00       	mov    $0x1,%eax
}
f0107071:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0107074:	5b                   	pop    %ebx
f0107075:	5e                   	pop    %esi
f0107076:	5d                   	pop    %ebp
f0107077:	c3                   	ret    

f0107078 <pci_init>:
	return totaldev;
}

int
pci_init(void)
{
f0107078:	55                   	push   %ebp
f0107079:	89 e5                	mov    %esp,%ebp
f010707b:	83 ec 18             	sub    $0x18,%esp
	struct pci_bus root_bus = { NULL, 0 };
f010707e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
f0107085:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	return pci_scan_bus(&root_bus);
f010708c:	8d 45 f0             	lea    -0x10(%ebp),%eax
f010708f:	e8 ab fd ff ff       	call   f0106e3f <pci_scan_bus>
}
f0107094:	c9                   	leave  
f0107095:	c3                   	ret    

f0107096 <pci_func_enable>:

// External PCI subsystem interface

void
pci_func_enable(struct pci_func *f)
{
f0107096:	55                   	push   %ebp
f0107097:	89 e5                	mov    %esp,%ebp
f0107099:	57                   	push   %edi
f010709a:	56                   	push   %esi
f010709b:	53                   	push   %ebx
f010709c:	83 ec 1c             	sub    $0x1c,%esp
f010709f:	8b 75 08             	mov    0x8(%ebp),%esi
	pci_conf_write(f, PCI_COMMAND_STATUS_REG,
f01070a2:	b9 07 00 00 00       	mov    $0x7,%ecx
f01070a7:	ba 04 00 00 00       	mov    $0x4,%edx
f01070ac:	89 f0                	mov    %esi,%eax
f01070ae:	e8 ff fc ff ff       	call   f0106db2 <pci_conf_write>
		       PCI_COMMAND_IO_ENABLE |
		       PCI_COMMAND_MEM_ENABLE |
		       PCI_COMMAND_MASTER_ENABLE);

	uint32_t bar, bar_width;
	for (bar = PCI_MAPREG_START; bar < PCI_MAPREG_END; bar += bar_width) {
f01070b3:	bf 10 00 00 00       	mov    $0x10,%edi
		uint32_t oldv = pci_conf_read(f, bar);
f01070b8:	89 fa                	mov    %edi,%edx
f01070ba:	89 f0                	mov    %esi,%eax
f01070bc:	e8 cc fc ff ff       	call   f0106d8d <pci_conf_read>
f01070c1:	89 c3                	mov    %eax,%ebx
f01070c3:	89 45 e0             	mov    %eax,-0x20(%ebp)

		bar_width = 4;
		pci_conf_write(f, bar, 0xffffffff);
f01070c6:	b9 ff ff ff ff       	mov    $0xffffffff,%ecx
f01070cb:	89 fa                	mov    %edi,%edx
f01070cd:	89 f0                	mov    %esi,%eax
f01070cf:	e8 de fc ff ff       	call   f0106db2 <pci_conf_write>
		uint32_t rv = pci_conf_read(f, bar);
f01070d4:	89 fa                	mov    %edi,%edx
f01070d6:	89 f0                	mov    %esi,%eax
f01070d8:	e8 b0 fc ff ff       	call   f0106d8d <pci_conf_read>

		if (rv == 0)
f01070dd:	85 c0                	test   %eax,%eax
f01070df:	0f 84 ce 00 00 00    	je     f01071b3 <pci_func_enable+0x11d>
			continue;

		int regnum = PCI_MAPREG_NUM(bar);
f01070e5:	8d 57 f0             	lea    -0x10(%edi),%edx
f01070e8:	c1 ea 02             	shr    $0x2,%edx
f01070eb:	89 55 dc             	mov    %edx,-0x24(%ebp)
		uint32_t base, size;
		if (PCI_MAPREG_TYPE(rv) == PCI_MAPREG_TYPE_MEM) {
f01070ee:	a8 01                	test   $0x1,%al
f01070f0:	75 3f                	jne    f0107131 <pci_func_enable+0x9b>
			if (PCI_MAPREG_MEM_TYPE(rv) == PCI_MAPREG_MEM_TYPE_64BIT)
f01070f2:	89 c1                	mov    %eax,%ecx
f01070f4:	83 e1 06             	and    $0x6,%ecx
				bar_width = 8;
f01070f7:	83 f9 04             	cmp    $0x4,%ecx
f01070fa:	0f 94 c1             	sete   %cl
f01070fd:	0f b6 c9             	movzbl %cl,%ecx
f0107100:	8d 0c 8d 04 00 00 00 	lea    0x4(,%ecx,4),%ecx
f0107107:	89 4d e4             	mov    %ecx,-0x1c(%ebp)

			size = PCI_MAPREG_MEM_SIZE(rv);
f010710a:	83 e0 f0             	and    $0xfffffff0,%eax
f010710d:	89 c1                	mov    %eax,%ecx
f010710f:	f7 d9                	neg    %ecx
f0107111:	21 c1                	and    %eax,%ecx
f0107113:	89 4d d8             	mov    %ecx,-0x28(%ebp)
			base = PCI_MAPREG_MEM_ADDR(oldv);
f0107116:	83 e3 f0             	and    $0xfffffff0,%ebx
			if (pci_show_addrs)
				cprintf("  BAR %d [mem %08p-%08p]\n",
f0107119:	8d 44 0b ff          	lea    -0x1(%ebx,%ecx,1),%eax
f010711d:	50                   	push   %eax
f010711e:	53                   	push   %ebx
f010711f:	ff 75 dc             	pushl  -0x24(%ebp)
f0107122:	68 fd 9b 10 f0       	push   $0xf0109bfd
f0107127:	e8 e5 ca ff ff       	call   f0103c11 <cprintf>
f010712c:	83 c4 10             	add    $0x10,%esp
f010712f:	eb 2f                	jmp    f0107160 <pci_func_enable+0xca>
					regnum, base, base + size - 1);
		} else {
			size = PCI_MAPREG_IO_SIZE(rv);
f0107131:	83 e0 fc             	and    $0xfffffffc,%eax
f0107134:	89 c2                	mov    %eax,%edx
f0107136:	f7 da                	neg    %edx
f0107138:	21 c2                	and    %eax,%edx
f010713a:	89 55 d8             	mov    %edx,-0x28(%ebp)
			base = PCI_MAPREG_IO_ADDR(oldv);
f010713d:	8b 5d e0             	mov    -0x20(%ebp),%ebx
f0107140:	83 e3 fc             	and    $0xfffffffc,%ebx
			if (pci_show_addrs)
				cprintf("  BAR %d [port %04p-%04p]\n",
f0107143:	8d 44 13 ff          	lea    -0x1(%ebx,%edx,1),%eax
f0107147:	50                   	push   %eax
f0107148:	53                   	push   %ebx
f0107149:	ff 75 dc             	pushl  -0x24(%ebp)
f010714c:	68 17 9c 10 f0       	push   $0xf0109c17
f0107151:	e8 bb ca ff ff       	call   f0103c11 <cprintf>
f0107156:	83 c4 10             	add    $0x10,%esp

	uint32_t bar, bar_width;
	for (bar = PCI_MAPREG_START; bar < PCI_MAPREG_END; bar += bar_width) {
		uint32_t oldv = pci_conf_read(f, bar);

		bar_width = 4;
f0107159:	c7 45 e4 04 00 00 00 	movl   $0x4,-0x1c(%ebp)
			if (pci_show_addrs)
				cprintf("  BAR %d [port %04p-%04p]\n",
					regnum, base, base + size - 1);
		}

		pci_conf_write(f, bar, oldv);
f0107160:	8b 4d e0             	mov    -0x20(%ebp),%ecx
f0107163:	89 fa                	mov    %edi,%edx
f0107165:	89 f0                	mov    %esi,%eax
f0107167:	e8 46 fc ff ff       	call   f0106db2 <pci_conf_write>
f010716c:	8b 45 dc             	mov    -0x24(%ebp),%eax
f010716f:	8d 04 86             	lea    (%esi,%eax,4),%eax
		f->reg_base[regnum] = base;
f0107172:	89 58 14             	mov    %ebx,0x14(%eax)
		f->reg_size[regnum] = size;
f0107175:	8b 4d d8             	mov    -0x28(%ebp),%ecx
f0107178:	89 48 2c             	mov    %ecx,0x2c(%eax)

		if (size && !base)
f010717b:	85 c9                	test   %ecx,%ecx
f010717d:	74 3b                	je     f01071ba <pci_func_enable+0x124>
f010717f:	85 db                	test   %ebx,%ebx
f0107181:	75 37                	jne    f01071ba <pci_func_enable+0x124>
			cprintf("PCI device %02x:%02x.%d (%04x:%04x) "
				"may be misconfigured: "
				"region %d: base 0x%x, size %d\n",
				f->bus->busno, f->dev, f->func,
				PCI_VENDOR(f->dev_id), PCI_PRODUCT(f->dev_id),
f0107183:	8b 46 0c             	mov    0xc(%esi),%eax
		pci_conf_write(f, bar, oldv);
		f->reg_base[regnum] = base;
		f->reg_size[regnum] = size;

		if (size && !base)
			cprintf("PCI device %02x:%02x.%d (%04x:%04x) "
f0107186:	83 ec 0c             	sub    $0xc,%esp
f0107189:	51                   	push   %ecx
f010718a:	6a 00                	push   $0x0
f010718c:	ff 75 dc             	pushl  -0x24(%ebp)
f010718f:	89 c2                	mov    %eax,%edx
f0107191:	c1 ea 10             	shr    $0x10,%edx
f0107194:	52                   	push   %edx
f0107195:	0f b7 c0             	movzwl %ax,%eax
f0107198:	50                   	push   %eax
f0107199:	ff 76 08             	pushl  0x8(%esi)
f010719c:	ff 76 04             	pushl  0x4(%esi)
f010719f:	8b 06                	mov    (%esi),%eax
f01071a1:	ff 70 04             	pushl  0x4(%eax)
f01071a4:	68 6c 9d 10 f0       	push   $0xf0109d6c
f01071a9:	e8 63 ca ff ff       	call   f0103c11 <cprintf>
f01071ae:	83 c4 30             	add    $0x30,%esp
f01071b1:	eb 07                	jmp    f01071ba <pci_func_enable+0x124>

	uint32_t bar, bar_width;
	for (bar = PCI_MAPREG_START; bar < PCI_MAPREG_END; bar += bar_width) {
		uint32_t oldv = pci_conf_read(f, bar);

		bar_width = 4;
f01071b3:	c7 45 e4 04 00 00 00 	movl   $0x4,-0x1c(%ebp)
		       PCI_COMMAND_IO_ENABLE |
		       PCI_COMMAND_MEM_ENABLE |
		       PCI_COMMAND_MASTER_ENABLE);

	uint32_t bar, bar_width;
	for (bar = PCI_MAPREG_START; bar < PCI_MAPREG_END; bar += bar_width) {
f01071ba:	03 7d e4             	add    -0x1c(%ebp),%edi
f01071bd:	83 ff 27             	cmp    $0x27,%edi
f01071c0:	0f 86 f2 fe ff ff    	jbe    f01070b8 <pci_func_enable+0x22>
				regnum, base, size);
	}

	cprintf("PCI function %02x:%02x.%d (%04x:%04x) enabled\n",
		f->bus->busno, f->dev, f->func,
		PCI_VENDOR(f->dev_id), PCI_PRODUCT(f->dev_id));
f01071c6:	8b 46 0c             	mov    0xc(%esi),%eax
				f->bus->busno, f->dev, f->func,
				PCI_VENDOR(f->dev_id), PCI_PRODUCT(f->dev_id),
				regnum, base, size);
	}

	cprintf("PCI function %02x:%02x.%d (%04x:%04x) enabled\n",
f01071c9:	83 ec 08             	sub    $0x8,%esp
f01071cc:	89 c2                	mov    %eax,%edx
f01071ce:	c1 ea 10             	shr    $0x10,%edx
f01071d1:	52                   	push   %edx
f01071d2:	0f b7 c0             	movzwl %ax,%eax
f01071d5:	50                   	push   %eax
f01071d6:	ff 76 08             	pushl  0x8(%esi)
f01071d9:	ff 76 04             	pushl  0x4(%esi)
f01071dc:	8b 06                	mov    (%esi),%eax
f01071de:	ff 70 04             	pushl  0x4(%eax)
f01071e1:	68 c8 9d 10 f0       	push   $0xf0109dc8
f01071e6:	e8 26 ca ff ff       	call   f0103c11 <cprintf>
		f->bus->busno, f->dev, f->func,
		PCI_VENDOR(f->dev_id), PCI_PRODUCT(f->dev_id));
}
f01071eb:	83 c4 20             	add    $0x20,%esp
f01071ee:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01071f1:	5b                   	pop    %ebx
f01071f2:	5e                   	pop    %esi
f01071f3:	5f                   	pop    %edi
f01071f4:	5d                   	pop    %ebp
f01071f5:	c3                   	ret    

f01071f6 <ahci_attach>:
// --------------------------------------------------------------
void *ahci_va;

static int
ahci_attach(struct pci_func *pcif)
{
f01071f6:	55                   	push   %ebp
f01071f7:	89 e5                	mov    %esp,%ebp
f01071f9:	53                   	push   %ebx
f01071fa:	83 ec 04             	sub    $0x4,%esp
f01071fd:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (PCI_INTERFACE(pcif->dev_class) != PCI_INTERFACE_SATA_AHCI10)
f0107200:	80 7b 11 01          	cmpb   $0x1,0x11(%ebx)
f0107204:	75 26                	jne    f010722c <ahci_attach+0x36>
		return 0;
	pci_func_enable(pcif);
f0107206:	83 ec 0c             	sub    $0xc,%esp
f0107209:	53                   	push   %ebx
f010720a:	e8 87 fe ff ff       	call   f0107096 <pci_func_enable>
	// BAR 5: AHCI base address
	ahci_va = mmio_map_region(pcif->reg_base[5], pcif->reg_size[5]);
f010720f:	83 c4 08             	add    $0x8,%esp
f0107212:	ff 73 40             	pushl  0x40(%ebx)
f0107215:	ff 73 28             	pushl  0x28(%ebx)
f0107218:	e8 e7 a4 ff ff       	call   f0101704 <mmio_map_region>
f010721d:	a3 08 60 27 f0       	mov    %eax,0xf0276008
	return 1;
f0107222:	83 c4 10             	add    $0x10,%esp
f0107225:	b8 01 00 00 00       	mov    $0x1,%eax
f010722a:	eb 05                	jmp    f0107231 <ahci_attach+0x3b>

static int
ahci_attach(struct pci_func *pcif)
{
	if (PCI_INTERFACE(pcif->dev_class) != PCI_INTERFACE_SATA_AHCI10)
		return 0;
f010722c:	b8 00 00 00 00       	mov    $0x0,%eax
	pci_func_enable(pcif);
	// BAR 5: AHCI base address
	ahci_va = mmio_map_region(pcif->reg_base[5], pcif->reg_size[5]);
	return 1;
}
f0107231:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0107234:	c9                   	leave  
f0107235:	c3                   	ret    
f0107236:	66 90                	xchg   %ax,%ax

f0107238 <__udivdi3>:
f0107238:	55                   	push   %ebp
f0107239:	57                   	push   %edi
f010723a:	56                   	push   %esi
f010723b:	53                   	push   %ebx
f010723c:	83 ec 1c             	sub    $0x1c,%esp
f010723f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f0107243:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f0107247:	8b 7c 24 38          	mov    0x38(%esp),%edi
f010724b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f010724f:	89 ca                	mov    %ecx,%edx
f0107251:	89 f8                	mov    %edi,%eax
f0107253:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f0107257:	85 f6                	test   %esi,%esi
f0107259:	75 2d                	jne    f0107288 <__udivdi3+0x50>
f010725b:	39 cf                	cmp    %ecx,%edi
f010725d:	77 65                	ja     f01072c4 <__udivdi3+0x8c>
f010725f:	89 fd                	mov    %edi,%ebp
f0107261:	85 ff                	test   %edi,%edi
f0107263:	75 0b                	jne    f0107270 <__udivdi3+0x38>
f0107265:	b8 01 00 00 00       	mov    $0x1,%eax
f010726a:	31 d2                	xor    %edx,%edx
f010726c:	f7 f7                	div    %edi
f010726e:	89 c5                	mov    %eax,%ebp
f0107270:	31 d2                	xor    %edx,%edx
f0107272:	89 c8                	mov    %ecx,%eax
f0107274:	f7 f5                	div    %ebp
f0107276:	89 c1                	mov    %eax,%ecx
f0107278:	89 d8                	mov    %ebx,%eax
f010727a:	f7 f5                	div    %ebp
f010727c:	89 cf                	mov    %ecx,%edi
f010727e:	89 fa                	mov    %edi,%edx
f0107280:	83 c4 1c             	add    $0x1c,%esp
f0107283:	5b                   	pop    %ebx
f0107284:	5e                   	pop    %esi
f0107285:	5f                   	pop    %edi
f0107286:	5d                   	pop    %ebp
f0107287:	c3                   	ret    
f0107288:	39 ce                	cmp    %ecx,%esi
f010728a:	77 28                	ja     f01072b4 <__udivdi3+0x7c>
f010728c:	0f bd fe             	bsr    %esi,%edi
f010728f:	83 f7 1f             	xor    $0x1f,%edi
f0107292:	75 40                	jne    f01072d4 <__udivdi3+0x9c>
f0107294:	39 ce                	cmp    %ecx,%esi
f0107296:	72 0a                	jb     f01072a2 <__udivdi3+0x6a>
f0107298:	3b 44 24 08          	cmp    0x8(%esp),%eax
f010729c:	0f 87 9e 00 00 00    	ja     f0107340 <__udivdi3+0x108>
f01072a2:	b8 01 00 00 00       	mov    $0x1,%eax
f01072a7:	89 fa                	mov    %edi,%edx
f01072a9:	83 c4 1c             	add    $0x1c,%esp
f01072ac:	5b                   	pop    %ebx
f01072ad:	5e                   	pop    %esi
f01072ae:	5f                   	pop    %edi
f01072af:	5d                   	pop    %ebp
f01072b0:	c3                   	ret    
f01072b1:	8d 76 00             	lea    0x0(%esi),%esi
f01072b4:	31 ff                	xor    %edi,%edi
f01072b6:	31 c0                	xor    %eax,%eax
f01072b8:	89 fa                	mov    %edi,%edx
f01072ba:	83 c4 1c             	add    $0x1c,%esp
f01072bd:	5b                   	pop    %ebx
f01072be:	5e                   	pop    %esi
f01072bf:	5f                   	pop    %edi
f01072c0:	5d                   	pop    %ebp
f01072c1:	c3                   	ret    
f01072c2:	66 90                	xchg   %ax,%ax
f01072c4:	89 d8                	mov    %ebx,%eax
f01072c6:	f7 f7                	div    %edi
f01072c8:	31 ff                	xor    %edi,%edi
f01072ca:	89 fa                	mov    %edi,%edx
f01072cc:	83 c4 1c             	add    $0x1c,%esp
f01072cf:	5b                   	pop    %ebx
f01072d0:	5e                   	pop    %esi
f01072d1:	5f                   	pop    %edi
f01072d2:	5d                   	pop    %ebp
f01072d3:	c3                   	ret    
f01072d4:	bd 20 00 00 00       	mov    $0x20,%ebp
f01072d9:	89 eb                	mov    %ebp,%ebx
f01072db:	29 fb                	sub    %edi,%ebx
f01072dd:	89 f9                	mov    %edi,%ecx
f01072df:	d3 e6                	shl    %cl,%esi
f01072e1:	89 c5                	mov    %eax,%ebp
f01072e3:	88 d9                	mov    %bl,%cl
f01072e5:	d3 ed                	shr    %cl,%ebp
f01072e7:	89 e9                	mov    %ebp,%ecx
f01072e9:	09 f1                	or     %esi,%ecx
f01072eb:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f01072ef:	89 f9                	mov    %edi,%ecx
f01072f1:	d3 e0                	shl    %cl,%eax
f01072f3:	89 c5                	mov    %eax,%ebp
f01072f5:	89 d6                	mov    %edx,%esi
f01072f7:	88 d9                	mov    %bl,%cl
f01072f9:	d3 ee                	shr    %cl,%esi
f01072fb:	89 f9                	mov    %edi,%ecx
f01072fd:	d3 e2                	shl    %cl,%edx
f01072ff:	8b 44 24 08          	mov    0x8(%esp),%eax
f0107303:	88 d9                	mov    %bl,%cl
f0107305:	d3 e8                	shr    %cl,%eax
f0107307:	09 c2                	or     %eax,%edx
f0107309:	89 d0                	mov    %edx,%eax
f010730b:	89 f2                	mov    %esi,%edx
f010730d:	f7 74 24 0c          	divl   0xc(%esp)
f0107311:	89 d6                	mov    %edx,%esi
f0107313:	89 c3                	mov    %eax,%ebx
f0107315:	f7 e5                	mul    %ebp
f0107317:	39 d6                	cmp    %edx,%esi
f0107319:	72 19                	jb     f0107334 <__udivdi3+0xfc>
f010731b:	74 0b                	je     f0107328 <__udivdi3+0xf0>
f010731d:	89 d8                	mov    %ebx,%eax
f010731f:	31 ff                	xor    %edi,%edi
f0107321:	e9 58 ff ff ff       	jmp    f010727e <__udivdi3+0x46>
f0107326:	66 90                	xchg   %ax,%ax
f0107328:	8b 54 24 08          	mov    0x8(%esp),%edx
f010732c:	89 f9                	mov    %edi,%ecx
f010732e:	d3 e2                	shl    %cl,%edx
f0107330:	39 c2                	cmp    %eax,%edx
f0107332:	73 e9                	jae    f010731d <__udivdi3+0xe5>
f0107334:	8d 43 ff             	lea    -0x1(%ebx),%eax
f0107337:	31 ff                	xor    %edi,%edi
f0107339:	e9 40 ff ff ff       	jmp    f010727e <__udivdi3+0x46>
f010733e:	66 90                	xchg   %ax,%ax
f0107340:	31 c0                	xor    %eax,%eax
f0107342:	e9 37 ff ff ff       	jmp    f010727e <__udivdi3+0x46>
f0107347:	90                   	nop

f0107348 <__umoddi3>:
f0107348:	55                   	push   %ebp
f0107349:	57                   	push   %edi
f010734a:	56                   	push   %esi
f010734b:	53                   	push   %ebx
f010734c:	83 ec 1c             	sub    $0x1c,%esp
f010734f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f0107353:	8b 74 24 34          	mov    0x34(%esp),%esi
f0107357:	8b 7c 24 38          	mov    0x38(%esp),%edi
f010735b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
f010735f:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0107363:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0107367:	89 f3                	mov    %esi,%ebx
f0107369:	89 fa                	mov    %edi,%edx
f010736b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f010736f:	89 34 24             	mov    %esi,(%esp)
f0107372:	85 c0                	test   %eax,%eax
f0107374:	75 1a                	jne    f0107390 <__umoddi3+0x48>
f0107376:	39 f7                	cmp    %esi,%edi
f0107378:	0f 86 a2 00 00 00    	jbe    f0107420 <__umoddi3+0xd8>
f010737e:	89 c8                	mov    %ecx,%eax
f0107380:	89 f2                	mov    %esi,%edx
f0107382:	f7 f7                	div    %edi
f0107384:	89 d0                	mov    %edx,%eax
f0107386:	31 d2                	xor    %edx,%edx
f0107388:	83 c4 1c             	add    $0x1c,%esp
f010738b:	5b                   	pop    %ebx
f010738c:	5e                   	pop    %esi
f010738d:	5f                   	pop    %edi
f010738e:	5d                   	pop    %ebp
f010738f:	c3                   	ret    
f0107390:	39 f0                	cmp    %esi,%eax
f0107392:	0f 87 ac 00 00 00    	ja     f0107444 <__umoddi3+0xfc>
f0107398:	0f bd e8             	bsr    %eax,%ebp
f010739b:	83 f5 1f             	xor    $0x1f,%ebp
f010739e:	0f 84 ac 00 00 00    	je     f0107450 <__umoddi3+0x108>
f01073a4:	bf 20 00 00 00       	mov    $0x20,%edi
f01073a9:	29 ef                	sub    %ebp,%edi
f01073ab:	89 fe                	mov    %edi,%esi
f01073ad:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f01073b1:	89 e9                	mov    %ebp,%ecx
f01073b3:	d3 e0                	shl    %cl,%eax
f01073b5:	89 d7                	mov    %edx,%edi
f01073b7:	89 f1                	mov    %esi,%ecx
f01073b9:	d3 ef                	shr    %cl,%edi
f01073bb:	09 c7                	or     %eax,%edi
f01073bd:	89 e9                	mov    %ebp,%ecx
f01073bf:	d3 e2                	shl    %cl,%edx
f01073c1:	89 14 24             	mov    %edx,(%esp)
f01073c4:	89 d8                	mov    %ebx,%eax
f01073c6:	d3 e0                	shl    %cl,%eax
f01073c8:	89 c2                	mov    %eax,%edx
f01073ca:	8b 44 24 08          	mov    0x8(%esp),%eax
f01073ce:	d3 e0                	shl    %cl,%eax
f01073d0:	89 44 24 04          	mov    %eax,0x4(%esp)
f01073d4:	8b 44 24 08          	mov    0x8(%esp),%eax
f01073d8:	89 f1                	mov    %esi,%ecx
f01073da:	d3 e8                	shr    %cl,%eax
f01073dc:	09 d0                	or     %edx,%eax
f01073de:	d3 eb                	shr    %cl,%ebx
f01073e0:	89 da                	mov    %ebx,%edx
f01073e2:	f7 f7                	div    %edi
f01073e4:	89 d3                	mov    %edx,%ebx
f01073e6:	f7 24 24             	mull   (%esp)
f01073e9:	89 c6                	mov    %eax,%esi
f01073eb:	89 d1                	mov    %edx,%ecx
f01073ed:	39 d3                	cmp    %edx,%ebx
f01073ef:	0f 82 87 00 00 00    	jb     f010747c <__umoddi3+0x134>
f01073f5:	0f 84 91 00 00 00    	je     f010748c <__umoddi3+0x144>
f01073fb:	8b 54 24 04          	mov    0x4(%esp),%edx
f01073ff:	29 f2                	sub    %esi,%edx
f0107401:	19 cb                	sbb    %ecx,%ebx
f0107403:	89 d8                	mov    %ebx,%eax
f0107405:	8a 4c 24 0c          	mov    0xc(%esp),%cl
f0107409:	d3 e0                	shl    %cl,%eax
f010740b:	89 e9                	mov    %ebp,%ecx
f010740d:	d3 ea                	shr    %cl,%edx
f010740f:	09 d0                	or     %edx,%eax
f0107411:	89 e9                	mov    %ebp,%ecx
f0107413:	d3 eb                	shr    %cl,%ebx
f0107415:	89 da                	mov    %ebx,%edx
f0107417:	83 c4 1c             	add    $0x1c,%esp
f010741a:	5b                   	pop    %ebx
f010741b:	5e                   	pop    %esi
f010741c:	5f                   	pop    %edi
f010741d:	5d                   	pop    %ebp
f010741e:	c3                   	ret    
f010741f:	90                   	nop
f0107420:	89 fd                	mov    %edi,%ebp
f0107422:	85 ff                	test   %edi,%edi
f0107424:	75 0b                	jne    f0107431 <__umoddi3+0xe9>
f0107426:	b8 01 00 00 00       	mov    $0x1,%eax
f010742b:	31 d2                	xor    %edx,%edx
f010742d:	f7 f7                	div    %edi
f010742f:	89 c5                	mov    %eax,%ebp
f0107431:	89 f0                	mov    %esi,%eax
f0107433:	31 d2                	xor    %edx,%edx
f0107435:	f7 f5                	div    %ebp
f0107437:	89 c8                	mov    %ecx,%eax
f0107439:	f7 f5                	div    %ebp
f010743b:	89 d0                	mov    %edx,%eax
f010743d:	e9 44 ff ff ff       	jmp    f0107386 <__umoddi3+0x3e>
f0107442:	66 90                	xchg   %ax,%ax
f0107444:	89 c8                	mov    %ecx,%eax
f0107446:	89 f2                	mov    %esi,%edx
f0107448:	83 c4 1c             	add    $0x1c,%esp
f010744b:	5b                   	pop    %ebx
f010744c:	5e                   	pop    %esi
f010744d:	5f                   	pop    %edi
f010744e:	5d                   	pop    %ebp
f010744f:	c3                   	ret    
f0107450:	3b 04 24             	cmp    (%esp),%eax
f0107453:	72 06                	jb     f010745b <__umoddi3+0x113>
f0107455:	3b 7c 24 04          	cmp    0x4(%esp),%edi
f0107459:	77 0f                	ja     f010746a <__umoddi3+0x122>
f010745b:	89 f2                	mov    %esi,%edx
f010745d:	29 f9                	sub    %edi,%ecx
f010745f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
f0107463:	89 14 24             	mov    %edx,(%esp)
f0107466:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f010746a:	8b 44 24 04          	mov    0x4(%esp),%eax
f010746e:	8b 14 24             	mov    (%esp),%edx
f0107471:	83 c4 1c             	add    $0x1c,%esp
f0107474:	5b                   	pop    %ebx
f0107475:	5e                   	pop    %esi
f0107476:	5f                   	pop    %edi
f0107477:	5d                   	pop    %ebp
f0107478:	c3                   	ret    
f0107479:	8d 76 00             	lea    0x0(%esi),%esi
f010747c:	2b 04 24             	sub    (%esp),%eax
f010747f:	19 fa                	sbb    %edi,%edx
f0107481:	89 d1                	mov    %edx,%ecx
f0107483:	89 c6                	mov    %eax,%esi
f0107485:	e9 71 ff ff ff       	jmp    f01073fb <__umoddi3+0xb3>
f010748a:	66 90                	xchg   %ax,%ax
f010748c:	39 44 24 04          	cmp    %eax,0x4(%esp)
f0107490:	72 ea                	jb     f010747c <__umoddi3+0x134>
f0107492:	89 d9                	mov    %ebx,%ecx
f0107494:	e9 62 ff ff ff       	jmp    f01073fb <__umoddi3+0xb3>
