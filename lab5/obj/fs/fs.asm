
obj/fs/fs:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 ad 1b 00 00       	call   801bde <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <ahci_port_rw>:
	ahci_port_wait(port, 1);
}

static int
ahci_port_rw(volatile struct ahci_port *port, uint64_t secno, void *buf, uint16_t nsecs, bool iswrite)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 3c             	sub    $0x3c,%esp
  80003c:	89 c6                	mov    %eax,%esi
  80003e:	89 55 c0             	mov    %edx,-0x40(%ebp)
  800041:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  800044:	8b 45 0c             	mov    0xc(%ebp),%eax
  800047:	89 45 bc             	mov    %eax,-0x44(%ebp)
  80004a:	8b 55 10             	mov    0x10(%ebp),%edx
	int portno = port - regs->ports;
  80004d:	89 f0                	mov    %esi,%eax
  80004f:	2b 05 00 b0 82 00    	sub    0x82b000,%eax
  800055:	8d 80 00 ff ff ff    	lea    -0x100(%eax),%eax
  80005b:	c1 f8 07             	sar    $0x7,%eax
  80005e:	89 45 b8             	mov    %eax,-0x48(%ebp)
	struct ahci_port_page *page = &port_pages[portno];
	volatile struct ahci_cmd_header *cmdh = &page->cmdh[0];
	struct sata_fis_reg_h2d fis = {
  800061:	8d 7d d4             	lea    -0x2c(%ebp),%edi
  800064:	b9 05 00 00 00       	mov    $0x5,%ecx
  800069:	b8 00 00 00 00       	mov    $0x0,%eax
  80006e:	f3 ab                	rep stos %eax,%es:(%edi)
  800070:	c6 45 d4 27          	movb   $0x27,-0x2c(%ebp)
  800074:	c6 45 d5 80          	movb   $0x80,-0x2b(%ebp)
  800078:	80 fa 01             	cmp    $0x1,%dl
  80007b:	19 c0                	sbb    %eax,%eax
  80007d:	83 e0 f0             	and    $0xfffffff0,%eax
  800080:	83 c0 35             	add    $0x35,%eax
  800083:	88 45 d6             	mov    %al,-0x2a(%ebp)
  800086:	8b 4d c0             	mov    -0x40(%ebp),%ecx
  800089:	8b 5d c4             	mov    -0x3c(%ebp),%ebx
  80008c:	88 4d d8             	mov    %cl,-0x28(%ebp)
  80008f:	0f ac d9 08          	shrd   $0x8,%ebx,%ecx
  800093:	88 4d d9             	mov    %cl,-0x27(%ebp)
  800096:	8b 4d c0             	mov    -0x40(%ebp),%ecx
  800099:	8b 5d c4             	mov    -0x3c(%ebp),%ebx
  80009c:	0f ac d9 10          	shrd   $0x10,%ebx,%ecx
  8000a0:	88 4d da             	mov    %cl,-0x26(%ebp)
  8000a3:	c6 45 db 40          	movb   $0x40,-0x25(%ebp)
  8000a7:	8b 4d c0             	mov    -0x40(%ebp),%ecx
  8000aa:	8b 5d c4             	mov    -0x3c(%ebp),%ebx
  8000ad:	0f ac d9 18          	shrd   $0x18,%ebx,%ecx
  8000b1:	88 4d dc             	mov    %cl,-0x24(%ebp)
  8000b4:	8b 5d c4             	mov    -0x3c(%ebp),%ebx
  8000b7:	89 d8                	mov    %ebx,%eax
  8000b9:	88 5d dd             	mov    %bl,-0x23(%ebp)
  8000bc:	c1 e8 08             	shr    $0x8,%eax
  8000bf:	88 45 de             	mov    %al,-0x22(%ebp)
  8000c2:	8b 7d bc             	mov    -0x44(%ebp),%edi
  8000c5:	89 f8                	mov    %edi,%eax
  8000c7:	88 45 e0             	mov    %al,-0x20(%ebp)
  8000ca:	89 f8                	mov    %edi,%eax
  8000cc:	66 c1 e8 08          	shr    $0x8,%ax
  8000d0:	88 45 e1             	mov    %al,-0x1f(%ebp)
  8000d3:	c6 45 e3 80          	movb   $0x80,-0x1d(%ebp)
		.lba5 = (secno >> 40) & 0xff,
		.device = ATA_DEV_LBA,
		.control = ATA_CTL_HOB,
	};

	assert(nsecs <= BLKSECTS);
  8000d7:	66 83 ff 08          	cmp    $0x8,%di
  8000db:	76 16                	jbe    8000f3 <ahci_port_rw+0xc0>
  8000dd:	68 40 39 80 00       	push   $0x803940
  8000e2:	68 52 39 80 00       	push   $0x803952
  8000e7:	6a 78                	push   $0x78
  8000e9:	68 67 39 80 00       	push   $0x803967
  8000ee:	e8 4c 1b 00 00       	call   801c3f <_panic>
	if (iswrite) {
  8000f3:	84 d2                	test   %dl,%dl
  8000f5:	74 26                	je     80011d <ahci_port_rw+0xea>
		cmdh->prdbc = nsecs * SECTSIZE;
  8000f7:	0f b7 55 bc          	movzwl -0x44(%ebp),%edx
  8000fb:	c1 e2 09             	shl    $0x9,%edx
  8000fe:	8b 45 b8             	mov    -0x48(%ebp),%eax
  800101:	c1 e0 0c             	shl    $0xc,%eax
  800104:	89 90 04 b4 80 00    	mov    %edx,0x80b404(%eax)
		cmdh->w = 1;
  80010a:	66 8b 90 00 b4 80 00 	mov    0x80b400(%eax),%dx
  800111:	83 ca 40             	or     $0x40,%edx
  800114:	66 89 90 00 b4 80 00 	mov    %dx,0x80b400(%eax)
  80011b:	eb 10                	jmp    80012d <ahci_port_rw+0xfa>
	} else {
		cmdh->prdbc = 0;
  80011d:	8b 45 b8             	mov    -0x48(%ebp),%eax
  800120:	c1 e0 0c             	shl    $0xc,%eax
  800123:	c7 80 04 b4 80 00 00 	movl   $0x0,0x80b404(%eax)
  80012a:	00 00 00 
}

static void
fill_prd(volatile struct ahci_port *port, void *buf, size_t len)
{
	int portno = port - regs->ports;
  80012d:	89 f0                	mov    %esi,%eax
  80012f:	2b 05 00 b0 82 00    	sub    0x82b000,%eax
  800135:	8d 98 00 ff ff ff    	lea    -0x100(%eax),%ebx
  80013b:	c1 fb 07             	sar    $0x7,%ebx
	struct ahci_port_page *page = &port_pages[portno];

	page->cmdt.prdt[0].dba = physaddr(buf);
  80013e:	83 ec 0c             	sub    $0xc,%esp
  800141:	ff 75 08             	pushl  0x8(%ebp)
  800144:	e8 5e 05 00 00       	call   8006a7 <physaddr>
  800149:	ba 00 00 00 00       	mov    $0x0,%edx
  80014e:	c1 e3 0c             	shl    $0xc,%ebx
  800151:	8d 8b 00 b0 80 00    	lea    0x80b000(%ebx),%ecx
  800157:	89 83 80 b8 80 00    	mov    %eax,0x80b880(%ebx)
  80015d:	89 93 84 b8 80 00    	mov    %edx,0x80b884(%ebx)
	page->cmdt.prdt[0].dbc = len - 1;
  800163:	0f b7 45 bc          	movzwl -0x44(%ebp),%eax
  800167:	c1 e0 09             	shl    $0x9,%eax
  80016a:	48                   	dec    %eax
  80016b:	25 ff ff 3f 00       	and    $0x3fffff,%eax
  800170:	89 c2                	mov    %eax,%edx
  800172:	8b 83 8c b8 80 00    	mov    0x80b88c(%ebx),%eax
  800178:	25 00 00 c0 ff       	and    $0xffc00000,%eax
  80017d:	09 d0                	or     %edx,%eax
  80017f:	89 83 8c b8 80 00    	mov    %eax,0x80b88c(%ebx)
	page->cmdh[0].prdtl = 1;
  800185:	66 c7 81 02 04 00 00 	movw   $0x1,0x402(%ecx)
  80018c:	01 00 
}

static void
fill_fis(volatile struct ahci_port *port, void *fis, size_t len)
{
	int portno = port - regs->ports;
  80018e:	89 f0                	mov    %esi,%eax
  800190:	2b 05 00 b0 82 00    	sub    0x82b000,%eax
  800196:	8d 98 00 ff ff ff    	lea    -0x100(%eax),%ebx
  80019c:	c1 fb 07             	sar    $0x7,%ebx
	struct ahci_port_page *page = &port_pages[portno];

	memcpy((void *)page->cmdt.cfis, fis, len);
  80019f:	83 c4 0c             	add    $0xc,%esp
  8001a2:	6a 14                	push   $0x14
  8001a4:	8d 45 d4             	lea    -0x2c(%ebp),%eax
  8001a7:	50                   	push   %eax
  8001a8:	c1 e3 0c             	shl    $0xc,%ebx
  8001ab:	8d 83 00 b8 80 00    	lea    0x80b800(%ebx),%eax
  8001b1:	50                   	push   %eax
  8001b2:	e8 9f 22 00 00       	call   802456 <memcpy>
	page->cmdh[0].cfl = len / sizeof(uint32_t);
  8001b7:	66 8b 83 00 b4 80 00 	mov    0x80b400(%ebx),%ax
  8001be:	83 e0 e0             	and    $0xffffffe0,%eax
  8001c1:	83 c8 05             	or     $0x5,%eax
  8001c4:	66 89 83 00 b4 80 00 	mov    %ax,0x80b400(%ebx)
		cmdh->prdbc = 0;
	}

	fill_prd(port, buf, nsecs * SECTSIZE);
	fill_fis(port, &fis, sizeof(fis));
	port->ci |= 1;
  8001cb:	8b 46 38             	mov    0x38(%esi),%eax
  8001ce:	83 c8 01             	or     $0x1,%eax
  8001d1:	89 46 38             	mov    %eax,0x38(%esi)
  8001d4:	83 c4 10             	add    $0x10,%esp
static int fs_portno;

static void
ahci_port_wait(volatile struct ahci_port *port, uint32_t ci)
{
	while ((port->tfd.sts & ATA_STAT_BSY) || (port->ci & ci))
  8001d7:	8a 46 20             	mov    0x20(%esi),%al
  8001da:	84 c0                	test   %al,%al
  8001dc:	78 f9                	js     8001d7 <ahci_port_rw+0x1a4>
  8001de:	8b 46 38             	mov    0x38(%esi),%eax
  8001e1:	a8 01                	test   $0x1,%al
  8001e3:	75 f2                	jne    8001d7 <ahci_port_rw+0x1a4>
	fill_prd(port, buf, nsecs * SECTSIZE);
	fill_fis(port, &fis, sizeof(fis));
	port->ci |= 1;
	ahci_port_wait(port, 1);
	return 0;
}
  8001e5:	b8 00 00 00 00       	mov    $0x0,%eax
  8001ea:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001ed:	5b                   	pop    %ebx
  8001ee:	5e                   	pop    %esi
  8001ef:	5f                   	pop    %edi
  8001f0:	5d                   	pop    %ebp
  8001f1:	c3                   	ret    

008001f2 <ahci_init>:

void
ahci_init(void)
{
  8001f2:	55                   	push   %ebp
  8001f3:	89 e5                	mov    %esp,%ebp
  8001f5:	57                   	push   %edi
  8001f6:	56                   	push   %esi
  8001f7:	53                   	push   %ebx
  8001f8:	81 ec 48 02 00 00    	sub    $0x248,%esp
	static_assert(sizeof(struct ahci_recv_fis) == 0x100);
	static_assert(sizeof(struct ahci_cmd_header) == 0x20);
	static_assert(sizeof(struct ahci_port_page) <= PGSIZE);

	// AHCI registers
	if (!va_is_mapped(addr))
  8001fe:	68 00 00 00 e0       	push   $0xe0000000
  800203:	e8 54 04 00 00       	call   80065c <va_is_mapped>
  800208:	83 c4 10             	add    $0x10,%esp
  80020b:	84 c0                	test   %al,%al
  80020d:	75 17                	jne    800226 <ahci_init+0x34>
		panic("AHCI not mapped");
  80020f:	83 ec 04             	sub    $0x4,%esp
  800212:	68 71 39 80 00       	push   $0x803971
  800217:	68 96 00 00 00       	push   $0x96
  80021c:	68 67 39 80 00       	push   $0x803967
  800221:	e8 19 1a 00 00       	call   801c3f <_panic>
	regs = addr;
  800226:	c7 05 00 b0 82 00 00 	movl   $0xe0000000,0x82b000
  80022d:	00 00 e0 

	// Enable AHCI
	regs->ghc |= AHCI_GHC_AE;
  800230:	a1 04 00 00 e0       	mov    0xe0000004,%eax
  800235:	0d 00 00 00 80       	or     $0x80000000,%eax
  80023a:	a3 04 00 00 e0       	mov    %eax,0xe0000004

	// Initialize each port
	for (i = 0; i < 32; i++) {
  80023f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800244:	89 d9                	mov    %ebx,%ecx
		volatile struct ahci_port *port;
		struct ata_identify_device dev;

		if (!(regs->pi & BIT(i)))
  800246:	a1 0c 00 00 e0       	mov    0xe000000c,%eax
  80024b:	d3 e8                	shr    %cl,%eax
  80024d:	a8 01                	test   $0x1,%al
  80024f:	0f 84 1c 02 00 00    	je     800471 <ahci_init+0x27f>
ahci_port_reset(volatile struct ahci_port *port)
{
	int portno = port - regs->ports;
	struct ahci_port_page *page = &port_pages[port - regs->ports];

	if (!port->ssts)
  800255:	89 c8                	mov    %ecx,%eax
  800257:	c1 e0 07             	shl    $0x7,%eax
  80025a:	8b 80 28 01 00 e0    	mov    -0x1ffffed8(%eax),%eax
  800260:	85 c0                	test   %eax,%eax
  800262:	0f 84 09 02 00 00    	je     800471 <ahci_init+0x27f>
  800268:	89 cb                	mov    %ecx,%ebx
		return -1;

	// Clear ST and then FRE
	port->cmd &= ~AHCI_PORT_CMD_ST;
  80026a:	89 c8                	mov    %ecx,%eax
  80026c:	c1 e0 07             	shl    $0x7,%eax
  80026f:	8b 90 18 01 00 e0    	mov    -0x1ffffee8(%eax),%edx
  800275:	83 e2 fe             	and    $0xfffffffe,%edx
  800278:	89 90 18 01 00 e0    	mov    %edx,-0x1ffffee8(%eax)
	port->cmd &= ~AHCI_PORT_CMD_FRE;
  80027e:	8b 90 18 01 00 e0    	mov    -0x1ffffee8(%eax),%edx
  800284:	83 e2 ef             	and    $0xffffffef,%edx
  800287:	89 90 18 01 00 e0    	mov    %edx,-0x1ffffee8(%eax)
	while (port->cmd & (AHCI_PORT_CMD_CR | AHCI_PORT_CMD_FR))
  80028d:	89 c2                	mov    %eax,%edx
  80028f:	8b 82 18 01 00 e0    	mov    -0x1ffffee8(%edx),%eax
  800295:	f6 c4 c0             	test   $0xc0,%ah
  800298:	75 f5                	jne    80028f <ahci_init+0x9d>
		volatile struct ahci_port *port;
		struct ata_identify_device dev;

		if (!(regs->pi & BIT(i)))
			continue;
		port = &regs->ports[i];
  80029a:	8d 43 02             	lea    0x2(%ebx),%eax
  80029d:	c1 e0 07             	shl    $0x7,%eax
  8002a0:	8d b0 00 00 00 e0    	lea    -0x20000000(%eax),%esi
  8002a6:	89 b5 c4 fd ff ff    	mov    %esi,-0x23c(%ebp)
	port->cmd &= ~AHCI_PORT_CMD_ST;
	port->cmd &= ~AHCI_PORT_CMD_FRE;
	while (port->cmd & (AHCI_PORT_CMD_CR | AHCI_PORT_CMD_FR))
		;

	page->cmdh[0].ctba = physaddr((void *)&page->cmdt);
  8002ac:	83 ec 0c             	sub    $0xc,%esp
  8002af:	c1 e0 05             	shl    $0x5,%eax
  8002b2:	8d b8 00 90 80 00    	lea    0x809000(%eax),%edi
  8002b8:	05 00 98 80 00       	add    $0x809800,%eax
  8002bd:	50                   	push   %eax
  8002be:	e8 e4 03 00 00       	call   8006a7 <physaddr>
  8002c3:	ba 00 00 00 00       	mov    $0x0,%edx
  8002c8:	89 87 08 04 00 00    	mov    %eax,0x408(%edi)
  8002ce:	89 97 0c 04 00 00    	mov    %edx,0x40c(%edi)
	port->clb = physaddr((void *)&page->cmdh);
  8002d4:	8d 87 00 04 00 00    	lea    0x400(%edi),%eax
  8002da:	89 04 24             	mov    %eax,(%esp)
  8002dd:	e8 c5 03 00 00       	call   8006a7 <physaddr>
  8002e2:	ba 00 00 00 00       	mov    $0x0,%edx
  8002e7:	89 de                	mov    %ebx,%esi
  8002e9:	c1 e6 07             	shl    $0x7,%esi
  8002ec:	8d 8e 00 00 00 e0    	lea    -0x20000000(%esi),%ecx
  8002f2:	89 8d c0 fd ff ff    	mov    %ecx,-0x240(%ebp)
  8002f8:	89 86 00 01 00 e0    	mov    %eax,-0x1fffff00(%esi)
  8002fe:	89 96 04 01 00 e0    	mov    %edx,-0x1ffffefc(%esi)
	port->fb = physaddr((void *)&page->rfis);
  800304:	89 3c 24             	mov    %edi,(%esp)
  800307:	e8 9b 03 00 00       	call   8006a7 <physaddr>
  80030c:	ba 00 00 00 00       	mov    $0x0,%edx
  800311:	8b 8d c0 fd ff ff    	mov    -0x240(%ebp),%ecx
  800317:	89 81 08 01 00 00    	mov    %eax,0x108(%ecx)
  80031d:	89 91 0c 01 00 00    	mov    %edx,0x10c(%ecx)

	port->serr = ~0U;
  800323:	c7 86 30 01 00 e0 ff 	movl   $0xffffffff,-0x1ffffed0(%esi)
  80032a:	ff ff ff 
	port->serr = 0;
  80032d:	c7 86 30 01 00 e0 00 	movl   $0x0,-0x1ffffed0(%esi)
  800334:	00 00 00 

	// Set FRE and then ST
	port->cmd |= AHCI_PORT_CMD_FRE;
  800337:	8b 86 18 01 00 e0    	mov    -0x1ffffee8(%esi),%eax
  80033d:	83 c8 10             	or     $0x10,%eax
  800340:	89 86 18 01 00 e0    	mov    %eax,-0x1ffffee8(%esi)
	port->cmd |= AHCI_PORT_CMD_ST;
  800346:	8b 86 18 01 00 e0    	mov    -0x1ffffee8(%esi),%eax
  80034c:	83 c8 01             	or     $0x1,%eax
  80034f:	89 86 18 01 00 e0    	mov    %eax,-0x1ffffee8(%esi)
}

static void
ahci_port_identify(volatile struct ahci_port *port, void *buf)
{
	struct sata_fis_reg_h2d fis = {
  800355:	8d bd d4 fd ff ff    	lea    -0x22c(%ebp),%edi
  80035b:	b9 05 00 00 00       	mov    $0x5,%ecx
  800360:	b8 00 00 00 00       	mov    $0x0,%eax
  800365:	f3 ab                	rep stos %eax,%es:(%edi)
  800367:	c6 85 d4 fd ff ff 27 	movb   $0x27,-0x22c(%ebp)
  80036e:	c6 85 d5 fd ff ff 80 	movb   $0x80,-0x22b(%ebp)
  800375:	c6 85 d6 fd ff ff ec 	movb   $0xec,-0x22a(%ebp)
  80037c:	c6 85 e0 fd ff ff 01 	movb   $0x1,-0x220(%ebp)
}

static void
fill_prd(volatile struct ahci_port *port, void *buf, size_t len)
{
	int portno = port - regs->ports;
  800383:	8b 85 c4 fd ff ff    	mov    -0x23c(%ebp),%eax
  800389:	2b 05 00 b0 82 00    	sub    0x82b000,%eax
  80038f:	8d b8 00 ff ff ff    	lea    -0x100(%eax),%edi
  800395:	c1 ff 07             	sar    $0x7,%edi
	struct ahci_port_page *page = &port_pages[portno];

	page->cmdt.prdt[0].dba = physaddr(buf);
  800398:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  80039e:	89 04 24             	mov    %eax,(%esp)
  8003a1:	e8 01 03 00 00       	call   8006a7 <physaddr>
  8003a6:	ba 00 00 00 00       	mov    $0x0,%edx
  8003ab:	c1 e7 0c             	shl    $0xc,%edi
  8003ae:	89 87 80 b8 80 00    	mov    %eax,0x80b880(%edi)
  8003b4:	89 97 84 b8 80 00    	mov    %edx,0x80b884(%edi)
	page->cmdt.prdt[0].dbc = len - 1;
  8003ba:	8b 97 8c b8 80 00    	mov    0x80b88c(%edi),%edx
  8003c0:	81 e2 00 00 c0 ff    	and    $0xffc00000,%edx
  8003c6:	81 ca ff 01 00 00    	or     $0x1ff,%edx
  8003cc:	89 97 8c b8 80 00    	mov    %edx,0x80b88c(%edi)
	page->cmdh[0].prdtl = 1;
  8003d2:	66 c7 87 02 b4 80 00 	movw   $0x1,0x80b402(%edi)
  8003d9:	01 00 
}

static void
fill_fis(volatile struct ahci_port *port, void *fis, size_t len)
{
	int portno = port - regs->ports;
  8003db:	8b 85 c4 fd ff ff    	mov    -0x23c(%ebp),%eax
  8003e1:	2b 05 00 b0 82 00    	sub    0x82b000,%eax
  8003e7:	8d b8 00 ff ff ff    	lea    -0x100(%eax),%edi
  8003ed:	c1 ff 07             	sar    $0x7,%edi
	struct ahci_port_page *page = &port_pages[portno];

	memcpy((void *)page->cmdt.cfis, fis, len);
  8003f0:	83 c4 0c             	add    $0xc,%esp
  8003f3:	6a 14                	push   $0x14
  8003f5:	8d 85 d4 fd ff ff    	lea    -0x22c(%ebp),%eax
  8003fb:	50                   	push   %eax
  8003fc:	c1 e7 0c             	shl    $0xc,%edi
  8003ff:	8d 87 00 b8 80 00    	lea    0x80b800(%edi),%eax
  800405:	50                   	push   %eax
  800406:	e8 4b 20 00 00       	call   802456 <memcpy>
	page->cmdh[0].cfl = len / sizeof(uint32_t);
  80040b:	66 8b 87 00 b4 80 00 	mov    0x80b400(%edi),%ax
  800412:	83 e0 e0             	and    $0xffffffe0,%eax
  800415:	83 c8 05             	or     $0x5,%eax
  800418:	66 89 87 00 b4 80 00 	mov    %ax,0x80b400(%edi)
		.count0 = 1,
	};

	fill_prd(port, buf, 512);
	fill_fis(port, &fis, sizeof(fis));
	port->ci |= 1;
  80041f:	8b 86 38 01 00 e0    	mov    -0x1ffffec8(%esi),%eax
  800425:	83 c8 01             	or     $0x1,%eax
  800428:	89 86 38 01 00 e0    	mov    %eax,-0x1ffffec8(%esi)
  80042e:	83 c4 10             	add    $0x10,%esp
static int fs_portno;

static void
ahci_port_wait(volatile struct ahci_port *port, uint32_t ci)
{
	while ((port->tfd.sts & ATA_STAT_BSY) || (port->ci & ci))
  800431:	89 f2                	mov    %esi,%edx
  800433:	8a 82 20 01 00 e0    	mov    -0x1ffffee0(%edx),%al
  800439:	84 c0                	test   %al,%al
  80043b:	78 f6                	js     800433 <ahci_init+0x241>
  80043d:	8b 82 38 01 00 e0    	mov    -0x1ffffec8(%edx),%eax
  800443:	a8 01                	test   $0x1,%al
  800445:	75 ec                	jne    800433 <ahci_init+0x241>
			continue;
		port = &regs->ports[i];
		if (ahci_port_reset(port))
			continue;
		ahci_port_identify(port, &dev);
		cprintf("AHCI.%d: %llu bytes\n",
  800447:	8b 85 b0 fe ff ff    	mov    -0x150(%ebp),%eax
  80044d:	8b 95 b4 fe ff ff    	mov    -0x14c(%ebp),%edx
  800453:	0f a4 c2 09          	shld   $0x9,%eax,%edx
  800457:	c1 e0 09             	shl    $0x9,%eax
  80045a:	52                   	push   %edx
  80045b:	50                   	push   %eax
  80045c:	53                   	push   %ebx
  80045d:	68 81 39 80 00       	push   $0x803981
  800462:	e8 b0 18 00 00       	call   801d17 <cprintf>
			i, *(uint64_t *)dev.lba48_sectors * SECTSIZE);
		break;
	}
	// Use the first port found for FS
	if (i == 32)
  800467:	83 c4 10             	add    $0x10,%esp
  80046a:	83 fb 20             	cmp    $0x20,%ebx
  80046d:	75 23                	jne    800492 <ahci_init+0x2a0>
  80046f:	eb 0a                	jmp    80047b <ahci_init+0x289>

	// Enable AHCI
	regs->ghc |= AHCI_GHC_AE;

	// Initialize each port
	for (i = 0; i < 32; i++) {
  800471:	41                   	inc    %ecx
  800472:	83 f9 20             	cmp    $0x20,%ecx
  800475:	0f 85 cb fd ff ff    	jne    800246 <ahci_init+0x54>
			i, *(uint64_t *)dev.lba48_sectors * SECTSIZE);
		break;
	}
	// Use the first port found for FS
	if (i == 32)
		panic("Disk not found!");
  80047b:	83 ec 04             	sub    $0x4,%esp
  80047e:	68 96 39 80 00       	push   $0x803996
  800483:	68 ad 00 00 00       	push   $0xad
  800488:	68 67 39 80 00       	push   $0x803967
  80048d:	e8 ad 17 00 00       	call   801c3f <_panic>
	fs_portno = i;
  800492:	89 1d 00 a0 80 00    	mov    %ebx,0x80a000
}
  800498:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80049b:	5b                   	pop    %ebx
  80049c:	5e                   	pop    %esi
  80049d:	5f                   	pop    %edi
  80049e:	5d                   	pop    %ebp
  80049f:	c3                   	ret    

008004a0 <ahci_read>:

int
ahci_read(uint64_t secno, void *buf, uint16_t nsecs)
{
  8004a0:	55                   	push   %ebp
  8004a1:	89 e5                	mov    %esp,%ebp
  8004a3:	83 ec 0c             	sub    $0xc,%esp
	return ahci_port_rw(&regs->ports[fs_portno], secno, buf, nsecs, false);
  8004a6:	a1 00 a0 80 00       	mov    0x80a000,%eax
  8004ab:	83 c0 02             	add    $0x2,%eax
  8004ae:	c1 e0 07             	shl    $0x7,%eax
  8004b1:	03 05 00 b0 82 00    	add    0x82b000,%eax
  8004b7:	6a 00                	push   $0x0
  8004b9:	0f b7 55 14          	movzwl 0x14(%ebp),%edx
  8004bd:	52                   	push   %edx
  8004be:	ff 75 10             	pushl  0x10(%ebp)
  8004c1:	8b 55 08             	mov    0x8(%ebp),%edx
  8004c4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8004c7:	e8 67 fb ff ff       	call   800033 <ahci_port_rw>
}
  8004cc:	c9                   	leave  
  8004cd:	c3                   	ret    

008004ce <ahci_write>:

int
ahci_write(uint64_t secno, void *buf, uint16_t nsecs)
{
  8004ce:	55                   	push   %ebp
  8004cf:	89 e5                	mov    %esp,%ebp
  8004d1:	83 ec 0c             	sub    $0xc,%esp
	return ahci_port_rw(&regs->ports[fs_portno], secno, buf, nsecs, true);
  8004d4:	a1 00 a0 80 00       	mov    0x80a000,%eax
  8004d9:	83 c0 02             	add    $0x2,%eax
  8004dc:	c1 e0 07             	shl    $0x7,%eax
  8004df:	03 05 00 b0 82 00    	add    0x82b000,%eax
  8004e5:	6a 01                	push   $0x1
  8004e7:	0f b7 55 14          	movzwl 0x14(%ebp),%edx
  8004eb:	52                   	push   %edx
  8004ec:	ff 75 10             	pushl  0x10(%ebp)
  8004ef:	8b 55 08             	mov    0x8(%ebp),%edx
  8004f2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8004f5:	e8 39 fb ff ff       	call   800033 <ahci_port_rw>
}
  8004fa:	c9                   	leave  
  8004fb:	c3                   	ret    

008004fc <bc_pgfault>:

// Fault any disk block that is read in to memory by
// loading it from disk.
static void
bc_pgfault(struct UTrapframe *utf)
{
  8004fc:	55                   	push   %ebp
  8004fd:	89 e5                	mov    %esp,%ebp
  8004ff:	57                   	push   %edi
  800500:	56                   	push   %esi
  800501:	53                   	push   %ebx
  800502:	83 ec 0c             	sub    $0xc,%esp
  800505:	8b 55 08             	mov    0x8(%ebp),%edx
	void *addr = (void *) utf->utf_fault_va;
  800508:	8b 1a                	mov    (%edx),%ebx
	uint32_t blockno = ((uint32_t)addr - DISKMAP) / BLKSIZE;
  80050a:	8d 83 00 00 00 f0    	lea    -0x10000000(%ebx),%eax
  800510:	89 c6                	mov    %eax,%esi
  800512:	c1 ee 0c             	shr    $0xc,%esi
	int r;

	// Check that the fault was within the block cache region
	if (addr < (void*)DISKMAP || addr >= (void*)(DISKMAP + DISKSIZE))
  800515:	3d ff ff ff bf       	cmp    $0xbfffffff,%eax
  80051a:	76 1b                	jbe    800537 <bc_pgfault+0x3b>
		panic("page fault in FS: eip %08x, va %08x, err %04x",
  80051c:	83 ec 08             	sub    $0x8,%esp
  80051f:	ff 72 04             	pushl  0x4(%edx)
  800522:	53                   	push   %ebx
  800523:	ff 72 28             	pushl  0x28(%edx)
  800526:	68 a8 39 80 00       	push   $0x8039a8
  80052b:	6a 2f                	push   $0x2f
  80052d:	68 64 3a 80 00       	push   $0x803a64
  800532:	e8 08 17 00 00       	call   801c3f <_panic>
		      utf->utf_eip, addr, utf->utf_err);

	// Sanity check the block number.
	if (super && blockno >= super->s_nblocks)
  800537:	a1 0c b0 82 00       	mov    0x82b00c,%eax
  80053c:	85 c0                	test   %eax,%eax
  80053e:	74 17                	je     800557 <bc_pgfault+0x5b>
  800540:	3b 70 04             	cmp    0x4(%eax),%esi
  800543:	72 12                	jb     800557 <bc_pgfault+0x5b>
		panic("reading non-existent block %08x\n", blockno);
  800545:	56                   	push   %esi
  800546:	68 d8 39 80 00       	push   $0x8039d8
  80054b:	6a 33                	push   $0x33
  80054d:	68 64 3a 80 00       	push   $0x803a64
  800552:	e8 e8 16 00 00       	call   801c3f <_panic>
	// of the block from the disk into that page.
	// Hint: first round addr to page boundary. fs/fs.h has code
	// to read the disk (see also fs/ahci.c).
	//
	// LAB 5: you code here:
    void *pgva = ROUNDDOWN(addr, BLKSIZE);
  800557:	89 df                	mov    %ebx,%edi
  800559:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
	if (sys_page_alloc(0, pgva, PTE_U | PTE_W | PTE_P))
  80055f:	83 ec 04             	sub    $0x4,%esp
  800562:	6a 07                	push   $0x7
  800564:	57                   	push   %edi
  800565:	6a 00                	push   $0x0
  800567:	e8 f3 20 00 00       	call   80265f <sys_page_alloc>
  80056c:	83 c4 10             	add    $0x10,%esp
  80056f:	85 c0                	test   %eax,%eax
  800571:	74 14                	je     800587 <bc_pgfault+0x8b>
		panic("bc_pgfault: no phys mem");
  800573:	83 ec 04             	sub    $0x4,%esp
  800576:	68 6c 3a 80 00       	push   $0x803a6c
  80057b:	6a 3d                	push   $0x3d
  80057d:	68 64 3a 80 00       	push   $0x803a64
  800582:	e8 b8 16 00 00       	call   801c3f <_panic>

	//int	ahci_read(uint64_t secno, void *buf, uint16_t nsecs);
    uint32_t secno = blockno * BLKSECTS;
    if (ahci_read(secno, pgva, BLKSECTS)) 
  800587:	6a 08                	push   $0x8
  800589:	57                   	push   %edi
  80058a:	8d 04 f5 00 00 00 00 	lea    0x0(,%esi,8),%eax
  800591:	ba 00 00 00 00       	mov    $0x0,%edx
  800596:	52                   	push   %edx
  800597:	50                   	push   %eax
  800598:	e8 03 ff ff ff       	call   8004a0 <ahci_read>
  80059d:	83 c4 10             	add    $0x10,%esp
  8005a0:	85 c0                	test   %eax,%eax
  8005a2:	74 14                	je     8005b8 <bc_pgfault+0xbc>
        panic("bc_pgfault: ide read error");
  8005a4:	83 ec 04             	sub    $0x4,%esp
  8005a7:	68 84 3a 80 00       	push   $0x803a84
  8005ac:	6a 42                	push   $0x42
  8005ae:	68 64 3a 80 00       	push   $0x803a64
  8005b3:	e8 87 16 00 00       	call   801c3f <_panic>

	// Clear the dirty bit for the disk block page since we just read the
	// block from disk
	if ((r = sys_page_map(0, addr, 0, addr, uvpt[PGNUM(addr)] & PTE_SYSCALL)) < 0)
  8005b8:	89 d8                	mov    %ebx,%eax
  8005ba:	c1 e8 0c             	shr    $0xc,%eax
  8005bd:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8005c4:	83 ec 0c             	sub    $0xc,%esp
  8005c7:	25 07 0e 00 00       	and    $0xe07,%eax
  8005cc:	50                   	push   %eax
  8005cd:	53                   	push   %ebx
  8005ce:	6a 00                	push   $0x0
  8005d0:	53                   	push   %ebx
  8005d1:	6a 00                	push   $0x0
  8005d3:	e8 ca 20 00 00       	call   8026a2 <sys_page_map>
  8005d8:	83 c4 20             	add    $0x20,%esp
  8005db:	85 c0                	test   %eax,%eax
  8005dd:	79 12                	jns    8005f1 <bc_pgfault+0xf5>
		panic("in bc_pgfault, sys_page_map: %e", r);
  8005df:	50                   	push   %eax
  8005e0:	68 fc 39 80 00       	push   $0x8039fc
  8005e5:	6a 47                	push   $0x47
  8005e7:	68 64 3a 80 00       	push   $0x803a64
  8005ec:	e8 4e 16 00 00       	call   801c3f <_panic>

	// Check that the block we read was allocated. (exercise for
	// the reader: why do we do this *after* reading the block
	// in?)
	if (bitmap && block_is_free(blockno))
  8005f1:	83 3d 08 b0 82 00 00 	cmpl   $0x0,0x82b008
  8005f8:	74 22                	je     80061c <bc_pgfault+0x120>
  8005fa:	83 ec 0c             	sub    $0xc,%esp
  8005fd:	56                   	push   %esi
  8005fe:	e8 ba 03 00 00       	call   8009bd <block_is_free>
  800603:	83 c4 10             	add    $0x10,%esp
  800606:	84 c0                	test   %al,%al
  800608:	74 12                	je     80061c <bc_pgfault+0x120>
		panic("reading free block %08x\n", blockno);
  80060a:	56                   	push   %esi
  80060b:	68 9f 3a 80 00       	push   $0x803a9f
  800610:	6a 4d                	push   $0x4d
  800612:	68 64 3a 80 00       	push   $0x803a64
  800617:	e8 23 16 00 00       	call   801c3f <_panic>
}
  80061c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80061f:	5b                   	pop    %ebx
  800620:	5e                   	pop    %esi
  800621:	5f                   	pop    %edi
  800622:	5d                   	pop    %ebp
  800623:	c3                   	ret    

00800624 <diskaddr>:
#include "fs.h"

// Return the virtual address of this disk block.
void*
diskaddr(uint32_t blockno)
{
  800624:	55                   	push   %ebp
  800625:	89 e5                	mov    %esp,%ebp
  800627:	83 ec 08             	sub    $0x8,%esp
  80062a:	8b 45 08             	mov    0x8(%ebp),%eax
	if (blockno == 0 || (super && blockno >= super->s_nblocks))
  80062d:	85 c0                	test   %eax,%eax
  80062f:	74 0f                	je     800640 <diskaddr+0x1c>
  800631:	8b 15 0c b0 82 00    	mov    0x82b00c,%edx
  800637:	85 d2                	test   %edx,%edx
  800639:	74 17                	je     800652 <diskaddr+0x2e>
  80063b:	3b 42 04             	cmp    0x4(%edx),%eax
  80063e:	72 12                	jb     800652 <diskaddr+0x2e>
		panic("bad block number %08x in diskaddr", blockno);
  800640:	50                   	push   %eax
  800641:	68 1c 3a 80 00       	push   $0x803a1c
  800646:	6a 09                	push   $0x9
  800648:	68 64 3a 80 00       	push   $0x803a64
  80064d:	e8 ed 15 00 00       	call   801c3f <_panic>
	return (char*) (DISKMAP + blockno * BLKSIZE);
  800652:	05 00 00 01 00       	add    $0x10000,%eax
  800657:	c1 e0 0c             	shl    $0xc,%eax
}
  80065a:	c9                   	leave  
  80065b:	c3                   	ret    

0080065c <va_is_mapped>:

// Is this virtual address mapped?
bool
va_is_mapped(void *va)
{
  80065c:	55                   	push   %ebp
  80065d:	89 e5                	mov    %esp,%ebp
	return (uvpd[PDX(va)] & PTE_U) && (uvpt[PGNUM(va)] & PTE_U);
  80065f:	8b 45 08             	mov    0x8(%ebp),%eax
  800662:	c1 e8 16             	shr    $0x16,%eax
  800665:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  80066c:	a8 04                	test   $0x4,%al
  80066e:	74 15                	je     800685 <va_is_mapped+0x29>
  800670:	8b 45 08             	mov    0x8(%ebp),%eax
  800673:	c1 e8 0c             	shr    $0xc,%eax
  800676:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80067d:	c1 e8 02             	shr    $0x2,%eax
  800680:	83 e0 01             	and    $0x1,%eax
  800683:	eb 05                	jmp    80068a <va_is_mapped+0x2e>
  800685:	b8 00 00 00 00       	mov    $0x0,%eax
  80068a:	83 e0 01             	and    $0x1,%eax
}
  80068d:	5d                   	pop    %ebp
  80068e:	c3                   	ret    

0080068f <va_is_dirty>:

// Is this virtual address dirty?
bool
va_is_dirty(void *va)
{
  80068f:	55                   	push   %ebp
  800690:	89 e5                	mov    %esp,%ebp
	return (uvpt[PGNUM(va)] & PTE_D) != 0;
  800692:	8b 45 08             	mov    0x8(%ebp),%eax
  800695:	c1 e8 0c             	shr    $0xc,%eax
  800698:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80069f:	c1 e8 06             	shr    $0x6,%eax
  8006a2:	83 e0 01             	and    $0x1,%eax
}
  8006a5:	5d                   	pop    %ebp
  8006a6:	c3                   	ret    

008006a7 <physaddr>:

// Return the physical address of this virtual address
physaddr_t
physaddr(void *va)
{
  8006a7:	55                   	push   %ebp
  8006a8:	89 e5                	mov    %esp,%ebp
  8006aa:	53                   	push   %ebx
  8006ab:	83 ec 04             	sub    $0x4,%esp
  8006ae:	8b 5d 08             	mov    0x8(%ebp),%ebx
	assert(va_is_mapped(va));
  8006b1:	53                   	push   %ebx
  8006b2:	e8 a5 ff ff ff       	call   80065c <va_is_mapped>
  8006b7:	83 c4 04             	add    $0x4,%esp
  8006ba:	84 c0                	test   %al,%al
  8006bc:	75 16                	jne    8006d4 <physaddr+0x2d>
  8006be:	68 b8 3a 80 00       	push   $0x803ab8
  8006c3:	68 52 39 80 00       	push   $0x803952
  8006c8:	6a 1f                	push   $0x1f
  8006ca:	68 64 3a 80 00       	push   $0x803a64
  8006cf:	e8 6b 15 00 00       	call   801c3f <_panic>
	return PTE_ADDR(uvpt[PGNUM(va)]) + PGOFF(va);
  8006d4:	89 d8                	mov    %ebx,%eax
  8006d6:	c1 e8 0c             	shr    $0xc,%eax
  8006d9:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8006e0:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  8006e5:	81 e3 ff 0f 00 00    	and    $0xfff,%ebx
  8006eb:	01 d8                	add    %ebx,%eax
}
  8006ed:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006f0:	c9                   	leave  
  8006f1:	c3                   	ret    

008006f2 <flush_block>:
// Hint: Use va_is_mapped, va_is_dirty, and ahci_write.
// Hint: Use the PTE_SYSCALL constant when calling sys_page_map.
// Hint: Don't forget to round addr down.
void
flush_block(void *addr)
{
  8006f2:	55                   	push   %ebp
  8006f3:	89 e5                	mov    %esp,%ebp
  8006f5:	57                   	push   %edi
  8006f6:	56                   	push   %esi
  8006f7:	53                   	push   %ebx
  8006f8:	83 ec 0c             	sub    $0xc,%esp
  8006fb:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint32_t blockno = ((uint32_t)addr - DISKMAP) / BLKSIZE;

	if (addr < (void*)DISKMAP || addr >= (void*)(DISKMAP + DISKSIZE))
  8006fe:	8d 87 00 00 00 f0    	lea    -0x10000000(%edi),%eax
  800704:	3d ff ff ff bf       	cmp    $0xbfffffff,%eax
  800709:	76 12                	jbe    80071d <flush_block+0x2b>
		panic("flush_block of bad va %08x", addr);
  80070b:	57                   	push   %edi
  80070c:	68 c9 3a 80 00       	push   $0x803ac9
  800711:	6a 5d                	push   $0x5d
  800713:	68 64 3a 80 00       	push   $0x803a64
  800718:	e8 22 15 00 00       	call   801c3f <_panic>

	// LAB 5: Your code here.
	void *pgva = ROUNDDOWN(addr, BLKSIZE);
  80071d:	89 fb                	mov    %edi,%ebx
  80071f:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    pte_t pte = uvpt[PGNUM(pgva)];
  800725:	89 d8                	mov    %ebx,%eax
  800727:	c1 e8 0c             	shr    $0xc,%eax
  80072a:	8b 34 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%esi

    if (!va_is_mapped(pgva) || !va_is_dirty(pgva))
  800731:	83 ec 0c             	sub    $0xc,%esp
  800734:	53                   	push   %ebx
  800735:	e8 22 ff ff ff       	call   80065c <va_is_mapped>
  80073a:	83 c4 10             	add    $0x10,%esp
  80073d:	84 c0                	test   %al,%al
  80073f:	74 76                	je     8007b7 <flush_block+0xc5>
  800741:	83 ec 0c             	sub    $0xc,%esp
  800744:	53                   	push   %ebx
  800745:	e8 45 ff ff ff       	call   80068f <va_is_dirty>
  80074a:	83 c4 10             	add    $0x10,%esp
  80074d:	84 c0                	test   %al,%al
  80074f:	74 66                	je     8007b7 <flush_block+0xc5>
        return;

    uint32_t secno = blockno * BLKSECTS;
    if (ahci_write(secno, pgva, BLKSECTS))
  800751:	6a 08                	push   $0x8
  800753:	53                   	push   %ebx
  800754:	8d 87 00 00 00 f0    	lea    -0x10000000(%edi),%eax
  80075a:	c1 e8 0c             	shr    $0xc,%eax
  80075d:	c1 e0 03             	shl    $0x3,%eax
  800760:	ba 00 00 00 00       	mov    $0x0,%edx
  800765:	52                   	push   %edx
  800766:	50                   	push   %eax
  800767:	e8 62 fd ff ff       	call   8004ce <ahci_write>
  80076c:	83 c4 10             	add    $0x10,%esp
  80076f:	85 c0                	test   %eax,%eax
  800771:	74 14                	je     800787 <flush_block+0x95>
        panic("flush_block: ide write error");
  800773:	83 ec 04             	sub    $0x4,%esp
  800776:	68 e4 3a 80 00       	push   $0x803ae4
  80077b:	6a 68                	push   $0x68
  80077d:	68 64 3a 80 00       	push   $0x803a64
  800782:	e8 b8 14 00 00       	call   801c3f <_panic>
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    //clear the PTE_D bit using sys_page_map.
    if (sys_page_map(0, pgva, 0, pgva, pte & PTE_SYSCALL))
  800787:	83 ec 0c             	sub    $0xc,%esp
  80078a:	81 e6 07 0e 00 00    	and    $0xe07,%esi
  800790:	56                   	push   %esi
  800791:	53                   	push   %ebx
  800792:	6a 00                	push   $0x0
  800794:	53                   	push   %ebx
  800795:	6a 00                	push   $0x0
  800797:	e8 06 1f 00 00       	call   8026a2 <sys_page_map>
  80079c:	83 c4 20             	add    $0x20,%esp
  80079f:	85 c0                	test   %eax,%eax
  8007a1:	74 14                	je     8007b7 <flush_block+0xc5>
        panic("flush_block: map error");
  8007a3:	83 ec 04             	sub    $0x4,%esp
  8007a6:	68 01 3b 80 00       	push   $0x803b01
  8007ab:	6a 6c                	push   $0x6c
  8007ad:	68 64 3a 80 00       	push   $0x803a64
  8007b2:	e8 88 14 00 00       	call   801c3f <_panic>

	//panic("flush_block not implemented");
}
  8007b7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8007ba:	5b                   	pop    %ebx
  8007bb:	5e                   	pop    %esi
  8007bc:	5f                   	pop    %edi
  8007bd:	5d                   	pop    %ebp
  8007be:	c3                   	ret    

008007bf <bc_init>:
	cprintf("block cache is good\n");
}

void
bc_init(void)
{
  8007bf:	55                   	push   %ebp
  8007c0:	89 e5                	mov    %esp,%ebp
  8007c2:	81 ec 24 02 00 00    	sub    $0x224,%esp
	struct Super super;
	set_pgfault_handler(bc_pgfault);
  8007c8:	68 fc 04 80 00       	push   $0x8004fc
  8007cd:	e8 9d 20 00 00       	call   80286f <set_pgfault_handler>
check_bc(void)
{
	struct Super backup;

	// back up super block
	memmove(&backup, diskaddr(1), sizeof backup);
  8007d2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  8007d9:	e8 46 fe ff ff       	call   800624 <diskaddr>
  8007de:	83 c4 0c             	add    $0xc,%esp
  8007e1:	68 08 01 00 00       	push   $0x108
  8007e6:	50                   	push   %eax
  8007e7:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  8007ed:	50                   	push   %eax
  8007ee:	e8 fd 1b 00 00       	call   8023f0 <memmove>

	// smash it
	strcpy(diskaddr(1), "OOPS!\n");
  8007f3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  8007fa:	e8 25 fe ff ff       	call   800624 <diskaddr>
  8007ff:	83 c4 08             	add    $0x8,%esp
  800802:	68 18 3b 80 00       	push   $0x803b18
  800807:	50                   	push   %eax
  800808:	e8 6e 1a 00 00       	call   80227b <strcpy>
	flush_block(diskaddr(1));
  80080d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800814:	e8 0b fe ff ff       	call   800624 <diskaddr>
  800819:	89 04 24             	mov    %eax,(%esp)
  80081c:	e8 d1 fe ff ff       	call   8006f2 <flush_block>
	assert(va_is_mapped(diskaddr(1)));
  800821:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800828:	e8 f7 fd ff ff       	call   800624 <diskaddr>
  80082d:	89 04 24             	mov    %eax,(%esp)
  800830:	e8 27 fe ff ff       	call   80065c <va_is_mapped>
  800835:	83 c4 10             	add    $0x10,%esp
  800838:	84 c0                	test   %al,%al
  80083a:	75 16                	jne    800852 <bc_init+0x93>
  80083c:	68 3a 3b 80 00       	push   $0x803b3a
  800841:	68 52 39 80 00       	push   $0x803952
  800846:	6a 7e                	push   $0x7e
  800848:	68 64 3a 80 00       	push   $0x803a64
  80084d:	e8 ed 13 00 00       	call   801c3f <_panic>
	assert(!va_is_dirty(diskaddr(1)));
  800852:	83 ec 0c             	sub    $0xc,%esp
  800855:	6a 01                	push   $0x1
  800857:	e8 c8 fd ff ff       	call   800624 <diskaddr>
  80085c:	89 04 24             	mov    %eax,(%esp)
  80085f:	e8 2b fe ff ff       	call   80068f <va_is_dirty>
  800864:	83 c4 10             	add    $0x10,%esp
  800867:	84 c0                	test   %al,%al
  800869:	74 16                	je     800881 <bc_init+0xc2>
  80086b:	68 1f 3b 80 00       	push   $0x803b1f
  800870:	68 52 39 80 00       	push   $0x803952
  800875:	6a 7f                	push   $0x7f
  800877:	68 64 3a 80 00       	push   $0x803a64
  80087c:	e8 be 13 00 00       	call   801c3f <_panic>

	// clear it out
	sys_page_unmap(0, diskaddr(1));
  800881:	83 ec 0c             	sub    $0xc,%esp
  800884:	6a 01                	push   $0x1
  800886:	e8 99 fd ff ff       	call   800624 <diskaddr>
  80088b:	83 c4 08             	add    $0x8,%esp
  80088e:	50                   	push   %eax
  80088f:	6a 00                	push   $0x0
  800891:	e8 4e 1e 00 00       	call   8026e4 <sys_page_unmap>
	assert(!va_is_mapped(diskaddr(1)));
  800896:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  80089d:	e8 82 fd ff ff       	call   800624 <diskaddr>
  8008a2:	89 04 24             	mov    %eax,(%esp)
  8008a5:	e8 b2 fd ff ff       	call   80065c <va_is_mapped>
  8008aa:	83 c4 10             	add    $0x10,%esp
  8008ad:	84 c0                	test   %al,%al
  8008af:	74 19                	je     8008ca <bc_init+0x10b>
  8008b1:	68 39 3b 80 00       	push   $0x803b39
  8008b6:	68 52 39 80 00       	push   $0x803952
  8008bb:	68 83 00 00 00       	push   $0x83
  8008c0:	68 64 3a 80 00       	push   $0x803a64
  8008c5:	e8 75 13 00 00       	call   801c3f <_panic>

	// read it back in
	assert(strcmp(diskaddr(1), "OOPS!\n") == 0);
  8008ca:	83 ec 0c             	sub    $0xc,%esp
  8008cd:	6a 01                	push   $0x1
  8008cf:	e8 50 fd ff ff       	call   800624 <diskaddr>
  8008d4:	83 c4 08             	add    $0x8,%esp
  8008d7:	68 18 3b 80 00       	push   $0x803b18
  8008dc:	50                   	push   %eax
  8008dd:	e8 38 1a 00 00       	call   80231a <strcmp>
  8008e2:	83 c4 10             	add    $0x10,%esp
  8008e5:	85 c0                	test   %eax,%eax
  8008e7:	74 19                	je     800902 <bc_init+0x143>
  8008e9:	68 40 3a 80 00       	push   $0x803a40
  8008ee:	68 52 39 80 00       	push   $0x803952
  8008f3:	68 86 00 00 00       	push   $0x86
  8008f8:	68 64 3a 80 00       	push   $0x803a64
  8008fd:	e8 3d 13 00 00       	call   801c3f <_panic>

	// fix it
	memmove(diskaddr(1), &backup, sizeof backup);
  800902:	83 ec 0c             	sub    $0xc,%esp
  800905:	6a 01                	push   $0x1
  800907:	e8 18 fd ff ff       	call   800624 <diskaddr>
  80090c:	83 c4 0c             	add    $0xc,%esp
  80090f:	68 08 01 00 00       	push   $0x108
  800914:	8d 95 e8 fd ff ff    	lea    -0x218(%ebp),%edx
  80091a:	52                   	push   %edx
  80091b:	50                   	push   %eax
  80091c:	e8 cf 1a 00 00       	call   8023f0 <memmove>
	flush_block(diskaddr(1));
  800921:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800928:	e8 f7 fc ff ff       	call   800624 <diskaddr>
  80092d:	89 04 24             	mov    %eax,(%esp)
  800930:	e8 bd fd ff ff       	call   8006f2 <flush_block>

	cprintf("block cache is good\n");
  800935:	c7 04 24 54 3b 80 00 	movl   $0x803b54,(%esp)
  80093c:	e8 d6 13 00 00       	call   801d17 <cprintf>
	struct Super super;
	set_pgfault_handler(bc_pgfault);
	check_bc();

	// cache the super block by reading it once
	memmove(&super, diskaddr(1), sizeof super);
  800941:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800948:	e8 d7 fc ff ff       	call   800624 <diskaddr>
  80094d:	83 c4 0c             	add    $0xc,%esp
  800950:	68 08 01 00 00       	push   $0x108
  800955:	50                   	push   %eax
  800956:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80095c:	50                   	push   %eax
  80095d:	e8 8e 1a 00 00       	call   8023f0 <memmove>
}
  800962:	83 c4 10             	add    $0x10,%esp
  800965:	c9                   	leave  
  800966:	c3                   	ret    

00800967 <check_super>:
// --------------------------------------------------------------

// Validate the file system super-block.
void
check_super(void)
{
  800967:	55                   	push   %ebp
  800968:	89 e5                	mov    %esp,%ebp
  80096a:	83 ec 08             	sub    $0x8,%esp
	if (super->s_magic != FS_MAGIC)
  80096d:	a1 0c b0 82 00       	mov    0x82b00c,%eax
  800972:	81 38 ae 30 05 4a    	cmpl   $0x4a0530ae,(%eax)
  800978:	74 14                	je     80098e <check_super+0x27>
		panic("bad file system magic number");
  80097a:	83 ec 04             	sub    $0x4,%esp
  80097d:	68 69 3b 80 00       	push   $0x803b69
  800982:	6a 0e                	push   $0xe
  800984:	68 86 3b 80 00       	push   $0x803b86
  800989:	e8 b1 12 00 00       	call   801c3f <_panic>

	if (super->s_nblocks > DISKSIZE/BLKSIZE)
  80098e:	81 78 04 00 00 0c 00 	cmpl   $0xc0000,0x4(%eax)
  800995:	76 14                	jbe    8009ab <check_super+0x44>
		panic("file system is too large");
  800997:	83 ec 04             	sub    $0x4,%esp
  80099a:	68 8e 3b 80 00       	push   $0x803b8e
  80099f:	6a 11                	push   $0x11
  8009a1:	68 86 3b 80 00       	push   $0x803b86
  8009a6:	e8 94 12 00 00       	call   801c3f <_panic>

	cprintf("superblock is good\n");
  8009ab:	83 ec 0c             	sub    $0xc,%esp
  8009ae:	68 a7 3b 80 00       	push   $0x803ba7
  8009b3:	e8 5f 13 00 00       	call   801d17 <cprintf>
}
  8009b8:	83 c4 10             	add    $0x10,%esp
  8009bb:	c9                   	leave  
  8009bc:	c3                   	ret    

008009bd <block_is_free>:

// Check to see if the block bitmap indicates that block 'blockno' is free.
// Return 1 if the block is free, 0 if not.
bool
block_is_free(uint32_t blockno)
{
  8009bd:	55                   	push   %ebp
  8009be:	89 e5                	mov    %esp,%ebp
  8009c0:	8b 4d 08             	mov    0x8(%ebp),%ecx
	if (super == 0 || blockno >= super->s_nblocks)
  8009c3:	a1 0c b0 82 00       	mov    0x82b00c,%eax
  8009c8:	85 c0                	test   %eax,%eax
  8009ca:	74 19                	je     8009e5 <block_is_free+0x28>
  8009cc:	39 48 04             	cmp    %ecx,0x4(%eax)
  8009cf:	76 18                	jbe    8009e9 <block_is_free+0x2c>
		return 0;
	if (bitmap[blockno / 32] & BIT(blockno % 32))
  8009d1:	89 ca                	mov    %ecx,%edx
  8009d3:	c1 ea 05             	shr    $0x5,%edx
  8009d6:	a1 08 b0 82 00       	mov    0x82b008,%eax
  8009db:	8b 04 90             	mov    (%eax,%edx,4),%eax
  8009de:	d3 e8                	shr    %cl,%eax
  8009e0:	83 e0 01             	and    $0x1,%eax
  8009e3:	eb 06                	jmp    8009eb <block_is_free+0x2e>
// Return 1 if the block is free, 0 if not.
bool
block_is_free(uint32_t blockno)
{
	if (super == 0 || blockno >= super->s_nblocks)
		return 0;
  8009e5:	b0 00                	mov    $0x0,%al
  8009e7:	eb 02                	jmp    8009eb <block_is_free+0x2e>
  8009e9:	b0 00                	mov    $0x0,%al
	if (bitmap[blockno / 32] & BIT(blockno % 32))
		return 1;
	return 0;
}
  8009eb:	5d                   	pop    %ebp
  8009ec:	c3                   	ret    

008009ed <free_block>:

// Mark a block free in the bitmap
void
free_block(uint32_t blockno)
{
  8009ed:	55                   	push   %ebp
  8009ee:	89 e5                	mov    %esp,%ebp
  8009f0:	53                   	push   %ebx
  8009f1:	83 ec 04             	sub    $0x4,%esp
  8009f4:	8b 4d 08             	mov    0x8(%ebp),%ecx
	// Blockno zero is the null pointer of block numbers.
	if (blockno == 0)
  8009f7:	85 c9                	test   %ecx,%ecx
  8009f9:	75 14                	jne    800a0f <free_block+0x22>
		panic("attempt to free zero block");
  8009fb:	83 ec 04             	sub    $0x4,%esp
  8009fe:	68 bb 3b 80 00       	push   $0x803bbb
  800a03:	6a 2c                	push   $0x2c
  800a05:	68 86 3b 80 00       	push   $0x803b86
  800a0a:	e8 30 12 00 00       	call   801c3f <_panic>
	bitmap[blockno / 32] |= BIT(blockno % 32);
  800a0f:	89 cb                	mov    %ecx,%ebx
  800a11:	c1 eb 05             	shr    $0x5,%ebx
  800a14:	8b 15 08 b0 82 00    	mov    0x82b008,%edx
  800a1a:	b8 01 00 00 00       	mov    $0x1,%eax
  800a1f:	d3 e0                	shl    %cl,%eax
  800a21:	09 04 9a             	or     %eax,(%edx,%ebx,4)
}
  800a24:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800a27:	c9                   	leave  
  800a28:	c3                   	ret    

00800a29 <alloc_block>:
// -E_NO_DISK if we are out of blocks.
//
// Hint: use free_block as an example for manipulating the bitmap.
int
alloc_block(void)
{
  800a29:	55                   	push   %ebp
  800a2a:	89 e5                	mov    %esp,%ebp
  800a2c:	56                   	push   %esi
  800a2d:	53                   	push   %ebx
	// super->s_nblocks blocks in the disk altogether.

	// LAB 5: Your code here.
	//panic("alloc_block not implemented");
	uint32_t blkno = 0;
    for (blkno = 0; blkno != super->s_nblocks * BLKBITSIZE; ++blkno) {
  800a2e:	a1 0c b0 82 00       	mov    0x82b00c,%eax
  800a33:	8b 70 04             	mov    0x4(%eax),%esi
  800a36:	c1 e6 0f             	shl    $0xf,%esi
  800a39:	bb 00 00 00 00       	mov    $0x0,%ebx
  800a3e:	eb 3f                	jmp    800a7f <alloc_block+0x56>
        if (block_is_free(blkno)) {
  800a40:	53                   	push   %ebx
  800a41:	e8 77 ff ff ff       	call   8009bd <block_is_free>
  800a46:	83 c4 04             	add    $0x4,%esp
  800a49:	84 c0                	test   %al,%al
  800a4b:	74 31                	je     800a7e <alloc_block+0x55>
            bitmap[blkno/32] &= ~(1<<(blkno%32));
  800a4d:	89 de                	mov    %ebx,%esi
  800a4f:	c1 ee 05             	shr    $0x5,%esi
  800a52:	8b 15 08 b0 82 00    	mov    0x82b008,%edx
  800a58:	b8 01 00 00 00       	mov    $0x1,%eax
  800a5d:	89 d9                	mov    %ebx,%ecx
  800a5f:	d3 e0                	shl    %cl,%eax
  800a61:	f7 d0                	not    %eax
  800a63:	21 04 b2             	and    %eax,(%edx,%esi,4)
            flush_block(diskaddr(blkno));
  800a66:	83 ec 0c             	sub    $0xc,%esp
  800a69:	53                   	push   %ebx
  800a6a:	e8 b5 fb ff ff       	call   800624 <diskaddr>
  800a6f:	89 04 24             	mov    %eax,(%esp)
  800a72:	e8 7b fc ff ff       	call   8006f2 <flush_block>
            return blkno;
  800a77:	89 d8                	mov    %ebx,%eax
  800a79:	83 c4 10             	add    $0x10,%esp
  800a7c:	eb 0a                	jmp    800a88 <alloc_block+0x5f>
	// super->s_nblocks blocks in the disk altogether.

	// LAB 5: Your code here.
	//panic("alloc_block not implemented");
	uint32_t blkno = 0;
    for (blkno = 0; blkno != super->s_nblocks * BLKBITSIZE; ++blkno) {
  800a7e:	43                   	inc    %ebx
  800a7f:	39 f3                	cmp    %esi,%ebx
  800a81:	75 bd                	jne    800a40 <alloc_block+0x17>
            bitmap[blkno/32] &= ~(1<<(blkno%32));
            flush_block(diskaddr(blkno));
            return blkno;
        }
    }
	return -E_NO_DISK;
  800a83:	b8 f7 ff ff ff       	mov    $0xfffffff7,%eax
}
  800a88:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800a8b:	5b                   	pop    %ebx
  800a8c:	5e                   	pop    %esi
  800a8d:	5d                   	pop    %ebp
  800a8e:	c3                   	ret    

00800a8f <file_block_walk>:
//
// Analogy: This is like pgdir_walk for files.
// Hint: Don't forget to clear any block you allocate.
static int
file_block_walk(struct File *f, uint32_t filebno, uint32_t **ppdiskbno, bool alloc)
{
  800a8f:	55                   	push   %ebp
  800a90:	89 e5                	mov    %esp,%ebp
  800a92:	57                   	push   %edi
  800a93:	56                   	push   %esi
  800a94:	53                   	push   %ebx
  800a95:	83 ec 1c             	sub    $0x1c,%esp
  800a98:	8b 5d 08             	mov    0x8(%ebp),%ebx
       // LAB 5: Your code here.
    if (filebno < NDIRECT) {
  800a9b:	83 fa 09             	cmp    $0x9,%edx
  800a9e:	77 10                	ja     800ab0 <file_block_walk+0x21>
        // Block can be found directly.
        *ppdiskbno = &(f->f_direct[filebno]);
  800aa0:	8d 84 90 88 00 00 00 	lea    0x88(%eax,%edx,4),%eax
  800aa7:	89 01                	mov    %eax,(%ecx)
        return 0;
  800aa9:	b8 00 00 00 00       	mov    $0x0,%eax
  800aae:	eb 7b                	jmp    800b2b <file_block_walk+0x9c>
    } else if (filebno < NDIRECT + NINDIRECT) {
  800ab0:	81 fa 09 04 00 00    	cmp    $0x409,%edx
  800ab6:	77 60                	ja     800b18 <file_block_walk+0x89>
  800ab8:	89 ce                	mov    %ecx,%esi
  800aba:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  800abd:	89 c7                	mov    %eax,%edi
        // Block must be found indirectly.
        if (!f->f_indirect) {
  800abf:	83 b8 b0 00 00 00 00 	cmpl   $0x0,0xb0(%eax)
  800ac6:	75 2f                	jne    800af7 <file_block_walk+0x68>
            if (!alloc)
  800ac8:	84 db                	test   %bl,%bl
  800aca:	74 53                	je     800b1f <file_block_walk+0x90>
                return -E_NOT_FOUND;
            
            // Allocate indirect block.
            f->f_indirect = alloc_block();
  800acc:	e8 58 ff ff ff       	call   800a29 <alloc_block>
  800ad1:	89 87 b0 00 00 00    	mov    %eax,0xb0(%edi)
            if (!f->f_indirect)
  800ad7:	85 c0                	test   %eax,%eax
  800ad9:	74 4b                	je     800b26 <file_block_walk+0x97>
                return -E_NO_DISK;

            // Clear block.
            memset(diskaddr(f->f_indirect), 0, BLKSIZE);
  800adb:	83 ec 0c             	sub    $0xc,%esp
  800ade:	50                   	push   %eax
  800adf:	e8 40 fb ff ff       	call   800624 <diskaddr>
  800ae4:	83 c4 0c             	add    $0xc,%esp
  800ae7:	68 00 10 00 00       	push   $0x1000
  800aec:	6a 00                	push   $0x0
  800aee:	50                   	push   %eax
  800aef:	e8 af 18 00 00       	call   8023a3 <memset>
  800af4:	83 c4 10             	add    $0x10,%esp
        }

        uint32_t *ind_blk = (uint32_t *)diskaddr(f->f_indirect);
  800af7:	83 ec 0c             	sub    $0xc,%esp
  800afa:	ff b7 b0 00 00 00    	pushl  0xb0(%edi)
  800b00:	e8 1f fb ff ff       	call   800624 <diskaddr>
        *ppdiskbno = &ind_blk[filebno - NDIRECT];
  800b05:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
  800b08:	8d 44 98 d8          	lea    -0x28(%eax,%ebx,4),%eax
  800b0c:	89 06                	mov    %eax,(%esi)
        return 0;
  800b0e:	83 c4 10             	add    $0x10,%esp
  800b11:	b8 00 00 00 00       	mov    $0x0,%eax
  800b16:	eb 13                	jmp    800b2b <file_block_walk+0x9c>
    } else {
        // There is no so many blocks.
        return -E_INVAL;
  800b18:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800b1d:	eb 0c                	jmp    800b2b <file_block_walk+0x9c>
        return 0;
    } else if (filebno < NDIRECT + NINDIRECT) {
        // Block must be found indirectly.
        if (!f->f_indirect) {
            if (!alloc)
                return -E_NOT_FOUND;
  800b1f:	b8 f5 ff ff ff       	mov    $0xfffffff5,%eax
  800b24:	eb 05                	jmp    800b2b <file_block_walk+0x9c>
            
            // Allocate indirect block.
            f->f_indirect = alloc_block();
            if (!f->f_indirect)
                return -E_NO_DISK;
  800b26:	b8 f7 ff ff ff       	mov    $0xfffffff7,%eax
    } else {
        // There is no so many blocks.
        return -E_INVAL;
    }
       //panic("file_block_walk not implemented");
}
  800b2b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b2e:	5b                   	pop    %ebx
  800b2f:	5e                   	pop    %esi
  800b30:	5f                   	pop    %edi
  800b31:	5d                   	pop    %ebp
  800b32:	c3                   	ret    

00800b33 <check_bitmap>:
//
// Check that all reserved blocks -- 0, 1, and the bitmap blocks themselves --
// are all marked as in-use.
void
check_bitmap(void)
{
  800b33:	55                   	push   %ebp
  800b34:	89 e5                	mov    %esp,%ebp
  800b36:	57                   	push   %edi
  800b37:	56                   	push   %esi
  800b38:	53                   	push   %ebx
  800b39:	83 ec 0c             	sub    $0xc,%esp
	uint32_t i;

	// Make sure all bitmap blocks are marked in-use
	for (i = 0; i * BLKBITSIZE < super->s_nblocks; i++)
  800b3c:	a1 0c b0 82 00       	mov    0x82b00c,%eax
  800b41:	8b 78 04             	mov    0x4(%eax),%edi
  800b44:	be 02 00 00 00       	mov    $0x2,%esi
  800b49:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b4e:	eb 2a                	jmp    800b7a <check_bitmap+0x47>
		assert(!block_is_free(2+i));
  800b50:	56                   	push   %esi
  800b51:	e8 67 fe ff ff       	call   8009bd <block_is_free>
  800b56:	83 c4 04             	add    $0x4,%esp
  800b59:	81 c3 00 80 00 00    	add    $0x8000,%ebx
  800b5f:	46                   	inc    %esi
  800b60:	84 c0                	test   %al,%al
  800b62:	74 16                	je     800b7a <check_bitmap+0x47>
  800b64:	68 d6 3b 80 00       	push   $0x803bd6
  800b69:	68 52 39 80 00       	push   $0x803952
  800b6e:	6a 57                	push   $0x57
  800b70:	68 86 3b 80 00       	push   $0x803b86
  800b75:	e8 c5 10 00 00       	call   801c3f <_panic>
check_bitmap(void)
{
	uint32_t i;

	// Make sure all bitmap blocks are marked in-use
	for (i = 0; i * BLKBITSIZE < super->s_nblocks; i++)
  800b7a:	39 fb                	cmp    %edi,%ebx
  800b7c:	72 d2                	jb     800b50 <check_bitmap+0x1d>
		assert(!block_is_free(2+i));

	// Make sure the reserved and root blocks are marked in-use.
	assert(!block_is_free(0));
  800b7e:	83 ec 0c             	sub    $0xc,%esp
  800b81:	6a 00                	push   $0x0
  800b83:	e8 35 fe ff ff       	call   8009bd <block_is_free>
  800b88:	83 c4 10             	add    $0x10,%esp
  800b8b:	84 c0                	test   %al,%al
  800b8d:	74 16                	je     800ba5 <check_bitmap+0x72>
  800b8f:	68 ea 3b 80 00       	push   $0x803bea
  800b94:	68 52 39 80 00       	push   $0x803952
  800b99:	6a 5a                	push   $0x5a
  800b9b:	68 86 3b 80 00       	push   $0x803b86
  800ba0:	e8 9a 10 00 00       	call   801c3f <_panic>
	assert(!block_is_free(1));
  800ba5:	83 ec 0c             	sub    $0xc,%esp
  800ba8:	6a 01                	push   $0x1
  800baa:	e8 0e fe ff ff       	call   8009bd <block_is_free>
  800baf:	83 c4 10             	add    $0x10,%esp
  800bb2:	84 c0                	test   %al,%al
  800bb4:	74 16                	je     800bcc <check_bitmap+0x99>
  800bb6:	68 fc 3b 80 00       	push   $0x803bfc
  800bbb:	68 52 39 80 00       	push   $0x803952
  800bc0:	6a 5b                	push   $0x5b
  800bc2:	68 86 3b 80 00       	push   $0x803b86
  800bc7:	e8 73 10 00 00       	call   801c3f <_panic>

	cprintf("bitmap is good\n");
  800bcc:	83 ec 0c             	sub    $0xc,%esp
  800bcf:	68 0e 3c 80 00       	push   $0x803c0e
  800bd4:	e8 3e 11 00 00       	call   801d17 <cprintf>
}
  800bd9:	83 c4 10             	add    $0x10,%esp
  800bdc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bdf:	5b                   	pop    %ebx
  800be0:	5e                   	pop    %esi
  800be1:	5f                   	pop    %edi
  800be2:	5d                   	pop    %ebp
  800be3:	c3                   	ret    

00800be4 <fs_init>:
// --------------------------------------------------------------

// Initialize the file system
void
fs_init(void)
{
  800be4:	55                   	push   %ebp
  800be5:	89 e5                	mov    %esp,%ebp
  800be7:	83 ec 08             	sub    $0x8,%esp
	static_assert(sizeof(struct File) == 256);

	bc_init();
  800bea:	e8 d0 fb ff ff       	call   8007bf <bc_init>

	// Set "super" to point to the super block.
	super = diskaddr(1);
  800bef:	83 ec 0c             	sub    $0xc,%esp
  800bf2:	6a 01                	push   $0x1
  800bf4:	e8 2b fa ff ff       	call   800624 <diskaddr>
  800bf9:	a3 0c b0 82 00       	mov    %eax,0x82b00c
	check_super();
  800bfe:	e8 64 fd ff ff       	call   800967 <check_super>

	// Set "bitmap" to the beginning of the first bitmap block.
	bitmap = diskaddr(2);
  800c03:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  800c0a:	e8 15 fa ff ff       	call   800624 <diskaddr>
  800c0f:	a3 08 b0 82 00       	mov    %eax,0x82b008
	check_bitmap();
  800c14:	e8 1a ff ff ff       	call   800b33 <check_bitmap>
	
}
  800c19:	83 c4 10             	add    $0x10,%esp
  800c1c:	c9                   	leave  
  800c1d:	c3                   	ret    

00800c1e <file_get_block>:
//	-E_INVAL if filebno is out of range.
//
// Hint: Use file_block_walk and alloc_block.
int
file_get_block(struct File *f, uint32_t filebno, char **blk)
{
  800c1e:	55                   	push   %ebp
  800c1f:	89 e5                	mov    %esp,%ebp
  800c21:	53                   	push   %ebx
  800c22:	83 ec 20             	sub    $0x20,%esp
    // LAB 5: Your code here.
    // Find block's disk block number.
    uint32_t *pdiskbno;
    int r = file_block_walk(f, filebno, &pdiskbno, 1);
  800c25:	6a 01                	push   $0x1
  800c27:	8d 4d f4             	lea    -0xc(%ebp),%ecx
  800c2a:	8b 55 0c             	mov    0xc(%ebp),%edx
  800c2d:	8b 45 08             	mov    0x8(%ebp),%eax
  800c30:	e8 5a fe ff ff       	call   800a8f <file_block_walk>
    if (r)
  800c35:	83 c4 10             	add    $0x10,%esp
  800c38:	85 c0                	test   %eax,%eax
  800c3a:	75 36                	jne    800c72 <file_get_block+0x54>
        return r;

    // Not exist yet.
    if (!*pdiskbno)
  800c3c:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  800c3f:	8b 03                	mov    (%ebx),%eax
  800c41:	85 c0                	test   %eax,%eax
  800c43:	75 10                	jne    800c55 <file_get_block+0x37>
        *pdiskbno = alloc_block();
  800c45:	e8 df fd ff ff       	call   800a29 <alloc_block>
  800c4a:	89 03                	mov    %eax,(%ebx)

    // Allocation failed.
    if (!*pdiskbno)
  800c4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800c4f:	8b 00                	mov    (%eax),%eax
  800c51:	85 c0                	test   %eax,%eax
  800c53:	74 18                	je     800c6d <file_get_block+0x4f>
        return -E_NO_DISK;

    *blk = (char *)diskaddr(*pdiskbno);
  800c55:	83 ec 0c             	sub    $0xc,%esp
  800c58:	50                   	push   %eax
  800c59:	e8 c6 f9 ff ff       	call   800624 <diskaddr>
  800c5e:	8b 55 10             	mov    0x10(%ebp),%edx
  800c61:	89 02                	mov    %eax,(%edx)
    return 0;       
  800c63:	83 c4 10             	add    $0x10,%esp
  800c66:	b8 00 00 00 00       	mov    $0x0,%eax
  800c6b:	eb 05                	jmp    800c72 <file_get_block+0x54>
    if (!*pdiskbno)
        *pdiskbno = alloc_block();

    // Allocation failed.
    if (!*pdiskbno)
        return -E_NO_DISK;
  800c6d:	b8 f7 ff ff ff       	mov    $0xfffffff7,%eax

    *blk = (char *)diskaddr(*pdiskbno);
    return 0;       
    //panic("file_get_block not implemented");
}
  800c72:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800c75:	c9                   	leave  
  800c76:	c3                   	ret    

00800c77 <walk_path>:
// If we cannot find the file but find the directory
// it should be in, set *pdir and copy the final path
// element into lastelem.
static int
walk_path(const char *path, struct File **pdir, struct File **pf, char *lastelem)
{
  800c77:	55                   	push   %ebp
  800c78:	89 e5                	mov    %esp,%ebp
  800c7a:	57                   	push   %edi
  800c7b:	56                   	push   %esi
  800c7c:	53                   	push   %ebx
  800c7d:	81 ec bc 00 00 00    	sub    $0xbc,%esp
  800c83:	89 95 40 ff ff ff    	mov    %edx,-0xc0(%ebp)
  800c89:	89 8d 3c ff ff ff    	mov    %ecx,-0xc4(%ebp)
  800c8f:	eb 01                	jmp    800c92 <walk_path+0x1b>
// Skip over slashes.
static const char*
skip_slash(const char *p)
{
	while (*p == '/')
		p++;
  800c91:	40                   	inc    %eax

// Skip over slashes.
static const char*
skip_slash(const char *p)
{
	while (*p == '/')
  800c92:	80 38 2f             	cmpb   $0x2f,(%eax)
  800c95:	74 fa                	je     800c91 <walk_path+0x1a>
	int r;

	// if (*path != '/')
	//	return -E_BAD_PATH;
	path = skip_slash(path);
	f = &super->s_root;
  800c97:	8b 0d 0c b0 82 00    	mov    0x82b00c,%ecx
  800c9d:	83 c1 08             	add    $0x8,%ecx
  800ca0:	89 8d 4c ff ff ff    	mov    %ecx,-0xb4(%ebp)
	dir = 0;
	name[0] = 0;
  800ca6:	c6 85 68 ff ff ff 00 	movb   $0x0,-0x98(%ebp)

	if (pdir)
  800cad:	8b 8d 40 ff ff ff    	mov    -0xc0(%ebp),%ecx
  800cb3:	85 c9                	test   %ecx,%ecx
  800cb5:	74 06                	je     800cbd <walk_path+0x46>
		*pdir = 0;
  800cb7:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	*pf = 0;
  800cbd:	8b 8d 3c ff ff ff    	mov    -0xc4(%ebp),%ecx
  800cc3:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)

	// if (*path != '/')
	//	return -E_BAD_PATH;
	path = skip_slash(path);
	f = &super->s_root;
	dir = 0;
  800cc9:	ba 00 00 00 00       	mov    $0x0,%edx
		p = path;
		while (*path != '/' && *path != '\0')
			path++;
		if (path - p >= MAXNAMELEN)
			return -E_BAD_PATH;
		memmove(name, p, path - p);
  800cce:	8d b5 68 ff ff ff    	lea    -0x98(%ebp),%esi
	name[0] = 0;

	if (pdir)
		*pdir = 0;
	*pf = 0;
	while (*path != '\0') {
  800cd4:	e9 51 01 00 00       	jmp    800e2a <walk_path+0x1b3>
		dir = f;
		p = path;
		while (*path != '/' && *path != '\0')
			path++;
  800cd9:	47                   	inc    %edi
  800cda:	eb 02                	jmp    800cde <walk_path+0x67>
  800cdc:	89 c7                	mov    %eax,%edi
		*pdir = 0;
	*pf = 0;
	while (*path != '\0') {
		dir = f;
		p = path;
		while (*path != '/' && *path != '\0')
  800cde:	8a 17                	mov    (%edi),%dl
  800ce0:	80 fa 2f             	cmp    $0x2f,%dl
  800ce3:	74 04                	je     800ce9 <walk_path+0x72>
  800ce5:	84 d2                	test   %dl,%dl
  800ce7:	75 f0                	jne    800cd9 <walk_path+0x62>
			path++;
		if (path - p >= MAXNAMELEN)
  800ce9:	89 fb                	mov    %edi,%ebx
  800ceb:	29 c3                	sub    %eax,%ebx
  800ced:	83 fb 7f             	cmp    $0x7f,%ebx
  800cf0:	0f 8f 5e 01 00 00    	jg     800e54 <walk_path+0x1dd>
			return -E_BAD_PATH;
		memmove(name, p, path - p);
  800cf6:	83 ec 04             	sub    $0x4,%esp
  800cf9:	53                   	push   %ebx
  800cfa:	50                   	push   %eax
  800cfb:	56                   	push   %esi
  800cfc:	e8 ef 16 00 00       	call   8023f0 <memmove>
		name[path - p] = '\0';
  800d01:	c6 84 1d 68 ff ff ff 	movb   $0x0,-0x98(%ebp,%ebx,1)
  800d08:	00 
  800d09:	83 c4 10             	add    $0x10,%esp
  800d0c:	eb 01                	jmp    800d0f <walk_path+0x98>
// Skip over slashes.
static const char*
skip_slash(const char *p)
{
	while (*p == '/')
		p++;
  800d0e:	47                   	inc    %edi

// Skip over slashes.
static const char*
skip_slash(const char *p)
{
	while (*p == '/')
  800d0f:	80 3f 2f             	cmpb   $0x2f,(%edi)
  800d12:	74 fa                	je     800d0e <walk_path+0x97>
			return -E_BAD_PATH;
		memmove(name, p, path - p);
		name[path - p] = '\0';
		path = skip_slash(path);

		if (dir->f_type != FTYPE_DIR)
  800d14:	8b 85 4c ff ff ff    	mov    -0xb4(%ebp),%eax
  800d1a:	83 b8 84 00 00 00 01 	cmpl   $0x1,0x84(%eax)
  800d21:	0f 85 34 01 00 00    	jne    800e5b <walk_path+0x1e4>
	struct File *f;

	// Search dir for name.
	// We maintain the invariant that the size of a directory-file
	// is always a multiple of the file system's block size.
	assert((dir->f_size % BLKSIZE) == 0);
  800d27:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
  800d2d:	a9 ff 0f 00 00       	test   $0xfff,%eax
  800d32:	74 19                	je     800d4d <walk_path+0xd6>
  800d34:	68 1e 3c 80 00       	push   $0x803c1e
  800d39:	68 52 39 80 00       	push   $0x803952
  800d3e:	68 d5 00 00 00       	push   $0xd5
  800d43:	68 86 3b 80 00       	push   $0x803b86
  800d48:	e8 f2 0e 00 00       	call   801c3f <_panic>
	nblock = dir->f_size / BLKSIZE;
  800d4d:	89 c2                	mov    %eax,%edx
  800d4f:	85 c0                	test   %eax,%eax
  800d51:	79 06                	jns    800d59 <walk_path+0xe2>
  800d53:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
  800d59:	c1 fa 0c             	sar    $0xc,%edx
  800d5c:	89 95 48 ff ff ff    	mov    %edx,-0xb8(%ebp)
	for (i = 0; i < nblock; i++) {
  800d62:	c7 85 50 ff ff ff 00 	movl   $0x0,-0xb0(%ebp)
  800d69:	00 00 00 
  800d6c:	89 bd 44 ff ff ff    	mov    %edi,-0xbc(%ebp)
  800d72:	eb 5d                	jmp    800dd1 <walk_path+0x15a>
		if ((r = file_get_block(dir, i, &blk)) < 0)
  800d74:	83 ec 04             	sub    $0x4,%esp
  800d77:	8d 85 64 ff ff ff    	lea    -0x9c(%ebp),%eax
  800d7d:	50                   	push   %eax
  800d7e:	ff b5 50 ff ff ff    	pushl  -0xb0(%ebp)
  800d84:	ff b5 4c ff ff ff    	pushl  -0xb4(%ebp)
  800d8a:	e8 8f fe ff ff       	call   800c1e <file_get_block>
  800d8f:	83 c4 10             	add    $0x10,%esp
  800d92:	85 c0                	test   %eax,%eax
  800d94:	0f 88 eb 00 00 00    	js     800e85 <walk_path+0x20e>
			return r;
		f = (struct File*) blk;
  800d9a:	8b 9d 64 ff ff ff    	mov    -0x9c(%ebp),%ebx
  800da0:	8d bb 00 10 00 00    	lea    0x1000(%ebx),%edi
		for (j = 0; j < BLKFILES; j++)
			if (strcmp(f[j].f_name, name) == 0) {
  800da6:	89 9d 54 ff ff ff    	mov    %ebx,-0xac(%ebp)
  800dac:	83 ec 08             	sub    $0x8,%esp
  800daf:	56                   	push   %esi
  800db0:	53                   	push   %ebx
  800db1:	e8 64 15 00 00       	call   80231a <strcmp>
  800db6:	83 c4 10             	add    $0x10,%esp
  800db9:	85 c0                	test   %eax,%eax
  800dbb:	0f 84 a8 00 00 00    	je     800e69 <walk_path+0x1f2>
  800dc1:	81 c3 00 01 00 00    	add    $0x100,%ebx
	nblock = dir->f_size / BLKSIZE;
	for (i = 0; i < nblock; i++) {
		if ((r = file_get_block(dir, i, &blk)) < 0)
			return r;
		f = (struct File*) blk;
		for (j = 0; j < BLKFILES; j++)
  800dc7:	39 fb                	cmp    %edi,%ebx
  800dc9:	75 db                	jne    800da6 <walk_path+0x12f>
	// Search dir for name.
	// We maintain the invariant that the size of a directory-file
	// is always a multiple of the file system's block size.
	assert((dir->f_size % BLKSIZE) == 0);
	nblock = dir->f_size / BLKSIZE;
	for (i = 0; i < nblock; i++) {
  800dcb:	ff 85 50 ff ff ff    	incl   -0xb0(%ebp)
  800dd1:	8b 8d 50 ff ff ff    	mov    -0xb0(%ebp),%ecx
  800dd7:	39 8d 48 ff ff ff    	cmp    %ecx,-0xb8(%ebp)
  800ddd:	75 95                	jne    800d74 <walk_path+0xfd>
  800ddf:	8b bd 44 ff ff ff    	mov    -0xbc(%ebp),%edi

		if (dir->f_type != FTYPE_DIR)
			return -E_NOT_FOUND;

		if ((r = dir_lookup(dir, name, &f)) < 0) {
			if (r == -E_NOT_FOUND && *path == '\0') {
  800de5:	80 3f 00             	cmpb   $0x0,(%edi)
  800de8:	75 78                	jne    800e62 <walk_path+0x1eb>
				if (pdir)
  800dea:	8b 85 40 ff ff ff    	mov    -0xc0(%ebp),%eax
  800df0:	85 c0                	test   %eax,%eax
  800df2:	74 08                	je     800dfc <walk_path+0x185>
					*pdir = dir;
  800df4:	8b 8d 4c ff ff ff    	mov    -0xb4(%ebp),%ecx
  800dfa:	89 08                	mov    %ecx,(%eax)
				if (lastelem)
  800dfc:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800e00:	74 15                	je     800e17 <walk_path+0x1a0>
					strcpy(lastelem, name);
  800e02:	83 ec 08             	sub    $0x8,%esp
  800e05:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
  800e0b:	50                   	push   %eax
  800e0c:	ff 75 08             	pushl  0x8(%ebp)
  800e0f:	e8 67 14 00 00       	call   80227b <strcpy>
  800e14:	83 c4 10             	add    $0x10,%esp
				*pf = 0;
  800e17:	8b 85 3c ff ff ff    	mov    -0xc4(%ebp),%eax
  800e1d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
			}
			return r;
  800e23:	b8 f5 ff ff ff       	mov    $0xfffffff5,%eax
  800e28:	eb 6a                	jmp    800e94 <walk_path+0x21d>
	name[0] = 0;

	if (pdir)
		*pdir = 0;
	*pf = 0;
	while (*path != '\0') {
  800e2a:	80 38 00             	cmpb   $0x0,(%eax)
  800e2d:	0f 85 a9 fe ff ff    	jne    800cdc <walk_path+0x65>
			}
			return r;
		}
	}

	if (pdir)
  800e33:	8b 85 40 ff ff ff    	mov    -0xc0(%ebp),%eax
  800e39:	85 c0                	test   %eax,%eax
  800e3b:	74 02                	je     800e3f <walk_path+0x1c8>
		*pdir = dir;
  800e3d:	89 10                	mov    %edx,(%eax)
	*pf = f;
  800e3f:	8b 85 3c ff ff ff    	mov    -0xc4(%ebp),%eax
  800e45:	8b 8d 4c ff ff ff    	mov    -0xb4(%ebp),%ecx
  800e4b:	89 08                	mov    %ecx,(%eax)
	return 0;
  800e4d:	b8 00 00 00 00       	mov    $0x0,%eax
  800e52:	eb 40                	jmp    800e94 <walk_path+0x21d>
		dir = f;
		p = path;
		while (*path != '/' && *path != '\0')
			path++;
		if (path - p >= MAXNAMELEN)
			return -E_BAD_PATH;
  800e54:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
  800e59:	eb 39                	jmp    800e94 <walk_path+0x21d>
		memmove(name, p, path - p);
		name[path - p] = '\0';
		path = skip_slash(path);

		if (dir->f_type != FTYPE_DIR)
			return -E_NOT_FOUND;
  800e5b:	b8 f5 ff ff ff       	mov    $0xfffffff5,%eax
  800e60:	eb 32                	jmp    800e94 <walk_path+0x21d>
					*pdir = dir;
				if (lastelem)
					strcpy(lastelem, name);
				*pf = 0;
			}
			return r;
  800e62:	b8 f5 ff ff ff       	mov    $0xfffffff5,%eax
  800e67:	eb 2b                	jmp    800e94 <walk_path+0x21d>
  800e69:	8b bd 44 ff ff ff    	mov    -0xbc(%ebp),%edi
  800e6f:	8b 95 4c ff ff ff    	mov    -0xb4(%ebp),%edx
	for (i = 0; i < nblock; i++) {
		if ((r = file_get_block(dir, i, &blk)) < 0)
			return r;
		f = (struct File*) blk;
		for (j = 0; j < BLKFILES; j++)
			if (strcmp(f[j].f_name, name) == 0) {
  800e75:	8b 85 54 ff ff ff    	mov    -0xac(%ebp),%eax
  800e7b:	89 85 4c ff ff ff    	mov    %eax,-0xb4(%ebp)
  800e81:	89 f8                	mov    %edi,%eax
  800e83:	eb a5                	jmp    800e2a <walk_path+0x1b3>
  800e85:	8b bd 44 ff ff ff    	mov    -0xbc(%ebp),%edi

		if (dir->f_type != FTYPE_DIR)
			return -E_NOT_FOUND;

		if ((r = dir_lookup(dir, name, &f)) < 0) {
			if (r == -E_NOT_FOUND && *path == '\0') {
  800e8b:	83 f8 f5             	cmp    $0xfffffff5,%eax
  800e8e:	0f 84 51 ff ff ff    	je     800de5 <walk_path+0x16e>

	if (pdir)
		*pdir = dir;
	*pf = f;
	return 0;
}
  800e94:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e97:	5b                   	pop    %ebx
  800e98:	5e                   	pop    %esi
  800e99:	5f                   	pop    %edi
  800e9a:	5d                   	pop    %ebp
  800e9b:	c3                   	ret    

00800e9c <file_open>:

// Open "path".  On success set *pf to point at the file and return 0.
// On error return < 0.
int
file_open(const char *path, struct File **pf)
{
  800e9c:	55                   	push   %ebp
  800e9d:	89 e5                	mov    %esp,%ebp
  800e9f:	83 ec 14             	sub    $0x14,%esp
	return walk_path(path, 0, pf, 0);
  800ea2:	6a 00                	push   $0x0
  800ea4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ea7:	ba 00 00 00 00       	mov    $0x0,%edx
  800eac:	8b 45 08             	mov    0x8(%ebp),%eax
  800eaf:	e8 c3 fd ff ff       	call   800c77 <walk_path>
}
  800eb4:	c9                   	leave  
  800eb5:	c3                   	ret    

00800eb6 <file_read>:
// Read count bytes from f into buf, starting from seek position
// offset.  This meant to mimic the standard pread function.
// Returns the number of bytes read, < 0 on error.
ssize_t
file_read(struct File *f, void *buf, size_t count, off_t offset)
{
  800eb6:	55                   	push   %ebp
  800eb7:	89 e5                	mov    %esp,%ebp
  800eb9:	57                   	push   %edi
  800eba:	56                   	push   %esi
  800ebb:	53                   	push   %ebx
  800ebc:	83 ec 2c             	sub    $0x2c,%esp
  800ebf:	8b 7d 0c             	mov    0xc(%ebp),%edi
  800ec2:	8b 55 14             	mov    0x14(%ebp),%edx
	int r, bn;
	off_t pos;
	char *blk;

	if (offset >= f->f_size)
  800ec5:	8b 45 08             	mov    0x8(%ebp),%eax
  800ec8:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
  800ece:	39 d0                	cmp    %edx,%eax
  800ed0:	0f 8e 89 00 00 00    	jle    800f5f <file_read+0xa9>
		return 0;

	count = MIN(count, f->f_size - offset);
  800ed6:	29 d0                	sub    %edx,%eax
  800ed8:	89 45 d0             	mov    %eax,-0x30(%ebp)
  800edb:	3b 45 10             	cmp    0x10(%ebp),%eax
  800ede:	76 06                	jbe    800ee6 <file_read+0x30>
  800ee0:	8b 45 10             	mov    0x10(%ebp),%eax
  800ee3:	89 45 d0             	mov    %eax,-0x30(%ebp)

	for (pos = offset; pos < offset + count; ) {
  800ee6:	89 d3                	mov    %edx,%ebx
  800ee8:	03 55 d0             	add    -0x30(%ebp),%edx
  800eeb:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  800eee:	eb 63                	jmp    800f53 <file_read+0x9d>
		if ((r = file_get_block(f, pos / BLKSIZE, &blk)) < 0)
  800ef0:	83 ec 04             	sub    $0x4,%esp
  800ef3:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  800ef6:	50                   	push   %eax
  800ef7:	89 d8                	mov    %ebx,%eax
  800ef9:	85 db                	test   %ebx,%ebx
  800efb:	79 06                	jns    800f03 <file_read+0x4d>
  800efd:	8d 83 ff 0f 00 00    	lea    0xfff(%ebx),%eax
  800f03:	c1 f8 0c             	sar    $0xc,%eax
  800f06:	50                   	push   %eax
  800f07:	ff 75 08             	pushl  0x8(%ebp)
  800f0a:	e8 0f fd ff ff       	call   800c1e <file_get_block>
  800f0f:	83 c4 10             	add    $0x10,%esp
  800f12:	85 c0                	test   %eax,%eax
  800f14:	78 4e                	js     800f64 <file_read+0xae>
			return r;
		bn = MIN(BLKSIZE - pos % BLKSIZE, offset + count - pos);
  800f16:	89 d8                	mov    %ebx,%eax
  800f18:	25 ff 0f 00 80       	and    $0x80000fff,%eax
  800f1d:	79 07                	jns    800f26 <file_read+0x70>
  800f1f:	48                   	dec    %eax
  800f20:	0d 00 f0 ff ff       	or     $0xfffff000,%eax
  800f25:	40                   	inc    %eax
  800f26:	89 c2                	mov    %eax,%edx
  800f28:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
  800f2b:	29 f1                	sub    %esi,%ecx
  800f2d:	be 00 10 00 00       	mov    $0x1000,%esi
  800f32:	29 c6                	sub    %eax,%esi
  800f34:	89 f0                	mov    %esi,%eax
  800f36:	89 ce                	mov    %ecx,%esi
  800f38:	39 c1                	cmp    %eax,%ecx
  800f3a:	76 02                	jbe    800f3e <file_read+0x88>
  800f3c:	89 c6                	mov    %eax,%esi
		memmove(buf, blk + pos % BLKSIZE, bn);
  800f3e:	83 ec 04             	sub    $0x4,%esp
  800f41:	56                   	push   %esi
  800f42:	03 55 e4             	add    -0x1c(%ebp),%edx
  800f45:	52                   	push   %edx
  800f46:	57                   	push   %edi
  800f47:	e8 a4 14 00 00       	call   8023f0 <memmove>
		pos += bn;
  800f4c:	01 f3                	add    %esi,%ebx
		buf += bn;
  800f4e:	01 f7                	add    %esi,%edi
  800f50:	83 c4 10             	add    $0x10,%esp
	if (offset >= f->f_size)
		return 0;

	count = MIN(count, f->f_size - offset);

	for (pos = offset; pos < offset + count; ) {
  800f53:	89 de                	mov    %ebx,%esi
  800f55:	39 5d d4             	cmp    %ebx,-0x2c(%ebp)
  800f58:	77 96                	ja     800ef0 <file_read+0x3a>
		memmove(buf, blk + pos % BLKSIZE, bn);
		pos += bn;
		buf += bn;
	}

	return count;
  800f5a:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800f5d:	eb 05                	jmp    800f64 <file_read+0xae>
	int r, bn;
	off_t pos;
	char *blk;

	if (offset >= f->f_size)
		return 0;
  800f5f:	b8 00 00 00 00       	mov    $0x0,%eax
		pos += bn;
		buf += bn;
	}

	return count;
}
  800f64:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f67:	5b                   	pop    %ebx
  800f68:	5e                   	pop    %esi
  800f69:	5f                   	pop    %edi
  800f6a:	5d                   	pop    %ebp
  800f6b:	c3                   	ret    

00800f6c <file_set_size>:
}

// Set the size of file f, truncating or extending as necessary.
int
file_set_size(struct File *f, off_t newsize)
{
  800f6c:	55                   	push   %ebp
  800f6d:	89 e5                	mov    %esp,%ebp
  800f6f:	57                   	push   %edi
  800f70:	56                   	push   %esi
  800f71:	53                   	push   %ebx
  800f72:	83 ec 2c             	sub    $0x2c,%esp
  800f75:	8b 75 08             	mov    0x8(%ebp),%esi
	if (f->f_size > newsize)
  800f78:	8b 86 80 00 00 00    	mov    0x80(%esi),%eax
  800f7e:	3b 45 0c             	cmp    0xc(%ebp),%eax
  800f81:	0f 8e a8 00 00 00    	jle    80102f <file_set_size+0xc3>
file_truncate_blocks(struct File *f, off_t newsize)
{
	int r;
	uint32_t bno, old_nblocks, new_nblocks;

	old_nblocks = (f->f_size + BLKSIZE - 1) / BLKSIZE;
  800f87:	05 ff 0f 00 00       	add    $0xfff,%eax
  800f8c:	89 c7                	mov    %eax,%edi
  800f8e:	85 c0                	test   %eax,%eax
  800f90:	79 06                	jns    800f98 <file_set_size+0x2c>
  800f92:	8d b8 ff 0f 00 00    	lea    0xfff(%eax),%edi
  800f98:	c1 ff 0c             	sar    $0xc,%edi
	new_nblocks = (newsize + BLKSIZE - 1) / BLKSIZE;
  800f9b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800f9e:	05 ff 0f 00 00       	add    $0xfff,%eax
  800fa3:	89 c2                	mov    %eax,%edx
  800fa5:	85 c0                	test   %eax,%eax
  800fa7:	79 06                	jns    800faf <file_set_size+0x43>
  800fa9:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
  800faf:	c1 fa 0c             	sar    $0xc,%edx
  800fb2:	89 55 d4             	mov    %edx,-0x2c(%ebp)
	for (bno = new_nblocks; bno < old_nblocks; bno++)
  800fb5:	89 d3                	mov    %edx,%ebx
  800fb7:	eb 37                	jmp    800ff0 <file_set_size+0x84>
file_free_block(struct File *f, uint32_t filebno)
{
	int r;
	uint32_t *ptr;

	if ((r = file_block_walk(f, filebno, &ptr, 0)) < 0)
  800fb9:	83 ec 0c             	sub    $0xc,%esp
  800fbc:	6a 00                	push   $0x0
  800fbe:	8d 4d e4             	lea    -0x1c(%ebp),%ecx
  800fc1:	89 da                	mov    %ebx,%edx
  800fc3:	89 f0                	mov    %esi,%eax
  800fc5:	e8 c5 fa ff ff       	call   800a8f <file_block_walk>
  800fca:	83 c4 10             	add    $0x10,%esp
  800fcd:	85 c0                	test   %eax,%eax
  800fcf:	78 4b                	js     80101c <file_set_size+0xb0>
		return r;
	if (*ptr) {
  800fd1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  800fd4:	8b 00                	mov    (%eax),%eax
  800fd6:	85 c0                	test   %eax,%eax
  800fd8:	74 15                	je     800fef <file_set_size+0x83>
		free_block(*ptr);
  800fda:	83 ec 0c             	sub    $0xc,%esp
  800fdd:	50                   	push   %eax
  800fde:	e8 0a fa ff ff       	call   8009ed <free_block>
		*ptr = 0;
  800fe3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  800fe6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  800fec:	83 c4 10             	add    $0x10,%esp
	int r;
	uint32_t bno, old_nblocks, new_nblocks;

	old_nblocks = (f->f_size + BLKSIZE - 1) / BLKSIZE;
	new_nblocks = (newsize + BLKSIZE - 1) / BLKSIZE;
	for (bno = new_nblocks; bno < old_nblocks; bno++)
  800fef:	43                   	inc    %ebx
  800ff0:	39 df                	cmp    %ebx,%edi
  800ff2:	77 c5                	ja     800fb9 <file_set_size+0x4d>
		if ((r = file_free_block(f, bno)) < 0)
			cprintf("warning: file_free_block: %e", r);

	if (new_nblocks <= NDIRECT && f->f_indirect) {
  800ff4:	83 7d d4 0a          	cmpl   $0xa,-0x2c(%ebp)
  800ff8:	77 35                	ja     80102f <file_set_size+0xc3>
  800ffa:	8b 86 b0 00 00 00    	mov    0xb0(%esi),%eax
  801000:	85 c0                	test   %eax,%eax
  801002:	74 2b                	je     80102f <file_set_size+0xc3>
		free_block(f->f_indirect);
  801004:	83 ec 0c             	sub    $0xc,%esp
  801007:	50                   	push   %eax
  801008:	e8 e0 f9 ff ff       	call   8009ed <free_block>
		f->f_indirect = 0;
  80100d:	c7 86 b0 00 00 00 00 	movl   $0x0,0xb0(%esi)
  801014:	00 00 00 
  801017:	83 c4 10             	add    $0x10,%esp
  80101a:	eb 13                	jmp    80102f <file_set_size+0xc3>

	old_nblocks = (f->f_size + BLKSIZE - 1) / BLKSIZE;
	new_nblocks = (newsize + BLKSIZE - 1) / BLKSIZE;
	for (bno = new_nblocks; bno < old_nblocks; bno++)
		if ((r = file_free_block(f, bno)) < 0)
			cprintf("warning: file_free_block: %e", r);
  80101c:	83 ec 08             	sub    $0x8,%esp
  80101f:	50                   	push   %eax
  801020:	68 3b 3c 80 00       	push   $0x803c3b
  801025:	e8 ed 0c 00 00       	call   801d17 <cprintf>
  80102a:	83 c4 10             	add    $0x10,%esp
  80102d:	eb c0                	jmp    800fef <file_set_size+0x83>
int
file_set_size(struct File *f, off_t newsize)
{
	if (f->f_size > newsize)
		file_truncate_blocks(f, newsize);
	f->f_size = newsize;
  80102f:	8b 45 0c             	mov    0xc(%ebp),%eax
  801032:	89 86 80 00 00 00    	mov    %eax,0x80(%esi)
	flush_block(f);
  801038:	83 ec 0c             	sub    $0xc,%esp
  80103b:	56                   	push   %esi
  80103c:	e8 b1 f6 ff ff       	call   8006f2 <flush_block>
	return 0;
}
  801041:	b8 00 00 00 00       	mov    $0x0,%eax
  801046:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801049:	5b                   	pop    %ebx
  80104a:	5e                   	pop    %esi
  80104b:	5f                   	pop    %edi
  80104c:	5d                   	pop    %ebp
  80104d:	c3                   	ret    

0080104e <file_write>:
// offset.  This is meant to mimic the standard pwrite function.
// Extends the file if necessary.
// Returns the number of bytes written, < 0 on error.
int
file_write(struct File *f, const void *buf, size_t count, off_t offset)
{
  80104e:	55                   	push   %ebp
  80104f:	89 e5                	mov    %esp,%ebp
  801051:	57                   	push   %edi
  801052:	56                   	push   %esi
  801053:	53                   	push   %ebx
  801054:	83 ec 2c             	sub    $0x2c,%esp
  801057:	8b 7d 0c             	mov    0xc(%ebp),%edi
  80105a:	8b 5d 14             	mov    0x14(%ebp),%ebx
	int r, bn;
	off_t pos;
	char *blk;

	// Extend file if necessary
	if (offset + count > f->f_size)
  80105d:	89 d8                	mov    %ebx,%eax
  80105f:	03 45 10             	add    0x10(%ebp),%eax
  801062:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  801065:	8b 55 08             	mov    0x8(%ebp),%edx
  801068:	3b 82 80 00 00 00    	cmp    0x80(%edx),%eax
  80106e:	76 78                	jbe    8010e8 <file_write+0x9a>
		if ((r = file_set_size(f, offset + count)) < 0)
  801070:	83 ec 08             	sub    $0x8,%esp
  801073:	50                   	push   %eax
  801074:	52                   	push   %edx
  801075:	e8 f2 fe ff ff       	call   800f6c <file_set_size>
  80107a:	83 c4 10             	add    $0x10,%esp
  80107d:	85 c0                	test   %eax,%eax
  80107f:	79 67                	jns    8010e8 <file_write+0x9a>
  801081:	eb 6f                	jmp    8010f2 <file_write+0xa4>
			return r;

	for (pos = offset; pos < offset + count; ) {
		if ((r = file_get_block(f, pos / BLKSIZE, &blk)) < 0)
  801083:	83 ec 04             	sub    $0x4,%esp
  801086:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801089:	50                   	push   %eax
  80108a:	89 d8                	mov    %ebx,%eax
  80108c:	85 db                	test   %ebx,%ebx
  80108e:	79 06                	jns    801096 <file_write+0x48>
  801090:	8d 83 ff 0f 00 00    	lea    0xfff(%ebx),%eax
  801096:	c1 f8 0c             	sar    $0xc,%eax
  801099:	50                   	push   %eax
  80109a:	ff 75 08             	pushl  0x8(%ebp)
  80109d:	e8 7c fb ff ff       	call   800c1e <file_get_block>
  8010a2:	83 c4 10             	add    $0x10,%esp
  8010a5:	85 c0                	test   %eax,%eax
  8010a7:	78 49                	js     8010f2 <file_write+0xa4>
			return r;
		bn = MIN(BLKSIZE - pos % BLKSIZE, offset + count - pos);
  8010a9:	89 d8                	mov    %ebx,%eax
  8010ab:	25 ff 0f 00 80       	and    $0x80000fff,%eax
  8010b0:	79 07                	jns    8010b9 <file_write+0x6b>
  8010b2:	48                   	dec    %eax
  8010b3:	0d 00 f0 ff ff       	or     $0xfffff000,%eax
  8010b8:	40                   	inc    %eax
  8010b9:	89 c2                	mov    %eax,%edx
  8010bb:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
  8010be:	29 f1                	sub    %esi,%ecx
  8010c0:	be 00 10 00 00       	mov    $0x1000,%esi
  8010c5:	29 c6                	sub    %eax,%esi
  8010c7:	89 f0                	mov    %esi,%eax
  8010c9:	89 ce                	mov    %ecx,%esi
  8010cb:	39 c1                	cmp    %eax,%ecx
  8010cd:	76 02                	jbe    8010d1 <file_write+0x83>
  8010cf:	89 c6                	mov    %eax,%esi
		memmove(blk + pos % BLKSIZE, buf, bn);
  8010d1:	83 ec 04             	sub    $0x4,%esp
  8010d4:	56                   	push   %esi
  8010d5:	57                   	push   %edi
  8010d6:	89 d0                	mov    %edx,%eax
  8010d8:	03 45 e4             	add    -0x1c(%ebp),%eax
  8010db:	50                   	push   %eax
  8010dc:	e8 0f 13 00 00       	call   8023f0 <memmove>
		pos += bn;
  8010e1:	01 f3                	add    %esi,%ebx
		buf += bn;
  8010e3:	01 f7                	add    %esi,%edi
  8010e5:	83 c4 10             	add    $0x10,%esp
	// Extend file if necessary
	if (offset + count > f->f_size)
		if ((r = file_set_size(f, offset + count)) < 0)
			return r;

	for (pos = offset; pos < offset + count; ) {
  8010e8:	89 de                	mov    %ebx,%esi
  8010ea:	39 5d d4             	cmp    %ebx,-0x2c(%ebp)
  8010ed:	77 94                	ja     801083 <file_write+0x35>
		memmove(blk + pos % BLKSIZE, buf, bn);
		pos += bn;
		buf += bn;
	}

	return count;
  8010ef:	8b 45 10             	mov    0x10(%ebp),%eax
}
  8010f2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010f5:	5b                   	pop    %ebx
  8010f6:	5e                   	pop    %esi
  8010f7:	5f                   	pop    %edi
  8010f8:	5d                   	pop    %ebp
  8010f9:	c3                   	ret    

008010fa <file_flush>:
// Loop over all the blocks in file.
// Translate the file block number into a disk block number
// and then check whether that disk block is dirty.  If so, write it out.
void
file_flush(struct File *f)
{
  8010fa:	55                   	push   %ebp
  8010fb:	89 e5                	mov    %esp,%ebp
  8010fd:	56                   	push   %esi
  8010fe:	53                   	push   %ebx
  8010ff:	83 ec 10             	sub    $0x10,%esp
  801102:	8b 75 08             	mov    0x8(%ebp),%esi
	int i;
	uint32_t *pdiskbno;

	for (i = 0; i < (f->f_size + BLKSIZE - 1) / BLKSIZE; i++) {
  801105:	bb 00 00 00 00       	mov    $0x0,%ebx
  80110a:	eb 3a                	jmp    801146 <file_flush+0x4c>
		if (file_block_walk(f, i, &pdiskbno, 0) < 0 ||
  80110c:	83 ec 0c             	sub    $0xc,%esp
  80110f:	6a 00                	push   $0x0
  801111:	8d 4d f4             	lea    -0xc(%ebp),%ecx
  801114:	89 da                	mov    %ebx,%edx
  801116:	89 f0                	mov    %esi,%eax
  801118:	e8 72 f9 ff ff       	call   800a8f <file_block_walk>
  80111d:	83 c4 10             	add    $0x10,%esp
  801120:	85 c0                	test   %eax,%eax
  801122:	78 21                	js     801145 <file_flush+0x4b>
		    pdiskbno == NULL || *pdiskbno == 0)
  801124:	8b 45 f4             	mov    -0xc(%ebp),%eax
{
	int i;
	uint32_t *pdiskbno;

	for (i = 0; i < (f->f_size + BLKSIZE - 1) / BLKSIZE; i++) {
		if (file_block_walk(f, i, &pdiskbno, 0) < 0 ||
  801127:	85 c0                	test   %eax,%eax
  801129:	74 1a                	je     801145 <file_flush+0x4b>
		    pdiskbno == NULL || *pdiskbno == 0)
  80112b:	8b 00                	mov    (%eax),%eax
  80112d:	85 c0                	test   %eax,%eax
  80112f:	74 14                	je     801145 <file_flush+0x4b>
			continue;
		flush_block(diskaddr(*pdiskbno));
  801131:	83 ec 0c             	sub    $0xc,%esp
  801134:	50                   	push   %eax
  801135:	e8 ea f4 ff ff       	call   800624 <diskaddr>
  80113a:	89 04 24             	mov    %eax,(%esp)
  80113d:	e8 b0 f5 ff ff       	call   8006f2 <flush_block>
  801142:	83 c4 10             	add    $0x10,%esp
file_flush(struct File *f)
{
	int i;
	uint32_t *pdiskbno;

	for (i = 0; i < (f->f_size + BLKSIZE - 1) / BLKSIZE; i++) {
  801145:	43                   	inc    %ebx
  801146:	8b 86 80 00 00 00    	mov    0x80(%esi),%eax
  80114c:	05 ff 0f 00 00       	add    $0xfff,%eax
  801151:	89 c2                	mov    %eax,%edx
  801153:	85 c0                	test   %eax,%eax
  801155:	79 06                	jns    80115d <file_flush+0x63>
  801157:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
  80115d:	c1 fa 0c             	sar    $0xc,%edx
  801160:	39 d3                	cmp    %edx,%ebx
  801162:	7c a8                	jl     80110c <file_flush+0x12>
		if (file_block_walk(f, i, &pdiskbno, 0) < 0 ||
		    pdiskbno == NULL || *pdiskbno == 0)
			continue;
		flush_block(diskaddr(*pdiskbno));
	}
	flush_block(f);
  801164:	83 ec 0c             	sub    $0xc,%esp
  801167:	56                   	push   %esi
  801168:	e8 85 f5 ff ff       	call   8006f2 <flush_block>
	if (f->f_indirect)
  80116d:	8b 86 b0 00 00 00    	mov    0xb0(%esi),%eax
  801173:	83 c4 10             	add    $0x10,%esp
  801176:	85 c0                	test   %eax,%eax
  801178:	74 14                	je     80118e <file_flush+0x94>
		flush_block(diskaddr(f->f_indirect));
  80117a:	83 ec 0c             	sub    $0xc,%esp
  80117d:	50                   	push   %eax
  80117e:	e8 a1 f4 ff ff       	call   800624 <diskaddr>
  801183:	89 04 24             	mov    %eax,(%esp)
  801186:	e8 67 f5 ff ff       	call   8006f2 <flush_block>
  80118b:	83 c4 10             	add    $0x10,%esp
}
  80118e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801191:	5b                   	pop    %ebx
  801192:	5e                   	pop    %esi
  801193:	5d                   	pop    %ebp
  801194:	c3                   	ret    

00801195 <file_create>:

// Create "path".  On success set *pf to point at the file and return 0.
// On error return < 0.
int
file_create(const char *path, struct File **pf)
{
  801195:	55                   	push   %ebp
  801196:	89 e5                	mov    %esp,%ebp
  801198:	57                   	push   %edi
  801199:	56                   	push   %esi
  80119a:	53                   	push   %ebx
  80119b:	81 ec b8 00 00 00    	sub    $0xb8,%esp
	char name[MAXNAMELEN];
	int r;
	struct File *dir, *f;

	if ((r = walk_path(path, &dir, &f, name)) == 0)
  8011a1:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
  8011a7:	50                   	push   %eax
  8011a8:	8d 8d 60 ff ff ff    	lea    -0xa0(%ebp),%ecx
  8011ae:	8d 95 64 ff ff ff    	lea    -0x9c(%ebp),%edx
  8011b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8011b7:	e8 bb fa ff ff       	call   800c77 <walk_path>
  8011bc:	83 c4 10             	add    $0x10,%esp
  8011bf:	85 c0                	test   %eax,%eax
  8011c1:	0f 84 d0 00 00 00    	je     801297 <file_create+0x102>
		return -E_FILE_EXISTS;
	if (r != -E_NOT_FOUND || dir == 0)
  8011c7:	83 f8 f5             	cmp    $0xfffffff5,%eax
  8011ca:	0f 85 0b 01 00 00    	jne    8012db <file_create+0x146>
  8011d0:	8b b5 64 ff ff ff    	mov    -0x9c(%ebp),%esi
  8011d6:	85 f6                	test   %esi,%esi
  8011d8:	0f 84 c0 00 00 00    	je     80129e <file_create+0x109>
	int r;
	uint32_t nblock, i, j;
	char *blk;
	struct File *f;

	assert((dir->f_size % BLKSIZE) == 0);
  8011de:	8b 86 80 00 00 00    	mov    0x80(%esi),%eax
  8011e4:	a9 ff 0f 00 00       	test   $0xfff,%eax
  8011e9:	74 19                	je     801204 <file_create+0x6f>
  8011eb:	68 1e 3c 80 00       	push   $0x803c1e
  8011f0:	68 52 39 80 00       	push   $0x803952
  8011f5:	68 ee 00 00 00       	push   $0xee
  8011fa:	68 86 3b 80 00       	push   $0x803b86
  8011ff:	e8 3b 0a 00 00       	call   801c3f <_panic>
	nblock = dir->f_size / BLKSIZE;
  801204:	89 c2                	mov    %eax,%edx
  801206:	85 c0                	test   %eax,%eax
  801208:	79 06                	jns    801210 <file_create+0x7b>
  80120a:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
  801210:	c1 fa 0c             	sar    $0xc,%edx
  801213:	89 95 54 ff ff ff    	mov    %edx,-0xac(%ebp)
	for (i = 0; i < nblock; i++) {
  801219:	bb 00 00 00 00       	mov    $0x0,%ebx
		if ((r = file_get_block(dir, i, &blk)) < 0)
  80121e:	8d bd 5c ff ff ff    	lea    -0xa4(%ebp),%edi
  801224:	eb 39                	jmp    80125f <file_create+0xca>
  801226:	83 ec 04             	sub    $0x4,%esp
  801229:	57                   	push   %edi
  80122a:	53                   	push   %ebx
  80122b:	56                   	push   %esi
  80122c:	e8 ed f9 ff ff       	call   800c1e <file_get_block>
  801231:	83 c4 10             	add    $0x10,%esp
  801234:	85 c0                	test   %eax,%eax
  801236:	0f 88 9f 00 00 00    	js     8012db <file_create+0x146>
			return r;
		f = (struct File*) blk;
  80123c:	8b 85 5c ff ff ff    	mov    -0xa4(%ebp),%eax
  801242:	8d 90 00 10 00 00    	lea    0x1000(%eax),%edx
		for (j = 0; j < BLKFILES; j++)
			if (f[j].f_name[0] == '\0') {
  801248:	80 38 00             	cmpb   $0x0,(%eax)
  80124b:	75 08                	jne    801255 <file_create+0xc0>
				*file = &f[j];
  80124d:	89 85 60 ff ff ff    	mov    %eax,-0xa0(%ebp)
  801253:	eb 50                	jmp    8012a5 <file_create+0x110>
  801255:	05 00 01 00 00       	add    $0x100,%eax
	nblock = dir->f_size / BLKSIZE;
	for (i = 0; i < nblock; i++) {
		if ((r = file_get_block(dir, i, &blk)) < 0)
			return r;
		f = (struct File*) blk;
		for (j = 0; j < BLKFILES; j++)
  80125a:	39 d0                	cmp    %edx,%eax
  80125c:	75 ea                	jne    801248 <file_create+0xb3>
	char *blk;
	struct File *f;

	assert((dir->f_size % BLKSIZE) == 0);
	nblock = dir->f_size / BLKSIZE;
	for (i = 0; i < nblock; i++) {
  80125e:	43                   	inc    %ebx
  80125f:	39 9d 54 ff ff ff    	cmp    %ebx,-0xac(%ebp)
  801265:	75 bf                	jne    801226 <file_create+0x91>
			if (f[j].f_name[0] == '\0') {
				*file = &f[j];
				return 0;
			}
	}
	dir->f_size += BLKSIZE;
  801267:	81 86 80 00 00 00 00 	addl   $0x1000,0x80(%esi)
  80126e:	10 00 00 
	if ((r = file_get_block(dir, i, &blk)) < 0)
  801271:	83 ec 04             	sub    $0x4,%esp
  801274:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
  80127a:	50                   	push   %eax
  80127b:	53                   	push   %ebx
  80127c:	56                   	push   %esi
  80127d:	e8 9c f9 ff ff       	call   800c1e <file_get_block>
  801282:	83 c4 10             	add    $0x10,%esp
  801285:	85 c0                	test   %eax,%eax
  801287:	78 52                	js     8012db <file_create+0x146>
		return r;
	f = (struct File*) blk;
	*file = &f[0];
  801289:	8b 85 5c ff ff ff    	mov    -0xa4(%ebp),%eax
  80128f:	89 85 60 ff ff ff    	mov    %eax,-0xa0(%ebp)
  801295:	eb 0e                	jmp    8012a5 <file_create+0x110>
	char name[MAXNAMELEN];
	int r;
	struct File *dir, *f;

	if ((r = walk_path(path, &dir, &f, name)) == 0)
		return -E_FILE_EXISTS;
  801297:	b8 f3 ff ff ff       	mov    $0xfffffff3,%eax
  80129c:	eb 3d                	jmp    8012db <file_create+0x146>
	if (r != -E_NOT_FOUND || dir == 0)
		return r;
  80129e:	b8 f5 ff ff ff       	mov    $0xfffffff5,%eax
  8012a3:	eb 36                	jmp    8012db <file_create+0x146>
	if ((r = dir_alloc_file(dir, &f)) < 0)
		return r;

	strcpy(f->f_name, name);
  8012a5:	83 ec 08             	sub    $0x8,%esp
  8012a8:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
  8012ae:	50                   	push   %eax
  8012af:	ff b5 60 ff ff ff    	pushl  -0xa0(%ebp)
  8012b5:	e8 c1 0f 00 00       	call   80227b <strcpy>
	*pf = f;
  8012ba:	8b 95 60 ff ff ff    	mov    -0xa0(%ebp),%edx
  8012c0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8012c3:	89 10                	mov    %edx,(%eax)
	file_flush(dir);
  8012c5:	83 c4 04             	add    $0x4,%esp
  8012c8:	ff b5 64 ff ff ff    	pushl  -0x9c(%ebp)
  8012ce:	e8 27 fe ff ff       	call   8010fa <file_flush>
	return 0;
  8012d3:	83 c4 10             	add    $0x10,%esp
  8012d6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8012db:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8012de:	5b                   	pop    %ebx
  8012df:	5e                   	pop    %esi
  8012e0:	5f                   	pop    %edi
  8012e1:	5d                   	pop    %ebp
  8012e2:	c3                   	ret    

008012e3 <fs_sync>:


// Sync the entire file system.  A big hammer.
void
fs_sync(void)
{
  8012e3:	55                   	push   %ebp
  8012e4:	89 e5                	mov    %esp,%ebp
  8012e6:	53                   	push   %ebx
  8012e7:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 1; i < super->s_nblocks; i++)
  8012ea:	bb 01 00 00 00       	mov    $0x1,%ebx
  8012ef:	eb 15                	jmp    801306 <fs_sync+0x23>
		flush_block(diskaddr(i));
  8012f1:	83 ec 0c             	sub    $0xc,%esp
  8012f4:	53                   	push   %ebx
  8012f5:	e8 2a f3 ff ff       	call   800624 <diskaddr>
  8012fa:	89 04 24             	mov    %eax,(%esp)
  8012fd:	e8 f0 f3 ff ff       	call   8006f2 <flush_block>
// Sync the entire file system.  A big hammer.
void
fs_sync(void)
{
	int i;
	for (i = 1; i < super->s_nblocks; i++)
  801302:	43                   	inc    %ebx
  801303:	83 c4 10             	add    $0x10,%esp
  801306:	a1 0c b0 82 00       	mov    0x82b00c,%eax
  80130b:	39 58 04             	cmp    %ebx,0x4(%eax)
  80130e:	77 e1                	ja     8012f1 <fs_sync+0xe>
		flush_block(diskaddr(i));
}
  801310:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801313:	c9                   	leave  
  801314:	c3                   	ret    

00801315 <serve_sync>:
}


int
serve_sync(envid_t envid, union Fsipc *req)
{
  801315:	55                   	push   %ebp
  801316:	89 e5                	mov    %esp,%ebp
  801318:	83 ec 08             	sub    $0x8,%esp
	fs_sync();
  80131b:	e8 c3 ff ff ff       	call   8012e3 <fs_sync>
	return 0;
}
  801320:	b8 00 00 00 00       	mov    $0x0,%eax
  801325:	c9                   	leave  
  801326:	c3                   	ret    

00801327 <serve_init>:
// Virtual address at which to receive page mappings containing client requests.
union Fsipc *fsreq = (union Fsipc *)0x0ffff000;

void
serve_init(void)
{
  801327:	55                   	push   %ebp
  801328:	89 e5                	mov    %esp,%ebp
  80132a:	ba 40 50 80 00       	mov    $0x805040,%edx
	int i;
	uintptr_t va = FILEVA;
  80132f:	b9 00 00 00 d0       	mov    $0xd0000000,%ecx
	for (i = 0; i < MAXOPEN; i++) {
  801334:	b8 00 00 00 00       	mov    $0x0,%eax
		opentab[i].o_fileid = i;
  801339:	89 02                	mov    %eax,(%edx)
		opentab[i].o_fd = (struct Fd*) va;
  80133b:	89 4a 0c             	mov    %ecx,0xc(%edx)
		va += PGSIZE;
  80133e:	81 c1 00 10 00 00    	add    $0x1000,%ecx
void
serve_init(void)
{
	int i;
	uintptr_t va = FILEVA;
	for (i = 0; i < MAXOPEN; i++) {
  801344:	40                   	inc    %eax
  801345:	83 c2 10             	add    $0x10,%edx
  801348:	3d 00 04 00 00       	cmp    $0x400,%eax
  80134d:	75 ea                	jne    801339 <serve_init+0x12>
		opentab[i].o_fileid = i;
		opentab[i].o_fd = (struct Fd*) va;
		va += PGSIZE;
	}
}
  80134f:	5d                   	pop    %ebp
  801350:	c3                   	ret    

00801351 <openfile_alloc>:

// Allocate an open file.
int
openfile_alloc(struct OpenFile **o)
{
  801351:	55                   	push   %ebp
  801352:	89 e5                	mov    %esp,%ebp
  801354:	57                   	push   %edi
  801355:	56                   	push   %esi
  801356:	53                   	push   %ebx
  801357:	83 ec 0c             	sub    $0xc,%esp
  80135a:	8b 75 08             	mov    0x8(%ebp),%esi
  80135d:	bf 4c 50 80 00       	mov    $0x80504c,%edi
	int i, r;

	// Find an available open-file table entry
	for (i = 0; i < MAXOPEN; i++) {
  801362:	bb 00 00 00 00       	mov    $0x0,%ebx
		switch (pageref(opentab[i].o_fd)) {
  801367:	83 ec 0c             	sub    $0xc,%esp
  80136a:	ff 37                	pushl  (%edi)
  80136c:	e8 d6 18 00 00       	call   802c47 <pageref>
  801371:	83 c4 10             	add    $0x10,%esp
  801374:	85 c0                	test   %eax,%eax
  801376:	74 07                	je     80137f <openfile_alloc+0x2e>
  801378:	83 f8 01             	cmp    $0x1,%eax
  80137b:	74 20                	je     80139d <openfile_alloc+0x4c>
  80137d:	eb 51                	jmp    8013d0 <openfile_alloc+0x7f>
		case 0:
			if ((r = sys_page_alloc(0, opentab[i].o_fd, PTE_P|PTE_U|PTE_W)) < 0)
  80137f:	83 ec 04             	sub    $0x4,%esp
  801382:	6a 07                	push   $0x7
  801384:	89 d8                	mov    %ebx,%eax
  801386:	c1 e0 04             	shl    $0x4,%eax
  801389:	ff b0 4c 50 80 00    	pushl  0x80504c(%eax)
  80138f:	6a 00                	push   $0x0
  801391:	e8 c9 12 00 00       	call   80265f <sys_page_alloc>
  801396:	83 c4 10             	add    $0x10,%esp
  801399:	85 c0                	test   %eax,%eax
  80139b:	78 44                	js     8013e1 <openfile_alloc+0x90>
				return r;
			/* fall through */
		case 1:
			opentab[i].o_fileid += MAXOPEN;
  80139d:	c1 e3 04             	shl    $0x4,%ebx
  8013a0:	8d 83 40 50 80 00    	lea    0x805040(%ebx),%eax
  8013a6:	81 83 40 50 80 00 00 	addl   $0x400,0x805040(%ebx)
  8013ad:	04 00 00 
			*o = &opentab[i];
  8013b0:	89 06                	mov    %eax,(%esi)
			memset(opentab[i].o_fd, 0, PGSIZE);
  8013b2:	83 ec 04             	sub    $0x4,%esp
  8013b5:	68 00 10 00 00       	push   $0x1000
  8013ba:	6a 00                	push   $0x0
  8013bc:	ff b3 4c 50 80 00    	pushl  0x80504c(%ebx)
  8013c2:	e8 dc 0f 00 00       	call   8023a3 <memset>
			return (*o)->o_fileid;
  8013c7:	8b 06                	mov    (%esi),%eax
  8013c9:	8b 00                	mov    (%eax),%eax
  8013cb:	83 c4 10             	add    $0x10,%esp
  8013ce:	eb 11                	jmp    8013e1 <openfile_alloc+0x90>
openfile_alloc(struct OpenFile **o)
{
	int i, r;

	// Find an available open-file table entry
	for (i = 0; i < MAXOPEN; i++) {
  8013d0:	43                   	inc    %ebx
  8013d1:	83 c7 10             	add    $0x10,%edi
  8013d4:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  8013da:	75 8b                	jne    801367 <openfile_alloc+0x16>
			*o = &opentab[i];
			memset(opentab[i].o_fd, 0, PGSIZE);
			return (*o)->o_fileid;
		}
	}
	return -E_MAX_OPEN;
  8013dc:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  8013e1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8013e4:	5b                   	pop    %ebx
  8013e5:	5e                   	pop    %esi
  8013e6:	5f                   	pop    %edi
  8013e7:	5d                   	pop    %ebp
  8013e8:	c3                   	ret    

008013e9 <openfile_lookup>:

// Look up an open file for envid.
int
openfile_lookup(envid_t envid, uint32_t fileid, struct OpenFile **po)
{
  8013e9:	55                   	push   %ebp
  8013ea:	89 e5                	mov    %esp,%ebp
  8013ec:	57                   	push   %edi
  8013ed:	56                   	push   %esi
  8013ee:	53                   	push   %ebx
  8013ef:	83 ec 18             	sub    $0x18,%esp
  8013f2:	8b 7d 0c             	mov    0xc(%ebp),%edi
	struct OpenFile *o;

	o = &opentab[fileid % MAXOPEN];
  8013f5:	89 fb                	mov    %edi,%ebx
  8013f7:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
  8013fd:	89 de                	mov    %ebx,%esi
  8013ff:	c1 e6 04             	shl    $0x4,%esi
	if (pageref(o->o_fd) <= 1 || o->o_fileid != fileid)
  801402:	ff b6 4c 50 80 00    	pushl  0x80504c(%esi)
int
openfile_lookup(envid_t envid, uint32_t fileid, struct OpenFile **po)
{
	struct OpenFile *o;

	o = &opentab[fileid % MAXOPEN];
  801408:	81 c6 40 50 80 00    	add    $0x805040,%esi
	if (pageref(o->o_fd) <= 1 || o->o_fileid != fileid)
  80140e:	e8 34 18 00 00       	call   802c47 <pageref>
  801413:	83 c4 10             	add    $0x10,%esp
  801416:	83 f8 01             	cmp    $0x1,%eax
  801419:	7e 17                	jle    801432 <openfile_lookup+0x49>
  80141b:	c1 e3 04             	shl    $0x4,%ebx
  80141e:	3b bb 40 50 80 00    	cmp    0x805040(%ebx),%edi
  801424:	75 13                	jne    801439 <openfile_lookup+0x50>
		return -E_INVAL;
	*po = o;
  801426:	8b 45 10             	mov    0x10(%ebp),%eax
  801429:	89 30                	mov    %esi,(%eax)
	return 0;
  80142b:	b8 00 00 00 00       	mov    $0x0,%eax
  801430:	eb 0c                	jmp    80143e <openfile_lookup+0x55>
{
	struct OpenFile *o;

	o = &opentab[fileid % MAXOPEN];
	if (pageref(o->o_fd) <= 1 || o->o_fileid != fileid)
		return -E_INVAL;
  801432:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801437:	eb 05                	jmp    80143e <openfile_lookup+0x55>
  801439:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	*po = o;
	return 0;
}
  80143e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801441:	5b                   	pop    %ebx
  801442:	5e                   	pop    %esi
  801443:	5f                   	pop    %edi
  801444:	5d                   	pop    %ebp
  801445:	c3                   	ret    

00801446 <serve_set_size>:

// Set the size of req->req_fileid to req->req_size bytes, truncating
// or extending the file as necessary.
int
serve_set_size(envid_t envid, struct Fsreq_set_size *req)
{
  801446:	55                   	push   %ebp
  801447:	89 e5                	mov    %esp,%ebp
  801449:	53                   	push   %ebx
  80144a:	83 ec 18             	sub    $0x18,%esp
  80144d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Every file system IPC call has the same general structure.
	// Here's how it goes.

	// First, use openfile_lookup to find the relevant open file.
	// On failure, return the error code to the client with ipc_send.
	if ((r = openfile_lookup(envid, req->req_fileid, &o)) < 0)
  801450:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801453:	50                   	push   %eax
  801454:	ff 33                	pushl  (%ebx)
  801456:	ff 75 08             	pushl  0x8(%ebp)
  801459:	e8 8b ff ff ff       	call   8013e9 <openfile_lookup>
  80145e:	83 c4 10             	add    $0x10,%esp
  801461:	85 c0                	test   %eax,%eax
  801463:	78 14                	js     801479 <serve_set_size+0x33>
		return r;

	// Second, call the relevant file system function (from fs/fs.c).
	// On failure, return the error code to the client.
	return file_set_size(o->o_file, req->req_size);
  801465:	83 ec 08             	sub    $0x8,%esp
  801468:	ff 73 04             	pushl  0x4(%ebx)
  80146b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80146e:	ff 70 04             	pushl  0x4(%eax)
  801471:	e8 f6 fa ff ff       	call   800f6c <file_set_size>
  801476:	83 c4 10             	add    $0x10,%esp
}
  801479:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80147c:	c9                   	leave  
  80147d:	c3                   	ret    

0080147e <serve_read>:
// in ipc->read.req_fileid.  Return the bytes read from the file to
// the caller in ipc->readRet, then update the seek position.  Returns
// the number of bytes successfully read, or < 0 on error.
int
serve_read(envid_t envid, union Fsipc *ipc)
{
  80147e:	55                   	push   %ebp
  80147f:	89 e5                	mov    %esp,%ebp
  801481:	56                   	push   %esi
  801482:	53                   	push   %ebx
  801483:	83 ec 14             	sub    $0x14,%esp
  801486:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Hint: Use file_read.
	// Hint: The seek position is stored in the struct Fd.
	// Lab 5: Your code here:
    // Find open file.
    struct OpenFile *o;
    int r = openfile_lookup(envid, ipc->read.req_fileid, &o);
  801489:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80148c:	50                   	push   %eax
  80148d:	ff 33                	pushl  (%ebx)
  80148f:	ff 75 08             	pushl  0x8(%ebp)
  801492:	e8 52 ff ff ff       	call   8013e9 <openfile_lookup>
    if (r < 0)
  801497:	83 c4 10             	add    $0x10,%esp
  80149a:	85 c0                	test   %eax,%eax
  80149c:	78 2c                	js     8014ca <serve_read+0x4c>
        return r;

    struct File *f = o->o_file;
  80149e:	8b 55 f4             	mov    -0xc(%ebp),%edx
    struct Fd *fd = o->o_fd;
  8014a1:	8b 72 0c             	mov    0xc(%edx),%esi

    // Actual size to read.
    size_t rdsz = req->req_n < PGSIZE ? req->req_n : PGSIZE;

    // Actual size readed.
    ssize_t rddsz = file_read(f, ret->ret_buf, rdsz, fd->fd_offset);
  8014a4:	ff 76 04             	pushl  0x4(%esi)
  8014a7:	8b 43 04             	mov    0x4(%ebx),%eax
  8014aa:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8014af:	76 05                	jbe    8014b6 <serve_read+0x38>
  8014b1:	b8 00 10 00 00       	mov    $0x1000,%eax
  8014b6:	50                   	push   %eax
  8014b7:	53                   	push   %ebx
  8014b8:	ff 72 04             	pushl  0x4(%edx)
  8014bb:	e8 f6 f9 ff ff       	call   800eb6 <file_read>
    if (rddsz < 0)
  8014c0:	83 c4 10             	add    $0x10,%esp
  8014c3:	85 c0                	test   %eax,%eax
  8014c5:	78 03                	js     8014ca <serve_read+0x4c>
        return rddsz;

    // Update current seek position.
    fd->fd_offset += rddsz;
  8014c7:	01 46 04             	add    %eax,0x4(%esi)
    
    return rddsz;
	//return 0;
}
  8014ca:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8014cd:	5b                   	pop    %ebx
  8014ce:	5e                   	pop    %esi
  8014cf:	5d                   	pop    %ebp
  8014d0:	c3                   	ret    

008014d1 <serve_write>:
// the current seek position, and update the seek position
// accordingly.  Extend the file if necessary.  Returns the number of
// bytes written, or < 0 on error.
int
serve_write(envid_t envid, struct Fsreq_write *req)
{
  8014d1:	55                   	push   %ebp
  8014d2:	89 e5                	mov    %esp,%ebp
  8014d4:	56                   	push   %esi
  8014d5:	53                   	push   %ebx
  8014d6:	83 ec 14             	sub    $0x14,%esp
  8014d9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	if (debug)
		cprintf("serve_write %08x %08x %08x\n", envid, req->req_fileid, req->req_n);

	// LAB 5: Your code here.
    struct OpenFile *o;
    int r = openfile_lookup(envid, req->req_fileid, &o);
  8014dc:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8014df:	50                   	push   %eax
  8014e0:	ff 33                	pushl  (%ebx)
  8014e2:	ff 75 08             	pushl  0x8(%ebp)
  8014e5:	e8 ff fe ff ff       	call   8013e9 <openfile_lookup>
    if (r < 0)
  8014ea:	83 c4 10             	add    $0x10,%esp
  8014ed:	85 c0                	test   %eax,%eax
  8014ef:	78 43                	js     801534 <serve_write+0x63>
        return r;

    struct File *f = o->o_file;
  8014f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8014f4:	8b 50 04             	mov    0x4(%eax),%edx
    struct Fd *fd = o->o_fd;
  8014f7:	8b 70 0c             	mov    0xc(%eax),%esi

    // Actual size to write.
    size_t wrsz = req->req_n < PGSIZE ? req->req_n : PGSIZE;
  8014fa:	8b 43 04             	mov    0x4(%ebx),%eax
  8014fd:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801502:	76 05                	jbe    801509 <serve_write+0x38>
  801504:	b8 00 10 00 00       	mov    $0x1000,%eax

    // Maybe the file needs extention.
    if (fd->fd_offset + wrsz > f->f_size)
  801509:	89 c1                	mov    %eax,%ecx
  80150b:	03 4e 04             	add    0x4(%esi),%ecx
  80150e:	3b 8a 80 00 00 00    	cmp    0x80(%edx),%ecx
  801514:	76 06                	jbe    80151c <serve_write+0x4b>
        f->f_size = fd->fd_offset + wrsz;
  801516:	89 8a 80 00 00 00    	mov    %ecx,0x80(%edx)

    // Actual size written.
    ssize_t wrnsz = file_write(f, req->req_buf, wrsz, fd->fd_offset);
  80151c:	ff 76 04             	pushl  0x4(%esi)
  80151f:	50                   	push   %eax
  801520:	83 c3 08             	add    $0x8,%ebx
  801523:	53                   	push   %ebx
  801524:	52                   	push   %edx
  801525:	e8 24 fb ff ff       	call   80104e <file_write>
    if (wrnsz < 0)
  80152a:	83 c4 10             	add    $0x10,%esp
  80152d:	85 c0                	test   %eax,%eax
  80152f:	78 03                	js     801534 <serve_write+0x63>
        return wrnsz;

    // Update current seek position.
    fd->fd_offset += wrnsz;
  801531:	01 46 04             	add    %eax,0x4(%esi)
    
    return wrnsz;
	//panic("serve_write not implemented");
}
  801534:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801537:	5b                   	pop    %ebx
  801538:	5e                   	pop    %esi
  801539:	5d                   	pop    %ebp
  80153a:	c3                   	ret    

0080153b <serve_stat>:

// Stat ipc->stat.req_fileid.  Return the file's struct Stat to the
// caller in ipc->statRet.
int
serve_stat(envid_t envid, union Fsipc *ipc)
{
  80153b:	55                   	push   %ebp
  80153c:	89 e5                	mov    %esp,%ebp
  80153e:	53                   	push   %ebx
  80153f:	83 ec 18             	sub    $0x18,%esp
  801542:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	if (debug)
		cprintf("serve_stat %08x %08x\n", envid, req->req_fileid);

	if ((r = openfile_lookup(envid, req->req_fileid, &o)) < 0)
  801545:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801548:	50                   	push   %eax
  801549:	ff 33                	pushl  (%ebx)
  80154b:	ff 75 08             	pushl  0x8(%ebp)
  80154e:	e8 96 fe ff ff       	call   8013e9 <openfile_lookup>
  801553:	83 c4 10             	add    $0x10,%esp
  801556:	85 c0                	test   %eax,%eax
  801558:	78 3f                	js     801599 <serve_stat+0x5e>
		return r;

	strcpy(ret->ret_name, o->o_file->f_name);
  80155a:	83 ec 08             	sub    $0x8,%esp
  80155d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801560:	ff 70 04             	pushl  0x4(%eax)
  801563:	53                   	push   %ebx
  801564:	e8 12 0d 00 00       	call   80227b <strcpy>
	ret->ret_size = o->o_file->f_size;
  801569:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80156c:	8b 50 04             	mov    0x4(%eax),%edx
  80156f:	8b 92 80 00 00 00    	mov    0x80(%edx),%edx
  801575:	89 93 80 00 00 00    	mov    %edx,0x80(%ebx)
	ret->ret_isdir = (o->o_file->f_type == FTYPE_DIR);
  80157b:	8b 40 04             	mov    0x4(%eax),%eax
  80157e:	83 c4 10             	add    $0x10,%esp
  801581:	83 b8 84 00 00 00 01 	cmpl   $0x1,0x84(%eax)
  801588:	0f 94 c0             	sete   %al
  80158b:	0f b6 c0             	movzbl %al,%eax
  80158e:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801594:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801599:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80159c:	c9                   	leave  
  80159d:	c3                   	ret    

0080159e <serve_flush>:

// Flush all data and metadata of req->req_fileid to disk.
int
serve_flush(envid_t envid, struct Fsreq_flush *req)
{
  80159e:	55                   	push   %ebp
  80159f:	89 e5                	mov    %esp,%ebp
  8015a1:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	if (debug)
		cprintf("serve_flush %08x %08x\n", envid, req->req_fileid);

	if ((r = openfile_lookup(envid, req->req_fileid, &o)) < 0)
  8015a4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8015a7:	50                   	push   %eax
  8015a8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8015ab:	ff 30                	pushl  (%eax)
  8015ad:	ff 75 08             	pushl  0x8(%ebp)
  8015b0:	e8 34 fe ff ff       	call   8013e9 <openfile_lookup>
  8015b5:	83 c4 10             	add    $0x10,%esp
  8015b8:	85 c0                	test   %eax,%eax
  8015ba:	78 16                	js     8015d2 <serve_flush+0x34>
		return r;
	file_flush(o->o_file);
  8015bc:	83 ec 0c             	sub    $0xc,%esp
  8015bf:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8015c2:	ff 70 04             	pushl  0x4(%eax)
  8015c5:	e8 30 fb ff ff       	call   8010fa <file_flush>
	return 0;
  8015ca:	83 c4 10             	add    $0x10,%esp
  8015cd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8015d2:	c9                   	leave  
  8015d3:	c3                   	ret    

008015d4 <serve_open>:
// permissions to return to the calling environment in *pg_store and
// *perm_store respectively.
int
serve_open(envid_t envid, struct Fsreq_open *req,
	   void **pg_store, int *perm_store)
{
  8015d4:	55                   	push   %ebp
  8015d5:	89 e5                	mov    %esp,%ebp
  8015d7:	53                   	push   %ebx
  8015d8:	81 ec 18 04 00 00    	sub    $0x418,%esp
  8015de:	8b 5d 0c             	mov    0xc(%ebp),%ebx

	if (debug)
		cprintf("serve_open %08x %s 0x%x\n", envid, req->req_path, req->req_omode);

	// Copy in the path, making sure it's null-terminated
	memmove(path, req->req_path, MAXPATHLEN);
  8015e1:	68 00 04 00 00       	push   $0x400
  8015e6:	53                   	push   %ebx
  8015e7:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
  8015ed:	50                   	push   %eax
  8015ee:	e8 fd 0d 00 00       	call   8023f0 <memmove>
	path[MAXPATHLEN-1] = 0;
  8015f3:	c6 45 f7 00          	movb   $0x0,-0x9(%ebp)

	// Find an open file ID
	if ((r = openfile_alloc(&o)) < 0) {
  8015f7:	8d 85 f0 fb ff ff    	lea    -0x410(%ebp),%eax
  8015fd:	89 04 24             	mov    %eax,(%esp)
  801600:	e8 4c fd ff ff       	call   801351 <openfile_alloc>
  801605:	83 c4 10             	add    $0x10,%esp
  801608:	85 c0                	test   %eax,%eax
  80160a:	0f 88 f0 00 00 00    	js     801700 <serve_open+0x12c>
		return r;
	}
	fileid = r;

	// Open the file
	if (req->req_omode & O_CREAT) {
  801610:	f6 83 01 04 00 00 01 	testb  $0x1,0x401(%ebx)
  801617:	74 33                	je     80164c <serve_open+0x78>
		if ((r = file_create(path, &f)) < 0) {
  801619:	83 ec 08             	sub    $0x8,%esp
  80161c:	8d 85 f4 fb ff ff    	lea    -0x40c(%ebp),%eax
  801622:	50                   	push   %eax
  801623:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
  801629:	50                   	push   %eax
  80162a:	e8 66 fb ff ff       	call   801195 <file_create>
  80162f:	83 c4 10             	add    $0x10,%esp
  801632:	85 c0                	test   %eax,%eax
  801634:	79 37                	jns    80166d <serve_open+0x99>
			if (!(req->req_omode & O_EXCL) && r == -E_FILE_EXISTS)
  801636:	f6 83 01 04 00 00 04 	testb  $0x4,0x401(%ebx)
  80163d:	0f 85 bd 00 00 00    	jne    801700 <serve_open+0x12c>
  801643:	83 f8 f3             	cmp    $0xfffffff3,%eax
  801646:	0f 85 b4 00 00 00    	jne    801700 <serve_open+0x12c>
				cprintf("file_create failed: %e", r);
			return r;
		}
	} else {
try_open:
		if ((r = file_open(path, &f)) < 0) {
  80164c:	83 ec 08             	sub    $0x8,%esp
  80164f:	8d 85 f4 fb ff ff    	lea    -0x40c(%ebp),%eax
  801655:	50                   	push   %eax
  801656:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
  80165c:	50                   	push   %eax
  80165d:	e8 3a f8 ff ff       	call   800e9c <file_open>
  801662:	83 c4 10             	add    $0x10,%esp
  801665:	85 c0                	test   %eax,%eax
  801667:	0f 88 93 00 00 00    	js     801700 <serve_open+0x12c>
			return r;
		}
	}

	// Truncate
	if (req->req_omode & O_TRUNC) {
  80166d:	f6 83 01 04 00 00 02 	testb  $0x2,0x401(%ebx)
  801674:	74 17                	je     80168d <serve_open+0xb9>
		if ((r = file_set_size(f, 0)) < 0) {
  801676:	83 ec 08             	sub    $0x8,%esp
  801679:	6a 00                	push   $0x0
  80167b:	ff b5 f4 fb ff ff    	pushl  -0x40c(%ebp)
  801681:	e8 e6 f8 ff ff       	call   800f6c <file_set_size>
  801686:	83 c4 10             	add    $0x10,%esp
  801689:	85 c0                	test   %eax,%eax
  80168b:	78 73                	js     801700 <serve_open+0x12c>
			if (debug)
				cprintf("file_set_size failed: %e", r);
			return r;
		}
	}
	if ((r = file_open(path, &f)) < 0) {
  80168d:	83 ec 08             	sub    $0x8,%esp
  801690:	8d 85 f4 fb ff ff    	lea    -0x40c(%ebp),%eax
  801696:	50                   	push   %eax
  801697:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
  80169d:	50                   	push   %eax
  80169e:	e8 f9 f7 ff ff       	call   800e9c <file_open>
  8016a3:	83 c4 10             	add    $0x10,%esp
  8016a6:	85 c0                	test   %eax,%eax
  8016a8:	78 56                	js     801700 <serve_open+0x12c>
			cprintf("file_open failed: %e", r);
		return r;
	}

	// Save the file pointer
	o->o_file = f;
  8016aa:	8b 85 f0 fb ff ff    	mov    -0x410(%ebp),%eax
  8016b0:	8b 95 f4 fb ff ff    	mov    -0x40c(%ebp),%edx
  8016b6:	89 50 04             	mov    %edx,0x4(%eax)

	// Fill out the Fd structure
	o->o_fd->fd_file.id = o->o_fileid;
  8016b9:	8b 50 0c             	mov    0xc(%eax),%edx
  8016bc:	8b 08                	mov    (%eax),%ecx
  8016be:	89 4a 0c             	mov    %ecx,0xc(%edx)
	o->o_fd->fd_omode = req->req_omode & O_ACCMODE;
  8016c1:	8b 48 0c             	mov    0xc(%eax),%ecx
  8016c4:	8b 93 00 04 00 00    	mov    0x400(%ebx),%edx
  8016ca:	83 e2 03             	and    $0x3,%edx
  8016cd:	89 51 08             	mov    %edx,0x8(%ecx)
	o->o_fd->fd_dev_id = devfile.dev_id;
  8016d0:	8b 40 0c             	mov    0xc(%eax),%eax
  8016d3:	8b 15 44 90 80 00    	mov    0x809044,%edx
  8016d9:	89 10                	mov    %edx,(%eax)
	o->o_mode = req->req_omode;
  8016db:	8b 85 f0 fb ff ff    	mov    -0x410(%ebp),%eax
  8016e1:	8b 93 00 04 00 00    	mov    0x400(%ebx),%edx
  8016e7:	89 50 08             	mov    %edx,0x8(%eax)
	if (debug)
		cprintf("sending success, page %08x\n", (uintptr_t) o->o_fd);

	// Share the FD page with the caller by setting *pg_store,
	// store its permission in *perm_store
	*pg_store = o->o_fd;
  8016ea:	8b 50 0c             	mov    0xc(%eax),%edx
  8016ed:	8b 45 10             	mov    0x10(%ebp),%eax
  8016f0:	89 10                	mov    %edx,(%eax)
	*perm_store = PTE_P|PTE_U|PTE_W|PTE_SHARE;
  8016f2:	8b 45 14             	mov    0x14(%ebp),%eax
  8016f5:	c7 00 07 04 00 00    	movl   $0x407,(%eax)

	return 0;
  8016fb:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801700:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801703:	c9                   	leave  
  801704:	c3                   	ret    

00801705 <serve>:
	[FSREQ_SYNC] =		serve_sync,
};

void
serve(void)
{
  801705:	55                   	push   %ebp
  801706:	89 e5                	mov    %esp,%ebp
  801708:	56                   	push   %esi
  801709:	53                   	push   %ebx
  80170a:	83 ec 10             	sub    $0x10,%esp
	int perm, r;
	void *pg;

	while (1) {
		perm = 0;
		req = ipc_recv((int32_t *) &whom, fsreq, &perm);
  80170d:	8d 5d f0             	lea    -0x10(%ebp),%ebx
  801710:	8d 75 f4             	lea    -0xc(%ebp),%esi
	uint32_t req, whom;
	int perm, r;
	void *pg;

	while (1) {
		perm = 0;
  801713:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
		req = ipc_recv((int32_t *) &whom, fsreq, &perm);
  80171a:	83 ec 04             	sub    $0x4,%esp
  80171d:	53                   	push   %ebx
  80171e:	ff 35 24 50 80 00    	pushl  0x805024
  801724:	56                   	push   %esi
  801725:	e8 c4 11 00 00       	call   8028ee <ipc_recv>
		if (debug)
			cprintf("fs req %d from %08x [page %08x: %s]\n",
				req, whom, uvpt[PGNUM(fsreq)], fsreq);

		// All requests must contain an argument page
		if (!(perm & PTE_P)) {
  80172a:	83 c4 10             	add    $0x10,%esp
  80172d:	f6 45 f0 01          	testb  $0x1,-0x10(%ebp)
  801731:	75 15                	jne    801748 <serve+0x43>
			cprintf("Invalid request from %08x: no argument page\n",
  801733:	83 ec 08             	sub    $0x8,%esp
  801736:	ff 75 f4             	pushl  -0xc(%ebp)
  801739:	68 58 3c 80 00       	push   $0x803c58
  80173e:	e8 d4 05 00 00       	call   801d17 <cprintf>
				whom);
			continue; // just leave it hanging...
  801743:	83 c4 10             	add    $0x10,%esp
  801746:	eb cb                	jmp    801713 <serve+0xe>
		}

		pg = NULL;
  801748:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
		if (req == FSREQ_OPEN) {
  80174f:	83 f8 01             	cmp    $0x1,%eax
  801752:	75 18                	jne    80176c <serve+0x67>
			r = serve_open(whom, (struct Fsreq_open*)fsreq, &pg, &perm);
  801754:	53                   	push   %ebx
  801755:	8d 45 ec             	lea    -0x14(%ebp),%eax
  801758:	50                   	push   %eax
  801759:	ff 35 24 50 80 00    	pushl  0x805024
  80175f:	ff 75 f4             	pushl  -0xc(%ebp)
  801762:	e8 6d fe ff ff       	call   8015d4 <serve_open>
  801767:	83 c4 10             	add    $0x10,%esp
  80176a:	eb 3c                	jmp    8017a8 <serve+0xa3>
		} else if (req < ARRAY_SIZE(handlers) && handlers[req]) {
  80176c:	83 f8 08             	cmp    $0x8,%eax
  80176f:	77 1e                	ja     80178f <serve+0x8a>
  801771:	8b 14 85 00 50 80 00 	mov    0x805000(,%eax,4),%edx
  801778:	85 d2                	test   %edx,%edx
  80177a:	74 13                	je     80178f <serve+0x8a>
			r = handlers[req](whom, fsreq);
  80177c:	83 ec 08             	sub    $0x8,%esp
  80177f:	ff 35 24 50 80 00    	pushl  0x805024
  801785:	ff 75 f4             	pushl  -0xc(%ebp)
  801788:	ff d2                	call   *%edx
  80178a:	83 c4 10             	add    $0x10,%esp
  80178d:	eb 19                	jmp    8017a8 <serve+0xa3>
		} else {
			cprintf("Invalid request code %d from %08x\n", req, whom);
  80178f:	83 ec 04             	sub    $0x4,%esp
  801792:	ff 75 f4             	pushl  -0xc(%ebp)
  801795:	50                   	push   %eax
  801796:	68 88 3c 80 00       	push   $0x803c88
  80179b:	e8 77 05 00 00       	call   801d17 <cprintf>
  8017a0:	83 c4 10             	add    $0x10,%esp
			r = -E_INVAL;
  8017a3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
		}
		ipc_send(whom, r, pg, perm);
  8017a8:	ff 75 f0             	pushl  -0x10(%ebp)
  8017ab:	ff 75 ec             	pushl  -0x14(%ebp)
  8017ae:	50                   	push   %eax
  8017af:	ff 75 f4             	pushl  -0xc(%ebp)
  8017b2:	e8 9f 11 00 00       	call   802956 <ipc_send>
		sys_page_unmap(0, fsreq);
  8017b7:	83 c4 08             	add    $0x8,%esp
  8017ba:	ff 35 24 50 80 00    	pushl  0x805024
  8017c0:	6a 00                	push   $0x0
  8017c2:	e8 1d 0f 00 00       	call   8026e4 <sys_page_unmap>
  8017c7:	83 c4 10             	add    $0x10,%esp
  8017ca:	e9 44 ff ff ff       	jmp    801713 <serve+0xe>

008017cf <umain>:
	}
}

void
umain(int argc, char **argv)
{
  8017cf:	55                   	push   %ebp
  8017d0:	89 e5                	mov    %esp,%ebp
  8017d2:	83 ec 14             	sub    $0x14,%esp
	static_assert(sizeof(struct File) == 256);
	binaryname = "fs";
  8017d5:	c7 05 40 90 80 00 ab 	movl   $0x803cab,0x809040
  8017dc:	3c 80 00 
	cprintf("FS is running\n");
  8017df:	68 ae 3c 80 00       	push   $0x803cae
  8017e4:	e8 2e 05 00 00       	call   801d17 <cprintf>

	// Make sure we are able to do I/O
	ahci_init();
  8017e9:	e8 04 ea ff ff       	call   8001f2 <ahci_init>
	cprintf("FS can do I/O\n");
  8017ee:	c7 04 24 bd 3c 80 00 	movl   $0x803cbd,(%esp)
  8017f5:	e8 1d 05 00 00       	call   801d17 <cprintf>

	serve_init();
  8017fa:	e8 28 fb ff ff       	call   801327 <serve_init>
	fs_init();
  8017ff:	e8 e0 f3 ff ff       	call   800be4 <fs_init>
        fs_test();
  801804:	e8 05 00 00 00       	call   80180e <fs_test>
	serve();
  801809:	e8 f7 fe ff ff       	call   801705 <serve>

0080180e <fs_test>:

static char *msg = "This is the NEW message of the day!\n\n";

void
fs_test(void)
{
  80180e:	55                   	push   %ebp
  80180f:	89 e5                	mov    %esp,%ebp
  801811:	53                   	push   %ebx
  801812:	83 ec 18             	sub    $0x18,%esp
	int r;
	char *blk;
	uint32_t *bits;

	// back up bitmap
	if ((r = sys_page_alloc(0, (void*) PGSIZE, PTE_P|PTE_U|PTE_W)) < 0)
  801815:	6a 07                	push   $0x7
  801817:	68 00 10 00 00       	push   $0x1000
  80181c:	6a 00                	push   $0x0
  80181e:	e8 3c 0e 00 00       	call   80265f <sys_page_alloc>
  801823:	83 c4 10             	add    $0x10,%esp
  801826:	85 c0                	test   %eax,%eax
  801828:	79 12                	jns    80183c <fs_test+0x2e>
		panic("sys_page_alloc: %e", r);
  80182a:	50                   	push   %eax
  80182b:	68 cc 3c 80 00       	push   $0x803ccc
  801830:	6a 12                	push   $0x12
  801832:	68 df 3c 80 00       	push   $0x803cdf
  801837:	e8 03 04 00 00       	call   801c3f <_panic>
	bits = (uint32_t*) PGSIZE;
	memmove(bits, bitmap, PGSIZE);
  80183c:	83 ec 04             	sub    $0x4,%esp
  80183f:	68 00 10 00 00       	push   $0x1000
  801844:	ff 35 08 b0 82 00    	pushl  0x82b008
  80184a:	68 00 10 00 00       	push   $0x1000
  80184f:	e8 9c 0b 00 00       	call   8023f0 <memmove>
	// allocate block
	if ((r = alloc_block()) < 0)
  801854:	e8 d0 f1 ff ff       	call   800a29 <alloc_block>
  801859:	83 c4 10             	add    $0x10,%esp
  80185c:	85 c0                	test   %eax,%eax
  80185e:	79 12                	jns    801872 <fs_test+0x64>
		panic("alloc_block: %e", r);
  801860:	50                   	push   %eax
  801861:	68 e9 3c 80 00       	push   $0x803ce9
  801866:	6a 17                	push   $0x17
  801868:	68 df 3c 80 00       	push   $0x803cdf
  80186d:	e8 cd 03 00 00       	call   801c3f <_panic>
	// check that block was free
	assert(bits[r/32] & (1 << (r%32)));
  801872:	89 c2                	mov    %eax,%edx
  801874:	85 c0                	test   %eax,%eax
  801876:	79 03                	jns    80187b <fs_test+0x6d>
  801878:	8d 50 1f             	lea    0x1f(%eax),%edx
  80187b:	c1 fa 05             	sar    $0x5,%edx
  80187e:	c1 e2 02             	shl    $0x2,%edx
  801881:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801886:	89 c1                	mov    %eax,%ecx
  801888:	79 05                	jns    80188f <fs_test+0x81>
  80188a:	49                   	dec    %ecx
  80188b:	83 c9 e0             	or     $0xffffffe0,%ecx
  80188e:	41                   	inc    %ecx
  80188f:	bb 01 00 00 00       	mov    $0x1,%ebx
  801894:	89 d8                	mov    %ebx,%eax
  801896:	d3 e0                	shl    %cl,%eax
  801898:	85 82 00 10 00 00    	test   %eax,0x1000(%edx)
  80189e:	75 16                	jne    8018b6 <fs_test+0xa8>
  8018a0:	68 f9 3c 80 00       	push   $0x803cf9
  8018a5:	68 52 39 80 00       	push   $0x803952
  8018aa:	6a 19                	push   $0x19
  8018ac:	68 df 3c 80 00       	push   $0x803cdf
  8018b1:	e8 89 03 00 00       	call   801c3f <_panic>
	// and is not free any more
	assert(!(bitmap[r/32] & (1 << (r%32))));
  8018b6:	8b 0d 08 b0 82 00    	mov    0x82b008,%ecx
  8018bc:	85 04 11             	test   %eax,(%ecx,%edx,1)
  8018bf:	74 16                	je     8018d7 <fs_test+0xc9>
  8018c1:	68 74 3e 80 00       	push   $0x803e74
  8018c6:	68 52 39 80 00       	push   $0x803952
  8018cb:	6a 1b                	push   $0x1b
  8018cd:	68 df 3c 80 00       	push   $0x803cdf
  8018d2:	e8 68 03 00 00       	call   801c3f <_panic>
	cprintf("alloc_block is good\n");
  8018d7:	83 ec 0c             	sub    $0xc,%esp
  8018da:	68 14 3d 80 00       	push   $0x803d14
  8018df:	e8 33 04 00 00       	call   801d17 <cprintf>

	if ((r = file_open("/not-found", &f)) < 0 && r != -E_NOT_FOUND)
  8018e4:	83 c4 08             	add    $0x8,%esp
  8018e7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8018ea:	50                   	push   %eax
  8018eb:	68 29 3d 80 00       	push   $0x803d29
  8018f0:	e8 a7 f5 ff ff       	call   800e9c <file_open>
  8018f5:	83 c4 10             	add    $0x10,%esp
  8018f8:	85 c0                	test   %eax,%eax
  8018fa:	79 17                	jns    801913 <fs_test+0x105>
  8018fc:	83 f8 f5             	cmp    $0xfffffff5,%eax
  8018ff:	74 2a                	je     80192b <fs_test+0x11d>
		panic("file_open /not-found: %e", r);
  801901:	50                   	push   %eax
  801902:	68 34 3d 80 00       	push   $0x803d34
  801907:	6a 1f                	push   $0x1f
  801909:	68 df 3c 80 00       	push   $0x803cdf
  80190e:	e8 2c 03 00 00       	call   801c3f <_panic>
	else if (r == 0)
  801913:	85 c0                	test   %eax,%eax
  801915:	75 14                	jne    80192b <fs_test+0x11d>
		panic("file_open /not-found succeeded!");
  801917:	83 ec 04             	sub    $0x4,%esp
  80191a:	68 94 3e 80 00       	push   $0x803e94
  80191f:	6a 21                	push   $0x21
  801921:	68 df 3c 80 00       	push   $0x803cdf
  801926:	e8 14 03 00 00       	call   801c3f <_panic>
	if ((r = file_open("/newmotd", &f)) < 0)
  80192b:	83 ec 08             	sub    $0x8,%esp
  80192e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801931:	50                   	push   %eax
  801932:	68 4d 3d 80 00       	push   $0x803d4d
  801937:	e8 60 f5 ff ff       	call   800e9c <file_open>
  80193c:	83 c4 10             	add    $0x10,%esp
  80193f:	85 c0                	test   %eax,%eax
  801941:	79 12                	jns    801955 <fs_test+0x147>
		panic("file_open /newmotd: %e", r);
  801943:	50                   	push   %eax
  801944:	68 56 3d 80 00       	push   $0x803d56
  801949:	6a 23                	push   $0x23
  80194b:	68 df 3c 80 00       	push   $0x803cdf
  801950:	e8 ea 02 00 00       	call   801c3f <_panic>
	cprintf("file_open is good\n");
  801955:	83 ec 0c             	sub    $0xc,%esp
  801958:	68 6d 3d 80 00       	push   $0x803d6d
  80195d:	e8 b5 03 00 00       	call   801d17 <cprintf>

	if ((r = file_get_block(f, 0, &blk)) < 0)
  801962:	83 c4 0c             	add    $0xc,%esp
  801965:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801968:	50                   	push   %eax
  801969:	6a 00                	push   $0x0
  80196b:	ff 75 f4             	pushl  -0xc(%ebp)
  80196e:	e8 ab f2 ff ff       	call   800c1e <file_get_block>
  801973:	83 c4 10             	add    $0x10,%esp
  801976:	85 c0                	test   %eax,%eax
  801978:	79 12                	jns    80198c <fs_test+0x17e>
		panic("file_get_block: %e", r);
  80197a:	50                   	push   %eax
  80197b:	68 80 3d 80 00       	push   $0x803d80
  801980:	6a 27                	push   $0x27
  801982:	68 df 3c 80 00       	push   $0x803cdf
  801987:	e8 b3 02 00 00       	call   801c3f <_panic>
	if (strcmp(blk, msg) != 0)
  80198c:	83 ec 08             	sub    $0x8,%esp
  80198f:	68 b4 3e 80 00       	push   $0x803eb4
  801994:	ff 75 f0             	pushl  -0x10(%ebp)
  801997:	e8 7e 09 00 00       	call   80231a <strcmp>
  80199c:	83 c4 10             	add    $0x10,%esp
  80199f:	85 c0                	test   %eax,%eax
  8019a1:	74 14                	je     8019b7 <fs_test+0x1a9>
		panic("file_get_block returned wrong data");
  8019a3:	83 ec 04             	sub    $0x4,%esp
  8019a6:	68 dc 3e 80 00       	push   $0x803edc
  8019ab:	6a 29                	push   $0x29
  8019ad:	68 df 3c 80 00       	push   $0x803cdf
  8019b2:	e8 88 02 00 00       	call   801c3f <_panic>
	cprintf("file_get_block is good\n");
  8019b7:	83 ec 0c             	sub    $0xc,%esp
  8019ba:	68 93 3d 80 00       	push   $0x803d93
  8019bf:	e8 53 03 00 00       	call   801d17 <cprintf>

	*(volatile char*)blk = *(volatile char*)blk;
  8019c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8019c7:	8a 10                	mov    (%eax),%dl
  8019c9:	88 10                	mov    %dl,(%eax)
	assert((uvpt[PGNUM(blk)] & PTE_D));
  8019cb:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8019ce:	c1 e8 0c             	shr    $0xc,%eax
  8019d1:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8019d8:	83 c4 10             	add    $0x10,%esp
  8019db:	a8 40                	test   $0x40,%al
  8019dd:	75 16                	jne    8019f5 <fs_test+0x1e7>
  8019df:	68 ac 3d 80 00       	push   $0x803dac
  8019e4:	68 52 39 80 00       	push   $0x803952
  8019e9:	6a 2d                	push   $0x2d
  8019eb:	68 df 3c 80 00       	push   $0x803cdf
  8019f0:	e8 4a 02 00 00       	call   801c3f <_panic>
	file_flush(f);
  8019f5:	83 ec 0c             	sub    $0xc,%esp
  8019f8:	ff 75 f4             	pushl  -0xc(%ebp)
  8019fb:	e8 fa f6 ff ff       	call   8010fa <file_flush>
	assert(!(uvpt[PGNUM(blk)] & PTE_D));
  801a00:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801a03:	c1 e8 0c             	shr    $0xc,%eax
  801a06:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801a0d:	83 c4 10             	add    $0x10,%esp
  801a10:	a8 40                	test   $0x40,%al
  801a12:	74 16                	je     801a2a <fs_test+0x21c>
  801a14:	68 ab 3d 80 00       	push   $0x803dab
  801a19:	68 52 39 80 00       	push   $0x803952
  801a1e:	6a 2f                	push   $0x2f
  801a20:	68 df 3c 80 00       	push   $0x803cdf
  801a25:	e8 15 02 00 00       	call   801c3f <_panic>
	cprintf("file_flush is good\n");
  801a2a:	83 ec 0c             	sub    $0xc,%esp
  801a2d:	68 c7 3d 80 00       	push   $0x803dc7
  801a32:	e8 e0 02 00 00       	call   801d17 <cprintf>

	if ((r = file_set_size(f, 0)) < 0)
  801a37:	83 c4 08             	add    $0x8,%esp
  801a3a:	6a 00                	push   $0x0
  801a3c:	ff 75 f4             	pushl  -0xc(%ebp)
  801a3f:	e8 28 f5 ff ff       	call   800f6c <file_set_size>
  801a44:	83 c4 10             	add    $0x10,%esp
  801a47:	85 c0                	test   %eax,%eax
  801a49:	79 12                	jns    801a5d <fs_test+0x24f>
		panic("file_set_size: %e", r);
  801a4b:	50                   	push   %eax
  801a4c:	68 db 3d 80 00       	push   $0x803ddb
  801a51:	6a 33                	push   $0x33
  801a53:	68 df 3c 80 00       	push   $0x803cdf
  801a58:	e8 e2 01 00 00       	call   801c3f <_panic>
	assert(f->f_direct[0] == 0);
  801a5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801a60:	83 b8 88 00 00 00 00 	cmpl   $0x0,0x88(%eax)
  801a67:	74 16                	je     801a7f <fs_test+0x271>
  801a69:	68 ed 3d 80 00       	push   $0x803ded
  801a6e:	68 52 39 80 00       	push   $0x803952
  801a73:	6a 34                	push   $0x34
  801a75:	68 df 3c 80 00       	push   $0x803cdf
  801a7a:	e8 c0 01 00 00       	call   801c3f <_panic>
	assert(!(uvpt[PGNUM(f)] & PTE_D));
  801a7f:	c1 e8 0c             	shr    $0xc,%eax
  801a82:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801a89:	a8 40                	test   $0x40,%al
  801a8b:	74 16                	je     801aa3 <fs_test+0x295>
  801a8d:	68 01 3e 80 00       	push   $0x803e01
  801a92:	68 52 39 80 00       	push   $0x803952
  801a97:	6a 35                	push   $0x35
  801a99:	68 df 3c 80 00       	push   $0x803cdf
  801a9e:	e8 9c 01 00 00       	call   801c3f <_panic>
	cprintf("file_truncate is good\n");
  801aa3:	83 ec 0c             	sub    $0xc,%esp
  801aa6:	68 1b 3e 80 00       	push   $0x803e1b
  801aab:	e8 67 02 00 00       	call   801d17 <cprintf>

	if ((r = file_set_size(f, strlen(msg))) < 0)
  801ab0:	c7 04 24 b4 3e 80 00 	movl   $0x803eb4,(%esp)
  801ab7:	e8 8a 07 00 00       	call   802246 <strlen>
  801abc:	83 c4 08             	add    $0x8,%esp
  801abf:	50                   	push   %eax
  801ac0:	ff 75 f4             	pushl  -0xc(%ebp)
  801ac3:	e8 a4 f4 ff ff       	call   800f6c <file_set_size>
  801ac8:	83 c4 10             	add    $0x10,%esp
  801acb:	85 c0                	test   %eax,%eax
  801acd:	79 12                	jns    801ae1 <fs_test+0x2d3>
		panic("file_set_size 2: %e", r);
  801acf:	50                   	push   %eax
  801ad0:	68 32 3e 80 00       	push   $0x803e32
  801ad5:	6a 39                	push   $0x39
  801ad7:	68 df 3c 80 00       	push   $0x803cdf
  801adc:	e8 5e 01 00 00       	call   801c3f <_panic>
	assert(!(uvpt[PGNUM(f)] & PTE_D));
  801ae1:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ae4:	89 c2                	mov    %eax,%edx
  801ae6:	c1 ea 0c             	shr    $0xc,%edx
  801ae9:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801af0:	f6 c2 40             	test   $0x40,%dl
  801af3:	74 16                	je     801b0b <fs_test+0x2fd>
  801af5:	68 01 3e 80 00       	push   $0x803e01
  801afa:	68 52 39 80 00       	push   $0x803952
  801aff:	6a 3a                	push   $0x3a
  801b01:	68 df 3c 80 00       	push   $0x803cdf
  801b06:	e8 34 01 00 00       	call   801c3f <_panic>
	if ((r = file_get_block(f, 0, &blk)) < 0)
  801b0b:	83 ec 04             	sub    $0x4,%esp
  801b0e:	8d 55 f0             	lea    -0x10(%ebp),%edx
  801b11:	52                   	push   %edx
  801b12:	6a 00                	push   $0x0
  801b14:	50                   	push   %eax
  801b15:	e8 04 f1 ff ff       	call   800c1e <file_get_block>
  801b1a:	83 c4 10             	add    $0x10,%esp
  801b1d:	85 c0                	test   %eax,%eax
  801b1f:	79 12                	jns    801b33 <fs_test+0x325>
		panic("file_get_block 2: %e", r);
  801b21:	50                   	push   %eax
  801b22:	68 46 3e 80 00       	push   $0x803e46
  801b27:	6a 3c                	push   $0x3c
  801b29:	68 df 3c 80 00       	push   $0x803cdf
  801b2e:	e8 0c 01 00 00       	call   801c3f <_panic>
	strcpy(blk, msg);
  801b33:	83 ec 08             	sub    $0x8,%esp
  801b36:	68 b4 3e 80 00       	push   $0x803eb4
  801b3b:	ff 75 f0             	pushl  -0x10(%ebp)
  801b3e:	e8 38 07 00 00       	call   80227b <strcpy>
	assert((uvpt[PGNUM(blk)] & PTE_D));
  801b43:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801b46:	c1 e8 0c             	shr    $0xc,%eax
  801b49:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801b50:	83 c4 10             	add    $0x10,%esp
  801b53:	a8 40                	test   $0x40,%al
  801b55:	75 16                	jne    801b6d <fs_test+0x35f>
  801b57:	68 ac 3d 80 00       	push   $0x803dac
  801b5c:	68 52 39 80 00       	push   $0x803952
  801b61:	6a 3e                	push   $0x3e
  801b63:	68 df 3c 80 00       	push   $0x803cdf
  801b68:	e8 d2 00 00 00       	call   801c3f <_panic>
	file_flush(f);
  801b6d:	83 ec 0c             	sub    $0xc,%esp
  801b70:	ff 75 f4             	pushl  -0xc(%ebp)
  801b73:	e8 82 f5 ff ff       	call   8010fa <file_flush>
	assert(!(uvpt[PGNUM(blk)] & PTE_D));
  801b78:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801b7b:	c1 e8 0c             	shr    $0xc,%eax
  801b7e:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801b85:	83 c4 10             	add    $0x10,%esp
  801b88:	a8 40                	test   $0x40,%al
  801b8a:	74 16                	je     801ba2 <fs_test+0x394>
  801b8c:	68 ab 3d 80 00       	push   $0x803dab
  801b91:	68 52 39 80 00       	push   $0x803952
  801b96:	6a 40                	push   $0x40
  801b98:	68 df 3c 80 00       	push   $0x803cdf
  801b9d:	e8 9d 00 00 00       	call   801c3f <_panic>
	assert(!(uvpt[PGNUM(f)] & PTE_D));
  801ba2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ba5:	c1 e8 0c             	shr    $0xc,%eax
  801ba8:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801baf:	a8 40                	test   $0x40,%al
  801bb1:	74 16                	je     801bc9 <fs_test+0x3bb>
  801bb3:	68 01 3e 80 00       	push   $0x803e01
  801bb8:	68 52 39 80 00       	push   $0x803952
  801bbd:	6a 41                	push   $0x41
  801bbf:	68 df 3c 80 00       	push   $0x803cdf
  801bc4:	e8 76 00 00 00       	call   801c3f <_panic>
	cprintf("file rewrite is good\n");
  801bc9:	83 ec 0c             	sub    $0xc,%esp
  801bcc:	68 5b 3e 80 00       	push   $0x803e5b
  801bd1:	e8 41 01 00 00       	call   801d17 <cprintf>
}
  801bd6:	83 c4 10             	add    $0x10,%esp
  801bd9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801bdc:	c9                   	leave  
  801bdd:	c3                   	ret    

00801bde <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  801bde:	55                   	push   %ebp
  801bdf:	89 e5                	mov    %esp,%ebp
  801be1:	56                   	push   %esi
  801be2:	53                   	push   %ebx
  801be3:	8b 5d 08             	mov    0x8(%ebp),%ebx
  801be6:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  801be9:	e8 33 0a 00 00       	call   802621 <sys_getenvid>
  801bee:	25 ff 03 00 00       	and    $0x3ff,%eax
  801bf3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  801bfa:	c1 e0 07             	shl    $0x7,%eax
  801bfd:	29 d0                	sub    %edx,%eax
  801bff:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801c04:	a3 10 b0 82 00       	mov    %eax,0x82b010
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  801c09:	85 db                	test   %ebx,%ebx
  801c0b:	7e 07                	jle    801c14 <libmain+0x36>
		binaryname = argv[0];
  801c0d:	8b 06                	mov    (%esi),%eax
  801c0f:	a3 40 90 80 00       	mov    %eax,0x809040

	// call user main routine
	umain(argc, argv);
  801c14:	83 ec 08             	sub    $0x8,%esp
  801c17:	56                   	push   %esi
  801c18:	53                   	push   %ebx
  801c19:	e8 b1 fb ff ff       	call   8017cf <umain>

	// exit gracefully
	exit();
  801c1e:	e8 0a 00 00 00       	call   801c2d <exit>
}
  801c23:	83 c4 10             	add    $0x10,%esp
  801c26:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c29:	5b                   	pop    %ebx
  801c2a:	5e                   	pop    %esi
  801c2b:	5d                   	pop    %ebp
  801c2c:	c3                   	ret    

00801c2d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  801c2d:	55                   	push   %ebp
  801c2e:	89 e5                	mov    %esp,%ebp
  801c30:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  801c33:	6a 00                	push   $0x0
  801c35:	e8 a6 09 00 00       	call   8025e0 <sys_env_destroy>
}
  801c3a:	83 c4 10             	add    $0x10,%esp
  801c3d:	c9                   	leave  
  801c3e:	c3                   	ret    

00801c3f <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  801c3f:	55                   	push   %ebp
  801c40:	89 e5                	mov    %esp,%ebp
  801c42:	56                   	push   %esi
  801c43:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  801c44:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  801c47:	8b 35 40 90 80 00    	mov    0x809040,%esi
  801c4d:	e8 cf 09 00 00       	call   802621 <sys_getenvid>
  801c52:	83 ec 0c             	sub    $0xc,%esp
  801c55:	ff 75 0c             	pushl  0xc(%ebp)
  801c58:	ff 75 08             	pushl  0x8(%ebp)
  801c5b:	56                   	push   %esi
  801c5c:	50                   	push   %eax
  801c5d:	68 0c 3f 80 00       	push   $0x803f0c
  801c62:	e8 b0 00 00 00       	call   801d17 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  801c67:	83 c4 18             	add    $0x18,%esp
  801c6a:	53                   	push   %ebx
  801c6b:	ff 75 10             	pushl  0x10(%ebp)
  801c6e:	e8 53 00 00 00       	call   801cc6 <vcprintf>
	cprintf("\n");
  801c73:	c7 04 24 1d 3b 80 00 	movl   $0x803b1d,(%esp)
  801c7a:	e8 98 00 00 00       	call   801d17 <cprintf>
  801c7f:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801c82:	cc                   	int3   
  801c83:	eb fd                	jmp    801c82 <_panic+0x43>

00801c85 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  801c85:	55                   	push   %ebp
  801c86:	89 e5                	mov    %esp,%ebp
  801c88:	53                   	push   %ebx
  801c89:	83 ec 04             	sub    $0x4,%esp
  801c8c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  801c8f:	8b 13                	mov    (%ebx),%edx
  801c91:	8d 42 01             	lea    0x1(%edx),%eax
  801c94:	89 03                	mov    %eax,(%ebx)
  801c96:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801c99:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  801c9d:	3d ff 00 00 00       	cmp    $0xff,%eax
  801ca2:	75 1a                	jne    801cbe <putch+0x39>
		sys_cputs(b->buf, b->idx);
  801ca4:	83 ec 08             	sub    $0x8,%esp
  801ca7:	68 ff 00 00 00       	push   $0xff
  801cac:	8d 43 08             	lea    0x8(%ebx),%eax
  801caf:	50                   	push   %eax
  801cb0:	e8 ee 08 00 00       	call   8025a3 <sys_cputs>
		b->idx = 0;
  801cb5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801cbb:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  801cbe:	ff 43 04             	incl   0x4(%ebx)
}
  801cc1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801cc4:	c9                   	leave  
  801cc5:	c3                   	ret    

00801cc6 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  801cc6:	55                   	push   %ebp
  801cc7:	89 e5                	mov    %esp,%ebp
  801cc9:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  801ccf:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  801cd6:	00 00 00 
	b.cnt = 0;
  801cd9:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  801ce0:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  801ce3:	ff 75 0c             	pushl  0xc(%ebp)
  801ce6:	ff 75 08             	pushl  0x8(%ebp)
  801ce9:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  801cef:	50                   	push   %eax
  801cf0:	68 85 1c 80 00       	push   $0x801c85
  801cf5:	e8 51 01 00 00       	call   801e4b <vprintfmt>
	sys_cputs(b.buf, b.idx);
  801cfa:	83 c4 08             	add    $0x8,%esp
  801cfd:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  801d03:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  801d09:	50                   	push   %eax
  801d0a:	e8 94 08 00 00       	call   8025a3 <sys_cputs>

	return b.cnt;
}
  801d0f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  801d15:	c9                   	leave  
  801d16:	c3                   	ret    

00801d17 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  801d17:	55                   	push   %ebp
  801d18:	89 e5                	mov    %esp,%ebp
  801d1a:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  801d1d:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  801d20:	50                   	push   %eax
  801d21:	ff 75 08             	pushl  0x8(%ebp)
  801d24:	e8 9d ff ff ff       	call   801cc6 <vcprintf>
	va_end(ap);

	return cnt;
}
  801d29:	c9                   	leave  
  801d2a:	c3                   	ret    

00801d2b <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  801d2b:	55                   	push   %ebp
  801d2c:	89 e5                	mov    %esp,%ebp
  801d2e:	57                   	push   %edi
  801d2f:	56                   	push   %esi
  801d30:	53                   	push   %ebx
  801d31:	83 ec 1c             	sub    $0x1c,%esp
  801d34:	89 c7                	mov    %eax,%edi
  801d36:	89 d6                	mov    %edx,%esi
  801d38:	8b 45 08             	mov    0x8(%ebp),%eax
  801d3b:	8b 55 0c             	mov    0xc(%ebp),%edx
  801d3e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  801d41:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  801d44:	8b 4d 10             	mov    0x10(%ebp),%ecx
  801d47:	bb 00 00 00 00       	mov    $0x0,%ebx
  801d4c:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  801d4f:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  801d52:	39 d3                	cmp    %edx,%ebx
  801d54:	72 05                	jb     801d5b <printnum+0x30>
  801d56:	39 45 10             	cmp    %eax,0x10(%ebp)
  801d59:	77 45                	ja     801da0 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  801d5b:	83 ec 0c             	sub    $0xc,%esp
  801d5e:	ff 75 18             	pushl  0x18(%ebp)
  801d61:	8b 45 14             	mov    0x14(%ebp),%eax
  801d64:	8d 58 ff             	lea    -0x1(%eax),%ebx
  801d67:	53                   	push   %ebx
  801d68:	ff 75 10             	pushl  0x10(%ebp)
  801d6b:	83 ec 08             	sub    $0x8,%esp
  801d6e:	ff 75 e4             	pushl  -0x1c(%ebp)
  801d71:	ff 75 e0             	pushl  -0x20(%ebp)
  801d74:	ff 75 dc             	pushl  -0x24(%ebp)
  801d77:	ff 75 d8             	pushl  -0x28(%ebp)
  801d7a:	e8 55 19 00 00       	call   8036d4 <__udivdi3>
  801d7f:	83 c4 18             	add    $0x18,%esp
  801d82:	52                   	push   %edx
  801d83:	50                   	push   %eax
  801d84:	89 f2                	mov    %esi,%edx
  801d86:	89 f8                	mov    %edi,%eax
  801d88:	e8 9e ff ff ff       	call   801d2b <printnum>
  801d8d:	83 c4 20             	add    $0x20,%esp
  801d90:	eb 16                	jmp    801da8 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  801d92:	83 ec 08             	sub    $0x8,%esp
  801d95:	56                   	push   %esi
  801d96:	ff 75 18             	pushl  0x18(%ebp)
  801d99:	ff d7                	call   *%edi
  801d9b:	83 c4 10             	add    $0x10,%esp
  801d9e:	eb 03                	jmp    801da3 <printnum+0x78>
  801da0:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  801da3:	4b                   	dec    %ebx
  801da4:	85 db                	test   %ebx,%ebx
  801da6:	7f ea                	jg     801d92 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  801da8:	83 ec 08             	sub    $0x8,%esp
  801dab:	56                   	push   %esi
  801dac:	83 ec 04             	sub    $0x4,%esp
  801daf:	ff 75 e4             	pushl  -0x1c(%ebp)
  801db2:	ff 75 e0             	pushl  -0x20(%ebp)
  801db5:	ff 75 dc             	pushl  -0x24(%ebp)
  801db8:	ff 75 d8             	pushl  -0x28(%ebp)
  801dbb:	e8 24 1a 00 00       	call   8037e4 <__umoddi3>
  801dc0:	83 c4 14             	add    $0x14,%esp
  801dc3:	0f be 80 2f 3f 80 00 	movsbl 0x803f2f(%eax),%eax
  801dca:	50                   	push   %eax
  801dcb:	ff d7                	call   *%edi
}
  801dcd:	83 c4 10             	add    $0x10,%esp
  801dd0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801dd3:	5b                   	pop    %ebx
  801dd4:	5e                   	pop    %esi
  801dd5:	5f                   	pop    %edi
  801dd6:	5d                   	pop    %ebp
  801dd7:	c3                   	ret    

00801dd8 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  801dd8:	55                   	push   %ebp
  801dd9:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  801ddb:	83 fa 01             	cmp    $0x1,%edx
  801dde:	7e 0e                	jle    801dee <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  801de0:	8b 10                	mov    (%eax),%edx
  801de2:	8d 4a 08             	lea    0x8(%edx),%ecx
  801de5:	89 08                	mov    %ecx,(%eax)
  801de7:	8b 02                	mov    (%edx),%eax
  801de9:	8b 52 04             	mov    0x4(%edx),%edx
  801dec:	eb 22                	jmp    801e10 <getuint+0x38>
	else if (lflag)
  801dee:	85 d2                	test   %edx,%edx
  801df0:	74 10                	je     801e02 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  801df2:	8b 10                	mov    (%eax),%edx
  801df4:	8d 4a 04             	lea    0x4(%edx),%ecx
  801df7:	89 08                	mov    %ecx,(%eax)
  801df9:	8b 02                	mov    (%edx),%eax
  801dfb:	ba 00 00 00 00       	mov    $0x0,%edx
  801e00:	eb 0e                	jmp    801e10 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  801e02:	8b 10                	mov    (%eax),%edx
  801e04:	8d 4a 04             	lea    0x4(%edx),%ecx
  801e07:	89 08                	mov    %ecx,(%eax)
  801e09:	8b 02                	mov    (%edx),%eax
  801e0b:	ba 00 00 00 00       	mov    $0x0,%edx
}
  801e10:	5d                   	pop    %ebp
  801e11:	c3                   	ret    

00801e12 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  801e12:	55                   	push   %ebp
  801e13:	89 e5                	mov    %esp,%ebp
  801e15:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  801e18:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  801e1b:	8b 10                	mov    (%eax),%edx
  801e1d:	3b 50 04             	cmp    0x4(%eax),%edx
  801e20:	73 0a                	jae    801e2c <sprintputch+0x1a>
		*b->buf++ = ch;
  801e22:	8d 4a 01             	lea    0x1(%edx),%ecx
  801e25:	89 08                	mov    %ecx,(%eax)
  801e27:	8b 45 08             	mov    0x8(%ebp),%eax
  801e2a:	88 02                	mov    %al,(%edx)
}
  801e2c:	5d                   	pop    %ebp
  801e2d:	c3                   	ret    

00801e2e <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  801e2e:	55                   	push   %ebp
  801e2f:	89 e5                	mov    %esp,%ebp
  801e31:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  801e34:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  801e37:	50                   	push   %eax
  801e38:	ff 75 10             	pushl  0x10(%ebp)
  801e3b:	ff 75 0c             	pushl  0xc(%ebp)
  801e3e:	ff 75 08             	pushl  0x8(%ebp)
  801e41:	e8 05 00 00 00       	call   801e4b <vprintfmt>
	va_end(ap);
}
  801e46:	83 c4 10             	add    $0x10,%esp
  801e49:	c9                   	leave  
  801e4a:	c3                   	ret    

00801e4b <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  801e4b:	55                   	push   %ebp
  801e4c:	89 e5                	mov    %esp,%ebp
  801e4e:	57                   	push   %edi
  801e4f:	56                   	push   %esi
  801e50:	53                   	push   %ebx
  801e51:	83 ec 2c             	sub    $0x2c,%esp
  801e54:	8b 75 08             	mov    0x8(%ebp),%esi
  801e57:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801e5a:	8b 7d 10             	mov    0x10(%ebp),%edi
  801e5d:	eb 12                	jmp    801e71 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  801e5f:	85 c0                	test   %eax,%eax
  801e61:	0f 84 68 03 00 00    	je     8021cf <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  801e67:	83 ec 08             	sub    $0x8,%esp
  801e6a:	53                   	push   %ebx
  801e6b:	50                   	push   %eax
  801e6c:	ff d6                	call   *%esi
  801e6e:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  801e71:	47                   	inc    %edi
  801e72:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  801e76:	83 f8 25             	cmp    $0x25,%eax
  801e79:	75 e4                	jne    801e5f <vprintfmt+0x14>
  801e7b:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  801e7f:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  801e86:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  801e8d:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  801e94:	ba 00 00 00 00       	mov    $0x0,%edx
  801e99:	eb 07                	jmp    801ea2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801e9b:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  801e9e:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801ea2:	8d 47 01             	lea    0x1(%edi),%eax
  801ea5:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801ea8:	0f b6 0f             	movzbl (%edi),%ecx
  801eab:	8a 07                	mov    (%edi),%al
  801ead:	83 e8 23             	sub    $0x23,%eax
  801eb0:	3c 55                	cmp    $0x55,%al
  801eb2:	0f 87 fe 02 00 00    	ja     8021b6 <vprintfmt+0x36b>
  801eb8:	0f b6 c0             	movzbl %al,%eax
  801ebb:	ff 24 85 80 40 80 00 	jmp    *0x804080(,%eax,4)
  801ec2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  801ec5:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  801ec9:	eb d7                	jmp    801ea2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801ecb:	8b 7d e0             	mov    -0x20(%ebp),%edi
  801ece:	b8 00 00 00 00       	mov    $0x0,%eax
  801ed3:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  801ed6:	8d 04 80             	lea    (%eax,%eax,4),%eax
  801ed9:	01 c0                	add    %eax,%eax
  801edb:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  801edf:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  801ee2:	8d 51 d0             	lea    -0x30(%ecx),%edx
  801ee5:	83 fa 09             	cmp    $0x9,%edx
  801ee8:	77 34                	ja     801f1e <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  801eea:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  801eeb:	eb e9                	jmp    801ed6 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  801eed:	8b 45 14             	mov    0x14(%ebp),%eax
  801ef0:	8d 48 04             	lea    0x4(%eax),%ecx
  801ef3:	89 4d 14             	mov    %ecx,0x14(%ebp)
  801ef6:	8b 00                	mov    (%eax),%eax
  801ef8:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801efb:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  801efe:	eb 24                	jmp    801f24 <vprintfmt+0xd9>
  801f00:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  801f04:	79 07                	jns    801f0d <vprintfmt+0xc2>
  801f06:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801f0d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  801f10:	eb 90                	jmp    801ea2 <vprintfmt+0x57>
  801f12:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  801f15:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  801f1c:	eb 84                	jmp    801ea2 <vprintfmt+0x57>
  801f1e:	8b 55 e0             	mov    -0x20(%ebp),%edx
  801f21:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  801f24:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  801f28:	0f 89 74 ff ff ff    	jns    801ea2 <vprintfmt+0x57>
				width = precision, precision = -1;
  801f2e:	8b 45 d0             	mov    -0x30(%ebp),%eax
  801f31:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  801f34:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  801f3b:	e9 62 ff ff ff       	jmp    801ea2 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  801f40:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801f41:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  801f44:	e9 59 ff ff ff       	jmp    801ea2 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  801f49:	8b 45 14             	mov    0x14(%ebp),%eax
  801f4c:	8d 50 04             	lea    0x4(%eax),%edx
  801f4f:	89 55 14             	mov    %edx,0x14(%ebp)
  801f52:	83 ec 08             	sub    $0x8,%esp
  801f55:	53                   	push   %ebx
  801f56:	ff 30                	pushl  (%eax)
  801f58:	ff d6                	call   *%esi
			break;
  801f5a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801f5d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  801f60:	e9 0c ff ff ff       	jmp    801e71 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  801f65:	8b 45 14             	mov    0x14(%ebp),%eax
  801f68:	8d 50 04             	lea    0x4(%eax),%edx
  801f6b:	89 55 14             	mov    %edx,0x14(%ebp)
  801f6e:	8b 00                	mov    (%eax),%eax
  801f70:	85 c0                	test   %eax,%eax
  801f72:	79 02                	jns    801f76 <vprintfmt+0x12b>
  801f74:	f7 d8                	neg    %eax
  801f76:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  801f78:	83 f8 0f             	cmp    $0xf,%eax
  801f7b:	7f 0b                	jg     801f88 <vprintfmt+0x13d>
  801f7d:	8b 04 85 e0 41 80 00 	mov    0x8041e0(,%eax,4),%eax
  801f84:	85 c0                	test   %eax,%eax
  801f86:	75 18                	jne    801fa0 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  801f88:	52                   	push   %edx
  801f89:	68 47 3f 80 00       	push   $0x803f47
  801f8e:	53                   	push   %ebx
  801f8f:	56                   	push   %esi
  801f90:	e8 99 fe ff ff       	call   801e2e <printfmt>
  801f95:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801f98:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  801f9b:	e9 d1 fe ff ff       	jmp    801e71 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  801fa0:	50                   	push   %eax
  801fa1:	68 64 39 80 00       	push   $0x803964
  801fa6:	53                   	push   %ebx
  801fa7:	56                   	push   %esi
  801fa8:	e8 81 fe ff ff       	call   801e2e <printfmt>
  801fad:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801fb0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  801fb3:	e9 b9 fe ff ff       	jmp    801e71 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  801fb8:	8b 45 14             	mov    0x14(%ebp),%eax
  801fbb:	8d 50 04             	lea    0x4(%eax),%edx
  801fbe:	89 55 14             	mov    %edx,0x14(%ebp)
  801fc1:	8b 38                	mov    (%eax),%edi
  801fc3:	85 ff                	test   %edi,%edi
  801fc5:	75 05                	jne    801fcc <vprintfmt+0x181>
				p = "(null)";
  801fc7:	bf 40 3f 80 00       	mov    $0x803f40,%edi
			if (width > 0 && padc != '-')
  801fcc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  801fd0:	0f 8e 90 00 00 00    	jle    802066 <vprintfmt+0x21b>
  801fd6:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  801fda:	0f 84 8e 00 00 00    	je     80206e <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  801fe0:	83 ec 08             	sub    $0x8,%esp
  801fe3:	ff 75 d0             	pushl  -0x30(%ebp)
  801fe6:	57                   	push   %edi
  801fe7:	e8 70 02 00 00       	call   80225c <strnlen>
  801fec:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  801fef:	29 c1                	sub    %eax,%ecx
  801ff1:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  801ff4:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  801ff7:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  801ffb:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  801ffe:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  802001:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  802003:	eb 0d                	jmp    802012 <vprintfmt+0x1c7>
					putch(padc, putdat);
  802005:	83 ec 08             	sub    $0x8,%esp
  802008:	53                   	push   %ebx
  802009:	ff 75 e4             	pushl  -0x1c(%ebp)
  80200c:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80200e:	4f                   	dec    %edi
  80200f:	83 c4 10             	add    $0x10,%esp
  802012:	85 ff                	test   %edi,%edi
  802014:	7f ef                	jg     802005 <vprintfmt+0x1ba>
  802016:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  802019:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80201c:	89 c8                	mov    %ecx,%eax
  80201e:	85 c9                	test   %ecx,%ecx
  802020:	79 05                	jns    802027 <vprintfmt+0x1dc>
  802022:	b8 00 00 00 00       	mov    $0x0,%eax
  802027:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80202a:	29 c1                	sub    %eax,%ecx
  80202c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80202f:	89 75 08             	mov    %esi,0x8(%ebp)
  802032:	8b 75 d0             	mov    -0x30(%ebp),%esi
  802035:	eb 3d                	jmp    802074 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  802037:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80203b:	74 19                	je     802056 <vprintfmt+0x20b>
  80203d:	0f be c0             	movsbl %al,%eax
  802040:	83 e8 20             	sub    $0x20,%eax
  802043:	83 f8 5e             	cmp    $0x5e,%eax
  802046:	76 0e                	jbe    802056 <vprintfmt+0x20b>
					putch('?', putdat);
  802048:	83 ec 08             	sub    $0x8,%esp
  80204b:	53                   	push   %ebx
  80204c:	6a 3f                	push   $0x3f
  80204e:	ff 55 08             	call   *0x8(%ebp)
  802051:	83 c4 10             	add    $0x10,%esp
  802054:	eb 0b                	jmp    802061 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  802056:	83 ec 08             	sub    $0x8,%esp
  802059:	53                   	push   %ebx
  80205a:	52                   	push   %edx
  80205b:	ff 55 08             	call   *0x8(%ebp)
  80205e:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  802061:	ff 4d e4             	decl   -0x1c(%ebp)
  802064:	eb 0e                	jmp    802074 <vprintfmt+0x229>
  802066:	89 75 08             	mov    %esi,0x8(%ebp)
  802069:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80206c:	eb 06                	jmp    802074 <vprintfmt+0x229>
  80206e:	89 75 08             	mov    %esi,0x8(%ebp)
  802071:	8b 75 d0             	mov    -0x30(%ebp),%esi
  802074:	47                   	inc    %edi
  802075:	8a 47 ff             	mov    -0x1(%edi),%al
  802078:	0f be d0             	movsbl %al,%edx
  80207b:	85 d2                	test   %edx,%edx
  80207d:	74 1d                	je     80209c <vprintfmt+0x251>
  80207f:	85 f6                	test   %esi,%esi
  802081:	78 b4                	js     802037 <vprintfmt+0x1ec>
  802083:	4e                   	dec    %esi
  802084:	79 b1                	jns    802037 <vprintfmt+0x1ec>
  802086:	8b 75 08             	mov    0x8(%ebp),%esi
  802089:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80208c:	eb 14                	jmp    8020a2 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80208e:	83 ec 08             	sub    $0x8,%esp
  802091:	53                   	push   %ebx
  802092:	6a 20                	push   $0x20
  802094:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  802096:	4f                   	dec    %edi
  802097:	83 c4 10             	add    $0x10,%esp
  80209a:	eb 06                	jmp    8020a2 <vprintfmt+0x257>
  80209c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80209f:	8b 75 08             	mov    0x8(%ebp),%esi
  8020a2:	85 ff                	test   %edi,%edi
  8020a4:	7f e8                	jg     80208e <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8020a6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8020a9:	e9 c3 fd ff ff       	jmp    801e71 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8020ae:	83 fa 01             	cmp    $0x1,%edx
  8020b1:	7e 16                	jle    8020c9 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8020b3:	8b 45 14             	mov    0x14(%ebp),%eax
  8020b6:	8d 50 08             	lea    0x8(%eax),%edx
  8020b9:	89 55 14             	mov    %edx,0x14(%ebp)
  8020bc:	8b 50 04             	mov    0x4(%eax),%edx
  8020bf:	8b 00                	mov    (%eax),%eax
  8020c1:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8020c4:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8020c7:	eb 32                	jmp    8020fb <vprintfmt+0x2b0>
	else if (lflag)
  8020c9:	85 d2                	test   %edx,%edx
  8020cb:	74 18                	je     8020e5 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8020cd:	8b 45 14             	mov    0x14(%ebp),%eax
  8020d0:	8d 50 04             	lea    0x4(%eax),%edx
  8020d3:	89 55 14             	mov    %edx,0x14(%ebp)
  8020d6:	8b 00                	mov    (%eax),%eax
  8020d8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8020db:	89 c1                	mov    %eax,%ecx
  8020dd:	c1 f9 1f             	sar    $0x1f,%ecx
  8020e0:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8020e3:	eb 16                	jmp    8020fb <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8020e5:	8b 45 14             	mov    0x14(%ebp),%eax
  8020e8:	8d 50 04             	lea    0x4(%eax),%edx
  8020eb:	89 55 14             	mov    %edx,0x14(%ebp)
  8020ee:	8b 00                	mov    (%eax),%eax
  8020f0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8020f3:	89 c1                	mov    %eax,%ecx
  8020f5:	c1 f9 1f             	sar    $0x1f,%ecx
  8020f8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8020fb:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8020fe:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  802101:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  802105:	79 76                	jns    80217d <vprintfmt+0x332>
				putch('-', putdat);
  802107:	83 ec 08             	sub    $0x8,%esp
  80210a:	53                   	push   %ebx
  80210b:	6a 2d                	push   $0x2d
  80210d:	ff d6                	call   *%esi
				num = -(long long) num;
  80210f:	8b 45 d8             	mov    -0x28(%ebp),%eax
  802112:	8b 55 dc             	mov    -0x24(%ebp),%edx
  802115:	f7 d8                	neg    %eax
  802117:	83 d2 00             	adc    $0x0,%edx
  80211a:	f7 da                	neg    %edx
  80211c:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80211f:	b9 0a 00 00 00       	mov    $0xa,%ecx
  802124:	eb 5c                	jmp    802182 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  802126:	8d 45 14             	lea    0x14(%ebp),%eax
  802129:	e8 aa fc ff ff       	call   801dd8 <getuint>
			base = 10;
  80212e:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  802133:	eb 4d                	jmp    802182 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  802135:	8d 45 14             	lea    0x14(%ebp),%eax
  802138:	e8 9b fc ff ff       	call   801dd8 <getuint>
			base = 8;
  80213d:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  802142:	eb 3e                	jmp    802182 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  802144:	83 ec 08             	sub    $0x8,%esp
  802147:	53                   	push   %ebx
  802148:	6a 30                	push   $0x30
  80214a:	ff d6                	call   *%esi
			putch('x', putdat);
  80214c:	83 c4 08             	add    $0x8,%esp
  80214f:	53                   	push   %ebx
  802150:	6a 78                	push   $0x78
  802152:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  802154:	8b 45 14             	mov    0x14(%ebp),%eax
  802157:	8d 50 04             	lea    0x4(%eax),%edx
  80215a:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80215d:	8b 00                	mov    (%eax),%eax
  80215f:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  802164:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  802167:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80216c:	eb 14                	jmp    802182 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80216e:	8d 45 14             	lea    0x14(%ebp),%eax
  802171:	e8 62 fc ff ff       	call   801dd8 <getuint>
			base = 16;
  802176:	b9 10 00 00 00       	mov    $0x10,%ecx
  80217b:	eb 05                	jmp    802182 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80217d:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  802182:	83 ec 0c             	sub    $0xc,%esp
  802185:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  802189:	57                   	push   %edi
  80218a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80218d:	51                   	push   %ecx
  80218e:	52                   	push   %edx
  80218f:	50                   	push   %eax
  802190:	89 da                	mov    %ebx,%edx
  802192:	89 f0                	mov    %esi,%eax
  802194:	e8 92 fb ff ff       	call   801d2b <printnum>
			break;
  802199:	83 c4 20             	add    $0x20,%esp
  80219c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80219f:	e9 cd fc ff ff       	jmp    801e71 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8021a4:	83 ec 08             	sub    $0x8,%esp
  8021a7:	53                   	push   %ebx
  8021a8:	51                   	push   %ecx
  8021a9:	ff d6                	call   *%esi
			break;
  8021ab:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8021ae:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8021b1:	e9 bb fc ff ff       	jmp    801e71 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8021b6:	83 ec 08             	sub    $0x8,%esp
  8021b9:	53                   	push   %ebx
  8021ba:	6a 25                	push   $0x25
  8021bc:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8021be:	83 c4 10             	add    $0x10,%esp
  8021c1:	eb 01                	jmp    8021c4 <vprintfmt+0x379>
  8021c3:	4f                   	dec    %edi
  8021c4:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8021c8:	75 f9                	jne    8021c3 <vprintfmt+0x378>
  8021ca:	e9 a2 fc ff ff       	jmp    801e71 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8021cf:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8021d2:	5b                   	pop    %ebx
  8021d3:	5e                   	pop    %esi
  8021d4:	5f                   	pop    %edi
  8021d5:	5d                   	pop    %ebp
  8021d6:	c3                   	ret    

008021d7 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8021d7:	55                   	push   %ebp
  8021d8:	89 e5                	mov    %esp,%ebp
  8021da:	83 ec 18             	sub    $0x18,%esp
  8021dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8021e0:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8021e3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8021e6:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8021ea:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8021ed:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8021f4:	85 c0                	test   %eax,%eax
  8021f6:	74 26                	je     80221e <vsnprintf+0x47>
  8021f8:	85 d2                	test   %edx,%edx
  8021fa:	7e 29                	jle    802225 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8021fc:	ff 75 14             	pushl  0x14(%ebp)
  8021ff:	ff 75 10             	pushl  0x10(%ebp)
  802202:	8d 45 ec             	lea    -0x14(%ebp),%eax
  802205:	50                   	push   %eax
  802206:	68 12 1e 80 00       	push   $0x801e12
  80220b:	e8 3b fc ff ff       	call   801e4b <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  802210:	8b 45 ec             	mov    -0x14(%ebp),%eax
  802213:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  802216:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802219:	83 c4 10             	add    $0x10,%esp
  80221c:	eb 0c                	jmp    80222a <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80221e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  802223:	eb 05                	jmp    80222a <vsnprintf+0x53>
  802225:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80222a:	c9                   	leave  
  80222b:	c3                   	ret    

0080222c <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80222c:	55                   	push   %ebp
  80222d:	89 e5                	mov    %esp,%ebp
  80222f:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  802232:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  802235:	50                   	push   %eax
  802236:	ff 75 10             	pushl  0x10(%ebp)
  802239:	ff 75 0c             	pushl  0xc(%ebp)
  80223c:	ff 75 08             	pushl  0x8(%ebp)
  80223f:	e8 93 ff ff ff       	call   8021d7 <vsnprintf>
	va_end(ap);

	return rc;
}
  802244:	c9                   	leave  
  802245:	c3                   	ret    

00802246 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  802246:	55                   	push   %ebp
  802247:	89 e5                	mov    %esp,%ebp
  802249:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80224c:	b8 00 00 00 00       	mov    $0x0,%eax
  802251:	eb 01                	jmp    802254 <strlen+0xe>
		n++;
  802253:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  802254:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  802258:	75 f9                	jne    802253 <strlen+0xd>
		n++;
	return n;
}
  80225a:	5d                   	pop    %ebp
  80225b:	c3                   	ret    

0080225c <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80225c:	55                   	push   %ebp
  80225d:	89 e5                	mov    %esp,%ebp
  80225f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802262:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  802265:	ba 00 00 00 00       	mov    $0x0,%edx
  80226a:	eb 01                	jmp    80226d <strnlen+0x11>
		n++;
  80226c:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80226d:	39 c2                	cmp    %eax,%edx
  80226f:	74 08                	je     802279 <strnlen+0x1d>
  802271:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  802275:	75 f5                	jne    80226c <strnlen+0x10>
  802277:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  802279:	5d                   	pop    %ebp
  80227a:	c3                   	ret    

0080227b <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80227b:	55                   	push   %ebp
  80227c:	89 e5                	mov    %esp,%ebp
  80227e:	53                   	push   %ebx
  80227f:	8b 45 08             	mov    0x8(%ebp),%eax
  802282:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  802285:	89 c2                	mov    %eax,%edx
  802287:	42                   	inc    %edx
  802288:	41                   	inc    %ecx
  802289:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80228c:	88 5a ff             	mov    %bl,-0x1(%edx)
  80228f:	84 db                	test   %bl,%bl
  802291:	75 f4                	jne    802287 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  802293:	5b                   	pop    %ebx
  802294:	5d                   	pop    %ebp
  802295:	c3                   	ret    

00802296 <strcat>:

char *
strcat(char *dst, const char *src)
{
  802296:	55                   	push   %ebp
  802297:	89 e5                	mov    %esp,%ebp
  802299:	53                   	push   %ebx
  80229a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80229d:	53                   	push   %ebx
  80229e:	e8 a3 ff ff ff       	call   802246 <strlen>
  8022a3:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8022a6:	ff 75 0c             	pushl  0xc(%ebp)
  8022a9:	01 d8                	add    %ebx,%eax
  8022ab:	50                   	push   %eax
  8022ac:	e8 ca ff ff ff       	call   80227b <strcpy>
	return dst;
}
  8022b1:	89 d8                	mov    %ebx,%eax
  8022b3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8022b6:	c9                   	leave  
  8022b7:	c3                   	ret    

008022b8 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8022b8:	55                   	push   %ebp
  8022b9:	89 e5                	mov    %esp,%ebp
  8022bb:	56                   	push   %esi
  8022bc:	53                   	push   %ebx
  8022bd:	8b 75 08             	mov    0x8(%ebp),%esi
  8022c0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8022c3:	89 f3                	mov    %esi,%ebx
  8022c5:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8022c8:	89 f2                	mov    %esi,%edx
  8022ca:	eb 0c                	jmp    8022d8 <strncpy+0x20>
		*dst++ = *src;
  8022cc:	42                   	inc    %edx
  8022cd:	8a 01                	mov    (%ecx),%al
  8022cf:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8022d2:	80 39 01             	cmpb   $0x1,(%ecx)
  8022d5:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8022d8:	39 da                	cmp    %ebx,%edx
  8022da:	75 f0                	jne    8022cc <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8022dc:	89 f0                	mov    %esi,%eax
  8022de:	5b                   	pop    %ebx
  8022df:	5e                   	pop    %esi
  8022e0:	5d                   	pop    %ebp
  8022e1:	c3                   	ret    

008022e2 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8022e2:	55                   	push   %ebp
  8022e3:	89 e5                	mov    %esp,%ebp
  8022e5:	56                   	push   %esi
  8022e6:	53                   	push   %ebx
  8022e7:	8b 75 08             	mov    0x8(%ebp),%esi
  8022ea:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8022ed:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8022f0:	85 c0                	test   %eax,%eax
  8022f2:	74 1e                	je     802312 <strlcpy+0x30>
  8022f4:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8022f8:	89 f2                	mov    %esi,%edx
  8022fa:	eb 05                	jmp    802301 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8022fc:	42                   	inc    %edx
  8022fd:	41                   	inc    %ecx
  8022fe:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  802301:	39 c2                	cmp    %eax,%edx
  802303:	74 08                	je     80230d <strlcpy+0x2b>
  802305:	8a 19                	mov    (%ecx),%bl
  802307:	84 db                	test   %bl,%bl
  802309:	75 f1                	jne    8022fc <strlcpy+0x1a>
  80230b:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80230d:	c6 00 00             	movb   $0x0,(%eax)
  802310:	eb 02                	jmp    802314 <strlcpy+0x32>
  802312:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  802314:	29 f0                	sub    %esi,%eax
}
  802316:	5b                   	pop    %ebx
  802317:	5e                   	pop    %esi
  802318:	5d                   	pop    %ebp
  802319:	c3                   	ret    

0080231a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80231a:	55                   	push   %ebp
  80231b:	89 e5                	mov    %esp,%ebp
  80231d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802320:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  802323:	eb 02                	jmp    802327 <strcmp+0xd>
		p++, q++;
  802325:	41                   	inc    %ecx
  802326:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  802327:	8a 01                	mov    (%ecx),%al
  802329:	84 c0                	test   %al,%al
  80232b:	74 04                	je     802331 <strcmp+0x17>
  80232d:	3a 02                	cmp    (%edx),%al
  80232f:	74 f4                	je     802325 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  802331:	0f b6 c0             	movzbl %al,%eax
  802334:	0f b6 12             	movzbl (%edx),%edx
  802337:	29 d0                	sub    %edx,%eax
}
  802339:	5d                   	pop    %ebp
  80233a:	c3                   	ret    

0080233b <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80233b:	55                   	push   %ebp
  80233c:	89 e5                	mov    %esp,%ebp
  80233e:	53                   	push   %ebx
  80233f:	8b 45 08             	mov    0x8(%ebp),%eax
  802342:	8b 55 0c             	mov    0xc(%ebp),%edx
  802345:	89 c3                	mov    %eax,%ebx
  802347:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80234a:	eb 02                	jmp    80234e <strncmp+0x13>
		n--, p++, q++;
  80234c:	40                   	inc    %eax
  80234d:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80234e:	39 d8                	cmp    %ebx,%eax
  802350:	74 14                	je     802366 <strncmp+0x2b>
  802352:	8a 08                	mov    (%eax),%cl
  802354:	84 c9                	test   %cl,%cl
  802356:	74 04                	je     80235c <strncmp+0x21>
  802358:	3a 0a                	cmp    (%edx),%cl
  80235a:	74 f0                	je     80234c <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  80235c:	0f b6 00             	movzbl (%eax),%eax
  80235f:	0f b6 12             	movzbl (%edx),%edx
  802362:	29 d0                	sub    %edx,%eax
  802364:	eb 05                	jmp    80236b <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  802366:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80236b:	5b                   	pop    %ebx
  80236c:	5d                   	pop    %ebp
  80236d:	c3                   	ret    

0080236e <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80236e:	55                   	push   %ebp
  80236f:	89 e5                	mov    %esp,%ebp
  802371:	8b 45 08             	mov    0x8(%ebp),%eax
  802374:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  802377:	eb 05                	jmp    80237e <strchr+0x10>
		if (*s == c)
  802379:	38 ca                	cmp    %cl,%dl
  80237b:	74 0c                	je     802389 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80237d:	40                   	inc    %eax
  80237e:	8a 10                	mov    (%eax),%dl
  802380:	84 d2                	test   %dl,%dl
  802382:	75 f5                	jne    802379 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  802384:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802389:	5d                   	pop    %ebp
  80238a:	c3                   	ret    

0080238b <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80238b:	55                   	push   %ebp
  80238c:	89 e5                	mov    %esp,%ebp
  80238e:	8b 45 08             	mov    0x8(%ebp),%eax
  802391:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  802394:	eb 05                	jmp    80239b <strfind+0x10>
		if (*s == c)
  802396:	38 ca                	cmp    %cl,%dl
  802398:	74 07                	je     8023a1 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80239a:	40                   	inc    %eax
  80239b:	8a 10                	mov    (%eax),%dl
  80239d:	84 d2                	test   %dl,%dl
  80239f:	75 f5                	jne    802396 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8023a1:	5d                   	pop    %ebp
  8023a2:	c3                   	ret    

008023a3 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8023a3:	55                   	push   %ebp
  8023a4:	89 e5                	mov    %esp,%ebp
  8023a6:	57                   	push   %edi
  8023a7:	56                   	push   %esi
  8023a8:	53                   	push   %ebx
  8023a9:	8b 7d 08             	mov    0x8(%ebp),%edi
  8023ac:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8023af:	85 c9                	test   %ecx,%ecx
  8023b1:	74 36                	je     8023e9 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8023b3:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8023b9:	75 28                	jne    8023e3 <memset+0x40>
  8023bb:	f6 c1 03             	test   $0x3,%cl
  8023be:	75 23                	jne    8023e3 <memset+0x40>
		c &= 0xFF;
  8023c0:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8023c4:	89 d3                	mov    %edx,%ebx
  8023c6:	c1 e3 08             	shl    $0x8,%ebx
  8023c9:	89 d6                	mov    %edx,%esi
  8023cb:	c1 e6 18             	shl    $0x18,%esi
  8023ce:	89 d0                	mov    %edx,%eax
  8023d0:	c1 e0 10             	shl    $0x10,%eax
  8023d3:	09 f0                	or     %esi,%eax
  8023d5:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8023d7:	89 d8                	mov    %ebx,%eax
  8023d9:	09 d0                	or     %edx,%eax
  8023db:	c1 e9 02             	shr    $0x2,%ecx
  8023de:	fc                   	cld    
  8023df:	f3 ab                	rep stos %eax,%es:(%edi)
  8023e1:	eb 06                	jmp    8023e9 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8023e3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8023e6:	fc                   	cld    
  8023e7:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8023e9:	89 f8                	mov    %edi,%eax
  8023eb:	5b                   	pop    %ebx
  8023ec:	5e                   	pop    %esi
  8023ed:	5f                   	pop    %edi
  8023ee:	5d                   	pop    %ebp
  8023ef:	c3                   	ret    

008023f0 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8023f0:	55                   	push   %ebp
  8023f1:	89 e5                	mov    %esp,%ebp
  8023f3:	57                   	push   %edi
  8023f4:	56                   	push   %esi
  8023f5:	8b 45 08             	mov    0x8(%ebp),%eax
  8023f8:	8b 75 0c             	mov    0xc(%ebp),%esi
  8023fb:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8023fe:	39 c6                	cmp    %eax,%esi
  802400:	73 33                	jae    802435 <memmove+0x45>
  802402:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  802405:	39 d0                	cmp    %edx,%eax
  802407:	73 2c                	jae    802435 <memmove+0x45>
		s += n;
		d += n;
  802409:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80240c:	89 d6                	mov    %edx,%esi
  80240e:	09 fe                	or     %edi,%esi
  802410:	f7 c6 03 00 00 00    	test   $0x3,%esi
  802416:	75 13                	jne    80242b <memmove+0x3b>
  802418:	f6 c1 03             	test   $0x3,%cl
  80241b:	75 0e                	jne    80242b <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80241d:	83 ef 04             	sub    $0x4,%edi
  802420:	8d 72 fc             	lea    -0x4(%edx),%esi
  802423:	c1 e9 02             	shr    $0x2,%ecx
  802426:	fd                   	std    
  802427:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  802429:	eb 07                	jmp    802432 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80242b:	4f                   	dec    %edi
  80242c:	8d 72 ff             	lea    -0x1(%edx),%esi
  80242f:	fd                   	std    
  802430:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  802432:	fc                   	cld    
  802433:	eb 1d                	jmp    802452 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  802435:	89 f2                	mov    %esi,%edx
  802437:	09 c2                	or     %eax,%edx
  802439:	f6 c2 03             	test   $0x3,%dl
  80243c:	75 0f                	jne    80244d <memmove+0x5d>
  80243e:	f6 c1 03             	test   $0x3,%cl
  802441:	75 0a                	jne    80244d <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  802443:	c1 e9 02             	shr    $0x2,%ecx
  802446:	89 c7                	mov    %eax,%edi
  802448:	fc                   	cld    
  802449:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80244b:	eb 05                	jmp    802452 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  80244d:	89 c7                	mov    %eax,%edi
  80244f:	fc                   	cld    
  802450:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  802452:	5e                   	pop    %esi
  802453:	5f                   	pop    %edi
  802454:	5d                   	pop    %ebp
  802455:	c3                   	ret    

00802456 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  802456:	55                   	push   %ebp
  802457:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  802459:	ff 75 10             	pushl  0x10(%ebp)
  80245c:	ff 75 0c             	pushl  0xc(%ebp)
  80245f:	ff 75 08             	pushl  0x8(%ebp)
  802462:	e8 89 ff ff ff       	call   8023f0 <memmove>
}
  802467:	c9                   	leave  
  802468:	c3                   	ret    

00802469 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  802469:	55                   	push   %ebp
  80246a:	89 e5                	mov    %esp,%ebp
  80246c:	56                   	push   %esi
  80246d:	53                   	push   %ebx
  80246e:	8b 45 08             	mov    0x8(%ebp),%eax
  802471:	8b 55 0c             	mov    0xc(%ebp),%edx
  802474:	89 c6                	mov    %eax,%esi
  802476:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  802479:	eb 14                	jmp    80248f <memcmp+0x26>
		if (*s1 != *s2)
  80247b:	8a 08                	mov    (%eax),%cl
  80247d:	8a 1a                	mov    (%edx),%bl
  80247f:	38 d9                	cmp    %bl,%cl
  802481:	74 0a                	je     80248d <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  802483:	0f b6 c1             	movzbl %cl,%eax
  802486:	0f b6 db             	movzbl %bl,%ebx
  802489:	29 d8                	sub    %ebx,%eax
  80248b:	eb 0b                	jmp    802498 <memcmp+0x2f>
		s1++, s2++;
  80248d:	40                   	inc    %eax
  80248e:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80248f:	39 f0                	cmp    %esi,%eax
  802491:	75 e8                	jne    80247b <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  802493:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802498:	5b                   	pop    %ebx
  802499:	5e                   	pop    %esi
  80249a:	5d                   	pop    %ebp
  80249b:	c3                   	ret    

0080249c <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  80249c:	55                   	push   %ebp
  80249d:	89 e5                	mov    %esp,%ebp
  80249f:	53                   	push   %ebx
  8024a0:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8024a3:	89 c1                	mov    %eax,%ecx
  8024a5:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8024a8:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8024ac:	eb 08                	jmp    8024b6 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8024ae:	0f b6 10             	movzbl (%eax),%edx
  8024b1:	39 da                	cmp    %ebx,%edx
  8024b3:	74 05                	je     8024ba <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8024b5:	40                   	inc    %eax
  8024b6:	39 c8                	cmp    %ecx,%eax
  8024b8:	72 f4                	jb     8024ae <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8024ba:	5b                   	pop    %ebx
  8024bb:	5d                   	pop    %ebp
  8024bc:	c3                   	ret    

008024bd <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8024bd:	55                   	push   %ebp
  8024be:	89 e5                	mov    %esp,%ebp
  8024c0:	57                   	push   %edi
  8024c1:	56                   	push   %esi
  8024c2:	53                   	push   %ebx
  8024c3:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8024c6:	eb 01                	jmp    8024c9 <strtol+0xc>
		s++;
  8024c8:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8024c9:	8a 01                	mov    (%ecx),%al
  8024cb:	3c 20                	cmp    $0x20,%al
  8024cd:	74 f9                	je     8024c8 <strtol+0xb>
  8024cf:	3c 09                	cmp    $0x9,%al
  8024d1:	74 f5                	je     8024c8 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8024d3:	3c 2b                	cmp    $0x2b,%al
  8024d5:	75 08                	jne    8024df <strtol+0x22>
		s++;
  8024d7:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8024d8:	bf 00 00 00 00       	mov    $0x0,%edi
  8024dd:	eb 11                	jmp    8024f0 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8024df:	3c 2d                	cmp    $0x2d,%al
  8024e1:	75 08                	jne    8024eb <strtol+0x2e>
		s++, neg = 1;
  8024e3:	41                   	inc    %ecx
  8024e4:	bf 01 00 00 00       	mov    $0x1,%edi
  8024e9:	eb 05                	jmp    8024f0 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8024eb:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8024f0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8024f4:	0f 84 87 00 00 00    	je     802581 <strtol+0xc4>
  8024fa:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8024fe:	75 27                	jne    802527 <strtol+0x6a>
  802500:	80 39 30             	cmpb   $0x30,(%ecx)
  802503:	75 22                	jne    802527 <strtol+0x6a>
  802505:	e9 88 00 00 00       	jmp    802592 <strtol+0xd5>
		s += 2, base = 16;
  80250a:	83 c1 02             	add    $0x2,%ecx
  80250d:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  802514:	eb 11                	jmp    802527 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  802516:	41                   	inc    %ecx
  802517:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80251e:	eb 07                	jmp    802527 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  802520:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  802527:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  80252c:	8a 11                	mov    (%ecx),%dl
  80252e:	8d 5a d0             	lea    -0x30(%edx),%ebx
  802531:	80 fb 09             	cmp    $0x9,%bl
  802534:	77 08                	ja     80253e <strtol+0x81>
			dig = *s - '0';
  802536:	0f be d2             	movsbl %dl,%edx
  802539:	83 ea 30             	sub    $0x30,%edx
  80253c:	eb 22                	jmp    802560 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80253e:	8d 72 9f             	lea    -0x61(%edx),%esi
  802541:	89 f3                	mov    %esi,%ebx
  802543:	80 fb 19             	cmp    $0x19,%bl
  802546:	77 08                	ja     802550 <strtol+0x93>
			dig = *s - 'a' + 10;
  802548:	0f be d2             	movsbl %dl,%edx
  80254b:	83 ea 57             	sub    $0x57,%edx
  80254e:	eb 10                	jmp    802560 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  802550:	8d 72 bf             	lea    -0x41(%edx),%esi
  802553:	89 f3                	mov    %esi,%ebx
  802555:	80 fb 19             	cmp    $0x19,%bl
  802558:	77 14                	ja     80256e <strtol+0xb1>
			dig = *s - 'A' + 10;
  80255a:	0f be d2             	movsbl %dl,%edx
  80255d:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  802560:	3b 55 10             	cmp    0x10(%ebp),%edx
  802563:	7d 09                	jge    80256e <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  802565:	41                   	inc    %ecx
  802566:	0f af 45 10          	imul   0x10(%ebp),%eax
  80256a:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  80256c:	eb be                	jmp    80252c <strtol+0x6f>

	if (endptr)
  80256e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  802572:	74 05                	je     802579 <strtol+0xbc>
		*endptr = (char *) s;
  802574:	8b 75 0c             	mov    0xc(%ebp),%esi
  802577:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  802579:	85 ff                	test   %edi,%edi
  80257b:	74 21                	je     80259e <strtol+0xe1>
  80257d:	f7 d8                	neg    %eax
  80257f:	eb 1d                	jmp    80259e <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  802581:	80 39 30             	cmpb   $0x30,(%ecx)
  802584:	75 9a                	jne    802520 <strtol+0x63>
  802586:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  80258a:	0f 84 7a ff ff ff    	je     80250a <strtol+0x4d>
  802590:	eb 84                	jmp    802516 <strtol+0x59>
  802592:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  802596:	0f 84 6e ff ff ff    	je     80250a <strtol+0x4d>
  80259c:	eb 89                	jmp    802527 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  80259e:	5b                   	pop    %ebx
  80259f:	5e                   	pop    %esi
  8025a0:	5f                   	pop    %edi
  8025a1:	5d                   	pop    %ebp
  8025a2:	c3                   	ret    

008025a3 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8025a3:	55                   	push   %ebp
  8025a4:	89 e5                	mov    %esp,%ebp
  8025a6:	57                   	push   %edi
  8025a7:	56                   	push   %esi
  8025a8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8025a9:	b8 00 00 00 00       	mov    $0x0,%eax
  8025ae:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8025b1:	8b 55 08             	mov    0x8(%ebp),%edx
  8025b4:	89 c3                	mov    %eax,%ebx
  8025b6:	89 c7                	mov    %eax,%edi
  8025b8:	89 c6                	mov    %eax,%esi
  8025ba:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8025bc:	5b                   	pop    %ebx
  8025bd:	5e                   	pop    %esi
  8025be:	5f                   	pop    %edi
  8025bf:	5d                   	pop    %ebp
  8025c0:	c3                   	ret    

008025c1 <sys_cgetc>:

int
sys_cgetc(void)
{
  8025c1:	55                   	push   %ebp
  8025c2:	89 e5                	mov    %esp,%ebp
  8025c4:	57                   	push   %edi
  8025c5:	56                   	push   %esi
  8025c6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8025c7:	ba 00 00 00 00       	mov    $0x0,%edx
  8025cc:	b8 01 00 00 00       	mov    $0x1,%eax
  8025d1:	89 d1                	mov    %edx,%ecx
  8025d3:	89 d3                	mov    %edx,%ebx
  8025d5:	89 d7                	mov    %edx,%edi
  8025d7:	89 d6                	mov    %edx,%esi
  8025d9:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8025db:	5b                   	pop    %ebx
  8025dc:	5e                   	pop    %esi
  8025dd:	5f                   	pop    %edi
  8025de:	5d                   	pop    %ebp
  8025df:	c3                   	ret    

008025e0 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8025e0:	55                   	push   %ebp
  8025e1:	89 e5                	mov    %esp,%ebp
  8025e3:	57                   	push   %edi
  8025e4:	56                   	push   %esi
  8025e5:	53                   	push   %ebx
  8025e6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8025e9:	b9 00 00 00 00       	mov    $0x0,%ecx
  8025ee:	b8 03 00 00 00       	mov    $0x3,%eax
  8025f3:	8b 55 08             	mov    0x8(%ebp),%edx
  8025f6:	89 cb                	mov    %ecx,%ebx
  8025f8:	89 cf                	mov    %ecx,%edi
  8025fa:	89 ce                	mov    %ecx,%esi
  8025fc:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8025fe:	85 c0                	test   %eax,%eax
  802600:	7e 17                	jle    802619 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  802602:	83 ec 0c             	sub    $0xc,%esp
  802605:	50                   	push   %eax
  802606:	6a 03                	push   $0x3
  802608:	68 3f 42 80 00       	push   $0x80423f
  80260d:	6a 23                	push   $0x23
  80260f:	68 5c 42 80 00       	push   $0x80425c
  802614:	e8 26 f6 ff ff       	call   801c3f <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  802619:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80261c:	5b                   	pop    %ebx
  80261d:	5e                   	pop    %esi
  80261e:	5f                   	pop    %edi
  80261f:	5d                   	pop    %ebp
  802620:	c3                   	ret    

00802621 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  802621:	55                   	push   %ebp
  802622:	89 e5                	mov    %esp,%ebp
  802624:	57                   	push   %edi
  802625:	56                   	push   %esi
  802626:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  802627:	ba 00 00 00 00       	mov    $0x0,%edx
  80262c:	b8 02 00 00 00       	mov    $0x2,%eax
  802631:	89 d1                	mov    %edx,%ecx
  802633:	89 d3                	mov    %edx,%ebx
  802635:	89 d7                	mov    %edx,%edi
  802637:	89 d6                	mov    %edx,%esi
  802639:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80263b:	5b                   	pop    %ebx
  80263c:	5e                   	pop    %esi
  80263d:	5f                   	pop    %edi
  80263e:	5d                   	pop    %ebp
  80263f:	c3                   	ret    

00802640 <sys_yield>:

void
sys_yield(void)
{
  802640:	55                   	push   %ebp
  802641:	89 e5                	mov    %esp,%ebp
  802643:	57                   	push   %edi
  802644:	56                   	push   %esi
  802645:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  802646:	ba 00 00 00 00       	mov    $0x0,%edx
  80264b:	b8 0b 00 00 00       	mov    $0xb,%eax
  802650:	89 d1                	mov    %edx,%ecx
  802652:	89 d3                	mov    %edx,%ebx
  802654:	89 d7                	mov    %edx,%edi
  802656:	89 d6                	mov    %edx,%esi
  802658:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80265a:	5b                   	pop    %ebx
  80265b:	5e                   	pop    %esi
  80265c:	5f                   	pop    %edi
  80265d:	5d                   	pop    %ebp
  80265e:	c3                   	ret    

0080265f <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80265f:	55                   	push   %ebp
  802660:	89 e5                	mov    %esp,%ebp
  802662:	57                   	push   %edi
  802663:	56                   	push   %esi
  802664:	53                   	push   %ebx
  802665:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  802668:	be 00 00 00 00       	mov    $0x0,%esi
  80266d:	b8 04 00 00 00       	mov    $0x4,%eax
  802672:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  802675:	8b 55 08             	mov    0x8(%ebp),%edx
  802678:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80267b:	89 f7                	mov    %esi,%edi
  80267d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80267f:	85 c0                	test   %eax,%eax
  802681:	7e 17                	jle    80269a <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  802683:	83 ec 0c             	sub    $0xc,%esp
  802686:	50                   	push   %eax
  802687:	6a 04                	push   $0x4
  802689:	68 3f 42 80 00       	push   $0x80423f
  80268e:	6a 23                	push   $0x23
  802690:	68 5c 42 80 00       	push   $0x80425c
  802695:	e8 a5 f5 ff ff       	call   801c3f <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  80269a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80269d:	5b                   	pop    %ebx
  80269e:	5e                   	pop    %esi
  80269f:	5f                   	pop    %edi
  8026a0:	5d                   	pop    %ebp
  8026a1:	c3                   	ret    

008026a2 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8026a2:	55                   	push   %ebp
  8026a3:	89 e5                	mov    %esp,%ebp
  8026a5:	57                   	push   %edi
  8026a6:	56                   	push   %esi
  8026a7:	53                   	push   %ebx
  8026a8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8026ab:	b8 05 00 00 00       	mov    $0x5,%eax
  8026b0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8026b3:	8b 55 08             	mov    0x8(%ebp),%edx
  8026b6:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8026b9:	8b 7d 14             	mov    0x14(%ebp),%edi
  8026bc:	8b 75 18             	mov    0x18(%ebp),%esi
  8026bf:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8026c1:	85 c0                	test   %eax,%eax
  8026c3:	7e 17                	jle    8026dc <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8026c5:	83 ec 0c             	sub    $0xc,%esp
  8026c8:	50                   	push   %eax
  8026c9:	6a 05                	push   $0x5
  8026cb:	68 3f 42 80 00       	push   $0x80423f
  8026d0:	6a 23                	push   $0x23
  8026d2:	68 5c 42 80 00       	push   $0x80425c
  8026d7:	e8 63 f5 ff ff       	call   801c3f <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8026dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8026df:	5b                   	pop    %ebx
  8026e0:	5e                   	pop    %esi
  8026e1:	5f                   	pop    %edi
  8026e2:	5d                   	pop    %ebp
  8026e3:	c3                   	ret    

008026e4 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8026e4:	55                   	push   %ebp
  8026e5:	89 e5                	mov    %esp,%ebp
  8026e7:	57                   	push   %edi
  8026e8:	56                   	push   %esi
  8026e9:	53                   	push   %ebx
  8026ea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8026ed:	bb 00 00 00 00       	mov    $0x0,%ebx
  8026f2:	b8 06 00 00 00       	mov    $0x6,%eax
  8026f7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8026fa:	8b 55 08             	mov    0x8(%ebp),%edx
  8026fd:	89 df                	mov    %ebx,%edi
  8026ff:	89 de                	mov    %ebx,%esi
  802701:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  802703:	85 c0                	test   %eax,%eax
  802705:	7e 17                	jle    80271e <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  802707:	83 ec 0c             	sub    $0xc,%esp
  80270a:	50                   	push   %eax
  80270b:	6a 06                	push   $0x6
  80270d:	68 3f 42 80 00       	push   $0x80423f
  802712:	6a 23                	push   $0x23
  802714:	68 5c 42 80 00       	push   $0x80425c
  802719:	e8 21 f5 ff ff       	call   801c3f <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80271e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802721:	5b                   	pop    %ebx
  802722:	5e                   	pop    %esi
  802723:	5f                   	pop    %edi
  802724:	5d                   	pop    %ebp
  802725:	c3                   	ret    

00802726 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  802726:	55                   	push   %ebp
  802727:	89 e5                	mov    %esp,%ebp
  802729:	57                   	push   %edi
  80272a:	56                   	push   %esi
  80272b:	53                   	push   %ebx
  80272c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80272f:	bb 00 00 00 00       	mov    $0x0,%ebx
  802734:	b8 08 00 00 00       	mov    $0x8,%eax
  802739:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80273c:	8b 55 08             	mov    0x8(%ebp),%edx
  80273f:	89 df                	mov    %ebx,%edi
  802741:	89 de                	mov    %ebx,%esi
  802743:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  802745:	85 c0                	test   %eax,%eax
  802747:	7e 17                	jle    802760 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  802749:	83 ec 0c             	sub    $0xc,%esp
  80274c:	50                   	push   %eax
  80274d:	6a 08                	push   $0x8
  80274f:	68 3f 42 80 00       	push   $0x80423f
  802754:	6a 23                	push   $0x23
  802756:	68 5c 42 80 00       	push   $0x80425c
  80275b:	e8 df f4 ff ff       	call   801c3f <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  802760:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802763:	5b                   	pop    %ebx
  802764:	5e                   	pop    %esi
  802765:	5f                   	pop    %edi
  802766:	5d                   	pop    %ebp
  802767:	c3                   	ret    

00802768 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  802768:	55                   	push   %ebp
  802769:	89 e5                	mov    %esp,%ebp
  80276b:	57                   	push   %edi
  80276c:	56                   	push   %esi
  80276d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80276e:	ba 00 00 00 00       	mov    $0x0,%edx
  802773:	b8 0c 00 00 00       	mov    $0xc,%eax
  802778:	89 d1                	mov    %edx,%ecx
  80277a:	89 d3                	mov    %edx,%ebx
  80277c:	89 d7                	mov    %edx,%edi
  80277e:	89 d6                	mov    %edx,%esi
  802780:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  802782:	5b                   	pop    %ebx
  802783:	5e                   	pop    %esi
  802784:	5f                   	pop    %edi
  802785:	5d                   	pop    %ebp
  802786:	c3                   	ret    

00802787 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  802787:	55                   	push   %ebp
  802788:	89 e5                	mov    %esp,%ebp
  80278a:	57                   	push   %edi
  80278b:	56                   	push   %esi
  80278c:	53                   	push   %ebx
  80278d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  802790:	bb 00 00 00 00       	mov    $0x0,%ebx
  802795:	b8 09 00 00 00       	mov    $0x9,%eax
  80279a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80279d:	8b 55 08             	mov    0x8(%ebp),%edx
  8027a0:	89 df                	mov    %ebx,%edi
  8027a2:	89 de                	mov    %ebx,%esi
  8027a4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8027a6:	85 c0                	test   %eax,%eax
  8027a8:	7e 17                	jle    8027c1 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8027aa:	83 ec 0c             	sub    $0xc,%esp
  8027ad:	50                   	push   %eax
  8027ae:	6a 09                	push   $0x9
  8027b0:	68 3f 42 80 00       	push   $0x80423f
  8027b5:	6a 23                	push   $0x23
  8027b7:	68 5c 42 80 00       	push   $0x80425c
  8027bc:	e8 7e f4 ff ff       	call   801c3f <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8027c1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8027c4:	5b                   	pop    %ebx
  8027c5:	5e                   	pop    %esi
  8027c6:	5f                   	pop    %edi
  8027c7:	5d                   	pop    %ebp
  8027c8:	c3                   	ret    

008027c9 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8027c9:	55                   	push   %ebp
  8027ca:	89 e5                	mov    %esp,%ebp
  8027cc:	57                   	push   %edi
  8027cd:	56                   	push   %esi
  8027ce:	53                   	push   %ebx
  8027cf:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8027d2:	bb 00 00 00 00       	mov    $0x0,%ebx
  8027d7:	b8 0a 00 00 00       	mov    $0xa,%eax
  8027dc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8027df:	8b 55 08             	mov    0x8(%ebp),%edx
  8027e2:	89 df                	mov    %ebx,%edi
  8027e4:	89 de                	mov    %ebx,%esi
  8027e6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8027e8:	85 c0                	test   %eax,%eax
  8027ea:	7e 17                	jle    802803 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8027ec:	83 ec 0c             	sub    $0xc,%esp
  8027ef:	50                   	push   %eax
  8027f0:	6a 0a                	push   $0xa
  8027f2:	68 3f 42 80 00       	push   $0x80423f
  8027f7:	6a 23                	push   $0x23
  8027f9:	68 5c 42 80 00       	push   $0x80425c
  8027fe:	e8 3c f4 ff ff       	call   801c3f <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  802803:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802806:	5b                   	pop    %ebx
  802807:	5e                   	pop    %esi
  802808:	5f                   	pop    %edi
  802809:	5d                   	pop    %ebp
  80280a:	c3                   	ret    

0080280b <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  80280b:	55                   	push   %ebp
  80280c:	89 e5                	mov    %esp,%ebp
  80280e:	57                   	push   %edi
  80280f:	56                   	push   %esi
  802810:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  802811:	be 00 00 00 00       	mov    $0x0,%esi
  802816:	b8 0d 00 00 00       	mov    $0xd,%eax
  80281b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80281e:	8b 55 08             	mov    0x8(%ebp),%edx
  802821:	8b 5d 10             	mov    0x10(%ebp),%ebx
  802824:	8b 7d 14             	mov    0x14(%ebp),%edi
  802827:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  802829:	5b                   	pop    %ebx
  80282a:	5e                   	pop    %esi
  80282b:	5f                   	pop    %edi
  80282c:	5d                   	pop    %ebp
  80282d:	c3                   	ret    

0080282e <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  80282e:	55                   	push   %ebp
  80282f:	89 e5                	mov    %esp,%ebp
  802831:	57                   	push   %edi
  802832:	56                   	push   %esi
  802833:	53                   	push   %ebx
  802834:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  802837:	b9 00 00 00 00       	mov    $0x0,%ecx
  80283c:	b8 0e 00 00 00       	mov    $0xe,%eax
  802841:	8b 55 08             	mov    0x8(%ebp),%edx
  802844:	89 cb                	mov    %ecx,%ebx
  802846:	89 cf                	mov    %ecx,%edi
  802848:	89 ce                	mov    %ecx,%esi
  80284a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80284c:	85 c0                	test   %eax,%eax
  80284e:	7e 17                	jle    802867 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  802850:	83 ec 0c             	sub    $0xc,%esp
  802853:	50                   	push   %eax
  802854:	6a 0e                	push   $0xe
  802856:	68 3f 42 80 00       	push   $0x80423f
  80285b:	6a 23                	push   $0x23
  80285d:	68 5c 42 80 00       	push   $0x80425c
  802862:	e8 d8 f3 ff ff       	call   801c3f <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  802867:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80286a:	5b                   	pop    %ebx
  80286b:	5e                   	pop    %esi
  80286c:	5f                   	pop    %edi
  80286d:	5d                   	pop    %ebp
  80286e:	c3                   	ret    

0080286f <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  80286f:	55                   	push   %ebp
  802870:	89 e5                	mov    %esp,%ebp
  802872:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  802875:	83 3d 14 b0 82 00 00 	cmpl   $0x0,0x82b014
  80287c:	75 3e                	jne    8028bc <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  80287e:	83 ec 04             	sub    $0x4,%esp
  802881:	6a 07                	push   $0x7
  802883:	68 00 f0 bf ee       	push   $0xeebff000
  802888:	6a 00                	push   $0x0
  80288a:	e8 d0 fd ff ff       	call   80265f <sys_page_alloc>
  80288f:	83 c4 10             	add    $0x10,%esp
  802892:	85 c0                	test   %eax,%eax
  802894:	74 14                	je     8028aa <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  802896:	83 ec 04             	sub    $0x4,%esp
  802899:	68 6c 42 80 00       	push   $0x80426c
  80289e:	6a 23                	push   $0x23
  8028a0:	68 8d 42 80 00       	push   $0x80428d
  8028a5:	e8 95 f3 ff ff       	call   801c3f <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  8028aa:	83 ec 08             	sub    $0x8,%esp
  8028ad:	68 c6 28 80 00       	push   $0x8028c6
  8028b2:	6a 00                	push   $0x0
  8028b4:	e8 10 ff ff ff       	call   8027c9 <sys_env_set_pgfault_upcall>
  8028b9:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  8028bc:	8b 45 08             	mov    0x8(%ebp),%eax
  8028bf:	a3 14 b0 82 00       	mov    %eax,0x82b014
}
  8028c4:	c9                   	leave  
  8028c5:	c3                   	ret    

008028c6 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  8028c6:	54                   	push   %esp
	movl _pgfault_handler, %eax
  8028c7:	a1 14 b0 82 00       	mov    0x82b014,%eax
	call *%eax
  8028cc:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  8028ce:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  8028d1:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  8028d3:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  8028d7:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  8028db:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  8028dc:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  8028df:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  8028e1:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  8028e4:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  8028e7:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  8028e8:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8028eb:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8028ec:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8028ed:	c3                   	ret    

008028ee <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  8028ee:	55                   	push   %ebp
  8028ef:	89 e5                	mov    %esp,%ebp
  8028f1:	56                   	push   %esi
  8028f2:	53                   	push   %ebx
  8028f3:	8b 75 08             	mov    0x8(%ebp),%esi
  8028f6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8028f9:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  8028fc:	85 c0                	test   %eax,%eax
  8028fe:	75 05                	jne    802905 <ipc_recv+0x17>
        pg = (void *)UTOP;
  802900:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  802905:	83 ec 0c             	sub    $0xc,%esp
  802908:	50                   	push   %eax
  802909:	e8 20 ff ff ff       	call   80282e <sys_ipc_recv>
  80290e:	83 c4 10             	add    $0x10,%esp
  802911:	85 c0                	test   %eax,%eax
  802913:	74 16                	je     80292b <ipc_recv+0x3d>
        if (from_env_store)
  802915:	85 f6                	test   %esi,%esi
  802917:	74 06                	je     80291f <ipc_recv+0x31>
            *from_env_store = 0;
  802919:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  80291f:	85 db                	test   %ebx,%ebx
  802921:	74 2c                	je     80294f <ipc_recv+0x61>
            *perm_store = 0;
  802923:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  802929:	eb 24                	jmp    80294f <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  80292b:	85 f6                	test   %esi,%esi
  80292d:	74 0a                	je     802939 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  80292f:	a1 10 b0 82 00       	mov    0x82b010,%eax
  802934:	8b 40 74             	mov    0x74(%eax),%eax
  802937:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  802939:	85 db                	test   %ebx,%ebx
  80293b:	74 0a                	je     802947 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  80293d:	a1 10 b0 82 00       	mov    0x82b010,%eax
  802942:	8b 40 78             	mov    0x78(%eax),%eax
  802945:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  802947:	a1 10 b0 82 00       	mov    0x82b010,%eax
  80294c:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  80294f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802952:	5b                   	pop    %ebx
  802953:	5e                   	pop    %esi
  802954:	5d                   	pop    %ebp
  802955:	c3                   	ret    

00802956 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  802956:	55                   	push   %ebp
  802957:	89 e5                	mov    %esp,%ebp
  802959:	57                   	push   %edi
  80295a:	56                   	push   %esi
  80295b:	53                   	push   %ebx
  80295c:	83 ec 0c             	sub    $0xc,%esp
  80295f:	8b 75 0c             	mov    0xc(%ebp),%esi
  802962:	8b 5d 10             	mov    0x10(%ebp),%ebx
  802965:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  802968:	85 db                	test   %ebx,%ebx
  80296a:	75 0c                	jne    802978 <ipc_send+0x22>
        pg = (void *)UTOP;
  80296c:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  802971:	eb 05                	jmp    802978 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  802973:	e8 c8 fc ff ff       	call   802640 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  802978:	57                   	push   %edi
  802979:	53                   	push   %ebx
  80297a:	56                   	push   %esi
  80297b:	ff 75 08             	pushl  0x8(%ebp)
  80297e:	e8 88 fe ff ff       	call   80280b <sys_ipc_try_send>
  802983:	83 c4 10             	add    $0x10,%esp
  802986:	83 f8 f9             	cmp    $0xfffffff9,%eax
  802989:	74 e8                	je     802973 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  80298b:	85 c0                	test   %eax,%eax
  80298d:	74 14                	je     8029a3 <ipc_send+0x4d>
        panic("ipc_send: error");
  80298f:	83 ec 04             	sub    $0x4,%esp
  802992:	68 9b 42 80 00       	push   $0x80429b
  802997:	6a 6a                	push   $0x6a
  802999:	68 ab 42 80 00       	push   $0x8042ab
  80299e:	e8 9c f2 ff ff       	call   801c3f <_panic>
    }
	//panic("ipc_send not implemented");
}
  8029a3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8029a6:	5b                   	pop    %ebx
  8029a7:	5e                   	pop    %esi
  8029a8:	5f                   	pop    %edi
  8029a9:	5d                   	pop    %ebp
  8029aa:	c3                   	ret    

008029ab <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  8029ab:	55                   	push   %ebp
  8029ac:	89 e5                	mov    %esp,%ebp
  8029ae:	53                   	push   %ebx
  8029af:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  8029b2:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  8029b7:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  8029be:	89 d0                	mov    %edx,%eax
  8029c0:	c1 e0 07             	shl    $0x7,%eax
  8029c3:	29 d8                	sub    %ebx,%eax
  8029c5:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8029ca:	8b 40 50             	mov    0x50(%eax),%eax
  8029cd:	39 c8                	cmp    %ecx,%eax
  8029cf:	75 0d                	jne    8029de <ipc_find_env+0x33>
			return envs[i].env_id;
  8029d1:	c1 e2 07             	shl    $0x7,%edx
  8029d4:	29 da                	sub    %ebx,%edx
  8029d6:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  8029dc:	eb 0e                	jmp    8029ec <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  8029de:	42                   	inc    %edx
  8029df:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  8029e5:	75 d0                	jne    8029b7 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  8029e7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8029ec:	5b                   	pop    %ebx
  8029ed:	5d                   	pop    %ebp
  8029ee:	c3                   	ret    

008029ef <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8029ef:	55                   	push   %ebp
  8029f0:	89 e5                	mov    %esp,%ebp
  8029f2:	56                   	push   %esi
  8029f3:	53                   	push   %ebx
  8029f4:	89 c6                	mov    %eax,%esi
  8029f6:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8029f8:	83 3d 04 b0 82 00 00 	cmpl   $0x0,0x82b004
  8029ff:	75 12                	jne    802a13 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  802a01:	83 ec 0c             	sub    $0xc,%esp
  802a04:	6a 01                	push   $0x1
  802a06:	e8 a0 ff ff ff       	call   8029ab <ipc_find_env>
  802a0b:	a3 04 b0 82 00       	mov    %eax,0x82b004
  802a10:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  802a13:	6a 07                	push   $0x7
  802a15:	68 00 c0 82 00       	push   $0x82c000
  802a1a:	56                   	push   %esi
  802a1b:	ff 35 04 b0 82 00    	pushl  0x82b004
  802a21:	e8 30 ff ff ff       	call   802956 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  802a26:	83 c4 0c             	add    $0xc,%esp
  802a29:	6a 00                	push   $0x0
  802a2b:	53                   	push   %ebx
  802a2c:	6a 00                	push   $0x0
  802a2e:	e8 bb fe ff ff       	call   8028ee <ipc_recv>
}
  802a33:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802a36:	5b                   	pop    %ebx
  802a37:	5e                   	pop    %esi
  802a38:	5d                   	pop    %ebp
  802a39:	c3                   	ret    

00802a3a <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  802a3a:	55                   	push   %ebp
  802a3b:	89 e5                	mov    %esp,%ebp
  802a3d:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  802a40:	8b 45 08             	mov    0x8(%ebp),%eax
  802a43:	8b 40 0c             	mov    0xc(%eax),%eax
  802a46:	a3 00 c0 82 00       	mov    %eax,0x82c000
	fsipcbuf.set_size.req_size = newsize;
  802a4b:	8b 45 0c             	mov    0xc(%ebp),%eax
  802a4e:	a3 04 c0 82 00       	mov    %eax,0x82c004
	return fsipc(FSREQ_SET_SIZE, NULL);
  802a53:	ba 00 00 00 00       	mov    $0x0,%edx
  802a58:	b8 02 00 00 00       	mov    $0x2,%eax
  802a5d:	e8 8d ff ff ff       	call   8029ef <fsipc>
}
  802a62:	c9                   	leave  
  802a63:	c3                   	ret    

00802a64 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  802a64:	55                   	push   %ebp
  802a65:	89 e5                	mov    %esp,%ebp
  802a67:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  802a6a:	8b 45 08             	mov    0x8(%ebp),%eax
  802a6d:	8b 40 0c             	mov    0xc(%eax),%eax
  802a70:	a3 00 c0 82 00       	mov    %eax,0x82c000
	return fsipc(FSREQ_FLUSH, NULL);
  802a75:	ba 00 00 00 00       	mov    $0x0,%edx
  802a7a:	b8 06 00 00 00       	mov    $0x6,%eax
  802a7f:	e8 6b ff ff ff       	call   8029ef <fsipc>
}
  802a84:	c9                   	leave  
  802a85:	c3                   	ret    

00802a86 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  802a86:	55                   	push   %ebp
  802a87:	89 e5                	mov    %esp,%ebp
  802a89:	53                   	push   %ebx
  802a8a:	83 ec 04             	sub    $0x4,%esp
  802a8d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  802a90:	8b 45 08             	mov    0x8(%ebp),%eax
  802a93:	8b 40 0c             	mov    0xc(%eax),%eax
  802a96:	a3 00 c0 82 00       	mov    %eax,0x82c000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  802a9b:	ba 00 00 00 00       	mov    $0x0,%edx
  802aa0:	b8 05 00 00 00       	mov    $0x5,%eax
  802aa5:	e8 45 ff ff ff       	call   8029ef <fsipc>
  802aaa:	85 c0                	test   %eax,%eax
  802aac:	78 2c                	js     802ada <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  802aae:	83 ec 08             	sub    $0x8,%esp
  802ab1:	68 00 c0 82 00       	push   $0x82c000
  802ab6:	53                   	push   %ebx
  802ab7:	e8 bf f7 ff ff       	call   80227b <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  802abc:	a1 80 c0 82 00       	mov    0x82c080,%eax
  802ac1:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  802ac7:	a1 84 c0 82 00       	mov    0x82c084,%eax
  802acc:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  802ad2:	83 c4 10             	add    $0x10,%esp
  802ad5:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802ada:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802add:	c9                   	leave  
  802ade:	c3                   	ret    

00802adf <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  802adf:	55                   	push   %ebp
  802ae0:	89 e5                	mov    %esp,%ebp
  802ae2:	83 ec 08             	sub    $0x8,%esp
  802ae5:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  802ae8:	8b 55 08             	mov    0x8(%ebp),%edx
  802aeb:	8b 52 0c             	mov    0xc(%edx),%edx
  802aee:	89 15 00 c0 82 00    	mov    %edx,0x82c000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  802af4:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  802af9:	76 05                	jbe    802b00 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  802afb:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  802b00:	a3 04 c0 82 00       	mov    %eax,0x82c004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  802b05:	83 ec 04             	sub    $0x4,%esp
  802b08:	50                   	push   %eax
  802b09:	ff 75 0c             	pushl  0xc(%ebp)
  802b0c:	68 08 c0 82 00       	push   $0x82c008
  802b11:	e8 da f8 ff ff       	call   8023f0 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  802b16:	ba 00 00 00 00       	mov    $0x0,%edx
  802b1b:	b8 04 00 00 00       	mov    $0x4,%eax
  802b20:	e8 ca fe ff ff       	call   8029ef <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  802b25:	c9                   	leave  
  802b26:	c3                   	ret    

00802b27 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  802b27:	55                   	push   %ebp
  802b28:	89 e5                	mov    %esp,%ebp
  802b2a:	56                   	push   %esi
  802b2b:	53                   	push   %ebx
  802b2c:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  802b2f:	8b 45 08             	mov    0x8(%ebp),%eax
  802b32:	8b 40 0c             	mov    0xc(%eax),%eax
  802b35:	a3 00 c0 82 00       	mov    %eax,0x82c000
	fsipcbuf.read.req_n = n;
  802b3a:	89 35 04 c0 82 00    	mov    %esi,0x82c004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  802b40:	ba 00 00 00 00       	mov    $0x0,%edx
  802b45:	b8 03 00 00 00       	mov    $0x3,%eax
  802b4a:	e8 a0 fe ff ff       	call   8029ef <fsipc>
  802b4f:	89 c3                	mov    %eax,%ebx
  802b51:	85 c0                	test   %eax,%eax
  802b53:	78 4b                	js     802ba0 <devfile_read+0x79>
		return r;
	assert(r <= n);
  802b55:	39 c6                	cmp    %eax,%esi
  802b57:	73 16                	jae    802b6f <devfile_read+0x48>
  802b59:	68 b5 42 80 00       	push   $0x8042b5
  802b5e:	68 52 39 80 00       	push   $0x803952
  802b63:	6a 7c                	push   $0x7c
  802b65:	68 bc 42 80 00       	push   $0x8042bc
  802b6a:	e8 d0 f0 ff ff       	call   801c3f <_panic>
	assert(r <= PGSIZE);
  802b6f:	3d 00 10 00 00       	cmp    $0x1000,%eax
  802b74:	7e 16                	jle    802b8c <devfile_read+0x65>
  802b76:	68 c7 42 80 00       	push   $0x8042c7
  802b7b:	68 52 39 80 00       	push   $0x803952
  802b80:	6a 7d                	push   $0x7d
  802b82:	68 bc 42 80 00       	push   $0x8042bc
  802b87:	e8 b3 f0 ff ff       	call   801c3f <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  802b8c:	83 ec 04             	sub    $0x4,%esp
  802b8f:	50                   	push   %eax
  802b90:	68 00 c0 82 00       	push   $0x82c000
  802b95:	ff 75 0c             	pushl  0xc(%ebp)
  802b98:	e8 53 f8 ff ff       	call   8023f0 <memmove>
	return r;
  802b9d:	83 c4 10             	add    $0x10,%esp
}
  802ba0:	89 d8                	mov    %ebx,%eax
  802ba2:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802ba5:	5b                   	pop    %ebx
  802ba6:	5e                   	pop    %esi
  802ba7:	5d                   	pop    %ebp
  802ba8:	c3                   	ret    

00802ba9 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  802ba9:	55                   	push   %ebp
  802baa:	89 e5                	mov    %esp,%ebp
  802bac:	53                   	push   %ebx
  802bad:	83 ec 20             	sub    $0x20,%esp
  802bb0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  802bb3:	53                   	push   %ebx
  802bb4:	e8 8d f6 ff ff       	call   802246 <strlen>
  802bb9:	83 c4 10             	add    $0x10,%esp
  802bbc:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  802bc1:	7f 63                	jg     802c26 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  802bc3:	83 ec 0c             	sub    $0xc,%esp
  802bc6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802bc9:	50                   	push   %eax
  802bca:	e8 e2 00 00 00       	call   802cb1 <fd_alloc>
  802bcf:	83 c4 10             	add    $0x10,%esp
  802bd2:	85 c0                	test   %eax,%eax
  802bd4:	78 55                	js     802c2b <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  802bd6:	83 ec 08             	sub    $0x8,%esp
  802bd9:	53                   	push   %ebx
  802bda:	68 00 c0 82 00       	push   $0x82c000
  802bdf:	e8 97 f6 ff ff       	call   80227b <strcpy>
	fsipcbuf.open.req_omode = mode;
  802be4:	8b 45 0c             	mov    0xc(%ebp),%eax
  802be7:	a3 00 c4 82 00       	mov    %eax,0x82c400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  802bec:	8b 55 f4             	mov    -0xc(%ebp),%edx
  802bef:	b8 01 00 00 00       	mov    $0x1,%eax
  802bf4:	e8 f6 fd ff ff       	call   8029ef <fsipc>
  802bf9:	89 c3                	mov    %eax,%ebx
  802bfb:	83 c4 10             	add    $0x10,%esp
  802bfe:	85 c0                	test   %eax,%eax
  802c00:	79 14                	jns    802c16 <open+0x6d>
		fd_close(fd, 0);
  802c02:	83 ec 08             	sub    $0x8,%esp
  802c05:	6a 00                	push   $0x0
  802c07:	ff 75 f4             	pushl  -0xc(%ebp)
  802c0a:	e8 9b 01 00 00       	call   802daa <fd_close>
		return r;
  802c0f:	83 c4 10             	add    $0x10,%esp
  802c12:	89 d8                	mov    %ebx,%eax
  802c14:	eb 15                	jmp    802c2b <open+0x82>
	}

	return fd2num(fd);
  802c16:	83 ec 0c             	sub    $0xc,%esp
  802c19:	ff 75 f4             	pushl  -0xc(%ebp)
  802c1c:	e8 69 00 00 00       	call   802c8a <fd2num>
  802c21:	83 c4 10             	add    $0x10,%esp
  802c24:	eb 05                	jmp    802c2b <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  802c26:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  802c2b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802c2e:	c9                   	leave  
  802c2f:	c3                   	ret    

00802c30 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  802c30:	55                   	push   %ebp
  802c31:	89 e5                	mov    %esp,%ebp
  802c33:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  802c36:	ba 00 00 00 00       	mov    $0x0,%edx
  802c3b:	b8 08 00 00 00       	mov    $0x8,%eax
  802c40:	e8 aa fd ff ff       	call   8029ef <fsipc>
}
  802c45:	c9                   	leave  
  802c46:	c3                   	ret    

00802c47 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802c47:	55                   	push   %ebp
  802c48:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  802c4a:	8b 45 08             	mov    0x8(%ebp),%eax
  802c4d:	c1 e8 16             	shr    $0x16,%eax
  802c50:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  802c57:	a8 01                	test   $0x1,%al
  802c59:	74 21                	je     802c7c <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  802c5b:	8b 45 08             	mov    0x8(%ebp),%eax
  802c5e:	c1 e8 0c             	shr    $0xc,%eax
  802c61:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  802c68:	a8 01                	test   $0x1,%al
  802c6a:	74 17                	je     802c83 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  802c6c:	c1 e8 0c             	shr    $0xc,%eax
  802c6f:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  802c76:	ef 
  802c77:	0f b7 c0             	movzwl %ax,%eax
  802c7a:	eb 0c                	jmp    802c88 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  802c7c:	b8 00 00 00 00       	mov    $0x0,%eax
  802c81:	eb 05                	jmp    802c88 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  802c83:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  802c88:	5d                   	pop    %ebp
  802c89:	c3                   	ret    

00802c8a <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  802c8a:	55                   	push   %ebp
  802c8b:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  802c8d:	8b 45 08             	mov    0x8(%ebp),%eax
  802c90:	05 00 00 00 30       	add    $0x30000000,%eax
  802c95:	c1 e8 0c             	shr    $0xc,%eax
}
  802c98:	5d                   	pop    %ebp
  802c99:	c3                   	ret    

00802c9a <fd2data>:

char*
fd2data(struct Fd *fd)
{
  802c9a:	55                   	push   %ebp
  802c9b:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  802c9d:	8b 45 08             	mov    0x8(%ebp),%eax
  802ca0:	05 00 00 00 30       	add    $0x30000000,%eax
  802ca5:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  802caa:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  802caf:	5d                   	pop    %ebp
  802cb0:	c3                   	ret    

00802cb1 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  802cb1:	55                   	push   %ebp
  802cb2:	89 e5                	mov    %esp,%ebp
  802cb4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802cb7:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  802cbc:	89 c2                	mov    %eax,%edx
  802cbe:	c1 ea 16             	shr    $0x16,%edx
  802cc1:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  802cc8:	f6 c2 01             	test   $0x1,%dl
  802ccb:	74 11                	je     802cde <fd_alloc+0x2d>
  802ccd:	89 c2                	mov    %eax,%edx
  802ccf:	c1 ea 0c             	shr    $0xc,%edx
  802cd2:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  802cd9:	f6 c2 01             	test   $0x1,%dl
  802cdc:	75 09                	jne    802ce7 <fd_alloc+0x36>
			*fd_store = fd;
  802cde:	89 01                	mov    %eax,(%ecx)
			return 0;
  802ce0:	b8 00 00 00 00       	mov    $0x0,%eax
  802ce5:	eb 17                	jmp    802cfe <fd_alloc+0x4d>
  802ce7:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  802cec:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  802cf1:	75 c9                	jne    802cbc <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  802cf3:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  802cf9:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  802cfe:	5d                   	pop    %ebp
  802cff:	c3                   	ret    

00802d00 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  802d00:	55                   	push   %ebp
  802d01:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  802d03:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  802d07:	77 39                	ja     802d42 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  802d09:	8b 45 08             	mov    0x8(%ebp),%eax
  802d0c:	c1 e0 0c             	shl    $0xc,%eax
  802d0f:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  802d14:	89 c2                	mov    %eax,%edx
  802d16:	c1 ea 16             	shr    $0x16,%edx
  802d19:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  802d20:	f6 c2 01             	test   $0x1,%dl
  802d23:	74 24                	je     802d49 <fd_lookup+0x49>
  802d25:	89 c2                	mov    %eax,%edx
  802d27:	c1 ea 0c             	shr    $0xc,%edx
  802d2a:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  802d31:	f6 c2 01             	test   $0x1,%dl
  802d34:	74 1a                	je     802d50 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  802d36:	8b 55 0c             	mov    0xc(%ebp),%edx
  802d39:	89 02                	mov    %eax,(%edx)
	return 0;
  802d3b:	b8 00 00 00 00       	mov    $0x0,%eax
  802d40:	eb 13                	jmp    802d55 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  802d42:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  802d47:	eb 0c                	jmp    802d55 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  802d49:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  802d4e:	eb 05                	jmp    802d55 <fd_lookup+0x55>
  802d50:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  802d55:	5d                   	pop    %ebp
  802d56:	c3                   	ret    

00802d57 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  802d57:	55                   	push   %ebp
  802d58:	89 e5                	mov    %esp,%ebp
  802d5a:	83 ec 08             	sub    $0x8,%esp
  802d5d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802d60:	ba 54 43 80 00       	mov    $0x804354,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  802d65:	eb 13                	jmp    802d7a <dev_lookup+0x23>
  802d67:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  802d6a:	39 08                	cmp    %ecx,(%eax)
  802d6c:	75 0c                	jne    802d7a <dev_lookup+0x23>
			*dev = devtab[i];
  802d6e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  802d71:	89 01                	mov    %eax,(%ecx)
			return 0;
  802d73:	b8 00 00 00 00       	mov    $0x0,%eax
  802d78:	eb 2e                	jmp    802da8 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  802d7a:	8b 02                	mov    (%edx),%eax
  802d7c:	85 c0                	test   %eax,%eax
  802d7e:	75 e7                	jne    802d67 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  802d80:	a1 10 b0 82 00       	mov    0x82b010,%eax
  802d85:	8b 40 48             	mov    0x48(%eax),%eax
  802d88:	83 ec 04             	sub    $0x4,%esp
  802d8b:	51                   	push   %ecx
  802d8c:	50                   	push   %eax
  802d8d:	68 d4 42 80 00       	push   $0x8042d4
  802d92:	e8 80 ef ff ff       	call   801d17 <cprintf>
	*dev = 0;
  802d97:	8b 45 0c             	mov    0xc(%ebp),%eax
  802d9a:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  802da0:	83 c4 10             	add    $0x10,%esp
  802da3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  802da8:	c9                   	leave  
  802da9:	c3                   	ret    

00802daa <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  802daa:	55                   	push   %ebp
  802dab:	89 e5                	mov    %esp,%ebp
  802dad:	56                   	push   %esi
  802dae:	53                   	push   %ebx
  802daf:	83 ec 10             	sub    $0x10,%esp
  802db2:	8b 75 08             	mov    0x8(%ebp),%esi
  802db5:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  802db8:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802dbb:	50                   	push   %eax
  802dbc:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  802dc2:	c1 e8 0c             	shr    $0xc,%eax
  802dc5:	50                   	push   %eax
  802dc6:	e8 35 ff ff ff       	call   802d00 <fd_lookup>
  802dcb:	83 c4 08             	add    $0x8,%esp
  802dce:	85 c0                	test   %eax,%eax
  802dd0:	78 05                	js     802dd7 <fd_close+0x2d>
	    || fd != fd2)
  802dd2:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  802dd5:	74 06                	je     802ddd <fd_close+0x33>
		return (must_exist ? r : 0);
  802dd7:	84 db                	test   %bl,%bl
  802dd9:	74 47                	je     802e22 <fd_close+0x78>
  802ddb:	eb 4a                	jmp    802e27 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  802ddd:	83 ec 08             	sub    $0x8,%esp
  802de0:	8d 45 f0             	lea    -0x10(%ebp),%eax
  802de3:	50                   	push   %eax
  802de4:	ff 36                	pushl  (%esi)
  802de6:	e8 6c ff ff ff       	call   802d57 <dev_lookup>
  802deb:	89 c3                	mov    %eax,%ebx
  802ded:	83 c4 10             	add    $0x10,%esp
  802df0:	85 c0                	test   %eax,%eax
  802df2:	78 1c                	js     802e10 <fd_close+0x66>
		if (dev->dev_close)
  802df4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802df7:	8b 40 10             	mov    0x10(%eax),%eax
  802dfa:	85 c0                	test   %eax,%eax
  802dfc:	74 0d                	je     802e0b <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  802dfe:	83 ec 0c             	sub    $0xc,%esp
  802e01:	56                   	push   %esi
  802e02:	ff d0                	call   *%eax
  802e04:	89 c3                	mov    %eax,%ebx
  802e06:	83 c4 10             	add    $0x10,%esp
  802e09:	eb 05                	jmp    802e10 <fd_close+0x66>
		else
			r = 0;
  802e0b:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  802e10:	83 ec 08             	sub    $0x8,%esp
  802e13:	56                   	push   %esi
  802e14:	6a 00                	push   $0x0
  802e16:	e8 c9 f8 ff ff       	call   8026e4 <sys_page_unmap>
	return r;
  802e1b:	83 c4 10             	add    $0x10,%esp
  802e1e:	89 d8                	mov    %ebx,%eax
  802e20:	eb 05                	jmp    802e27 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  802e22:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  802e27:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802e2a:	5b                   	pop    %ebx
  802e2b:	5e                   	pop    %esi
  802e2c:	5d                   	pop    %ebp
  802e2d:	c3                   	ret    

00802e2e <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  802e2e:	55                   	push   %ebp
  802e2f:	89 e5                	mov    %esp,%ebp
  802e31:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  802e34:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802e37:	50                   	push   %eax
  802e38:	ff 75 08             	pushl  0x8(%ebp)
  802e3b:	e8 c0 fe ff ff       	call   802d00 <fd_lookup>
  802e40:	83 c4 08             	add    $0x8,%esp
  802e43:	85 c0                	test   %eax,%eax
  802e45:	78 10                	js     802e57 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  802e47:	83 ec 08             	sub    $0x8,%esp
  802e4a:	6a 01                	push   $0x1
  802e4c:	ff 75 f4             	pushl  -0xc(%ebp)
  802e4f:	e8 56 ff ff ff       	call   802daa <fd_close>
  802e54:	83 c4 10             	add    $0x10,%esp
}
  802e57:	c9                   	leave  
  802e58:	c3                   	ret    

00802e59 <close_all>:

void
close_all(void)
{
  802e59:	55                   	push   %ebp
  802e5a:	89 e5                	mov    %esp,%ebp
  802e5c:	53                   	push   %ebx
  802e5d:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  802e60:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  802e65:	83 ec 0c             	sub    $0xc,%esp
  802e68:	53                   	push   %ebx
  802e69:	e8 c0 ff ff ff       	call   802e2e <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  802e6e:	43                   	inc    %ebx
  802e6f:	83 c4 10             	add    $0x10,%esp
  802e72:	83 fb 20             	cmp    $0x20,%ebx
  802e75:	75 ee                	jne    802e65 <close_all+0xc>
		close(i);
}
  802e77:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802e7a:	c9                   	leave  
  802e7b:	c3                   	ret    

00802e7c <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  802e7c:	55                   	push   %ebp
  802e7d:	89 e5                	mov    %esp,%ebp
  802e7f:	57                   	push   %edi
  802e80:	56                   	push   %esi
  802e81:	53                   	push   %ebx
  802e82:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  802e85:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  802e88:	50                   	push   %eax
  802e89:	ff 75 08             	pushl  0x8(%ebp)
  802e8c:	e8 6f fe ff ff       	call   802d00 <fd_lookup>
  802e91:	83 c4 08             	add    $0x8,%esp
  802e94:	85 c0                	test   %eax,%eax
  802e96:	0f 88 c2 00 00 00    	js     802f5e <dup+0xe2>
		return r;
	close(newfdnum);
  802e9c:	83 ec 0c             	sub    $0xc,%esp
  802e9f:	ff 75 0c             	pushl  0xc(%ebp)
  802ea2:	e8 87 ff ff ff       	call   802e2e <close>

	newfd = INDEX2FD(newfdnum);
  802ea7:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  802eaa:	c1 e3 0c             	shl    $0xc,%ebx
  802ead:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  802eb3:	83 c4 04             	add    $0x4,%esp
  802eb6:	ff 75 e4             	pushl  -0x1c(%ebp)
  802eb9:	e8 dc fd ff ff       	call   802c9a <fd2data>
  802ebe:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  802ec0:	89 1c 24             	mov    %ebx,(%esp)
  802ec3:	e8 d2 fd ff ff       	call   802c9a <fd2data>
  802ec8:	83 c4 10             	add    $0x10,%esp
  802ecb:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  802ecd:	89 f0                	mov    %esi,%eax
  802ecf:	c1 e8 16             	shr    $0x16,%eax
  802ed2:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  802ed9:	a8 01                	test   $0x1,%al
  802edb:	74 35                	je     802f12 <dup+0x96>
  802edd:	89 f0                	mov    %esi,%eax
  802edf:	c1 e8 0c             	shr    $0xc,%eax
  802ee2:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  802ee9:	f6 c2 01             	test   $0x1,%dl
  802eec:	74 24                	je     802f12 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  802eee:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  802ef5:	83 ec 0c             	sub    $0xc,%esp
  802ef8:	25 07 0e 00 00       	and    $0xe07,%eax
  802efd:	50                   	push   %eax
  802efe:	57                   	push   %edi
  802eff:	6a 00                	push   $0x0
  802f01:	56                   	push   %esi
  802f02:	6a 00                	push   $0x0
  802f04:	e8 99 f7 ff ff       	call   8026a2 <sys_page_map>
  802f09:	89 c6                	mov    %eax,%esi
  802f0b:	83 c4 20             	add    $0x20,%esp
  802f0e:	85 c0                	test   %eax,%eax
  802f10:	78 2c                	js     802f3e <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  802f12:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  802f15:	89 d0                	mov    %edx,%eax
  802f17:	c1 e8 0c             	shr    $0xc,%eax
  802f1a:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  802f21:	83 ec 0c             	sub    $0xc,%esp
  802f24:	25 07 0e 00 00       	and    $0xe07,%eax
  802f29:	50                   	push   %eax
  802f2a:	53                   	push   %ebx
  802f2b:	6a 00                	push   $0x0
  802f2d:	52                   	push   %edx
  802f2e:	6a 00                	push   $0x0
  802f30:	e8 6d f7 ff ff       	call   8026a2 <sys_page_map>
  802f35:	89 c6                	mov    %eax,%esi
  802f37:	83 c4 20             	add    $0x20,%esp
  802f3a:	85 c0                	test   %eax,%eax
  802f3c:	79 1d                	jns    802f5b <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  802f3e:	83 ec 08             	sub    $0x8,%esp
  802f41:	53                   	push   %ebx
  802f42:	6a 00                	push   $0x0
  802f44:	e8 9b f7 ff ff       	call   8026e4 <sys_page_unmap>
	sys_page_unmap(0, nva);
  802f49:	83 c4 08             	add    $0x8,%esp
  802f4c:	57                   	push   %edi
  802f4d:	6a 00                	push   $0x0
  802f4f:	e8 90 f7 ff ff       	call   8026e4 <sys_page_unmap>
	return r;
  802f54:	83 c4 10             	add    $0x10,%esp
  802f57:	89 f0                	mov    %esi,%eax
  802f59:	eb 03                	jmp    802f5e <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  802f5b:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  802f5e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802f61:	5b                   	pop    %ebx
  802f62:	5e                   	pop    %esi
  802f63:	5f                   	pop    %edi
  802f64:	5d                   	pop    %ebp
  802f65:	c3                   	ret    

00802f66 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  802f66:	55                   	push   %ebp
  802f67:	89 e5                	mov    %esp,%ebp
  802f69:	53                   	push   %ebx
  802f6a:	83 ec 14             	sub    $0x14,%esp
  802f6d:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  802f70:	8d 45 f0             	lea    -0x10(%ebp),%eax
  802f73:	50                   	push   %eax
  802f74:	53                   	push   %ebx
  802f75:	e8 86 fd ff ff       	call   802d00 <fd_lookup>
  802f7a:	83 c4 08             	add    $0x8,%esp
  802f7d:	85 c0                	test   %eax,%eax
  802f7f:	78 67                	js     802fe8 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  802f81:	83 ec 08             	sub    $0x8,%esp
  802f84:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802f87:	50                   	push   %eax
  802f88:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802f8b:	ff 30                	pushl  (%eax)
  802f8d:	e8 c5 fd ff ff       	call   802d57 <dev_lookup>
  802f92:	83 c4 10             	add    $0x10,%esp
  802f95:	85 c0                	test   %eax,%eax
  802f97:	78 4f                	js     802fe8 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  802f99:	8b 55 f0             	mov    -0x10(%ebp),%edx
  802f9c:	8b 42 08             	mov    0x8(%edx),%eax
  802f9f:	83 e0 03             	and    $0x3,%eax
  802fa2:	83 f8 01             	cmp    $0x1,%eax
  802fa5:	75 21                	jne    802fc8 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  802fa7:	a1 10 b0 82 00       	mov    0x82b010,%eax
  802fac:	8b 40 48             	mov    0x48(%eax),%eax
  802faf:	83 ec 04             	sub    $0x4,%esp
  802fb2:	53                   	push   %ebx
  802fb3:	50                   	push   %eax
  802fb4:	68 18 43 80 00       	push   $0x804318
  802fb9:	e8 59 ed ff ff       	call   801d17 <cprintf>
		return -E_INVAL;
  802fbe:	83 c4 10             	add    $0x10,%esp
  802fc1:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  802fc6:	eb 20                	jmp    802fe8 <read+0x82>
	}
	if (!dev->dev_read)
  802fc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802fcb:	8b 40 08             	mov    0x8(%eax),%eax
  802fce:	85 c0                	test   %eax,%eax
  802fd0:	74 11                	je     802fe3 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  802fd2:	83 ec 04             	sub    $0x4,%esp
  802fd5:	ff 75 10             	pushl  0x10(%ebp)
  802fd8:	ff 75 0c             	pushl  0xc(%ebp)
  802fdb:	52                   	push   %edx
  802fdc:	ff d0                	call   *%eax
  802fde:	83 c4 10             	add    $0x10,%esp
  802fe1:	eb 05                	jmp    802fe8 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  802fe3:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  802fe8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802feb:	c9                   	leave  
  802fec:	c3                   	ret    

00802fed <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  802fed:	55                   	push   %ebp
  802fee:	89 e5                	mov    %esp,%ebp
  802ff0:	57                   	push   %edi
  802ff1:	56                   	push   %esi
  802ff2:	53                   	push   %ebx
  802ff3:	83 ec 0c             	sub    $0xc,%esp
  802ff6:	8b 7d 08             	mov    0x8(%ebp),%edi
  802ff9:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  802ffc:	bb 00 00 00 00       	mov    $0x0,%ebx
  803001:	eb 21                	jmp    803024 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  803003:	83 ec 04             	sub    $0x4,%esp
  803006:	89 f0                	mov    %esi,%eax
  803008:	29 d8                	sub    %ebx,%eax
  80300a:	50                   	push   %eax
  80300b:	89 d8                	mov    %ebx,%eax
  80300d:	03 45 0c             	add    0xc(%ebp),%eax
  803010:	50                   	push   %eax
  803011:	57                   	push   %edi
  803012:	e8 4f ff ff ff       	call   802f66 <read>
		if (m < 0)
  803017:	83 c4 10             	add    $0x10,%esp
  80301a:	85 c0                	test   %eax,%eax
  80301c:	78 10                	js     80302e <readn+0x41>
			return m;
		if (m == 0)
  80301e:	85 c0                	test   %eax,%eax
  803020:	74 0a                	je     80302c <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  803022:	01 c3                	add    %eax,%ebx
  803024:	39 f3                	cmp    %esi,%ebx
  803026:	72 db                	jb     803003 <readn+0x16>
  803028:	89 d8                	mov    %ebx,%eax
  80302a:	eb 02                	jmp    80302e <readn+0x41>
  80302c:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  80302e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  803031:	5b                   	pop    %ebx
  803032:	5e                   	pop    %esi
  803033:	5f                   	pop    %edi
  803034:	5d                   	pop    %ebp
  803035:	c3                   	ret    

00803036 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  803036:	55                   	push   %ebp
  803037:	89 e5                	mov    %esp,%ebp
  803039:	53                   	push   %ebx
  80303a:	83 ec 14             	sub    $0x14,%esp
  80303d:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  803040:	8d 45 f0             	lea    -0x10(%ebp),%eax
  803043:	50                   	push   %eax
  803044:	53                   	push   %ebx
  803045:	e8 b6 fc ff ff       	call   802d00 <fd_lookup>
  80304a:	83 c4 08             	add    $0x8,%esp
  80304d:	85 c0                	test   %eax,%eax
  80304f:	78 62                	js     8030b3 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  803051:	83 ec 08             	sub    $0x8,%esp
  803054:	8d 45 f4             	lea    -0xc(%ebp),%eax
  803057:	50                   	push   %eax
  803058:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80305b:	ff 30                	pushl  (%eax)
  80305d:	e8 f5 fc ff ff       	call   802d57 <dev_lookup>
  803062:	83 c4 10             	add    $0x10,%esp
  803065:	85 c0                	test   %eax,%eax
  803067:	78 4a                	js     8030b3 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  803069:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80306c:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  803070:	75 21                	jne    803093 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  803072:	a1 10 b0 82 00       	mov    0x82b010,%eax
  803077:	8b 40 48             	mov    0x48(%eax),%eax
  80307a:	83 ec 04             	sub    $0x4,%esp
  80307d:	53                   	push   %ebx
  80307e:	50                   	push   %eax
  80307f:	68 34 43 80 00       	push   $0x804334
  803084:	e8 8e ec ff ff       	call   801d17 <cprintf>
		return -E_INVAL;
  803089:	83 c4 10             	add    $0x10,%esp
  80308c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  803091:	eb 20                	jmp    8030b3 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  803093:	8b 55 f4             	mov    -0xc(%ebp),%edx
  803096:	8b 52 0c             	mov    0xc(%edx),%edx
  803099:	85 d2                	test   %edx,%edx
  80309b:	74 11                	je     8030ae <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  80309d:	83 ec 04             	sub    $0x4,%esp
  8030a0:	ff 75 10             	pushl  0x10(%ebp)
  8030a3:	ff 75 0c             	pushl  0xc(%ebp)
  8030a6:	50                   	push   %eax
  8030a7:	ff d2                	call   *%edx
  8030a9:	83 c4 10             	add    $0x10,%esp
  8030ac:	eb 05                	jmp    8030b3 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8030ae:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8030b3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8030b6:	c9                   	leave  
  8030b7:	c3                   	ret    

008030b8 <seek>:

int
seek(int fdnum, off_t offset)
{
  8030b8:	55                   	push   %ebp
  8030b9:	89 e5                	mov    %esp,%ebp
  8030bb:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8030be:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8030c1:	50                   	push   %eax
  8030c2:	ff 75 08             	pushl  0x8(%ebp)
  8030c5:	e8 36 fc ff ff       	call   802d00 <fd_lookup>
  8030ca:	83 c4 08             	add    $0x8,%esp
  8030cd:	85 c0                	test   %eax,%eax
  8030cf:	78 0e                	js     8030df <seek+0x27>
		return r;
	fd->fd_offset = offset;
  8030d1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8030d4:	8b 55 0c             	mov    0xc(%ebp),%edx
  8030d7:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  8030da:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8030df:	c9                   	leave  
  8030e0:	c3                   	ret    

008030e1 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  8030e1:	55                   	push   %ebp
  8030e2:	89 e5                	mov    %esp,%ebp
  8030e4:	53                   	push   %ebx
  8030e5:	83 ec 14             	sub    $0x14,%esp
  8030e8:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  8030eb:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8030ee:	50                   	push   %eax
  8030ef:	53                   	push   %ebx
  8030f0:	e8 0b fc ff ff       	call   802d00 <fd_lookup>
  8030f5:	83 c4 08             	add    $0x8,%esp
  8030f8:	85 c0                	test   %eax,%eax
  8030fa:	78 5f                	js     80315b <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8030fc:	83 ec 08             	sub    $0x8,%esp
  8030ff:	8d 45 f4             	lea    -0xc(%ebp),%eax
  803102:	50                   	push   %eax
  803103:	8b 45 f0             	mov    -0x10(%ebp),%eax
  803106:	ff 30                	pushl  (%eax)
  803108:	e8 4a fc ff ff       	call   802d57 <dev_lookup>
  80310d:	83 c4 10             	add    $0x10,%esp
  803110:	85 c0                	test   %eax,%eax
  803112:	78 47                	js     80315b <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  803114:	8b 45 f0             	mov    -0x10(%ebp),%eax
  803117:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80311b:	75 21                	jne    80313e <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  80311d:	a1 10 b0 82 00       	mov    0x82b010,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  803122:	8b 40 48             	mov    0x48(%eax),%eax
  803125:	83 ec 04             	sub    $0x4,%esp
  803128:	53                   	push   %ebx
  803129:	50                   	push   %eax
  80312a:	68 f4 42 80 00       	push   $0x8042f4
  80312f:	e8 e3 eb ff ff       	call   801d17 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  803134:	83 c4 10             	add    $0x10,%esp
  803137:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80313c:	eb 1d                	jmp    80315b <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  80313e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  803141:	8b 52 18             	mov    0x18(%edx),%edx
  803144:	85 d2                	test   %edx,%edx
  803146:	74 0e                	je     803156 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  803148:	83 ec 08             	sub    $0x8,%esp
  80314b:	ff 75 0c             	pushl  0xc(%ebp)
  80314e:	50                   	push   %eax
  80314f:	ff d2                	call   *%edx
  803151:	83 c4 10             	add    $0x10,%esp
  803154:	eb 05                	jmp    80315b <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  803156:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  80315b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80315e:	c9                   	leave  
  80315f:	c3                   	ret    

00803160 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  803160:	55                   	push   %ebp
  803161:	89 e5                	mov    %esp,%ebp
  803163:	53                   	push   %ebx
  803164:	83 ec 14             	sub    $0x14,%esp
  803167:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80316a:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80316d:	50                   	push   %eax
  80316e:	ff 75 08             	pushl  0x8(%ebp)
  803171:	e8 8a fb ff ff       	call   802d00 <fd_lookup>
  803176:	83 c4 08             	add    $0x8,%esp
  803179:	85 c0                	test   %eax,%eax
  80317b:	78 52                	js     8031cf <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80317d:	83 ec 08             	sub    $0x8,%esp
  803180:	8d 45 f4             	lea    -0xc(%ebp),%eax
  803183:	50                   	push   %eax
  803184:	8b 45 f0             	mov    -0x10(%ebp),%eax
  803187:	ff 30                	pushl  (%eax)
  803189:	e8 c9 fb ff ff       	call   802d57 <dev_lookup>
  80318e:	83 c4 10             	add    $0x10,%esp
  803191:	85 c0                	test   %eax,%eax
  803193:	78 3a                	js     8031cf <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  803195:	8b 45 f4             	mov    -0xc(%ebp),%eax
  803198:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  80319c:	74 2c                	je     8031ca <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  80319e:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8031a1:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8031a8:	00 00 00 
	stat->st_isdir = 0;
  8031ab:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8031b2:	00 00 00 
	stat->st_dev = dev;
  8031b5:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8031bb:	83 ec 08             	sub    $0x8,%esp
  8031be:	53                   	push   %ebx
  8031bf:	ff 75 f0             	pushl  -0x10(%ebp)
  8031c2:	ff 50 14             	call   *0x14(%eax)
  8031c5:	83 c4 10             	add    $0x10,%esp
  8031c8:	eb 05                	jmp    8031cf <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8031ca:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8031cf:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8031d2:	c9                   	leave  
  8031d3:	c3                   	ret    

008031d4 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8031d4:	55                   	push   %ebp
  8031d5:	89 e5                	mov    %esp,%ebp
  8031d7:	56                   	push   %esi
  8031d8:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8031d9:	83 ec 08             	sub    $0x8,%esp
  8031dc:	6a 00                	push   $0x0
  8031de:	ff 75 08             	pushl  0x8(%ebp)
  8031e1:	e8 c3 f9 ff ff       	call   802ba9 <open>
  8031e6:	89 c3                	mov    %eax,%ebx
  8031e8:	83 c4 10             	add    $0x10,%esp
  8031eb:	85 c0                	test   %eax,%eax
  8031ed:	78 1d                	js     80320c <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  8031ef:	83 ec 08             	sub    $0x8,%esp
  8031f2:	ff 75 0c             	pushl  0xc(%ebp)
  8031f5:	50                   	push   %eax
  8031f6:	e8 65 ff ff ff       	call   803160 <fstat>
  8031fb:	89 c6                	mov    %eax,%esi
	close(fd);
  8031fd:	89 1c 24             	mov    %ebx,(%esp)
  803200:	e8 29 fc ff ff       	call   802e2e <close>
	return r;
  803205:	83 c4 10             	add    $0x10,%esp
  803208:	89 f0                	mov    %esi,%eax
  80320a:	eb 00                	jmp    80320c <stat+0x38>
}
  80320c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80320f:	5b                   	pop    %ebx
  803210:	5e                   	pop    %esi
  803211:	5d                   	pop    %ebp
  803212:	c3                   	ret    

00803213 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  803213:	55                   	push   %ebp
  803214:	89 e5                	mov    %esp,%ebp
  803216:	56                   	push   %esi
  803217:	53                   	push   %ebx
  803218:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  80321b:	83 ec 0c             	sub    $0xc,%esp
  80321e:	ff 75 08             	pushl  0x8(%ebp)
  803221:	e8 74 fa ff ff       	call   802c9a <fd2data>
  803226:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  803228:	83 c4 08             	add    $0x8,%esp
  80322b:	68 64 43 80 00       	push   $0x804364
  803230:	53                   	push   %ebx
  803231:	e8 45 f0 ff ff       	call   80227b <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  803236:	8b 46 04             	mov    0x4(%esi),%eax
  803239:	2b 06                	sub    (%esi),%eax
  80323b:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  803241:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  803248:	00 00 00 
	stat->st_dev = &devpipe;
  80324b:	c7 83 88 00 00 00 60 	movl   $0x809060,0x88(%ebx)
  803252:	90 80 00 
	return 0;
}
  803255:	b8 00 00 00 00       	mov    $0x0,%eax
  80325a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80325d:	5b                   	pop    %ebx
  80325e:	5e                   	pop    %esi
  80325f:	5d                   	pop    %ebp
  803260:	c3                   	ret    

00803261 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  803261:	55                   	push   %ebp
  803262:	89 e5                	mov    %esp,%ebp
  803264:	53                   	push   %ebx
  803265:	83 ec 0c             	sub    $0xc,%esp
  803268:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  80326b:	53                   	push   %ebx
  80326c:	6a 00                	push   $0x0
  80326e:	e8 71 f4 ff ff       	call   8026e4 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  803273:	89 1c 24             	mov    %ebx,(%esp)
  803276:	e8 1f fa ff ff       	call   802c9a <fd2data>
  80327b:	83 c4 08             	add    $0x8,%esp
  80327e:	50                   	push   %eax
  80327f:	6a 00                	push   $0x0
  803281:	e8 5e f4 ff ff       	call   8026e4 <sys_page_unmap>
}
  803286:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  803289:	c9                   	leave  
  80328a:	c3                   	ret    

0080328b <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  80328b:	55                   	push   %ebp
  80328c:	89 e5                	mov    %esp,%ebp
  80328e:	57                   	push   %edi
  80328f:	56                   	push   %esi
  803290:	53                   	push   %ebx
  803291:	83 ec 1c             	sub    $0x1c,%esp
  803294:	89 45 e0             	mov    %eax,-0x20(%ebp)
  803297:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  803299:	a1 10 b0 82 00       	mov    0x82b010,%eax
  80329e:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  8032a1:	83 ec 0c             	sub    $0xc,%esp
  8032a4:	ff 75 e0             	pushl  -0x20(%ebp)
  8032a7:	e8 9b f9 ff ff       	call   802c47 <pageref>
  8032ac:	89 c3                	mov    %eax,%ebx
  8032ae:	89 3c 24             	mov    %edi,(%esp)
  8032b1:	e8 91 f9 ff ff       	call   802c47 <pageref>
  8032b6:	83 c4 10             	add    $0x10,%esp
  8032b9:	39 c3                	cmp    %eax,%ebx
  8032bb:	0f 94 c1             	sete   %cl
  8032be:	0f b6 c9             	movzbl %cl,%ecx
  8032c1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  8032c4:	8b 15 10 b0 82 00    	mov    0x82b010,%edx
  8032ca:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  8032cd:	39 ce                	cmp    %ecx,%esi
  8032cf:	74 1b                	je     8032ec <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  8032d1:	39 c3                	cmp    %eax,%ebx
  8032d3:	75 c4                	jne    803299 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  8032d5:	8b 42 58             	mov    0x58(%edx),%eax
  8032d8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8032db:	50                   	push   %eax
  8032dc:	56                   	push   %esi
  8032dd:	68 6b 43 80 00       	push   $0x80436b
  8032e2:	e8 30 ea ff ff       	call   801d17 <cprintf>
  8032e7:	83 c4 10             	add    $0x10,%esp
  8032ea:	eb ad                	jmp    803299 <_pipeisclosed+0xe>
	}
}
  8032ec:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8032ef:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8032f2:	5b                   	pop    %ebx
  8032f3:	5e                   	pop    %esi
  8032f4:	5f                   	pop    %edi
  8032f5:	5d                   	pop    %ebp
  8032f6:	c3                   	ret    

008032f7 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8032f7:	55                   	push   %ebp
  8032f8:	89 e5                	mov    %esp,%ebp
  8032fa:	57                   	push   %edi
  8032fb:	56                   	push   %esi
  8032fc:	53                   	push   %ebx
  8032fd:	83 ec 18             	sub    $0x18,%esp
  803300:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  803303:	56                   	push   %esi
  803304:	e8 91 f9 ff ff       	call   802c9a <fd2data>
  803309:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80330b:	83 c4 10             	add    $0x10,%esp
  80330e:	bf 00 00 00 00       	mov    $0x0,%edi
  803313:	eb 3b                	jmp    803350 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  803315:	89 da                	mov    %ebx,%edx
  803317:	89 f0                	mov    %esi,%eax
  803319:	e8 6d ff ff ff       	call   80328b <_pipeisclosed>
  80331e:	85 c0                	test   %eax,%eax
  803320:	75 38                	jne    80335a <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  803322:	e8 19 f3 ff ff       	call   802640 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  803327:	8b 53 04             	mov    0x4(%ebx),%edx
  80332a:	8b 03                	mov    (%ebx),%eax
  80332c:	83 c0 20             	add    $0x20,%eax
  80332f:	39 c2                	cmp    %eax,%edx
  803331:	73 e2                	jae    803315 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  803333:	8b 45 0c             	mov    0xc(%ebp),%eax
  803336:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  803339:	89 d0                	mov    %edx,%eax
  80333b:	25 1f 00 00 80       	and    $0x8000001f,%eax
  803340:	79 05                	jns    803347 <devpipe_write+0x50>
  803342:	48                   	dec    %eax
  803343:	83 c8 e0             	or     $0xffffffe0,%eax
  803346:	40                   	inc    %eax
  803347:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  80334b:	42                   	inc    %edx
  80334c:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80334f:	47                   	inc    %edi
  803350:	3b 7d 10             	cmp    0x10(%ebp),%edi
  803353:	75 d2                	jne    803327 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  803355:	8b 45 10             	mov    0x10(%ebp),%eax
  803358:	eb 05                	jmp    80335f <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  80335a:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  80335f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  803362:	5b                   	pop    %ebx
  803363:	5e                   	pop    %esi
  803364:	5f                   	pop    %edi
  803365:	5d                   	pop    %ebp
  803366:	c3                   	ret    

00803367 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  803367:	55                   	push   %ebp
  803368:	89 e5                	mov    %esp,%ebp
  80336a:	57                   	push   %edi
  80336b:	56                   	push   %esi
  80336c:	53                   	push   %ebx
  80336d:	83 ec 18             	sub    $0x18,%esp
  803370:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  803373:	57                   	push   %edi
  803374:	e8 21 f9 ff ff       	call   802c9a <fd2data>
  803379:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80337b:	83 c4 10             	add    $0x10,%esp
  80337e:	bb 00 00 00 00       	mov    $0x0,%ebx
  803383:	eb 3a                	jmp    8033bf <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  803385:	85 db                	test   %ebx,%ebx
  803387:	74 04                	je     80338d <devpipe_read+0x26>
				return i;
  803389:	89 d8                	mov    %ebx,%eax
  80338b:	eb 41                	jmp    8033ce <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  80338d:	89 f2                	mov    %esi,%edx
  80338f:	89 f8                	mov    %edi,%eax
  803391:	e8 f5 fe ff ff       	call   80328b <_pipeisclosed>
  803396:	85 c0                	test   %eax,%eax
  803398:	75 2f                	jne    8033c9 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  80339a:	e8 a1 f2 ff ff       	call   802640 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  80339f:	8b 06                	mov    (%esi),%eax
  8033a1:	3b 46 04             	cmp    0x4(%esi),%eax
  8033a4:	74 df                	je     803385 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  8033a6:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8033ab:	79 05                	jns    8033b2 <devpipe_read+0x4b>
  8033ad:	48                   	dec    %eax
  8033ae:	83 c8 e0             	or     $0xffffffe0,%eax
  8033b1:	40                   	inc    %eax
  8033b2:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  8033b6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8033b9:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  8033bc:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8033be:	43                   	inc    %ebx
  8033bf:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  8033c2:	75 db                	jne    80339f <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  8033c4:	8b 45 10             	mov    0x10(%ebp),%eax
  8033c7:	eb 05                	jmp    8033ce <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8033c9:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  8033ce:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8033d1:	5b                   	pop    %ebx
  8033d2:	5e                   	pop    %esi
  8033d3:	5f                   	pop    %edi
  8033d4:	5d                   	pop    %ebp
  8033d5:	c3                   	ret    

008033d6 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  8033d6:	55                   	push   %ebp
  8033d7:	89 e5                	mov    %esp,%ebp
  8033d9:	56                   	push   %esi
  8033da:	53                   	push   %ebx
  8033db:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  8033de:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8033e1:	50                   	push   %eax
  8033e2:	e8 ca f8 ff ff       	call   802cb1 <fd_alloc>
  8033e7:	83 c4 10             	add    $0x10,%esp
  8033ea:	85 c0                	test   %eax,%eax
  8033ec:	0f 88 2a 01 00 00    	js     80351c <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8033f2:	83 ec 04             	sub    $0x4,%esp
  8033f5:	68 07 04 00 00       	push   $0x407
  8033fa:	ff 75 f4             	pushl  -0xc(%ebp)
  8033fd:	6a 00                	push   $0x0
  8033ff:	e8 5b f2 ff ff       	call   80265f <sys_page_alloc>
  803404:	83 c4 10             	add    $0x10,%esp
  803407:	85 c0                	test   %eax,%eax
  803409:	0f 88 0d 01 00 00    	js     80351c <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  80340f:	83 ec 0c             	sub    $0xc,%esp
  803412:	8d 45 f0             	lea    -0x10(%ebp),%eax
  803415:	50                   	push   %eax
  803416:	e8 96 f8 ff ff       	call   802cb1 <fd_alloc>
  80341b:	89 c3                	mov    %eax,%ebx
  80341d:	83 c4 10             	add    $0x10,%esp
  803420:	85 c0                	test   %eax,%eax
  803422:	0f 88 e2 00 00 00    	js     80350a <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  803428:	83 ec 04             	sub    $0x4,%esp
  80342b:	68 07 04 00 00       	push   $0x407
  803430:	ff 75 f0             	pushl  -0x10(%ebp)
  803433:	6a 00                	push   $0x0
  803435:	e8 25 f2 ff ff       	call   80265f <sys_page_alloc>
  80343a:	89 c3                	mov    %eax,%ebx
  80343c:	83 c4 10             	add    $0x10,%esp
  80343f:	85 c0                	test   %eax,%eax
  803441:	0f 88 c3 00 00 00    	js     80350a <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  803447:	83 ec 0c             	sub    $0xc,%esp
  80344a:	ff 75 f4             	pushl  -0xc(%ebp)
  80344d:	e8 48 f8 ff ff       	call   802c9a <fd2data>
  803452:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  803454:	83 c4 0c             	add    $0xc,%esp
  803457:	68 07 04 00 00       	push   $0x407
  80345c:	50                   	push   %eax
  80345d:	6a 00                	push   $0x0
  80345f:	e8 fb f1 ff ff       	call   80265f <sys_page_alloc>
  803464:	89 c3                	mov    %eax,%ebx
  803466:	83 c4 10             	add    $0x10,%esp
  803469:	85 c0                	test   %eax,%eax
  80346b:	0f 88 89 00 00 00    	js     8034fa <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  803471:	83 ec 0c             	sub    $0xc,%esp
  803474:	ff 75 f0             	pushl  -0x10(%ebp)
  803477:	e8 1e f8 ff ff       	call   802c9a <fd2data>
  80347c:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  803483:	50                   	push   %eax
  803484:	6a 00                	push   $0x0
  803486:	56                   	push   %esi
  803487:	6a 00                	push   $0x0
  803489:	e8 14 f2 ff ff       	call   8026a2 <sys_page_map>
  80348e:	89 c3                	mov    %eax,%ebx
  803490:	83 c4 20             	add    $0x20,%esp
  803493:	85 c0                	test   %eax,%eax
  803495:	78 55                	js     8034ec <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  803497:	8b 15 60 90 80 00    	mov    0x809060,%edx
  80349d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8034a0:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  8034a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8034a5:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  8034ac:	8b 15 60 90 80 00    	mov    0x809060,%edx
  8034b2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8034b5:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  8034b7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8034ba:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  8034c1:	83 ec 0c             	sub    $0xc,%esp
  8034c4:	ff 75 f4             	pushl  -0xc(%ebp)
  8034c7:	e8 be f7 ff ff       	call   802c8a <fd2num>
  8034cc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8034cf:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  8034d1:	83 c4 04             	add    $0x4,%esp
  8034d4:	ff 75 f0             	pushl  -0x10(%ebp)
  8034d7:	e8 ae f7 ff ff       	call   802c8a <fd2num>
  8034dc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8034df:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  8034e2:	83 c4 10             	add    $0x10,%esp
  8034e5:	b8 00 00 00 00       	mov    $0x0,%eax
  8034ea:	eb 30                	jmp    80351c <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  8034ec:	83 ec 08             	sub    $0x8,%esp
  8034ef:	56                   	push   %esi
  8034f0:	6a 00                	push   $0x0
  8034f2:	e8 ed f1 ff ff       	call   8026e4 <sys_page_unmap>
  8034f7:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  8034fa:	83 ec 08             	sub    $0x8,%esp
  8034fd:	ff 75 f0             	pushl  -0x10(%ebp)
  803500:	6a 00                	push   $0x0
  803502:	e8 dd f1 ff ff       	call   8026e4 <sys_page_unmap>
  803507:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  80350a:	83 ec 08             	sub    $0x8,%esp
  80350d:	ff 75 f4             	pushl  -0xc(%ebp)
  803510:	6a 00                	push   $0x0
  803512:	e8 cd f1 ff ff       	call   8026e4 <sys_page_unmap>
  803517:	83 c4 10             	add    $0x10,%esp
  80351a:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  80351c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80351f:	5b                   	pop    %ebx
  803520:	5e                   	pop    %esi
  803521:	5d                   	pop    %ebp
  803522:	c3                   	ret    

00803523 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  803523:	55                   	push   %ebp
  803524:	89 e5                	mov    %esp,%ebp
  803526:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  803529:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80352c:	50                   	push   %eax
  80352d:	ff 75 08             	pushl  0x8(%ebp)
  803530:	e8 cb f7 ff ff       	call   802d00 <fd_lookup>
  803535:	83 c4 10             	add    $0x10,%esp
  803538:	85 c0                	test   %eax,%eax
  80353a:	78 18                	js     803554 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  80353c:	83 ec 0c             	sub    $0xc,%esp
  80353f:	ff 75 f4             	pushl  -0xc(%ebp)
  803542:	e8 53 f7 ff ff       	call   802c9a <fd2data>
	return _pipeisclosed(fd, p);
  803547:	89 c2                	mov    %eax,%edx
  803549:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80354c:	e8 3a fd ff ff       	call   80328b <_pipeisclosed>
  803551:	83 c4 10             	add    $0x10,%esp
}
  803554:	c9                   	leave  
  803555:	c3                   	ret    

00803556 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  803556:	55                   	push   %ebp
  803557:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  803559:	b8 00 00 00 00       	mov    $0x0,%eax
  80355e:	5d                   	pop    %ebp
  80355f:	c3                   	ret    

00803560 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  803560:	55                   	push   %ebp
  803561:	89 e5                	mov    %esp,%ebp
  803563:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  803566:	68 83 43 80 00       	push   $0x804383
  80356b:	ff 75 0c             	pushl  0xc(%ebp)
  80356e:	e8 08 ed ff ff       	call   80227b <strcpy>
	return 0;
}
  803573:	b8 00 00 00 00       	mov    $0x0,%eax
  803578:	c9                   	leave  
  803579:	c3                   	ret    

0080357a <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  80357a:	55                   	push   %ebp
  80357b:	89 e5                	mov    %esp,%ebp
  80357d:	57                   	push   %edi
  80357e:	56                   	push   %esi
  80357f:	53                   	push   %ebx
  803580:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  803586:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  80358b:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  803591:	eb 2c                	jmp    8035bf <devcons_write+0x45>
		m = n - tot;
  803593:	8b 5d 10             	mov    0x10(%ebp),%ebx
  803596:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  803598:	83 fb 7f             	cmp    $0x7f,%ebx
  80359b:	76 05                	jbe    8035a2 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  80359d:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  8035a2:	83 ec 04             	sub    $0x4,%esp
  8035a5:	53                   	push   %ebx
  8035a6:	03 45 0c             	add    0xc(%ebp),%eax
  8035a9:	50                   	push   %eax
  8035aa:	57                   	push   %edi
  8035ab:	e8 40 ee ff ff       	call   8023f0 <memmove>
		sys_cputs(buf, m);
  8035b0:	83 c4 08             	add    $0x8,%esp
  8035b3:	53                   	push   %ebx
  8035b4:	57                   	push   %edi
  8035b5:	e8 e9 ef ff ff       	call   8025a3 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  8035ba:	01 de                	add    %ebx,%esi
  8035bc:	83 c4 10             	add    $0x10,%esp
  8035bf:	89 f0                	mov    %esi,%eax
  8035c1:	3b 75 10             	cmp    0x10(%ebp),%esi
  8035c4:	72 cd                	jb     803593 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  8035c6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8035c9:	5b                   	pop    %ebx
  8035ca:	5e                   	pop    %esi
  8035cb:	5f                   	pop    %edi
  8035cc:	5d                   	pop    %ebp
  8035cd:	c3                   	ret    

008035ce <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  8035ce:	55                   	push   %ebp
  8035cf:	89 e5                	mov    %esp,%ebp
  8035d1:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  8035d4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8035d8:	75 07                	jne    8035e1 <devcons_read+0x13>
  8035da:	eb 23                	jmp    8035ff <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  8035dc:	e8 5f f0 ff ff       	call   802640 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  8035e1:	e8 db ef ff ff       	call   8025c1 <sys_cgetc>
  8035e6:	85 c0                	test   %eax,%eax
  8035e8:	74 f2                	je     8035dc <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  8035ea:	85 c0                	test   %eax,%eax
  8035ec:	78 1d                	js     80360b <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  8035ee:	83 f8 04             	cmp    $0x4,%eax
  8035f1:	74 13                	je     803606 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  8035f3:	8b 55 0c             	mov    0xc(%ebp),%edx
  8035f6:	88 02                	mov    %al,(%edx)
	return 1;
  8035f8:	b8 01 00 00 00       	mov    $0x1,%eax
  8035fd:	eb 0c                	jmp    80360b <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  8035ff:	b8 00 00 00 00       	mov    $0x0,%eax
  803604:	eb 05                	jmp    80360b <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  803606:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  80360b:	c9                   	leave  
  80360c:	c3                   	ret    

0080360d <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  80360d:	55                   	push   %ebp
  80360e:	89 e5                	mov    %esp,%ebp
  803610:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  803613:	8b 45 08             	mov    0x8(%ebp),%eax
  803616:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  803619:	6a 01                	push   $0x1
  80361b:	8d 45 f7             	lea    -0x9(%ebp),%eax
  80361e:	50                   	push   %eax
  80361f:	e8 7f ef ff ff       	call   8025a3 <sys_cputs>
}
  803624:	83 c4 10             	add    $0x10,%esp
  803627:	c9                   	leave  
  803628:	c3                   	ret    

00803629 <getchar>:

int
getchar(void)
{
  803629:	55                   	push   %ebp
  80362a:	89 e5                	mov    %esp,%ebp
  80362c:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  80362f:	6a 01                	push   $0x1
  803631:	8d 45 f7             	lea    -0x9(%ebp),%eax
  803634:	50                   	push   %eax
  803635:	6a 00                	push   $0x0
  803637:	e8 2a f9 ff ff       	call   802f66 <read>
	if (r < 0)
  80363c:	83 c4 10             	add    $0x10,%esp
  80363f:	85 c0                	test   %eax,%eax
  803641:	78 0f                	js     803652 <getchar+0x29>
		return r;
	if (r < 1)
  803643:	85 c0                	test   %eax,%eax
  803645:	7e 06                	jle    80364d <getchar+0x24>
		return -E_EOF;
	return c;
  803647:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  80364b:	eb 05                	jmp    803652 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  80364d:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  803652:	c9                   	leave  
  803653:	c3                   	ret    

00803654 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  803654:	55                   	push   %ebp
  803655:	89 e5                	mov    %esp,%ebp
  803657:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80365a:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80365d:	50                   	push   %eax
  80365e:	ff 75 08             	pushl  0x8(%ebp)
  803661:	e8 9a f6 ff ff       	call   802d00 <fd_lookup>
  803666:	83 c4 10             	add    $0x10,%esp
  803669:	85 c0                	test   %eax,%eax
  80366b:	78 11                	js     80367e <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  80366d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  803670:	8b 15 7c 90 80 00    	mov    0x80907c,%edx
  803676:	39 10                	cmp    %edx,(%eax)
  803678:	0f 94 c0             	sete   %al
  80367b:	0f b6 c0             	movzbl %al,%eax
}
  80367e:	c9                   	leave  
  80367f:	c3                   	ret    

00803680 <opencons>:

int
opencons(void)
{
  803680:	55                   	push   %ebp
  803681:	89 e5                	mov    %esp,%ebp
  803683:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  803686:	8d 45 f4             	lea    -0xc(%ebp),%eax
  803689:	50                   	push   %eax
  80368a:	e8 22 f6 ff ff       	call   802cb1 <fd_alloc>
  80368f:	83 c4 10             	add    $0x10,%esp
  803692:	85 c0                	test   %eax,%eax
  803694:	78 3a                	js     8036d0 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  803696:	83 ec 04             	sub    $0x4,%esp
  803699:	68 07 04 00 00       	push   $0x407
  80369e:	ff 75 f4             	pushl  -0xc(%ebp)
  8036a1:	6a 00                	push   $0x0
  8036a3:	e8 b7 ef ff ff       	call   80265f <sys_page_alloc>
  8036a8:	83 c4 10             	add    $0x10,%esp
  8036ab:	85 c0                	test   %eax,%eax
  8036ad:	78 21                	js     8036d0 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  8036af:	8b 15 7c 90 80 00    	mov    0x80907c,%edx
  8036b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8036b8:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  8036ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8036bd:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  8036c4:	83 ec 0c             	sub    $0xc,%esp
  8036c7:	50                   	push   %eax
  8036c8:	e8 bd f5 ff ff       	call   802c8a <fd2num>
  8036cd:	83 c4 10             	add    $0x10,%esp
}
  8036d0:	c9                   	leave  
  8036d1:	c3                   	ret    
  8036d2:	66 90                	xchg   %ax,%ax

008036d4 <__udivdi3>:
  8036d4:	55                   	push   %ebp
  8036d5:	57                   	push   %edi
  8036d6:	56                   	push   %esi
  8036d7:	53                   	push   %ebx
  8036d8:	83 ec 1c             	sub    $0x1c,%esp
  8036db:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8036df:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8036e3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8036e7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8036eb:	89 ca                	mov    %ecx,%edx
  8036ed:	89 f8                	mov    %edi,%eax
  8036ef:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8036f3:	85 f6                	test   %esi,%esi
  8036f5:	75 2d                	jne    803724 <__udivdi3+0x50>
  8036f7:	39 cf                	cmp    %ecx,%edi
  8036f9:	77 65                	ja     803760 <__udivdi3+0x8c>
  8036fb:	89 fd                	mov    %edi,%ebp
  8036fd:	85 ff                	test   %edi,%edi
  8036ff:	75 0b                	jne    80370c <__udivdi3+0x38>
  803701:	b8 01 00 00 00       	mov    $0x1,%eax
  803706:	31 d2                	xor    %edx,%edx
  803708:	f7 f7                	div    %edi
  80370a:	89 c5                	mov    %eax,%ebp
  80370c:	31 d2                	xor    %edx,%edx
  80370e:	89 c8                	mov    %ecx,%eax
  803710:	f7 f5                	div    %ebp
  803712:	89 c1                	mov    %eax,%ecx
  803714:	89 d8                	mov    %ebx,%eax
  803716:	f7 f5                	div    %ebp
  803718:	89 cf                	mov    %ecx,%edi
  80371a:	89 fa                	mov    %edi,%edx
  80371c:	83 c4 1c             	add    $0x1c,%esp
  80371f:	5b                   	pop    %ebx
  803720:	5e                   	pop    %esi
  803721:	5f                   	pop    %edi
  803722:	5d                   	pop    %ebp
  803723:	c3                   	ret    
  803724:	39 ce                	cmp    %ecx,%esi
  803726:	77 28                	ja     803750 <__udivdi3+0x7c>
  803728:	0f bd fe             	bsr    %esi,%edi
  80372b:	83 f7 1f             	xor    $0x1f,%edi
  80372e:	75 40                	jne    803770 <__udivdi3+0x9c>
  803730:	39 ce                	cmp    %ecx,%esi
  803732:	72 0a                	jb     80373e <__udivdi3+0x6a>
  803734:	3b 44 24 08          	cmp    0x8(%esp),%eax
  803738:	0f 87 9e 00 00 00    	ja     8037dc <__udivdi3+0x108>
  80373e:	b8 01 00 00 00       	mov    $0x1,%eax
  803743:	89 fa                	mov    %edi,%edx
  803745:	83 c4 1c             	add    $0x1c,%esp
  803748:	5b                   	pop    %ebx
  803749:	5e                   	pop    %esi
  80374a:	5f                   	pop    %edi
  80374b:	5d                   	pop    %ebp
  80374c:	c3                   	ret    
  80374d:	8d 76 00             	lea    0x0(%esi),%esi
  803750:	31 ff                	xor    %edi,%edi
  803752:	31 c0                	xor    %eax,%eax
  803754:	89 fa                	mov    %edi,%edx
  803756:	83 c4 1c             	add    $0x1c,%esp
  803759:	5b                   	pop    %ebx
  80375a:	5e                   	pop    %esi
  80375b:	5f                   	pop    %edi
  80375c:	5d                   	pop    %ebp
  80375d:	c3                   	ret    
  80375e:	66 90                	xchg   %ax,%ax
  803760:	89 d8                	mov    %ebx,%eax
  803762:	f7 f7                	div    %edi
  803764:	31 ff                	xor    %edi,%edi
  803766:	89 fa                	mov    %edi,%edx
  803768:	83 c4 1c             	add    $0x1c,%esp
  80376b:	5b                   	pop    %ebx
  80376c:	5e                   	pop    %esi
  80376d:	5f                   	pop    %edi
  80376e:	5d                   	pop    %ebp
  80376f:	c3                   	ret    
  803770:	bd 20 00 00 00       	mov    $0x20,%ebp
  803775:	89 eb                	mov    %ebp,%ebx
  803777:	29 fb                	sub    %edi,%ebx
  803779:	89 f9                	mov    %edi,%ecx
  80377b:	d3 e6                	shl    %cl,%esi
  80377d:	89 c5                	mov    %eax,%ebp
  80377f:	88 d9                	mov    %bl,%cl
  803781:	d3 ed                	shr    %cl,%ebp
  803783:	89 e9                	mov    %ebp,%ecx
  803785:	09 f1                	or     %esi,%ecx
  803787:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80378b:	89 f9                	mov    %edi,%ecx
  80378d:	d3 e0                	shl    %cl,%eax
  80378f:	89 c5                	mov    %eax,%ebp
  803791:	89 d6                	mov    %edx,%esi
  803793:	88 d9                	mov    %bl,%cl
  803795:	d3 ee                	shr    %cl,%esi
  803797:	89 f9                	mov    %edi,%ecx
  803799:	d3 e2                	shl    %cl,%edx
  80379b:	8b 44 24 08          	mov    0x8(%esp),%eax
  80379f:	88 d9                	mov    %bl,%cl
  8037a1:	d3 e8                	shr    %cl,%eax
  8037a3:	09 c2                	or     %eax,%edx
  8037a5:	89 d0                	mov    %edx,%eax
  8037a7:	89 f2                	mov    %esi,%edx
  8037a9:	f7 74 24 0c          	divl   0xc(%esp)
  8037ad:	89 d6                	mov    %edx,%esi
  8037af:	89 c3                	mov    %eax,%ebx
  8037b1:	f7 e5                	mul    %ebp
  8037b3:	39 d6                	cmp    %edx,%esi
  8037b5:	72 19                	jb     8037d0 <__udivdi3+0xfc>
  8037b7:	74 0b                	je     8037c4 <__udivdi3+0xf0>
  8037b9:	89 d8                	mov    %ebx,%eax
  8037bb:	31 ff                	xor    %edi,%edi
  8037bd:	e9 58 ff ff ff       	jmp    80371a <__udivdi3+0x46>
  8037c2:	66 90                	xchg   %ax,%ax
  8037c4:	8b 54 24 08          	mov    0x8(%esp),%edx
  8037c8:	89 f9                	mov    %edi,%ecx
  8037ca:	d3 e2                	shl    %cl,%edx
  8037cc:	39 c2                	cmp    %eax,%edx
  8037ce:	73 e9                	jae    8037b9 <__udivdi3+0xe5>
  8037d0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8037d3:	31 ff                	xor    %edi,%edi
  8037d5:	e9 40 ff ff ff       	jmp    80371a <__udivdi3+0x46>
  8037da:	66 90                	xchg   %ax,%ax
  8037dc:	31 c0                	xor    %eax,%eax
  8037de:	e9 37 ff ff ff       	jmp    80371a <__udivdi3+0x46>
  8037e3:	90                   	nop

008037e4 <__umoddi3>:
  8037e4:	55                   	push   %ebp
  8037e5:	57                   	push   %edi
  8037e6:	56                   	push   %esi
  8037e7:	53                   	push   %ebx
  8037e8:	83 ec 1c             	sub    $0x1c,%esp
  8037eb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8037ef:	8b 74 24 34          	mov    0x34(%esp),%esi
  8037f3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8037f7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8037fb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8037ff:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  803803:	89 f3                	mov    %esi,%ebx
  803805:	89 fa                	mov    %edi,%edx
  803807:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80380b:	89 34 24             	mov    %esi,(%esp)
  80380e:	85 c0                	test   %eax,%eax
  803810:	75 1a                	jne    80382c <__umoddi3+0x48>
  803812:	39 f7                	cmp    %esi,%edi
  803814:	0f 86 a2 00 00 00    	jbe    8038bc <__umoddi3+0xd8>
  80381a:	89 c8                	mov    %ecx,%eax
  80381c:	89 f2                	mov    %esi,%edx
  80381e:	f7 f7                	div    %edi
  803820:	89 d0                	mov    %edx,%eax
  803822:	31 d2                	xor    %edx,%edx
  803824:	83 c4 1c             	add    $0x1c,%esp
  803827:	5b                   	pop    %ebx
  803828:	5e                   	pop    %esi
  803829:	5f                   	pop    %edi
  80382a:	5d                   	pop    %ebp
  80382b:	c3                   	ret    
  80382c:	39 f0                	cmp    %esi,%eax
  80382e:	0f 87 ac 00 00 00    	ja     8038e0 <__umoddi3+0xfc>
  803834:	0f bd e8             	bsr    %eax,%ebp
  803837:	83 f5 1f             	xor    $0x1f,%ebp
  80383a:	0f 84 ac 00 00 00    	je     8038ec <__umoddi3+0x108>
  803840:	bf 20 00 00 00       	mov    $0x20,%edi
  803845:	29 ef                	sub    %ebp,%edi
  803847:	89 fe                	mov    %edi,%esi
  803849:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80384d:	89 e9                	mov    %ebp,%ecx
  80384f:	d3 e0                	shl    %cl,%eax
  803851:	89 d7                	mov    %edx,%edi
  803853:	89 f1                	mov    %esi,%ecx
  803855:	d3 ef                	shr    %cl,%edi
  803857:	09 c7                	or     %eax,%edi
  803859:	89 e9                	mov    %ebp,%ecx
  80385b:	d3 e2                	shl    %cl,%edx
  80385d:	89 14 24             	mov    %edx,(%esp)
  803860:	89 d8                	mov    %ebx,%eax
  803862:	d3 e0                	shl    %cl,%eax
  803864:	89 c2                	mov    %eax,%edx
  803866:	8b 44 24 08          	mov    0x8(%esp),%eax
  80386a:	d3 e0                	shl    %cl,%eax
  80386c:	89 44 24 04          	mov    %eax,0x4(%esp)
  803870:	8b 44 24 08          	mov    0x8(%esp),%eax
  803874:	89 f1                	mov    %esi,%ecx
  803876:	d3 e8                	shr    %cl,%eax
  803878:	09 d0                	or     %edx,%eax
  80387a:	d3 eb                	shr    %cl,%ebx
  80387c:	89 da                	mov    %ebx,%edx
  80387e:	f7 f7                	div    %edi
  803880:	89 d3                	mov    %edx,%ebx
  803882:	f7 24 24             	mull   (%esp)
  803885:	89 c6                	mov    %eax,%esi
  803887:	89 d1                	mov    %edx,%ecx
  803889:	39 d3                	cmp    %edx,%ebx
  80388b:	0f 82 87 00 00 00    	jb     803918 <__umoddi3+0x134>
  803891:	0f 84 91 00 00 00    	je     803928 <__umoddi3+0x144>
  803897:	8b 54 24 04          	mov    0x4(%esp),%edx
  80389b:	29 f2                	sub    %esi,%edx
  80389d:	19 cb                	sbb    %ecx,%ebx
  80389f:	89 d8                	mov    %ebx,%eax
  8038a1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8038a5:	d3 e0                	shl    %cl,%eax
  8038a7:	89 e9                	mov    %ebp,%ecx
  8038a9:	d3 ea                	shr    %cl,%edx
  8038ab:	09 d0                	or     %edx,%eax
  8038ad:	89 e9                	mov    %ebp,%ecx
  8038af:	d3 eb                	shr    %cl,%ebx
  8038b1:	89 da                	mov    %ebx,%edx
  8038b3:	83 c4 1c             	add    $0x1c,%esp
  8038b6:	5b                   	pop    %ebx
  8038b7:	5e                   	pop    %esi
  8038b8:	5f                   	pop    %edi
  8038b9:	5d                   	pop    %ebp
  8038ba:	c3                   	ret    
  8038bb:	90                   	nop
  8038bc:	89 fd                	mov    %edi,%ebp
  8038be:	85 ff                	test   %edi,%edi
  8038c0:	75 0b                	jne    8038cd <__umoddi3+0xe9>
  8038c2:	b8 01 00 00 00       	mov    $0x1,%eax
  8038c7:	31 d2                	xor    %edx,%edx
  8038c9:	f7 f7                	div    %edi
  8038cb:	89 c5                	mov    %eax,%ebp
  8038cd:	89 f0                	mov    %esi,%eax
  8038cf:	31 d2                	xor    %edx,%edx
  8038d1:	f7 f5                	div    %ebp
  8038d3:	89 c8                	mov    %ecx,%eax
  8038d5:	f7 f5                	div    %ebp
  8038d7:	89 d0                	mov    %edx,%eax
  8038d9:	e9 44 ff ff ff       	jmp    803822 <__umoddi3+0x3e>
  8038de:	66 90                	xchg   %ax,%ax
  8038e0:	89 c8                	mov    %ecx,%eax
  8038e2:	89 f2                	mov    %esi,%edx
  8038e4:	83 c4 1c             	add    $0x1c,%esp
  8038e7:	5b                   	pop    %ebx
  8038e8:	5e                   	pop    %esi
  8038e9:	5f                   	pop    %edi
  8038ea:	5d                   	pop    %ebp
  8038eb:	c3                   	ret    
  8038ec:	3b 04 24             	cmp    (%esp),%eax
  8038ef:	72 06                	jb     8038f7 <__umoddi3+0x113>
  8038f1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8038f5:	77 0f                	ja     803906 <__umoddi3+0x122>
  8038f7:	89 f2                	mov    %esi,%edx
  8038f9:	29 f9                	sub    %edi,%ecx
  8038fb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8038ff:	89 14 24             	mov    %edx,(%esp)
  803902:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  803906:	8b 44 24 04          	mov    0x4(%esp),%eax
  80390a:	8b 14 24             	mov    (%esp),%edx
  80390d:	83 c4 1c             	add    $0x1c,%esp
  803910:	5b                   	pop    %ebx
  803911:	5e                   	pop    %esi
  803912:	5f                   	pop    %edi
  803913:	5d                   	pop    %ebp
  803914:	c3                   	ret    
  803915:	8d 76 00             	lea    0x0(%esi),%esi
  803918:	2b 04 24             	sub    (%esp),%eax
  80391b:	19 fa                	sbb    %edi,%edx
  80391d:	89 d1                	mov    %edx,%ecx
  80391f:	89 c6                	mov    %eax,%esi
  803921:	e9 71 ff ff ff       	jmp    803897 <__umoddi3+0xb3>
  803926:	66 90                	xchg   %ax,%ax
  803928:	39 44 24 04          	cmp    %eax,0x4(%esp)
  80392c:	72 ea                	jb     803918 <__umoddi3+0x134>
  80392e:	89 d9                	mov    %ebx,%ecx
  803930:	e9 62 ff ff ff       	jmp    803897 <__umoddi3+0xb3>
