
obj/user/divzero.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 2f 00 00 00       	call   800060 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

int zero;

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	zero = 0;
  800039:	c7 05 04 20 80 00 00 	movl   $0x0,0x802004
  800040:	00 00 00 
	cprintf("1/0 is %08x!\n", 1/zero);
  800043:	b8 01 00 00 00       	mov    $0x1,%eax
  800048:	b9 00 00 00 00       	mov    $0x0,%ecx
  80004d:	99                   	cltd   
  80004e:	f7 f9                	idiv   %ecx
  800050:	50                   	push   %eax
  800051:	68 60 0f 80 00       	push   $0x800f60
  800056:	e8 f8 00 00 00       	call   800153 <cprintf>
}
  80005b:	83 c4 10             	add    $0x10,%esp
  80005e:	c9                   	leave  
  80005f:	c3                   	ret    

00800060 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800060:	55                   	push   %ebp
  800061:	89 e5                	mov    %esp,%ebp
  800063:	56                   	push   %esi
  800064:	53                   	push   %ebx
  800065:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800068:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80006b:	e8 ed 09 00 00       	call   800a5d <sys_getenvid>
  800070:	25 ff 03 00 00       	and    $0x3ff,%eax
  800075:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80007c:	c1 e0 07             	shl    $0x7,%eax
  80007f:	29 d0                	sub    %edx,%eax
  800081:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800086:	a3 08 20 80 00       	mov    %eax,0x802008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80008b:	85 db                	test   %ebx,%ebx
  80008d:	7e 07                	jle    800096 <libmain+0x36>
		binaryname = argv[0];
  80008f:	8b 06                	mov    (%esi),%eax
  800091:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800096:	83 ec 08             	sub    $0x8,%esp
  800099:	56                   	push   %esi
  80009a:	53                   	push   %ebx
  80009b:	e8 93 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000a0:	e8 0a 00 00 00       	call   8000af <exit>
}
  8000a5:	83 c4 10             	add    $0x10,%esp
  8000a8:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000ab:	5b                   	pop    %ebx
  8000ac:	5e                   	pop    %esi
  8000ad:	5d                   	pop    %ebp
  8000ae:	c3                   	ret    

008000af <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000af:	55                   	push   %ebp
  8000b0:	89 e5                	mov    %esp,%ebp
  8000b2:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000b5:	6a 00                	push   $0x0
  8000b7:	e8 60 09 00 00       	call   800a1c <sys_env_destroy>
}
  8000bc:	83 c4 10             	add    $0x10,%esp
  8000bf:	c9                   	leave  
  8000c0:	c3                   	ret    

008000c1 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000c1:	55                   	push   %ebp
  8000c2:	89 e5                	mov    %esp,%ebp
  8000c4:	53                   	push   %ebx
  8000c5:	83 ec 04             	sub    $0x4,%esp
  8000c8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000cb:	8b 13                	mov    (%ebx),%edx
  8000cd:	8d 42 01             	lea    0x1(%edx),%eax
  8000d0:	89 03                	mov    %eax,(%ebx)
  8000d2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000d5:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000d9:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000de:	75 1a                	jne    8000fa <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000e0:	83 ec 08             	sub    $0x8,%esp
  8000e3:	68 ff 00 00 00       	push   $0xff
  8000e8:	8d 43 08             	lea    0x8(%ebx),%eax
  8000eb:	50                   	push   %eax
  8000ec:	e8 ee 08 00 00       	call   8009df <sys_cputs>
		b->idx = 0;
  8000f1:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8000f7:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8000fa:	ff 43 04             	incl   0x4(%ebx)
}
  8000fd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800100:	c9                   	leave  
  800101:	c3                   	ret    

00800102 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800102:	55                   	push   %ebp
  800103:	89 e5                	mov    %esp,%ebp
  800105:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80010b:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800112:	00 00 00 
	b.cnt = 0;
  800115:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80011c:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80011f:	ff 75 0c             	pushl  0xc(%ebp)
  800122:	ff 75 08             	pushl  0x8(%ebp)
  800125:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80012b:	50                   	push   %eax
  80012c:	68 c1 00 80 00       	push   $0x8000c1
  800131:	e8 51 01 00 00       	call   800287 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800136:	83 c4 08             	add    $0x8,%esp
  800139:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80013f:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800145:	50                   	push   %eax
  800146:	e8 94 08 00 00       	call   8009df <sys_cputs>

	return b.cnt;
}
  80014b:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800151:	c9                   	leave  
  800152:	c3                   	ret    

00800153 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800153:	55                   	push   %ebp
  800154:	89 e5                	mov    %esp,%ebp
  800156:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800159:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80015c:	50                   	push   %eax
  80015d:	ff 75 08             	pushl  0x8(%ebp)
  800160:	e8 9d ff ff ff       	call   800102 <vcprintf>
	va_end(ap);

	return cnt;
}
  800165:	c9                   	leave  
  800166:	c3                   	ret    

00800167 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800167:	55                   	push   %ebp
  800168:	89 e5                	mov    %esp,%ebp
  80016a:	57                   	push   %edi
  80016b:	56                   	push   %esi
  80016c:	53                   	push   %ebx
  80016d:	83 ec 1c             	sub    $0x1c,%esp
  800170:	89 c7                	mov    %eax,%edi
  800172:	89 d6                	mov    %edx,%esi
  800174:	8b 45 08             	mov    0x8(%ebp),%eax
  800177:	8b 55 0c             	mov    0xc(%ebp),%edx
  80017a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80017d:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800180:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800183:	bb 00 00 00 00       	mov    $0x0,%ebx
  800188:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80018b:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80018e:	39 d3                	cmp    %edx,%ebx
  800190:	72 05                	jb     800197 <printnum+0x30>
  800192:	39 45 10             	cmp    %eax,0x10(%ebp)
  800195:	77 45                	ja     8001dc <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800197:	83 ec 0c             	sub    $0xc,%esp
  80019a:	ff 75 18             	pushl  0x18(%ebp)
  80019d:	8b 45 14             	mov    0x14(%ebp),%eax
  8001a0:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001a3:	53                   	push   %ebx
  8001a4:	ff 75 10             	pushl  0x10(%ebp)
  8001a7:	83 ec 08             	sub    $0x8,%esp
  8001aa:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ad:	ff 75 e0             	pushl  -0x20(%ebp)
  8001b0:	ff 75 dc             	pushl  -0x24(%ebp)
  8001b3:	ff 75 d8             	pushl  -0x28(%ebp)
  8001b6:	e8 39 0b 00 00       	call   800cf4 <__udivdi3>
  8001bb:	83 c4 18             	add    $0x18,%esp
  8001be:	52                   	push   %edx
  8001bf:	50                   	push   %eax
  8001c0:	89 f2                	mov    %esi,%edx
  8001c2:	89 f8                	mov    %edi,%eax
  8001c4:	e8 9e ff ff ff       	call   800167 <printnum>
  8001c9:	83 c4 20             	add    $0x20,%esp
  8001cc:	eb 16                	jmp    8001e4 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001ce:	83 ec 08             	sub    $0x8,%esp
  8001d1:	56                   	push   %esi
  8001d2:	ff 75 18             	pushl  0x18(%ebp)
  8001d5:	ff d7                	call   *%edi
  8001d7:	83 c4 10             	add    $0x10,%esp
  8001da:	eb 03                	jmp    8001df <printnum+0x78>
  8001dc:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001df:	4b                   	dec    %ebx
  8001e0:	85 db                	test   %ebx,%ebx
  8001e2:	7f ea                	jg     8001ce <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001e4:	83 ec 08             	sub    $0x8,%esp
  8001e7:	56                   	push   %esi
  8001e8:	83 ec 04             	sub    $0x4,%esp
  8001eb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ee:	ff 75 e0             	pushl  -0x20(%ebp)
  8001f1:	ff 75 dc             	pushl  -0x24(%ebp)
  8001f4:	ff 75 d8             	pushl  -0x28(%ebp)
  8001f7:	e8 08 0c 00 00       	call   800e04 <__umoddi3>
  8001fc:	83 c4 14             	add    $0x14,%esp
  8001ff:	0f be 80 78 0f 80 00 	movsbl 0x800f78(%eax),%eax
  800206:	50                   	push   %eax
  800207:	ff d7                	call   *%edi
}
  800209:	83 c4 10             	add    $0x10,%esp
  80020c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80020f:	5b                   	pop    %ebx
  800210:	5e                   	pop    %esi
  800211:	5f                   	pop    %edi
  800212:	5d                   	pop    %ebp
  800213:	c3                   	ret    

00800214 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800214:	55                   	push   %ebp
  800215:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800217:	83 fa 01             	cmp    $0x1,%edx
  80021a:	7e 0e                	jle    80022a <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80021c:	8b 10                	mov    (%eax),%edx
  80021e:	8d 4a 08             	lea    0x8(%edx),%ecx
  800221:	89 08                	mov    %ecx,(%eax)
  800223:	8b 02                	mov    (%edx),%eax
  800225:	8b 52 04             	mov    0x4(%edx),%edx
  800228:	eb 22                	jmp    80024c <getuint+0x38>
	else if (lflag)
  80022a:	85 d2                	test   %edx,%edx
  80022c:	74 10                	je     80023e <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80022e:	8b 10                	mov    (%eax),%edx
  800230:	8d 4a 04             	lea    0x4(%edx),%ecx
  800233:	89 08                	mov    %ecx,(%eax)
  800235:	8b 02                	mov    (%edx),%eax
  800237:	ba 00 00 00 00       	mov    $0x0,%edx
  80023c:	eb 0e                	jmp    80024c <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80023e:	8b 10                	mov    (%eax),%edx
  800240:	8d 4a 04             	lea    0x4(%edx),%ecx
  800243:	89 08                	mov    %ecx,(%eax)
  800245:	8b 02                	mov    (%edx),%eax
  800247:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80024c:	5d                   	pop    %ebp
  80024d:	c3                   	ret    

0080024e <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80024e:	55                   	push   %ebp
  80024f:	89 e5                	mov    %esp,%ebp
  800251:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800254:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800257:	8b 10                	mov    (%eax),%edx
  800259:	3b 50 04             	cmp    0x4(%eax),%edx
  80025c:	73 0a                	jae    800268 <sprintputch+0x1a>
		*b->buf++ = ch;
  80025e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800261:	89 08                	mov    %ecx,(%eax)
  800263:	8b 45 08             	mov    0x8(%ebp),%eax
  800266:	88 02                	mov    %al,(%edx)
}
  800268:	5d                   	pop    %ebp
  800269:	c3                   	ret    

0080026a <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80026a:	55                   	push   %ebp
  80026b:	89 e5                	mov    %esp,%ebp
  80026d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800270:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800273:	50                   	push   %eax
  800274:	ff 75 10             	pushl  0x10(%ebp)
  800277:	ff 75 0c             	pushl  0xc(%ebp)
  80027a:	ff 75 08             	pushl  0x8(%ebp)
  80027d:	e8 05 00 00 00       	call   800287 <vprintfmt>
	va_end(ap);
}
  800282:	83 c4 10             	add    $0x10,%esp
  800285:	c9                   	leave  
  800286:	c3                   	ret    

00800287 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800287:	55                   	push   %ebp
  800288:	89 e5                	mov    %esp,%ebp
  80028a:	57                   	push   %edi
  80028b:	56                   	push   %esi
  80028c:	53                   	push   %ebx
  80028d:	83 ec 2c             	sub    $0x2c,%esp
  800290:	8b 75 08             	mov    0x8(%ebp),%esi
  800293:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800296:	8b 7d 10             	mov    0x10(%ebp),%edi
  800299:	eb 12                	jmp    8002ad <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80029b:	85 c0                	test   %eax,%eax
  80029d:	0f 84 68 03 00 00    	je     80060b <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002a3:	83 ec 08             	sub    $0x8,%esp
  8002a6:	53                   	push   %ebx
  8002a7:	50                   	push   %eax
  8002a8:	ff d6                	call   *%esi
  8002aa:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002ad:	47                   	inc    %edi
  8002ae:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002b2:	83 f8 25             	cmp    $0x25,%eax
  8002b5:	75 e4                	jne    80029b <vprintfmt+0x14>
  8002b7:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002bb:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002c2:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002c9:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002d0:	ba 00 00 00 00       	mov    $0x0,%edx
  8002d5:	eb 07                	jmp    8002de <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002d7:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002da:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002de:	8d 47 01             	lea    0x1(%edi),%eax
  8002e1:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002e4:	0f b6 0f             	movzbl (%edi),%ecx
  8002e7:	8a 07                	mov    (%edi),%al
  8002e9:	83 e8 23             	sub    $0x23,%eax
  8002ec:	3c 55                	cmp    $0x55,%al
  8002ee:	0f 87 fe 02 00 00    	ja     8005f2 <vprintfmt+0x36b>
  8002f4:	0f b6 c0             	movzbl %al,%eax
  8002f7:	ff 24 85 c0 10 80 00 	jmp    *0x8010c0(,%eax,4)
  8002fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800301:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800305:	eb d7                	jmp    8002de <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800307:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80030a:	b8 00 00 00 00       	mov    $0x0,%eax
  80030f:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800312:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800315:	01 c0                	add    %eax,%eax
  800317:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80031b:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80031e:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800321:	83 fa 09             	cmp    $0x9,%edx
  800324:	77 34                	ja     80035a <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800326:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800327:	eb e9                	jmp    800312 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800329:	8b 45 14             	mov    0x14(%ebp),%eax
  80032c:	8d 48 04             	lea    0x4(%eax),%ecx
  80032f:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800332:	8b 00                	mov    (%eax),%eax
  800334:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800337:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80033a:	eb 24                	jmp    800360 <vprintfmt+0xd9>
  80033c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800340:	79 07                	jns    800349 <vprintfmt+0xc2>
  800342:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800349:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80034c:	eb 90                	jmp    8002de <vprintfmt+0x57>
  80034e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800351:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800358:	eb 84                	jmp    8002de <vprintfmt+0x57>
  80035a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80035d:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800360:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800364:	0f 89 74 ff ff ff    	jns    8002de <vprintfmt+0x57>
				width = precision, precision = -1;
  80036a:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80036d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800370:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800377:	e9 62 ff ff ff       	jmp    8002de <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80037c:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80037d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800380:	e9 59 ff ff ff       	jmp    8002de <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800385:	8b 45 14             	mov    0x14(%ebp),%eax
  800388:	8d 50 04             	lea    0x4(%eax),%edx
  80038b:	89 55 14             	mov    %edx,0x14(%ebp)
  80038e:	83 ec 08             	sub    $0x8,%esp
  800391:	53                   	push   %ebx
  800392:	ff 30                	pushl  (%eax)
  800394:	ff d6                	call   *%esi
			break;
  800396:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800399:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80039c:	e9 0c ff ff ff       	jmp    8002ad <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003a1:	8b 45 14             	mov    0x14(%ebp),%eax
  8003a4:	8d 50 04             	lea    0x4(%eax),%edx
  8003a7:	89 55 14             	mov    %edx,0x14(%ebp)
  8003aa:	8b 00                	mov    (%eax),%eax
  8003ac:	85 c0                	test   %eax,%eax
  8003ae:	79 02                	jns    8003b2 <vprintfmt+0x12b>
  8003b0:	f7 d8                	neg    %eax
  8003b2:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003b4:	83 f8 0f             	cmp    $0xf,%eax
  8003b7:	7f 0b                	jg     8003c4 <vprintfmt+0x13d>
  8003b9:	8b 04 85 20 12 80 00 	mov    0x801220(,%eax,4),%eax
  8003c0:	85 c0                	test   %eax,%eax
  8003c2:	75 18                	jne    8003dc <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003c4:	52                   	push   %edx
  8003c5:	68 90 0f 80 00       	push   $0x800f90
  8003ca:	53                   	push   %ebx
  8003cb:	56                   	push   %esi
  8003cc:	e8 99 fe ff ff       	call   80026a <printfmt>
  8003d1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003d7:	e9 d1 fe ff ff       	jmp    8002ad <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003dc:	50                   	push   %eax
  8003dd:	68 99 0f 80 00       	push   $0x800f99
  8003e2:	53                   	push   %ebx
  8003e3:	56                   	push   %esi
  8003e4:	e8 81 fe ff ff       	call   80026a <printfmt>
  8003e9:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ec:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003ef:	e9 b9 fe ff ff       	jmp    8002ad <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8003f4:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f7:	8d 50 04             	lea    0x4(%eax),%edx
  8003fa:	89 55 14             	mov    %edx,0x14(%ebp)
  8003fd:	8b 38                	mov    (%eax),%edi
  8003ff:	85 ff                	test   %edi,%edi
  800401:	75 05                	jne    800408 <vprintfmt+0x181>
				p = "(null)";
  800403:	bf 89 0f 80 00       	mov    $0x800f89,%edi
			if (width > 0 && padc != '-')
  800408:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80040c:	0f 8e 90 00 00 00    	jle    8004a2 <vprintfmt+0x21b>
  800412:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800416:	0f 84 8e 00 00 00    	je     8004aa <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80041c:	83 ec 08             	sub    $0x8,%esp
  80041f:	ff 75 d0             	pushl  -0x30(%ebp)
  800422:	57                   	push   %edi
  800423:	e8 70 02 00 00       	call   800698 <strnlen>
  800428:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80042b:	29 c1                	sub    %eax,%ecx
  80042d:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800430:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800433:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800437:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80043a:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80043d:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80043f:	eb 0d                	jmp    80044e <vprintfmt+0x1c7>
					putch(padc, putdat);
  800441:	83 ec 08             	sub    $0x8,%esp
  800444:	53                   	push   %ebx
  800445:	ff 75 e4             	pushl  -0x1c(%ebp)
  800448:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80044a:	4f                   	dec    %edi
  80044b:	83 c4 10             	add    $0x10,%esp
  80044e:	85 ff                	test   %edi,%edi
  800450:	7f ef                	jg     800441 <vprintfmt+0x1ba>
  800452:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800455:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800458:	89 c8                	mov    %ecx,%eax
  80045a:	85 c9                	test   %ecx,%ecx
  80045c:	79 05                	jns    800463 <vprintfmt+0x1dc>
  80045e:	b8 00 00 00 00       	mov    $0x0,%eax
  800463:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800466:	29 c1                	sub    %eax,%ecx
  800468:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80046b:	89 75 08             	mov    %esi,0x8(%ebp)
  80046e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800471:	eb 3d                	jmp    8004b0 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800473:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800477:	74 19                	je     800492 <vprintfmt+0x20b>
  800479:	0f be c0             	movsbl %al,%eax
  80047c:	83 e8 20             	sub    $0x20,%eax
  80047f:	83 f8 5e             	cmp    $0x5e,%eax
  800482:	76 0e                	jbe    800492 <vprintfmt+0x20b>
					putch('?', putdat);
  800484:	83 ec 08             	sub    $0x8,%esp
  800487:	53                   	push   %ebx
  800488:	6a 3f                	push   $0x3f
  80048a:	ff 55 08             	call   *0x8(%ebp)
  80048d:	83 c4 10             	add    $0x10,%esp
  800490:	eb 0b                	jmp    80049d <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800492:	83 ec 08             	sub    $0x8,%esp
  800495:	53                   	push   %ebx
  800496:	52                   	push   %edx
  800497:	ff 55 08             	call   *0x8(%ebp)
  80049a:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80049d:	ff 4d e4             	decl   -0x1c(%ebp)
  8004a0:	eb 0e                	jmp    8004b0 <vprintfmt+0x229>
  8004a2:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a5:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004a8:	eb 06                	jmp    8004b0 <vprintfmt+0x229>
  8004aa:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ad:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004b0:	47                   	inc    %edi
  8004b1:	8a 47 ff             	mov    -0x1(%edi),%al
  8004b4:	0f be d0             	movsbl %al,%edx
  8004b7:	85 d2                	test   %edx,%edx
  8004b9:	74 1d                	je     8004d8 <vprintfmt+0x251>
  8004bb:	85 f6                	test   %esi,%esi
  8004bd:	78 b4                	js     800473 <vprintfmt+0x1ec>
  8004bf:	4e                   	dec    %esi
  8004c0:	79 b1                	jns    800473 <vprintfmt+0x1ec>
  8004c2:	8b 75 08             	mov    0x8(%ebp),%esi
  8004c5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004c8:	eb 14                	jmp    8004de <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004ca:	83 ec 08             	sub    $0x8,%esp
  8004cd:	53                   	push   %ebx
  8004ce:	6a 20                	push   $0x20
  8004d0:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004d2:	4f                   	dec    %edi
  8004d3:	83 c4 10             	add    $0x10,%esp
  8004d6:	eb 06                	jmp    8004de <vprintfmt+0x257>
  8004d8:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004db:	8b 75 08             	mov    0x8(%ebp),%esi
  8004de:	85 ff                	test   %edi,%edi
  8004e0:	7f e8                	jg     8004ca <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004e5:	e9 c3 fd ff ff       	jmp    8002ad <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004ea:	83 fa 01             	cmp    $0x1,%edx
  8004ed:	7e 16                	jle    800505 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8004ef:	8b 45 14             	mov    0x14(%ebp),%eax
  8004f2:	8d 50 08             	lea    0x8(%eax),%edx
  8004f5:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f8:	8b 50 04             	mov    0x4(%eax),%edx
  8004fb:	8b 00                	mov    (%eax),%eax
  8004fd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800500:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800503:	eb 32                	jmp    800537 <vprintfmt+0x2b0>
	else if (lflag)
  800505:	85 d2                	test   %edx,%edx
  800507:	74 18                	je     800521 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800509:	8b 45 14             	mov    0x14(%ebp),%eax
  80050c:	8d 50 04             	lea    0x4(%eax),%edx
  80050f:	89 55 14             	mov    %edx,0x14(%ebp)
  800512:	8b 00                	mov    (%eax),%eax
  800514:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800517:	89 c1                	mov    %eax,%ecx
  800519:	c1 f9 1f             	sar    $0x1f,%ecx
  80051c:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80051f:	eb 16                	jmp    800537 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800521:	8b 45 14             	mov    0x14(%ebp),%eax
  800524:	8d 50 04             	lea    0x4(%eax),%edx
  800527:	89 55 14             	mov    %edx,0x14(%ebp)
  80052a:	8b 00                	mov    (%eax),%eax
  80052c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80052f:	89 c1                	mov    %eax,%ecx
  800531:	c1 f9 1f             	sar    $0x1f,%ecx
  800534:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800537:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80053a:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80053d:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800541:	79 76                	jns    8005b9 <vprintfmt+0x332>
				putch('-', putdat);
  800543:	83 ec 08             	sub    $0x8,%esp
  800546:	53                   	push   %ebx
  800547:	6a 2d                	push   $0x2d
  800549:	ff d6                	call   *%esi
				num = -(long long) num;
  80054b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80054e:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800551:	f7 d8                	neg    %eax
  800553:	83 d2 00             	adc    $0x0,%edx
  800556:	f7 da                	neg    %edx
  800558:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80055b:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800560:	eb 5c                	jmp    8005be <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800562:	8d 45 14             	lea    0x14(%ebp),%eax
  800565:	e8 aa fc ff ff       	call   800214 <getuint>
			base = 10;
  80056a:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80056f:	eb 4d                	jmp    8005be <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800571:	8d 45 14             	lea    0x14(%ebp),%eax
  800574:	e8 9b fc ff ff       	call   800214 <getuint>
			base = 8;
  800579:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80057e:	eb 3e                	jmp    8005be <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800580:	83 ec 08             	sub    $0x8,%esp
  800583:	53                   	push   %ebx
  800584:	6a 30                	push   $0x30
  800586:	ff d6                	call   *%esi
			putch('x', putdat);
  800588:	83 c4 08             	add    $0x8,%esp
  80058b:	53                   	push   %ebx
  80058c:	6a 78                	push   $0x78
  80058e:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800590:	8b 45 14             	mov    0x14(%ebp),%eax
  800593:	8d 50 04             	lea    0x4(%eax),%edx
  800596:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800599:	8b 00                	mov    (%eax),%eax
  80059b:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005a0:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005a3:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005a8:	eb 14                	jmp    8005be <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005aa:	8d 45 14             	lea    0x14(%ebp),%eax
  8005ad:	e8 62 fc ff ff       	call   800214 <getuint>
			base = 16;
  8005b2:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005b7:	eb 05                	jmp    8005be <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005b9:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005be:	83 ec 0c             	sub    $0xc,%esp
  8005c1:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005c5:	57                   	push   %edi
  8005c6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005c9:	51                   	push   %ecx
  8005ca:	52                   	push   %edx
  8005cb:	50                   	push   %eax
  8005cc:	89 da                	mov    %ebx,%edx
  8005ce:	89 f0                	mov    %esi,%eax
  8005d0:	e8 92 fb ff ff       	call   800167 <printnum>
			break;
  8005d5:	83 c4 20             	add    $0x20,%esp
  8005d8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005db:	e9 cd fc ff ff       	jmp    8002ad <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005e0:	83 ec 08             	sub    $0x8,%esp
  8005e3:	53                   	push   %ebx
  8005e4:	51                   	push   %ecx
  8005e5:	ff d6                	call   *%esi
			break;
  8005e7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ea:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8005ed:	e9 bb fc ff ff       	jmp    8002ad <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8005f2:	83 ec 08             	sub    $0x8,%esp
  8005f5:	53                   	push   %ebx
  8005f6:	6a 25                	push   $0x25
  8005f8:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8005fa:	83 c4 10             	add    $0x10,%esp
  8005fd:	eb 01                	jmp    800600 <vprintfmt+0x379>
  8005ff:	4f                   	dec    %edi
  800600:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800604:	75 f9                	jne    8005ff <vprintfmt+0x378>
  800606:	e9 a2 fc ff ff       	jmp    8002ad <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80060b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80060e:	5b                   	pop    %ebx
  80060f:	5e                   	pop    %esi
  800610:	5f                   	pop    %edi
  800611:	5d                   	pop    %ebp
  800612:	c3                   	ret    

00800613 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800613:	55                   	push   %ebp
  800614:	89 e5                	mov    %esp,%ebp
  800616:	83 ec 18             	sub    $0x18,%esp
  800619:	8b 45 08             	mov    0x8(%ebp),%eax
  80061c:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80061f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800622:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800626:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800629:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800630:	85 c0                	test   %eax,%eax
  800632:	74 26                	je     80065a <vsnprintf+0x47>
  800634:	85 d2                	test   %edx,%edx
  800636:	7e 29                	jle    800661 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800638:	ff 75 14             	pushl  0x14(%ebp)
  80063b:	ff 75 10             	pushl  0x10(%ebp)
  80063e:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800641:	50                   	push   %eax
  800642:	68 4e 02 80 00       	push   $0x80024e
  800647:	e8 3b fc ff ff       	call   800287 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80064c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80064f:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800652:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800655:	83 c4 10             	add    $0x10,%esp
  800658:	eb 0c                	jmp    800666 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80065a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80065f:	eb 05                	jmp    800666 <vsnprintf+0x53>
  800661:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800666:	c9                   	leave  
  800667:	c3                   	ret    

00800668 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800668:	55                   	push   %ebp
  800669:	89 e5                	mov    %esp,%ebp
  80066b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80066e:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800671:	50                   	push   %eax
  800672:	ff 75 10             	pushl  0x10(%ebp)
  800675:	ff 75 0c             	pushl  0xc(%ebp)
  800678:	ff 75 08             	pushl  0x8(%ebp)
  80067b:	e8 93 ff ff ff       	call   800613 <vsnprintf>
	va_end(ap);

	return rc;
}
  800680:	c9                   	leave  
  800681:	c3                   	ret    

00800682 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800682:	55                   	push   %ebp
  800683:	89 e5                	mov    %esp,%ebp
  800685:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800688:	b8 00 00 00 00       	mov    $0x0,%eax
  80068d:	eb 01                	jmp    800690 <strlen+0xe>
		n++;
  80068f:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800690:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800694:	75 f9                	jne    80068f <strlen+0xd>
		n++;
	return n;
}
  800696:	5d                   	pop    %ebp
  800697:	c3                   	ret    

00800698 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800698:	55                   	push   %ebp
  800699:	89 e5                	mov    %esp,%ebp
  80069b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80069e:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006a1:	ba 00 00 00 00       	mov    $0x0,%edx
  8006a6:	eb 01                	jmp    8006a9 <strnlen+0x11>
		n++;
  8006a8:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006a9:	39 c2                	cmp    %eax,%edx
  8006ab:	74 08                	je     8006b5 <strnlen+0x1d>
  8006ad:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006b1:	75 f5                	jne    8006a8 <strnlen+0x10>
  8006b3:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006b5:	5d                   	pop    %ebp
  8006b6:	c3                   	ret    

008006b7 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006b7:	55                   	push   %ebp
  8006b8:	89 e5                	mov    %esp,%ebp
  8006ba:	53                   	push   %ebx
  8006bb:	8b 45 08             	mov    0x8(%ebp),%eax
  8006be:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006c1:	89 c2                	mov    %eax,%edx
  8006c3:	42                   	inc    %edx
  8006c4:	41                   	inc    %ecx
  8006c5:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006c8:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006cb:	84 db                	test   %bl,%bl
  8006cd:	75 f4                	jne    8006c3 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006cf:	5b                   	pop    %ebx
  8006d0:	5d                   	pop    %ebp
  8006d1:	c3                   	ret    

008006d2 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006d2:	55                   	push   %ebp
  8006d3:	89 e5                	mov    %esp,%ebp
  8006d5:	53                   	push   %ebx
  8006d6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006d9:	53                   	push   %ebx
  8006da:	e8 a3 ff ff ff       	call   800682 <strlen>
  8006df:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006e2:	ff 75 0c             	pushl  0xc(%ebp)
  8006e5:	01 d8                	add    %ebx,%eax
  8006e7:	50                   	push   %eax
  8006e8:	e8 ca ff ff ff       	call   8006b7 <strcpy>
	return dst;
}
  8006ed:	89 d8                	mov    %ebx,%eax
  8006ef:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006f2:	c9                   	leave  
  8006f3:	c3                   	ret    

008006f4 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8006f4:	55                   	push   %ebp
  8006f5:	89 e5                	mov    %esp,%ebp
  8006f7:	56                   	push   %esi
  8006f8:	53                   	push   %ebx
  8006f9:	8b 75 08             	mov    0x8(%ebp),%esi
  8006fc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006ff:	89 f3                	mov    %esi,%ebx
  800701:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800704:	89 f2                	mov    %esi,%edx
  800706:	eb 0c                	jmp    800714 <strncpy+0x20>
		*dst++ = *src;
  800708:	42                   	inc    %edx
  800709:	8a 01                	mov    (%ecx),%al
  80070b:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80070e:	80 39 01             	cmpb   $0x1,(%ecx)
  800711:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800714:	39 da                	cmp    %ebx,%edx
  800716:	75 f0                	jne    800708 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800718:	89 f0                	mov    %esi,%eax
  80071a:	5b                   	pop    %ebx
  80071b:	5e                   	pop    %esi
  80071c:	5d                   	pop    %ebp
  80071d:	c3                   	ret    

0080071e <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80071e:	55                   	push   %ebp
  80071f:	89 e5                	mov    %esp,%ebp
  800721:	56                   	push   %esi
  800722:	53                   	push   %ebx
  800723:	8b 75 08             	mov    0x8(%ebp),%esi
  800726:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800729:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80072c:	85 c0                	test   %eax,%eax
  80072e:	74 1e                	je     80074e <strlcpy+0x30>
  800730:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800734:	89 f2                	mov    %esi,%edx
  800736:	eb 05                	jmp    80073d <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800738:	42                   	inc    %edx
  800739:	41                   	inc    %ecx
  80073a:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80073d:	39 c2                	cmp    %eax,%edx
  80073f:	74 08                	je     800749 <strlcpy+0x2b>
  800741:	8a 19                	mov    (%ecx),%bl
  800743:	84 db                	test   %bl,%bl
  800745:	75 f1                	jne    800738 <strlcpy+0x1a>
  800747:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800749:	c6 00 00             	movb   $0x0,(%eax)
  80074c:	eb 02                	jmp    800750 <strlcpy+0x32>
  80074e:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800750:	29 f0                	sub    %esi,%eax
}
  800752:	5b                   	pop    %ebx
  800753:	5e                   	pop    %esi
  800754:	5d                   	pop    %ebp
  800755:	c3                   	ret    

00800756 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800756:	55                   	push   %ebp
  800757:	89 e5                	mov    %esp,%ebp
  800759:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80075c:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80075f:	eb 02                	jmp    800763 <strcmp+0xd>
		p++, q++;
  800761:	41                   	inc    %ecx
  800762:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800763:	8a 01                	mov    (%ecx),%al
  800765:	84 c0                	test   %al,%al
  800767:	74 04                	je     80076d <strcmp+0x17>
  800769:	3a 02                	cmp    (%edx),%al
  80076b:	74 f4                	je     800761 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80076d:	0f b6 c0             	movzbl %al,%eax
  800770:	0f b6 12             	movzbl (%edx),%edx
  800773:	29 d0                	sub    %edx,%eax
}
  800775:	5d                   	pop    %ebp
  800776:	c3                   	ret    

00800777 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800777:	55                   	push   %ebp
  800778:	89 e5                	mov    %esp,%ebp
  80077a:	53                   	push   %ebx
  80077b:	8b 45 08             	mov    0x8(%ebp),%eax
  80077e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800781:	89 c3                	mov    %eax,%ebx
  800783:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800786:	eb 02                	jmp    80078a <strncmp+0x13>
		n--, p++, q++;
  800788:	40                   	inc    %eax
  800789:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80078a:	39 d8                	cmp    %ebx,%eax
  80078c:	74 14                	je     8007a2 <strncmp+0x2b>
  80078e:	8a 08                	mov    (%eax),%cl
  800790:	84 c9                	test   %cl,%cl
  800792:	74 04                	je     800798 <strncmp+0x21>
  800794:	3a 0a                	cmp    (%edx),%cl
  800796:	74 f0                	je     800788 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800798:	0f b6 00             	movzbl (%eax),%eax
  80079b:	0f b6 12             	movzbl (%edx),%edx
  80079e:	29 d0                	sub    %edx,%eax
  8007a0:	eb 05                	jmp    8007a7 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007a2:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007a7:	5b                   	pop    %ebx
  8007a8:	5d                   	pop    %ebp
  8007a9:	c3                   	ret    

008007aa <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007aa:	55                   	push   %ebp
  8007ab:	89 e5                	mov    %esp,%ebp
  8007ad:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b0:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007b3:	eb 05                	jmp    8007ba <strchr+0x10>
		if (*s == c)
  8007b5:	38 ca                	cmp    %cl,%dl
  8007b7:	74 0c                	je     8007c5 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007b9:	40                   	inc    %eax
  8007ba:	8a 10                	mov    (%eax),%dl
  8007bc:	84 d2                	test   %dl,%dl
  8007be:	75 f5                	jne    8007b5 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007c0:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007c5:	5d                   	pop    %ebp
  8007c6:	c3                   	ret    

008007c7 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007c7:	55                   	push   %ebp
  8007c8:	89 e5                	mov    %esp,%ebp
  8007ca:	8b 45 08             	mov    0x8(%ebp),%eax
  8007cd:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007d0:	eb 05                	jmp    8007d7 <strfind+0x10>
		if (*s == c)
  8007d2:	38 ca                	cmp    %cl,%dl
  8007d4:	74 07                	je     8007dd <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007d6:	40                   	inc    %eax
  8007d7:	8a 10                	mov    (%eax),%dl
  8007d9:	84 d2                	test   %dl,%dl
  8007db:	75 f5                	jne    8007d2 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007dd:	5d                   	pop    %ebp
  8007de:	c3                   	ret    

008007df <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007df:	55                   	push   %ebp
  8007e0:	89 e5                	mov    %esp,%ebp
  8007e2:	57                   	push   %edi
  8007e3:	56                   	push   %esi
  8007e4:	53                   	push   %ebx
  8007e5:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007e8:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007eb:	85 c9                	test   %ecx,%ecx
  8007ed:	74 36                	je     800825 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8007ef:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8007f5:	75 28                	jne    80081f <memset+0x40>
  8007f7:	f6 c1 03             	test   $0x3,%cl
  8007fa:	75 23                	jne    80081f <memset+0x40>
		c &= 0xFF;
  8007fc:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800800:	89 d3                	mov    %edx,%ebx
  800802:	c1 e3 08             	shl    $0x8,%ebx
  800805:	89 d6                	mov    %edx,%esi
  800807:	c1 e6 18             	shl    $0x18,%esi
  80080a:	89 d0                	mov    %edx,%eax
  80080c:	c1 e0 10             	shl    $0x10,%eax
  80080f:	09 f0                	or     %esi,%eax
  800811:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800813:	89 d8                	mov    %ebx,%eax
  800815:	09 d0                	or     %edx,%eax
  800817:	c1 e9 02             	shr    $0x2,%ecx
  80081a:	fc                   	cld    
  80081b:	f3 ab                	rep stos %eax,%es:(%edi)
  80081d:	eb 06                	jmp    800825 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80081f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800822:	fc                   	cld    
  800823:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800825:	89 f8                	mov    %edi,%eax
  800827:	5b                   	pop    %ebx
  800828:	5e                   	pop    %esi
  800829:	5f                   	pop    %edi
  80082a:	5d                   	pop    %ebp
  80082b:	c3                   	ret    

0080082c <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80082c:	55                   	push   %ebp
  80082d:	89 e5                	mov    %esp,%ebp
  80082f:	57                   	push   %edi
  800830:	56                   	push   %esi
  800831:	8b 45 08             	mov    0x8(%ebp),%eax
  800834:	8b 75 0c             	mov    0xc(%ebp),%esi
  800837:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80083a:	39 c6                	cmp    %eax,%esi
  80083c:	73 33                	jae    800871 <memmove+0x45>
  80083e:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800841:	39 d0                	cmp    %edx,%eax
  800843:	73 2c                	jae    800871 <memmove+0x45>
		s += n;
		d += n;
  800845:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800848:	89 d6                	mov    %edx,%esi
  80084a:	09 fe                	or     %edi,%esi
  80084c:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800852:	75 13                	jne    800867 <memmove+0x3b>
  800854:	f6 c1 03             	test   $0x3,%cl
  800857:	75 0e                	jne    800867 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800859:	83 ef 04             	sub    $0x4,%edi
  80085c:	8d 72 fc             	lea    -0x4(%edx),%esi
  80085f:	c1 e9 02             	shr    $0x2,%ecx
  800862:	fd                   	std    
  800863:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800865:	eb 07                	jmp    80086e <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800867:	4f                   	dec    %edi
  800868:	8d 72 ff             	lea    -0x1(%edx),%esi
  80086b:	fd                   	std    
  80086c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80086e:	fc                   	cld    
  80086f:	eb 1d                	jmp    80088e <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800871:	89 f2                	mov    %esi,%edx
  800873:	09 c2                	or     %eax,%edx
  800875:	f6 c2 03             	test   $0x3,%dl
  800878:	75 0f                	jne    800889 <memmove+0x5d>
  80087a:	f6 c1 03             	test   $0x3,%cl
  80087d:	75 0a                	jne    800889 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80087f:	c1 e9 02             	shr    $0x2,%ecx
  800882:	89 c7                	mov    %eax,%edi
  800884:	fc                   	cld    
  800885:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800887:	eb 05                	jmp    80088e <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800889:	89 c7                	mov    %eax,%edi
  80088b:	fc                   	cld    
  80088c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80088e:	5e                   	pop    %esi
  80088f:	5f                   	pop    %edi
  800890:	5d                   	pop    %ebp
  800891:	c3                   	ret    

00800892 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800892:	55                   	push   %ebp
  800893:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800895:	ff 75 10             	pushl  0x10(%ebp)
  800898:	ff 75 0c             	pushl  0xc(%ebp)
  80089b:	ff 75 08             	pushl  0x8(%ebp)
  80089e:	e8 89 ff ff ff       	call   80082c <memmove>
}
  8008a3:	c9                   	leave  
  8008a4:	c3                   	ret    

008008a5 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008a5:	55                   	push   %ebp
  8008a6:	89 e5                	mov    %esp,%ebp
  8008a8:	56                   	push   %esi
  8008a9:	53                   	push   %ebx
  8008aa:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ad:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008b0:	89 c6                	mov    %eax,%esi
  8008b2:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008b5:	eb 14                	jmp    8008cb <memcmp+0x26>
		if (*s1 != *s2)
  8008b7:	8a 08                	mov    (%eax),%cl
  8008b9:	8a 1a                	mov    (%edx),%bl
  8008bb:	38 d9                	cmp    %bl,%cl
  8008bd:	74 0a                	je     8008c9 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008bf:	0f b6 c1             	movzbl %cl,%eax
  8008c2:	0f b6 db             	movzbl %bl,%ebx
  8008c5:	29 d8                	sub    %ebx,%eax
  8008c7:	eb 0b                	jmp    8008d4 <memcmp+0x2f>
		s1++, s2++;
  8008c9:	40                   	inc    %eax
  8008ca:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008cb:	39 f0                	cmp    %esi,%eax
  8008cd:	75 e8                	jne    8008b7 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008cf:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008d4:	5b                   	pop    %ebx
  8008d5:	5e                   	pop    %esi
  8008d6:	5d                   	pop    %ebp
  8008d7:	c3                   	ret    

008008d8 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008d8:	55                   	push   %ebp
  8008d9:	89 e5                	mov    %esp,%ebp
  8008db:	53                   	push   %ebx
  8008dc:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008df:	89 c1                	mov    %eax,%ecx
  8008e1:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008e4:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008e8:	eb 08                	jmp    8008f2 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008ea:	0f b6 10             	movzbl (%eax),%edx
  8008ed:	39 da                	cmp    %ebx,%edx
  8008ef:	74 05                	je     8008f6 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008f1:	40                   	inc    %eax
  8008f2:	39 c8                	cmp    %ecx,%eax
  8008f4:	72 f4                	jb     8008ea <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8008f6:	5b                   	pop    %ebx
  8008f7:	5d                   	pop    %ebp
  8008f8:	c3                   	ret    

008008f9 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8008f9:	55                   	push   %ebp
  8008fa:	89 e5                	mov    %esp,%ebp
  8008fc:	57                   	push   %edi
  8008fd:	56                   	push   %esi
  8008fe:	53                   	push   %ebx
  8008ff:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800902:	eb 01                	jmp    800905 <strtol+0xc>
		s++;
  800904:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800905:	8a 01                	mov    (%ecx),%al
  800907:	3c 20                	cmp    $0x20,%al
  800909:	74 f9                	je     800904 <strtol+0xb>
  80090b:	3c 09                	cmp    $0x9,%al
  80090d:	74 f5                	je     800904 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80090f:	3c 2b                	cmp    $0x2b,%al
  800911:	75 08                	jne    80091b <strtol+0x22>
		s++;
  800913:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800914:	bf 00 00 00 00       	mov    $0x0,%edi
  800919:	eb 11                	jmp    80092c <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80091b:	3c 2d                	cmp    $0x2d,%al
  80091d:	75 08                	jne    800927 <strtol+0x2e>
		s++, neg = 1;
  80091f:	41                   	inc    %ecx
  800920:	bf 01 00 00 00       	mov    $0x1,%edi
  800925:	eb 05                	jmp    80092c <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800927:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80092c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800930:	0f 84 87 00 00 00    	je     8009bd <strtol+0xc4>
  800936:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80093a:	75 27                	jne    800963 <strtol+0x6a>
  80093c:	80 39 30             	cmpb   $0x30,(%ecx)
  80093f:	75 22                	jne    800963 <strtol+0x6a>
  800941:	e9 88 00 00 00       	jmp    8009ce <strtol+0xd5>
		s += 2, base = 16;
  800946:	83 c1 02             	add    $0x2,%ecx
  800949:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800950:	eb 11                	jmp    800963 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800952:	41                   	inc    %ecx
  800953:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80095a:	eb 07                	jmp    800963 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  80095c:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800963:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800968:	8a 11                	mov    (%ecx),%dl
  80096a:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80096d:	80 fb 09             	cmp    $0x9,%bl
  800970:	77 08                	ja     80097a <strtol+0x81>
			dig = *s - '0';
  800972:	0f be d2             	movsbl %dl,%edx
  800975:	83 ea 30             	sub    $0x30,%edx
  800978:	eb 22                	jmp    80099c <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80097a:	8d 72 9f             	lea    -0x61(%edx),%esi
  80097d:	89 f3                	mov    %esi,%ebx
  80097f:	80 fb 19             	cmp    $0x19,%bl
  800982:	77 08                	ja     80098c <strtol+0x93>
			dig = *s - 'a' + 10;
  800984:	0f be d2             	movsbl %dl,%edx
  800987:	83 ea 57             	sub    $0x57,%edx
  80098a:	eb 10                	jmp    80099c <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  80098c:	8d 72 bf             	lea    -0x41(%edx),%esi
  80098f:	89 f3                	mov    %esi,%ebx
  800991:	80 fb 19             	cmp    $0x19,%bl
  800994:	77 14                	ja     8009aa <strtol+0xb1>
			dig = *s - 'A' + 10;
  800996:	0f be d2             	movsbl %dl,%edx
  800999:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  80099c:	3b 55 10             	cmp    0x10(%ebp),%edx
  80099f:	7d 09                	jge    8009aa <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009a1:	41                   	inc    %ecx
  8009a2:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009a6:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009a8:	eb be                	jmp    800968 <strtol+0x6f>

	if (endptr)
  8009aa:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009ae:	74 05                	je     8009b5 <strtol+0xbc>
		*endptr = (char *) s;
  8009b0:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009b3:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009b5:	85 ff                	test   %edi,%edi
  8009b7:	74 21                	je     8009da <strtol+0xe1>
  8009b9:	f7 d8                	neg    %eax
  8009bb:	eb 1d                	jmp    8009da <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009bd:	80 39 30             	cmpb   $0x30,(%ecx)
  8009c0:	75 9a                	jne    80095c <strtol+0x63>
  8009c2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009c6:	0f 84 7a ff ff ff    	je     800946 <strtol+0x4d>
  8009cc:	eb 84                	jmp    800952 <strtol+0x59>
  8009ce:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009d2:	0f 84 6e ff ff ff    	je     800946 <strtol+0x4d>
  8009d8:	eb 89                	jmp    800963 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009da:	5b                   	pop    %ebx
  8009db:	5e                   	pop    %esi
  8009dc:	5f                   	pop    %edi
  8009dd:	5d                   	pop    %ebp
  8009de:	c3                   	ret    

008009df <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009df:	55                   	push   %ebp
  8009e0:	89 e5                	mov    %esp,%ebp
  8009e2:	57                   	push   %edi
  8009e3:	56                   	push   %esi
  8009e4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009e5:	b8 00 00 00 00       	mov    $0x0,%eax
  8009ea:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009ed:	8b 55 08             	mov    0x8(%ebp),%edx
  8009f0:	89 c3                	mov    %eax,%ebx
  8009f2:	89 c7                	mov    %eax,%edi
  8009f4:	89 c6                	mov    %eax,%esi
  8009f6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8009f8:	5b                   	pop    %ebx
  8009f9:	5e                   	pop    %esi
  8009fa:	5f                   	pop    %edi
  8009fb:	5d                   	pop    %ebp
  8009fc:	c3                   	ret    

008009fd <sys_cgetc>:

int
sys_cgetc(void)
{
  8009fd:	55                   	push   %ebp
  8009fe:	89 e5                	mov    %esp,%ebp
  800a00:	57                   	push   %edi
  800a01:	56                   	push   %esi
  800a02:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a03:	ba 00 00 00 00       	mov    $0x0,%edx
  800a08:	b8 01 00 00 00       	mov    $0x1,%eax
  800a0d:	89 d1                	mov    %edx,%ecx
  800a0f:	89 d3                	mov    %edx,%ebx
  800a11:	89 d7                	mov    %edx,%edi
  800a13:	89 d6                	mov    %edx,%esi
  800a15:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a17:	5b                   	pop    %ebx
  800a18:	5e                   	pop    %esi
  800a19:	5f                   	pop    %edi
  800a1a:	5d                   	pop    %ebp
  800a1b:	c3                   	ret    

00800a1c <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a1c:	55                   	push   %ebp
  800a1d:	89 e5                	mov    %esp,%ebp
  800a1f:	57                   	push   %edi
  800a20:	56                   	push   %esi
  800a21:	53                   	push   %ebx
  800a22:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a25:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a2a:	b8 03 00 00 00       	mov    $0x3,%eax
  800a2f:	8b 55 08             	mov    0x8(%ebp),%edx
  800a32:	89 cb                	mov    %ecx,%ebx
  800a34:	89 cf                	mov    %ecx,%edi
  800a36:	89 ce                	mov    %ecx,%esi
  800a38:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a3a:	85 c0                	test   %eax,%eax
  800a3c:	7e 17                	jle    800a55 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a3e:	83 ec 0c             	sub    $0xc,%esp
  800a41:	50                   	push   %eax
  800a42:	6a 03                	push   $0x3
  800a44:	68 7f 12 80 00       	push   $0x80127f
  800a49:	6a 23                	push   $0x23
  800a4b:	68 9c 12 80 00       	push   $0x80129c
  800a50:	e8 56 02 00 00       	call   800cab <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a55:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a58:	5b                   	pop    %ebx
  800a59:	5e                   	pop    %esi
  800a5a:	5f                   	pop    %edi
  800a5b:	5d                   	pop    %ebp
  800a5c:	c3                   	ret    

00800a5d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a5d:	55                   	push   %ebp
  800a5e:	89 e5                	mov    %esp,%ebp
  800a60:	57                   	push   %edi
  800a61:	56                   	push   %esi
  800a62:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a63:	ba 00 00 00 00       	mov    $0x0,%edx
  800a68:	b8 02 00 00 00       	mov    $0x2,%eax
  800a6d:	89 d1                	mov    %edx,%ecx
  800a6f:	89 d3                	mov    %edx,%ebx
  800a71:	89 d7                	mov    %edx,%edi
  800a73:	89 d6                	mov    %edx,%esi
  800a75:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a77:	5b                   	pop    %ebx
  800a78:	5e                   	pop    %esi
  800a79:	5f                   	pop    %edi
  800a7a:	5d                   	pop    %ebp
  800a7b:	c3                   	ret    

00800a7c <sys_yield>:

void
sys_yield(void)
{
  800a7c:	55                   	push   %ebp
  800a7d:	89 e5                	mov    %esp,%ebp
  800a7f:	57                   	push   %edi
  800a80:	56                   	push   %esi
  800a81:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a82:	ba 00 00 00 00       	mov    $0x0,%edx
  800a87:	b8 0b 00 00 00       	mov    $0xb,%eax
  800a8c:	89 d1                	mov    %edx,%ecx
  800a8e:	89 d3                	mov    %edx,%ebx
  800a90:	89 d7                	mov    %edx,%edi
  800a92:	89 d6                	mov    %edx,%esi
  800a94:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800a96:	5b                   	pop    %ebx
  800a97:	5e                   	pop    %esi
  800a98:	5f                   	pop    %edi
  800a99:	5d                   	pop    %ebp
  800a9a:	c3                   	ret    

00800a9b <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800a9b:	55                   	push   %ebp
  800a9c:	89 e5                	mov    %esp,%ebp
  800a9e:	57                   	push   %edi
  800a9f:	56                   	push   %esi
  800aa0:	53                   	push   %ebx
  800aa1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aa4:	be 00 00 00 00       	mov    $0x0,%esi
  800aa9:	b8 04 00 00 00       	mov    $0x4,%eax
  800aae:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ab1:	8b 55 08             	mov    0x8(%ebp),%edx
  800ab4:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ab7:	89 f7                	mov    %esi,%edi
  800ab9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800abb:	85 c0                	test   %eax,%eax
  800abd:	7e 17                	jle    800ad6 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800abf:	83 ec 0c             	sub    $0xc,%esp
  800ac2:	50                   	push   %eax
  800ac3:	6a 04                	push   $0x4
  800ac5:	68 7f 12 80 00       	push   $0x80127f
  800aca:	6a 23                	push   $0x23
  800acc:	68 9c 12 80 00       	push   $0x80129c
  800ad1:	e8 d5 01 00 00       	call   800cab <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800ad6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ad9:	5b                   	pop    %ebx
  800ada:	5e                   	pop    %esi
  800adb:	5f                   	pop    %edi
  800adc:	5d                   	pop    %ebp
  800add:	c3                   	ret    

00800ade <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800ade:	55                   	push   %ebp
  800adf:	89 e5                	mov    %esp,%ebp
  800ae1:	57                   	push   %edi
  800ae2:	56                   	push   %esi
  800ae3:	53                   	push   %ebx
  800ae4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae7:	b8 05 00 00 00       	mov    $0x5,%eax
  800aec:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800aef:	8b 55 08             	mov    0x8(%ebp),%edx
  800af2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800af5:	8b 7d 14             	mov    0x14(%ebp),%edi
  800af8:	8b 75 18             	mov    0x18(%ebp),%esi
  800afb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800afd:	85 c0                	test   %eax,%eax
  800aff:	7e 17                	jle    800b18 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b01:	83 ec 0c             	sub    $0xc,%esp
  800b04:	50                   	push   %eax
  800b05:	6a 05                	push   $0x5
  800b07:	68 7f 12 80 00       	push   $0x80127f
  800b0c:	6a 23                	push   $0x23
  800b0e:	68 9c 12 80 00       	push   $0x80129c
  800b13:	e8 93 01 00 00       	call   800cab <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b18:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b1b:	5b                   	pop    %ebx
  800b1c:	5e                   	pop    %esi
  800b1d:	5f                   	pop    %edi
  800b1e:	5d                   	pop    %ebp
  800b1f:	c3                   	ret    

00800b20 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b20:	55                   	push   %ebp
  800b21:	89 e5                	mov    %esp,%ebp
  800b23:	57                   	push   %edi
  800b24:	56                   	push   %esi
  800b25:	53                   	push   %ebx
  800b26:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b29:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b2e:	b8 06 00 00 00       	mov    $0x6,%eax
  800b33:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b36:	8b 55 08             	mov    0x8(%ebp),%edx
  800b39:	89 df                	mov    %ebx,%edi
  800b3b:	89 de                	mov    %ebx,%esi
  800b3d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b3f:	85 c0                	test   %eax,%eax
  800b41:	7e 17                	jle    800b5a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b43:	83 ec 0c             	sub    $0xc,%esp
  800b46:	50                   	push   %eax
  800b47:	6a 06                	push   $0x6
  800b49:	68 7f 12 80 00       	push   $0x80127f
  800b4e:	6a 23                	push   $0x23
  800b50:	68 9c 12 80 00       	push   $0x80129c
  800b55:	e8 51 01 00 00       	call   800cab <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800b5a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b5d:	5b                   	pop    %ebx
  800b5e:	5e                   	pop    %esi
  800b5f:	5f                   	pop    %edi
  800b60:	5d                   	pop    %ebp
  800b61:	c3                   	ret    

00800b62 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800b62:	55                   	push   %ebp
  800b63:	89 e5                	mov    %esp,%ebp
  800b65:	57                   	push   %edi
  800b66:	56                   	push   %esi
  800b67:	53                   	push   %ebx
  800b68:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b6b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b70:	b8 08 00 00 00       	mov    $0x8,%eax
  800b75:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b78:	8b 55 08             	mov    0x8(%ebp),%edx
  800b7b:	89 df                	mov    %ebx,%edi
  800b7d:	89 de                	mov    %ebx,%esi
  800b7f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b81:	85 c0                	test   %eax,%eax
  800b83:	7e 17                	jle    800b9c <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b85:	83 ec 0c             	sub    $0xc,%esp
  800b88:	50                   	push   %eax
  800b89:	6a 08                	push   $0x8
  800b8b:	68 7f 12 80 00       	push   $0x80127f
  800b90:	6a 23                	push   $0x23
  800b92:	68 9c 12 80 00       	push   $0x80129c
  800b97:	e8 0f 01 00 00       	call   800cab <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800b9c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b9f:	5b                   	pop    %ebx
  800ba0:	5e                   	pop    %esi
  800ba1:	5f                   	pop    %edi
  800ba2:	5d                   	pop    %ebp
  800ba3:	c3                   	ret    

00800ba4 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800ba4:	55                   	push   %ebp
  800ba5:	89 e5                	mov    %esp,%ebp
  800ba7:	57                   	push   %edi
  800ba8:	56                   	push   %esi
  800ba9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800baa:	ba 00 00 00 00       	mov    $0x0,%edx
  800baf:	b8 0c 00 00 00       	mov    $0xc,%eax
  800bb4:	89 d1                	mov    %edx,%ecx
  800bb6:	89 d3                	mov    %edx,%ebx
  800bb8:	89 d7                	mov    %edx,%edi
  800bba:	89 d6                	mov    %edx,%esi
  800bbc:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800bbe:	5b                   	pop    %ebx
  800bbf:	5e                   	pop    %esi
  800bc0:	5f                   	pop    %edi
  800bc1:	5d                   	pop    %ebp
  800bc2:	c3                   	ret    

00800bc3 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800bc3:	55                   	push   %ebp
  800bc4:	89 e5                	mov    %esp,%ebp
  800bc6:	57                   	push   %edi
  800bc7:	56                   	push   %esi
  800bc8:	53                   	push   %ebx
  800bc9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bcc:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bd1:	b8 09 00 00 00       	mov    $0x9,%eax
  800bd6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bd9:	8b 55 08             	mov    0x8(%ebp),%edx
  800bdc:	89 df                	mov    %ebx,%edi
  800bde:	89 de                	mov    %ebx,%esi
  800be0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800be2:	85 c0                	test   %eax,%eax
  800be4:	7e 17                	jle    800bfd <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800be6:	83 ec 0c             	sub    $0xc,%esp
  800be9:	50                   	push   %eax
  800bea:	6a 09                	push   $0x9
  800bec:	68 7f 12 80 00       	push   $0x80127f
  800bf1:	6a 23                	push   $0x23
  800bf3:	68 9c 12 80 00       	push   $0x80129c
  800bf8:	e8 ae 00 00 00       	call   800cab <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800bfd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c00:	5b                   	pop    %ebx
  800c01:	5e                   	pop    %esi
  800c02:	5f                   	pop    %edi
  800c03:	5d                   	pop    %ebp
  800c04:	c3                   	ret    

00800c05 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c05:	55                   	push   %ebp
  800c06:	89 e5                	mov    %esp,%ebp
  800c08:	57                   	push   %edi
  800c09:	56                   	push   %esi
  800c0a:	53                   	push   %ebx
  800c0b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c0e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c13:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c18:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c1b:	8b 55 08             	mov    0x8(%ebp),%edx
  800c1e:	89 df                	mov    %ebx,%edi
  800c20:	89 de                	mov    %ebx,%esi
  800c22:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c24:	85 c0                	test   %eax,%eax
  800c26:	7e 17                	jle    800c3f <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c28:	83 ec 0c             	sub    $0xc,%esp
  800c2b:	50                   	push   %eax
  800c2c:	6a 0a                	push   $0xa
  800c2e:	68 7f 12 80 00       	push   $0x80127f
  800c33:	6a 23                	push   $0x23
  800c35:	68 9c 12 80 00       	push   $0x80129c
  800c3a:	e8 6c 00 00 00       	call   800cab <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800c3f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c42:	5b                   	pop    %ebx
  800c43:	5e                   	pop    %esi
  800c44:	5f                   	pop    %edi
  800c45:	5d                   	pop    %ebp
  800c46:	c3                   	ret    

00800c47 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800c47:	55                   	push   %ebp
  800c48:	89 e5                	mov    %esp,%ebp
  800c4a:	57                   	push   %edi
  800c4b:	56                   	push   %esi
  800c4c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c4d:	be 00 00 00 00       	mov    $0x0,%esi
  800c52:	b8 0d 00 00 00       	mov    $0xd,%eax
  800c57:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c5a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c5d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c60:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c63:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800c65:	5b                   	pop    %ebx
  800c66:	5e                   	pop    %esi
  800c67:	5f                   	pop    %edi
  800c68:	5d                   	pop    %ebp
  800c69:	c3                   	ret    

00800c6a <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800c6a:	55                   	push   %ebp
  800c6b:	89 e5                	mov    %esp,%ebp
  800c6d:	57                   	push   %edi
  800c6e:	56                   	push   %esi
  800c6f:	53                   	push   %ebx
  800c70:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c73:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c78:	b8 0e 00 00 00       	mov    $0xe,%eax
  800c7d:	8b 55 08             	mov    0x8(%ebp),%edx
  800c80:	89 cb                	mov    %ecx,%ebx
  800c82:	89 cf                	mov    %ecx,%edi
  800c84:	89 ce                	mov    %ecx,%esi
  800c86:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c88:	85 c0                	test   %eax,%eax
  800c8a:	7e 17                	jle    800ca3 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c8c:	83 ec 0c             	sub    $0xc,%esp
  800c8f:	50                   	push   %eax
  800c90:	6a 0e                	push   $0xe
  800c92:	68 7f 12 80 00       	push   $0x80127f
  800c97:	6a 23                	push   $0x23
  800c99:	68 9c 12 80 00       	push   $0x80129c
  800c9e:	e8 08 00 00 00       	call   800cab <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800ca3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ca6:	5b                   	pop    %ebx
  800ca7:	5e                   	pop    %esi
  800ca8:	5f                   	pop    %edi
  800ca9:	5d                   	pop    %ebp
  800caa:	c3                   	ret    

00800cab <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800cab:	55                   	push   %ebp
  800cac:	89 e5                	mov    %esp,%ebp
  800cae:	56                   	push   %esi
  800caf:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800cb0:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800cb3:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800cb9:	e8 9f fd ff ff       	call   800a5d <sys_getenvid>
  800cbe:	83 ec 0c             	sub    $0xc,%esp
  800cc1:	ff 75 0c             	pushl  0xc(%ebp)
  800cc4:	ff 75 08             	pushl  0x8(%ebp)
  800cc7:	56                   	push   %esi
  800cc8:	50                   	push   %eax
  800cc9:	68 ac 12 80 00       	push   $0x8012ac
  800cce:	e8 80 f4 ff ff       	call   800153 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800cd3:	83 c4 18             	add    $0x18,%esp
  800cd6:	53                   	push   %ebx
  800cd7:	ff 75 10             	pushl  0x10(%ebp)
  800cda:	e8 23 f4 ff ff       	call   800102 <vcprintf>
	cprintf("\n");
  800cdf:	c7 04 24 6c 0f 80 00 	movl   $0x800f6c,(%esp)
  800ce6:	e8 68 f4 ff ff       	call   800153 <cprintf>
  800ceb:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800cee:	cc                   	int3   
  800cef:	eb fd                	jmp    800cee <_panic+0x43>
  800cf1:	66 90                	xchg   %ax,%ax
  800cf3:	90                   	nop

00800cf4 <__udivdi3>:
  800cf4:	55                   	push   %ebp
  800cf5:	57                   	push   %edi
  800cf6:	56                   	push   %esi
  800cf7:	53                   	push   %ebx
  800cf8:	83 ec 1c             	sub    $0x1c,%esp
  800cfb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cff:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d03:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d07:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d0b:	89 ca                	mov    %ecx,%edx
  800d0d:	89 f8                	mov    %edi,%eax
  800d0f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800d13:	85 f6                	test   %esi,%esi
  800d15:	75 2d                	jne    800d44 <__udivdi3+0x50>
  800d17:	39 cf                	cmp    %ecx,%edi
  800d19:	77 65                	ja     800d80 <__udivdi3+0x8c>
  800d1b:	89 fd                	mov    %edi,%ebp
  800d1d:	85 ff                	test   %edi,%edi
  800d1f:	75 0b                	jne    800d2c <__udivdi3+0x38>
  800d21:	b8 01 00 00 00       	mov    $0x1,%eax
  800d26:	31 d2                	xor    %edx,%edx
  800d28:	f7 f7                	div    %edi
  800d2a:	89 c5                	mov    %eax,%ebp
  800d2c:	31 d2                	xor    %edx,%edx
  800d2e:	89 c8                	mov    %ecx,%eax
  800d30:	f7 f5                	div    %ebp
  800d32:	89 c1                	mov    %eax,%ecx
  800d34:	89 d8                	mov    %ebx,%eax
  800d36:	f7 f5                	div    %ebp
  800d38:	89 cf                	mov    %ecx,%edi
  800d3a:	89 fa                	mov    %edi,%edx
  800d3c:	83 c4 1c             	add    $0x1c,%esp
  800d3f:	5b                   	pop    %ebx
  800d40:	5e                   	pop    %esi
  800d41:	5f                   	pop    %edi
  800d42:	5d                   	pop    %ebp
  800d43:	c3                   	ret    
  800d44:	39 ce                	cmp    %ecx,%esi
  800d46:	77 28                	ja     800d70 <__udivdi3+0x7c>
  800d48:	0f bd fe             	bsr    %esi,%edi
  800d4b:	83 f7 1f             	xor    $0x1f,%edi
  800d4e:	75 40                	jne    800d90 <__udivdi3+0x9c>
  800d50:	39 ce                	cmp    %ecx,%esi
  800d52:	72 0a                	jb     800d5e <__udivdi3+0x6a>
  800d54:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d58:	0f 87 9e 00 00 00    	ja     800dfc <__udivdi3+0x108>
  800d5e:	b8 01 00 00 00       	mov    $0x1,%eax
  800d63:	89 fa                	mov    %edi,%edx
  800d65:	83 c4 1c             	add    $0x1c,%esp
  800d68:	5b                   	pop    %ebx
  800d69:	5e                   	pop    %esi
  800d6a:	5f                   	pop    %edi
  800d6b:	5d                   	pop    %ebp
  800d6c:	c3                   	ret    
  800d6d:	8d 76 00             	lea    0x0(%esi),%esi
  800d70:	31 ff                	xor    %edi,%edi
  800d72:	31 c0                	xor    %eax,%eax
  800d74:	89 fa                	mov    %edi,%edx
  800d76:	83 c4 1c             	add    $0x1c,%esp
  800d79:	5b                   	pop    %ebx
  800d7a:	5e                   	pop    %esi
  800d7b:	5f                   	pop    %edi
  800d7c:	5d                   	pop    %ebp
  800d7d:	c3                   	ret    
  800d7e:	66 90                	xchg   %ax,%ax
  800d80:	89 d8                	mov    %ebx,%eax
  800d82:	f7 f7                	div    %edi
  800d84:	31 ff                	xor    %edi,%edi
  800d86:	89 fa                	mov    %edi,%edx
  800d88:	83 c4 1c             	add    $0x1c,%esp
  800d8b:	5b                   	pop    %ebx
  800d8c:	5e                   	pop    %esi
  800d8d:	5f                   	pop    %edi
  800d8e:	5d                   	pop    %ebp
  800d8f:	c3                   	ret    
  800d90:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d95:	89 eb                	mov    %ebp,%ebx
  800d97:	29 fb                	sub    %edi,%ebx
  800d99:	89 f9                	mov    %edi,%ecx
  800d9b:	d3 e6                	shl    %cl,%esi
  800d9d:	89 c5                	mov    %eax,%ebp
  800d9f:	88 d9                	mov    %bl,%cl
  800da1:	d3 ed                	shr    %cl,%ebp
  800da3:	89 e9                	mov    %ebp,%ecx
  800da5:	09 f1                	or     %esi,%ecx
  800da7:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800dab:	89 f9                	mov    %edi,%ecx
  800dad:	d3 e0                	shl    %cl,%eax
  800daf:	89 c5                	mov    %eax,%ebp
  800db1:	89 d6                	mov    %edx,%esi
  800db3:	88 d9                	mov    %bl,%cl
  800db5:	d3 ee                	shr    %cl,%esi
  800db7:	89 f9                	mov    %edi,%ecx
  800db9:	d3 e2                	shl    %cl,%edx
  800dbb:	8b 44 24 08          	mov    0x8(%esp),%eax
  800dbf:	88 d9                	mov    %bl,%cl
  800dc1:	d3 e8                	shr    %cl,%eax
  800dc3:	09 c2                	or     %eax,%edx
  800dc5:	89 d0                	mov    %edx,%eax
  800dc7:	89 f2                	mov    %esi,%edx
  800dc9:	f7 74 24 0c          	divl   0xc(%esp)
  800dcd:	89 d6                	mov    %edx,%esi
  800dcf:	89 c3                	mov    %eax,%ebx
  800dd1:	f7 e5                	mul    %ebp
  800dd3:	39 d6                	cmp    %edx,%esi
  800dd5:	72 19                	jb     800df0 <__udivdi3+0xfc>
  800dd7:	74 0b                	je     800de4 <__udivdi3+0xf0>
  800dd9:	89 d8                	mov    %ebx,%eax
  800ddb:	31 ff                	xor    %edi,%edi
  800ddd:	e9 58 ff ff ff       	jmp    800d3a <__udivdi3+0x46>
  800de2:	66 90                	xchg   %ax,%ax
  800de4:	8b 54 24 08          	mov    0x8(%esp),%edx
  800de8:	89 f9                	mov    %edi,%ecx
  800dea:	d3 e2                	shl    %cl,%edx
  800dec:	39 c2                	cmp    %eax,%edx
  800dee:	73 e9                	jae    800dd9 <__udivdi3+0xe5>
  800df0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800df3:	31 ff                	xor    %edi,%edi
  800df5:	e9 40 ff ff ff       	jmp    800d3a <__udivdi3+0x46>
  800dfa:	66 90                	xchg   %ax,%ax
  800dfc:	31 c0                	xor    %eax,%eax
  800dfe:	e9 37 ff ff ff       	jmp    800d3a <__udivdi3+0x46>
  800e03:	90                   	nop

00800e04 <__umoddi3>:
  800e04:	55                   	push   %ebp
  800e05:	57                   	push   %edi
  800e06:	56                   	push   %esi
  800e07:	53                   	push   %ebx
  800e08:	83 ec 1c             	sub    $0x1c,%esp
  800e0b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800e0f:	8b 74 24 34          	mov    0x34(%esp),%esi
  800e13:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e17:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e1b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e1f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e23:	89 f3                	mov    %esi,%ebx
  800e25:	89 fa                	mov    %edi,%edx
  800e27:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e2b:	89 34 24             	mov    %esi,(%esp)
  800e2e:	85 c0                	test   %eax,%eax
  800e30:	75 1a                	jne    800e4c <__umoddi3+0x48>
  800e32:	39 f7                	cmp    %esi,%edi
  800e34:	0f 86 a2 00 00 00    	jbe    800edc <__umoddi3+0xd8>
  800e3a:	89 c8                	mov    %ecx,%eax
  800e3c:	89 f2                	mov    %esi,%edx
  800e3e:	f7 f7                	div    %edi
  800e40:	89 d0                	mov    %edx,%eax
  800e42:	31 d2                	xor    %edx,%edx
  800e44:	83 c4 1c             	add    $0x1c,%esp
  800e47:	5b                   	pop    %ebx
  800e48:	5e                   	pop    %esi
  800e49:	5f                   	pop    %edi
  800e4a:	5d                   	pop    %ebp
  800e4b:	c3                   	ret    
  800e4c:	39 f0                	cmp    %esi,%eax
  800e4e:	0f 87 ac 00 00 00    	ja     800f00 <__umoddi3+0xfc>
  800e54:	0f bd e8             	bsr    %eax,%ebp
  800e57:	83 f5 1f             	xor    $0x1f,%ebp
  800e5a:	0f 84 ac 00 00 00    	je     800f0c <__umoddi3+0x108>
  800e60:	bf 20 00 00 00       	mov    $0x20,%edi
  800e65:	29 ef                	sub    %ebp,%edi
  800e67:	89 fe                	mov    %edi,%esi
  800e69:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e6d:	89 e9                	mov    %ebp,%ecx
  800e6f:	d3 e0                	shl    %cl,%eax
  800e71:	89 d7                	mov    %edx,%edi
  800e73:	89 f1                	mov    %esi,%ecx
  800e75:	d3 ef                	shr    %cl,%edi
  800e77:	09 c7                	or     %eax,%edi
  800e79:	89 e9                	mov    %ebp,%ecx
  800e7b:	d3 e2                	shl    %cl,%edx
  800e7d:	89 14 24             	mov    %edx,(%esp)
  800e80:	89 d8                	mov    %ebx,%eax
  800e82:	d3 e0                	shl    %cl,%eax
  800e84:	89 c2                	mov    %eax,%edx
  800e86:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e8a:	d3 e0                	shl    %cl,%eax
  800e8c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e90:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e94:	89 f1                	mov    %esi,%ecx
  800e96:	d3 e8                	shr    %cl,%eax
  800e98:	09 d0                	or     %edx,%eax
  800e9a:	d3 eb                	shr    %cl,%ebx
  800e9c:	89 da                	mov    %ebx,%edx
  800e9e:	f7 f7                	div    %edi
  800ea0:	89 d3                	mov    %edx,%ebx
  800ea2:	f7 24 24             	mull   (%esp)
  800ea5:	89 c6                	mov    %eax,%esi
  800ea7:	89 d1                	mov    %edx,%ecx
  800ea9:	39 d3                	cmp    %edx,%ebx
  800eab:	0f 82 87 00 00 00    	jb     800f38 <__umoddi3+0x134>
  800eb1:	0f 84 91 00 00 00    	je     800f48 <__umoddi3+0x144>
  800eb7:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ebb:	29 f2                	sub    %esi,%edx
  800ebd:	19 cb                	sbb    %ecx,%ebx
  800ebf:	89 d8                	mov    %ebx,%eax
  800ec1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ec5:	d3 e0                	shl    %cl,%eax
  800ec7:	89 e9                	mov    %ebp,%ecx
  800ec9:	d3 ea                	shr    %cl,%edx
  800ecb:	09 d0                	or     %edx,%eax
  800ecd:	89 e9                	mov    %ebp,%ecx
  800ecf:	d3 eb                	shr    %cl,%ebx
  800ed1:	89 da                	mov    %ebx,%edx
  800ed3:	83 c4 1c             	add    $0x1c,%esp
  800ed6:	5b                   	pop    %ebx
  800ed7:	5e                   	pop    %esi
  800ed8:	5f                   	pop    %edi
  800ed9:	5d                   	pop    %ebp
  800eda:	c3                   	ret    
  800edb:	90                   	nop
  800edc:	89 fd                	mov    %edi,%ebp
  800ede:	85 ff                	test   %edi,%edi
  800ee0:	75 0b                	jne    800eed <__umoddi3+0xe9>
  800ee2:	b8 01 00 00 00       	mov    $0x1,%eax
  800ee7:	31 d2                	xor    %edx,%edx
  800ee9:	f7 f7                	div    %edi
  800eeb:	89 c5                	mov    %eax,%ebp
  800eed:	89 f0                	mov    %esi,%eax
  800eef:	31 d2                	xor    %edx,%edx
  800ef1:	f7 f5                	div    %ebp
  800ef3:	89 c8                	mov    %ecx,%eax
  800ef5:	f7 f5                	div    %ebp
  800ef7:	89 d0                	mov    %edx,%eax
  800ef9:	e9 44 ff ff ff       	jmp    800e42 <__umoddi3+0x3e>
  800efe:	66 90                	xchg   %ax,%ax
  800f00:	89 c8                	mov    %ecx,%eax
  800f02:	89 f2                	mov    %esi,%edx
  800f04:	83 c4 1c             	add    $0x1c,%esp
  800f07:	5b                   	pop    %ebx
  800f08:	5e                   	pop    %esi
  800f09:	5f                   	pop    %edi
  800f0a:	5d                   	pop    %ebp
  800f0b:	c3                   	ret    
  800f0c:	3b 04 24             	cmp    (%esp),%eax
  800f0f:	72 06                	jb     800f17 <__umoddi3+0x113>
  800f11:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f15:	77 0f                	ja     800f26 <__umoddi3+0x122>
  800f17:	89 f2                	mov    %esi,%edx
  800f19:	29 f9                	sub    %edi,%ecx
  800f1b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f1f:	89 14 24             	mov    %edx,(%esp)
  800f22:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f26:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f2a:	8b 14 24             	mov    (%esp),%edx
  800f2d:	83 c4 1c             	add    $0x1c,%esp
  800f30:	5b                   	pop    %ebx
  800f31:	5e                   	pop    %esi
  800f32:	5f                   	pop    %edi
  800f33:	5d                   	pop    %ebp
  800f34:	c3                   	ret    
  800f35:	8d 76 00             	lea    0x0(%esi),%esi
  800f38:	2b 04 24             	sub    (%esp),%eax
  800f3b:	19 fa                	sbb    %edi,%edx
  800f3d:	89 d1                	mov    %edx,%ecx
  800f3f:	89 c6                	mov    %eax,%esi
  800f41:	e9 71 ff ff ff       	jmp    800eb7 <__umoddi3+0xb3>
  800f46:	66 90                	xchg   %ax,%ax
  800f48:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f4c:	72 ea                	jb     800f38 <__umoddi3+0x134>
  800f4e:	89 d9                	mov    %ebx,%ecx
  800f50:	e9 62 ff ff ff       	jmp    800eb7 <__umoddi3+0xb3>
