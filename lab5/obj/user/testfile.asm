
obj/user/testfile.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 d4 05 00 00       	call   800605 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <xopen>:

#define FVA ((struct Fd*)0xCCCCC000)

static int
xopen(const char *path, int mode)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 0c             	sub    $0xc,%esp
  80003a:	89 d3                	mov    %edx,%ebx
	extern union Fsipc fsipcbuf;
	envid_t fsenv;
	
	strcpy(fsipcbuf.open.req_path, path);
  80003c:	50                   	push   %eax
  80003d:	68 00 50 80 00       	push   $0x805000
  800042:	e8 5b 0c 00 00       	call   800ca2 <strcpy>
	fsipcbuf.open.req_omode = mode;
  800047:	89 1d 00 54 80 00    	mov    %ebx,0x805400

	fsenv = ipc_find_env(ENV_TYPE_FS);
  80004d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800054:	e8 fa 12 00 00       	call   801353 <ipc_find_env>
	ipc_send(fsenv, FSREQ_OPEN, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  800059:	6a 07                	push   $0x7
  80005b:	68 00 50 80 00       	push   $0x805000
  800060:	6a 01                	push   $0x1
  800062:	50                   	push   %eax
  800063:	e8 96 12 00 00       	call   8012fe <ipc_send>
	return ipc_recv(NULL, FVA, NULL);
  800068:	83 c4 1c             	add    $0x1c,%esp
  80006b:	6a 00                	push   $0x0
  80006d:	68 00 c0 cc cc       	push   $0xccccc000
  800072:	6a 00                	push   $0x0
  800074:	e8 1d 12 00 00       	call   801296 <ipc_recv>
}
  800079:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80007c:	c9                   	leave  
  80007d:	c3                   	ret    

0080007e <umain>:

void
umain(int argc, char **argv)
{
  80007e:	55                   	push   %ebp
  80007f:	89 e5                	mov    %esp,%ebp
  800081:	57                   	push   %edi
  800082:	56                   	push   %esi
  800083:	53                   	push   %ebx
  800084:	81 ec ac 02 00 00    	sub    $0x2ac,%esp
	struct Fd fdcopy;
	struct Stat st;
	char buf[512];

	// We open files manually first, to avoid the FD layer
	if ((r = xopen("/not-found", O_RDONLY)) < 0 && r != -E_NOT_FOUND)
  80008a:	ba 00 00 00 00       	mov    $0x0,%edx
  80008f:	b8 e0 22 80 00       	mov    $0x8022e0,%eax
  800094:	e8 9a ff ff ff       	call   800033 <xopen>
  800099:	85 c0                	test   %eax,%eax
  80009b:	79 17                	jns    8000b4 <umain+0x36>
  80009d:	83 f8 f5             	cmp    $0xfffffff5,%eax
  8000a0:	74 26                	je     8000c8 <umain+0x4a>
		panic("serve_open /not-found: %e", r);
  8000a2:	50                   	push   %eax
  8000a3:	68 eb 22 80 00       	push   $0x8022eb
  8000a8:	6a 20                	push   $0x20
  8000aa:	68 05 23 80 00       	push   $0x802305
  8000af:	e8 b2 05 00 00       	call   800666 <_panic>
	else if (r >= 0)
		panic("serve_open /not-found succeeded!");
  8000b4:	83 ec 04             	sub    $0x4,%esp
  8000b7:	68 a0 24 80 00       	push   $0x8024a0
  8000bc:	6a 22                	push   $0x22
  8000be:	68 05 23 80 00       	push   $0x802305
  8000c3:	e8 9e 05 00 00       	call   800666 <_panic>

	if ((r = xopen("/newmotd", O_RDONLY)) < 0)
  8000c8:	ba 00 00 00 00       	mov    $0x0,%edx
  8000cd:	b8 15 23 80 00       	mov    $0x802315,%eax
  8000d2:	e8 5c ff ff ff       	call   800033 <xopen>
  8000d7:	85 c0                	test   %eax,%eax
  8000d9:	79 12                	jns    8000ed <umain+0x6f>
		panic("serve_open /newmotd: %e", r);
  8000db:	50                   	push   %eax
  8000dc:	68 1e 23 80 00       	push   $0x80231e
  8000e1:	6a 25                	push   $0x25
  8000e3:	68 05 23 80 00       	push   $0x802305
  8000e8:	e8 79 05 00 00       	call   800666 <_panic>
	if (FVA->fd_dev_id != 'f' || FVA->fd_offset != 0 || FVA->fd_omode != O_RDONLY)
  8000ed:	83 3d 00 c0 cc cc 66 	cmpl   $0x66,0xccccc000
  8000f4:	75 12                	jne    800108 <umain+0x8a>
  8000f6:	83 3d 04 c0 cc cc 00 	cmpl   $0x0,0xccccc004
  8000fd:	75 09                	jne    800108 <umain+0x8a>
  8000ff:	83 3d 08 c0 cc cc 00 	cmpl   $0x0,0xccccc008
  800106:	74 14                	je     80011c <umain+0x9e>
		panic("serve_open did not fill struct Fd correctly\n");
  800108:	83 ec 04             	sub    $0x4,%esp
  80010b:	68 c4 24 80 00       	push   $0x8024c4
  800110:	6a 27                	push   $0x27
  800112:	68 05 23 80 00       	push   $0x802305
  800117:	e8 4a 05 00 00       	call   800666 <_panic>
	cprintf("serve_open is good\n");
  80011c:	83 ec 0c             	sub    $0xc,%esp
  80011f:	68 36 23 80 00       	push   $0x802336
  800124:	e8 15 06 00 00       	call   80073e <cprintf>

	if ((r = devfile.dev_stat(FVA, &st)) < 0)
  800129:	83 c4 08             	add    $0x8,%esp
  80012c:	8d 85 4c ff ff ff    	lea    -0xb4(%ebp),%eax
  800132:	50                   	push   %eax
  800133:	68 00 c0 cc cc       	push   $0xccccc000
  800138:	ff 15 1c 30 80 00    	call   *0x80301c
  80013e:	83 c4 10             	add    $0x10,%esp
  800141:	85 c0                	test   %eax,%eax
  800143:	79 12                	jns    800157 <umain+0xd9>
		panic("file_stat: %e", r);
  800145:	50                   	push   %eax
  800146:	68 4a 23 80 00       	push   $0x80234a
  80014b:	6a 2b                	push   $0x2b
  80014d:	68 05 23 80 00       	push   $0x802305
  800152:	e8 0f 05 00 00       	call   800666 <_panic>
	if (strlen(msg) != st.st_size)
  800157:	83 ec 0c             	sub    $0xc,%esp
  80015a:	ff 35 00 30 80 00    	pushl  0x803000
  800160:	e8 08 0b 00 00       	call   800c6d <strlen>
  800165:	83 c4 10             	add    $0x10,%esp
  800168:	3b 45 cc             	cmp    -0x34(%ebp),%eax
  80016b:	74 25                	je     800192 <umain+0x114>
		panic("file_stat returned size %d wanted %d\n", st.st_size, strlen(msg));
  80016d:	83 ec 0c             	sub    $0xc,%esp
  800170:	ff 35 00 30 80 00    	pushl  0x803000
  800176:	e8 f2 0a 00 00       	call   800c6d <strlen>
  80017b:	89 04 24             	mov    %eax,(%esp)
  80017e:	ff 75 cc             	pushl  -0x34(%ebp)
  800181:	68 f4 24 80 00       	push   $0x8024f4
  800186:	6a 2d                	push   $0x2d
  800188:	68 05 23 80 00       	push   $0x802305
  80018d:	e8 d4 04 00 00       	call   800666 <_panic>
	cprintf("file_stat is good\n");
  800192:	83 ec 0c             	sub    $0xc,%esp
  800195:	68 58 23 80 00       	push   $0x802358
  80019a:	e8 9f 05 00 00       	call   80073e <cprintf>

	memset(buf, 0, sizeof buf);
  80019f:	83 c4 0c             	add    $0xc,%esp
  8001a2:	68 00 02 00 00       	push   $0x200
  8001a7:	6a 00                	push   $0x0
  8001a9:	8d 9d 4c fd ff ff    	lea    -0x2b4(%ebp),%ebx
  8001af:	53                   	push   %ebx
  8001b0:	e8 15 0c 00 00       	call   800dca <memset>
	if ((r = devfile.dev_read(FVA, buf, sizeof buf)) < 0)
  8001b5:	83 c4 0c             	add    $0xc,%esp
  8001b8:	68 00 02 00 00       	push   $0x200
  8001bd:	53                   	push   %ebx
  8001be:	68 00 c0 cc cc       	push   $0xccccc000
  8001c3:	ff 15 10 30 80 00    	call   *0x803010
  8001c9:	83 c4 10             	add    $0x10,%esp
  8001cc:	85 c0                	test   %eax,%eax
  8001ce:	79 12                	jns    8001e2 <umain+0x164>
		panic("file_read: %e", r);
  8001d0:	50                   	push   %eax
  8001d1:	68 6b 23 80 00       	push   $0x80236b
  8001d6:	6a 32                	push   $0x32
  8001d8:	68 05 23 80 00       	push   $0x802305
  8001dd:	e8 84 04 00 00       	call   800666 <_panic>
	if (strcmp(buf, msg) != 0)
  8001e2:	83 ec 08             	sub    $0x8,%esp
  8001e5:	ff 35 00 30 80 00    	pushl  0x803000
  8001eb:	8d 85 4c fd ff ff    	lea    -0x2b4(%ebp),%eax
  8001f1:	50                   	push   %eax
  8001f2:	e8 4a 0b 00 00       	call   800d41 <strcmp>
  8001f7:	83 c4 10             	add    $0x10,%esp
  8001fa:	85 c0                	test   %eax,%eax
  8001fc:	74 14                	je     800212 <umain+0x194>
		panic("file_read returned wrong data");
  8001fe:	83 ec 04             	sub    $0x4,%esp
  800201:	68 79 23 80 00       	push   $0x802379
  800206:	6a 34                	push   $0x34
  800208:	68 05 23 80 00       	push   $0x802305
  80020d:	e8 54 04 00 00       	call   800666 <_panic>
	cprintf("file_read is good\n");
  800212:	83 ec 0c             	sub    $0xc,%esp
  800215:	68 97 23 80 00       	push   $0x802397
  80021a:	e8 1f 05 00 00       	call   80073e <cprintf>

	if ((r = devfile.dev_close(FVA)) < 0)
  80021f:	c7 04 24 00 c0 cc cc 	movl   $0xccccc000,(%esp)
  800226:	ff 15 18 30 80 00    	call   *0x803018
  80022c:	83 c4 10             	add    $0x10,%esp
  80022f:	85 c0                	test   %eax,%eax
  800231:	79 12                	jns    800245 <umain+0x1c7>
		panic("file_close: %e", r);
  800233:	50                   	push   %eax
  800234:	68 aa 23 80 00       	push   $0x8023aa
  800239:	6a 38                	push   $0x38
  80023b:	68 05 23 80 00       	push   $0x802305
  800240:	e8 21 04 00 00       	call   800666 <_panic>
	cprintf("file_close is good\n");
  800245:	83 ec 0c             	sub    $0xc,%esp
  800248:	68 b9 23 80 00       	push   $0x8023b9
  80024d:	e8 ec 04 00 00       	call   80073e <cprintf>

	// We're about to unmap the FD, but still need a way to get
	// the stale filenum to serve_read, so we make a local copy.
	// The file server won't think it's stale until we unmap the
	// FD page.
	fdcopy = *FVA;
  800252:	be 00 c0 cc cc       	mov    $0xccccc000,%esi
  800257:	8d 7d d8             	lea    -0x28(%ebp),%edi
  80025a:	b9 04 00 00 00       	mov    $0x4,%ecx
  80025f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	sys_page_unmap(0, FVA);
  800261:	83 c4 08             	add    $0x8,%esp
  800264:	68 00 c0 cc cc       	push   $0xccccc000
  800269:	6a 00                	push   $0x0
  80026b:	e8 9b 0e 00 00       	call   80110b <sys_page_unmap>

	if ((r = devfile.dev_read(&fdcopy, buf, sizeof buf)) != -E_INVAL)
  800270:	83 c4 0c             	add    $0xc,%esp
  800273:	68 00 02 00 00       	push   $0x200
  800278:	8d 85 4c fd ff ff    	lea    -0x2b4(%ebp),%eax
  80027e:	50                   	push   %eax
  80027f:	8d 45 d8             	lea    -0x28(%ebp),%eax
  800282:	50                   	push   %eax
  800283:	ff 15 10 30 80 00    	call   *0x803010
  800289:	83 c4 10             	add    $0x10,%esp
  80028c:	83 f8 fd             	cmp    $0xfffffffd,%eax
  80028f:	74 12                	je     8002a3 <umain+0x225>
		panic("serve_read does not handle stale fileids correctly: %e", r);
  800291:	50                   	push   %eax
  800292:	68 1c 25 80 00       	push   $0x80251c
  800297:	6a 43                	push   $0x43
  800299:	68 05 23 80 00       	push   $0x802305
  80029e:	e8 c3 03 00 00       	call   800666 <_panic>
	cprintf("stale fileid is good\n");
  8002a3:	83 ec 0c             	sub    $0xc,%esp
  8002a6:	68 cd 23 80 00       	push   $0x8023cd
  8002ab:	e8 8e 04 00 00       	call   80073e <cprintf>

	// Try writing
	if ((r = xopen("/new-file", O_RDWR|O_CREAT)) < 0)
  8002b0:	ba 02 01 00 00       	mov    $0x102,%edx
  8002b5:	b8 e3 23 80 00       	mov    $0x8023e3,%eax
  8002ba:	e8 74 fd ff ff       	call   800033 <xopen>
  8002bf:	83 c4 10             	add    $0x10,%esp
  8002c2:	85 c0                	test   %eax,%eax
  8002c4:	79 12                	jns    8002d8 <umain+0x25a>
		panic("serve_open /new-file: %e", r);
  8002c6:	50                   	push   %eax
  8002c7:	68 ed 23 80 00       	push   $0x8023ed
  8002cc:	6a 48                	push   $0x48
  8002ce:	68 05 23 80 00       	push   $0x802305
  8002d3:	e8 8e 03 00 00       	call   800666 <_panic>

	if ((r = devfile.dev_write(FVA, msg, strlen(msg))) != strlen(msg))
  8002d8:	8b 1d 14 30 80 00    	mov    0x803014,%ebx
  8002de:	83 ec 0c             	sub    $0xc,%esp
  8002e1:	ff 35 00 30 80 00    	pushl  0x803000
  8002e7:	e8 81 09 00 00       	call   800c6d <strlen>
  8002ec:	83 c4 0c             	add    $0xc,%esp
  8002ef:	50                   	push   %eax
  8002f0:	ff 35 00 30 80 00    	pushl  0x803000
  8002f6:	68 00 c0 cc cc       	push   $0xccccc000
  8002fb:	ff d3                	call   *%ebx
  8002fd:	89 c3                	mov    %eax,%ebx
  8002ff:	83 c4 04             	add    $0x4,%esp
  800302:	ff 35 00 30 80 00    	pushl  0x803000
  800308:	e8 60 09 00 00       	call   800c6d <strlen>
  80030d:	83 c4 10             	add    $0x10,%esp
  800310:	39 c3                	cmp    %eax,%ebx
  800312:	74 12                	je     800326 <umain+0x2a8>
		panic("file_write: %e", r);
  800314:	53                   	push   %ebx
  800315:	68 06 24 80 00       	push   $0x802406
  80031a:	6a 4b                	push   $0x4b
  80031c:	68 05 23 80 00       	push   $0x802305
  800321:	e8 40 03 00 00       	call   800666 <_panic>
	cprintf("file_write is good\n");
  800326:	83 ec 0c             	sub    $0xc,%esp
  800329:	68 15 24 80 00       	push   $0x802415
  80032e:	e8 0b 04 00 00       	call   80073e <cprintf>

	FVA->fd_offset = 0;
  800333:	c7 05 04 c0 cc cc 00 	movl   $0x0,0xccccc004
  80033a:	00 00 00 
	memset(buf, 0, sizeof buf);
  80033d:	83 c4 0c             	add    $0xc,%esp
  800340:	68 00 02 00 00       	push   $0x200
  800345:	6a 00                	push   $0x0
  800347:	8d 9d 4c fd ff ff    	lea    -0x2b4(%ebp),%ebx
  80034d:	53                   	push   %ebx
  80034e:	e8 77 0a 00 00       	call   800dca <memset>
	if ((r = devfile.dev_read(FVA, buf, sizeof buf)) < 0)
  800353:	83 c4 0c             	add    $0xc,%esp
  800356:	68 00 02 00 00       	push   $0x200
  80035b:	53                   	push   %ebx
  80035c:	68 00 c0 cc cc       	push   $0xccccc000
  800361:	ff 15 10 30 80 00    	call   *0x803010
  800367:	89 c3                	mov    %eax,%ebx
  800369:	83 c4 10             	add    $0x10,%esp
  80036c:	85 c0                	test   %eax,%eax
  80036e:	79 12                	jns    800382 <umain+0x304>
		panic("file_read after file_write: %e", r);
  800370:	50                   	push   %eax
  800371:	68 54 25 80 00       	push   $0x802554
  800376:	6a 51                	push   $0x51
  800378:	68 05 23 80 00       	push   $0x802305
  80037d:	e8 e4 02 00 00       	call   800666 <_panic>
	if (r != strlen(msg))
  800382:	83 ec 0c             	sub    $0xc,%esp
  800385:	ff 35 00 30 80 00    	pushl  0x803000
  80038b:	e8 dd 08 00 00       	call   800c6d <strlen>
  800390:	83 c4 10             	add    $0x10,%esp
  800393:	39 c3                	cmp    %eax,%ebx
  800395:	74 12                	je     8003a9 <umain+0x32b>
		panic("file_read after file_write returned wrong length: %d", r);
  800397:	53                   	push   %ebx
  800398:	68 74 25 80 00       	push   $0x802574
  80039d:	6a 53                	push   $0x53
  80039f:	68 05 23 80 00       	push   $0x802305
  8003a4:	e8 bd 02 00 00       	call   800666 <_panic>
	if (strcmp(buf, msg) != 0)
  8003a9:	83 ec 08             	sub    $0x8,%esp
  8003ac:	ff 35 00 30 80 00    	pushl  0x803000
  8003b2:	8d 85 4c fd ff ff    	lea    -0x2b4(%ebp),%eax
  8003b8:	50                   	push   %eax
  8003b9:	e8 83 09 00 00       	call   800d41 <strcmp>
  8003be:	83 c4 10             	add    $0x10,%esp
  8003c1:	85 c0                	test   %eax,%eax
  8003c3:	74 14                	je     8003d9 <umain+0x35b>
		panic("file_read after file_write returned wrong data");
  8003c5:	83 ec 04             	sub    $0x4,%esp
  8003c8:	68 ac 25 80 00       	push   $0x8025ac
  8003cd:	6a 55                	push   $0x55
  8003cf:	68 05 23 80 00       	push   $0x802305
  8003d4:	e8 8d 02 00 00       	call   800666 <_panic>
	cprintf("file_read after file_write is good\n");
  8003d9:	83 ec 0c             	sub    $0xc,%esp
  8003dc:	68 dc 25 80 00       	push   $0x8025dc
  8003e1:	e8 58 03 00 00       	call   80073e <cprintf>

	// Now we'll try out open
	if ((r = open("/not-found", O_RDONLY)) < 0 && r != -E_NOT_FOUND)
  8003e6:	83 c4 08             	add    $0x8,%esp
  8003e9:	6a 00                	push   $0x0
  8003eb:	68 e0 22 80 00       	push   $0x8022e0
  8003f0:	e8 e5 16 00 00       	call   801ada <open>
  8003f5:	83 c4 10             	add    $0x10,%esp
  8003f8:	85 c0                	test   %eax,%eax
  8003fa:	79 17                	jns    800413 <umain+0x395>
  8003fc:	83 f8 f5             	cmp    $0xfffffff5,%eax
  8003ff:	74 26                	je     800427 <umain+0x3a9>
		panic("open /not-found: %e", r);
  800401:	50                   	push   %eax
  800402:	68 f1 22 80 00       	push   $0x8022f1
  800407:	6a 5a                	push   $0x5a
  800409:	68 05 23 80 00       	push   $0x802305
  80040e:	e8 53 02 00 00       	call   800666 <_panic>
	else if (r >= 0)
		panic("open /not-found succeeded!");
  800413:	83 ec 04             	sub    $0x4,%esp
  800416:	68 29 24 80 00       	push   $0x802429
  80041b:	6a 5c                	push   $0x5c
  80041d:	68 05 23 80 00       	push   $0x802305
  800422:	e8 3f 02 00 00       	call   800666 <_panic>

	if ((r = open("/newmotd", O_RDONLY)) < 0)
  800427:	83 ec 08             	sub    $0x8,%esp
  80042a:	6a 00                	push   $0x0
  80042c:	68 15 23 80 00       	push   $0x802315
  800431:	e8 a4 16 00 00       	call   801ada <open>
  800436:	83 c4 10             	add    $0x10,%esp
  800439:	85 c0                	test   %eax,%eax
  80043b:	79 12                	jns    80044f <umain+0x3d1>
		panic("open /newmotd: %e", r);
  80043d:	50                   	push   %eax
  80043e:	68 24 23 80 00       	push   $0x802324
  800443:	6a 5f                	push   $0x5f
  800445:	68 05 23 80 00       	push   $0x802305
  80044a:	e8 17 02 00 00       	call   800666 <_panic>
	fd = (struct Fd*) (0xD0000000 + r*PGSIZE);
  80044f:	c1 e0 0c             	shl    $0xc,%eax
	if (fd->fd_dev_id != 'f' || fd->fd_offset != 0 || fd->fd_omode != O_RDONLY)
  800452:	83 b8 00 00 00 d0 66 	cmpl   $0x66,-0x30000000(%eax)
  800459:	75 12                	jne    80046d <umain+0x3ef>
  80045b:	83 b8 04 00 00 d0 00 	cmpl   $0x0,-0x2ffffffc(%eax)
  800462:	75 09                	jne    80046d <umain+0x3ef>
  800464:	83 b8 08 00 00 d0 00 	cmpl   $0x0,-0x2ffffff8(%eax)
  80046b:	74 14                	je     800481 <umain+0x403>
		panic("open did not fill struct Fd correctly\n");
  80046d:	83 ec 04             	sub    $0x4,%esp
  800470:	68 00 26 80 00       	push   $0x802600
  800475:	6a 62                	push   $0x62
  800477:	68 05 23 80 00       	push   $0x802305
  80047c:	e8 e5 01 00 00       	call   800666 <_panic>
	cprintf("open is good\n");
  800481:	83 ec 0c             	sub    $0xc,%esp
  800484:	68 3c 23 80 00       	push   $0x80233c
  800489:	e8 b0 02 00 00       	call   80073e <cprintf>

	// Try files with indirect blocks
	if ((f = open("/big", O_WRONLY|O_CREAT)) < 0)
  80048e:	83 c4 08             	add    $0x8,%esp
  800491:	68 01 01 00 00       	push   $0x101
  800496:	68 44 24 80 00       	push   $0x802444
  80049b:	e8 3a 16 00 00       	call   801ada <open>
  8004a0:	89 c6                	mov    %eax,%esi
  8004a2:	83 c4 10             	add    $0x10,%esp
  8004a5:	85 c0                	test   %eax,%eax
  8004a7:	79 12                	jns    8004bb <umain+0x43d>
		panic("creat /big: %e", f);
  8004a9:	50                   	push   %eax
  8004aa:	68 49 24 80 00       	push   $0x802449
  8004af:	6a 67                	push   $0x67
  8004b1:	68 05 23 80 00       	push   $0x802305
  8004b6:	e8 ab 01 00 00       	call   800666 <_panic>
	memset(buf, 0, sizeof(buf));
  8004bb:	83 ec 04             	sub    $0x4,%esp
  8004be:	68 00 02 00 00       	push   $0x200
  8004c3:	6a 00                	push   $0x0
  8004c5:	8d 85 4c fd ff ff    	lea    -0x2b4(%ebp),%eax
  8004cb:	50                   	push   %eax
  8004cc:	e8 f9 08 00 00       	call   800dca <memset>
  8004d1:	83 c4 10             	add    $0x10,%esp
	for (i = 0; i < (NDIRECT*3)*BLKSIZE; i += sizeof(buf)) {
  8004d4:	bb 00 00 00 00       	mov    $0x0,%ebx
		*(int*)buf = i;
		if ((r = write(f, buf, sizeof(buf))) < 0)
  8004d9:	8d bd 4c fd ff ff    	lea    -0x2b4(%ebp),%edi
	// Try files with indirect blocks
	if ((f = open("/big", O_WRONLY|O_CREAT)) < 0)
		panic("creat /big: %e", f);
	memset(buf, 0, sizeof(buf));
	for (i = 0; i < (NDIRECT*3)*BLKSIZE; i += sizeof(buf)) {
		*(int*)buf = i;
  8004df:	89 9d 4c fd ff ff    	mov    %ebx,-0x2b4(%ebp)
		if ((r = write(f, buf, sizeof(buf))) < 0)
  8004e5:	83 ec 04             	sub    $0x4,%esp
  8004e8:	68 00 02 00 00       	push   $0x200
  8004ed:	57                   	push   %edi
  8004ee:	56                   	push   %esi
  8004ef:	e8 4f 12 00 00       	call   801743 <write>
  8004f4:	83 c4 10             	add    $0x10,%esp
  8004f7:	85 c0                	test   %eax,%eax
  8004f9:	79 16                	jns    800511 <umain+0x493>
			panic("write /big@%d: %e", i, r);
  8004fb:	83 ec 0c             	sub    $0xc,%esp
  8004fe:	50                   	push   %eax
  8004ff:	53                   	push   %ebx
  800500:	68 58 24 80 00       	push   $0x802458
  800505:	6a 6c                	push   $0x6c
  800507:	68 05 23 80 00       	push   $0x802305
  80050c:	e8 55 01 00 00       	call   800666 <_panic>
  800511:	8d 83 00 02 00 00    	lea    0x200(%ebx),%eax
  800517:	89 c3                	mov    %eax,%ebx

	// Try files with indirect blocks
	if ((f = open("/big", O_WRONLY|O_CREAT)) < 0)
		panic("creat /big: %e", f);
	memset(buf, 0, sizeof(buf));
	for (i = 0; i < (NDIRECT*3)*BLKSIZE; i += sizeof(buf)) {
  800519:	3d 00 e0 01 00       	cmp    $0x1e000,%eax
  80051e:	75 bf                	jne    8004df <umain+0x461>
		*(int*)buf = i;
		if ((r = write(f, buf, sizeof(buf))) < 0)
			panic("write /big@%d: %e", i, r);
	}
	close(f);
  800520:	83 ec 0c             	sub    $0xc,%esp
  800523:	56                   	push   %esi
  800524:	e8 12 10 00 00       	call   80153b <close>

	if ((f = open("/big", O_RDONLY)) < 0)
  800529:	83 c4 08             	add    $0x8,%esp
  80052c:	6a 00                	push   $0x0
  80052e:	68 44 24 80 00       	push   $0x802444
  800533:	e8 a2 15 00 00       	call   801ada <open>
  800538:	89 c6                	mov    %eax,%esi
  80053a:	83 c4 10             	add    $0x10,%esp
  80053d:	85 c0                	test   %eax,%eax
  80053f:	79 12                	jns    800553 <umain+0x4d5>
		panic("open /big: %e", f);
  800541:	50                   	push   %eax
  800542:	68 6a 24 80 00       	push   $0x80246a
  800547:	6a 71                	push   $0x71
  800549:	68 05 23 80 00       	push   $0x802305
  80054e:	e8 13 01 00 00       	call   800666 <_panic>
  800553:	bb 00 00 00 00       	mov    $0x0,%ebx
	for (i = 0; i < (NDIRECT*3)*BLKSIZE; i += sizeof(buf)) {
		*(int*)buf = i;
		if ((r = readn(f, buf, sizeof(buf))) < 0)
  800558:	8d bd 4c fd ff ff    	lea    -0x2b4(%ebp),%edi
	close(f);

	if ((f = open("/big", O_RDONLY)) < 0)
		panic("open /big: %e", f);
	for (i = 0; i < (NDIRECT*3)*BLKSIZE; i += sizeof(buf)) {
		*(int*)buf = i;
  80055e:	89 9d 4c fd ff ff    	mov    %ebx,-0x2b4(%ebp)
		if ((r = readn(f, buf, sizeof(buf))) < 0)
  800564:	83 ec 04             	sub    $0x4,%esp
  800567:	68 00 02 00 00       	push   $0x200
  80056c:	57                   	push   %edi
  80056d:	56                   	push   %esi
  80056e:	e8 87 11 00 00       	call   8016fa <readn>
  800573:	83 c4 10             	add    $0x10,%esp
  800576:	85 c0                	test   %eax,%eax
  800578:	79 16                	jns    800590 <umain+0x512>
			panic("read /big@%d: %e", i, r);
  80057a:	83 ec 0c             	sub    $0xc,%esp
  80057d:	50                   	push   %eax
  80057e:	53                   	push   %ebx
  80057f:	68 78 24 80 00       	push   $0x802478
  800584:	6a 75                	push   $0x75
  800586:	68 05 23 80 00       	push   $0x802305
  80058b:	e8 d6 00 00 00       	call   800666 <_panic>
		if (r != sizeof(buf))
  800590:	3d 00 02 00 00       	cmp    $0x200,%eax
  800595:	74 1b                	je     8005b2 <umain+0x534>
			panic("read /big from %d returned %d < %d bytes",
  800597:	83 ec 08             	sub    $0x8,%esp
  80059a:	68 00 02 00 00       	push   $0x200
  80059f:	50                   	push   %eax
  8005a0:	53                   	push   %ebx
  8005a1:	68 28 26 80 00       	push   $0x802628
  8005a6:	6a 78                	push   $0x78
  8005a8:	68 05 23 80 00       	push   $0x802305
  8005ad:	e8 b4 00 00 00       	call   800666 <_panic>
			      i, r, sizeof(buf));
		if (*(int*)buf != i)
  8005b2:	8b 85 4c fd ff ff    	mov    -0x2b4(%ebp),%eax
  8005b8:	39 d8                	cmp    %ebx,%eax
  8005ba:	74 16                	je     8005d2 <umain+0x554>
			panic("read /big from %d returned bad data %d",
  8005bc:	83 ec 0c             	sub    $0xc,%esp
  8005bf:	50                   	push   %eax
  8005c0:	53                   	push   %ebx
  8005c1:	68 54 26 80 00       	push   $0x802654
  8005c6:	6a 7b                	push   $0x7b
  8005c8:	68 05 23 80 00       	push   $0x802305
  8005cd:	e8 94 00 00 00       	call   800666 <_panic>
  8005d2:	8d 83 00 02 00 00    	lea    0x200(%ebx),%eax
  8005d8:	89 c3                	mov    %eax,%ebx
	}
	close(f);

	if ((f = open("/big", O_RDONLY)) < 0)
		panic("open /big: %e", f);
	for (i = 0; i < (NDIRECT*3)*BLKSIZE; i += sizeof(buf)) {
  8005da:	3d 00 e0 01 00       	cmp    $0x1e000,%eax
  8005df:	0f 85 79 ff ff ff    	jne    80055e <umain+0x4e0>
			      i, r, sizeof(buf));
		if (*(int*)buf != i)
			panic("read /big from %d returned bad data %d",
			      i, *(int*)buf);
	}
	close(f);
  8005e5:	83 ec 0c             	sub    $0xc,%esp
  8005e8:	56                   	push   %esi
  8005e9:	e8 4d 0f 00 00       	call   80153b <close>
	cprintf("large file is good\n");
  8005ee:	c7 04 24 89 24 80 00 	movl   $0x802489,(%esp)
  8005f5:	e8 44 01 00 00       	call   80073e <cprintf>
}
  8005fa:	83 c4 10             	add    $0x10,%esp
  8005fd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800600:	5b                   	pop    %ebx
  800601:	5e                   	pop    %esi
  800602:	5f                   	pop    %edi
  800603:	5d                   	pop    %ebp
  800604:	c3                   	ret    

00800605 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800605:	55                   	push   %ebp
  800606:	89 e5                	mov    %esp,%ebp
  800608:	56                   	push   %esi
  800609:	53                   	push   %ebx
  80060a:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80060d:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800610:	e8 33 0a 00 00       	call   801048 <sys_getenvid>
  800615:	25 ff 03 00 00       	and    $0x3ff,%eax
  80061a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800621:	c1 e0 07             	shl    $0x7,%eax
  800624:	29 d0                	sub    %edx,%eax
  800626:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80062b:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800630:	85 db                	test   %ebx,%ebx
  800632:	7e 07                	jle    80063b <libmain+0x36>
		binaryname = argv[0];
  800634:	8b 06                	mov    (%esi),%eax
  800636:	a3 04 30 80 00       	mov    %eax,0x803004

	// call user main routine
	umain(argc, argv);
  80063b:	83 ec 08             	sub    $0x8,%esp
  80063e:	56                   	push   %esi
  80063f:	53                   	push   %ebx
  800640:	e8 39 fa ff ff       	call   80007e <umain>

	// exit gracefully
	exit();
  800645:	e8 0a 00 00 00       	call   800654 <exit>
}
  80064a:	83 c4 10             	add    $0x10,%esp
  80064d:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800650:	5b                   	pop    %ebx
  800651:	5e                   	pop    %esi
  800652:	5d                   	pop    %ebp
  800653:	c3                   	ret    

00800654 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800654:	55                   	push   %ebp
  800655:	89 e5                	mov    %esp,%ebp
  800657:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80065a:	6a 00                	push   $0x0
  80065c:	e8 a6 09 00 00       	call   801007 <sys_env_destroy>
}
  800661:	83 c4 10             	add    $0x10,%esp
  800664:	c9                   	leave  
  800665:	c3                   	ret    

00800666 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800666:	55                   	push   %ebp
  800667:	89 e5                	mov    %esp,%ebp
  800669:	56                   	push   %esi
  80066a:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80066b:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80066e:	8b 35 04 30 80 00    	mov    0x803004,%esi
  800674:	e8 cf 09 00 00       	call   801048 <sys_getenvid>
  800679:	83 ec 0c             	sub    $0xc,%esp
  80067c:	ff 75 0c             	pushl  0xc(%ebp)
  80067f:	ff 75 08             	pushl  0x8(%ebp)
  800682:	56                   	push   %esi
  800683:	50                   	push   %eax
  800684:	68 ac 26 80 00       	push   $0x8026ac
  800689:	e8 b0 00 00 00       	call   80073e <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80068e:	83 c4 18             	add    $0x18,%esp
  800691:	53                   	push   %ebx
  800692:	ff 75 10             	pushl  0x10(%ebp)
  800695:	e8 53 00 00 00       	call   8006ed <vcprintf>
	cprintf("\n");
  80069a:	c7 04 24 ff 2a 80 00 	movl   $0x802aff,(%esp)
  8006a1:	e8 98 00 00 00       	call   80073e <cprintf>
  8006a6:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8006a9:	cc                   	int3   
  8006aa:	eb fd                	jmp    8006a9 <_panic+0x43>

008006ac <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8006ac:	55                   	push   %ebp
  8006ad:	89 e5                	mov    %esp,%ebp
  8006af:	53                   	push   %ebx
  8006b0:	83 ec 04             	sub    $0x4,%esp
  8006b3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8006b6:	8b 13                	mov    (%ebx),%edx
  8006b8:	8d 42 01             	lea    0x1(%edx),%eax
  8006bb:	89 03                	mov    %eax,(%ebx)
  8006bd:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006c0:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8006c4:	3d ff 00 00 00       	cmp    $0xff,%eax
  8006c9:	75 1a                	jne    8006e5 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8006cb:	83 ec 08             	sub    $0x8,%esp
  8006ce:	68 ff 00 00 00       	push   $0xff
  8006d3:	8d 43 08             	lea    0x8(%ebx),%eax
  8006d6:	50                   	push   %eax
  8006d7:	e8 ee 08 00 00       	call   800fca <sys_cputs>
		b->idx = 0;
  8006dc:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8006e2:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8006e5:	ff 43 04             	incl   0x4(%ebx)
}
  8006e8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006eb:	c9                   	leave  
  8006ec:	c3                   	ret    

008006ed <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8006ed:	55                   	push   %ebp
  8006ee:	89 e5                	mov    %esp,%ebp
  8006f0:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8006f6:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8006fd:	00 00 00 
	b.cnt = 0;
  800700:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800707:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80070a:	ff 75 0c             	pushl  0xc(%ebp)
  80070d:	ff 75 08             	pushl  0x8(%ebp)
  800710:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800716:	50                   	push   %eax
  800717:	68 ac 06 80 00       	push   $0x8006ac
  80071c:	e8 51 01 00 00       	call   800872 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800721:	83 c4 08             	add    $0x8,%esp
  800724:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80072a:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800730:	50                   	push   %eax
  800731:	e8 94 08 00 00       	call   800fca <sys_cputs>

	return b.cnt;
}
  800736:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80073c:	c9                   	leave  
  80073d:	c3                   	ret    

0080073e <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80073e:	55                   	push   %ebp
  80073f:	89 e5                	mov    %esp,%ebp
  800741:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800744:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800747:	50                   	push   %eax
  800748:	ff 75 08             	pushl  0x8(%ebp)
  80074b:	e8 9d ff ff ff       	call   8006ed <vcprintf>
	va_end(ap);

	return cnt;
}
  800750:	c9                   	leave  
  800751:	c3                   	ret    

00800752 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800752:	55                   	push   %ebp
  800753:	89 e5                	mov    %esp,%ebp
  800755:	57                   	push   %edi
  800756:	56                   	push   %esi
  800757:	53                   	push   %ebx
  800758:	83 ec 1c             	sub    $0x1c,%esp
  80075b:	89 c7                	mov    %eax,%edi
  80075d:	89 d6                	mov    %edx,%esi
  80075f:	8b 45 08             	mov    0x8(%ebp),%eax
  800762:	8b 55 0c             	mov    0xc(%ebp),%edx
  800765:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800768:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80076b:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80076e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800773:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800776:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800779:	39 d3                	cmp    %edx,%ebx
  80077b:	72 05                	jb     800782 <printnum+0x30>
  80077d:	39 45 10             	cmp    %eax,0x10(%ebp)
  800780:	77 45                	ja     8007c7 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800782:	83 ec 0c             	sub    $0xc,%esp
  800785:	ff 75 18             	pushl  0x18(%ebp)
  800788:	8b 45 14             	mov    0x14(%ebp),%eax
  80078b:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80078e:	53                   	push   %ebx
  80078f:	ff 75 10             	pushl  0x10(%ebp)
  800792:	83 ec 08             	sub    $0x8,%esp
  800795:	ff 75 e4             	pushl  -0x1c(%ebp)
  800798:	ff 75 e0             	pushl  -0x20(%ebp)
  80079b:	ff 75 dc             	pushl  -0x24(%ebp)
  80079e:	ff 75 d8             	pushl  -0x28(%ebp)
  8007a1:	e8 d6 18 00 00       	call   80207c <__udivdi3>
  8007a6:	83 c4 18             	add    $0x18,%esp
  8007a9:	52                   	push   %edx
  8007aa:	50                   	push   %eax
  8007ab:	89 f2                	mov    %esi,%edx
  8007ad:	89 f8                	mov    %edi,%eax
  8007af:	e8 9e ff ff ff       	call   800752 <printnum>
  8007b4:	83 c4 20             	add    $0x20,%esp
  8007b7:	eb 16                	jmp    8007cf <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8007b9:	83 ec 08             	sub    $0x8,%esp
  8007bc:	56                   	push   %esi
  8007bd:	ff 75 18             	pushl  0x18(%ebp)
  8007c0:	ff d7                	call   *%edi
  8007c2:	83 c4 10             	add    $0x10,%esp
  8007c5:	eb 03                	jmp    8007ca <printnum+0x78>
  8007c7:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8007ca:	4b                   	dec    %ebx
  8007cb:	85 db                	test   %ebx,%ebx
  8007cd:	7f ea                	jg     8007b9 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8007cf:	83 ec 08             	sub    $0x8,%esp
  8007d2:	56                   	push   %esi
  8007d3:	83 ec 04             	sub    $0x4,%esp
  8007d6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8007d9:	ff 75 e0             	pushl  -0x20(%ebp)
  8007dc:	ff 75 dc             	pushl  -0x24(%ebp)
  8007df:	ff 75 d8             	pushl  -0x28(%ebp)
  8007e2:	e8 a5 19 00 00       	call   80218c <__umoddi3>
  8007e7:	83 c4 14             	add    $0x14,%esp
  8007ea:	0f be 80 cf 26 80 00 	movsbl 0x8026cf(%eax),%eax
  8007f1:	50                   	push   %eax
  8007f2:	ff d7                	call   *%edi
}
  8007f4:	83 c4 10             	add    $0x10,%esp
  8007f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8007fa:	5b                   	pop    %ebx
  8007fb:	5e                   	pop    %esi
  8007fc:	5f                   	pop    %edi
  8007fd:	5d                   	pop    %ebp
  8007fe:	c3                   	ret    

008007ff <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8007ff:	55                   	push   %ebp
  800800:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800802:	83 fa 01             	cmp    $0x1,%edx
  800805:	7e 0e                	jle    800815 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800807:	8b 10                	mov    (%eax),%edx
  800809:	8d 4a 08             	lea    0x8(%edx),%ecx
  80080c:	89 08                	mov    %ecx,(%eax)
  80080e:	8b 02                	mov    (%edx),%eax
  800810:	8b 52 04             	mov    0x4(%edx),%edx
  800813:	eb 22                	jmp    800837 <getuint+0x38>
	else if (lflag)
  800815:	85 d2                	test   %edx,%edx
  800817:	74 10                	je     800829 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800819:	8b 10                	mov    (%eax),%edx
  80081b:	8d 4a 04             	lea    0x4(%edx),%ecx
  80081e:	89 08                	mov    %ecx,(%eax)
  800820:	8b 02                	mov    (%edx),%eax
  800822:	ba 00 00 00 00       	mov    $0x0,%edx
  800827:	eb 0e                	jmp    800837 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800829:	8b 10                	mov    (%eax),%edx
  80082b:	8d 4a 04             	lea    0x4(%edx),%ecx
  80082e:	89 08                	mov    %ecx,(%eax)
  800830:	8b 02                	mov    (%edx),%eax
  800832:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800837:	5d                   	pop    %ebp
  800838:	c3                   	ret    

00800839 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800839:	55                   	push   %ebp
  80083a:	89 e5                	mov    %esp,%ebp
  80083c:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80083f:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800842:	8b 10                	mov    (%eax),%edx
  800844:	3b 50 04             	cmp    0x4(%eax),%edx
  800847:	73 0a                	jae    800853 <sprintputch+0x1a>
		*b->buf++ = ch;
  800849:	8d 4a 01             	lea    0x1(%edx),%ecx
  80084c:	89 08                	mov    %ecx,(%eax)
  80084e:	8b 45 08             	mov    0x8(%ebp),%eax
  800851:	88 02                	mov    %al,(%edx)
}
  800853:	5d                   	pop    %ebp
  800854:	c3                   	ret    

00800855 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800855:	55                   	push   %ebp
  800856:	89 e5                	mov    %esp,%ebp
  800858:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80085b:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80085e:	50                   	push   %eax
  80085f:	ff 75 10             	pushl  0x10(%ebp)
  800862:	ff 75 0c             	pushl  0xc(%ebp)
  800865:	ff 75 08             	pushl  0x8(%ebp)
  800868:	e8 05 00 00 00       	call   800872 <vprintfmt>
	va_end(ap);
}
  80086d:	83 c4 10             	add    $0x10,%esp
  800870:	c9                   	leave  
  800871:	c3                   	ret    

00800872 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800872:	55                   	push   %ebp
  800873:	89 e5                	mov    %esp,%ebp
  800875:	57                   	push   %edi
  800876:	56                   	push   %esi
  800877:	53                   	push   %ebx
  800878:	83 ec 2c             	sub    $0x2c,%esp
  80087b:	8b 75 08             	mov    0x8(%ebp),%esi
  80087e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800881:	8b 7d 10             	mov    0x10(%ebp),%edi
  800884:	eb 12                	jmp    800898 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800886:	85 c0                	test   %eax,%eax
  800888:	0f 84 68 03 00 00    	je     800bf6 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80088e:	83 ec 08             	sub    $0x8,%esp
  800891:	53                   	push   %ebx
  800892:	50                   	push   %eax
  800893:	ff d6                	call   *%esi
  800895:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800898:	47                   	inc    %edi
  800899:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80089d:	83 f8 25             	cmp    $0x25,%eax
  8008a0:	75 e4                	jne    800886 <vprintfmt+0x14>
  8008a2:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8008a6:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8008ad:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8008b4:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8008bb:	ba 00 00 00 00       	mov    $0x0,%edx
  8008c0:	eb 07                	jmp    8008c9 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008c2:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8008c5:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008c9:	8d 47 01             	lea    0x1(%edi),%eax
  8008cc:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8008cf:	0f b6 0f             	movzbl (%edi),%ecx
  8008d2:	8a 07                	mov    (%edi),%al
  8008d4:	83 e8 23             	sub    $0x23,%eax
  8008d7:	3c 55                	cmp    $0x55,%al
  8008d9:	0f 87 fe 02 00 00    	ja     800bdd <vprintfmt+0x36b>
  8008df:	0f b6 c0             	movzbl %al,%eax
  8008e2:	ff 24 85 20 28 80 00 	jmp    *0x802820(,%eax,4)
  8008e9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8008ec:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8008f0:	eb d7                	jmp    8008c9 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008f2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008f5:	b8 00 00 00 00       	mov    $0x0,%eax
  8008fa:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8008fd:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800900:	01 c0                	add    %eax,%eax
  800902:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800906:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800909:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80090c:	83 fa 09             	cmp    $0x9,%edx
  80090f:	77 34                	ja     800945 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800911:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800912:	eb e9                	jmp    8008fd <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800914:	8b 45 14             	mov    0x14(%ebp),%eax
  800917:	8d 48 04             	lea    0x4(%eax),%ecx
  80091a:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80091d:	8b 00                	mov    (%eax),%eax
  80091f:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800922:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800925:	eb 24                	jmp    80094b <vprintfmt+0xd9>
  800927:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80092b:	79 07                	jns    800934 <vprintfmt+0xc2>
  80092d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800934:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800937:	eb 90                	jmp    8008c9 <vprintfmt+0x57>
  800939:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80093c:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800943:	eb 84                	jmp    8008c9 <vprintfmt+0x57>
  800945:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800948:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80094b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80094f:	0f 89 74 ff ff ff    	jns    8008c9 <vprintfmt+0x57>
				width = precision, precision = -1;
  800955:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800958:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80095b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800962:	e9 62 ff ff ff       	jmp    8008c9 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800967:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800968:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80096b:	e9 59 ff ff ff       	jmp    8008c9 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800970:	8b 45 14             	mov    0x14(%ebp),%eax
  800973:	8d 50 04             	lea    0x4(%eax),%edx
  800976:	89 55 14             	mov    %edx,0x14(%ebp)
  800979:	83 ec 08             	sub    $0x8,%esp
  80097c:	53                   	push   %ebx
  80097d:	ff 30                	pushl  (%eax)
  80097f:	ff d6                	call   *%esi
			break;
  800981:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800984:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800987:	e9 0c ff ff ff       	jmp    800898 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80098c:	8b 45 14             	mov    0x14(%ebp),%eax
  80098f:	8d 50 04             	lea    0x4(%eax),%edx
  800992:	89 55 14             	mov    %edx,0x14(%ebp)
  800995:	8b 00                	mov    (%eax),%eax
  800997:	85 c0                	test   %eax,%eax
  800999:	79 02                	jns    80099d <vprintfmt+0x12b>
  80099b:	f7 d8                	neg    %eax
  80099d:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80099f:	83 f8 0f             	cmp    $0xf,%eax
  8009a2:	7f 0b                	jg     8009af <vprintfmt+0x13d>
  8009a4:	8b 04 85 80 29 80 00 	mov    0x802980(,%eax,4),%eax
  8009ab:	85 c0                	test   %eax,%eax
  8009ad:	75 18                	jne    8009c7 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8009af:	52                   	push   %edx
  8009b0:	68 e7 26 80 00       	push   $0x8026e7
  8009b5:	53                   	push   %ebx
  8009b6:	56                   	push   %esi
  8009b7:	e8 99 fe ff ff       	call   800855 <printfmt>
  8009bc:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8009bf:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8009c2:	e9 d1 fe ff ff       	jmp    800898 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8009c7:	50                   	push   %eax
  8009c8:	68 cd 2a 80 00       	push   $0x802acd
  8009cd:	53                   	push   %ebx
  8009ce:	56                   	push   %esi
  8009cf:	e8 81 fe ff ff       	call   800855 <printfmt>
  8009d4:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8009d7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8009da:	e9 b9 fe ff ff       	jmp    800898 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8009df:	8b 45 14             	mov    0x14(%ebp),%eax
  8009e2:	8d 50 04             	lea    0x4(%eax),%edx
  8009e5:	89 55 14             	mov    %edx,0x14(%ebp)
  8009e8:	8b 38                	mov    (%eax),%edi
  8009ea:	85 ff                	test   %edi,%edi
  8009ec:	75 05                	jne    8009f3 <vprintfmt+0x181>
				p = "(null)";
  8009ee:	bf e0 26 80 00       	mov    $0x8026e0,%edi
			if (width > 0 && padc != '-')
  8009f3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8009f7:	0f 8e 90 00 00 00    	jle    800a8d <vprintfmt+0x21b>
  8009fd:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800a01:	0f 84 8e 00 00 00    	je     800a95 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800a07:	83 ec 08             	sub    $0x8,%esp
  800a0a:	ff 75 d0             	pushl  -0x30(%ebp)
  800a0d:	57                   	push   %edi
  800a0e:	e8 70 02 00 00       	call   800c83 <strnlen>
  800a13:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800a16:	29 c1                	sub    %eax,%ecx
  800a18:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800a1b:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800a1e:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800a22:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800a25:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800a28:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800a2a:	eb 0d                	jmp    800a39 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800a2c:	83 ec 08             	sub    $0x8,%esp
  800a2f:	53                   	push   %ebx
  800a30:	ff 75 e4             	pushl  -0x1c(%ebp)
  800a33:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800a35:	4f                   	dec    %edi
  800a36:	83 c4 10             	add    $0x10,%esp
  800a39:	85 ff                	test   %edi,%edi
  800a3b:	7f ef                	jg     800a2c <vprintfmt+0x1ba>
  800a3d:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800a40:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800a43:	89 c8                	mov    %ecx,%eax
  800a45:	85 c9                	test   %ecx,%ecx
  800a47:	79 05                	jns    800a4e <vprintfmt+0x1dc>
  800a49:	b8 00 00 00 00       	mov    $0x0,%eax
  800a4e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800a51:	29 c1                	sub    %eax,%ecx
  800a53:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800a56:	89 75 08             	mov    %esi,0x8(%ebp)
  800a59:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800a5c:	eb 3d                	jmp    800a9b <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800a5e:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800a62:	74 19                	je     800a7d <vprintfmt+0x20b>
  800a64:	0f be c0             	movsbl %al,%eax
  800a67:	83 e8 20             	sub    $0x20,%eax
  800a6a:	83 f8 5e             	cmp    $0x5e,%eax
  800a6d:	76 0e                	jbe    800a7d <vprintfmt+0x20b>
					putch('?', putdat);
  800a6f:	83 ec 08             	sub    $0x8,%esp
  800a72:	53                   	push   %ebx
  800a73:	6a 3f                	push   $0x3f
  800a75:	ff 55 08             	call   *0x8(%ebp)
  800a78:	83 c4 10             	add    $0x10,%esp
  800a7b:	eb 0b                	jmp    800a88 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800a7d:	83 ec 08             	sub    $0x8,%esp
  800a80:	53                   	push   %ebx
  800a81:	52                   	push   %edx
  800a82:	ff 55 08             	call   *0x8(%ebp)
  800a85:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800a88:	ff 4d e4             	decl   -0x1c(%ebp)
  800a8b:	eb 0e                	jmp    800a9b <vprintfmt+0x229>
  800a8d:	89 75 08             	mov    %esi,0x8(%ebp)
  800a90:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800a93:	eb 06                	jmp    800a9b <vprintfmt+0x229>
  800a95:	89 75 08             	mov    %esi,0x8(%ebp)
  800a98:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800a9b:	47                   	inc    %edi
  800a9c:	8a 47 ff             	mov    -0x1(%edi),%al
  800a9f:	0f be d0             	movsbl %al,%edx
  800aa2:	85 d2                	test   %edx,%edx
  800aa4:	74 1d                	je     800ac3 <vprintfmt+0x251>
  800aa6:	85 f6                	test   %esi,%esi
  800aa8:	78 b4                	js     800a5e <vprintfmt+0x1ec>
  800aaa:	4e                   	dec    %esi
  800aab:	79 b1                	jns    800a5e <vprintfmt+0x1ec>
  800aad:	8b 75 08             	mov    0x8(%ebp),%esi
  800ab0:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800ab3:	eb 14                	jmp    800ac9 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800ab5:	83 ec 08             	sub    $0x8,%esp
  800ab8:	53                   	push   %ebx
  800ab9:	6a 20                	push   $0x20
  800abb:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800abd:	4f                   	dec    %edi
  800abe:	83 c4 10             	add    $0x10,%esp
  800ac1:	eb 06                	jmp    800ac9 <vprintfmt+0x257>
  800ac3:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800ac6:	8b 75 08             	mov    0x8(%ebp),%esi
  800ac9:	85 ff                	test   %edi,%edi
  800acb:	7f e8                	jg     800ab5 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800acd:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800ad0:	e9 c3 fd ff ff       	jmp    800898 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800ad5:	83 fa 01             	cmp    $0x1,%edx
  800ad8:	7e 16                	jle    800af0 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800ada:	8b 45 14             	mov    0x14(%ebp),%eax
  800add:	8d 50 08             	lea    0x8(%eax),%edx
  800ae0:	89 55 14             	mov    %edx,0x14(%ebp)
  800ae3:	8b 50 04             	mov    0x4(%eax),%edx
  800ae6:	8b 00                	mov    (%eax),%eax
  800ae8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800aeb:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800aee:	eb 32                	jmp    800b22 <vprintfmt+0x2b0>
	else if (lflag)
  800af0:	85 d2                	test   %edx,%edx
  800af2:	74 18                	je     800b0c <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800af4:	8b 45 14             	mov    0x14(%ebp),%eax
  800af7:	8d 50 04             	lea    0x4(%eax),%edx
  800afa:	89 55 14             	mov    %edx,0x14(%ebp)
  800afd:	8b 00                	mov    (%eax),%eax
  800aff:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800b02:	89 c1                	mov    %eax,%ecx
  800b04:	c1 f9 1f             	sar    $0x1f,%ecx
  800b07:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800b0a:	eb 16                	jmp    800b22 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800b0c:	8b 45 14             	mov    0x14(%ebp),%eax
  800b0f:	8d 50 04             	lea    0x4(%eax),%edx
  800b12:	89 55 14             	mov    %edx,0x14(%ebp)
  800b15:	8b 00                	mov    (%eax),%eax
  800b17:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800b1a:	89 c1                	mov    %eax,%ecx
  800b1c:	c1 f9 1f             	sar    $0x1f,%ecx
  800b1f:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800b22:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800b25:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800b28:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800b2c:	79 76                	jns    800ba4 <vprintfmt+0x332>
				putch('-', putdat);
  800b2e:	83 ec 08             	sub    $0x8,%esp
  800b31:	53                   	push   %ebx
  800b32:	6a 2d                	push   $0x2d
  800b34:	ff d6                	call   *%esi
				num = -(long long) num;
  800b36:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800b39:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800b3c:	f7 d8                	neg    %eax
  800b3e:	83 d2 00             	adc    $0x0,%edx
  800b41:	f7 da                	neg    %edx
  800b43:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800b46:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800b4b:	eb 5c                	jmp    800ba9 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800b4d:	8d 45 14             	lea    0x14(%ebp),%eax
  800b50:	e8 aa fc ff ff       	call   8007ff <getuint>
			base = 10;
  800b55:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800b5a:	eb 4d                	jmp    800ba9 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800b5c:	8d 45 14             	lea    0x14(%ebp),%eax
  800b5f:	e8 9b fc ff ff       	call   8007ff <getuint>
			base = 8;
  800b64:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800b69:	eb 3e                	jmp    800ba9 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800b6b:	83 ec 08             	sub    $0x8,%esp
  800b6e:	53                   	push   %ebx
  800b6f:	6a 30                	push   $0x30
  800b71:	ff d6                	call   *%esi
			putch('x', putdat);
  800b73:	83 c4 08             	add    $0x8,%esp
  800b76:	53                   	push   %ebx
  800b77:	6a 78                	push   $0x78
  800b79:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800b7b:	8b 45 14             	mov    0x14(%ebp),%eax
  800b7e:	8d 50 04             	lea    0x4(%eax),%edx
  800b81:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800b84:	8b 00                	mov    (%eax),%eax
  800b86:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800b8b:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800b8e:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800b93:	eb 14                	jmp    800ba9 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800b95:	8d 45 14             	lea    0x14(%ebp),%eax
  800b98:	e8 62 fc ff ff       	call   8007ff <getuint>
			base = 16;
  800b9d:	b9 10 00 00 00       	mov    $0x10,%ecx
  800ba2:	eb 05                	jmp    800ba9 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800ba4:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800ba9:	83 ec 0c             	sub    $0xc,%esp
  800bac:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800bb0:	57                   	push   %edi
  800bb1:	ff 75 e4             	pushl  -0x1c(%ebp)
  800bb4:	51                   	push   %ecx
  800bb5:	52                   	push   %edx
  800bb6:	50                   	push   %eax
  800bb7:	89 da                	mov    %ebx,%edx
  800bb9:	89 f0                	mov    %esi,%eax
  800bbb:	e8 92 fb ff ff       	call   800752 <printnum>
			break;
  800bc0:	83 c4 20             	add    $0x20,%esp
  800bc3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800bc6:	e9 cd fc ff ff       	jmp    800898 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800bcb:	83 ec 08             	sub    $0x8,%esp
  800bce:	53                   	push   %ebx
  800bcf:	51                   	push   %ecx
  800bd0:	ff d6                	call   *%esi
			break;
  800bd2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800bd5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800bd8:	e9 bb fc ff ff       	jmp    800898 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800bdd:	83 ec 08             	sub    $0x8,%esp
  800be0:	53                   	push   %ebx
  800be1:	6a 25                	push   $0x25
  800be3:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800be5:	83 c4 10             	add    $0x10,%esp
  800be8:	eb 01                	jmp    800beb <vprintfmt+0x379>
  800bea:	4f                   	dec    %edi
  800beb:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800bef:	75 f9                	jne    800bea <vprintfmt+0x378>
  800bf1:	e9 a2 fc ff ff       	jmp    800898 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800bf6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf9:	5b                   	pop    %ebx
  800bfa:	5e                   	pop    %esi
  800bfb:	5f                   	pop    %edi
  800bfc:	5d                   	pop    %ebp
  800bfd:	c3                   	ret    

00800bfe <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800bfe:	55                   	push   %ebp
  800bff:	89 e5                	mov    %esp,%ebp
  800c01:	83 ec 18             	sub    $0x18,%esp
  800c04:	8b 45 08             	mov    0x8(%ebp),%eax
  800c07:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800c0a:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800c0d:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800c11:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800c14:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800c1b:	85 c0                	test   %eax,%eax
  800c1d:	74 26                	je     800c45 <vsnprintf+0x47>
  800c1f:	85 d2                	test   %edx,%edx
  800c21:	7e 29                	jle    800c4c <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800c23:	ff 75 14             	pushl  0x14(%ebp)
  800c26:	ff 75 10             	pushl  0x10(%ebp)
  800c29:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800c2c:	50                   	push   %eax
  800c2d:	68 39 08 80 00       	push   $0x800839
  800c32:	e8 3b fc ff ff       	call   800872 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800c37:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800c3a:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800c3d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800c40:	83 c4 10             	add    $0x10,%esp
  800c43:	eb 0c                	jmp    800c51 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800c45:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800c4a:	eb 05                	jmp    800c51 <vsnprintf+0x53>
  800c4c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800c51:	c9                   	leave  
  800c52:	c3                   	ret    

00800c53 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800c53:	55                   	push   %ebp
  800c54:	89 e5                	mov    %esp,%ebp
  800c56:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800c59:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800c5c:	50                   	push   %eax
  800c5d:	ff 75 10             	pushl  0x10(%ebp)
  800c60:	ff 75 0c             	pushl  0xc(%ebp)
  800c63:	ff 75 08             	pushl  0x8(%ebp)
  800c66:	e8 93 ff ff ff       	call   800bfe <vsnprintf>
	va_end(ap);

	return rc;
}
  800c6b:	c9                   	leave  
  800c6c:	c3                   	ret    

00800c6d <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800c6d:	55                   	push   %ebp
  800c6e:	89 e5                	mov    %esp,%ebp
  800c70:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800c73:	b8 00 00 00 00       	mov    $0x0,%eax
  800c78:	eb 01                	jmp    800c7b <strlen+0xe>
		n++;
  800c7a:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800c7b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800c7f:	75 f9                	jne    800c7a <strlen+0xd>
		n++;
	return n;
}
  800c81:	5d                   	pop    %ebp
  800c82:	c3                   	ret    

00800c83 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800c83:	55                   	push   %ebp
  800c84:	89 e5                	mov    %esp,%ebp
  800c86:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800c89:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800c8c:	ba 00 00 00 00       	mov    $0x0,%edx
  800c91:	eb 01                	jmp    800c94 <strnlen+0x11>
		n++;
  800c93:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800c94:	39 c2                	cmp    %eax,%edx
  800c96:	74 08                	je     800ca0 <strnlen+0x1d>
  800c98:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800c9c:	75 f5                	jne    800c93 <strnlen+0x10>
  800c9e:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800ca0:	5d                   	pop    %ebp
  800ca1:	c3                   	ret    

00800ca2 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800ca2:	55                   	push   %ebp
  800ca3:	89 e5                	mov    %esp,%ebp
  800ca5:	53                   	push   %ebx
  800ca6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ca9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800cac:	89 c2                	mov    %eax,%edx
  800cae:	42                   	inc    %edx
  800caf:	41                   	inc    %ecx
  800cb0:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800cb3:	88 5a ff             	mov    %bl,-0x1(%edx)
  800cb6:	84 db                	test   %bl,%bl
  800cb8:	75 f4                	jne    800cae <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800cba:	5b                   	pop    %ebx
  800cbb:	5d                   	pop    %ebp
  800cbc:	c3                   	ret    

00800cbd <strcat>:

char *
strcat(char *dst, const char *src)
{
  800cbd:	55                   	push   %ebp
  800cbe:	89 e5                	mov    %esp,%ebp
  800cc0:	53                   	push   %ebx
  800cc1:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800cc4:	53                   	push   %ebx
  800cc5:	e8 a3 ff ff ff       	call   800c6d <strlen>
  800cca:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800ccd:	ff 75 0c             	pushl  0xc(%ebp)
  800cd0:	01 d8                	add    %ebx,%eax
  800cd2:	50                   	push   %eax
  800cd3:	e8 ca ff ff ff       	call   800ca2 <strcpy>
	return dst;
}
  800cd8:	89 d8                	mov    %ebx,%eax
  800cda:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800cdd:	c9                   	leave  
  800cde:	c3                   	ret    

00800cdf <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800cdf:	55                   	push   %ebp
  800ce0:	89 e5                	mov    %esp,%ebp
  800ce2:	56                   	push   %esi
  800ce3:	53                   	push   %ebx
  800ce4:	8b 75 08             	mov    0x8(%ebp),%esi
  800ce7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cea:	89 f3                	mov    %esi,%ebx
  800cec:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800cef:	89 f2                	mov    %esi,%edx
  800cf1:	eb 0c                	jmp    800cff <strncpy+0x20>
		*dst++ = *src;
  800cf3:	42                   	inc    %edx
  800cf4:	8a 01                	mov    (%ecx),%al
  800cf6:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800cf9:	80 39 01             	cmpb   $0x1,(%ecx)
  800cfc:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800cff:	39 da                	cmp    %ebx,%edx
  800d01:	75 f0                	jne    800cf3 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800d03:	89 f0                	mov    %esi,%eax
  800d05:	5b                   	pop    %ebx
  800d06:	5e                   	pop    %esi
  800d07:	5d                   	pop    %ebp
  800d08:	c3                   	ret    

00800d09 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800d09:	55                   	push   %ebp
  800d0a:	89 e5                	mov    %esp,%ebp
  800d0c:	56                   	push   %esi
  800d0d:	53                   	push   %ebx
  800d0e:	8b 75 08             	mov    0x8(%ebp),%esi
  800d11:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d14:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800d17:	85 c0                	test   %eax,%eax
  800d19:	74 1e                	je     800d39 <strlcpy+0x30>
  800d1b:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800d1f:	89 f2                	mov    %esi,%edx
  800d21:	eb 05                	jmp    800d28 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800d23:	42                   	inc    %edx
  800d24:	41                   	inc    %ecx
  800d25:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800d28:	39 c2                	cmp    %eax,%edx
  800d2a:	74 08                	je     800d34 <strlcpy+0x2b>
  800d2c:	8a 19                	mov    (%ecx),%bl
  800d2e:	84 db                	test   %bl,%bl
  800d30:	75 f1                	jne    800d23 <strlcpy+0x1a>
  800d32:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800d34:	c6 00 00             	movb   $0x0,(%eax)
  800d37:	eb 02                	jmp    800d3b <strlcpy+0x32>
  800d39:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800d3b:	29 f0                	sub    %esi,%eax
}
  800d3d:	5b                   	pop    %ebx
  800d3e:	5e                   	pop    %esi
  800d3f:	5d                   	pop    %ebp
  800d40:	c3                   	ret    

00800d41 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800d41:	55                   	push   %ebp
  800d42:	89 e5                	mov    %esp,%ebp
  800d44:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800d47:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800d4a:	eb 02                	jmp    800d4e <strcmp+0xd>
		p++, q++;
  800d4c:	41                   	inc    %ecx
  800d4d:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800d4e:	8a 01                	mov    (%ecx),%al
  800d50:	84 c0                	test   %al,%al
  800d52:	74 04                	je     800d58 <strcmp+0x17>
  800d54:	3a 02                	cmp    (%edx),%al
  800d56:	74 f4                	je     800d4c <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800d58:	0f b6 c0             	movzbl %al,%eax
  800d5b:	0f b6 12             	movzbl (%edx),%edx
  800d5e:	29 d0                	sub    %edx,%eax
}
  800d60:	5d                   	pop    %ebp
  800d61:	c3                   	ret    

00800d62 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800d62:	55                   	push   %ebp
  800d63:	89 e5                	mov    %esp,%ebp
  800d65:	53                   	push   %ebx
  800d66:	8b 45 08             	mov    0x8(%ebp),%eax
  800d69:	8b 55 0c             	mov    0xc(%ebp),%edx
  800d6c:	89 c3                	mov    %eax,%ebx
  800d6e:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800d71:	eb 02                	jmp    800d75 <strncmp+0x13>
		n--, p++, q++;
  800d73:	40                   	inc    %eax
  800d74:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800d75:	39 d8                	cmp    %ebx,%eax
  800d77:	74 14                	je     800d8d <strncmp+0x2b>
  800d79:	8a 08                	mov    (%eax),%cl
  800d7b:	84 c9                	test   %cl,%cl
  800d7d:	74 04                	je     800d83 <strncmp+0x21>
  800d7f:	3a 0a                	cmp    (%edx),%cl
  800d81:	74 f0                	je     800d73 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800d83:	0f b6 00             	movzbl (%eax),%eax
  800d86:	0f b6 12             	movzbl (%edx),%edx
  800d89:	29 d0                	sub    %edx,%eax
  800d8b:	eb 05                	jmp    800d92 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800d8d:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800d92:	5b                   	pop    %ebx
  800d93:	5d                   	pop    %ebp
  800d94:	c3                   	ret    

00800d95 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800d95:	55                   	push   %ebp
  800d96:	89 e5                	mov    %esp,%ebp
  800d98:	8b 45 08             	mov    0x8(%ebp),%eax
  800d9b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800d9e:	eb 05                	jmp    800da5 <strchr+0x10>
		if (*s == c)
  800da0:	38 ca                	cmp    %cl,%dl
  800da2:	74 0c                	je     800db0 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800da4:	40                   	inc    %eax
  800da5:	8a 10                	mov    (%eax),%dl
  800da7:	84 d2                	test   %dl,%dl
  800da9:	75 f5                	jne    800da0 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800dab:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800db0:	5d                   	pop    %ebp
  800db1:	c3                   	ret    

00800db2 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800db2:	55                   	push   %ebp
  800db3:	89 e5                	mov    %esp,%ebp
  800db5:	8b 45 08             	mov    0x8(%ebp),%eax
  800db8:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800dbb:	eb 05                	jmp    800dc2 <strfind+0x10>
		if (*s == c)
  800dbd:	38 ca                	cmp    %cl,%dl
  800dbf:	74 07                	je     800dc8 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800dc1:	40                   	inc    %eax
  800dc2:	8a 10                	mov    (%eax),%dl
  800dc4:	84 d2                	test   %dl,%dl
  800dc6:	75 f5                	jne    800dbd <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800dc8:	5d                   	pop    %ebp
  800dc9:	c3                   	ret    

00800dca <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800dca:	55                   	push   %ebp
  800dcb:	89 e5                	mov    %esp,%ebp
  800dcd:	57                   	push   %edi
  800dce:	56                   	push   %esi
  800dcf:	53                   	push   %ebx
  800dd0:	8b 7d 08             	mov    0x8(%ebp),%edi
  800dd3:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800dd6:	85 c9                	test   %ecx,%ecx
  800dd8:	74 36                	je     800e10 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800dda:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800de0:	75 28                	jne    800e0a <memset+0x40>
  800de2:	f6 c1 03             	test   $0x3,%cl
  800de5:	75 23                	jne    800e0a <memset+0x40>
		c &= 0xFF;
  800de7:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800deb:	89 d3                	mov    %edx,%ebx
  800ded:	c1 e3 08             	shl    $0x8,%ebx
  800df0:	89 d6                	mov    %edx,%esi
  800df2:	c1 e6 18             	shl    $0x18,%esi
  800df5:	89 d0                	mov    %edx,%eax
  800df7:	c1 e0 10             	shl    $0x10,%eax
  800dfa:	09 f0                	or     %esi,%eax
  800dfc:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800dfe:	89 d8                	mov    %ebx,%eax
  800e00:	09 d0                	or     %edx,%eax
  800e02:	c1 e9 02             	shr    $0x2,%ecx
  800e05:	fc                   	cld    
  800e06:	f3 ab                	rep stos %eax,%es:(%edi)
  800e08:	eb 06                	jmp    800e10 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800e0a:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e0d:	fc                   	cld    
  800e0e:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800e10:	89 f8                	mov    %edi,%eax
  800e12:	5b                   	pop    %ebx
  800e13:	5e                   	pop    %esi
  800e14:	5f                   	pop    %edi
  800e15:	5d                   	pop    %ebp
  800e16:	c3                   	ret    

00800e17 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800e17:	55                   	push   %ebp
  800e18:	89 e5                	mov    %esp,%ebp
  800e1a:	57                   	push   %edi
  800e1b:	56                   	push   %esi
  800e1c:	8b 45 08             	mov    0x8(%ebp),%eax
  800e1f:	8b 75 0c             	mov    0xc(%ebp),%esi
  800e22:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800e25:	39 c6                	cmp    %eax,%esi
  800e27:	73 33                	jae    800e5c <memmove+0x45>
  800e29:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800e2c:	39 d0                	cmp    %edx,%eax
  800e2e:	73 2c                	jae    800e5c <memmove+0x45>
		s += n;
		d += n;
  800e30:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800e33:	89 d6                	mov    %edx,%esi
  800e35:	09 fe                	or     %edi,%esi
  800e37:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800e3d:	75 13                	jne    800e52 <memmove+0x3b>
  800e3f:	f6 c1 03             	test   $0x3,%cl
  800e42:	75 0e                	jne    800e52 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800e44:	83 ef 04             	sub    $0x4,%edi
  800e47:	8d 72 fc             	lea    -0x4(%edx),%esi
  800e4a:	c1 e9 02             	shr    $0x2,%ecx
  800e4d:	fd                   	std    
  800e4e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800e50:	eb 07                	jmp    800e59 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800e52:	4f                   	dec    %edi
  800e53:	8d 72 ff             	lea    -0x1(%edx),%esi
  800e56:	fd                   	std    
  800e57:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800e59:	fc                   	cld    
  800e5a:	eb 1d                	jmp    800e79 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800e5c:	89 f2                	mov    %esi,%edx
  800e5e:	09 c2                	or     %eax,%edx
  800e60:	f6 c2 03             	test   $0x3,%dl
  800e63:	75 0f                	jne    800e74 <memmove+0x5d>
  800e65:	f6 c1 03             	test   $0x3,%cl
  800e68:	75 0a                	jne    800e74 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800e6a:	c1 e9 02             	shr    $0x2,%ecx
  800e6d:	89 c7                	mov    %eax,%edi
  800e6f:	fc                   	cld    
  800e70:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800e72:	eb 05                	jmp    800e79 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800e74:	89 c7                	mov    %eax,%edi
  800e76:	fc                   	cld    
  800e77:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800e79:	5e                   	pop    %esi
  800e7a:	5f                   	pop    %edi
  800e7b:	5d                   	pop    %ebp
  800e7c:	c3                   	ret    

00800e7d <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800e7d:	55                   	push   %ebp
  800e7e:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800e80:	ff 75 10             	pushl  0x10(%ebp)
  800e83:	ff 75 0c             	pushl  0xc(%ebp)
  800e86:	ff 75 08             	pushl  0x8(%ebp)
  800e89:	e8 89 ff ff ff       	call   800e17 <memmove>
}
  800e8e:	c9                   	leave  
  800e8f:	c3                   	ret    

00800e90 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800e90:	55                   	push   %ebp
  800e91:	89 e5                	mov    %esp,%ebp
  800e93:	56                   	push   %esi
  800e94:	53                   	push   %ebx
  800e95:	8b 45 08             	mov    0x8(%ebp),%eax
  800e98:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e9b:	89 c6                	mov    %eax,%esi
  800e9d:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ea0:	eb 14                	jmp    800eb6 <memcmp+0x26>
		if (*s1 != *s2)
  800ea2:	8a 08                	mov    (%eax),%cl
  800ea4:	8a 1a                	mov    (%edx),%bl
  800ea6:	38 d9                	cmp    %bl,%cl
  800ea8:	74 0a                	je     800eb4 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800eaa:	0f b6 c1             	movzbl %cl,%eax
  800ead:	0f b6 db             	movzbl %bl,%ebx
  800eb0:	29 d8                	sub    %ebx,%eax
  800eb2:	eb 0b                	jmp    800ebf <memcmp+0x2f>
		s1++, s2++;
  800eb4:	40                   	inc    %eax
  800eb5:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800eb6:	39 f0                	cmp    %esi,%eax
  800eb8:	75 e8                	jne    800ea2 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800eba:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ebf:	5b                   	pop    %ebx
  800ec0:	5e                   	pop    %esi
  800ec1:	5d                   	pop    %ebp
  800ec2:	c3                   	ret    

00800ec3 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800ec3:	55                   	push   %ebp
  800ec4:	89 e5                	mov    %esp,%ebp
  800ec6:	53                   	push   %ebx
  800ec7:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800eca:	89 c1                	mov    %eax,%ecx
  800ecc:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800ecf:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ed3:	eb 08                	jmp    800edd <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800ed5:	0f b6 10             	movzbl (%eax),%edx
  800ed8:	39 da                	cmp    %ebx,%edx
  800eda:	74 05                	je     800ee1 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800edc:	40                   	inc    %eax
  800edd:	39 c8                	cmp    %ecx,%eax
  800edf:	72 f4                	jb     800ed5 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800ee1:	5b                   	pop    %ebx
  800ee2:	5d                   	pop    %ebp
  800ee3:	c3                   	ret    

00800ee4 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800ee4:	55                   	push   %ebp
  800ee5:	89 e5                	mov    %esp,%ebp
  800ee7:	57                   	push   %edi
  800ee8:	56                   	push   %esi
  800ee9:	53                   	push   %ebx
  800eea:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800eed:	eb 01                	jmp    800ef0 <strtol+0xc>
		s++;
  800eef:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800ef0:	8a 01                	mov    (%ecx),%al
  800ef2:	3c 20                	cmp    $0x20,%al
  800ef4:	74 f9                	je     800eef <strtol+0xb>
  800ef6:	3c 09                	cmp    $0x9,%al
  800ef8:	74 f5                	je     800eef <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800efa:	3c 2b                	cmp    $0x2b,%al
  800efc:	75 08                	jne    800f06 <strtol+0x22>
		s++;
  800efe:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800eff:	bf 00 00 00 00       	mov    $0x0,%edi
  800f04:	eb 11                	jmp    800f17 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800f06:	3c 2d                	cmp    $0x2d,%al
  800f08:	75 08                	jne    800f12 <strtol+0x2e>
		s++, neg = 1;
  800f0a:	41                   	inc    %ecx
  800f0b:	bf 01 00 00 00       	mov    $0x1,%edi
  800f10:	eb 05                	jmp    800f17 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800f12:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800f17:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800f1b:	0f 84 87 00 00 00    	je     800fa8 <strtol+0xc4>
  800f21:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800f25:	75 27                	jne    800f4e <strtol+0x6a>
  800f27:	80 39 30             	cmpb   $0x30,(%ecx)
  800f2a:	75 22                	jne    800f4e <strtol+0x6a>
  800f2c:	e9 88 00 00 00       	jmp    800fb9 <strtol+0xd5>
		s += 2, base = 16;
  800f31:	83 c1 02             	add    $0x2,%ecx
  800f34:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800f3b:	eb 11                	jmp    800f4e <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800f3d:	41                   	inc    %ecx
  800f3e:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800f45:	eb 07                	jmp    800f4e <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800f47:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800f4e:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800f53:	8a 11                	mov    (%ecx),%dl
  800f55:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800f58:	80 fb 09             	cmp    $0x9,%bl
  800f5b:	77 08                	ja     800f65 <strtol+0x81>
			dig = *s - '0';
  800f5d:	0f be d2             	movsbl %dl,%edx
  800f60:	83 ea 30             	sub    $0x30,%edx
  800f63:	eb 22                	jmp    800f87 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800f65:	8d 72 9f             	lea    -0x61(%edx),%esi
  800f68:	89 f3                	mov    %esi,%ebx
  800f6a:	80 fb 19             	cmp    $0x19,%bl
  800f6d:	77 08                	ja     800f77 <strtol+0x93>
			dig = *s - 'a' + 10;
  800f6f:	0f be d2             	movsbl %dl,%edx
  800f72:	83 ea 57             	sub    $0x57,%edx
  800f75:	eb 10                	jmp    800f87 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800f77:	8d 72 bf             	lea    -0x41(%edx),%esi
  800f7a:	89 f3                	mov    %esi,%ebx
  800f7c:	80 fb 19             	cmp    $0x19,%bl
  800f7f:	77 14                	ja     800f95 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800f81:	0f be d2             	movsbl %dl,%edx
  800f84:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800f87:	3b 55 10             	cmp    0x10(%ebp),%edx
  800f8a:	7d 09                	jge    800f95 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800f8c:	41                   	inc    %ecx
  800f8d:	0f af 45 10          	imul   0x10(%ebp),%eax
  800f91:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800f93:	eb be                	jmp    800f53 <strtol+0x6f>

	if (endptr)
  800f95:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800f99:	74 05                	je     800fa0 <strtol+0xbc>
		*endptr = (char *) s;
  800f9b:	8b 75 0c             	mov    0xc(%ebp),%esi
  800f9e:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800fa0:	85 ff                	test   %edi,%edi
  800fa2:	74 21                	je     800fc5 <strtol+0xe1>
  800fa4:	f7 d8                	neg    %eax
  800fa6:	eb 1d                	jmp    800fc5 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800fa8:	80 39 30             	cmpb   $0x30,(%ecx)
  800fab:	75 9a                	jne    800f47 <strtol+0x63>
  800fad:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800fb1:	0f 84 7a ff ff ff    	je     800f31 <strtol+0x4d>
  800fb7:	eb 84                	jmp    800f3d <strtol+0x59>
  800fb9:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800fbd:	0f 84 6e ff ff ff    	je     800f31 <strtol+0x4d>
  800fc3:	eb 89                	jmp    800f4e <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800fc5:	5b                   	pop    %ebx
  800fc6:	5e                   	pop    %esi
  800fc7:	5f                   	pop    %edi
  800fc8:	5d                   	pop    %ebp
  800fc9:	c3                   	ret    

00800fca <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800fca:	55                   	push   %ebp
  800fcb:	89 e5                	mov    %esp,%ebp
  800fcd:	57                   	push   %edi
  800fce:	56                   	push   %esi
  800fcf:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fd0:	b8 00 00 00 00       	mov    $0x0,%eax
  800fd5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800fd8:	8b 55 08             	mov    0x8(%ebp),%edx
  800fdb:	89 c3                	mov    %eax,%ebx
  800fdd:	89 c7                	mov    %eax,%edi
  800fdf:	89 c6                	mov    %eax,%esi
  800fe1:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800fe3:	5b                   	pop    %ebx
  800fe4:	5e                   	pop    %esi
  800fe5:	5f                   	pop    %edi
  800fe6:	5d                   	pop    %ebp
  800fe7:	c3                   	ret    

00800fe8 <sys_cgetc>:

int
sys_cgetc(void)
{
  800fe8:	55                   	push   %ebp
  800fe9:	89 e5                	mov    %esp,%ebp
  800feb:	57                   	push   %edi
  800fec:	56                   	push   %esi
  800fed:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fee:	ba 00 00 00 00       	mov    $0x0,%edx
  800ff3:	b8 01 00 00 00       	mov    $0x1,%eax
  800ff8:	89 d1                	mov    %edx,%ecx
  800ffa:	89 d3                	mov    %edx,%ebx
  800ffc:	89 d7                	mov    %edx,%edi
  800ffe:	89 d6                	mov    %edx,%esi
  801000:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  801002:	5b                   	pop    %ebx
  801003:	5e                   	pop    %esi
  801004:	5f                   	pop    %edi
  801005:	5d                   	pop    %ebp
  801006:	c3                   	ret    

00801007 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  801007:	55                   	push   %ebp
  801008:	89 e5                	mov    %esp,%ebp
  80100a:	57                   	push   %edi
  80100b:	56                   	push   %esi
  80100c:	53                   	push   %ebx
  80100d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801010:	b9 00 00 00 00       	mov    $0x0,%ecx
  801015:	b8 03 00 00 00       	mov    $0x3,%eax
  80101a:	8b 55 08             	mov    0x8(%ebp),%edx
  80101d:	89 cb                	mov    %ecx,%ebx
  80101f:	89 cf                	mov    %ecx,%edi
  801021:	89 ce                	mov    %ecx,%esi
  801023:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801025:	85 c0                	test   %eax,%eax
  801027:	7e 17                	jle    801040 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  801029:	83 ec 0c             	sub    $0xc,%esp
  80102c:	50                   	push   %eax
  80102d:	6a 03                	push   $0x3
  80102f:	68 df 29 80 00       	push   $0x8029df
  801034:	6a 23                	push   $0x23
  801036:	68 fc 29 80 00       	push   $0x8029fc
  80103b:	e8 26 f6 ff ff       	call   800666 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  801040:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801043:	5b                   	pop    %ebx
  801044:	5e                   	pop    %esi
  801045:	5f                   	pop    %edi
  801046:	5d                   	pop    %ebp
  801047:	c3                   	ret    

00801048 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  801048:	55                   	push   %ebp
  801049:	89 e5                	mov    %esp,%ebp
  80104b:	57                   	push   %edi
  80104c:	56                   	push   %esi
  80104d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80104e:	ba 00 00 00 00       	mov    $0x0,%edx
  801053:	b8 02 00 00 00       	mov    $0x2,%eax
  801058:	89 d1                	mov    %edx,%ecx
  80105a:	89 d3                	mov    %edx,%ebx
  80105c:	89 d7                	mov    %edx,%edi
  80105e:	89 d6                	mov    %edx,%esi
  801060:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  801062:	5b                   	pop    %ebx
  801063:	5e                   	pop    %esi
  801064:	5f                   	pop    %edi
  801065:	5d                   	pop    %ebp
  801066:	c3                   	ret    

00801067 <sys_yield>:

void
sys_yield(void)
{
  801067:	55                   	push   %ebp
  801068:	89 e5                	mov    %esp,%ebp
  80106a:	57                   	push   %edi
  80106b:	56                   	push   %esi
  80106c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80106d:	ba 00 00 00 00       	mov    $0x0,%edx
  801072:	b8 0b 00 00 00       	mov    $0xb,%eax
  801077:	89 d1                	mov    %edx,%ecx
  801079:	89 d3                	mov    %edx,%ebx
  80107b:	89 d7                	mov    %edx,%edi
  80107d:	89 d6                	mov    %edx,%esi
  80107f:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  801081:	5b                   	pop    %ebx
  801082:	5e                   	pop    %esi
  801083:	5f                   	pop    %edi
  801084:	5d                   	pop    %ebp
  801085:	c3                   	ret    

00801086 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  801086:	55                   	push   %ebp
  801087:	89 e5                	mov    %esp,%ebp
  801089:	57                   	push   %edi
  80108a:	56                   	push   %esi
  80108b:	53                   	push   %ebx
  80108c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80108f:	be 00 00 00 00       	mov    $0x0,%esi
  801094:	b8 04 00 00 00       	mov    $0x4,%eax
  801099:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80109c:	8b 55 08             	mov    0x8(%ebp),%edx
  80109f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8010a2:	89 f7                	mov    %esi,%edi
  8010a4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8010a6:	85 c0                	test   %eax,%eax
  8010a8:	7e 17                	jle    8010c1 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  8010aa:	83 ec 0c             	sub    $0xc,%esp
  8010ad:	50                   	push   %eax
  8010ae:	6a 04                	push   $0x4
  8010b0:	68 df 29 80 00       	push   $0x8029df
  8010b5:	6a 23                	push   $0x23
  8010b7:	68 fc 29 80 00       	push   $0x8029fc
  8010bc:	e8 a5 f5 ff ff       	call   800666 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8010c1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010c4:	5b                   	pop    %ebx
  8010c5:	5e                   	pop    %esi
  8010c6:	5f                   	pop    %edi
  8010c7:	5d                   	pop    %ebp
  8010c8:	c3                   	ret    

008010c9 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8010c9:	55                   	push   %ebp
  8010ca:	89 e5                	mov    %esp,%ebp
  8010cc:	57                   	push   %edi
  8010cd:	56                   	push   %esi
  8010ce:	53                   	push   %ebx
  8010cf:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8010d2:	b8 05 00 00 00       	mov    $0x5,%eax
  8010d7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8010da:	8b 55 08             	mov    0x8(%ebp),%edx
  8010dd:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8010e0:	8b 7d 14             	mov    0x14(%ebp),%edi
  8010e3:	8b 75 18             	mov    0x18(%ebp),%esi
  8010e6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8010e8:	85 c0                	test   %eax,%eax
  8010ea:	7e 17                	jle    801103 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8010ec:	83 ec 0c             	sub    $0xc,%esp
  8010ef:	50                   	push   %eax
  8010f0:	6a 05                	push   $0x5
  8010f2:	68 df 29 80 00       	push   $0x8029df
  8010f7:	6a 23                	push   $0x23
  8010f9:	68 fc 29 80 00       	push   $0x8029fc
  8010fe:	e8 63 f5 ff ff       	call   800666 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  801103:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801106:	5b                   	pop    %ebx
  801107:	5e                   	pop    %esi
  801108:	5f                   	pop    %edi
  801109:	5d                   	pop    %ebp
  80110a:	c3                   	ret    

0080110b <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  80110b:	55                   	push   %ebp
  80110c:	89 e5                	mov    %esp,%ebp
  80110e:	57                   	push   %edi
  80110f:	56                   	push   %esi
  801110:	53                   	push   %ebx
  801111:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801114:	bb 00 00 00 00       	mov    $0x0,%ebx
  801119:	b8 06 00 00 00       	mov    $0x6,%eax
  80111e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801121:	8b 55 08             	mov    0x8(%ebp),%edx
  801124:	89 df                	mov    %ebx,%edi
  801126:	89 de                	mov    %ebx,%esi
  801128:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80112a:	85 c0                	test   %eax,%eax
  80112c:	7e 17                	jle    801145 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80112e:	83 ec 0c             	sub    $0xc,%esp
  801131:	50                   	push   %eax
  801132:	6a 06                	push   $0x6
  801134:	68 df 29 80 00       	push   $0x8029df
  801139:	6a 23                	push   $0x23
  80113b:	68 fc 29 80 00       	push   $0x8029fc
  801140:	e8 21 f5 ff ff       	call   800666 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  801145:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801148:	5b                   	pop    %ebx
  801149:	5e                   	pop    %esi
  80114a:	5f                   	pop    %edi
  80114b:	5d                   	pop    %ebp
  80114c:	c3                   	ret    

0080114d <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80114d:	55                   	push   %ebp
  80114e:	89 e5                	mov    %esp,%ebp
  801150:	57                   	push   %edi
  801151:	56                   	push   %esi
  801152:	53                   	push   %ebx
  801153:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801156:	bb 00 00 00 00       	mov    $0x0,%ebx
  80115b:	b8 08 00 00 00       	mov    $0x8,%eax
  801160:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801163:	8b 55 08             	mov    0x8(%ebp),%edx
  801166:	89 df                	mov    %ebx,%edi
  801168:	89 de                	mov    %ebx,%esi
  80116a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80116c:	85 c0                	test   %eax,%eax
  80116e:	7e 17                	jle    801187 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  801170:	83 ec 0c             	sub    $0xc,%esp
  801173:	50                   	push   %eax
  801174:	6a 08                	push   $0x8
  801176:	68 df 29 80 00       	push   $0x8029df
  80117b:	6a 23                	push   $0x23
  80117d:	68 fc 29 80 00       	push   $0x8029fc
  801182:	e8 df f4 ff ff       	call   800666 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  801187:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80118a:	5b                   	pop    %ebx
  80118b:	5e                   	pop    %esi
  80118c:	5f                   	pop    %edi
  80118d:	5d                   	pop    %ebp
  80118e:	c3                   	ret    

0080118f <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80118f:	55                   	push   %ebp
  801190:	89 e5                	mov    %esp,%ebp
  801192:	57                   	push   %edi
  801193:	56                   	push   %esi
  801194:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801195:	ba 00 00 00 00       	mov    $0x0,%edx
  80119a:	b8 0c 00 00 00       	mov    $0xc,%eax
  80119f:	89 d1                	mov    %edx,%ecx
  8011a1:	89 d3                	mov    %edx,%ebx
  8011a3:	89 d7                	mov    %edx,%edi
  8011a5:	89 d6                	mov    %edx,%esi
  8011a7:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  8011a9:	5b                   	pop    %ebx
  8011aa:	5e                   	pop    %esi
  8011ab:	5f                   	pop    %edi
  8011ac:	5d                   	pop    %ebp
  8011ad:	c3                   	ret    

008011ae <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  8011ae:	55                   	push   %ebp
  8011af:	89 e5                	mov    %esp,%ebp
  8011b1:	57                   	push   %edi
  8011b2:	56                   	push   %esi
  8011b3:	53                   	push   %ebx
  8011b4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8011b7:	bb 00 00 00 00       	mov    $0x0,%ebx
  8011bc:	b8 09 00 00 00       	mov    $0x9,%eax
  8011c1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8011c4:	8b 55 08             	mov    0x8(%ebp),%edx
  8011c7:	89 df                	mov    %ebx,%edi
  8011c9:	89 de                	mov    %ebx,%esi
  8011cb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8011cd:	85 c0                	test   %eax,%eax
  8011cf:	7e 17                	jle    8011e8 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8011d1:	83 ec 0c             	sub    $0xc,%esp
  8011d4:	50                   	push   %eax
  8011d5:	6a 09                	push   $0x9
  8011d7:	68 df 29 80 00       	push   $0x8029df
  8011dc:	6a 23                	push   $0x23
  8011de:	68 fc 29 80 00       	push   $0x8029fc
  8011e3:	e8 7e f4 ff ff       	call   800666 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8011e8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8011eb:	5b                   	pop    %ebx
  8011ec:	5e                   	pop    %esi
  8011ed:	5f                   	pop    %edi
  8011ee:	5d                   	pop    %ebp
  8011ef:	c3                   	ret    

008011f0 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8011f0:	55                   	push   %ebp
  8011f1:	89 e5                	mov    %esp,%ebp
  8011f3:	57                   	push   %edi
  8011f4:	56                   	push   %esi
  8011f5:	53                   	push   %ebx
  8011f6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8011f9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8011fe:	b8 0a 00 00 00       	mov    $0xa,%eax
  801203:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801206:	8b 55 08             	mov    0x8(%ebp),%edx
  801209:	89 df                	mov    %ebx,%edi
  80120b:	89 de                	mov    %ebx,%esi
  80120d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80120f:	85 c0                	test   %eax,%eax
  801211:	7e 17                	jle    80122a <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  801213:	83 ec 0c             	sub    $0xc,%esp
  801216:	50                   	push   %eax
  801217:	6a 0a                	push   $0xa
  801219:	68 df 29 80 00       	push   $0x8029df
  80121e:	6a 23                	push   $0x23
  801220:	68 fc 29 80 00       	push   $0x8029fc
  801225:	e8 3c f4 ff ff       	call   800666 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  80122a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80122d:	5b                   	pop    %ebx
  80122e:	5e                   	pop    %esi
  80122f:	5f                   	pop    %edi
  801230:	5d                   	pop    %ebp
  801231:	c3                   	ret    

00801232 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  801232:	55                   	push   %ebp
  801233:	89 e5                	mov    %esp,%ebp
  801235:	57                   	push   %edi
  801236:	56                   	push   %esi
  801237:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801238:	be 00 00 00 00       	mov    $0x0,%esi
  80123d:	b8 0d 00 00 00       	mov    $0xd,%eax
  801242:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801245:	8b 55 08             	mov    0x8(%ebp),%edx
  801248:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80124b:	8b 7d 14             	mov    0x14(%ebp),%edi
  80124e:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  801250:	5b                   	pop    %ebx
  801251:	5e                   	pop    %esi
  801252:	5f                   	pop    %edi
  801253:	5d                   	pop    %ebp
  801254:	c3                   	ret    

00801255 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  801255:	55                   	push   %ebp
  801256:	89 e5                	mov    %esp,%ebp
  801258:	57                   	push   %edi
  801259:	56                   	push   %esi
  80125a:	53                   	push   %ebx
  80125b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80125e:	b9 00 00 00 00       	mov    $0x0,%ecx
  801263:	b8 0e 00 00 00       	mov    $0xe,%eax
  801268:	8b 55 08             	mov    0x8(%ebp),%edx
  80126b:	89 cb                	mov    %ecx,%ebx
  80126d:	89 cf                	mov    %ecx,%edi
  80126f:	89 ce                	mov    %ecx,%esi
  801271:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801273:	85 c0                	test   %eax,%eax
  801275:	7e 17                	jle    80128e <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  801277:	83 ec 0c             	sub    $0xc,%esp
  80127a:	50                   	push   %eax
  80127b:	6a 0e                	push   $0xe
  80127d:	68 df 29 80 00       	push   $0x8029df
  801282:	6a 23                	push   $0x23
  801284:	68 fc 29 80 00       	push   $0x8029fc
  801289:	e8 d8 f3 ff ff       	call   800666 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80128e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801291:	5b                   	pop    %ebx
  801292:	5e                   	pop    %esi
  801293:	5f                   	pop    %edi
  801294:	5d                   	pop    %ebp
  801295:	c3                   	ret    

00801296 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801296:	55                   	push   %ebp
  801297:	89 e5                	mov    %esp,%ebp
  801299:	56                   	push   %esi
  80129a:	53                   	push   %ebx
  80129b:	8b 75 08             	mov    0x8(%ebp),%esi
  80129e:	8b 45 0c             	mov    0xc(%ebp),%eax
  8012a1:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  8012a4:	85 c0                	test   %eax,%eax
  8012a6:	75 05                	jne    8012ad <ipc_recv+0x17>
        pg = (void *)UTOP;
  8012a8:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  8012ad:	83 ec 0c             	sub    $0xc,%esp
  8012b0:	50                   	push   %eax
  8012b1:	e8 9f ff ff ff       	call   801255 <sys_ipc_recv>
  8012b6:	83 c4 10             	add    $0x10,%esp
  8012b9:	85 c0                	test   %eax,%eax
  8012bb:	74 16                	je     8012d3 <ipc_recv+0x3d>
        if (from_env_store)
  8012bd:	85 f6                	test   %esi,%esi
  8012bf:	74 06                	je     8012c7 <ipc_recv+0x31>
            *from_env_store = 0;
  8012c1:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  8012c7:	85 db                	test   %ebx,%ebx
  8012c9:	74 2c                	je     8012f7 <ipc_recv+0x61>
            *perm_store = 0;
  8012cb:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8012d1:	eb 24                	jmp    8012f7 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  8012d3:	85 f6                	test   %esi,%esi
  8012d5:	74 0a                	je     8012e1 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  8012d7:	a1 04 40 80 00       	mov    0x804004,%eax
  8012dc:	8b 40 74             	mov    0x74(%eax),%eax
  8012df:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  8012e1:	85 db                	test   %ebx,%ebx
  8012e3:	74 0a                	je     8012ef <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  8012e5:	a1 04 40 80 00       	mov    0x804004,%eax
  8012ea:	8b 40 78             	mov    0x78(%eax),%eax
  8012ed:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  8012ef:	a1 04 40 80 00       	mov    0x804004,%eax
  8012f4:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  8012f7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8012fa:	5b                   	pop    %ebx
  8012fb:	5e                   	pop    %esi
  8012fc:	5d                   	pop    %ebp
  8012fd:	c3                   	ret    

008012fe <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  8012fe:	55                   	push   %ebp
  8012ff:	89 e5                	mov    %esp,%ebp
  801301:	57                   	push   %edi
  801302:	56                   	push   %esi
  801303:	53                   	push   %ebx
  801304:	83 ec 0c             	sub    $0xc,%esp
  801307:	8b 75 0c             	mov    0xc(%ebp),%esi
  80130a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80130d:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801310:	85 db                	test   %ebx,%ebx
  801312:	75 0c                	jne    801320 <ipc_send+0x22>
        pg = (void *)UTOP;
  801314:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801319:	eb 05                	jmp    801320 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  80131b:	e8 47 fd ff ff       	call   801067 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801320:	57                   	push   %edi
  801321:	53                   	push   %ebx
  801322:	56                   	push   %esi
  801323:	ff 75 08             	pushl  0x8(%ebp)
  801326:	e8 07 ff ff ff       	call   801232 <sys_ipc_try_send>
  80132b:	83 c4 10             	add    $0x10,%esp
  80132e:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801331:	74 e8                	je     80131b <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801333:	85 c0                	test   %eax,%eax
  801335:	74 14                	je     80134b <ipc_send+0x4d>
        panic("ipc_send: error");
  801337:	83 ec 04             	sub    $0x4,%esp
  80133a:	68 0a 2a 80 00       	push   $0x802a0a
  80133f:	6a 6a                	push   $0x6a
  801341:	68 1a 2a 80 00       	push   $0x802a1a
  801346:	e8 1b f3 ff ff       	call   800666 <_panic>
    }
	//panic("ipc_send not implemented");
}
  80134b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80134e:	5b                   	pop    %ebx
  80134f:	5e                   	pop    %esi
  801350:	5f                   	pop    %edi
  801351:	5d                   	pop    %ebp
  801352:	c3                   	ret    

00801353 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801353:	55                   	push   %ebp
  801354:	89 e5                	mov    %esp,%ebp
  801356:	53                   	push   %ebx
  801357:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  80135a:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  80135f:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801366:	89 d0                	mov    %edx,%eax
  801368:	c1 e0 07             	shl    $0x7,%eax
  80136b:	29 d8                	sub    %ebx,%eax
  80136d:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801372:	8b 40 50             	mov    0x50(%eax),%eax
  801375:	39 c8                	cmp    %ecx,%eax
  801377:	75 0d                	jne    801386 <ipc_find_env+0x33>
			return envs[i].env_id;
  801379:	c1 e2 07             	shl    $0x7,%edx
  80137c:	29 da                	sub    %ebx,%edx
  80137e:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801384:	eb 0e                	jmp    801394 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801386:	42                   	inc    %edx
  801387:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  80138d:	75 d0                	jne    80135f <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  80138f:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801394:	5b                   	pop    %ebx
  801395:	5d                   	pop    %ebp
  801396:	c3                   	ret    

00801397 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  801397:	55                   	push   %ebp
  801398:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  80139a:	8b 45 08             	mov    0x8(%ebp),%eax
  80139d:	05 00 00 00 30       	add    $0x30000000,%eax
  8013a2:	c1 e8 0c             	shr    $0xc,%eax
}
  8013a5:	5d                   	pop    %ebp
  8013a6:	c3                   	ret    

008013a7 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  8013a7:	55                   	push   %ebp
  8013a8:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  8013aa:	8b 45 08             	mov    0x8(%ebp),%eax
  8013ad:	05 00 00 00 30       	add    $0x30000000,%eax
  8013b2:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  8013b7:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  8013bc:	5d                   	pop    %ebp
  8013bd:	c3                   	ret    

008013be <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  8013be:	55                   	push   %ebp
  8013bf:	89 e5                	mov    %esp,%ebp
  8013c1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8013c4:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  8013c9:	89 c2                	mov    %eax,%edx
  8013cb:	c1 ea 16             	shr    $0x16,%edx
  8013ce:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8013d5:	f6 c2 01             	test   $0x1,%dl
  8013d8:	74 11                	je     8013eb <fd_alloc+0x2d>
  8013da:	89 c2                	mov    %eax,%edx
  8013dc:	c1 ea 0c             	shr    $0xc,%edx
  8013df:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8013e6:	f6 c2 01             	test   $0x1,%dl
  8013e9:	75 09                	jne    8013f4 <fd_alloc+0x36>
			*fd_store = fd;
  8013eb:	89 01                	mov    %eax,(%ecx)
			return 0;
  8013ed:	b8 00 00 00 00       	mov    $0x0,%eax
  8013f2:	eb 17                	jmp    80140b <fd_alloc+0x4d>
  8013f4:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  8013f9:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  8013fe:	75 c9                	jne    8013c9 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801400:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801406:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  80140b:	5d                   	pop    %ebp
  80140c:	c3                   	ret    

0080140d <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  80140d:	55                   	push   %ebp
  80140e:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  801410:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801414:	77 39                	ja     80144f <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801416:	8b 45 08             	mov    0x8(%ebp),%eax
  801419:	c1 e0 0c             	shl    $0xc,%eax
  80141c:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  801421:	89 c2                	mov    %eax,%edx
  801423:	c1 ea 16             	shr    $0x16,%edx
  801426:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  80142d:	f6 c2 01             	test   $0x1,%dl
  801430:	74 24                	je     801456 <fd_lookup+0x49>
  801432:	89 c2                	mov    %eax,%edx
  801434:	c1 ea 0c             	shr    $0xc,%edx
  801437:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80143e:	f6 c2 01             	test   $0x1,%dl
  801441:	74 1a                	je     80145d <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  801443:	8b 55 0c             	mov    0xc(%ebp),%edx
  801446:	89 02                	mov    %eax,(%edx)
	return 0;
  801448:	b8 00 00 00 00       	mov    $0x0,%eax
  80144d:	eb 13                	jmp    801462 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  80144f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801454:	eb 0c                	jmp    801462 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801456:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80145b:	eb 05                	jmp    801462 <fd_lookup+0x55>
  80145d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  801462:	5d                   	pop    %ebp
  801463:	c3                   	ret    

00801464 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  801464:	55                   	push   %ebp
  801465:	89 e5                	mov    %esp,%ebp
  801467:	83 ec 08             	sub    $0x8,%esp
  80146a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80146d:	ba a4 2a 80 00       	mov    $0x802aa4,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801472:	eb 13                	jmp    801487 <dev_lookup+0x23>
  801474:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  801477:	39 08                	cmp    %ecx,(%eax)
  801479:	75 0c                	jne    801487 <dev_lookup+0x23>
			*dev = devtab[i];
  80147b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80147e:	89 01                	mov    %eax,(%ecx)
			return 0;
  801480:	b8 00 00 00 00       	mov    $0x0,%eax
  801485:	eb 2e                	jmp    8014b5 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801487:	8b 02                	mov    (%edx),%eax
  801489:	85 c0                	test   %eax,%eax
  80148b:	75 e7                	jne    801474 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  80148d:	a1 04 40 80 00       	mov    0x804004,%eax
  801492:	8b 40 48             	mov    0x48(%eax),%eax
  801495:	83 ec 04             	sub    $0x4,%esp
  801498:	51                   	push   %ecx
  801499:	50                   	push   %eax
  80149a:	68 24 2a 80 00       	push   $0x802a24
  80149f:	e8 9a f2 ff ff       	call   80073e <cprintf>
	*dev = 0;
  8014a4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8014a7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  8014ad:	83 c4 10             	add    $0x10,%esp
  8014b0:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  8014b5:	c9                   	leave  
  8014b6:	c3                   	ret    

008014b7 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  8014b7:	55                   	push   %ebp
  8014b8:	89 e5                	mov    %esp,%ebp
  8014ba:	56                   	push   %esi
  8014bb:	53                   	push   %ebx
  8014bc:	83 ec 10             	sub    $0x10,%esp
  8014bf:	8b 75 08             	mov    0x8(%ebp),%esi
  8014c2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  8014c5:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8014c8:	50                   	push   %eax
  8014c9:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  8014cf:	c1 e8 0c             	shr    $0xc,%eax
  8014d2:	50                   	push   %eax
  8014d3:	e8 35 ff ff ff       	call   80140d <fd_lookup>
  8014d8:	83 c4 08             	add    $0x8,%esp
  8014db:	85 c0                	test   %eax,%eax
  8014dd:	78 05                	js     8014e4 <fd_close+0x2d>
	    || fd != fd2)
  8014df:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  8014e2:	74 06                	je     8014ea <fd_close+0x33>
		return (must_exist ? r : 0);
  8014e4:	84 db                	test   %bl,%bl
  8014e6:	74 47                	je     80152f <fd_close+0x78>
  8014e8:	eb 4a                	jmp    801534 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  8014ea:	83 ec 08             	sub    $0x8,%esp
  8014ed:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8014f0:	50                   	push   %eax
  8014f1:	ff 36                	pushl  (%esi)
  8014f3:	e8 6c ff ff ff       	call   801464 <dev_lookup>
  8014f8:	89 c3                	mov    %eax,%ebx
  8014fa:	83 c4 10             	add    $0x10,%esp
  8014fd:	85 c0                	test   %eax,%eax
  8014ff:	78 1c                	js     80151d <fd_close+0x66>
		if (dev->dev_close)
  801501:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801504:	8b 40 10             	mov    0x10(%eax),%eax
  801507:	85 c0                	test   %eax,%eax
  801509:	74 0d                	je     801518 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  80150b:	83 ec 0c             	sub    $0xc,%esp
  80150e:	56                   	push   %esi
  80150f:	ff d0                	call   *%eax
  801511:	89 c3                	mov    %eax,%ebx
  801513:	83 c4 10             	add    $0x10,%esp
  801516:	eb 05                	jmp    80151d <fd_close+0x66>
		else
			r = 0;
  801518:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  80151d:	83 ec 08             	sub    $0x8,%esp
  801520:	56                   	push   %esi
  801521:	6a 00                	push   $0x0
  801523:	e8 e3 fb ff ff       	call   80110b <sys_page_unmap>
	return r;
  801528:	83 c4 10             	add    $0x10,%esp
  80152b:	89 d8                	mov    %ebx,%eax
  80152d:	eb 05                	jmp    801534 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  80152f:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801534:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801537:	5b                   	pop    %ebx
  801538:	5e                   	pop    %esi
  801539:	5d                   	pop    %ebp
  80153a:	c3                   	ret    

0080153b <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  80153b:	55                   	push   %ebp
  80153c:	89 e5                	mov    %esp,%ebp
  80153e:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801541:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801544:	50                   	push   %eax
  801545:	ff 75 08             	pushl  0x8(%ebp)
  801548:	e8 c0 fe ff ff       	call   80140d <fd_lookup>
  80154d:	83 c4 08             	add    $0x8,%esp
  801550:	85 c0                	test   %eax,%eax
  801552:	78 10                	js     801564 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  801554:	83 ec 08             	sub    $0x8,%esp
  801557:	6a 01                	push   $0x1
  801559:	ff 75 f4             	pushl  -0xc(%ebp)
  80155c:	e8 56 ff ff ff       	call   8014b7 <fd_close>
  801561:	83 c4 10             	add    $0x10,%esp
}
  801564:	c9                   	leave  
  801565:	c3                   	ret    

00801566 <close_all>:

void
close_all(void)
{
  801566:	55                   	push   %ebp
  801567:	89 e5                	mov    %esp,%ebp
  801569:	53                   	push   %ebx
  80156a:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  80156d:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801572:	83 ec 0c             	sub    $0xc,%esp
  801575:	53                   	push   %ebx
  801576:	e8 c0 ff ff ff       	call   80153b <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  80157b:	43                   	inc    %ebx
  80157c:	83 c4 10             	add    $0x10,%esp
  80157f:	83 fb 20             	cmp    $0x20,%ebx
  801582:	75 ee                	jne    801572 <close_all+0xc>
		close(i);
}
  801584:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801587:	c9                   	leave  
  801588:	c3                   	ret    

00801589 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801589:	55                   	push   %ebp
  80158a:	89 e5                	mov    %esp,%ebp
  80158c:	57                   	push   %edi
  80158d:	56                   	push   %esi
  80158e:	53                   	push   %ebx
  80158f:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801592:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801595:	50                   	push   %eax
  801596:	ff 75 08             	pushl  0x8(%ebp)
  801599:	e8 6f fe ff ff       	call   80140d <fd_lookup>
  80159e:	83 c4 08             	add    $0x8,%esp
  8015a1:	85 c0                	test   %eax,%eax
  8015a3:	0f 88 c2 00 00 00    	js     80166b <dup+0xe2>
		return r;
	close(newfdnum);
  8015a9:	83 ec 0c             	sub    $0xc,%esp
  8015ac:	ff 75 0c             	pushl  0xc(%ebp)
  8015af:	e8 87 ff ff ff       	call   80153b <close>

	newfd = INDEX2FD(newfdnum);
  8015b4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8015b7:	c1 e3 0c             	shl    $0xc,%ebx
  8015ba:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  8015c0:	83 c4 04             	add    $0x4,%esp
  8015c3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8015c6:	e8 dc fd ff ff       	call   8013a7 <fd2data>
  8015cb:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  8015cd:	89 1c 24             	mov    %ebx,(%esp)
  8015d0:	e8 d2 fd ff ff       	call   8013a7 <fd2data>
  8015d5:	83 c4 10             	add    $0x10,%esp
  8015d8:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8015da:	89 f0                	mov    %esi,%eax
  8015dc:	c1 e8 16             	shr    $0x16,%eax
  8015df:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8015e6:	a8 01                	test   $0x1,%al
  8015e8:	74 35                	je     80161f <dup+0x96>
  8015ea:	89 f0                	mov    %esi,%eax
  8015ec:	c1 e8 0c             	shr    $0xc,%eax
  8015ef:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  8015f6:	f6 c2 01             	test   $0x1,%dl
  8015f9:	74 24                	je     80161f <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  8015fb:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801602:	83 ec 0c             	sub    $0xc,%esp
  801605:	25 07 0e 00 00       	and    $0xe07,%eax
  80160a:	50                   	push   %eax
  80160b:	57                   	push   %edi
  80160c:	6a 00                	push   $0x0
  80160e:	56                   	push   %esi
  80160f:	6a 00                	push   $0x0
  801611:	e8 b3 fa ff ff       	call   8010c9 <sys_page_map>
  801616:	89 c6                	mov    %eax,%esi
  801618:	83 c4 20             	add    $0x20,%esp
  80161b:	85 c0                	test   %eax,%eax
  80161d:	78 2c                	js     80164b <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  80161f:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801622:	89 d0                	mov    %edx,%eax
  801624:	c1 e8 0c             	shr    $0xc,%eax
  801627:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80162e:	83 ec 0c             	sub    $0xc,%esp
  801631:	25 07 0e 00 00       	and    $0xe07,%eax
  801636:	50                   	push   %eax
  801637:	53                   	push   %ebx
  801638:	6a 00                	push   $0x0
  80163a:	52                   	push   %edx
  80163b:	6a 00                	push   $0x0
  80163d:	e8 87 fa ff ff       	call   8010c9 <sys_page_map>
  801642:	89 c6                	mov    %eax,%esi
  801644:	83 c4 20             	add    $0x20,%esp
  801647:	85 c0                	test   %eax,%eax
  801649:	79 1d                	jns    801668 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  80164b:	83 ec 08             	sub    $0x8,%esp
  80164e:	53                   	push   %ebx
  80164f:	6a 00                	push   $0x0
  801651:	e8 b5 fa ff ff       	call   80110b <sys_page_unmap>
	sys_page_unmap(0, nva);
  801656:	83 c4 08             	add    $0x8,%esp
  801659:	57                   	push   %edi
  80165a:	6a 00                	push   $0x0
  80165c:	e8 aa fa ff ff       	call   80110b <sys_page_unmap>
	return r;
  801661:	83 c4 10             	add    $0x10,%esp
  801664:	89 f0                	mov    %esi,%eax
  801666:	eb 03                	jmp    80166b <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801668:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  80166b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80166e:	5b                   	pop    %ebx
  80166f:	5e                   	pop    %esi
  801670:	5f                   	pop    %edi
  801671:	5d                   	pop    %ebp
  801672:	c3                   	ret    

00801673 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801673:	55                   	push   %ebp
  801674:	89 e5                	mov    %esp,%ebp
  801676:	53                   	push   %ebx
  801677:	83 ec 14             	sub    $0x14,%esp
  80167a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80167d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801680:	50                   	push   %eax
  801681:	53                   	push   %ebx
  801682:	e8 86 fd ff ff       	call   80140d <fd_lookup>
  801687:	83 c4 08             	add    $0x8,%esp
  80168a:	85 c0                	test   %eax,%eax
  80168c:	78 67                	js     8016f5 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80168e:	83 ec 08             	sub    $0x8,%esp
  801691:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801694:	50                   	push   %eax
  801695:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801698:	ff 30                	pushl  (%eax)
  80169a:	e8 c5 fd ff ff       	call   801464 <dev_lookup>
  80169f:	83 c4 10             	add    $0x10,%esp
  8016a2:	85 c0                	test   %eax,%eax
  8016a4:	78 4f                	js     8016f5 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8016a6:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8016a9:	8b 42 08             	mov    0x8(%edx),%eax
  8016ac:	83 e0 03             	and    $0x3,%eax
  8016af:	83 f8 01             	cmp    $0x1,%eax
  8016b2:	75 21                	jne    8016d5 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  8016b4:	a1 04 40 80 00       	mov    0x804004,%eax
  8016b9:	8b 40 48             	mov    0x48(%eax),%eax
  8016bc:	83 ec 04             	sub    $0x4,%esp
  8016bf:	53                   	push   %ebx
  8016c0:	50                   	push   %eax
  8016c1:	68 68 2a 80 00       	push   $0x802a68
  8016c6:	e8 73 f0 ff ff       	call   80073e <cprintf>
		return -E_INVAL;
  8016cb:	83 c4 10             	add    $0x10,%esp
  8016ce:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8016d3:	eb 20                	jmp    8016f5 <read+0x82>
	}
	if (!dev->dev_read)
  8016d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8016d8:	8b 40 08             	mov    0x8(%eax),%eax
  8016db:	85 c0                	test   %eax,%eax
  8016dd:	74 11                	je     8016f0 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  8016df:	83 ec 04             	sub    $0x4,%esp
  8016e2:	ff 75 10             	pushl  0x10(%ebp)
  8016e5:	ff 75 0c             	pushl  0xc(%ebp)
  8016e8:	52                   	push   %edx
  8016e9:	ff d0                	call   *%eax
  8016eb:	83 c4 10             	add    $0x10,%esp
  8016ee:	eb 05                	jmp    8016f5 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  8016f0:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  8016f5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8016f8:	c9                   	leave  
  8016f9:	c3                   	ret    

008016fa <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  8016fa:	55                   	push   %ebp
  8016fb:	89 e5                	mov    %esp,%ebp
  8016fd:	57                   	push   %edi
  8016fe:	56                   	push   %esi
  8016ff:	53                   	push   %ebx
  801700:	83 ec 0c             	sub    $0xc,%esp
  801703:	8b 7d 08             	mov    0x8(%ebp),%edi
  801706:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801709:	bb 00 00 00 00       	mov    $0x0,%ebx
  80170e:	eb 21                	jmp    801731 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801710:	83 ec 04             	sub    $0x4,%esp
  801713:	89 f0                	mov    %esi,%eax
  801715:	29 d8                	sub    %ebx,%eax
  801717:	50                   	push   %eax
  801718:	89 d8                	mov    %ebx,%eax
  80171a:	03 45 0c             	add    0xc(%ebp),%eax
  80171d:	50                   	push   %eax
  80171e:	57                   	push   %edi
  80171f:	e8 4f ff ff ff       	call   801673 <read>
		if (m < 0)
  801724:	83 c4 10             	add    $0x10,%esp
  801727:	85 c0                	test   %eax,%eax
  801729:	78 10                	js     80173b <readn+0x41>
			return m;
		if (m == 0)
  80172b:	85 c0                	test   %eax,%eax
  80172d:	74 0a                	je     801739 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80172f:	01 c3                	add    %eax,%ebx
  801731:	39 f3                	cmp    %esi,%ebx
  801733:	72 db                	jb     801710 <readn+0x16>
  801735:	89 d8                	mov    %ebx,%eax
  801737:	eb 02                	jmp    80173b <readn+0x41>
  801739:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  80173b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80173e:	5b                   	pop    %ebx
  80173f:	5e                   	pop    %esi
  801740:	5f                   	pop    %edi
  801741:	5d                   	pop    %ebp
  801742:	c3                   	ret    

00801743 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801743:	55                   	push   %ebp
  801744:	89 e5                	mov    %esp,%ebp
  801746:	53                   	push   %ebx
  801747:	83 ec 14             	sub    $0x14,%esp
  80174a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80174d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801750:	50                   	push   %eax
  801751:	53                   	push   %ebx
  801752:	e8 b6 fc ff ff       	call   80140d <fd_lookup>
  801757:	83 c4 08             	add    $0x8,%esp
  80175a:	85 c0                	test   %eax,%eax
  80175c:	78 62                	js     8017c0 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80175e:	83 ec 08             	sub    $0x8,%esp
  801761:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801764:	50                   	push   %eax
  801765:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801768:	ff 30                	pushl  (%eax)
  80176a:	e8 f5 fc ff ff       	call   801464 <dev_lookup>
  80176f:	83 c4 10             	add    $0x10,%esp
  801772:	85 c0                	test   %eax,%eax
  801774:	78 4a                	js     8017c0 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801776:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801779:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80177d:	75 21                	jne    8017a0 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  80177f:	a1 04 40 80 00       	mov    0x804004,%eax
  801784:	8b 40 48             	mov    0x48(%eax),%eax
  801787:	83 ec 04             	sub    $0x4,%esp
  80178a:	53                   	push   %ebx
  80178b:	50                   	push   %eax
  80178c:	68 84 2a 80 00       	push   $0x802a84
  801791:	e8 a8 ef ff ff       	call   80073e <cprintf>
		return -E_INVAL;
  801796:	83 c4 10             	add    $0x10,%esp
  801799:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80179e:	eb 20                	jmp    8017c0 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8017a0:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8017a3:	8b 52 0c             	mov    0xc(%edx),%edx
  8017a6:	85 d2                	test   %edx,%edx
  8017a8:	74 11                	je     8017bb <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8017aa:	83 ec 04             	sub    $0x4,%esp
  8017ad:	ff 75 10             	pushl  0x10(%ebp)
  8017b0:	ff 75 0c             	pushl  0xc(%ebp)
  8017b3:	50                   	push   %eax
  8017b4:	ff d2                	call   *%edx
  8017b6:	83 c4 10             	add    $0x10,%esp
  8017b9:	eb 05                	jmp    8017c0 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8017bb:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8017c0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017c3:	c9                   	leave  
  8017c4:	c3                   	ret    

008017c5 <seek>:

int
seek(int fdnum, off_t offset)
{
  8017c5:	55                   	push   %ebp
  8017c6:	89 e5                	mov    %esp,%ebp
  8017c8:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8017cb:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8017ce:	50                   	push   %eax
  8017cf:	ff 75 08             	pushl  0x8(%ebp)
  8017d2:	e8 36 fc ff ff       	call   80140d <fd_lookup>
  8017d7:	83 c4 08             	add    $0x8,%esp
  8017da:	85 c0                	test   %eax,%eax
  8017dc:	78 0e                	js     8017ec <seek+0x27>
		return r;
	fd->fd_offset = offset;
  8017de:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8017e1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8017e4:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  8017e7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8017ec:	c9                   	leave  
  8017ed:	c3                   	ret    

008017ee <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  8017ee:	55                   	push   %ebp
  8017ef:	89 e5                	mov    %esp,%ebp
  8017f1:	53                   	push   %ebx
  8017f2:	83 ec 14             	sub    $0x14,%esp
  8017f5:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  8017f8:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8017fb:	50                   	push   %eax
  8017fc:	53                   	push   %ebx
  8017fd:	e8 0b fc ff ff       	call   80140d <fd_lookup>
  801802:	83 c4 08             	add    $0x8,%esp
  801805:	85 c0                	test   %eax,%eax
  801807:	78 5f                	js     801868 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801809:	83 ec 08             	sub    $0x8,%esp
  80180c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80180f:	50                   	push   %eax
  801810:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801813:	ff 30                	pushl  (%eax)
  801815:	e8 4a fc ff ff       	call   801464 <dev_lookup>
  80181a:	83 c4 10             	add    $0x10,%esp
  80181d:	85 c0                	test   %eax,%eax
  80181f:	78 47                	js     801868 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801821:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801824:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801828:	75 21                	jne    80184b <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  80182a:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80182f:	8b 40 48             	mov    0x48(%eax),%eax
  801832:	83 ec 04             	sub    $0x4,%esp
  801835:	53                   	push   %ebx
  801836:	50                   	push   %eax
  801837:	68 44 2a 80 00       	push   $0x802a44
  80183c:	e8 fd ee ff ff       	call   80073e <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  801841:	83 c4 10             	add    $0x10,%esp
  801844:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801849:	eb 1d                	jmp    801868 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  80184b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80184e:	8b 52 18             	mov    0x18(%edx),%edx
  801851:	85 d2                	test   %edx,%edx
  801853:	74 0e                	je     801863 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  801855:	83 ec 08             	sub    $0x8,%esp
  801858:	ff 75 0c             	pushl  0xc(%ebp)
  80185b:	50                   	push   %eax
  80185c:	ff d2                	call   *%edx
  80185e:	83 c4 10             	add    $0x10,%esp
  801861:	eb 05                	jmp    801868 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  801863:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  801868:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80186b:	c9                   	leave  
  80186c:	c3                   	ret    

0080186d <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  80186d:	55                   	push   %ebp
  80186e:	89 e5                	mov    %esp,%ebp
  801870:	53                   	push   %ebx
  801871:	83 ec 14             	sub    $0x14,%esp
  801874:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801877:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80187a:	50                   	push   %eax
  80187b:	ff 75 08             	pushl  0x8(%ebp)
  80187e:	e8 8a fb ff ff       	call   80140d <fd_lookup>
  801883:	83 c4 08             	add    $0x8,%esp
  801886:	85 c0                	test   %eax,%eax
  801888:	78 52                	js     8018dc <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80188a:	83 ec 08             	sub    $0x8,%esp
  80188d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801890:	50                   	push   %eax
  801891:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801894:	ff 30                	pushl  (%eax)
  801896:	e8 c9 fb ff ff       	call   801464 <dev_lookup>
  80189b:	83 c4 10             	add    $0x10,%esp
  80189e:	85 c0                	test   %eax,%eax
  8018a0:	78 3a                	js     8018dc <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8018a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8018a5:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8018a9:	74 2c                	je     8018d7 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8018ab:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8018ae:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8018b5:	00 00 00 
	stat->st_isdir = 0;
  8018b8:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8018bf:	00 00 00 
	stat->st_dev = dev;
  8018c2:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8018c8:	83 ec 08             	sub    $0x8,%esp
  8018cb:	53                   	push   %ebx
  8018cc:	ff 75 f0             	pushl  -0x10(%ebp)
  8018cf:	ff 50 14             	call   *0x14(%eax)
  8018d2:	83 c4 10             	add    $0x10,%esp
  8018d5:	eb 05                	jmp    8018dc <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8018d7:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8018dc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8018df:	c9                   	leave  
  8018e0:	c3                   	ret    

008018e1 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8018e1:	55                   	push   %ebp
  8018e2:	89 e5                	mov    %esp,%ebp
  8018e4:	56                   	push   %esi
  8018e5:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8018e6:	83 ec 08             	sub    $0x8,%esp
  8018e9:	6a 00                	push   $0x0
  8018eb:	ff 75 08             	pushl  0x8(%ebp)
  8018ee:	e8 e7 01 00 00       	call   801ada <open>
  8018f3:	89 c3                	mov    %eax,%ebx
  8018f5:	83 c4 10             	add    $0x10,%esp
  8018f8:	85 c0                	test   %eax,%eax
  8018fa:	78 1d                	js     801919 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  8018fc:	83 ec 08             	sub    $0x8,%esp
  8018ff:	ff 75 0c             	pushl  0xc(%ebp)
  801902:	50                   	push   %eax
  801903:	e8 65 ff ff ff       	call   80186d <fstat>
  801908:	89 c6                	mov    %eax,%esi
	close(fd);
  80190a:	89 1c 24             	mov    %ebx,(%esp)
  80190d:	e8 29 fc ff ff       	call   80153b <close>
	return r;
  801912:	83 c4 10             	add    $0x10,%esp
  801915:	89 f0                	mov    %esi,%eax
  801917:	eb 00                	jmp    801919 <stat+0x38>
}
  801919:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80191c:	5b                   	pop    %ebx
  80191d:	5e                   	pop    %esi
  80191e:	5d                   	pop    %ebp
  80191f:	c3                   	ret    

00801920 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  801920:	55                   	push   %ebp
  801921:	89 e5                	mov    %esp,%ebp
  801923:	56                   	push   %esi
  801924:	53                   	push   %ebx
  801925:	89 c6                	mov    %eax,%esi
  801927:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801929:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  801930:	75 12                	jne    801944 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801932:	83 ec 0c             	sub    $0xc,%esp
  801935:	6a 01                	push   $0x1
  801937:	e8 17 fa ff ff       	call   801353 <ipc_find_env>
  80193c:	a3 00 40 80 00       	mov    %eax,0x804000
  801941:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  801944:	6a 07                	push   $0x7
  801946:	68 00 50 80 00       	push   $0x805000
  80194b:	56                   	push   %esi
  80194c:	ff 35 00 40 80 00    	pushl  0x804000
  801952:	e8 a7 f9 ff ff       	call   8012fe <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  801957:	83 c4 0c             	add    $0xc,%esp
  80195a:	6a 00                	push   $0x0
  80195c:	53                   	push   %ebx
  80195d:	6a 00                	push   $0x0
  80195f:	e8 32 f9 ff ff       	call   801296 <ipc_recv>
}
  801964:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801967:	5b                   	pop    %ebx
  801968:	5e                   	pop    %esi
  801969:	5d                   	pop    %ebp
  80196a:	c3                   	ret    

0080196b <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  80196b:	55                   	push   %ebp
  80196c:	89 e5                	mov    %esp,%ebp
  80196e:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801971:	8b 45 08             	mov    0x8(%ebp),%eax
  801974:	8b 40 0c             	mov    0xc(%eax),%eax
  801977:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  80197c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80197f:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801984:	ba 00 00 00 00       	mov    $0x0,%edx
  801989:	b8 02 00 00 00       	mov    $0x2,%eax
  80198e:	e8 8d ff ff ff       	call   801920 <fsipc>
}
  801993:	c9                   	leave  
  801994:	c3                   	ret    

00801995 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  801995:	55                   	push   %ebp
  801996:	89 e5                	mov    %esp,%ebp
  801998:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  80199b:	8b 45 08             	mov    0x8(%ebp),%eax
  80199e:	8b 40 0c             	mov    0xc(%eax),%eax
  8019a1:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  8019a6:	ba 00 00 00 00       	mov    $0x0,%edx
  8019ab:	b8 06 00 00 00       	mov    $0x6,%eax
  8019b0:	e8 6b ff ff ff       	call   801920 <fsipc>
}
  8019b5:	c9                   	leave  
  8019b6:	c3                   	ret    

008019b7 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  8019b7:	55                   	push   %ebp
  8019b8:	89 e5                	mov    %esp,%ebp
  8019ba:	53                   	push   %ebx
  8019bb:	83 ec 04             	sub    $0x4,%esp
  8019be:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8019c1:	8b 45 08             	mov    0x8(%ebp),%eax
  8019c4:	8b 40 0c             	mov    0xc(%eax),%eax
  8019c7:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8019cc:	ba 00 00 00 00       	mov    $0x0,%edx
  8019d1:	b8 05 00 00 00       	mov    $0x5,%eax
  8019d6:	e8 45 ff ff ff       	call   801920 <fsipc>
  8019db:	85 c0                	test   %eax,%eax
  8019dd:	78 2c                	js     801a0b <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  8019df:	83 ec 08             	sub    $0x8,%esp
  8019e2:	68 00 50 80 00       	push   $0x805000
  8019e7:	53                   	push   %ebx
  8019e8:	e8 b5 f2 ff ff       	call   800ca2 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  8019ed:	a1 80 50 80 00       	mov    0x805080,%eax
  8019f2:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  8019f8:	a1 84 50 80 00       	mov    0x805084,%eax
  8019fd:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801a03:	83 c4 10             	add    $0x10,%esp
  801a06:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801a0b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801a0e:	c9                   	leave  
  801a0f:	c3                   	ret    

00801a10 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801a10:	55                   	push   %ebp
  801a11:	89 e5                	mov    %esp,%ebp
  801a13:	83 ec 08             	sub    $0x8,%esp
  801a16:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801a19:	8b 55 08             	mov    0x8(%ebp),%edx
  801a1c:	8b 52 0c             	mov    0xc(%edx),%edx
  801a1f:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801a25:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801a2a:	76 05                	jbe    801a31 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801a2c:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801a31:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801a36:	83 ec 04             	sub    $0x4,%esp
  801a39:	50                   	push   %eax
  801a3a:	ff 75 0c             	pushl  0xc(%ebp)
  801a3d:	68 08 50 80 00       	push   $0x805008
  801a42:	e8 d0 f3 ff ff       	call   800e17 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801a47:	ba 00 00 00 00       	mov    $0x0,%edx
  801a4c:	b8 04 00 00 00       	mov    $0x4,%eax
  801a51:	e8 ca fe ff ff       	call   801920 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801a56:	c9                   	leave  
  801a57:	c3                   	ret    

00801a58 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801a58:	55                   	push   %ebp
  801a59:	89 e5                	mov    %esp,%ebp
  801a5b:	56                   	push   %esi
  801a5c:	53                   	push   %ebx
  801a5d:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  801a60:	8b 45 08             	mov    0x8(%ebp),%eax
  801a63:	8b 40 0c             	mov    0xc(%eax),%eax
  801a66:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801a6b:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801a71:	ba 00 00 00 00       	mov    $0x0,%edx
  801a76:	b8 03 00 00 00       	mov    $0x3,%eax
  801a7b:	e8 a0 fe ff ff       	call   801920 <fsipc>
  801a80:	89 c3                	mov    %eax,%ebx
  801a82:	85 c0                	test   %eax,%eax
  801a84:	78 4b                	js     801ad1 <devfile_read+0x79>
		return r;
	assert(r <= n);
  801a86:	39 c6                	cmp    %eax,%esi
  801a88:	73 16                	jae    801aa0 <devfile_read+0x48>
  801a8a:	68 b4 2a 80 00       	push   $0x802ab4
  801a8f:	68 bb 2a 80 00       	push   $0x802abb
  801a94:	6a 7c                	push   $0x7c
  801a96:	68 d0 2a 80 00       	push   $0x802ad0
  801a9b:	e8 c6 eb ff ff       	call   800666 <_panic>
	assert(r <= PGSIZE);
  801aa0:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801aa5:	7e 16                	jle    801abd <devfile_read+0x65>
  801aa7:	68 db 2a 80 00       	push   $0x802adb
  801aac:	68 bb 2a 80 00       	push   $0x802abb
  801ab1:	6a 7d                	push   $0x7d
  801ab3:	68 d0 2a 80 00       	push   $0x802ad0
  801ab8:	e8 a9 eb ff ff       	call   800666 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801abd:	83 ec 04             	sub    $0x4,%esp
  801ac0:	50                   	push   %eax
  801ac1:	68 00 50 80 00       	push   $0x805000
  801ac6:	ff 75 0c             	pushl  0xc(%ebp)
  801ac9:	e8 49 f3 ff ff       	call   800e17 <memmove>
	return r;
  801ace:	83 c4 10             	add    $0x10,%esp
}
  801ad1:	89 d8                	mov    %ebx,%eax
  801ad3:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801ad6:	5b                   	pop    %ebx
  801ad7:	5e                   	pop    %esi
  801ad8:	5d                   	pop    %ebp
  801ad9:	c3                   	ret    

00801ada <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801ada:	55                   	push   %ebp
  801adb:	89 e5                	mov    %esp,%ebp
  801add:	53                   	push   %ebx
  801ade:	83 ec 20             	sub    $0x20,%esp
  801ae1:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801ae4:	53                   	push   %ebx
  801ae5:	e8 83 f1 ff ff       	call   800c6d <strlen>
  801aea:	83 c4 10             	add    $0x10,%esp
  801aed:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801af2:	7f 63                	jg     801b57 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801af4:	83 ec 0c             	sub    $0xc,%esp
  801af7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801afa:	50                   	push   %eax
  801afb:	e8 be f8 ff ff       	call   8013be <fd_alloc>
  801b00:	83 c4 10             	add    $0x10,%esp
  801b03:	85 c0                	test   %eax,%eax
  801b05:	78 55                	js     801b5c <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801b07:	83 ec 08             	sub    $0x8,%esp
  801b0a:	53                   	push   %ebx
  801b0b:	68 00 50 80 00       	push   $0x805000
  801b10:	e8 8d f1 ff ff       	call   800ca2 <strcpy>
	fsipcbuf.open.req_omode = mode;
  801b15:	8b 45 0c             	mov    0xc(%ebp),%eax
  801b18:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801b1d:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801b20:	b8 01 00 00 00       	mov    $0x1,%eax
  801b25:	e8 f6 fd ff ff       	call   801920 <fsipc>
  801b2a:	89 c3                	mov    %eax,%ebx
  801b2c:	83 c4 10             	add    $0x10,%esp
  801b2f:	85 c0                	test   %eax,%eax
  801b31:	79 14                	jns    801b47 <open+0x6d>
		fd_close(fd, 0);
  801b33:	83 ec 08             	sub    $0x8,%esp
  801b36:	6a 00                	push   $0x0
  801b38:	ff 75 f4             	pushl  -0xc(%ebp)
  801b3b:	e8 77 f9 ff ff       	call   8014b7 <fd_close>
		return r;
  801b40:	83 c4 10             	add    $0x10,%esp
  801b43:	89 d8                	mov    %ebx,%eax
  801b45:	eb 15                	jmp    801b5c <open+0x82>
	}

	return fd2num(fd);
  801b47:	83 ec 0c             	sub    $0xc,%esp
  801b4a:	ff 75 f4             	pushl  -0xc(%ebp)
  801b4d:	e8 45 f8 ff ff       	call   801397 <fd2num>
  801b52:	83 c4 10             	add    $0x10,%esp
  801b55:	eb 05                	jmp    801b5c <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801b57:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801b5c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801b5f:	c9                   	leave  
  801b60:	c3                   	ret    

00801b61 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801b61:	55                   	push   %ebp
  801b62:	89 e5                	mov    %esp,%ebp
  801b64:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801b67:	ba 00 00 00 00       	mov    $0x0,%edx
  801b6c:	b8 08 00 00 00       	mov    $0x8,%eax
  801b71:	e8 aa fd ff ff       	call   801920 <fsipc>
}
  801b76:	c9                   	leave  
  801b77:	c3                   	ret    

00801b78 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801b78:	55                   	push   %ebp
  801b79:	89 e5                	mov    %esp,%ebp
  801b7b:	56                   	push   %esi
  801b7c:	53                   	push   %ebx
  801b7d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801b80:	83 ec 0c             	sub    $0xc,%esp
  801b83:	ff 75 08             	pushl  0x8(%ebp)
  801b86:	e8 1c f8 ff ff       	call   8013a7 <fd2data>
  801b8b:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801b8d:	83 c4 08             	add    $0x8,%esp
  801b90:	68 e7 2a 80 00       	push   $0x802ae7
  801b95:	53                   	push   %ebx
  801b96:	e8 07 f1 ff ff       	call   800ca2 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801b9b:	8b 46 04             	mov    0x4(%esi),%eax
  801b9e:	2b 06                	sub    (%esi),%eax
  801ba0:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801ba6:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801bad:	00 00 00 
	stat->st_dev = &devpipe;
  801bb0:	c7 83 88 00 00 00 24 	movl   $0x803024,0x88(%ebx)
  801bb7:	30 80 00 
	return 0;
}
  801bba:	b8 00 00 00 00       	mov    $0x0,%eax
  801bbf:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801bc2:	5b                   	pop    %ebx
  801bc3:	5e                   	pop    %esi
  801bc4:	5d                   	pop    %ebp
  801bc5:	c3                   	ret    

00801bc6 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801bc6:	55                   	push   %ebp
  801bc7:	89 e5                	mov    %esp,%ebp
  801bc9:	53                   	push   %ebx
  801bca:	83 ec 0c             	sub    $0xc,%esp
  801bcd:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801bd0:	53                   	push   %ebx
  801bd1:	6a 00                	push   $0x0
  801bd3:	e8 33 f5 ff ff       	call   80110b <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801bd8:	89 1c 24             	mov    %ebx,(%esp)
  801bdb:	e8 c7 f7 ff ff       	call   8013a7 <fd2data>
  801be0:	83 c4 08             	add    $0x8,%esp
  801be3:	50                   	push   %eax
  801be4:	6a 00                	push   $0x0
  801be6:	e8 20 f5 ff ff       	call   80110b <sys_page_unmap>
}
  801beb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801bee:	c9                   	leave  
  801bef:	c3                   	ret    

00801bf0 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801bf0:	55                   	push   %ebp
  801bf1:	89 e5                	mov    %esp,%ebp
  801bf3:	57                   	push   %edi
  801bf4:	56                   	push   %esi
  801bf5:	53                   	push   %ebx
  801bf6:	83 ec 1c             	sub    $0x1c,%esp
  801bf9:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801bfc:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801bfe:	a1 04 40 80 00       	mov    0x804004,%eax
  801c03:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801c06:	83 ec 0c             	sub    $0xc,%esp
  801c09:	ff 75 e0             	pushl  -0x20(%ebp)
  801c0c:	e8 26 04 00 00       	call   802037 <pageref>
  801c11:	89 c3                	mov    %eax,%ebx
  801c13:	89 3c 24             	mov    %edi,(%esp)
  801c16:	e8 1c 04 00 00       	call   802037 <pageref>
  801c1b:	83 c4 10             	add    $0x10,%esp
  801c1e:	39 c3                	cmp    %eax,%ebx
  801c20:	0f 94 c1             	sete   %cl
  801c23:	0f b6 c9             	movzbl %cl,%ecx
  801c26:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801c29:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801c2f:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801c32:	39 ce                	cmp    %ecx,%esi
  801c34:	74 1b                	je     801c51 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801c36:	39 c3                	cmp    %eax,%ebx
  801c38:	75 c4                	jne    801bfe <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801c3a:	8b 42 58             	mov    0x58(%edx),%eax
  801c3d:	ff 75 e4             	pushl  -0x1c(%ebp)
  801c40:	50                   	push   %eax
  801c41:	56                   	push   %esi
  801c42:	68 ee 2a 80 00       	push   $0x802aee
  801c47:	e8 f2 ea ff ff       	call   80073e <cprintf>
  801c4c:	83 c4 10             	add    $0x10,%esp
  801c4f:	eb ad                	jmp    801bfe <_pipeisclosed+0xe>
	}
}
  801c51:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801c54:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c57:	5b                   	pop    %ebx
  801c58:	5e                   	pop    %esi
  801c59:	5f                   	pop    %edi
  801c5a:	5d                   	pop    %ebp
  801c5b:	c3                   	ret    

00801c5c <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801c5c:	55                   	push   %ebp
  801c5d:	89 e5                	mov    %esp,%ebp
  801c5f:	57                   	push   %edi
  801c60:	56                   	push   %esi
  801c61:	53                   	push   %ebx
  801c62:	83 ec 18             	sub    $0x18,%esp
  801c65:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801c68:	56                   	push   %esi
  801c69:	e8 39 f7 ff ff       	call   8013a7 <fd2data>
  801c6e:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c70:	83 c4 10             	add    $0x10,%esp
  801c73:	bf 00 00 00 00       	mov    $0x0,%edi
  801c78:	eb 3b                	jmp    801cb5 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801c7a:	89 da                	mov    %ebx,%edx
  801c7c:	89 f0                	mov    %esi,%eax
  801c7e:	e8 6d ff ff ff       	call   801bf0 <_pipeisclosed>
  801c83:	85 c0                	test   %eax,%eax
  801c85:	75 38                	jne    801cbf <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801c87:	e8 db f3 ff ff       	call   801067 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801c8c:	8b 53 04             	mov    0x4(%ebx),%edx
  801c8f:	8b 03                	mov    (%ebx),%eax
  801c91:	83 c0 20             	add    $0x20,%eax
  801c94:	39 c2                	cmp    %eax,%edx
  801c96:	73 e2                	jae    801c7a <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801c98:	8b 45 0c             	mov    0xc(%ebp),%eax
  801c9b:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801c9e:	89 d0                	mov    %edx,%eax
  801ca0:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801ca5:	79 05                	jns    801cac <devpipe_write+0x50>
  801ca7:	48                   	dec    %eax
  801ca8:	83 c8 e0             	or     $0xffffffe0,%eax
  801cab:	40                   	inc    %eax
  801cac:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801cb0:	42                   	inc    %edx
  801cb1:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801cb4:	47                   	inc    %edi
  801cb5:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801cb8:	75 d2                	jne    801c8c <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801cba:	8b 45 10             	mov    0x10(%ebp),%eax
  801cbd:	eb 05                	jmp    801cc4 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801cbf:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801cc4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801cc7:	5b                   	pop    %ebx
  801cc8:	5e                   	pop    %esi
  801cc9:	5f                   	pop    %edi
  801cca:	5d                   	pop    %ebp
  801ccb:	c3                   	ret    

00801ccc <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801ccc:	55                   	push   %ebp
  801ccd:	89 e5                	mov    %esp,%ebp
  801ccf:	57                   	push   %edi
  801cd0:	56                   	push   %esi
  801cd1:	53                   	push   %ebx
  801cd2:	83 ec 18             	sub    $0x18,%esp
  801cd5:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801cd8:	57                   	push   %edi
  801cd9:	e8 c9 f6 ff ff       	call   8013a7 <fd2data>
  801cde:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801ce0:	83 c4 10             	add    $0x10,%esp
  801ce3:	bb 00 00 00 00       	mov    $0x0,%ebx
  801ce8:	eb 3a                	jmp    801d24 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801cea:	85 db                	test   %ebx,%ebx
  801cec:	74 04                	je     801cf2 <devpipe_read+0x26>
				return i;
  801cee:	89 d8                	mov    %ebx,%eax
  801cf0:	eb 41                	jmp    801d33 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801cf2:	89 f2                	mov    %esi,%edx
  801cf4:	89 f8                	mov    %edi,%eax
  801cf6:	e8 f5 fe ff ff       	call   801bf0 <_pipeisclosed>
  801cfb:	85 c0                	test   %eax,%eax
  801cfd:	75 2f                	jne    801d2e <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801cff:	e8 63 f3 ff ff       	call   801067 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801d04:	8b 06                	mov    (%esi),%eax
  801d06:	3b 46 04             	cmp    0x4(%esi),%eax
  801d09:	74 df                	je     801cea <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801d0b:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801d10:	79 05                	jns    801d17 <devpipe_read+0x4b>
  801d12:	48                   	dec    %eax
  801d13:	83 c8 e0             	or     $0xffffffe0,%eax
  801d16:	40                   	inc    %eax
  801d17:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801d1b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801d1e:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801d21:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801d23:	43                   	inc    %ebx
  801d24:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801d27:	75 db                	jne    801d04 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801d29:	8b 45 10             	mov    0x10(%ebp),%eax
  801d2c:	eb 05                	jmp    801d33 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801d2e:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801d33:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d36:	5b                   	pop    %ebx
  801d37:	5e                   	pop    %esi
  801d38:	5f                   	pop    %edi
  801d39:	5d                   	pop    %ebp
  801d3a:	c3                   	ret    

00801d3b <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801d3b:	55                   	push   %ebp
  801d3c:	89 e5                	mov    %esp,%ebp
  801d3e:	56                   	push   %esi
  801d3f:	53                   	push   %ebx
  801d40:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801d43:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801d46:	50                   	push   %eax
  801d47:	e8 72 f6 ff ff       	call   8013be <fd_alloc>
  801d4c:	83 c4 10             	add    $0x10,%esp
  801d4f:	85 c0                	test   %eax,%eax
  801d51:	0f 88 2a 01 00 00    	js     801e81 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801d57:	83 ec 04             	sub    $0x4,%esp
  801d5a:	68 07 04 00 00       	push   $0x407
  801d5f:	ff 75 f4             	pushl  -0xc(%ebp)
  801d62:	6a 00                	push   $0x0
  801d64:	e8 1d f3 ff ff       	call   801086 <sys_page_alloc>
  801d69:	83 c4 10             	add    $0x10,%esp
  801d6c:	85 c0                	test   %eax,%eax
  801d6e:	0f 88 0d 01 00 00    	js     801e81 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801d74:	83 ec 0c             	sub    $0xc,%esp
  801d77:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801d7a:	50                   	push   %eax
  801d7b:	e8 3e f6 ff ff       	call   8013be <fd_alloc>
  801d80:	89 c3                	mov    %eax,%ebx
  801d82:	83 c4 10             	add    $0x10,%esp
  801d85:	85 c0                	test   %eax,%eax
  801d87:	0f 88 e2 00 00 00    	js     801e6f <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801d8d:	83 ec 04             	sub    $0x4,%esp
  801d90:	68 07 04 00 00       	push   $0x407
  801d95:	ff 75 f0             	pushl  -0x10(%ebp)
  801d98:	6a 00                	push   $0x0
  801d9a:	e8 e7 f2 ff ff       	call   801086 <sys_page_alloc>
  801d9f:	89 c3                	mov    %eax,%ebx
  801da1:	83 c4 10             	add    $0x10,%esp
  801da4:	85 c0                	test   %eax,%eax
  801da6:	0f 88 c3 00 00 00    	js     801e6f <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801dac:	83 ec 0c             	sub    $0xc,%esp
  801daf:	ff 75 f4             	pushl  -0xc(%ebp)
  801db2:	e8 f0 f5 ff ff       	call   8013a7 <fd2data>
  801db7:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801db9:	83 c4 0c             	add    $0xc,%esp
  801dbc:	68 07 04 00 00       	push   $0x407
  801dc1:	50                   	push   %eax
  801dc2:	6a 00                	push   $0x0
  801dc4:	e8 bd f2 ff ff       	call   801086 <sys_page_alloc>
  801dc9:	89 c3                	mov    %eax,%ebx
  801dcb:	83 c4 10             	add    $0x10,%esp
  801dce:	85 c0                	test   %eax,%eax
  801dd0:	0f 88 89 00 00 00    	js     801e5f <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801dd6:	83 ec 0c             	sub    $0xc,%esp
  801dd9:	ff 75 f0             	pushl  -0x10(%ebp)
  801ddc:	e8 c6 f5 ff ff       	call   8013a7 <fd2data>
  801de1:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801de8:	50                   	push   %eax
  801de9:	6a 00                	push   $0x0
  801deb:	56                   	push   %esi
  801dec:	6a 00                	push   $0x0
  801dee:	e8 d6 f2 ff ff       	call   8010c9 <sys_page_map>
  801df3:	89 c3                	mov    %eax,%ebx
  801df5:	83 c4 20             	add    $0x20,%esp
  801df8:	85 c0                	test   %eax,%eax
  801dfa:	78 55                	js     801e51 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801dfc:	8b 15 24 30 80 00    	mov    0x803024,%edx
  801e02:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e05:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801e07:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e0a:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801e11:	8b 15 24 30 80 00    	mov    0x803024,%edx
  801e17:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801e1a:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801e1c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801e1f:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801e26:	83 ec 0c             	sub    $0xc,%esp
  801e29:	ff 75 f4             	pushl  -0xc(%ebp)
  801e2c:	e8 66 f5 ff ff       	call   801397 <fd2num>
  801e31:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801e34:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801e36:	83 c4 04             	add    $0x4,%esp
  801e39:	ff 75 f0             	pushl  -0x10(%ebp)
  801e3c:	e8 56 f5 ff ff       	call   801397 <fd2num>
  801e41:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801e44:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801e47:	83 c4 10             	add    $0x10,%esp
  801e4a:	b8 00 00 00 00       	mov    $0x0,%eax
  801e4f:	eb 30                	jmp    801e81 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801e51:	83 ec 08             	sub    $0x8,%esp
  801e54:	56                   	push   %esi
  801e55:	6a 00                	push   $0x0
  801e57:	e8 af f2 ff ff       	call   80110b <sys_page_unmap>
  801e5c:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801e5f:	83 ec 08             	sub    $0x8,%esp
  801e62:	ff 75 f0             	pushl  -0x10(%ebp)
  801e65:	6a 00                	push   $0x0
  801e67:	e8 9f f2 ff ff       	call   80110b <sys_page_unmap>
  801e6c:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801e6f:	83 ec 08             	sub    $0x8,%esp
  801e72:	ff 75 f4             	pushl  -0xc(%ebp)
  801e75:	6a 00                	push   $0x0
  801e77:	e8 8f f2 ff ff       	call   80110b <sys_page_unmap>
  801e7c:	83 c4 10             	add    $0x10,%esp
  801e7f:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801e81:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801e84:	5b                   	pop    %ebx
  801e85:	5e                   	pop    %esi
  801e86:	5d                   	pop    %ebp
  801e87:	c3                   	ret    

00801e88 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801e88:	55                   	push   %ebp
  801e89:	89 e5                	mov    %esp,%ebp
  801e8b:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801e8e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801e91:	50                   	push   %eax
  801e92:	ff 75 08             	pushl  0x8(%ebp)
  801e95:	e8 73 f5 ff ff       	call   80140d <fd_lookup>
  801e9a:	83 c4 10             	add    $0x10,%esp
  801e9d:	85 c0                	test   %eax,%eax
  801e9f:	78 18                	js     801eb9 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801ea1:	83 ec 0c             	sub    $0xc,%esp
  801ea4:	ff 75 f4             	pushl  -0xc(%ebp)
  801ea7:	e8 fb f4 ff ff       	call   8013a7 <fd2data>
	return _pipeisclosed(fd, p);
  801eac:	89 c2                	mov    %eax,%edx
  801eae:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801eb1:	e8 3a fd ff ff       	call   801bf0 <_pipeisclosed>
  801eb6:	83 c4 10             	add    $0x10,%esp
}
  801eb9:	c9                   	leave  
  801eba:	c3                   	ret    

00801ebb <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801ebb:	55                   	push   %ebp
  801ebc:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801ebe:	b8 00 00 00 00       	mov    $0x0,%eax
  801ec3:	5d                   	pop    %ebp
  801ec4:	c3                   	ret    

00801ec5 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801ec5:	55                   	push   %ebp
  801ec6:	89 e5                	mov    %esp,%ebp
  801ec8:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801ecb:	68 06 2b 80 00       	push   $0x802b06
  801ed0:	ff 75 0c             	pushl  0xc(%ebp)
  801ed3:	e8 ca ed ff ff       	call   800ca2 <strcpy>
	return 0;
}
  801ed8:	b8 00 00 00 00       	mov    $0x0,%eax
  801edd:	c9                   	leave  
  801ede:	c3                   	ret    

00801edf <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801edf:	55                   	push   %ebp
  801ee0:	89 e5                	mov    %esp,%ebp
  801ee2:	57                   	push   %edi
  801ee3:	56                   	push   %esi
  801ee4:	53                   	push   %ebx
  801ee5:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801eeb:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801ef0:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801ef6:	eb 2c                	jmp    801f24 <devcons_write+0x45>
		m = n - tot;
  801ef8:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801efb:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801efd:	83 fb 7f             	cmp    $0x7f,%ebx
  801f00:	76 05                	jbe    801f07 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801f02:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801f07:	83 ec 04             	sub    $0x4,%esp
  801f0a:	53                   	push   %ebx
  801f0b:	03 45 0c             	add    0xc(%ebp),%eax
  801f0e:	50                   	push   %eax
  801f0f:	57                   	push   %edi
  801f10:	e8 02 ef ff ff       	call   800e17 <memmove>
		sys_cputs(buf, m);
  801f15:	83 c4 08             	add    $0x8,%esp
  801f18:	53                   	push   %ebx
  801f19:	57                   	push   %edi
  801f1a:	e8 ab f0 ff ff       	call   800fca <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801f1f:	01 de                	add    %ebx,%esi
  801f21:	83 c4 10             	add    $0x10,%esp
  801f24:	89 f0                	mov    %esi,%eax
  801f26:	3b 75 10             	cmp    0x10(%ebp),%esi
  801f29:	72 cd                	jb     801ef8 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801f2b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f2e:	5b                   	pop    %ebx
  801f2f:	5e                   	pop    %esi
  801f30:	5f                   	pop    %edi
  801f31:	5d                   	pop    %ebp
  801f32:	c3                   	ret    

00801f33 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801f33:	55                   	push   %ebp
  801f34:	89 e5                	mov    %esp,%ebp
  801f36:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801f39:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801f3d:	75 07                	jne    801f46 <devcons_read+0x13>
  801f3f:	eb 23                	jmp    801f64 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801f41:	e8 21 f1 ff ff       	call   801067 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801f46:	e8 9d f0 ff ff       	call   800fe8 <sys_cgetc>
  801f4b:	85 c0                	test   %eax,%eax
  801f4d:	74 f2                	je     801f41 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801f4f:	85 c0                	test   %eax,%eax
  801f51:	78 1d                	js     801f70 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801f53:	83 f8 04             	cmp    $0x4,%eax
  801f56:	74 13                	je     801f6b <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801f58:	8b 55 0c             	mov    0xc(%ebp),%edx
  801f5b:	88 02                	mov    %al,(%edx)
	return 1;
  801f5d:	b8 01 00 00 00       	mov    $0x1,%eax
  801f62:	eb 0c                	jmp    801f70 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801f64:	b8 00 00 00 00       	mov    $0x0,%eax
  801f69:	eb 05                	jmp    801f70 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801f6b:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801f70:	c9                   	leave  
  801f71:	c3                   	ret    

00801f72 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801f72:	55                   	push   %ebp
  801f73:	89 e5                	mov    %esp,%ebp
  801f75:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801f78:	8b 45 08             	mov    0x8(%ebp),%eax
  801f7b:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801f7e:	6a 01                	push   $0x1
  801f80:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801f83:	50                   	push   %eax
  801f84:	e8 41 f0 ff ff       	call   800fca <sys_cputs>
}
  801f89:	83 c4 10             	add    $0x10,%esp
  801f8c:	c9                   	leave  
  801f8d:	c3                   	ret    

00801f8e <getchar>:

int
getchar(void)
{
  801f8e:	55                   	push   %ebp
  801f8f:	89 e5                	mov    %esp,%ebp
  801f91:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801f94:	6a 01                	push   $0x1
  801f96:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801f99:	50                   	push   %eax
  801f9a:	6a 00                	push   $0x0
  801f9c:	e8 d2 f6 ff ff       	call   801673 <read>
	if (r < 0)
  801fa1:	83 c4 10             	add    $0x10,%esp
  801fa4:	85 c0                	test   %eax,%eax
  801fa6:	78 0f                	js     801fb7 <getchar+0x29>
		return r;
	if (r < 1)
  801fa8:	85 c0                	test   %eax,%eax
  801faa:	7e 06                	jle    801fb2 <getchar+0x24>
		return -E_EOF;
	return c;
  801fac:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801fb0:	eb 05                	jmp    801fb7 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801fb2:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801fb7:	c9                   	leave  
  801fb8:	c3                   	ret    

00801fb9 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801fb9:	55                   	push   %ebp
  801fba:	89 e5                	mov    %esp,%ebp
  801fbc:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801fbf:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801fc2:	50                   	push   %eax
  801fc3:	ff 75 08             	pushl  0x8(%ebp)
  801fc6:	e8 42 f4 ff ff       	call   80140d <fd_lookup>
  801fcb:	83 c4 10             	add    $0x10,%esp
  801fce:	85 c0                	test   %eax,%eax
  801fd0:	78 11                	js     801fe3 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801fd2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801fd5:	8b 15 40 30 80 00    	mov    0x803040,%edx
  801fdb:	39 10                	cmp    %edx,(%eax)
  801fdd:	0f 94 c0             	sete   %al
  801fe0:	0f b6 c0             	movzbl %al,%eax
}
  801fe3:	c9                   	leave  
  801fe4:	c3                   	ret    

00801fe5 <opencons>:

int
opencons(void)
{
  801fe5:	55                   	push   %ebp
  801fe6:	89 e5                	mov    %esp,%ebp
  801fe8:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801feb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801fee:	50                   	push   %eax
  801fef:	e8 ca f3 ff ff       	call   8013be <fd_alloc>
  801ff4:	83 c4 10             	add    $0x10,%esp
  801ff7:	85 c0                	test   %eax,%eax
  801ff9:	78 3a                	js     802035 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801ffb:	83 ec 04             	sub    $0x4,%esp
  801ffe:	68 07 04 00 00       	push   $0x407
  802003:	ff 75 f4             	pushl  -0xc(%ebp)
  802006:	6a 00                	push   $0x0
  802008:	e8 79 f0 ff ff       	call   801086 <sys_page_alloc>
  80200d:	83 c4 10             	add    $0x10,%esp
  802010:	85 c0                	test   %eax,%eax
  802012:	78 21                	js     802035 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  802014:	8b 15 40 30 80 00    	mov    0x803040,%edx
  80201a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80201d:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  80201f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802022:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  802029:	83 ec 0c             	sub    $0xc,%esp
  80202c:	50                   	push   %eax
  80202d:	e8 65 f3 ff ff       	call   801397 <fd2num>
  802032:	83 c4 10             	add    $0x10,%esp
}
  802035:	c9                   	leave  
  802036:	c3                   	ret    

00802037 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802037:	55                   	push   %ebp
  802038:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  80203a:	8b 45 08             	mov    0x8(%ebp),%eax
  80203d:	c1 e8 16             	shr    $0x16,%eax
  802040:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  802047:	a8 01                	test   $0x1,%al
  802049:	74 21                	je     80206c <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  80204b:	8b 45 08             	mov    0x8(%ebp),%eax
  80204e:	c1 e8 0c             	shr    $0xc,%eax
  802051:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  802058:	a8 01                	test   $0x1,%al
  80205a:	74 17                	je     802073 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  80205c:	c1 e8 0c             	shr    $0xc,%eax
  80205f:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  802066:	ef 
  802067:	0f b7 c0             	movzwl %ax,%eax
  80206a:	eb 0c                	jmp    802078 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  80206c:	b8 00 00 00 00       	mov    $0x0,%eax
  802071:	eb 05                	jmp    802078 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  802073:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  802078:	5d                   	pop    %ebp
  802079:	c3                   	ret    
  80207a:	66 90                	xchg   %ax,%ax

0080207c <__udivdi3>:
  80207c:	55                   	push   %ebp
  80207d:	57                   	push   %edi
  80207e:	56                   	push   %esi
  80207f:	53                   	push   %ebx
  802080:	83 ec 1c             	sub    $0x1c,%esp
  802083:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  802087:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  80208b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80208f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  802093:	89 ca                	mov    %ecx,%edx
  802095:	89 f8                	mov    %edi,%eax
  802097:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  80209b:	85 f6                	test   %esi,%esi
  80209d:	75 2d                	jne    8020cc <__udivdi3+0x50>
  80209f:	39 cf                	cmp    %ecx,%edi
  8020a1:	77 65                	ja     802108 <__udivdi3+0x8c>
  8020a3:	89 fd                	mov    %edi,%ebp
  8020a5:	85 ff                	test   %edi,%edi
  8020a7:	75 0b                	jne    8020b4 <__udivdi3+0x38>
  8020a9:	b8 01 00 00 00       	mov    $0x1,%eax
  8020ae:	31 d2                	xor    %edx,%edx
  8020b0:	f7 f7                	div    %edi
  8020b2:	89 c5                	mov    %eax,%ebp
  8020b4:	31 d2                	xor    %edx,%edx
  8020b6:	89 c8                	mov    %ecx,%eax
  8020b8:	f7 f5                	div    %ebp
  8020ba:	89 c1                	mov    %eax,%ecx
  8020bc:	89 d8                	mov    %ebx,%eax
  8020be:	f7 f5                	div    %ebp
  8020c0:	89 cf                	mov    %ecx,%edi
  8020c2:	89 fa                	mov    %edi,%edx
  8020c4:	83 c4 1c             	add    $0x1c,%esp
  8020c7:	5b                   	pop    %ebx
  8020c8:	5e                   	pop    %esi
  8020c9:	5f                   	pop    %edi
  8020ca:	5d                   	pop    %ebp
  8020cb:	c3                   	ret    
  8020cc:	39 ce                	cmp    %ecx,%esi
  8020ce:	77 28                	ja     8020f8 <__udivdi3+0x7c>
  8020d0:	0f bd fe             	bsr    %esi,%edi
  8020d3:	83 f7 1f             	xor    $0x1f,%edi
  8020d6:	75 40                	jne    802118 <__udivdi3+0x9c>
  8020d8:	39 ce                	cmp    %ecx,%esi
  8020da:	72 0a                	jb     8020e6 <__udivdi3+0x6a>
  8020dc:	3b 44 24 08          	cmp    0x8(%esp),%eax
  8020e0:	0f 87 9e 00 00 00    	ja     802184 <__udivdi3+0x108>
  8020e6:	b8 01 00 00 00       	mov    $0x1,%eax
  8020eb:	89 fa                	mov    %edi,%edx
  8020ed:	83 c4 1c             	add    $0x1c,%esp
  8020f0:	5b                   	pop    %ebx
  8020f1:	5e                   	pop    %esi
  8020f2:	5f                   	pop    %edi
  8020f3:	5d                   	pop    %ebp
  8020f4:	c3                   	ret    
  8020f5:	8d 76 00             	lea    0x0(%esi),%esi
  8020f8:	31 ff                	xor    %edi,%edi
  8020fa:	31 c0                	xor    %eax,%eax
  8020fc:	89 fa                	mov    %edi,%edx
  8020fe:	83 c4 1c             	add    $0x1c,%esp
  802101:	5b                   	pop    %ebx
  802102:	5e                   	pop    %esi
  802103:	5f                   	pop    %edi
  802104:	5d                   	pop    %ebp
  802105:	c3                   	ret    
  802106:	66 90                	xchg   %ax,%ax
  802108:	89 d8                	mov    %ebx,%eax
  80210a:	f7 f7                	div    %edi
  80210c:	31 ff                	xor    %edi,%edi
  80210e:	89 fa                	mov    %edi,%edx
  802110:	83 c4 1c             	add    $0x1c,%esp
  802113:	5b                   	pop    %ebx
  802114:	5e                   	pop    %esi
  802115:	5f                   	pop    %edi
  802116:	5d                   	pop    %ebp
  802117:	c3                   	ret    
  802118:	bd 20 00 00 00       	mov    $0x20,%ebp
  80211d:	89 eb                	mov    %ebp,%ebx
  80211f:	29 fb                	sub    %edi,%ebx
  802121:	89 f9                	mov    %edi,%ecx
  802123:	d3 e6                	shl    %cl,%esi
  802125:	89 c5                	mov    %eax,%ebp
  802127:	88 d9                	mov    %bl,%cl
  802129:	d3 ed                	shr    %cl,%ebp
  80212b:	89 e9                	mov    %ebp,%ecx
  80212d:	09 f1                	or     %esi,%ecx
  80212f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  802133:	89 f9                	mov    %edi,%ecx
  802135:	d3 e0                	shl    %cl,%eax
  802137:	89 c5                	mov    %eax,%ebp
  802139:	89 d6                	mov    %edx,%esi
  80213b:	88 d9                	mov    %bl,%cl
  80213d:	d3 ee                	shr    %cl,%esi
  80213f:	89 f9                	mov    %edi,%ecx
  802141:	d3 e2                	shl    %cl,%edx
  802143:	8b 44 24 08          	mov    0x8(%esp),%eax
  802147:	88 d9                	mov    %bl,%cl
  802149:	d3 e8                	shr    %cl,%eax
  80214b:	09 c2                	or     %eax,%edx
  80214d:	89 d0                	mov    %edx,%eax
  80214f:	89 f2                	mov    %esi,%edx
  802151:	f7 74 24 0c          	divl   0xc(%esp)
  802155:	89 d6                	mov    %edx,%esi
  802157:	89 c3                	mov    %eax,%ebx
  802159:	f7 e5                	mul    %ebp
  80215b:	39 d6                	cmp    %edx,%esi
  80215d:	72 19                	jb     802178 <__udivdi3+0xfc>
  80215f:	74 0b                	je     80216c <__udivdi3+0xf0>
  802161:	89 d8                	mov    %ebx,%eax
  802163:	31 ff                	xor    %edi,%edi
  802165:	e9 58 ff ff ff       	jmp    8020c2 <__udivdi3+0x46>
  80216a:	66 90                	xchg   %ax,%ax
  80216c:	8b 54 24 08          	mov    0x8(%esp),%edx
  802170:	89 f9                	mov    %edi,%ecx
  802172:	d3 e2                	shl    %cl,%edx
  802174:	39 c2                	cmp    %eax,%edx
  802176:	73 e9                	jae    802161 <__udivdi3+0xe5>
  802178:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80217b:	31 ff                	xor    %edi,%edi
  80217d:	e9 40 ff ff ff       	jmp    8020c2 <__udivdi3+0x46>
  802182:	66 90                	xchg   %ax,%ax
  802184:	31 c0                	xor    %eax,%eax
  802186:	e9 37 ff ff ff       	jmp    8020c2 <__udivdi3+0x46>
  80218b:	90                   	nop

0080218c <__umoddi3>:
  80218c:	55                   	push   %ebp
  80218d:	57                   	push   %edi
  80218e:	56                   	push   %esi
  80218f:	53                   	push   %ebx
  802190:	83 ec 1c             	sub    $0x1c,%esp
  802193:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  802197:	8b 74 24 34          	mov    0x34(%esp),%esi
  80219b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80219f:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8021a3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8021a7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8021ab:	89 f3                	mov    %esi,%ebx
  8021ad:	89 fa                	mov    %edi,%edx
  8021af:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8021b3:	89 34 24             	mov    %esi,(%esp)
  8021b6:	85 c0                	test   %eax,%eax
  8021b8:	75 1a                	jne    8021d4 <__umoddi3+0x48>
  8021ba:	39 f7                	cmp    %esi,%edi
  8021bc:	0f 86 a2 00 00 00    	jbe    802264 <__umoddi3+0xd8>
  8021c2:	89 c8                	mov    %ecx,%eax
  8021c4:	89 f2                	mov    %esi,%edx
  8021c6:	f7 f7                	div    %edi
  8021c8:	89 d0                	mov    %edx,%eax
  8021ca:	31 d2                	xor    %edx,%edx
  8021cc:	83 c4 1c             	add    $0x1c,%esp
  8021cf:	5b                   	pop    %ebx
  8021d0:	5e                   	pop    %esi
  8021d1:	5f                   	pop    %edi
  8021d2:	5d                   	pop    %ebp
  8021d3:	c3                   	ret    
  8021d4:	39 f0                	cmp    %esi,%eax
  8021d6:	0f 87 ac 00 00 00    	ja     802288 <__umoddi3+0xfc>
  8021dc:	0f bd e8             	bsr    %eax,%ebp
  8021df:	83 f5 1f             	xor    $0x1f,%ebp
  8021e2:	0f 84 ac 00 00 00    	je     802294 <__umoddi3+0x108>
  8021e8:	bf 20 00 00 00       	mov    $0x20,%edi
  8021ed:	29 ef                	sub    %ebp,%edi
  8021ef:	89 fe                	mov    %edi,%esi
  8021f1:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8021f5:	89 e9                	mov    %ebp,%ecx
  8021f7:	d3 e0                	shl    %cl,%eax
  8021f9:	89 d7                	mov    %edx,%edi
  8021fb:	89 f1                	mov    %esi,%ecx
  8021fd:	d3 ef                	shr    %cl,%edi
  8021ff:	09 c7                	or     %eax,%edi
  802201:	89 e9                	mov    %ebp,%ecx
  802203:	d3 e2                	shl    %cl,%edx
  802205:	89 14 24             	mov    %edx,(%esp)
  802208:	89 d8                	mov    %ebx,%eax
  80220a:	d3 e0                	shl    %cl,%eax
  80220c:	89 c2                	mov    %eax,%edx
  80220e:	8b 44 24 08          	mov    0x8(%esp),%eax
  802212:	d3 e0                	shl    %cl,%eax
  802214:	89 44 24 04          	mov    %eax,0x4(%esp)
  802218:	8b 44 24 08          	mov    0x8(%esp),%eax
  80221c:	89 f1                	mov    %esi,%ecx
  80221e:	d3 e8                	shr    %cl,%eax
  802220:	09 d0                	or     %edx,%eax
  802222:	d3 eb                	shr    %cl,%ebx
  802224:	89 da                	mov    %ebx,%edx
  802226:	f7 f7                	div    %edi
  802228:	89 d3                	mov    %edx,%ebx
  80222a:	f7 24 24             	mull   (%esp)
  80222d:	89 c6                	mov    %eax,%esi
  80222f:	89 d1                	mov    %edx,%ecx
  802231:	39 d3                	cmp    %edx,%ebx
  802233:	0f 82 87 00 00 00    	jb     8022c0 <__umoddi3+0x134>
  802239:	0f 84 91 00 00 00    	je     8022d0 <__umoddi3+0x144>
  80223f:	8b 54 24 04          	mov    0x4(%esp),%edx
  802243:	29 f2                	sub    %esi,%edx
  802245:	19 cb                	sbb    %ecx,%ebx
  802247:	89 d8                	mov    %ebx,%eax
  802249:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  80224d:	d3 e0                	shl    %cl,%eax
  80224f:	89 e9                	mov    %ebp,%ecx
  802251:	d3 ea                	shr    %cl,%edx
  802253:	09 d0                	or     %edx,%eax
  802255:	89 e9                	mov    %ebp,%ecx
  802257:	d3 eb                	shr    %cl,%ebx
  802259:	89 da                	mov    %ebx,%edx
  80225b:	83 c4 1c             	add    $0x1c,%esp
  80225e:	5b                   	pop    %ebx
  80225f:	5e                   	pop    %esi
  802260:	5f                   	pop    %edi
  802261:	5d                   	pop    %ebp
  802262:	c3                   	ret    
  802263:	90                   	nop
  802264:	89 fd                	mov    %edi,%ebp
  802266:	85 ff                	test   %edi,%edi
  802268:	75 0b                	jne    802275 <__umoddi3+0xe9>
  80226a:	b8 01 00 00 00       	mov    $0x1,%eax
  80226f:	31 d2                	xor    %edx,%edx
  802271:	f7 f7                	div    %edi
  802273:	89 c5                	mov    %eax,%ebp
  802275:	89 f0                	mov    %esi,%eax
  802277:	31 d2                	xor    %edx,%edx
  802279:	f7 f5                	div    %ebp
  80227b:	89 c8                	mov    %ecx,%eax
  80227d:	f7 f5                	div    %ebp
  80227f:	89 d0                	mov    %edx,%eax
  802281:	e9 44 ff ff ff       	jmp    8021ca <__umoddi3+0x3e>
  802286:	66 90                	xchg   %ax,%ax
  802288:	89 c8                	mov    %ecx,%eax
  80228a:	89 f2                	mov    %esi,%edx
  80228c:	83 c4 1c             	add    $0x1c,%esp
  80228f:	5b                   	pop    %ebx
  802290:	5e                   	pop    %esi
  802291:	5f                   	pop    %edi
  802292:	5d                   	pop    %ebp
  802293:	c3                   	ret    
  802294:	3b 04 24             	cmp    (%esp),%eax
  802297:	72 06                	jb     80229f <__umoddi3+0x113>
  802299:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  80229d:	77 0f                	ja     8022ae <__umoddi3+0x122>
  80229f:	89 f2                	mov    %esi,%edx
  8022a1:	29 f9                	sub    %edi,%ecx
  8022a3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8022a7:	89 14 24             	mov    %edx,(%esp)
  8022aa:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8022ae:	8b 44 24 04          	mov    0x4(%esp),%eax
  8022b2:	8b 14 24             	mov    (%esp),%edx
  8022b5:	83 c4 1c             	add    $0x1c,%esp
  8022b8:	5b                   	pop    %ebx
  8022b9:	5e                   	pop    %esi
  8022ba:	5f                   	pop    %edi
  8022bb:	5d                   	pop    %ebp
  8022bc:	c3                   	ret    
  8022bd:	8d 76 00             	lea    0x0(%esi),%esi
  8022c0:	2b 04 24             	sub    (%esp),%eax
  8022c3:	19 fa                	sbb    %edi,%edx
  8022c5:	89 d1                	mov    %edx,%ecx
  8022c7:	89 c6                	mov    %eax,%esi
  8022c9:	e9 71 ff ff ff       	jmp    80223f <__umoddi3+0xb3>
  8022ce:	66 90                	xchg   %ax,%ax
  8022d0:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8022d4:	72 ea                	jb     8022c0 <__umoddi3+0x134>
  8022d6:	89 d9                	mov    %ebx,%ecx
  8022d8:	e9 62 ff ff ff       	jmp    80223f <__umoddi3+0xb3>
