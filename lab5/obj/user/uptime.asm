
obj/user/uptime.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 55 00 00 00       	call   800086 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
	unsigned msec = sys_time_msec();
  800038:	e8 6f 02 00 00       	call   8002ac <sys_time_msec>
  80003d:	89 c1                	mov    %eax,%ecx

	printf("%u.%02u\n", msec / 1000, (msec % 1000) * 100 / 1000);
  80003f:	83 ec 04             	sub    $0x4,%esp
  800042:	bb e8 03 00 00       	mov    $0x3e8,%ebx
  800047:	ba 00 00 00 00       	mov    $0x0,%edx
  80004c:	f7 f3                	div    %ebx
  80004e:	8d 04 92             	lea    (%edx,%edx,4),%eax
  800051:	8d 34 80             	lea    (%eax,%eax,4),%esi
  800054:	8d 04 b5 00 00 00 00 	lea    0x0(,%esi,4),%eax
  80005b:	bb d3 4d 62 10       	mov    $0x10624dd3,%ebx
  800060:	f7 e3                	mul    %ebx
  800062:	89 d0                	mov    %edx,%eax
  800064:	c1 e8 06             	shr    $0x6,%eax
  800067:	50                   	push   %eax
  800068:	89 c8                	mov    %ecx,%eax
  80006a:	f7 e3                	mul    %ebx
  80006c:	89 d0                	mov    %edx,%eax
  80006e:	c1 e8 06             	shr    $0x6,%eax
  800071:	50                   	push   %eax
  800072:	68 80 1e 80 00       	push   $0x801e80
  800077:	e8 32 04 00 00       	call   8004ae <printf>
}
  80007c:	83 c4 10             	add    $0x10,%esp
  80007f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800082:	5b                   	pop    %ebx
  800083:	5e                   	pop    %esi
  800084:	5d                   	pop    %ebp
  800085:	c3                   	ret    

00800086 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800086:	55                   	push   %ebp
  800087:	89 e5                	mov    %esp,%ebp
  800089:	56                   	push   %esi
  80008a:	53                   	push   %ebx
  80008b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80008e:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800091:	e8 cf 00 00 00       	call   800165 <sys_getenvid>
  800096:	25 ff 03 00 00       	and    $0x3ff,%eax
  80009b:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000a2:	c1 e0 07             	shl    $0x7,%eax
  8000a5:	29 d0                	sub    %edx,%eax
  8000a7:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000ac:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000b1:	85 db                	test   %ebx,%ebx
  8000b3:	7e 07                	jle    8000bc <libmain+0x36>
		binaryname = argv[0];
  8000b5:	8b 06                	mov    (%esi),%eax
  8000b7:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  8000bc:	83 ec 08             	sub    $0x8,%esp
  8000bf:	56                   	push   %esi
  8000c0:	53                   	push   %ebx
  8000c1:	e8 6d ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000c6:	e8 0a 00 00 00       	call   8000d5 <exit>
}
  8000cb:	83 c4 10             	add    $0x10,%esp
  8000ce:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000d1:	5b                   	pop    %ebx
  8000d2:	5e                   	pop    %esi
  8000d3:	5d                   	pop    %ebp
  8000d4:	c3                   	ret    

008000d5 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000d5:	55                   	push   %ebp
  8000d6:	89 e5                	mov    %esp,%ebp
  8000d8:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000db:	6a 00                	push   $0x0
  8000dd:	e8 42 00 00 00       	call   800124 <sys_env_destroy>
}
  8000e2:	83 c4 10             	add    $0x10,%esp
  8000e5:	c9                   	leave  
  8000e6:	c3                   	ret    

008000e7 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000e7:	55                   	push   %ebp
  8000e8:	89 e5                	mov    %esp,%ebp
  8000ea:	57                   	push   %edi
  8000eb:	56                   	push   %esi
  8000ec:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ed:	b8 00 00 00 00       	mov    $0x0,%eax
  8000f2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000f5:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f8:	89 c3                	mov    %eax,%ebx
  8000fa:	89 c7                	mov    %eax,%edi
  8000fc:	89 c6                	mov    %eax,%esi
  8000fe:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800100:	5b                   	pop    %ebx
  800101:	5e                   	pop    %esi
  800102:	5f                   	pop    %edi
  800103:	5d                   	pop    %ebp
  800104:	c3                   	ret    

00800105 <sys_cgetc>:

int
sys_cgetc(void)
{
  800105:	55                   	push   %ebp
  800106:	89 e5                	mov    %esp,%ebp
  800108:	57                   	push   %edi
  800109:	56                   	push   %esi
  80010a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80010b:	ba 00 00 00 00       	mov    $0x0,%edx
  800110:	b8 01 00 00 00       	mov    $0x1,%eax
  800115:	89 d1                	mov    %edx,%ecx
  800117:	89 d3                	mov    %edx,%ebx
  800119:	89 d7                	mov    %edx,%edi
  80011b:	89 d6                	mov    %edx,%esi
  80011d:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  80011f:	5b                   	pop    %ebx
  800120:	5e                   	pop    %esi
  800121:	5f                   	pop    %edi
  800122:	5d                   	pop    %ebp
  800123:	c3                   	ret    

00800124 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800124:	55                   	push   %ebp
  800125:	89 e5                	mov    %esp,%ebp
  800127:	57                   	push   %edi
  800128:	56                   	push   %esi
  800129:	53                   	push   %ebx
  80012a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012d:	b9 00 00 00 00       	mov    $0x0,%ecx
  800132:	b8 03 00 00 00       	mov    $0x3,%eax
  800137:	8b 55 08             	mov    0x8(%ebp),%edx
  80013a:	89 cb                	mov    %ecx,%ebx
  80013c:	89 cf                	mov    %ecx,%edi
  80013e:	89 ce                	mov    %ecx,%esi
  800140:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800142:	85 c0                	test   %eax,%eax
  800144:	7e 17                	jle    80015d <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800146:	83 ec 0c             	sub    $0xc,%esp
  800149:	50                   	push   %eax
  80014a:	6a 03                	push   $0x3
  80014c:	68 93 1e 80 00       	push   $0x801e93
  800151:	6a 23                	push   $0x23
  800153:	68 b0 1e 80 00       	push   $0x801eb0
  800158:	e8 67 03 00 00       	call   8004c4 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80015d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800160:	5b                   	pop    %ebx
  800161:	5e                   	pop    %esi
  800162:	5f                   	pop    %edi
  800163:	5d                   	pop    %ebp
  800164:	c3                   	ret    

00800165 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800165:	55                   	push   %ebp
  800166:	89 e5                	mov    %esp,%ebp
  800168:	57                   	push   %edi
  800169:	56                   	push   %esi
  80016a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80016b:	ba 00 00 00 00       	mov    $0x0,%edx
  800170:	b8 02 00 00 00       	mov    $0x2,%eax
  800175:	89 d1                	mov    %edx,%ecx
  800177:	89 d3                	mov    %edx,%ebx
  800179:	89 d7                	mov    %edx,%edi
  80017b:	89 d6                	mov    %edx,%esi
  80017d:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80017f:	5b                   	pop    %ebx
  800180:	5e                   	pop    %esi
  800181:	5f                   	pop    %edi
  800182:	5d                   	pop    %ebp
  800183:	c3                   	ret    

00800184 <sys_yield>:

void
sys_yield(void)
{
  800184:	55                   	push   %ebp
  800185:	89 e5                	mov    %esp,%ebp
  800187:	57                   	push   %edi
  800188:	56                   	push   %esi
  800189:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80018a:	ba 00 00 00 00       	mov    $0x0,%edx
  80018f:	b8 0b 00 00 00       	mov    $0xb,%eax
  800194:	89 d1                	mov    %edx,%ecx
  800196:	89 d3                	mov    %edx,%ebx
  800198:	89 d7                	mov    %edx,%edi
  80019a:	89 d6                	mov    %edx,%esi
  80019c:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80019e:	5b                   	pop    %ebx
  80019f:	5e                   	pop    %esi
  8001a0:	5f                   	pop    %edi
  8001a1:	5d                   	pop    %ebp
  8001a2:	c3                   	ret    

008001a3 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  8001a3:	55                   	push   %ebp
  8001a4:	89 e5                	mov    %esp,%ebp
  8001a6:	57                   	push   %edi
  8001a7:	56                   	push   %esi
  8001a8:	53                   	push   %ebx
  8001a9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ac:	be 00 00 00 00       	mov    $0x0,%esi
  8001b1:	b8 04 00 00 00       	mov    $0x4,%eax
  8001b6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001b9:	8b 55 08             	mov    0x8(%ebp),%edx
  8001bc:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001bf:	89 f7                	mov    %esi,%edi
  8001c1:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c3:	85 c0                	test   %eax,%eax
  8001c5:	7e 17                	jle    8001de <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001c7:	83 ec 0c             	sub    $0xc,%esp
  8001ca:	50                   	push   %eax
  8001cb:	6a 04                	push   $0x4
  8001cd:	68 93 1e 80 00       	push   $0x801e93
  8001d2:	6a 23                	push   $0x23
  8001d4:	68 b0 1e 80 00       	push   $0x801eb0
  8001d9:	e8 e6 02 00 00       	call   8004c4 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001de:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001e1:	5b                   	pop    %ebx
  8001e2:	5e                   	pop    %esi
  8001e3:	5f                   	pop    %edi
  8001e4:	5d                   	pop    %ebp
  8001e5:	c3                   	ret    

008001e6 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001e6:	55                   	push   %ebp
  8001e7:	89 e5                	mov    %esp,%ebp
  8001e9:	57                   	push   %edi
  8001ea:	56                   	push   %esi
  8001eb:	53                   	push   %ebx
  8001ec:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ef:	b8 05 00 00 00       	mov    $0x5,%eax
  8001f4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001f7:	8b 55 08             	mov    0x8(%ebp),%edx
  8001fa:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001fd:	8b 7d 14             	mov    0x14(%ebp),%edi
  800200:	8b 75 18             	mov    0x18(%ebp),%esi
  800203:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800205:	85 c0                	test   %eax,%eax
  800207:	7e 17                	jle    800220 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800209:	83 ec 0c             	sub    $0xc,%esp
  80020c:	50                   	push   %eax
  80020d:	6a 05                	push   $0x5
  80020f:	68 93 1e 80 00       	push   $0x801e93
  800214:	6a 23                	push   $0x23
  800216:	68 b0 1e 80 00       	push   $0x801eb0
  80021b:	e8 a4 02 00 00       	call   8004c4 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800220:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800223:	5b                   	pop    %ebx
  800224:	5e                   	pop    %esi
  800225:	5f                   	pop    %edi
  800226:	5d                   	pop    %ebp
  800227:	c3                   	ret    

00800228 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800228:	55                   	push   %ebp
  800229:	89 e5                	mov    %esp,%ebp
  80022b:	57                   	push   %edi
  80022c:	56                   	push   %esi
  80022d:	53                   	push   %ebx
  80022e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800231:	bb 00 00 00 00       	mov    $0x0,%ebx
  800236:	b8 06 00 00 00       	mov    $0x6,%eax
  80023b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80023e:	8b 55 08             	mov    0x8(%ebp),%edx
  800241:	89 df                	mov    %ebx,%edi
  800243:	89 de                	mov    %ebx,%esi
  800245:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800247:	85 c0                	test   %eax,%eax
  800249:	7e 17                	jle    800262 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80024b:	83 ec 0c             	sub    $0xc,%esp
  80024e:	50                   	push   %eax
  80024f:	6a 06                	push   $0x6
  800251:	68 93 1e 80 00       	push   $0x801e93
  800256:	6a 23                	push   $0x23
  800258:	68 b0 1e 80 00       	push   $0x801eb0
  80025d:	e8 62 02 00 00       	call   8004c4 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800262:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800265:	5b                   	pop    %ebx
  800266:	5e                   	pop    %esi
  800267:	5f                   	pop    %edi
  800268:	5d                   	pop    %ebp
  800269:	c3                   	ret    

0080026a <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80026a:	55                   	push   %ebp
  80026b:	89 e5                	mov    %esp,%ebp
  80026d:	57                   	push   %edi
  80026e:	56                   	push   %esi
  80026f:	53                   	push   %ebx
  800270:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800273:	bb 00 00 00 00       	mov    $0x0,%ebx
  800278:	b8 08 00 00 00       	mov    $0x8,%eax
  80027d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800280:	8b 55 08             	mov    0x8(%ebp),%edx
  800283:	89 df                	mov    %ebx,%edi
  800285:	89 de                	mov    %ebx,%esi
  800287:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800289:	85 c0                	test   %eax,%eax
  80028b:	7e 17                	jle    8002a4 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80028d:	83 ec 0c             	sub    $0xc,%esp
  800290:	50                   	push   %eax
  800291:	6a 08                	push   $0x8
  800293:	68 93 1e 80 00       	push   $0x801e93
  800298:	6a 23                	push   $0x23
  80029a:	68 b0 1e 80 00       	push   $0x801eb0
  80029f:	e8 20 02 00 00       	call   8004c4 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  8002a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002a7:	5b                   	pop    %ebx
  8002a8:	5e                   	pop    %esi
  8002a9:	5f                   	pop    %edi
  8002aa:	5d                   	pop    %ebp
  8002ab:	c3                   	ret    

008002ac <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  8002ac:	55                   	push   %ebp
  8002ad:	89 e5                	mov    %esp,%ebp
  8002af:	57                   	push   %edi
  8002b0:	56                   	push   %esi
  8002b1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002b2:	ba 00 00 00 00       	mov    $0x0,%edx
  8002b7:	b8 0c 00 00 00       	mov    $0xc,%eax
  8002bc:	89 d1                	mov    %edx,%ecx
  8002be:	89 d3                	mov    %edx,%ebx
  8002c0:	89 d7                	mov    %edx,%edi
  8002c2:	89 d6                	mov    %edx,%esi
  8002c4:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  8002c6:	5b                   	pop    %ebx
  8002c7:	5e                   	pop    %esi
  8002c8:	5f                   	pop    %edi
  8002c9:	5d                   	pop    %ebp
  8002ca:	c3                   	ret    

008002cb <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  8002cb:	55                   	push   %ebp
  8002cc:	89 e5                	mov    %esp,%ebp
  8002ce:	57                   	push   %edi
  8002cf:	56                   	push   %esi
  8002d0:	53                   	push   %ebx
  8002d1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002d4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002d9:	b8 09 00 00 00       	mov    $0x9,%eax
  8002de:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002e1:	8b 55 08             	mov    0x8(%ebp),%edx
  8002e4:	89 df                	mov    %ebx,%edi
  8002e6:	89 de                	mov    %ebx,%esi
  8002e8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002ea:	85 c0                	test   %eax,%eax
  8002ec:	7e 17                	jle    800305 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002ee:	83 ec 0c             	sub    $0xc,%esp
  8002f1:	50                   	push   %eax
  8002f2:	6a 09                	push   $0x9
  8002f4:	68 93 1e 80 00       	push   $0x801e93
  8002f9:	6a 23                	push   $0x23
  8002fb:	68 b0 1e 80 00       	push   $0x801eb0
  800300:	e8 bf 01 00 00       	call   8004c4 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800305:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800308:	5b                   	pop    %ebx
  800309:	5e                   	pop    %esi
  80030a:	5f                   	pop    %edi
  80030b:	5d                   	pop    %ebp
  80030c:	c3                   	ret    

0080030d <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80030d:	55                   	push   %ebp
  80030e:	89 e5                	mov    %esp,%ebp
  800310:	57                   	push   %edi
  800311:	56                   	push   %esi
  800312:	53                   	push   %ebx
  800313:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800316:	bb 00 00 00 00       	mov    $0x0,%ebx
  80031b:	b8 0a 00 00 00       	mov    $0xa,%eax
  800320:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800323:	8b 55 08             	mov    0x8(%ebp),%edx
  800326:	89 df                	mov    %ebx,%edi
  800328:	89 de                	mov    %ebx,%esi
  80032a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80032c:	85 c0                	test   %eax,%eax
  80032e:	7e 17                	jle    800347 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800330:	83 ec 0c             	sub    $0xc,%esp
  800333:	50                   	push   %eax
  800334:	6a 0a                	push   $0xa
  800336:	68 93 1e 80 00       	push   $0x801e93
  80033b:	6a 23                	push   $0x23
  80033d:	68 b0 1e 80 00       	push   $0x801eb0
  800342:	e8 7d 01 00 00       	call   8004c4 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800347:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80034a:	5b                   	pop    %ebx
  80034b:	5e                   	pop    %esi
  80034c:	5f                   	pop    %edi
  80034d:	5d                   	pop    %ebp
  80034e:	c3                   	ret    

0080034f <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  80034f:	55                   	push   %ebp
  800350:	89 e5                	mov    %esp,%ebp
  800352:	57                   	push   %edi
  800353:	56                   	push   %esi
  800354:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800355:	be 00 00 00 00       	mov    $0x0,%esi
  80035a:	b8 0d 00 00 00       	mov    $0xd,%eax
  80035f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800362:	8b 55 08             	mov    0x8(%ebp),%edx
  800365:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800368:	8b 7d 14             	mov    0x14(%ebp),%edi
  80036b:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  80036d:	5b                   	pop    %ebx
  80036e:	5e                   	pop    %esi
  80036f:	5f                   	pop    %edi
  800370:	5d                   	pop    %ebp
  800371:	c3                   	ret    

00800372 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800372:	55                   	push   %ebp
  800373:	89 e5                	mov    %esp,%ebp
  800375:	57                   	push   %edi
  800376:	56                   	push   %esi
  800377:	53                   	push   %ebx
  800378:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80037b:	b9 00 00 00 00       	mov    $0x0,%ecx
  800380:	b8 0e 00 00 00       	mov    $0xe,%eax
  800385:	8b 55 08             	mov    0x8(%ebp),%edx
  800388:	89 cb                	mov    %ecx,%ebx
  80038a:	89 cf                	mov    %ecx,%edi
  80038c:	89 ce                	mov    %ecx,%esi
  80038e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800390:	85 c0                	test   %eax,%eax
  800392:	7e 17                	jle    8003ab <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800394:	83 ec 0c             	sub    $0xc,%esp
  800397:	50                   	push   %eax
  800398:	6a 0e                	push   $0xe
  80039a:	68 93 1e 80 00       	push   $0x801e93
  80039f:	6a 23                	push   $0x23
  8003a1:	68 b0 1e 80 00       	push   $0x801eb0
  8003a6:	e8 19 01 00 00       	call   8004c4 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  8003ab:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8003ae:	5b                   	pop    %ebx
  8003af:	5e                   	pop    %esi
  8003b0:	5f                   	pop    %edi
  8003b1:	5d                   	pop    %ebp
  8003b2:	c3                   	ret    

008003b3 <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  8003b3:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  8003b7:	7e 38                	jle    8003f1 <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  8003b9:	55                   	push   %ebp
  8003ba:	89 e5                	mov    %esp,%ebp
  8003bc:	53                   	push   %ebx
  8003bd:	83 ec 08             	sub    $0x8,%esp
  8003c0:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  8003c2:	ff 70 04             	pushl  0x4(%eax)
  8003c5:	8d 40 10             	lea    0x10(%eax),%eax
  8003c8:	50                   	push   %eax
  8003c9:	ff 33                	pushl  (%ebx)
  8003cb:	e8 04 0e 00 00       	call   8011d4 <write>
		if (result > 0)
  8003d0:	83 c4 10             	add    $0x10,%esp
  8003d3:	85 c0                	test   %eax,%eax
  8003d5:	7e 03                	jle    8003da <writebuf+0x27>
			b->result += result;
  8003d7:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  8003da:	3b 43 04             	cmp    0x4(%ebx),%eax
  8003dd:	74 0e                	je     8003ed <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  8003df:	89 c2                	mov    %eax,%edx
  8003e1:	85 c0                	test   %eax,%eax
  8003e3:	7e 05                	jle    8003ea <writebuf+0x37>
  8003e5:	ba 00 00 00 00       	mov    $0x0,%edx
  8003ea:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  8003ed:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003f0:	c9                   	leave  
  8003f1:	c3                   	ret    

008003f2 <putch>:

static void
putch(int ch, void *thunk)
{
  8003f2:	55                   	push   %ebp
  8003f3:	89 e5                	mov    %esp,%ebp
  8003f5:	53                   	push   %ebx
  8003f6:	83 ec 04             	sub    $0x4,%esp
  8003f9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  8003fc:	8b 53 04             	mov    0x4(%ebx),%edx
  8003ff:	8d 42 01             	lea    0x1(%edx),%eax
  800402:	89 43 04             	mov    %eax,0x4(%ebx)
  800405:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800408:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  80040c:	3d 00 01 00 00       	cmp    $0x100,%eax
  800411:	75 0e                	jne    800421 <putch+0x2f>
		writebuf(b);
  800413:	89 d8                	mov    %ebx,%eax
  800415:	e8 99 ff ff ff       	call   8003b3 <writebuf>
		b->idx = 0;
  80041a:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  800421:	83 c4 04             	add    $0x4,%esp
  800424:	5b                   	pop    %ebx
  800425:	5d                   	pop    %ebp
  800426:	c3                   	ret    

00800427 <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  800427:	55                   	push   %ebp
  800428:	89 e5                	mov    %esp,%ebp
  80042a:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  800430:	8b 45 08             	mov    0x8(%ebp),%eax
  800433:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  800439:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  800440:	00 00 00 
	b.result = 0;
  800443:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80044a:	00 00 00 
	b.error = 1;
  80044d:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  800454:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  800457:	ff 75 10             	pushl  0x10(%ebp)
  80045a:	ff 75 0c             	pushl  0xc(%ebp)
  80045d:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  800463:	50                   	push   %eax
  800464:	68 f2 03 80 00       	push   $0x8003f2
  800469:	e8 62 02 00 00       	call   8006d0 <vprintfmt>
	if (b.idx > 0)
  80046e:	83 c4 10             	add    $0x10,%esp
  800471:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  800478:	7e 0b                	jle    800485 <vfprintf+0x5e>
		writebuf(&b);
  80047a:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  800480:	e8 2e ff ff ff       	call   8003b3 <writebuf>

	return (b.result ? b.result : b.error);
  800485:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  80048b:	85 c0                	test   %eax,%eax
  80048d:	75 06                	jne    800495 <vfprintf+0x6e>
  80048f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  800495:	c9                   	leave  
  800496:	c3                   	ret    

00800497 <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  800497:	55                   	push   %ebp
  800498:	89 e5                	mov    %esp,%ebp
  80049a:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80049d:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  8004a0:	50                   	push   %eax
  8004a1:	ff 75 0c             	pushl  0xc(%ebp)
  8004a4:	ff 75 08             	pushl  0x8(%ebp)
  8004a7:	e8 7b ff ff ff       	call   800427 <vfprintf>
	va_end(ap);

	return cnt;
}
  8004ac:	c9                   	leave  
  8004ad:	c3                   	ret    

008004ae <printf>:

int
printf(const char *fmt, ...)
{
  8004ae:	55                   	push   %ebp
  8004af:	89 e5                	mov    %esp,%ebp
  8004b1:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8004b4:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  8004b7:	50                   	push   %eax
  8004b8:	ff 75 08             	pushl  0x8(%ebp)
  8004bb:	6a 01                	push   $0x1
  8004bd:	e8 65 ff ff ff       	call   800427 <vfprintf>
	va_end(ap);

	return cnt;
}
  8004c2:	c9                   	leave  
  8004c3:	c3                   	ret    

008004c4 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8004c4:	55                   	push   %ebp
  8004c5:	89 e5                	mov    %esp,%ebp
  8004c7:	56                   	push   %esi
  8004c8:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8004c9:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8004cc:	8b 35 00 30 80 00    	mov    0x803000,%esi
  8004d2:	e8 8e fc ff ff       	call   800165 <sys_getenvid>
  8004d7:	83 ec 0c             	sub    $0xc,%esp
  8004da:	ff 75 0c             	pushl  0xc(%ebp)
  8004dd:	ff 75 08             	pushl  0x8(%ebp)
  8004e0:	56                   	push   %esi
  8004e1:	50                   	push   %eax
  8004e2:	68 c0 1e 80 00       	push   $0x801ec0
  8004e7:	e8 b0 00 00 00       	call   80059c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8004ec:	83 c4 18             	add    $0x18,%esp
  8004ef:	53                   	push   %ebx
  8004f0:	ff 75 10             	pushl  0x10(%ebp)
  8004f3:	e8 53 00 00 00       	call   80054b <vcprintf>
	cprintf("\n");
  8004f8:	c7 04 24 bb 22 80 00 	movl   $0x8022bb,(%esp)
  8004ff:	e8 98 00 00 00       	call   80059c <cprintf>
  800504:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800507:	cc                   	int3   
  800508:	eb fd                	jmp    800507 <_panic+0x43>

0080050a <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80050a:	55                   	push   %ebp
  80050b:	89 e5                	mov    %esp,%ebp
  80050d:	53                   	push   %ebx
  80050e:	83 ec 04             	sub    $0x4,%esp
  800511:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800514:	8b 13                	mov    (%ebx),%edx
  800516:	8d 42 01             	lea    0x1(%edx),%eax
  800519:	89 03                	mov    %eax,(%ebx)
  80051b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80051e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800522:	3d ff 00 00 00       	cmp    $0xff,%eax
  800527:	75 1a                	jne    800543 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800529:	83 ec 08             	sub    $0x8,%esp
  80052c:	68 ff 00 00 00       	push   $0xff
  800531:	8d 43 08             	lea    0x8(%ebx),%eax
  800534:	50                   	push   %eax
  800535:	e8 ad fb ff ff       	call   8000e7 <sys_cputs>
		b->idx = 0;
  80053a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800540:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800543:	ff 43 04             	incl   0x4(%ebx)
}
  800546:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800549:	c9                   	leave  
  80054a:	c3                   	ret    

0080054b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80054b:	55                   	push   %ebp
  80054c:	89 e5                	mov    %esp,%ebp
  80054e:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800554:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80055b:	00 00 00 
	b.cnt = 0;
  80055e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800565:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800568:	ff 75 0c             	pushl  0xc(%ebp)
  80056b:	ff 75 08             	pushl  0x8(%ebp)
  80056e:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800574:	50                   	push   %eax
  800575:	68 0a 05 80 00       	push   $0x80050a
  80057a:	e8 51 01 00 00       	call   8006d0 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80057f:	83 c4 08             	add    $0x8,%esp
  800582:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800588:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80058e:	50                   	push   %eax
  80058f:	e8 53 fb ff ff       	call   8000e7 <sys_cputs>

	return b.cnt;
}
  800594:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80059a:	c9                   	leave  
  80059b:	c3                   	ret    

0080059c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80059c:	55                   	push   %ebp
  80059d:	89 e5                	mov    %esp,%ebp
  80059f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8005a2:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8005a5:	50                   	push   %eax
  8005a6:	ff 75 08             	pushl  0x8(%ebp)
  8005a9:	e8 9d ff ff ff       	call   80054b <vcprintf>
	va_end(ap);

	return cnt;
}
  8005ae:	c9                   	leave  
  8005af:	c3                   	ret    

008005b0 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8005b0:	55                   	push   %ebp
  8005b1:	89 e5                	mov    %esp,%ebp
  8005b3:	57                   	push   %edi
  8005b4:	56                   	push   %esi
  8005b5:	53                   	push   %ebx
  8005b6:	83 ec 1c             	sub    $0x1c,%esp
  8005b9:	89 c7                	mov    %eax,%edi
  8005bb:	89 d6                	mov    %edx,%esi
  8005bd:	8b 45 08             	mov    0x8(%ebp),%eax
  8005c0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8005c3:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005c6:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8005c9:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8005cc:	bb 00 00 00 00       	mov    $0x0,%ebx
  8005d1:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8005d4:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8005d7:	39 d3                	cmp    %edx,%ebx
  8005d9:	72 05                	jb     8005e0 <printnum+0x30>
  8005db:	39 45 10             	cmp    %eax,0x10(%ebp)
  8005de:	77 45                	ja     800625 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8005e0:	83 ec 0c             	sub    $0xc,%esp
  8005e3:	ff 75 18             	pushl  0x18(%ebp)
  8005e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e9:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8005ec:	53                   	push   %ebx
  8005ed:	ff 75 10             	pushl  0x10(%ebp)
  8005f0:	83 ec 08             	sub    $0x8,%esp
  8005f3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005f6:	ff 75 e0             	pushl  -0x20(%ebp)
  8005f9:	ff 75 dc             	pushl  -0x24(%ebp)
  8005fc:	ff 75 d8             	pushl  -0x28(%ebp)
  8005ff:	e8 08 16 00 00       	call   801c0c <__udivdi3>
  800604:	83 c4 18             	add    $0x18,%esp
  800607:	52                   	push   %edx
  800608:	50                   	push   %eax
  800609:	89 f2                	mov    %esi,%edx
  80060b:	89 f8                	mov    %edi,%eax
  80060d:	e8 9e ff ff ff       	call   8005b0 <printnum>
  800612:	83 c4 20             	add    $0x20,%esp
  800615:	eb 16                	jmp    80062d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800617:	83 ec 08             	sub    $0x8,%esp
  80061a:	56                   	push   %esi
  80061b:	ff 75 18             	pushl  0x18(%ebp)
  80061e:	ff d7                	call   *%edi
  800620:	83 c4 10             	add    $0x10,%esp
  800623:	eb 03                	jmp    800628 <printnum+0x78>
  800625:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800628:	4b                   	dec    %ebx
  800629:	85 db                	test   %ebx,%ebx
  80062b:	7f ea                	jg     800617 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80062d:	83 ec 08             	sub    $0x8,%esp
  800630:	56                   	push   %esi
  800631:	83 ec 04             	sub    $0x4,%esp
  800634:	ff 75 e4             	pushl  -0x1c(%ebp)
  800637:	ff 75 e0             	pushl  -0x20(%ebp)
  80063a:	ff 75 dc             	pushl  -0x24(%ebp)
  80063d:	ff 75 d8             	pushl  -0x28(%ebp)
  800640:	e8 d7 16 00 00       	call   801d1c <__umoddi3>
  800645:	83 c4 14             	add    $0x14,%esp
  800648:	0f be 80 e3 1e 80 00 	movsbl 0x801ee3(%eax),%eax
  80064f:	50                   	push   %eax
  800650:	ff d7                	call   *%edi
}
  800652:	83 c4 10             	add    $0x10,%esp
  800655:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800658:	5b                   	pop    %ebx
  800659:	5e                   	pop    %esi
  80065a:	5f                   	pop    %edi
  80065b:	5d                   	pop    %ebp
  80065c:	c3                   	ret    

0080065d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80065d:	55                   	push   %ebp
  80065e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800660:	83 fa 01             	cmp    $0x1,%edx
  800663:	7e 0e                	jle    800673 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800665:	8b 10                	mov    (%eax),%edx
  800667:	8d 4a 08             	lea    0x8(%edx),%ecx
  80066a:	89 08                	mov    %ecx,(%eax)
  80066c:	8b 02                	mov    (%edx),%eax
  80066e:	8b 52 04             	mov    0x4(%edx),%edx
  800671:	eb 22                	jmp    800695 <getuint+0x38>
	else if (lflag)
  800673:	85 d2                	test   %edx,%edx
  800675:	74 10                	je     800687 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800677:	8b 10                	mov    (%eax),%edx
  800679:	8d 4a 04             	lea    0x4(%edx),%ecx
  80067c:	89 08                	mov    %ecx,(%eax)
  80067e:	8b 02                	mov    (%edx),%eax
  800680:	ba 00 00 00 00       	mov    $0x0,%edx
  800685:	eb 0e                	jmp    800695 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800687:	8b 10                	mov    (%eax),%edx
  800689:	8d 4a 04             	lea    0x4(%edx),%ecx
  80068c:	89 08                	mov    %ecx,(%eax)
  80068e:	8b 02                	mov    (%edx),%eax
  800690:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800695:	5d                   	pop    %ebp
  800696:	c3                   	ret    

00800697 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800697:	55                   	push   %ebp
  800698:	89 e5                	mov    %esp,%ebp
  80069a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80069d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8006a0:	8b 10                	mov    (%eax),%edx
  8006a2:	3b 50 04             	cmp    0x4(%eax),%edx
  8006a5:	73 0a                	jae    8006b1 <sprintputch+0x1a>
		*b->buf++ = ch;
  8006a7:	8d 4a 01             	lea    0x1(%edx),%ecx
  8006aa:	89 08                	mov    %ecx,(%eax)
  8006ac:	8b 45 08             	mov    0x8(%ebp),%eax
  8006af:	88 02                	mov    %al,(%edx)
}
  8006b1:	5d                   	pop    %ebp
  8006b2:	c3                   	ret    

008006b3 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8006b3:	55                   	push   %ebp
  8006b4:	89 e5                	mov    %esp,%ebp
  8006b6:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8006b9:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8006bc:	50                   	push   %eax
  8006bd:	ff 75 10             	pushl  0x10(%ebp)
  8006c0:	ff 75 0c             	pushl  0xc(%ebp)
  8006c3:	ff 75 08             	pushl  0x8(%ebp)
  8006c6:	e8 05 00 00 00       	call   8006d0 <vprintfmt>
	va_end(ap);
}
  8006cb:	83 c4 10             	add    $0x10,%esp
  8006ce:	c9                   	leave  
  8006cf:	c3                   	ret    

008006d0 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8006d0:	55                   	push   %ebp
  8006d1:	89 e5                	mov    %esp,%ebp
  8006d3:	57                   	push   %edi
  8006d4:	56                   	push   %esi
  8006d5:	53                   	push   %ebx
  8006d6:	83 ec 2c             	sub    $0x2c,%esp
  8006d9:	8b 75 08             	mov    0x8(%ebp),%esi
  8006dc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8006df:	8b 7d 10             	mov    0x10(%ebp),%edi
  8006e2:	eb 12                	jmp    8006f6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8006e4:	85 c0                	test   %eax,%eax
  8006e6:	0f 84 68 03 00 00    	je     800a54 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8006ec:	83 ec 08             	sub    $0x8,%esp
  8006ef:	53                   	push   %ebx
  8006f0:	50                   	push   %eax
  8006f1:	ff d6                	call   *%esi
  8006f3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8006f6:	47                   	inc    %edi
  8006f7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8006fb:	83 f8 25             	cmp    $0x25,%eax
  8006fe:	75 e4                	jne    8006e4 <vprintfmt+0x14>
  800700:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800704:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80070b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800712:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800719:	ba 00 00 00 00       	mov    $0x0,%edx
  80071e:	eb 07                	jmp    800727 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800720:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800723:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800727:	8d 47 01             	lea    0x1(%edi),%eax
  80072a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80072d:	0f b6 0f             	movzbl (%edi),%ecx
  800730:	8a 07                	mov    (%edi),%al
  800732:	83 e8 23             	sub    $0x23,%eax
  800735:	3c 55                	cmp    $0x55,%al
  800737:	0f 87 fe 02 00 00    	ja     800a3b <vprintfmt+0x36b>
  80073d:	0f b6 c0             	movzbl %al,%eax
  800740:	ff 24 85 20 20 80 00 	jmp    *0x802020(,%eax,4)
  800747:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80074a:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80074e:	eb d7                	jmp    800727 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800750:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800753:	b8 00 00 00 00       	mov    $0x0,%eax
  800758:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80075b:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80075e:	01 c0                	add    %eax,%eax
  800760:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800764:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800767:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80076a:	83 fa 09             	cmp    $0x9,%edx
  80076d:	77 34                	ja     8007a3 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80076f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800770:	eb e9                	jmp    80075b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800772:	8b 45 14             	mov    0x14(%ebp),%eax
  800775:	8d 48 04             	lea    0x4(%eax),%ecx
  800778:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80077b:	8b 00                	mov    (%eax),%eax
  80077d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800780:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800783:	eb 24                	jmp    8007a9 <vprintfmt+0xd9>
  800785:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800789:	79 07                	jns    800792 <vprintfmt+0xc2>
  80078b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800792:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800795:	eb 90                	jmp    800727 <vprintfmt+0x57>
  800797:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80079a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8007a1:	eb 84                	jmp    800727 <vprintfmt+0x57>
  8007a3:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8007a6:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8007a9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8007ad:	0f 89 74 ff ff ff    	jns    800727 <vprintfmt+0x57>
				width = precision, precision = -1;
  8007b3:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8007b6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8007b9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8007c0:	e9 62 ff ff ff       	jmp    800727 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8007c5:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007c6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8007c9:	e9 59 ff ff ff       	jmp    800727 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8007ce:	8b 45 14             	mov    0x14(%ebp),%eax
  8007d1:	8d 50 04             	lea    0x4(%eax),%edx
  8007d4:	89 55 14             	mov    %edx,0x14(%ebp)
  8007d7:	83 ec 08             	sub    $0x8,%esp
  8007da:	53                   	push   %ebx
  8007db:	ff 30                	pushl  (%eax)
  8007dd:	ff d6                	call   *%esi
			break;
  8007df:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8007e5:	e9 0c ff ff ff       	jmp    8006f6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8007ea:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ed:	8d 50 04             	lea    0x4(%eax),%edx
  8007f0:	89 55 14             	mov    %edx,0x14(%ebp)
  8007f3:	8b 00                	mov    (%eax),%eax
  8007f5:	85 c0                	test   %eax,%eax
  8007f7:	79 02                	jns    8007fb <vprintfmt+0x12b>
  8007f9:	f7 d8                	neg    %eax
  8007fb:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8007fd:	83 f8 0f             	cmp    $0xf,%eax
  800800:	7f 0b                	jg     80080d <vprintfmt+0x13d>
  800802:	8b 04 85 80 21 80 00 	mov    0x802180(,%eax,4),%eax
  800809:	85 c0                	test   %eax,%eax
  80080b:	75 18                	jne    800825 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80080d:	52                   	push   %edx
  80080e:	68 fb 1e 80 00       	push   $0x801efb
  800813:	53                   	push   %ebx
  800814:	56                   	push   %esi
  800815:	e8 99 fe ff ff       	call   8006b3 <printfmt>
  80081a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80081d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800820:	e9 d1 fe ff ff       	jmp    8006f6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800825:	50                   	push   %eax
  800826:	68 89 22 80 00       	push   $0x802289
  80082b:	53                   	push   %ebx
  80082c:	56                   	push   %esi
  80082d:	e8 81 fe ff ff       	call   8006b3 <printfmt>
  800832:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800835:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800838:	e9 b9 fe ff ff       	jmp    8006f6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80083d:	8b 45 14             	mov    0x14(%ebp),%eax
  800840:	8d 50 04             	lea    0x4(%eax),%edx
  800843:	89 55 14             	mov    %edx,0x14(%ebp)
  800846:	8b 38                	mov    (%eax),%edi
  800848:	85 ff                	test   %edi,%edi
  80084a:	75 05                	jne    800851 <vprintfmt+0x181>
				p = "(null)";
  80084c:	bf f4 1e 80 00       	mov    $0x801ef4,%edi
			if (width > 0 && padc != '-')
  800851:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800855:	0f 8e 90 00 00 00    	jle    8008eb <vprintfmt+0x21b>
  80085b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80085f:	0f 84 8e 00 00 00    	je     8008f3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800865:	83 ec 08             	sub    $0x8,%esp
  800868:	ff 75 d0             	pushl  -0x30(%ebp)
  80086b:	57                   	push   %edi
  80086c:	e8 70 02 00 00       	call   800ae1 <strnlen>
  800871:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800874:	29 c1                	sub    %eax,%ecx
  800876:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800879:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80087c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800880:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800883:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800886:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800888:	eb 0d                	jmp    800897 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80088a:	83 ec 08             	sub    $0x8,%esp
  80088d:	53                   	push   %ebx
  80088e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800891:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800893:	4f                   	dec    %edi
  800894:	83 c4 10             	add    $0x10,%esp
  800897:	85 ff                	test   %edi,%edi
  800899:	7f ef                	jg     80088a <vprintfmt+0x1ba>
  80089b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80089e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8008a1:	89 c8                	mov    %ecx,%eax
  8008a3:	85 c9                	test   %ecx,%ecx
  8008a5:	79 05                	jns    8008ac <vprintfmt+0x1dc>
  8008a7:	b8 00 00 00 00       	mov    $0x0,%eax
  8008ac:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8008af:	29 c1                	sub    %eax,%ecx
  8008b1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8008b4:	89 75 08             	mov    %esi,0x8(%ebp)
  8008b7:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8008ba:	eb 3d                	jmp    8008f9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8008bc:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8008c0:	74 19                	je     8008db <vprintfmt+0x20b>
  8008c2:	0f be c0             	movsbl %al,%eax
  8008c5:	83 e8 20             	sub    $0x20,%eax
  8008c8:	83 f8 5e             	cmp    $0x5e,%eax
  8008cb:	76 0e                	jbe    8008db <vprintfmt+0x20b>
					putch('?', putdat);
  8008cd:	83 ec 08             	sub    $0x8,%esp
  8008d0:	53                   	push   %ebx
  8008d1:	6a 3f                	push   $0x3f
  8008d3:	ff 55 08             	call   *0x8(%ebp)
  8008d6:	83 c4 10             	add    $0x10,%esp
  8008d9:	eb 0b                	jmp    8008e6 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8008db:	83 ec 08             	sub    $0x8,%esp
  8008de:	53                   	push   %ebx
  8008df:	52                   	push   %edx
  8008e0:	ff 55 08             	call   *0x8(%ebp)
  8008e3:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8008e6:	ff 4d e4             	decl   -0x1c(%ebp)
  8008e9:	eb 0e                	jmp    8008f9 <vprintfmt+0x229>
  8008eb:	89 75 08             	mov    %esi,0x8(%ebp)
  8008ee:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8008f1:	eb 06                	jmp    8008f9 <vprintfmt+0x229>
  8008f3:	89 75 08             	mov    %esi,0x8(%ebp)
  8008f6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8008f9:	47                   	inc    %edi
  8008fa:	8a 47 ff             	mov    -0x1(%edi),%al
  8008fd:	0f be d0             	movsbl %al,%edx
  800900:	85 d2                	test   %edx,%edx
  800902:	74 1d                	je     800921 <vprintfmt+0x251>
  800904:	85 f6                	test   %esi,%esi
  800906:	78 b4                	js     8008bc <vprintfmt+0x1ec>
  800908:	4e                   	dec    %esi
  800909:	79 b1                	jns    8008bc <vprintfmt+0x1ec>
  80090b:	8b 75 08             	mov    0x8(%ebp),%esi
  80090e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800911:	eb 14                	jmp    800927 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800913:	83 ec 08             	sub    $0x8,%esp
  800916:	53                   	push   %ebx
  800917:	6a 20                	push   $0x20
  800919:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80091b:	4f                   	dec    %edi
  80091c:	83 c4 10             	add    $0x10,%esp
  80091f:	eb 06                	jmp    800927 <vprintfmt+0x257>
  800921:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800924:	8b 75 08             	mov    0x8(%ebp),%esi
  800927:	85 ff                	test   %edi,%edi
  800929:	7f e8                	jg     800913 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80092b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80092e:	e9 c3 fd ff ff       	jmp    8006f6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800933:	83 fa 01             	cmp    $0x1,%edx
  800936:	7e 16                	jle    80094e <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800938:	8b 45 14             	mov    0x14(%ebp),%eax
  80093b:	8d 50 08             	lea    0x8(%eax),%edx
  80093e:	89 55 14             	mov    %edx,0x14(%ebp)
  800941:	8b 50 04             	mov    0x4(%eax),%edx
  800944:	8b 00                	mov    (%eax),%eax
  800946:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800949:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80094c:	eb 32                	jmp    800980 <vprintfmt+0x2b0>
	else if (lflag)
  80094e:	85 d2                	test   %edx,%edx
  800950:	74 18                	je     80096a <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800952:	8b 45 14             	mov    0x14(%ebp),%eax
  800955:	8d 50 04             	lea    0x4(%eax),%edx
  800958:	89 55 14             	mov    %edx,0x14(%ebp)
  80095b:	8b 00                	mov    (%eax),%eax
  80095d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800960:	89 c1                	mov    %eax,%ecx
  800962:	c1 f9 1f             	sar    $0x1f,%ecx
  800965:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800968:	eb 16                	jmp    800980 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80096a:	8b 45 14             	mov    0x14(%ebp),%eax
  80096d:	8d 50 04             	lea    0x4(%eax),%edx
  800970:	89 55 14             	mov    %edx,0x14(%ebp)
  800973:	8b 00                	mov    (%eax),%eax
  800975:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800978:	89 c1                	mov    %eax,%ecx
  80097a:	c1 f9 1f             	sar    $0x1f,%ecx
  80097d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800980:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800983:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800986:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80098a:	79 76                	jns    800a02 <vprintfmt+0x332>
				putch('-', putdat);
  80098c:	83 ec 08             	sub    $0x8,%esp
  80098f:	53                   	push   %ebx
  800990:	6a 2d                	push   $0x2d
  800992:	ff d6                	call   *%esi
				num = -(long long) num;
  800994:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800997:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80099a:	f7 d8                	neg    %eax
  80099c:	83 d2 00             	adc    $0x0,%edx
  80099f:	f7 da                	neg    %edx
  8009a1:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8009a4:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8009a9:	eb 5c                	jmp    800a07 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8009ab:	8d 45 14             	lea    0x14(%ebp),%eax
  8009ae:	e8 aa fc ff ff       	call   80065d <getuint>
			base = 10;
  8009b3:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8009b8:	eb 4d                	jmp    800a07 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8009ba:	8d 45 14             	lea    0x14(%ebp),%eax
  8009bd:	e8 9b fc ff ff       	call   80065d <getuint>
			base = 8;
  8009c2:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8009c7:	eb 3e                	jmp    800a07 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8009c9:	83 ec 08             	sub    $0x8,%esp
  8009cc:	53                   	push   %ebx
  8009cd:	6a 30                	push   $0x30
  8009cf:	ff d6                	call   *%esi
			putch('x', putdat);
  8009d1:	83 c4 08             	add    $0x8,%esp
  8009d4:	53                   	push   %ebx
  8009d5:	6a 78                	push   $0x78
  8009d7:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8009d9:	8b 45 14             	mov    0x14(%ebp),%eax
  8009dc:	8d 50 04             	lea    0x4(%eax),%edx
  8009df:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8009e2:	8b 00                	mov    (%eax),%eax
  8009e4:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8009e9:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8009ec:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8009f1:	eb 14                	jmp    800a07 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8009f3:	8d 45 14             	lea    0x14(%ebp),%eax
  8009f6:	e8 62 fc ff ff       	call   80065d <getuint>
			base = 16;
  8009fb:	b9 10 00 00 00       	mov    $0x10,%ecx
  800a00:	eb 05                	jmp    800a07 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800a02:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800a07:	83 ec 0c             	sub    $0xc,%esp
  800a0a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800a0e:	57                   	push   %edi
  800a0f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800a12:	51                   	push   %ecx
  800a13:	52                   	push   %edx
  800a14:	50                   	push   %eax
  800a15:	89 da                	mov    %ebx,%edx
  800a17:	89 f0                	mov    %esi,%eax
  800a19:	e8 92 fb ff ff       	call   8005b0 <printnum>
			break;
  800a1e:	83 c4 20             	add    $0x20,%esp
  800a21:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800a24:	e9 cd fc ff ff       	jmp    8006f6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800a29:	83 ec 08             	sub    $0x8,%esp
  800a2c:	53                   	push   %ebx
  800a2d:	51                   	push   %ecx
  800a2e:	ff d6                	call   *%esi
			break;
  800a30:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800a33:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800a36:	e9 bb fc ff ff       	jmp    8006f6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800a3b:	83 ec 08             	sub    $0x8,%esp
  800a3e:	53                   	push   %ebx
  800a3f:	6a 25                	push   $0x25
  800a41:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800a43:	83 c4 10             	add    $0x10,%esp
  800a46:	eb 01                	jmp    800a49 <vprintfmt+0x379>
  800a48:	4f                   	dec    %edi
  800a49:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800a4d:	75 f9                	jne    800a48 <vprintfmt+0x378>
  800a4f:	e9 a2 fc ff ff       	jmp    8006f6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800a54:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a57:	5b                   	pop    %ebx
  800a58:	5e                   	pop    %esi
  800a59:	5f                   	pop    %edi
  800a5a:	5d                   	pop    %ebp
  800a5b:	c3                   	ret    

00800a5c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800a5c:	55                   	push   %ebp
  800a5d:	89 e5                	mov    %esp,%ebp
  800a5f:	83 ec 18             	sub    $0x18,%esp
  800a62:	8b 45 08             	mov    0x8(%ebp),%eax
  800a65:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800a68:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800a6b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800a6f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800a72:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800a79:	85 c0                	test   %eax,%eax
  800a7b:	74 26                	je     800aa3 <vsnprintf+0x47>
  800a7d:	85 d2                	test   %edx,%edx
  800a7f:	7e 29                	jle    800aaa <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800a81:	ff 75 14             	pushl  0x14(%ebp)
  800a84:	ff 75 10             	pushl  0x10(%ebp)
  800a87:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800a8a:	50                   	push   %eax
  800a8b:	68 97 06 80 00       	push   $0x800697
  800a90:	e8 3b fc ff ff       	call   8006d0 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800a95:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800a98:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800a9b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800a9e:	83 c4 10             	add    $0x10,%esp
  800aa1:	eb 0c                	jmp    800aaf <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800aa3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800aa8:	eb 05                	jmp    800aaf <vsnprintf+0x53>
  800aaa:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800aaf:	c9                   	leave  
  800ab0:	c3                   	ret    

00800ab1 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800ab1:	55                   	push   %ebp
  800ab2:	89 e5                	mov    %esp,%ebp
  800ab4:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800ab7:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800aba:	50                   	push   %eax
  800abb:	ff 75 10             	pushl  0x10(%ebp)
  800abe:	ff 75 0c             	pushl  0xc(%ebp)
  800ac1:	ff 75 08             	pushl  0x8(%ebp)
  800ac4:	e8 93 ff ff ff       	call   800a5c <vsnprintf>
	va_end(ap);

	return rc;
}
  800ac9:	c9                   	leave  
  800aca:	c3                   	ret    

00800acb <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800acb:	55                   	push   %ebp
  800acc:	89 e5                	mov    %esp,%ebp
  800ace:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800ad1:	b8 00 00 00 00       	mov    $0x0,%eax
  800ad6:	eb 01                	jmp    800ad9 <strlen+0xe>
		n++;
  800ad8:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800ad9:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800add:	75 f9                	jne    800ad8 <strlen+0xd>
		n++;
	return n;
}
  800adf:	5d                   	pop    %ebp
  800ae0:	c3                   	ret    

00800ae1 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800ae1:	55                   	push   %ebp
  800ae2:	89 e5                	mov    %esp,%ebp
  800ae4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800ae7:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800aea:	ba 00 00 00 00       	mov    $0x0,%edx
  800aef:	eb 01                	jmp    800af2 <strnlen+0x11>
		n++;
  800af1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800af2:	39 c2                	cmp    %eax,%edx
  800af4:	74 08                	je     800afe <strnlen+0x1d>
  800af6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800afa:	75 f5                	jne    800af1 <strnlen+0x10>
  800afc:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800afe:	5d                   	pop    %ebp
  800aff:	c3                   	ret    

00800b00 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800b00:	55                   	push   %ebp
  800b01:	89 e5                	mov    %esp,%ebp
  800b03:	53                   	push   %ebx
  800b04:	8b 45 08             	mov    0x8(%ebp),%eax
  800b07:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800b0a:	89 c2                	mov    %eax,%edx
  800b0c:	42                   	inc    %edx
  800b0d:	41                   	inc    %ecx
  800b0e:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800b11:	88 5a ff             	mov    %bl,-0x1(%edx)
  800b14:	84 db                	test   %bl,%bl
  800b16:	75 f4                	jne    800b0c <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800b18:	5b                   	pop    %ebx
  800b19:	5d                   	pop    %ebp
  800b1a:	c3                   	ret    

00800b1b <strcat>:

char *
strcat(char *dst, const char *src)
{
  800b1b:	55                   	push   %ebp
  800b1c:	89 e5                	mov    %esp,%ebp
  800b1e:	53                   	push   %ebx
  800b1f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800b22:	53                   	push   %ebx
  800b23:	e8 a3 ff ff ff       	call   800acb <strlen>
  800b28:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800b2b:	ff 75 0c             	pushl  0xc(%ebp)
  800b2e:	01 d8                	add    %ebx,%eax
  800b30:	50                   	push   %eax
  800b31:	e8 ca ff ff ff       	call   800b00 <strcpy>
	return dst;
}
  800b36:	89 d8                	mov    %ebx,%eax
  800b38:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800b3b:	c9                   	leave  
  800b3c:	c3                   	ret    

00800b3d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800b3d:	55                   	push   %ebp
  800b3e:	89 e5                	mov    %esp,%ebp
  800b40:	56                   	push   %esi
  800b41:	53                   	push   %ebx
  800b42:	8b 75 08             	mov    0x8(%ebp),%esi
  800b45:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b48:	89 f3                	mov    %esi,%ebx
  800b4a:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800b4d:	89 f2                	mov    %esi,%edx
  800b4f:	eb 0c                	jmp    800b5d <strncpy+0x20>
		*dst++ = *src;
  800b51:	42                   	inc    %edx
  800b52:	8a 01                	mov    (%ecx),%al
  800b54:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800b57:	80 39 01             	cmpb   $0x1,(%ecx)
  800b5a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800b5d:	39 da                	cmp    %ebx,%edx
  800b5f:	75 f0                	jne    800b51 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800b61:	89 f0                	mov    %esi,%eax
  800b63:	5b                   	pop    %ebx
  800b64:	5e                   	pop    %esi
  800b65:	5d                   	pop    %ebp
  800b66:	c3                   	ret    

00800b67 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800b67:	55                   	push   %ebp
  800b68:	89 e5                	mov    %esp,%ebp
  800b6a:	56                   	push   %esi
  800b6b:	53                   	push   %ebx
  800b6c:	8b 75 08             	mov    0x8(%ebp),%esi
  800b6f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b72:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800b75:	85 c0                	test   %eax,%eax
  800b77:	74 1e                	je     800b97 <strlcpy+0x30>
  800b79:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800b7d:	89 f2                	mov    %esi,%edx
  800b7f:	eb 05                	jmp    800b86 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800b81:	42                   	inc    %edx
  800b82:	41                   	inc    %ecx
  800b83:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800b86:	39 c2                	cmp    %eax,%edx
  800b88:	74 08                	je     800b92 <strlcpy+0x2b>
  800b8a:	8a 19                	mov    (%ecx),%bl
  800b8c:	84 db                	test   %bl,%bl
  800b8e:	75 f1                	jne    800b81 <strlcpy+0x1a>
  800b90:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800b92:	c6 00 00             	movb   $0x0,(%eax)
  800b95:	eb 02                	jmp    800b99 <strlcpy+0x32>
  800b97:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800b99:	29 f0                	sub    %esi,%eax
}
  800b9b:	5b                   	pop    %ebx
  800b9c:	5e                   	pop    %esi
  800b9d:	5d                   	pop    %ebp
  800b9e:	c3                   	ret    

00800b9f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800b9f:	55                   	push   %ebp
  800ba0:	89 e5                	mov    %esp,%ebp
  800ba2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800ba5:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800ba8:	eb 02                	jmp    800bac <strcmp+0xd>
		p++, q++;
  800baa:	41                   	inc    %ecx
  800bab:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800bac:	8a 01                	mov    (%ecx),%al
  800bae:	84 c0                	test   %al,%al
  800bb0:	74 04                	je     800bb6 <strcmp+0x17>
  800bb2:	3a 02                	cmp    (%edx),%al
  800bb4:	74 f4                	je     800baa <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800bb6:	0f b6 c0             	movzbl %al,%eax
  800bb9:	0f b6 12             	movzbl (%edx),%edx
  800bbc:	29 d0                	sub    %edx,%eax
}
  800bbe:	5d                   	pop    %ebp
  800bbf:	c3                   	ret    

00800bc0 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800bc0:	55                   	push   %ebp
  800bc1:	89 e5                	mov    %esp,%ebp
  800bc3:	53                   	push   %ebx
  800bc4:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc7:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bca:	89 c3                	mov    %eax,%ebx
  800bcc:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800bcf:	eb 02                	jmp    800bd3 <strncmp+0x13>
		n--, p++, q++;
  800bd1:	40                   	inc    %eax
  800bd2:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800bd3:	39 d8                	cmp    %ebx,%eax
  800bd5:	74 14                	je     800beb <strncmp+0x2b>
  800bd7:	8a 08                	mov    (%eax),%cl
  800bd9:	84 c9                	test   %cl,%cl
  800bdb:	74 04                	je     800be1 <strncmp+0x21>
  800bdd:	3a 0a                	cmp    (%edx),%cl
  800bdf:	74 f0                	je     800bd1 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800be1:	0f b6 00             	movzbl (%eax),%eax
  800be4:	0f b6 12             	movzbl (%edx),%edx
  800be7:	29 d0                	sub    %edx,%eax
  800be9:	eb 05                	jmp    800bf0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800beb:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800bf0:	5b                   	pop    %ebx
  800bf1:	5d                   	pop    %ebp
  800bf2:	c3                   	ret    

00800bf3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800bf3:	55                   	push   %ebp
  800bf4:	89 e5                	mov    %esp,%ebp
  800bf6:	8b 45 08             	mov    0x8(%ebp),%eax
  800bf9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800bfc:	eb 05                	jmp    800c03 <strchr+0x10>
		if (*s == c)
  800bfe:	38 ca                	cmp    %cl,%dl
  800c00:	74 0c                	je     800c0e <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800c02:	40                   	inc    %eax
  800c03:	8a 10                	mov    (%eax),%dl
  800c05:	84 d2                	test   %dl,%dl
  800c07:	75 f5                	jne    800bfe <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800c09:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c0e:	5d                   	pop    %ebp
  800c0f:	c3                   	ret    

00800c10 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800c10:	55                   	push   %ebp
  800c11:	89 e5                	mov    %esp,%ebp
  800c13:	8b 45 08             	mov    0x8(%ebp),%eax
  800c16:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800c19:	eb 05                	jmp    800c20 <strfind+0x10>
		if (*s == c)
  800c1b:	38 ca                	cmp    %cl,%dl
  800c1d:	74 07                	je     800c26 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800c1f:	40                   	inc    %eax
  800c20:	8a 10                	mov    (%eax),%dl
  800c22:	84 d2                	test   %dl,%dl
  800c24:	75 f5                	jne    800c1b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800c26:	5d                   	pop    %ebp
  800c27:	c3                   	ret    

00800c28 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800c28:	55                   	push   %ebp
  800c29:	89 e5                	mov    %esp,%ebp
  800c2b:	57                   	push   %edi
  800c2c:	56                   	push   %esi
  800c2d:	53                   	push   %ebx
  800c2e:	8b 7d 08             	mov    0x8(%ebp),%edi
  800c31:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800c34:	85 c9                	test   %ecx,%ecx
  800c36:	74 36                	je     800c6e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800c38:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800c3e:	75 28                	jne    800c68 <memset+0x40>
  800c40:	f6 c1 03             	test   $0x3,%cl
  800c43:	75 23                	jne    800c68 <memset+0x40>
		c &= 0xFF;
  800c45:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800c49:	89 d3                	mov    %edx,%ebx
  800c4b:	c1 e3 08             	shl    $0x8,%ebx
  800c4e:	89 d6                	mov    %edx,%esi
  800c50:	c1 e6 18             	shl    $0x18,%esi
  800c53:	89 d0                	mov    %edx,%eax
  800c55:	c1 e0 10             	shl    $0x10,%eax
  800c58:	09 f0                	or     %esi,%eax
  800c5a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800c5c:	89 d8                	mov    %ebx,%eax
  800c5e:	09 d0                	or     %edx,%eax
  800c60:	c1 e9 02             	shr    $0x2,%ecx
  800c63:	fc                   	cld    
  800c64:	f3 ab                	rep stos %eax,%es:(%edi)
  800c66:	eb 06                	jmp    800c6e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800c68:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c6b:	fc                   	cld    
  800c6c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800c6e:	89 f8                	mov    %edi,%eax
  800c70:	5b                   	pop    %ebx
  800c71:	5e                   	pop    %esi
  800c72:	5f                   	pop    %edi
  800c73:	5d                   	pop    %ebp
  800c74:	c3                   	ret    

00800c75 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800c75:	55                   	push   %ebp
  800c76:	89 e5                	mov    %esp,%ebp
  800c78:	57                   	push   %edi
  800c79:	56                   	push   %esi
  800c7a:	8b 45 08             	mov    0x8(%ebp),%eax
  800c7d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c80:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800c83:	39 c6                	cmp    %eax,%esi
  800c85:	73 33                	jae    800cba <memmove+0x45>
  800c87:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800c8a:	39 d0                	cmp    %edx,%eax
  800c8c:	73 2c                	jae    800cba <memmove+0x45>
		s += n;
		d += n;
  800c8e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800c91:	89 d6                	mov    %edx,%esi
  800c93:	09 fe                	or     %edi,%esi
  800c95:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800c9b:	75 13                	jne    800cb0 <memmove+0x3b>
  800c9d:	f6 c1 03             	test   $0x3,%cl
  800ca0:	75 0e                	jne    800cb0 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800ca2:	83 ef 04             	sub    $0x4,%edi
  800ca5:	8d 72 fc             	lea    -0x4(%edx),%esi
  800ca8:	c1 e9 02             	shr    $0x2,%ecx
  800cab:	fd                   	std    
  800cac:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800cae:	eb 07                	jmp    800cb7 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800cb0:	4f                   	dec    %edi
  800cb1:	8d 72 ff             	lea    -0x1(%edx),%esi
  800cb4:	fd                   	std    
  800cb5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800cb7:	fc                   	cld    
  800cb8:	eb 1d                	jmp    800cd7 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800cba:	89 f2                	mov    %esi,%edx
  800cbc:	09 c2                	or     %eax,%edx
  800cbe:	f6 c2 03             	test   $0x3,%dl
  800cc1:	75 0f                	jne    800cd2 <memmove+0x5d>
  800cc3:	f6 c1 03             	test   $0x3,%cl
  800cc6:	75 0a                	jne    800cd2 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800cc8:	c1 e9 02             	shr    $0x2,%ecx
  800ccb:	89 c7                	mov    %eax,%edi
  800ccd:	fc                   	cld    
  800cce:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800cd0:	eb 05                	jmp    800cd7 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800cd2:	89 c7                	mov    %eax,%edi
  800cd4:	fc                   	cld    
  800cd5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800cd7:	5e                   	pop    %esi
  800cd8:	5f                   	pop    %edi
  800cd9:	5d                   	pop    %ebp
  800cda:	c3                   	ret    

00800cdb <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800cdb:	55                   	push   %ebp
  800cdc:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800cde:	ff 75 10             	pushl  0x10(%ebp)
  800ce1:	ff 75 0c             	pushl  0xc(%ebp)
  800ce4:	ff 75 08             	pushl  0x8(%ebp)
  800ce7:	e8 89 ff ff ff       	call   800c75 <memmove>
}
  800cec:	c9                   	leave  
  800ced:	c3                   	ret    

00800cee <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800cee:	55                   	push   %ebp
  800cef:	89 e5                	mov    %esp,%ebp
  800cf1:	56                   	push   %esi
  800cf2:	53                   	push   %ebx
  800cf3:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf6:	8b 55 0c             	mov    0xc(%ebp),%edx
  800cf9:	89 c6                	mov    %eax,%esi
  800cfb:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800cfe:	eb 14                	jmp    800d14 <memcmp+0x26>
		if (*s1 != *s2)
  800d00:	8a 08                	mov    (%eax),%cl
  800d02:	8a 1a                	mov    (%edx),%bl
  800d04:	38 d9                	cmp    %bl,%cl
  800d06:	74 0a                	je     800d12 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800d08:	0f b6 c1             	movzbl %cl,%eax
  800d0b:	0f b6 db             	movzbl %bl,%ebx
  800d0e:	29 d8                	sub    %ebx,%eax
  800d10:	eb 0b                	jmp    800d1d <memcmp+0x2f>
		s1++, s2++;
  800d12:	40                   	inc    %eax
  800d13:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800d14:	39 f0                	cmp    %esi,%eax
  800d16:	75 e8                	jne    800d00 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800d18:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800d1d:	5b                   	pop    %ebx
  800d1e:	5e                   	pop    %esi
  800d1f:	5d                   	pop    %ebp
  800d20:	c3                   	ret    

00800d21 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800d21:	55                   	push   %ebp
  800d22:	89 e5                	mov    %esp,%ebp
  800d24:	53                   	push   %ebx
  800d25:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800d28:	89 c1                	mov    %eax,%ecx
  800d2a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800d2d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800d31:	eb 08                	jmp    800d3b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800d33:	0f b6 10             	movzbl (%eax),%edx
  800d36:	39 da                	cmp    %ebx,%edx
  800d38:	74 05                	je     800d3f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800d3a:	40                   	inc    %eax
  800d3b:	39 c8                	cmp    %ecx,%eax
  800d3d:	72 f4                	jb     800d33 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800d3f:	5b                   	pop    %ebx
  800d40:	5d                   	pop    %ebp
  800d41:	c3                   	ret    

00800d42 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800d42:	55                   	push   %ebp
  800d43:	89 e5                	mov    %esp,%ebp
  800d45:	57                   	push   %edi
  800d46:	56                   	push   %esi
  800d47:	53                   	push   %ebx
  800d48:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d4b:	eb 01                	jmp    800d4e <strtol+0xc>
		s++;
  800d4d:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d4e:	8a 01                	mov    (%ecx),%al
  800d50:	3c 20                	cmp    $0x20,%al
  800d52:	74 f9                	je     800d4d <strtol+0xb>
  800d54:	3c 09                	cmp    $0x9,%al
  800d56:	74 f5                	je     800d4d <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800d58:	3c 2b                	cmp    $0x2b,%al
  800d5a:	75 08                	jne    800d64 <strtol+0x22>
		s++;
  800d5c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800d5d:	bf 00 00 00 00       	mov    $0x0,%edi
  800d62:	eb 11                	jmp    800d75 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800d64:	3c 2d                	cmp    $0x2d,%al
  800d66:	75 08                	jne    800d70 <strtol+0x2e>
		s++, neg = 1;
  800d68:	41                   	inc    %ecx
  800d69:	bf 01 00 00 00       	mov    $0x1,%edi
  800d6e:	eb 05                	jmp    800d75 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800d70:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800d75:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d79:	0f 84 87 00 00 00    	je     800e06 <strtol+0xc4>
  800d7f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800d83:	75 27                	jne    800dac <strtol+0x6a>
  800d85:	80 39 30             	cmpb   $0x30,(%ecx)
  800d88:	75 22                	jne    800dac <strtol+0x6a>
  800d8a:	e9 88 00 00 00       	jmp    800e17 <strtol+0xd5>
		s += 2, base = 16;
  800d8f:	83 c1 02             	add    $0x2,%ecx
  800d92:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800d99:	eb 11                	jmp    800dac <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800d9b:	41                   	inc    %ecx
  800d9c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800da3:	eb 07                	jmp    800dac <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800da5:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800dac:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800db1:	8a 11                	mov    (%ecx),%dl
  800db3:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800db6:	80 fb 09             	cmp    $0x9,%bl
  800db9:	77 08                	ja     800dc3 <strtol+0x81>
			dig = *s - '0';
  800dbb:	0f be d2             	movsbl %dl,%edx
  800dbe:	83 ea 30             	sub    $0x30,%edx
  800dc1:	eb 22                	jmp    800de5 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800dc3:	8d 72 9f             	lea    -0x61(%edx),%esi
  800dc6:	89 f3                	mov    %esi,%ebx
  800dc8:	80 fb 19             	cmp    $0x19,%bl
  800dcb:	77 08                	ja     800dd5 <strtol+0x93>
			dig = *s - 'a' + 10;
  800dcd:	0f be d2             	movsbl %dl,%edx
  800dd0:	83 ea 57             	sub    $0x57,%edx
  800dd3:	eb 10                	jmp    800de5 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800dd5:	8d 72 bf             	lea    -0x41(%edx),%esi
  800dd8:	89 f3                	mov    %esi,%ebx
  800dda:	80 fb 19             	cmp    $0x19,%bl
  800ddd:	77 14                	ja     800df3 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800ddf:	0f be d2             	movsbl %dl,%edx
  800de2:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800de5:	3b 55 10             	cmp    0x10(%ebp),%edx
  800de8:	7d 09                	jge    800df3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800dea:	41                   	inc    %ecx
  800deb:	0f af 45 10          	imul   0x10(%ebp),%eax
  800def:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800df1:	eb be                	jmp    800db1 <strtol+0x6f>

	if (endptr)
  800df3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800df7:	74 05                	je     800dfe <strtol+0xbc>
		*endptr = (char *) s;
  800df9:	8b 75 0c             	mov    0xc(%ebp),%esi
  800dfc:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800dfe:	85 ff                	test   %edi,%edi
  800e00:	74 21                	je     800e23 <strtol+0xe1>
  800e02:	f7 d8                	neg    %eax
  800e04:	eb 1d                	jmp    800e23 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800e06:	80 39 30             	cmpb   $0x30,(%ecx)
  800e09:	75 9a                	jne    800da5 <strtol+0x63>
  800e0b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800e0f:	0f 84 7a ff ff ff    	je     800d8f <strtol+0x4d>
  800e15:	eb 84                	jmp    800d9b <strtol+0x59>
  800e17:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800e1b:	0f 84 6e ff ff ff    	je     800d8f <strtol+0x4d>
  800e21:	eb 89                	jmp    800dac <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800e23:	5b                   	pop    %ebx
  800e24:	5e                   	pop    %esi
  800e25:	5f                   	pop    %edi
  800e26:	5d                   	pop    %ebp
  800e27:	c3                   	ret    

00800e28 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800e28:	55                   	push   %ebp
  800e29:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  800e2b:	8b 45 08             	mov    0x8(%ebp),%eax
  800e2e:	05 00 00 00 30       	add    $0x30000000,%eax
  800e33:	c1 e8 0c             	shr    $0xc,%eax
}
  800e36:	5d                   	pop    %ebp
  800e37:	c3                   	ret    

00800e38 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  800e38:	55                   	push   %ebp
  800e39:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  800e3b:	8b 45 08             	mov    0x8(%ebp),%eax
  800e3e:	05 00 00 00 30       	add    $0x30000000,%eax
  800e43:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800e48:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  800e4d:	5d                   	pop    %ebp
  800e4e:	c3                   	ret    

00800e4f <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  800e4f:	55                   	push   %ebp
  800e50:	89 e5                	mov    %esp,%ebp
  800e52:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800e55:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  800e5a:	89 c2                	mov    %eax,%edx
  800e5c:	c1 ea 16             	shr    $0x16,%edx
  800e5f:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800e66:	f6 c2 01             	test   $0x1,%dl
  800e69:	74 11                	je     800e7c <fd_alloc+0x2d>
  800e6b:	89 c2                	mov    %eax,%edx
  800e6d:	c1 ea 0c             	shr    $0xc,%edx
  800e70:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800e77:	f6 c2 01             	test   $0x1,%dl
  800e7a:	75 09                	jne    800e85 <fd_alloc+0x36>
			*fd_store = fd;
  800e7c:	89 01                	mov    %eax,(%ecx)
			return 0;
  800e7e:	b8 00 00 00 00       	mov    $0x0,%eax
  800e83:	eb 17                	jmp    800e9c <fd_alloc+0x4d>
  800e85:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  800e8a:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  800e8f:	75 c9                	jne    800e5a <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  800e91:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  800e97:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  800e9c:	5d                   	pop    %ebp
  800e9d:	c3                   	ret    

00800e9e <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  800e9e:	55                   	push   %ebp
  800e9f:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  800ea1:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  800ea5:	77 39                	ja     800ee0 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  800ea7:	8b 45 08             	mov    0x8(%ebp),%eax
  800eaa:	c1 e0 0c             	shl    $0xc,%eax
  800ead:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  800eb2:	89 c2                	mov    %eax,%edx
  800eb4:	c1 ea 16             	shr    $0x16,%edx
  800eb7:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800ebe:	f6 c2 01             	test   $0x1,%dl
  800ec1:	74 24                	je     800ee7 <fd_lookup+0x49>
  800ec3:	89 c2                	mov    %eax,%edx
  800ec5:	c1 ea 0c             	shr    $0xc,%edx
  800ec8:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800ecf:	f6 c2 01             	test   $0x1,%dl
  800ed2:	74 1a                	je     800eee <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  800ed4:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ed7:	89 02                	mov    %eax,(%edx)
	return 0;
  800ed9:	b8 00 00 00 00       	mov    $0x0,%eax
  800ede:	eb 13                	jmp    800ef3 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800ee0:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800ee5:	eb 0c                	jmp    800ef3 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800ee7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800eec:	eb 05                	jmp    800ef3 <fd_lookup+0x55>
  800eee:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  800ef3:	5d                   	pop    %ebp
  800ef4:	c3                   	ret    

00800ef5 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  800ef5:	55                   	push   %ebp
  800ef6:	89 e5                	mov    %esp,%ebp
  800ef8:	83 ec 08             	sub    $0x8,%esp
  800efb:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800efe:	ba 60 22 80 00       	mov    $0x802260,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  800f03:	eb 13                	jmp    800f18 <dev_lookup+0x23>
  800f05:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  800f08:	39 08                	cmp    %ecx,(%eax)
  800f0a:	75 0c                	jne    800f18 <dev_lookup+0x23>
			*dev = devtab[i];
  800f0c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f0f:	89 01                	mov    %eax,(%ecx)
			return 0;
  800f11:	b8 00 00 00 00       	mov    $0x0,%eax
  800f16:	eb 2e                	jmp    800f46 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  800f18:	8b 02                	mov    (%edx),%eax
  800f1a:	85 c0                	test   %eax,%eax
  800f1c:	75 e7                	jne    800f05 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  800f1e:	a1 04 40 80 00       	mov    0x804004,%eax
  800f23:	8b 40 48             	mov    0x48(%eax),%eax
  800f26:	83 ec 04             	sub    $0x4,%esp
  800f29:	51                   	push   %ecx
  800f2a:	50                   	push   %eax
  800f2b:	68 e0 21 80 00       	push   $0x8021e0
  800f30:	e8 67 f6 ff ff       	call   80059c <cprintf>
	*dev = 0;
  800f35:	8b 45 0c             	mov    0xc(%ebp),%eax
  800f38:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  800f3e:	83 c4 10             	add    $0x10,%esp
  800f41:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  800f46:	c9                   	leave  
  800f47:	c3                   	ret    

00800f48 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  800f48:	55                   	push   %ebp
  800f49:	89 e5                	mov    %esp,%ebp
  800f4b:	56                   	push   %esi
  800f4c:	53                   	push   %ebx
  800f4d:	83 ec 10             	sub    $0x10,%esp
  800f50:	8b 75 08             	mov    0x8(%ebp),%esi
  800f53:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  800f56:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800f59:	50                   	push   %eax
  800f5a:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  800f60:	c1 e8 0c             	shr    $0xc,%eax
  800f63:	50                   	push   %eax
  800f64:	e8 35 ff ff ff       	call   800e9e <fd_lookup>
  800f69:	83 c4 08             	add    $0x8,%esp
  800f6c:	85 c0                	test   %eax,%eax
  800f6e:	78 05                	js     800f75 <fd_close+0x2d>
	    || fd != fd2)
  800f70:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  800f73:	74 06                	je     800f7b <fd_close+0x33>
		return (must_exist ? r : 0);
  800f75:	84 db                	test   %bl,%bl
  800f77:	74 47                	je     800fc0 <fd_close+0x78>
  800f79:	eb 4a                	jmp    800fc5 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  800f7b:	83 ec 08             	sub    $0x8,%esp
  800f7e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800f81:	50                   	push   %eax
  800f82:	ff 36                	pushl  (%esi)
  800f84:	e8 6c ff ff ff       	call   800ef5 <dev_lookup>
  800f89:	89 c3                	mov    %eax,%ebx
  800f8b:	83 c4 10             	add    $0x10,%esp
  800f8e:	85 c0                	test   %eax,%eax
  800f90:	78 1c                	js     800fae <fd_close+0x66>
		if (dev->dev_close)
  800f92:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800f95:	8b 40 10             	mov    0x10(%eax),%eax
  800f98:	85 c0                	test   %eax,%eax
  800f9a:	74 0d                	je     800fa9 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  800f9c:	83 ec 0c             	sub    $0xc,%esp
  800f9f:	56                   	push   %esi
  800fa0:	ff d0                	call   *%eax
  800fa2:	89 c3                	mov    %eax,%ebx
  800fa4:	83 c4 10             	add    $0x10,%esp
  800fa7:	eb 05                	jmp    800fae <fd_close+0x66>
		else
			r = 0;
  800fa9:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  800fae:	83 ec 08             	sub    $0x8,%esp
  800fb1:	56                   	push   %esi
  800fb2:	6a 00                	push   $0x0
  800fb4:	e8 6f f2 ff ff       	call   800228 <sys_page_unmap>
	return r;
  800fb9:	83 c4 10             	add    $0x10,%esp
  800fbc:	89 d8                	mov    %ebx,%eax
  800fbe:	eb 05                	jmp    800fc5 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  800fc0:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  800fc5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800fc8:	5b                   	pop    %ebx
  800fc9:	5e                   	pop    %esi
  800fca:	5d                   	pop    %ebp
  800fcb:	c3                   	ret    

00800fcc <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  800fcc:	55                   	push   %ebp
  800fcd:	89 e5                	mov    %esp,%ebp
  800fcf:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800fd2:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800fd5:	50                   	push   %eax
  800fd6:	ff 75 08             	pushl  0x8(%ebp)
  800fd9:	e8 c0 fe ff ff       	call   800e9e <fd_lookup>
  800fde:	83 c4 08             	add    $0x8,%esp
  800fe1:	85 c0                	test   %eax,%eax
  800fe3:	78 10                	js     800ff5 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  800fe5:	83 ec 08             	sub    $0x8,%esp
  800fe8:	6a 01                	push   $0x1
  800fea:	ff 75 f4             	pushl  -0xc(%ebp)
  800fed:	e8 56 ff ff ff       	call   800f48 <fd_close>
  800ff2:	83 c4 10             	add    $0x10,%esp
}
  800ff5:	c9                   	leave  
  800ff6:	c3                   	ret    

00800ff7 <close_all>:

void
close_all(void)
{
  800ff7:	55                   	push   %ebp
  800ff8:	89 e5                	mov    %esp,%ebp
  800ffa:	53                   	push   %ebx
  800ffb:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  800ffe:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801003:	83 ec 0c             	sub    $0xc,%esp
  801006:	53                   	push   %ebx
  801007:	e8 c0 ff ff ff       	call   800fcc <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  80100c:	43                   	inc    %ebx
  80100d:	83 c4 10             	add    $0x10,%esp
  801010:	83 fb 20             	cmp    $0x20,%ebx
  801013:	75 ee                	jne    801003 <close_all+0xc>
		close(i);
}
  801015:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801018:	c9                   	leave  
  801019:	c3                   	ret    

0080101a <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  80101a:	55                   	push   %ebp
  80101b:	89 e5                	mov    %esp,%ebp
  80101d:	57                   	push   %edi
  80101e:	56                   	push   %esi
  80101f:	53                   	push   %ebx
  801020:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801023:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801026:	50                   	push   %eax
  801027:	ff 75 08             	pushl  0x8(%ebp)
  80102a:	e8 6f fe ff ff       	call   800e9e <fd_lookup>
  80102f:	83 c4 08             	add    $0x8,%esp
  801032:	85 c0                	test   %eax,%eax
  801034:	0f 88 c2 00 00 00    	js     8010fc <dup+0xe2>
		return r;
	close(newfdnum);
  80103a:	83 ec 0c             	sub    $0xc,%esp
  80103d:	ff 75 0c             	pushl  0xc(%ebp)
  801040:	e8 87 ff ff ff       	call   800fcc <close>

	newfd = INDEX2FD(newfdnum);
  801045:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801048:	c1 e3 0c             	shl    $0xc,%ebx
  80104b:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801051:	83 c4 04             	add    $0x4,%esp
  801054:	ff 75 e4             	pushl  -0x1c(%ebp)
  801057:	e8 dc fd ff ff       	call   800e38 <fd2data>
  80105c:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  80105e:	89 1c 24             	mov    %ebx,(%esp)
  801061:	e8 d2 fd ff ff       	call   800e38 <fd2data>
  801066:	83 c4 10             	add    $0x10,%esp
  801069:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  80106b:	89 f0                	mov    %esi,%eax
  80106d:	c1 e8 16             	shr    $0x16,%eax
  801070:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801077:	a8 01                	test   $0x1,%al
  801079:	74 35                	je     8010b0 <dup+0x96>
  80107b:	89 f0                	mov    %esi,%eax
  80107d:	c1 e8 0c             	shr    $0xc,%eax
  801080:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801087:	f6 c2 01             	test   $0x1,%dl
  80108a:	74 24                	je     8010b0 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  80108c:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801093:	83 ec 0c             	sub    $0xc,%esp
  801096:	25 07 0e 00 00       	and    $0xe07,%eax
  80109b:	50                   	push   %eax
  80109c:	57                   	push   %edi
  80109d:	6a 00                	push   $0x0
  80109f:	56                   	push   %esi
  8010a0:	6a 00                	push   $0x0
  8010a2:	e8 3f f1 ff ff       	call   8001e6 <sys_page_map>
  8010a7:	89 c6                	mov    %eax,%esi
  8010a9:	83 c4 20             	add    $0x20,%esp
  8010ac:	85 c0                	test   %eax,%eax
  8010ae:	78 2c                	js     8010dc <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8010b0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8010b3:	89 d0                	mov    %edx,%eax
  8010b5:	c1 e8 0c             	shr    $0xc,%eax
  8010b8:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8010bf:	83 ec 0c             	sub    $0xc,%esp
  8010c2:	25 07 0e 00 00       	and    $0xe07,%eax
  8010c7:	50                   	push   %eax
  8010c8:	53                   	push   %ebx
  8010c9:	6a 00                	push   $0x0
  8010cb:	52                   	push   %edx
  8010cc:	6a 00                	push   $0x0
  8010ce:	e8 13 f1 ff ff       	call   8001e6 <sys_page_map>
  8010d3:	89 c6                	mov    %eax,%esi
  8010d5:	83 c4 20             	add    $0x20,%esp
  8010d8:	85 c0                	test   %eax,%eax
  8010da:	79 1d                	jns    8010f9 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8010dc:	83 ec 08             	sub    $0x8,%esp
  8010df:	53                   	push   %ebx
  8010e0:	6a 00                	push   $0x0
  8010e2:	e8 41 f1 ff ff       	call   800228 <sys_page_unmap>
	sys_page_unmap(0, nva);
  8010e7:	83 c4 08             	add    $0x8,%esp
  8010ea:	57                   	push   %edi
  8010eb:	6a 00                	push   $0x0
  8010ed:	e8 36 f1 ff ff       	call   800228 <sys_page_unmap>
	return r;
  8010f2:	83 c4 10             	add    $0x10,%esp
  8010f5:	89 f0                	mov    %esi,%eax
  8010f7:	eb 03                	jmp    8010fc <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8010f9:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8010fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010ff:	5b                   	pop    %ebx
  801100:	5e                   	pop    %esi
  801101:	5f                   	pop    %edi
  801102:	5d                   	pop    %ebp
  801103:	c3                   	ret    

00801104 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801104:	55                   	push   %ebp
  801105:	89 e5                	mov    %esp,%ebp
  801107:	53                   	push   %ebx
  801108:	83 ec 14             	sub    $0x14,%esp
  80110b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80110e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801111:	50                   	push   %eax
  801112:	53                   	push   %ebx
  801113:	e8 86 fd ff ff       	call   800e9e <fd_lookup>
  801118:	83 c4 08             	add    $0x8,%esp
  80111b:	85 c0                	test   %eax,%eax
  80111d:	78 67                	js     801186 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80111f:	83 ec 08             	sub    $0x8,%esp
  801122:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801125:	50                   	push   %eax
  801126:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801129:	ff 30                	pushl  (%eax)
  80112b:	e8 c5 fd ff ff       	call   800ef5 <dev_lookup>
  801130:	83 c4 10             	add    $0x10,%esp
  801133:	85 c0                	test   %eax,%eax
  801135:	78 4f                	js     801186 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801137:	8b 55 f0             	mov    -0x10(%ebp),%edx
  80113a:	8b 42 08             	mov    0x8(%edx),%eax
  80113d:	83 e0 03             	and    $0x3,%eax
  801140:	83 f8 01             	cmp    $0x1,%eax
  801143:	75 21                	jne    801166 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801145:	a1 04 40 80 00       	mov    0x804004,%eax
  80114a:	8b 40 48             	mov    0x48(%eax),%eax
  80114d:	83 ec 04             	sub    $0x4,%esp
  801150:	53                   	push   %ebx
  801151:	50                   	push   %eax
  801152:	68 24 22 80 00       	push   $0x802224
  801157:	e8 40 f4 ff ff       	call   80059c <cprintf>
		return -E_INVAL;
  80115c:	83 c4 10             	add    $0x10,%esp
  80115f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801164:	eb 20                	jmp    801186 <read+0x82>
	}
	if (!dev->dev_read)
  801166:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801169:	8b 40 08             	mov    0x8(%eax),%eax
  80116c:	85 c0                	test   %eax,%eax
  80116e:	74 11                	je     801181 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801170:	83 ec 04             	sub    $0x4,%esp
  801173:	ff 75 10             	pushl  0x10(%ebp)
  801176:	ff 75 0c             	pushl  0xc(%ebp)
  801179:	52                   	push   %edx
  80117a:	ff d0                	call   *%eax
  80117c:	83 c4 10             	add    $0x10,%esp
  80117f:	eb 05                	jmp    801186 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801181:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801186:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801189:	c9                   	leave  
  80118a:	c3                   	ret    

0080118b <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  80118b:	55                   	push   %ebp
  80118c:	89 e5                	mov    %esp,%ebp
  80118e:	57                   	push   %edi
  80118f:	56                   	push   %esi
  801190:	53                   	push   %ebx
  801191:	83 ec 0c             	sub    $0xc,%esp
  801194:	8b 7d 08             	mov    0x8(%ebp),%edi
  801197:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80119a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80119f:	eb 21                	jmp    8011c2 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  8011a1:	83 ec 04             	sub    $0x4,%esp
  8011a4:	89 f0                	mov    %esi,%eax
  8011a6:	29 d8                	sub    %ebx,%eax
  8011a8:	50                   	push   %eax
  8011a9:	89 d8                	mov    %ebx,%eax
  8011ab:	03 45 0c             	add    0xc(%ebp),%eax
  8011ae:	50                   	push   %eax
  8011af:	57                   	push   %edi
  8011b0:	e8 4f ff ff ff       	call   801104 <read>
		if (m < 0)
  8011b5:	83 c4 10             	add    $0x10,%esp
  8011b8:	85 c0                	test   %eax,%eax
  8011ba:	78 10                	js     8011cc <readn+0x41>
			return m;
		if (m == 0)
  8011bc:	85 c0                	test   %eax,%eax
  8011be:	74 0a                	je     8011ca <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8011c0:	01 c3                	add    %eax,%ebx
  8011c2:	39 f3                	cmp    %esi,%ebx
  8011c4:	72 db                	jb     8011a1 <readn+0x16>
  8011c6:	89 d8                	mov    %ebx,%eax
  8011c8:	eb 02                	jmp    8011cc <readn+0x41>
  8011ca:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  8011cc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8011cf:	5b                   	pop    %ebx
  8011d0:	5e                   	pop    %esi
  8011d1:	5f                   	pop    %edi
  8011d2:	5d                   	pop    %ebp
  8011d3:	c3                   	ret    

008011d4 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8011d4:	55                   	push   %ebp
  8011d5:	89 e5                	mov    %esp,%ebp
  8011d7:	53                   	push   %ebx
  8011d8:	83 ec 14             	sub    $0x14,%esp
  8011db:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8011de:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8011e1:	50                   	push   %eax
  8011e2:	53                   	push   %ebx
  8011e3:	e8 b6 fc ff ff       	call   800e9e <fd_lookup>
  8011e8:	83 c4 08             	add    $0x8,%esp
  8011eb:	85 c0                	test   %eax,%eax
  8011ed:	78 62                	js     801251 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8011ef:	83 ec 08             	sub    $0x8,%esp
  8011f2:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8011f5:	50                   	push   %eax
  8011f6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011f9:	ff 30                	pushl  (%eax)
  8011fb:	e8 f5 fc ff ff       	call   800ef5 <dev_lookup>
  801200:	83 c4 10             	add    $0x10,%esp
  801203:	85 c0                	test   %eax,%eax
  801205:	78 4a                	js     801251 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801207:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80120a:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80120e:	75 21                	jne    801231 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801210:	a1 04 40 80 00       	mov    0x804004,%eax
  801215:	8b 40 48             	mov    0x48(%eax),%eax
  801218:	83 ec 04             	sub    $0x4,%esp
  80121b:	53                   	push   %ebx
  80121c:	50                   	push   %eax
  80121d:	68 40 22 80 00       	push   $0x802240
  801222:	e8 75 f3 ff ff       	call   80059c <cprintf>
		return -E_INVAL;
  801227:	83 c4 10             	add    $0x10,%esp
  80122a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80122f:	eb 20                	jmp    801251 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801231:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801234:	8b 52 0c             	mov    0xc(%edx),%edx
  801237:	85 d2                	test   %edx,%edx
  801239:	74 11                	je     80124c <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  80123b:	83 ec 04             	sub    $0x4,%esp
  80123e:	ff 75 10             	pushl  0x10(%ebp)
  801241:	ff 75 0c             	pushl  0xc(%ebp)
  801244:	50                   	push   %eax
  801245:	ff d2                	call   *%edx
  801247:	83 c4 10             	add    $0x10,%esp
  80124a:	eb 05                	jmp    801251 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  80124c:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801251:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801254:	c9                   	leave  
  801255:	c3                   	ret    

00801256 <seek>:

int
seek(int fdnum, off_t offset)
{
  801256:	55                   	push   %ebp
  801257:	89 e5                	mov    %esp,%ebp
  801259:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80125c:	8d 45 fc             	lea    -0x4(%ebp),%eax
  80125f:	50                   	push   %eax
  801260:	ff 75 08             	pushl  0x8(%ebp)
  801263:	e8 36 fc ff ff       	call   800e9e <fd_lookup>
  801268:	83 c4 08             	add    $0x8,%esp
  80126b:	85 c0                	test   %eax,%eax
  80126d:	78 0e                	js     80127d <seek+0x27>
		return r;
	fd->fd_offset = offset;
  80126f:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801272:	8b 55 0c             	mov    0xc(%ebp),%edx
  801275:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801278:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80127d:	c9                   	leave  
  80127e:	c3                   	ret    

0080127f <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  80127f:	55                   	push   %ebp
  801280:	89 e5                	mov    %esp,%ebp
  801282:	53                   	push   %ebx
  801283:	83 ec 14             	sub    $0x14,%esp
  801286:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801289:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80128c:	50                   	push   %eax
  80128d:	53                   	push   %ebx
  80128e:	e8 0b fc ff ff       	call   800e9e <fd_lookup>
  801293:	83 c4 08             	add    $0x8,%esp
  801296:	85 c0                	test   %eax,%eax
  801298:	78 5f                	js     8012f9 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80129a:	83 ec 08             	sub    $0x8,%esp
  80129d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012a0:	50                   	push   %eax
  8012a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8012a4:	ff 30                	pushl  (%eax)
  8012a6:	e8 4a fc ff ff       	call   800ef5 <dev_lookup>
  8012ab:	83 c4 10             	add    $0x10,%esp
  8012ae:	85 c0                	test   %eax,%eax
  8012b0:	78 47                	js     8012f9 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8012b2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8012b5:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8012b9:	75 21                	jne    8012dc <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  8012bb:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  8012c0:	8b 40 48             	mov    0x48(%eax),%eax
  8012c3:	83 ec 04             	sub    $0x4,%esp
  8012c6:	53                   	push   %ebx
  8012c7:	50                   	push   %eax
  8012c8:	68 00 22 80 00       	push   $0x802200
  8012cd:	e8 ca f2 ff ff       	call   80059c <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8012d2:	83 c4 10             	add    $0x10,%esp
  8012d5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8012da:	eb 1d                	jmp    8012f9 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8012dc:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8012df:	8b 52 18             	mov    0x18(%edx),%edx
  8012e2:	85 d2                	test   %edx,%edx
  8012e4:	74 0e                	je     8012f4 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8012e6:	83 ec 08             	sub    $0x8,%esp
  8012e9:	ff 75 0c             	pushl  0xc(%ebp)
  8012ec:	50                   	push   %eax
  8012ed:	ff d2                	call   *%edx
  8012ef:	83 c4 10             	add    $0x10,%esp
  8012f2:	eb 05                	jmp    8012f9 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8012f4:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8012f9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8012fc:	c9                   	leave  
  8012fd:	c3                   	ret    

008012fe <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8012fe:	55                   	push   %ebp
  8012ff:	89 e5                	mov    %esp,%ebp
  801301:	53                   	push   %ebx
  801302:	83 ec 14             	sub    $0x14,%esp
  801305:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801308:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80130b:	50                   	push   %eax
  80130c:	ff 75 08             	pushl  0x8(%ebp)
  80130f:	e8 8a fb ff ff       	call   800e9e <fd_lookup>
  801314:	83 c4 08             	add    $0x8,%esp
  801317:	85 c0                	test   %eax,%eax
  801319:	78 52                	js     80136d <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80131b:	83 ec 08             	sub    $0x8,%esp
  80131e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801321:	50                   	push   %eax
  801322:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801325:	ff 30                	pushl  (%eax)
  801327:	e8 c9 fb ff ff       	call   800ef5 <dev_lookup>
  80132c:	83 c4 10             	add    $0x10,%esp
  80132f:	85 c0                	test   %eax,%eax
  801331:	78 3a                	js     80136d <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801333:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801336:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  80133a:	74 2c                	je     801368 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  80133c:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  80133f:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801346:	00 00 00 
	stat->st_isdir = 0;
  801349:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801350:	00 00 00 
	stat->st_dev = dev;
  801353:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  801359:	83 ec 08             	sub    $0x8,%esp
  80135c:	53                   	push   %ebx
  80135d:	ff 75 f0             	pushl  -0x10(%ebp)
  801360:	ff 50 14             	call   *0x14(%eax)
  801363:	83 c4 10             	add    $0x10,%esp
  801366:	eb 05                	jmp    80136d <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801368:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  80136d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801370:	c9                   	leave  
  801371:	c3                   	ret    

00801372 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801372:	55                   	push   %ebp
  801373:	89 e5                	mov    %esp,%ebp
  801375:	56                   	push   %esi
  801376:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801377:	83 ec 08             	sub    $0x8,%esp
  80137a:	6a 00                	push   $0x0
  80137c:	ff 75 08             	pushl  0x8(%ebp)
  80137f:	e8 e7 01 00 00       	call   80156b <open>
  801384:	89 c3                	mov    %eax,%ebx
  801386:	83 c4 10             	add    $0x10,%esp
  801389:	85 c0                	test   %eax,%eax
  80138b:	78 1d                	js     8013aa <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  80138d:	83 ec 08             	sub    $0x8,%esp
  801390:	ff 75 0c             	pushl  0xc(%ebp)
  801393:	50                   	push   %eax
  801394:	e8 65 ff ff ff       	call   8012fe <fstat>
  801399:	89 c6                	mov    %eax,%esi
	close(fd);
  80139b:	89 1c 24             	mov    %ebx,(%esp)
  80139e:	e8 29 fc ff ff       	call   800fcc <close>
	return r;
  8013a3:	83 c4 10             	add    $0x10,%esp
  8013a6:	89 f0                	mov    %esi,%eax
  8013a8:	eb 00                	jmp    8013aa <stat+0x38>
}
  8013aa:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8013ad:	5b                   	pop    %ebx
  8013ae:	5e                   	pop    %esi
  8013af:	5d                   	pop    %ebp
  8013b0:	c3                   	ret    

008013b1 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8013b1:	55                   	push   %ebp
  8013b2:	89 e5                	mov    %esp,%ebp
  8013b4:	56                   	push   %esi
  8013b5:	53                   	push   %ebx
  8013b6:	89 c6                	mov    %eax,%esi
  8013b8:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8013ba:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  8013c1:	75 12                	jne    8013d5 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  8013c3:	83 ec 0c             	sub    $0xc,%esp
  8013c6:	6a 01                	push   $0x1
  8013c8:	e8 b8 07 00 00       	call   801b85 <ipc_find_env>
  8013cd:	a3 00 40 80 00       	mov    %eax,0x804000
  8013d2:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8013d5:	6a 07                	push   $0x7
  8013d7:	68 00 50 80 00       	push   $0x805000
  8013dc:	56                   	push   %esi
  8013dd:	ff 35 00 40 80 00    	pushl  0x804000
  8013e3:	e8 48 07 00 00       	call   801b30 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8013e8:	83 c4 0c             	add    $0xc,%esp
  8013eb:	6a 00                	push   $0x0
  8013ed:	53                   	push   %ebx
  8013ee:	6a 00                	push   $0x0
  8013f0:	e8 d3 06 00 00       	call   801ac8 <ipc_recv>
}
  8013f5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8013f8:	5b                   	pop    %ebx
  8013f9:	5e                   	pop    %esi
  8013fa:	5d                   	pop    %ebp
  8013fb:	c3                   	ret    

008013fc <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8013fc:	55                   	push   %ebp
  8013fd:	89 e5                	mov    %esp,%ebp
  8013ff:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801402:	8b 45 08             	mov    0x8(%ebp),%eax
  801405:	8b 40 0c             	mov    0xc(%eax),%eax
  801408:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  80140d:	8b 45 0c             	mov    0xc(%ebp),%eax
  801410:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801415:	ba 00 00 00 00       	mov    $0x0,%edx
  80141a:	b8 02 00 00 00       	mov    $0x2,%eax
  80141f:	e8 8d ff ff ff       	call   8013b1 <fsipc>
}
  801424:	c9                   	leave  
  801425:	c3                   	ret    

00801426 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  801426:	55                   	push   %ebp
  801427:	89 e5                	mov    %esp,%ebp
  801429:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  80142c:	8b 45 08             	mov    0x8(%ebp),%eax
  80142f:	8b 40 0c             	mov    0xc(%eax),%eax
  801432:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  801437:	ba 00 00 00 00       	mov    $0x0,%edx
  80143c:	b8 06 00 00 00       	mov    $0x6,%eax
  801441:	e8 6b ff ff ff       	call   8013b1 <fsipc>
}
  801446:	c9                   	leave  
  801447:	c3                   	ret    

00801448 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801448:	55                   	push   %ebp
  801449:	89 e5                	mov    %esp,%ebp
  80144b:	53                   	push   %ebx
  80144c:	83 ec 04             	sub    $0x4,%esp
  80144f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801452:	8b 45 08             	mov    0x8(%ebp),%eax
  801455:	8b 40 0c             	mov    0xc(%eax),%eax
  801458:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  80145d:	ba 00 00 00 00       	mov    $0x0,%edx
  801462:	b8 05 00 00 00       	mov    $0x5,%eax
  801467:	e8 45 ff ff ff       	call   8013b1 <fsipc>
  80146c:	85 c0                	test   %eax,%eax
  80146e:	78 2c                	js     80149c <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801470:	83 ec 08             	sub    $0x8,%esp
  801473:	68 00 50 80 00       	push   $0x805000
  801478:	53                   	push   %ebx
  801479:	e8 82 f6 ff ff       	call   800b00 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  80147e:	a1 80 50 80 00       	mov    0x805080,%eax
  801483:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801489:	a1 84 50 80 00       	mov    0x805084,%eax
  80148e:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801494:	83 c4 10             	add    $0x10,%esp
  801497:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80149c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80149f:	c9                   	leave  
  8014a0:	c3                   	ret    

008014a1 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  8014a1:	55                   	push   %ebp
  8014a2:	89 e5                	mov    %esp,%ebp
  8014a4:	83 ec 08             	sub    $0x8,%esp
  8014a7:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  8014aa:	8b 55 08             	mov    0x8(%ebp),%edx
  8014ad:	8b 52 0c             	mov    0xc(%edx),%edx
  8014b0:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  8014b6:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  8014bb:	76 05                	jbe    8014c2 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  8014bd:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  8014c2:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  8014c7:	83 ec 04             	sub    $0x4,%esp
  8014ca:	50                   	push   %eax
  8014cb:	ff 75 0c             	pushl  0xc(%ebp)
  8014ce:	68 08 50 80 00       	push   $0x805008
  8014d3:	e8 9d f7 ff ff       	call   800c75 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8014d8:	ba 00 00 00 00       	mov    $0x0,%edx
  8014dd:	b8 04 00 00 00       	mov    $0x4,%eax
  8014e2:	e8 ca fe ff ff       	call   8013b1 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8014e7:	c9                   	leave  
  8014e8:	c3                   	ret    

008014e9 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8014e9:	55                   	push   %ebp
  8014ea:	89 e5                	mov    %esp,%ebp
  8014ec:	56                   	push   %esi
  8014ed:	53                   	push   %ebx
  8014ee:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8014f1:	8b 45 08             	mov    0x8(%ebp),%eax
  8014f4:	8b 40 0c             	mov    0xc(%eax),%eax
  8014f7:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  8014fc:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801502:	ba 00 00 00 00       	mov    $0x0,%edx
  801507:	b8 03 00 00 00       	mov    $0x3,%eax
  80150c:	e8 a0 fe ff ff       	call   8013b1 <fsipc>
  801511:	89 c3                	mov    %eax,%ebx
  801513:	85 c0                	test   %eax,%eax
  801515:	78 4b                	js     801562 <devfile_read+0x79>
		return r;
	assert(r <= n);
  801517:	39 c6                	cmp    %eax,%esi
  801519:	73 16                	jae    801531 <devfile_read+0x48>
  80151b:	68 70 22 80 00       	push   $0x802270
  801520:	68 77 22 80 00       	push   $0x802277
  801525:	6a 7c                	push   $0x7c
  801527:	68 8c 22 80 00       	push   $0x80228c
  80152c:	e8 93 ef ff ff       	call   8004c4 <_panic>
	assert(r <= PGSIZE);
  801531:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801536:	7e 16                	jle    80154e <devfile_read+0x65>
  801538:	68 97 22 80 00       	push   $0x802297
  80153d:	68 77 22 80 00       	push   $0x802277
  801542:	6a 7d                	push   $0x7d
  801544:	68 8c 22 80 00       	push   $0x80228c
  801549:	e8 76 ef ff ff       	call   8004c4 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  80154e:	83 ec 04             	sub    $0x4,%esp
  801551:	50                   	push   %eax
  801552:	68 00 50 80 00       	push   $0x805000
  801557:	ff 75 0c             	pushl  0xc(%ebp)
  80155a:	e8 16 f7 ff ff       	call   800c75 <memmove>
	return r;
  80155f:	83 c4 10             	add    $0x10,%esp
}
  801562:	89 d8                	mov    %ebx,%eax
  801564:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801567:	5b                   	pop    %ebx
  801568:	5e                   	pop    %esi
  801569:	5d                   	pop    %ebp
  80156a:	c3                   	ret    

0080156b <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  80156b:	55                   	push   %ebp
  80156c:	89 e5                	mov    %esp,%ebp
  80156e:	53                   	push   %ebx
  80156f:	83 ec 20             	sub    $0x20,%esp
  801572:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801575:	53                   	push   %ebx
  801576:	e8 50 f5 ff ff       	call   800acb <strlen>
  80157b:	83 c4 10             	add    $0x10,%esp
  80157e:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801583:	7f 63                	jg     8015e8 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801585:	83 ec 0c             	sub    $0xc,%esp
  801588:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80158b:	50                   	push   %eax
  80158c:	e8 be f8 ff ff       	call   800e4f <fd_alloc>
  801591:	83 c4 10             	add    $0x10,%esp
  801594:	85 c0                	test   %eax,%eax
  801596:	78 55                	js     8015ed <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801598:	83 ec 08             	sub    $0x8,%esp
  80159b:	53                   	push   %ebx
  80159c:	68 00 50 80 00       	push   $0x805000
  8015a1:	e8 5a f5 ff ff       	call   800b00 <strcpy>
	fsipcbuf.open.req_omode = mode;
  8015a6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8015a9:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  8015ae:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8015b1:	b8 01 00 00 00       	mov    $0x1,%eax
  8015b6:	e8 f6 fd ff ff       	call   8013b1 <fsipc>
  8015bb:	89 c3                	mov    %eax,%ebx
  8015bd:	83 c4 10             	add    $0x10,%esp
  8015c0:	85 c0                	test   %eax,%eax
  8015c2:	79 14                	jns    8015d8 <open+0x6d>
		fd_close(fd, 0);
  8015c4:	83 ec 08             	sub    $0x8,%esp
  8015c7:	6a 00                	push   $0x0
  8015c9:	ff 75 f4             	pushl  -0xc(%ebp)
  8015cc:	e8 77 f9 ff ff       	call   800f48 <fd_close>
		return r;
  8015d1:	83 c4 10             	add    $0x10,%esp
  8015d4:	89 d8                	mov    %ebx,%eax
  8015d6:	eb 15                	jmp    8015ed <open+0x82>
	}

	return fd2num(fd);
  8015d8:	83 ec 0c             	sub    $0xc,%esp
  8015db:	ff 75 f4             	pushl  -0xc(%ebp)
  8015de:	e8 45 f8 ff ff       	call   800e28 <fd2num>
  8015e3:	83 c4 10             	add    $0x10,%esp
  8015e6:	eb 05                	jmp    8015ed <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8015e8:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8015ed:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015f0:	c9                   	leave  
  8015f1:	c3                   	ret    

008015f2 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8015f2:	55                   	push   %ebp
  8015f3:	89 e5                	mov    %esp,%ebp
  8015f5:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8015f8:	ba 00 00 00 00       	mov    $0x0,%edx
  8015fd:	b8 08 00 00 00       	mov    $0x8,%eax
  801602:	e8 aa fd ff ff       	call   8013b1 <fsipc>
}
  801607:	c9                   	leave  
  801608:	c3                   	ret    

00801609 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801609:	55                   	push   %ebp
  80160a:	89 e5                	mov    %esp,%ebp
  80160c:	56                   	push   %esi
  80160d:	53                   	push   %ebx
  80160e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801611:	83 ec 0c             	sub    $0xc,%esp
  801614:	ff 75 08             	pushl  0x8(%ebp)
  801617:	e8 1c f8 ff ff       	call   800e38 <fd2data>
  80161c:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  80161e:	83 c4 08             	add    $0x8,%esp
  801621:	68 a3 22 80 00       	push   $0x8022a3
  801626:	53                   	push   %ebx
  801627:	e8 d4 f4 ff ff       	call   800b00 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  80162c:	8b 46 04             	mov    0x4(%esi),%eax
  80162f:	2b 06                	sub    (%esi),%eax
  801631:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801637:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  80163e:	00 00 00 
	stat->st_dev = &devpipe;
  801641:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  801648:	30 80 00 
	return 0;
}
  80164b:	b8 00 00 00 00       	mov    $0x0,%eax
  801650:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801653:	5b                   	pop    %ebx
  801654:	5e                   	pop    %esi
  801655:	5d                   	pop    %ebp
  801656:	c3                   	ret    

00801657 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801657:	55                   	push   %ebp
  801658:	89 e5                	mov    %esp,%ebp
  80165a:	53                   	push   %ebx
  80165b:	83 ec 0c             	sub    $0xc,%esp
  80165e:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801661:	53                   	push   %ebx
  801662:	6a 00                	push   $0x0
  801664:	e8 bf eb ff ff       	call   800228 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801669:	89 1c 24             	mov    %ebx,(%esp)
  80166c:	e8 c7 f7 ff ff       	call   800e38 <fd2data>
  801671:	83 c4 08             	add    $0x8,%esp
  801674:	50                   	push   %eax
  801675:	6a 00                	push   $0x0
  801677:	e8 ac eb ff ff       	call   800228 <sys_page_unmap>
}
  80167c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80167f:	c9                   	leave  
  801680:	c3                   	ret    

00801681 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801681:	55                   	push   %ebp
  801682:	89 e5                	mov    %esp,%ebp
  801684:	57                   	push   %edi
  801685:	56                   	push   %esi
  801686:	53                   	push   %ebx
  801687:	83 ec 1c             	sub    $0x1c,%esp
  80168a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80168d:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  80168f:	a1 04 40 80 00       	mov    0x804004,%eax
  801694:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801697:	83 ec 0c             	sub    $0xc,%esp
  80169a:	ff 75 e0             	pushl  -0x20(%ebp)
  80169d:	e8 27 05 00 00       	call   801bc9 <pageref>
  8016a2:	89 c3                	mov    %eax,%ebx
  8016a4:	89 3c 24             	mov    %edi,(%esp)
  8016a7:	e8 1d 05 00 00       	call   801bc9 <pageref>
  8016ac:	83 c4 10             	add    $0x10,%esp
  8016af:	39 c3                	cmp    %eax,%ebx
  8016b1:	0f 94 c1             	sete   %cl
  8016b4:	0f b6 c9             	movzbl %cl,%ecx
  8016b7:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  8016ba:	8b 15 04 40 80 00    	mov    0x804004,%edx
  8016c0:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  8016c3:	39 ce                	cmp    %ecx,%esi
  8016c5:	74 1b                	je     8016e2 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  8016c7:	39 c3                	cmp    %eax,%ebx
  8016c9:	75 c4                	jne    80168f <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  8016cb:	8b 42 58             	mov    0x58(%edx),%eax
  8016ce:	ff 75 e4             	pushl  -0x1c(%ebp)
  8016d1:	50                   	push   %eax
  8016d2:	56                   	push   %esi
  8016d3:	68 aa 22 80 00       	push   $0x8022aa
  8016d8:	e8 bf ee ff ff       	call   80059c <cprintf>
  8016dd:	83 c4 10             	add    $0x10,%esp
  8016e0:	eb ad                	jmp    80168f <_pipeisclosed+0xe>
	}
}
  8016e2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8016e5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8016e8:	5b                   	pop    %ebx
  8016e9:	5e                   	pop    %esi
  8016ea:	5f                   	pop    %edi
  8016eb:	5d                   	pop    %ebp
  8016ec:	c3                   	ret    

008016ed <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8016ed:	55                   	push   %ebp
  8016ee:	89 e5                	mov    %esp,%ebp
  8016f0:	57                   	push   %edi
  8016f1:	56                   	push   %esi
  8016f2:	53                   	push   %ebx
  8016f3:	83 ec 18             	sub    $0x18,%esp
  8016f6:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  8016f9:	56                   	push   %esi
  8016fa:	e8 39 f7 ff ff       	call   800e38 <fd2data>
  8016ff:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801701:	83 c4 10             	add    $0x10,%esp
  801704:	bf 00 00 00 00       	mov    $0x0,%edi
  801709:	eb 3b                	jmp    801746 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  80170b:	89 da                	mov    %ebx,%edx
  80170d:	89 f0                	mov    %esi,%eax
  80170f:	e8 6d ff ff ff       	call   801681 <_pipeisclosed>
  801714:	85 c0                	test   %eax,%eax
  801716:	75 38                	jne    801750 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801718:	e8 67 ea ff ff       	call   800184 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  80171d:	8b 53 04             	mov    0x4(%ebx),%edx
  801720:	8b 03                	mov    (%ebx),%eax
  801722:	83 c0 20             	add    $0x20,%eax
  801725:	39 c2                	cmp    %eax,%edx
  801727:	73 e2                	jae    80170b <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801729:	8b 45 0c             	mov    0xc(%ebp),%eax
  80172c:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  80172f:	89 d0                	mov    %edx,%eax
  801731:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801736:	79 05                	jns    80173d <devpipe_write+0x50>
  801738:	48                   	dec    %eax
  801739:	83 c8 e0             	or     $0xffffffe0,%eax
  80173c:	40                   	inc    %eax
  80173d:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801741:	42                   	inc    %edx
  801742:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801745:	47                   	inc    %edi
  801746:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801749:	75 d2                	jne    80171d <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  80174b:	8b 45 10             	mov    0x10(%ebp),%eax
  80174e:	eb 05                	jmp    801755 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801750:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801755:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801758:	5b                   	pop    %ebx
  801759:	5e                   	pop    %esi
  80175a:	5f                   	pop    %edi
  80175b:	5d                   	pop    %ebp
  80175c:	c3                   	ret    

0080175d <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  80175d:	55                   	push   %ebp
  80175e:	89 e5                	mov    %esp,%ebp
  801760:	57                   	push   %edi
  801761:	56                   	push   %esi
  801762:	53                   	push   %ebx
  801763:	83 ec 18             	sub    $0x18,%esp
  801766:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801769:	57                   	push   %edi
  80176a:	e8 c9 f6 ff ff       	call   800e38 <fd2data>
  80176f:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801771:	83 c4 10             	add    $0x10,%esp
  801774:	bb 00 00 00 00       	mov    $0x0,%ebx
  801779:	eb 3a                	jmp    8017b5 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  80177b:	85 db                	test   %ebx,%ebx
  80177d:	74 04                	je     801783 <devpipe_read+0x26>
				return i;
  80177f:	89 d8                	mov    %ebx,%eax
  801781:	eb 41                	jmp    8017c4 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801783:	89 f2                	mov    %esi,%edx
  801785:	89 f8                	mov    %edi,%eax
  801787:	e8 f5 fe ff ff       	call   801681 <_pipeisclosed>
  80178c:	85 c0                	test   %eax,%eax
  80178e:	75 2f                	jne    8017bf <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801790:	e8 ef e9 ff ff       	call   800184 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801795:	8b 06                	mov    (%esi),%eax
  801797:	3b 46 04             	cmp    0x4(%esi),%eax
  80179a:	74 df                	je     80177b <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  80179c:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8017a1:	79 05                	jns    8017a8 <devpipe_read+0x4b>
  8017a3:	48                   	dec    %eax
  8017a4:	83 c8 e0             	or     $0xffffffe0,%eax
  8017a7:	40                   	inc    %eax
  8017a8:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  8017ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8017af:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  8017b2:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8017b4:	43                   	inc    %ebx
  8017b5:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  8017b8:	75 db                	jne    801795 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  8017ba:	8b 45 10             	mov    0x10(%ebp),%eax
  8017bd:	eb 05                	jmp    8017c4 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8017bf:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  8017c4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8017c7:	5b                   	pop    %ebx
  8017c8:	5e                   	pop    %esi
  8017c9:	5f                   	pop    %edi
  8017ca:	5d                   	pop    %ebp
  8017cb:	c3                   	ret    

008017cc <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  8017cc:	55                   	push   %ebp
  8017cd:	89 e5                	mov    %esp,%ebp
  8017cf:	56                   	push   %esi
  8017d0:	53                   	push   %ebx
  8017d1:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  8017d4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8017d7:	50                   	push   %eax
  8017d8:	e8 72 f6 ff ff       	call   800e4f <fd_alloc>
  8017dd:	83 c4 10             	add    $0x10,%esp
  8017e0:	85 c0                	test   %eax,%eax
  8017e2:	0f 88 2a 01 00 00    	js     801912 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8017e8:	83 ec 04             	sub    $0x4,%esp
  8017eb:	68 07 04 00 00       	push   $0x407
  8017f0:	ff 75 f4             	pushl  -0xc(%ebp)
  8017f3:	6a 00                	push   $0x0
  8017f5:	e8 a9 e9 ff ff       	call   8001a3 <sys_page_alloc>
  8017fa:	83 c4 10             	add    $0x10,%esp
  8017fd:	85 c0                	test   %eax,%eax
  8017ff:	0f 88 0d 01 00 00    	js     801912 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801805:	83 ec 0c             	sub    $0xc,%esp
  801808:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80180b:	50                   	push   %eax
  80180c:	e8 3e f6 ff ff       	call   800e4f <fd_alloc>
  801811:	89 c3                	mov    %eax,%ebx
  801813:	83 c4 10             	add    $0x10,%esp
  801816:	85 c0                	test   %eax,%eax
  801818:	0f 88 e2 00 00 00    	js     801900 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80181e:	83 ec 04             	sub    $0x4,%esp
  801821:	68 07 04 00 00       	push   $0x407
  801826:	ff 75 f0             	pushl  -0x10(%ebp)
  801829:	6a 00                	push   $0x0
  80182b:	e8 73 e9 ff ff       	call   8001a3 <sys_page_alloc>
  801830:	89 c3                	mov    %eax,%ebx
  801832:	83 c4 10             	add    $0x10,%esp
  801835:	85 c0                	test   %eax,%eax
  801837:	0f 88 c3 00 00 00    	js     801900 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  80183d:	83 ec 0c             	sub    $0xc,%esp
  801840:	ff 75 f4             	pushl  -0xc(%ebp)
  801843:	e8 f0 f5 ff ff       	call   800e38 <fd2data>
  801848:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80184a:	83 c4 0c             	add    $0xc,%esp
  80184d:	68 07 04 00 00       	push   $0x407
  801852:	50                   	push   %eax
  801853:	6a 00                	push   $0x0
  801855:	e8 49 e9 ff ff       	call   8001a3 <sys_page_alloc>
  80185a:	89 c3                	mov    %eax,%ebx
  80185c:	83 c4 10             	add    $0x10,%esp
  80185f:	85 c0                	test   %eax,%eax
  801861:	0f 88 89 00 00 00    	js     8018f0 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801867:	83 ec 0c             	sub    $0xc,%esp
  80186a:	ff 75 f0             	pushl  -0x10(%ebp)
  80186d:	e8 c6 f5 ff ff       	call   800e38 <fd2data>
  801872:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801879:	50                   	push   %eax
  80187a:	6a 00                	push   $0x0
  80187c:	56                   	push   %esi
  80187d:	6a 00                	push   $0x0
  80187f:	e8 62 e9 ff ff       	call   8001e6 <sys_page_map>
  801884:	89 c3                	mov    %eax,%ebx
  801886:	83 c4 20             	add    $0x20,%esp
  801889:	85 c0                	test   %eax,%eax
  80188b:	78 55                	js     8018e2 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  80188d:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801893:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801896:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801898:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80189b:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  8018a2:	8b 15 20 30 80 00    	mov    0x803020,%edx
  8018a8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8018ab:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  8018ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8018b0:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  8018b7:	83 ec 0c             	sub    $0xc,%esp
  8018ba:	ff 75 f4             	pushl  -0xc(%ebp)
  8018bd:	e8 66 f5 ff ff       	call   800e28 <fd2num>
  8018c2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8018c5:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  8018c7:	83 c4 04             	add    $0x4,%esp
  8018ca:	ff 75 f0             	pushl  -0x10(%ebp)
  8018cd:	e8 56 f5 ff ff       	call   800e28 <fd2num>
  8018d2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8018d5:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  8018d8:	83 c4 10             	add    $0x10,%esp
  8018db:	b8 00 00 00 00       	mov    $0x0,%eax
  8018e0:	eb 30                	jmp    801912 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  8018e2:	83 ec 08             	sub    $0x8,%esp
  8018e5:	56                   	push   %esi
  8018e6:	6a 00                	push   $0x0
  8018e8:	e8 3b e9 ff ff       	call   800228 <sys_page_unmap>
  8018ed:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  8018f0:	83 ec 08             	sub    $0x8,%esp
  8018f3:	ff 75 f0             	pushl  -0x10(%ebp)
  8018f6:	6a 00                	push   $0x0
  8018f8:	e8 2b e9 ff ff       	call   800228 <sys_page_unmap>
  8018fd:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801900:	83 ec 08             	sub    $0x8,%esp
  801903:	ff 75 f4             	pushl  -0xc(%ebp)
  801906:	6a 00                	push   $0x0
  801908:	e8 1b e9 ff ff       	call   800228 <sys_page_unmap>
  80190d:	83 c4 10             	add    $0x10,%esp
  801910:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801912:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801915:	5b                   	pop    %ebx
  801916:	5e                   	pop    %esi
  801917:	5d                   	pop    %ebp
  801918:	c3                   	ret    

00801919 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801919:	55                   	push   %ebp
  80191a:	89 e5                	mov    %esp,%ebp
  80191c:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80191f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801922:	50                   	push   %eax
  801923:	ff 75 08             	pushl  0x8(%ebp)
  801926:	e8 73 f5 ff ff       	call   800e9e <fd_lookup>
  80192b:	83 c4 10             	add    $0x10,%esp
  80192e:	85 c0                	test   %eax,%eax
  801930:	78 18                	js     80194a <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801932:	83 ec 0c             	sub    $0xc,%esp
  801935:	ff 75 f4             	pushl  -0xc(%ebp)
  801938:	e8 fb f4 ff ff       	call   800e38 <fd2data>
	return _pipeisclosed(fd, p);
  80193d:	89 c2                	mov    %eax,%edx
  80193f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801942:	e8 3a fd ff ff       	call   801681 <_pipeisclosed>
  801947:	83 c4 10             	add    $0x10,%esp
}
  80194a:	c9                   	leave  
  80194b:	c3                   	ret    

0080194c <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  80194c:	55                   	push   %ebp
  80194d:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  80194f:	b8 00 00 00 00       	mov    $0x0,%eax
  801954:	5d                   	pop    %ebp
  801955:	c3                   	ret    

00801956 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801956:	55                   	push   %ebp
  801957:	89 e5                	mov    %esp,%ebp
  801959:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  80195c:	68 c2 22 80 00       	push   $0x8022c2
  801961:	ff 75 0c             	pushl  0xc(%ebp)
  801964:	e8 97 f1 ff ff       	call   800b00 <strcpy>
	return 0;
}
  801969:	b8 00 00 00 00       	mov    $0x0,%eax
  80196e:	c9                   	leave  
  80196f:	c3                   	ret    

00801970 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801970:	55                   	push   %ebp
  801971:	89 e5                	mov    %esp,%ebp
  801973:	57                   	push   %edi
  801974:	56                   	push   %esi
  801975:	53                   	push   %ebx
  801976:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  80197c:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801981:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801987:	eb 2c                	jmp    8019b5 <devcons_write+0x45>
		m = n - tot;
  801989:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80198c:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  80198e:	83 fb 7f             	cmp    $0x7f,%ebx
  801991:	76 05                	jbe    801998 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801993:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801998:	83 ec 04             	sub    $0x4,%esp
  80199b:	53                   	push   %ebx
  80199c:	03 45 0c             	add    0xc(%ebp),%eax
  80199f:	50                   	push   %eax
  8019a0:	57                   	push   %edi
  8019a1:	e8 cf f2 ff ff       	call   800c75 <memmove>
		sys_cputs(buf, m);
  8019a6:	83 c4 08             	add    $0x8,%esp
  8019a9:	53                   	push   %ebx
  8019aa:	57                   	push   %edi
  8019ab:	e8 37 e7 ff ff       	call   8000e7 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  8019b0:	01 de                	add    %ebx,%esi
  8019b2:	83 c4 10             	add    $0x10,%esp
  8019b5:	89 f0                	mov    %esi,%eax
  8019b7:	3b 75 10             	cmp    0x10(%ebp),%esi
  8019ba:	72 cd                	jb     801989 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  8019bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8019bf:	5b                   	pop    %ebx
  8019c0:	5e                   	pop    %esi
  8019c1:	5f                   	pop    %edi
  8019c2:	5d                   	pop    %ebp
  8019c3:	c3                   	ret    

008019c4 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  8019c4:	55                   	push   %ebp
  8019c5:	89 e5                	mov    %esp,%ebp
  8019c7:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  8019ca:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8019ce:	75 07                	jne    8019d7 <devcons_read+0x13>
  8019d0:	eb 23                	jmp    8019f5 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  8019d2:	e8 ad e7 ff ff       	call   800184 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  8019d7:	e8 29 e7 ff ff       	call   800105 <sys_cgetc>
  8019dc:	85 c0                	test   %eax,%eax
  8019de:	74 f2                	je     8019d2 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  8019e0:	85 c0                	test   %eax,%eax
  8019e2:	78 1d                	js     801a01 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  8019e4:	83 f8 04             	cmp    $0x4,%eax
  8019e7:	74 13                	je     8019fc <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  8019e9:	8b 55 0c             	mov    0xc(%ebp),%edx
  8019ec:	88 02                	mov    %al,(%edx)
	return 1;
  8019ee:	b8 01 00 00 00       	mov    $0x1,%eax
  8019f3:	eb 0c                	jmp    801a01 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  8019f5:	b8 00 00 00 00       	mov    $0x0,%eax
  8019fa:	eb 05                	jmp    801a01 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  8019fc:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801a01:	c9                   	leave  
  801a02:	c3                   	ret    

00801a03 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801a03:	55                   	push   %ebp
  801a04:	89 e5                	mov    %esp,%ebp
  801a06:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801a09:	8b 45 08             	mov    0x8(%ebp),%eax
  801a0c:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801a0f:	6a 01                	push   $0x1
  801a11:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801a14:	50                   	push   %eax
  801a15:	e8 cd e6 ff ff       	call   8000e7 <sys_cputs>
}
  801a1a:	83 c4 10             	add    $0x10,%esp
  801a1d:	c9                   	leave  
  801a1e:	c3                   	ret    

00801a1f <getchar>:

int
getchar(void)
{
  801a1f:	55                   	push   %ebp
  801a20:	89 e5                	mov    %esp,%ebp
  801a22:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801a25:	6a 01                	push   $0x1
  801a27:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801a2a:	50                   	push   %eax
  801a2b:	6a 00                	push   $0x0
  801a2d:	e8 d2 f6 ff ff       	call   801104 <read>
	if (r < 0)
  801a32:	83 c4 10             	add    $0x10,%esp
  801a35:	85 c0                	test   %eax,%eax
  801a37:	78 0f                	js     801a48 <getchar+0x29>
		return r;
	if (r < 1)
  801a39:	85 c0                	test   %eax,%eax
  801a3b:	7e 06                	jle    801a43 <getchar+0x24>
		return -E_EOF;
	return c;
  801a3d:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801a41:	eb 05                	jmp    801a48 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801a43:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801a48:	c9                   	leave  
  801a49:	c3                   	ret    

00801a4a <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801a4a:	55                   	push   %ebp
  801a4b:	89 e5                	mov    %esp,%ebp
  801a4d:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801a50:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801a53:	50                   	push   %eax
  801a54:	ff 75 08             	pushl  0x8(%ebp)
  801a57:	e8 42 f4 ff ff       	call   800e9e <fd_lookup>
  801a5c:	83 c4 10             	add    $0x10,%esp
  801a5f:	85 c0                	test   %eax,%eax
  801a61:	78 11                	js     801a74 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801a63:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801a66:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801a6c:	39 10                	cmp    %edx,(%eax)
  801a6e:	0f 94 c0             	sete   %al
  801a71:	0f b6 c0             	movzbl %al,%eax
}
  801a74:	c9                   	leave  
  801a75:	c3                   	ret    

00801a76 <opencons>:

int
opencons(void)
{
  801a76:	55                   	push   %ebp
  801a77:	89 e5                	mov    %esp,%ebp
  801a79:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801a7c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801a7f:	50                   	push   %eax
  801a80:	e8 ca f3 ff ff       	call   800e4f <fd_alloc>
  801a85:	83 c4 10             	add    $0x10,%esp
  801a88:	85 c0                	test   %eax,%eax
  801a8a:	78 3a                	js     801ac6 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801a8c:	83 ec 04             	sub    $0x4,%esp
  801a8f:	68 07 04 00 00       	push   $0x407
  801a94:	ff 75 f4             	pushl  -0xc(%ebp)
  801a97:	6a 00                	push   $0x0
  801a99:	e8 05 e7 ff ff       	call   8001a3 <sys_page_alloc>
  801a9e:	83 c4 10             	add    $0x10,%esp
  801aa1:	85 c0                	test   %eax,%eax
  801aa3:	78 21                	js     801ac6 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801aa5:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801aab:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801aae:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801ab0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ab3:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801aba:	83 ec 0c             	sub    $0xc,%esp
  801abd:	50                   	push   %eax
  801abe:	e8 65 f3 ff ff       	call   800e28 <fd2num>
  801ac3:	83 c4 10             	add    $0x10,%esp
}
  801ac6:	c9                   	leave  
  801ac7:	c3                   	ret    

00801ac8 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801ac8:	55                   	push   %ebp
  801ac9:	89 e5                	mov    %esp,%ebp
  801acb:	56                   	push   %esi
  801acc:	53                   	push   %ebx
  801acd:	8b 75 08             	mov    0x8(%ebp),%esi
  801ad0:	8b 45 0c             	mov    0xc(%ebp),%eax
  801ad3:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801ad6:	85 c0                	test   %eax,%eax
  801ad8:	75 05                	jne    801adf <ipc_recv+0x17>
        pg = (void *)UTOP;
  801ada:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801adf:	83 ec 0c             	sub    $0xc,%esp
  801ae2:	50                   	push   %eax
  801ae3:	e8 8a e8 ff ff       	call   800372 <sys_ipc_recv>
  801ae8:	83 c4 10             	add    $0x10,%esp
  801aeb:	85 c0                	test   %eax,%eax
  801aed:	74 16                	je     801b05 <ipc_recv+0x3d>
        if (from_env_store)
  801aef:	85 f6                	test   %esi,%esi
  801af1:	74 06                	je     801af9 <ipc_recv+0x31>
            *from_env_store = 0;
  801af3:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801af9:	85 db                	test   %ebx,%ebx
  801afb:	74 2c                	je     801b29 <ipc_recv+0x61>
            *perm_store = 0;
  801afd:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801b03:	eb 24                	jmp    801b29 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801b05:	85 f6                	test   %esi,%esi
  801b07:	74 0a                	je     801b13 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801b09:	a1 04 40 80 00       	mov    0x804004,%eax
  801b0e:	8b 40 74             	mov    0x74(%eax),%eax
  801b11:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801b13:	85 db                	test   %ebx,%ebx
  801b15:	74 0a                	je     801b21 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801b17:	a1 04 40 80 00       	mov    0x804004,%eax
  801b1c:	8b 40 78             	mov    0x78(%eax),%eax
  801b1f:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801b21:	a1 04 40 80 00       	mov    0x804004,%eax
  801b26:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801b29:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801b2c:	5b                   	pop    %ebx
  801b2d:	5e                   	pop    %esi
  801b2e:	5d                   	pop    %ebp
  801b2f:	c3                   	ret    

00801b30 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801b30:	55                   	push   %ebp
  801b31:	89 e5                	mov    %esp,%ebp
  801b33:	57                   	push   %edi
  801b34:	56                   	push   %esi
  801b35:	53                   	push   %ebx
  801b36:	83 ec 0c             	sub    $0xc,%esp
  801b39:	8b 75 0c             	mov    0xc(%ebp),%esi
  801b3c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801b3f:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801b42:	85 db                	test   %ebx,%ebx
  801b44:	75 0c                	jne    801b52 <ipc_send+0x22>
        pg = (void *)UTOP;
  801b46:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801b4b:	eb 05                	jmp    801b52 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801b4d:	e8 32 e6 ff ff       	call   800184 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801b52:	57                   	push   %edi
  801b53:	53                   	push   %ebx
  801b54:	56                   	push   %esi
  801b55:	ff 75 08             	pushl  0x8(%ebp)
  801b58:	e8 f2 e7 ff ff       	call   80034f <sys_ipc_try_send>
  801b5d:	83 c4 10             	add    $0x10,%esp
  801b60:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801b63:	74 e8                	je     801b4d <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801b65:	85 c0                	test   %eax,%eax
  801b67:	74 14                	je     801b7d <ipc_send+0x4d>
        panic("ipc_send: error");
  801b69:	83 ec 04             	sub    $0x4,%esp
  801b6c:	68 ce 22 80 00       	push   $0x8022ce
  801b71:	6a 6a                	push   $0x6a
  801b73:	68 de 22 80 00       	push   $0x8022de
  801b78:	e8 47 e9 ff ff       	call   8004c4 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801b7d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b80:	5b                   	pop    %ebx
  801b81:	5e                   	pop    %esi
  801b82:	5f                   	pop    %edi
  801b83:	5d                   	pop    %ebp
  801b84:	c3                   	ret    

00801b85 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801b85:	55                   	push   %ebp
  801b86:	89 e5                	mov    %esp,%ebp
  801b88:	53                   	push   %ebx
  801b89:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801b8c:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801b91:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801b98:	89 d0                	mov    %edx,%eax
  801b9a:	c1 e0 07             	shl    $0x7,%eax
  801b9d:	29 d8                	sub    %ebx,%eax
  801b9f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801ba4:	8b 40 50             	mov    0x50(%eax),%eax
  801ba7:	39 c8                	cmp    %ecx,%eax
  801ba9:	75 0d                	jne    801bb8 <ipc_find_env+0x33>
			return envs[i].env_id;
  801bab:	c1 e2 07             	shl    $0x7,%edx
  801bae:	29 da                	sub    %ebx,%edx
  801bb0:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801bb6:	eb 0e                	jmp    801bc6 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801bb8:	42                   	inc    %edx
  801bb9:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801bbf:	75 d0                	jne    801b91 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801bc1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801bc6:	5b                   	pop    %ebx
  801bc7:	5d                   	pop    %ebp
  801bc8:	c3                   	ret    

00801bc9 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801bc9:	55                   	push   %ebp
  801bca:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801bcc:	8b 45 08             	mov    0x8(%ebp),%eax
  801bcf:	c1 e8 16             	shr    $0x16,%eax
  801bd2:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801bd9:	a8 01                	test   $0x1,%al
  801bdb:	74 21                	je     801bfe <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801bdd:	8b 45 08             	mov    0x8(%ebp),%eax
  801be0:	c1 e8 0c             	shr    $0xc,%eax
  801be3:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801bea:	a8 01                	test   $0x1,%al
  801bec:	74 17                	je     801c05 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801bee:	c1 e8 0c             	shr    $0xc,%eax
  801bf1:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801bf8:	ef 
  801bf9:	0f b7 c0             	movzwl %ax,%eax
  801bfc:	eb 0c                	jmp    801c0a <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801bfe:	b8 00 00 00 00       	mov    $0x0,%eax
  801c03:	eb 05                	jmp    801c0a <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801c05:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801c0a:	5d                   	pop    %ebp
  801c0b:	c3                   	ret    

00801c0c <__udivdi3>:
  801c0c:	55                   	push   %ebp
  801c0d:	57                   	push   %edi
  801c0e:	56                   	push   %esi
  801c0f:	53                   	push   %ebx
  801c10:	83 ec 1c             	sub    $0x1c,%esp
  801c13:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801c17:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801c1b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801c1f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801c23:	89 ca                	mov    %ecx,%edx
  801c25:	89 f8                	mov    %edi,%eax
  801c27:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801c2b:	85 f6                	test   %esi,%esi
  801c2d:	75 2d                	jne    801c5c <__udivdi3+0x50>
  801c2f:	39 cf                	cmp    %ecx,%edi
  801c31:	77 65                	ja     801c98 <__udivdi3+0x8c>
  801c33:	89 fd                	mov    %edi,%ebp
  801c35:	85 ff                	test   %edi,%edi
  801c37:	75 0b                	jne    801c44 <__udivdi3+0x38>
  801c39:	b8 01 00 00 00       	mov    $0x1,%eax
  801c3e:	31 d2                	xor    %edx,%edx
  801c40:	f7 f7                	div    %edi
  801c42:	89 c5                	mov    %eax,%ebp
  801c44:	31 d2                	xor    %edx,%edx
  801c46:	89 c8                	mov    %ecx,%eax
  801c48:	f7 f5                	div    %ebp
  801c4a:	89 c1                	mov    %eax,%ecx
  801c4c:	89 d8                	mov    %ebx,%eax
  801c4e:	f7 f5                	div    %ebp
  801c50:	89 cf                	mov    %ecx,%edi
  801c52:	89 fa                	mov    %edi,%edx
  801c54:	83 c4 1c             	add    $0x1c,%esp
  801c57:	5b                   	pop    %ebx
  801c58:	5e                   	pop    %esi
  801c59:	5f                   	pop    %edi
  801c5a:	5d                   	pop    %ebp
  801c5b:	c3                   	ret    
  801c5c:	39 ce                	cmp    %ecx,%esi
  801c5e:	77 28                	ja     801c88 <__udivdi3+0x7c>
  801c60:	0f bd fe             	bsr    %esi,%edi
  801c63:	83 f7 1f             	xor    $0x1f,%edi
  801c66:	75 40                	jne    801ca8 <__udivdi3+0x9c>
  801c68:	39 ce                	cmp    %ecx,%esi
  801c6a:	72 0a                	jb     801c76 <__udivdi3+0x6a>
  801c6c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801c70:	0f 87 9e 00 00 00    	ja     801d14 <__udivdi3+0x108>
  801c76:	b8 01 00 00 00       	mov    $0x1,%eax
  801c7b:	89 fa                	mov    %edi,%edx
  801c7d:	83 c4 1c             	add    $0x1c,%esp
  801c80:	5b                   	pop    %ebx
  801c81:	5e                   	pop    %esi
  801c82:	5f                   	pop    %edi
  801c83:	5d                   	pop    %ebp
  801c84:	c3                   	ret    
  801c85:	8d 76 00             	lea    0x0(%esi),%esi
  801c88:	31 ff                	xor    %edi,%edi
  801c8a:	31 c0                	xor    %eax,%eax
  801c8c:	89 fa                	mov    %edi,%edx
  801c8e:	83 c4 1c             	add    $0x1c,%esp
  801c91:	5b                   	pop    %ebx
  801c92:	5e                   	pop    %esi
  801c93:	5f                   	pop    %edi
  801c94:	5d                   	pop    %ebp
  801c95:	c3                   	ret    
  801c96:	66 90                	xchg   %ax,%ax
  801c98:	89 d8                	mov    %ebx,%eax
  801c9a:	f7 f7                	div    %edi
  801c9c:	31 ff                	xor    %edi,%edi
  801c9e:	89 fa                	mov    %edi,%edx
  801ca0:	83 c4 1c             	add    $0x1c,%esp
  801ca3:	5b                   	pop    %ebx
  801ca4:	5e                   	pop    %esi
  801ca5:	5f                   	pop    %edi
  801ca6:	5d                   	pop    %ebp
  801ca7:	c3                   	ret    
  801ca8:	bd 20 00 00 00       	mov    $0x20,%ebp
  801cad:	89 eb                	mov    %ebp,%ebx
  801caf:	29 fb                	sub    %edi,%ebx
  801cb1:	89 f9                	mov    %edi,%ecx
  801cb3:	d3 e6                	shl    %cl,%esi
  801cb5:	89 c5                	mov    %eax,%ebp
  801cb7:	88 d9                	mov    %bl,%cl
  801cb9:	d3 ed                	shr    %cl,%ebp
  801cbb:	89 e9                	mov    %ebp,%ecx
  801cbd:	09 f1                	or     %esi,%ecx
  801cbf:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801cc3:	89 f9                	mov    %edi,%ecx
  801cc5:	d3 e0                	shl    %cl,%eax
  801cc7:	89 c5                	mov    %eax,%ebp
  801cc9:	89 d6                	mov    %edx,%esi
  801ccb:	88 d9                	mov    %bl,%cl
  801ccd:	d3 ee                	shr    %cl,%esi
  801ccf:	89 f9                	mov    %edi,%ecx
  801cd1:	d3 e2                	shl    %cl,%edx
  801cd3:	8b 44 24 08          	mov    0x8(%esp),%eax
  801cd7:	88 d9                	mov    %bl,%cl
  801cd9:	d3 e8                	shr    %cl,%eax
  801cdb:	09 c2                	or     %eax,%edx
  801cdd:	89 d0                	mov    %edx,%eax
  801cdf:	89 f2                	mov    %esi,%edx
  801ce1:	f7 74 24 0c          	divl   0xc(%esp)
  801ce5:	89 d6                	mov    %edx,%esi
  801ce7:	89 c3                	mov    %eax,%ebx
  801ce9:	f7 e5                	mul    %ebp
  801ceb:	39 d6                	cmp    %edx,%esi
  801ced:	72 19                	jb     801d08 <__udivdi3+0xfc>
  801cef:	74 0b                	je     801cfc <__udivdi3+0xf0>
  801cf1:	89 d8                	mov    %ebx,%eax
  801cf3:	31 ff                	xor    %edi,%edi
  801cf5:	e9 58 ff ff ff       	jmp    801c52 <__udivdi3+0x46>
  801cfa:	66 90                	xchg   %ax,%ax
  801cfc:	8b 54 24 08          	mov    0x8(%esp),%edx
  801d00:	89 f9                	mov    %edi,%ecx
  801d02:	d3 e2                	shl    %cl,%edx
  801d04:	39 c2                	cmp    %eax,%edx
  801d06:	73 e9                	jae    801cf1 <__udivdi3+0xe5>
  801d08:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801d0b:	31 ff                	xor    %edi,%edi
  801d0d:	e9 40 ff ff ff       	jmp    801c52 <__udivdi3+0x46>
  801d12:	66 90                	xchg   %ax,%ax
  801d14:	31 c0                	xor    %eax,%eax
  801d16:	e9 37 ff ff ff       	jmp    801c52 <__udivdi3+0x46>
  801d1b:	90                   	nop

00801d1c <__umoddi3>:
  801d1c:	55                   	push   %ebp
  801d1d:	57                   	push   %edi
  801d1e:	56                   	push   %esi
  801d1f:	53                   	push   %ebx
  801d20:	83 ec 1c             	sub    $0x1c,%esp
  801d23:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  801d27:	8b 74 24 34          	mov    0x34(%esp),%esi
  801d2b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801d2f:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801d33:	89 44 24 0c          	mov    %eax,0xc(%esp)
  801d37:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801d3b:	89 f3                	mov    %esi,%ebx
  801d3d:	89 fa                	mov    %edi,%edx
  801d3f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801d43:	89 34 24             	mov    %esi,(%esp)
  801d46:	85 c0                	test   %eax,%eax
  801d48:	75 1a                	jne    801d64 <__umoddi3+0x48>
  801d4a:	39 f7                	cmp    %esi,%edi
  801d4c:	0f 86 a2 00 00 00    	jbe    801df4 <__umoddi3+0xd8>
  801d52:	89 c8                	mov    %ecx,%eax
  801d54:	89 f2                	mov    %esi,%edx
  801d56:	f7 f7                	div    %edi
  801d58:	89 d0                	mov    %edx,%eax
  801d5a:	31 d2                	xor    %edx,%edx
  801d5c:	83 c4 1c             	add    $0x1c,%esp
  801d5f:	5b                   	pop    %ebx
  801d60:	5e                   	pop    %esi
  801d61:	5f                   	pop    %edi
  801d62:	5d                   	pop    %ebp
  801d63:	c3                   	ret    
  801d64:	39 f0                	cmp    %esi,%eax
  801d66:	0f 87 ac 00 00 00    	ja     801e18 <__umoddi3+0xfc>
  801d6c:	0f bd e8             	bsr    %eax,%ebp
  801d6f:	83 f5 1f             	xor    $0x1f,%ebp
  801d72:	0f 84 ac 00 00 00    	je     801e24 <__umoddi3+0x108>
  801d78:	bf 20 00 00 00       	mov    $0x20,%edi
  801d7d:	29 ef                	sub    %ebp,%edi
  801d7f:	89 fe                	mov    %edi,%esi
  801d81:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801d85:	89 e9                	mov    %ebp,%ecx
  801d87:	d3 e0                	shl    %cl,%eax
  801d89:	89 d7                	mov    %edx,%edi
  801d8b:	89 f1                	mov    %esi,%ecx
  801d8d:	d3 ef                	shr    %cl,%edi
  801d8f:	09 c7                	or     %eax,%edi
  801d91:	89 e9                	mov    %ebp,%ecx
  801d93:	d3 e2                	shl    %cl,%edx
  801d95:	89 14 24             	mov    %edx,(%esp)
  801d98:	89 d8                	mov    %ebx,%eax
  801d9a:	d3 e0                	shl    %cl,%eax
  801d9c:	89 c2                	mov    %eax,%edx
  801d9e:	8b 44 24 08          	mov    0x8(%esp),%eax
  801da2:	d3 e0                	shl    %cl,%eax
  801da4:	89 44 24 04          	mov    %eax,0x4(%esp)
  801da8:	8b 44 24 08          	mov    0x8(%esp),%eax
  801dac:	89 f1                	mov    %esi,%ecx
  801dae:	d3 e8                	shr    %cl,%eax
  801db0:	09 d0                	or     %edx,%eax
  801db2:	d3 eb                	shr    %cl,%ebx
  801db4:	89 da                	mov    %ebx,%edx
  801db6:	f7 f7                	div    %edi
  801db8:	89 d3                	mov    %edx,%ebx
  801dba:	f7 24 24             	mull   (%esp)
  801dbd:	89 c6                	mov    %eax,%esi
  801dbf:	89 d1                	mov    %edx,%ecx
  801dc1:	39 d3                	cmp    %edx,%ebx
  801dc3:	0f 82 87 00 00 00    	jb     801e50 <__umoddi3+0x134>
  801dc9:	0f 84 91 00 00 00    	je     801e60 <__umoddi3+0x144>
  801dcf:	8b 54 24 04          	mov    0x4(%esp),%edx
  801dd3:	29 f2                	sub    %esi,%edx
  801dd5:	19 cb                	sbb    %ecx,%ebx
  801dd7:	89 d8                	mov    %ebx,%eax
  801dd9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801ddd:	d3 e0                	shl    %cl,%eax
  801ddf:	89 e9                	mov    %ebp,%ecx
  801de1:	d3 ea                	shr    %cl,%edx
  801de3:	09 d0                	or     %edx,%eax
  801de5:	89 e9                	mov    %ebp,%ecx
  801de7:	d3 eb                	shr    %cl,%ebx
  801de9:	89 da                	mov    %ebx,%edx
  801deb:	83 c4 1c             	add    $0x1c,%esp
  801dee:	5b                   	pop    %ebx
  801def:	5e                   	pop    %esi
  801df0:	5f                   	pop    %edi
  801df1:	5d                   	pop    %ebp
  801df2:	c3                   	ret    
  801df3:	90                   	nop
  801df4:	89 fd                	mov    %edi,%ebp
  801df6:	85 ff                	test   %edi,%edi
  801df8:	75 0b                	jne    801e05 <__umoddi3+0xe9>
  801dfa:	b8 01 00 00 00       	mov    $0x1,%eax
  801dff:	31 d2                	xor    %edx,%edx
  801e01:	f7 f7                	div    %edi
  801e03:	89 c5                	mov    %eax,%ebp
  801e05:	89 f0                	mov    %esi,%eax
  801e07:	31 d2                	xor    %edx,%edx
  801e09:	f7 f5                	div    %ebp
  801e0b:	89 c8                	mov    %ecx,%eax
  801e0d:	f7 f5                	div    %ebp
  801e0f:	89 d0                	mov    %edx,%eax
  801e11:	e9 44 ff ff ff       	jmp    801d5a <__umoddi3+0x3e>
  801e16:	66 90                	xchg   %ax,%ax
  801e18:	89 c8                	mov    %ecx,%eax
  801e1a:	89 f2                	mov    %esi,%edx
  801e1c:	83 c4 1c             	add    $0x1c,%esp
  801e1f:	5b                   	pop    %ebx
  801e20:	5e                   	pop    %esi
  801e21:	5f                   	pop    %edi
  801e22:	5d                   	pop    %ebp
  801e23:	c3                   	ret    
  801e24:	3b 04 24             	cmp    (%esp),%eax
  801e27:	72 06                	jb     801e2f <__umoddi3+0x113>
  801e29:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801e2d:	77 0f                	ja     801e3e <__umoddi3+0x122>
  801e2f:	89 f2                	mov    %esi,%edx
  801e31:	29 f9                	sub    %edi,%ecx
  801e33:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  801e37:	89 14 24             	mov    %edx,(%esp)
  801e3a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801e3e:	8b 44 24 04          	mov    0x4(%esp),%eax
  801e42:	8b 14 24             	mov    (%esp),%edx
  801e45:	83 c4 1c             	add    $0x1c,%esp
  801e48:	5b                   	pop    %ebx
  801e49:	5e                   	pop    %esi
  801e4a:	5f                   	pop    %edi
  801e4b:	5d                   	pop    %ebp
  801e4c:	c3                   	ret    
  801e4d:	8d 76 00             	lea    0x0(%esi),%esi
  801e50:	2b 04 24             	sub    (%esp),%eax
  801e53:	19 fa                	sbb    %edi,%edx
  801e55:	89 d1                	mov    %edx,%ecx
  801e57:	89 c6                	mov    %eax,%esi
  801e59:	e9 71 ff ff ff       	jmp    801dcf <__umoddi3+0xb3>
  801e5e:	66 90                	xchg   %ax,%ax
  801e60:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801e64:	72 ea                	jb     801e50 <__umoddi3+0x134>
  801e66:	89 d9                	mov    %ebx,%ecx
  801e68:	e9 62 ff ff ff       	jmp    801dcf <__umoddi3+0xb3>
