
obj/user/spin.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 92 00 00 00       	call   8000c3 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 10             	sub    $0x10,%esp
	envid_t env;

	cprintf("I am the parent.  Forking the child...\n");
  80003a:	68 20 13 80 00       	push   $0x801320
  80003f:	e8 72 01 00 00       	call   8001b6 <cprintf>
	if ((env = fork()) == 0) {
  800044:	e8 77 0d 00 00       	call   800dc0 <fork>
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	85 c0                	test   %eax,%eax
  80004e:	75 12                	jne    800062 <umain+0x2f>
		cprintf("I am the child.  Spinning...\n");
  800050:	83 ec 0c             	sub    $0xc,%esp
  800053:	68 bb 13 80 00       	push   $0x8013bb
  800058:	e8 59 01 00 00       	call   8001b6 <cprintf>
  80005d:	83 c4 10             	add    $0x10,%esp
  800060:	eb fe                	jmp    800060 <umain+0x2d>
  800062:	89 c3                	mov    %eax,%ebx
		while (1)
			/* do nothing */;
	}

	cprintf("I am the parent.  Running the child...\n");
  800064:	83 ec 0c             	sub    $0xc,%esp
  800067:	68 48 13 80 00       	push   $0x801348
  80006c:	e8 45 01 00 00       	call   8001b6 <cprintf>
	sys_yield();
  800071:	e8 69 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  800076:	e8 64 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  80007b:	e8 5f 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  800080:	e8 5a 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  800085:	e8 55 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  80008a:	e8 50 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  80008f:	e8 4b 0a 00 00       	call   800adf <sys_yield>
	sys_yield();
  800094:	e8 46 0a 00 00       	call   800adf <sys_yield>
	//if(env != 0){
		cprintf("I am the parent.  Killing the child...\n");
  800099:	c7 04 24 70 13 80 00 	movl   $0x801370,(%esp)
  8000a0:	e8 11 01 00 00       	call   8001b6 <cprintf>
		cprintf("env_id to sys_env_destroy() is %d\n", env);
  8000a5:	83 c4 08             	add    $0x8,%esp
  8000a8:	53                   	push   %ebx
  8000a9:	68 98 13 80 00       	push   $0x801398
  8000ae:	e8 03 01 00 00       	call   8001b6 <cprintf>
		sys_env_destroy(env);
  8000b3:	89 1c 24             	mov    %ebx,(%esp)
  8000b6:	e8 c4 09 00 00       	call   800a7f <sys_env_destroy>
	//}
}
  8000bb:	83 c4 10             	add    $0x10,%esp
  8000be:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000c1:	c9                   	leave  
  8000c2:	c3                   	ret    

008000c3 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000c3:	55                   	push   %ebp
  8000c4:	89 e5                	mov    %esp,%ebp
  8000c6:	56                   	push   %esi
  8000c7:	53                   	push   %ebx
  8000c8:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000cb:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000ce:	e8 ed 09 00 00       	call   800ac0 <sys_getenvid>
  8000d3:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000d8:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000df:	c1 e0 07             	shl    $0x7,%eax
  8000e2:	29 d0                	sub    %edx,%eax
  8000e4:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000e9:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000ee:	85 db                	test   %ebx,%ebx
  8000f0:	7e 07                	jle    8000f9 <libmain+0x36>
		binaryname = argv[0];
  8000f2:	8b 06                	mov    (%esi),%eax
  8000f4:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000f9:	83 ec 08             	sub    $0x8,%esp
  8000fc:	56                   	push   %esi
  8000fd:	53                   	push   %ebx
  8000fe:	e8 30 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800103:	e8 0a 00 00 00       	call   800112 <exit>
}
  800108:	83 c4 10             	add    $0x10,%esp
  80010b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80010e:	5b                   	pop    %ebx
  80010f:	5e                   	pop    %esi
  800110:	5d                   	pop    %ebp
  800111:	c3                   	ret    

00800112 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800112:	55                   	push   %ebp
  800113:	89 e5                	mov    %esp,%ebp
  800115:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800118:	6a 00                	push   $0x0
  80011a:	e8 60 09 00 00       	call   800a7f <sys_env_destroy>
}
  80011f:	83 c4 10             	add    $0x10,%esp
  800122:	c9                   	leave  
  800123:	c3                   	ret    

00800124 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800124:	55                   	push   %ebp
  800125:	89 e5                	mov    %esp,%ebp
  800127:	53                   	push   %ebx
  800128:	83 ec 04             	sub    $0x4,%esp
  80012b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80012e:	8b 13                	mov    (%ebx),%edx
  800130:	8d 42 01             	lea    0x1(%edx),%eax
  800133:	89 03                	mov    %eax,(%ebx)
  800135:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800138:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80013c:	3d ff 00 00 00       	cmp    $0xff,%eax
  800141:	75 1a                	jne    80015d <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800143:	83 ec 08             	sub    $0x8,%esp
  800146:	68 ff 00 00 00       	push   $0xff
  80014b:	8d 43 08             	lea    0x8(%ebx),%eax
  80014e:	50                   	push   %eax
  80014f:	e8 ee 08 00 00       	call   800a42 <sys_cputs>
		b->idx = 0;
  800154:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80015a:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80015d:	ff 43 04             	incl   0x4(%ebx)
}
  800160:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800163:	c9                   	leave  
  800164:	c3                   	ret    

00800165 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800165:	55                   	push   %ebp
  800166:	89 e5                	mov    %esp,%ebp
  800168:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80016e:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800175:	00 00 00 
	b.cnt = 0;
  800178:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80017f:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800182:	ff 75 0c             	pushl  0xc(%ebp)
  800185:	ff 75 08             	pushl  0x8(%ebp)
  800188:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80018e:	50                   	push   %eax
  80018f:	68 24 01 80 00       	push   $0x800124
  800194:	e8 51 01 00 00       	call   8002ea <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800199:	83 c4 08             	add    $0x8,%esp
  80019c:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001a2:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001a8:	50                   	push   %eax
  8001a9:	e8 94 08 00 00       	call   800a42 <sys_cputs>

	return b.cnt;
}
  8001ae:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001b4:	c9                   	leave  
  8001b5:	c3                   	ret    

008001b6 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001b6:	55                   	push   %ebp
  8001b7:	89 e5                	mov    %esp,%ebp
  8001b9:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001bc:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8001bf:	50                   	push   %eax
  8001c0:	ff 75 08             	pushl  0x8(%ebp)
  8001c3:	e8 9d ff ff ff       	call   800165 <vcprintf>
	va_end(ap);

	return cnt;
}
  8001c8:	c9                   	leave  
  8001c9:	c3                   	ret    

008001ca <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001ca:	55                   	push   %ebp
  8001cb:	89 e5                	mov    %esp,%ebp
  8001cd:	57                   	push   %edi
  8001ce:	56                   	push   %esi
  8001cf:	53                   	push   %ebx
  8001d0:	83 ec 1c             	sub    $0x1c,%esp
  8001d3:	89 c7                	mov    %eax,%edi
  8001d5:	89 d6                	mov    %edx,%esi
  8001d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8001da:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001dd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001e0:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001e3:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001e6:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001eb:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001ee:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001f1:	39 d3                	cmp    %edx,%ebx
  8001f3:	72 05                	jb     8001fa <printnum+0x30>
  8001f5:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001f8:	77 45                	ja     80023f <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001fa:	83 ec 0c             	sub    $0xc,%esp
  8001fd:	ff 75 18             	pushl  0x18(%ebp)
  800200:	8b 45 14             	mov    0x14(%ebp),%eax
  800203:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800206:	53                   	push   %ebx
  800207:	ff 75 10             	pushl  0x10(%ebp)
  80020a:	83 ec 08             	sub    $0x8,%esp
  80020d:	ff 75 e4             	pushl  -0x1c(%ebp)
  800210:	ff 75 e0             	pushl  -0x20(%ebp)
  800213:	ff 75 dc             	pushl  -0x24(%ebp)
  800216:	ff 75 d8             	pushl  -0x28(%ebp)
  800219:	e8 8e 0e 00 00       	call   8010ac <__udivdi3>
  80021e:	83 c4 18             	add    $0x18,%esp
  800221:	52                   	push   %edx
  800222:	50                   	push   %eax
  800223:	89 f2                	mov    %esi,%edx
  800225:	89 f8                	mov    %edi,%eax
  800227:	e8 9e ff ff ff       	call   8001ca <printnum>
  80022c:	83 c4 20             	add    $0x20,%esp
  80022f:	eb 16                	jmp    800247 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800231:	83 ec 08             	sub    $0x8,%esp
  800234:	56                   	push   %esi
  800235:	ff 75 18             	pushl  0x18(%ebp)
  800238:	ff d7                	call   *%edi
  80023a:	83 c4 10             	add    $0x10,%esp
  80023d:	eb 03                	jmp    800242 <printnum+0x78>
  80023f:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800242:	4b                   	dec    %ebx
  800243:	85 db                	test   %ebx,%ebx
  800245:	7f ea                	jg     800231 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800247:	83 ec 08             	sub    $0x8,%esp
  80024a:	56                   	push   %esi
  80024b:	83 ec 04             	sub    $0x4,%esp
  80024e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800251:	ff 75 e0             	pushl  -0x20(%ebp)
  800254:	ff 75 dc             	pushl  -0x24(%ebp)
  800257:	ff 75 d8             	pushl  -0x28(%ebp)
  80025a:	e8 5d 0f 00 00       	call   8011bc <__umoddi3>
  80025f:	83 c4 14             	add    $0x14,%esp
  800262:	0f be 80 e3 13 80 00 	movsbl 0x8013e3(%eax),%eax
  800269:	50                   	push   %eax
  80026a:	ff d7                	call   *%edi
}
  80026c:	83 c4 10             	add    $0x10,%esp
  80026f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800272:	5b                   	pop    %ebx
  800273:	5e                   	pop    %esi
  800274:	5f                   	pop    %edi
  800275:	5d                   	pop    %ebp
  800276:	c3                   	ret    

00800277 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800277:	55                   	push   %ebp
  800278:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80027a:	83 fa 01             	cmp    $0x1,%edx
  80027d:	7e 0e                	jle    80028d <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80027f:	8b 10                	mov    (%eax),%edx
  800281:	8d 4a 08             	lea    0x8(%edx),%ecx
  800284:	89 08                	mov    %ecx,(%eax)
  800286:	8b 02                	mov    (%edx),%eax
  800288:	8b 52 04             	mov    0x4(%edx),%edx
  80028b:	eb 22                	jmp    8002af <getuint+0x38>
	else if (lflag)
  80028d:	85 d2                	test   %edx,%edx
  80028f:	74 10                	je     8002a1 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800291:	8b 10                	mov    (%eax),%edx
  800293:	8d 4a 04             	lea    0x4(%edx),%ecx
  800296:	89 08                	mov    %ecx,(%eax)
  800298:	8b 02                	mov    (%edx),%eax
  80029a:	ba 00 00 00 00       	mov    $0x0,%edx
  80029f:	eb 0e                	jmp    8002af <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002a1:	8b 10                	mov    (%eax),%edx
  8002a3:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002a6:	89 08                	mov    %ecx,(%eax)
  8002a8:	8b 02                	mov    (%edx),%eax
  8002aa:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002af:	5d                   	pop    %ebp
  8002b0:	c3                   	ret    

008002b1 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002b1:	55                   	push   %ebp
  8002b2:	89 e5                	mov    %esp,%ebp
  8002b4:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002b7:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8002ba:	8b 10                	mov    (%eax),%edx
  8002bc:	3b 50 04             	cmp    0x4(%eax),%edx
  8002bf:	73 0a                	jae    8002cb <sprintputch+0x1a>
		*b->buf++ = ch;
  8002c1:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002c4:	89 08                	mov    %ecx,(%eax)
  8002c6:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c9:	88 02                	mov    %al,(%edx)
}
  8002cb:	5d                   	pop    %ebp
  8002cc:	c3                   	ret    

008002cd <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002cd:	55                   	push   %ebp
  8002ce:	89 e5                	mov    %esp,%ebp
  8002d0:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002d3:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002d6:	50                   	push   %eax
  8002d7:	ff 75 10             	pushl  0x10(%ebp)
  8002da:	ff 75 0c             	pushl  0xc(%ebp)
  8002dd:	ff 75 08             	pushl  0x8(%ebp)
  8002e0:	e8 05 00 00 00       	call   8002ea <vprintfmt>
	va_end(ap);
}
  8002e5:	83 c4 10             	add    $0x10,%esp
  8002e8:	c9                   	leave  
  8002e9:	c3                   	ret    

008002ea <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002ea:	55                   	push   %ebp
  8002eb:	89 e5                	mov    %esp,%ebp
  8002ed:	57                   	push   %edi
  8002ee:	56                   	push   %esi
  8002ef:	53                   	push   %ebx
  8002f0:	83 ec 2c             	sub    $0x2c,%esp
  8002f3:	8b 75 08             	mov    0x8(%ebp),%esi
  8002f6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002f9:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002fc:	eb 12                	jmp    800310 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002fe:	85 c0                	test   %eax,%eax
  800300:	0f 84 68 03 00 00    	je     80066e <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800306:	83 ec 08             	sub    $0x8,%esp
  800309:	53                   	push   %ebx
  80030a:	50                   	push   %eax
  80030b:	ff d6                	call   *%esi
  80030d:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800310:	47                   	inc    %edi
  800311:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800315:	83 f8 25             	cmp    $0x25,%eax
  800318:	75 e4                	jne    8002fe <vprintfmt+0x14>
  80031a:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80031e:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800325:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80032c:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800333:	ba 00 00 00 00       	mov    $0x0,%edx
  800338:	eb 07                	jmp    800341 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80033a:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80033d:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800341:	8d 47 01             	lea    0x1(%edi),%eax
  800344:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800347:	0f b6 0f             	movzbl (%edi),%ecx
  80034a:	8a 07                	mov    (%edi),%al
  80034c:	83 e8 23             	sub    $0x23,%eax
  80034f:	3c 55                	cmp    $0x55,%al
  800351:	0f 87 fe 02 00 00    	ja     800655 <vprintfmt+0x36b>
  800357:	0f b6 c0             	movzbl %al,%eax
  80035a:	ff 24 85 20 15 80 00 	jmp    *0x801520(,%eax,4)
  800361:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800364:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800368:	eb d7                	jmp    800341 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80036a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80036d:	b8 00 00 00 00       	mov    $0x0,%eax
  800372:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800375:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800378:	01 c0                	add    %eax,%eax
  80037a:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80037e:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800381:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800384:	83 fa 09             	cmp    $0x9,%edx
  800387:	77 34                	ja     8003bd <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800389:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80038a:	eb e9                	jmp    800375 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80038c:	8b 45 14             	mov    0x14(%ebp),%eax
  80038f:	8d 48 04             	lea    0x4(%eax),%ecx
  800392:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800395:	8b 00                	mov    (%eax),%eax
  800397:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80039d:	eb 24                	jmp    8003c3 <vprintfmt+0xd9>
  80039f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003a3:	79 07                	jns    8003ac <vprintfmt+0xc2>
  8003a5:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ac:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003af:	eb 90                	jmp    800341 <vprintfmt+0x57>
  8003b1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003b4:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8003bb:	eb 84                	jmp    800341 <vprintfmt+0x57>
  8003bd:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003c0:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003c3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003c7:	0f 89 74 ff ff ff    	jns    800341 <vprintfmt+0x57>
				width = precision, precision = -1;
  8003cd:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003d0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003d3:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003da:	e9 62 ff ff ff       	jmp    800341 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003df:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003e3:	e9 59 ff ff ff       	jmp    800341 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003e8:	8b 45 14             	mov    0x14(%ebp),%eax
  8003eb:	8d 50 04             	lea    0x4(%eax),%edx
  8003ee:	89 55 14             	mov    %edx,0x14(%ebp)
  8003f1:	83 ec 08             	sub    $0x8,%esp
  8003f4:	53                   	push   %ebx
  8003f5:	ff 30                	pushl  (%eax)
  8003f7:	ff d6                	call   *%esi
			break;
  8003f9:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003fc:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003ff:	e9 0c ff ff ff       	jmp    800310 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800404:	8b 45 14             	mov    0x14(%ebp),%eax
  800407:	8d 50 04             	lea    0x4(%eax),%edx
  80040a:	89 55 14             	mov    %edx,0x14(%ebp)
  80040d:	8b 00                	mov    (%eax),%eax
  80040f:	85 c0                	test   %eax,%eax
  800411:	79 02                	jns    800415 <vprintfmt+0x12b>
  800413:	f7 d8                	neg    %eax
  800415:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800417:	83 f8 0f             	cmp    $0xf,%eax
  80041a:	7f 0b                	jg     800427 <vprintfmt+0x13d>
  80041c:	8b 04 85 80 16 80 00 	mov    0x801680(,%eax,4),%eax
  800423:	85 c0                	test   %eax,%eax
  800425:	75 18                	jne    80043f <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800427:	52                   	push   %edx
  800428:	68 fb 13 80 00       	push   $0x8013fb
  80042d:	53                   	push   %ebx
  80042e:	56                   	push   %esi
  80042f:	e8 99 fe ff ff       	call   8002cd <printfmt>
  800434:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800437:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80043a:	e9 d1 fe ff ff       	jmp    800310 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80043f:	50                   	push   %eax
  800440:	68 04 14 80 00       	push   $0x801404
  800445:	53                   	push   %ebx
  800446:	56                   	push   %esi
  800447:	e8 81 fe ff ff       	call   8002cd <printfmt>
  80044c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80044f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800452:	e9 b9 fe ff ff       	jmp    800310 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800457:	8b 45 14             	mov    0x14(%ebp),%eax
  80045a:	8d 50 04             	lea    0x4(%eax),%edx
  80045d:	89 55 14             	mov    %edx,0x14(%ebp)
  800460:	8b 38                	mov    (%eax),%edi
  800462:	85 ff                	test   %edi,%edi
  800464:	75 05                	jne    80046b <vprintfmt+0x181>
				p = "(null)";
  800466:	bf f4 13 80 00       	mov    $0x8013f4,%edi
			if (width > 0 && padc != '-')
  80046b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80046f:	0f 8e 90 00 00 00    	jle    800505 <vprintfmt+0x21b>
  800475:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800479:	0f 84 8e 00 00 00    	je     80050d <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80047f:	83 ec 08             	sub    $0x8,%esp
  800482:	ff 75 d0             	pushl  -0x30(%ebp)
  800485:	57                   	push   %edi
  800486:	e8 70 02 00 00       	call   8006fb <strnlen>
  80048b:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80048e:	29 c1                	sub    %eax,%ecx
  800490:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800493:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800496:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80049a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80049d:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004a0:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004a2:	eb 0d                	jmp    8004b1 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004a4:	83 ec 08             	sub    $0x8,%esp
  8004a7:	53                   	push   %ebx
  8004a8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004ab:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004ad:	4f                   	dec    %edi
  8004ae:	83 c4 10             	add    $0x10,%esp
  8004b1:	85 ff                	test   %edi,%edi
  8004b3:	7f ef                	jg     8004a4 <vprintfmt+0x1ba>
  8004b5:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8004b8:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004bb:	89 c8                	mov    %ecx,%eax
  8004bd:	85 c9                	test   %ecx,%ecx
  8004bf:	79 05                	jns    8004c6 <vprintfmt+0x1dc>
  8004c1:	b8 00 00 00 00       	mov    $0x0,%eax
  8004c6:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004c9:	29 c1                	sub    %eax,%ecx
  8004cb:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004ce:	89 75 08             	mov    %esi,0x8(%ebp)
  8004d1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d4:	eb 3d                	jmp    800513 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004d6:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004da:	74 19                	je     8004f5 <vprintfmt+0x20b>
  8004dc:	0f be c0             	movsbl %al,%eax
  8004df:	83 e8 20             	sub    $0x20,%eax
  8004e2:	83 f8 5e             	cmp    $0x5e,%eax
  8004e5:	76 0e                	jbe    8004f5 <vprintfmt+0x20b>
					putch('?', putdat);
  8004e7:	83 ec 08             	sub    $0x8,%esp
  8004ea:	53                   	push   %ebx
  8004eb:	6a 3f                	push   $0x3f
  8004ed:	ff 55 08             	call   *0x8(%ebp)
  8004f0:	83 c4 10             	add    $0x10,%esp
  8004f3:	eb 0b                	jmp    800500 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004f5:	83 ec 08             	sub    $0x8,%esp
  8004f8:	53                   	push   %ebx
  8004f9:	52                   	push   %edx
  8004fa:	ff 55 08             	call   *0x8(%ebp)
  8004fd:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800500:	ff 4d e4             	decl   -0x1c(%ebp)
  800503:	eb 0e                	jmp    800513 <vprintfmt+0x229>
  800505:	89 75 08             	mov    %esi,0x8(%ebp)
  800508:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80050b:	eb 06                	jmp    800513 <vprintfmt+0x229>
  80050d:	89 75 08             	mov    %esi,0x8(%ebp)
  800510:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800513:	47                   	inc    %edi
  800514:	8a 47 ff             	mov    -0x1(%edi),%al
  800517:	0f be d0             	movsbl %al,%edx
  80051a:	85 d2                	test   %edx,%edx
  80051c:	74 1d                	je     80053b <vprintfmt+0x251>
  80051e:	85 f6                	test   %esi,%esi
  800520:	78 b4                	js     8004d6 <vprintfmt+0x1ec>
  800522:	4e                   	dec    %esi
  800523:	79 b1                	jns    8004d6 <vprintfmt+0x1ec>
  800525:	8b 75 08             	mov    0x8(%ebp),%esi
  800528:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80052b:	eb 14                	jmp    800541 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80052d:	83 ec 08             	sub    $0x8,%esp
  800530:	53                   	push   %ebx
  800531:	6a 20                	push   $0x20
  800533:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800535:	4f                   	dec    %edi
  800536:	83 c4 10             	add    $0x10,%esp
  800539:	eb 06                	jmp    800541 <vprintfmt+0x257>
  80053b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80053e:	8b 75 08             	mov    0x8(%ebp),%esi
  800541:	85 ff                	test   %edi,%edi
  800543:	7f e8                	jg     80052d <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800545:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800548:	e9 c3 fd ff ff       	jmp    800310 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80054d:	83 fa 01             	cmp    $0x1,%edx
  800550:	7e 16                	jle    800568 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800552:	8b 45 14             	mov    0x14(%ebp),%eax
  800555:	8d 50 08             	lea    0x8(%eax),%edx
  800558:	89 55 14             	mov    %edx,0x14(%ebp)
  80055b:	8b 50 04             	mov    0x4(%eax),%edx
  80055e:	8b 00                	mov    (%eax),%eax
  800560:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800563:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800566:	eb 32                	jmp    80059a <vprintfmt+0x2b0>
	else if (lflag)
  800568:	85 d2                	test   %edx,%edx
  80056a:	74 18                	je     800584 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80056c:	8b 45 14             	mov    0x14(%ebp),%eax
  80056f:	8d 50 04             	lea    0x4(%eax),%edx
  800572:	89 55 14             	mov    %edx,0x14(%ebp)
  800575:	8b 00                	mov    (%eax),%eax
  800577:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80057a:	89 c1                	mov    %eax,%ecx
  80057c:	c1 f9 1f             	sar    $0x1f,%ecx
  80057f:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800582:	eb 16                	jmp    80059a <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800584:	8b 45 14             	mov    0x14(%ebp),%eax
  800587:	8d 50 04             	lea    0x4(%eax),%edx
  80058a:	89 55 14             	mov    %edx,0x14(%ebp)
  80058d:	8b 00                	mov    (%eax),%eax
  80058f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800592:	89 c1                	mov    %eax,%ecx
  800594:	c1 f9 1f             	sar    $0x1f,%ecx
  800597:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80059a:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80059d:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005a0:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005a4:	79 76                	jns    80061c <vprintfmt+0x332>
				putch('-', putdat);
  8005a6:	83 ec 08             	sub    $0x8,%esp
  8005a9:	53                   	push   %ebx
  8005aa:	6a 2d                	push   $0x2d
  8005ac:	ff d6                	call   *%esi
				num = -(long long) num;
  8005ae:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005b1:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005b4:	f7 d8                	neg    %eax
  8005b6:	83 d2 00             	adc    $0x0,%edx
  8005b9:	f7 da                	neg    %edx
  8005bb:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8005be:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005c3:	eb 5c                	jmp    800621 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005c5:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c8:	e8 aa fc ff ff       	call   800277 <getuint>
			base = 10;
  8005cd:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005d2:	eb 4d                	jmp    800621 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005d4:	8d 45 14             	lea    0x14(%ebp),%eax
  8005d7:	e8 9b fc ff ff       	call   800277 <getuint>
			base = 8;
  8005dc:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005e1:	eb 3e                	jmp    800621 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005e3:	83 ec 08             	sub    $0x8,%esp
  8005e6:	53                   	push   %ebx
  8005e7:	6a 30                	push   $0x30
  8005e9:	ff d6                	call   *%esi
			putch('x', putdat);
  8005eb:	83 c4 08             	add    $0x8,%esp
  8005ee:	53                   	push   %ebx
  8005ef:	6a 78                	push   $0x78
  8005f1:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005f3:	8b 45 14             	mov    0x14(%ebp),%eax
  8005f6:	8d 50 04             	lea    0x4(%eax),%edx
  8005f9:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005fc:	8b 00                	mov    (%eax),%eax
  8005fe:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800603:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800606:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80060b:	eb 14                	jmp    800621 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80060d:	8d 45 14             	lea    0x14(%ebp),%eax
  800610:	e8 62 fc ff ff       	call   800277 <getuint>
			base = 16;
  800615:	b9 10 00 00 00       	mov    $0x10,%ecx
  80061a:	eb 05                	jmp    800621 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80061c:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800621:	83 ec 0c             	sub    $0xc,%esp
  800624:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800628:	57                   	push   %edi
  800629:	ff 75 e4             	pushl  -0x1c(%ebp)
  80062c:	51                   	push   %ecx
  80062d:	52                   	push   %edx
  80062e:	50                   	push   %eax
  80062f:	89 da                	mov    %ebx,%edx
  800631:	89 f0                	mov    %esi,%eax
  800633:	e8 92 fb ff ff       	call   8001ca <printnum>
			break;
  800638:	83 c4 20             	add    $0x20,%esp
  80063b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80063e:	e9 cd fc ff ff       	jmp    800310 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800643:	83 ec 08             	sub    $0x8,%esp
  800646:	53                   	push   %ebx
  800647:	51                   	push   %ecx
  800648:	ff d6                	call   *%esi
			break;
  80064a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80064d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800650:	e9 bb fc ff ff       	jmp    800310 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800655:	83 ec 08             	sub    $0x8,%esp
  800658:	53                   	push   %ebx
  800659:	6a 25                	push   $0x25
  80065b:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80065d:	83 c4 10             	add    $0x10,%esp
  800660:	eb 01                	jmp    800663 <vprintfmt+0x379>
  800662:	4f                   	dec    %edi
  800663:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800667:	75 f9                	jne    800662 <vprintfmt+0x378>
  800669:	e9 a2 fc ff ff       	jmp    800310 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80066e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800671:	5b                   	pop    %ebx
  800672:	5e                   	pop    %esi
  800673:	5f                   	pop    %edi
  800674:	5d                   	pop    %ebp
  800675:	c3                   	ret    

00800676 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800676:	55                   	push   %ebp
  800677:	89 e5                	mov    %esp,%ebp
  800679:	83 ec 18             	sub    $0x18,%esp
  80067c:	8b 45 08             	mov    0x8(%ebp),%eax
  80067f:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800682:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800685:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800689:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80068c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800693:	85 c0                	test   %eax,%eax
  800695:	74 26                	je     8006bd <vsnprintf+0x47>
  800697:	85 d2                	test   %edx,%edx
  800699:	7e 29                	jle    8006c4 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80069b:	ff 75 14             	pushl  0x14(%ebp)
  80069e:	ff 75 10             	pushl  0x10(%ebp)
  8006a1:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006a4:	50                   	push   %eax
  8006a5:	68 b1 02 80 00       	push   $0x8002b1
  8006aa:	e8 3b fc ff ff       	call   8002ea <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006af:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006b2:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8006b8:	83 c4 10             	add    $0x10,%esp
  8006bb:	eb 0c                	jmp    8006c9 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8006bd:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006c2:	eb 05                	jmp    8006c9 <vsnprintf+0x53>
  8006c4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8006c9:	c9                   	leave  
  8006ca:	c3                   	ret    

008006cb <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006cb:	55                   	push   %ebp
  8006cc:	89 e5                	mov    %esp,%ebp
  8006ce:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006d1:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006d4:	50                   	push   %eax
  8006d5:	ff 75 10             	pushl  0x10(%ebp)
  8006d8:	ff 75 0c             	pushl  0xc(%ebp)
  8006db:	ff 75 08             	pushl  0x8(%ebp)
  8006de:	e8 93 ff ff ff       	call   800676 <vsnprintf>
	va_end(ap);

	return rc;
}
  8006e3:	c9                   	leave  
  8006e4:	c3                   	ret    

008006e5 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006e5:	55                   	push   %ebp
  8006e6:	89 e5                	mov    %esp,%ebp
  8006e8:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006eb:	b8 00 00 00 00       	mov    $0x0,%eax
  8006f0:	eb 01                	jmp    8006f3 <strlen+0xe>
		n++;
  8006f2:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006f3:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006f7:	75 f9                	jne    8006f2 <strlen+0xd>
		n++;
	return n;
}
  8006f9:	5d                   	pop    %ebp
  8006fa:	c3                   	ret    

008006fb <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006fb:	55                   	push   %ebp
  8006fc:	89 e5                	mov    %esp,%ebp
  8006fe:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800701:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800704:	ba 00 00 00 00       	mov    $0x0,%edx
  800709:	eb 01                	jmp    80070c <strnlen+0x11>
		n++;
  80070b:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80070c:	39 c2                	cmp    %eax,%edx
  80070e:	74 08                	je     800718 <strnlen+0x1d>
  800710:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800714:	75 f5                	jne    80070b <strnlen+0x10>
  800716:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800718:	5d                   	pop    %ebp
  800719:	c3                   	ret    

0080071a <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80071a:	55                   	push   %ebp
  80071b:	89 e5                	mov    %esp,%ebp
  80071d:	53                   	push   %ebx
  80071e:	8b 45 08             	mov    0x8(%ebp),%eax
  800721:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800724:	89 c2                	mov    %eax,%edx
  800726:	42                   	inc    %edx
  800727:	41                   	inc    %ecx
  800728:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80072b:	88 5a ff             	mov    %bl,-0x1(%edx)
  80072e:	84 db                	test   %bl,%bl
  800730:	75 f4                	jne    800726 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800732:	5b                   	pop    %ebx
  800733:	5d                   	pop    %ebp
  800734:	c3                   	ret    

00800735 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800735:	55                   	push   %ebp
  800736:	89 e5                	mov    %esp,%ebp
  800738:	53                   	push   %ebx
  800739:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80073c:	53                   	push   %ebx
  80073d:	e8 a3 ff ff ff       	call   8006e5 <strlen>
  800742:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800745:	ff 75 0c             	pushl  0xc(%ebp)
  800748:	01 d8                	add    %ebx,%eax
  80074a:	50                   	push   %eax
  80074b:	e8 ca ff ff ff       	call   80071a <strcpy>
	return dst;
}
  800750:	89 d8                	mov    %ebx,%eax
  800752:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800755:	c9                   	leave  
  800756:	c3                   	ret    

00800757 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800757:	55                   	push   %ebp
  800758:	89 e5                	mov    %esp,%ebp
  80075a:	56                   	push   %esi
  80075b:	53                   	push   %ebx
  80075c:	8b 75 08             	mov    0x8(%ebp),%esi
  80075f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800762:	89 f3                	mov    %esi,%ebx
  800764:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800767:	89 f2                	mov    %esi,%edx
  800769:	eb 0c                	jmp    800777 <strncpy+0x20>
		*dst++ = *src;
  80076b:	42                   	inc    %edx
  80076c:	8a 01                	mov    (%ecx),%al
  80076e:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800771:	80 39 01             	cmpb   $0x1,(%ecx)
  800774:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800777:	39 da                	cmp    %ebx,%edx
  800779:	75 f0                	jne    80076b <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80077b:	89 f0                	mov    %esi,%eax
  80077d:	5b                   	pop    %ebx
  80077e:	5e                   	pop    %esi
  80077f:	5d                   	pop    %ebp
  800780:	c3                   	ret    

00800781 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800781:	55                   	push   %ebp
  800782:	89 e5                	mov    %esp,%ebp
  800784:	56                   	push   %esi
  800785:	53                   	push   %ebx
  800786:	8b 75 08             	mov    0x8(%ebp),%esi
  800789:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80078c:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80078f:	85 c0                	test   %eax,%eax
  800791:	74 1e                	je     8007b1 <strlcpy+0x30>
  800793:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800797:	89 f2                	mov    %esi,%edx
  800799:	eb 05                	jmp    8007a0 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80079b:	42                   	inc    %edx
  80079c:	41                   	inc    %ecx
  80079d:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007a0:	39 c2                	cmp    %eax,%edx
  8007a2:	74 08                	je     8007ac <strlcpy+0x2b>
  8007a4:	8a 19                	mov    (%ecx),%bl
  8007a6:	84 db                	test   %bl,%bl
  8007a8:	75 f1                	jne    80079b <strlcpy+0x1a>
  8007aa:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007ac:	c6 00 00             	movb   $0x0,(%eax)
  8007af:	eb 02                	jmp    8007b3 <strlcpy+0x32>
  8007b1:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007b3:	29 f0                	sub    %esi,%eax
}
  8007b5:	5b                   	pop    %ebx
  8007b6:	5e                   	pop    %esi
  8007b7:	5d                   	pop    %ebp
  8007b8:	c3                   	ret    

008007b9 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8007b9:	55                   	push   %ebp
  8007ba:	89 e5                	mov    %esp,%ebp
  8007bc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007bf:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007c2:	eb 02                	jmp    8007c6 <strcmp+0xd>
		p++, q++;
  8007c4:	41                   	inc    %ecx
  8007c5:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007c6:	8a 01                	mov    (%ecx),%al
  8007c8:	84 c0                	test   %al,%al
  8007ca:	74 04                	je     8007d0 <strcmp+0x17>
  8007cc:	3a 02                	cmp    (%edx),%al
  8007ce:	74 f4                	je     8007c4 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007d0:	0f b6 c0             	movzbl %al,%eax
  8007d3:	0f b6 12             	movzbl (%edx),%edx
  8007d6:	29 d0                	sub    %edx,%eax
}
  8007d8:	5d                   	pop    %ebp
  8007d9:	c3                   	ret    

008007da <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007da:	55                   	push   %ebp
  8007db:	89 e5                	mov    %esp,%ebp
  8007dd:	53                   	push   %ebx
  8007de:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007e4:	89 c3                	mov    %eax,%ebx
  8007e6:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007e9:	eb 02                	jmp    8007ed <strncmp+0x13>
		n--, p++, q++;
  8007eb:	40                   	inc    %eax
  8007ec:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007ed:	39 d8                	cmp    %ebx,%eax
  8007ef:	74 14                	je     800805 <strncmp+0x2b>
  8007f1:	8a 08                	mov    (%eax),%cl
  8007f3:	84 c9                	test   %cl,%cl
  8007f5:	74 04                	je     8007fb <strncmp+0x21>
  8007f7:	3a 0a                	cmp    (%edx),%cl
  8007f9:	74 f0                	je     8007eb <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007fb:	0f b6 00             	movzbl (%eax),%eax
  8007fe:	0f b6 12             	movzbl (%edx),%edx
  800801:	29 d0                	sub    %edx,%eax
  800803:	eb 05                	jmp    80080a <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800805:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80080a:	5b                   	pop    %ebx
  80080b:	5d                   	pop    %ebp
  80080c:	c3                   	ret    

0080080d <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80080d:	55                   	push   %ebp
  80080e:	89 e5                	mov    %esp,%ebp
  800810:	8b 45 08             	mov    0x8(%ebp),%eax
  800813:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800816:	eb 05                	jmp    80081d <strchr+0x10>
		if (*s == c)
  800818:	38 ca                	cmp    %cl,%dl
  80081a:	74 0c                	je     800828 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80081c:	40                   	inc    %eax
  80081d:	8a 10                	mov    (%eax),%dl
  80081f:	84 d2                	test   %dl,%dl
  800821:	75 f5                	jne    800818 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800823:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800828:	5d                   	pop    %ebp
  800829:	c3                   	ret    

0080082a <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80082a:	55                   	push   %ebp
  80082b:	89 e5                	mov    %esp,%ebp
  80082d:	8b 45 08             	mov    0x8(%ebp),%eax
  800830:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800833:	eb 05                	jmp    80083a <strfind+0x10>
		if (*s == c)
  800835:	38 ca                	cmp    %cl,%dl
  800837:	74 07                	je     800840 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800839:	40                   	inc    %eax
  80083a:	8a 10                	mov    (%eax),%dl
  80083c:	84 d2                	test   %dl,%dl
  80083e:	75 f5                	jne    800835 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800840:	5d                   	pop    %ebp
  800841:	c3                   	ret    

00800842 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800842:	55                   	push   %ebp
  800843:	89 e5                	mov    %esp,%ebp
  800845:	57                   	push   %edi
  800846:	56                   	push   %esi
  800847:	53                   	push   %ebx
  800848:	8b 7d 08             	mov    0x8(%ebp),%edi
  80084b:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80084e:	85 c9                	test   %ecx,%ecx
  800850:	74 36                	je     800888 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800852:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800858:	75 28                	jne    800882 <memset+0x40>
  80085a:	f6 c1 03             	test   $0x3,%cl
  80085d:	75 23                	jne    800882 <memset+0x40>
		c &= 0xFF;
  80085f:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800863:	89 d3                	mov    %edx,%ebx
  800865:	c1 e3 08             	shl    $0x8,%ebx
  800868:	89 d6                	mov    %edx,%esi
  80086a:	c1 e6 18             	shl    $0x18,%esi
  80086d:	89 d0                	mov    %edx,%eax
  80086f:	c1 e0 10             	shl    $0x10,%eax
  800872:	09 f0                	or     %esi,%eax
  800874:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800876:	89 d8                	mov    %ebx,%eax
  800878:	09 d0                	or     %edx,%eax
  80087a:	c1 e9 02             	shr    $0x2,%ecx
  80087d:	fc                   	cld    
  80087e:	f3 ab                	rep stos %eax,%es:(%edi)
  800880:	eb 06                	jmp    800888 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800882:	8b 45 0c             	mov    0xc(%ebp),%eax
  800885:	fc                   	cld    
  800886:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800888:	89 f8                	mov    %edi,%eax
  80088a:	5b                   	pop    %ebx
  80088b:	5e                   	pop    %esi
  80088c:	5f                   	pop    %edi
  80088d:	5d                   	pop    %ebp
  80088e:	c3                   	ret    

0080088f <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80088f:	55                   	push   %ebp
  800890:	89 e5                	mov    %esp,%ebp
  800892:	57                   	push   %edi
  800893:	56                   	push   %esi
  800894:	8b 45 08             	mov    0x8(%ebp),%eax
  800897:	8b 75 0c             	mov    0xc(%ebp),%esi
  80089a:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80089d:	39 c6                	cmp    %eax,%esi
  80089f:	73 33                	jae    8008d4 <memmove+0x45>
  8008a1:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008a4:	39 d0                	cmp    %edx,%eax
  8008a6:	73 2c                	jae    8008d4 <memmove+0x45>
		s += n;
		d += n;
  8008a8:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008ab:	89 d6                	mov    %edx,%esi
  8008ad:	09 fe                	or     %edi,%esi
  8008af:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008b5:	75 13                	jne    8008ca <memmove+0x3b>
  8008b7:	f6 c1 03             	test   $0x3,%cl
  8008ba:	75 0e                	jne    8008ca <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8008bc:	83 ef 04             	sub    $0x4,%edi
  8008bf:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008c2:	c1 e9 02             	shr    $0x2,%ecx
  8008c5:	fd                   	std    
  8008c6:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008c8:	eb 07                	jmp    8008d1 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008ca:	4f                   	dec    %edi
  8008cb:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008ce:	fd                   	std    
  8008cf:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008d1:	fc                   	cld    
  8008d2:	eb 1d                	jmp    8008f1 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008d4:	89 f2                	mov    %esi,%edx
  8008d6:	09 c2                	or     %eax,%edx
  8008d8:	f6 c2 03             	test   $0x3,%dl
  8008db:	75 0f                	jne    8008ec <memmove+0x5d>
  8008dd:	f6 c1 03             	test   $0x3,%cl
  8008e0:	75 0a                	jne    8008ec <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008e2:	c1 e9 02             	shr    $0x2,%ecx
  8008e5:	89 c7                	mov    %eax,%edi
  8008e7:	fc                   	cld    
  8008e8:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008ea:	eb 05                	jmp    8008f1 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008ec:	89 c7                	mov    %eax,%edi
  8008ee:	fc                   	cld    
  8008ef:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008f1:	5e                   	pop    %esi
  8008f2:	5f                   	pop    %edi
  8008f3:	5d                   	pop    %ebp
  8008f4:	c3                   	ret    

008008f5 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008f5:	55                   	push   %ebp
  8008f6:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008f8:	ff 75 10             	pushl  0x10(%ebp)
  8008fb:	ff 75 0c             	pushl  0xc(%ebp)
  8008fe:	ff 75 08             	pushl  0x8(%ebp)
  800901:	e8 89 ff ff ff       	call   80088f <memmove>
}
  800906:	c9                   	leave  
  800907:	c3                   	ret    

00800908 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800908:	55                   	push   %ebp
  800909:	89 e5                	mov    %esp,%ebp
  80090b:	56                   	push   %esi
  80090c:	53                   	push   %ebx
  80090d:	8b 45 08             	mov    0x8(%ebp),%eax
  800910:	8b 55 0c             	mov    0xc(%ebp),%edx
  800913:	89 c6                	mov    %eax,%esi
  800915:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800918:	eb 14                	jmp    80092e <memcmp+0x26>
		if (*s1 != *s2)
  80091a:	8a 08                	mov    (%eax),%cl
  80091c:	8a 1a                	mov    (%edx),%bl
  80091e:	38 d9                	cmp    %bl,%cl
  800920:	74 0a                	je     80092c <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800922:	0f b6 c1             	movzbl %cl,%eax
  800925:	0f b6 db             	movzbl %bl,%ebx
  800928:	29 d8                	sub    %ebx,%eax
  80092a:	eb 0b                	jmp    800937 <memcmp+0x2f>
		s1++, s2++;
  80092c:	40                   	inc    %eax
  80092d:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80092e:	39 f0                	cmp    %esi,%eax
  800930:	75 e8                	jne    80091a <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800932:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800937:	5b                   	pop    %ebx
  800938:	5e                   	pop    %esi
  800939:	5d                   	pop    %ebp
  80093a:	c3                   	ret    

0080093b <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  80093b:	55                   	push   %ebp
  80093c:	89 e5                	mov    %esp,%ebp
  80093e:	53                   	push   %ebx
  80093f:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800942:	89 c1                	mov    %eax,%ecx
  800944:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800947:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80094b:	eb 08                	jmp    800955 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80094d:	0f b6 10             	movzbl (%eax),%edx
  800950:	39 da                	cmp    %ebx,%edx
  800952:	74 05                	je     800959 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800954:	40                   	inc    %eax
  800955:	39 c8                	cmp    %ecx,%eax
  800957:	72 f4                	jb     80094d <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800959:	5b                   	pop    %ebx
  80095a:	5d                   	pop    %ebp
  80095b:	c3                   	ret    

0080095c <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80095c:	55                   	push   %ebp
  80095d:	89 e5                	mov    %esp,%ebp
  80095f:	57                   	push   %edi
  800960:	56                   	push   %esi
  800961:	53                   	push   %ebx
  800962:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800965:	eb 01                	jmp    800968 <strtol+0xc>
		s++;
  800967:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800968:	8a 01                	mov    (%ecx),%al
  80096a:	3c 20                	cmp    $0x20,%al
  80096c:	74 f9                	je     800967 <strtol+0xb>
  80096e:	3c 09                	cmp    $0x9,%al
  800970:	74 f5                	je     800967 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800972:	3c 2b                	cmp    $0x2b,%al
  800974:	75 08                	jne    80097e <strtol+0x22>
		s++;
  800976:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800977:	bf 00 00 00 00       	mov    $0x0,%edi
  80097c:	eb 11                	jmp    80098f <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80097e:	3c 2d                	cmp    $0x2d,%al
  800980:	75 08                	jne    80098a <strtol+0x2e>
		s++, neg = 1;
  800982:	41                   	inc    %ecx
  800983:	bf 01 00 00 00       	mov    $0x1,%edi
  800988:	eb 05                	jmp    80098f <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80098a:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80098f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800993:	0f 84 87 00 00 00    	je     800a20 <strtol+0xc4>
  800999:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80099d:	75 27                	jne    8009c6 <strtol+0x6a>
  80099f:	80 39 30             	cmpb   $0x30,(%ecx)
  8009a2:	75 22                	jne    8009c6 <strtol+0x6a>
  8009a4:	e9 88 00 00 00       	jmp    800a31 <strtol+0xd5>
		s += 2, base = 16;
  8009a9:	83 c1 02             	add    $0x2,%ecx
  8009ac:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009b3:	eb 11                	jmp    8009c6 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009b5:	41                   	inc    %ecx
  8009b6:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8009bd:	eb 07                	jmp    8009c6 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8009bf:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009c6:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009cb:	8a 11                	mov    (%ecx),%dl
  8009cd:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009d0:	80 fb 09             	cmp    $0x9,%bl
  8009d3:	77 08                	ja     8009dd <strtol+0x81>
			dig = *s - '0';
  8009d5:	0f be d2             	movsbl %dl,%edx
  8009d8:	83 ea 30             	sub    $0x30,%edx
  8009db:	eb 22                	jmp    8009ff <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009dd:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009e0:	89 f3                	mov    %esi,%ebx
  8009e2:	80 fb 19             	cmp    $0x19,%bl
  8009e5:	77 08                	ja     8009ef <strtol+0x93>
			dig = *s - 'a' + 10;
  8009e7:	0f be d2             	movsbl %dl,%edx
  8009ea:	83 ea 57             	sub    $0x57,%edx
  8009ed:	eb 10                	jmp    8009ff <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009ef:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009f2:	89 f3                	mov    %esi,%ebx
  8009f4:	80 fb 19             	cmp    $0x19,%bl
  8009f7:	77 14                	ja     800a0d <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009f9:	0f be d2             	movsbl %dl,%edx
  8009fc:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009ff:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a02:	7d 09                	jge    800a0d <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a04:	41                   	inc    %ecx
  800a05:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a09:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a0b:	eb be                	jmp    8009cb <strtol+0x6f>

	if (endptr)
  800a0d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a11:	74 05                	je     800a18 <strtol+0xbc>
		*endptr = (char *) s;
  800a13:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a16:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a18:	85 ff                	test   %edi,%edi
  800a1a:	74 21                	je     800a3d <strtol+0xe1>
  800a1c:	f7 d8                	neg    %eax
  800a1e:	eb 1d                	jmp    800a3d <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a20:	80 39 30             	cmpb   $0x30,(%ecx)
  800a23:	75 9a                	jne    8009bf <strtol+0x63>
  800a25:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a29:	0f 84 7a ff ff ff    	je     8009a9 <strtol+0x4d>
  800a2f:	eb 84                	jmp    8009b5 <strtol+0x59>
  800a31:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a35:	0f 84 6e ff ff ff    	je     8009a9 <strtol+0x4d>
  800a3b:	eb 89                	jmp    8009c6 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a3d:	5b                   	pop    %ebx
  800a3e:	5e                   	pop    %esi
  800a3f:	5f                   	pop    %edi
  800a40:	5d                   	pop    %ebp
  800a41:	c3                   	ret    

00800a42 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a42:	55                   	push   %ebp
  800a43:	89 e5                	mov    %esp,%ebp
  800a45:	57                   	push   %edi
  800a46:	56                   	push   %esi
  800a47:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a48:	b8 00 00 00 00       	mov    $0x0,%eax
  800a4d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a50:	8b 55 08             	mov    0x8(%ebp),%edx
  800a53:	89 c3                	mov    %eax,%ebx
  800a55:	89 c7                	mov    %eax,%edi
  800a57:	89 c6                	mov    %eax,%esi
  800a59:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a5b:	5b                   	pop    %ebx
  800a5c:	5e                   	pop    %esi
  800a5d:	5f                   	pop    %edi
  800a5e:	5d                   	pop    %ebp
  800a5f:	c3                   	ret    

00800a60 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a60:	55                   	push   %ebp
  800a61:	89 e5                	mov    %esp,%ebp
  800a63:	57                   	push   %edi
  800a64:	56                   	push   %esi
  800a65:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a66:	ba 00 00 00 00       	mov    $0x0,%edx
  800a6b:	b8 01 00 00 00       	mov    $0x1,%eax
  800a70:	89 d1                	mov    %edx,%ecx
  800a72:	89 d3                	mov    %edx,%ebx
  800a74:	89 d7                	mov    %edx,%edi
  800a76:	89 d6                	mov    %edx,%esi
  800a78:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a7a:	5b                   	pop    %ebx
  800a7b:	5e                   	pop    %esi
  800a7c:	5f                   	pop    %edi
  800a7d:	5d                   	pop    %ebp
  800a7e:	c3                   	ret    

00800a7f <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a7f:	55                   	push   %ebp
  800a80:	89 e5                	mov    %esp,%ebp
  800a82:	57                   	push   %edi
  800a83:	56                   	push   %esi
  800a84:	53                   	push   %ebx
  800a85:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a88:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a8d:	b8 03 00 00 00       	mov    $0x3,%eax
  800a92:	8b 55 08             	mov    0x8(%ebp),%edx
  800a95:	89 cb                	mov    %ecx,%ebx
  800a97:	89 cf                	mov    %ecx,%edi
  800a99:	89 ce                	mov    %ecx,%esi
  800a9b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a9d:	85 c0                	test   %eax,%eax
  800a9f:	7e 17                	jle    800ab8 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800aa1:	83 ec 0c             	sub    $0xc,%esp
  800aa4:	50                   	push   %eax
  800aa5:	6a 03                	push   $0x3
  800aa7:	68 df 16 80 00       	push   $0x8016df
  800aac:	6a 23                	push   $0x23
  800aae:	68 fc 16 80 00       	push   $0x8016fc
  800ab3:	e8 2c 05 00 00       	call   800fe4 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800ab8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800abb:	5b                   	pop    %ebx
  800abc:	5e                   	pop    %esi
  800abd:	5f                   	pop    %edi
  800abe:	5d                   	pop    %ebp
  800abf:	c3                   	ret    

00800ac0 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800ac0:	55                   	push   %ebp
  800ac1:	89 e5                	mov    %esp,%ebp
  800ac3:	57                   	push   %edi
  800ac4:	56                   	push   %esi
  800ac5:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac6:	ba 00 00 00 00       	mov    $0x0,%edx
  800acb:	b8 02 00 00 00       	mov    $0x2,%eax
  800ad0:	89 d1                	mov    %edx,%ecx
  800ad2:	89 d3                	mov    %edx,%ebx
  800ad4:	89 d7                	mov    %edx,%edi
  800ad6:	89 d6                	mov    %edx,%esi
  800ad8:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800ada:	5b                   	pop    %ebx
  800adb:	5e                   	pop    %esi
  800adc:	5f                   	pop    %edi
  800add:	5d                   	pop    %ebp
  800ade:	c3                   	ret    

00800adf <sys_yield>:

void
sys_yield(void)
{
  800adf:	55                   	push   %ebp
  800ae0:	89 e5                	mov    %esp,%ebp
  800ae2:	57                   	push   %edi
  800ae3:	56                   	push   %esi
  800ae4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae5:	ba 00 00 00 00       	mov    $0x0,%edx
  800aea:	b8 0b 00 00 00       	mov    $0xb,%eax
  800aef:	89 d1                	mov    %edx,%ecx
  800af1:	89 d3                	mov    %edx,%ebx
  800af3:	89 d7                	mov    %edx,%edi
  800af5:	89 d6                	mov    %edx,%esi
  800af7:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800af9:	5b                   	pop    %ebx
  800afa:	5e                   	pop    %esi
  800afb:	5f                   	pop    %edi
  800afc:	5d                   	pop    %ebp
  800afd:	c3                   	ret    

00800afe <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800afe:	55                   	push   %ebp
  800aff:	89 e5                	mov    %esp,%ebp
  800b01:	57                   	push   %edi
  800b02:	56                   	push   %esi
  800b03:	53                   	push   %ebx
  800b04:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b07:	be 00 00 00 00       	mov    $0x0,%esi
  800b0c:	b8 04 00 00 00       	mov    $0x4,%eax
  800b11:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b14:	8b 55 08             	mov    0x8(%ebp),%edx
  800b17:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b1a:	89 f7                	mov    %esi,%edi
  800b1c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b1e:	85 c0                	test   %eax,%eax
  800b20:	7e 17                	jle    800b39 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b22:	83 ec 0c             	sub    $0xc,%esp
  800b25:	50                   	push   %eax
  800b26:	6a 04                	push   $0x4
  800b28:	68 df 16 80 00       	push   $0x8016df
  800b2d:	6a 23                	push   $0x23
  800b2f:	68 fc 16 80 00       	push   $0x8016fc
  800b34:	e8 ab 04 00 00       	call   800fe4 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b39:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b3c:	5b                   	pop    %ebx
  800b3d:	5e                   	pop    %esi
  800b3e:	5f                   	pop    %edi
  800b3f:	5d                   	pop    %ebp
  800b40:	c3                   	ret    

00800b41 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b41:	55                   	push   %ebp
  800b42:	89 e5                	mov    %esp,%ebp
  800b44:	57                   	push   %edi
  800b45:	56                   	push   %esi
  800b46:	53                   	push   %ebx
  800b47:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b4a:	b8 05 00 00 00       	mov    $0x5,%eax
  800b4f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b52:	8b 55 08             	mov    0x8(%ebp),%edx
  800b55:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b58:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b5b:	8b 75 18             	mov    0x18(%ebp),%esi
  800b5e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b60:	85 c0                	test   %eax,%eax
  800b62:	7e 17                	jle    800b7b <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b64:	83 ec 0c             	sub    $0xc,%esp
  800b67:	50                   	push   %eax
  800b68:	6a 05                	push   $0x5
  800b6a:	68 df 16 80 00       	push   $0x8016df
  800b6f:	6a 23                	push   $0x23
  800b71:	68 fc 16 80 00       	push   $0x8016fc
  800b76:	e8 69 04 00 00       	call   800fe4 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b7b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b7e:	5b                   	pop    %ebx
  800b7f:	5e                   	pop    %esi
  800b80:	5f                   	pop    %edi
  800b81:	5d                   	pop    %ebp
  800b82:	c3                   	ret    

00800b83 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b83:	55                   	push   %ebp
  800b84:	89 e5                	mov    %esp,%ebp
  800b86:	57                   	push   %edi
  800b87:	56                   	push   %esi
  800b88:	53                   	push   %ebx
  800b89:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b8c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b91:	b8 06 00 00 00       	mov    $0x6,%eax
  800b96:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b99:	8b 55 08             	mov    0x8(%ebp),%edx
  800b9c:	89 df                	mov    %ebx,%edi
  800b9e:	89 de                	mov    %ebx,%esi
  800ba0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ba2:	85 c0                	test   %eax,%eax
  800ba4:	7e 17                	jle    800bbd <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ba6:	83 ec 0c             	sub    $0xc,%esp
  800ba9:	50                   	push   %eax
  800baa:	6a 06                	push   $0x6
  800bac:	68 df 16 80 00       	push   $0x8016df
  800bb1:	6a 23                	push   $0x23
  800bb3:	68 fc 16 80 00       	push   $0x8016fc
  800bb8:	e8 27 04 00 00       	call   800fe4 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800bbd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bc0:	5b                   	pop    %ebx
  800bc1:	5e                   	pop    %esi
  800bc2:	5f                   	pop    %edi
  800bc3:	5d                   	pop    %ebp
  800bc4:	c3                   	ret    

00800bc5 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800bc5:	55                   	push   %ebp
  800bc6:	89 e5                	mov    %esp,%ebp
  800bc8:	57                   	push   %edi
  800bc9:	56                   	push   %esi
  800bca:	53                   	push   %ebx
  800bcb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bce:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bd3:	b8 08 00 00 00       	mov    $0x8,%eax
  800bd8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bdb:	8b 55 08             	mov    0x8(%ebp),%edx
  800bde:	89 df                	mov    %ebx,%edi
  800be0:	89 de                	mov    %ebx,%esi
  800be2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800be4:	85 c0                	test   %eax,%eax
  800be6:	7e 17                	jle    800bff <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800be8:	83 ec 0c             	sub    $0xc,%esp
  800beb:	50                   	push   %eax
  800bec:	6a 08                	push   $0x8
  800bee:	68 df 16 80 00       	push   $0x8016df
  800bf3:	6a 23                	push   $0x23
  800bf5:	68 fc 16 80 00       	push   $0x8016fc
  800bfa:	e8 e5 03 00 00       	call   800fe4 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800bff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c02:	5b                   	pop    %ebx
  800c03:	5e                   	pop    %esi
  800c04:	5f                   	pop    %edi
  800c05:	5d                   	pop    %ebp
  800c06:	c3                   	ret    

00800c07 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c07:	55                   	push   %ebp
  800c08:	89 e5                	mov    %esp,%ebp
  800c0a:	57                   	push   %edi
  800c0b:	56                   	push   %esi
  800c0c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c0d:	ba 00 00 00 00       	mov    $0x0,%edx
  800c12:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c17:	89 d1                	mov    %edx,%ecx
  800c19:	89 d3                	mov    %edx,%ebx
  800c1b:	89 d7                	mov    %edx,%edi
  800c1d:	89 d6                	mov    %edx,%esi
  800c1f:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c21:	5b                   	pop    %ebx
  800c22:	5e                   	pop    %esi
  800c23:	5f                   	pop    %edi
  800c24:	5d                   	pop    %ebp
  800c25:	c3                   	ret    

00800c26 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c26:	55                   	push   %ebp
  800c27:	89 e5                	mov    %esp,%ebp
  800c29:	57                   	push   %edi
  800c2a:	56                   	push   %esi
  800c2b:	53                   	push   %ebx
  800c2c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c34:	b8 09 00 00 00       	mov    $0x9,%eax
  800c39:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c3c:	8b 55 08             	mov    0x8(%ebp),%edx
  800c3f:	89 df                	mov    %ebx,%edi
  800c41:	89 de                	mov    %ebx,%esi
  800c43:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c45:	85 c0                	test   %eax,%eax
  800c47:	7e 17                	jle    800c60 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c49:	83 ec 0c             	sub    $0xc,%esp
  800c4c:	50                   	push   %eax
  800c4d:	6a 09                	push   $0x9
  800c4f:	68 df 16 80 00       	push   $0x8016df
  800c54:	6a 23                	push   $0x23
  800c56:	68 fc 16 80 00       	push   $0x8016fc
  800c5b:	e8 84 03 00 00       	call   800fe4 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c60:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c63:	5b                   	pop    %ebx
  800c64:	5e                   	pop    %esi
  800c65:	5f                   	pop    %edi
  800c66:	5d                   	pop    %ebp
  800c67:	c3                   	ret    

00800c68 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c68:	55                   	push   %ebp
  800c69:	89 e5                	mov    %esp,%ebp
  800c6b:	57                   	push   %edi
  800c6c:	56                   	push   %esi
  800c6d:	53                   	push   %ebx
  800c6e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c71:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c76:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c7b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c7e:	8b 55 08             	mov    0x8(%ebp),%edx
  800c81:	89 df                	mov    %ebx,%edi
  800c83:	89 de                	mov    %ebx,%esi
  800c85:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c87:	85 c0                	test   %eax,%eax
  800c89:	7e 17                	jle    800ca2 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c8b:	83 ec 0c             	sub    $0xc,%esp
  800c8e:	50                   	push   %eax
  800c8f:	6a 0a                	push   $0xa
  800c91:	68 df 16 80 00       	push   $0x8016df
  800c96:	6a 23                	push   $0x23
  800c98:	68 fc 16 80 00       	push   $0x8016fc
  800c9d:	e8 42 03 00 00       	call   800fe4 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800ca2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ca5:	5b                   	pop    %ebx
  800ca6:	5e                   	pop    %esi
  800ca7:	5f                   	pop    %edi
  800ca8:	5d                   	pop    %ebp
  800ca9:	c3                   	ret    

00800caa <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800caa:	55                   	push   %ebp
  800cab:	89 e5                	mov    %esp,%ebp
  800cad:	57                   	push   %edi
  800cae:	56                   	push   %esi
  800caf:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cb0:	be 00 00 00 00       	mov    $0x0,%esi
  800cb5:	b8 0d 00 00 00       	mov    $0xd,%eax
  800cba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cbd:	8b 55 08             	mov    0x8(%ebp),%edx
  800cc0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cc3:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cc6:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800cc8:	5b                   	pop    %ebx
  800cc9:	5e                   	pop    %esi
  800cca:	5f                   	pop    %edi
  800ccb:	5d                   	pop    %ebp
  800ccc:	c3                   	ret    

00800ccd <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800ccd:	55                   	push   %ebp
  800cce:	89 e5                	mov    %esp,%ebp
  800cd0:	57                   	push   %edi
  800cd1:	56                   	push   %esi
  800cd2:	53                   	push   %ebx
  800cd3:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cd6:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cdb:	b8 0e 00 00 00       	mov    $0xe,%eax
  800ce0:	8b 55 08             	mov    0x8(%ebp),%edx
  800ce3:	89 cb                	mov    %ecx,%ebx
  800ce5:	89 cf                	mov    %ecx,%edi
  800ce7:	89 ce                	mov    %ecx,%esi
  800ce9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ceb:	85 c0                	test   %eax,%eax
  800ced:	7e 17                	jle    800d06 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cef:	83 ec 0c             	sub    $0xc,%esp
  800cf2:	50                   	push   %eax
  800cf3:	6a 0e                	push   $0xe
  800cf5:	68 df 16 80 00       	push   $0x8016df
  800cfa:	6a 23                	push   $0x23
  800cfc:	68 fc 16 80 00       	push   $0x8016fc
  800d01:	e8 de 02 00 00       	call   800fe4 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d06:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d09:	5b                   	pop    %ebx
  800d0a:	5e                   	pop    %esi
  800d0b:	5f                   	pop    %edi
  800d0c:	5d                   	pop    %ebp
  800d0d:	c3                   	ret    

00800d0e <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800d0e:	55                   	push   %ebp
  800d0f:	89 e5                	mov    %esp,%ebp
  800d11:	53                   	push   %ebx
  800d12:	83 ec 04             	sub    $0x4,%esp
  800d15:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800d18:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800d1a:	89 da                	mov    %ebx,%edx
  800d1c:	c1 ea 0c             	shr    $0xc,%edx
  800d1f:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800d26:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800d2a:	74 05                	je     800d31 <pgfault+0x23>
  800d2c:	f6 c6 08             	test   $0x8,%dh
  800d2f:	75 14                	jne    800d45 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800d31:	83 ec 04             	sub    $0x4,%esp
  800d34:	68 0c 17 80 00       	push   $0x80170c
  800d39:	6a 26                	push   $0x26
  800d3b:	68 70 17 80 00       	push   $0x801770
  800d40:	e8 9f 02 00 00       	call   800fe4 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800d45:	83 ec 04             	sub    $0x4,%esp
  800d48:	6a 07                	push   $0x7
  800d4a:	68 00 f0 7f 00       	push   $0x7ff000
  800d4f:	6a 00                	push   $0x0
  800d51:	e8 a8 fd ff ff       	call   800afe <sys_page_alloc>
  800d56:	83 c4 10             	add    $0x10,%esp
  800d59:	85 c0                	test   %eax,%eax
  800d5b:	74 14                	je     800d71 <pgfault+0x63>
        panic("pgfault: no phys mem");
  800d5d:	83 ec 04             	sub    $0x4,%esp
  800d60:	68 7b 17 80 00       	push   $0x80177b
  800d65:	6a 32                	push   $0x32
  800d67:	68 70 17 80 00       	push   $0x801770
  800d6c:	e8 73 02 00 00       	call   800fe4 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800d71:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800d77:	83 ec 04             	sub    $0x4,%esp
  800d7a:	68 00 10 00 00       	push   $0x1000
  800d7f:	53                   	push   %ebx
  800d80:	68 00 f0 7f 00       	push   $0x7ff000
  800d85:	e8 05 fb ff ff       	call   80088f <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800d8a:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800d91:	53                   	push   %ebx
  800d92:	6a 00                	push   $0x0
  800d94:	68 00 f0 7f 00       	push   $0x7ff000
  800d99:	6a 00                	push   $0x0
  800d9b:	e8 a1 fd ff ff       	call   800b41 <sys_page_map>
  800da0:	83 c4 20             	add    $0x20,%esp
  800da3:	85 c0                	test   %eax,%eax
  800da5:	74 14                	je     800dbb <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800da7:	83 ec 04             	sub    $0x4,%esp
  800daa:	68 90 17 80 00       	push   $0x801790
  800daf:	6a 3f                	push   $0x3f
  800db1:	68 70 17 80 00       	push   $0x801770
  800db6:	e8 29 02 00 00       	call   800fe4 <_panic>


	//panic("pgfault not implemented");
}
  800dbb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800dbe:	c9                   	leave  
  800dbf:	c3                   	ret    

00800dc0 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800dc0:	55                   	push   %ebp
  800dc1:	89 e5                	mov    %esp,%ebp
  800dc3:	57                   	push   %edi
  800dc4:	56                   	push   %esi
  800dc5:	53                   	push   %ebx
  800dc6:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800dc9:	68 0e 0d 80 00       	push   $0x800d0e
  800dce:	e8 57 02 00 00       	call   80102a <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800dd3:	b8 07 00 00 00       	mov    $0x7,%eax
  800dd8:	cd 30                	int    $0x30
  800dda:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800ddd:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800de0:	83 c4 10             	add    $0x10,%esp
  800de3:	85 c0                	test   %eax,%eax
  800de5:	79 17                	jns    800dfe <fork+0x3e>
        panic("fork: cannot create child env");
  800de7:	83 ec 04             	sub    $0x4,%esp
  800dea:	68 a3 17 80 00       	push   $0x8017a3
  800def:	68 97 00 00 00       	push   $0x97
  800df4:	68 70 17 80 00       	push   $0x801770
  800df9:	e8 e6 01 00 00       	call   800fe4 <_panic>
    } else if (envid == 0) {
  800dfe:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800e02:	75 2a                	jne    800e2e <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800e04:	e8 b7 fc ff ff       	call   800ac0 <sys_getenvid>
  800e09:	25 ff 03 00 00       	and    $0x3ff,%eax
  800e0e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e15:	c1 e0 07             	shl    $0x7,%eax
  800e18:	29 d0                	sub    %edx,%eax
  800e1a:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800e1f:	a3 04 20 80 00       	mov    %eax,0x802004
        return 0;
  800e24:	b8 00 00 00 00       	mov    $0x0,%eax
  800e29:	e9 94 01 00 00       	jmp    800fc2 <fork+0x202>
  800e2e:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800e35:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800e38:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800e3f:	a8 01                	test   $0x1,%al
  800e41:	0f 84 0a 01 00 00    	je     800f51 <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800e47:	c1 e7 0a             	shl    $0xa,%edi
  800e4a:	be 00 00 00 00       	mov    $0x0,%esi
  800e4f:	89 fb                	mov    %edi,%ebx
  800e51:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800e53:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800e59:	75 34                	jne    800e8f <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800e5b:	83 ec 04             	sub    $0x4,%esp
  800e5e:	6a 07                	push   $0x7
  800e60:	68 00 f0 bf ee       	push   $0xeebff000
  800e65:	ff 75 e4             	pushl  -0x1c(%ebp)
  800e68:	e8 91 fc ff ff       	call   800afe <sys_page_alloc>
  800e6d:	83 c4 10             	add    $0x10,%esp
  800e70:	85 c0                	test   %eax,%eax
  800e72:	0f 84 cc 00 00 00    	je     800f44 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800e78:	83 ec 04             	sub    $0x4,%esp
  800e7b:	68 c1 17 80 00       	push   $0x8017c1
  800e80:	68 ad 00 00 00       	push   $0xad
  800e85:	68 70 17 80 00       	push   $0x801770
  800e8a:	e8 55 01 00 00       	call   800fe4 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800e8f:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800e96:	a8 01                	test   $0x1,%al
  800e98:	0f 84 a6 00 00 00    	je     800f44 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800e9e:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800ea5:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800ea8:	a9 02 08 00 00       	test   $0x802,%eax
  800ead:	74 62                	je     800f11 <fork+0x151>
  800eaf:	f6 c4 04             	test   $0x4,%ah
  800eb2:	75 78                	jne    800f2c <fork+0x16c>
        if (sys_page_map(0,
  800eb4:	83 ec 0c             	sub    $0xc,%esp
  800eb7:	68 05 08 00 00       	push   $0x805
  800ebc:	53                   	push   %ebx
  800ebd:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ec0:	53                   	push   %ebx
  800ec1:	6a 00                	push   $0x0
  800ec3:	e8 79 fc ff ff       	call   800b41 <sys_page_map>
  800ec8:	83 c4 20             	add    $0x20,%esp
  800ecb:	85 c0                	test   %eax,%eax
  800ecd:	74 14                	je     800ee3 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800ecf:	83 ec 04             	sub    $0x4,%esp
  800ed2:	68 dc 17 80 00       	push   $0x8017dc
  800ed7:	6a 64                	push   $0x64
  800ed9:	68 70 17 80 00       	push   $0x801770
  800ede:	e8 01 01 00 00       	call   800fe4 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800ee3:	83 ec 0c             	sub    $0xc,%esp
  800ee6:	68 05 08 00 00       	push   $0x805
  800eeb:	53                   	push   %ebx
  800eec:	6a 00                	push   $0x0
  800eee:	53                   	push   %ebx
  800eef:	6a 00                	push   $0x0
  800ef1:	e8 4b fc ff ff       	call   800b41 <sys_page_map>
  800ef6:	83 c4 20             	add    $0x20,%esp
  800ef9:	85 c0                	test   %eax,%eax
  800efb:	74 47                	je     800f44 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  800efd:	83 ec 04             	sub    $0x4,%esp
  800f00:	68 f3 17 80 00       	push   $0x8017f3
  800f05:	6a 6d                	push   $0x6d
  800f07:	68 70 17 80 00       	push   $0x801770
  800f0c:	e8 d3 00 00 00       	call   800fe4 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  800f11:	f6 c4 04             	test   $0x4,%ah
  800f14:	75 16                	jne    800f2c <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  800f16:	83 ec 0c             	sub    $0xc,%esp
  800f19:	6a 05                	push   $0x5
  800f1b:	53                   	push   %ebx
  800f1c:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f1f:	53                   	push   %ebx
  800f20:	6a 00                	push   $0x0
  800f22:	e8 1a fc ff ff       	call   800b41 <sys_page_map>
  800f27:	83 c4 20             	add    $0x20,%esp
  800f2a:	eb 18                	jmp    800f44 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  800f2c:	83 ec 0c             	sub    $0xc,%esp
  800f2f:	25 07 0e 00 00       	and    $0xe07,%eax
  800f34:	50                   	push   %eax
  800f35:	53                   	push   %ebx
  800f36:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f39:	53                   	push   %ebx
  800f3a:	6a 00                	push   $0x0
  800f3c:	e8 00 fc ff ff       	call   800b41 <sys_page_map>
  800f41:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  800f44:	46                   	inc    %esi
  800f45:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  800f4b:	0f 85 fe fe ff ff    	jne    800e4f <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  800f51:	ff 45 e0             	incl   -0x20(%ebp)
  800f54:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800f57:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  800f5c:	0f 85 d3 fe ff ff    	jne    800e35 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  800f62:	a1 04 20 80 00       	mov    0x802004,%eax
  800f67:	8b 40 64             	mov    0x64(%eax),%eax
  800f6a:	83 ec 08             	sub    $0x8,%esp
  800f6d:	50                   	push   %eax
  800f6e:	ff 75 dc             	pushl  -0x24(%ebp)
  800f71:	e8 f2 fc ff ff       	call   800c68 <sys_env_set_pgfault_upcall>
  800f76:	83 c4 10             	add    $0x10,%esp
  800f79:	85 c0                	test   %eax,%eax
  800f7b:	74 17                	je     800f94 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  800f7d:	83 ec 04             	sub    $0x4,%esp
  800f80:	68 50 17 80 00       	push   $0x801750
  800f85:	68 b9 00 00 00       	push   $0xb9
  800f8a:	68 70 17 80 00       	push   $0x801770
  800f8f:	e8 50 00 00 00       	call   800fe4 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  800f94:	83 ec 08             	sub    $0x8,%esp
  800f97:	6a 02                	push   $0x2
  800f99:	ff 75 dc             	pushl  -0x24(%ebp)
  800f9c:	e8 24 fc ff ff       	call   800bc5 <sys_env_set_status>
  800fa1:	83 c4 10             	add    $0x10,%esp
  800fa4:	85 c0                	test   %eax,%eax
  800fa6:	74 17                	je     800fbf <fork+0x1ff>
        panic("fork: cannot set env status");
  800fa8:	83 ec 04             	sub    $0x4,%esp
  800fab:	68 0e 18 80 00       	push   $0x80180e
  800fb0:	68 bd 00 00 00       	push   $0xbd
  800fb5:	68 70 17 80 00       	push   $0x801770
  800fba:	e8 25 00 00 00       	call   800fe4 <_panic>

    return envid;
  800fbf:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  800fc2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fc5:	5b                   	pop    %ebx
  800fc6:	5e                   	pop    %esi
  800fc7:	5f                   	pop    %edi
  800fc8:	5d                   	pop    %ebp
  800fc9:	c3                   	ret    

00800fca <sfork>:

// Challenge!
int
sfork(void)
{
  800fca:	55                   	push   %ebp
  800fcb:	89 e5                	mov    %esp,%ebp
  800fcd:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  800fd0:	68 2a 18 80 00       	push   $0x80182a
  800fd5:	68 c8 00 00 00       	push   $0xc8
  800fda:	68 70 17 80 00       	push   $0x801770
  800fdf:	e8 00 00 00 00       	call   800fe4 <_panic>

00800fe4 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800fe4:	55                   	push   %ebp
  800fe5:	89 e5                	mov    %esp,%ebp
  800fe7:	56                   	push   %esi
  800fe8:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800fe9:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800fec:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800ff2:	e8 c9 fa ff ff       	call   800ac0 <sys_getenvid>
  800ff7:	83 ec 0c             	sub    $0xc,%esp
  800ffa:	ff 75 0c             	pushl  0xc(%ebp)
  800ffd:	ff 75 08             	pushl  0x8(%ebp)
  801000:	56                   	push   %esi
  801001:	50                   	push   %eax
  801002:	68 40 18 80 00       	push   $0x801840
  801007:	e8 aa f1 ff ff       	call   8001b6 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80100c:	83 c4 18             	add    $0x18,%esp
  80100f:	53                   	push   %ebx
  801010:	ff 75 10             	pushl  0x10(%ebp)
  801013:	e8 4d f1 ff ff       	call   800165 <vcprintf>
	cprintf("\n");
  801018:	c7 04 24 d7 13 80 00 	movl   $0x8013d7,(%esp)
  80101f:	e8 92 f1 ff ff       	call   8001b6 <cprintf>
  801024:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801027:	cc                   	int3   
  801028:	eb fd                	jmp    801027 <_panic+0x43>

0080102a <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  80102a:	55                   	push   %ebp
  80102b:	89 e5                	mov    %esp,%ebp
  80102d:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801030:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  801037:	75 3e                	jne    801077 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801039:	83 ec 04             	sub    $0x4,%esp
  80103c:	6a 07                	push   $0x7
  80103e:	68 00 f0 bf ee       	push   $0xeebff000
  801043:	6a 00                	push   $0x0
  801045:	e8 b4 fa ff ff       	call   800afe <sys_page_alloc>
  80104a:	83 c4 10             	add    $0x10,%esp
  80104d:	85 c0                	test   %eax,%eax
  80104f:	74 14                	je     801065 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801051:	83 ec 04             	sub    $0x4,%esp
  801054:	68 64 18 80 00       	push   $0x801864
  801059:	6a 23                	push   $0x23
  80105b:	68 88 18 80 00       	push   $0x801888
  801060:	e8 7f ff ff ff       	call   800fe4 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801065:	83 ec 08             	sub    $0x8,%esp
  801068:	68 81 10 80 00       	push   $0x801081
  80106d:	6a 00                	push   $0x0
  80106f:	e8 f4 fb ff ff       	call   800c68 <sys_env_set_pgfault_upcall>
  801074:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801077:	8b 45 08             	mov    0x8(%ebp),%eax
  80107a:	a3 08 20 80 00       	mov    %eax,0x802008
}
  80107f:	c9                   	leave  
  801080:	c3                   	ret    

00801081 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801081:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801082:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  801087:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801089:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  80108c:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  80108e:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801092:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801096:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801097:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  80109a:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  80109c:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  80109f:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  8010a2:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  8010a3:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8010a6:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8010a7:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8010a8:	c3                   	ret    
  8010a9:	66 90                	xchg   %ax,%ax
  8010ab:	90                   	nop

008010ac <__udivdi3>:
  8010ac:	55                   	push   %ebp
  8010ad:	57                   	push   %edi
  8010ae:	56                   	push   %esi
  8010af:	53                   	push   %ebx
  8010b0:	83 ec 1c             	sub    $0x1c,%esp
  8010b3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8010b7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8010bb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8010bf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8010c3:	89 ca                	mov    %ecx,%edx
  8010c5:	89 f8                	mov    %edi,%eax
  8010c7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8010cb:	85 f6                	test   %esi,%esi
  8010cd:	75 2d                	jne    8010fc <__udivdi3+0x50>
  8010cf:	39 cf                	cmp    %ecx,%edi
  8010d1:	77 65                	ja     801138 <__udivdi3+0x8c>
  8010d3:	89 fd                	mov    %edi,%ebp
  8010d5:	85 ff                	test   %edi,%edi
  8010d7:	75 0b                	jne    8010e4 <__udivdi3+0x38>
  8010d9:	b8 01 00 00 00       	mov    $0x1,%eax
  8010de:	31 d2                	xor    %edx,%edx
  8010e0:	f7 f7                	div    %edi
  8010e2:	89 c5                	mov    %eax,%ebp
  8010e4:	31 d2                	xor    %edx,%edx
  8010e6:	89 c8                	mov    %ecx,%eax
  8010e8:	f7 f5                	div    %ebp
  8010ea:	89 c1                	mov    %eax,%ecx
  8010ec:	89 d8                	mov    %ebx,%eax
  8010ee:	f7 f5                	div    %ebp
  8010f0:	89 cf                	mov    %ecx,%edi
  8010f2:	89 fa                	mov    %edi,%edx
  8010f4:	83 c4 1c             	add    $0x1c,%esp
  8010f7:	5b                   	pop    %ebx
  8010f8:	5e                   	pop    %esi
  8010f9:	5f                   	pop    %edi
  8010fa:	5d                   	pop    %ebp
  8010fb:	c3                   	ret    
  8010fc:	39 ce                	cmp    %ecx,%esi
  8010fe:	77 28                	ja     801128 <__udivdi3+0x7c>
  801100:	0f bd fe             	bsr    %esi,%edi
  801103:	83 f7 1f             	xor    $0x1f,%edi
  801106:	75 40                	jne    801148 <__udivdi3+0x9c>
  801108:	39 ce                	cmp    %ecx,%esi
  80110a:	72 0a                	jb     801116 <__udivdi3+0x6a>
  80110c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801110:	0f 87 9e 00 00 00    	ja     8011b4 <__udivdi3+0x108>
  801116:	b8 01 00 00 00       	mov    $0x1,%eax
  80111b:	89 fa                	mov    %edi,%edx
  80111d:	83 c4 1c             	add    $0x1c,%esp
  801120:	5b                   	pop    %ebx
  801121:	5e                   	pop    %esi
  801122:	5f                   	pop    %edi
  801123:	5d                   	pop    %ebp
  801124:	c3                   	ret    
  801125:	8d 76 00             	lea    0x0(%esi),%esi
  801128:	31 ff                	xor    %edi,%edi
  80112a:	31 c0                	xor    %eax,%eax
  80112c:	89 fa                	mov    %edi,%edx
  80112e:	83 c4 1c             	add    $0x1c,%esp
  801131:	5b                   	pop    %ebx
  801132:	5e                   	pop    %esi
  801133:	5f                   	pop    %edi
  801134:	5d                   	pop    %ebp
  801135:	c3                   	ret    
  801136:	66 90                	xchg   %ax,%ax
  801138:	89 d8                	mov    %ebx,%eax
  80113a:	f7 f7                	div    %edi
  80113c:	31 ff                	xor    %edi,%edi
  80113e:	89 fa                	mov    %edi,%edx
  801140:	83 c4 1c             	add    $0x1c,%esp
  801143:	5b                   	pop    %ebx
  801144:	5e                   	pop    %esi
  801145:	5f                   	pop    %edi
  801146:	5d                   	pop    %ebp
  801147:	c3                   	ret    
  801148:	bd 20 00 00 00       	mov    $0x20,%ebp
  80114d:	89 eb                	mov    %ebp,%ebx
  80114f:	29 fb                	sub    %edi,%ebx
  801151:	89 f9                	mov    %edi,%ecx
  801153:	d3 e6                	shl    %cl,%esi
  801155:	89 c5                	mov    %eax,%ebp
  801157:	88 d9                	mov    %bl,%cl
  801159:	d3 ed                	shr    %cl,%ebp
  80115b:	89 e9                	mov    %ebp,%ecx
  80115d:	09 f1                	or     %esi,%ecx
  80115f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801163:	89 f9                	mov    %edi,%ecx
  801165:	d3 e0                	shl    %cl,%eax
  801167:	89 c5                	mov    %eax,%ebp
  801169:	89 d6                	mov    %edx,%esi
  80116b:	88 d9                	mov    %bl,%cl
  80116d:	d3 ee                	shr    %cl,%esi
  80116f:	89 f9                	mov    %edi,%ecx
  801171:	d3 e2                	shl    %cl,%edx
  801173:	8b 44 24 08          	mov    0x8(%esp),%eax
  801177:	88 d9                	mov    %bl,%cl
  801179:	d3 e8                	shr    %cl,%eax
  80117b:	09 c2                	or     %eax,%edx
  80117d:	89 d0                	mov    %edx,%eax
  80117f:	89 f2                	mov    %esi,%edx
  801181:	f7 74 24 0c          	divl   0xc(%esp)
  801185:	89 d6                	mov    %edx,%esi
  801187:	89 c3                	mov    %eax,%ebx
  801189:	f7 e5                	mul    %ebp
  80118b:	39 d6                	cmp    %edx,%esi
  80118d:	72 19                	jb     8011a8 <__udivdi3+0xfc>
  80118f:	74 0b                	je     80119c <__udivdi3+0xf0>
  801191:	89 d8                	mov    %ebx,%eax
  801193:	31 ff                	xor    %edi,%edi
  801195:	e9 58 ff ff ff       	jmp    8010f2 <__udivdi3+0x46>
  80119a:	66 90                	xchg   %ax,%ax
  80119c:	8b 54 24 08          	mov    0x8(%esp),%edx
  8011a0:	89 f9                	mov    %edi,%ecx
  8011a2:	d3 e2                	shl    %cl,%edx
  8011a4:	39 c2                	cmp    %eax,%edx
  8011a6:	73 e9                	jae    801191 <__udivdi3+0xe5>
  8011a8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8011ab:	31 ff                	xor    %edi,%edi
  8011ad:	e9 40 ff ff ff       	jmp    8010f2 <__udivdi3+0x46>
  8011b2:	66 90                	xchg   %ax,%ax
  8011b4:	31 c0                	xor    %eax,%eax
  8011b6:	e9 37 ff ff ff       	jmp    8010f2 <__udivdi3+0x46>
  8011bb:	90                   	nop

008011bc <__umoddi3>:
  8011bc:	55                   	push   %ebp
  8011bd:	57                   	push   %edi
  8011be:	56                   	push   %esi
  8011bf:	53                   	push   %ebx
  8011c0:	83 ec 1c             	sub    $0x1c,%esp
  8011c3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8011c7:	8b 74 24 34          	mov    0x34(%esp),%esi
  8011cb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8011cf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8011d3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8011d7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8011db:	89 f3                	mov    %esi,%ebx
  8011dd:	89 fa                	mov    %edi,%edx
  8011df:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8011e3:	89 34 24             	mov    %esi,(%esp)
  8011e6:	85 c0                	test   %eax,%eax
  8011e8:	75 1a                	jne    801204 <__umoddi3+0x48>
  8011ea:	39 f7                	cmp    %esi,%edi
  8011ec:	0f 86 a2 00 00 00    	jbe    801294 <__umoddi3+0xd8>
  8011f2:	89 c8                	mov    %ecx,%eax
  8011f4:	89 f2                	mov    %esi,%edx
  8011f6:	f7 f7                	div    %edi
  8011f8:	89 d0                	mov    %edx,%eax
  8011fa:	31 d2                	xor    %edx,%edx
  8011fc:	83 c4 1c             	add    $0x1c,%esp
  8011ff:	5b                   	pop    %ebx
  801200:	5e                   	pop    %esi
  801201:	5f                   	pop    %edi
  801202:	5d                   	pop    %ebp
  801203:	c3                   	ret    
  801204:	39 f0                	cmp    %esi,%eax
  801206:	0f 87 ac 00 00 00    	ja     8012b8 <__umoddi3+0xfc>
  80120c:	0f bd e8             	bsr    %eax,%ebp
  80120f:	83 f5 1f             	xor    $0x1f,%ebp
  801212:	0f 84 ac 00 00 00    	je     8012c4 <__umoddi3+0x108>
  801218:	bf 20 00 00 00       	mov    $0x20,%edi
  80121d:	29 ef                	sub    %ebp,%edi
  80121f:	89 fe                	mov    %edi,%esi
  801221:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801225:	89 e9                	mov    %ebp,%ecx
  801227:	d3 e0                	shl    %cl,%eax
  801229:	89 d7                	mov    %edx,%edi
  80122b:	89 f1                	mov    %esi,%ecx
  80122d:	d3 ef                	shr    %cl,%edi
  80122f:	09 c7                	or     %eax,%edi
  801231:	89 e9                	mov    %ebp,%ecx
  801233:	d3 e2                	shl    %cl,%edx
  801235:	89 14 24             	mov    %edx,(%esp)
  801238:	89 d8                	mov    %ebx,%eax
  80123a:	d3 e0                	shl    %cl,%eax
  80123c:	89 c2                	mov    %eax,%edx
  80123e:	8b 44 24 08          	mov    0x8(%esp),%eax
  801242:	d3 e0                	shl    %cl,%eax
  801244:	89 44 24 04          	mov    %eax,0x4(%esp)
  801248:	8b 44 24 08          	mov    0x8(%esp),%eax
  80124c:	89 f1                	mov    %esi,%ecx
  80124e:	d3 e8                	shr    %cl,%eax
  801250:	09 d0                	or     %edx,%eax
  801252:	d3 eb                	shr    %cl,%ebx
  801254:	89 da                	mov    %ebx,%edx
  801256:	f7 f7                	div    %edi
  801258:	89 d3                	mov    %edx,%ebx
  80125a:	f7 24 24             	mull   (%esp)
  80125d:	89 c6                	mov    %eax,%esi
  80125f:	89 d1                	mov    %edx,%ecx
  801261:	39 d3                	cmp    %edx,%ebx
  801263:	0f 82 87 00 00 00    	jb     8012f0 <__umoddi3+0x134>
  801269:	0f 84 91 00 00 00    	je     801300 <__umoddi3+0x144>
  80126f:	8b 54 24 04          	mov    0x4(%esp),%edx
  801273:	29 f2                	sub    %esi,%edx
  801275:	19 cb                	sbb    %ecx,%ebx
  801277:	89 d8                	mov    %ebx,%eax
  801279:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  80127d:	d3 e0                	shl    %cl,%eax
  80127f:	89 e9                	mov    %ebp,%ecx
  801281:	d3 ea                	shr    %cl,%edx
  801283:	09 d0                	or     %edx,%eax
  801285:	89 e9                	mov    %ebp,%ecx
  801287:	d3 eb                	shr    %cl,%ebx
  801289:	89 da                	mov    %ebx,%edx
  80128b:	83 c4 1c             	add    $0x1c,%esp
  80128e:	5b                   	pop    %ebx
  80128f:	5e                   	pop    %esi
  801290:	5f                   	pop    %edi
  801291:	5d                   	pop    %ebp
  801292:	c3                   	ret    
  801293:	90                   	nop
  801294:	89 fd                	mov    %edi,%ebp
  801296:	85 ff                	test   %edi,%edi
  801298:	75 0b                	jne    8012a5 <__umoddi3+0xe9>
  80129a:	b8 01 00 00 00       	mov    $0x1,%eax
  80129f:	31 d2                	xor    %edx,%edx
  8012a1:	f7 f7                	div    %edi
  8012a3:	89 c5                	mov    %eax,%ebp
  8012a5:	89 f0                	mov    %esi,%eax
  8012a7:	31 d2                	xor    %edx,%edx
  8012a9:	f7 f5                	div    %ebp
  8012ab:	89 c8                	mov    %ecx,%eax
  8012ad:	f7 f5                	div    %ebp
  8012af:	89 d0                	mov    %edx,%eax
  8012b1:	e9 44 ff ff ff       	jmp    8011fa <__umoddi3+0x3e>
  8012b6:	66 90                	xchg   %ax,%ax
  8012b8:	89 c8                	mov    %ecx,%eax
  8012ba:	89 f2                	mov    %esi,%edx
  8012bc:	83 c4 1c             	add    $0x1c,%esp
  8012bf:	5b                   	pop    %ebx
  8012c0:	5e                   	pop    %esi
  8012c1:	5f                   	pop    %edi
  8012c2:	5d                   	pop    %ebp
  8012c3:	c3                   	ret    
  8012c4:	3b 04 24             	cmp    (%esp),%eax
  8012c7:	72 06                	jb     8012cf <__umoddi3+0x113>
  8012c9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8012cd:	77 0f                	ja     8012de <__umoddi3+0x122>
  8012cf:	89 f2                	mov    %esi,%edx
  8012d1:	29 f9                	sub    %edi,%ecx
  8012d3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8012d7:	89 14 24             	mov    %edx,(%esp)
  8012da:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8012de:	8b 44 24 04          	mov    0x4(%esp),%eax
  8012e2:	8b 14 24             	mov    (%esp),%edx
  8012e5:	83 c4 1c             	add    $0x1c,%esp
  8012e8:	5b                   	pop    %ebx
  8012e9:	5e                   	pop    %esi
  8012ea:	5f                   	pop    %edi
  8012eb:	5d                   	pop    %ebp
  8012ec:	c3                   	ret    
  8012ed:	8d 76 00             	lea    0x0(%esi),%esi
  8012f0:	2b 04 24             	sub    (%esp),%eax
  8012f3:	19 fa                	sbb    %edi,%edx
  8012f5:	89 d1                	mov    %edx,%ecx
  8012f7:	89 c6                	mov    %eax,%esi
  8012f9:	e9 71 ff ff ff       	jmp    80126f <__umoddi3+0xb3>
  8012fe:	66 90                	xchg   %ax,%ax
  801300:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801304:	72 ea                	jb     8012f0 <__umoddi3+0x134>
  801306:	89 d9                	mov    %ebx,%ecx
  801308:	e9 62 ff ff ff       	jmp    80126f <__umoddi3+0xb3>
