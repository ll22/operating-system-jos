
obj/user/faultevilhandler.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 34 00 00 00       	call   800065 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 0c             	sub    $0xc,%esp
	sys_page_alloc(0, (void*) (UXSTACKTOP - PGSIZE), PTE_P|PTE_U|PTE_W);
  800039:	6a 07                	push   $0x7
  80003b:	68 00 f0 bf ee       	push   $0xeebff000
  800040:	6a 00                	push   $0x0
  800042:	e8 3b 01 00 00       	call   800182 <sys_page_alloc>
	sys_env_set_pgfault_upcall(0, (void*) 0xF0100020);
  800047:	83 c4 08             	add    $0x8,%esp
  80004a:	68 20 00 10 f0       	push   $0xf0100020
  80004f:	6a 00                	push   $0x0
  800051:	e8 96 02 00 00       	call   8002ec <sys_env_set_pgfault_upcall>
	*(int*)0 = 0;
  800056:	c7 05 00 00 00 00 00 	movl   $0x0,0x0
  80005d:	00 00 00 
}
  800060:	83 c4 10             	add    $0x10,%esp
  800063:	c9                   	leave  
  800064:	c3                   	ret    

00800065 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800065:	55                   	push   %ebp
  800066:	89 e5                	mov    %esp,%ebp
  800068:	56                   	push   %esi
  800069:	53                   	push   %ebx
  80006a:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80006d:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800070:	e8 cf 00 00 00       	call   800144 <sys_getenvid>
  800075:	25 ff 03 00 00       	and    $0x3ff,%eax
  80007a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800081:	c1 e0 07             	shl    $0x7,%eax
  800084:	29 d0                	sub    %edx,%eax
  800086:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80008b:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800090:	85 db                	test   %ebx,%ebx
  800092:	7e 07                	jle    80009b <libmain+0x36>
		binaryname = argv[0];
  800094:	8b 06                	mov    (%esi),%eax
  800096:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80009b:	83 ec 08             	sub    $0x8,%esp
  80009e:	56                   	push   %esi
  80009f:	53                   	push   %ebx
  8000a0:	e8 8e ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000a5:	e8 0a 00 00 00       	call   8000b4 <exit>
}
  8000aa:	83 c4 10             	add    $0x10,%esp
  8000ad:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000b0:	5b                   	pop    %ebx
  8000b1:	5e                   	pop    %esi
  8000b2:	5d                   	pop    %ebp
  8000b3:	c3                   	ret    

008000b4 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000b4:	55                   	push   %ebp
  8000b5:	89 e5                	mov    %esp,%ebp
  8000b7:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000ba:	6a 00                	push   $0x0
  8000bc:	e8 42 00 00 00       	call   800103 <sys_env_destroy>
}
  8000c1:	83 c4 10             	add    $0x10,%esp
  8000c4:	c9                   	leave  
  8000c5:	c3                   	ret    

008000c6 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000c6:	55                   	push   %ebp
  8000c7:	89 e5                	mov    %esp,%ebp
  8000c9:	57                   	push   %edi
  8000ca:	56                   	push   %esi
  8000cb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cc:	b8 00 00 00 00       	mov    $0x0,%eax
  8000d1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000d4:	8b 55 08             	mov    0x8(%ebp),%edx
  8000d7:	89 c3                	mov    %eax,%ebx
  8000d9:	89 c7                	mov    %eax,%edi
  8000db:	89 c6                	mov    %eax,%esi
  8000dd:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000df:	5b                   	pop    %ebx
  8000e0:	5e                   	pop    %esi
  8000e1:	5f                   	pop    %edi
  8000e2:	5d                   	pop    %ebp
  8000e3:	c3                   	ret    

008000e4 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000e4:	55                   	push   %ebp
  8000e5:	89 e5                	mov    %esp,%ebp
  8000e7:	57                   	push   %edi
  8000e8:	56                   	push   %esi
  8000e9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ea:	ba 00 00 00 00       	mov    $0x0,%edx
  8000ef:	b8 01 00 00 00       	mov    $0x1,%eax
  8000f4:	89 d1                	mov    %edx,%ecx
  8000f6:	89 d3                	mov    %edx,%ebx
  8000f8:	89 d7                	mov    %edx,%edi
  8000fa:	89 d6                	mov    %edx,%esi
  8000fc:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000fe:	5b                   	pop    %ebx
  8000ff:	5e                   	pop    %esi
  800100:	5f                   	pop    %edi
  800101:	5d                   	pop    %ebp
  800102:	c3                   	ret    

00800103 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800103:	55                   	push   %ebp
  800104:	89 e5                	mov    %esp,%ebp
  800106:	57                   	push   %edi
  800107:	56                   	push   %esi
  800108:	53                   	push   %ebx
  800109:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80010c:	b9 00 00 00 00       	mov    $0x0,%ecx
  800111:	b8 03 00 00 00       	mov    $0x3,%eax
  800116:	8b 55 08             	mov    0x8(%ebp),%edx
  800119:	89 cb                	mov    %ecx,%ebx
  80011b:	89 cf                	mov    %ecx,%edi
  80011d:	89 ce                	mov    %ecx,%esi
  80011f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800121:	85 c0                	test   %eax,%eax
  800123:	7e 17                	jle    80013c <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800125:	83 ec 0c             	sub    $0xc,%esp
  800128:	50                   	push   %eax
  800129:	6a 03                	push   $0x3
  80012b:	68 6a 0f 80 00       	push   $0x800f6a
  800130:	6a 23                	push   $0x23
  800132:	68 87 0f 80 00       	push   $0x800f87
  800137:	e8 56 02 00 00       	call   800392 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80013c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80013f:	5b                   	pop    %ebx
  800140:	5e                   	pop    %esi
  800141:	5f                   	pop    %edi
  800142:	5d                   	pop    %ebp
  800143:	c3                   	ret    

00800144 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800144:	55                   	push   %ebp
  800145:	89 e5                	mov    %esp,%ebp
  800147:	57                   	push   %edi
  800148:	56                   	push   %esi
  800149:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80014a:	ba 00 00 00 00       	mov    $0x0,%edx
  80014f:	b8 02 00 00 00       	mov    $0x2,%eax
  800154:	89 d1                	mov    %edx,%ecx
  800156:	89 d3                	mov    %edx,%ebx
  800158:	89 d7                	mov    %edx,%edi
  80015a:	89 d6                	mov    %edx,%esi
  80015c:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80015e:	5b                   	pop    %ebx
  80015f:	5e                   	pop    %esi
  800160:	5f                   	pop    %edi
  800161:	5d                   	pop    %ebp
  800162:	c3                   	ret    

00800163 <sys_yield>:

void
sys_yield(void)
{
  800163:	55                   	push   %ebp
  800164:	89 e5                	mov    %esp,%ebp
  800166:	57                   	push   %edi
  800167:	56                   	push   %esi
  800168:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800169:	ba 00 00 00 00       	mov    $0x0,%edx
  80016e:	b8 0b 00 00 00       	mov    $0xb,%eax
  800173:	89 d1                	mov    %edx,%ecx
  800175:	89 d3                	mov    %edx,%ebx
  800177:	89 d7                	mov    %edx,%edi
  800179:	89 d6                	mov    %edx,%esi
  80017b:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80017d:	5b                   	pop    %ebx
  80017e:	5e                   	pop    %esi
  80017f:	5f                   	pop    %edi
  800180:	5d                   	pop    %ebp
  800181:	c3                   	ret    

00800182 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800182:	55                   	push   %ebp
  800183:	89 e5                	mov    %esp,%ebp
  800185:	57                   	push   %edi
  800186:	56                   	push   %esi
  800187:	53                   	push   %ebx
  800188:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80018b:	be 00 00 00 00       	mov    $0x0,%esi
  800190:	b8 04 00 00 00       	mov    $0x4,%eax
  800195:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800198:	8b 55 08             	mov    0x8(%ebp),%edx
  80019b:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80019e:	89 f7                	mov    %esi,%edi
  8001a0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001a2:	85 c0                	test   %eax,%eax
  8001a4:	7e 17                	jle    8001bd <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001a6:	83 ec 0c             	sub    $0xc,%esp
  8001a9:	50                   	push   %eax
  8001aa:	6a 04                	push   $0x4
  8001ac:	68 6a 0f 80 00       	push   $0x800f6a
  8001b1:	6a 23                	push   $0x23
  8001b3:	68 87 0f 80 00       	push   $0x800f87
  8001b8:	e8 d5 01 00 00       	call   800392 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001c0:	5b                   	pop    %ebx
  8001c1:	5e                   	pop    %esi
  8001c2:	5f                   	pop    %edi
  8001c3:	5d                   	pop    %ebp
  8001c4:	c3                   	ret    

008001c5 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001c5:	55                   	push   %ebp
  8001c6:	89 e5                	mov    %esp,%ebp
  8001c8:	57                   	push   %edi
  8001c9:	56                   	push   %esi
  8001ca:	53                   	push   %ebx
  8001cb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ce:	b8 05 00 00 00       	mov    $0x5,%eax
  8001d3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001d6:	8b 55 08             	mov    0x8(%ebp),%edx
  8001d9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001dc:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001df:	8b 75 18             	mov    0x18(%ebp),%esi
  8001e2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001e4:	85 c0                	test   %eax,%eax
  8001e6:	7e 17                	jle    8001ff <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001e8:	83 ec 0c             	sub    $0xc,%esp
  8001eb:	50                   	push   %eax
  8001ec:	6a 05                	push   $0x5
  8001ee:	68 6a 0f 80 00       	push   $0x800f6a
  8001f3:	6a 23                	push   $0x23
  8001f5:	68 87 0f 80 00       	push   $0x800f87
  8001fa:	e8 93 01 00 00       	call   800392 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800202:	5b                   	pop    %ebx
  800203:	5e                   	pop    %esi
  800204:	5f                   	pop    %edi
  800205:	5d                   	pop    %ebp
  800206:	c3                   	ret    

00800207 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800207:	55                   	push   %ebp
  800208:	89 e5                	mov    %esp,%ebp
  80020a:	57                   	push   %edi
  80020b:	56                   	push   %esi
  80020c:	53                   	push   %ebx
  80020d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800210:	bb 00 00 00 00       	mov    $0x0,%ebx
  800215:	b8 06 00 00 00       	mov    $0x6,%eax
  80021a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80021d:	8b 55 08             	mov    0x8(%ebp),%edx
  800220:	89 df                	mov    %ebx,%edi
  800222:	89 de                	mov    %ebx,%esi
  800224:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800226:	85 c0                	test   %eax,%eax
  800228:	7e 17                	jle    800241 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80022a:	83 ec 0c             	sub    $0xc,%esp
  80022d:	50                   	push   %eax
  80022e:	6a 06                	push   $0x6
  800230:	68 6a 0f 80 00       	push   $0x800f6a
  800235:	6a 23                	push   $0x23
  800237:	68 87 0f 80 00       	push   $0x800f87
  80023c:	e8 51 01 00 00       	call   800392 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800241:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800244:	5b                   	pop    %ebx
  800245:	5e                   	pop    %esi
  800246:	5f                   	pop    %edi
  800247:	5d                   	pop    %ebp
  800248:	c3                   	ret    

00800249 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800249:	55                   	push   %ebp
  80024a:	89 e5                	mov    %esp,%ebp
  80024c:	57                   	push   %edi
  80024d:	56                   	push   %esi
  80024e:	53                   	push   %ebx
  80024f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800252:	bb 00 00 00 00       	mov    $0x0,%ebx
  800257:	b8 08 00 00 00       	mov    $0x8,%eax
  80025c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80025f:	8b 55 08             	mov    0x8(%ebp),%edx
  800262:	89 df                	mov    %ebx,%edi
  800264:	89 de                	mov    %ebx,%esi
  800266:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800268:	85 c0                	test   %eax,%eax
  80026a:	7e 17                	jle    800283 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80026c:	83 ec 0c             	sub    $0xc,%esp
  80026f:	50                   	push   %eax
  800270:	6a 08                	push   $0x8
  800272:	68 6a 0f 80 00       	push   $0x800f6a
  800277:	6a 23                	push   $0x23
  800279:	68 87 0f 80 00       	push   $0x800f87
  80027e:	e8 0f 01 00 00       	call   800392 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800283:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800286:	5b                   	pop    %ebx
  800287:	5e                   	pop    %esi
  800288:	5f                   	pop    %edi
  800289:	5d                   	pop    %ebp
  80028a:	c3                   	ret    

0080028b <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80028b:	55                   	push   %ebp
  80028c:	89 e5                	mov    %esp,%ebp
  80028e:	57                   	push   %edi
  80028f:	56                   	push   %esi
  800290:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800291:	ba 00 00 00 00       	mov    $0x0,%edx
  800296:	b8 0c 00 00 00       	mov    $0xc,%eax
  80029b:	89 d1                	mov    %edx,%ecx
  80029d:	89 d3                	mov    %edx,%ebx
  80029f:	89 d7                	mov    %edx,%edi
  8002a1:	89 d6                	mov    %edx,%esi
  8002a3:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  8002a5:	5b                   	pop    %ebx
  8002a6:	5e                   	pop    %esi
  8002a7:	5f                   	pop    %edi
  8002a8:	5d                   	pop    %ebp
  8002a9:	c3                   	ret    

008002aa <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  8002aa:	55                   	push   %ebp
  8002ab:	89 e5                	mov    %esp,%ebp
  8002ad:	57                   	push   %edi
  8002ae:	56                   	push   %esi
  8002af:	53                   	push   %ebx
  8002b0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002b3:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002b8:	b8 09 00 00 00       	mov    $0x9,%eax
  8002bd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002c0:	8b 55 08             	mov    0x8(%ebp),%edx
  8002c3:	89 df                	mov    %ebx,%edi
  8002c5:	89 de                	mov    %ebx,%esi
  8002c7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002c9:	85 c0                	test   %eax,%eax
  8002cb:	7e 17                	jle    8002e4 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002cd:	83 ec 0c             	sub    $0xc,%esp
  8002d0:	50                   	push   %eax
  8002d1:	6a 09                	push   $0x9
  8002d3:	68 6a 0f 80 00       	push   $0x800f6a
  8002d8:	6a 23                	push   $0x23
  8002da:	68 87 0f 80 00       	push   $0x800f87
  8002df:	e8 ae 00 00 00       	call   800392 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002e4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002e7:	5b                   	pop    %ebx
  8002e8:	5e                   	pop    %esi
  8002e9:	5f                   	pop    %edi
  8002ea:	5d                   	pop    %ebp
  8002eb:	c3                   	ret    

008002ec <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002ec:	55                   	push   %ebp
  8002ed:	89 e5                	mov    %esp,%ebp
  8002ef:	57                   	push   %edi
  8002f0:	56                   	push   %esi
  8002f1:	53                   	push   %ebx
  8002f2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002f5:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002fa:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002ff:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800302:	8b 55 08             	mov    0x8(%ebp),%edx
  800305:	89 df                	mov    %ebx,%edi
  800307:	89 de                	mov    %ebx,%esi
  800309:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80030b:	85 c0                	test   %eax,%eax
  80030d:	7e 17                	jle    800326 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80030f:	83 ec 0c             	sub    $0xc,%esp
  800312:	50                   	push   %eax
  800313:	6a 0a                	push   $0xa
  800315:	68 6a 0f 80 00       	push   $0x800f6a
  80031a:	6a 23                	push   $0x23
  80031c:	68 87 0f 80 00       	push   $0x800f87
  800321:	e8 6c 00 00 00       	call   800392 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800326:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800329:	5b                   	pop    %ebx
  80032a:	5e                   	pop    %esi
  80032b:	5f                   	pop    %edi
  80032c:	5d                   	pop    %ebp
  80032d:	c3                   	ret    

0080032e <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  80032e:	55                   	push   %ebp
  80032f:	89 e5                	mov    %esp,%ebp
  800331:	57                   	push   %edi
  800332:	56                   	push   %esi
  800333:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800334:	be 00 00 00 00       	mov    $0x0,%esi
  800339:	b8 0d 00 00 00       	mov    $0xd,%eax
  80033e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800341:	8b 55 08             	mov    0x8(%ebp),%edx
  800344:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800347:	8b 7d 14             	mov    0x14(%ebp),%edi
  80034a:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  80034c:	5b                   	pop    %ebx
  80034d:	5e                   	pop    %esi
  80034e:	5f                   	pop    %edi
  80034f:	5d                   	pop    %ebp
  800350:	c3                   	ret    

00800351 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800351:	55                   	push   %ebp
  800352:	89 e5                	mov    %esp,%ebp
  800354:	57                   	push   %edi
  800355:	56                   	push   %esi
  800356:	53                   	push   %ebx
  800357:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80035a:	b9 00 00 00 00       	mov    $0x0,%ecx
  80035f:	b8 0e 00 00 00       	mov    $0xe,%eax
  800364:	8b 55 08             	mov    0x8(%ebp),%edx
  800367:	89 cb                	mov    %ecx,%ebx
  800369:	89 cf                	mov    %ecx,%edi
  80036b:	89 ce                	mov    %ecx,%esi
  80036d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80036f:	85 c0                	test   %eax,%eax
  800371:	7e 17                	jle    80038a <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800373:	83 ec 0c             	sub    $0xc,%esp
  800376:	50                   	push   %eax
  800377:	6a 0e                	push   $0xe
  800379:	68 6a 0f 80 00       	push   $0x800f6a
  80037e:	6a 23                	push   $0x23
  800380:	68 87 0f 80 00       	push   $0x800f87
  800385:	e8 08 00 00 00       	call   800392 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80038a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80038d:	5b                   	pop    %ebx
  80038e:	5e                   	pop    %esi
  80038f:	5f                   	pop    %edi
  800390:	5d                   	pop    %ebp
  800391:	c3                   	ret    

00800392 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800392:	55                   	push   %ebp
  800393:	89 e5                	mov    %esp,%ebp
  800395:	56                   	push   %esi
  800396:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800397:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80039a:	8b 35 00 20 80 00    	mov    0x802000,%esi
  8003a0:	e8 9f fd ff ff       	call   800144 <sys_getenvid>
  8003a5:	83 ec 0c             	sub    $0xc,%esp
  8003a8:	ff 75 0c             	pushl  0xc(%ebp)
  8003ab:	ff 75 08             	pushl  0x8(%ebp)
  8003ae:	56                   	push   %esi
  8003af:	50                   	push   %eax
  8003b0:	68 98 0f 80 00       	push   $0x800f98
  8003b5:	e8 b0 00 00 00       	call   80046a <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8003ba:	83 c4 18             	add    $0x18,%esp
  8003bd:	53                   	push   %ebx
  8003be:	ff 75 10             	pushl  0x10(%ebp)
  8003c1:	e8 53 00 00 00       	call   800419 <vcprintf>
	cprintf("\n");
  8003c6:	c7 04 24 bb 0f 80 00 	movl   $0x800fbb,(%esp)
  8003cd:	e8 98 00 00 00       	call   80046a <cprintf>
  8003d2:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003d5:	cc                   	int3   
  8003d6:	eb fd                	jmp    8003d5 <_panic+0x43>

008003d8 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003d8:	55                   	push   %ebp
  8003d9:	89 e5                	mov    %esp,%ebp
  8003db:	53                   	push   %ebx
  8003dc:	83 ec 04             	sub    $0x4,%esp
  8003df:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003e2:	8b 13                	mov    (%ebx),%edx
  8003e4:	8d 42 01             	lea    0x1(%edx),%eax
  8003e7:	89 03                	mov    %eax,(%ebx)
  8003e9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003ec:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003f0:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003f5:	75 1a                	jne    800411 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003f7:	83 ec 08             	sub    $0x8,%esp
  8003fa:	68 ff 00 00 00       	push   $0xff
  8003ff:	8d 43 08             	lea    0x8(%ebx),%eax
  800402:	50                   	push   %eax
  800403:	e8 be fc ff ff       	call   8000c6 <sys_cputs>
		b->idx = 0;
  800408:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80040e:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800411:	ff 43 04             	incl   0x4(%ebx)
}
  800414:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800417:	c9                   	leave  
  800418:	c3                   	ret    

00800419 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800419:	55                   	push   %ebp
  80041a:	89 e5                	mov    %esp,%ebp
  80041c:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800422:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800429:	00 00 00 
	b.cnt = 0;
  80042c:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800433:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800436:	ff 75 0c             	pushl  0xc(%ebp)
  800439:	ff 75 08             	pushl  0x8(%ebp)
  80043c:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800442:	50                   	push   %eax
  800443:	68 d8 03 80 00       	push   $0x8003d8
  800448:	e8 51 01 00 00       	call   80059e <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80044d:	83 c4 08             	add    $0x8,%esp
  800450:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800456:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80045c:	50                   	push   %eax
  80045d:	e8 64 fc ff ff       	call   8000c6 <sys_cputs>

	return b.cnt;
}
  800462:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800468:	c9                   	leave  
  800469:	c3                   	ret    

0080046a <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80046a:	55                   	push   %ebp
  80046b:	89 e5                	mov    %esp,%ebp
  80046d:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800470:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800473:	50                   	push   %eax
  800474:	ff 75 08             	pushl  0x8(%ebp)
  800477:	e8 9d ff ff ff       	call   800419 <vcprintf>
	va_end(ap);

	return cnt;
}
  80047c:	c9                   	leave  
  80047d:	c3                   	ret    

0080047e <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80047e:	55                   	push   %ebp
  80047f:	89 e5                	mov    %esp,%ebp
  800481:	57                   	push   %edi
  800482:	56                   	push   %esi
  800483:	53                   	push   %ebx
  800484:	83 ec 1c             	sub    $0x1c,%esp
  800487:	89 c7                	mov    %eax,%edi
  800489:	89 d6                	mov    %edx,%esi
  80048b:	8b 45 08             	mov    0x8(%ebp),%eax
  80048e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800491:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800494:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800497:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80049a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80049f:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8004a2:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8004a5:	39 d3                	cmp    %edx,%ebx
  8004a7:	72 05                	jb     8004ae <printnum+0x30>
  8004a9:	39 45 10             	cmp    %eax,0x10(%ebp)
  8004ac:	77 45                	ja     8004f3 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8004ae:	83 ec 0c             	sub    $0xc,%esp
  8004b1:	ff 75 18             	pushl  0x18(%ebp)
  8004b4:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b7:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8004ba:	53                   	push   %ebx
  8004bb:	ff 75 10             	pushl  0x10(%ebp)
  8004be:	83 ec 08             	sub    $0x8,%esp
  8004c1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004c4:	ff 75 e0             	pushl  -0x20(%ebp)
  8004c7:	ff 75 dc             	pushl  -0x24(%ebp)
  8004ca:	ff 75 d8             	pushl  -0x28(%ebp)
  8004cd:	e8 26 08 00 00       	call   800cf8 <__udivdi3>
  8004d2:	83 c4 18             	add    $0x18,%esp
  8004d5:	52                   	push   %edx
  8004d6:	50                   	push   %eax
  8004d7:	89 f2                	mov    %esi,%edx
  8004d9:	89 f8                	mov    %edi,%eax
  8004db:	e8 9e ff ff ff       	call   80047e <printnum>
  8004e0:	83 c4 20             	add    $0x20,%esp
  8004e3:	eb 16                	jmp    8004fb <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004e5:	83 ec 08             	sub    $0x8,%esp
  8004e8:	56                   	push   %esi
  8004e9:	ff 75 18             	pushl  0x18(%ebp)
  8004ec:	ff d7                	call   *%edi
  8004ee:	83 c4 10             	add    $0x10,%esp
  8004f1:	eb 03                	jmp    8004f6 <printnum+0x78>
  8004f3:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004f6:	4b                   	dec    %ebx
  8004f7:	85 db                	test   %ebx,%ebx
  8004f9:	7f ea                	jg     8004e5 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004fb:	83 ec 08             	sub    $0x8,%esp
  8004fe:	56                   	push   %esi
  8004ff:	83 ec 04             	sub    $0x4,%esp
  800502:	ff 75 e4             	pushl  -0x1c(%ebp)
  800505:	ff 75 e0             	pushl  -0x20(%ebp)
  800508:	ff 75 dc             	pushl  -0x24(%ebp)
  80050b:	ff 75 d8             	pushl  -0x28(%ebp)
  80050e:	e8 f5 08 00 00       	call   800e08 <__umoddi3>
  800513:	83 c4 14             	add    $0x14,%esp
  800516:	0f be 80 bd 0f 80 00 	movsbl 0x800fbd(%eax),%eax
  80051d:	50                   	push   %eax
  80051e:	ff d7                	call   *%edi
}
  800520:	83 c4 10             	add    $0x10,%esp
  800523:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800526:	5b                   	pop    %ebx
  800527:	5e                   	pop    %esi
  800528:	5f                   	pop    %edi
  800529:	5d                   	pop    %ebp
  80052a:	c3                   	ret    

0080052b <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80052b:	55                   	push   %ebp
  80052c:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80052e:	83 fa 01             	cmp    $0x1,%edx
  800531:	7e 0e                	jle    800541 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800533:	8b 10                	mov    (%eax),%edx
  800535:	8d 4a 08             	lea    0x8(%edx),%ecx
  800538:	89 08                	mov    %ecx,(%eax)
  80053a:	8b 02                	mov    (%edx),%eax
  80053c:	8b 52 04             	mov    0x4(%edx),%edx
  80053f:	eb 22                	jmp    800563 <getuint+0x38>
	else if (lflag)
  800541:	85 d2                	test   %edx,%edx
  800543:	74 10                	je     800555 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800545:	8b 10                	mov    (%eax),%edx
  800547:	8d 4a 04             	lea    0x4(%edx),%ecx
  80054a:	89 08                	mov    %ecx,(%eax)
  80054c:	8b 02                	mov    (%edx),%eax
  80054e:	ba 00 00 00 00       	mov    $0x0,%edx
  800553:	eb 0e                	jmp    800563 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800555:	8b 10                	mov    (%eax),%edx
  800557:	8d 4a 04             	lea    0x4(%edx),%ecx
  80055a:	89 08                	mov    %ecx,(%eax)
  80055c:	8b 02                	mov    (%edx),%eax
  80055e:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800563:	5d                   	pop    %ebp
  800564:	c3                   	ret    

00800565 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800565:	55                   	push   %ebp
  800566:	89 e5                	mov    %esp,%ebp
  800568:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80056b:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80056e:	8b 10                	mov    (%eax),%edx
  800570:	3b 50 04             	cmp    0x4(%eax),%edx
  800573:	73 0a                	jae    80057f <sprintputch+0x1a>
		*b->buf++ = ch;
  800575:	8d 4a 01             	lea    0x1(%edx),%ecx
  800578:	89 08                	mov    %ecx,(%eax)
  80057a:	8b 45 08             	mov    0x8(%ebp),%eax
  80057d:	88 02                	mov    %al,(%edx)
}
  80057f:	5d                   	pop    %ebp
  800580:	c3                   	ret    

00800581 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800581:	55                   	push   %ebp
  800582:	89 e5                	mov    %esp,%ebp
  800584:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800587:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80058a:	50                   	push   %eax
  80058b:	ff 75 10             	pushl  0x10(%ebp)
  80058e:	ff 75 0c             	pushl  0xc(%ebp)
  800591:	ff 75 08             	pushl  0x8(%ebp)
  800594:	e8 05 00 00 00       	call   80059e <vprintfmt>
	va_end(ap);
}
  800599:	83 c4 10             	add    $0x10,%esp
  80059c:	c9                   	leave  
  80059d:	c3                   	ret    

0080059e <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80059e:	55                   	push   %ebp
  80059f:	89 e5                	mov    %esp,%ebp
  8005a1:	57                   	push   %edi
  8005a2:	56                   	push   %esi
  8005a3:	53                   	push   %ebx
  8005a4:	83 ec 2c             	sub    $0x2c,%esp
  8005a7:	8b 75 08             	mov    0x8(%ebp),%esi
  8005aa:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8005ad:	8b 7d 10             	mov    0x10(%ebp),%edi
  8005b0:	eb 12                	jmp    8005c4 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8005b2:	85 c0                	test   %eax,%eax
  8005b4:	0f 84 68 03 00 00    	je     800922 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8005ba:	83 ec 08             	sub    $0x8,%esp
  8005bd:	53                   	push   %ebx
  8005be:	50                   	push   %eax
  8005bf:	ff d6                	call   *%esi
  8005c1:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8005c4:	47                   	inc    %edi
  8005c5:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005c9:	83 f8 25             	cmp    $0x25,%eax
  8005cc:	75 e4                	jne    8005b2 <vprintfmt+0x14>
  8005ce:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005d2:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005d9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005e0:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005e7:	ba 00 00 00 00       	mov    $0x0,%edx
  8005ec:	eb 07                	jmp    8005f5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ee:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005f1:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f5:	8d 47 01             	lea    0x1(%edi),%eax
  8005f8:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005fb:	0f b6 0f             	movzbl (%edi),%ecx
  8005fe:	8a 07                	mov    (%edi),%al
  800600:	83 e8 23             	sub    $0x23,%eax
  800603:	3c 55                	cmp    $0x55,%al
  800605:	0f 87 fe 02 00 00    	ja     800909 <vprintfmt+0x36b>
  80060b:	0f b6 c0             	movzbl %al,%eax
  80060e:	ff 24 85 00 11 80 00 	jmp    *0x801100(,%eax,4)
  800615:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800618:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80061c:	eb d7                	jmp    8005f5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80061e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800621:	b8 00 00 00 00       	mov    $0x0,%eax
  800626:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800629:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80062c:	01 c0                	add    %eax,%eax
  80062e:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800632:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800635:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800638:	83 fa 09             	cmp    $0x9,%edx
  80063b:	77 34                	ja     800671 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80063d:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80063e:	eb e9                	jmp    800629 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800640:	8b 45 14             	mov    0x14(%ebp),%eax
  800643:	8d 48 04             	lea    0x4(%eax),%ecx
  800646:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800649:	8b 00                	mov    (%eax),%eax
  80064b:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80064e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800651:	eb 24                	jmp    800677 <vprintfmt+0xd9>
  800653:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800657:	79 07                	jns    800660 <vprintfmt+0xc2>
  800659:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800660:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800663:	eb 90                	jmp    8005f5 <vprintfmt+0x57>
  800665:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800668:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80066f:	eb 84                	jmp    8005f5 <vprintfmt+0x57>
  800671:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800674:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800677:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80067b:	0f 89 74 ff ff ff    	jns    8005f5 <vprintfmt+0x57>
				width = precision, precision = -1;
  800681:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800684:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800687:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80068e:	e9 62 ff ff ff       	jmp    8005f5 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800693:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800694:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800697:	e9 59 ff ff ff       	jmp    8005f5 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80069c:	8b 45 14             	mov    0x14(%ebp),%eax
  80069f:	8d 50 04             	lea    0x4(%eax),%edx
  8006a2:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a5:	83 ec 08             	sub    $0x8,%esp
  8006a8:	53                   	push   %ebx
  8006a9:	ff 30                	pushl  (%eax)
  8006ab:	ff d6                	call   *%esi
			break;
  8006ad:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006b0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8006b3:	e9 0c ff ff ff       	jmp    8005c4 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8006b8:	8b 45 14             	mov    0x14(%ebp),%eax
  8006bb:	8d 50 04             	lea    0x4(%eax),%edx
  8006be:	89 55 14             	mov    %edx,0x14(%ebp)
  8006c1:	8b 00                	mov    (%eax),%eax
  8006c3:	85 c0                	test   %eax,%eax
  8006c5:	79 02                	jns    8006c9 <vprintfmt+0x12b>
  8006c7:	f7 d8                	neg    %eax
  8006c9:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006cb:	83 f8 0f             	cmp    $0xf,%eax
  8006ce:	7f 0b                	jg     8006db <vprintfmt+0x13d>
  8006d0:	8b 04 85 60 12 80 00 	mov    0x801260(,%eax,4),%eax
  8006d7:	85 c0                	test   %eax,%eax
  8006d9:	75 18                	jne    8006f3 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006db:	52                   	push   %edx
  8006dc:	68 d5 0f 80 00       	push   $0x800fd5
  8006e1:	53                   	push   %ebx
  8006e2:	56                   	push   %esi
  8006e3:	e8 99 fe ff ff       	call   800581 <printfmt>
  8006e8:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006eb:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006ee:	e9 d1 fe ff ff       	jmp    8005c4 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006f3:	50                   	push   %eax
  8006f4:	68 de 0f 80 00       	push   $0x800fde
  8006f9:	53                   	push   %ebx
  8006fa:	56                   	push   %esi
  8006fb:	e8 81 fe ff ff       	call   800581 <printfmt>
  800700:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800703:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800706:	e9 b9 fe ff ff       	jmp    8005c4 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80070b:	8b 45 14             	mov    0x14(%ebp),%eax
  80070e:	8d 50 04             	lea    0x4(%eax),%edx
  800711:	89 55 14             	mov    %edx,0x14(%ebp)
  800714:	8b 38                	mov    (%eax),%edi
  800716:	85 ff                	test   %edi,%edi
  800718:	75 05                	jne    80071f <vprintfmt+0x181>
				p = "(null)";
  80071a:	bf ce 0f 80 00       	mov    $0x800fce,%edi
			if (width > 0 && padc != '-')
  80071f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800723:	0f 8e 90 00 00 00    	jle    8007b9 <vprintfmt+0x21b>
  800729:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80072d:	0f 84 8e 00 00 00    	je     8007c1 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800733:	83 ec 08             	sub    $0x8,%esp
  800736:	ff 75 d0             	pushl  -0x30(%ebp)
  800739:	57                   	push   %edi
  80073a:	e8 70 02 00 00       	call   8009af <strnlen>
  80073f:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800742:	29 c1                	sub    %eax,%ecx
  800744:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800747:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80074a:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80074e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800751:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800754:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800756:	eb 0d                	jmp    800765 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800758:	83 ec 08             	sub    $0x8,%esp
  80075b:	53                   	push   %ebx
  80075c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80075f:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800761:	4f                   	dec    %edi
  800762:	83 c4 10             	add    $0x10,%esp
  800765:	85 ff                	test   %edi,%edi
  800767:	7f ef                	jg     800758 <vprintfmt+0x1ba>
  800769:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80076c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80076f:	89 c8                	mov    %ecx,%eax
  800771:	85 c9                	test   %ecx,%ecx
  800773:	79 05                	jns    80077a <vprintfmt+0x1dc>
  800775:	b8 00 00 00 00       	mov    $0x0,%eax
  80077a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80077d:	29 c1                	sub    %eax,%ecx
  80077f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800782:	89 75 08             	mov    %esi,0x8(%ebp)
  800785:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800788:	eb 3d                	jmp    8007c7 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80078a:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80078e:	74 19                	je     8007a9 <vprintfmt+0x20b>
  800790:	0f be c0             	movsbl %al,%eax
  800793:	83 e8 20             	sub    $0x20,%eax
  800796:	83 f8 5e             	cmp    $0x5e,%eax
  800799:	76 0e                	jbe    8007a9 <vprintfmt+0x20b>
					putch('?', putdat);
  80079b:	83 ec 08             	sub    $0x8,%esp
  80079e:	53                   	push   %ebx
  80079f:	6a 3f                	push   $0x3f
  8007a1:	ff 55 08             	call   *0x8(%ebp)
  8007a4:	83 c4 10             	add    $0x10,%esp
  8007a7:	eb 0b                	jmp    8007b4 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8007a9:	83 ec 08             	sub    $0x8,%esp
  8007ac:	53                   	push   %ebx
  8007ad:	52                   	push   %edx
  8007ae:	ff 55 08             	call   *0x8(%ebp)
  8007b1:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8007b4:	ff 4d e4             	decl   -0x1c(%ebp)
  8007b7:	eb 0e                	jmp    8007c7 <vprintfmt+0x229>
  8007b9:	89 75 08             	mov    %esi,0x8(%ebp)
  8007bc:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007bf:	eb 06                	jmp    8007c7 <vprintfmt+0x229>
  8007c1:	89 75 08             	mov    %esi,0x8(%ebp)
  8007c4:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007c7:	47                   	inc    %edi
  8007c8:	8a 47 ff             	mov    -0x1(%edi),%al
  8007cb:	0f be d0             	movsbl %al,%edx
  8007ce:	85 d2                	test   %edx,%edx
  8007d0:	74 1d                	je     8007ef <vprintfmt+0x251>
  8007d2:	85 f6                	test   %esi,%esi
  8007d4:	78 b4                	js     80078a <vprintfmt+0x1ec>
  8007d6:	4e                   	dec    %esi
  8007d7:	79 b1                	jns    80078a <vprintfmt+0x1ec>
  8007d9:	8b 75 08             	mov    0x8(%ebp),%esi
  8007dc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007df:	eb 14                	jmp    8007f5 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007e1:	83 ec 08             	sub    $0x8,%esp
  8007e4:	53                   	push   %ebx
  8007e5:	6a 20                	push   $0x20
  8007e7:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007e9:	4f                   	dec    %edi
  8007ea:	83 c4 10             	add    $0x10,%esp
  8007ed:	eb 06                	jmp    8007f5 <vprintfmt+0x257>
  8007ef:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007f2:	8b 75 08             	mov    0x8(%ebp),%esi
  8007f5:	85 ff                	test   %edi,%edi
  8007f7:	7f e8                	jg     8007e1 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007f9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007fc:	e9 c3 fd ff ff       	jmp    8005c4 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800801:	83 fa 01             	cmp    $0x1,%edx
  800804:	7e 16                	jle    80081c <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800806:	8b 45 14             	mov    0x14(%ebp),%eax
  800809:	8d 50 08             	lea    0x8(%eax),%edx
  80080c:	89 55 14             	mov    %edx,0x14(%ebp)
  80080f:	8b 50 04             	mov    0x4(%eax),%edx
  800812:	8b 00                	mov    (%eax),%eax
  800814:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800817:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80081a:	eb 32                	jmp    80084e <vprintfmt+0x2b0>
	else if (lflag)
  80081c:	85 d2                	test   %edx,%edx
  80081e:	74 18                	je     800838 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800820:	8b 45 14             	mov    0x14(%ebp),%eax
  800823:	8d 50 04             	lea    0x4(%eax),%edx
  800826:	89 55 14             	mov    %edx,0x14(%ebp)
  800829:	8b 00                	mov    (%eax),%eax
  80082b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80082e:	89 c1                	mov    %eax,%ecx
  800830:	c1 f9 1f             	sar    $0x1f,%ecx
  800833:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800836:	eb 16                	jmp    80084e <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800838:	8b 45 14             	mov    0x14(%ebp),%eax
  80083b:	8d 50 04             	lea    0x4(%eax),%edx
  80083e:	89 55 14             	mov    %edx,0x14(%ebp)
  800841:	8b 00                	mov    (%eax),%eax
  800843:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800846:	89 c1                	mov    %eax,%ecx
  800848:	c1 f9 1f             	sar    $0x1f,%ecx
  80084b:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80084e:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800851:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800854:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800858:	79 76                	jns    8008d0 <vprintfmt+0x332>
				putch('-', putdat);
  80085a:	83 ec 08             	sub    $0x8,%esp
  80085d:	53                   	push   %ebx
  80085e:	6a 2d                	push   $0x2d
  800860:	ff d6                	call   *%esi
				num = -(long long) num;
  800862:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800865:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800868:	f7 d8                	neg    %eax
  80086a:	83 d2 00             	adc    $0x0,%edx
  80086d:	f7 da                	neg    %edx
  80086f:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800872:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800877:	eb 5c                	jmp    8008d5 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800879:	8d 45 14             	lea    0x14(%ebp),%eax
  80087c:	e8 aa fc ff ff       	call   80052b <getuint>
			base = 10;
  800881:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800886:	eb 4d                	jmp    8008d5 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800888:	8d 45 14             	lea    0x14(%ebp),%eax
  80088b:	e8 9b fc ff ff       	call   80052b <getuint>
			base = 8;
  800890:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800895:	eb 3e                	jmp    8008d5 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800897:	83 ec 08             	sub    $0x8,%esp
  80089a:	53                   	push   %ebx
  80089b:	6a 30                	push   $0x30
  80089d:	ff d6                	call   *%esi
			putch('x', putdat);
  80089f:	83 c4 08             	add    $0x8,%esp
  8008a2:	53                   	push   %ebx
  8008a3:	6a 78                	push   $0x78
  8008a5:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8008a7:	8b 45 14             	mov    0x14(%ebp),%eax
  8008aa:	8d 50 04             	lea    0x4(%eax),%edx
  8008ad:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8008b0:	8b 00                	mov    (%eax),%eax
  8008b2:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8008b7:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8008ba:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8008bf:	eb 14                	jmp    8008d5 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8008c1:	8d 45 14             	lea    0x14(%ebp),%eax
  8008c4:	e8 62 fc ff ff       	call   80052b <getuint>
			base = 16;
  8008c9:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008ce:	eb 05                	jmp    8008d5 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008d0:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008d5:	83 ec 0c             	sub    $0xc,%esp
  8008d8:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008dc:	57                   	push   %edi
  8008dd:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008e0:	51                   	push   %ecx
  8008e1:	52                   	push   %edx
  8008e2:	50                   	push   %eax
  8008e3:	89 da                	mov    %ebx,%edx
  8008e5:	89 f0                	mov    %esi,%eax
  8008e7:	e8 92 fb ff ff       	call   80047e <printnum>
			break;
  8008ec:	83 c4 20             	add    $0x20,%esp
  8008ef:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008f2:	e9 cd fc ff ff       	jmp    8005c4 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008f7:	83 ec 08             	sub    $0x8,%esp
  8008fa:	53                   	push   %ebx
  8008fb:	51                   	push   %ecx
  8008fc:	ff d6                	call   *%esi
			break;
  8008fe:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800901:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800904:	e9 bb fc ff ff       	jmp    8005c4 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800909:	83 ec 08             	sub    $0x8,%esp
  80090c:	53                   	push   %ebx
  80090d:	6a 25                	push   $0x25
  80090f:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800911:	83 c4 10             	add    $0x10,%esp
  800914:	eb 01                	jmp    800917 <vprintfmt+0x379>
  800916:	4f                   	dec    %edi
  800917:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80091b:	75 f9                	jne    800916 <vprintfmt+0x378>
  80091d:	e9 a2 fc ff ff       	jmp    8005c4 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800922:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800925:	5b                   	pop    %ebx
  800926:	5e                   	pop    %esi
  800927:	5f                   	pop    %edi
  800928:	5d                   	pop    %ebp
  800929:	c3                   	ret    

0080092a <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80092a:	55                   	push   %ebp
  80092b:	89 e5                	mov    %esp,%ebp
  80092d:	83 ec 18             	sub    $0x18,%esp
  800930:	8b 45 08             	mov    0x8(%ebp),%eax
  800933:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800936:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800939:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80093d:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800940:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800947:	85 c0                	test   %eax,%eax
  800949:	74 26                	je     800971 <vsnprintf+0x47>
  80094b:	85 d2                	test   %edx,%edx
  80094d:	7e 29                	jle    800978 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80094f:	ff 75 14             	pushl  0x14(%ebp)
  800952:	ff 75 10             	pushl  0x10(%ebp)
  800955:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800958:	50                   	push   %eax
  800959:	68 65 05 80 00       	push   $0x800565
  80095e:	e8 3b fc ff ff       	call   80059e <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800963:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800966:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800969:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80096c:	83 c4 10             	add    $0x10,%esp
  80096f:	eb 0c                	jmp    80097d <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800971:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800976:	eb 05                	jmp    80097d <vsnprintf+0x53>
  800978:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80097d:	c9                   	leave  
  80097e:	c3                   	ret    

0080097f <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80097f:	55                   	push   %ebp
  800980:	89 e5                	mov    %esp,%ebp
  800982:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800985:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800988:	50                   	push   %eax
  800989:	ff 75 10             	pushl  0x10(%ebp)
  80098c:	ff 75 0c             	pushl  0xc(%ebp)
  80098f:	ff 75 08             	pushl  0x8(%ebp)
  800992:	e8 93 ff ff ff       	call   80092a <vsnprintf>
	va_end(ap);

	return rc;
}
  800997:	c9                   	leave  
  800998:	c3                   	ret    

00800999 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800999:	55                   	push   %ebp
  80099a:	89 e5                	mov    %esp,%ebp
  80099c:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80099f:	b8 00 00 00 00       	mov    $0x0,%eax
  8009a4:	eb 01                	jmp    8009a7 <strlen+0xe>
		n++;
  8009a6:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8009a7:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8009ab:	75 f9                	jne    8009a6 <strlen+0xd>
		n++;
	return n;
}
  8009ad:	5d                   	pop    %ebp
  8009ae:	c3                   	ret    

008009af <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8009af:	55                   	push   %ebp
  8009b0:	89 e5                	mov    %esp,%ebp
  8009b2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8009b5:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009b8:	ba 00 00 00 00       	mov    $0x0,%edx
  8009bd:	eb 01                	jmp    8009c0 <strnlen+0x11>
		n++;
  8009bf:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009c0:	39 c2                	cmp    %eax,%edx
  8009c2:	74 08                	je     8009cc <strnlen+0x1d>
  8009c4:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009c8:	75 f5                	jne    8009bf <strnlen+0x10>
  8009ca:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009cc:	5d                   	pop    %ebp
  8009cd:	c3                   	ret    

008009ce <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009ce:	55                   	push   %ebp
  8009cf:	89 e5                	mov    %esp,%ebp
  8009d1:	53                   	push   %ebx
  8009d2:	8b 45 08             	mov    0x8(%ebp),%eax
  8009d5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009d8:	89 c2                	mov    %eax,%edx
  8009da:	42                   	inc    %edx
  8009db:	41                   	inc    %ecx
  8009dc:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009df:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009e2:	84 db                	test   %bl,%bl
  8009e4:	75 f4                	jne    8009da <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009e6:	5b                   	pop    %ebx
  8009e7:	5d                   	pop    %ebp
  8009e8:	c3                   	ret    

008009e9 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009e9:	55                   	push   %ebp
  8009ea:	89 e5                	mov    %esp,%ebp
  8009ec:	53                   	push   %ebx
  8009ed:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009f0:	53                   	push   %ebx
  8009f1:	e8 a3 ff ff ff       	call   800999 <strlen>
  8009f6:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009f9:	ff 75 0c             	pushl  0xc(%ebp)
  8009fc:	01 d8                	add    %ebx,%eax
  8009fe:	50                   	push   %eax
  8009ff:	e8 ca ff ff ff       	call   8009ce <strcpy>
	return dst;
}
  800a04:	89 d8                	mov    %ebx,%eax
  800a06:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800a09:	c9                   	leave  
  800a0a:	c3                   	ret    

00800a0b <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800a0b:	55                   	push   %ebp
  800a0c:	89 e5                	mov    %esp,%ebp
  800a0e:	56                   	push   %esi
  800a0f:	53                   	push   %ebx
  800a10:	8b 75 08             	mov    0x8(%ebp),%esi
  800a13:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a16:	89 f3                	mov    %esi,%ebx
  800a18:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a1b:	89 f2                	mov    %esi,%edx
  800a1d:	eb 0c                	jmp    800a2b <strncpy+0x20>
		*dst++ = *src;
  800a1f:	42                   	inc    %edx
  800a20:	8a 01                	mov    (%ecx),%al
  800a22:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a25:	80 39 01             	cmpb   $0x1,(%ecx)
  800a28:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a2b:	39 da                	cmp    %ebx,%edx
  800a2d:	75 f0                	jne    800a1f <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a2f:	89 f0                	mov    %esi,%eax
  800a31:	5b                   	pop    %ebx
  800a32:	5e                   	pop    %esi
  800a33:	5d                   	pop    %ebp
  800a34:	c3                   	ret    

00800a35 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a35:	55                   	push   %ebp
  800a36:	89 e5                	mov    %esp,%ebp
  800a38:	56                   	push   %esi
  800a39:	53                   	push   %ebx
  800a3a:	8b 75 08             	mov    0x8(%ebp),%esi
  800a3d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a40:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a43:	85 c0                	test   %eax,%eax
  800a45:	74 1e                	je     800a65 <strlcpy+0x30>
  800a47:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a4b:	89 f2                	mov    %esi,%edx
  800a4d:	eb 05                	jmp    800a54 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a4f:	42                   	inc    %edx
  800a50:	41                   	inc    %ecx
  800a51:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a54:	39 c2                	cmp    %eax,%edx
  800a56:	74 08                	je     800a60 <strlcpy+0x2b>
  800a58:	8a 19                	mov    (%ecx),%bl
  800a5a:	84 db                	test   %bl,%bl
  800a5c:	75 f1                	jne    800a4f <strlcpy+0x1a>
  800a5e:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a60:	c6 00 00             	movb   $0x0,(%eax)
  800a63:	eb 02                	jmp    800a67 <strlcpy+0x32>
  800a65:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a67:	29 f0                	sub    %esi,%eax
}
  800a69:	5b                   	pop    %ebx
  800a6a:	5e                   	pop    %esi
  800a6b:	5d                   	pop    %ebp
  800a6c:	c3                   	ret    

00800a6d <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a6d:	55                   	push   %ebp
  800a6e:	89 e5                	mov    %esp,%ebp
  800a70:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a73:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a76:	eb 02                	jmp    800a7a <strcmp+0xd>
		p++, q++;
  800a78:	41                   	inc    %ecx
  800a79:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a7a:	8a 01                	mov    (%ecx),%al
  800a7c:	84 c0                	test   %al,%al
  800a7e:	74 04                	je     800a84 <strcmp+0x17>
  800a80:	3a 02                	cmp    (%edx),%al
  800a82:	74 f4                	je     800a78 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a84:	0f b6 c0             	movzbl %al,%eax
  800a87:	0f b6 12             	movzbl (%edx),%edx
  800a8a:	29 d0                	sub    %edx,%eax
}
  800a8c:	5d                   	pop    %ebp
  800a8d:	c3                   	ret    

00800a8e <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a8e:	55                   	push   %ebp
  800a8f:	89 e5                	mov    %esp,%ebp
  800a91:	53                   	push   %ebx
  800a92:	8b 45 08             	mov    0x8(%ebp),%eax
  800a95:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a98:	89 c3                	mov    %eax,%ebx
  800a9a:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a9d:	eb 02                	jmp    800aa1 <strncmp+0x13>
		n--, p++, q++;
  800a9f:	40                   	inc    %eax
  800aa0:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800aa1:	39 d8                	cmp    %ebx,%eax
  800aa3:	74 14                	je     800ab9 <strncmp+0x2b>
  800aa5:	8a 08                	mov    (%eax),%cl
  800aa7:	84 c9                	test   %cl,%cl
  800aa9:	74 04                	je     800aaf <strncmp+0x21>
  800aab:	3a 0a                	cmp    (%edx),%cl
  800aad:	74 f0                	je     800a9f <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800aaf:	0f b6 00             	movzbl (%eax),%eax
  800ab2:	0f b6 12             	movzbl (%edx),%edx
  800ab5:	29 d0                	sub    %edx,%eax
  800ab7:	eb 05                	jmp    800abe <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800ab9:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800abe:	5b                   	pop    %ebx
  800abf:	5d                   	pop    %ebp
  800ac0:	c3                   	ret    

00800ac1 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800ac1:	55                   	push   %ebp
  800ac2:	89 e5                	mov    %esp,%ebp
  800ac4:	8b 45 08             	mov    0x8(%ebp),%eax
  800ac7:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800aca:	eb 05                	jmp    800ad1 <strchr+0x10>
		if (*s == c)
  800acc:	38 ca                	cmp    %cl,%dl
  800ace:	74 0c                	je     800adc <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800ad0:	40                   	inc    %eax
  800ad1:	8a 10                	mov    (%eax),%dl
  800ad3:	84 d2                	test   %dl,%dl
  800ad5:	75 f5                	jne    800acc <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800ad7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800adc:	5d                   	pop    %ebp
  800add:	c3                   	ret    

00800ade <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ade:	55                   	push   %ebp
  800adf:	89 e5                	mov    %esp,%ebp
  800ae1:	8b 45 08             	mov    0x8(%ebp),%eax
  800ae4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ae7:	eb 05                	jmp    800aee <strfind+0x10>
		if (*s == c)
  800ae9:	38 ca                	cmp    %cl,%dl
  800aeb:	74 07                	je     800af4 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800aed:	40                   	inc    %eax
  800aee:	8a 10                	mov    (%eax),%dl
  800af0:	84 d2                	test   %dl,%dl
  800af2:	75 f5                	jne    800ae9 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800af4:	5d                   	pop    %ebp
  800af5:	c3                   	ret    

00800af6 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800af6:	55                   	push   %ebp
  800af7:	89 e5                	mov    %esp,%ebp
  800af9:	57                   	push   %edi
  800afa:	56                   	push   %esi
  800afb:	53                   	push   %ebx
  800afc:	8b 7d 08             	mov    0x8(%ebp),%edi
  800aff:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800b02:	85 c9                	test   %ecx,%ecx
  800b04:	74 36                	je     800b3c <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800b06:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800b0c:	75 28                	jne    800b36 <memset+0x40>
  800b0e:	f6 c1 03             	test   $0x3,%cl
  800b11:	75 23                	jne    800b36 <memset+0x40>
		c &= 0xFF;
  800b13:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800b17:	89 d3                	mov    %edx,%ebx
  800b19:	c1 e3 08             	shl    $0x8,%ebx
  800b1c:	89 d6                	mov    %edx,%esi
  800b1e:	c1 e6 18             	shl    $0x18,%esi
  800b21:	89 d0                	mov    %edx,%eax
  800b23:	c1 e0 10             	shl    $0x10,%eax
  800b26:	09 f0                	or     %esi,%eax
  800b28:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b2a:	89 d8                	mov    %ebx,%eax
  800b2c:	09 d0                	or     %edx,%eax
  800b2e:	c1 e9 02             	shr    $0x2,%ecx
  800b31:	fc                   	cld    
  800b32:	f3 ab                	rep stos %eax,%es:(%edi)
  800b34:	eb 06                	jmp    800b3c <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b36:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b39:	fc                   	cld    
  800b3a:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b3c:	89 f8                	mov    %edi,%eax
  800b3e:	5b                   	pop    %ebx
  800b3f:	5e                   	pop    %esi
  800b40:	5f                   	pop    %edi
  800b41:	5d                   	pop    %ebp
  800b42:	c3                   	ret    

00800b43 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b43:	55                   	push   %ebp
  800b44:	89 e5                	mov    %esp,%ebp
  800b46:	57                   	push   %edi
  800b47:	56                   	push   %esi
  800b48:	8b 45 08             	mov    0x8(%ebp),%eax
  800b4b:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b4e:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b51:	39 c6                	cmp    %eax,%esi
  800b53:	73 33                	jae    800b88 <memmove+0x45>
  800b55:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b58:	39 d0                	cmp    %edx,%eax
  800b5a:	73 2c                	jae    800b88 <memmove+0x45>
		s += n;
		d += n;
  800b5c:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b5f:	89 d6                	mov    %edx,%esi
  800b61:	09 fe                	or     %edi,%esi
  800b63:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b69:	75 13                	jne    800b7e <memmove+0x3b>
  800b6b:	f6 c1 03             	test   $0x3,%cl
  800b6e:	75 0e                	jne    800b7e <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b70:	83 ef 04             	sub    $0x4,%edi
  800b73:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b76:	c1 e9 02             	shr    $0x2,%ecx
  800b79:	fd                   	std    
  800b7a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b7c:	eb 07                	jmp    800b85 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b7e:	4f                   	dec    %edi
  800b7f:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b82:	fd                   	std    
  800b83:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b85:	fc                   	cld    
  800b86:	eb 1d                	jmp    800ba5 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b88:	89 f2                	mov    %esi,%edx
  800b8a:	09 c2                	or     %eax,%edx
  800b8c:	f6 c2 03             	test   $0x3,%dl
  800b8f:	75 0f                	jne    800ba0 <memmove+0x5d>
  800b91:	f6 c1 03             	test   $0x3,%cl
  800b94:	75 0a                	jne    800ba0 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b96:	c1 e9 02             	shr    $0x2,%ecx
  800b99:	89 c7                	mov    %eax,%edi
  800b9b:	fc                   	cld    
  800b9c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b9e:	eb 05                	jmp    800ba5 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800ba0:	89 c7                	mov    %eax,%edi
  800ba2:	fc                   	cld    
  800ba3:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800ba5:	5e                   	pop    %esi
  800ba6:	5f                   	pop    %edi
  800ba7:	5d                   	pop    %ebp
  800ba8:	c3                   	ret    

00800ba9 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800ba9:	55                   	push   %ebp
  800baa:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800bac:	ff 75 10             	pushl  0x10(%ebp)
  800baf:	ff 75 0c             	pushl  0xc(%ebp)
  800bb2:	ff 75 08             	pushl  0x8(%ebp)
  800bb5:	e8 89 ff ff ff       	call   800b43 <memmove>
}
  800bba:	c9                   	leave  
  800bbb:	c3                   	ret    

00800bbc <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800bbc:	55                   	push   %ebp
  800bbd:	89 e5                	mov    %esp,%ebp
  800bbf:	56                   	push   %esi
  800bc0:	53                   	push   %ebx
  800bc1:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc4:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bc7:	89 c6                	mov    %eax,%esi
  800bc9:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bcc:	eb 14                	jmp    800be2 <memcmp+0x26>
		if (*s1 != *s2)
  800bce:	8a 08                	mov    (%eax),%cl
  800bd0:	8a 1a                	mov    (%edx),%bl
  800bd2:	38 d9                	cmp    %bl,%cl
  800bd4:	74 0a                	je     800be0 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bd6:	0f b6 c1             	movzbl %cl,%eax
  800bd9:	0f b6 db             	movzbl %bl,%ebx
  800bdc:	29 d8                	sub    %ebx,%eax
  800bde:	eb 0b                	jmp    800beb <memcmp+0x2f>
		s1++, s2++;
  800be0:	40                   	inc    %eax
  800be1:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800be2:	39 f0                	cmp    %esi,%eax
  800be4:	75 e8                	jne    800bce <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800be6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800beb:	5b                   	pop    %ebx
  800bec:	5e                   	pop    %esi
  800bed:	5d                   	pop    %ebp
  800bee:	c3                   	ret    

00800bef <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bef:	55                   	push   %ebp
  800bf0:	89 e5                	mov    %esp,%ebp
  800bf2:	53                   	push   %ebx
  800bf3:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bf6:	89 c1                	mov    %eax,%ecx
  800bf8:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bfb:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bff:	eb 08                	jmp    800c09 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800c01:	0f b6 10             	movzbl (%eax),%edx
  800c04:	39 da                	cmp    %ebx,%edx
  800c06:	74 05                	je     800c0d <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c08:	40                   	inc    %eax
  800c09:	39 c8                	cmp    %ecx,%eax
  800c0b:	72 f4                	jb     800c01 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800c0d:	5b                   	pop    %ebx
  800c0e:	5d                   	pop    %ebp
  800c0f:	c3                   	ret    

00800c10 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800c10:	55                   	push   %ebp
  800c11:	89 e5                	mov    %esp,%ebp
  800c13:	57                   	push   %edi
  800c14:	56                   	push   %esi
  800c15:	53                   	push   %ebx
  800c16:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c19:	eb 01                	jmp    800c1c <strtol+0xc>
		s++;
  800c1b:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c1c:	8a 01                	mov    (%ecx),%al
  800c1e:	3c 20                	cmp    $0x20,%al
  800c20:	74 f9                	je     800c1b <strtol+0xb>
  800c22:	3c 09                	cmp    $0x9,%al
  800c24:	74 f5                	je     800c1b <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c26:	3c 2b                	cmp    $0x2b,%al
  800c28:	75 08                	jne    800c32 <strtol+0x22>
		s++;
  800c2a:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c2b:	bf 00 00 00 00       	mov    $0x0,%edi
  800c30:	eb 11                	jmp    800c43 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c32:	3c 2d                	cmp    $0x2d,%al
  800c34:	75 08                	jne    800c3e <strtol+0x2e>
		s++, neg = 1;
  800c36:	41                   	inc    %ecx
  800c37:	bf 01 00 00 00       	mov    $0x1,%edi
  800c3c:	eb 05                	jmp    800c43 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c3e:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c43:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c47:	0f 84 87 00 00 00    	je     800cd4 <strtol+0xc4>
  800c4d:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c51:	75 27                	jne    800c7a <strtol+0x6a>
  800c53:	80 39 30             	cmpb   $0x30,(%ecx)
  800c56:	75 22                	jne    800c7a <strtol+0x6a>
  800c58:	e9 88 00 00 00       	jmp    800ce5 <strtol+0xd5>
		s += 2, base = 16;
  800c5d:	83 c1 02             	add    $0x2,%ecx
  800c60:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c67:	eb 11                	jmp    800c7a <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c69:	41                   	inc    %ecx
  800c6a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c71:	eb 07                	jmp    800c7a <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c73:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c7a:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c7f:	8a 11                	mov    (%ecx),%dl
  800c81:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c84:	80 fb 09             	cmp    $0x9,%bl
  800c87:	77 08                	ja     800c91 <strtol+0x81>
			dig = *s - '0';
  800c89:	0f be d2             	movsbl %dl,%edx
  800c8c:	83 ea 30             	sub    $0x30,%edx
  800c8f:	eb 22                	jmp    800cb3 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c91:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c94:	89 f3                	mov    %esi,%ebx
  800c96:	80 fb 19             	cmp    $0x19,%bl
  800c99:	77 08                	ja     800ca3 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c9b:	0f be d2             	movsbl %dl,%edx
  800c9e:	83 ea 57             	sub    $0x57,%edx
  800ca1:	eb 10                	jmp    800cb3 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800ca3:	8d 72 bf             	lea    -0x41(%edx),%esi
  800ca6:	89 f3                	mov    %esi,%ebx
  800ca8:	80 fb 19             	cmp    $0x19,%bl
  800cab:	77 14                	ja     800cc1 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800cad:	0f be d2             	movsbl %dl,%edx
  800cb0:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800cb3:	3b 55 10             	cmp    0x10(%ebp),%edx
  800cb6:	7d 09                	jge    800cc1 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800cb8:	41                   	inc    %ecx
  800cb9:	0f af 45 10          	imul   0x10(%ebp),%eax
  800cbd:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800cbf:	eb be                	jmp    800c7f <strtol+0x6f>

	if (endptr)
  800cc1:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800cc5:	74 05                	je     800ccc <strtol+0xbc>
		*endptr = (char *) s;
  800cc7:	8b 75 0c             	mov    0xc(%ebp),%esi
  800cca:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ccc:	85 ff                	test   %edi,%edi
  800cce:	74 21                	je     800cf1 <strtol+0xe1>
  800cd0:	f7 d8                	neg    %eax
  800cd2:	eb 1d                	jmp    800cf1 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800cd4:	80 39 30             	cmpb   $0x30,(%ecx)
  800cd7:	75 9a                	jne    800c73 <strtol+0x63>
  800cd9:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cdd:	0f 84 7a ff ff ff    	je     800c5d <strtol+0x4d>
  800ce3:	eb 84                	jmp    800c69 <strtol+0x59>
  800ce5:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800ce9:	0f 84 6e ff ff ff    	je     800c5d <strtol+0x4d>
  800cef:	eb 89                	jmp    800c7a <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cf1:	5b                   	pop    %ebx
  800cf2:	5e                   	pop    %esi
  800cf3:	5f                   	pop    %edi
  800cf4:	5d                   	pop    %ebp
  800cf5:	c3                   	ret    
  800cf6:	66 90                	xchg   %ax,%ax

00800cf8 <__udivdi3>:
  800cf8:	55                   	push   %ebp
  800cf9:	57                   	push   %edi
  800cfa:	56                   	push   %esi
  800cfb:	53                   	push   %ebx
  800cfc:	83 ec 1c             	sub    $0x1c,%esp
  800cff:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d03:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d07:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d0b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d0f:	89 ca                	mov    %ecx,%edx
  800d11:	89 f8                	mov    %edi,%eax
  800d13:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800d17:	85 f6                	test   %esi,%esi
  800d19:	75 2d                	jne    800d48 <__udivdi3+0x50>
  800d1b:	39 cf                	cmp    %ecx,%edi
  800d1d:	77 65                	ja     800d84 <__udivdi3+0x8c>
  800d1f:	89 fd                	mov    %edi,%ebp
  800d21:	85 ff                	test   %edi,%edi
  800d23:	75 0b                	jne    800d30 <__udivdi3+0x38>
  800d25:	b8 01 00 00 00       	mov    $0x1,%eax
  800d2a:	31 d2                	xor    %edx,%edx
  800d2c:	f7 f7                	div    %edi
  800d2e:	89 c5                	mov    %eax,%ebp
  800d30:	31 d2                	xor    %edx,%edx
  800d32:	89 c8                	mov    %ecx,%eax
  800d34:	f7 f5                	div    %ebp
  800d36:	89 c1                	mov    %eax,%ecx
  800d38:	89 d8                	mov    %ebx,%eax
  800d3a:	f7 f5                	div    %ebp
  800d3c:	89 cf                	mov    %ecx,%edi
  800d3e:	89 fa                	mov    %edi,%edx
  800d40:	83 c4 1c             	add    $0x1c,%esp
  800d43:	5b                   	pop    %ebx
  800d44:	5e                   	pop    %esi
  800d45:	5f                   	pop    %edi
  800d46:	5d                   	pop    %ebp
  800d47:	c3                   	ret    
  800d48:	39 ce                	cmp    %ecx,%esi
  800d4a:	77 28                	ja     800d74 <__udivdi3+0x7c>
  800d4c:	0f bd fe             	bsr    %esi,%edi
  800d4f:	83 f7 1f             	xor    $0x1f,%edi
  800d52:	75 40                	jne    800d94 <__udivdi3+0x9c>
  800d54:	39 ce                	cmp    %ecx,%esi
  800d56:	72 0a                	jb     800d62 <__udivdi3+0x6a>
  800d58:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d5c:	0f 87 9e 00 00 00    	ja     800e00 <__udivdi3+0x108>
  800d62:	b8 01 00 00 00       	mov    $0x1,%eax
  800d67:	89 fa                	mov    %edi,%edx
  800d69:	83 c4 1c             	add    $0x1c,%esp
  800d6c:	5b                   	pop    %ebx
  800d6d:	5e                   	pop    %esi
  800d6e:	5f                   	pop    %edi
  800d6f:	5d                   	pop    %ebp
  800d70:	c3                   	ret    
  800d71:	8d 76 00             	lea    0x0(%esi),%esi
  800d74:	31 ff                	xor    %edi,%edi
  800d76:	31 c0                	xor    %eax,%eax
  800d78:	89 fa                	mov    %edi,%edx
  800d7a:	83 c4 1c             	add    $0x1c,%esp
  800d7d:	5b                   	pop    %ebx
  800d7e:	5e                   	pop    %esi
  800d7f:	5f                   	pop    %edi
  800d80:	5d                   	pop    %ebp
  800d81:	c3                   	ret    
  800d82:	66 90                	xchg   %ax,%ax
  800d84:	89 d8                	mov    %ebx,%eax
  800d86:	f7 f7                	div    %edi
  800d88:	31 ff                	xor    %edi,%edi
  800d8a:	89 fa                	mov    %edi,%edx
  800d8c:	83 c4 1c             	add    $0x1c,%esp
  800d8f:	5b                   	pop    %ebx
  800d90:	5e                   	pop    %esi
  800d91:	5f                   	pop    %edi
  800d92:	5d                   	pop    %ebp
  800d93:	c3                   	ret    
  800d94:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d99:	89 eb                	mov    %ebp,%ebx
  800d9b:	29 fb                	sub    %edi,%ebx
  800d9d:	89 f9                	mov    %edi,%ecx
  800d9f:	d3 e6                	shl    %cl,%esi
  800da1:	89 c5                	mov    %eax,%ebp
  800da3:	88 d9                	mov    %bl,%cl
  800da5:	d3 ed                	shr    %cl,%ebp
  800da7:	89 e9                	mov    %ebp,%ecx
  800da9:	09 f1                	or     %esi,%ecx
  800dab:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800daf:	89 f9                	mov    %edi,%ecx
  800db1:	d3 e0                	shl    %cl,%eax
  800db3:	89 c5                	mov    %eax,%ebp
  800db5:	89 d6                	mov    %edx,%esi
  800db7:	88 d9                	mov    %bl,%cl
  800db9:	d3 ee                	shr    %cl,%esi
  800dbb:	89 f9                	mov    %edi,%ecx
  800dbd:	d3 e2                	shl    %cl,%edx
  800dbf:	8b 44 24 08          	mov    0x8(%esp),%eax
  800dc3:	88 d9                	mov    %bl,%cl
  800dc5:	d3 e8                	shr    %cl,%eax
  800dc7:	09 c2                	or     %eax,%edx
  800dc9:	89 d0                	mov    %edx,%eax
  800dcb:	89 f2                	mov    %esi,%edx
  800dcd:	f7 74 24 0c          	divl   0xc(%esp)
  800dd1:	89 d6                	mov    %edx,%esi
  800dd3:	89 c3                	mov    %eax,%ebx
  800dd5:	f7 e5                	mul    %ebp
  800dd7:	39 d6                	cmp    %edx,%esi
  800dd9:	72 19                	jb     800df4 <__udivdi3+0xfc>
  800ddb:	74 0b                	je     800de8 <__udivdi3+0xf0>
  800ddd:	89 d8                	mov    %ebx,%eax
  800ddf:	31 ff                	xor    %edi,%edi
  800de1:	e9 58 ff ff ff       	jmp    800d3e <__udivdi3+0x46>
  800de6:	66 90                	xchg   %ax,%ax
  800de8:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dec:	89 f9                	mov    %edi,%ecx
  800dee:	d3 e2                	shl    %cl,%edx
  800df0:	39 c2                	cmp    %eax,%edx
  800df2:	73 e9                	jae    800ddd <__udivdi3+0xe5>
  800df4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800df7:	31 ff                	xor    %edi,%edi
  800df9:	e9 40 ff ff ff       	jmp    800d3e <__udivdi3+0x46>
  800dfe:	66 90                	xchg   %ax,%ax
  800e00:	31 c0                	xor    %eax,%eax
  800e02:	e9 37 ff ff ff       	jmp    800d3e <__udivdi3+0x46>
  800e07:	90                   	nop

00800e08 <__umoddi3>:
  800e08:	55                   	push   %ebp
  800e09:	57                   	push   %edi
  800e0a:	56                   	push   %esi
  800e0b:	53                   	push   %ebx
  800e0c:	83 ec 1c             	sub    $0x1c,%esp
  800e0f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800e13:	8b 74 24 34          	mov    0x34(%esp),%esi
  800e17:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e1b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e1f:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e23:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e27:	89 f3                	mov    %esi,%ebx
  800e29:	89 fa                	mov    %edi,%edx
  800e2b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e2f:	89 34 24             	mov    %esi,(%esp)
  800e32:	85 c0                	test   %eax,%eax
  800e34:	75 1a                	jne    800e50 <__umoddi3+0x48>
  800e36:	39 f7                	cmp    %esi,%edi
  800e38:	0f 86 a2 00 00 00    	jbe    800ee0 <__umoddi3+0xd8>
  800e3e:	89 c8                	mov    %ecx,%eax
  800e40:	89 f2                	mov    %esi,%edx
  800e42:	f7 f7                	div    %edi
  800e44:	89 d0                	mov    %edx,%eax
  800e46:	31 d2                	xor    %edx,%edx
  800e48:	83 c4 1c             	add    $0x1c,%esp
  800e4b:	5b                   	pop    %ebx
  800e4c:	5e                   	pop    %esi
  800e4d:	5f                   	pop    %edi
  800e4e:	5d                   	pop    %ebp
  800e4f:	c3                   	ret    
  800e50:	39 f0                	cmp    %esi,%eax
  800e52:	0f 87 ac 00 00 00    	ja     800f04 <__umoddi3+0xfc>
  800e58:	0f bd e8             	bsr    %eax,%ebp
  800e5b:	83 f5 1f             	xor    $0x1f,%ebp
  800e5e:	0f 84 ac 00 00 00    	je     800f10 <__umoddi3+0x108>
  800e64:	bf 20 00 00 00       	mov    $0x20,%edi
  800e69:	29 ef                	sub    %ebp,%edi
  800e6b:	89 fe                	mov    %edi,%esi
  800e6d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e71:	89 e9                	mov    %ebp,%ecx
  800e73:	d3 e0                	shl    %cl,%eax
  800e75:	89 d7                	mov    %edx,%edi
  800e77:	89 f1                	mov    %esi,%ecx
  800e79:	d3 ef                	shr    %cl,%edi
  800e7b:	09 c7                	or     %eax,%edi
  800e7d:	89 e9                	mov    %ebp,%ecx
  800e7f:	d3 e2                	shl    %cl,%edx
  800e81:	89 14 24             	mov    %edx,(%esp)
  800e84:	89 d8                	mov    %ebx,%eax
  800e86:	d3 e0                	shl    %cl,%eax
  800e88:	89 c2                	mov    %eax,%edx
  800e8a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e8e:	d3 e0                	shl    %cl,%eax
  800e90:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e94:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e98:	89 f1                	mov    %esi,%ecx
  800e9a:	d3 e8                	shr    %cl,%eax
  800e9c:	09 d0                	or     %edx,%eax
  800e9e:	d3 eb                	shr    %cl,%ebx
  800ea0:	89 da                	mov    %ebx,%edx
  800ea2:	f7 f7                	div    %edi
  800ea4:	89 d3                	mov    %edx,%ebx
  800ea6:	f7 24 24             	mull   (%esp)
  800ea9:	89 c6                	mov    %eax,%esi
  800eab:	89 d1                	mov    %edx,%ecx
  800ead:	39 d3                	cmp    %edx,%ebx
  800eaf:	0f 82 87 00 00 00    	jb     800f3c <__umoddi3+0x134>
  800eb5:	0f 84 91 00 00 00    	je     800f4c <__umoddi3+0x144>
  800ebb:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ebf:	29 f2                	sub    %esi,%edx
  800ec1:	19 cb                	sbb    %ecx,%ebx
  800ec3:	89 d8                	mov    %ebx,%eax
  800ec5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ec9:	d3 e0                	shl    %cl,%eax
  800ecb:	89 e9                	mov    %ebp,%ecx
  800ecd:	d3 ea                	shr    %cl,%edx
  800ecf:	09 d0                	or     %edx,%eax
  800ed1:	89 e9                	mov    %ebp,%ecx
  800ed3:	d3 eb                	shr    %cl,%ebx
  800ed5:	89 da                	mov    %ebx,%edx
  800ed7:	83 c4 1c             	add    $0x1c,%esp
  800eda:	5b                   	pop    %ebx
  800edb:	5e                   	pop    %esi
  800edc:	5f                   	pop    %edi
  800edd:	5d                   	pop    %ebp
  800ede:	c3                   	ret    
  800edf:	90                   	nop
  800ee0:	89 fd                	mov    %edi,%ebp
  800ee2:	85 ff                	test   %edi,%edi
  800ee4:	75 0b                	jne    800ef1 <__umoddi3+0xe9>
  800ee6:	b8 01 00 00 00       	mov    $0x1,%eax
  800eeb:	31 d2                	xor    %edx,%edx
  800eed:	f7 f7                	div    %edi
  800eef:	89 c5                	mov    %eax,%ebp
  800ef1:	89 f0                	mov    %esi,%eax
  800ef3:	31 d2                	xor    %edx,%edx
  800ef5:	f7 f5                	div    %ebp
  800ef7:	89 c8                	mov    %ecx,%eax
  800ef9:	f7 f5                	div    %ebp
  800efb:	89 d0                	mov    %edx,%eax
  800efd:	e9 44 ff ff ff       	jmp    800e46 <__umoddi3+0x3e>
  800f02:	66 90                	xchg   %ax,%ax
  800f04:	89 c8                	mov    %ecx,%eax
  800f06:	89 f2                	mov    %esi,%edx
  800f08:	83 c4 1c             	add    $0x1c,%esp
  800f0b:	5b                   	pop    %ebx
  800f0c:	5e                   	pop    %esi
  800f0d:	5f                   	pop    %edi
  800f0e:	5d                   	pop    %ebp
  800f0f:	c3                   	ret    
  800f10:	3b 04 24             	cmp    (%esp),%eax
  800f13:	72 06                	jb     800f1b <__umoddi3+0x113>
  800f15:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f19:	77 0f                	ja     800f2a <__umoddi3+0x122>
  800f1b:	89 f2                	mov    %esi,%edx
  800f1d:	29 f9                	sub    %edi,%ecx
  800f1f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f23:	89 14 24             	mov    %edx,(%esp)
  800f26:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f2a:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f2e:	8b 14 24             	mov    (%esp),%edx
  800f31:	83 c4 1c             	add    $0x1c,%esp
  800f34:	5b                   	pop    %ebx
  800f35:	5e                   	pop    %esi
  800f36:	5f                   	pop    %edi
  800f37:	5d                   	pop    %ebp
  800f38:	c3                   	ret    
  800f39:	8d 76 00             	lea    0x0(%esi),%esi
  800f3c:	2b 04 24             	sub    (%esp),%eax
  800f3f:	19 fa                	sbb    %edi,%edx
  800f41:	89 d1                	mov    %edx,%ecx
  800f43:	89 c6                	mov    %eax,%esi
  800f45:	e9 71 ff ff ff       	jmp    800ebb <__umoddi3+0xb3>
  800f4a:	66 90                	xchg   %ax,%ax
  800f4c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f50:	72 ea                	jb     800f3c <__umoddi3+0x134>
  800f52:	89 d9                	mov    %ebx,%ecx
  800f54:	e9 62 ff ff ff       	jmp    800ebb <__umoddi3+0xb3>
