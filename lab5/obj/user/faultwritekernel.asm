
obj/user/faultwritekernel.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 11 00 00 00       	call   800042 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	*(unsigned*)0xf0100000 = 0;
  800036:	c7 05 00 00 10 f0 00 	movl   $0x0,0xf0100000
  80003d:	00 00 00 
}
  800040:	5d                   	pop    %ebp
  800041:	c3                   	ret    

00800042 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800042:	55                   	push   %ebp
  800043:	89 e5                	mov    %esp,%ebp
  800045:	56                   	push   %esi
  800046:	53                   	push   %ebx
  800047:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004a:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80004d:	e8 cf 00 00 00       	call   800121 <sys_getenvid>
  800052:	25 ff 03 00 00       	and    $0x3ff,%eax
  800057:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80005e:	c1 e0 07             	shl    $0x7,%eax
  800061:	29 d0                	sub    %edx,%eax
  800063:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800068:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80006d:	85 db                	test   %ebx,%ebx
  80006f:	7e 07                	jle    800078 <libmain+0x36>
		binaryname = argv[0];
  800071:	8b 06                	mov    (%esi),%eax
  800073:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800078:	83 ec 08             	sub    $0x8,%esp
  80007b:	56                   	push   %esi
  80007c:	53                   	push   %ebx
  80007d:	e8 b1 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800082:	e8 0a 00 00 00       	call   800091 <exit>
}
  800087:	83 c4 10             	add    $0x10,%esp
  80008a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80008d:	5b                   	pop    %ebx
  80008e:	5e                   	pop    %esi
  80008f:	5d                   	pop    %ebp
  800090:	c3                   	ret    

00800091 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800091:	55                   	push   %ebp
  800092:	89 e5                	mov    %esp,%ebp
  800094:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800097:	6a 00                	push   $0x0
  800099:	e8 42 00 00 00       	call   8000e0 <sys_env_destroy>
}
  80009e:	83 c4 10             	add    $0x10,%esp
  8000a1:	c9                   	leave  
  8000a2:	c3                   	ret    

008000a3 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000a3:	55                   	push   %ebp
  8000a4:	89 e5                	mov    %esp,%ebp
  8000a6:	57                   	push   %edi
  8000a7:	56                   	push   %esi
  8000a8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a9:	b8 00 00 00 00       	mov    $0x0,%eax
  8000ae:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b1:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b4:	89 c3                	mov    %eax,%ebx
  8000b6:	89 c7                	mov    %eax,%edi
  8000b8:	89 c6                	mov    %eax,%esi
  8000ba:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000bc:	5b                   	pop    %ebx
  8000bd:	5e                   	pop    %esi
  8000be:	5f                   	pop    %edi
  8000bf:	5d                   	pop    %ebp
  8000c0:	c3                   	ret    

008000c1 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c1:	55                   	push   %ebp
  8000c2:	89 e5                	mov    %esp,%ebp
  8000c4:	57                   	push   %edi
  8000c5:	56                   	push   %esi
  8000c6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000c7:	ba 00 00 00 00       	mov    $0x0,%edx
  8000cc:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d1:	89 d1                	mov    %edx,%ecx
  8000d3:	89 d3                	mov    %edx,%ebx
  8000d5:	89 d7                	mov    %edx,%edi
  8000d7:	89 d6                	mov    %edx,%esi
  8000d9:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000db:	5b                   	pop    %ebx
  8000dc:	5e                   	pop    %esi
  8000dd:	5f                   	pop    %edi
  8000de:	5d                   	pop    %ebp
  8000df:	c3                   	ret    

008000e0 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e0:	55                   	push   %ebp
  8000e1:	89 e5                	mov    %esp,%ebp
  8000e3:	57                   	push   %edi
  8000e4:	56                   	push   %esi
  8000e5:	53                   	push   %ebx
  8000e6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e9:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000ee:	b8 03 00 00 00       	mov    $0x3,%eax
  8000f3:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f6:	89 cb                	mov    %ecx,%ebx
  8000f8:	89 cf                	mov    %ecx,%edi
  8000fa:	89 ce                	mov    %ecx,%esi
  8000fc:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000fe:	85 c0                	test   %eax,%eax
  800100:	7e 17                	jle    800119 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800102:	83 ec 0c             	sub    $0xc,%esp
  800105:	50                   	push   %eax
  800106:	6a 03                	push   $0x3
  800108:	68 4a 0f 80 00       	push   $0x800f4a
  80010d:	6a 23                	push   $0x23
  80010f:	68 67 0f 80 00       	push   $0x800f67
  800114:	e8 56 02 00 00       	call   80036f <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800119:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80011c:	5b                   	pop    %ebx
  80011d:	5e                   	pop    %esi
  80011e:	5f                   	pop    %edi
  80011f:	5d                   	pop    %ebp
  800120:	c3                   	ret    

00800121 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800121:	55                   	push   %ebp
  800122:	89 e5                	mov    %esp,%ebp
  800124:	57                   	push   %edi
  800125:	56                   	push   %esi
  800126:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800127:	ba 00 00 00 00       	mov    $0x0,%edx
  80012c:	b8 02 00 00 00       	mov    $0x2,%eax
  800131:	89 d1                	mov    %edx,%ecx
  800133:	89 d3                	mov    %edx,%ebx
  800135:	89 d7                	mov    %edx,%edi
  800137:	89 d6                	mov    %edx,%esi
  800139:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80013b:	5b                   	pop    %ebx
  80013c:	5e                   	pop    %esi
  80013d:	5f                   	pop    %edi
  80013e:	5d                   	pop    %ebp
  80013f:	c3                   	ret    

00800140 <sys_yield>:

void
sys_yield(void)
{
  800140:	55                   	push   %ebp
  800141:	89 e5                	mov    %esp,%ebp
  800143:	57                   	push   %edi
  800144:	56                   	push   %esi
  800145:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800146:	ba 00 00 00 00       	mov    $0x0,%edx
  80014b:	b8 0b 00 00 00       	mov    $0xb,%eax
  800150:	89 d1                	mov    %edx,%ecx
  800152:	89 d3                	mov    %edx,%ebx
  800154:	89 d7                	mov    %edx,%edi
  800156:	89 d6                	mov    %edx,%esi
  800158:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80015a:	5b                   	pop    %ebx
  80015b:	5e                   	pop    %esi
  80015c:	5f                   	pop    %edi
  80015d:	5d                   	pop    %ebp
  80015e:	c3                   	ret    

0080015f <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80015f:	55                   	push   %ebp
  800160:	89 e5                	mov    %esp,%ebp
  800162:	57                   	push   %edi
  800163:	56                   	push   %esi
  800164:	53                   	push   %ebx
  800165:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800168:	be 00 00 00 00       	mov    $0x0,%esi
  80016d:	b8 04 00 00 00       	mov    $0x4,%eax
  800172:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800175:	8b 55 08             	mov    0x8(%ebp),%edx
  800178:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80017b:	89 f7                	mov    %esi,%edi
  80017d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80017f:	85 c0                	test   %eax,%eax
  800181:	7e 17                	jle    80019a <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800183:	83 ec 0c             	sub    $0xc,%esp
  800186:	50                   	push   %eax
  800187:	6a 04                	push   $0x4
  800189:	68 4a 0f 80 00       	push   $0x800f4a
  80018e:	6a 23                	push   $0x23
  800190:	68 67 0f 80 00       	push   $0x800f67
  800195:	e8 d5 01 00 00       	call   80036f <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  80019a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80019d:	5b                   	pop    %ebx
  80019e:	5e                   	pop    %esi
  80019f:	5f                   	pop    %edi
  8001a0:	5d                   	pop    %ebp
  8001a1:	c3                   	ret    

008001a2 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001a2:	55                   	push   %ebp
  8001a3:	89 e5                	mov    %esp,%ebp
  8001a5:	57                   	push   %edi
  8001a6:	56                   	push   %esi
  8001a7:	53                   	push   %ebx
  8001a8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ab:	b8 05 00 00 00       	mov    $0x5,%eax
  8001b0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001b3:	8b 55 08             	mov    0x8(%ebp),%edx
  8001b6:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b9:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001bc:	8b 75 18             	mov    0x18(%ebp),%esi
  8001bf:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c1:	85 c0                	test   %eax,%eax
  8001c3:	7e 17                	jle    8001dc <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001c5:	83 ec 0c             	sub    $0xc,%esp
  8001c8:	50                   	push   %eax
  8001c9:	6a 05                	push   $0x5
  8001cb:	68 4a 0f 80 00       	push   $0x800f4a
  8001d0:	6a 23                	push   $0x23
  8001d2:	68 67 0f 80 00       	push   $0x800f67
  8001d7:	e8 93 01 00 00       	call   80036f <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001df:	5b                   	pop    %ebx
  8001e0:	5e                   	pop    %esi
  8001e1:	5f                   	pop    %edi
  8001e2:	5d                   	pop    %ebp
  8001e3:	c3                   	ret    

008001e4 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001e4:	55                   	push   %ebp
  8001e5:	89 e5                	mov    %esp,%ebp
  8001e7:	57                   	push   %edi
  8001e8:	56                   	push   %esi
  8001e9:	53                   	push   %ebx
  8001ea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ed:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001f2:	b8 06 00 00 00       	mov    $0x6,%eax
  8001f7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001fa:	8b 55 08             	mov    0x8(%ebp),%edx
  8001fd:	89 df                	mov    %ebx,%edi
  8001ff:	89 de                	mov    %ebx,%esi
  800201:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800203:	85 c0                	test   %eax,%eax
  800205:	7e 17                	jle    80021e <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800207:	83 ec 0c             	sub    $0xc,%esp
  80020a:	50                   	push   %eax
  80020b:	6a 06                	push   $0x6
  80020d:	68 4a 0f 80 00       	push   $0x800f4a
  800212:	6a 23                	push   $0x23
  800214:	68 67 0f 80 00       	push   $0x800f67
  800219:	e8 51 01 00 00       	call   80036f <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80021e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800221:	5b                   	pop    %ebx
  800222:	5e                   	pop    %esi
  800223:	5f                   	pop    %edi
  800224:	5d                   	pop    %ebp
  800225:	c3                   	ret    

00800226 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800226:	55                   	push   %ebp
  800227:	89 e5                	mov    %esp,%ebp
  800229:	57                   	push   %edi
  80022a:	56                   	push   %esi
  80022b:	53                   	push   %ebx
  80022c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80022f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800234:	b8 08 00 00 00       	mov    $0x8,%eax
  800239:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80023c:	8b 55 08             	mov    0x8(%ebp),%edx
  80023f:	89 df                	mov    %ebx,%edi
  800241:	89 de                	mov    %ebx,%esi
  800243:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800245:	85 c0                	test   %eax,%eax
  800247:	7e 17                	jle    800260 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800249:	83 ec 0c             	sub    $0xc,%esp
  80024c:	50                   	push   %eax
  80024d:	6a 08                	push   $0x8
  80024f:	68 4a 0f 80 00       	push   $0x800f4a
  800254:	6a 23                	push   $0x23
  800256:	68 67 0f 80 00       	push   $0x800f67
  80025b:	e8 0f 01 00 00       	call   80036f <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800260:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800263:	5b                   	pop    %ebx
  800264:	5e                   	pop    %esi
  800265:	5f                   	pop    %edi
  800266:	5d                   	pop    %ebp
  800267:	c3                   	ret    

00800268 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800268:	55                   	push   %ebp
  800269:	89 e5                	mov    %esp,%ebp
  80026b:	57                   	push   %edi
  80026c:	56                   	push   %esi
  80026d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80026e:	ba 00 00 00 00       	mov    $0x0,%edx
  800273:	b8 0c 00 00 00       	mov    $0xc,%eax
  800278:	89 d1                	mov    %edx,%ecx
  80027a:	89 d3                	mov    %edx,%ebx
  80027c:	89 d7                	mov    %edx,%edi
  80027e:	89 d6                	mov    %edx,%esi
  800280:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800282:	5b                   	pop    %ebx
  800283:	5e                   	pop    %esi
  800284:	5f                   	pop    %edi
  800285:	5d                   	pop    %ebp
  800286:	c3                   	ret    

00800287 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800287:	55                   	push   %ebp
  800288:	89 e5                	mov    %esp,%ebp
  80028a:	57                   	push   %edi
  80028b:	56                   	push   %esi
  80028c:	53                   	push   %ebx
  80028d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800290:	bb 00 00 00 00       	mov    $0x0,%ebx
  800295:	b8 09 00 00 00       	mov    $0x9,%eax
  80029a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80029d:	8b 55 08             	mov    0x8(%ebp),%edx
  8002a0:	89 df                	mov    %ebx,%edi
  8002a2:	89 de                	mov    %ebx,%esi
  8002a4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002a6:	85 c0                	test   %eax,%eax
  8002a8:	7e 17                	jle    8002c1 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002aa:	83 ec 0c             	sub    $0xc,%esp
  8002ad:	50                   	push   %eax
  8002ae:	6a 09                	push   $0x9
  8002b0:	68 4a 0f 80 00       	push   $0x800f4a
  8002b5:	6a 23                	push   $0x23
  8002b7:	68 67 0f 80 00       	push   $0x800f67
  8002bc:	e8 ae 00 00 00       	call   80036f <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002c1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c4:	5b                   	pop    %ebx
  8002c5:	5e                   	pop    %esi
  8002c6:	5f                   	pop    %edi
  8002c7:	5d                   	pop    %ebp
  8002c8:	c3                   	ret    

008002c9 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002c9:	55                   	push   %ebp
  8002ca:	89 e5                	mov    %esp,%ebp
  8002cc:	57                   	push   %edi
  8002cd:	56                   	push   %esi
  8002ce:	53                   	push   %ebx
  8002cf:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002d2:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002d7:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002dc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002df:	8b 55 08             	mov    0x8(%ebp),%edx
  8002e2:	89 df                	mov    %ebx,%edi
  8002e4:	89 de                	mov    %ebx,%esi
  8002e6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002e8:	85 c0                	test   %eax,%eax
  8002ea:	7e 17                	jle    800303 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002ec:	83 ec 0c             	sub    $0xc,%esp
  8002ef:	50                   	push   %eax
  8002f0:	6a 0a                	push   $0xa
  8002f2:	68 4a 0f 80 00       	push   $0x800f4a
  8002f7:	6a 23                	push   $0x23
  8002f9:	68 67 0f 80 00       	push   $0x800f67
  8002fe:	e8 6c 00 00 00       	call   80036f <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800303:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800306:	5b                   	pop    %ebx
  800307:	5e                   	pop    %esi
  800308:	5f                   	pop    %edi
  800309:	5d                   	pop    %ebp
  80030a:	c3                   	ret    

0080030b <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  80030b:	55                   	push   %ebp
  80030c:	89 e5                	mov    %esp,%ebp
  80030e:	57                   	push   %edi
  80030f:	56                   	push   %esi
  800310:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800311:	be 00 00 00 00       	mov    $0x0,%esi
  800316:	b8 0d 00 00 00       	mov    $0xd,%eax
  80031b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80031e:	8b 55 08             	mov    0x8(%ebp),%edx
  800321:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800324:	8b 7d 14             	mov    0x14(%ebp),%edi
  800327:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800329:	5b                   	pop    %ebx
  80032a:	5e                   	pop    %esi
  80032b:	5f                   	pop    %edi
  80032c:	5d                   	pop    %ebp
  80032d:	c3                   	ret    

0080032e <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  80032e:	55                   	push   %ebp
  80032f:	89 e5                	mov    %esp,%ebp
  800331:	57                   	push   %edi
  800332:	56                   	push   %esi
  800333:	53                   	push   %ebx
  800334:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800337:	b9 00 00 00 00       	mov    $0x0,%ecx
  80033c:	b8 0e 00 00 00       	mov    $0xe,%eax
  800341:	8b 55 08             	mov    0x8(%ebp),%edx
  800344:	89 cb                	mov    %ecx,%ebx
  800346:	89 cf                	mov    %ecx,%edi
  800348:	89 ce                	mov    %ecx,%esi
  80034a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80034c:	85 c0                	test   %eax,%eax
  80034e:	7e 17                	jle    800367 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800350:	83 ec 0c             	sub    $0xc,%esp
  800353:	50                   	push   %eax
  800354:	6a 0e                	push   $0xe
  800356:	68 4a 0f 80 00       	push   $0x800f4a
  80035b:	6a 23                	push   $0x23
  80035d:	68 67 0f 80 00       	push   $0x800f67
  800362:	e8 08 00 00 00       	call   80036f <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800367:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80036a:	5b                   	pop    %ebx
  80036b:	5e                   	pop    %esi
  80036c:	5f                   	pop    %edi
  80036d:	5d                   	pop    %ebp
  80036e:	c3                   	ret    

0080036f <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80036f:	55                   	push   %ebp
  800370:	89 e5                	mov    %esp,%ebp
  800372:	56                   	push   %esi
  800373:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800374:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800377:	8b 35 00 20 80 00    	mov    0x802000,%esi
  80037d:	e8 9f fd ff ff       	call   800121 <sys_getenvid>
  800382:	83 ec 0c             	sub    $0xc,%esp
  800385:	ff 75 0c             	pushl  0xc(%ebp)
  800388:	ff 75 08             	pushl  0x8(%ebp)
  80038b:	56                   	push   %esi
  80038c:	50                   	push   %eax
  80038d:	68 78 0f 80 00       	push   $0x800f78
  800392:	e8 b0 00 00 00       	call   800447 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800397:	83 c4 18             	add    $0x18,%esp
  80039a:	53                   	push   %ebx
  80039b:	ff 75 10             	pushl  0x10(%ebp)
  80039e:	e8 53 00 00 00       	call   8003f6 <vcprintf>
	cprintf("\n");
  8003a3:	c7 04 24 9b 0f 80 00 	movl   $0x800f9b,(%esp)
  8003aa:	e8 98 00 00 00       	call   800447 <cprintf>
  8003af:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003b2:	cc                   	int3   
  8003b3:	eb fd                	jmp    8003b2 <_panic+0x43>

008003b5 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003b5:	55                   	push   %ebp
  8003b6:	89 e5                	mov    %esp,%ebp
  8003b8:	53                   	push   %ebx
  8003b9:	83 ec 04             	sub    $0x4,%esp
  8003bc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003bf:	8b 13                	mov    (%ebx),%edx
  8003c1:	8d 42 01             	lea    0x1(%edx),%eax
  8003c4:	89 03                	mov    %eax,(%ebx)
  8003c6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003c9:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003cd:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003d2:	75 1a                	jne    8003ee <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003d4:	83 ec 08             	sub    $0x8,%esp
  8003d7:	68 ff 00 00 00       	push   $0xff
  8003dc:	8d 43 08             	lea    0x8(%ebx),%eax
  8003df:	50                   	push   %eax
  8003e0:	e8 be fc ff ff       	call   8000a3 <sys_cputs>
		b->idx = 0;
  8003e5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003eb:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003ee:	ff 43 04             	incl   0x4(%ebx)
}
  8003f1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003f4:	c9                   	leave  
  8003f5:	c3                   	ret    

008003f6 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003f6:	55                   	push   %ebp
  8003f7:	89 e5                	mov    %esp,%ebp
  8003f9:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003ff:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800406:	00 00 00 
	b.cnt = 0;
  800409:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800410:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800413:	ff 75 0c             	pushl  0xc(%ebp)
  800416:	ff 75 08             	pushl  0x8(%ebp)
  800419:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80041f:	50                   	push   %eax
  800420:	68 b5 03 80 00       	push   $0x8003b5
  800425:	e8 51 01 00 00       	call   80057b <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80042a:	83 c4 08             	add    $0x8,%esp
  80042d:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800433:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800439:	50                   	push   %eax
  80043a:	e8 64 fc ff ff       	call   8000a3 <sys_cputs>

	return b.cnt;
}
  80043f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800445:	c9                   	leave  
  800446:	c3                   	ret    

00800447 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800447:	55                   	push   %ebp
  800448:	89 e5                	mov    %esp,%ebp
  80044a:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80044d:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800450:	50                   	push   %eax
  800451:	ff 75 08             	pushl  0x8(%ebp)
  800454:	e8 9d ff ff ff       	call   8003f6 <vcprintf>
	va_end(ap);

	return cnt;
}
  800459:	c9                   	leave  
  80045a:	c3                   	ret    

0080045b <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80045b:	55                   	push   %ebp
  80045c:	89 e5                	mov    %esp,%ebp
  80045e:	57                   	push   %edi
  80045f:	56                   	push   %esi
  800460:	53                   	push   %ebx
  800461:	83 ec 1c             	sub    $0x1c,%esp
  800464:	89 c7                	mov    %eax,%edi
  800466:	89 d6                	mov    %edx,%esi
  800468:	8b 45 08             	mov    0x8(%ebp),%eax
  80046b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80046e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800471:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800474:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800477:	bb 00 00 00 00       	mov    $0x0,%ebx
  80047c:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80047f:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800482:	39 d3                	cmp    %edx,%ebx
  800484:	72 05                	jb     80048b <printnum+0x30>
  800486:	39 45 10             	cmp    %eax,0x10(%ebp)
  800489:	77 45                	ja     8004d0 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80048b:	83 ec 0c             	sub    $0xc,%esp
  80048e:	ff 75 18             	pushl  0x18(%ebp)
  800491:	8b 45 14             	mov    0x14(%ebp),%eax
  800494:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800497:	53                   	push   %ebx
  800498:	ff 75 10             	pushl  0x10(%ebp)
  80049b:	83 ec 08             	sub    $0x8,%esp
  80049e:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a1:	ff 75 e0             	pushl  -0x20(%ebp)
  8004a4:	ff 75 dc             	pushl  -0x24(%ebp)
  8004a7:	ff 75 d8             	pushl  -0x28(%ebp)
  8004aa:	e8 25 08 00 00       	call   800cd4 <__udivdi3>
  8004af:	83 c4 18             	add    $0x18,%esp
  8004b2:	52                   	push   %edx
  8004b3:	50                   	push   %eax
  8004b4:	89 f2                	mov    %esi,%edx
  8004b6:	89 f8                	mov    %edi,%eax
  8004b8:	e8 9e ff ff ff       	call   80045b <printnum>
  8004bd:	83 c4 20             	add    $0x20,%esp
  8004c0:	eb 16                	jmp    8004d8 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004c2:	83 ec 08             	sub    $0x8,%esp
  8004c5:	56                   	push   %esi
  8004c6:	ff 75 18             	pushl  0x18(%ebp)
  8004c9:	ff d7                	call   *%edi
  8004cb:	83 c4 10             	add    $0x10,%esp
  8004ce:	eb 03                	jmp    8004d3 <printnum+0x78>
  8004d0:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004d3:	4b                   	dec    %ebx
  8004d4:	85 db                	test   %ebx,%ebx
  8004d6:	7f ea                	jg     8004c2 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004d8:	83 ec 08             	sub    $0x8,%esp
  8004db:	56                   	push   %esi
  8004dc:	83 ec 04             	sub    $0x4,%esp
  8004df:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004e2:	ff 75 e0             	pushl  -0x20(%ebp)
  8004e5:	ff 75 dc             	pushl  -0x24(%ebp)
  8004e8:	ff 75 d8             	pushl  -0x28(%ebp)
  8004eb:	e8 f4 08 00 00       	call   800de4 <__umoddi3>
  8004f0:	83 c4 14             	add    $0x14,%esp
  8004f3:	0f be 80 9d 0f 80 00 	movsbl 0x800f9d(%eax),%eax
  8004fa:	50                   	push   %eax
  8004fb:	ff d7                	call   *%edi
}
  8004fd:	83 c4 10             	add    $0x10,%esp
  800500:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800503:	5b                   	pop    %ebx
  800504:	5e                   	pop    %esi
  800505:	5f                   	pop    %edi
  800506:	5d                   	pop    %ebp
  800507:	c3                   	ret    

00800508 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800508:	55                   	push   %ebp
  800509:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80050b:	83 fa 01             	cmp    $0x1,%edx
  80050e:	7e 0e                	jle    80051e <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800510:	8b 10                	mov    (%eax),%edx
  800512:	8d 4a 08             	lea    0x8(%edx),%ecx
  800515:	89 08                	mov    %ecx,(%eax)
  800517:	8b 02                	mov    (%edx),%eax
  800519:	8b 52 04             	mov    0x4(%edx),%edx
  80051c:	eb 22                	jmp    800540 <getuint+0x38>
	else if (lflag)
  80051e:	85 d2                	test   %edx,%edx
  800520:	74 10                	je     800532 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800522:	8b 10                	mov    (%eax),%edx
  800524:	8d 4a 04             	lea    0x4(%edx),%ecx
  800527:	89 08                	mov    %ecx,(%eax)
  800529:	8b 02                	mov    (%edx),%eax
  80052b:	ba 00 00 00 00       	mov    $0x0,%edx
  800530:	eb 0e                	jmp    800540 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800532:	8b 10                	mov    (%eax),%edx
  800534:	8d 4a 04             	lea    0x4(%edx),%ecx
  800537:	89 08                	mov    %ecx,(%eax)
  800539:	8b 02                	mov    (%edx),%eax
  80053b:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800540:	5d                   	pop    %ebp
  800541:	c3                   	ret    

00800542 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800542:	55                   	push   %ebp
  800543:	89 e5                	mov    %esp,%ebp
  800545:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800548:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80054b:	8b 10                	mov    (%eax),%edx
  80054d:	3b 50 04             	cmp    0x4(%eax),%edx
  800550:	73 0a                	jae    80055c <sprintputch+0x1a>
		*b->buf++ = ch;
  800552:	8d 4a 01             	lea    0x1(%edx),%ecx
  800555:	89 08                	mov    %ecx,(%eax)
  800557:	8b 45 08             	mov    0x8(%ebp),%eax
  80055a:	88 02                	mov    %al,(%edx)
}
  80055c:	5d                   	pop    %ebp
  80055d:	c3                   	ret    

0080055e <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80055e:	55                   	push   %ebp
  80055f:	89 e5                	mov    %esp,%ebp
  800561:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800564:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800567:	50                   	push   %eax
  800568:	ff 75 10             	pushl  0x10(%ebp)
  80056b:	ff 75 0c             	pushl  0xc(%ebp)
  80056e:	ff 75 08             	pushl  0x8(%ebp)
  800571:	e8 05 00 00 00       	call   80057b <vprintfmt>
	va_end(ap);
}
  800576:	83 c4 10             	add    $0x10,%esp
  800579:	c9                   	leave  
  80057a:	c3                   	ret    

0080057b <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80057b:	55                   	push   %ebp
  80057c:	89 e5                	mov    %esp,%ebp
  80057e:	57                   	push   %edi
  80057f:	56                   	push   %esi
  800580:	53                   	push   %ebx
  800581:	83 ec 2c             	sub    $0x2c,%esp
  800584:	8b 75 08             	mov    0x8(%ebp),%esi
  800587:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80058a:	8b 7d 10             	mov    0x10(%ebp),%edi
  80058d:	eb 12                	jmp    8005a1 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80058f:	85 c0                	test   %eax,%eax
  800591:	0f 84 68 03 00 00    	je     8008ff <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800597:	83 ec 08             	sub    $0x8,%esp
  80059a:	53                   	push   %ebx
  80059b:	50                   	push   %eax
  80059c:	ff d6                	call   *%esi
  80059e:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8005a1:	47                   	inc    %edi
  8005a2:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005a6:	83 f8 25             	cmp    $0x25,%eax
  8005a9:	75 e4                	jne    80058f <vprintfmt+0x14>
  8005ab:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005af:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005b6:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005bd:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005c4:	ba 00 00 00 00       	mov    $0x0,%edx
  8005c9:	eb 07                	jmp    8005d2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005cb:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005ce:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d2:	8d 47 01             	lea    0x1(%edi),%eax
  8005d5:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005d8:	0f b6 0f             	movzbl (%edi),%ecx
  8005db:	8a 07                	mov    (%edi),%al
  8005dd:	83 e8 23             	sub    $0x23,%eax
  8005e0:	3c 55                	cmp    $0x55,%al
  8005e2:	0f 87 fe 02 00 00    	ja     8008e6 <vprintfmt+0x36b>
  8005e8:	0f b6 c0             	movzbl %al,%eax
  8005eb:	ff 24 85 e0 10 80 00 	jmp    *0x8010e0(,%eax,4)
  8005f2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005f5:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005f9:	eb d7                	jmp    8005d2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005fb:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fe:	b8 00 00 00 00       	mov    $0x0,%eax
  800603:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800606:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800609:	01 c0                	add    %eax,%eax
  80060b:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80060f:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800612:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800615:	83 fa 09             	cmp    $0x9,%edx
  800618:	77 34                	ja     80064e <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80061a:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80061b:	eb e9                	jmp    800606 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80061d:	8b 45 14             	mov    0x14(%ebp),%eax
  800620:	8d 48 04             	lea    0x4(%eax),%ecx
  800623:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800626:	8b 00                	mov    (%eax),%eax
  800628:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80062b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80062e:	eb 24                	jmp    800654 <vprintfmt+0xd9>
  800630:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800634:	79 07                	jns    80063d <vprintfmt+0xc2>
  800636:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80063d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800640:	eb 90                	jmp    8005d2 <vprintfmt+0x57>
  800642:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800645:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80064c:	eb 84                	jmp    8005d2 <vprintfmt+0x57>
  80064e:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800651:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800654:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800658:	0f 89 74 ff ff ff    	jns    8005d2 <vprintfmt+0x57>
				width = precision, precision = -1;
  80065e:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800661:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800664:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80066b:	e9 62 ff ff ff       	jmp    8005d2 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800670:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800671:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800674:	e9 59 ff ff ff       	jmp    8005d2 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800679:	8b 45 14             	mov    0x14(%ebp),%eax
  80067c:	8d 50 04             	lea    0x4(%eax),%edx
  80067f:	89 55 14             	mov    %edx,0x14(%ebp)
  800682:	83 ec 08             	sub    $0x8,%esp
  800685:	53                   	push   %ebx
  800686:	ff 30                	pushl  (%eax)
  800688:	ff d6                	call   *%esi
			break;
  80068a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80068d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800690:	e9 0c ff ff ff       	jmp    8005a1 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800695:	8b 45 14             	mov    0x14(%ebp),%eax
  800698:	8d 50 04             	lea    0x4(%eax),%edx
  80069b:	89 55 14             	mov    %edx,0x14(%ebp)
  80069e:	8b 00                	mov    (%eax),%eax
  8006a0:	85 c0                	test   %eax,%eax
  8006a2:	79 02                	jns    8006a6 <vprintfmt+0x12b>
  8006a4:	f7 d8                	neg    %eax
  8006a6:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006a8:	83 f8 0f             	cmp    $0xf,%eax
  8006ab:	7f 0b                	jg     8006b8 <vprintfmt+0x13d>
  8006ad:	8b 04 85 40 12 80 00 	mov    0x801240(,%eax,4),%eax
  8006b4:	85 c0                	test   %eax,%eax
  8006b6:	75 18                	jne    8006d0 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006b8:	52                   	push   %edx
  8006b9:	68 b5 0f 80 00       	push   $0x800fb5
  8006be:	53                   	push   %ebx
  8006bf:	56                   	push   %esi
  8006c0:	e8 99 fe ff ff       	call   80055e <printfmt>
  8006c5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c8:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006cb:	e9 d1 fe ff ff       	jmp    8005a1 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006d0:	50                   	push   %eax
  8006d1:	68 be 0f 80 00       	push   $0x800fbe
  8006d6:	53                   	push   %ebx
  8006d7:	56                   	push   %esi
  8006d8:	e8 81 fe ff ff       	call   80055e <printfmt>
  8006dd:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006e0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006e3:	e9 b9 fe ff ff       	jmp    8005a1 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006e8:	8b 45 14             	mov    0x14(%ebp),%eax
  8006eb:	8d 50 04             	lea    0x4(%eax),%edx
  8006ee:	89 55 14             	mov    %edx,0x14(%ebp)
  8006f1:	8b 38                	mov    (%eax),%edi
  8006f3:	85 ff                	test   %edi,%edi
  8006f5:	75 05                	jne    8006fc <vprintfmt+0x181>
				p = "(null)";
  8006f7:	bf ae 0f 80 00       	mov    $0x800fae,%edi
			if (width > 0 && padc != '-')
  8006fc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800700:	0f 8e 90 00 00 00    	jle    800796 <vprintfmt+0x21b>
  800706:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80070a:	0f 84 8e 00 00 00    	je     80079e <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800710:	83 ec 08             	sub    $0x8,%esp
  800713:	ff 75 d0             	pushl  -0x30(%ebp)
  800716:	57                   	push   %edi
  800717:	e8 70 02 00 00       	call   80098c <strnlen>
  80071c:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80071f:	29 c1                	sub    %eax,%ecx
  800721:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800724:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800727:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80072b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80072e:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800731:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800733:	eb 0d                	jmp    800742 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800735:	83 ec 08             	sub    $0x8,%esp
  800738:	53                   	push   %ebx
  800739:	ff 75 e4             	pushl  -0x1c(%ebp)
  80073c:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80073e:	4f                   	dec    %edi
  80073f:	83 c4 10             	add    $0x10,%esp
  800742:	85 ff                	test   %edi,%edi
  800744:	7f ef                	jg     800735 <vprintfmt+0x1ba>
  800746:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800749:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80074c:	89 c8                	mov    %ecx,%eax
  80074e:	85 c9                	test   %ecx,%ecx
  800750:	79 05                	jns    800757 <vprintfmt+0x1dc>
  800752:	b8 00 00 00 00       	mov    $0x0,%eax
  800757:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80075a:	29 c1                	sub    %eax,%ecx
  80075c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80075f:	89 75 08             	mov    %esi,0x8(%ebp)
  800762:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800765:	eb 3d                	jmp    8007a4 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800767:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80076b:	74 19                	je     800786 <vprintfmt+0x20b>
  80076d:	0f be c0             	movsbl %al,%eax
  800770:	83 e8 20             	sub    $0x20,%eax
  800773:	83 f8 5e             	cmp    $0x5e,%eax
  800776:	76 0e                	jbe    800786 <vprintfmt+0x20b>
					putch('?', putdat);
  800778:	83 ec 08             	sub    $0x8,%esp
  80077b:	53                   	push   %ebx
  80077c:	6a 3f                	push   $0x3f
  80077e:	ff 55 08             	call   *0x8(%ebp)
  800781:	83 c4 10             	add    $0x10,%esp
  800784:	eb 0b                	jmp    800791 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800786:	83 ec 08             	sub    $0x8,%esp
  800789:	53                   	push   %ebx
  80078a:	52                   	push   %edx
  80078b:	ff 55 08             	call   *0x8(%ebp)
  80078e:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800791:	ff 4d e4             	decl   -0x1c(%ebp)
  800794:	eb 0e                	jmp    8007a4 <vprintfmt+0x229>
  800796:	89 75 08             	mov    %esi,0x8(%ebp)
  800799:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80079c:	eb 06                	jmp    8007a4 <vprintfmt+0x229>
  80079e:	89 75 08             	mov    %esi,0x8(%ebp)
  8007a1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007a4:	47                   	inc    %edi
  8007a5:	8a 47 ff             	mov    -0x1(%edi),%al
  8007a8:	0f be d0             	movsbl %al,%edx
  8007ab:	85 d2                	test   %edx,%edx
  8007ad:	74 1d                	je     8007cc <vprintfmt+0x251>
  8007af:	85 f6                	test   %esi,%esi
  8007b1:	78 b4                	js     800767 <vprintfmt+0x1ec>
  8007b3:	4e                   	dec    %esi
  8007b4:	79 b1                	jns    800767 <vprintfmt+0x1ec>
  8007b6:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b9:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007bc:	eb 14                	jmp    8007d2 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007be:	83 ec 08             	sub    $0x8,%esp
  8007c1:	53                   	push   %ebx
  8007c2:	6a 20                	push   $0x20
  8007c4:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007c6:	4f                   	dec    %edi
  8007c7:	83 c4 10             	add    $0x10,%esp
  8007ca:	eb 06                	jmp    8007d2 <vprintfmt+0x257>
  8007cc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007cf:	8b 75 08             	mov    0x8(%ebp),%esi
  8007d2:	85 ff                	test   %edi,%edi
  8007d4:	7f e8                	jg     8007be <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007d6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007d9:	e9 c3 fd ff ff       	jmp    8005a1 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007de:	83 fa 01             	cmp    $0x1,%edx
  8007e1:	7e 16                	jle    8007f9 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007e3:	8b 45 14             	mov    0x14(%ebp),%eax
  8007e6:	8d 50 08             	lea    0x8(%eax),%edx
  8007e9:	89 55 14             	mov    %edx,0x14(%ebp)
  8007ec:	8b 50 04             	mov    0x4(%eax),%edx
  8007ef:	8b 00                	mov    (%eax),%eax
  8007f1:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007f4:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007f7:	eb 32                	jmp    80082b <vprintfmt+0x2b0>
	else if (lflag)
  8007f9:	85 d2                	test   %edx,%edx
  8007fb:	74 18                	je     800815 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007fd:	8b 45 14             	mov    0x14(%ebp),%eax
  800800:	8d 50 04             	lea    0x4(%eax),%edx
  800803:	89 55 14             	mov    %edx,0x14(%ebp)
  800806:	8b 00                	mov    (%eax),%eax
  800808:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80080b:	89 c1                	mov    %eax,%ecx
  80080d:	c1 f9 1f             	sar    $0x1f,%ecx
  800810:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800813:	eb 16                	jmp    80082b <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800815:	8b 45 14             	mov    0x14(%ebp),%eax
  800818:	8d 50 04             	lea    0x4(%eax),%edx
  80081b:	89 55 14             	mov    %edx,0x14(%ebp)
  80081e:	8b 00                	mov    (%eax),%eax
  800820:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800823:	89 c1                	mov    %eax,%ecx
  800825:	c1 f9 1f             	sar    $0x1f,%ecx
  800828:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80082b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80082e:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800831:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800835:	79 76                	jns    8008ad <vprintfmt+0x332>
				putch('-', putdat);
  800837:	83 ec 08             	sub    $0x8,%esp
  80083a:	53                   	push   %ebx
  80083b:	6a 2d                	push   $0x2d
  80083d:	ff d6                	call   *%esi
				num = -(long long) num;
  80083f:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800842:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800845:	f7 d8                	neg    %eax
  800847:	83 d2 00             	adc    $0x0,%edx
  80084a:	f7 da                	neg    %edx
  80084c:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80084f:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800854:	eb 5c                	jmp    8008b2 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800856:	8d 45 14             	lea    0x14(%ebp),%eax
  800859:	e8 aa fc ff ff       	call   800508 <getuint>
			base = 10;
  80085e:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800863:	eb 4d                	jmp    8008b2 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800865:	8d 45 14             	lea    0x14(%ebp),%eax
  800868:	e8 9b fc ff ff       	call   800508 <getuint>
			base = 8;
  80086d:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800872:	eb 3e                	jmp    8008b2 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800874:	83 ec 08             	sub    $0x8,%esp
  800877:	53                   	push   %ebx
  800878:	6a 30                	push   $0x30
  80087a:	ff d6                	call   *%esi
			putch('x', putdat);
  80087c:	83 c4 08             	add    $0x8,%esp
  80087f:	53                   	push   %ebx
  800880:	6a 78                	push   $0x78
  800882:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800884:	8b 45 14             	mov    0x14(%ebp),%eax
  800887:	8d 50 04             	lea    0x4(%eax),%edx
  80088a:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80088d:	8b 00                	mov    (%eax),%eax
  80088f:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800894:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800897:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80089c:	eb 14                	jmp    8008b2 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80089e:	8d 45 14             	lea    0x14(%ebp),%eax
  8008a1:	e8 62 fc ff ff       	call   800508 <getuint>
			base = 16;
  8008a6:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008ab:	eb 05                	jmp    8008b2 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008ad:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008b2:	83 ec 0c             	sub    $0xc,%esp
  8008b5:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008b9:	57                   	push   %edi
  8008ba:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008bd:	51                   	push   %ecx
  8008be:	52                   	push   %edx
  8008bf:	50                   	push   %eax
  8008c0:	89 da                	mov    %ebx,%edx
  8008c2:	89 f0                	mov    %esi,%eax
  8008c4:	e8 92 fb ff ff       	call   80045b <printnum>
			break;
  8008c9:	83 c4 20             	add    $0x20,%esp
  8008cc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008cf:	e9 cd fc ff ff       	jmp    8005a1 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008d4:	83 ec 08             	sub    $0x8,%esp
  8008d7:	53                   	push   %ebx
  8008d8:	51                   	push   %ecx
  8008d9:	ff d6                	call   *%esi
			break;
  8008db:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008de:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008e1:	e9 bb fc ff ff       	jmp    8005a1 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008e6:	83 ec 08             	sub    $0x8,%esp
  8008e9:	53                   	push   %ebx
  8008ea:	6a 25                	push   $0x25
  8008ec:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008ee:	83 c4 10             	add    $0x10,%esp
  8008f1:	eb 01                	jmp    8008f4 <vprintfmt+0x379>
  8008f3:	4f                   	dec    %edi
  8008f4:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008f8:	75 f9                	jne    8008f3 <vprintfmt+0x378>
  8008fa:	e9 a2 fc ff ff       	jmp    8005a1 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800902:	5b                   	pop    %ebx
  800903:	5e                   	pop    %esi
  800904:	5f                   	pop    %edi
  800905:	5d                   	pop    %ebp
  800906:	c3                   	ret    

00800907 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800907:	55                   	push   %ebp
  800908:	89 e5                	mov    %esp,%ebp
  80090a:	83 ec 18             	sub    $0x18,%esp
  80090d:	8b 45 08             	mov    0x8(%ebp),%eax
  800910:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800913:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800916:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80091a:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80091d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800924:	85 c0                	test   %eax,%eax
  800926:	74 26                	je     80094e <vsnprintf+0x47>
  800928:	85 d2                	test   %edx,%edx
  80092a:	7e 29                	jle    800955 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80092c:	ff 75 14             	pushl  0x14(%ebp)
  80092f:	ff 75 10             	pushl  0x10(%ebp)
  800932:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800935:	50                   	push   %eax
  800936:	68 42 05 80 00       	push   $0x800542
  80093b:	e8 3b fc ff ff       	call   80057b <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800940:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800943:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800946:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800949:	83 c4 10             	add    $0x10,%esp
  80094c:	eb 0c                	jmp    80095a <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80094e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800953:	eb 05                	jmp    80095a <vsnprintf+0x53>
  800955:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80095a:	c9                   	leave  
  80095b:	c3                   	ret    

0080095c <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80095c:	55                   	push   %ebp
  80095d:	89 e5                	mov    %esp,%ebp
  80095f:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800962:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800965:	50                   	push   %eax
  800966:	ff 75 10             	pushl  0x10(%ebp)
  800969:	ff 75 0c             	pushl  0xc(%ebp)
  80096c:	ff 75 08             	pushl  0x8(%ebp)
  80096f:	e8 93 ff ff ff       	call   800907 <vsnprintf>
	va_end(ap);

	return rc;
}
  800974:	c9                   	leave  
  800975:	c3                   	ret    

00800976 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800976:	55                   	push   %ebp
  800977:	89 e5                	mov    %esp,%ebp
  800979:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80097c:	b8 00 00 00 00       	mov    $0x0,%eax
  800981:	eb 01                	jmp    800984 <strlen+0xe>
		n++;
  800983:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800984:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800988:	75 f9                	jne    800983 <strlen+0xd>
		n++;
	return n;
}
  80098a:	5d                   	pop    %ebp
  80098b:	c3                   	ret    

0080098c <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80098c:	55                   	push   %ebp
  80098d:	89 e5                	mov    %esp,%ebp
  80098f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800992:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800995:	ba 00 00 00 00       	mov    $0x0,%edx
  80099a:	eb 01                	jmp    80099d <strnlen+0x11>
		n++;
  80099c:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80099d:	39 c2                	cmp    %eax,%edx
  80099f:	74 08                	je     8009a9 <strnlen+0x1d>
  8009a1:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009a5:	75 f5                	jne    80099c <strnlen+0x10>
  8009a7:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009a9:	5d                   	pop    %ebp
  8009aa:	c3                   	ret    

008009ab <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009ab:	55                   	push   %ebp
  8009ac:	89 e5                	mov    %esp,%ebp
  8009ae:	53                   	push   %ebx
  8009af:	8b 45 08             	mov    0x8(%ebp),%eax
  8009b2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009b5:	89 c2                	mov    %eax,%edx
  8009b7:	42                   	inc    %edx
  8009b8:	41                   	inc    %ecx
  8009b9:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009bc:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009bf:	84 db                	test   %bl,%bl
  8009c1:	75 f4                	jne    8009b7 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009c3:	5b                   	pop    %ebx
  8009c4:	5d                   	pop    %ebp
  8009c5:	c3                   	ret    

008009c6 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009c6:	55                   	push   %ebp
  8009c7:	89 e5                	mov    %esp,%ebp
  8009c9:	53                   	push   %ebx
  8009ca:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009cd:	53                   	push   %ebx
  8009ce:	e8 a3 ff ff ff       	call   800976 <strlen>
  8009d3:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009d6:	ff 75 0c             	pushl  0xc(%ebp)
  8009d9:	01 d8                	add    %ebx,%eax
  8009db:	50                   	push   %eax
  8009dc:	e8 ca ff ff ff       	call   8009ab <strcpy>
	return dst;
}
  8009e1:	89 d8                	mov    %ebx,%eax
  8009e3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009e6:	c9                   	leave  
  8009e7:	c3                   	ret    

008009e8 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009e8:	55                   	push   %ebp
  8009e9:	89 e5                	mov    %esp,%ebp
  8009eb:	56                   	push   %esi
  8009ec:	53                   	push   %ebx
  8009ed:	8b 75 08             	mov    0x8(%ebp),%esi
  8009f0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009f3:	89 f3                	mov    %esi,%ebx
  8009f5:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009f8:	89 f2                	mov    %esi,%edx
  8009fa:	eb 0c                	jmp    800a08 <strncpy+0x20>
		*dst++ = *src;
  8009fc:	42                   	inc    %edx
  8009fd:	8a 01                	mov    (%ecx),%al
  8009ff:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a02:	80 39 01             	cmpb   $0x1,(%ecx)
  800a05:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a08:	39 da                	cmp    %ebx,%edx
  800a0a:	75 f0                	jne    8009fc <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a0c:	89 f0                	mov    %esi,%eax
  800a0e:	5b                   	pop    %ebx
  800a0f:	5e                   	pop    %esi
  800a10:	5d                   	pop    %ebp
  800a11:	c3                   	ret    

00800a12 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a12:	55                   	push   %ebp
  800a13:	89 e5                	mov    %esp,%ebp
  800a15:	56                   	push   %esi
  800a16:	53                   	push   %ebx
  800a17:	8b 75 08             	mov    0x8(%ebp),%esi
  800a1a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a1d:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a20:	85 c0                	test   %eax,%eax
  800a22:	74 1e                	je     800a42 <strlcpy+0x30>
  800a24:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a28:	89 f2                	mov    %esi,%edx
  800a2a:	eb 05                	jmp    800a31 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a2c:	42                   	inc    %edx
  800a2d:	41                   	inc    %ecx
  800a2e:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a31:	39 c2                	cmp    %eax,%edx
  800a33:	74 08                	je     800a3d <strlcpy+0x2b>
  800a35:	8a 19                	mov    (%ecx),%bl
  800a37:	84 db                	test   %bl,%bl
  800a39:	75 f1                	jne    800a2c <strlcpy+0x1a>
  800a3b:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a3d:	c6 00 00             	movb   $0x0,(%eax)
  800a40:	eb 02                	jmp    800a44 <strlcpy+0x32>
  800a42:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a44:	29 f0                	sub    %esi,%eax
}
  800a46:	5b                   	pop    %ebx
  800a47:	5e                   	pop    %esi
  800a48:	5d                   	pop    %ebp
  800a49:	c3                   	ret    

00800a4a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a4a:	55                   	push   %ebp
  800a4b:	89 e5                	mov    %esp,%ebp
  800a4d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a50:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a53:	eb 02                	jmp    800a57 <strcmp+0xd>
		p++, q++;
  800a55:	41                   	inc    %ecx
  800a56:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a57:	8a 01                	mov    (%ecx),%al
  800a59:	84 c0                	test   %al,%al
  800a5b:	74 04                	je     800a61 <strcmp+0x17>
  800a5d:	3a 02                	cmp    (%edx),%al
  800a5f:	74 f4                	je     800a55 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a61:	0f b6 c0             	movzbl %al,%eax
  800a64:	0f b6 12             	movzbl (%edx),%edx
  800a67:	29 d0                	sub    %edx,%eax
}
  800a69:	5d                   	pop    %ebp
  800a6a:	c3                   	ret    

00800a6b <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a6b:	55                   	push   %ebp
  800a6c:	89 e5                	mov    %esp,%ebp
  800a6e:	53                   	push   %ebx
  800a6f:	8b 45 08             	mov    0x8(%ebp),%eax
  800a72:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a75:	89 c3                	mov    %eax,%ebx
  800a77:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a7a:	eb 02                	jmp    800a7e <strncmp+0x13>
		n--, p++, q++;
  800a7c:	40                   	inc    %eax
  800a7d:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a7e:	39 d8                	cmp    %ebx,%eax
  800a80:	74 14                	je     800a96 <strncmp+0x2b>
  800a82:	8a 08                	mov    (%eax),%cl
  800a84:	84 c9                	test   %cl,%cl
  800a86:	74 04                	je     800a8c <strncmp+0x21>
  800a88:	3a 0a                	cmp    (%edx),%cl
  800a8a:	74 f0                	je     800a7c <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a8c:	0f b6 00             	movzbl (%eax),%eax
  800a8f:	0f b6 12             	movzbl (%edx),%edx
  800a92:	29 d0                	sub    %edx,%eax
  800a94:	eb 05                	jmp    800a9b <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a96:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a9b:	5b                   	pop    %ebx
  800a9c:	5d                   	pop    %ebp
  800a9d:	c3                   	ret    

00800a9e <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a9e:	55                   	push   %ebp
  800a9f:	89 e5                	mov    %esp,%ebp
  800aa1:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800aa7:	eb 05                	jmp    800aae <strchr+0x10>
		if (*s == c)
  800aa9:	38 ca                	cmp    %cl,%dl
  800aab:	74 0c                	je     800ab9 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800aad:	40                   	inc    %eax
  800aae:	8a 10                	mov    (%eax),%dl
  800ab0:	84 d2                	test   %dl,%dl
  800ab2:	75 f5                	jne    800aa9 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800ab4:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ab9:	5d                   	pop    %ebp
  800aba:	c3                   	ret    

00800abb <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800abb:	55                   	push   %ebp
  800abc:	89 e5                	mov    %esp,%ebp
  800abe:	8b 45 08             	mov    0x8(%ebp),%eax
  800ac1:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ac4:	eb 05                	jmp    800acb <strfind+0x10>
		if (*s == c)
  800ac6:	38 ca                	cmp    %cl,%dl
  800ac8:	74 07                	je     800ad1 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800aca:	40                   	inc    %eax
  800acb:	8a 10                	mov    (%eax),%dl
  800acd:	84 d2                	test   %dl,%dl
  800acf:	75 f5                	jne    800ac6 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800ad1:	5d                   	pop    %ebp
  800ad2:	c3                   	ret    

00800ad3 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800ad3:	55                   	push   %ebp
  800ad4:	89 e5                	mov    %esp,%ebp
  800ad6:	57                   	push   %edi
  800ad7:	56                   	push   %esi
  800ad8:	53                   	push   %ebx
  800ad9:	8b 7d 08             	mov    0x8(%ebp),%edi
  800adc:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800adf:	85 c9                	test   %ecx,%ecx
  800ae1:	74 36                	je     800b19 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800ae3:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800ae9:	75 28                	jne    800b13 <memset+0x40>
  800aeb:	f6 c1 03             	test   $0x3,%cl
  800aee:	75 23                	jne    800b13 <memset+0x40>
		c &= 0xFF;
  800af0:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800af4:	89 d3                	mov    %edx,%ebx
  800af6:	c1 e3 08             	shl    $0x8,%ebx
  800af9:	89 d6                	mov    %edx,%esi
  800afb:	c1 e6 18             	shl    $0x18,%esi
  800afe:	89 d0                	mov    %edx,%eax
  800b00:	c1 e0 10             	shl    $0x10,%eax
  800b03:	09 f0                	or     %esi,%eax
  800b05:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b07:	89 d8                	mov    %ebx,%eax
  800b09:	09 d0                	or     %edx,%eax
  800b0b:	c1 e9 02             	shr    $0x2,%ecx
  800b0e:	fc                   	cld    
  800b0f:	f3 ab                	rep stos %eax,%es:(%edi)
  800b11:	eb 06                	jmp    800b19 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b13:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b16:	fc                   	cld    
  800b17:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b19:	89 f8                	mov    %edi,%eax
  800b1b:	5b                   	pop    %ebx
  800b1c:	5e                   	pop    %esi
  800b1d:	5f                   	pop    %edi
  800b1e:	5d                   	pop    %ebp
  800b1f:	c3                   	ret    

00800b20 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b20:	55                   	push   %ebp
  800b21:	89 e5                	mov    %esp,%ebp
  800b23:	57                   	push   %edi
  800b24:	56                   	push   %esi
  800b25:	8b 45 08             	mov    0x8(%ebp),%eax
  800b28:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b2b:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b2e:	39 c6                	cmp    %eax,%esi
  800b30:	73 33                	jae    800b65 <memmove+0x45>
  800b32:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b35:	39 d0                	cmp    %edx,%eax
  800b37:	73 2c                	jae    800b65 <memmove+0x45>
		s += n;
		d += n;
  800b39:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b3c:	89 d6                	mov    %edx,%esi
  800b3e:	09 fe                	or     %edi,%esi
  800b40:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b46:	75 13                	jne    800b5b <memmove+0x3b>
  800b48:	f6 c1 03             	test   $0x3,%cl
  800b4b:	75 0e                	jne    800b5b <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b4d:	83 ef 04             	sub    $0x4,%edi
  800b50:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b53:	c1 e9 02             	shr    $0x2,%ecx
  800b56:	fd                   	std    
  800b57:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b59:	eb 07                	jmp    800b62 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b5b:	4f                   	dec    %edi
  800b5c:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b5f:	fd                   	std    
  800b60:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b62:	fc                   	cld    
  800b63:	eb 1d                	jmp    800b82 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b65:	89 f2                	mov    %esi,%edx
  800b67:	09 c2                	or     %eax,%edx
  800b69:	f6 c2 03             	test   $0x3,%dl
  800b6c:	75 0f                	jne    800b7d <memmove+0x5d>
  800b6e:	f6 c1 03             	test   $0x3,%cl
  800b71:	75 0a                	jne    800b7d <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b73:	c1 e9 02             	shr    $0x2,%ecx
  800b76:	89 c7                	mov    %eax,%edi
  800b78:	fc                   	cld    
  800b79:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b7b:	eb 05                	jmp    800b82 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b7d:	89 c7                	mov    %eax,%edi
  800b7f:	fc                   	cld    
  800b80:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b82:	5e                   	pop    %esi
  800b83:	5f                   	pop    %edi
  800b84:	5d                   	pop    %ebp
  800b85:	c3                   	ret    

00800b86 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b86:	55                   	push   %ebp
  800b87:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b89:	ff 75 10             	pushl  0x10(%ebp)
  800b8c:	ff 75 0c             	pushl  0xc(%ebp)
  800b8f:	ff 75 08             	pushl  0x8(%ebp)
  800b92:	e8 89 ff ff ff       	call   800b20 <memmove>
}
  800b97:	c9                   	leave  
  800b98:	c3                   	ret    

00800b99 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b99:	55                   	push   %ebp
  800b9a:	89 e5                	mov    %esp,%ebp
  800b9c:	56                   	push   %esi
  800b9d:	53                   	push   %ebx
  800b9e:	8b 45 08             	mov    0x8(%ebp),%eax
  800ba1:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ba4:	89 c6                	mov    %eax,%esi
  800ba6:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ba9:	eb 14                	jmp    800bbf <memcmp+0x26>
		if (*s1 != *s2)
  800bab:	8a 08                	mov    (%eax),%cl
  800bad:	8a 1a                	mov    (%edx),%bl
  800baf:	38 d9                	cmp    %bl,%cl
  800bb1:	74 0a                	je     800bbd <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bb3:	0f b6 c1             	movzbl %cl,%eax
  800bb6:	0f b6 db             	movzbl %bl,%ebx
  800bb9:	29 d8                	sub    %ebx,%eax
  800bbb:	eb 0b                	jmp    800bc8 <memcmp+0x2f>
		s1++, s2++;
  800bbd:	40                   	inc    %eax
  800bbe:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bbf:	39 f0                	cmp    %esi,%eax
  800bc1:	75 e8                	jne    800bab <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bc3:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bc8:	5b                   	pop    %ebx
  800bc9:	5e                   	pop    %esi
  800bca:	5d                   	pop    %ebp
  800bcb:	c3                   	ret    

00800bcc <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bcc:	55                   	push   %ebp
  800bcd:	89 e5                	mov    %esp,%ebp
  800bcf:	53                   	push   %ebx
  800bd0:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bd3:	89 c1                	mov    %eax,%ecx
  800bd5:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bd8:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bdc:	eb 08                	jmp    800be6 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bde:	0f b6 10             	movzbl (%eax),%edx
  800be1:	39 da                	cmp    %ebx,%edx
  800be3:	74 05                	je     800bea <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800be5:	40                   	inc    %eax
  800be6:	39 c8                	cmp    %ecx,%eax
  800be8:	72 f4                	jb     800bde <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bea:	5b                   	pop    %ebx
  800beb:	5d                   	pop    %ebp
  800bec:	c3                   	ret    

00800bed <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bed:	55                   	push   %ebp
  800bee:	89 e5                	mov    %esp,%ebp
  800bf0:	57                   	push   %edi
  800bf1:	56                   	push   %esi
  800bf2:	53                   	push   %ebx
  800bf3:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf6:	eb 01                	jmp    800bf9 <strtol+0xc>
		s++;
  800bf8:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf9:	8a 01                	mov    (%ecx),%al
  800bfb:	3c 20                	cmp    $0x20,%al
  800bfd:	74 f9                	je     800bf8 <strtol+0xb>
  800bff:	3c 09                	cmp    $0x9,%al
  800c01:	74 f5                	je     800bf8 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c03:	3c 2b                	cmp    $0x2b,%al
  800c05:	75 08                	jne    800c0f <strtol+0x22>
		s++;
  800c07:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c08:	bf 00 00 00 00       	mov    $0x0,%edi
  800c0d:	eb 11                	jmp    800c20 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c0f:	3c 2d                	cmp    $0x2d,%al
  800c11:	75 08                	jne    800c1b <strtol+0x2e>
		s++, neg = 1;
  800c13:	41                   	inc    %ecx
  800c14:	bf 01 00 00 00       	mov    $0x1,%edi
  800c19:	eb 05                	jmp    800c20 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c1b:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c20:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c24:	0f 84 87 00 00 00    	je     800cb1 <strtol+0xc4>
  800c2a:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c2e:	75 27                	jne    800c57 <strtol+0x6a>
  800c30:	80 39 30             	cmpb   $0x30,(%ecx)
  800c33:	75 22                	jne    800c57 <strtol+0x6a>
  800c35:	e9 88 00 00 00       	jmp    800cc2 <strtol+0xd5>
		s += 2, base = 16;
  800c3a:	83 c1 02             	add    $0x2,%ecx
  800c3d:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c44:	eb 11                	jmp    800c57 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c46:	41                   	inc    %ecx
  800c47:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c4e:	eb 07                	jmp    800c57 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c50:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c57:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c5c:	8a 11                	mov    (%ecx),%dl
  800c5e:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c61:	80 fb 09             	cmp    $0x9,%bl
  800c64:	77 08                	ja     800c6e <strtol+0x81>
			dig = *s - '0';
  800c66:	0f be d2             	movsbl %dl,%edx
  800c69:	83 ea 30             	sub    $0x30,%edx
  800c6c:	eb 22                	jmp    800c90 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c6e:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c71:	89 f3                	mov    %esi,%ebx
  800c73:	80 fb 19             	cmp    $0x19,%bl
  800c76:	77 08                	ja     800c80 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c78:	0f be d2             	movsbl %dl,%edx
  800c7b:	83 ea 57             	sub    $0x57,%edx
  800c7e:	eb 10                	jmp    800c90 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c80:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c83:	89 f3                	mov    %esi,%ebx
  800c85:	80 fb 19             	cmp    $0x19,%bl
  800c88:	77 14                	ja     800c9e <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c8a:	0f be d2             	movsbl %dl,%edx
  800c8d:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c90:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c93:	7d 09                	jge    800c9e <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c95:	41                   	inc    %ecx
  800c96:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c9a:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c9c:	eb be                	jmp    800c5c <strtol+0x6f>

	if (endptr)
  800c9e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ca2:	74 05                	je     800ca9 <strtol+0xbc>
		*endptr = (char *) s;
  800ca4:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ca7:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ca9:	85 ff                	test   %edi,%edi
  800cab:	74 21                	je     800cce <strtol+0xe1>
  800cad:	f7 d8                	neg    %eax
  800caf:	eb 1d                	jmp    800cce <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800cb1:	80 39 30             	cmpb   $0x30,(%ecx)
  800cb4:	75 9a                	jne    800c50 <strtol+0x63>
  800cb6:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cba:	0f 84 7a ff ff ff    	je     800c3a <strtol+0x4d>
  800cc0:	eb 84                	jmp    800c46 <strtol+0x59>
  800cc2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cc6:	0f 84 6e ff ff ff    	je     800c3a <strtol+0x4d>
  800ccc:	eb 89                	jmp    800c57 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cce:	5b                   	pop    %ebx
  800ccf:	5e                   	pop    %esi
  800cd0:	5f                   	pop    %edi
  800cd1:	5d                   	pop    %ebp
  800cd2:	c3                   	ret    
  800cd3:	90                   	nop

00800cd4 <__udivdi3>:
  800cd4:	55                   	push   %ebp
  800cd5:	57                   	push   %edi
  800cd6:	56                   	push   %esi
  800cd7:	53                   	push   %ebx
  800cd8:	83 ec 1c             	sub    $0x1c,%esp
  800cdb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cdf:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ce3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ce7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ceb:	89 ca                	mov    %ecx,%edx
  800ced:	89 f8                	mov    %edi,%eax
  800cef:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cf3:	85 f6                	test   %esi,%esi
  800cf5:	75 2d                	jne    800d24 <__udivdi3+0x50>
  800cf7:	39 cf                	cmp    %ecx,%edi
  800cf9:	77 65                	ja     800d60 <__udivdi3+0x8c>
  800cfb:	89 fd                	mov    %edi,%ebp
  800cfd:	85 ff                	test   %edi,%edi
  800cff:	75 0b                	jne    800d0c <__udivdi3+0x38>
  800d01:	b8 01 00 00 00       	mov    $0x1,%eax
  800d06:	31 d2                	xor    %edx,%edx
  800d08:	f7 f7                	div    %edi
  800d0a:	89 c5                	mov    %eax,%ebp
  800d0c:	31 d2                	xor    %edx,%edx
  800d0e:	89 c8                	mov    %ecx,%eax
  800d10:	f7 f5                	div    %ebp
  800d12:	89 c1                	mov    %eax,%ecx
  800d14:	89 d8                	mov    %ebx,%eax
  800d16:	f7 f5                	div    %ebp
  800d18:	89 cf                	mov    %ecx,%edi
  800d1a:	89 fa                	mov    %edi,%edx
  800d1c:	83 c4 1c             	add    $0x1c,%esp
  800d1f:	5b                   	pop    %ebx
  800d20:	5e                   	pop    %esi
  800d21:	5f                   	pop    %edi
  800d22:	5d                   	pop    %ebp
  800d23:	c3                   	ret    
  800d24:	39 ce                	cmp    %ecx,%esi
  800d26:	77 28                	ja     800d50 <__udivdi3+0x7c>
  800d28:	0f bd fe             	bsr    %esi,%edi
  800d2b:	83 f7 1f             	xor    $0x1f,%edi
  800d2e:	75 40                	jne    800d70 <__udivdi3+0x9c>
  800d30:	39 ce                	cmp    %ecx,%esi
  800d32:	72 0a                	jb     800d3e <__udivdi3+0x6a>
  800d34:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d38:	0f 87 9e 00 00 00    	ja     800ddc <__udivdi3+0x108>
  800d3e:	b8 01 00 00 00       	mov    $0x1,%eax
  800d43:	89 fa                	mov    %edi,%edx
  800d45:	83 c4 1c             	add    $0x1c,%esp
  800d48:	5b                   	pop    %ebx
  800d49:	5e                   	pop    %esi
  800d4a:	5f                   	pop    %edi
  800d4b:	5d                   	pop    %ebp
  800d4c:	c3                   	ret    
  800d4d:	8d 76 00             	lea    0x0(%esi),%esi
  800d50:	31 ff                	xor    %edi,%edi
  800d52:	31 c0                	xor    %eax,%eax
  800d54:	89 fa                	mov    %edi,%edx
  800d56:	83 c4 1c             	add    $0x1c,%esp
  800d59:	5b                   	pop    %ebx
  800d5a:	5e                   	pop    %esi
  800d5b:	5f                   	pop    %edi
  800d5c:	5d                   	pop    %ebp
  800d5d:	c3                   	ret    
  800d5e:	66 90                	xchg   %ax,%ax
  800d60:	89 d8                	mov    %ebx,%eax
  800d62:	f7 f7                	div    %edi
  800d64:	31 ff                	xor    %edi,%edi
  800d66:	89 fa                	mov    %edi,%edx
  800d68:	83 c4 1c             	add    $0x1c,%esp
  800d6b:	5b                   	pop    %ebx
  800d6c:	5e                   	pop    %esi
  800d6d:	5f                   	pop    %edi
  800d6e:	5d                   	pop    %ebp
  800d6f:	c3                   	ret    
  800d70:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d75:	89 eb                	mov    %ebp,%ebx
  800d77:	29 fb                	sub    %edi,%ebx
  800d79:	89 f9                	mov    %edi,%ecx
  800d7b:	d3 e6                	shl    %cl,%esi
  800d7d:	89 c5                	mov    %eax,%ebp
  800d7f:	88 d9                	mov    %bl,%cl
  800d81:	d3 ed                	shr    %cl,%ebp
  800d83:	89 e9                	mov    %ebp,%ecx
  800d85:	09 f1                	or     %esi,%ecx
  800d87:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d8b:	89 f9                	mov    %edi,%ecx
  800d8d:	d3 e0                	shl    %cl,%eax
  800d8f:	89 c5                	mov    %eax,%ebp
  800d91:	89 d6                	mov    %edx,%esi
  800d93:	88 d9                	mov    %bl,%cl
  800d95:	d3 ee                	shr    %cl,%esi
  800d97:	89 f9                	mov    %edi,%ecx
  800d99:	d3 e2                	shl    %cl,%edx
  800d9b:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d9f:	88 d9                	mov    %bl,%cl
  800da1:	d3 e8                	shr    %cl,%eax
  800da3:	09 c2                	or     %eax,%edx
  800da5:	89 d0                	mov    %edx,%eax
  800da7:	89 f2                	mov    %esi,%edx
  800da9:	f7 74 24 0c          	divl   0xc(%esp)
  800dad:	89 d6                	mov    %edx,%esi
  800daf:	89 c3                	mov    %eax,%ebx
  800db1:	f7 e5                	mul    %ebp
  800db3:	39 d6                	cmp    %edx,%esi
  800db5:	72 19                	jb     800dd0 <__udivdi3+0xfc>
  800db7:	74 0b                	je     800dc4 <__udivdi3+0xf0>
  800db9:	89 d8                	mov    %ebx,%eax
  800dbb:	31 ff                	xor    %edi,%edi
  800dbd:	e9 58 ff ff ff       	jmp    800d1a <__udivdi3+0x46>
  800dc2:	66 90                	xchg   %ax,%ax
  800dc4:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dc8:	89 f9                	mov    %edi,%ecx
  800dca:	d3 e2                	shl    %cl,%edx
  800dcc:	39 c2                	cmp    %eax,%edx
  800dce:	73 e9                	jae    800db9 <__udivdi3+0xe5>
  800dd0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800dd3:	31 ff                	xor    %edi,%edi
  800dd5:	e9 40 ff ff ff       	jmp    800d1a <__udivdi3+0x46>
  800dda:	66 90                	xchg   %ax,%ax
  800ddc:	31 c0                	xor    %eax,%eax
  800dde:	e9 37 ff ff ff       	jmp    800d1a <__udivdi3+0x46>
  800de3:	90                   	nop

00800de4 <__umoddi3>:
  800de4:	55                   	push   %ebp
  800de5:	57                   	push   %edi
  800de6:	56                   	push   %esi
  800de7:	53                   	push   %ebx
  800de8:	83 ec 1c             	sub    $0x1c,%esp
  800deb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800def:	8b 74 24 34          	mov    0x34(%esp),%esi
  800df3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800df7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dfb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dff:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e03:	89 f3                	mov    %esi,%ebx
  800e05:	89 fa                	mov    %edi,%edx
  800e07:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e0b:	89 34 24             	mov    %esi,(%esp)
  800e0e:	85 c0                	test   %eax,%eax
  800e10:	75 1a                	jne    800e2c <__umoddi3+0x48>
  800e12:	39 f7                	cmp    %esi,%edi
  800e14:	0f 86 a2 00 00 00    	jbe    800ebc <__umoddi3+0xd8>
  800e1a:	89 c8                	mov    %ecx,%eax
  800e1c:	89 f2                	mov    %esi,%edx
  800e1e:	f7 f7                	div    %edi
  800e20:	89 d0                	mov    %edx,%eax
  800e22:	31 d2                	xor    %edx,%edx
  800e24:	83 c4 1c             	add    $0x1c,%esp
  800e27:	5b                   	pop    %ebx
  800e28:	5e                   	pop    %esi
  800e29:	5f                   	pop    %edi
  800e2a:	5d                   	pop    %ebp
  800e2b:	c3                   	ret    
  800e2c:	39 f0                	cmp    %esi,%eax
  800e2e:	0f 87 ac 00 00 00    	ja     800ee0 <__umoddi3+0xfc>
  800e34:	0f bd e8             	bsr    %eax,%ebp
  800e37:	83 f5 1f             	xor    $0x1f,%ebp
  800e3a:	0f 84 ac 00 00 00    	je     800eec <__umoddi3+0x108>
  800e40:	bf 20 00 00 00       	mov    $0x20,%edi
  800e45:	29 ef                	sub    %ebp,%edi
  800e47:	89 fe                	mov    %edi,%esi
  800e49:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e4d:	89 e9                	mov    %ebp,%ecx
  800e4f:	d3 e0                	shl    %cl,%eax
  800e51:	89 d7                	mov    %edx,%edi
  800e53:	89 f1                	mov    %esi,%ecx
  800e55:	d3 ef                	shr    %cl,%edi
  800e57:	09 c7                	or     %eax,%edi
  800e59:	89 e9                	mov    %ebp,%ecx
  800e5b:	d3 e2                	shl    %cl,%edx
  800e5d:	89 14 24             	mov    %edx,(%esp)
  800e60:	89 d8                	mov    %ebx,%eax
  800e62:	d3 e0                	shl    %cl,%eax
  800e64:	89 c2                	mov    %eax,%edx
  800e66:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e6a:	d3 e0                	shl    %cl,%eax
  800e6c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e70:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e74:	89 f1                	mov    %esi,%ecx
  800e76:	d3 e8                	shr    %cl,%eax
  800e78:	09 d0                	or     %edx,%eax
  800e7a:	d3 eb                	shr    %cl,%ebx
  800e7c:	89 da                	mov    %ebx,%edx
  800e7e:	f7 f7                	div    %edi
  800e80:	89 d3                	mov    %edx,%ebx
  800e82:	f7 24 24             	mull   (%esp)
  800e85:	89 c6                	mov    %eax,%esi
  800e87:	89 d1                	mov    %edx,%ecx
  800e89:	39 d3                	cmp    %edx,%ebx
  800e8b:	0f 82 87 00 00 00    	jb     800f18 <__umoddi3+0x134>
  800e91:	0f 84 91 00 00 00    	je     800f28 <__umoddi3+0x144>
  800e97:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e9b:	29 f2                	sub    %esi,%edx
  800e9d:	19 cb                	sbb    %ecx,%ebx
  800e9f:	89 d8                	mov    %ebx,%eax
  800ea1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ea5:	d3 e0                	shl    %cl,%eax
  800ea7:	89 e9                	mov    %ebp,%ecx
  800ea9:	d3 ea                	shr    %cl,%edx
  800eab:	09 d0                	or     %edx,%eax
  800ead:	89 e9                	mov    %ebp,%ecx
  800eaf:	d3 eb                	shr    %cl,%ebx
  800eb1:	89 da                	mov    %ebx,%edx
  800eb3:	83 c4 1c             	add    $0x1c,%esp
  800eb6:	5b                   	pop    %ebx
  800eb7:	5e                   	pop    %esi
  800eb8:	5f                   	pop    %edi
  800eb9:	5d                   	pop    %ebp
  800eba:	c3                   	ret    
  800ebb:	90                   	nop
  800ebc:	89 fd                	mov    %edi,%ebp
  800ebe:	85 ff                	test   %edi,%edi
  800ec0:	75 0b                	jne    800ecd <__umoddi3+0xe9>
  800ec2:	b8 01 00 00 00       	mov    $0x1,%eax
  800ec7:	31 d2                	xor    %edx,%edx
  800ec9:	f7 f7                	div    %edi
  800ecb:	89 c5                	mov    %eax,%ebp
  800ecd:	89 f0                	mov    %esi,%eax
  800ecf:	31 d2                	xor    %edx,%edx
  800ed1:	f7 f5                	div    %ebp
  800ed3:	89 c8                	mov    %ecx,%eax
  800ed5:	f7 f5                	div    %ebp
  800ed7:	89 d0                	mov    %edx,%eax
  800ed9:	e9 44 ff ff ff       	jmp    800e22 <__umoddi3+0x3e>
  800ede:	66 90                	xchg   %ax,%ax
  800ee0:	89 c8                	mov    %ecx,%eax
  800ee2:	89 f2                	mov    %esi,%edx
  800ee4:	83 c4 1c             	add    $0x1c,%esp
  800ee7:	5b                   	pop    %ebx
  800ee8:	5e                   	pop    %esi
  800ee9:	5f                   	pop    %edi
  800eea:	5d                   	pop    %ebp
  800eeb:	c3                   	ret    
  800eec:	3b 04 24             	cmp    (%esp),%eax
  800eef:	72 06                	jb     800ef7 <__umoddi3+0x113>
  800ef1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ef5:	77 0f                	ja     800f06 <__umoddi3+0x122>
  800ef7:	89 f2                	mov    %esi,%edx
  800ef9:	29 f9                	sub    %edi,%ecx
  800efb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800eff:	89 14 24             	mov    %edx,(%esp)
  800f02:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f06:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f0a:	8b 14 24             	mov    (%esp),%edx
  800f0d:	83 c4 1c             	add    $0x1c,%esp
  800f10:	5b                   	pop    %ebx
  800f11:	5e                   	pop    %esi
  800f12:	5f                   	pop    %edi
  800f13:	5d                   	pop    %ebp
  800f14:	c3                   	ret    
  800f15:	8d 76 00             	lea    0x0(%esi),%esi
  800f18:	2b 04 24             	sub    (%esp),%eax
  800f1b:	19 fa                	sbb    %edi,%edx
  800f1d:	89 d1                	mov    %edx,%ecx
  800f1f:	89 c6                	mov    %eax,%esi
  800f21:	e9 71 ff ff ff       	jmp    800e97 <__umoddi3+0xb3>
  800f26:	66 90                	xchg   %ax,%ax
  800f28:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f2c:	72 ea                	jb     800f18 <__umoddi3+0x134>
  800f2e:	89 d9                	mov    %ebx,%ecx
  800f30:	e9 62 ff ff ff       	jmp    800e97 <__umoddi3+0xb3>
