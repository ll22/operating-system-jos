
obj/user/faultallocbad.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 84 00 00 00       	call   8000b5 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <handler>:

#include <inc/lib.h>

void
handler(struct UTrapframe *utf)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 0c             	sub    $0xc,%esp
	int r;
	void *addr = (void*)utf->utf_fault_va;
  80003a:	8b 45 08             	mov    0x8(%ebp),%eax
  80003d:	8b 18                	mov    (%eax),%ebx

	cprintf("fault %x\n", addr);
  80003f:	53                   	push   %ebx
  800040:	68 40 10 80 00       	push   $0x801040
  800045:	e8 a4 01 00 00       	call   8001ee <cprintf>
	if ((r = sys_page_alloc(0, ROUNDDOWN(addr, PGSIZE),
  80004a:	83 c4 0c             	add    $0xc,%esp
  80004d:	6a 07                	push   $0x7
  80004f:	89 d8                	mov    %ebx,%eax
  800051:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800056:	50                   	push   %eax
  800057:	6a 00                	push   $0x0
  800059:	e8 d8 0a 00 00       	call   800b36 <sys_page_alloc>
  80005e:	83 c4 10             	add    $0x10,%esp
  800061:	85 c0                	test   %eax,%eax
  800063:	79 16                	jns    80007b <handler+0x48>
				PTE_P|PTE_U|PTE_W)) < 0)
		panic("allocating at %x in page fault handler: %e", addr, r);
  800065:	83 ec 0c             	sub    $0xc,%esp
  800068:	50                   	push   %eax
  800069:	53                   	push   %ebx
  80006a:	68 60 10 80 00       	push   $0x801060
  80006f:	6a 0f                	push   $0xf
  800071:	68 4a 10 80 00       	push   $0x80104a
  800076:	e8 9b 00 00 00       	call   800116 <_panic>
	snprintf((char*) addr, 100, "this string was faulted in at %x", addr);
  80007b:	53                   	push   %ebx
  80007c:	68 8c 10 80 00       	push   $0x80108c
  800081:	6a 64                	push   $0x64
  800083:	53                   	push   %ebx
  800084:	e8 7a 06 00 00       	call   800703 <snprintf>
}
  800089:	83 c4 10             	add    $0x10,%esp
  80008c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80008f:	c9                   	leave  
  800090:	c3                   	ret    

00800091 <umain>:

void
umain(int argc, char **argv)
{
  800091:	55                   	push   %ebp
  800092:	89 e5                	mov    %esp,%ebp
  800094:	83 ec 14             	sub    $0x14,%esp
	set_pgfault_handler(handler);
  800097:	68 33 00 80 00       	push   $0x800033
  80009c:	e8 a5 0c 00 00       	call   800d46 <set_pgfault_handler>
	sys_cputs((char*)0xDEADBEEF, 4);
  8000a1:	83 c4 08             	add    $0x8,%esp
  8000a4:	6a 04                	push   $0x4
  8000a6:	68 ef be ad de       	push   $0xdeadbeef
  8000ab:	e8 ca 09 00 00       	call   800a7a <sys_cputs>
}
  8000b0:	83 c4 10             	add    $0x10,%esp
  8000b3:	c9                   	leave  
  8000b4:	c3                   	ret    

008000b5 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000b5:	55                   	push   %ebp
  8000b6:	89 e5                	mov    %esp,%ebp
  8000b8:	56                   	push   %esi
  8000b9:	53                   	push   %ebx
  8000ba:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000bd:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000c0:	e8 33 0a 00 00       	call   800af8 <sys_getenvid>
  8000c5:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000ca:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000d1:	c1 e0 07             	shl    $0x7,%eax
  8000d4:	29 d0                	sub    %edx,%eax
  8000d6:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000db:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000e0:	85 db                	test   %ebx,%ebx
  8000e2:	7e 07                	jle    8000eb <libmain+0x36>
		binaryname = argv[0];
  8000e4:	8b 06                	mov    (%esi),%eax
  8000e6:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000eb:	83 ec 08             	sub    $0x8,%esp
  8000ee:	56                   	push   %esi
  8000ef:	53                   	push   %ebx
  8000f0:	e8 9c ff ff ff       	call   800091 <umain>

	// exit gracefully
	exit();
  8000f5:	e8 0a 00 00 00       	call   800104 <exit>
}
  8000fa:	83 c4 10             	add    $0x10,%esp
  8000fd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800100:	5b                   	pop    %ebx
  800101:	5e                   	pop    %esi
  800102:	5d                   	pop    %ebp
  800103:	c3                   	ret    

00800104 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800104:	55                   	push   %ebp
  800105:	89 e5                	mov    %esp,%ebp
  800107:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80010a:	6a 00                	push   $0x0
  80010c:	e8 a6 09 00 00       	call   800ab7 <sys_env_destroy>
}
  800111:	83 c4 10             	add    $0x10,%esp
  800114:	c9                   	leave  
  800115:	c3                   	ret    

00800116 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800116:	55                   	push   %ebp
  800117:	89 e5                	mov    %esp,%ebp
  800119:	56                   	push   %esi
  80011a:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80011b:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80011e:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800124:	e8 cf 09 00 00       	call   800af8 <sys_getenvid>
  800129:	83 ec 0c             	sub    $0xc,%esp
  80012c:	ff 75 0c             	pushl  0xc(%ebp)
  80012f:	ff 75 08             	pushl  0x8(%ebp)
  800132:	56                   	push   %esi
  800133:	50                   	push   %eax
  800134:	68 b8 10 80 00       	push   $0x8010b8
  800139:	e8 b0 00 00 00       	call   8001ee <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80013e:	83 c4 18             	add    $0x18,%esp
  800141:	53                   	push   %ebx
  800142:	ff 75 10             	pushl  0x10(%ebp)
  800145:	e8 53 00 00 00       	call   80019d <vcprintf>
	cprintf("\n");
  80014a:	c7 04 24 48 10 80 00 	movl   $0x801048,(%esp)
  800151:	e8 98 00 00 00       	call   8001ee <cprintf>
  800156:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800159:	cc                   	int3   
  80015a:	eb fd                	jmp    800159 <_panic+0x43>

0080015c <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80015c:	55                   	push   %ebp
  80015d:	89 e5                	mov    %esp,%ebp
  80015f:	53                   	push   %ebx
  800160:	83 ec 04             	sub    $0x4,%esp
  800163:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800166:	8b 13                	mov    (%ebx),%edx
  800168:	8d 42 01             	lea    0x1(%edx),%eax
  80016b:	89 03                	mov    %eax,(%ebx)
  80016d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800170:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800174:	3d ff 00 00 00       	cmp    $0xff,%eax
  800179:	75 1a                	jne    800195 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80017b:	83 ec 08             	sub    $0x8,%esp
  80017e:	68 ff 00 00 00       	push   $0xff
  800183:	8d 43 08             	lea    0x8(%ebx),%eax
  800186:	50                   	push   %eax
  800187:	e8 ee 08 00 00       	call   800a7a <sys_cputs>
		b->idx = 0;
  80018c:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800192:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800195:	ff 43 04             	incl   0x4(%ebx)
}
  800198:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80019b:	c9                   	leave  
  80019c:	c3                   	ret    

0080019d <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80019d:	55                   	push   %ebp
  80019e:	89 e5                	mov    %esp,%ebp
  8001a0:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001a6:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001ad:	00 00 00 
	b.cnt = 0;
  8001b0:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001b7:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001ba:	ff 75 0c             	pushl  0xc(%ebp)
  8001bd:	ff 75 08             	pushl  0x8(%ebp)
  8001c0:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001c6:	50                   	push   %eax
  8001c7:	68 5c 01 80 00       	push   $0x80015c
  8001cc:	e8 51 01 00 00       	call   800322 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001d1:	83 c4 08             	add    $0x8,%esp
  8001d4:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001da:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001e0:	50                   	push   %eax
  8001e1:	e8 94 08 00 00       	call   800a7a <sys_cputs>

	return b.cnt;
}
  8001e6:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001ec:	c9                   	leave  
  8001ed:	c3                   	ret    

008001ee <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001ee:	55                   	push   %ebp
  8001ef:	89 e5                	mov    %esp,%ebp
  8001f1:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001f4:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8001f7:	50                   	push   %eax
  8001f8:	ff 75 08             	pushl  0x8(%ebp)
  8001fb:	e8 9d ff ff ff       	call   80019d <vcprintf>
	va_end(ap);

	return cnt;
}
  800200:	c9                   	leave  
  800201:	c3                   	ret    

00800202 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800202:	55                   	push   %ebp
  800203:	89 e5                	mov    %esp,%ebp
  800205:	57                   	push   %edi
  800206:	56                   	push   %esi
  800207:	53                   	push   %ebx
  800208:	83 ec 1c             	sub    $0x1c,%esp
  80020b:	89 c7                	mov    %eax,%edi
  80020d:	89 d6                	mov    %edx,%esi
  80020f:	8b 45 08             	mov    0x8(%ebp),%eax
  800212:	8b 55 0c             	mov    0xc(%ebp),%edx
  800215:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800218:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80021b:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80021e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800223:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800226:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800229:	39 d3                	cmp    %edx,%ebx
  80022b:	72 05                	jb     800232 <printnum+0x30>
  80022d:	39 45 10             	cmp    %eax,0x10(%ebp)
  800230:	77 45                	ja     800277 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800232:	83 ec 0c             	sub    $0xc,%esp
  800235:	ff 75 18             	pushl  0x18(%ebp)
  800238:	8b 45 14             	mov    0x14(%ebp),%eax
  80023b:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80023e:	53                   	push   %ebx
  80023f:	ff 75 10             	pushl  0x10(%ebp)
  800242:	83 ec 08             	sub    $0x8,%esp
  800245:	ff 75 e4             	pushl  -0x1c(%ebp)
  800248:	ff 75 e0             	pushl  -0x20(%ebp)
  80024b:	ff 75 dc             	pushl  -0x24(%ebp)
  80024e:	ff 75 d8             	pushl  -0x28(%ebp)
  800251:	e8 72 0b 00 00       	call   800dc8 <__udivdi3>
  800256:	83 c4 18             	add    $0x18,%esp
  800259:	52                   	push   %edx
  80025a:	50                   	push   %eax
  80025b:	89 f2                	mov    %esi,%edx
  80025d:	89 f8                	mov    %edi,%eax
  80025f:	e8 9e ff ff ff       	call   800202 <printnum>
  800264:	83 c4 20             	add    $0x20,%esp
  800267:	eb 16                	jmp    80027f <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800269:	83 ec 08             	sub    $0x8,%esp
  80026c:	56                   	push   %esi
  80026d:	ff 75 18             	pushl  0x18(%ebp)
  800270:	ff d7                	call   *%edi
  800272:	83 c4 10             	add    $0x10,%esp
  800275:	eb 03                	jmp    80027a <printnum+0x78>
  800277:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80027a:	4b                   	dec    %ebx
  80027b:	85 db                	test   %ebx,%ebx
  80027d:	7f ea                	jg     800269 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80027f:	83 ec 08             	sub    $0x8,%esp
  800282:	56                   	push   %esi
  800283:	83 ec 04             	sub    $0x4,%esp
  800286:	ff 75 e4             	pushl  -0x1c(%ebp)
  800289:	ff 75 e0             	pushl  -0x20(%ebp)
  80028c:	ff 75 dc             	pushl  -0x24(%ebp)
  80028f:	ff 75 d8             	pushl  -0x28(%ebp)
  800292:	e8 41 0c 00 00       	call   800ed8 <__umoddi3>
  800297:	83 c4 14             	add    $0x14,%esp
  80029a:	0f be 80 db 10 80 00 	movsbl 0x8010db(%eax),%eax
  8002a1:	50                   	push   %eax
  8002a2:	ff d7                	call   *%edi
}
  8002a4:	83 c4 10             	add    $0x10,%esp
  8002a7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002aa:	5b                   	pop    %ebx
  8002ab:	5e                   	pop    %esi
  8002ac:	5f                   	pop    %edi
  8002ad:	5d                   	pop    %ebp
  8002ae:	c3                   	ret    

008002af <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002af:	55                   	push   %ebp
  8002b0:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002b2:	83 fa 01             	cmp    $0x1,%edx
  8002b5:	7e 0e                	jle    8002c5 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002b7:	8b 10                	mov    (%eax),%edx
  8002b9:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002bc:	89 08                	mov    %ecx,(%eax)
  8002be:	8b 02                	mov    (%edx),%eax
  8002c0:	8b 52 04             	mov    0x4(%edx),%edx
  8002c3:	eb 22                	jmp    8002e7 <getuint+0x38>
	else if (lflag)
  8002c5:	85 d2                	test   %edx,%edx
  8002c7:	74 10                	je     8002d9 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002c9:	8b 10                	mov    (%eax),%edx
  8002cb:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002ce:	89 08                	mov    %ecx,(%eax)
  8002d0:	8b 02                	mov    (%edx),%eax
  8002d2:	ba 00 00 00 00       	mov    $0x0,%edx
  8002d7:	eb 0e                	jmp    8002e7 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002d9:	8b 10                	mov    (%eax),%edx
  8002db:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002de:	89 08                	mov    %ecx,(%eax)
  8002e0:	8b 02                	mov    (%edx),%eax
  8002e2:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002e7:	5d                   	pop    %ebp
  8002e8:	c3                   	ret    

008002e9 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002e9:	55                   	push   %ebp
  8002ea:	89 e5                	mov    %esp,%ebp
  8002ec:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002ef:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8002f2:	8b 10                	mov    (%eax),%edx
  8002f4:	3b 50 04             	cmp    0x4(%eax),%edx
  8002f7:	73 0a                	jae    800303 <sprintputch+0x1a>
		*b->buf++ = ch;
  8002f9:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002fc:	89 08                	mov    %ecx,(%eax)
  8002fe:	8b 45 08             	mov    0x8(%ebp),%eax
  800301:	88 02                	mov    %al,(%edx)
}
  800303:	5d                   	pop    %ebp
  800304:	c3                   	ret    

00800305 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800305:	55                   	push   %ebp
  800306:	89 e5                	mov    %esp,%ebp
  800308:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80030b:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80030e:	50                   	push   %eax
  80030f:	ff 75 10             	pushl  0x10(%ebp)
  800312:	ff 75 0c             	pushl  0xc(%ebp)
  800315:	ff 75 08             	pushl  0x8(%ebp)
  800318:	e8 05 00 00 00       	call   800322 <vprintfmt>
	va_end(ap);
}
  80031d:	83 c4 10             	add    $0x10,%esp
  800320:	c9                   	leave  
  800321:	c3                   	ret    

00800322 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800322:	55                   	push   %ebp
  800323:	89 e5                	mov    %esp,%ebp
  800325:	57                   	push   %edi
  800326:	56                   	push   %esi
  800327:	53                   	push   %ebx
  800328:	83 ec 2c             	sub    $0x2c,%esp
  80032b:	8b 75 08             	mov    0x8(%ebp),%esi
  80032e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800331:	8b 7d 10             	mov    0x10(%ebp),%edi
  800334:	eb 12                	jmp    800348 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800336:	85 c0                	test   %eax,%eax
  800338:	0f 84 68 03 00 00    	je     8006a6 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80033e:	83 ec 08             	sub    $0x8,%esp
  800341:	53                   	push   %ebx
  800342:	50                   	push   %eax
  800343:	ff d6                	call   *%esi
  800345:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800348:	47                   	inc    %edi
  800349:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80034d:	83 f8 25             	cmp    $0x25,%eax
  800350:	75 e4                	jne    800336 <vprintfmt+0x14>
  800352:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800356:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80035d:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800364:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80036b:	ba 00 00 00 00       	mov    $0x0,%edx
  800370:	eb 07                	jmp    800379 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800372:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800375:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800379:	8d 47 01             	lea    0x1(%edi),%eax
  80037c:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80037f:	0f b6 0f             	movzbl (%edi),%ecx
  800382:	8a 07                	mov    (%edi),%al
  800384:	83 e8 23             	sub    $0x23,%eax
  800387:	3c 55                	cmp    $0x55,%al
  800389:	0f 87 fe 02 00 00    	ja     80068d <vprintfmt+0x36b>
  80038f:	0f b6 c0             	movzbl %al,%eax
  800392:	ff 24 85 20 12 80 00 	jmp    *0x801220(,%eax,4)
  800399:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80039c:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003a0:	eb d7                	jmp    800379 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003a5:	b8 00 00 00 00       	mov    $0x0,%eax
  8003aa:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003ad:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003b0:	01 c0                	add    %eax,%eax
  8003b2:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003b6:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003b9:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003bc:	83 fa 09             	cmp    $0x9,%edx
  8003bf:	77 34                	ja     8003f5 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003c1:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003c2:	eb e9                	jmp    8003ad <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003c4:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c7:	8d 48 04             	lea    0x4(%eax),%ecx
  8003ca:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003cd:	8b 00                	mov    (%eax),%eax
  8003cf:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003d5:	eb 24                	jmp    8003fb <vprintfmt+0xd9>
  8003d7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003db:	79 07                	jns    8003e4 <vprintfmt+0xc2>
  8003dd:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003e7:	eb 90                	jmp    800379 <vprintfmt+0x57>
  8003e9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003ec:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8003f3:	eb 84                	jmp    800379 <vprintfmt+0x57>
  8003f5:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003f8:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003fb:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003ff:	0f 89 74 ff ff ff    	jns    800379 <vprintfmt+0x57>
				width = precision, precision = -1;
  800405:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800408:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80040b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800412:	e9 62 ff ff ff       	jmp    800379 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800417:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800418:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80041b:	e9 59 ff ff ff       	jmp    800379 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800420:	8b 45 14             	mov    0x14(%ebp),%eax
  800423:	8d 50 04             	lea    0x4(%eax),%edx
  800426:	89 55 14             	mov    %edx,0x14(%ebp)
  800429:	83 ec 08             	sub    $0x8,%esp
  80042c:	53                   	push   %ebx
  80042d:	ff 30                	pushl  (%eax)
  80042f:	ff d6                	call   *%esi
			break;
  800431:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800434:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800437:	e9 0c ff ff ff       	jmp    800348 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80043c:	8b 45 14             	mov    0x14(%ebp),%eax
  80043f:	8d 50 04             	lea    0x4(%eax),%edx
  800442:	89 55 14             	mov    %edx,0x14(%ebp)
  800445:	8b 00                	mov    (%eax),%eax
  800447:	85 c0                	test   %eax,%eax
  800449:	79 02                	jns    80044d <vprintfmt+0x12b>
  80044b:	f7 d8                	neg    %eax
  80044d:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80044f:	83 f8 0f             	cmp    $0xf,%eax
  800452:	7f 0b                	jg     80045f <vprintfmt+0x13d>
  800454:	8b 04 85 80 13 80 00 	mov    0x801380(,%eax,4),%eax
  80045b:	85 c0                	test   %eax,%eax
  80045d:	75 18                	jne    800477 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80045f:	52                   	push   %edx
  800460:	68 f3 10 80 00       	push   $0x8010f3
  800465:	53                   	push   %ebx
  800466:	56                   	push   %esi
  800467:	e8 99 fe ff ff       	call   800305 <printfmt>
  80046c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80046f:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800472:	e9 d1 fe ff ff       	jmp    800348 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800477:	50                   	push   %eax
  800478:	68 fc 10 80 00       	push   $0x8010fc
  80047d:	53                   	push   %ebx
  80047e:	56                   	push   %esi
  80047f:	e8 81 fe ff ff       	call   800305 <printfmt>
  800484:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800487:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80048a:	e9 b9 fe ff ff       	jmp    800348 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80048f:	8b 45 14             	mov    0x14(%ebp),%eax
  800492:	8d 50 04             	lea    0x4(%eax),%edx
  800495:	89 55 14             	mov    %edx,0x14(%ebp)
  800498:	8b 38                	mov    (%eax),%edi
  80049a:	85 ff                	test   %edi,%edi
  80049c:	75 05                	jne    8004a3 <vprintfmt+0x181>
				p = "(null)";
  80049e:	bf ec 10 80 00       	mov    $0x8010ec,%edi
			if (width > 0 && padc != '-')
  8004a3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004a7:	0f 8e 90 00 00 00    	jle    80053d <vprintfmt+0x21b>
  8004ad:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004b1:	0f 84 8e 00 00 00    	je     800545 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004b7:	83 ec 08             	sub    $0x8,%esp
  8004ba:	ff 75 d0             	pushl  -0x30(%ebp)
  8004bd:	57                   	push   %edi
  8004be:	e8 70 02 00 00       	call   800733 <strnlen>
  8004c3:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004c6:	29 c1                	sub    %eax,%ecx
  8004c8:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004cb:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004ce:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004d2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004d5:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004d8:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004da:	eb 0d                	jmp    8004e9 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004dc:	83 ec 08             	sub    $0x8,%esp
  8004df:	53                   	push   %ebx
  8004e0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004e3:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004e5:	4f                   	dec    %edi
  8004e6:	83 c4 10             	add    $0x10,%esp
  8004e9:	85 ff                	test   %edi,%edi
  8004eb:	7f ef                	jg     8004dc <vprintfmt+0x1ba>
  8004ed:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8004f0:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004f3:	89 c8                	mov    %ecx,%eax
  8004f5:	85 c9                	test   %ecx,%ecx
  8004f7:	79 05                	jns    8004fe <vprintfmt+0x1dc>
  8004f9:	b8 00 00 00 00       	mov    $0x0,%eax
  8004fe:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800501:	29 c1                	sub    %eax,%ecx
  800503:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800506:	89 75 08             	mov    %esi,0x8(%ebp)
  800509:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80050c:	eb 3d                	jmp    80054b <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80050e:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800512:	74 19                	je     80052d <vprintfmt+0x20b>
  800514:	0f be c0             	movsbl %al,%eax
  800517:	83 e8 20             	sub    $0x20,%eax
  80051a:	83 f8 5e             	cmp    $0x5e,%eax
  80051d:	76 0e                	jbe    80052d <vprintfmt+0x20b>
					putch('?', putdat);
  80051f:	83 ec 08             	sub    $0x8,%esp
  800522:	53                   	push   %ebx
  800523:	6a 3f                	push   $0x3f
  800525:	ff 55 08             	call   *0x8(%ebp)
  800528:	83 c4 10             	add    $0x10,%esp
  80052b:	eb 0b                	jmp    800538 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80052d:	83 ec 08             	sub    $0x8,%esp
  800530:	53                   	push   %ebx
  800531:	52                   	push   %edx
  800532:	ff 55 08             	call   *0x8(%ebp)
  800535:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800538:	ff 4d e4             	decl   -0x1c(%ebp)
  80053b:	eb 0e                	jmp    80054b <vprintfmt+0x229>
  80053d:	89 75 08             	mov    %esi,0x8(%ebp)
  800540:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800543:	eb 06                	jmp    80054b <vprintfmt+0x229>
  800545:	89 75 08             	mov    %esi,0x8(%ebp)
  800548:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80054b:	47                   	inc    %edi
  80054c:	8a 47 ff             	mov    -0x1(%edi),%al
  80054f:	0f be d0             	movsbl %al,%edx
  800552:	85 d2                	test   %edx,%edx
  800554:	74 1d                	je     800573 <vprintfmt+0x251>
  800556:	85 f6                	test   %esi,%esi
  800558:	78 b4                	js     80050e <vprintfmt+0x1ec>
  80055a:	4e                   	dec    %esi
  80055b:	79 b1                	jns    80050e <vprintfmt+0x1ec>
  80055d:	8b 75 08             	mov    0x8(%ebp),%esi
  800560:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800563:	eb 14                	jmp    800579 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800565:	83 ec 08             	sub    $0x8,%esp
  800568:	53                   	push   %ebx
  800569:	6a 20                	push   $0x20
  80056b:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80056d:	4f                   	dec    %edi
  80056e:	83 c4 10             	add    $0x10,%esp
  800571:	eb 06                	jmp    800579 <vprintfmt+0x257>
  800573:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800576:	8b 75 08             	mov    0x8(%ebp),%esi
  800579:	85 ff                	test   %edi,%edi
  80057b:	7f e8                	jg     800565 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80057d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800580:	e9 c3 fd ff ff       	jmp    800348 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800585:	83 fa 01             	cmp    $0x1,%edx
  800588:	7e 16                	jle    8005a0 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80058a:	8b 45 14             	mov    0x14(%ebp),%eax
  80058d:	8d 50 08             	lea    0x8(%eax),%edx
  800590:	89 55 14             	mov    %edx,0x14(%ebp)
  800593:	8b 50 04             	mov    0x4(%eax),%edx
  800596:	8b 00                	mov    (%eax),%eax
  800598:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80059b:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80059e:	eb 32                	jmp    8005d2 <vprintfmt+0x2b0>
	else if (lflag)
  8005a0:	85 d2                	test   %edx,%edx
  8005a2:	74 18                	je     8005bc <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005a4:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a7:	8d 50 04             	lea    0x4(%eax),%edx
  8005aa:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ad:	8b 00                	mov    (%eax),%eax
  8005af:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005b2:	89 c1                	mov    %eax,%ecx
  8005b4:	c1 f9 1f             	sar    $0x1f,%ecx
  8005b7:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005ba:	eb 16                	jmp    8005d2 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005bc:	8b 45 14             	mov    0x14(%ebp),%eax
  8005bf:	8d 50 04             	lea    0x4(%eax),%edx
  8005c2:	89 55 14             	mov    %edx,0x14(%ebp)
  8005c5:	8b 00                	mov    (%eax),%eax
  8005c7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005ca:	89 c1                	mov    %eax,%ecx
  8005cc:	c1 f9 1f             	sar    $0x1f,%ecx
  8005cf:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005d2:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005d5:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005d8:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005dc:	79 76                	jns    800654 <vprintfmt+0x332>
				putch('-', putdat);
  8005de:	83 ec 08             	sub    $0x8,%esp
  8005e1:	53                   	push   %ebx
  8005e2:	6a 2d                	push   $0x2d
  8005e4:	ff d6                	call   *%esi
				num = -(long long) num;
  8005e6:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005e9:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005ec:	f7 d8                	neg    %eax
  8005ee:	83 d2 00             	adc    $0x0,%edx
  8005f1:	f7 da                	neg    %edx
  8005f3:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8005f6:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005fb:	eb 5c                	jmp    800659 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005fd:	8d 45 14             	lea    0x14(%ebp),%eax
  800600:	e8 aa fc ff ff       	call   8002af <getuint>
			base = 10;
  800605:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80060a:	eb 4d                	jmp    800659 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80060c:	8d 45 14             	lea    0x14(%ebp),%eax
  80060f:	e8 9b fc ff ff       	call   8002af <getuint>
			base = 8;
  800614:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800619:	eb 3e                	jmp    800659 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80061b:	83 ec 08             	sub    $0x8,%esp
  80061e:	53                   	push   %ebx
  80061f:	6a 30                	push   $0x30
  800621:	ff d6                	call   *%esi
			putch('x', putdat);
  800623:	83 c4 08             	add    $0x8,%esp
  800626:	53                   	push   %ebx
  800627:	6a 78                	push   $0x78
  800629:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80062b:	8b 45 14             	mov    0x14(%ebp),%eax
  80062e:	8d 50 04             	lea    0x4(%eax),%edx
  800631:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800634:	8b 00                	mov    (%eax),%eax
  800636:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80063b:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80063e:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800643:	eb 14                	jmp    800659 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800645:	8d 45 14             	lea    0x14(%ebp),%eax
  800648:	e8 62 fc ff ff       	call   8002af <getuint>
			base = 16;
  80064d:	b9 10 00 00 00       	mov    $0x10,%ecx
  800652:	eb 05                	jmp    800659 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800654:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800659:	83 ec 0c             	sub    $0xc,%esp
  80065c:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800660:	57                   	push   %edi
  800661:	ff 75 e4             	pushl  -0x1c(%ebp)
  800664:	51                   	push   %ecx
  800665:	52                   	push   %edx
  800666:	50                   	push   %eax
  800667:	89 da                	mov    %ebx,%edx
  800669:	89 f0                	mov    %esi,%eax
  80066b:	e8 92 fb ff ff       	call   800202 <printnum>
			break;
  800670:	83 c4 20             	add    $0x20,%esp
  800673:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800676:	e9 cd fc ff ff       	jmp    800348 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80067b:	83 ec 08             	sub    $0x8,%esp
  80067e:	53                   	push   %ebx
  80067f:	51                   	push   %ecx
  800680:	ff d6                	call   *%esi
			break;
  800682:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800685:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800688:	e9 bb fc ff ff       	jmp    800348 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80068d:	83 ec 08             	sub    $0x8,%esp
  800690:	53                   	push   %ebx
  800691:	6a 25                	push   $0x25
  800693:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800695:	83 c4 10             	add    $0x10,%esp
  800698:	eb 01                	jmp    80069b <vprintfmt+0x379>
  80069a:	4f                   	dec    %edi
  80069b:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80069f:	75 f9                	jne    80069a <vprintfmt+0x378>
  8006a1:	e9 a2 fc ff ff       	jmp    800348 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006a6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006a9:	5b                   	pop    %ebx
  8006aa:	5e                   	pop    %esi
  8006ab:	5f                   	pop    %edi
  8006ac:	5d                   	pop    %ebp
  8006ad:	c3                   	ret    

008006ae <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006ae:	55                   	push   %ebp
  8006af:	89 e5                	mov    %esp,%ebp
  8006b1:	83 ec 18             	sub    $0x18,%esp
  8006b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8006b7:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006ba:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006bd:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006c1:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006c4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006cb:	85 c0                	test   %eax,%eax
  8006cd:	74 26                	je     8006f5 <vsnprintf+0x47>
  8006cf:	85 d2                	test   %edx,%edx
  8006d1:	7e 29                	jle    8006fc <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006d3:	ff 75 14             	pushl  0x14(%ebp)
  8006d6:	ff 75 10             	pushl  0x10(%ebp)
  8006d9:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006dc:	50                   	push   %eax
  8006dd:	68 e9 02 80 00       	push   $0x8002e9
  8006e2:	e8 3b fc ff ff       	call   800322 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006ea:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8006f0:	83 c4 10             	add    $0x10,%esp
  8006f3:	eb 0c                	jmp    800701 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8006f5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006fa:	eb 05                	jmp    800701 <vsnprintf+0x53>
  8006fc:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800701:	c9                   	leave  
  800702:	c3                   	ret    

00800703 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800703:	55                   	push   %ebp
  800704:	89 e5                	mov    %esp,%ebp
  800706:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800709:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80070c:	50                   	push   %eax
  80070d:	ff 75 10             	pushl  0x10(%ebp)
  800710:	ff 75 0c             	pushl  0xc(%ebp)
  800713:	ff 75 08             	pushl  0x8(%ebp)
  800716:	e8 93 ff ff ff       	call   8006ae <vsnprintf>
	va_end(ap);

	return rc;
}
  80071b:	c9                   	leave  
  80071c:	c3                   	ret    

0080071d <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80071d:	55                   	push   %ebp
  80071e:	89 e5                	mov    %esp,%ebp
  800720:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800723:	b8 00 00 00 00       	mov    $0x0,%eax
  800728:	eb 01                	jmp    80072b <strlen+0xe>
		n++;
  80072a:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80072b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80072f:	75 f9                	jne    80072a <strlen+0xd>
		n++;
	return n;
}
  800731:	5d                   	pop    %ebp
  800732:	c3                   	ret    

00800733 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800733:	55                   	push   %ebp
  800734:	89 e5                	mov    %esp,%ebp
  800736:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800739:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80073c:	ba 00 00 00 00       	mov    $0x0,%edx
  800741:	eb 01                	jmp    800744 <strnlen+0x11>
		n++;
  800743:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800744:	39 c2                	cmp    %eax,%edx
  800746:	74 08                	je     800750 <strnlen+0x1d>
  800748:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80074c:	75 f5                	jne    800743 <strnlen+0x10>
  80074e:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800750:	5d                   	pop    %ebp
  800751:	c3                   	ret    

00800752 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800752:	55                   	push   %ebp
  800753:	89 e5                	mov    %esp,%ebp
  800755:	53                   	push   %ebx
  800756:	8b 45 08             	mov    0x8(%ebp),%eax
  800759:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80075c:	89 c2                	mov    %eax,%edx
  80075e:	42                   	inc    %edx
  80075f:	41                   	inc    %ecx
  800760:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800763:	88 5a ff             	mov    %bl,-0x1(%edx)
  800766:	84 db                	test   %bl,%bl
  800768:	75 f4                	jne    80075e <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80076a:	5b                   	pop    %ebx
  80076b:	5d                   	pop    %ebp
  80076c:	c3                   	ret    

0080076d <strcat>:

char *
strcat(char *dst, const char *src)
{
  80076d:	55                   	push   %ebp
  80076e:	89 e5                	mov    %esp,%ebp
  800770:	53                   	push   %ebx
  800771:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800774:	53                   	push   %ebx
  800775:	e8 a3 ff ff ff       	call   80071d <strlen>
  80077a:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80077d:	ff 75 0c             	pushl  0xc(%ebp)
  800780:	01 d8                	add    %ebx,%eax
  800782:	50                   	push   %eax
  800783:	e8 ca ff ff ff       	call   800752 <strcpy>
	return dst;
}
  800788:	89 d8                	mov    %ebx,%eax
  80078a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80078d:	c9                   	leave  
  80078e:	c3                   	ret    

0080078f <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80078f:	55                   	push   %ebp
  800790:	89 e5                	mov    %esp,%ebp
  800792:	56                   	push   %esi
  800793:	53                   	push   %ebx
  800794:	8b 75 08             	mov    0x8(%ebp),%esi
  800797:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80079a:	89 f3                	mov    %esi,%ebx
  80079c:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80079f:	89 f2                	mov    %esi,%edx
  8007a1:	eb 0c                	jmp    8007af <strncpy+0x20>
		*dst++ = *src;
  8007a3:	42                   	inc    %edx
  8007a4:	8a 01                	mov    (%ecx),%al
  8007a6:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007a9:	80 39 01             	cmpb   $0x1,(%ecx)
  8007ac:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007af:	39 da                	cmp    %ebx,%edx
  8007b1:	75 f0                	jne    8007a3 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007b3:	89 f0                	mov    %esi,%eax
  8007b5:	5b                   	pop    %ebx
  8007b6:	5e                   	pop    %esi
  8007b7:	5d                   	pop    %ebp
  8007b8:	c3                   	ret    

008007b9 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007b9:	55                   	push   %ebp
  8007ba:	89 e5                	mov    %esp,%ebp
  8007bc:	56                   	push   %esi
  8007bd:	53                   	push   %ebx
  8007be:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007c4:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007c7:	85 c0                	test   %eax,%eax
  8007c9:	74 1e                	je     8007e9 <strlcpy+0x30>
  8007cb:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007cf:	89 f2                	mov    %esi,%edx
  8007d1:	eb 05                	jmp    8007d8 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007d3:	42                   	inc    %edx
  8007d4:	41                   	inc    %ecx
  8007d5:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007d8:	39 c2                	cmp    %eax,%edx
  8007da:	74 08                	je     8007e4 <strlcpy+0x2b>
  8007dc:	8a 19                	mov    (%ecx),%bl
  8007de:	84 db                	test   %bl,%bl
  8007e0:	75 f1                	jne    8007d3 <strlcpy+0x1a>
  8007e2:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007e4:	c6 00 00             	movb   $0x0,(%eax)
  8007e7:	eb 02                	jmp    8007eb <strlcpy+0x32>
  8007e9:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007eb:	29 f0                	sub    %esi,%eax
}
  8007ed:	5b                   	pop    %ebx
  8007ee:	5e                   	pop    %esi
  8007ef:	5d                   	pop    %ebp
  8007f0:	c3                   	ret    

008007f1 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8007f1:	55                   	push   %ebp
  8007f2:	89 e5                	mov    %esp,%ebp
  8007f4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007f7:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007fa:	eb 02                	jmp    8007fe <strcmp+0xd>
		p++, q++;
  8007fc:	41                   	inc    %ecx
  8007fd:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007fe:	8a 01                	mov    (%ecx),%al
  800800:	84 c0                	test   %al,%al
  800802:	74 04                	je     800808 <strcmp+0x17>
  800804:	3a 02                	cmp    (%edx),%al
  800806:	74 f4                	je     8007fc <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800808:	0f b6 c0             	movzbl %al,%eax
  80080b:	0f b6 12             	movzbl (%edx),%edx
  80080e:	29 d0                	sub    %edx,%eax
}
  800810:	5d                   	pop    %ebp
  800811:	c3                   	ret    

00800812 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800812:	55                   	push   %ebp
  800813:	89 e5                	mov    %esp,%ebp
  800815:	53                   	push   %ebx
  800816:	8b 45 08             	mov    0x8(%ebp),%eax
  800819:	8b 55 0c             	mov    0xc(%ebp),%edx
  80081c:	89 c3                	mov    %eax,%ebx
  80081e:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800821:	eb 02                	jmp    800825 <strncmp+0x13>
		n--, p++, q++;
  800823:	40                   	inc    %eax
  800824:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800825:	39 d8                	cmp    %ebx,%eax
  800827:	74 14                	je     80083d <strncmp+0x2b>
  800829:	8a 08                	mov    (%eax),%cl
  80082b:	84 c9                	test   %cl,%cl
  80082d:	74 04                	je     800833 <strncmp+0x21>
  80082f:	3a 0a                	cmp    (%edx),%cl
  800831:	74 f0                	je     800823 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800833:	0f b6 00             	movzbl (%eax),%eax
  800836:	0f b6 12             	movzbl (%edx),%edx
  800839:	29 d0                	sub    %edx,%eax
  80083b:	eb 05                	jmp    800842 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80083d:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800842:	5b                   	pop    %ebx
  800843:	5d                   	pop    %ebp
  800844:	c3                   	ret    

00800845 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800845:	55                   	push   %ebp
  800846:	89 e5                	mov    %esp,%ebp
  800848:	8b 45 08             	mov    0x8(%ebp),%eax
  80084b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80084e:	eb 05                	jmp    800855 <strchr+0x10>
		if (*s == c)
  800850:	38 ca                	cmp    %cl,%dl
  800852:	74 0c                	je     800860 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800854:	40                   	inc    %eax
  800855:	8a 10                	mov    (%eax),%dl
  800857:	84 d2                	test   %dl,%dl
  800859:	75 f5                	jne    800850 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80085b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800860:	5d                   	pop    %ebp
  800861:	c3                   	ret    

00800862 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800862:	55                   	push   %ebp
  800863:	89 e5                	mov    %esp,%ebp
  800865:	8b 45 08             	mov    0x8(%ebp),%eax
  800868:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80086b:	eb 05                	jmp    800872 <strfind+0x10>
		if (*s == c)
  80086d:	38 ca                	cmp    %cl,%dl
  80086f:	74 07                	je     800878 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800871:	40                   	inc    %eax
  800872:	8a 10                	mov    (%eax),%dl
  800874:	84 d2                	test   %dl,%dl
  800876:	75 f5                	jne    80086d <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800878:	5d                   	pop    %ebp
  800879:	c3                   	ret    

0080087a <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80087a:	55                   	push   %ebp
  80087b:	89 e5                	mov    %esp,%ebp
  80087d:	57                   	push   %edi
  80087e:	56                   	push   %esi
  80087f:	53                   	push   %ebx
  800880:	8b 7d 08             	mov    0x8(%ebp),%edi
  800883:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800886:	85 c9                	test   %ecx,%ecx
  800888:	74 36                	je     8008c0 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80088a:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800890:	75 28                	jne    8008ba <memset+0x40>
  800892:	f6 c1 03             	test   $0x3,%cl
  800895:	75 23                	jne    8008ba <memset+0x40>
		c &= 0xFF;
  800897:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80089b:	89 d3                	mov    %edx,%ebx
  80089d:	c1 e3 08             	shl    $0x8,%ebx
  8008a0:	89 d6                	mov    %edx,%esi
  8008a2:	c1 e6 18             	shl    $0x18,%esi
  8008a5:	89 d0                	mov    %edx,%eax
  8008a7:	c1 e0 10             	shl    $0x10,%eax
  8008aa:	09 f0                	or     %esi,%eax
  8008ac:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008ae:	89 d8                	mov    %ebx,%eax
  8008b0:	09 d0                	or     %edx,%eax
  8008b2:	c1 e9 02             	shr    $0x2,%ecx
  8008b5:	fc                   	cld    
  8008b6:	f3 ab                	rep stos %eax,%es:(%edi)
  8008b8:	eb 06                	jmp    8008c0 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008ba:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008bd:	fc                   	cld    
  8008be:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008c0:	89 f8                	mov    %edi,%eax
  8008c2:	5b                   	pop    %ebx
  8008c3:	5e                   	pop    %esi
  8008c4:	5f                   	pop    %edi
  8008c5:	5d                   	pop    %ebp
  8008c6:	c3                   	ret    

008008c7 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008c7:	55                   	push   %ebp
  8008c8:	89 e5                	mov    %esp,%ebp
  8008ca:	57                   	push   %edi
  8008cb:	56                   	push   %esi
  8008cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8008cf:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008d2:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008d5:	39 c6                	cmp    %eax,%esi
  8008d7:	73 33                	jae    80090c <memmove+0x45>
  8008d9:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008dc:	39 d0                	cmp    %edx,%eax
  8008de:	73 2c                	jae    80090c <memmove+0x45>
		s += n;
		d += n;
  8008e0:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008e3:	89 d6                	mov    %edx,%esi
  8008e5:	09 fe                	or     %edi,%esi
  8008e7:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008ed:	75 13                	jne    800902 <memmove+0x3b>
  8008ef:	f6 c1 03             	test   $0x3,%cl
  8008f2:	75 0e                	jne    800902 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8008f4:	83 ef 04             	sub    $0x4,%edi
  8008f7:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008fa:	c1 e9 02             	shr    $0x2,%ecx
  8008fd:	fd                   	std    
  8008fe:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800900:	eb 07                	jmp    800909 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800902:	4f                   	dec    %edi
  800903:	8d 72 ff             	lea    -0x1(%edx),%esi
  800906:	fd                   	std    
  800907:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800909:	fc                   	cld    
  80090a:	eb 1d                	jmp    800929 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80090c:	89 f2                	mov    %esi,%edx
  80090e:	09 c2                	or     %eax,%edx
  800910:	f6 c2 03             	test   $0x3,%dl
  800913:	75 0f                	jne    800924 <memmove+0x5d>
  800915:	f6 c1 03             	test   $0x3,%cl
  800918:	75 0a                	jne    800924 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80091a:	c1 e9 02             	shr    $0x2,%ecx
  80091d:	89 c7                	mov    %eax,%edi
  80091f:	fc                   	cld    
  800920:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800922:	eb 05                	jmp    800929 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800924:	89 c7                	mov    %eax,%edi
  800926:	fc                   	cld    
  800927:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800929:	5e                   	pop    %esi
  80092a:	5f                   	pop    %edi
  80092b:	5d                   	pop    %ebp
  80092c:	c3                   	ret    

0080092d <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80092d:	55                   	push   %ebp
  80092e:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800930:	ff 75 10             	pushl  0x10(%ebp)
  800933:	ff 75 0c             	pushl  0xc(%ebp)
  800936:	ff 75 08             	pushl  0x8(%ebp)
  800939:	e8 89 ff ff ff       	call   8008c7 <memmove>
}
  80093e:	c9                   	leave  
  80093f:	c3                   	ret    

00800940 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800940:	55                   	push   %ebp
  800941:	89 e5                	mov    %esp,%ebp
  800943:	56                   	push   %esi
  800944:	53                   	push   %ebx
  800945:	8b 45 08             	mov    0x8(%ebp),%eax
  800948:	8b 55 0c             	mov    0xc(%ebp),%edx
  80094b:	89 c6                	mov    %eax,%esi
  80094d:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800950:	eb 14                	jmp    800966 <memcmp+0x26>
		if (*s1 != *s2)
  800952:	8a 08                	mov    (%eax),%cl
  800954:	8a 1a                	mov    (%edx),%bl
  800956:	38 d9                	cmp    %bl,%cl
  800958:	74 0a                	je     800964 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80095a:	0f b6 c1             	movzbl %cl,%eax
  80095d:	0f b6 db             	movzbl %bl,%ebx
  800960:	29 d8                	sub    %ebx,%eax
  800962:	eb 0b                	jmp    80096f <memcmp+0x2f>
		s1++, s2++;
  800964:	40                   	inc    %eax
  800965:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800966:	39 f0                	cmp    %esi,%eax
  800968:	75 e8                	jne    800952 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80096a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80096f:	5b                   	pop    %ebx
  800970:	5e                   	pop    %esi
  800971:	5d                   	pop    %ebp
  800972:	c3                   	ret    

00800973 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800973:	55                   	push   %ebp
  800974:	89 e5                	mov    %esp,%ebp
  800976:	53                   	push   %ebx
  800977:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80097a:	89 c1                	mov    %eax,%ecx
  80097c:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80097f:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800983:	eb 08                	jmp    80098d <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800985:	0f b6 10             	movzbl (%eax),%edx
  800988:	39 da                	cmp    %ebx,%edx
  80098a:	74 05                	je     800991 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80098c:	40                   	inc    %eax
  80098d:	39 c8                	cmp    %ecx,%eax
  80098f:	72 f4                	jb     800985 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800991:	5b                   	pop    %ebx
  800992:	5d                   	pop    %ebp
  800993:	c3                   	ret    

00800994 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800994:	55                   	push   %ebp
  800995:	89 e5                	mov    %esp,%ebp
  800997:	57                   	push   %edi
  800998:	56                   	push   %esi
  800999:	53                   	push   %ebx
  80099a:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80099d:	eb 01                	jmp    8009a0 <strtol+0xc>
		s++;
  80099f:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009a0:	8a 01                	mov    (%ecx),%al
  8009a2:	3c 20                	cmp    $0x20,%al
  8009a4:	74 f9                	je     80099f <strtol+0xb>
  8009a6:	3c 09                	cmp    $0x9,%al
  8009a8:	74 f5                	je     80099f <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009aa:	3c 2b                	cmp    $0x2b,%al
  8009ac:	75 08                	jne    8009b6 <strtol+0x22>
		s++;
  8009ae:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009af:	bf 00 00 00 00       	mov    $0x0,%edi
  8009b4:	eb 11                	jmp    8009c7 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009b6:	3c 2d                	cmp    $0x2d,%al
  8009b8:	75 08                	jne    8009c2 <strtol+0x2e>
		s++, neg = 1;
  8009ba:	41                   	inc    %ecx
  8009bb:	bf 01 00 00 00       	mov    $0x1,%edi
  8009c0:	eb 05                	jmp    8009c7 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009c2:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009c7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009cb:	0f 84 87 00 00 00    	je     800a58 <strtol+0xc4>
  8009d1:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009d5:	75 27                	jne    8009fe <strtol+0x6a>
  8009d7:	80 39 30             	cmpb   $0x30,(%ecx)
  8009da:	75 22                	jne    8009fe <strtol+0x6a>
  8009dc:	e9 88 00 00 00       	jmp    800a69 <strtol+0xd5>
		s += 2, base = 16;
  8009e1:	83 c1 02             	add    $0x2,%ecx
  8009e4:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009eb:	eb 11                	jmp    8009fe <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009ed:	41                   	inc    %ecx
  8009ee:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8009f5:	eb 07                	jmp    8009fe <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8009f7:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009fe:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a03:	8a 11                	mov    (%ecx),%dl
  800a05:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a08:	80 fb 09             	cmp    $0x9,%bl
  800a0b:	77 08                	ja     800a15 <strtol+0x81>
			dig = *s - '0';
  800a0d:	0f be d2             	movsbl %dl,%edx
  800a10:	83 ea 30             	sub    $0x30,%edx
  800a13:	eb 22                	jmp    800a37 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a15:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a18:	89 f3                	mov    %esi,%ebx
  800a1a:	80 fb 19             	cmp    $0x19,%bl
  800a1d:	77 08                	ja     800a27 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a1f:	0f be d2             	movsbl %dl,%edx
  800a22:	83 ea 57             	sub    $0x57,%edx
  800a25:	eb 10                	jmp    800a37 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a27:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a2a:	89 f3                	mov    %esi,%ebx
  800a2c:	80 fb 19             	cmp    $0x19,%bl
  800a2f:	77 14                	ja     800a45 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a31:	0f be d2             	movsbl %dl,%edx
  800a34:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a37:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a3a:	7d 09                	jge    800a45 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a3c:	41                   	inc    %ecx
  800a3d:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a41:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a43:	eb be                	jmp    800a03 <strtol+0x6f>

	if (endptr)
  800a45:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a49:	74 05                	je     800a50 <strtol+0xbc>
		*endptr = (char *) s;
  800a4b:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a4e:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a50:	85 ff                	test   %edi,%edi
  800a52:	74 21                	je     800a75 <strtol+0xe1>
  800a54:	f7 d8                	neg    %eax
  800a56:	eb 1d                	jmp    800a75 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a58:	80 39 30             	cmpb   $0x30,(%ecx)
  800a5b:	75 9a                	jne    8009f7 <strtol+0x63>
  800a5d:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a61:	0f 84 7a ff ff ff    	je     8009e1 <strtol+0x4d>
  800a67:	eb 84                	jmp    8009ed <strtol+0x59>
  800a69:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a6d:	0f 84 6e ff ff ff    	je     8009e1 <strtol+0x4d>
  800a73:	eb 89                	jmp    8009fe <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a75:	5b                   	pop    %ebx
  800a76:	5e                   	pop    %esi
  800a77:	5f                   	pop    %edi
  800a78:	5d                   	pop    %ebp
  800a79:	c3                   	ret    

00800a7a <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a7a:	55                   	push   %ebp
  800a7b:	89 e5                	mov    %esp,%ebp
  800a7d:	57                   	push   %edi
  800a7e:	56                   	push   %esi
  800a7f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a80:	b8 00 00 00 00       	mov    $0x0,%eax
  800a85:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a88:	8b 55 08             	mov    0x8(%ebp),%edx
  800a8b:	89 c3                	mov    %eax,%ebx
  800a8d:	89 c7                	mov    %eax,%edi
  800a8f:	89 c6                	mov    %eax,%esi
  800a91:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a93:	5b                   	pop    %ebx
  800a94:	5e                   	pop    %esi
  800a95:	5f                   	pop    %edi
  800a96:	5d                   	pop    %ebp
  800a97:	c3                   	ret    

00800a98 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a98:	55                   	push   %ebp
  800a99:	89 e5                	mov    %esp,%ebp
  800a9b:	57                   	push   %edi
  800a9c:	56                   	push   %esi
  800a9d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a9e:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa3:	b8 01 00 00 00       	mov    $0x1,%eax
  800aa8:	89 d1                	mov    %edx,%ecx
  800aaa:	89 d3                	mov    %edx,%ebx
  800aac:	89 d7                	mov    %edx,%edi
  800aae:	89 d6                	mov    %edx,%esi
  800ab0:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800ab2:	5b                   	pop    %ebx
  800ab3:	5e                   	pop    %esi
  800ab4:	5f                   	pop    %edi
  800ab5:	5d                   	pop    %ebp
  800ab6:	c3                   	ret    

00800ab7 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800ab7:	55                   	push   %ebp
  800ab8:	89 e5                	mov    %esp,%ebp
  800aba:	57                   	push   %edi
  800abb:	56                   	push   %esi
  800abc:	53                   	push   %ebx
  800abd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac0:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ac5:	b8 03 00 00 00       	mov    $0x3,%eax
  800aca:	8b 55 08             	mov    0x8(%ebp),%edx
  800acd:	89 cb                	mov    %ecx,%ebx
  800acf:	89 cf                	mov    %ecx,%edi
  800ad1:	89 ce                	mov    %ecx,%esi
  800ad3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ad5:	85 c0                	test   %eax,%eax
  800ad7:	7e 17                	jle    800af0 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ad9:	83 ec 0c             	sub    $0xc,%esp
  800adc:	50                   	push   %eax
  800add:	6a 03                	push   $0x3
  800adf:	68 df 13 80 00       	push   $0x8013df
  800ae4:	6a 23                	push   $0x23
  800ae6:	68 fc 13 80 00       	push   $0x8013fc
  800aeb:	e8 26 f6 ff ff       	call   800116 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800af0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800af3:	5b                   	pop    %ebx
  800af4:	5e                   	pop    %esi
  800af5:	5f                   	pop    %edi
  800af6:	5d                   	pop    %ebp
  800af7:	c3                   	ret    

00800af8 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800af8:	55                   	push   %ebp
  800af9:	89 e5                	mov    %esp,%ebp
  800afb:	57                   	push   %edi
  800afc:	56                   	push   %esi
  800afd:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800afe:	ba 00 00 00 00       	mov    $0x0,%edx
  800b03:	b8 02 00 00 00       	mov    $0x2,%eax
  800b08:	89 d1                	mov    %edx,%ecx
  800b0a:	89 d3                	mov    %edx,%ebx
  800b0c:	89 d7                	mov    %edx,%edi
  800b0e:	89 d6                	mov    %edx,%esi
  800b10:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b12:	5b                   	pop    %ebx
  800b13:	5e                   	pop    %esi
  800b14:	5f                   	pop    %edi
  800b15:	5d                   	pop    %ebp
  800b16:	c3                   	ret    

00800b17 <sys_yield>:

void
sys_yield(void)
{
  800b17:	55                   	push   %ebp
  800b18:	89 e5                	mov    %esp,%ebp
  800b1a:	57                   	push   %edi
  800b1b:	56                   	push   %esi
  800b1c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1d:	ba 00 00 00 00       	mov    $0x0,%edx
  800b22:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b27:	89 d1                	mov    %edx,%ecx
  800b29:	89 d3                	mov    %edx,%ebx
  800b2b:	89 d7                	mov    %edx,%edi
  800b2d:	89 d6                	mov    %edx,%esi
  800b2f:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b31:	5b                   	pop    %ebx
  800b32:	5e                   	pop    %esi
  800b33:	5f                   	pop    %edi
  800b34:	5d                   	pop    %ebp
  800b35:	c3                   	ret    

00800b36 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b36:	55                   	push   %ebp
  800b37:	89 e5                	mov    %esp,%ebp
  800b39:	57                   	push   %edi
  800b3a:	56                   	push   %esi
  800b3b:	53                   	push   %ebx
  800b3c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b3f:	be 00 00 00 00       	mov    $0x0,%esi
  800b44:	b8 04 00 00 00       	mov    $0x4,%eax
  800b49:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b4c:	8b 55 08             	mov    0x8(%ebp),%edx
  800b4f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b52:	89 f7                	mov    %esi,%edi
  800b54:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b56:	85 c0                	test   %eax,%eax
  800b58:	7e 17                	jle    800b71 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b5a:	83 ec 0c             	sub    $0xc,%esp
  800b5d:	50                   	push   %eax
  800b5e:	6a 04                	push   $0x4
  800b60:	68 df 13 80 00       	push   $0x8013df
  800b65:	6a 23                	push   $0x23
  800b67:	68 fc 13 80 00       	push   $0x8013fc
  800b6c:	e8 a5 f5 ff ff       	call   800116 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b71:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b74:	5b                   	pop    %ebx
  800b75:	5e                   	pop    %esi
  800b76:	5f                   	pop    %edi
  800b77:	5d                   	pop    %ebp
  800b78:	c3                   	ret    

00800b79 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b79:	55                   	push   %ebp
  800b7a:	89 e5                	mov    %esp,%ebp
  800b7c:	57                   	push   %edi
  800b7d:	56                   	push   %esi
  800b7e:	53                   	push   %ebx
  800b7f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b82:	b8 05 00 00 00       	mov    $0x5,%eax
  800b87:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b8a:	8b 55 08             	mov    0x8(%ebp),%edx
  800b8d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b90:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b93:	8b 75 18             	mov    0x18(%ebp),%esi
  800b96:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b98:	85 c0                	test   %eax,%eax
  800b9a:	7e 17                	jle    800bb3 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b9c:	83 ec 0c             	sub    $0xc,%esp
  800b9f:	50                   	push   %eax
  800ba0:	6a 05                	push   $0x5
  800ba2:	68 df 13 80 00       	push   $0x8013df
  800ba7:	6a 23                	push   $0x23
  800ba9:	68 fc 13 80 00       	push   $0x8013fc
  800bae:	e8 63 f5 ff ff       	call   800116 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bb3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bb6:	5b                   	pop    %ebx
  800bb7:	5e                   	pop    %esi
  800bb8:	5f                   	pop    %edi
  800bb9:	5d                   	pop    %ebp
  800bba:	c3                   	ret    

00800bbb <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bbb:	55                   	push   %ebp
  800bbc:	89 e5                	mov    %esp,%ebp
  800bbe:	57                   	push   %edi
  800bbf:	56                   	push   %esi
  800bc0:	53                   	push   %ebx
  800bc1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc4:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bc9:	b8 06 00 00 00       	mov    $0x6,%eax
  800bce:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bd1:	8b 55 08             	mov    0x8(%ebp),%edx
  800bd4:	89 df                	mov    %ebx,%edi
  800bd6:	89 de                	mov    %ebx,%esi
  800bd8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bda:	85 c0                	test   %eax,%eax
  800bdc:	7e 17                	jle    800bf5 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bde:	83 ec 0c             	sub    $0xc,%esp
  800be1:	50                   	push   %eax
  800be2:	6a 06                	push   $0x6
  800be4:	68 df 13 80 00       	push   $0x8013df
  800be9:	6a 23                	push   $0x23
  800beb:	68 fc 13 80 00       	push   $0x8013fc
  800bf0:	e8 21 f5 ff ff       	call   800116 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800bf5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf8:	5b                   	pop    %ebx
  800bf9:	5e                   	pop    %esi
  800bfa:	5f                   	pop    %edi
  800bfb:	5d                   	pop    %ebp
  800bfc:	c3                   	ret    

00800bfd <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800bfd:	55                   	push   %ebp
  800bfe:	89 e5                	mov    %esp,%ebp
  800c00:	57                   	push   %edi
  800c01:	56                   	push   %esi
  800c02:	53                   	push   %ebx
  800c03:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c06:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c0b:	b8 08 00 00 00       	mov    $0x8,%eax
  800c10:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c13:	8b 55 08             	mov    0x8(%ebp),%edx
  800c16:	89 df                	mov    %ebx,%edi
  800c18:	89 de                	mov    %ebx,%esi
  800c1a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c1c:	85 c0                	test   %eax,%eax
  800c1e:	7e 17                	jle    800c37 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c20:	83 ec 0c             	sub    $0xc,%esp
  800c23:	50                   	push   %eax
  800c24:	6a 08                	push   $0x8
  800c26:	68 df 13 80 00       	push   $0x8013df
  800c2b:	6a 23                	push   $0x23
  800c2d:	68 fc 13 80 00       	push   $0x8013fc
  800c32:	e8 df f4 ff ff       	call   800116 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c37:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c3a:	5b                   	pop    %ebx
  800c3b:	5e                   	pop    %esi
  800c3c:	5f                   	pop    %edi
  800c3d:	5d                   	pop    %ebp
  800c3e:	c3                   	ret    

00800c3f <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c3f:	55                   	push   %ebp
  800c40:	89 e5                	mov    %esp,%ebp
  800c42:	57                   	push   %edi
  800c43:	56                   	push   %esi
  800c44:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c45:	ba 00 00 00 00       	mov    $0x0,%edx
  800c4a:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c4f:	89 d1                	mov    %edx,%ecx
  800c51:	89 d3                	mov    %edx,%ebx
  800c53:	89 d7                	mov    %edx,%edi
  800c55:	89 d6                	mov    %edx,%esi
  800c57:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c59:	5b                   	pop    %ebx
  800c5a:	5e                   	pop    %esi
  800c5b:	5f                   	pop    %edi
  800c5c:	5d                   	pop    %ebp
  800c5d:	c3                   	ret    

00800c5e <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c5e:	55                   	push   %ebp
  800c5f:	89 e5                	mov    %esp,%ebp
  800c61:	57                   	push   %edi
  800c62:	56                   	push   %esi
  800c63:	53                   	push   %ebx
  800c64:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c67:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c6c:	b8 09 00 00 00       	mov    $0x9,%eax
  800c71:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c74:	8b 55 08             	mov    0x8(%ebp),%edx
  800c77:	89 df                	mov    %ebx,%edi
  800c79:	89 de                	mov    %ebx,%esi
  800c7b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c7d:	85 c0                	test   %eax,%eax
  800c7f:	7e 17                	jle    800c98 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c81:	83 ec 0c             	sub    $0xc,%esp
  800c84:	50                   	push   %eax
  800c85:	6a 09                	push   $0x9
  800c87:	68 df 13 80 00       	push   $0x8013df
  800c8c:	6a 23                	push   $0x23
  800c8e:	68 fc 13 80 00       	push   $0x8013fc
  800c93:	e8 7e f4 ff ff       	call   800116 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c98:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c9b:	5b                   	pop    %ebx
  800c9c:	5e                   	pop    %esi
  800c9d:	5f                   	pop    %edi
  800c9e:	5d                   	pop    %ebp
  800c9f:	c3                   	ret    

00800ca0 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800ca0:	55                   	push   %ebp
  800ca1:	89 e5                	mov    %esp,%ebp
  800ca3:	57                   	push   %edi
  800ca4:	56                   	push   %esi
  800ca5:	53                   	push   %ebx
  800ca6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ca9:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cae:	b8 0a 00 00 00       	mov    $0xa,%eax
  800cb3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cb6:	8b 55 08             	mov    0x8(%ebp),%edx
  800cb9:	89 df                	mov    %ebx,%edi
  800cbb:	89 de                	mov    %ebx,%esi
  800cbd:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cbf:	85 c0                	test   %eax,%eax
  800cc1:	7e 17                	jle    800cda <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cc3:	83 ec 0c             	sub    $0xc,%esp
  800cc6:	50                   	push   %eax
  800cc7:	6a 0a                	push   $0xa
  800cc9:	68 df 13 80 00       	push   $0x8013df
  800cce:	6a 23                	push   $0x23
  800cd0:	68 fc 13 80 00       	push   $0x8013fc
  800cd5:	e8 3c f4 ff ff       	call   800116 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cda:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cdd:	5b                   	pop    %ebx
  800cde:	5e                   	pop    %esi
  800cdf:	5f                   	pop    %edi
  800ce0:	5d                   	pop    %ebp
  800ce1:	c3                   	ret    

00800ce2 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800ce2:	55                   	push   %ebp
  800ce3:	89 e5                	mov    %esp,%ebp
  800ce5:	57                   	push   %edi
  800ce6:	56                   	push   %esi
  800ce7:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ce8:	be 00 00 00 00       	mov    $0x0,%esi
  800ced:	b8 0d 00 00 00       	mov    $0xd,%eax
  800cf2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cf5:	8b 55 08             	mov    0x8(%ebp),%edx
  800cf8:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cfb:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cfe:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d00:	5b                   	pop    %ebx
  800d01:	5e                   	pop    %esi
  800d02:	5f                   	pop    %edi
  800d03:	5d                   	pop    %ebp
  800d04:	c3                   	ret    

00800d05 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d05:	55                   	push   %ebp
  800d06:	89 e5                	mov    %esp,%ebp
  800d08:	57                   	push   %edi
  800d09:	56                   	push   %esi
  800d0a:	53                   	push   %ebx
  800d0b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d0e:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d13:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d18:	8b 55 08             	mov    0x8(%ebp),%edx
  800d1b:	89 cb                	mov    %ecx,%ebx
  800d1d:	89 cf                	mov    %ecx,%edi
  800d1f:	89 ce                	mov    %ecx,%esi
  800d21:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d23:	85 c0                	test   %eax,%eax
  800d25:	7e 17                	jle    800d3e <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d27:	83 ec 0c             	sub    $0xc,%esp
  800d2a:	50                   	push   %eax
  800d2b:	6a 0e                	push   $0xe
  800d2d:	68 df 13 80 00       	push   $0x8013df
  800d32:	6a 23                	push   $0x23
  800d34:	68 fc 13 80 00       	push   $0x8013fc
  800d39:	e8 d8 f3 ff ff       	call   800116 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d3e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d41:	5b                   	pop    %ebx
  800d42:	5e                   	pop    %esi
  800d43:	5f                   	pop    %edi
  800d44:	5d                   	pop    %ebp
  800d45:	c3                   	ret    

00800d46 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  800d46:	55                   	push   %ebp
  800d47:	89 e5                	mov    %esp,%ebp
  800d49:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  800d4c:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  800d53:	75 3e                	jne    800d93 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  800d55:	83 ec 04             	sub    $0x4,%esp
  800d58:	6a 07                	push   $0x7
  800d5a:	68 00 f0 bf ee       	push   $0xeebff000
  800d5f:	6a 00                	push   $0x0
  800d61:	e8 d0 fd ff ff       	call   800b36 <sys_page_alloc>
  800d66:	83 c4 10             	add    $0x10,%esp
  800d69:	85 c0                	test   %eax,%eax
  800d6b:	74 14                	je     800d81 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  800d6d:	83 ec 04             	sub    $0x4,%esp
  800d70:	68 0c 14 80 00       	push   $0x80140c
  800d75:	6a 23                	push   $0x23
  800d77:	68 30 14 80 00       	push   $0x801430
  800d7c:	e8 95 f3 ff ff       	call   800116 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  800d81:	83 ec 08             	sub    $0x8,%esp
  800d84:	68 9d 0d 80 00       	push   $0x800d9d
  800d89:	6a 00                	push   $0x0
  800d8b:	e8 10 ff ff ff       	call   800ca0 <sys_env_set_pgfault_upcall>
  800d90:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  800d93:	8b 45 08             	mov    0x8(%ebp),%eax
  800d96:	a3 08 20 80 00       	mov    %eax,0x802008
}
  800d9b:	c9                   	leave  
  800d9c:	c3                   	ret    

00800d9d <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  800d9d:	54                   	push   %esp
	movl _pgfault_handler, %eax
  800d9e:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  800da3:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  800da5:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  800da8:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  800daa:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  800dae:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  800db2:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  800db3:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  800db6:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  800db8:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  800dbb:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  800dbe:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  800dbf:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  800dc2:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  800dc3:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  800dc4:	c3                   	ret    
  800dc5:	66 90                	xchg   %ax,%ax
  800dc7:	90                   	nop

00800dc8 <__udivdi3>:
  800dc8:	55                   	push   %ebp
  800dc9:	57                   	push   %edi
  800dca:	56                   	push   %esi
  800dcb:	53                   	push   %ebx
  800dcc:	83 ec 1c             	sub    $0x1c,%esp
  800dcf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800dd3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800dd7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ddb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ddf:	89 ca                	mov    %ecx,%edx
  800de1:	89 f8                	mov    %edi,%eax
  800de3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800de7:	85 f6                	test   %esi,%esi
  800de9:	75 2d                	jne    800e18 <__udivdi3+0x50>
  800deb:	39 cf                	cmp    %ecx,%edi
  800ded:	77 65                	ja     800e54 <__udivdi3+0x8c>
  800def:	89 fd                	mov    %edi,%ebp
  800df1:	85 ff                	test   %edi,%edi
  800df3:	75 0b                	jne    800e00 <__udivdi3+0x38>
  800df5:	b8 01 00 00 00       	mov    $0x1,%eax
  800dfa:	31 d2                	xor    %edx,%edx
  800dfc:	f7 f7                	div    %edi
  800dfe:	89 c5                	mov    %eax,%ebp
  800e00:	31 d2                	xor    %edx,%edx
  800e02:	89 c8                	mov    %ecx,%eax
  800e04:	f7 f5                	div    %ebp
  800e06:	89 c1                	mov    %eax,%ecx
  800e08:	89 d8                	mov    %ebx,%eax
  800e0a:	f7 f5                	div    %ebp
  800e0c:	89 cf                	mov    %ecx,%edi
  800e0e:	89 fa                	mov    %edi,%edx
  800e10:	83 c4 1c             	add    $0x1c,%esp
  800e13:	5b                   	pop    %ebx
  800e14:	5e                   	pop    %esi
  800e15:	5f                   	pop    %edi
  800e16:	5d                   	pop    %ebp
  800e17:	c3                   	ret    
  800e18:	39 ce                	cmp    %ecx,%esi
  800e1a:	77 28                	ja     800e44 <__udivdi3+0x7c>
  800e1c:	0f bd fe             	bsr    %esi,%edi
  800e1f:	83 f7 1f             	xor    $0x1f,%edi
  800e22:	75 40                	jne    800e64 <__udivdi3+0x9c>
  800e24:	39 ce                	cmp    %ecx,%esi
  800e26:	72 0a                	jb     800e32 <__udivdi3+0x6a>
  800e28:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800e2c:	0f 87 9e 00 00 00    	ja     800ed0 <__udivdi3+0x108>
  800e32:	b8 01 00 00 00       	mov    $0x1,%eax
  800e37:	89 fa                	mov    %edi,%edx
  800e39:	83 c4 1c             	add    $0x1c,%esp
  800e3c:	5b                   	pop    %ebx
  800e3d:	5e                   	pop    %esi
  800e3e:	5f                   	pop    %edi
  800e3f:	5d                   	pop    %ebp
  800e40:	c3                   	ret    
  800e41:	8d 76 00             	lea    0x0(%esi),%esi
  800e44:	31 ff                	xor    %edi,%edi
  800e46:	31 c0                	xor    %eax,%eax
  800e48:	89 fa                	mov    %edi,%edx
  800e4a:	83 c4 1c             	add    $0x1c,%esp
  800e4d:	5b                   	pop    %ebx
  800e4e:	5e                   	pop    %esi
  800e4f:	5f                   	pop    %edi
  800e50:	5d                   	pop    %ebp
  800e51:	c3                   	ret    
  800e52:	66 90                	xchg   %ax,%ax
  800e54:	89 d8                	mov    %ebx,%eax
  800e56:	f7 f7                	div    %edi
  800e58:	31 ff                	xor    %edi,%edi
  800e5a:	89 fa                	mov    %edi,%edx
  800e5c:	83 c4 1c             	add    $0x1c,%esp
  800e5f:	5b                   	pop    %ebx
  800e60:	5e                   	pop    %esi
  800e61:	5f                   	pop    %edi
  800e62:	5d                   	pop    %ebp
  800e63:	c3                   	ret    
  800e64:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e69:	89 eb                	mov    %ebp,%ebx
  800e6b:	29 fb                	sub    %edi,%ebx
  800e6d:	89 f9                	mov    %edi,%ecx
  800e6f:	d3 e6                	shl    %cl,%esi
  800e71:	89 c5                	mov    %eax,%ebp
  800e73:	88 d9                	mov    %bl,%cl
  800e75:	d3 ed                	shr    %cl,%ebp
  800e77:	89 e9                	mov    %ebp,%ecx
  800e79:	09 f1                	or     %esi,%ecx
  800e7b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800e7f:	89 f9                	mov    %edi,%ecx
  800e81:	d3 e0                	shl    %cl,%eax
  800e83:	89 c5                	mov    %eax,%ebp
  800e85:	89 d6                	mov    %edx,%esi
  800e87:	88 d9                	mov    %bl,%cl
  800e89:	d3 ee                	shr    %cl,%esi
  800e8b:	89 f9                	mov    %edi,%ecx
  800e8d:	d3 e2                	shl    %cl,%edx
  800e8f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e93:	88 d9                	mov    %bl,%cl
  800e95:	d3 e8                	shr    %cl,%eax
  800e97:	09 c2                	or     %eax,%edx
  800e99:	89 d0                	mov    %edx,%eax
  800e9b:	89 f2                	mov    %esi,%edx
  800e9d:	f7 74 24 0c          	divl   0xc(%esp)
  800ea1:	89 d6                	mov    %edx,%esi
  800ea3:	89 c3                	mov    %eax,%ebx
  800ea5:	f7 e5                	mul    %ebp
  800ea7:	39 d6                	cmp    %edx,%esi
  800ea9:	72 19                	jb     800ec4 <__udivdi3+0xfc>
  800eab:	74 0b                	je     800eb8 <__udivdi3+0xf0>
  800ead:	89 d8                	mov    %ebx,%eax
  800eaf:	31 ff                	xor    %edi,%edi
  800eb1:	e9 58 ff ff ff       	jmp    800e0e <__udivdi3+0x46>
  800eb6:	66 90                	xchg   %ax,%ax
  800eb8:	8b 54 24 08          	mov    0x8(%esp),%edx
  800ebc:	89 f9                	mov    %edi,%ecx
  800ebe:	d3 e2                	shl    %cl,%edx
  800ec0:	39 c2                	cmp    %eax,%edx
  800ec2:	73 e9                	jae    800ead <__udivdi3+0xe5>
  800ec4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ec7:	31 ff                	xor    %edi,%edi
  800ec9:	e9 40 ff ff ff       	jmp    800e0e <__udivdi3+0x46>
  800ece:	66 90                	xchg   %ax,%ax
  800ed0:	31 c0                	xor    %eax,%eax
  800ed2:	e9 37 ff ff ff       	jmp    800e0e <__udivdi3+0x46>
  800ed7:	90                   	nop

00800ed8 <__umoddi3>:
  800ed8:	55                   	push   %ebp
  800ed9:	57                   	push   %edi
  800eda:	56                   	push   %esi
  800edb:	53                   	push   %ebx
  800edc:	83 ec 1c             	sub    $0x1c,%esp
  800edf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800ee3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800ee7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800eeb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800eef:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800ef3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800ef7:	89 f3                	mov    %esi,%ebx
  800ef9:	89 fa                	mov    %edi,%edx
  800efb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800eff:	89 34 24             	mov    %esi,(%esp)
  800f02:	85 c0                	test   %eax,%eax
  800f04:	75 1a                	jne    800f20 <__umoddi3+0x48>
  800f06:	39 f7                	cmp    %esi,%edi
  800f08:	0f 86 a2 00 00 00    	jbe    800fb0 <__umoddi3+0xd8>
  800f0e:	89 c8                	mov    %ecx,%eax
  800f10:	89 f2                	mov    %esi,%edx
  800f12:	f7 f7                	div    %edi
  800f14:	89 d0                	mov    %edx,%eax
  800f16:	31 d2                	xor    %edx,%edx
  800f18:	83 c4 1c             	add    $0x1c,%esp
  800f1b:	5b                   	pop    %ebx
  800f1c:	5e                   	pop    %esi
  800f1d:	5f                   	pop    %edi
  800f1e:	5d                   	pop    %ebp
  800f1f:	c3                   	ret    
  800f20:	39 f0                	cmp    %esi,%eax
  800f22:	0f 87 ac 00 00 00    	ja     800fd4 <__umoddi3+0xfc>
  800f28:	0f bd e8             	bsr    %eax,%ebp
  800f2b:	83 f5 1f             	xor    $0x1f,%ebp
  800f2e:	0f 84 ac 00 00 00    	je     800fe0 <__umoddi3+0x108>
  800f34:	bf 20 00 00 00       	mov    $0x20,%edi
  800f39:	29 ef                	sub    %ebp,%edi
  800f3b:	89 fe                	mov    %edi,%esi
  800f3d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800f41:	89 e9                	mov    %ebp,%ecx
  800f43:	d3 e0                	shl    %cl,%eax
  800f45:	89 d7                	mov    %edx,%edi
  800f47:	89 f1                	mov    %esi,%ecx
  800f49:	d3 ef                	shr    %cl,%edi
  800f4b:	09 c7                	or     %eax,%edi
  800f4d:	89 e9                	mov    %ebp,%ecx
  800f4f:	d3 e2                	shl    %cl,%edx
  800f51:	89 14 24             	mov    %edx,(%esp)
  800f54:	89 d8                	mov    %ebx,%eax
  800f56:	d3 e0                	shl    %cl,%eax
  800f58:	89 c2                	mov    %eax,%edx
  800f5a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f5e:	d3 e0                	shl    %cl,%eax
  800f60:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f64:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f68:	89 f1                	mov    %esi,%ecx
  800f6a:	d3 e8                	shr    %cl,%eax
  800f6c:	09 d0                	or     %edx,%eax
  800f6e:	d3 eb                	shr    %cl,%ebx
  800f70:	89 da                	mov    %ebx,%edx
  800f72:	f7 f7                	div    %edi
  800f74:	89 d3                	mov    %edx,%ebx
  800f76:	f7 24 24             	mull   (%esp)
  800f79:	89 c6                	mov    %eax,%esi
  800f7b:	89 d1                	mov    %edx,%ecx
  800f7d:	39 d3                	cmp    %edx,%ebx
  800f7f:	0f 82 87 00 00 00    	jb     80100c <__umoddi3+0x134>
  800f85:	0f 84 91 00 00 00    	je     80101c <__umoddi3+0x144>
  800f8b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800f8f:	29 f2                	sub    %esi,%edx
  800f91:	19 cb                	sbb    %ecx,%ebx
  800f93:	89 d8                	mov    %ebx,%eax
  800f95:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800f99:	d3 e0                	shl    %cl,%eax
  800f9b:	89 e9                	mov    %ebp,%ecx
  800f9d:	d3 ea                	shr    %cl,%edx
  800f9f:	09 d0                	or     %edx,%eax
  800fa1:	89 e9                	mov    %ebp,%ecx
  800fa3:	d3 eb                	shr    %cl,%ebx
  800fa5:	89 da                	mov    %ebx,%edx
  800fa7:	83 c4 1c             	add    $0x1c,%esp
  800faa:	5b                   	pop    %ebx
  800fab:	5e                   	pop    %esi
  800fac:	5f                   	pop    %edi
  800fad:	5d                   	pop    %ebp
  800fae:	c3                   	ret    
  800faf:	90                   	nop
  800fb0:	89 fd                	mov    %edi,%ebp
  800fb2:	85 ff                	test   %edi,%edi
  800fb4:	75 0b                	jne    800fc1 <__umoddi3+0xe9>
  800fb6:	b8 01 00 00 00       	mov    $0x1,%eax
  800fbb:	31 d2                	xor    %edx,%edx
  800fbd:	f7 f7                	div    %edi
  800fbf:	89 c5                	mov    %eax,%ebp
  800fc1:	89 f0                	mov    %esi,%eax
  800fc3:	31 d2                	xor    %edx,%edx
  800fc5:	f7 f5                	div    %ebp
  800fc7:	89 c8                	mov    %ecx,%eax
  800fc9:	f7 f5                	div    %ebp
  800fcb:	89 d0                	mov    %edx,%eax
  800fcd:	e9 44 ff ff ff       	jmp    800f16 <__umoddi3+0x3e>
  800fd2:	66 90                	xchg   %ax,%ax
  800fd4:	89 c8                	mov    %ecx,%eax
  800fd6:	89 f2                	mov    %esi,%edx
  800fd8:	83 c4 1c             	add    $0x1c,%esp
  800fdb:	5b                   	pop    %ebx
  800fdc:	5e                   	pop    %esi
  800fdd:	5f                   	pop    %edi
  800fde:	5d                   	pop    %ebp
  800fdf:	c3                   	ret    
  800fe0:	3b 04 24             	cmp    (%esp),%eax
  800fe3:	72 06                	jb     800feb <__umoddi3+0x113>
  800fe5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800fe9:	77 0f                	ja     800ffa <__umoddi3+0x122>
  800feb:	89 f2                	mov    %esi,%edx
  800fed:	29 f9                	sub    %edi,%ecx
  800fef:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ff3:	89 14 24             	mov    %edx,(%esp)
  800ff6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ffa:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ffe:	8b 14 24             	mov    (%esp),%edx
  801001:	83 c4 1c             	add    $0x1c,%esp
  801004:	5b                   	pop    %ebx
  801005:	5e                   	pop    %esi
  801006:	5f                   	pop    %edi
  801007:	5d                   	pop    %ebp
  801008:	c3                   	ret    
  801009:	8d 76 00             	lea    0x0(%esi),%esi
  80100c:	2b 04 24             	sub    (%esp),%eax
  80100f:	19 fa                	sbb    %edi,%edx
  801011:	89 d1                	mov    %edx,%ecx
  801013:	89 c6                	mov    %eax,%esi
  801015:	e9 71 ff ff ff       	jmp    800f8b <__umoddi3+0xb3>
  80101a:	66 90                	xchg   %ax,%ax
  80101c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801020:	72 ea                	jb     80100c <__umoddi3+0x134>
  801022:	89 d9                	mov    %ebx,%ecx
  801024:	e9 62 ff ff ff       	jmp    800f8b <__umoddi3+0xb3>
