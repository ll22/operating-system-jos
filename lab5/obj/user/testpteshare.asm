
obj/user/testpteshare.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 4f 01 00 00       	call   800180 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <childofspawn>:
	breakpoint();
}

void
childofspawn(void)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	strcpy(VA, msg2);
  800039:	ff 35 00 30 80 00    	pushl  0x803000
  80003f:	68 00 00 00 a0       	push   $0xa0000000
  800044:	e8 d4 07 00 00       	call   80081d <strcpy>
	exit();
  800049:	e8 81 01 00 00       	call   8001cf <exit>
}
  80004e:	83 c4 10             	add    $0x10,%esp
  800051:	c9                   	leave  
  800052:	c3                   	ret    

00800053 <umain>:

void childofspawn(void);

void
umain(int argc, char **argv)
{
  800053:	55                   	push   %ebp
  800054:	89 e5                	mov    %esp,%ebp
  800056:	53                   	push   %ebx
  800057:	83 ec 04             	sub    $0x4,%esp
	int r;

	if (argc != 0)
  80005a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  80005e:	74 05                	je     800065 <umain+0x12>
		childofspawn();
  800060:	e8 ce ff ff ff       	call   800033 <childofspawn>

	if ((r = sys_page_alloc(0, VA, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  800065:	83 ec 04             	sub    $0x4,%esp
  800068:	68 07 04 00 00       	push   $0x407
  80006d:	68 00 00 00 a0       	push   $0xa0000000
  800072:	6a 00                	push   $0x0
  800074:	e8 88 0b 00 00       	call   800c01 <sys_page_alloc>
  800079:	83 c4 10             	add    $0x10,%esp
  80007c:	85 c0                	test   %eax,%eax
  80007e:	79 12                	jns    800092 <umain+0x3f>
		panic("sys_page_alloc: %e", r);
  800080:	50                   	push   %eax
  800081:	68 0c 28 80 00       	push   $0x80280c
  800086:	6a 13                	push   $0x13
  800088:	68 1f 28 80 00       	push   $0x80281f
  80008d:	e8 4f 01 00 00       	call   8001e1 <_panic>

	// check fork
	if ((r = fork()) < 0)
  800092:	e8 2c 0e 00 00       	call   800ec3 <fork>
  800097:	89 c3                	mov    %eax,%ebx
  800099:	85 c0                	test   %eax,%eax
  80009b:	79 12                	jns    8000af <umain+0x5c>
		panic("fork: %e", r);
  80009d:	50                   	push   %eax
  80009e:	68 33 28 80 00       	push   $0x802833
  8000a3:	6a 17                	push   $0x17
  8000a5:	68 1f 28 80 00       	push   $0x80281f
  8000aa:	e8 32 01 00 00       	call   8001e1 <_panic>
	if (r == 0) {
  8000af:	85 c0                	test   %eax,%eax
  8000b1:	75 1b                	jne    8000ce <umain+0x7b>
		strcpy(VA, msg);
  8000b3:	83 ec 08             	sub    $0x8,%esp
  8000b6:	ff 35 04 30 80 00    	pushl  0x803004
  8000bc:	68 00 00 00 a0       	push   $0xa0000000
  8000c1:	e8 57 07 00 00       	call   80081d <strcpy>
		exit();
  8000c6:	e8 04 01 00 00       	call   8001cf <exit>
  8000cb:	83 c4 10             	add    $0x10,%esp
	}
	wait(r);
  8000ce:	83 ec 0c             	sub    $0xc,%esp
  8000d1:	53                   	push   %ebx
  8000d2:	e8 f7 15 00 00       	call   8016ce <wait>
	cprintf("fork handles PTE_SHARE %s\n", strcmp(VA, msg) == 0 ? "right" : "wrong");
  8000d7:	83 c4 08             	add    $0x8,%esp
  8000da:	ff 35 04 30 80 00    	pushl  0x803004
  8000e0:	68 00 00 00 a0       	push   $0xa0000000
  8000e5:	e8 d2 07 00 00       	call   8008bc <strcmp>
  8000ea:	83 c4 10             	add    $0x10,%esp
  8000ed:	85 c0                	test   %eax,%eax
  8000ef:	74 07                	je     8000f8 <umain+0xa5>
  8000f1:	b8 06 28 80 00       	mov    $0x802806,%eax
  8000f6:	eb 05                	jmp    8000fd <umain+0xaa>
  8000f8:	b8 00 28 80 00       	mov    $0x802800,%eax
  8000fd:	83 ec 08             	sub    $0x8,%esp
  800100:	50                   	push   %eax
  800101:	68 3c 28 80 00       	push   $0x80283c
  800106:	e8 ae 01 00 00       	call   8002b9 <cprintf>

	// check spawn
	if ((r = spawnl("/testpteshare", "testpteshare", "arg", 0)) < 0)
  80010b:	6a 00                	push   $0x0
  80010d:	68 57 28 80 00       	push   $0x802857
  800112:	68 5c 28 80 00       	push   $0x80285c
  800117:	68 5b 28 80 00       	push   $0x80285b
  80011c:	e8 3e 15 00 00       	call   80165f <spawnl>
  800121:	83 c4 20             	add    $0x20,%esp
  800124:	85 c0                	test   %eax,%eax
  800126:	79 12                	jns    80013a <umain+0xe7>
		panic("spawn: %e", r);
  800128:	50                   	push   %eax
  800129:	68 69 28 80 00       	push   $0x802869
  80012e:	6a 21                	push   $0x21
  800130:	68 1f 28 80 00       	push   $0x80281f
  800135:	e8 a7 00 00 00       	call   8001e1 <_panic>
	wait(r);
  80013a:	83 ec 0c             	sub    $0xc,%esp
  80013d:	50                   	push   %eax
  80013e:	e8 8b 15 00 00       	call   8016ce <wait>
	cprintf("spawn handles PTE_SHARE %s\n", strcmp(VA, msg2) == 0 ? "right" : "wrong");
  800143:	83 c4 08             	add    $0x8,%esp
  800146:	ff 35 00 30 80 00    	pushl  0x803000
  80014c:	68 00 00 00 a0       	push   $0xa0000000
  800151:	e8 66 07 00 00       	call   8008bc <strcmp>
  800156:	83 c4 10             	add    $0x10,%esp
  800159:	85 c0                	test   %eax,%eax
  80015b:	74 07                	je     800164 <umain+0x111>
  80015d:	b8 06 28 80 00       	mov    $0x802806,%eax
  800162:	eb 05                	jmp    800169 <umain+0x116>
  800164:	b8 00 28 80 00       	mov    $0x802800,%eax
  800169:	83 ec 08             	sub    $0x8,%esp
  80016c:	50                   	push   %eax
  80016d:	68 73 28 80 00       	push   $0x802873
  800172:	e8 42 01 00 00       	call   8002b9 <cprintf>
#include <inc/types.h>

static inline void
breakpoint(void)
{
	asm volatile("int3");
  800177:	cc                   	int3   

	breakpoint();
}
  800178:	83 c4 10             	add    $0x10,%esp
  80017b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80017e:	c9                   	leave  
  80017f:	c3                   	ret    

00800180 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800180:	55                   	push   %ebp
  800181:	89 e5                	mov    %esp,%ebp
  800183:	56                   	push   %esi
  800184:	53                   	push   %ebx
  800185:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800188:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80018b:	e8 33 0a 00 00       	call   800bc3 <sys_getenvid>
  800190:	25 ff 03 00 00       	and    $0x3ff,%eax
  800195:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80019c:	c1 e0 07             	shl    $0x7,%eax
  80019f:	29 d0                	sub    %edx,%eax
  8001a1:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8001a6:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8001ab:	85 db                	test   %ebx,%ebx
  8001ad:	7e 07                	jle    8001b6 <libmain+0x36>
		binaryname = argv[0];
  8001af:	8b 06                	mov    (%esi),%eax
  8001b1:	a3 08 30 80 00       	mov    %eax,0x803008

	// call user main routine
	umain(argc, argv);
  8001b6:	83 ec 08             	sub    $0x8,%esp
  8001b9:	56                   	push   %esi
  8001ba:	53                   	push   %ebx
  8001bb:	e8 93 fe ff ff       	call   800053 <umain>

	// exit gracefully
	exit();
  8001c0:	e8 0a 00 00 00       	call   8001cf <exit>
}
  8001c5:	83 c4 10             	add    $0x10,%esp
  8001c8:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8001cb:	5b                   	pop    %ebx
  8001cc:	5e                   	pop    %esi
  8001cd:	5d                   	pop    %ebp
  8001ce:	c3                   	ret    

008001cf <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8001cf:	55                   	push   %ebp
  8001d0:	89 e5                	mov    %esp,%ebp
  8001d2:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8001d5:	6a 00                	push   $0x0
  8001d7:	e8 a6 09 00 00       	call   800b82 <sys_env_destroy>
}
  8001dc:	83 c4 10             	add    $0x10,%esp
  8001df:	c9                   	leave  
  8001e0:	c3                   	ret    

008001e1 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8001e1:	55                   	push   %ebp
  8001e2:	89 e5                	mov    %esp,%ebp
  8001e4:	56                   	push   %esi
  8001e5:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8001e6:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8001e9:	8b 35 08 30 80 00    	mov    0x803008,%esi
  8001ef:	e8 cf 09 00 00       	call   800bc3 <sys_getenvid>
  8001f4:	83 ec 0c             	sub    $0xc,%esp
  8001f7:	ff 75 0c             	pushl  0xc(%ebp)
  8001fa:	ff 75 08             	pushl  0x8(%ebp)
  8001fd:	56                   	push   %esi
  8001fe:	50                   	push   %eax
  8001ff:	68 b8 28 80 00       	push   $0x8028b8
  800204:	e8 b0 00 00 00       	call   8002b9 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800209:	83 c4 18             	add    $0x18,%esp
  80020c:	53                   	push   %ebx
  80020d:	ff 75 10             	pushl  0x10(%ebp)
  800210:	e8 53 00 00 00       	call   800268 <vcprintf>
	cprintf("\n");
  800215:	c7 04 24 12 2f 80 00 	movl   $0x802f12,(%esp)
  80021c:	e8 98 00 00 00       	call   8002b9 <cprintf>
  800221:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800224:	cc                   	int3   
  800225:	eb fd                	jmp    800224 <_panic+0x43>

00800227 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800227:	55                   	push   %ebp
  800228:	89 e5                	mov    %esp,%ebp
  80022a:	53                   	push   %ebx
  80022b:	83 ec 04             	sub    $0x4,%esp
  80022e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800231:	8b 13                	mov    (%ebx),%edx
  800233:	8d 42 01             	lea    0x1(%edx),%eax
  800236:	89 03                	mov    %eax,(%ebx)
  800238:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80023b:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80023f:	3d ff 00 00 00       	cmp    $0xff,%eax
  800244:	75 1a                	jne    800260 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800246:	83 ec 08             	sub    $0x8,%esp
  800249:	68 ff 00 00 00       	push   $0xff
  80024e:	8d 43 08             	lea    0x8(%ebx),%eax
  800251:	50                   	push   %eax
  800252:	e8 ee 08 00 00       	call   800b45 <sys_cputs>
		b->idx = 0;
  800257:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80025d:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800260:	ff 43 04             	incl   0x4(%ebx)
}
  800263:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800266:	c9                   	leave  
  800267:	c3                   	ret    

00800268 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800268:	55                   	push   %ebp
  800269:	89 e5                	mov    %esp,%ebp
  80026b:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800271:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800278:	00 00 00 
	b.cnt = 0;
  80027b:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800282:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800285:	ff 75 0c             	pushl  0xc(%ebp)
  800288:	ff 75 08             	pushl  0x8(%ebp)
  80028b:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800291:	50                   	push   %eax
  800292:	68 27 02 80 00       	push   $0x800227
  800297:	e8 51 01 00 00       	call   8003ed <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80029c:	83 c4 08             	add    $0x8,%esp
  80029f:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8002a5:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8002ab:	50                   	push   %eax
  8002ac:	e8 94 08 00 00       	call   800b45 <sys_cputs>

	return b.cnt;
}
  8002b1:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8002b7:	c9                   	leave  
  8002b8:	c3                   	ret    

008002b9 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8002b9:	55                   	push   %ebp
  8002ba:	89 e5                	mov    %esp,%ebp
  8002bc:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8002bf:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8002c2:	50                   	push   %eax
  8002c3:	ff 75 08             	pushl  0x8(%ebp)
  8002c6:	e8 9d ff ff ff       	call   800268 <vcprintf>
	va_end(ap);

	return cnt;
}
  8002cb:	c9                   	leave  
  8002cc:	c3                   	ret    

008002cd <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8002cd:	55                   	push   %ebp
  8002ce:	89 e5                	mov    %esp,%ebp
  8002d0:	57                   	push   %edi
  8002d1:	56                   	push   %esi
  8002d2:	53                   	push   %ebx
  8002d3:	83 ec 1c             	sub    $0x1c,%esp
  8002d6:	89 c7                	mov    %eax,%edi
  8002d8:	89 d6                	mov    %edx,%esi
  8002da:	8b 45 08             	mov    0x8(%ebp),%eax
  8002dd:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002e0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8002e3:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8002e6:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8002e9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002ee:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8002f1:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8002f4:	39 d3                	cmp    %edx,%ebx
  8002f6:	72 05                	jb     8002fd <printnum+0x30>
  8002f8:	39 45 10             	cmp    %eax,0x10(%ebp)
  8002fb:	77 45                	ja     800342 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8002fd:	83 ec 0c             	sub    $0xc,%esp
  800300:	ff 75 18             	pushl  0x18(%ebp)
  800303:	8b 45 14             	mov    0x14(%ebp),%eax
  800306:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800309:	53                   	push   %ebx
  80030a:	ff 75 10             	pushl  0x10(%ebp)
  80030d:	83 ec 08             	sub    $0x8,%esp
  800310:	ff 75 e4             	pushl  -0x1c(%ebp)
  800313:	ff 75 e0             	pushl  -0x20(%ebp)
  800316:	ff 75 dc             	pushl  -0x24(%ebp)
  800319:	ff 75 d8             	pushl  -0x28(%ebp)
  80031c:	e8 6b 22 00 00       	call   80258c <__udivdi3>
  800321:	83 c4 18             	add    $0x18,%esp
  800324:	52                   	push   %edx
  800325:	50                   	push   %eax
  800326:	89 f2                	mov    %esi,%edx
  800328:	89 f8                	mov    %edi,%eax
  80032a:	e8 9e ff ff ff       	call   8002cd <printnum>
  80032f:	83 c4 20             	add    $0x20,%esp
  800332:	eb 16                	jmp    80034a <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800334:	83 ec 08             	sub    $0x8,%esp
  800337:	56                   	push   %esi
  800338:	ff 75 18             	pushl  0x18(%ebp)
  80033b:	ff d7                	call   *%edi
  80033d:	83 c4 10             	add    $0x10,%esp
  800340:	eb 03                	jmp    800345 <printnum+0x78>
  800342:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800345:	4b                   	dec    %ebx
  800346:	85 db                	test   %ebx,%ebx
  800348:	7f ea                	jg     800334 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80034a:	83 ec 08             	sub    $0x8,%esp
  80034d:	56                   	push   %esi
  80034e:	83 ec 04             	sub    $0x4,%esp
  800351:	ff 75 e4             	pushl  -0x1c(%ebp)
  800354:	ff 75 e0             	pushl  -0x20(%ebp)
  800357:	ff 75 dc             	pushl  -0x24(%ebp)
  80035a:	ff 75 d8             	pushl  -0x28(%ebp)
  80035d:	e8 3a 23 00 00       	call   80269c <__umoddi3>
  800362:	83 c4 14             	add    $0x14,%esp
  800365:	0f be 80 db 28 80 00 	movsbl 0x8028db(%eax),%eax
  80036c:	50                   	push   %eax
  80036d:	ff d7                	call   *%edi
}
  80036f:	83 c4 10             	add    $0x10,%esp
  800372:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800375:	5b                   	pop    %ebx
  800376:	5e                   	pop    %esi
  800377:	5f                   	pop    %edi
  800378:	5d                   	pop    %ebp
  800379:	c3                   	ret    

0080037a <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80037a:	55                   	push   %ebp
  80037b:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80037d:	83 fa 01             	cmp    $0x1,%edx
  800380:	7e 0e                	jle    800390 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800382:	8b 10                	mov    (%eax),%edx
  800384:	8d 4a 08             	lea    0x8(%edx),%ecx
  800387:	89 08                	mov    %ecx,(%eax)
  800389:	8b 02                	mov    (%edx),%eax
  80038b:	8b 52 04             	mov    0x4(%edx),%edx
  80038e:	eb 22                	jmp    8003b2 <getuint+0x38>
	else if (lflag)
  800390:	85 d2                	test   %edx,%edx
  800392:	74 10                	je     8003a4 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800394:	8b 10                	mov    (%eax),%edx
  800396:	8d 4a 04             	lea    0x4(%edx),%ecx
  800399:	89 08                	mov    %ecx,(%eax)
  80039b:	8b 02                	mov    (%edx),%eax
  80039d:	ba 00 00 00 00       	mov    $0x0,%edx
  8003a2:	eb 0e                	jmp    8003b2 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8003a4:	8b 10                	mov    (%eax),%edx
  8003a6:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003a9:	89 08                	mov    %ecx,(%eax)
  8003ab:	8b 02                	mov    (%edx),%eax
  8003ad:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8003b2:	5d                   	pop    %ebp
  8003b3:	c3                   	ret    

008003b4 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8003b4:	55                   	push   %ebp
  8003b5:	89 e5                	mov    %esp,%ebp
  8003b7:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8003ba:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8003bd:	8b 10                	mov    (%eax),%edx
  8003bf:	3b 50 04             	cmp    0x4(%eax),%edx
  8003c2:	73 0a                	jae    8003ce <sprintputch+0x1a>
		*b->buf++ = ch;
  8003c4:	8d 4a 01             	lea    0x1(%edx),%ecx
  8003c7:	89 08                	mov    %ecx,(%eax)
  8003c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8003cc:	88 02                	mov    %al,(%edx)
}
  8003ce:	5d                   	pop    %ebp
  8003cf:	c3                   	ret    

008003d0 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8003d0:	55                   	push   %ebp
  8003d1:	89 e5                	mov    %esp,%ebp
  8003d3:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8003d6:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8003d9:	50                   	push   %eax
  8003da:	ff 75 10             	pushl  0x10(%ebp)
  8003dd:	ff 75 0c             	pushl  0xc(%ebp)
  8003e0:	ff 75 08             	pushl  0x8(%ebp)
  8003e3:	e8 05 00 00 00       	call   8003ed <vprintfmt>
	va_end(ap);
}
  8003e8:	83 c4 10             	add    $0x10,%esp
  8003eb:	c9                   	leave  
  8003ec:	c3                   	ret    

008003ed <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8003ed:	55                   	push   %ebp
  8003ee:	89 e5                	mov    %esp,%ebp
  8003f0:	57                   	push   %edi
  8003f1:	56                   	push   %esi
  8003f2:	53                   	push   %ebx
  8003f3:	83 ec 2c             	sub    $0x2c,%esp
  8003f6:	8b 75 08             	mov    0x8(%ebp),%esi
  8003f9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8003fc:	8b 7d 10             	mov    0x10(%ebp),%edi
  8003ff:	eb 12                	jmp    800413 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800401:	85 c0                	test   %eax,%eax
  800403:	0f 84 68 03 00 00    	je     800771 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800409:	83 ec 08             	sub    $0x8,%esp
  80040c:	53                   	push   %ebx
  80040d:	50                   	push   %eax
  80040e:	ff d6                	call   *%esi
  800410:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800413:	47                   	inc    %edi
  800414:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800418:	83 f8 25             	cmp    $0x25,%eax
  80041b:	75 e4                	jne    800401 <vprintfmt+0x14>
  80041d:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800421:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800428:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80042f:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800436:	ba 00 00 00 00       	mov    $0x0,%edx
  80043b:	eb 07                	jmp    800444 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80043d:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800440:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800444:	8d 47 01             	lea    0x1(%edi),%eax
  800447:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80044a:	0f b6 0f             	movzbl (%edi),%ecx
  80044d:	8a 07                	mov    (%edi),%al
  80044f:	83 e8 23             	sub    $0x23,%eax
  800452:	3c 55                	cmp    $0x55,%al
  800454:	0f 87 fe 02 00 00    	ja     800758 <vprintfmt+0x36b>
  80045a:	0f b6 c0             	movzbl %al,%eax
  80045d:	ff 24 85 20 2a 80 00 	jmp    *0x802a20(,%eax,4)
  800464:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800467:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80046b:	eb d7                	jmp    800444 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80046d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800470:	b8 00 00 00 00       	mov    $0x0,%eax
  800475:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800478:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80047b:	01 c0                	add    %eax,%eax
  80047d:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800481:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800484:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800487:	83 fa 09             	cmp    $0x9,%edx
  80048a:	77 34                	ja     8004c0 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80048c:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80048d:	eb e9                	jmp    800478 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80048f:	8b 45 14             	mov    0x14(%ebp),%eax
  800492:	8d 48 04             	lea    0x4(%eax),%ecx
  800495:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800498:	8b 00                	mov    (%eax),%eax
  80049a:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80049d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8004a0:	eb 24                	jmp    8004c6 <vprintfmt+0xd9>
  8004a2:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004a6:	79 07                	jns    8004af <vprintfmt+0xc2>
  8004a8:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004af:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004b2:	eb 90                	jmp    800444 <vprintfmt+0x57>
  8004b4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8004b7:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8004be:	eb 84                	jmp    800444 <vprintfmt+0x57>
  8004c0:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8004c3:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8004c6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004ca:	0f 89 74 ff ff ff    	jns    800444 <vprintfmt+0x57>
				width = precision, precision = -1;
  8004d0:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8004d3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004d6:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8004dd:	e9 62 ff ff ff       	jmp    800444 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8004e2:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8004e6:	e9 59 ff ff ff       	jmp    800444 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8004eb:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ee:	8d 50 04             	lea    0x4(%eax),%edx
  8004f1:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f4:	83 ec 08             	sub    $0x8,%esp
  8004f7:	53                   	push   %ebx
  8004f8:	ff 30                	pushl  (%eax)
  8004fa:	ff d6                	call   *%esi
			break;
  8004fc:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ff:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800502:	e9 0c ff ff ff       	jmp    800413 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800507:	8b 45 14             	mov    0x14(%ebp),%eax
  80050a:	8d 50 04             	lea    0x4(%eax),%edx
  80050d:	89 55 14             	mov    %edx,0x14(%ebp)
  800510:	8b 00                	mov    (%eax),%eax
  800512:	85 c0                	test   %eax,%eax
  800514:	79 02                	jns    800518 <vprintfmt+0x12b>
  800516:	f7 d8                	neg    %eax
  800518:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80051a:	83 f8 0f             	cmp    $0xf,%eax
  80051d:	7f 0b                	jg     80052a <vprintfmt+0x13d>
  80051f:	8b 04 85 80 2b 80 00 	mov    0x802b80(,%eax,4),%eax
  800526:	85 c0                	test   %eax,%eax
  800528:	75 18                	jne    800542 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80052a:	52                   	push   %edx
  80052b:	68 f3 28 80 00       	push   $0x8028f3
  800530:	53                   	push   %ebx
  800531:	56                   	push   %esi
  800532:	e8 99 fe ff ff       	call   8003d0 <printfmt>
  800537:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80053a:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80053d:	e9 d1 fe ff ff       	jmp    800413 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800542:	50                   	push   %eax
  800543:	68 6c 2d 80 00       	push   $0x802d6c
  800548:	53                   	push   %ebx
  800549:	56                   	push   %esi
  80054a:	e8 81 fe ff ff       	call   8003d0 <printfmt>
  80054f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800552:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800555:	e9 b9 fe ff ff       	jmp    800413 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80055a:	8b 45 14             	mov    0x14(%ebp),%eax
  80055d:	8d 50 04             	lea    0x4(%eax),%edx
  800560:	89 55 14             	mov    %edx,0x14(%ebp)
  800563:	8b 38                	mov    (%eax),%edi
  800565:	85 ff                	test   %edi,%edi
  800567:	75 05                	jne    80056e <vprintfmt+0x181>
				p = "(null)";
  800569:	bf ec 28 80 00       	mov    $0x8028ec,%edi
			if (width > 0 && padc != '-')
  80056e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800572:	0f 8e 90 00 00 00    	jle    800608 <vprintfmt+0x21b>
  800578:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80057c:	0f 84 8e 00 00 00    	je     800610 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800582:	83 ec 08             	sub    $0x8,%esp
  800585:	ff 75 d0             	pushl  -0x30(%ebp)
  800588:	57                   	push   %edi
  800589:	e8 70 02 00 00       	call   8007fe <strnlen>
  80058e:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800591:	29 c1                	sub    %eax,%ecx
  800593:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800596:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800599:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80059d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8005a0:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8005a3:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005a5:	eb 0d                	jmp    8005b4 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8005a7:	83 ec 08             	sub    $0x8,%esp
  8005aa:	53                   	push   %ebx
  8005ab:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005ae:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005b0:	4f                   	dec    %edi
  8005b1:	83 c4 10             	add    $0x10,%esp
  8005b4:	85 ff                	test   %edi,%edi
  8005b6:	7f ef                	jg     8005a7 <vprintfmt+0x1ba>
  8005b8:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8005bb:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8005be:	89 c8                	mov    %ecx,%eax
  8005c0:	85 c9                	test   %ecx,%ecx
  8005c2:	79 05                	jns    8005c9 <vprintfmt+0x1dc>
  8005c4:	b8 00 00 00 00       	mov    $0x0,%eax
  8005c9:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8005cc:	29 c1                	sub    %eax,%ecx
  8005ce:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8005d1:	89 75 08             	mov    %esi,0x8(%ebp)
  8005d4:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005d7:	eb 3d                	jmp    800616 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8005d9:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8005dd:	74 19                	je     8005f8 <vprintfmt+0x20b>
  8005df:	0f be c0             	movsbl %al,%eax
  8005e2:	83 e8 20             	sub    $0x20,%eax
  8005e5:	83 f8 5e             	cmp    $0x5e,%eax
  8005e8:	76 0e                	jbe    8005f8 <vprintfmt+0x20b>
					putch('?', putdat);
  8005ea:	83 ec 08             	sub    $0x8,%esp
  8005ed:	53                   	push   %ebx
  8005ee:	6a 3f                	push   $0x3f
  8005f0:	ff 55 08             	call   *0x8(%ebp)
  8005f3:	83 c4 10             	add    $0x10,%esp
  8005f6:	eb 0b                	jmp    800603 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8005f8:	83 ec 08             	sub    $0x8,%esp
  8005fb:	53                   	push   %ebx
  8005fc:	52                   	push   %edx
  8005fd:	ff 55 08             	call   *0x8(%ebp)
  800600:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800603:	ff 4d e4             	decl   -0x1c(%ebp)
  800606:	eb 0e                	jmp    800616 <vprintfmt+0x229>
  800608:	89 75 08             	mov    %esi,0x8(%ebp)
  80060b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80060e:	eb 06                	jmp    800616 <vprintfmt+0x229>
  800610:	89 75 08             	mov    %esi,0x8(%ebp)
  800613:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800616:	47                   	inc    %edi
  800617:	8a 47 ff             	mov    -0x1(%edi),%al
  80061a:	0f be d0             	movsbl %al,%edx
  80061d:	85 d2                	test   %edx,%edx
  80061f:	74 1d                	je     80063e <vprintfmt+0x251>
  800621:	85 f6                	test   %esi,%esi
  800623:	78 b4                	js     8005d9 <vprintfmt+0x1ec>
  800625:	4e                   	dec    %esi
  800626:	79 b1                	jns    8005d9 <vprintfmt+0x1ec>
  800628:	8b 75 08             	mov    0x8(%ebp),%esi
  80062b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80062e:	eb 14                	jmp    800644 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800630:	83 ec 08             	sub    $0x8,%esp
  800633:	53                   	push   %ebx
  800634:	6a 20                	push   $0x20
  800636:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800638:	4f                   	dec    %edi
  800639:	83 c4 10             	add    $0x10,%esp
  80063c:	eb 06                	jmp    800644 <vprintfmt+0x257>
  80063e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800641:	8b 75 08             	mov    0x8(%ebp),%esi
  800644:	85 ff                	test   %edi,%edi
  800646:	7f e8                	jg     800630 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800648:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80064b:	e9 c3 fd ff ff       	jmp    800413 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800650:	83 fa 01             	cmp    $0x1,%edx
  800653:	7e 16                	jle    80066b <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800655:	8b 45 14             	mov    0x14(%ebp),%eax
  800658:	8d 50 08             	lea    0x8(%eax),%edx
  80065b:	89 55 14             	mov    %edx,0x14(%ebp)
  80065e:	8b 50 04             	mov    0x4(%eax),%edx
  800661:	8b 00                	mov    (%eax),%eax
  800663:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800666:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800669:	eb 32                	jmp    80069d <vprintfmt+0x2b0>
	else if (lflag)
  80066b:	85 d2                	test   %edx,%edx
  80066d:	74 18                	je     800687 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80066f:	8b 45 14             	mov    0x14(%ebp),%eax
  800672:	8d 50 04             	lea    0x4(%eax),%edx
  800675:	89 55 14             	mov    %edx,0x14(%ebp)
  800678:	8b 00                	mov    (%eax),%eax
  80067a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80067d:	89 c1                	mov    %eax,%ecx
  80067f:	c1 f9 1f             	sar    $0x1f,%ecx
  800682:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800685:	eb 16                	jmp    80069d <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800687:	8b 45 14             	mov    0x14(%ebp),%eax
  80068a:	8d 50 04             	lea    0x4(%eax),%edx
  80068d:	89 55 14             	mov    %edx,0x14(%ebp)
  800690:	8b 00                	mov    (%eax),%eax
  800692:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800695:	89 c1                	mov    %eax,%ecx
  800697:	c1 f9 1f             	sar    $0x1f,%ecx
  80069a:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80069d:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006a0:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8006a3:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8006a7:	79 76                	jns    80071f <vprintfmt+0x332>
				putch('-', putdat);
  8006a9:	83 ec 08             	sub    $0x8,%esp
  8006ac:	53                   	push   %ebx
  8006ad:	6a 2d                	push   $0x2d
  8006af:	ff d6                	call   *%esi
				num = -(long long) num;
  8006b1:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006b4:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8006b7:	f7 d8                	neg    %eax
  8006b9:	83 d2 00             	adc    $0x0,%edx
  8006bc:	f7 da                	neg    %edx
  8006be:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8006c1:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8006c6:	eb 5c                	jmp    800724 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8006c8:	8d 45 14             	lea    0x14(%ebp),%eax
  8006cb:	e8 aa fc ff ff       	call   80037a <getuint>
			base = 10;
  8006d0:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8006d5:	eb 4d                	jmp    800724 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8006d7:	8d 45 14             	lea    0x14(%ebp),%eax
  8006da:	e8 9b fc ff ff       	call   80037a <getuint>
			base = 8;
  8006df:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8006e4:	eb 3e                	jmp    800724 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8006e6:	83 ec 08             	sub    $0x8,%esp
  8006e9:	53                   	push   %ebx
  8006ea:	6a 30                	push   $0x30
  8006ec:	ff d6                	call   *%esi
			putch('x', putdat);
  8006ee:	83 c4 08             	add    $0x8,%esp
  8006f1:	53                   	push   %ebx
  8006f2:	6a 78                	push   $0x78
  8006f4:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8006f6:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f9:	8d 50 04             	lea    0x4(%eax),%edx
  8006fc:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006ff:	8b 00                	mov    (%eax),%eax
  800701:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800706:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800709:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80070e:	eb 14                	jmp    800724 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800710:	8d 45 14             	lea    0x14(%ebp),%eax
  800713:	e8 62 fc ff ff       	call   80037a <getuint>
			base = 16;
  800718:	b9 10 00 00 00       	mov    $0x10,%ecx
  80071d:	eb 05                	jmp    800724 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80071f:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800724:	83 ec 0c             	sub    $0xc,%esp
  800727:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80072b:	57                   	push   %edi
  80072c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80072f:	51                   	push   %ecx
  800730:	52                   	push   %edx
  800731:	50                   	push   %eax
  800732:	89 da                	mov    %ebx,%edx
  800734:	89 f0                	mov    %esi,%eax
  800736:	e8 92 fb ff ff       	call   8002cd <printnum>
			break;
  80073b:	83 c4 20             	add    $0x20,%esp
  80073e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800741:	e9 cd fc ff ff       	jmp    800413 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800746:	83 ec 08             	sub    $0x8,%esp
  800749:	53                   	push   %ebx
  80074a:	51                   	push   %ecx
  80074b:	ff d6                	call   *%esi
			break;
  80074d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800750:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800753:	e9 bb fc ff ff       	jmp    800413 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800758:	83 ec 08             	sub    $0x8,%esp
  80075b:	53                   	push   %ebx
  80075c:	6a 25                	push   $0x25
  80075e:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800760:	83 c4 10             	add    $0x10,%esp
  800763:	eb 01                	jmp    800766 <vprintfmt+0x379>
  800765:	4f                   	dec    %edi
  800766:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80076a:	75 f9                	jne    800765 <vprintfmt+0x378>
  80076c:	e9 a2 fc ff ff       	jmp    800413 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800771:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800774:	5b                   	pop    %ebx
  800775:	5e                   	pop    %esi
  800776:	5f                   	pop    %edi
  800777:	5d                   	pop    %ebp
  800778:	c3                   	ret    

00800779 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800779:	55                   	push   %ebp
  80077a:	89 e5                	mov    %esp,%ebp
  80077c:	83 ec 18             	sub    $0x18,%esp
  80077f:	8b 45 08             	mov    0x8(%ebp),%eax
  800782:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800785:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800788:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80078c:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80078f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800796:	85 c0                	test   %eax,%eax
  800798:	74 26                	je     8007c0 <vsnprintf+0x47>
  80079a:	85 d2                	test   %edx,%edx
  80079c:	7e 29                	jle    8007c7 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80079e:	ff 75 14             	pushl  0x14(%ebp)
  8007a1:	ff 75 10             	pushl  0x10(%ebp)
  8007a4:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007a7:	50                   	push   %eax
  8007a8:	68 b4 03 80 00       	push   $0x8003b4
  8007ad:	e8 3b fc ff ff       	call   8003ed <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8007b2:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8007b5:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8007b8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8007bb:	83 c4 10             	add    $0x10,%esp
  8007be:	eb 0c                	jmp    8007cc <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8007c0:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8007c5:	eb 05                	jmp    8007cc <vsnprintf+0x53>
  8007c7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8007cc:	c9                   	leave  
  8007cd:	c3                   	ret    

008007ce <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8007ce:	55                   	push   %ebp
  8007cf:	89 e5                	mov    %esp,%ebp
  8007d1:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8007d4:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8007d7:	50                   	push   %eax
  8007d8:	ff 75 10             	pushl  0x10(%ebp)
  8007db:	ff 75 0c             	pushl  0xc(%ebp)
  8007de:	ff 75 08             	pushl  0x8(%ebp)
  8007e1:	e8 93 ff ff ff       	call   800779 <vsnprintf>
	va_end(ap);

	return rc;
}
  8007e6:	c9                   	leave  
  8007e7:	c3                   	ret    

008007e8 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8007e8:	55                   	push   %ebp
  8007e9:	89 e5                	mov    %esp,%ebp
  8007eb:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8007ee:	b8 00 00 00 00       	mov    $0x0,%eax
  8007f3:	eb 01                	jmp    8007f6 <strlen+0xe>
		n++;
  8007f5:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8007f6:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8007fa:	75 f9                	jne    8007f5 <strlen+0xd>
		n++;
	return n;
}
  8007fc:	5d                   	pop    %ebp
  8007fd:	c3                   	ret    

008007fe <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8007fe:	55                   	push   %ebp
  8007ff:	89 e5                	mov    %esp,%ebp
  800801:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800804:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800807:	ba 00 00 00 00       	mov    $0x0,%edx
  80080c:	eb 01                	jmp    80080f <strnlen+0x11>
		n++;
  80080e:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80080f:	39 c2                	cmp    %eax,%edx
  800811:	74 08                	je     80081b <strnlen+0x1d>
  800813:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800817:	75 f5                	jne    80080e <strnlen+0x10>
  800819:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80081b:	5d                   	pop    %ebp
  80081c:	c3                   	ret    

0080081d <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80081d:	55                   	push   %ebp
  80081e:	89 e5                	mov    %esp,%ebp
  800820:	53                   	push   %ebx
  800821:	8b 45 08             	mov    0x8(%ebp),%eax
  800824:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800827:	89 c2                	mov    %eax,%edx
  800829:	42                   	inc    %edx
  80082a:	41                   	inc    %ecx
  80082b:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80082e:	88 5a ff             	mov    %bl,-0x1(%edx)
  800831:	84 db                	test   %bl,%bl
  800833:	75 f4                	jne    800829 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800835:	5b                   	pop    %ebx
  800836:	5d                   	pop    %ebp
  800837:	c3                   	ret    

00800838 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800838:	55                   	push   %ebp
  800839:	89 e5                	mov    %esp,%ebp
  80083b:	53                   	push   %ebx
  80083c:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80083f:	53                   	push   %ebx
  800840:	e8 a3 ff ff ff       	call   8007e8 <strlen>
  800845:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800848:	ff 75 0c             	pushl  0xc(%ebp)
  80084b:	01 d8                	add    %ebx,%eax
  80084d:	50                   	push   %eax
  80084e:	e8 ca ff ff ff       	call   80081d <strcpy>
	return dst;
}
  800853:	89 d8                	mov    %ebx,%eax
  800855:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800858:	c9                   	leave  
  800859:	c3                   	ret    

0080085a <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80085a:	55                   	push   %ebp
  80085b:	89 e5                	mov    %esp,%ebp
  80085d:	56                   	push   %esi
  80085e:	53                   	push   %ebx
  80085f:	8b 75 08             	mov    0x8(%ebp),%esi
  800862:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800865:	89 f3                	mov    %esi,%ebx
  800867:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80086a:	89 f2                	mov    %esi,%edx
  80086c:	eb 0c                	jmp    80087a <strncpy+0x20>
		*dst++ = *src;
  80086e:	42                   	inc    %edx
  80086f:	8a 01                	mov    (%ecx),%al
  800871:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800874:	80 39 01             	cmpb   $0x1,(%ecx)
  800877:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80087a:	39 da                	cmp    %ebx,%edx
  80087c:	75 f0                	jne    80086e <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80087e:	89 f0                	mov    %esi,%eax
  800880:	5b                   	pop    %ebx
  800881:	5e                   	pop    %esi
  800882:	5d                   	pop    %ebp
  800883:	c3                   	ret    

00800884 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800884:	55                   	push   %ebp
  800885:	89 e5                	mov    %esp,%ebp
  800887:	56                   	push   %esi
  800888:	53                   	push   %ebx
  800889:	8b 75 08             	mov    0x8(%ebp),%esi
  80088c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80088f:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800892:	85 c0                	test   %eax,%eax
  800894:	74 1e                	je     8008b4 <strlcpy+0x30>
  800896:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80089a:	89 f2                	mov    %esi,%edx
  80089c:	eb 05                	jmp    8008a3 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80089e:	42                   	inc    %edx
  80089f:	41                   	inc    %ecx
  8008a0:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8008a3:	39 c2                	cmp    %eax,%edx
  8008a5:	74 08                	je     8008af <strlcpy+0x2b>
  8008a7:	8a 19                	mov    (%ecx),%bl
  8008a9:	84 db                	test   %bl,%bl
  8008ab:	75 f1                	jne    80089e <strlcpy+0x1a>
  8008ad:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8008af:	c6 00 00             	movb   $0x0,(%eax)
  8008b2:	eb 02                	jmp    8008b6 <strlcpy+0x32>
  8008b4:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8008b6:	29 f0                	sub    %esi,%eax
}
  8008b8:	5b                   	pop    %ebx
  8008b9:	5e                   	pop    %esi
  8008ba:	5d                   	pop    %ebp
  8008bb:	c3                   	ret    

008008bc <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8008bc:	55                   	push   %ebp
  8008bd:	89 e5                	mov    %esp,%ebp
  8008bf:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8008c2:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8008c5:	eb 02                	jmp    8008c9 <strcmp+0xd>
		p++, q++;
  8008c7:	41                   	inc    %ecx
  8008c8:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8008c9:	8a 01                	mov    (%ecx),%al
  8008cb:	84 c0                	test   %al,%al
  8008cd:	74 04                	je     8008d3 <strcmp+0x17>
  8008cf:	3a 02                	cmp    (%edx),%al
  8008d1:	74 f4                	je     8008c7 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8008d3:	0f b6 c0             	movzbl %al,%eax
  8008d6:	0f b6 12             	movzbl (%edx),%edx
  8008d9:	29 d0                	sub    %edx,%eax
}
  8008db:	5d                   	pop    %ebp
  8008dc:	c3                   	ret    

008008dd <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8008dd:	55                   	push   %ebp
  8008de:	89 e5                	mov    %esp,%ebp
  8008e0:	53                   	push   %ebx
  8008e1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e4:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008e7:	89 c3                	mov    %eax,%ebx
  8008e9:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8008ec:	eb 02                	jmp    8008f0 <strncmp+0x13>
		n--, p++, q++;
  8008ee:	40                   	inc    %eax
  8008ef:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8008f0:	39 d8                	cmp    %ebx,%eax
  8008f2:	74 14                	je     800908 <strncmp+0x2b>
  8008f4:	8a 08                	mov    (%eax),%cl
  8008f6:	84 c9                	test   %cl,%cl
  8008f8:	74 04                	je     8008fe <strncmp+0x21>
  8008fa:	3a 0a                	cmp    (%edx),%cl
  8008fc:	74 f0                	je     8008ee <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8008fe:	0f b6 00             	movzbl (%eax),%eax
  800901:	0f b6 12             	movzbl (%edx),%edx
  800904:	29 d0                	sub    %edx,%eax
  800906:	eb 05                	jmp    80090d <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800908:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80090d:	5b                   	pop    %ebx
  80090e:	5d                   	pop    %ebp
  80090f:	c3                   	ret    

00800910 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800910:	55                   	push   %ebp
  800911:	89 e5                	mov    %esp,%ebp
  800913:	8b 45 08             	mov    0x8(%ebp),%eax
  800916:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800919:	eb 05                	jmp    800920 <strchr+0x10>
		if (*s == c)
  80091b:	38 ca                	cmp    %cl,%dl
  80091d:	74 0c                	je     80092b <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80091f:	40                   	inc    %eax
  800920:	8a 10                	mov    (%eax),%dl
  800922:	84 d2                	test   %dl,%dl
  800924:	75 f5                	jne    80091b <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800926:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80092b:	5d                   	pop    %ebp
  80092c:	c3                   	ret    

0080092d <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80092d:	55                   	push   %ebp
  80092e:	89 e5                	mov    %esp,%ebp
  800930:	8b 45 08             	mov    0x8(%ebp),%eax
  800933:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800936:	eb 05                	jmp    80093d <strfind+0x10>
		if (*s == c)
  800938:	38 ca                	cmp    %cl,%dl
  80093a:	74 07                	je     800943 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80093c:	40                   	inc    %eax
  80093d:	8a 10                	mov    (%eax),%dl
  80093f:	84 d2                	test   %dl,%dl
  800941:	75 f5                	jne    800938 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800943:	5d                   	pop    %ebp
  800944:	c3                   	ret    

00800945 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800945:	55                   	push   %ebp
  800946:	89 e5                	mov    %esp,%ebp
  800948:	57                   	push   %edi
  800949:	56                   	push   %esi
  80094a:	53                   	push   %ebx
  80094b:	8b 7d 08             	mov    0x8(%ebp),%edi
  80094e:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800951:	85 c9                	test   %ecx,%ecx
  800953:	74 36                	je     80098b <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800955:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80095b:	75 28                	jne    800985 <memset+0x40>
  80095d:	f6 c1 03             	test   $0x3,%cl
  800960:	75 23                	jne    800985 <memset+0x40>
		c &= 0xFF;
  800962:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800966:	89 d3                	mov    %edx,%ebx
  800968:	c1 e3 08             	shl    $0x8,%ebx
  80096b:	89 d6                	mov    %edx,%esi
  80096d:	c1 e6 18             	shl    $0x18,%esi
  800970:	89 d0                	mov    %edx,%eax
  800972:	c1 e0 10             	shl    $0x10,%eax
  800975:	09 f0                	or     %esi,%eax
  800977:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800979:	89 d8                	mov    %ebx,%eax
  80097b:	09 d0                	or     %edx,%eax
  80097d:	c1 e9 02             	shr    $0x2,%ecx
  800980:	fc                   	cld    
  800981:	f3 ab                	rep stos %eax,%es:(%edi)
  800983:	eb 06                	jmp    80098b <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800985:	8b 45 0c             	mov    0xc(%ebp),%eax
  800988:	fc                   	cld    
  800989:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80098b:	89 f8                	mov    %edi,%eax
  80098d:	5b                   	pop    %ebx
  80098e:	5e                   	pop    %esi
  80098f:	5f                   	pop    %edi
  800990:	5d                   	pop    %ebp
  800991:	c3                   	ret    

00800992 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800992:	55                   	push   %ebp
  800993:	89 e5                	mov    %esp,%ebp
  800995:	57                   	push   %edi
  800996:	56                   	push   %esi
  800997:	8b 45 08             	mov    0x8(%ebp),%eax
  80099a:	8b 75 0c             	mov    0xc(%ebp),%esi
  80099d:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8009a0:	39 c6                	cmp    %eax,%esi
  8009a2:	73 33                	jae    8009d7 <memmove+0x45>
  8009a4:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8009a7:	39 d0                	cmp    %edx,%eax
  8009a9:	73 2c                	jae    8009d7 <memmove+0x45>
		s += n;
		d += n;
  8009ab:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009ae:	89 d6                	mov    %edx,%esi
  8009b0:	09 fe                	or     %edi,%esi
  8009b2:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8009b8:	75 13                	jne    8009cd <memmove+0x3b>
  8009ba:	f6 c1 03             	test   $0x3,%cl
  8009bd:	75 0e                	jne    8009cd <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8009bf:	83 ef 04             	sub    $0x4,%edi
  8009c2:	8d 72 fc             	lea    -0x4(%edx),%esi
  8009c5:	c1 e9 02             	shr    $0x2,%ecx
  8009c8:	fd                   	std    
  8009c9:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8009cb:	eb 07                	jmp    8009d4 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8009cd:	4f                   	dec    %edi
  8009ce:	8d 72 ff             	lea    -0x1(%edx),%esi
  8009d1:	fd                   	std    
  8009d2:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8009d4:	fc                   	cld    
  8009d5:	eb 1d                	jmp    8009f4 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009d7:	89 f2                	mov    %esi,%edx
  8009d9:	09 c2                	or     %eax,%edx
  8009db:	f6 c2 03             	test   $0x3,%dl
  8009de:	75 0f                	jne    8009ef <memmove+0x5d>
  8009e0:	f6 c1 03             	test   $0x3,%cl
  8009e3:	75 0a                	jne    8009ef <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8009e5:	c1 e9 02             	shr    $0x2,%ecx
  8009e8:	89 c7                	mov    %eax,%edi
  8009ea:	fc                   	cld    
  8009eb:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8009ed:	eb 05                	jmp    8009f4 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8009ef:	89 c7                	mov    %eax,%edi
  8009f1:	fc                   	cld    
  8009f2:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8009f4:	5e                   	pop    %esi
  8009f5:	5f                   	pop    %edi
  8009f6:	5d                   	pop    %ebp
  8009f7:	c3                   	ret    

008009f8 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8009f8:	55                   	push   %ebp
  8009f9:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8009fb:	ff 75 10             	pushl  0x10(%ebp)
  8009fe:	ff 75 0c             	pushl  0xc(%ebp)
  800a01:	ff 75 08             	pushl  0x8(%ebp)
  800a04:	e8 89 ff ff ff       	call   800992 <memmove>
}
  800a09:	c9                   	leave  
  800a0a:	c3                   	ret    

00800a0b <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800a0b:	55                   	push   %ebp
  800a0c:	89 e5                	mov    %esp,%ebp
  800a0e:	56                   	push   %esi
  800a0f:	53                   	push   %ebx
  800a10:	8b 45 08             	mov    0x8(%ebp),%eax
  800a13:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a16:	89 c6                	mov    %eax,%esi
  800a18:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a1b:	eb 14                	jmp    800a31 <memcmp+0x26>
		if (*s1 != *s2)
  800a1d:	8a 08                	mov    (%eax),%cl
  800a1f:	8a 1a                	mov    (%edx),%bl
  800a21:	38 d9                	cmp    %bl,%cl
  800a23:	74 0a                	je     800a2f <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800a25:	0f b6 c1             	movzbl %cl,%eax
  800a28:	0f b6 db             	movzbl %bl,%ebx
  800a2b:	29 d8                	sub    %ebx,%eax
  800a2d:	eb 0b                	jmp    800a3a <memcmp+0x2f>
		s1++, s2++;
  800a2f:	40                   	inc    %eax
  800a30:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a31:	39 f0                	cmp    %esi,%eax
  800a33:	75 e8                	jne    800a1d <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800a35:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a3a:	5b                   	pop    %ebx
  800a3b:	5e                   	pop    %esi
  800a3c:	5d                   	pop    %ebp
  800a3d:	c3                   	ret    

00800a3e <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800a3e:	55                   	push   %ebp
  800a3f:	89 e5                	mov    %esp,%ebp
  800a41:	53                   	push   %ebx
  800a42:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800a45:	89 c1                	mov    %eax,%ecx
  800a47:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800a4a:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a4e:	eb 08                	jmp    800a58 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a50:	0f b6 10             	movzbl (%eax),%edx
  800a53:	39 da                	cmp    %ebx,%edx
  800a55:	74 05                	je     800a5c <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a57:	40                   	inc    %eax
  800a58:	39 c8                	cmp    %ecx,%eax
  800a5a:	72 f4                	jb     800a50 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800a5c:	5b                   	pop    %ebx
  800a5d:	5d                   	pop    %ebp
  800a5e:	c3                   	ret    

00800a5f <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800a5f:	55                   	push   %ebp
  800a60:	89 e5                	mov    %esp,%ebp
  800a62:	57                   	push   %edi
  800a63:	56                   	push   %esi
  800a64:	53                   	push   %ebx
  800a65:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a68:	eb 01                	jmp    800a6b <strtol+0xc>
		s++;
  800a6a:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a6b:	8a 01                	mov    (%ecx),%al
  800a6d:	3c 20                	cmp    $0x20,%al
  800a6f:	74 f9                	je     800a6a <strtol+0xb>
  800a71:	3c 09                	cmp    $0x9,%al
  800a73:	74 f5                	je     800a6a <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800a75:	3c 2b                	cmp    $0x2b,%al
  800a77:	75 08                	jne    800a81 <strtol+0x22>
		s++;
  800a79:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a7a:	bf 00 00 00 00       	mov    $0x0,%edi
  800a7f:	eb 11                	jmp    800a92 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800a81:	3c 2d                	cmp    $0x2d,%al
  800a83:	75 08                	jne    800a8d <strtol+0x2e>
		s++, neg = 1;
  800a85:	41                   	inc    %ecx
  800a86:	bf 01 00 00 00       	mov    $0x1,%edi
  800a8b:	eb 05                	jmp    800a92 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a8d:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a92:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a96:	0f 84 87 00 00 00    	je     800b23 <strtol+0xc4>
  800a9c:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800aa0:	75 27                	jne    800ac9 <strtol+0x6a>
  800aa2:	80 39 30             	cmpb   $0x30,(%ecx)
  800aa5:	75 22                	jne    800ac9 <strtol+0x6a>
  800aa7:	e9 88 00 00 00       	jmp    800b34 <strtol+0xd5>
		s += 2, base = 16;
  800aac:	83 c1 02             	add    $0x2,%ecx
  800aaf:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800ab6:	eb 11                	jmp    800ac9 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800ab8:	41                   	inc    %ecx
  800ab9:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800ac0:	eb 07                	jmp    800ac9 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800ac2:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800ac9:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800ace:	8a 11                	mov    (%ecx),%dl
  800ad0:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800ad3:	80 fb 09             	cmp    $0x9,%bl
  800ad6:	77 08                	ja     800ae0 <strtol+0x81>
			dig = *s - '0';
  800ad8:	0f be d2             	movsbl %dl,%edx
  800adb:	83 ea 30             	sub    $0x30,%edx
  800ade:	eb 22                	jmp    800b02 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800ae0:	8d 72 9f             	lea    -0x61(%edx),%esi
  800ae3:	89 f3                	mov    %esi,%ebx
  800ae5:	80 fb 19             	cmp    $0x19,%bl
  800ae8:	77 08                	ja     800af2 <strtol+0x93>
			dig = *s - 'a' + 10;
  800aea:	0f be d2             	movsbl %dl,%edx
  800aed:	83 ea 57             	sub    $0x57,%edx
  800af0:	eb 10                	jmp    800b02 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800af2:	8d 72 bf             	lea    -0x41(%edx),%esi
  800af5:	89 f3                	mov    %esi,%ebx
  800af7:	80 fb 19             	cmp    $0x19,%bl
  800afa:	77 14                	ja     800b10 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800afc:	0f be d2             	movsbl %dl,%edx
  800aff:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800b02:	3b 55 10             	cmp    0x10(%ebp),%edx
  800b05:	7d 09                	jge    800b10 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800b07:	41                   	inc    %ecx
  800b08:	0f af 45 10          	imul   0x10(%ebp),%eax
  800b0c:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800b0e:	eb be                	jmp    800ace <strtol+0x6f>

	if (endptr)
  800b10:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800b14:	74 05                	je     800b1b <strtol+0xbc>
		*endptr = (char *) s;
  800b16:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b19:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800b1b:	85 ff                	test   %edi,%edi
  800b1d:	74 21                	je     800b40 <strtol+0xe1>
  800b1f:	f7 d8                	neg    %eax
  800b21:	eb 1d                	jmp    800b40 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b23:	80 39 30             	cmpb   $0x30,(%ecx)
  800b26:	75 9a                	jne    800ac2 <strtol+0x63>
  800b28:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b2c:	0f 84 7a ff ff ff    	je     800aac <strtol+0x4d>
  800b32:	eb 84                	jmp    800ab8 <strtol+0x59>
  800b34:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b38:	0f 84 6e ff ff ff    	je     800aac <strtol+0x4d>
  800b3e:	eb 89                	jmp    800ac9 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800b40:	5b                   	pop    %ebx
  800b41:	5e                   	pop    %esi
  800b42:	5f                   	pop    %edi
  800b43:	5d                   	pop    %ebp
  800b44:	c3                   	ret    

00800b45 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800b45:	55                   	push   %ebp
  800b46:	89 e5                	mov    %esp,%ebp
  800b48:	57                   	push   %edi
  800b49:	56                   	push   %esi
  800b4a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b4b:	b8 00 00 00 00       	mov    $0x0,%eax
  800b50:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b53:	8b 55 08             	mov    0x8(%ebp),%edx
  800b56:	89 c3                	mov    %eax,%ebx
  800b58:	89 c7                	mov    %eax,%edi
  800b5a:	89 c6                	mov    %eax,%esi
  800b5c:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800b5e:	5b                   	pop    %ebx
  800b5f:	5e                   	pop    %esi
  800b60:	5f                   	pop    %edi
  800b61:	5d                   	pop    %ebp
  800b62:	c3                   	ret    

00800b63 <sys_cgetc>:

int
sys_cgetc(void)
{
  800b63:	55                   	push   %ebp
  800b64:	89 e5                	mov    %esp,%ebp
  800b66:	57                   	push   %edi
  800b67:	56                   	push   %esi
  800b68:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b69:	ba 00 00 00 00       	mov    $0x0,%edx
  800b6e:	b8 01 00 00 00       	mov    $0x1,%eax
  800b73:	89 d1                	mov    %edx,%ecx
  800b75:	89 d3                	mov    %edx,%ebx
  800b77:	89 d7                	mov    %edx,%edi
  800b79:	89 d6                	mov    %edx,%esi
  800b7b:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800b7d:	5b                   	pop    %ebx
  800b7e:	5e                   	pop    %esi
  800b7f:	5f                   	pop    %edi
  800b80:	5d                   	pop    %ebp
  800b81:	c3                   	ret    

00800b82 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800b82:	55                   	push   %ebp
  800b83:	89 e5                	mov    %esp,%ebp
  800b85:	57                   	push   %edi
  800b86:	56                   	push   %esi
  800b87:	53                   	push   %ebx
  800b88:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b8b:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b90:	b8 03 00 00 00       	mov    $0x3,%eax
  800b95:	8b 55 08             	mov    0x8(%ebp),%edx
  800b98:	89 cb                	mov    %ecx,%ebx
  800b9a:	89 cf                	mov    %ecx,%edi
  800b9c:	89 ce                	mov    %ecx,%esi
  800b9e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ba0:	85 c0                	test   %eax,%eax
  800ba2:	7e 17                	jle    800bbb <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ba4:	83 ec 0c             	sub    $0xc,%esp
  800ba7:	50                   	push   %eax
  800ba8:	6a 03                	push   $0x3
  800baa:	68 df 2b 80 00       	push   $0x802bdf
  800baf:	6a 23                	push   $0x23
  800bb1:	68 fc 2b 80 00       	push   $0x802bfc
  800bb6:	e8 26 f6 ff ff       	call   8001e1 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800bbb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bbe:	5b                   	pop    %ebx
  800bbf:	5e                   	pop    %esi
  800bc0:	5f                   	pop    %edi
  800bc1:	5d                   	pop    %ebp
  800bc2:	c3                   	ret    

00800bc3 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800bc3:	55                   	push   %ebp
  800bc4:	89 e5                	mov    %esp,%ebp
  800bc6:	57                   	push   %edi
  800bc7:	56                   	push   %esi
  800bc8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc9:	ba 00 00 00 00       	mov    $0x0,%edx
  800bce:	b8 02 00 00 00       	mov    $0x2,%eax
  800bd3:	89 d1                	mov    %edx,%ecx
  800bd5:	89 d3                	mov    %edx,%ebx
  800bd7:	89 d7                	mov    %edx,%edi
  800bd9:	89 d6                	mov    %edx,%esi
  800bdb:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800bdd:	5b                   	pop    %ebx
  800bde:	5e                   	pop    %esi
  800bdf:	5f                   	pop    %edi
  800be0:	5d                   	pop    %ebp
  800be1:	c3                   	ret    

00800be2 <sys_yield>:

void
sys_yield(void)
{
  800be2:	55                   	push   %ebp
  800be3:	89 e5                	mov    %esp,%ebp
  800be5:	57                   	push   %edi
  800be6:	56                   	push   %esi
  800be7:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800be8:	ba 00 00 00 00       	mov    $0x0,%edx
  800bed:	b8 0b 00 00 00       	mov    $0xb,%eax
  800bf2:	89 d1                	mov    %edx,%ecx
  800bf4:	89 d3                	mov    %edx,%ebx
  800bf6:	89 d7                	mov    %edx,%edi
  800bf8:	89 d6                	mov    %edx,%esi
  800bfa:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800bfc:	5b                   	pop    %ebx
  800bfd:	5e                   	pop    %esi
  800bfe:	5f                   	pop    %edi
  800bff:	5d                   	pop    %ebp
  800c00:	c3                   	ret    

00800c01 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800c01:	55                   	push   %ebp
  800c02:	89 e5                	mov    %esp,%ebp
  800c04:	57                   	push   %edi
  800c05:	56                   	push   %esi
  800c06:	53                   	push   %ebx
  800c07:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c0a:	be 00 00 00 00       	mov    $0x0,%esi
  800c0f:	b8 04 00 00 00       	mov    $0x4,%eax
  800c14:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c17:	8b 55 08             	mov    0x8(%ebp),%edx
  800c1a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c1d:	89 f7                	mov    %esi,%edi
  800c1f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c21:	85 c0                	test   %eax,%eax
  800c23:	7e 17                	jle    800c3c <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c25:	83 ec 0c             	sub    $0xc,%esp
  800c28:	50                   	push   %eax
  800c29:	6a 04                	push   $0x4
  800c2b:	68 df 2b 80 00       	push   $0x802bdf
  800c30:	6a 23                	push   $0x23
  800c32:	68 fc 2b 80 00       	push   $0x802bfc
  800c37:	e8 a5 f5 ff ff       	call   8001e1 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800c3c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c3f:	5b                   	pop    %ebx
  800c40:	5e                   	pop    %esi
  800c41:	5f                   	pop    %edi
  800c42:	5d                   	pop    %ebp
  800c43:	c3                   	ret    

00800c44 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800c44:	55                   	push   %ebp
  800c45:	89 e5                	mov    %esp,%ebp
  800c47:	57                   	push   %edi
  800c48:	56                   	push   %esi
  800c49:	53                   	push   %ebx
  800c4a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c4d:	b8 05 00 00 00       	mov    $0x5,%eax
  800c52:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c55:	8b 55 08             	mov    0x8(%ebp),%edx
  800c58:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c5b:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c5e:	8b 75 18             	mov    0x18(%ebp),%esi
  800c61:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c63:	85 c0                	test   %eax,%eax
  800c65:	7e 17                	jle    800c7e <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c67:	83 ec 0c             	sub    $0xc,%esp
  800c6a:	50                   	push   %eax
  800c6b:	6a 05                	push   $0x5
  800c6d:	68 df 2b 80 00       	push   $0x802bdf
  800c72:	6a 23                	push   $0x23
  800c74:	68 fc 2b 80 00       	push   $0x802bfc
  800c79:	e8 63 f5 ff ff       	call   8001e1 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800c7e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c81:	5b                   	pop    %ebx
  800c82:	5e                   	pop    %esi
  800c83:	5f                   	pop    %edi
  800c84:	5d                   	pop    %ebp
  800c85:	c3                   	ret    

00800c86 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800c86:	55                   	push   %ebp
  800c87:	89 e5                	mov    %esp,%ebp
  800c89:	57                   	push   %edi
  800c8a:	56                   	push   %esi
  800c8b:	53                   	push   %ebx
  800c8c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c8f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c94:	b8 06 00 00 00       	mov    $0x6,%eax
  800c99:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c9c:	8b 55 08             	mov    0x8(%ebp),%edx
  800c9f:	89 df                	mov    %ebx,%edi
  800ca1:	89 de                	mov    %ebx,%esi
  800ca3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ca5:	85 c0                	test   %eax,%eax
  800ca7:	7e 17                	jle    800cc0 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ca9:	83 ec 0c             	sub    $0xc,%esp
  800cac:	50                   	push   %eax
  800cad:	6a 06                	push   $0x6
  800caf:	68 df 2b 80 00       	push   $0x802bdf
  800cb4:	6a 23                	push   $0x23
  800cb6:	68 fc 2b 80 00       	push   $0x802bfc
  800cbb:	e8 21 f5 ff ff       	call   8001e1 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800cc0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cc3:	5b                   	pop    %ebx
  800cc4:	5e                   	pop    %esi
  800cc5:	5f                   	pop    %edi
  800cc6:	5d                   	pop    %ebp
  800cc7:	c3                   	ret    

00800cc8 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800cc8:	55                   	push   %ebp
  800cc9:	89 e5                	mov    %esp,%ebp
  800ccb:	57                   	push   %edi
  800ccc:	56                   	push   %esi
  800ccd:	53                   	push   %ebx
  800cce:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cd1:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cd6:	b8 08 00 00 00       	mov    $0x8,%eax
  800cdb:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cde:	8b 55 08             	mov    0x8(%ebp),%edx
  800ce1:	89 df                	mov    %ebx,%edi
  800ce3:	89 de                	mov    %ebx,%esi
  800ce5:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ce7:	85 c0                	test   %eax,%eax
  800ce9:	7e 17                	jle    800d02 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ceb:	83 ec 0c             	sub    $0xc,%esp
  800cee:	50                   	push   %eax
  800cef:	6a 08                	push   $0x8
  800cf1:	68 df 2b 80 00       	push   $0x802bdf
  800cf6:	6a 23                	push   $0x23
  800cf8:	68 fc 2b 80 00       	push   $0x802bfc
  800cfd:	e8 df f4 ff ff       	call   8001e1 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800d02:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d05:	5b                   	pop    %ebx
  800d06:	5e                   	pop    %esi
  800d07:	5f                   	pop    %edi
  800d08:	5d                   	pop    %ebp
  800d09:	c3                   	ret    

00800d0a <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800d0a:	55                   	push   %ebp
  800d0b:	89 e5                	mov    %esp,%ebp
  800d0d:	57                   	push   %edi
  800d0e:	56                   	push   %esi
  800d0f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d10:	ba 00 00 00 00       	mov    $0x0,%edx
  800d15:	b8 0c 00 00 00       	mov    $0xc,%eax
  800d1a:	89 d1                	mov    %edx,%ecx
  800d1c:	89 d3                	mov    %edx,%ebx
  800d1e:	89 d7                	mov    %edx,%edi
  800d20:	89 d6                	mov    %edx,%esi
  800d22:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800d24:	5b                   	pop    %ebx
  800d25:	5e                   	pop    %esi
  800d26:	5f                   	pop    %edi
  800d27:	5d                   	pop    %ebp
  800d28:	c3                   	ret    

00800d29 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800d29:	55                   	push   %ebp
  800d2a:	89 e5                	mov    %esp,%ebp
  800d2c:	57                   	push   %edi
  800d2d:	56                   	push   %esi
  800d2e:	53                   	push   %ebx
  800d2f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d32:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d37:	b8 09 00 00 00       	mov    $0x9,%eax
  800d3c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d3f:	8b 55 08             	mov    0x8(%ebp),%edx
  800d42:	89 df                	mov    %ebx,%edi
  800d44:	89 de                	mov    %ebx,%esi
  800d46:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d48:	85 c0                	test   %eax,%eax
  800d4a:	7e 17                	jle    800d63 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d4c:	83 ec 0c             	sub    $0xc,%esp
  800d4f:	50                   	push   %eax
  800d50:	6a 09                	push   $0x9
  800d52:	68 df 2b 80 00       	push   $0x802bdf
  800d57:	6a 23                	push   $0x23
  800d59:	68 fc 2b 80 00       	push   $0x802bfc
  800d5e:	e8 7e f4 ff ff       	call   8001e1 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800d63:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d66:	5b                   	pop    %ebx
  800d67:	5e                   	pop    %esi
  800d68:	5f                   	pop    %edi
  800d69:	5d                   	pop    %ebp
  800d6a:	c3                   	ret    

00800d6b <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800d6b:	55                   	push   %ebp
  800d6c:	89 e5                	mov    %esp,%ebp
  800d6e:	57                   	push   %edi
  800d6f:	56                   	push   %esi
  800d70:	53                   	push   %ebx
  800d71:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d74:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d79:	b8 0a 00 00 00       	mov    $0xa,%eax
  800d7e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d81:	8b 55 08             	mov    0x8(%ebp),%edx
  800d84:	89 df                	mov    %ebx,%edi
  800d86:	89 de                	mov    %ebx,%esi
  800d88:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d8a:	85 c0                	test   %eax,%eax
  800d8c:	7e 17                	jle    800da5 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d8e:	83 ec 0c             	sub    $0xc,%esp
  800d91:	50                   	push   %eax
  800d92:	6a 0a                	push   $0xa
  800d94:	68 df 2b 80 00       	push   $0x802bdf
  800d99:	6a 23                	push   $0x23
  800d9b:	68 fc 2b 80 00       	push   $0x802bfc
  800da0:	e8 3c f4 ff ff       	call   8001e1 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800da5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800da8:	5b                   	pop    %ebx
  800da9:	5e                   	pop    %esi
  800daa:	5f                   	pop    %edi
  800dab:	5d                   	pop    %ebp
  800dac:	c3                   	ret    

00800dad <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800dad:	55                   	push   %ebp
  800dae:	89 e5                	mov    %esp,%ebp
  800db0:	57                   	push   %edi
  800db1:	56                   	push   %esi
  800db2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800db3:	be 00 00 00 00       	mov    $0x0,%esi
  800db8:	b8 0d 00 00 00       	mov    $0xd,%eax
  800dbd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800dc0:	8b 55 08             	mov    0x8(%ebp),%edx
  800dc3:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800dc6:	8b 7d 14             	mov    0x14(%ebp),%edi
  800dc9:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800dcb:	5b                   	pop    %ebx
  800dcc:	5e                   	pop    %esi
  800dcd:	5f                   	pop    %edi
  800dce:	5d                   	pop    %ebp
  800dcf:	c3                   	ret    

00800dd0 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800dd0:	55                   	push   %ebp
  800dd1:	89 e5                	mov    %esp,%ebp
  800dd3:	57                   	push   %edi
  800dd4:	56                   	push   %esi
  800dd5:	53                   	push   %ebx
  800dd6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dd9:	b9 00 00 00 00       	mov    $0x0,%ecx
  800dde:	b8 0e 00 00 00       	mov    $0xe,%eax
  800de3:	8b 55 08             	mov    0x8(%ebp),%edx
  800de6:	89 cb                	mov    %ecx,%ebx
  800de8:	89 cf                	mov    %ecx,%edi
  800dea:	89 ce                	mov    %ecx,%esi
  800dec:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800dee:	85 c0                	test   %eax,%eax
  800df0:	7e 17                	jle    800e09 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800df2:	83 ec 0c             	sub    $0xc,%esp
  800df5:	50                   	push   %eax
  800df6:	6a 0e                	push   $0xe
  800df8:	68 df 2b 80 00       	push   $0x802bdf
  800dfd:	6a 23                	push   $0x23
  800dff:	68 fc 2b 80 00       	push   $0x802bfc
  800e04:	e8 d8 f3 ff ff       	call   8001e1 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800e09:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e0c:	5b                   	pop    %ebx
  800e0d:	5e                   	pop    %esi
  800e0e:	5f                   	pop    %edi
  800e0f:	5d                   	pop    %ebp
  800e10:	c3                   	ret    

00800e11 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800e11:	55                   	push   %ebp
  800e12:	89 e5                	mov    %esp,%ebp
  800e14:	53                   	push   %ebx
  800e15:	83 ec 04             	sub    $0x4,%esp
  800e18:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800e1b:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800e1d:	89 da                	mov    %ebx,%edx
  800e1f:	c1 ea 0c             	shr    $0xc,%edx
  800e22:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800e29:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800e2d:	74 05                	je     800e34 <pgfault+0x23>
  800e2f:	f6 c6 08             	test   $0x8,%dh
  800e32:	75 14                	jne    800e48 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800e34:	83 ec 04             	sub    $0x4,%esp
  800e37:	68 0c 2c 80 00       	push   $0x802c0c
  800e3c:	6a 26                	push   $0x26
  800e3e:	68 70 2c 80 00       	push   $0x802c70
  800e43:	e8 99 f3 ff ff       	call   8001e1 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800e48:	83 ec 04             	sub    $0x4,%esp
  800e4b:	6a 07                	push   $0x7
  800e4d:	68 00 f0 7f 00       	push   $0x7ff000
  800e52:	6a 00                	push   $0x0
  800e54:	e8 a8 fd ff ff       	call   800c01 <sys_page_alloc>
  800e59:	83 c4 10             	add    $0x10,%esp
  800e5c:	85 c0                	test   %eax,%eax
  800e5e:	74 14                	je     800e74 <pgfault+0x63>
        panic("pgfault: no phys mem");
  800e60:	83 ec 04             	sub    $0x4,%esp
  800e63:	68 7b 2c 80 00       	push   $0x802c7b
  800e68:	6a 32                	push   $0x32
  800e6a:	68 70 2c 80 00       	push   $0x802c70
  800e6f:	e8 6d f3 ff ff       	call   8001e1 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800e74:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800e7a:	83 ec 04             	sub    $0x4,%esp
  800e7d:	68 00 10 00 00       	push   $0x1000
  800e82:	53                   	push   %ebx
  800e83:	68 00 f0 7f 00       	push   $0x7ff000
  800e88:	e8 05 fb ff ff       	call   800992 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800e8d:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800e94:	53                   	push   %ebx
  800e95:	6a 00                	push   $0x0
  800e97:	68 00 f0 7f 00       	push   $0x7ff000
  800e9c:	6a 00                	push   $0x0
  800e9e:	e8 a1 fd ff ff       	call   800c44 <sys_page_map>
  800ea3:	83 c4 20             	add    $0x20,%esp
  800ea6:	85 c0                	test   %eax,%eax
  800ea8:	74 14                	je     800ebe <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800eaa:	83 ec 04             	sub    $0x4,%esp
  800ead:	68 90 2c 80 00       	push   $0x802c90
  800eb2:	6a 3f                	push   $0x3f
  800eb4:	68 70 2c 80 00       	push   $0x802c70
  800eb9:	e8 23 f3 ff ff       	call   8001e1 <_panic>


	//panic("pgfault not implemented");
}
  800ebe:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ec1:	c9                   	leave  
  800ec2:	c3                   	ret    

00800ec3 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800ec3:	55                   	push   %ebp
  800ec4:	89 e5                	mov    %esp,%ebp
  800ec6:	57                   	push   %edi
  800ec7:	56                   	push   %esi
  800ec8:	53                   	push   %ebx
  800ec9:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800ecc:	68 11 0e 80 00       	push   $0x800e11
  800ed1:	e8 50 08 00 00       	call   801726 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800ed6:	b8 07 00 00 00       	mov    $0x7,%eax
  800edb:	cd 30                	int    $0x30
  800edd:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800ee0:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800ee3:	83 c4 10             	add    $0x10,%esp
  800ee6:	85 c0                	test   %eax,%eax
  800ee8:	79 17                	jns    800f01 <fork+0x3e>
        panic("fork: cannot create child env");
  800eea:	83 ec 04             	sub    $0x4,%esp
  800eed:	68 a3 2c 80 00       	push   $0x802ca3
  800ef2:	68 97 00 00 00       	push   $0x97
  800ef7:	68 70 2c 80 00       	push   $0x802c70
  800efc:	e8 e0 f2 ff ff       	call   8001e1 <_panic>
    } else if (envid == 0) {
  800f01:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800f05:	75 2a                	jne    800f31 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800f07:	e8 b7 fc ff ff       	call   800bc3 <sys_getenvid>
  800f0c:	25 ff 03 00 00       	and    $0x3ff,%eax
  800f11:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f18:	c1 e0 07             	shl    $0x7,%eax
  800f1b:	29 d0                	sub    %edx,%eax
  800f1d:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800f22:	a3 04 40 80 00       	mov    %eax,0x804004
        return 0;
  800f27:	b8 00 00 00 00       	mov    $0x0,%eax
  800f2c:	e9 94 01 00 00       	jmp    8010c5 <fork+0x202>
  800f31:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800f38:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800f3b:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800f42:	a8 01                	test   $0x1,%al
  800f44:	0f 84 0a 01 00 00    	je     801054 <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800f4a:	c1 e7 0a             	shl    $0xa,%edi
  800f4d:	be 00 00 00 00       	mov    $0x0,%esi
  800f52:	89 fb                	mov    %edi,%ebx
  800f54:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800f56:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800f5c:	75 34                	jne    800f92 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800f5e:	83 ec 04             	sub    $0x4,%esp
  800f61:	6a 07                	push   $0x7
  800f63:	68 00 f0 bf ee       	push   $0xeebff000
  800f68:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f6b:	e8 91 fc ff ff       	call   800c01 <sys_page_alloc>
  800f70:	83 c4 10             	add    $0x10,%esp
  800f73:	85 c0                	test   %eax,%eax
  800f75:	0f 84 cc 00 00 00    	je     801047 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800f7b:	83 ec 04             	sub    $0x4,%esp
  800f7e:	68 c1 2c 80 00       	push   $0x802cc1
  800f83:	68 ad 00 00 00       	push   $0xad
  800f88:	68 70 2c 80 00       	push   $0x802c70
  800f8d:	e8 4f f2 ff ff       	call   8001e1 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800f92:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800f99:	a8 01                	test   $0x1,%al
  800f9b:	0f 84 a6 00 00 00    	je     801047 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800fa1:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800fa8:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800fab:	a9 02 08 00 00       	test   $0x802,%eax
  800fb0:	74 62                	je     801014 <fork+0x151>
  800fb2:	f6 c4 04             	test   $0x4,%ah
  800fb5:	75 78                	jne    80102f <fork+0x16c>
        if (sys_page_map(0,
  800fb7:	83 ec 0c             	sub    $0xc,%esp
  800fba:	68 05 08 00 00       	push   $0x805
  800fbf:	53                   	push   %ebx
  800fc0:	ff 75 e4             	pushl  -0x1c(%ebp)
  800fc3:	53                   	push   %ebx
  800fc4:	6a 00                	push   $0x0
  800fc6:	e8 79 fc ff ff       	call   800c44 <sys_page_map>
  800fcb:	83 c4 20             	add    $0x20,%esp
  800fce:	85 c0                	test   %eax,%eax
  800fd0:	74 14                	je     800fe6 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800fd2:	83 ec 04             	sub    $0x4,%esp
  800fd5:	68 dc 2c 80 00       	push   $0x802cdc
  800fda:	6a 64                	push   $0x64
  800fdc:	68 70 2c 80 00       	push   $0x802c70
  800fe1:	e8 fb f1 ff ff       	call   8001e1 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800fe6:	83 ec 0c             	sub    $0xc,%esp
  800fe9:	68 05 08 00 00       	push   $0x805
  800fee:	53                   	push   %ebx
  800fef:	6a 00                	push   $0x0
  800ff1:	53                   	push   %ebx
  800ff2:	6a 00                	push   $0x0
  800ff4:	e8 4b fc ff ff       	call   800c44 <sys_page_map>
  800ff9:	83 c4 20             	add    $0x20,%esp
  800ffc:	85 c0                	test   %eax,%eax
  800ffe:	74 47                	je     801047 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  801000:	83 ec 04             	sub    $0x4,%esp
  801003:	68 f3 2c 80 00       	push   $0x802cf3
  801008:	6a 6d                	push   $0x6d
  80100a:	68 70 2c 80 00       	push   $0x802c70
  80100f:	e8 cd f1 ff ff       	call   8001e1 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  801014:	f6 c4 04             	test   $0x4,%ah
  801017:	75 16                	jne    80102f <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  801019:	83 ec 0c             	sub    $0xc,%esp
  80101c:	6a 05                	push   $0x5
  80101e:	53                   	push   %ebx
  80101f:	ff 75 e4             	pushl  -0x1c(%ebp)
  801022:	53                   	push   %ebx
  801023:	6a 00                	push   $0x0
  801025:	e8 1a fc ff ff       	call   800c44 <sys_page_map>
  80102a:	83 c4 20             	add    $0x20,%esp
  80102d:	eb 18                	jmp    801047 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  80102f:	83 ec 0c             	sub    $0xc,%esp
  801032:	25 07 0e 00 00       	and    $0xe07,%eax
  801037:	50                   	push   %eax
  801038:	53                   	push   %ebx
  801039:	ff 75 e4             	pushl  -0x1c(%ebp)
  80103c:	53                   	push   %ebx
  80103d:	6a 00                	push   $0x0
  80103f:	e8 00 fc ff ff       	call   800c44 <sys_page_map>
  801044:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801047:	46                   	inc    %esi
  801048:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  80104e:	0f 85 fe fe ff ff    	jne    800f52 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801054:	ff 45 e0             	incl   -0x20(%ebp)
  801057:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80105a:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  80105f:	0f 85 d3 fe ff ff    	jne    800f38 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  801065:	a1 04 40 80 00       	mov    0x804004,%eax
  80106a:	8b 40 64             	mov    0x64(%eax),%eax
  80106d:	83 ec 08             	sub    $0x8,%esp
  801070:	50                   	push   %eax
  801071:	ff 75 dc             	pushl  -0x24(%ebp)
  801074:	e8 f2 fc ff ff       	call   800d6b <sys_env_set_pgfault_upcall>
  801079:	83 c4 10             	add    $0x10,%esp
  80107c:	85 c0                	test   %eax,%eax
  80107e:	74 17                	je     801097 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  801080:	83 ec 04             	sub    $0x4,%esp
  801083:	68 50 2c 80 00       	push   $0x802c50
  801088:	68 b9 00 00 00       	push   $0xb9
  80108d:	68 70 2c 80 00       	push   $0x802c70
  801092:	e8 4a f1 ff ff       	call   8001e1 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  801097:	83 ec 08             	sub    $0x8,%esp
  80109a:	6a 02                	push   $0x2
  80109c:	ff 75 dc             	pushl  -0x24(%ebp)
  80109f:	e8 24 fc ff ff       	call   800cc8 <sys_env_set_status>
  8010a4:	83 c4 10             	add    $0x10,%esp
  8010a7:	85 c0                	test   %eax,%eax
  8010a9:	74 17                	je     8010c2 <fork+0x1ff>
        panic("fork: cannot set env status");
  8010ab:	83 ec 04             	sub    $0x4,%esp
  8010ae:	68 0e 2d 80 00       	push   $0x802d0e
  8010b3:	68 bd 00 00 00       	push   $0xbd
  8010b8:	68 70 2c 80 00       	push   $0x802c70
  8010bd:	e8 1f f1 ff ff       	call   8001e1 <_panic>

    return envid;
  8010c2:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  8010c5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010c8:	5b                   	pop    %ebx
  8010c9:	5e                   	pop    %esi
  8010ca:	5f                   	pop    %edi
  8010cb:	5d                   	pop    %ebp
  8010cc:	c3                   	ret    

008010cd <sfork>:

// Challenge!
int
sfork(void)
{
  8010cd:	55                   	push   %ebp
  8010ce:	89 e5                	mov    %esp,%ebp
  8010d0:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  8010d3:	68 2a 2d 80 00       	push   $0x802d2a
  8010d8:	68 c8 00 00 00       	push   $0xc8
  8010dd:	68 70 2c 80 00       	push   $0x802c70
  8010e2:	e8 fa f0 ff ff       	call   8001e1 <_panic>

008010e7 <spawn>:
// argv: pointer to null-terminated array of pointers to strings,
// 	 which will be passed to the child as its command-line arguments.
// Returns child envid on success, < 0 on failure.
int
spawn(const char *prog, const char **argv)
{
  8010e7:	55                   	push   %ebp
  8010e8:	89 e5                	mov    %esp,%ebp
  8010ea:	57                   	push   %edi
  8010eb:	56                   	push   %esi
  8010ec:	53                   	push   %ebx
  8010ed:	81 ec 94 02 00 00    	sub    $0x294,%esp
  8010f3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//   - Call sys_env_set_trapframe(child, &child_tf) to set up the
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
  8010f6:	6a 00                	push   $0x0
  8010f8:	ff 75 08             	pushl  0x8(%ebp)
  8010fb:	e8 e8 0d 00 00       	call   801ee8 <open>
  801100:	89 c1                	mov    %eax,%ecx
  801102:	89 85 8c fd ff ff    	mov    %eax,-0x274(%ebp)
  801108:	83 c4 10             	add    $0x10,%esp
  80110b:	85 c0                	test   %eax,%eax
  80110d:	0f 88 e2 04 00 00    	js     8015f5 <spawn+0x50e>
		return r;
	fd = r;

	// Read elf header
	elf = (struct Elf*) elf_buf;
	if (readn(fd, elf_buf, sizeof(elf_buf)) != sizeof(elf_buf)
  801113:	83 ec 04             	sub    $0x4,%esp
  801116:	68 00 02 00 00       	push   $0x200
  80111b:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  801121:	50                   	push   %eax
  801122:	51                   	push   %ecx
  801123:	e8 e0 09 00 00       	call   801b08 <readn>
  801128:	83 c4 10             	add    $0x10,%esp
  80112b:	3d 00 02 00 00       	cmp    $0x200,%eax
  801130:	75 0c                	jne    80113e <spawn+0x57>
	    || elf->e_magic != ELF_MAGIC) {
  801132:	81 bd e8 fd ff ff 7f 	cmpl   $0x464c457f,-0x218(%ebp)
  801139:	45 4c 46 
  80113c:	74 33                	je     801171 <spawn+0x8a>
		close(fd);
  80113e:	83 ec 0c             	sub    $0xc,%esp
  801141:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801147:	e8 fd 07 00 00       	call   801949 <close>
		cprintf("elf magic %08x want %08x\n", elf->e_magic, ELF_MAGIC);
  80114c:	83 c4 0c             	add    $0xc,%esp
  80114f:	68 7f 45 4c 46       	push   $0x464c457f
  801154:	ff b5 e8 fd ff ff    	pushl  -0x218(%ebp)
  80115a:	68 40 2d 80 00       	push   $0x802d40
  80115f:	e8 55 f1 ff ff       	call   8002b9 <cprintf>
		return -E_NOT_EXEC;
  801164:	83 c4 10             	add    $0x10,%esp
  801167:	bb f2 ff ff ff       	mov    $0xfffffff2,%ebx
  80116c:	e9 e4 04 00 00       	jmp    801655 <spawn+0x56e>
  801171:	b8 07 00 00 00       	mov    $0x7,%eax
  801176:	cd 30                	int    $0x30
  801178:	89 85 74 fd ff ff    	mov    %eax,-0x28c(%ebp)
  80117e:	89 85 84 fd ff ff    	mov    %eax,-0x27c(%ebp)
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
  801184:	85 c0                	test   %eax,%eax
  801186:	0f 88 71 04 00 00    	js     8015fd <spawn+0x516>
		return r;
	child = r;

	// Set up trap frame, including initial stack.
	child_tf = envs[ENVX(child)].env_tf;
  80118c:	25 ff 03 00 00       	and    $0x3ff,%eax
  801191:	89 c6                	mov    %eax,%esi
  801193:	8d 04 85 00 00 00 00 	lea    0x0(,%eax,4),%eax
  80119a:	c1 e6 07             	shl    $0x7,%esi
  80119d:	29 c6                	sub    %eax,%esi
  80119f:	81 c6 00 00 c0 ee    	add    $0xeec00000,%esi
  8011a5:	8d bd a4 fd ff ff    	lea    -0x25c(%ebp),%edi
  8011ab:	b9 11 00 00 00       	mov    $0x11,%ecx
  8011b0:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	child_tf.tf_eip = elf->e_entry;
  8011b2:	8b 85 00 fe ff ff    	mov    -0x200(%ebp),%eax
  8011b8:	89 85 d4 fd ff ff    	mov    %eax,-0x22c(%ebp)
  8011be:	ba 00 00 00 00       	mov    $0x0,%edx
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  8011c3:	b8 00 00 00 00       	mov    $0x0,%eax
	char *string_store;
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
  8011c8:	bf 00 00 00 00       	mov    $0x0,%edi
  8011cd:	89 5d 0c             	mov    %ebx,0xc(%ebp)
  8011d0:	89 c3                	mov    %eax,%ebx
  8011d2:	eb 13                	jmp    8011e7 <spawn+0x100>
	for (argc = 0; argv[argc] != 0; argc++)
		string_size += strlen(argv[argc]) + 1;
  8011d4:	83 ec 0c             	sub    $0xc,%esp
  8011d7:	50                   	push   %eax
  8011d8:	e8 0b f6 ff ff       	call   8007e8 <strlen>
  8011dd:	8d 7c 38 01          	lea    0x1(%eax,%edi,1),%edi
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  8011e1:	43                   	inc    %ebx
  8011e2:	89 f2                	mov    %esi,%edx
  8011e4:	83 c4 10             	add    $0x10,%esp
  8011e7:	89 d9                	mov    %ebx,%ecx
  8011e9:	8d 72 04             	lea    0x4(%edx),%esi
  8011ec:	8b 45 0c             	mov    0xc(%ebp),%eax
  8011ef:	8b 44 30 fc          	mov    -0x4(%eax,%esi,1),%eax
  8011f3:	85 c0                	test   %eax,%eax
  8011f5:	75 dd                	jne    8011d4 <spawn+0xed>
  8011f7:	89 9d 90 fd ff ff    	mov    %ebx,-0x270(%ebp)
  8011fd:	89 9d 88 fd ff ff    	mov    %ebx,-0x278(%ebp)
  801203:	89 95 80 fd ff ff    	mov    %edx,-0x280(%ebp)
  801209:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Determine where to place the strings and the argv array.
	// Set up pointers into the temporary page 'UTEMP'; we'll map a page
	// there later, then remap that page into the child environment
	// at (USTACKTOP - PGSIZE).
	// strings is the topmost thing on the stack.
	string_store = (char*) UTEMP + PGSIZE - string_size;
  80120c:	b8 00 10 40 00       	mov    $0x401000,%eax
  801211:	29 f8                	sub    %edi,%eax
  801213:	89 c7                	mov    %eax,%edi
	// argv is below that.  There's one argument pointer per argument, plus
	// a null pointer.
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));
  801215:	89 c2                	mov    %eax,%edx
  801217:	83 e2 fc             	and    $0xfffffffc,%edx
  80121a:	8d 04 8d 04 00 00 00 	lea    0x4(,%ecx,4),%eax
  801221:	29 c2                	sub    %eax,%edx
  801223:	89 95 94 fd ff ff    	mov    %edx,-0x26c(%ebp)

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
  801229:	8d 42 f8             	lea    -0x8(%edx),%eax
  80122c:	3d ff ff 3f 00       	cmp    $0x3fffff,%eax
  801231:	0f 86 d6 03 00 00    	jbe    80160d <spawn+0x526>
		return -E_NO_MEM;

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801237:	83 ec 04             	sub    $0x4,%esp
  80123a:	6a 07                	push   $0x7
  80123c:	68 00 00 40 00       	push   $0x400000
  801241:	6a 00                	push   $0x0
  801243:	e8 b9 f9 ff ff       	call   800c01 <sys_page_alloc>
  801248:	83 c4 10             	add    $0x10,%esp
  80124b:	85 c0                	test   %eax,%eax
  80124d:	0f 88 c1 03 00 00    	js     801614 <spawn+0x52d>
  801253:	be 00 00 00 00       	mov    $0x0,%esi
  801258:	eb 2e                	jmp    801288 <spawn+0x1a1>
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
		argv_store[i] = UTEMP2USTACK(string_store);
  80125a:	8d 87 00 d0 7f ee    	lea    -0x11803000(%edi),%eax
  801260:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  801266:	89 04 b1             	mov    %eax,(%ecx,%esi,4)
		strcpy(string_store, argv[i]);
  801269:	83 ec 08             	sub    $0x8,%esp
  80126c:	ff 34 b3             	pushl  (%ebx,%esi,4)
  80126f:	57                   	push   %edi
  801270:	e8 a8 f5 ff ff       	call   80081d <strcpy>
		string_store += strlen(argv[i]) + 1;
  801275:	83 c4 04             	add    $0x4,%esp
  801278:	ff 34 b3             	pushl  (%ebx,%esi,4)
  80127b:	e8 68 f5 ff ff       	call   8007e8 <strlen>
  801280:	8d 7c 07 01          	lea    0x1(%edi,%eax,1),%edi
	//	  (Again, argv should use an address valid in the child's
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
  801284:	46                   	inc    %esi
  801285:	83 c4 10             	add    $0x10,%esp
  801288:	39 b5 90 fd ff ff    	cmp    %esi,-0x270(%ebp)
  80128e:	7f ca                	jg     80125a <spawn+0x173>
		argv_store[i] = UTEMP2USTACK(string_store);
		strcpy(string_store, argv[i]);
		string_store += strlen(argv[i]) + 1;
	}
	argv_store[argc] = 0;
  801290:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801296:	8b 8d 80 fd ff ff    	mov    -0x280(%ebp),%ecx
  80129c:	c7 04 08 00 00 00 00 	movl   $0x0,(%eax,%ecx,1)
	assert(string_store == (char*)UTEMP + PGSIZE);
  8012a3:	81 ff 00 10 40 00    	cmp    $0x401000,%edi
  8012a9:	74 19                	je     8012c4 <spawn+0x1dd>
  8012ab:	68 e0 2d 80 00       	push   $0x802de0
  8012b0:	68 5a 2d 80 00       	push   $0x802d5a
  8012b5:	68 f1 00 00 00       	push   $0xf1
  8012ba:	68 6f 2d 80 00       	push   $0x802d6f
  8012bf:	e8 1d ef ff ff       	call   8001e1 <_panic>

	argv_store[-1] = UTEMP2USTACK(argv_store);
  8012c4:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  8012ca:	89 c8                	mov    %ecx,%eax
  8012cc:	2d 00 30 80 11       	sub    $0x11803000,%eax
  8012d1:	89 41 fc             	mov    %eax,-0x4(%ecx)
	argv_store[-2] = argc;
  8012d4:	89 c8                	mov    %ecx,%eax
  8012d6:	8b 8d 88 fd ff ff    	mov    -0x278(%ebp),%ecx
  8012dc:	89 48 f8             	mov    %ecx,-0x8(%eax)

	*init_esp = UTEMP2USTACK(&argv_store[-2]);
  8012df:	2d 08 30 80 11       	sub    $0x11803008,%eax
  8012e4:	89 85 e0 fd ff ff    	mov    %eax,-0x220(%ebp)

	// After completing the stack, map it into the child's address space
	// and unmap it from ours!
	if ((r = sys_page_map(0, UTEMP, child, (void*) (USTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W)) < 0)
  8012ea:	83 ec 0c             	sub    $0xc,%esp
  8012ed:	6a 07                	push   $0x7
  8012ef:	68 00 d0 bf ee       	push   $0xeebfd000
  8012f4:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8012fa:	68 00 00 40 00       	push   $0x400000
  8012ff:	6a 00                	push   $0x0
  801301:	e8 3e f9 ff ff       	call   800c44 <sys_page_map>
  801306:	89 c3                	mov    %eax,%ebx
  801308:	83 c4 20             	add    $0x20,%esp
  80130b:	85 c0                	test   %eax,%eax
  80130d:	0f 88 30 03 00 00    	js     801643 <spawn+0x55c>
		goto error;
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  801313:	83 ec 08             	sub    $0x8,%esp
  801316:	68 00 00 40 00       	push   $0x400000
  80131b:	6a 00                	push   $0x0
  80131d:	e8 64 f9 ff ff       	call   800c86 <sys_page_unmap>
  801322:	89 c3                	mov    %eax,%ebx
  801324:	83 c4 10             	add    $0x10,%esp
  801327:	85 c0                	test   %eax,%eax
  801329:	0f 88 14 03 00 00    	js     801643 <spawn+0x55c>

	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
  80132f:	8b 85 04 fe ff ff    	mov    -0x1fc(%ebp),%eax
  801335:	8d 84 05 e8 fd ff ff 	lea    -0x218(%ebp,%eax,1),%eax
  80133c:	89 85 7c fd ff ff    	mov    %eax,-0x284(%ebp)
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  801342:	c7 85 78 fd ff ff 00 	movl   $0x0,-0x288(%ebp)
  801349:	00 00 00 
  80134c:	e9 88 01 00 00       	jmp    8014d9 <spawn+0x3f2>
		if (ph->p_type != ELF_PROG_LOAD)
  801351:	8b 85 7c fd ff ff    	mov    -0x284(%ebp),%eax
  801357:	83 38 01             	cmpl   $0x1,(%eax)
  80135a:	0f 85 6c 01 00 00    	jne    8014cc <spawn+0x3e5>
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
  801360:	89 c1                	mov    %eax,%ecx
  801362:	8b 40 18             	mov    0x18(%eax),%eax
  801365:	83 e0 02             	and    $0x2,%eax
			perm |= PTE_W;
  801368:	83 f8 01             	cmp    $0x1,%eax
  80136b:	19 c0                	sbb    %eax,%eax
  80136d:	83 e0 fe             	and    $0xfffffffe,%eax
  801370:	83 c0 07             	add    $0x7,%eax
  801373:	89 85 88 fd ff ff    	mov    %eax,-0x278(%ebp)
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
  801379:	89 c8                	mov    %ecx,%eax
  80137b:	8b 49 04             	mov    0x4(%ecx),%ecx
  80137e:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
  801384:	8b 78 10             	mov    0x10(%eax),%edi
  801387:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  80138d:	8b 70 14             	mov    0x14(%eax),%esi
  801390:	89 f2                	mov    %esi,%edx
  801392:	89 b5 90 fd ff ff    	mov    %esi,-0x270(%ebp)
  801398:	8b 70 08             	mov    0x8(%eax),%esi
	int i, r;
	void *blk;

	//cprintf("map_segment %x+%x\n", va, memsz);

	if ((i = PGOFF(va))) {
  80139b:	89 f0                	mov    %esi,%eax
  80139d:	25 ff 0f 00 00       	and    $0xfff,%eax
  8013a2:	74 1a                	je     8013be <spawn+0x2d7>
		va -= i;
  8013a4:	29 c6                	sub    %eax,%esi
		memsz += i;
  8013a6:	01 c2                	add    %eax,%edx
  8013a8:	89 95 90 fd ff ff    	mov    %edx,-0x270(%ebp)
		filesz += i;
  8013ae:	01 c7                	add    %eax,%edi
  8013b0:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
		fileoffset -= i;
  8013b6:	29 c1                	sub    %eax,%ecx
  8013b8:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  8013be:	bb 00 00 00 00       	mov    $0x0,%ebx
  8013c3:	e9 f6 00 00 00       	jmp    8014be <spawn+0x3d7>
		if (i >= filesz) {
  8013c8:	39 9d 94 fd ff ff    	cmp    %ebx,-0x26c(%ebp)
  8013ce:	77 27                	ja     8013f7 <spawn+0x310>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
  8013d0:	83 ec 04             	sub    $0x4,%esp
  8013d3:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  8013d9:	56                   	push   %esi
  8013da:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  8013e0:	e8 1c f8 ff ff       	call   800c01 <sys_page_alloc>
  8013e5:	83 c4 10             	add    $0x10,%esp
  8013e8:	85 c0                	test   %eax,%eax
  8013ea:	0f 89 c2 00 00 00    	jns    8014b2 <spawn+0x3cb>
  8013f0:	89 c3                	mov    %eax,%ebx
  8013f2:	e9 2b 02 00 00       	jmp    801622 <spawn+0x53b>
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  8013f7:	83 ec 04             	sub    $0x4,%esp
  8013fa:	6a 07                	push   $0x7
  8013fc:	68 00 00 40 00       	push   $0x400000
  801401:	6a 00                	push   $0x0
  801403:	e8 f9 f7 ff ff       	call   800c01 <sys_page_alloc>
  801408:	83 c4 10             	add    $0x10,%esp
  80140b:	85 c0                	test   %eax,%eax
  80140d:	0f 88 05 02 00 00    	js     801618 <spawn+0x531>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801413:	83 ec 08             	sub    $0x8,%esp
  801416:	8b 85 80 fd ff ff    	mov    -0x280(%ebp),%eax
  80141c:	01 f8                	add    %edi,%eax
  80141e:	50                   	push   %eax
  80141f:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801425:	e8 a9 07 00 00       	call   801bd3 <seek>
  80142a:	83 c4 10             	add    $0x10,%esp
  80142d:	85 c0                	test   %eax,%eax
  80142f:	0f 88 e7 01 00 00    	js     80161c <spawn+0x535>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801435:	83 ec 04             	sub    $0x4,%esp
  801438:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  80143e:	29 f8                	sub    %edi,%eax
  801440:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801445:	76 05                	jbe    80144c <spawn+0x365>
  801447:	b8 00 10 00 00       	mov    $0x1000,%eax
  80144c:	50                   	push   %eax
  80144d:	68 00 00 40 00       	push   $0x400000
  801452:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801458:	e8 ab 06 00 00       	call   801b08 <readn>
  80145d:	83 c4 10             	add    $0x10,%esp
  801460:	85 c0                	test   %eax,%eax
  801462:	0f 88 b8 01 00 00    	js     801620 <spawn+0x539>
				return r;
			if ((r = sys_page_map(0, UTEMP, child, (void*) (va + i), perm)) < 0)
  801468:	83 ec 0c             	sub    $0xc,%esp
  80146b:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801471:	56                   	push   %esi
  801472:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801478:	68 00 00 40 00       	push   $0x400000
  80147d:	6a 00                	push   $0x0
  80147f:	e8 c0 f7 ff ff       	call   800c44 <sys_page_map>
  801484:	83 c4 20             	add    $0x20,%esp
  801487:	85 c0                	test   %eax,%eax
  801489:	79 15                	jns    8014a0 <spawn+0x3b9>
				panic("spawn: sys_page_map data: %e", r);
  80148b:	50                   	push   %eax
  80148c:	68 7b 2d 80 00       	push   $0x802d7b
  801491:	68 24 01 00 00       	push   $0x124
  801496:	68 6f 2d 80 00       	push   $0x802d6f
  80149b:	e8 41 ed ff ff       	call   8001e1 <_panic>
			sys_page_unmap(0, UTEMP);
  8014a0:	83 ec 08             	sub    $0x8,%esp
  8014a3:	68 00 00 40 00       	push   $0x400000
  8014a8:	6a 00                	push   $0x0
  8014aa:	e8 d7 f7 ff ff       	call   800c86 <sys_page_unmap>
  8014af:	83 c4 10             	add    $0x10,%esp
		memsz += i;
		filesz += i;
		fileoffset -= i;
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  8014b2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  8014b8:	81 c6 00 10 00 00    	add    $0x1000,%esi
  8014be:	89 df                	mov    %ebx,%edi
  8014c0:	39 9d 90 fd ff ff    	cmp    %ebx,-0x270(%ebp)
  8014c6:	0f 87 fc fe ff ff    	ja     8013c8 <spawn+0x2e1>
	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  8014cc:	ff 85 78 fd ff ff    	incl   -0x288(%ebp)
  8014d2:	83 85 7c fd ff ff 20 	addl   $0x20,-0x284(%ebp)
  8014d9:	0f b7 85 14 fe ff ff 	movzwl -0x1ec(%ebp),%eax
  8014e0:	39 85 78 fd ff ff    	cmp    %eax,-0x288(%ebp)
  8014e6:	0f 8c 65 fe ff ff    	jl     801351 <spawn+0x26a>
			perm |= PTE_W;
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
				     fd, ph->p_filesz, ph->p_offset, perm)) < 0)
			goto error;
	}
	close(fd);
  8014ec:	83 ec 0c             	sub    $0xc,%esp
  8014ef:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8014f5:	e8 4f 04 00 00       	call   801949 <close>
  8014fa:	83 c4 10             	add    $0x10,%esp
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  8014fd:	bf 00 00 00 00       	mov    $0x0,%edi
  801502:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  801508:	8b bd 84 fd ff ff    	mov    -0x27c(%ebp),%edi
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  80150e:	8b b5 94 fd ff ff    	mov    -0x26c(%ebp),%esi
  801514:	8b 04 b5 00 d0 7b ef 	mov    -0x10843000(,%esi,4),%eax
  80151b:	a8 01                	test   $0x1,%al
  80151d:	74 4b                	je     80156a <spawn+0x483>
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
        	/* LAB 4: Your code here.
		    pte_t pte = uvpt[pn];
		    void *va = (void *)(pn << PGSHIFT);
		    int perm = pte & PTE_SYSCALL;*/
		    unsigned pn = (ipd << 10) | ipt;
  80151f:	c1 e6 0a             	shl    $0xa,%esi
  801522:	bb 00 00 00 00       	mov    $0x0,%ebx
  801527:	89 f0                	mov    %esi,%eax
  801529:	09 d8                	or     %ebx,%eax
        	if(!(uvpt[pn] & PTE_P)){ //doesn't exist
  80152b:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801532:	f6 c2 01             	test   $0x1,%dl
  801535:	74 2a                	je     801561 <spawn+0x47a>
        		continue;
        	}
	        int perm = uvpt[pn] & PTE_SYSCALL;
  801537:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
	        if(perm & PTE_SHARE){
  80153e:	f6 c6 04             	test   $0x4,%dh
  801541:	74 1e                	je     801561 <spawn+0x47a>
			    void *va = (void *)(pn << PGSHIFT);
  801543:	c1 e0 0c             	shl    $0xc,%eax
			    r = sys_page_map(0, va, child, va, perm);
  801546:	83 ec 0c             	sub    $0xc,%esp
  801549:	81 e2 07 0e 00 00    	and    $0xe07,%edx
  80154f:	52                   	push   %edx
  801550:	50                   	push   %eax
  801551:	57                   	push   %edi
  801552:	50                   	push   %eax
  801553:	6a 00                	push   $0x0
  801555:	e8 ea f6 ff ff       	call   800c44 <sys_page_map>
			    if (r){
  80155a:	83 c4 20             	add    $0x20,%esp
  80155d:	85 c0                	test   %eax,%eax
  80155f:	75 1e                	jne    80157f <spawn+0x498>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801561:	43                   	inc    %ebx
  801562:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  801568:	75 bd                	jne    801527 <spawn+0x440>
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80156a:	ff 85 94 fd ff ff    	incl   -0x26c(%ebp)
  801570:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801576:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  80157b:	75 91                	jne    80150e <spawn+0x427>
  80157d:	eb 19                	jmp    801598 <spawn+0x4b1>
	}
	close(fd);
	fd = -1;

	// Copy shared library state.
	if ((r = copy_shared_pages(child)) < 0)
  80157f:	85 c0                	test   %eax,%eax
  801581:	79 15                	jns    801598 <spawn+0x4b1>
		panic("copy_shared_pages: %e", r);
  801583:	50                   	push   %eax
  801584:	68 98 2d 80 00       	push   $0x802d98
  801589:	68 82 00 00 00       	push   $0x82
  80158e:	68 6f 2d 80 00       	push   $0x802d6f
  801593:	e8 49 ec ff ff       	call   8001e1 <_panic>

	if ((r = sys_env_set_trapframe(child, &child_tf)) < 0)
  801598:	83 ec 08             	sub    $0x8,%esp
  80159b:	8d 85 a4 fd ff ff    	lea    -0x25c(%ebp),%eax
  8015a1:	50                   	push   %eax
  8015a2:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8015a8:	e8 7c f7 ff ff       	call   800d29 <sys_env_set_trapframe>
  8015ad:	83 c4 10             	add    $0x10,%esp
  8015b0:	85 c0                	test   %eax,%eax
  8015b2:	79 15                	jns    8015c9 <spawn+0x4e2>
		panic("sys_env_set_trapframe: %e", r);
  8015b4:	50                   	push   %eax
  8015b5:	68 ae 2d 80 00       	push   $0x802dae
  8015ba:	68 85 00 00 00       	push   $0x85
  8015bf:	68 6f 2d 80 00       	push   $0x802d6f
  8015c4:	e8 18 ec ff ff       	call   8001e1 <_panic>

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
  8015c9:	83 ec 08             	sub    $0x8,%esp
  8015cc:	6a 02                	push   $0x2
  8015ce:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8015d4:	e8 ef f6 ff ff       	call   800cc8 <sys_env_set_status>
  8015d9:	83 c4 10             	add    $0x10,%esp
  8015dc:	85 c0                	test   %eax,%eax
  8015de:	79 25                	jns    801605 <spawn+0x51e>
		panic("sys_env_set_status: %e", r);
  8015e0:	50                   	push   %eax
  8015e1:	68 c8 2d 80 00       	push   $0x802dc8
  8015e6:	68 88 00 00 00       	push   $0x88
  8015eb:	68 6f 2d 80 00       	push   $0x802d6f
  8015f0:	e8 ec eb ff ff       	call   8001e1 <_panic>
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
		return r;
  8015f5:	8b 9d 8c fd ff ff    	mov    -0x274(%ebp),%ebx
  8015fb:	eb 58                	jmp    801655 <spawn+0x56e>
		return -E_NOT_EXEC;
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
		return r;
  8015fd:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801603:	eb 50                	jmp    801655 <spawn+0x56e>
		panic("sys_env_set_trapframe: %e", r);

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
		panic("sys_env_set_status: %e", r);

	return child;
  801605:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  80160b:	eb 48                	jmp    801655 <spawn+0x56e>
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
		return -E_NO_MEM;
  80160d:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
  801612:	eb 41                	jmp    801655 <spawn+0x56e>

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
		return r;
  801614:	89 c3                	mov    %eax,%ebx
  801616:	eb 3d                	jmp    801655 <spawn+0x56e>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801618:	89 c3                	mov    %eax,%ebx
  80161a:	eb 06                	jmp    801622 <spawn+0x53b>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  80161c:	89 c3                	mov    %eax,%ebx
  80161e:	eb 02                	jmp    801622 <spawn+0x53b>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801620:	89 c3                	mov    %eax,%ebx
		panic("sys_env_set_status: %e", r);

	return child;

error:
	sys_env_destroy(child);
  801622:	83 ec 0c             	sub    $0xc,%esp
  801625:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  80162b:	e8 52 f5 ff ff       	call   800b82 <sys_env_destroy>
	close(fd);
  801630:	83 c4 04             	add    $0x4,%esp
  801633:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801639:	e8 0b 03 00 00       	call   801949 <close>
	return r;
  80163e:	83 c4 10             	add    $0x10,%esp
  801641:	eb 12                	jmp    801655 <spawn+0x56e>
		goto error;

	return 0;

error:
	sys_page_unmap(0, UTEMP);
  801643:	83 ec 08             	sub    $0x8,%esp
  801646:	68 00 00 40 00       	push   $0x400000
  80164b:	6a 00                	push   $0x0
  80164d:	e8 34 f6 ff ff       	call   800c86 <sys_page_unmap>
  801652:	83 c4 10             	add    $0x10,%esp

error:
	sys_env_destroy(child);
	close(fd);
	return r;
}
  801655:	89 d8                	mov    %ebx,%eax
  801657:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80165a:	5b                   	pop    %ebx
  80165b:	5e                   	pop    %esi
  80165c:	5f                   	pop    %edi
  80165d:	5d                   	pop    %ebp
  80165e:	c3                   	ret    

0080165f <spawnl>:
// Spawn, taking command-line arguments array directly on the stack.
// NOTE: Must have a sentinal of NULL at the end of the args
// (none of the args may be NULL).
int
spawnl(const char *prog, const char *arg0, ...)
{
  80165f:	55                   	push   %ebp
  801660:	89 e5                	mov    %esp,%ebp
  801662:	56                   	push   %esi
  801663:	53                   	push   %ebx
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801664:	8d 55 10             	lea    0x10(%ebp),%edx
{
	// We calculate argc by advancing the args until we hit NULL.
	// The contract of the function guarantees that the last
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
  801667:	b8 00 00 00 00       	mov    $0x0,%eax
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  80166c:	eb 01                	jmp    80166f <spawnl+0x10>
		argc++;
  80166e:	40                   	inc    %eax
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  80166f:	83 c2 04             	add    $0x4,%edx
  801672:	83 7a fc 00          	cmpl   $0x0,-0x4(%edx)
  801676:	75 f6                	jne    80166e <spawnl+0xf>
		argc++;
	va_end(vl);

	// Now that we have the size of the args, do a second pass
	// and store the values in a VLA, which has the format of argv
	const char *argv[argc+2];
  801678:	8d 14 85 1a 00 00 00 	lea    0x1a(,%eax,4),%edx
  80167f:	83 e2 f0             	and    $0xfffffff0,%edx
  801682:	29 d4                	sub    %edx,%esp
  801684:	8d 54 24 03          	lea    0x3(%esp),%edx
  801688:	c1 ea 02             	shr    $0x2,%edx
  80168b:	8d 34 95 00 00 00 00 	lea    0x0(,%edx,4),%esi
  801692:	89 f3                	mov    %esi,%ebx
	argv[0] = arg0;
  801694:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801697:	89 0c 95 00 00 00 00 	mov    %ecx,0x0(,%edx,4)
	argv[argc+1] = NULL;
  80169e:	c7 44 86 04 00 00 00 	movl   $0x0,0x4(%esi,%eax,4)
  8016a5:	00 
  8016a6:	89 c2                	mov    %eax,%edx

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  8016a8:	b8 00 00 00 00       	mov    $0x0,%eax
  8016ad:	eb 08                	jmp    8016b7 <spawnl+0x58>
		argv[i+1] = va_arg(vl, const char *);
  8016af:	40                   	inc    %eax
  8016b0:	8b 4c 85 0c          	mov    0xc(%ebp,%eax,4),%ecx
  8016b4:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
	argv[0] = arg0;
	argv[argc+1] = NULL;

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  8016b7:	39 d0                	cmp    %edx,%eax
  8016b9:	75 f4                	jne    8016af <spawnl+0x50>
		argv[i+1] = va_arg(vl, const char *);
	va_end(vl);
	return spawn(prog, argv);
  8016bb:	83 ec 08             	sub    $0x8,%esp
  8016be:	56                   	push   %esi
  8016bf:	ff 75 08             	pushl  0x8(%ebp)
  8016c2:	e8 20 fa ff ff       	call   8010e7 <spawn>
}
  8016c7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8016ca:	5b                   	pop    %ebx
  8016cb:	5e                   	pop    %esi
  8016cc:	5d                   	pop    %ebp
  8016cd:	c3                   	ret    

008016ce <wait>:
#include <inc/lib.h>

// Waits until 'envid' exits.
void
wait(envid_t envid)
{
  8016ce:	55                   	push   %ebp
  8016cf:	89 e5                	mov    %esp,%ebp
  8016d1:	56                   	push   %esi
  8016d2:	53                   	push   %ebx
  8016d3:	8b 75 08             	mov    0x8(%ebp),%esi
	const volatile struct Env *e;

	assert(envid != 0);
  8016d6:	85 f6                	test   %esi,%esi
  8016d8:	75 16                	jne    8016f0 <wait+0x22>
  8016da:	68 06 2e 80 00       	push   $0x802e06
  8016df:	68 5a 2d 80 00       	push   $0x802d5a
  8016e4:	6a 09                	push   $0x9
  8016e6:	68 11 2e 80 00       	push   $0x802e11
  8016eb:	e8 f1 ea ff ff       	call   8001e1 <_panic>
	e = &envs[ENVX(envid)];
  8016f0:	89 f3                	mov    %esi,%ebx
  8016f2:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
	while (e->env_id == envid && e->env_status != ENV_FREE)
  8016f8:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  8016ff:	c1 e3 07             	shl    $0x7,%ebx
  801702:	29 c3                	sub    %eax,%ebx
  801704:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  80170a:	eb 05                	jmp    801711 <wait+0x43>
		sys_yield();
  80170c:	e8 d1 f4 ff ff       	call   800be2 <sys_yield>
{
	const volatile struct Env *e;

	assert(envid != 0);
	e = &envs[ENVX(envid)];
	while (e->env_id == envid && e->env_status != ENV_FREE)
  801711:	8b 43 48             	mov    0x48(%ebx),%eax
  801714:	39 c6                	cmp    %eax,%esi
  801716:	75 07                	jne    80171f <wait+0x51>
  801718:	8b 43 54             	mov    0x54(%ebx),%eax
  80171b:	85 c0                	test   %eax,%eax
  80171d:	75 ed                	jne    80170c <wait+0x3e>
		sys_yield();
}
  80171f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801722:	5b                   	pop    %ebx
  801723:	5e                   	pop    %esi
  801724:	5d                   	pop    %ebp
  801725:	c3                   	ret    

00801726 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801726:	55                   	push   %ebp
  801727:	89 e5                	mov    %esp,%ebp
  801729:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  80172c:	83 3d 08 40 80 00 00 	cmpl   $0x0,0x804008
  801733:	75 3e                	jne    801773 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801735:	83 ec 04             	sub    $0x4,%esp
  801738:	6a 07                	push   $0x7
  80173a:	68 00 f0 bf ee       	push   $0xeebff000
  80173f:	6a 00                	push   $0x0
  801741:	e8 bb f4 ff ff       	call   800c01 <sys_page_alloc>
  801746:	83 c4 10             	add    $0x10,%esp
  801749:	85 c0                	test   %eax,%eax
  80174b:	74 14                	je     801761 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  80174d:	83 ec 04             	sub    $0x4,%esp
  801750:	68 1c 2e 80 00       	push   $0x802e1c
  801755:	6a 23                	push   $0x23
  801757:	68 3d 2e 80 00       	push   $0x802e3d
  80175c:	e8 80 ea ff ff       	call   8001e1 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801761:	83 ec 08             	sub    $0x8,%esp
  801764:	68 7d 17 80 00       	push   $0x80177d
  801769:	6a 00                	push   $0x0
  80176b:	e8 fb f5 ff ff       	call   800d6b <sys_env_set_pgfault_upcall>
  801770:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801773:	8b 45 08             	mov    0x8(%ebp),%eax
  801776:	a3 08 40 80 00       	mov    %eax,0x804008
}
  80177b:	c9                   	leave  
  80177c:	c3                   	ret    

0080177d <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  80177d:	54                   	push   %esp
	movl _pgfault_handler, %eax
  80177e:	a1 08 40 80 00       	mov    0x804008,%eax
	call *%eax
  801783:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801785:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801788:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  80178a:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  80178e:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801792:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801793:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801796:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801798:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  80179b:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  80179e:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  80179f:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8017a2:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8017a3:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8017a4:	c3                   	ret    

008017a5 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  8017a5:	55                   	push   %ebp
  8017a6:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  8017a8:	8b 45 08             	mov    0x8(%ebp),%eax
  8017ab:	05 00 00 00 30       	add    $0x30000000,%eax
  8017b0:	c1 e8 0c             	shr    $0xc,%eax
}
  8017b3:	5d                   	pop    %ebp
  8017b4:	c3                   	ret    

008017b5 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  8017b5:	55                   	push   %ebp
  8017b6:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  8017b8:	8b 45 08             	mov    0x8(%ebp),%eax
  8017bb:	05 00 00 00 30       	add    $0x30000000,%eax
  8017c0:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  8017c5:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  8017ca:	5d                   	pop    %ebp
  8017cb:	c3                   	ret    

008017cc <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  8017cc:	55                   	push   %ebp
  8017cd:	89 e5                	mov    %esp,%ebp
  8017cf:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8017d2:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  8017d7:	89 c2                	mov    %eax,%edx
  8017d9:	c1 ea 16             	shr    $0x16,%edx
  8017dc:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8017e3:	f6 c2 01             	test   $0x1,%dl
  8017e6:	74 11                	je     8017f9 <fd_alloc+0x2d>
  8017e8:	89 c2                	mov    %eax,%edx
  8017ea:	c1 ea 0c             	shr    $0xc,%edx
  8017ed:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8017f4:	f6 c2 01             	test   $0x1,%dl
  8017f7:	75 09                	jne    801802 <fd_alloc+0x36>
			*fd_store = fd;
  8017f9:	89 01                	mov    %eax,(%ecx)
			return 0;
  8017fb:	b8 00 00 00 00       	mov    $0x0,%eax
  801800:	eb 17                	jmp    801819 <fd_alloc+0x4d>
  801802:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801807:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  80180c:	75 c9                	jne    8017d7 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  80180e:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801814:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801819:	5d                   	pop    %ebp
  80181a:	c3                   	ret    

0080181b <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  80181b:	55                   	push   %ebp
  80181c:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  80181e:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801822:	77 39                	ja     80185d <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801824:	8b 45 08             	mov    0x8(%ebp),%eax
  801827:	c1 e0 0c             	shl    $0xc,%eax
  80182a:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  80182f:	89 c2                	mov    %eax,%edx
  801831:	c1 ea 16             	shr    $0x16,%edx
  801834:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  80183b:	f6 c2 01             	test   $0x1,%dl
  80183e:	74 24                	je     801864 <fd_lookup+0x49>
  801840:	89 c2                	mov    %eax,%edx
  801842:	c1 ea 0c             	shr    $0xc,%edx
  801845:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80184c:	f6 c2 01             	test   $0x1,%dl
  80184f:	74 1a                	je     80186b <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  801851:	8b 55 0c             	mov    0xc(%ebp),%edx
  801854:	89 02                	mov    %eax,(%edx)
	return 0;
  801856:	b8 00 00 00 00       	mov    $0x0,%eax
  80185b:	eb 13                	jmp    801870 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  80185d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801862:	eb 0c                	jmp    801870 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801864:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801869:	eb 05                	jmp    801870 <fd_lookup+0x55>
  80186b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  801870:	5d                   	pop    %ebp
  801871:	c3                   	ret    

00801872 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  801872:	55                   	push   %ebp
  801873:	89 e5                	mov    %esp,%ebp
  801875:	83 ec 08             	sub    $0x8,%esp
  801878:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80187b:	ba cc 2e 80 00       	mov    $0x802ecc,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801880:	eb 13                	jmp    801895 <dev_lookup+0x23>
  801882:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  801885:	39 08                	cmp    %ecx,(%eax)
  801887:	75 0c                	jne    801895 <dev_lookup+0x23>
			*dev = devtab[i];
  801889:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80188c:	89 01                	mov    %eax,(%ecx)
			return 0;
  80188e:	b8 00 00 00 00       	mov    $0x0,%eax
  801893:	eb 2e                	jmp    8018c3 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801895:	8b 02                	mov    (%edx),%eax
  801897:	85 c0                	test   %eax,%eax
  801899:	75 e7                	jne    801882 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  80189b:	a1 04 40 80 00       	mov    0x804004,%eax
  8018a0:	8b 40 48             	mov    0x48(%eax),%eax
  8018a3:	83 ec 04             	sub    $0x4,%esp
  8018a6:	51                   	push   %ecx
  8018a7:	50                   	push   %eax
  8018a8:	68 4c 2e 80 00       	push   $0x802e4c
  8018ad:	e8 07 ea ff ff       	call   8002b9 <cprintf>
	*dev = 0;
  8018b2:	8b 45 0c             	mov    0xc(%ebp),%eax
  8018b5:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  8018bb:	83 c4 10             	add    $0x10,%esp
  8018be:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  8018c3:	c9                   	leave  
  8018c4:	c3                   	ret    

008018c5 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  8018c5:	55                   	push   %ebp
  8018c6:	89 e5                	mov    %esp,%ebp
  8018c8:	56                   	push   %esi
  8018c9:	53                   	push   %ebx
  8018ca:	83 ec 10             	sub    $0x10,%esp
  8018cd:	8b 75 08             	mov    0x8(%ebp),%esi
  8018d0:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  8018d3:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8018d6:	50                   	push   %eax
  8018d7:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  8018dd:	c1 e8 0c             	shr    $0xc,%eax
  8018e0:	50                   	push   %eax
  8018e1:	e8 35 ff ff ff       	call   80181b <fd_lookup>
  8018e6:	83 c4 08             	add    $0x8,%esp
  8018e9:	85 c0                	test   %eax,%eax
  8018eb:	78 05                	js     8018f2 <fd_close+0x2d>
	    || fd != fd2)
  8018ed:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  8018f0:	74 06                	je     8018f8 <fd_close+0x33>
		return (must_exist ? r : 0);
  8018f2:	84 db                	test   %bl,%bl
  8018f4:	74 47                	je     80193d <fd_close+0x78>
  8018f6:	eb 4a                	jmp    801942 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  8018f8:	83 ec 08             	sub    $0x8,%esp
  8018fb:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8018fe:	50                   	push   %eax
  8018ff:	ff 36                	pushl  (%esi)
  801901:	e8 6c ff ff ff       	call   801872 <dev_lookup>
  801906:	89 c3                	mov    %eax,%ebx
  801908:	83 c4 10             	add    $0x10,%esp
  80190b:	85 c0                	test   %eax,%eax
  80190d:	78 1c                	js     80192b <fd_close+0x66>
		if (dev->dev_close)
  80190f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801912:	8b 40 10             	mov    0x10(%eax),%eax
  801915:	85 c0                	test   %eax,%eax
  801917:	74 0d                	je     801926 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801919:	83 ec 0c             	sub    $0xc,%esp
  80191c:	56                   	push   %esi
  80191d:	ff d0                	call   *%eax
  80191f:	89 c3                	mov    %eax,%ebx
  801921:	83 c4 10             	add    $0x10,%esp
  801924:	eb 05                	jmp    80192b <fd_close+0x66>
		else
			r = 0;
  801926:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  80192b:	83 ec 08             	sub    $0x8,%esp
  80192e:	56                   	push   %esi
  80192f:	6a 00                	push   $0x0
  801931:	e8 50 f3 ff ff       	call   800c86 <sys_page_unmap>
	return r;
  801936:	83 c4 10             	add    $0x10,%esp
  801939:	89 d8                	mov    %ebx,%eax
  80193b:	eb 05                	jmp    801942 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  80193d:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801942:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801945:	5b                   	pop    %ebx
  801946:	5e                   	pop    %esi
  801947:	5d                   	pop    %ebp
  801948:	c3                   	ret    

00801949 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  801949:	55                   	push   %ebp
  80194a:	89 e5                	mov    %esp,%ebp
  80194c:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80194f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801952:	50                   	push   %eax
  801953:	ff 75 08             	pushl  0x8(%ebp)
  801956:	e8 c0 fe ff ff       	call   80181b <fd_lookup>
  80195b:	83 c4 08             	add    $0x8,%esp
  80195e:	85 c0                	test   %eax,%eax
  801960:	78 10                	js     801972 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  801962:	83 ec 08             	sub    $0x8,%esp
  801965:	6a 01                	push   $0x1
  801967:	ff 75 f4             	pushl  -0xc(%ebp)
  80196a:	e8 56 ff ff ff       	call   8018c5 <fd_close>
  80196f:	83 c4 10             	add    $0x10,%esp
}
  801972:	c9                   	leave  
  801973:	c3                   	ret    

00801974 <close_all>:

void
close_all(void)
{
  801974:	55                   	push   %ebp
  801975:	89 e5                	mov    %esp,%ebp
  801977:	53                   	push   %ebx
  801978:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  80197b:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801980:	83 ec 0c             	sub    $0xc,%esp
  801983:	53                   	push   %ebx
  801984:	e8 c0 ff ff ff       	call   801949 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801989:	43                   	inc    %ebx
  80198a:	83 c4 10             	add    $0x10,%esp
  80198d:	83 fb 20             	cmp    $0x20,%ebx
  801990:	75 ee                	jne    801980 <close_all+0xc>
		close(i);
}
  801992:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801995:	c9                   	leave  
  801996:	c3                   	ret    

00801997 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801997:	55                   	push   %ebp
  801998:	89 e5                	mov    %esp,%ebp
  80199a:	57                   	push   %edi
  80199b:	56                   	push   %esi
  80199c:	53                   	push   %ebx
  80199d:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  8019a0:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8019a3:	50                   	push   %eax
  8019a4:	ff 75 08             	pushl  0x8(%ebp)
  8019a7:	e8 6f fe ff ff       	call   80181b <fd_lookup>
  8019ac:	83 c4 08             	add    $0x8,%esp
  8019af:	85 c0                	test   %eax,%eax
  8019b1:	0f 88 c2 00 00 00    	js     801a79 <dup+0xe2>
		return r;
	close(newfdnum);
  8019b7:	83 ec 0c             	sub    $0xc,%esp
  8019ba:	ff 75 0c             	pushl  0xc(%ebp)
  8019bd:	e8 87 ff ff ff       	call   801949 <close>

	newfd = INDEX2FD(newfdnum);
  8019c2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8019c5:	c1 e3 0c             	shl    $0xc,%ebx
  8019c8:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  8019ce:	83 c4 04             	add    $0x4,%esp
  8019d1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8019d4:	e8 dc fd ff ff       	call   8017b5 <fd2data>
  8019d9:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  8019db:	89 1c 24             	mov    %ebx,(%esp)
  8019de:	e8 d2 fd ff ff       	call   8017b5 <fd2data>
  8019e3:	83 c4 10             	add    $0x10,%esp
  8019e6:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8019e8:	89 f0                	mov    %esi,%eax
  8019ea:	c1 e8 16             	shr    $0x16,%eax
  8019ed:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8019f4:	a8 01                	test   $0x1,%al
  8019f6:	74 35                	je     801a2d <dup+0x96>
  8019f8:	89 f0                	mov    %esi,%eax
  8019fa:	c1 e8 0c             	shr    $0xc,%eax
  8019fd:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801a04:	f6 c2 01             	test   $0x1,%dl
  801a07:	74 24                	je     801a2d <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801a09:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801a10:	83 ec 0c             	sub    $0xc,%esp
  801a13:	25 07 0e 00 00       	and    $0xe07,%eax
  801a18:	50                   	push   %eax
  801a19:	57                   	push   %edi
  801a1a:	6a 00                	push   $0x0
  801a1c:	56                   	push   %esi
  801a1d:	6a 00                	push   $0x0
  801a1f:	e8 20 f2 ff ff       	call   800c44 <sys_page_map>
  801a24:	89 c6                	mov    %eax,%esi
  801a26:	83 c4 20             	add    $0x20,%esp
  801a29:	85 c0                	test   %eax,%eax
  801a2b:	78 2c                	js     801a59 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  801a2d:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801a30:	89 d0                	mov    %edx,%eax
  801a32:	c1 e8 0c             	shr    $0xc,%eax
  801a35:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801a3c:	83 ec 0c             	sub    $0xc,%esp
  801a3f:	25 07 0e 00 00       	and    $0xe07,%eax
  801a44:	50                   	push   %eax
  801a45:	53                   	push   %ebx
  801a46:	6a 00                	push   $0x0
  801a48:	52                   	push   %edx
  801a49:	6a 00                	push   $0x0
  801a4b:	e8 f4 f1 ff ff       	call   800c44 <sys_page_map>
  801a50:	89 c6                	mov    %eax,%esi
  801a52:	83 c4 20             	add    $0x20,%esp
  801a55:	85 c0                	test   %eax,%eax
  801a57:	79 1d                	jns    801a76 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801a59:	83 ec 08             	sub    $0x8,%esp
  801a5c:	53                   	push   %ebx
  801a5d:	6a 00                	push   $0x0
  801a5f:	e8 22 f2 ff ff       	call   800c86 <sys_page_unmap>
	sys_page_unmap(0, nva);
  801a64:	83 c4 08             	add    $0x8,%esp
  801a67:	57                   	push   %edi
  801a68:	6a 00                	push   $0x0
  801a6a:	e8 17 f2 ff ff       	call   800c86 <sys_page_unmap>
	return r;
  801a6f:	83 c4 10             	add    $0x10,%esp
  801a72:	89 f0                	mov    %esi,%eax
  801a74:	eb 03                	jmp    801a79 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801a76:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801a79:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a7c:	5b                   	pop    %ebx
  801a7d:	5e                   	pop    %esi
  801a7e:	5f                   	pop    %edi
  801a7f:	5d                   	pop    %ebp
  801a80:	c3                   	ret    

00801a81 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801a81:	55                   	push   %ebp
  801a82:	89 e5                	mov    %esp,%ebp
  801a84:	53                   	push   %ebx
  801a85:	83 ec 14             	sub    $0x14,%esp
  801a88:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801a8b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801a8e:	50                   	push   %eax
  801a8f:	53                   	push   %ebx
  801a90:	e8 86 fd ff ff       	call   80181b <fd_lookup>
  801a95:	83 c4 08             	add    $0x8,%esp
  801a98:	85 c0                	test   %eax,%eax
  801a9a:	78 67                	js     801b03 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801a9c:	83 ec 08             	sub    $0x8,%esp
  801a9f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801aa2:	50                   	push   %eax
  801aa3:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801aa6:	ff 30                	pushl  (%eax)
  801aa8:	e8 c5 fd ff ff       	call   801872 <dev_lookup>
  801aad:	83 c4 10             	add    $0x10,%esp
  801ab0:	85 c0                	test   %eax,%eax
  801ab2:	78 4f                	js     801b03 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801ab4:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801ab7:	8b 42 08             	mov    0x8(%edx),%eax
  801aba:	83 e0 03             	and    $0x3,%eax
  801abd:	83 f8 01             	cmp    $0x1,%eax
  801ac0:	75 21                	jne    801ae3 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801ac2:	a1 04 40 80 00       	mov    0x804004,%eax
  801ac7:	8b 40 48             	mov    0x48(%eax),%eax
  801aca:	83 ec 04             	sub    $0x4,%esp
  801acd:	53                   	push   %ebx
  801ace:	50                   	push   %eax
  801acf:	68 90 2e 80 00       	push   $0x802e90
  801ad4:	e8 e0 e7 ff ff       	call   8002b9 <cprintf>
		return -E_INVAL;
  801ad9:	83 c4 10             	add    $0x10,%esp
  801adc:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801ae1:	eb 20                	jmp    801b03 <read+0x82>
	}
	if (!dev->dev_read)
  801ae3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ae6:	8b 40 08             	mov    0x8(%eax),%eax
  801ae9:	85 c0                	test   %eax,%eax
  801aeb:	74 11                	je     801afe <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801aed:	83 ec 04             	sub    $0x4,%esp
  801af0:	ff 75 10             	pushl  0x10(%ebp)
  801af3:	ff 75 0c             	pushl  0xc(%ebp)
  801af6:	52                   	push   %edx
  801af7:	ff d0                	call   *%eax
  801af9:	83 c4 10             	add    $0x10,%esp
  801afc:	eb 05                	jmp    801b03 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801afe:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801b03:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801b06:	c9                   	leave  
  801b07:	c3                   	ret    

00801b08 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801b08:	55                   	push   %ebp
  801b09:	89 e5                	mov    %esp,%ebp
  801b0b:	57                   	push   %edi
  801b0c:	56                   	push   %esi
  801b0d:	53                   	push   %ebx
  801b0e:	83 ec 0c             	sub    $0xc,%esp
  801b11:	8b 7d 08             	mov    0x8(%ebp),%edi
  801b14:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801b17:	bb 00 00 00 00       	mov    $0x0,%ebx
  801b1c:	eb 21                	jmp    801b3f <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801b1e:	83 ec 04             	sub    $0x4,%esp
  801b21:	89 f0                	mov    %esi,%eax
  801b23:	29 d8                	sub    %ebx,%eax
  801b25:	50                   	push   %eax
  801b26:	89 d8                	mov    %ebx,%eax
  801b28:	03 45 0c             	add    0xc(%ebp),%eax
  801b2b:	50                   	push   %eax
  801b2c:	57                   	push   %edi
  801b2d:	e8 4f ff ff ff       	call   801a81 <read>
		if (m < 0)
  801b32:	83 c4 10             	add    $0x10,%esp
  801b35:	85 c0                	test   %eax,%eax
  801b37:	78 10                	js     801b49 <readn+0x41>
			return m;
		if (m == 0)
  801b39:	85 c0                	test   %eax,%eax
  801b3b:	74 0a                	je     801b47 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801b3d:	01 c3                	add    %eax,%ebx
  801b3f:	39 f3                	cmp    %esi,%ebx
  801b41:	72 db                	jb     801b1e <readn+0x16>
  801b43:	89 d8                	mov    %ebx,%eax
  801b45:	eb 02                	jmp    801b49 <readn+0x41>
  801b47:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801b49:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b4c:	5b                   	pop    %ebx
  801b4d:	5e                   	pop    %esi
  801b4e:	5f                   	pop    %edi
  801b4f:	5d                   	pop    %ebp
  801b50:	c3                   	ret    

00801b51 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801b51:	55                   	push   %ebp
  801b52:	89 e5                	mov    %esp,%ebp
  801b54:	53                   	push   %ebx
  801b55:	83 ec 14             	sub    $0x14,%esp
  801b58:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801b5b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801b5e:	50                   	push   %eax
  801b5f:	53                   	push   %ebx
  801b60:	e8 b6 fc ff ff       	call   80181b <fd_lookup>
  801b65:	83 c4 08             	add    $0x8,%esp
  801b68:	85 c0                	test   %eax,%eax
  801b6a:	78 62                	js     801bce <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801b6c:	83 ec 08             	sub    $0x8,%esp
  801b6f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b72:	50                   	push   %eax
  801b73:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801b76:	ff 30                	pushl  (%eax)
  801b78:	e8 f5 fc ff ff       	call   801872 <dev_lookup>
  801b7d:	83 c4 10             	add    $0x10,%esp
  801b80:	85 c0                	test   %eax,%eax
  801b82:	78 4a                	js     801bce <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801b84:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801b87:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801b8b:	75 21                	jne    801bae <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801b8d:	a1 04 40 80 00       	mov    0x804004,%eax
  801b92:	8b 40 48             	mov    0x48(%eax),%eax
  801b95:	83 ec 04             	sub    $0x4,%esp
  801b98:	53                   	push   %ebx
  801b99:	50                   	push   %eax
  801b9a:	68 ac 2e 80 00       	push   $0x802eac
  801b9f:	e8 15 e7 ff ff       	call   8002b9 <cprintf>
		return -E_INVAL;
  801ba4:	83 c4 10             	add    $0x10,%esp
  801ba7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801bac:	eb 20                	jmp    801bce <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801bae:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801bb1:	8b 52 0c             	mov    0xc(%edx),%edx
  801bb4:	85 d2                	test   %edx,%edx
  801bb6:	74 11                	je     801bc9 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801bb8:	83 ec 04             	sub    $0x4,%esp
  801bbb:	ff 75 10             	pushl  0x10(%ebp)
  801bbe:	ff 75 0c             	pushl  0xc(%ebp)
  801bc1:	50                   	push   %eax
  801bc2:	ff d2                	call   *%edx
  801bc4:	83 c4 10             	add    $0x10,%esp
  801bc7:	eb 05                	jmp    801bce <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801bc9:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801bce:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801bd1:	c9                   	leave  
  801bd2:	c3                   	ret    

00801bd3 <seek>:

int
seek(int fdnum, off_t offset)
{
  801bd3:	55                   	push   %ebp
  801bd4:	89 e5                	mov    %esp,%ebp
  801bd6:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801bd9:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801bdc:	50                   	push   %eax
  801bdd:	ff 75 08             	pushl  0x8(%ebp)
  801be0:	e8 36 fc ff ff       	call   80181b <fd_lookup>
  801be5:	83 c4 08             	add    $0x8,%esp
  801be8:	85 c0                	test   %eax,%eax
  801bea:	78 0e                	js     801bfa <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801bec:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801bef:	8b 55 0c             	mov    0xc(%ebp),%edx
  801bf2:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801bf5:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801bfa:	c9                   	leave  
  801bfb:	c3                   	ret    

00801bfc <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801bfc:	55                   	push   %ebp
  801bfd:	89 e5                	mov    %esp,%ebp
  801bff:	53                   	push   %ebx
  801c00:	83 ec 14             	sub    $0x14,%esp
  801c03:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801c06:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801c09:	50                   	push   %eax
  801c0a:	53                   	push   %ebx
  801c0b:	e8 0b fc ff ff       	call   80181b <fd_lookup>
  801c10:	83 c4 08             	add    $0x8,%esp
  801c13:	85 c0                	test   %eax,%eax
  801c15:	78 5f                	js     801c76 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801c17:	83 ec 08             	sub    $0x8,%esp
  801c1a:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c1d:	50                   	push   %eax
  801c1e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c21:	ff 30                	pushl  (%eax)
  801c23:	e8 4a fc ff ff       	call   801872 <dev_lookup>
  801c28:	83 c4 10             	add    $0x10,%esp
  801c2b:	85 c0                	test   %eax,%eax
  801c2d:	78 47                	js     801c76 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801c2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c32:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801c36:	75 21                	jne    801c59 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801c38:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  801c3d:	8b 40 48             	mov    0x48(%eax),%eax
  801c40:	83 ec 04             	sub    $0x4,%esp
  801c43:	53                   	push   %ebx
  801c44:	50                   	push   %eax
  801c45:	68 6c 2e 80 00       	push   $0x802e6c
  801c4a:	e8 6a e6 ff ff       	call   8002b9 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  801c4f:	83 c4 10             	add    $0x10,%esp
  801c52:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801c57:	eb 1d                	jmp    801c76 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  801c59:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801c5c:	8b 52 18             	mov    0x18(%edx),%edx
  801c5f:	85 d2                	test   %edx,%edx
  801c61:	74 0e                	je     801c71 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  801c63:	83 ec 08             	sub    $0x8,%esp
  801c66:	ff 75 0c             	pushl  0xc(%ebp)
  801c69:	50                   	push   %eax
  801c6a:	ff d2                	call   *%edx
  801c6c:	83 c4 10             	add    $0x10,%esp
  801c6f:	eb 05                	jmp    801c76 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  801c71:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  801c76:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801c79:	c9                   	leave  
  801c7a:	c3                   	ret    

00801c7b <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  801c7b:	55                   	push   %ebp
  801c7c:	89 e5                	mov    %esp,%ebp
  801c7e:	53                   	push   %ebx
  801c7f:	83 ec 14             	sub    $0x14,%esp
  801c82:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801c85:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801c88:	50                   	push   %eax
  801c89:	ff 75 08             	pushl  0x8(%ebp)
  801c8c:	e8 8a fb ff ff       	call   80181b <fd_lookup>
  801c91:	83 c4 08             	add    $0x8,%esp
  801c94:	85 c0                	test   %eax,%eax
  801c96:	78 52                	js     801cea <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801c98:	83 ec 08             	sub    $0x8,%esp
  801c9b:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c9e:	50                   	push   %eax
  801c9f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801ca2:	ff 30                	pushl  (%eax)
  801ca4:	e8 c9 fb ff ff       	call   801872 <dev_lookup>
  801ca9:	83 c4 10             	add    $0x10,%esp
  801cac:	85 c0                	test   %eax,%eax
  801cae:	78 3a                	js     801cea <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801cb0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801cb3:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  801cb7:	74 2c                	je     801ce5 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  801cb9:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  801cbc:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801cc3:	00 00 00 
	stat->st_isdir = 0;
  801cc6:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801ccd:	00 00 00 
	stat->st_dev = dev;
  801cd0:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  801cd6:	83 ec 08             	sub    $0x8,%esp
  801cd9:	53                   	push   %ebx
  801cda:	ff 75 f0             	pushl  -0x10(%ebp)
  801cdd:	ff 50 14             	call   *0x14(%eax)
  801ce0:	83 c4 10             	add    $0x10,%esp
  801ce3:	eb 05                	jmp    801cea <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801ce5:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  801cea:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801ced:	c9                   	leave  
  801cee:	c3                   	ret    

00801cef <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801cef:	55                   	push   %ebp
  801cf0:	89 e5                	mov    %esp,%ebp
  801cf2:	56                   	push   %esi
  801cf3:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801cf4:	83 ec 08             	sub    $0x8,%esp
  801cf7:	6a 00                	push   $0x0
  801cf9:	ff 75 08             	pushl  0x8(%ebp)
  801cfc:	e8 e7 01 00 00       	call   801ee8 <open>
  801d01:	89 c3                	mov    %eax,%ebx
  801d03:	83 c4 10             	add    $0x10,%esp
  801d06:	85 c0                	test   %eax,%eax
  801d08:	78 1d                	js     801d27 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801d0a:	83 ec 08             	sub    $0x8,%esp
  801d0d:	ff 75 0c             	pushl  0xc(%ebp)
  801d10:	50                   	push   %eax
  801d11:	e8 65 ff ff ff       	call   801c7b <fstat>
  801d16:	89 c6                	mov    %eax,%esi
	close(fd);
  801d18:	89 1c 24             	mov    %ebx,(%esp)
  801d1b:	e8 29 fc ff ff       	call   801949 <close>
	return r;
  801d20:	83 c4 10             	add    $0x10,%esp
  801d23:	89 f0                	mov    %esi,%eax
  801d25:	eb 00                	jmp    801d27 <stat+0x38>
}
  801d27:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801d2a:	5b                   	pop    %ebx
  801d2b:	5e                   	pop    %esi
  801d2c:	5d                   	pop    %ebp
  801d2d:	c3                   	ret    

00801d2e <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  801d2e:	55                   	push   %ebp
  801d2f:	89 e5                	mov    %esp,%ebp
  801d31:	56                   	push   %esi
  801d32:	53                   	push   %ebx
  801d33:	89 c6                	mov    %eax,%esi
  801d35:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801d37:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  801d3e:	75 12                	jne    801d52 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801d40:	83 ec 0c             	sub    $0xc,%esp
  801d43:	6a 01                	push   $0x1
  801d45:	e8 b8 07 00 00       	call   802502 <ipc_find_env>
  801d4a:	a3 00 40 80 00       	mov    %eax,0x804000
  801d4f:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  801d52:	6a 07                	push   $0x7
  801d54:	68 00 50 80 00       	push   $0x805000
  801d59:	56                   	push   %esi
  801d5a:	ff 35 00 40 80 00    	pushl  0x804000
  801d60:	e8 48 07 00 00       	call   8024ad <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  801d65:	83 c4 0c             	add    $0xc,%esp
  801d68:	6a 00                	push   $0x0
  801d6a:	53                   	push   %ebx
  801d6b:	6a 00                	push   $0x0
  801d6d:	e8 d3 06 00 00       	call   802445 <ipc_recv>
}
  801d72:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801d75:	5b                   	pop    %ebx
  801d76:	5e                   	pop    %esi
  801d77:	5d                   	pop    %ebp
  801d78:	c3                   	ret    

00801d79 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801d79:	55                   	push   %ebp
  801d7a:	89 e5                	mov    %esp,%ebp
  801d7c:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801d7f:	8b 45 08             	mov    0x8(%ebp),%eax
  801d82:	8b 40 0c             	mov    0xc(%eax),%eax
  801d85:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  801d8a:	8b 45 0c             	mov    0xc(%ebp),%eax
  801d8d:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801d92:	ba 00 00 00 00       	mov    $0x0,%edx
  801d97:	b8 02 00 00 00       	mov    $0x2,%eax
  801d9c:	e8 8d ff ff ff       	call   801d2e <fsipc>
}
  801da1:	c9                   	leave  
  801da2:	c3                   	ret    

00801da3 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  801da3:	55                   	push   %ebp
  801da4:	89 e5                	mov    %esp,%ebp
  801da6:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  801da9:	8b 45 08             	mov    0x8(%ebp),%eax
  801dac:	8b 40 0c             	mov    0xc(%eax),%eax
  801daf:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  801db4:	ba 00 00 00 00       	mov    $0x0,%edx
  801db9:	b8 06 00 00 00       	mov    $0x6,%eax
  801dbe:	e8 6b ff ff ff       	call   801d2e <fsipc>
}
  801dc3:	c9                   	leave  
  801dc4:	c3                   	ret    

00801dc5 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801dc5:	55                   	push   %ebp
  801dc6:	89 e5                	mov    %esp,%ebp
  801dc8:	53                   	push   %ebx
  801dc9:	83 ec 04             	sub    $0x4,%esp
  801dcc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801dcf:	8b 45 08             	mov    0x8(%ebp),%eax
  801dd2:	8b 40 0c             	mov    0xc(%eax),%eax
  801dd5:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  801dda:	ba 00 00 00 00       	mov    $0x0,%edx
  801ddf:	b8 05 00 00 00       	mov    $0x5,%eax
  801de4:	e8 45 ff ff ff       	call   801d2e <fsipc>
  801de9:	85 c0                	test   %eax,%eax
  801deb:	78 2c                	js     801e19 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801ded:	83 ec 08             	sub    $0x8,%esp
  801df0:	68 00 50 80 00       	push   $0x805000
  801df5:	53                   	push   %ebx
  801df6:	e8 22 ea ff ff       	call   80081d <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801dfb:	a1 80 50 80 00       	mov    0x805080,%eax
  801e00:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801e06:	a1 84 50 80 00       	mov    0x805084,%eax
  801e0b:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801e11:	83 c4 10             	add    $0x10,%esp
  801e14:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801e19:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801e1c:	c9                   	leave  
  801e1d:	c3                   	ret    

00801e1e <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801e1e:	55                   	push   %ebp
  801e1f:	89 e5                	mov    %esp,%ebp
  801e21:	83 ec 08             	sub    $0x8,%esp
  801e24:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801e27:	8b 55 08             	mov    0x8(%ebp),%edx
  801e2a:	8b 52 0c             	mov    0xc(%edx),%edx
  801e2d:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801e33:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801e38:	76 05                	jbe    801e3f <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801e3a:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801e3f:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801e44:	83 ec 04             	sub    $0x4,%esp
  801e47:	50                   	push   %eax
  801e48:	ff 75 0c             	pushl  0xc(%ebp)
  801e4b:	68 08 50 80 00       	push   $0x805008
  801e50:	e8 3d eb ff ff       	call   800992 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801e55:	ba 00 00 00 00       	mov    $0x0,%edx
  801e5a:	b8 04 00 00 00       	mov    $0x4,%eax
  801e5f:	e8 ca fe ff ff       	call   801d2e <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801e64:	c9                   	leave  
  801e65:	c3                   	ret    

00801e66 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801e66:	55                   	push   %ebp
  801e67:	89 e5                	mov    %esp,%ebp
  801e69:	56                   	push   %esi
  801e6a:	53                   	push   %ebx
  801e6b:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  801e6e:	8b 45 08             	mov    0x8(%ebp),%eax
  801e71:	8b 40 0c             	mov    0xc(%eax),%eax
  801e74:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801e79:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801e7f:	ba 00 00 00 00       	mov    $0x0,%edx
  801e84:	b8 03 00 00 00       	mov    $0x3,%eax
  801e89:	e8 a0 fe ff ff       	call   801d2e <fsipc>
  801e8e:	89 c3                	mov    %eax,%ebx
  801e90:	85 c0                	test   %eax,%eax
  801e92:	78 4b                	js     801edf <devfile_read+0x79>
		return r;
	assert(r <= n);
  801e94:	39 c6                	cmp    %eax,%esi
  801e96:	73 16                	jae    801eae <devfile_read+0x48>
  801e98:	68 dc 2e 80 00       	push   $0x802edc
  801e9d:	68 5a 2d 80 00       	push   $0x802d5a
  801ea2:	6a 7c                	push   $0x7c
  801ea4:	68 e3 2e 80 00       	push   $0x802ee3
  801ea9:	e8 33 e3 ff ff       	call   8001e1 <_panic>
	assert(r <= PGSIZE);
  801eae:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801eb3:	7e 16                	jle    801ecb <devfile_read+0x65>
  801eb5:	68 ee 2e 80 00       	push   $0x802eee
  801eba:	68 5a 2d 80 00       	push   $0x802d5a
  801ebf:	6a 7d                	push   $0x7d
  801ec1:	68 e3 2e 80 00       	push   $0x802ee3
  801ec6:	e8 16 e3 ff ff       	call   8001e1 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801ecb:	83 ec 04             	sub    $0x4,%esp
  801ece:	50                   	push   %eax
  801ecf:	68 00 50 80 00       	push   $0x805000
  801ed4:	ff 75 0c             	pushl  0xc(%ebp)
  801ed7:	e8 b6 ea ff ff       	call   800992 <memmove>
	return r;
  801edc:	83 c4 10             	add    $0x10,%esp
}
  801edf:	89 d8                	mov    %ebx,%eax
  801ee1:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801ee4:	5b                   	pop    %ebx
  801ee5:	5e                   	pop    %esi
  801ee6:	5d                   	pop    %ebp
  801ee7:	c3                   	ret    

00801ee8 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801ee8:	55                   	push   %ebp
  801ee9:	89 e5                	mov    %esp,%ebp
  801eeb:	53                   	push   %ebx
  801eec:	83 ec 20             	sub    $0x20,%esp
  801eef:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801ef2:	53                   	push   %ebx
  801ef3:	e8 f0 e8 ff ff       	call   8007e8 <strlen>
  801ef8:	83 c4 10             	add    $0x10,%esp
  801efb:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801f00:	7f 63                	jg     801f65 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801f02:	83 ec 0c             	sub    $0xc,%esp
  801f05:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801f08:	50                   	push   %eax
  801f09:	e8 be f8 ff ff       	call   8017cc <fd_alloc>
  801f0e:	83 c4 10             	add    $0x10,%esp
  801f11:	85 c0                	test   %eax,%eax
  801f13:	78 55                	js     801f6a <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801f15:	83 ec 08             	sub    $0x8,%esp
  801f18:	53                   	push   %ebx
  801f19:	68 00 50 80 00       	push   $0x805000
  801f1e:	e8 fa e8 ff ff       	call   80081d <strcpy>
	fsipcbuf.open.req_omode = mode;
  801f23:	8b 45 0c             	mov    0xc(%ebp),%eax
  801f26:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801f2b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801f2e:	b8 01 00 00 00       	mov    $0x1,%eax
  801f33:	e8 f6 fd ff ff       	call   801d2e <fsipc>
  801f38:	89 c3                	mov    %eax,%ebx
  801f3a:	83 c4 10             	add    $0x10,%esp
  801f3d:	85 c0                	test   %eax,%eax
  801f3f:	79 14                	jns    801f55 <open+0x6d>
		fd_close(fd, 0);
  801f41:	83 ec 08             	sub    $0x8,%esp
  801f44:	6a 00                	push   $0x0
  801f46:	ff 75 f4             	pushl  -0xc(%ebp)
  801f49:	e8 77 f9 ff ff       	call   8018c5 <fd_close>
		return r;
  801f4e:	83 c4 10             	add    $0x10,%esp
  801f51:	89 d8                	mov    %ebx,%eax
  801f53:	eb 15                	jmp    801f6a <open+0x82>
	}

	return fd2num(fd);
  801f55:	83 ec 0c             	sub    $0xc,%esp
  801f58:	ff 75 f4             	pushl  -0xc(%ebp)
  801f5b:	e8 45 f8 ff ff       	call   8017a5 <fd2num>
  801f60:	83 c4 10             	add    $0x10,%esp
  801f63:	eb 05                	jmp    801f6a <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801f65:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801f6a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801f6d:	c9                   	leave  
  801f6e:	c3                   	ret    

00801f6f <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801f6f:	55                   	push   %ebp
  801f70:	89 e5                	mov    %esp,%ebp
  801f72:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801f75:	ba 00 00 00 00       	mov    $0x0,%edx
  801f7a:	b8 08 00 00 00       	mov    $0x8,%eax
  801f7f:	e8 aa fd ff ff       	call   801d2e <fsipc>
}
  801f84:	c9                   	leave  
  801f85:	c3                   	ret    

00801f86 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801f86:	55                   	push   %ebp
  801f87:	89 e5                	mov    %esp,%ebp
  801f89:	56                   	push   %esi
  801f8a:	53                   	push   %ebx
  801f8b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801f8e:	83 ec 0c             	sub    $0xc,%esp
  801f91:	ff 75 08             	pushl  0x8(%ebp)
  801f94:	e8 1c f8 ff ff       	call   8017b5 <fd2data>
  801f99:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801f9b:	83 c4 08             	add    $0x8,%esp
  801f9e:	68 fa 2e 80 00       	push   $0x802efa
  801fa3:	53                   	push   %ebx
  801fa4:	e8 74 e8 ff ff       	call   80081d <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801fa9:	8b 46 04             	mov    0x4(%esi),%eax
  801fac:	2b 06                	sub    (%esi),%eax
  801fae:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801fb4:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801fbb:	00 00 00 
	stat->st_dev = &devpipe;
  801fbe:	c7 83 88 00 00 00 28 	movl   $0x803028,0x88(%ebx)
  801fc5:	30 80 00 
	return 0;
}
  801fc8:	b8 00 00 00 00       	mov    $0x0,%eax
  801fcd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801fd0:	5b                   	pop    %ebx
  801fd1:	5e                   	pop    %esi
  801fd2:	5d                   	pop    %ebp
  801fd3:	c3                   	ret    

00801fd4 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801fd4:	55                   	push   %ebp
  801fd5:	89 e5                	mov    %esp,%ebp
  801fd7:	53                   	push   %ebx
  801fd8:	83 ec 0c             	sub    $0xc,%esp
  801fdb:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801fde:	53                   	push   %ebx
  801fdf:	6a 00                	push   $0x0
  801fe1:	e8 a0 ec ff ff       	call   800c86 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801fe6:	89 1c 24             	mov    %ebx,(%esp)
  801fe9:	e8 c7 f7 ff ff       	call   8017b5 <fd2data>
  801fee:	83 c4 08             	add    $0x8,%esp
  801ff1:	50                   	push   %eax
  801ff2:	6a 00                	push   $0x0
  801ff4:	e8 8d ec ff ff       	call   800c86 <sys_page_unmap>
}
  801ff9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801ffc:	c9                   	leave  
  801ffd:	c3                   	ret    

00801ffe <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801ffe:	55                   	push   %ebp
  801fff:	89 e5                	mov    %esp,%ebp
  802001:	57                   	push   %edi
  802002:	56                   	push   %esi
  802003:	53                   	push   %ebx
  802004:	83 ec 1c             	sub    $0x1c,%esp
  802007:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80200a:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  80200c:	a1 04 40 80 00       	mov    0x804004,%eax
  802011:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  802014:	83 ec 0c             	sub    $0xc,%esp
  802017:	ff 75 e0             	pushl  -0x20(%ebp)
  80201a:	e8 27 05 00 00       	call   802546 <pageref>
  80201f:	89 c3                	mov    %eax,%ebx
  802021:	89 3c 24             	mov    %edi,(%esp)
  802024:	e8 1d 05 00 00       	call   802546 <pageref>
  802029:	83 c4 10             	add    $0x10,%esp
  80202c:	39 c3                	cmp    %eax,%ebx
  80202e:	0f 94 c1             	sete   %cl
  802031:	0f b6 c9             	movzbl %cl,%ecx
  802034:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  802037:	8b 15 04 40 80 00    	mov    0x804004,%edx
  80203d:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  802040:	39 ce                	cmp    %ecx,%esi
  802042:	74 1b                	je     80205f <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  802044:	39 c3                	cmp    %eax,%ebx
  802046:	75 c4                	jne    80200c <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  802048:	8b 42 58             	mov    0x58(%edx),%eax
  80204b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80204e:	50                   	push   %eax
  80204f:	56                   	push   %esi
  802050:	68 01 2f 80 00       	push   $0x802f01
  802055:	e8 5f e2 ff ff       	call   8002b9 <cprintf>
  80205a:	83 c4 10             	add    $0x10,%esp
  80205d:	eb ad                	jmp    80200c <_pipeisclosed+0xe>
	}
}
  80205f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  802062:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802065:	5b                   	pop    %ebx
  802066:	5e                   	pop    %esi
  802067:	5f                   	pop    %edi
  802068:	5d                   	pop    %ebp
  802069:	c3                   	ret    

0080206a <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  80206a:	55                   	push   %ebp
  80206b:	89 e5                	mov    %esp,%ebp
  80206d:	57                   	push   %edi
  80206e:	56                   	push   %esi
  80206f:	53                   	push   %ebx
  802070:	83 ec 18             	sub    $0x18,%esp
  802073:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  802076:	56                   	push   %esi
  802077:	e8 39 f7 ff ff       	call   8017b5 <fd2data>
  80207c:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80207e:	83 c4 10             	add    $0x10,%esp
  802081:	bf 00 00 00 00       	mov    $0x0,%edi
  802086:	eb 3b                	jmp    8020c3 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  802088:	89 da                	mov    %ebx,%edx
  80208a:	89 f0                	mov    %esi,%eax
  80208c:	e8 6d ff ff ff       	call   801ffe <_pipeisclosed>
  802091:	85 c0                	test   %eax,%eax
  802093:	75 38                	jne    8020cd <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  802095:	e8 48 eb ff ff       	call   800be2 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  80209a:	8b 53 04             	mov    0x4(%ebx),%edx
  80209d:	8b 03                	mov    (%ebx),%eax
  80209f:	83 c0 20             	add    $0x20,%eax
  8020a2:	39 c2                	cmp    %eax,%edx
  8020a4:	73 e2                	jae    802088 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  8020a6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8020a9:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  8020ac:	89 d0                	mov    %edx,%eax
  8020ae:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8020b3:	79 05                	jns    8020ba <devpipe_write+0x50>
  8020b5:	48                   	dec    %eax
  8020b6:	83 c8 e0             	or     $0xffffffe0,%eax
  8020b9:	40                   	inc    %eax
  8020ba:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  8020be:	42                   	inc    %edx
  8020bf:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8020c2:	47                   	inc    %edi
  8020c3:	3b 7d 10             	cmp    0x10(%ebp),%edi
  8020c6:	75 d2                	jne    80209a <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  8020c8:	8b 45 10             	mov    0x10(%ebp),%eax
  8020cb:	eb 05                	jmp    8020d2 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8020cd:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  8020d2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8020d5:	5b                   	pop    %ebx
  8020d6:	5e                   	pop    %esi
  8020d7:	5f                   	pop    %edi
  8020d8:	5d                   	pop    %ebp
  8020d9:	c3                   	ret    

008020da <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  8020da:	55                   	push   %ebp
  8020db:	89 e5                	mov    %esp,%ebp
  8020dd:	57                   	push   %edi
  8020de:	56                   	push   %esi
  8020df:	53                   	push   %ebx
  8020e0:	83 ec 18             	sub    $0x18,%esp
  8020e3:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  8020e6:	57                   	push   %edi
  8020e7:	e8 c9 f6 ff ff       	call   8017b5 <fd2data>
  8020ec:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8020ee:	83 c4 10             	add    $0x10,%esp
  8020f1:	bb 00 00 00 00       	mov    $0x0,%ebx
  8020f6:	eb 3a                	jmp    802132 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  8020f8:	85 db                	test   %ebx,%ebx
  8020fa:	74 04                	je     802100 <devpipe_read+0x26>
				return i;
  8020fc:	89 d8                	mov    %ebx,%eax
  8020fe:	eb 41                	jmp    802141 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  802100:	89 f2                	mov    %esi,%edx
  802102:	89 f8                	mov    %edi,%eax
  802104:	e8 f5 fe ff ff       	call   801ffe <_pipeisclosed>
  802109:	85 c0                	test   %eax,%eax
  80210b:	75 2f                	jne    80213c <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  80210d:	e8 d0 ea ff ff       	call   800be2 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  802112:	8b 06                	mov    (%esi),%eax
  802114:	3b 46 04             	cmp    0x4(%esi),%eax
  802117:	74 df                	je     8020f8 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  802119:	25 1f 00 00 80       	and    $0x8000001f,%eax
  80211e:	79 05                	jns    802125 <devpipe_read+0x4b>
  802120:	48                   	dec    %eax
  802121:	83 c8 e0             	or     $0xffffffe0,%eax
  802124:	40                   	inc    %eax
  802125:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  802129:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80212c:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  80212f:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802131:	43                   	inc    %ebx
  802132:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  802135:	75 db                	jne    802112 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  802137:	8b 45 10             	mov    0x10(%ebp),%eax
  80213a:	eb 05                	jmp    802141 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  80213c:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  802141:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802144:	5b                   	pop    %ebx
  802145:	5e                   	pop    %esi
  802146:	5f                   	pop    %edi
  802147:	5d                   	pop    %ebp
  802148:	c3                   	ret    

00802149 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  802149:	55                   	push   %ebp
  80214a:	89 e5                	mov    %esp,%ebp
  80214c:	56                   	push   %esi
  80214d:	53                   	push   %ebx
  80214e:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  802151:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802154:	50                   	push   %eax
  802155:	e8 72 f6 ff ff       	call   8017cc <fd_alloc>
  80215a:	83 c4 10             	add    $0x10,%esp
  80215d:	85 c0                	test   %eax,%eax
  80215f:	0f 88 2a 01 00 00    	js     80228f <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  802165:	83 ec 04             	sub    $0x4,%esp
  802168:	68 07 04 00 00       	push   $0x407
  80216d:	ff 75 f4             	pushl  -0xc(%ebp)
  802170:	6a 00                	push   $0x0
  802172:	e8 8a ea ff ff       	call   800c01 <sys_page_alloc>
  802177:	83 c4 10             	add    $0x10,%esp
  80217a:	85 c0                	test   %eax,%eax
  80217c:	0f 88 0d 01 00 00    	js     80228f <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  802182:	83 ec 0c             	sub    $0xc,%esp
  802185:	8d 45 f0             	lea    -0x10(%ebp),%eax
  802188:	50                   	push   %eax
  802189:	e8 3e f6 ff ff       	call   8017cc <fd_alloc>
  80218e:	89 c3                	mov    %eax,%ebx
  802190:	83 c4 10             	add    $0x10,%esp
  802193:	85 c0                	test   %eax,%eax
  802195:	0f 88 e2 00 00 00    	js     80227d <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80219b:	83 ec 04             	sub    $0x4,%esp
  80219e:	68 07 04 00 00       	push   $0x407
  8021a3:	ff 75 f0             	pushl  -0x10(%ebp)
  8021a6:	6a 00                	push   $0x0
  8021a8:	e8 54 ea ff ff       	call   800c01 <sys_page_alloc>
  8021ad:	89 c3                	mov    %eax,%ebx
  8021af:	83 c4 10             	add    $0x10,%esp
  8021b2:	85 c0                	test   %eax,%eax
  8021b4:	0f 88 c3 00 00 00    	js     80227d <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  8021ba:	83 ec 0c             	sub    $0xc,%esp
  8021bd:	ff 75 f4             	pushl  -0xc(%ebp)
  8021c0:	e8 f0 f5 ff ff       	call   8017b5 <fd2data>
  8021c5:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8021c7:	83 c4 0c             	add    $0xc,%esp
  8021ca:	68 07 04 00 00       	push   $0x407
  8021cf:	50                   	push   %eax
  8021d0:	6a 00                	push   $0x0
  8021d2:	e8 2a ea ff ff       	call   800c01 <sys_page_alloc>
  8021d7:	89 c3                	mov    %eax,%ebx
  8021d9:	83 c4 10             	add    $0x10,%esp
  8021dc:	85 c0                	test   %eax,%eax
  8021de:	0f 88 89 00 00 00    	js     80226d <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8021e4:	83 ec 0c             	sub    $0xc,%esp
  8021e7:	ff 75 f0             	pushl  -0x10(%ebp)
  8021ea:	e8 c6 f5 ff ff       	call   8017b5 <fd2data>
  8021ef:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  8021f6:	50                   	push   %eax
  8021f7:	6a 00                	push   $0x0
  8021f9:	56                   	push   %esi
  8021fa:	6a 00                	push   $0x0
  8021fc:	e8 43 ea ff ff       	call   800c44 <sys_page_map>
  802201:	89 c3                	mov    %eax,%ebx
  802203:	83 c4 20             	add    $0x20,%esp
  802206:	85 c0                	test   %eax,%eax
  802208:	78 55                	js     80225f <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  80220a:	8b 15 28 30 80 00    	mov    0x803028,%edx
  802210:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802213:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  802215:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802218:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  80221f:	8b 15 28 30 80 00    	mov    0x803028,%edx
  802225:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802228:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  80222a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80222d:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  802234:	83 ec 0c             	sub    $0xc,%esp
  802237:	ff 75 f4             	pushl  -0xc(%ebp)
  80223a:	e8 66 f5 ff ff       	call   8017a5 <fd2num>
  80223f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802242:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  802244:	83 c4 04             	add    $0x4,%esp
  802247:	ff 75 f0             	pushl  -0x10(%ebp)
  80224a:	e8 56 f5 ff ff       	call   8017a5 <fd2num>
  80224f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802252:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  802255:	83 c4 10             	add    $0x10,%esp
  802258:	b8 00 00 00 00       	mov    $0x0,%eax
  80225d:	eb 30                	jmp    80228f <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  80225f:	83 ec 08             	sub    $0x8,%esp
  802262:	56                   	push   %esi
  802263:	6a 00                	push   $0x0
  802265:	e8 1c ea ff ff       	call   800c86 <sys_page_unmap>
  80226a:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  80226d:	83 ec 08             	sub    $0x8,%esp
  802270:	ff 75 f0             	pushl  -0x10(%ebp)
  802273:	6a 00                	push   $0x0
  802275:	e8 0c ea ff ff       	call   800c86 <sys_page_unmap>
  80227a:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  80227d:	83 ec 08             	sub    $0x8,%esp
  802280:	ff 75 f4             	pushl  -0xc(%ebp)
  802283:	6a 00                	push   $0x0
  802285:	e8 fc e9 ff ff       	call   800c86 <sys_page_unmap>
  80228a:	83 c4 10             	add    $0x10,%esp
  80228d:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  80228f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802292:	5b                   	pop    %ebx
  802293:	5e                   	pop    %esi
  802294:	5d                   	pop    %ebp
  802295:	c3                   	ret    

00802296 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  802296:	55                   	push   %ebp
  802297:	89 e5                	mov    %esp,%ebp
  802299:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80229c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80229f:	50                   	push   %eax
  8022a0:	ff 75 08             	pushl  0x8(%ebp)
  8022a3:	e8 73 f5 ff ff       	call   80181b <fd_lookup>
  8022a8:	83 c4 10             	add    $0x10,%esp
  8022ab:	85 c0                	test   %eax,%eax
  8022ad:	78 18                	js     8022c7 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  8022af:	83 ec 0c             	sub    $0xc,%esp
  8022b2:	ff 75 f4             	pushl  -0xc(%ebp)
  8022b5:	e8 fb f4 ff ff       	call   8017b5 <fd2data>
	return _pipeisclosed(fd, p);
  8022ba:	89 c2                	mov    %eax,%edx
  8022bc:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8022bf:	e8 3a fd ff ff       	call   801ffe <_pipeisclosed>
  8022c4:	83 c4 10             	add    $0x10,%esp
}
  8022c7:	c9                   	leave  
  8022c8:	c3                   	ret    

008022c9 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  8022c9:	55                   	push   %ebp
  8022ca:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  8022cc:	b8 00 00 00 00       	mov    $0x0,%eax
  8022d1:	5d                   	pop    %ebp
  8022d2:	c3                   	ret    

008022d3 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  8022d3:	55                   	push   %ebp
  8022d4:	89 e5                	mov    %esp,%ebp
  8022d6:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  8022d9:	68 19 2f 80 00       	push   $0x802f19
  8022de:	ff 75 0c             	pushl  0xc(%ebp)
  8022e1:	e8 37 e5 ff ff       	call   80081d <strcpy>
	return 0;
}
  8022e6:	b8 00 00 00 00       	mov    $0x0,%eax
  8022eb:	c9                   	leave  
  8022ec:	c3                   	ret    

008022ed <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8022ed:	55                   	push   %ebp
  8022ee:	89 e5                	mov    %esp,%ebp
  8022f0:	57                   	push   %edi
  8022f1:	56                   	push   %esi
  8022f2:	53                   	push   %ebx
  8022f3:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  8022f9:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  8022fe:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  802304:	eb 2c                	jmp    802332 <devcons_write+0x45>
		m = n - tot;
  802306:	8b 5d 10             	mov    0x10(%ebp),%ebx
  802309:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  80230b:	83 fb 7f             	cmp    $0x7f,%ebx
  80230e:	76 05                	jbe    802315 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  802310:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  802315:	83 ec 04             	sub    $0x4,%esp
  802318:	53                   	push   %ebx
  802319:	03 45 0c             	add    0xc(%ebp),%eax
  80231c:	50                   	push   %eax
  80231d:	57                   	push   %edi
  80231e:	e8 6f e6 ff ff       	call   800992 <memmove>
		sys_cputs(buf, m);
  802323:	83 c4 08             	add    $0x8,%esp
  802326:	53                   	push   %ebx
  802327:	57                   	push   %edi
  802328:	e8 18 e8 ff ff       	call   800b45 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  80232d:	01 de                	add    %ebx,%esi
  80232f:	83 c4 10             	add    $0x10,%esp
  802332:	89 f0                	mov    %esi,%eax
  802334:	3b 75 10             	cmp    0x10(%ebp),%esi
  802337:	72 cd                	jb     802306 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  802339:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80233c:	5b                   	pop    %ebx
  80233d:	5e                   	pop    %esi
  80233e:	5f                   	pop    %edi
  80233f:	5d                   	pop    %ebp
  802340:	c3                   	ret    

00802341 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  802341:	55                   	push   %ebp
  802342:	89 e5                	mov    %esp,%ebp
  802344:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  802347:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80234b:	75 07                	jne    802354 <devcons_read+0x13>
  80234d:	eb 23                	jmp    802372 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  80234f:	e8 8e e8 ff ff       	call   800be2 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  802354:	e8 0a e8 ff ff       	call   800b63 <sys_cgetc>
  802359:	85 c0                	test   %eax,%eax
  80235b:	74 f2                	je     80234f <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  80235d:	85 c0                	test   %eax,%eax
  80235f:	78 1d                	js     80237e <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  802361:	83 f8 04             	cmp    $0x4,%eax
  802364:	74 13                	je     802379 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  802366:	8b 55 0c             	mov    0xc(%ebp),%edx
  802369:	88 02                	mov    %al,(%edx)
	return 1;
  80236b:	b8 01 00 00 00       	mov    $0x1,%eax
  802370:	eb 0c                	jmp    80237e <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  802372:	b8 00 00 00 00       	mov    $0x0,%eax
  802377:	eb 05                	jmp    80237e <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  802379:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  80237e:	c9                   	leave  
  80237f:	c3                   	ret    

00802380 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  802380:	55                   	push   %ebp
  802381:	89 e5                	mov    %esp,%ebp
  802383:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  802386:	8b 45 08             	mov    0x8(%ebp),%eax
  802389:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  80238c:	6a 01                	push   $0x1
  80238e:	8d 45 f7             	lea    -0x9(%ebp),%eax
  802391:	50                   	push   %eax
  802392:	e8 ae e7 ff ff       	call   800b45 <sys_cputs>
}
  802397:	83 c4 10             	add    $0x10,%esp
  80239a:	c9                   	leave  
  80239b:	c3                   	ret    

0080239c <getchar>:

int
getchar(void)
{
  80239c:	55                   	push   %ebp
  80239d:	89 e5                	mov    %esp,%ebp
  80239f:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  8023a2:	6a 01                	push   $0x1
  8023a4:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8023a7:	50                   	push   %eax
  8023a8:	6a 00                	push   $0x0
  8023aa:	e8 d2 f6 ff ff       	call   801a81 <read>
	if (r < 0)
  8023af:	83 c4 10             	add    $0x10,%esp
  8023b2:	85 c0                	test   %eax,%eax
  8023b4:	78 0f                	js     8023c5 <getchar+0x29>
		return r;
	if (r < 1)
  8023b6:	85 c0                	test   %eax,%eax
  8023b8:	7e 06                	jle    8023c0 <getchar+0x24>
		return -E_EOF;
	return c;
  8023ba:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  8023be:	eb 05                	jmp    8023c5 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  8023c0:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  8023c5:	c9                   	leave  
  8023c6:	c3                   	ret    

008023c7 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  8023c7:	55                   	push   %ebp
  8023c8:	89 e5                	mov    %esp,%ebp
  8023ca:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8023cd:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8023d0:	50                   	push   %eax
  8023d1:	ff 75 08             	pushl  0x8(%ebp)
  8023d4:	e8 42 f4 ff ff       	call   80181b <fd_lookup>
  8023d9:	83 c4 10             	add    $0x10,%esp
  8023dc:	85 c0                	test   %eax,%eax
  8023de:	78 11                	js     8023f1 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  8023e0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8023e3:	8b 15 44 30 80 00    	mov    0x803044,%edx
  8023e9:	39 10                	cmp    %edx,(%eax)
  8023eb:	0f 94 c0             	sete   %al
  8023ee:	0f b6 c0             	movzbl %al,%eax
}
  8023f1:	c9                   	leave  
  8023f2:	c3                   	ret    

008023f3 <opencons>:

int
opencons(void)
{
  8023f3:	55                   	push   %ebp
  8023f4:	89 e5                	mov    %esp,%ebp
  8023f6:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  8023f9:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8023fc:	50                   	push   %eax
  8023fd:	e8 ca f3 ff ff       	call   8017cc <fd_alloc>
  802402:	83 c4 10             	add    $0x10,%esp
  802405:	85 c0                	test   %eax,%eax
  802407:	78 3a                	js     802443 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  802409:	83 ec 04             	sub    $0x4,%esp
  80240c:	68 07 04 00 00       	push   $0x407
  802411:	ff 75 f4             	pushl  -0xc(%ebp)
  802414:	6a 00                	push   $0x0
  802416:	e8 e6 e7 ff ff       	call   800c01 <sys_page_alloc>
  80241b:	83 c4 10             	add    $0x10,%esp
  80241e:	85 c0                	test   %eax,%eax
  802420:	78 21                	js     802443 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  802422:	8b 15 44 30 80 00    	mov    0x803044,%edx
  802428:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80242b:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  80242d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802430:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  802437:	83 ec 0c             	sub    $0xc,%esp
  80243a:	50                   	push   %eax
  80243b:	e8 65 f3 ff ff       	call   8017a5 <fd2num>
  802440:	83 c4 10             	add    $0x10,%esp
}
  802443:	c9                   	leave  
  802444:	c3                   	ret    

00802445 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  802445:	55                   	push   %ebp
  802446:	89 e5                	mov    %esp,%ebp
  802448:	56                   	push   %esi
  802449:	53                   	push   %ebx
  80244a:	8b 75 08             	mov    0x8(%ebp),%esi
  80244d:	8b 45 0c             	mov    0xc(%ebp),%eax
  802450:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  802453:	85 c0                	test   %eax,%eax
  802455:	75 05                	jne    80245c <ipc_recv+0x17>
        pg = (void *)UTOP;
  802457:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  80245c:	83 ec 0c             	sub    $0xc,%esp
  80245f:	50                   	push   %eax
  802460:	e8 6b e9 ff ff       	call   800dd0 <sys_ipc_recv>
  802465:	83 c4 10             	add    $0x10,%esp
  802468:	85 c0                	test   %eax,%eax
  80246a:	74 16                	je     802482 <ipc_recv+0x3d>
        if (from_env_store)
  80246c:	85 f6                	test   %esi,%esi
  80246e:	74 06                	je     802476 <ipc_recv+0x31>
            *from_env_store = 0;
  802470:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  802476:	85 db                	test   %ebx,%ebx
  802478:	74 2c                	je     8024a6 <ipc_recv+0x61>
            *perm_store = 0;
  80247a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  802480:	eb 24                	jmp    8024a6 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  802482:	85 f6                	test   %esi,%esi
  802484:	74 0a                	je     802490 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  802486:	a1 04 40 80 00       	mov    0x804004,%eax
  80248b:	8b 40 74             	mov    0x74(%eax),%eax
  80248e:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  802490:	85 db                	test   %ebx,%ebx
  802492:	74 0a                	je     80249e <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  802494:	a1 04 40 80 00       	mov    0x804004,%eax
  802499:	8b 40 78             	mov    0x78(%eax),%eax
  80249c:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  80249e:	a1 04 40 80 00       	mov    0x804004,%eax
  8024a3:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  8024a6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8024a9:	5b                   	pop    %ebx
  8024aa:	5e                   	pop    %esi
  8024ab:	5d                   	pop    %ebp
  8024ac:	c3                   	ret    

008024ad <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  8024ad:	55                   	push   %ebp
  8024ae:	89 e5                	mov    %esp,%ebp
  8024b0:	57                   	push   %edi
  8024b1:	56                   	push   %esi
  8024b2:	53                   	push   %ebx
  8024b3:	83 ec 0c             	sub    $0xc,%esp
  8024b6:	8b 75 0c             	mov    0xc(%ebp),%esi
  8024b9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8024bc:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  8024bf:	85 db                	test   %ebx,%ebx
  8024c1:	75 0c                	jne    8024cf <ipc_send+0x22>
        pg = (void *)UTOP;
  8024c3:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  8024c8:	eb 05                	jmp    8024cf <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  8024ca:	e8 13 e7 ff ff       	call   800be2 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  8024cf:	57                   	push   %edi
  8024d0:	53                   	push   %ebx
  8024d1:	56                   	push   %esi
  8024d2:	ff 75 08             	pushl  0x8(%ebp)
  8024d5:	e8 d3 e8 ff ff       	call   800dad <sys_ipc_try_send>
  8024da:	83 c4 10             	add    $0x10,%esp
  8024dd:	83 f8 f9             	cmp    $0xfffffff9,%eax
  8024e0:	74 e8                	je     8024ca <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  8024e2:	85 c0                	test   %eax,%eax
  8024e4:	74 14                	je     8024fa <ipc_send+0x4d>
        panic("ipc_send: error");
  8024e6:	83 ec 04             	sub    $0x4,%esp
  8024e9:	68 25 2f 80 00       	push   $0x802f25
  8024ee:	6a 6a                	push   $0x6a
  8024f0:	68 35 2f 80 00       	push   $0x802f35
  8024f5:	e8 e7 dc ff ff       	call   8001e1 <_panic>
    }
	//panic("ipc_send not implemented");
}
  8024fa:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8024fd:	5b                   	pop    %ebx
  8024fe:	5e                   	pop    %esi
  8024ff:	5f                   	pop    %edi
  802500:	5d                   	pop    %ebp
  802501:	c3                   	ret    

00802502 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  802502:	55                   	push   %ebp
  802503:	89 e5                	mov    %esp,%ebp
  802505:	53                   	push   %ebx
  802506:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  802509:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  80250e:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  802515:	89 d0                	mov    %edx,%eax
  802517:	c1 e0 07             	shl    $0x7,%eax
  80251a:	29 d8                	sub    %ebx,%eax
  80251c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  802521:	8b 40 50             	mov    0x50(%eax),%eax
  802524:	39 c8                	cmp    %ecx,%eax
  802526:	75 0d                	jne    802535 <ipc_find_env+0x33>
			return envs[i].env_id;
  802528:	c1 e2 07             	shl    $0x7,%edx
  80252b:	29 da                	sub    %ebx,%edx
  80252d:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  802533:	eb 0e                	jmp    802543 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  802535:	42                   	inc    %edx
  802536:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  80253c:	75 d0                	jne    80250e <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  80253e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802543:	5b                   	pop    %ebx
  802544:	5d                   	pop    %ebp
  802545:	c3                   	ret    

00802546 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802546:	55                   	push   %ebp
  802547:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  802549:	8b 45 08             	mov    0x8(%ebp),%eax
  80254c:	c1 e8 16             	shr    $0x16,%eax
  80254f:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  802556:	a8 01                	test   $0x1,%al
  802558:	74 21                	je     80257b <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  80255a:	8b 45 08             	mov    0x8(%ebp),%eax
  80255d:	c1 e8 0c             	shr    $0xc,%eax
  802560:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  802567:	a8 01                	test   $0x1,%al
  802569:	74 17                	je     802582 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  80256b:	c1 e8 0c             	shr    $0xc,%eax
  80256e:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  802575:	ef 
  802576:	0f b7 c0             	movzwl %ax,%eax
  802579:	eb 0c                	jmp    802587 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  80257b:	b8 00 00 00 00       	mov    $0x0,%eax
  802580:	eb 05                	jmp    802587 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  802582:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  802587:	5d                   	pop    %ebp
  802588:	c3                   	ret    
  802589:	66 90                	xchg   %ax,%ax
  80258b:	90                   	nop

0080258c <__udivdi3>:
  80258c:	55                   	push   %ebp
  80258d:	57                   	push   %edi
  80258e:	56                   	push   %esi
  80258f:	53                   	push   %ebx
  802590:	83 ec 1c             	sub    $0x1c,%esp
  802593:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  802597:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  80259b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80259f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8025a3:	89 ca                	mov    %ecx,%edx
  8025a5:	89 f8                	mov    %edi,%eax
  8025a7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8025ab:	85 f6                	test   %esi,%esi
  8025ad:	75 2d                	jne    8025dc <__udivdi3+0x50>
  8025af:	39 cf                	cmp    %ecx,%edi
  8025b1:	77 65                	ja     802618 <__udivdi3+0x8c>
  8025b3:	89 fd                	mov    %edi,%ebp
  8025b5:	85 ff                	test   %edi,%edi
  8025b7:	75 0b                	jne    8025c4 <__udivdi3+0x38>
  8025b9:	b8 01 00 00 00       	mov    $0x1,%eax
  8025be:	31 d2                	xor    %edx,%edx
  8025c0:	f7 f7                	div    %edi
  8025c2:	89 c5                	mov    %eax,%ebp
  8025c4:	31 d2                	xor    %edx,%edx
  8025c6:	89 c8                	mov    %ecx,%eax
  8025c8:	f7 f5                	div    %ebp
  8025ca:	89 c1                	mov    %eax,%ecx
  8025cc:	89 d8                	mov    %ebx,%eax
  8025ce:	f7 f5                	div    %ebp
  8025d0:	89 cf                	mov    %ecx,%edi
  8025d2:	89 fa                	mov    %edi,%edx
  8025d4:	83 c4 1c             	add    $0x1c,%esp
  8025d7:	5b                   	pop    %ebx
  8025d8:	5e                   	pop    %esi
  8025d9:	5f                   	pop    %edi
  8025da:	5d                   	pop    %ebp
  8025db:	c3                   	ret    
  8025dc:	39 ce                	cmp    %ecx,%esi
  8025de:	77 28                	ja     802608 <__udivdi3+0x7c>
  8025e0:	0f bd fe             	bsr    %esi,%edi
  8025e3:	83 f7 1f             	xor    $0x1f,%edi
  8025e6:	75 40                	jne    802628 <__udivdi3+0x9c>
  8025e8:	39 ce                	cmp    %ecx,%esi
  8025ea:	72 0a                	jb     8025f6 <__udivdi3+0x6a>
  8025ec:	3b 44 24 08          	cmp    0x8(%esp),%eax
  8025f0:	0f 87 9e 00 00 00    	ja     802694 <__udivdi3+0x108>
  8025f6:	b8 01 00 00 00       	mov    $0x1,%eax
  8025fb:	89 fa                	mov    %edi,%edx
  8025fd:	83 c4 1c             	add    $0x1c,%esp
  802600:	5b                   	pop    %ebx
  802601:	5e                   	pop    %esi
  802602:	5f                   	pop    %edi
  802603:	5d                   	pop    %ebp
  802604:	c3                   	ret    
  802605:	8d 76 00             	lea    0x0(%esi),%esi
  802608:	31 ff                	xor    %edi,%edi
  80260a:	31 c0                	xor    %eax,%eax
  80260c:	89 fa                	mov    %edi,%edx
  80260e:	83 c4 1c             	add    $0x1c,%esp
  802611:	5b                   	pop    %ebx
  802612:	5e                   	pop    %esi
  802613:	5f                   	pop    %edi
  802614:	5d                   	pop    %ebp
  802615:	c3                   	ret    
  802616:	66 90                	xchg   %ax,%ax
  802618:	89 d8                	mov    %ebx,%eax
  80261a:	f7 f7                	div    %edi
  80261c:	31 ff                	xor    %edi,%edi
  80261e:	89 fa                	mov    %edi,%edx
  802620:	83 c4 1c             	add    $0x1c,%esp
  802623:	5b                   	pop    %ebx
  802624:	5e                   	pop    %esi
  802625:	5f                   	pop    %edi
  802626:	5d                   	pop    %ebp
  802627:	c3                   	ret    
  802628:	bd 20 00 00 00       	mov    $0x20,%ebp
  80262d:	89 eb                	mov    %ebp,%ebx
  80262f:	29 fb                	sub    %edi,%ebx
  802631:	89 f9                	mov    %edi,%ecx
  802633:	d3 e6                	shl    %cl,%esi
  802635:	89 c5                	mov    %eax,%ebp
  802637:	88 d9                	mov    %bl,%cl
  802639:	d3 ed                	shr    %cl,%ebp
  80263b:	89 e9                	mov    %ebp,%ecx
  80263d:	09 f1                	or     %esi,%ecx
  80263f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  802643:	89 f9                	mov    %edi,%ecx
  802645:	d3 e0                	shl    %cl,%eax
  802647:	89 c5                	mov    %eax,%ebp
  802649:	89 d6                	mov    %edx,%esi
  80264b:	88 d9                	mov    %bl,%cl
  80264d:	d3 ee                	shr    %cl,%esi
  80264f:	89 f9                	mov    %edi,%ecx
  802651:	d3 e2                	shl    %cl,%edx
  802653:	8b 44 24 08          	mov    0x8(%esp),%eax
  802657:	88 d9                	mov    %bl,%cl
  802659:	d3 e8                	shr    %cl,%eax
  80265b:	09 c2                	or     %eax,%edx
  80265d:	89 d0                	mov    %edx,%eax
  80265f:	89 f2                	mov    %esi,%edx
  802661:	f7 74 24 0c          	divl   0xc(%esp)
  802665:	89 d6                	mov    %edx,%esi
  802667:	89 c3                	mov    %eax,%ebx
  802669:	f7 e5                	mul    %ebp
  80266b:	39 d6                	cmp    %edx,%esi
  80266d:	72 19                	jb     802688 <__udivdi3+0xfc>
  80266f:	74 0b                	je     80267c <__udivdi3+0xf0>
  802671:	89 d8                	mov    %ebx,%eax
  802673:	31 ff                	xor    %edi,%edi
  802675:	e9 58 ff ff ff       	jmp    8025d2 <__udivdi3+0x46>
  80267a:	66 90                	xchg   %ax,%ax
  80267c:	8b 54 24 08          	mov    0x8(%esp),%edx
  802680:	89 f9                	mov    %edi,%ecx
  802682:	d3 e2                	shl    %cl,%edx
  802684:	39 c2                	cmp    %eax,%edx
  802686:	73 e9                	jae    802671 <__udivdi3+0xe5>
  802688:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80268b:	31 ff                	xor    %edi,%edi
  80268d:	e9 40 ff ff ff       	jmp    8025d2 <__udivdi3+0x46>
  802692:	66 90                	xchg   %ax,%ax
  802694:	31 c0                	xor    %eax,%eax
  802696:	e9 37 ff ff ff       	jmp    8025d2 <__udivdi3+0x46>
  80269b:	90                   	nop

0080269c <__umoddi3>:
  80269c:	55                   	push   %ebp
  80269d:	57                   	push   %edi
  80269e:	56                   	push   %esi
  80269f:	53                   	push   %ebx
  8026a0:	83 ec 1c             	sub    $0x1c,%esp
  8026a3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8026a7:	8b 74 24 34          	mov    0x34(%esp),%esi
  8026ab:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8026af:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8026b3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8026b7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8026bb:	89 f3                	mov    %esi,%ebx
  8026bd:	89 fa                	mov    %edi,%edx
  8026bf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8026c3:	89 34 24             	mov    %esi,(%esp)
  8026c6:	85 c0                	test   %eax,%eax
  8026c8:	75 1a                	jne    8026e4 <__umoddi3+0x48>
  8026ca:	39 f7                	cmp    %esi,%edi
  8026cc:	0f 86 a2 00 00 00    	jbe    802774 <__umoddi3+0xd8>
  8026d2:	89 c8                	mov    %ecx,%eax
  8026d4:	89 f2                	mov    %esi,%edx
  8026d6:	f7 f7                	div    %edi
  8026d8:	89 d0                	mov    %edx,%eax
  8026da:	31 d2                	xor    %edx,%edx
  8026dc:	83 c4 1c             	add    $0x1c,%esp
  8026df:	5b                   	pop    %ebx
  8026e0:	5e                   	pop    %esi
  8026e1:	5f                   	pop    %edi
  8026e2:	5d                   	pop    %ebp
  8026e3:	c3                   	ret    
  8026e4:	39 f0                	cmp    %esi,%eax
  8026e6:	0f 87 ac 00 00 00    	ja     802798 <__umoddi3+0xfc>
  8026ec:	0f bd e8             	bsr    %eax,%ebp
  8026ef:	83 f5 1f             	xor    $0x1f,%ebp
  8026f2:	0f 84 ac 00 00 00    	je     8027a4 <__umoddi3+0x108>
  8026f8:	bf 20 00 00 00       	mov    $0x20,%edi
  8026fd:	29 ef                	sub    %ebp,%edi
  8026ff:	89 fe                	mov    %edi,%esi
  802701:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802705:	89 e9                	mov    %ebp,%ecx
  802707:	d3 e0                	shl    %cl,%eax
  802709:	89 d7                	mov    %edx,%edi
  80270b:	89 f1                	mov    %esi,%ecx
  80270d:	d3 ef                	shr    %cl,%edi
  80270f:	09 c7                	or     %eax,%edi
  802711:	89 e9                	mov    %ebp,%ecx
  802713:	d3 e2                	shl    %cl,%edx
  802715:	89 14 24             	mov    %edx,(%esp)
  802718:	89 d8                	mov    %ebx,%eax
  80271a:	d3 e0                	shl    %cl,%eax
  80271c:	89 c2                	mov    %eax,%edx
  80271e:	8b 44 24 08          	mov    0x8(%esp),%eax
  802722:	d3 e0                	shl    %cl,%eax
  802724:	89 44 24 04          	mov    %eax,0x4(%esp)
  802728:	8b 44 24 08          	mov    0x8(%esp),%eax
  80272c:	89 f1                	mov    %esi,%ecx
  80272e:	d3 e8                	shr    %cl,%eax
  802730:	09 d0                	or     %edx,%eax
  802732:	d3 eb                	shr    %cl,%ebx
  802734:	89 da                	mov    %ebx,%edx
  802736:	f7 f7                	div    %edi
  802738:	89 d3                	mov    %edx,%ebx
  80273a:	f7 24 24             	mull   (%esp)
  80273d:	89 c6                	mov    %eax,%esi
  80273f:	89 d1                	mov    %edx,%ecx
  802741:	39 d3                	cmp    %edx,%ebx
  802743:	0f 82 87 00 00 00    	jb     8027d0 <__umoddi3+0x134>
  802749:	0f 84 91 00 00 00    	je     8027e0 <__umoddi3+0x144>
  80274f:	8b 54 24 04          	mov    0x4(%esp),%edx
  802753:	29 f2                	sub    %esi,%edx
  802755:	19 cb                	sbb    %ecx,%ebx
  802757:	89 d8                	mov    %ebx,%eax
  802759:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  80275d:	d3 e0                	shl    %cl,%eax
  80275f:	89 e9                	mov    %ebp,%ecx
  802761:	d3 ea                	shr    %cl,%edx
  802763:	09 d0                	or     %edx,%eax
  802765:	89 e9                	mov    %ebp,%ecx
  802767:	d3 eb                	shr    %cl,%ebx
  802769:	89 da                	mov    %ebx,%edx
  80276b:	83 c4 1c             	add    $0x1c,%esp
  80276e:	5b                   	pop    %ebx
  80276f:	5e                   	pop    %esi
  802770:	5f                   	pop    %edi
  802771:	5d                   	pop    %ebp
  802772:	c3                   	ret    
  802773:	90                   	nop
  802774:	89 fd                	mov    %edi,%ebp
  802776:	85 ff                	test   %edi,%edi
  802778:	75 0b                	jne    802785 <__umoddi3+0xe9>
  80277a:	b8 01 00 00 00       	mov    $0x1,%eax
  80277f:	31 d2                	xor    %edx,%edx
  802781:	f7 f7                	div    %edi
  802783:	89 c5                	mov    %eax,%ebp
  802785:	89 f0                	mov    %esi,%eax
  802787:	31 d2                	xor    %edx,%edx
  802789:	f7 f5                	div    %ebp
  80278b:	89 c8                	mov    %ecx,%eax
  80278d:	f7 f5                	div    %ebp
  80278f:	89 d0                	mov    %edx,%eax
  802791:	e9 44 ff ff ff       	jmp    8026da <__umoddi3+0x3e>
  802796:	66 90                	xchg   %ax,%ax
  802798:	89 c8                	mov    %ecx,%eax
  80279a:	89 f2                	mov    %esi,%edx
  80279c:	83 c4 1c             	add    $0x1c,%esp
  80279f:	5b                   	pop    %ebx
  8027a0:	5e                   	pop    %esi
  8027a1:	5f                   	pop    %edi
  8027a2:	5d                   	pop    %ebp
  8027a3:	c3                   	ret    
  8027a4:	3b 04 24             	cmp    (%esp),%eax
  8027a7:	72 06                	jb     8027af <__umoddi3+0x113>
  8027a9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8027ad:	77 0f                	ja     8027be <__umoddi3+0x122>
  8027af:	89 f2                	mov    %esi,%edx
  8027b1:	29 f9                	sub    %edi,%ecx
  8027b3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8027b7:	89 14 24             	mov    %edx,(%esp)
  8027ba:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8027be:	8b 44 24 04          	mov    0x4(%esp),%eax
  8027c2:	8b 14 24             	mov    (%esp),%edx
  8027c5:	83 c4 1c             	add    $0x1c,%esp
  8027c8:	5b                   	pop    %ebx
  8027c9:	5e                   	pop    %esi
  8027ca:	5f                   	pop    %edi
  8027cb:	5d                   	pop    %ebp
  8027cc:	c3                   	ret    
  8027cd:	8d 76 00             	lea    0x0(%esi),%esi
  8027d0:	2b 04 24             	sub    (%esp),%eax
  8027d3:	19 fa                	sbb    %edi,%edx
  8027d5:	89 d1                	mov    %edx,%ecx
  8027d7:	89 c6                	mov    %eax,%esi
  8027d9:	e9 71 ff ff ff       	jmp    80274f <__umoddi3+0xb3>
  8027de:	66 90                	xchg   %ax,%ax
  8027e0:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8027e4:	72 ea                	jb     8027d0 <__umoddi3+0x134>
  8027e6:	89 d9                	mov    %ebx,%ecx
  8027e8:	e9 62 ff ff ff       	jmp    80274f <__umoddi3+0xb3>
