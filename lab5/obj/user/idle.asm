
obj/user/idle.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 19 00 00 00       	call   80004a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
#include <inc/x86.h>
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 08             	sub    $0x8,%esp
	binaryname = "idle";
  800039:	c7 05 00 20 80 00 40 	movl   $0x800f40,0x802000
  800040:	0f 80 00 
	// Instead of busy-waiting like this,
	// a better way would be to use the processor's HLT instruction
	// to cause the processor to stop executing until the next interrupt -
	// doing so allows the processor to conserve power more effectively.
	while (1) {
		sys_yield();
  800043:	e8 00 01 00 00       	call   800148 <sys_yield>
  800048:	eb f9                	jmp    800043 <umain+0x10>

0080004a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004a:	55                   	push   %ebp
  80004b:	89 e5                	mov    %esp,%ebp
  80004d:	56                   	push   %esi
  80004e:	53                   	push   %ebx
  80004f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800052:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800055:	e8 cf 00 00 00       	call   800129 <sys_getenvid>
  80005a:	25 ff 03 00 00       	and    $0x3ff,%eax
  80005f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800066:	c1 e0 07             	shl    $0x7,%eax
  800069:	29 d0                	sub    %edx,%eax
  80006b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800070:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800075:	85 db                	test   %ebx,%ebx
  800077:	7e 07                	jle    800080 <libmain+0x36>
		binaryname = argv[0];
  800079:	8b 06                	mov    (%esi),%eax
  80007b:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800080:	83 ec 08             	sub    $0x8,%esp
  800083:	56                   	push   %esi
  800084:	53                   	push   %ebx
  800085:	e8 a9 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008a:	e8 0a 00 00 00       	call   800099 <exit>
}
  80008f:	83 c4 10             	add    $0x10,%esp
  800092:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800095:	5b                   	pop    %ebx
  800096:	5e                   	pop    %esi
  800097:	5d                   	pop    %ebp
  800098:	c3                   	ret    

00800099 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800099:	55                   	push   %ebp
  80009a:	89 e5                	mov    %esp,%ebp
  80009c:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80009f:	6a 00                	push   $0x0
  8000a1:	e8 42 00 00 00       	call   8000e8 <sys_env_destroy>
}
  8000a6:	83 c4 10             	add    $0x10,%esp
  8000a9:	c9                   	leave  
  8000aa:	c3                   	ret    

008000ab <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000ab:	55                   	push   %ebp
  8000ac:	89 e5                	mov    %esp,%ebp
  8000ae:	57                   	push   %edi
  8000af:	56                   	push   %esi
  8000b0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000b1:	b8 00 00 00 00       	mov    $0x0,%eax
  8000b6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b9:	8b 55 08             	mov    0x8(%ebp),%edx
  8000bc:	89 c3                	mov    %eax,%ebx
  8000be:	89 c7                	mov    %eax,%edi
  8000c0:	89 c6                	mov    %eax,%esi
  8000c2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c4:	5b                   	pop    %ebx
  8000c5:	5e                   	pop    %esi
  8000c6:	5f                   	pop    %edi
  8000c7:	5d                   	pop    %ebp
  8000c8:	c3                   	ret    

008000c9 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c9:	55                   	push   %ebp
  8000ca:	89 e5                	mov    %esp,%ebp
  8000cc:	57                   	push   %edi
  8000cd:	56                   	push   %esi
  8000ce:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d4:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d9:	89 d1                	mov    %edx,%ecx
  8000db:	89 d3                	mov    %edx,%ebx
  8000dd:	89 d7                	mov    %edx,%edi
  8000df:	89 d6                	mov    %edx,%esi
  8000e1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e3:	5b                   	pop    %ebx
  8000e4:	5e                   	pop    %esi
  8000e5:	5f                   	pop    %edi
  8000e6:	5d                   	pop    %ebp
  8000e7:	c3                   	ret    

008000e8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e8:	55                   	push   %ebp
  8000e9:	89 e5                	mov    %esp,%ebp
  8000eb:	57                   	push   %edi
  8000ec:	56                   	push   %esi
  8000ed:	53                   	push   %ebx
  8000ee:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000f1:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000f6:	b8 03 00 00 00       	mov    $0x3,%eax
  8000fb:	8b 55 08             	mov    0x8(%ebp),%edx
  8000fe:	89 cb                	mov    %ecx,%ebx
  800100:	89 cf                	mov    %ecx,%edi
  800102:	89 ce                	mov    %ecx,%esi
  800104:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800106:	85 c0                	test   %eax,%eax
  800108:	7e 17                	jle    800121 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80010a:	83 ec 0c             	sub    $0xc,%esp
  80010d:	50                   	push   %eax
  80010e:	6a 03                	push   $0x3
  800110:	68 4f 0f 80 00       	push   $0x800f4f
  800115:	6a 23                	push   $0x23
  800117:	68 6c 0f 80 00       	push   $0x800f6c
  80011c:	e8 56 02 00 00       	call   800377 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800121:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800124:	5b                   	pop    %ebx
  800125:	5e                   	pop    %esi
  800126:	5f                   	pop    %edi
  800127:	5d                   	pop    %ebp
  800128:	c3                   	ret    

00800129 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800129:	55                   	push   %ebp
  80012a:	89 e5                	mov    %esp,%ebp
  80012c:	57                   	push   %edi
  80012d:	56                   	push   %esi
  80012e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012f:	ba 00 00 00 00       	mov    $0x0,%edx
  800134:	b8 02 00 00 00       	mov    $0x2,%eax
  800139:	89 d1                	mov    %edx,%ecx
  80013b:	89 d3                	mov    %edx,%ebx
  80013d:	89 d7                	mov    %edx,%edi
  80013f:	89 d6                	mov    %edx,%esi
  800141:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800143:	5b                   	pop    %ebx
  800144:	5e                   	pop    %esi
  800145:	5f                   	pop    %edi
  800146:	5d                   	pop    %ebp
  800147:	c3                   	ret    

00800148 <sys_yield>:

void
sys_yield(void)
{
  800148:	55                   	push   %ebp
  800149:	89 e5                	mov    %esp,%ebp
  80014b:	57                   	push   %edi
  80014c:	56                   	push   %esi
  80014d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80014e:	ba 00 00 00 00       	mov    $0x0,%edx
  800153:	b8 0b 00 00 00       	mov    $0xb,%eax
  800158:	89 d1                	mov    %edx,%ecx
  80015a:	89 d3                	mov    %edx,%ebx
  80015c:	89 d7                	mov    %edx,%edi
  80015e:	89 d6                	mov    %edx,%esi
  800160:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800162:	5b                   	pop    %ebx
  800163:	5e                   	pop    %esi
  800164:	5f                   	pop    %edi
  800165:	5d                   	pop    %ebp
  800166:	c3                   	ret    

00800167 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800167:	55                   	push   %ebp
  800168:	89 e5                	mov    %esp,%ebp
  80016a:	57                   	push   %edi
  80016b:	56                   	push   %esi
  80016c:	53                   	push   %ebx
  80016d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800170:	be 00 00 00 00       	mov    $0x0,%esi
  800175:	b8 04 00 00 00       	mov    $0x4,%eax
  80017a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80017d:	8b 55 08             	mov    0x8(%ebp),%edx
  800180:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800183:	89 f7                	mov    %esi,%edi
  800185:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800187:	85 c0                	test   %eax,%eax
  800189:	7e 17                	jle    8001a2 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80018b:	83 ec 0c             	sub    $0xc,%esp
  80018e:	50                   	push   %eax
  80018f:	6a 04                	push   $0x4
  800191:	68 4f 0f 80 00       	push   $0x800f4f
  800196:	6a 23                	push   $0x23
  800198:	68 6c 0f 80 00       	push   $0x800f6c
  80019d:	e8 d5 01 00 00       	call   800377 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001a2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a5:	5b                   	pop    %ebx
  8001a6:	5e                   	pop    %esi
  8001a7:	5f                   	pop    %edi
  8001a8:	5d                   	pop    %ebp
  8001a9:	c3                   	ret    

008001aa <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001aa:	55                   	push   %ebp
  8001ab:	89 e5                	mov    %esp,%ebp
  8001ad:	57                   	push   %edi
  8001ae:	56                   	push   %esi
  8001af:	53                   	push   %ebx
  8001b0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001b3:	b8 05 00 00 00       	mov    $0x5,%eax
  8001b8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001bb:	8b 55 08             	mov    0x8(%ebp),%edx
  8001be:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001c1:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001c4:	8b 75 18             	mov    0x18(%ebp),%esi
  8001c7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c9:	85 c0                	test   %eax,%eax
  8001cb:	7e 17                	jle    8001e4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001cd:	83 ec 0c             	sub    $0xc,%esp
  8001d0:	50                   	push   %eax
  8001d1:	6a 05                	push   $0x5
  8001d3:	68 4f 0f 80 00       	push   $0x800f4f
  8001d8:	6a 23                	push   $0x23
  8001da:	68 6c 0f 80 00       	push   $0x800f6c
  8001df:	e8 93 01 00 00       	call   800377 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001e4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001e7:	5b                   	pop    %ebx
  8001e8:	5e                   	pop    %esi
  8001e9:	5f                   	pop    %edi
  8001ea:	5d                   	pop    %ebp
  8001eb:	c3                   	ret    

008001ec <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001ec:	55                   	push   %ebp
  8001ed:	89 e5                	mov    %esp,%ebp
  8001ef:	57                   	push   %edi
  8001f0:	56                   	push   %esi
  8001f1:	53                   	push   %ebx
  8001f2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001f5:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001fa:	b8 06 00 00 00       	mov    $0x6,%eax
  8001ff:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800202:	8b 55 08             	mov    0x8(%ebp),%edx
  800205:	89 df                	mov    %ebx,%edi
  800207:	89 de                	mov    %ebx,%esi
  800209:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80020b:	85 c0                	test   %eax,%eax
  80020d:	7e 17                	jle    800226 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80020f:	83 ec 0c             	sub    $0xc,%esp
  800212:	50                   	push   %eax
  800213:	6a 06                	push   $0x6
  800215:	68 4f 0f 80 00       	push   $0x800f4f
  80021a:	6a 23                	push   $0x23
  80021c:	68 6c 0f 80 00       	push   $0x800f6c
  800221:	e8 51 01 00 00       	call   800377 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800226:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800229:	5b                   	pop    %ebx
  80022a:	5e                   	pop    %esi
  80022b:	5f                   	pop    %edi
  80022c:	5d                   	pop    %ebp
  80022d:	c3                   	ret    

0080022e <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80022e:	55                   	push   %ebp
  80022f:	89 e5                	mov    %esp,%ebp
  800231:	57                   	push   %edi
  800232:	56                   	push   %esi
  800233:	53                   	push   %ebx
  800234:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800237:	bb 00 00 00 00       	mov    $0x0,%ebx
  80023c:	b8 08 00 00 00       	mov    $0x8,%eax
  800241:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800244:	8b 55 08             	mov    0x8(%ebp),%edx
  800247:	89 df                	mov    %ebx,%edi
  800249:	89 de                	mov    %ebx,%esi
  80024b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80024d:	85 c0                	test   %eax,%eax
  80024f:	7e 17                	jle    800268 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800251:	83 ec 0c             	sub    $0xc,%esp
  800254:	50                   	push   %eax
  800255:	6a 08                	push   $0x8
  800257:	68 4f 0f 80 00       	push   $0x800f4f
  80025c:	6a 23                	push   $0x23
  80025e:	68 6c 0f 80 00       	push   $0x800f6c
  800263:	e8 0f 01 00 00       	call   800377 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800268:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80026b:	5b                   	pop    %ebx
  80026c:	5e                   	pop    %esi
  80026d:	5f                   	pop    %edi
  80026e:	5d                   	pop    %ebp
  80026f:	c3                   	ret    

00800270 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800270:	55                   	push   %ebp
  800271:	89 e5                	mov    %esp,%ebp
  800273:	57                   	push   %edi
  800274:	56                   	push   %esi
  800275:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800276:	ba 00 00 00 00       	mov    $0x0,%edx
  80027b:	b8 0c 00 00 00       	mov    $0xc,%eax
  800280:	89 d1                	mov    %edx,%ecx
  800282:	89 d3                	mov    %edx,%ebx
  800284:	89 d7                	mov    %edx,%edi
  800286:	89 d6                	mov    %edx,%esi
  800288:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80028a:	5b                   	pop    %ebx
  80028b:	5e                   	pop    %esi
  80028c:	5f                   	pop    %edi
  80028d:	5d                   	pop    %ebp
  80028e:	c3                   	ret    

0080028f <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  80028f:	55                   	push   %ebp
  800290:	89 e5                	mov    %esp,%ebp
  800292:	57                   	push   %edi
  800293:	56                   	push   %esi
  800294:	53                   	push   %ebx
  800295:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800298:	bb 00 00 00 00       	mov    $0x0,%ebx
  80029d:	b8 09 00 00 00       	mov    $0x9,%eax
  8002a2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002a5:	8b 55 08             	mov    0x8(%ebp),%edx
  8002a8:	89 df                	mov    %ebx,%edi
  8002aa:	89 de                	mov    %ebx,%esi
  8002ac:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002ae:	85 c0                	test   %eax,%eax
  8002b0:	7e 17                	jle    8002c9 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002b2:	83 ec 0c             	sub    $0xc,%esp
  8002b5:	50                   	push   %eax
  8002b6:	6a 09                	push   $0x9
  8002b8:	68 4f 0f 80 00       	push   $0x800f4f
  8002bd:	6a 23                	push   $0x23
  8002bf:	68 6c 0f 80 00       	push   $0x800f6c
  8002c4:	e8 ae 00 00 00       	call   800377 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002cc:	5b                   	pop    %ebx
  8002cd:	5e                   	pop    %esi
  8002ce:	5f                   	pop    %edi
  8002cf:	5d                   	pop    %ebp
  8002d0:	c3                   	ret    

008002d1 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002d1:	55                   	push   %ebp
  8002d2:	89 e5                	mov    %esp,%ebp
  8002d4:	57                   	push   %edi
  8002d5:	56                   	push   %esi
  8002d6:	53                   	push   %ebx
  8002d7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002da:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002df:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002e4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002e7:	8b 55 08             	mov    0x8(%ebp),%edx
  8002ea:	89 df                	mov    %ebx,%edi
  8002ec:	89 de                	mov    %ebx,%esi
  8002ee:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002f0:	85 c0                	test   %eax,%eax
  8002f2:	7e 17                	jle    80030b <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002f4:	83 ec 0c             	sub    $0xc,%esp
  8002f7:	50                   	push   %eax
  8002f8:	6a 0a                	push   $0xa
  8002fa:	68 4f 0f 80 00       	push   $0x800f4f
  8002ff:	6a 23                	push   $0x23
  800301:	68 6c 0f 80 00       	push   $0x800f6c
  800306:	e8 6c 00 00 00       	call   800377 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  80030b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80030e:	5b                   	pop    %ebx
  80030f:	5e                   	pop    %esi
  800310:	5f                   	pop    %edi
  800311:	5d                   	pop    %ebp
  800312:	c3                   	ret    

00800313 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800313:	55                   	push   %ebp
  800314:	89 e5                	mov    %esp,%ebp
  800316:	57                   	push   %edi
  800317:	56                   	push   %esi
  800318:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800319:	be 00 00 00 00       	mov    $0x0,%esi
  80031e:	b8 0d 00 00 00       	mov    $0xd,%eax
  800323:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800326:	8b 55 08             	mov    0x8(%ebp),%edx
  800329:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80032c:	8b 7d 14             	mov    0x14(%ebp),%edi
  80032f:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800331:	5b                   	pop    %ebx
  800332:	5e                   	pop    %esi
  800333:	5f                   	pop    %edi
  800334:	5d                   	pop    %ebp
  800335:	c3                   	ret    

00800336 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800336:	55                   	push   %ebp
  800337:	89 e5                	mov    %esp,%ebp
  800339:	57                   	push   %edi
  80033a:	56                   	push   %esi
  80033b:	53                   	push   %ebx
  80033c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80033f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800344:	b8 0e 00 00 00       	mov    $0xe,%eax
  800349:	8b 55 08             	mov    0x8(%ebp),%edx
  80034c:	89 cb                	mov    %ecx,%ebx
  80034e:	89 cf                	mov    %ecx,%edi
  800350:	89 ce                	mov    %ecx,%esi
  800352:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800354:	85 c0                	test   %eax,%eax
  800356:	7e 17                	jle    80036f <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800358:	83 ec 0c             	sub    $0xc,%esp
  80035b:	50                   	push   %eax
  80035c:	6a 0e                	push   $0xe
  80035e:	68 4f 0f 80 00       	push   $0x800f4f
  800363:	6a 23                	push   $0x23
  800365:	68 6c 0f 80 00       	push   $0x800f6c
  80036a:	e8 08 00 00 00       	call   800377 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80036f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800372:	5b                   	pop    %ebx
  800373:	5e                   	pop    %esi
  800374:	5f                   	pop    %edi
  800375:	5d                   	pop    %ebp
  800376:	c3                   	ret    

00800377 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800377:	55                   	push   %ebp
  800378:	89 e5                	mov    %esp,%ebp
  80037a:	56                   	push   %esi
  80037b:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80037c:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80037f:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800385:	e8 9f fd ff ff       	call   800129 <sys_getenvid>
  80038a:	83 ec 0c             	sub    $0xc,%esp
  80038d:	ff 75 0c             	pushl  0xc(%ebp)
  800390:	ff 75 08             	pushl  0x8(%ebp)
  800393:	56                   	push   %esi
  800394:	50                   	push   %eax
  800395:	68 7c 0f 80 00       	push   $0x800f7c
  80039a:	e8 b0 00 00 00       	call   80044f <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80039f:	83 c4 18             	add    $0x18,%esp
  8003a2:	53                   	push   %ebx
  8003a3:	ff 75 10             	pushl  0x10(%ebp)
  8003a6:	e8 53 00 00 00       	call   8003fe <vcprintf>
	cprintf("\n");
  8003ab:	c7 04 24 9f 0f 80 00 	movl   $0x800f9f,(%esp)
  8003b2:	e8 98 00 00 00       	call   80044f <cprintf>
  8003b7:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003ba:	cc                   	int3   
  8003bb:	eb fd                	jmp    8003ba <_panic+0x43>

008003bd <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003bd:	55                   	push   %ebp
  8003be:	89 e5                	mov    %esp,%ebp
  8003c0:	53                   	push   %ebx
  8003c1:	83 ec 04             	sub    $0x4,%esp
  8003c4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003c7:	8b 13                	mov    (%ebx),%edx
  8003c9:	8d 42 01             	lea    0x1(%edx),%eax
  8003cc:	89 03                	mov    %eax,(%ebx)
  8003ce:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003d1:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003d5:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003da:	75 1a                	jne    8003f6 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003dc:	83 ec 08             	sub    $0x8,%esp
  8003df:	68 ff 00 00 00       	push   $0xff
  8003e4:	8d 43 08             	lea    0x8(%ebx),%eax
  8003e7:	50                   	push   %eax
  8003e8:	e8 be fc ff ff       	call   8000ab <sys_cputs>
		b->idx = 0;
  8003ed:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003f3:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003f6:	ff 43 04             	incl   0x4(%ebx)
}
  8003f9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003fc:	c9                   	leave  
  8003fd:	c3                   	ret    

008003fe <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003fe:	55                   	push   %ebp
  8003ff:	89 e5                	mov    %esp,%ebp
  800401:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800407:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80040e:	00 00 00 
	b.cnt = 0;
  800411:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800418:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80041b:	ff 75 0c             	pushl  0xc(%ebp)
  80041e:	ff 75 08             	pushl  0x8(%ebp)
  800421:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800427:	50                   	push   %eax
  800428:	68 bd 03 80 00       	push   $0x8003bd
  80042d:	e8 51 01 00 00       	call   800583 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800432:	83 c4 08             	add    $0x8,%esp
  800435:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80043b:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800441:	50                   	push   %eax
  800442:	e8 64 fc ff ff       	call   8000ab <sys_cputs>

	return b.cnt;
}
  800447:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80044d:	c9                   	leave  
  80044e:	c3                   	ret    

0080044f <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80044f:	55                   	push   %ebp
  800450:	89 e5                	mov    %esp,%ebp
  800452:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800455:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800458:	50                   	push   %eax
  800459:	ff 75 08             	pushl  0x8(%ebp)
  80045c:	e8 9d ff ff ff       	call   8003fe <vcprintf>
	va_end(ap);

	return cnt;
}
  800461:	c9                   	leave  
  800462:	c3                   	ret    

00800463 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800463:	55                   	push   %ebp
  800464:	89 e5                	mov    %esp,%ebp
  800466:	57                   	push   %edi
  800467:	56                   	push   %esi
  800468:	53                   	push   %ebx
  800469:	83 ec 1c             	sub    $0x1c,%esp
  80046c:	89 c7                	mov    %eax,%edi
  80046e:	89 d6                	mov    %edx,%esi
  800470:	8b 45 08             	mov    0x8(%ebp),%eax
  800473:	8b 55 0c             	mov    0xc(%ebp),%edx
  800476:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800479:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80047c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80047f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800484:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800487:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80048a:	39 d3                	cmp    %edx,%ebx
  80048c:	72 05                	jb     800493 <printnum+0x30>
  80048e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800491:	77 45                	ja     8004d8 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800493:	83 ec 0c             	sub    $0xc,%esp
  800496:	ff 75 18             	pushl  0x18(%ebp)
  800499:	8b 45 14             	mov    0x14(%ebp),%eax
  80049c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80049f:	53                   	push   %ebx
  8004a0:	ff 75 10             	pushl  0x10(%ebp)
  8004a3:	83 ec 08             	sub    $0x8,%esp
  8004a6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a9:	ff 75 e0             	pushl  -0x20(%ebp)
  8004ac:	ff 75 dc             	pushl  -0x24(%ebp)
  8004af:	ff 75 d8             	pushl  -0x28(%ebp)
  8004b2:	e8 25 08 00 00       	call   800cdc <__udivdi3>
  8004b7:	83 c4 18             	add    $0x18,%esp
  8004ba:	52                   	push   %edx
  8004bb:	50                   	push   %eax
  8004bc:	89 f2                	mov    %esi,%edx
  8004be:	89 f8                	mov    %edi,%eax
  8004c0:	e8 9e ff ff ff       	call   800463 <printnum>
  8004c5:	83 c4 20             	add    $0x20,%esp
  8004c8:	eb 16                	jmp    8004e0 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004ca:	83 ec 08             	sub    $0x8,%esp
  8004cd:	56                   	push   %esi
  8004ce:	ff 75 18             	pushl  0x18(%ebp)
  8004d1:	ff d7                	call   *%edi
  8004d3:	83 c4 10             	add    $0x10,%esp
  8004d6:	eb 03                	jmp    8004db <printnum+0x78>
  8004d8:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004db:	4b                   	dec    %ebx
  8004dc:	85 db                	test   %ebx,%ebx
  8004de:	7f ea                	jg     8004ca <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004e0:	83 ec 08             	sub    $0x8,%esp
  8004e3:	56                   	push   %esi
  8004e4:	83 ec 04             	sub    $0x4,%esp
  8004e7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004ea:	ff 75 e0             	pushl  -0x20(%ebp)
  8004ed:	ff 75 dc             	pushl  -0x24(%ebp)
  8004f0:	ff 75 d8             	pushl  -0x28(%ebp)
  8004f3:	e8 f4 08 00 00       	call   800dec <__umoddi3>
  8004f8:	83 c4 14             	add    $0x14,%esp
  8004fb:	0f be 80 a1 0f 80 00 	movsbl 0x800fa1(%eax),%eax
  800502:	50                   	push   %eax
  800503:	ff d7                	call   *%edi
}
  800505:	83 c4 10             	add    $0x10,%esp
  800508:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80050b:	5b                   	pop    %ebx
  80050c:	5e                   	pop    %esi
  80050d:	5f                   	pop    %edi
  80050e:	5d                   	pop    %ebp
  80050f:	c3                   	ret    

00800510 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800510:	55                   	push   %ebp
  800511:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800513:	83 fa 01             	cmp    $0x1,%edx
  800516:	7e 0e                	jle    800526 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800518:	8b 10                	mov    (%eax),%edx
  80051a:	8d 4a 08             	lea    0x8(%edx),%ecx
  80051d:	89 08                	mov    %ecx,(%eax)
  80051f:	8b 02                	mov    (%edx),%eax
  800521:	8b 52 04             	mov    0x4(%edx),%edx
  800524:	eb 22                	jmp    800548 <getuint+0x38>
	else if (lflag)
  800526:	85 d2                	test   %edx,%edx
  800528:	74 10                	je     80053a <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80052a:	8b 10                	mov    (%eax),%edx
  80052c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80052f:	89 08                	mov    %ecx,(%eax)
  800531:	8b 02                	mov    (%edx),%eax
  800533:	ba 00 00 00 00       	mov    $0x0,%edx
  800538:	eb 0e                	jmp    800548 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80053a:	8b 10                	mov    (%eax),%edx
  80053c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80053f:	89 08                	mov    %ecx,(%eax)
  800541:	8b 02                	mov    (%edx),%eax
  800543:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800548:	5d                   	pop    %ebp
  800549:	c3                   	ret    

0080054a <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80054a:	55                   	push   %ebp
  80054b:	89 e5                	mov    %esp,%ebp
  80054d:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800550:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800553:	8b 10                	mov    (%eax),%edx
  800555:	3b 50 04             	cmp    0x4(%eax),%edx
  800558:	73 0a                	jae    800564 <sprintputch+0x1a>
		*b->buf++ = ch;
  80055a:	8d 4a 01             	lea    0x1(%edx),%ecx
  80055d:	89 08                	mov    %ecx,(%eax)
  80055f:	8b 45 08             	mov    0x8(%ebp),%eax
  800562:	88 02                	mov    %al,(%edx)
}
  800564:	5d                   	pop    %ebp
  800565:	c3                   	ret    

00800566 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800566:	55                   	push   %ebp
  800567:	89 e5                	mov    %esp,%ebp
  800569:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80056c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80056f:	50                   	push   %eax
  800570:	ff 75 10             	pushl  0x10(%ebp)
  800573:	ff 75 0c             	pushl  0xc(%ebp)
  800576:	ff 75 08             	pushl  0x8(%ebp)
  800579:	e8 05 00 00 00       	call   800583 <vprintfmt>
	va_end(ap);
}
  80057e:	83 c4 10             	add    $0x10,%esp
  800581:	c9                   	leave  
  800582:	c3                   	ret    

00800583 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800583:	55                   	push   %ebp
  800584:	89 e5                	mov    %esp,%ebp
  800586:	57                   	push   %edi
  800587:	56                   	push   %esi
  800588:	53                   	push   %ebx
  800589:	83 ec 2c             	sub    $0x2c,%esp
  80058c:	8b 75 08             	mov    0x8(%ebp),%esi
  80058f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800592:	8b 7d 10             	mov    0x10(%ebp),%edi
  800595:	eb 12                	jmp    8005a9 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800597:	85 c0                	test   %eax,%eax
  800599:	0f 84 68 03 00 00    	je     800907 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80059f:	83 ec 08             	sub    $0x8,%esp
  8005a2:	53                   	push   %ebx
  8005a3:	50                   	push   %eax
  8005a4:	ff d6                	call   *%esi
  8005a6:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8005a9:	47                   	inc    %edi
  8005aa:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005ae:	83 f8 25             	cmp    $0x25,%eax
  8005b1:	75 e4                	jne    800597 <vprintfmt+0x14>
  8005b3:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005b7:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005be:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005c5:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005cc:	ba 00 00 00 00       	mov    $0x0,%edx
  8005d1:	eb 07                	jmp    8005da <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d3:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005d6:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005da:	8d 47 01             	lea    0x1(%edi),%eax
  8005dd:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005e0:	0f b6 0f             	movzbl (%edi),%ecx
  8005e3:	8a 07                	mov    (%edi),%al
  8005e5:	83 e8 23             	sub    $0x23,%eax
  8005e8:	3c 55                	cmp    $0x55,%al
  8005ea:	0f 87 fe 02 00 00    	ja     8008ee <vprintfmt+0x36b>
  8005f0:	0f b6 c0             	movzbl %al,%eax
  8005f3:	ff 24 85 e0 10 80 00 	jmp    *0x8010e0(,%eax,4)
  8005fa:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005fd:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800601:	eb d7                	jmp    8005da <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800603:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800606:	b8 00 00 00 00       	mov    $0x0,%eax
  80060b:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80060e:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800611:	01 c0                	add    %eax,%eax
  800613:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800617:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80061a:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80061d:	83 fa 09             	cmp    $0x9,%edx
  800620:	77 34                	ja     800656 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800622:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800623:	eb e9                	jmp    80060e <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800625:	8b 45 14             	mov    0x14(%ebp),%eax
  800628:	8d 48 04             	lea    0x4(%eax),%ecx
  80062b:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80062e:	8b 00                	mov    (%eax),%eax
  800630:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800633:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800636:	eb 24                	jmp    80065c <vprintfmt+0xd9>
  800638:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80063c:	79 07                	jns    800645 <vprintfmt+0xc2>
  80063e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800645:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800648:	eb 90                	jmp    8005da <vprintfmt+0x57>
  80064a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80064d:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800654:	eb 84                	jmp    8005da <vprintfmt+0x57>
  800656:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800659:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80065c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800660:	0f 89 74 ff ff ff    	jns    8005da <vprintfmt+0x57>
				width = precision, precision = -1;
  800666:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800669:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80066c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800673:	e9 62 ff ff ff       	jmp    8005da <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800678:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800679:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80067c:	e9 59 ff ff ff       	jmp    8005da <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800681:	8b 45 14             	mov    0x14(%ebp),%eax
  800684:	8d 50 04             	lea    0x4(%eax),%edx
  800687:	89 55 14             	mov    %edx,0x14(%ebp)
  80068a:	83 ec 08             	sub    $0x8,%esp
  80068d:	53                   	push   %ebx
  80068e:	ff 30                	pushl  (%eax)
  800690:	ff d6                	call   *%esi
			break;
  800692:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800695:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800698:	e9 0c ff ff ff       	jmp    8005a9 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80069d:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a0:	8d 50 04             	lea    0x4(%eax),%edx
  8006a3:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a6:	8b 00                	mov    (%eax),%eax
  8006a8:	85 c0                	test   %eax,%eax
  8006aa:	79 02                	jns    8006ae <vprintfmt+0x12b>
  8006ac:	f7 d8                	neg    %eax
  8006ae:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006b0:	83 f8 0f             	cmp    $0xf,%eax
  8006b3:	7f 0b                	jg     8006c0 <vprintfmt+0x13d>
  8006b5:	8b 04 85 40 12 80 00 	mov    0x801240(,%eax,4),%eax
  8006bc:	85 c0                	test   %eax,%eax
  8006be:	75 18                	jne    8006d8 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006c0:	52                   	push   %edx
  8006c1:	68 b9 0f 80 00       	push   $0x800fb9
  8006c6:	53                   	push   %ebx
  8006c7:	56                   	push   %esi
  8006c8:	e8 99 fe ff ff       	call   800566 <printfmt>
  8006cd:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006d0:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006d3:	e9 d1 fe ff ff       	jmp    8005a9 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006d8:	50                   	push   %eax
  8006d9:	68 c2 0f 80 00       	push   $0x800fc2
  8006de:	53                   	push   %ebx
  8006df:	56                   	push   %esi
  8006e0:	e8 81 fe ff ff       	call   800566 <printfmt>
  8006e5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006e8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006eb:	e9 b9 fe ff ff       	jmp    8005a9 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006f0:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f3:	8d 50 04             	lea    0x4(%eax),%edx
  8006f6:	89 55 14             	mov    %edx,0x14(%ebp)
  8006f9:	8b 38                	mov    (%eax),%edi
  8006fb:	85 ff                	test   %edi,%edi
  8006fd:	75 05                	jne    800704 <vprintfmt+0x181>
				p = "(null)";
  8006ff:	bf b2 0f 80 00       	mov    $0x800fb2,%edi
			if (width > 0 && padc != '-')
  800704:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800708:	0f 8e 90 00 00 00    	jle    80079e <vprintfmt+0x21b>
  80070e:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800712:	0f 84 8e 00 00 00    	je     8007a6 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800718:	83 ec 08             	sub    $0x8,%esp
  80071b:	ff 75 d0             	pushl  -0x30(%ebp)
  80071e:	57                   	push   %edi
  80071f:	e8 70 02 00 00       	call   800994 <strnlen>
  800724:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800727:	29 c1                	sub    %eax,%ecx
  800729:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80072c:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80072f:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800733:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800736:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800739:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80073b:	eb 0d                	jmp    80074a <vprintfmt+0x1c7>
					putch(padc, putdat);
  80073d:	83 ec 08             	sub    $0x8,%esp
  800740:	53                   	push   %ebx
  800741:	ff 75 e4             	pushl  -0x1c(%ebp)
  800744:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800746:	4f                   	dec    %edi
  800747:	83 c4 10             	add    $0x10,%esp
  80074a:	85 ff                	test   %edi,%edi
  80074c:	7f ef                	jg     80073d <vprintfmt+0x1ba>
  80074e:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800751:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800754:	89 c8                	mov    %ecx,%eax
  800756:	85 c9                	test   %ecx,%ecx
  800758:	79 05                	jns    80075f <vprintfmt+0x1dc>
  80075a:	b8 00 00 00 00       	mov    $0x0,%eax
  80075f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800762:	29 c1                	sub    %eax,%ecx
  800764:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800767:	89 75 08             	mov    %esi,0x8(%ebp)
  80076a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80076d:	eb 3d                	jmp    8007ac <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80076f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800773:	74 19                	je     80078e <vprintfmt+0x20b>
  800775:	0f be c0             	movsbl %al,%eax
  800778:	83 e8 20             	sub    $0x20,%eax
  80077b:	83 f8 5e             	cmp    $0x5e,%eax
  80077e:	76 0e                	jbe    80078e <vprintfmt+0x20b>
					putch('?', putdat);
  800780:	83 ec 08             	sub    $0x8,%esp
  800783:	53                   	push   %ebx
  800784:	6a 3f                	push   $0x3f
  800786:	ff 55 08             	call   *0x8(%ebp)
  800789:	83 c4 10             	add    $0x10,%esp
  80078c:	eb 0b                	jmp    800799 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80078e:	83 ec 08             	sub    $0x8,%esp
  800791:	53                   	push   %ebx
  800792:	52                   	push   %edx
  800793:	ff 55 08             	call   *0x8(%ebp)
  800796:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800799:	ff 4d e4             	decl   -0x1c(%ebp)
  80079c:	eb 0e                	jmp    8007ac <vprintfmt+0x229>
  80079e:	89 75 08             	mov    %esi,0x8(%ebp)
  8007a1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007a4:	eb 06                	jmp    8007ac <vprintfmt+0x229>
  8007a6:	89 75 08             	mov    %esi,0x8(%ebp)
  8007a9:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007ac:	47                   	inc    %edi
  8007ad:	8a 47 ff             	mov    -0x1(%edi),%al
  8007b0:	0f be d0             	movsbl %al,%edx
  8007b3:	85 d2                	test   %edx,%edx
  8007b5:	74 1d                	je     8007d4 <vprintfmt+0x251>
  8007b7:	85 f6                	test   %esi,%esi
  8007b9:	78 b4                	js     80076f <vprintfmt+0x1ec>
  8007bb:	4e                   	dec    %esi
  8007bc:	79 b1                	jns    80076f <vprintfmt+0x1ec>
  8007be:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007c4:	eb 14                	jmp    8007da <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007c6:	83 ec 08             	sub    $0x8,%esp
  8007c9:	53                   	push   %ebx
  8007ca:	6a 20                	push   $0x20
  8007cc:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007ce:	4f                   	dec    %edi
  8007cf:	83 c4 10             	add    $0x10,%esp
  8007d2:	eb 06                	jmp    8007da <vprintfmt+0x257>
  8007d4:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007d7:	8b 75 08             	mov    0x8(%ebp),%esi
  8007da:	85 ff                	test   %edi,%edi
  8007dc:	7f e8                	jg     8007c6 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007de:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007e1:	e9 c3 fd ff ff       	jmp    8005a9 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007e6:	83 fa 01             	cmp    $0x1,%edx
  8007e9:	7e 16                	jle    800801 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007eb:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ee:	8d 50 08             	lea    0x8(%eax),%edx
  8007f1:	89 55 14             	mov    %edx,0x14(%ebp)
  8007f4:	8b 50 04             	mov    0x4(%eax),%edx
  8007f7:	8b 00                	mov    (%eax),%eax
  8007f9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007fc:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ff:	eb 32                	jmp    800833 <vprintfmt+0x2b0>
	else if (lflag)
  800801:	85 d2                	test   %edx,%edx
  800803:	74 18                	je     80081d <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800805:	8b 45 14             	mov    0x14(%ebp),%eax
  800808:	8d 50 04             	lea    0x4(%eax),%edx
  80080b:	89 55 14             	mov    %edx,0x14(%ebp)
  80080e:	8b 00                	mov    (%eax),%eax
  800810:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800813:	89 c1                	mov    %eax,%ecx
  800815:	c1 f9 1f             	sar    $0x1f,%ecx
  800818:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80081b:	eb 16                	jmp    800833 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80081d:	8b 45 14             	mov    0x14(%ebp),%eax
  800820:	8d 50 04             	lea    0x4(%eax),%edx
  800823:	89 55 14             	mov    %edx,0x14(%ebp)
  800826:	8b 00                	mov    (%eax),%eax
  800828:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80082b:	89 c1                	mov    %eax,%ecx
  80082d:	c1 f9 1f             	sar    $0x1f,%ecx
  800830:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800833:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800836:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800839:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80083d:	79 76                	jns    8008b5 <vprintfmt+0x332>
				putch('-', putdat);
  80083f:	83 ec 08             	sub    $0x8,%esp
  800842:	53                   	push   %ebx
  800843:	6a 2d                	push   $0x2d
  800845:	ff d6                	call   *%esi
				num = -(long long) num;
  800847:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80084a:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80084d:	f7 d8                	neg    %eax
  80084f:	83 d2 00             	adc    $0x0,%edx
  800852:	f7 da                	neg    %edx
  800854:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800857:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80085c:	eb 5c                	jmp    8008ba <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80085e:	8d 45 14             	lea    0x14(%ebp),%eax
  800861:	e8 aa fc ff ff       	call   800510 <getuint>
			base = 10;
  800866:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80086b:	eb 4d                	jmp    8008ba <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80086d:	8d 45 14             	lea    0x14(%ebp),%eax
  800870:	e8 9b fc ff ff       	call   800510 <getuint>
			base = 8;
  800875:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80087a:	eb 3e                	jmp    8008ba <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80087c:	83 ec 08             	sub    $0x8,%esp
  80087f:	53                   	push   %ebx
  800880:	6a 30                	push   $0x30
  800882:	ff d6                	call   *%esi
			putch('x', putdat);
  800884:	83 c4 08             	add    $0x8,%esp
  800887:	53                   	push   %ebx
  800888:	6a 78                	push   $0x78
  80088a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80088c:	8b 45 14             	mov    0x14(%ebp),%eax
  80088f:	8d 50 04             	lea    0x4(%eax),%edx
  800892:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800895:	8b 00                	mov    (%eax),%eax
  800897:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80089c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80089f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8008a4:	eb 14                	jmp    8008ba <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8008a6:	8d 45 14             	lea    0x14(%ebp),%eax
  8008a9:	e8 62 fc ff ff       	call   800510 <getuint>
			base = 16;
  8008ae:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008b3:	eb 05                	jmp    8008ba <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008b5:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008ba:	83 ec 0c             	sub    $0xc,%esp
  8008bd:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008c1:	57                   	push   %edi
  8008c2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008c5:	51                   	push   %ecx
  8008c6:	52                   	push   %edx
  8008c7:	50                   	push   %eax
  8008c8:	89 da                	mov    %ebx,%edx
  8008ca:	89 f0                	mov    %esi,%eax
  8008cc:	e8 92 fb ff ff       	call   800463 <printnum>
			break;
  8008d1:	83 c4 20             	add    $0x20,%esp
  8008d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008d7:	e9 cd fc ff ff       	jmp    8005a9 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008dc:	83 ec 08             	sub    $0x8,%esp
  8008df:	53                   	push   %ebx
  8008e0:	51                   	push   %ecx
  8008e1:	ff d6                	call   *%esi
			break;
  8008e3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008e6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008e9:	e9 bb fc ff ff       	jmp    8005a9 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008ee:	83 ec 08             	sub    $0x8,%esp
  8008f1:	53                   	push   %ebx
  8008f2:	6a 25                	push   $0x25
  8008f4:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008f6:	83 c4 10             	add    $0x10,%esp
  8008f9:	eb 01                	jmp    8008fc <vprintfmt+0x379>
  8008fb:	4f                   	dec    %edi
  8008fc:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800900:	75 f9                	jne    8008fb <vprintfmt+0x378>
  800902:	e9 a2 fc ff ff       	jmp    8005a9 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800907:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80090a:	5b                   	pop    %ebx
  80090b:	5e                   	pop    %esi
  80090c:	5f                   	pop    %edi
  80090d:	5d                   	pop    %ebp
  80090e:	c3                   	ret    

0080090f <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80090f:	55                   	push   %ebp
  800910:	89 e5                	mov    %esp,%ebp
  800912:	83 ec 18             	sub    $0x18,%esp
  800915:	8b 45 08             	mov    0x8(%ebp),%eax
  800918:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80091b:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80091e:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800922:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800925:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80092c:	85 c0                	test   %eax,%eax
  80092e:	74 26                	je     800956 <vsnprintf+0x47>
  800930:	85 d2                	test   %edx,%edx
  800932:	7e 29                	jle    80095d <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800934:	ff 75 14             	pushl  0x14(%ebp)
  800937:	ff 75 10             	pushl  0x10(%ebp)
  80093a:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80093d:	50                   	push   %eax
  80093e:	68 4a 05 80 00       	push   $0x80054a
  800943:	e8 3b fc ff ff       	call   800583 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800948:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80094b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80094e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800951:	83 c4 10             	add    $0x10,%esp
  800954:	eb 0c                	jmp    800962 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800956:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80095b:	eb 05                	jmp    800962 <vsnprintf+0x53>
  80095d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800962:	c9                   	leave  
  800963:	c3                   	ret    

00800964 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800964:	55                   	push   %ebp
  800965:	89 e5                	mov    %esp,%ebp
  800967:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80096a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80096d:	50                   	push   %eax
  80096e:	ff 75 10             	pushl  0x10(%ebp)
  800971:	ff 75 0c             	pushl  0xc(%ebp)
  800974:	ff 75 08             	pushl  0x8(%ebp)
  800977:	e8 93 ff ff ff       	call   80090f <vsnprintf>
	va_end(ap);

	return rc;
}
  80097c:	c9                   	leave  
  80097d:	c3                   	ret    

0080097e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80097e:	55                   	push   %ebp
  80097f:	89 e5                	mov    %esp,%ebp
  800981:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800984:	b8 00 00 00 00       	mov    $0x0,%eax
  800989:	eb 01                	jmp    80098c <strlen+0xe>
		n++;
  80098b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80098c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800990:	75 f9                	jne    80098b <strlen+0xd>
		n++;
	return n;
}
  800992:	5d                   	pop    %ebp
  800993:	c3                   	ret    

00800994 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800994:	55                   	push   %ebp
  800995:	89 e5                	mov    %esp,%ebp
  800997:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80099a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80099d:	ba 00 00 00 00       	mov    $0x0,%edx
  8009a2:	eb 01                	jmp    8009a5 <strnlen+0x11>
		n++;
  8009a4:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009a5:	39 c2                	cmp    %eax,%edx
  8009a7:	74 08                	je     8009b1 <strnlen+0x1d>
  8009a9:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009ad:	75 f5                	jne    8009a4 <strnlen+0x10>
  8009af:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009b1:	5d                   	pop    %ebp
  8009b2:	c3                   	ret    

008009b3 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009b3:	55                   	push   %ebp
  8009b4:	89 e5                	mov    %esp,%ebp
  8009b6:	53                   	push   %ebx
  8009b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009bd:	89 c2                	mov    %eax,%edx
  8009bf:	42                   	inc    %edx
  8009c0:	41                   	inc    %ecx
  8009c1:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009c4:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009c7:	84 db                	test   %bl,%bl
  8009c9:	75 f4                	jne    8009bf <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009cb:	5b                   	pop    %ebx
  8009cc:	5d                   	pop    %ebp
  8009cd:	c3                   	ret    

008009ce <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009ce:	55                   	push   %ebp
  8009cf:	89 e5                	mov    %esp,%ebp
  8009d1:	53                   	push   %ebx
  8009d2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009d5:	53                   	push   %ebx
  8009d6:	e8 a3 ff ff ff       	call   80097e <strlen>
  8009db:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009de:	ff 75 0c             	pushl  0xc(%ebp)
  8009e1:	01 d8                	add    %ebx,%eax
  8009e3:	50                   	push   %eax
  8009e4:	e8 ca ff ff ff       	call   8009b3 <strcpy>
	return dst;
}
  8009e9:	89 d8                	mov    %ebx,%eax
  8009eb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009ee:	c9                   	leave  
  8009ef:	c3                   	ret    

008009f0 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009f0:	55                   	push   %ebp
  8009f1:	89 e5                	mov    %esp,%ebp
  8009f3:	56                   	push   %esi
  8009f4:	53                   	push   %ebx
  8009f5:	8b 75 08             	mov    0x8(%ebp),%esi
  8009f8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009fb:	89 f3                	mov    %esi,%ebx
  8009fd:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a00:	89 f2                	mov    %esi,%edx
  800a02:	eb 0c                	jmp    800a10 <strncpy+0x20>
		*dst++ = *src;
  800a04:	42                   	inc    %edx
  800a05:	8a 01                	mov    (%ecx),%al
  800a07:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a0a:	80 39 01             	cmpb   $0x1,(%ecx)
  800a0d:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a10:	39 da                	cmp    %ebx,%edx
  800a12:	75 f0                	jne    800a04 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a14:	89 f0                	mov    %esi,%eax
  800a16:	5b                   	pop    %ebx
  800a17:	5e                   	pop    %esi
  800a18:	5d                   	pop    %ebp
  800a19:	c3                   	ret    

00800a1a <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a1a:	55                   	push   %ebp
  800a1b:	89 e5                	mov    %esp,%ebp
  800a1d:	56                   	push   %esi
  800a1e:	53                   	push   %ebx
  800a1f:	8b 75 08             	mov    0x8(%ebp),%esi
  800a22:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a25:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a28:	85 c0                	test   %eax,%eax
  800a2a:	74 1e                	je     800a4a <strlcpy+0x30>
  800a2c:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a30:	89 f2                	mov    %esi,%edx
  800a32:	eb 05                	jmp    800a39 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a34:	42                   	inc    %edx
  800a35:	41                   	inc    %ecx
  800a36:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a39:	39 c2                	cmp    %eax,%edx
  800a3b:	74 08                	je     800a45 <strlcpy+0x2b>
  800a3d:	8a 19                	mov    (%ecx),%bl
  800a3f:	84 db                	test   %bl,%bl
  800a41:	75 f1                	jne    800a34 <strlcpy+0x1a>
  800a43:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a45:	c6 00 00             	movb   $0x0,(%eax)
  800a48:	eb 02                	jmp    800a4c <strlcpy+0x32>
  800a4a:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a4c:	29 f0                	sub    %esi,%eax
}
  800a4e:	5b                   	pop    %ebx
  800a4f:	5e                   	pop    %esi
  800a50:	5d                   	pop    %ebp
  800a51:	c3                   	ret    

00800a52 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a52:	55                   	push   %ebp
  800a53:	89 e5                	mov    %esp,%ebp
  800a55:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a58:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a5b:	eb 02                	jmp    800a5f <strcmp+0xd>
		p++, q++;
  800a5d:	41                   	inc    %ecx
  800a5e:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a5f:	8a 01                	mov    (%ecx),%al
  800a61:	84 c0                	test   %al,%al
  800a63:	74 04                	je     800a69 <strcmp+0x17>
  800a65:	3a 02                	cmp    (%edx),%al
  800a67:	74 f4                	je     800a5d <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a69:	0f b6 c0             	movzbl %al,%eax
  800a6c:	0f b6 12             	movzbl (%edx),%edx
  800a6f:	29 d0                	sub    %edx,%eax
}
  800a71:	5d                   	pop    %ebp
  800a72:	c3                   	ret    

00800a73 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a73:	55                   	push   %ebp
  800a74:	89 e5                	mov    %esp,%ebp
  800a76:	53                   	push   %ebx
  800a77:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7a:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a7d:	89 c3                	mov    %eax,%ebx
  800a7f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a82:	eb 02                	jmp    800a86 <strncmp+0x13>
		n--, p++, q++;
  800a84:	40                   	inc    %eax
  800a85:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a86:	39 d8                	cmp    %ebx,%eax
  800a88:	74 14                	je     800a9e <strncmp+0x2b>
  800a8a:	8a 08                	mov    (%eax),%cl
  800a8c:	84 c9                	test   %cl,%cl
  800a8e:	74 04                	je     800a94 <strncmp+0x21>
  800a90:	3a 0a                	cmp    (%edx),%cl
  800a92:	74 f0                	je     800a84 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a94:	0f b6 00             	movzbl (%eax),%eax
  800a97:	0f b6 12             	movzbl (%edx),%edx
  800a9a:	29 d0                	sub    %edx,%eax
  800a9c:	eb 05                	jmp    800aa3 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a9e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800aa3:	5b                   	pop    %ebx
  800aa4:	5d                   	pop    %ebp
  800aa5:	c3                   	ret    

00800aa6 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800aa6:	55                   	push   %ebp
  800aa7:	89 e5                	mov    %esp,%ebp
  800aa9:	8b 45 08             	mov    0x8(%ebp),%eax
  800aac:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800aaf:	eb 05                	jmp    800ab6 <strchr+0x10>
		if (*s == c)
  800ab1:	38 ca                	cmp    %cl,%dl
  800ab3:	74 0c                	je     800ac1 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800ab5:	40                   	inc    %eax
  800ab6:	8a 10                	mov    (%eax),%dl
  800ab8:	84 d2                	test   %dl,%dl
  800aba:	75 f5                	jne    800ab1 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800abc:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ac1:	5d                   	pop    %ebp
  800ac2:	c3                   	ret    

00800ac3 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ac3:	55                   	push   %ebp
  800ac4:	89 e5                	mov    %esp,%ebp
  800ac6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ac9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800acc:	eb 05                	jmp    800ad3 <strfind+0x10>
		if (*s == c)
  800ace:	38 ca                	cmp    %cl,%dl
  800ad0:	74 07                	je     800ad9 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800ad2:	40                   	inc    %eax
  800ad3:	8a 10                	mov    (%eax),%dl
  800ad5:	84 d2                	test   %dl,%dl
  800ad7:	75 f5                	jne    800ace <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800ad9:	5d                   	pop    %ebp
  800ada:	c3                   	ret    

00800adb <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800adb:	55                   	push   %ebp
  800adc:	89 e5                	mov    %esp,%ebp
  800ade:	57                   	push   %edi
  800adf:	56                   	push   %esi
  800ae0:	53                   	push   %ebx
  800ae1:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ae4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800ae7:	85 c9                	test   %ecx,%ecx
  800ae9:	74 36                	je     800b21 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800aeb:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800af1:	75 28                	jne    800b1b <memset+0x40>
  800af3:	f6 c1 03             	test   $0x3,%cl
  800af6:	75 23                	jne    800b1b <memset+0x40>
		c &= 0xFF;
  800af8:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800afc:	89 d3                	mov    %edx,%ebx
  800afe:	c1 e3 08             	shl    $0x8,%ebx
  800b01:	89 d6                	mov    %edx,%esi
  800b03:	c1 e6 18             	shl    $0x18,%esi
  800b06:	89 d0                	mov    %edx,%eax
  800b08:	c1 e0 10             	shl    $0x10,%eax
  800b0b:	09 f0                	or     %esi,%eax
  800b0d:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b0f:	89 d8                	mov    %ebx,%eax
  800b11:	09 d0                	or     %edx,%eax
  800b13:	c1 e9 02             	shr    $0x2,%ecx
  800b16:	fc                   	cld    
  800b17:	f3 ab                	rep stos %eax,%es:(%edi)
  800b19:	eb 06                	jmp    800b21 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b1b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b1e:	fc                   	cld    
  800b1f:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b21:	89 f8                	mov    %edi,%eax
  800b23:	5b                   	pop    %ebx
  800b24:	5e                   	pop    %esi
  800b25:	5f                   	pop    %edi
  800b26:	5d                   	pop    %ebp
  800b27:	c3                   	ret    

00800b28 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b28:	55                   	push   %ebp
  800b29:	89 e5                	mov    %esp,%ebp
  800b2b:	57                   	push   %edi
  800b2c:	56                   	push   %esi
  800b2d:	8b 45 08             	mov    0x8(%ebp),%eax
  800b30:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b33:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b36:	39 c6                	cmp    %eax,%esi
  800b38:	73 33                	jae    800b6d <memmove+0x45>
  800b3a:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b3d:	39 d0                	cmp    %edx,%eax
  800b3f:	73 2c                	jae    800b6d <memmove+0x45>
		s += n;
		d += n;
  800b41:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b44:	89 d6                	mov    %edx,%esi
  800b46:	09 fe                	or     %edi,%esi
  800b48:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b4e:	75 13                	jne    800b63 <memmove+0x3b>
  800b50:	f6 c1 03             	test   $0x3,%cl
  800b53:	75 0e                	jne    800b63 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b55:	83 ef 04             	sub    $0x4,%edi
  800b58:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b5b:	c1 e9 02             	shr    $0x2,%ecx
  800b5e:	fd                   	std    
  800b5f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b61:	eb 07                	jmp    800b6a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b63:	4f                   	dec    %edi
  800b64:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b67:	fd                   	std    
  800b68:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b6a:	fc                   	cld    
  800b6b:	eb 1d                	jmp    800b8a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b6d:	89 f2                	mov    %esi,%edx
  800b6f:	09 c2                	or     %eax,%edx
  800b71:	f6 c2 03             	test   $0x3,%dl
  800b74:	75 0f                	jne    800b85 <memmove+0x5d>
  800b76:	f6 c1 03             	test   $0x3,%cl
  800b79:	75 0a                	jne    800b85 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b7b:	c1 e9 02             	shr    $0x2,%ecx
  800b7e:	89 c7                	mov    %eax,%edi
  800b80:	fc                   	cld    
  800b81:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b83:	eb 05                	jmp    800b8a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b85:	89 c7                	mov    %eax,%edi
  800b87:	fc                   	cld    
  800b88:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b8a:	5e                   	pop    %esi
  800b8b:	5f                   	pop    %edi
  800b8c:	5d                   	pop    %ebp
  800b8d:	c3                   	ret    

00800b8e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b8e:	55                   	push   %ebp
  800b8f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b91:	ff 75 10             	pushl  0x10(%ebp)
  800b94:	ff 75 0c             	pushl  0xc(%ebp)
  800b97:	ff 75 08             	pushl  0x8(%ebp)
  800b9a:	e8 89 ff ff ff       	call   800b28 <memmove>
}
  800b9f:	c9                   	leave  
  800ba0:	c3                   	ret    

00800ba1 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800ba1:	55                   	push   %ebp
  800ba2:	89 e5                	mov    %esp,%ebp
  800ba4:	56                   	push   %esi
  800ba5:	53                   	push   %ebx
  800ba6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ba9:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bac:	89 c6                	mov    %eax,%esi
  800bae:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bb1:	eb 14                	jmp    800bc7 <memcmp+0x26>
		if (*s1 != *s2)
  800bb3:	8a 08                	mov    (%eax),%cl
  800bb5:	8a 1a                	mov    (%edx),%bl
  800bb7:	38 d9                	cmp    %bl,%cl
  800bb9:	74 0a                	je     800bc5 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bbb:	0f b6 c1             	movzbl %cl,%eax
  800bbe:	0f b6 db             	movzbl %bl,%ebx
  800bc1:	29 d8                	sub    %ebx,%eax
  800bc3:	eb 0b                	jmp    800bd0 <memcmp+0x2f>
		s1++, s2++;
  800bc5:	40                   	inc    %eax
  800bc6:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bc7:	39 f0                	cmp    %esi,%eax
  800bc9:	75 e8                	jne    800bb3 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bcb:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bd0:	5b                   	pop    %ebx
  800bd1:	5e                   	pop    %esi
  800bd2:	5d                   	pop    %ebp
  800bd3:	c3                   	ret    

00800bd4 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bd4:	55                   	push   %ebp
  800bd5:	89 e5                	mov    %esp,%ebp
  800bd7:	53                   	push   %ebx
  800bd8:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bdb:	89 c1                	mov    %eax,%ecx
  800bdd:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800be0:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800be4:	eb 08                	jmp    800bee <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800be6:	0f b6 10             	movzbl (%eax),%edx
  800be9:	39 da                	cmp    %ebx,%edx
  800beb:	74 05                	je     800bf2 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bed:	40                   	inc    %eax
  800bee:	39 c8                	cmp    %ecx,%eax
  800bf0:	72 f4                	jb     800be6 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bf2:	5b                   	pop    %ebx
  800bf3:	5d                   	pop    %ebp
  800bf4:	c3                   	ret    

00800bf5 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bf5:	55                   	push   %ebp
  800bf6:	89 e5                	mov    %esp,%ebp
  800bf8:	57                   	push   %edi
  800bf9:	56                   	push   %esi
  800bfa:	53                   	push   %ebx
  800bfb:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bfe:	eb 01                	jmp    800c01 <strtol+0xc>
		s++;
  800c00:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c01:	8a 01                	mov    (%ecx),%al
  800c03:	3c 20                	cmp    $0x20,%al
  800c05:	74 f9                	je     800c00 <strtol+0xb>
  800c07:	3c 09                	cmp    $0x9,%al
  800c09:	74 f5                	je     800c00 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c0b:	3c 2b                	cmp    $0x2b,%al
  800c0d:	75 08                	jne    800c17 <strtol+0x22>
		s++;
  800c0f:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c10:	bf 00 00 00 00       	mov    $0x0,%edi
  800c15:	eb 11                	jmp    800c28 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c17:	3c 2d                	cmp    $0x2d,%al
  800c19:	75 08                	jne    800c23 <strtol+0x2e>
		s++, neg = 1;
  800c1b:	41                   	inc    %ecx
  800c1c:	bf 01 00 00 00       	mov    $0x1,%edi
  800c21:	eb 05                	jmp    800c28 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c23:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c28:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c2c:	0f 84 87 00 00 00    	je     800cb9 <strtol+0xc4>
  800c32:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c36:	75 27                	jne    800c5f <strtol+0x6a>
  800c38:	80 39 30             	cmpb   $0x30,(%ecx)
  800c3b:	75 22                	jne    800c5f <strtol+0x6a>
  800c3d:	e9 88 00 00 00       	jmp    800cca <strtol+0xd5>
		s += 2, base = 16;
  800c42:	83 c1 02             	add    $0x2,%ecx
  800c45:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c4c:	eb 11                	jmp    800c5f <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c4e:	41                   	inc    %ecx
  800c4f:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c56:	eb 07                	jmp    800c5f <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c58:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c5f:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c64:	8a 11                	mov    (%ecx),%dl
  800c66:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c69:	80 fb 09             	cmp    $0x9,%bl
  800c6c:	77 08                	ja     800c76 <strtol+0x81>
			dig = *s - '0';
  800c6e:	0f be d2             	movsbl %dl,%edx
  800c71:	83 ea 30             	sub    $0x30,%edx
  800c74:	eb 22                	jmp    800c98 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c76:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c79:	89 f3                	mov    %esi,%ebx
  800c7b:	80 fb 19             	cmp    $0x19,%bl
  800c7e:	77 08                	ja     800c88 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c80:	0f be d2             	movsbl %dl,%edx
  800c83:	83 ea 57             	sub    $0x57,%edx
  800c86:	eb 10                	jmp    800c98 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c88:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c8b:	89 f3                	mov    %esi,%ebx
  800c8d:	80 fb 19             	cmp    $0x19,%bl
  800c90:	77 14                	ja     800ca6 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c92:	0f be d2             	movsbl %dl,%edx
  800c95:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c98:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c9b:	7d 09                	jge    800ca6 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c9d:	41                   	inc    %ecx
  800c9e:	0f af 45 10          	imul   0x10(%ebp),%eax
  800ca2:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800ca4:	eb be                	jmp    800c64 <strtol+0x6f>

	if (endptr)
  800ca6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800caa:	74 05                	je     800cb1 <strtol+0xbc>
		*endptr = (char *) s;
  800cac:	8b 75 0c             	mov    0xc(%ebp),%esi
  800caf:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800cb1:	85 ff                	test   %edi,%edi
  800cb3:	74 21                	je     800cd6 <strtol+0xe1>
  800cb5:	f7 d8                	neg    %eax
  800cb7:	eb 1d                	jmp    800cd6 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800cb9:	80 39 30             	cmpb   $0x30,(%ecx)
  800cbc:	75 9a                	jne    800c58 <strtol+0x63>
  800cbe:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cc2:	0f 84 7a ff ff ff    	je     800c42 <strtol+0x4d>
  800cc8:	eb 84                	jmp    800c4e <strtol+0x59>
  800cca:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cce:	0f 84 6e ff ff ff    	je     800c42 <strtol+0x4d>
  800cd4:	eb 89                	jmp    800c5f <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cd6:	5b                   	pop    %ebx
  800cd7:	5e                   	pop    %esi
  800cd8:	5f                   	pop    %edi
  800cd9:	5d                   	pop    %ebp
  800cda:	c3                   	ret    
  800cdb:	90                   	nop

00800cdc <__udivdi3>:
  800cdc:	55                   	push   %ebp
  800cdd:	57                   	push   %edi
  800cde:	56                   	push   %esi
  800cdf:	53                   	push   %ebx
  800ce0:	83 ec 1c             	sub    $0x1c,%esp
  800ce3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ce7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ceb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cef:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cf3:	89 ca                	mov    %ecx,%edx
  800cf5:	89 f8                	mov    %edi,%eax
  800cf7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cfb:	85 f6                	test   %esi,%esi
  800cfd:	75 2d                	jne    800d2c <__udivdi3+0x50>
  800cff:	39 cf                	cmp    %ecx,%edi
  800d01:	77 65                	ja     800d68 <__udivdi3+0x8c>
  800d03:	89 fd                	mov    %edi,%ebp
  800d05:	85 ff                	test   %edi,%edi
  800d07:	75 0b                	jne    800d14 <__udivdi3+0x38>
  800d09:	b8 01 00 00 00       	mov    $0x1,%eax
  800d0e:	31 d2                	xor    %edx,%edx
  800d10:	f7 f7                	div    %edi
  800d12:	89 c5                	mov    %eax,%ebp
  800d14:	31 d2                	xor    %edx,%edx
  800d16:	89 c8                	mov    %ecx,%eax
  800d18:	f7 f5                	div    %ebp
  800d1a:	89 c1                	mov    %eax,%ecx
  800d1c:	89 d8                	mov    %ebx,%eax
  800d1e:	f7 f5                	div    %ebp
  800d20:	89 cf                	mov    %ecx,%edi
  800d22:	89 fa                	mov    %edi,%edx
  800d24:	83 c4 1c             	add    $0x1c,%esp
  800d27:	5b                   	pop    %ebx
  800d28:	5e                   	pop    %esi
  800d29:	5f                   	pop    %edi
  800d2a:	5d                   	pop    %ebp
  800d2b:	c3                   	ret    
  800d2c:	39 ce                	cmp    %ecx,%esi
  800d2e:	77 28                	ja     800d58 <__udivdi3+0x7c>
  800d30:	0f bd fe             	bsr    %esi,%edi
  800d33:	83 f7 1f             	xor    $0x1f,%edi
  800d36:	75 40                	jne    800d78 <__udivdi3+0x9c>
  800d38:	39 ce                	cmp    %ecx,%esi
  800d3a:	72 0a                	jb     800d46 <__udivdi3+0x6a>
  800d3c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d40:	0f 87 9e 00 00 00    	ja     800de4 <__udivdi3+0x108>
  800d46:	b8 01 00 00 00       	mov    $0x1,%eax
  800d4b:	89 fa                	mov    %edi,%edx
  800d4d:	83 c4 1c             	add    $0x1c,%esp
  800d50:	5b                   	pop    %ebx
  800d51:	5e                   	pop    %esi
  800d52:	5f                   	pop    %edi
  800d53:	5d                   	pop    %ebp
  800d54:	c3                   	ret    
  800d55:	8d 76 00             	lea    0x0(%esi),%esi
  800d58:	31 ff                	xor    %edi,%edi
  800d5a:	31 c0                	xor    %eax,%eax
  800d5c:	89 fa                	mov    %edi,%edx
  800d5e:	83 c4 1c             	add    $0x1c,%esp
  800d61:	5b                   	pop    %ebx
  800d62:	5e                   	pop    %esi
  800d63:	5f                   	pop    %edi
  800d64:	5d                   	pop    %ebp
  800d65:	c3                   	ret    
  800d66:	66 90                	xchg   %ax,%ax
  800d68:	89 d8                	mov    %ebx,%eax
  800d6a:	f7 f7                	div    %edi
  800d6c:	31 ff                	xor    %edi,%edi
  800d6e:	89 fa                	mov    %edi,%edx
  800d70:	83 c4 1c             	add    $0x1c,%esp
  800d73:	5b                   	pop    %ebx
  800d74:	5e                   	pop    %esi
  800d75:	5f                   	pop    %edi
  800d76:	5d                   	pop    %ebp
  800d77:	c3                   	ret    
  800d78:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d7d:	89 eb                	mov    %ebp,%ebx
  800d7f:	29 fb                	sub    %edi,%ebx
  800d81:	89 f9                	mov    %edi,%ecx
  800d83:	d3 e6                	shl    %cl,%esi
  800d85:	89 c5                	mov    %eax,%ebp
  800d87:	88 d9                	mov    %bl,%cl
  800d89:	d3 ed                	shr    %cl,%ebp
  800d8b:	89 e9                	mov    %ebp,%ecx
  800d8d:	09 f1                	or     %esi,%ecx
  800d8f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d93:	89 f9                	mov    %edi,%ecx
  800d95:	d3 e0                	shl    %cl,%eax
  800d97:	89 c5                	mov    %eax,%ebp
  800d99:	89 d6                	mov    %edx,%esi
  800d9b:	88 d9                	mov    %bl,%cl
  800d9d:	d3 ee                	shr    %cl,%esi
  800d9f:	89 f9                	mov    %edi,%ecx
  800da1:	d3 e2                	shl    %cl,%edx
  800da3:	8b 44 24 08          	mov    0x8(%esp),%eax
  800da7:	88 d9                	mov    %bl,%cl
  800da9:	d3 e8                	shr    %cl,%eax
  800dab:	09 c2                	or     %eax,%edx
  800dad:	89 d0                	mov    %edx,%eax
  800daf:	89 f2                	mov    %esi,%edx
  800db1:	f7 74 24 0c          	divl   0xc(%esp)
  800db5:	89 d6                	mov    %edx,%esi
  800db7:	89 c3                	mov    %eax,%ebx
  800db9:	f7 e5                	mul    %ebp
  800dbb:	39 d6                	cmp    %edx,%esi
  800dbd:	72 19                	jb     800dd8 <__udivdi3+0xfc>
  800dbf:	74 0b                	je     800dcc <__udivdi3+0xf0>
  800dc1:	89 d8                	mov    %ebx,%eax
  800dc3:	31 ff                	xor    %edi,%edi
  800dc5:	e9 58 ff ff ff       	jmp    800d22 <__udivdi3+0x46>
  800dca:	66 90                	xchg   %ax,%ax
  800dcc:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dd0:	89 f9                	mov    %edi,%ecx
  800dd2:	d3 e2                	shl    %cl,%edx
  800dd4:	39 c2                	cmp    %eax,%edx
  800dd6:	73 e9                	jae    800dc1 <__udivdi3+0xe5>
  800dd8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ddb:	31 ff                	xor    %edi,%edi
  800ddd:	e9 40 ff ff ff       	jmp    800d22 <__udivdi3+0x46>
  800de2:	66 90                	xchg   %ax,%ax
  800de4:	31 c0                	xor    %eax,%eax
  800de6:	e9 37 ff ff ff       	jmp    800d22 <__udivdi3+0x46>
  800deb:	90                   	nop

00800dec <__umoddi3>:
  800dec:	55                   	push   %ebp
  800ded:	57                   	push   %edi
  800dee:	56                   	push   %esi
  800def:	53                   	push   %ebx
  800df0:	83 ec 1c             	sub    $0x1c,%esp
  800df3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800df7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dfb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dff:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e03:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e07:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e0b:	89 f3                	mov    %esi,%ebx
  800e0d:	89 fa                	mov    %edi,%edx
  800e0f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e13:	89 34 24             	mov    %esi,(%esp)
  800e16:	85 c0                	test   %eax,%eax
  800e18:	75 1a                	jne    800e34 <__umoddi3+0x48>
  800e1a:	39 f7                	cmp    %esi,%edi
  800e1c:	0f 86 a2 00 00 00    	jbe    800ec4 <__umoddi3+0xd8>
  800e22:	89 c8                	mov    %ecx,%eax
  800e24:	89 f2                	mov    %esi,%edx
  800e26:	f7 f7                	div    %edi
  800e28:	89 d0                	mov    %edx,%eax
  800e2a:	31 d2                	xor    %edx,%edx
  800e2c:	83 c4 1c             	add    $0x1c,%esp
  800e2f:	5b                   	pop    %ebx
  800e30:	5e                   	pop    %esi
  800e31:	5f                   	pop    %edi
  800e32:	5d                   	pop    %ebp
  800e33:	c3                   	ret    
  800e34:	39 f0                	cmp    %esi,%eax
  800e36:	0f 87 ac 00 00 00    	ja     800ee8 <__umoddi3+0xfc>
  800e3c:	0f bd e8             	bsr    %eax,%ebp
  800e3f:	83 f5 1f             	xor    $0x1f,%ebp
  800e42:	0f 84 ac 00 00 00    	je     800ef4 <__umoddi3+0x108>
  800e48:	bf 20 00 00 00       	mov    $0x20,%edi
  800e4d:	29 ef                	sub    %ebp,%edi
  800e4f:	89 fe                	mov    %edi,%esi
  800e51:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e55:	89 e9                	mov    %ebp,%ecx
  800e57:	d3 e0                	shl    %cl,%eax
  800e59:	89 d7                	mov    %edx,%edi
  800e5b:	89 f1                	mov    %esi,%ecx
  800e5d:	d3 ef                	shr    %cl,%edi
  800e5f:	09 c7                	or     %eax,%edi
  800e61:	89 e9                	mov    %ebp,%ecx
  800e63:	d3 e2                	shl    %cl,%edx
  800e65:	89 14 24             	mov    %edx,(%esp)
  800e68:	89 d8                	mov    %ebx,%eax
  800e6a:	d3 e0                	shl    %cl,%eax
  800e6c:	89 c2                	mov    %eax,%edx
  800e6e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e72:	d3 e0                	shl    %cl,%eax
  800e74:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e78:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e7c:	89 f1                	mov    %esi,%ecx
  800e7e:	d3 e8                	shr    %cl,%eax
  800e80:	09 d0                	or     %edx,%eax
  800e82:	d3 eb                	shr    %cl,%ebx
  800e84:	89 da                	mov    %ebx,%edx
  800e86:	f7 f7                	div    %edi
  800e88:	89 d3                	mov    %edx,%ebx
  800e8a:	f7 24 24             	mull   (%esp)
  800e8d:	89 c6                	mov    %eax,%esi
  800e8f:	89 d1                	mov    %edx,%ecx
  800e91:	39 d3                	cmp    %edx,%ebx
  800e93:	0f 82 87 00 00 00    	jb     800f20 <__umoddi3+0x134>
  800e99:	0f 84 91 00 00 00    	je     800f30 <__umoddi3+0x144>
  800e9f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ea3:	29 f2                	sub    %esi,%edx
  800ea5:	19 cb                	sbb    %ecx,%ebx
  800ea7:	89 d8                	mov    %ebx,%eax
  800ea9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ead:	d3 e0                	shl    %cl,%eax
  800eaf:	89 e9                	mov    %ebp,%ecx
  800eb1:	d3 ea                	shr    %cl,%edx
  800eb3:	09 d0                	or     %edx,%eax
  800eb5:	89 e9                	mov    %ebp,%ecx
  800eb7:	d3 eb                	shr    %cl,%ebx
  800eb9:	89 da                	mov    %ebx,%edx
  800ebb:	83 c4 1c             	add    $0x1c,%esp
  800ebe:	5b                   	pop    %ebx
  800ebf:	5e                   	pop    %esi
  800ec0:	5f                   	pop    %edi
  800ec1:	5d                   	pop    %ebp
  800ec2:	c3                   	ret    
  800ec3:	90                   	nop
  800ec4:	89 fd                	mov    %edi,%ebp
  800ec6:	85 ff                	test   %edi,%edi
  800ec8:	75 0b                	jne    800ed5 <__umoddi3+0xe9>
  800eca:	b8 01 00 00 00       	mov    $0x1,%eax
  800ecf:	31 d2                	xor    %edx,%edx
  800ed1:	f7 f7                	div    %edi
  800ed3:	89 c5                	mov    %eax,%ebp
  800ed5:	89 f0                	mov    %esi,%eax
  800ed7:	31 d2                	xor    %edx,%edx
  800ed9:	f7 f5                	div    %ebp
  800edb:	89 c8                	mov    %ecx,%eax
  800edd:	f7 f5                	div    %ebp
  800edf:	89 d0                	mov    %edx,%eax
  800ee1:	e9 44 ff ff ff       	jmp    800e2a <__umoddi3+0x3e>
  800ee6:	66 90                	xchg   %ax,%ax
  800ee8:	89 c8                	mov    %ecx,%eax
  800eea:	89 f2                	mov    %esi,%edx
  800eec:	83 c4 1c             	add    $0x1c,%esp
  800eef:	5b                   	pop    %ebx
  800ef0:	5e                   	pop    %esi
  800ef1:	5f                   	pop    %edi
  800ef2:	5d                   	pop    %ebp
  800ef3:	c3                   	ret    
  800ef4:	3b 04 24             	cmp    (%esp),%eax
  800ef7:	72 06                	jb     800eff <__umoddi3+0x113>
  800ef9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800efd:	77 0f                	ja     800f0e <__umoddi3+0x122>
  800eff:	89 f2                	mov    %esi,%edx
  800f01:	29 f9                	sub    %edi,%ecx
  800f03:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f07:	89 14 24             	mov    %edx,(%esp)
  800f0a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f0e:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f12:	8b 14 24             	mov    (%esp),%edx
  800f15:	83 c4 1c             	add    $0x1c,%esp
  800f18:	5b                   	pop    %ebx
  800f19:	5e                   	pop    %esi
  800f1a:	5f                   	pop    %edi
  800f1b:	5d                   	pop    %ebp
  800f1c:	c3                   	ret    
  800f1d:	8d 76 00             	lea    0x0(%esi),%esi
  800f20:	2b 04 24             	sub    (%esp),%eax
  800f23:	19 fa                	sbb    %edi,%edx
  800f25:	89 d1                	mov    %edx,%ecx
  800f27:	89 c6                	mov    %eax,%esi
  800f29:	e9 71 ff ff ff       	jmp    800e9f <__umoddi3+0xb3>
  800f2e:	66 90                	xchg   %ax,%ax
  800f30:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f34:	72 ea                	jb     800f20 <__umoddi3+0x134>
  800f36:	89 d9                	mov    %ebx,%ecx
  800f38:	e9 62 ff ff ff       	jmp    800e9f <__umoddi3+0xb3>
