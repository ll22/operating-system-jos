
obj/user/testshell.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 4e 04 00 00       	call   80047f <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <wrong>:
	breakpoint();
}

void
wrong(int rfd, int kfd, int off)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	81 ec 84 00 00 00    	sub    $0x84,%esp
  80003f:	8b 75 08             	mov    0x8(%ebp),%esi
  800042:	8b 7d 0c             	mov    0xc(%ebp),%edi
  800045:	8b 5d 10             	mov    0x10(%ebp),%ebx
	char buf[100];
	int n;

	seek(rfd, off);
  800048:	53                   	push   %ebx
  800049:	56                   	push   %esi
  80004a:	e8 c5 17 00 00       	call   801814 <seek>
	seek(kfd, off);
  80004f:	83 c4 08             	add    $0x8,%esp
  800052:	53                   	push   %ebx
  800053:	57                   	push   %edi
  800054:	e8 bb 17 00 00       	call   801814 <seek>

	cprintf("shell produced incorrect output.\n");
  800059:	c7 04 24 80 29 80 00 	movl   $0x802980,(%esp)
  800060:	e8 53 05 00 00       	call   8005b8 <cprintf>
	cprintf("expected:\n===\n");
  800065:	c7 04 24 eb 29 80 00 	movl   $0x8029eb,(%esp)
  80006c:	e8 47 05 00 00       	call   8005b8 <cprintf>
	while ((n = read(kfd, buf, sizeof buf-1)) > 0)
  800071:	83 c4 10             	add    $0x10,%esp
  800074:	8d 5d 84             	lea    -0x7c(%ebp),%ebx
  800077:	eb 0d                	jmp    800086 <wrong+0x53>
		sys_cputs(buf, n);
  800079:	83 ec 08             	sub    $0x8,%esp
  80007c:	50                   	push   %eax
  80007d:	53                   	push   %ebx
  80007e:	e8 c1 0d 00 00       	call   800e44 <sys_cputs>
  800083:	83 c4 10             	add    $0x10,%esp
	seek(rfd, off);
	seek(kfd, off);

	cprintf("shell produced incorrect output.\n");
	cprintf("expected:\n===\n");
	while ((n = read(kfd, buf, sizeof buf-1)) > 0)
  800086:	83 ec 04             	sub    $0x4,%esp
  800089:	6a 63                	push   $0x63
  80008b:	53                   	push   %ebx
  80008c:	57                   	push   %edi
  80008d:	e8 30 16 00 00       	call   8016c2 <read>
  800092:	83 c4 10             	add    $0x10,%esp
  800095:	85 c0                	test   %eax,%eax
  800097:	7f e0                	jg     800079 <wrong+0x46>
		sys_cputs(buf, n);
	cprintf("===\ngot:\n===\n");
  800099:	83 ec 0c             	sub    $0xc,%esp
  80009c:	68 fa 29 80 00       	push   $0x8029fa
  8000a1:	e8 12 05 00 00       	call   8005b8 <cprintf>
	while ((n = read(rfd, buf, sizeof buf-1)) > 0)
  8000a6:	83 c4 10             	add    $0x10,%esp
  8000a9:	8d 5d 84             	lea    -0x7c(%ebp),%ebx
  8000ac:	eb 0d                	jmp    8000bb <wrong+0x88>
		sys_cputs(buf, n);
  8000ae:	83 ec 08             	sub    $0x8,%esp
  8000b1:	50                   	push   %eax
  8000b2:	53                   	push   %ebx
  8000b3:	e8 8c 0d 00 00       	call   800e44 <sys_cputs>
  8000b8:	83 c4 10             	add    $0x10,%esp
	cprintf("shell produced incorrect output.\n");
	cprintf("expected:\n===\n");
	while ((n = read(kfd, buf, sizeof buf-1)) > 0)
		sys_cputs(buf, n);
	cprintf("===\ngot:\n===\n");
	while ((n = read(rfd, buf, sizeof buf-1)) > 0)
  8000bb:	83 ec 04             	sub    $0x4,%esp
  8000be:	6a 63                	push   $0x63
  8000c0:	53                   	push   %ebx
  8000c1:	56                   	push   %esi
  8000c2:	e8 fb 15 00 00       	call   8016c2 <read>
  8000c7:	83 c4 10             	add    $0x10,%esp
  8000ca:	85 c0                	test   %eax,%eax
  8000cc:	7f e0                	jg     8000ae <wrong+0x7b>
		sys_cputs(buf, n);
	cprintf("===\n");
  8000ce:	83 ec 0c             	sub    $0xc,%esp
  8000d1:	68 f5 29 80 00       	push   $0x8029f5
  8000d6:	e8 dd 04 00 00       	call   8005b8 <cprintf>
	exit();
  8000db:	e8 ee 03 00 00       	call   8004ce <exit>
}
  8000e0:	83 c4 10             	add    $0x10,%esp
  8000e3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000e6:	5b                   	pop    %ebx
  8000e7:	5e                   	pop    %esi
  8000e8:	5f                   	pop    %edi
  8000e9:	5d                   	pop    %ebp
  8000ea:	c3                   	ret    

008000eb <umain>:

void wrong(int, int, int);

void
umain(int argc, char **argv)
{
  8000eb:	55                   	push   %ebp
  8000ec:	89 e5                	mov    %esp,%ebp
  8000ee:	57                   	push   %edi
  8000ef:	56                   	push   %esi
  8000f0:	53                   	push   %ebx
  8000f1:	83 ec 38             	sub    $0x38,%esp
	char c1, c2;
	int r, rfd, wfd, kfd, n1, n2, off, nloff;
	int pfds[2];

	close(0);
  8000f4:	6a 00                	push   $0x0
  8000f6:	e8 8f 14 00 00       	call   80158a <close>
	close(1);
  8000fb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800102:	e8 83 14 00 00       	call   80158a <close>
	opencons();
  800107:	e8 21 03 00 00       	call   80042d <opencons>
	opencons();
  80010c:	e8 1c 03 00 00       	call   80042d <opencons>

	if ((rfd = open("testshell.sh", O_RDONLY)) < 0)
  800111:	83 c4 08             	add    $0x8,%esp
  800114:	6a 00                	push   $0x0
  800116:	68 08 2a 80 00       	push   $0x802a08
  80011b:	e8 09 1a 00 00       	call   801b29 <open>
  800120:	89 c3                	mov    %eax,%ebx
  800122:	83 c4 10             	add    $0x10,%esp
  800125:	85 c0                	test   %eax,%eax
  800127:	79 12                	jns    80013b <umain+0x50>
		panic("open testshell.sh: %e", rfd);
  800129:	50                   	push   %eax
  80012a:	68 15 2a 80 00       	push   $0x802a15
  80012f:	6a 13                	push   $0x13
  800131:	68 2b 2a 80 00       	push   $0x802a2b
  800136:	e8 a5 03 00 00       	call   8004e0 <_panic>
	if ((wfd = pipe(pfds)) < 0)
  80013b:	83 ec 0c             	sub    $0xc,%esp
  80013e:	8d 45 dc             	lea    -0x24(%ebp),%eax
  800141:	50                   	push   %eax
  800142:	e8 2a 22 00 00       	call   802371 <pipe>
  800147:	83 c4 10             	add    $0x10,%esp
  80014a:	85 c0                	test   %eax,%eax
  80014c:	79 12                	jns    800160 <umain+0x75>
		panic("pipe: %e", wfd);
  80014e:	50                   	push   %eax
  80014f:	68 3c 2a 80 00       	push   $0x802a3c
  800154:	6a 15                	push   $0x15
  800156:	68 2b 2a 80 00       	push   $0x802a2b
  80015b:	e8 80 03 00 00       	call   8004e0 <_panic>
	wfd = pfds[1];
  800160:	8b 75 e0             	mov    -0x20(%ebp),%esi

	cprintf("running sh -x < testshell.sh | cat\n");
  800163:	83 ec 0c             	sub    $0xc,%esp
  800166:	68 a4 29 80 00       	push   $0x8029a4
  80016b:	e8 48 04 00 00       	call   8005b8 <cprintf>
	if ((r = fork()) < 0)
  800170:	e8 4d 10 00 00       	call   8011c2 <fork>
  800175:	83 c4 10             	add    $0x10,%esp
  800178:	85 c0                	test   %eax,%eax
  80017a:	79 12                	jns    80018e <umain+0xa3>
		panic("fork: %e", r);
  80017c:	50                   	push   %eax
  80017d:	68 45 2a 80 00       	push   $0x802a45
  800182:	6a 1a                	push   $0x1a
  800184:	68 2b 2a 80 00       	push   $0x802a2b
  800189:	e8 52 03 00 00       	call   8004e0 <_panic>
	if (r == 0) {
  80018e:	85 c0                	test   %eax,%eax
  800190:	75 7d                	jne    80020f <umain+0x124>
		dup(rfd, 0);
  800192:	83 ec 08             	sub    $0x8,%esp
  800195:	6a 00                	push   $0x0
  800197:	53                   	push   %ebx
  800198:	e8 3b 14 00 00       	call   8015d8 <dup>
		dup(wfd, 1);
  80019d:	83 c4 08             	add    $0x8,%esp
  8001a0:	6a 01                	push   $0x1
  8001a2:	56                   	push   %esi
  8001a3:	e8 30 14 00 00       	call   8015d8 <dup>
		close(rfd);
  8001a8:	89 1c 24             	mov    %ebx,(%esp)
  8001ab:	e8 da 13 00 00       	call   80158a <close>
		close(wfd);
  8001b0:	89 34 24             	mov    %esi,(%esp)
  8001b3:	e8 d2 13 00 00       	call   80158a <close>
		if ((r = spawnl("/sh", "sh", "-x", 0)) < 0)
  8001b8:	6a 00                	push   $0x0
  8001ba:	68 4e 2a 80 00       	push   $0x802a4e
  8001bf:	68 12 2a 80 00       	push   $0x802a12
  8001c4:	68 51 2a 80 00       	push   $0x802a51
  8001c9:	e8 71 1f 00 00       	call   80213f <spawnl>
  8001ce:	89 c7                	mov    %eax,%edi
  8001d0:	83 c4 20             	add    $0x20,%esp
  8001d3:	85 c0                	test   %eax,%eax
  8001d5:	79 12                	jns    8001e9 <umain+0xfe>
			panic("spawn: %e", r);
  8001d7:	50                   	push   %eax
  8001d8:	68 55 2a 80 00       	push   $0x802a55
  8001dd:	6a 21                	push   $0x21
  8001df:	68 2b 2a 80 00       	push   $0x802a2b
  8001e4:	e8 f7 02 00 00       	call   8004e0 <_panic>
		close(0);
  8001e9:	83 ec 0c             	sub    $0xc,%esp
  8001ec:	6a 00                	push   $0x0
  8001ee:	e8 97 13 00 00       	call   80158a <close>
		close(1);
  8001f3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  8001fa:	e8 8b 13 00 00       	call   80158a <close>
		wait(r);
  8001ff:	89 3c 24             	mov    %edi,(%esp)
  800202:	e8 ea 22 00 00       	call   8024f1 <wait>
		exit();
  800207:	e8 c2 02 00 00       	call   8004ce <exit>
  80020c:	83 c4 10             	add    $0x10,%esp
	}
	close(rfd);
  80020f:	83 ec 0c             	sub    $0xc,%esp
  800212:	53                   	push   %ebx
  800213:	e8 72 13 00 00       	call   80158a <close>
	close(wfd);
  800218:	89 34 24             	mov    %esi,(%esp)
  80021b:	e8 6a 13 00 00       	call   80158a <close>

	rfd = pfds[0];
  800220:	8b 7d dc             	mov    -0x24(%ebp),%edi
	if ((kfd = open("testshell.key", O_RDONLY)) < 0)
  800223:	83 c4 08             	add    $0x8,%esp
  800226:	6a 00                	push   $0x0
  800228:	68 5f 2a 80 00       	push   $0x802a5f
  80022d:	e8 f7 18 00 00       	call   801b29 <open>
  800232:	89 c6                	mov    %eax,%esi
  800234:	83 c4 10             	add    $0x10,%esp
  800237:	85 c0                	test   %eax,%eax
  800239:	79 12                	jns    80024d <umain+0x162>
		panic("open testshell.key for reading: %e", kfd);
  80023b:	50                   	push   %eax
  80023c:	68 c8 29 80 00       	push   $0x8029c8
  800241:	6a 2c                	push   $0x2c
  800243:	68 2b 2a 80 00       	push   $0x802a2b
  800248:	e8 93 02 00 00       	call   8004e0 <_panic>
  80024d:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  800254:	c7 45 d0 00 00 00 00 	movl   $0x0,-0x30(%ebp)

	nloff = 0;
	for (off=0;; off++) {
		n1 = read(rfd, &c1, 1);
  80025b:	83 ec 04             	sub    $0x4,%esp
  80025e:	6a 01                	push   $0x1
  800260:	8d 45 e7             	lea    -0x19(%ebp),%eax
  800263:	50                   	push   %eax
  800264:	57                   	push   %edi
  800265:	e8 58 14 00 00       	call   8016c2 <read>
  80026a:	89 c3                	mov    %eax,%ebx
		n2 = read(kfd, &c2, 1);
  80026c:	83 c4 0c             	add    $0xc,%esp
  80026f:	6a 01                	push   $0x1
  800271:	8d 45 e6             	lea    -0x1a(%ebp),%eax
  800274:	50                   	push   %eax
  800275:	56                   	push   %esi
  800276:	e8 47 14 00 00       	call   8016c2 <read>
		if (n1 < 0)
  80027b:	83 c4 10             	add    $0x10,%esp
  80027e:	85 db                	test   %ebx,%ebx
  800280:	79 12                	jns    800294 <umain+0x1a9>
			panic("reading testshell.out: %e", n1);
  800282:	53                   	push   %ebx
  800283:	68 6d 2a 80 00       	push   $0x802a6d
  800288:	6a 33                	push   $0x33
  80028a:	68 2b 2a 80 00       	push   $0x802a2b
  80028f:	e8 4c 02 00 00       	call   8004e0 <_panic>
		if (n2 < 0)
  800294:	85 c0                	test   %eax,%eax
  800296:	79 12                	jns    8002aa <umain+0x1bf>
			panic("reading testshell.key: %e", n2);
  800298:	50                   	push   %eax
  800299:	68 87 2a 80 00       	push   $0x802a87
  80029e:	6a 35                	push   $0x35
  8002a0:	68 2b 2a 80 00       	push   $0x802a2b
  8002a5:	e8 36 02 00 00       	call   8004e0 <_panic>
		if (n1 == 0 && n2 == 0)
  8002aa:	85 db                	test   %ebx,%ebx
  8002ac:	75 06                	jne    8002b4 <umain+0x1c9>
  8002ae:	85 c0                	test   %eax,%eax
  8002b0:	75 14                	jne    8002c6 <umain+0x1db>
  8002b2:	eb 36                	jmp    8002ea <umain+0x1ff>
			break;
		if (n1 != 1 || n2 != 1 || c1 != c2)
  8002b4:	83 fb 01             	cmp    $0x1,%ebx
  8002b7:	75 0d                	jne    8002c6 <umain+0x1db>
  8002b9:	83 f8 01             	cmp    $0x1,%eax
  8002bc:	75 08                	jne    8002c6 <umain+0x1db>
  8002be:	8a 45 e6             	mov    -0x1a(%ebp),%al
  8002c1:	38 45 e7             	cmp    %al,-0x19(%ebp)
  8002c4:	74 10                	je     8002d6 <umain+0x1eb>
			wrong(rfd, kfd, nloff);
  8002c6:	83 ec 04             	sub    $0x4,%esp
  8002c9:	ff 75 d0             	pushl  -0x30(%ebp)
  8002cc:	56                   	push   %esi
  8002cd:	57                   	push   %edi
  8002ce:	e8 60 fd ff ff       	call   800033 <wrong>
  8002d3:	83 c4 10             	add    $0x10,%esp
		if (c1 == '\n')
  8002d6:	80 7d e7 0a          	cmpb   $0xa,-0x19(%ebp)
  8002da:	75 06                	jne    8002e2 <umain+0x1f7>
			nloff = off+1;
  8002dc:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  8002df:	89 45 d0             	mov    %eax,-0x30(%ebp)
  8002e2:	ff 45 d4             	incl   -0x2c(%ebp)
	}
  8002e5:	e9 71 ff ff ff       	jmp    80025b <umain+0x170>
	cprintf("shell ran correctly\n");
  8002ea:	83 ec 0c             	sub    $0xc,%esp
  8002ed:	68 a1 2a 80 00       	push   $0x802aa1
  8002f2:	e8 c1 02 00 00       	call   8005b8 <cprintf>
#include <inc/types.h>

static inline void
breakpoint(void)
{
	asm volatile("int3");
  8002f7:	cc                   	int3   

	breakpoint();
}
  8002f8:	83 c4 10             	add    $0x10,%esp
  8002fb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002fe:	5b                   	pop    %ebx
  8002ff:	5e                   	pop    %esi
  800300:	5f                   	pop    %edi
  800301:	5d                   	pop    %ebp
  800302:	c3                   	ret    

00800303 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  800303:	55                   	push   %ebp
  800304:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  800306:	b8 00 00 00 00       	mov    $0x0,%eax
  80030b:	5d                   	pop    %ebp
  80030c:	c3                   	ret    

0080030d <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  80030d:	55                   	push   %ebp
  80030e:	89 e5                	mov    %esp,%ebp
  800310:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  800313:	68 b6 2a 80 00       	push   $0x802ab6
  800318:	ff 75 0c             	pushl  0xc(%ebp)
  80031b:	e8 fc 07 00 00       	call   800b1c <strcpy>
	return 0;
}
  800320:	b8 00 00 00 00       	mov    $0x0,%eax
  800325:	c9                   	leave  
  800326:	c3                   	ret    

00800327 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  800327:	55                   	push   %ebp
  800328:	89 e5                	mov    %esp,%ebp
  80032a:	57                   	push   %edi
  80032b:	56                   	push   %esi
  80032c:	53                   	push   %ebx
  80032d:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800333:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  800338:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  80033e:	eb 2c                	jmp    80036c <devcons_write+0x45>
		m = n - tot;
  800340:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800343:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  800345:	83 fb 7f             	cmp    $0x7f,%ebx
  800348:	76 05                	jbe    80034f <devcons_write+0x28>
			m = sizeof(buf) - 1;
  80034a:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  80034f:	83 ec 04             	sub    $0x4,%esp
  800352:	53                   	push   %ebx
  800353:	03 45 0c             	add    0xc(%ebp),%eax
  800356:	50                   	push   %eax
  800357:	57                   	push   %edi
  800358:	e8 34 09 00 00       	call   800c91 <memmove>
		sys_cputs(buf, m);
  80035d:	83 c4 08             	add    $0x8,%esp
  800360:	53                   	push   %ebx
  800361:	57                   	push   %edi
  800362:	e8 dd 0a 00 00       	call   800e44 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800367:	01 de                	add    %ebx,%esi
  800369:	83 c4 10             	add    $0x10,%esp
  80036c:	89 f0                	mov    %esi,%eax
  80036e:	3b 75 10             	cmp    0x10(%ebp),%esi
  800371:	72 cd                	jb     800340 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  800373:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800376:	5b                   	pop    %ebx
  800377:	5e                   	pop    %esi
  800378:	5f                   	pop    %edi
  800379:	5d                   	pop    %ebp
  80037a:	c3                   	ret    

0080037b <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  80037b:	55                   	push   %ebp
  80037c:	89 e5                	mov    %esp,%ebp
  80037e:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  800381:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800385:	75 07                	jne    80038e <devcons_read+0x13>
  800387:	eb 23                	jmp    8003ac <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  800389:	e8 53 0b 00 00       	call   800ee1 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  80038e:	e8 cf 0a 00 00       	call   800e62 <sys_cgetc>
  800393:	85 c0                	test   %eax,%eax
  800395:	74 f2                	je     800389 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  800397:	85 c0                	test   %eax,%eax
  800399:	78 1d                	js     8003b8 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  80039b:	83 f8 04             	cmp    $0x4,%eax
  80039e:	74 13                	je     8003b3 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  8003a0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8003a3:	88 02                	mov    %al,(%edx)
	return 1;
  8003a5:	b8 01 00 00 00       	mov    $0x1,%eax
  8003aa:	eb 0c                	jmp    8003b8 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  8003ac:	b8 00 00 00 00       	mov    $0x0,%eax
  8003b1:	eb 05                	jmp    8003b8 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  8003b3:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  8003b8:	c9                   	leave  
  8003b9:	c3                   	ret    

008003ba <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  8003ba:	55                   	push   %ebp
  8003bb:	89 e5                	mov    %esp,%ebp
  8003bd:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  8003c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8003c3:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  8003c6:	6a 01                	push   $0x1
  8003c8:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8003cb:	50                   	push   %eax
  8003cc:	e8 73 0a 00 00       	call   800e44 <sys_cputs>
}
  8003d1:	83 c4 10             	add    $0x10,%esp
  8003d4:	c9                   	leave  
  8003d5:	c3                   	ret    

008003d6 <getchar>:

int
getchar(void)
{
  8003d6:	55                   	push   %ebp
  8003d7:	89 e5                	mov    %esp,%ebp
  8003d9:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  8003dc:	6a 01                	push   $0x1
  8003de:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8003e1:	50                   	push   %eax
  8003e2:	6a 00                	push   $0x0
  8003e4:	e8 d9 12 00 00       	call   8016c2 <read>
	if (r < 0)
  8003e9:	83 c4 10             	add    $0x10,%esp
  8003ec:	85 c0                	test   %eax,%eax
  8003ee:	78 0f                	js     8003ff <getchar+0x29>
		return r;
	if (r < 1)
  8003f0:	85 c0                	test   %eax,%eax
  8003f2:	7e 06                	jle    8003fa <getchar+0x24>
		return -E_EOF;
	return c;
  8003f4:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  8003f8:	eb 05                	jmp    8003ff <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  8003fa:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  8003ff:	c9                   	leave  
  800400:	c3                   	ret    

00800401 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  800401:	55                   	push   %ebp
  800402:	89 e5                	mov    %esp,%ebp
  800404:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800407:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80040a:	50                   	push   %eax
  80040b:	ff 75 08             	pushl  0x8(%ebp)
  80040e:	e8 49 10 00 00       	call   80145c <fd_lookup>
  800413:	83 c4 10             	add    $0x10,%esp
  800416:	85 c0                	test   %eax,%eax
  800418:	78 11                	js     80042b <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  80041a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80041d:	8b 15 00 40 80 00    	mov    0x804000,%edx
  800423:	39 10                	cmp    %edx,(%eax)
  800425:	0f 94 c0             	sete   %al
  800428:	0f b6 c0             	movzbl %al,%eax
}
  80042b:	c9                   	leave  
  80042c:	c3                   	ret    

0080042d <opencons>:

int
opencons(void)
{
  80042d:	55                   	push   %ebp
  80042e:	89 e5                	mov    %esp,%ebp
  800430:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  800433:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800436:	50                   	push   %eax
  800437:	e8 d1 0f 00 00       	call   80140d <fd_alloc>
  80043c:	83 c4 10             	add    $0x10,%esp
  80043f:	85 c0                	test   %eax,%eax
  800441:	78 3a                	js     80047d <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  800443:	83 ec 04             	sub    $0x4,%esp
  800446:	68 07 04 00 00       	push   $0x407
  80044b:	ff 75 f4             	pushl  -0xc(%ebp)
  80044e:	6a 00                	push   $0x0
  800450:	e8 ab 0a 00 00       	call   800f00 <sys_page_alloc>
  800455:	83 c4 10             	add    $0x10,%esp
  800458:	85 c0                	test   %eax,%eax
  80045a:	78 21                	js     80047d <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  80045c:	8b 15 00 40 80 00    	mov    0x804000,%edx
  800462:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800465:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  800467:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80046a:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  800471:	83 ec 0c             	sub    $0xc,%esp
  800474:	50                   	push   %eax
  800475:	e8 6c 0f 00 00       	call   8013e6 <fd2num>
  80047a:	83 c4 10             	add    $0x10,%esp
}
  80047d:	c9                   	leave  
  80047e:	c3                   	ret    

0080047f <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80047f:	55                   	push   %ebp
  800480:	89 e5                	mov    %esp,%ebp
  800482:	56                   	push   %esi
  800483:	53                   	push   %ebx
  800484:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800487:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80048a:	e8 33 0a 00 00       	call   800ec2 <sys_getenvid>
  80048f:	25 ff 03 00 00       	and    $0x3ff,%eax
  800494:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80049b:	c1 e0 07             	shl    $0x7,%eax
  80049e:	29 d0                	sub    %edx,%eax
  8004a0:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8004a5:	a3 04 50 80 00       	mov    %eax,0x805004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8004aa:	85 db                	test   %ebx,%ebx
  8004ac:	7e 07                	jle    8004b5 <libmain+0x36>
		binaryname = argv[0];
  8004ae:	8b 06                	mov    (%esi),%eax
  8004b0:	a3 1c 40 80 00       	mov    %eax,0x80401c

	// call user main routine
	umain(argc, argv);
  8004b5:	83 ec 08             	sub    $0x8,%esp
  8004b8:	56                   	push   %esi
  8004b9:	53                   	push   %ebx
  8004ba:	e8 2c fc ff ff       	call   8000eb <umain>

	// exit gracefully
	exit();
  8004bf:	e8 0a 00 00 00       	call   8004ce <exit>
}
  8004c4:	83 c4 10             	add    $0x10,%esp
  8004c7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8004ca:	5b                   	pop    %ebx
  8004cb:	5e                   	pop    %esi
  8004cc:	5d                   	pop    %ebp
  8004cd:	c3                   	ret    

008004ce <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8004ce:	55                   	push   %ebp
  8004cf:	89 e5                	mov    %esp,%ebp
  8004d1:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8004d4:	6a 00                	push   $0x0
  8004d6:	e8 a6 09 00 00       	call   800e81 <sys_env_destroy>
}
  8004db:	83 c4 10             	add    $0x10,%esp
  8004de:	c9                   	leave  
  8004df:	c3                   	ret    

008004e0 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8004e0:	55                   	push   %ebp
  8004e1:	89 e5                	mov    %esp,%ebp
  8004e3:	56                   	push   %esi
  8004e4:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8004e5:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8004e8:	8b 35 1c 40 80 00    	mov    0x80401c,%esi
  8004ee:	e8 cf 09 00 00       	call   800ec2 <sys_getenvid>
  8004f3:	83 ec 0c             	sub    $0xc,%esp
  8004f6:	ff 75 0c             	pushl  0xc(%ebp)
  8004f9:	ff 75 08             	pushl  0x8(%ebp)
  8004fc:	56                   	push   %esi
  8004fd:	50                   	push   %eax
  8004fe:	68 cc 2a 80 00       	push   $0x802acc
  800503:	e8 b0 00 00 00       	call   8005b8 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800508:	83 c4 18             	add    $0x18,%esp
  80050b:	53                   	push   %ebx
  80050c:	ff 75 10             	pushl  0x10(%ebp)
  80050f:	e8 53 00 00 00       	call   800567 <vcprintf>
	cprintf("\n");
  800514:	c7 04 24 f8 29 80 00 	movl   $0x8029f8,(%esp)
  80051b:	e8 98 00 00 00       	call   8005b8 <cprintf>
  800520:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800523:	cc                   	int3   
  800524:	eb fd                	jmp    800523 <_panic+0x43>

00800526 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800526:	55                   	push   %ebp
  800527:	89 e5                	mov    %esp,%ebp
  800529:	53                   	push   %ebx
  80052a:	83 ec 04             	sub    $0x4,%esp
  80052d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800530:	8b 13                	mov    (%ebx),%edx
  800532:	8d 42 01             	lea    0x1(%edx),%eax
  800535:	89 03                	mov    %eax,(%ebx)
  800537:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80053a:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80053e:	3d ff 00 00 00       	cmp    $0xff,%eax
  800543:	75 1a                	jne    80055f <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800545:	83 ec 08             	sub    $0x8,%esp
  800548:	68 ff 00 00 00       	push   $0xff
  80054d:	8d 43 08             	lea    0x8(%ebx),%eax
  800550:	50                   	push   %eax
  800551:	e8 ee 08 00 00       	call   800e44 <sys_cputs>
		b->idx = 0;
  800556:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80055c:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80055f:	ff 43 04             	incl   0x4(%ebx)
}
  800562:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800565:	c9                   	leave  
  800566:	c3                   	ret    

00800567 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800567:	55                   	push   %ebp
  800568:	89 e5                	mov    %esp,%ebp
  80056a:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800570:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800577:	00 00 00 
	b.cnt = 0;
  80057a:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800581:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800584:	ff 75 0c             	pushl  0xc(%ebp)
  800587:	ff 75 08             	pushl  0x8(%ebp)
  80058a:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800590:	50                   	push   %eax
  800591:	68 26 05 80 00       	push   $0x800526
  800596:	e8 51 01 00 00       	call   8006ec <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80059b:	83 c4 08             	add    $0x8,%esp
  80059e:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8005a4:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8005aa:	50                   	push   %eax
  8005ab:	e8 94 08 00 00       	call   800e44 <sys_cputs>

	return b.cnt;
}
  8005b0:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8005b6:	c9                   	leave  
  8005b7:	c3                   	ret    

008005b8 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8005b8:	55                   	push   %ebp
  8005b9:	89 e5                	mov    %esp,%ebp
  8005bb:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8005be:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8005c1:	50                   	push   %eax
  8005c2:	ff 75 08             	pushl  0x8(%ebp)
  8005c5:	e8 9d ff ff ff       	call   800567 <vcprintf>
	va_end(ap);

	return cnt;
}
  8005ca:	c9                   	leave  
  8005cb:	c3                   	ret    

008005cc <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8005cc:	55                   	push   %ebp
  8005cd:	89 e5                	mov    %esp,%ebp
  8005cf:	57                   	push   %edi
  8005d0:	56                   	push   %esi
  8005d1:	53                   	push   %ebx
  8005d2:	83 ec 1c             	sub    $0x1c,%esp
  8005d5:	89 c7                	mov    %eax,%edi
  8005d7:	89 d6                	mov    %edx,%esi
  8005d9:	8b 45 08             	mov    0x8(%ebp),%eax
  8005dc:	8b 55 0c             	mov    0xc(%ebp),%edx
  8005df:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005e2:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8005e5:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8005e8:	bb 00 00 00 00       	mov    $0x0,%ebx
  8005ed:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8005f0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8005f3:	39 d3                	cmp    %edx,%ebx
  8005f5:	72 05                	jb     8005fc <printnum+0x30>
  8005f7:	39 45 10             	cmp    %eax,0x10(%ebp)
  8005fa:	77 45                	ja     800641 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8005fc:	83 ec 0c             	sub    $0xc,%esp
  8005ff:	ff 75 18             	pushl  0x18(%ebp)
  800602:	8b 45 14             	mov    0x14(%ebp),%eax
  800605:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800608:	53                   	push   %ebx
  800609:	ff 75 10             	pushl  0x10(%ebp)
  80060c:	83 ec 08             	sub    $0x8,%esp
  80060f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800612:	ff 75 e0             	pushl  -0x20(%ebp)
  800615:	ff 75 dc             	pushl  -0x24(%ebp)
  800618:	ff 75 d8             	pushl  -0x28(%ebp)
  80061b:	e8 ec 20 00 00       	call   80270c <__udivdi3>
  800620:	83 c4 18             	add    $0x18,%esp
  800623:	52                   	push   %edx
  800624:	50                   	push   %eax
  800625:	89 f2                	mov    %esi,%edx
  800627:	89 f8                	mov    %edi,%eax
  800629:	e8 9e ff ff ff       	call   8005cc <printnum>
  80062e:	83 c4 20             	add    $0x20,%esp
  800631:	eb 16                	jmp    800649 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800633:	83 ec 08             	sub    $0x8,%esp
  800636:	56                   	push   %esi
  800637:	ff 75 18             	pushl  0x18(%ebp)
  80063a:	ff d7                	call   *%edi
  80063c:	83 c4 10             	add    $0x10,%esp
  80063f:	eb 03                	jmp    800644 <printnum+0x78>
  800641:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800644:	4b                   	dec    %ebx
  800645:	85 db                	test   %ebx,%ebx
  800647:	7f ea                	jg     800633 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800649:	83 ec 08             	sub    $0x8,%esp
  80064c:	56                   	push   %esi
  80064d:	83 ec 04             	sub    $0x4,%esp
  800650:	ff 75 e4             	pushl  -0x1c(%ebp)
  800653:	ff 75 e0             	pushl  -0x20(%ebp)
  800656:	ff 75 dc             	pushl  -0x24(%ebp)
  800659:	ff 75 d8             	pushl  -0x28(%ebp)
  80065c:	e8 bb 21 00 00       	call   80281c <__umoddi3>
  800661:	83 c4 14             	add    $0x14,%esp
  800664:	0f be 80 ef 2a 80 00 	movsbl 0x802aef(%eax),%eax
  80066b:	50                   	push   %eax
  80066c:	ff d7                	call   *%edi
}
  80066e:	83 c4 10             	add    $0x10,%esp
  800671:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800674:	5b                   	pop    %ebx
  800675:	5e                   	pop    %esi
  800676:	5f                   	pop    %edi
  800677:	5d                   	pop    %ebp
  800678:	c3                   	ret    

00800679 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800679:	55                   	push   %ebp
  80067a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80067c:	83 fa 01             	cmp    $0x1,%edx
  80067f:	7e 0e                	jle    80068f <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800681:	8b 10                	mov    (%eax),%edx
  800683:	8d 4a 08             	lea    0x8(%edx),%ecx
  800686:	89 08                	mov    %ecx,(%eax)
  800688:	8b 02                	mov    (%edx),%eax
  80068a:	8b 52 04             	mov    0x4(%edx),%edx
  80068d:	eb 22                	jmp    8006b1 <getuint+0x38>
	else if (lflag)
  80068f:	85 d2                	test   %edx,%edx
  800691:	74 10                	je     8006a3 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800693:	8b 10                	mov    (%eax),%edx
  800695:	8d 4a 04             	lea    0x4(%edx),%ecx
  800698:	89 08                	mov    %ecx,(%eax)
  80069a:	8b 02                	mov    (%edx),%eax
  80069c:	ba 00 00 00 00       	mov    $0x0,%edx
  8006a1:	eb 0e                	jmp    8006b1 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8006a3:	8b 10                	mov    (%eax),%edx
  8006a5:	8d 4a 04             	lea    0x4(%edx),%ecx
  8006a8:	89 08                	mov    %ecx,(%eax)
  8006aa:	8b 02                	mov    (%edx),%eax
  8006ac:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8006b1:	5d                   	pop    %ebp
  8006b2:	c3                   	ret    

008006b3 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8006b3:	55                   	push   %ebp
  8006b4:	89 e5                	mov    %esp,%ebp
  8006b6:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8006b9:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8006bc:	8b 10                	mov    (%eax),%edx
  8006be:	3b 50 04             	cmp    0x4(%eax),%edx
  8006c1:	73 0a                	jae    8006cd <sprintputch+0x1a>
		*b->buf++ = ch;
  8006c3:	8d 4a 01             	lea    0x1(%edx),%ecx
  8006c6:	89 08                	mov    %ecx,(%eax)
  8006c8:	8b 45 08             	mov    0x8(%ebp),%eax
  8006cb:	88 02                	mov    %al,(%edx)
}
  8006cd:	5d                   	pop    %ebp
  8006ce:	c3                   	ret    

008006cf <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8006cf:	55                   	push   %ebp
  8006d0:	89 e5                	mov    %esp,%ebp
  8006d2:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8006d5:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8006d8:	50                   	push   %eax
  8006d9:	ff 75 10             	pushl  0x10(%ebp)
  8006dc:	ff 75 0c             	pushl  0xc(%ebp)
  8006df:	ff 75 08             	pushl  0x8(%ebp)
  8006e2:	e8 05 00 00 00       	call   8006ec <vprintfmt>
	va_end(ap);
}
  8006e7:	83 c4 10             	add    $0x10,%esp
  8006ea:	c9                   	leave  
  8006eb:	c3                   	ret    

008006ec <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8006ec:	55                   	push   %ebp
  8006ed:	89 e5                	mov    %esp,%ebp
  8006ef:	57                   	push   %edi
  8006f0:	56                   	push   %esi
  8006f1:	53                   	push   %ebx
  8006f2:	83 ec 2c             	sub    $0x2c,%esp
  8006f5:	8b 75 08             	mov    0x8(%ebp),%esi
  8006f8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8006fb:	8b 7d 10             	mov    0x10(%ebp),%edi
  8006fe:	eb 12                	jmp    800712 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800700:	85 c0                	test   %eax,%eax
  800702:	0f 84 68 03 00 00    	je     800a70 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800708:	83 ec 08             	sub    $0x8,%esp
  80070b:	53                   	push   %ebx
  80070c:	50                   	push   %eax
  80070d:	ff d6                	call   *%esi
  80070f:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800712:	47                   	inc    %edi
  800713:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800717:	83 f8 25             	cmp    $0x25,%eax
  80071a:	75 e4                	jne    800700 <vprintfmt+0x14>
  80071c:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800720:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800727:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80072e:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800735:	ba 00 00 00 00       	mov    $0x0,%edx
  80073a:	eb 07                	jmp    800743 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80073c:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80073f:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800743:	8d 47 01             	lea    0x1(%edi),%eax
  800746:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800749:	0f b6 0f             	movzbl (%edi),%ecx
  80074c:	8a 07                	mov    (%edi),%al
  80074e:	83 e8 23             	sub    $0x23,%eax
  800751:	3c 55                	cmp    $0x55,%al
  800753:	0f 87 fe 02 00 00    	ja     800a57 <vprintfmt+0x36b>
  800759:	0f b6 c0             	movzbl %al,%eax
  80075c:	ff 24 85 40 2c 80 00 	jmp    *0x802c40(,%eax,4)
  800763:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800766:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80076a:	eb d7                	jmp    800743 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80076c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80076f:	b8 00 00 00 00       	mov    $0x0,%eax
  800774:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800777:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80077a:	01 c0                	add    %eax,%eax
  80077c:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800780:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800783:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800786:	83 fa 09             	cmp    $0x9,%edx
  800789:	77 34                	ja     8007bf <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80078b:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80078c:	eb e9                	jmp    800777 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80078e:	8b 45 14             	mov    0x14(%ebp),%eax
  800791:	8d 48 04             	lea    0x4(%eax),%ecx
  800794:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800797:	8b 00                	mov    (%eax),%eax
  800799:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80079c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80079f:	eb 24                	jmp    8007c5 <vprintfmt+0xd9>
  8007a1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8007a5:	79 07                	jns    8007ae <vprintfmt+0xc2>
  8007a7:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007ae:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007b1:	eb 90                	jmp    800743 <vprintfmt+0x57>
  8007b3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8007b6:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8007bd:	eb 84                	jmp    800743 <vprintfmt+0x57>
  8007bf:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8007c2:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8007c5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8007c9:	0f 89 74 ff ff ff    	jns    800743 <vprintfmt+0x57>
				width = precision, precision = -1;
  8007cf:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8007d2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8007d5:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8007dc:	e9 62 ff ff ff       	jmp    800743 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8007e1:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8007e5:	e9 59 ff ff ff       	jmp    800743 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8007ea:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ed:	8d 50 04             	lea    0x4(%eax),%edx
  8007f0:	89 55 14             	mov    %edx,0x14(%ebp)
  8007f3:	83 ec 08             	sub    $0x8,%esp
  8007f6:	53                   	push   %ebx
  8007f7:	ff 30                	pushl  (%eax)
  8007f9:	ff d6                	call   *%esi
			break;
  8007fb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800801:	e9 0c ff ff ff       	jmp    800712 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800806:	8b 45 14             	mov    0x14(%ebp),%eax
  800809:	8d 50 04             	lea    0x4(%eax),%edx
  80080c:	89 55 14             	mov    %edx,0x14(%ebp)
  80080f:	8b 00                	mov    (%eax),%eax
  800811:	85 c0                	test   %eax,%eax
  800813:	79 02                	jns    800817 <vprintfmt+0x12b>
  800815:	f7 d8                	neg    %eax
  800817:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800819:	83 f8 0f             	cmp    $0xf,%eax
  80081c:	7f 0b                	jg     800829 <vprintfmt+0x13d>
  80081e:	8b 04 85 a0 2d 80 00 	mov    0x802da0(,%eax,4),%eax
  800825:	85 c0                	test   %eax,%eax
  800827:	75 18                	jne    800841 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800829:	52                   	push   %edx
  80082a:	68 07 2b 80 00       	push   $0x802b07
  80082f:	53                   	push   %ebx
  800830:	56                   	push   %esi
  800831:	e8 99 fe ff ff       	call   8006cf <printfmt>
  800836:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800839:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80083c:	e9 d1 fe ff ff       	jmp    800712 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800841:	50                   	push   %eax
  800842:	68 05 30 80 00       	push   $0x803005
  800847:	53                   	push   %ebx
  800848:	56                   	push   %esi
  800849:	e8 81 fe ff ff       	call   8006cf <printfmt>
  80084e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800851:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800854:	e9 b9 fe ff ff       	jmp    800712 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800859:	8b 45 14             	mov    0x14(%ebp),%eax
  80085c:	8d 50 04             	lea    0x4(%eax),%edx
  80085f:	89 55 14             	mov    %edx,0x14(%ebp)
  800862:	8b 38                	mov    (%eax),%edi
  800864:	85 ff                	test   %edi,%edi
  800866:	75 05                	jne    80086d <vprintfmt+0x181>
				p = "(null)";
  800868:	bf 00 2b 80 00       	mov    $0x802b00,%edi
			if (width > 0 && padc != '-')
  80086d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800871:	0f 8e 90 00 00 00    	jle    800907 <vprintfmt+0x21b>
  800877:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80087b:	0f 84 8e 00 00 00    	je     80090f <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800881:	83 ec 08             	sub    $0x8,%esp
  800884:	ff 75 d0             	pushl  -0x30(%ebp)
  800887:	57                   	push   %edi
  800888:	e8 70 02 00 00       	call   800afd <strnlen>
  80088d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800890:	29 c1                	sub    %eax,%ecx
  800892:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800895:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800898:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80089c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80089f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8008a2:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8008a4:	eb 0d                	jmp    8008b3 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8008a6:	83 ec 08             	sub    $0x8,%esp
  8008a9:	53                   	push   %ebx
  8008aa:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008ad:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8008af:	4f                   	dec    %edi
  8008b0:	83 c4 10             	add    $0x10,%esp
  8008b3:	85 ff                	test   %edi,%edi
  8008b5:	7f ef                	jg     8008a6 <vprintfmt+0x1ba>
  8008b7:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8008ba:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8008bd:	89 c8                	mov    %ecx,%eax
  8008bf:	85 c9                	test   %ecx,%ecx
  8008c1:	79 05                	jns    8008c8 <vprintfmt+0x1dc>
  8008c3:	b8 00 00 00 00       	mov    $0x0,%eax
  8008c8:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8008cb:	29 c1                	sub    %eax,%ecx
  8008cd:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8008d0:	89 75 08             	mov    %esi,0x8(%ebp)
  8008d3:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8008d6:	eb 3d                	jmp    800915 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8008d8:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8008dc:	74 19                	je     8008f7 <vprintfmt+0x20b>
  8008de:	0f be c0             	movsbl %al,%eax
  8008e1:	83 e8 20             	sub    $0x20,%eax
  8008e4:	83 f8 5e             	cmp    $0x5e,%eax
  8008e7:	76 0e                	jbe    8008f7 <vprintfmt+0x20b>
					putch('?', putdat);
  8008e9:	83 ec 08             	sub    $0x8,%esp
  8008ec:	53                   	push   %ebx
  8008ed:	6a 3f                	push   $0x3f
  8008ef:	ff 55 08             	call   *0x8(%ebp)
  8008f2:	83 c4 10             	add    $0x10,%esp
  8008f5:	eb 0b                	jmp    800902 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8008f7:	83 ec 08             	sub    $0x8,%esp
  8008fa:	53                   	push   %ebx
  8008fb:	52                   	push   %edx
  8008fc:	ff 55 08             	call   *0x8(%ebp)
  8008ff:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800902:	ff 4d e4             	decl   -0x1c(%ebp)
  800905:	eb 0e                	jmp    800915 <vprintfmt+0x229>
  800907:	89 75 08             	mov    %esi,0x8(%ebp)
  80090a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80090d:	eb 06                	jmp    800915 <vprintfmt+0x229>
  80090f:	89 75 08             	mov    %esi,0x8(%ebp)
  800912:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800915:	47                   	inc    %edi
  800916:	8a 47 ff             	mov    -0x1(%edi),%al
  800919:	0f be d0             	movsbl %al,%edx
  80091c:	85 d2                	test   %edx,%edx
  80091e:	74 1d                	je     80093d <vprintfmt+0x251>
  800920:	85 f6                	test   %esi,%esi
  800922:	78 b4                	js     8008d8 <vprintfmt+0x1ec>
  800924:	4e                   	dec    %esi
  800925:	79 b1                	jns    8008d8 <vprintfmt+0x1ec>
  800927:	8b 75 08             	mov    0x8(%ebp),%esi
  80092a:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80092d:	eb 14                	jmp    800943 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80092f:	83 ec 08             	sub    $0x8,%esp
  800932:	53                   	push   %ebx
  800933:	6a 20                	push   $0x20
  800935:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800937:	4f                   	dec    %edi
  800938:	83 c4 10             	add    $0x10,%esp
  80093b:	eb 06                	jmp    800943 <vprintfmt+0x257>
  80093d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800940:	8b 75 08             	mov    0x8(%ebp),%esi
  800943:	85 ff                	test   %edi,%edi
  800945:	7f e8                	jg     80092f <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800947:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80094a:	e9 c3 fd ff ff       	jmp    800712 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80094f:	83 fa 01             	cmp    $0x1,%edx
  800952:	7e 16                	jle    80096a <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800954:	8b 45 14             	mov    0x14(%ebp),%eax
  800957:	8d 50 08             	lea    0x8(%eax),%edx
  80095a:	89 55 14             	mov    %edx,0x14(%ebp)
  80095d:	8b 50 04             	mov    0x4(%eax),%edx
  800960:	8b 00                	mov    (%eax),%eax
  800962:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800965:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800968:	eb 32                	jmp    80099c <vprintfmt+0x2b0>
	else if (lflag)
  80096a:	85 d2                	test   %edx,%edx
  80096c:	74 18                	je     800986 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80096e:	8b 45 14             	mov    0x14(%ebp),%eax
  800971:	8d 50 04             	lea    0x4(%eax),%edx
  800974:	89 55 14             	mov    %edx,0x14(%ebp)
  800977:	8b 00                	mov    (%eax),%eax
  800979:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80097c:	89 c1                	mov    %eax,%ecx
  80097e:	c1 f9 1f             	sar    $0x1f,%ecx
  800981:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800984:	eb 16                	jmp    80099c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800986:	8b 45 14             	mov    0x14(%ebp),%eax
  800989:	8d 50 04             	lea    0x4(%eax),%edx
  80098c:	89 55 14             	mov    %edx,0x14(%ebp)
  80098f:	8b 00                	mov    (%eax),%eax
  800991:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800994:	89 c1                	mov    %eax,%ecx
  800996:	c1 f9 1f             	sar    $0x1f,%ecx
  800999:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80099c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80099f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8009a2:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8009a6:	79 76                	jns    800a1e <vprintfmt+0x332>
				putch('-', putdat);
  8009a8:	83 ec 08             	sub    $0x8,%esp
  8009ab:	53                   	push   %ebx
  8009ac:	6a 2d                	push   $0x2d
  8009ae:	ff d6                	call   *%esi
				num = -(long long) num;
  8009b0:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8009b3:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8009b6:	f7 d8                	neg    %eax
  8009b8:	83 d2 00             	adc    $0x0,%edx
  8009bb:	f7 da                	neg    %edx
  8009bd:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8009c0:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8009c5:	eb 5c                	jmp    800a23 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8009c7:	8d 45 14             	lea    0x14(%ebp),%eax
  8009ca:	e8 aa fc ff ff       	call   800679 <getuint>
			base = 10;
  8009cf:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8009d4:	eb 4d                	jmp    800a23 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8009d6:	8d 45 14             	lea    0x14(%ebp),%eax
  8009d9:	e8 9b fc ff ff       	call   800679 <getuint>
			base = 8;
  8009de:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8009e3:	eb 3e                	jmp    800a23 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8009e5:	83 ec 08             	sub    $0x8,%esp
  8009e8:	53                   	push   %ebx
  8009e9:	6a 30                	push   $0x30
  8009eb:	ff d6                	call   *%esi
			putch('x', putdat);
  8009ed:	83 c4 08             	add    $0x8,%esp
  8009f0:	53                   	push   %ebx
  8009f1:	6a 78                	push   $0x78
  8009f3:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8009f5:	8b 45 14             	mov    0x14(%ebp),%eax
  8009f8:	8d 50 04             	lea    0x4(%eax),%edx
  8009fb:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8009fe:	8b 00                	mov    (%eax),%eax
  800a00:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800a05:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800a08:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800a0d:	eb 14                	jmp    800a23 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800a0f:	8d 45 14             	lea    0x14(%ebp),%eax
  800a12:	e8 62 fc ff ff       	call   800679 <getuint>
			base = 16;
  800a17:	b9 10 00 00 00       	mov    $0x10,%ecx
  800a1c:	eb 05                	jmp    800a23 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800a1e:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800a23:	83 ec 0c             	sub    $0xc,%esp
  800a26:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800a2a:	57                   	push   %edi
  800a2b:	ff 75 e4             	pushl  -0x1c(%ebp)
  800a2e:	51                   	push   %ecx
  800a2f:	52                   	push   %edx
  800a30:	50                   	push   %eax
  800a31:	89 da                	mov    %ebx,%edx
  800a33:	89 f0                	mov    %esi,%eax
  800a35:	e8 92 fb ff ff       	call   8005cc <printnum>
			break;
  800a3a:	83 c4 20             	add    $0x20,%esp
  800a3d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800a40:	e9 cd fc ff ff       	jmp    800712 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800a45:	83 ec 08             	sub    $0x8,%esp
  800a48:	53                   	push   %ebx
  800a49:	51                   	push   %ecx
  800a4a:	ff d6                	call   *%esi
			break;
  800a4c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800a4f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800a52:	e9 bb fc ff ff       	jmp    800712 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800a57:	83 ec 08             	sub    $0x8,%esp
  800a5a:	53                   	push   %ebx
  800a5b:	6a 25                	push   $0x25
  800a5d:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800a5f:	83 c4 10             	add    $0x10,%esp
  800a62:	eb 01                	jmp    800a65 <vprintfmt+0x379>
  800a64:	4f                   	dec    %edi
  800a65:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800a69:	75 f9                	jne    800a64 <vprintfmt+0x378>
  800a6b:	e9 a2 fc ff ff       	jmp    800712 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800a70:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a73:	5b                   	pop    %ebx
  800a74:	5e                   	pop    %esi
  800a75:	5f                   	pop    %edi
  800a76:	5d                   	pop    %ebp
  800a77:	c3                   	ret    

00800a78 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800a78:	55                   	push   %ebp
  800a79:	89 e5                	mov    %esp,%ebp
  800a7b:	83 ec 18             	sub    $0x18,%esp
  800a7e:	8b 45 08             	mov    0x8(%ebp),%eax
  800a81:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800a84:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800a87:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800a8b:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800a8e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800a95:	85 c0                	test   %eax,%eax
  800a97:	74 26                	je     800abf <vsnprintf+0x47>
  800a99:	85 d2                	test   %edx,%edx
  800a9b:	7e 29                	jle    800ac6 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800a9d:	ff 75 14             	pushl  0x14(%ebp)
  800aa0:	ff 75 10             	pushl  0x10(%ebp)
  800aa3:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800aa6:	50                   	push   %eax
  800aa7:	68 b3 06 80 00       	push   $0x8006b3
  800aac:	e8 3b fc ff ff       	call   8006ec <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800ab1:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800ab4:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800ab7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800aba:	83 c4 10             	add    $0x10,%esp
  800abd:	eb 0c                	jmp    800acb <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800abf:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800ac4:	eb 05                	jmp    800acb <vsnprintf+0x53>
  800ac6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800acb:	c9                   	leave  
  800acc:	c3                   	ret    

00800acd <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800acd:	55                   	push   %ebp
  800ace:	89 e5                	mov    %esp,%ebp
  800ad0:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800ad3:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800ad6:	50                   	push   %eax
  800ad7:	ff 75 10             	pushl  0x10(%ebp)
  800ada:	ff 75 0c             	pushl  0xc(%ebp)
  800add:	ff 75 08             	pushl  0x8(%ebp)
  800ae0:	e8 93 ff ff ff       	call   800a78 <vsnprintf>
	va_end(ap);

	return rc;
}
  800ae5:	c9                   	leave  
  800ae6:	c3                   	ret    

00800ae7 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800ae7:	55                   	push   %ebp
  800ae8:	89 e5                	mov    %esp,%ebp
  800aea:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800aed:	b8 00 00 00 00       	mov    $0x0,%eax
  800af2:	eb 01                	jmp    800af5 <strlen+0xe>
		n++;
  800af4:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800af5:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800af9:	75 f9                	jne    800af4 <strlen+0xd>
		n++;
	return n;
}
  800afb:	5d                   	pop    %ebp
  800afc:	c3                   	ret    

00800afd <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800afd:	55                   	push   %ebp
  800afe:	89 e5                	mov    %esp,%ebp
  800b00:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800b03:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800b06:	ba 00 00 00 00       	mov    $0x0,%edx
  800b0b:	eb 01                	jmp    800b0e <strnlen+0x11>
		n++;
  800b0d:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800b0e:	39 c2                	cmp    %eax,%edx
  800b10:	74 08                	je     800b1a <strnlen+0x1d>
  800b12:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800b16:	75 f5                	jne    800b0d <strnlen+0x10>
  800b18:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800b1a:	5d                   	pop    %ebp
  800b1b:	c3                   	ret    

00800b1c <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800b1c:	55                   	push   %ebp
  800b1d:	89 e5                	mov    %esp,%ebp
  800b1f:	53                   	push   %ebx
  800b20:	8b 45 08             	mov    0x8(%ebp),%eax
  800b23:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800b26:	89 c2                	mov    %eax,%edx
  800b28:	42                   	inc    %edx
  800b29:	41                   	inc    %ecx
  800b2a:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800b2d:	88 5a ff             	mov    %bl,-0x1(%edx)
  800b30:	84 db                	test   %bl,%bl
  800b32:	75 f4                	jne    800b28 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800b34:	5b                   	pop    %ebx
  800b35:	5d                   	pop    %ebp
  800b36:	c3                   	ret    

00800b37 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800b37:	55                   	push   %ebp
  800b38:	89 e5                	mov    %esp,%ebp
  800b3a:	53                   	push   %ebx
  800b3b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800b3e:	53                   	push   %ebx
  800b3f:	e8 a3 ff ff ff       	call   800ae7 <strlen>
  800b44:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800b47:	ff 75 0c             	pushl  0xc(%ebp)
  800b4a:	01 d8                	add    %ebx,%eax
  800b4c:	50                   	push   %eax
  800b4d:	e8 ca ff ff ff       	call   800b1c <strcpy>
	return dst;
}
  800b52:	89 d8                	mov    %ebx,%eax
  800b54:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800b57:	c9                   	leave  
  800b58:	c3                   	ret    

00800b59 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800b59:	55                   	push   %ebp
  800b5a:	89 e5                	mov    %esp,%ebp
  800b5c:	56                   	push   %esi
  800b5d:	53                   	push   %ebx
  800b5e:	8b 75 08             	mov    0x8(%ebp),%esi
  800b61:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b64:	89 f3                	mov    %esi,%ebx
  800b66:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800b69:	89 f2                	mov    %esi,%edx
  800b6b:	eb 0c                	jmp    800b79 <strncpy+0x20>
		*dst++ = *src;
  800b6d:	42                   	inc    %edx
  800b6e:	8a 01                	mov    (%ecx),%al
  800b70:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800b73:	80 39 01             	cmpb   $0x1,(%ecx)
  800b76:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800b79:	39 da                	cmp    %ebx,%edx
  800b7b:	75 f0                	jne    800b6d <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800b7d:	89 f0                	mov    %esi,%eax
  800b7f:	5b                   	pop    %ebx
  800b80:	5e                   	pop    %esi
  800b81:	5d                   	pop    %ebp
  800b82:	c3                   	ret    

00800b83 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800b83:	55                   	push   %ebp
  800b84:	89 e5                	mov    %esp,%ebp
  800b86:	56                   	push   %esi
  800b87:	53                   	push   %ebx
  800b88:	8b 75 08             	mov    0x8(%ebp),%esi
  800b8b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b8e:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800b91:	85 c0                	test   %eax,%eax
  800b93:	74 1e                	je     800bb3 <strlcpy+0x30>
  800b95:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800b99:	89 f2                	mov    %esi,%edx
  800b9b:	eb 05                	jmp    800ba2 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800b9d:	42                   	inc    %edx
  800b9e:	41                   	inc    %ecx
  800b9f:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800ba2:	39 c2                	cmp    %eax,%edx
  800ba4:	74 08                	je     800bae <strlcpy+0x2b>
  800ba6:	8a 19                	mov    (%ecx),%bl
  800ba8:	84 db                	test   %bl,%bl
  800baa:	75 f1                	jne    800b9d <strlcpy+0x1a>
  800bac:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800bae:	c6 00 00             	movb   $0x0,(%eax)
  800bb1:	eb 02                	jmp    800bb5 <strlcpy+0x32>
  800bb3:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800bb5:	29 f0                	sub    %esi,%eax
}
  800bb7:	5b                   	pop    %ebx
  800bb8:	5e                   	pop    %esi
  800bb9:	5d                   	pop    %ebp
  800bba:	c3                   	ret    

00800bbb <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800bbb:	55                   	push   %ebp
  800bbc:	89 e5                	mov    %esp,%ebp
  800bbe:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800bc1:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800bc4:	eb 02                	jmp    800bc8 <strcmp+0xd>
		p++, q++;
  800bc6:	41                   	inc    %ecx
  800bc7:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800bc8:	8a 01                	mov    (%ecx),%al
  800bca:	84 c0                	test   %al,%al
  800bcc:	74 04                	je     800bd2 <strcmp+0x17>
  800bce:	3a 02                	cmp    (%edx),%al
  800bd0:	74 f4                	je     800bc6 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800bd2:	0f b6 c0             	movzbl %al,%eax
  800bd5:	0f b6 12             	movzbl (%edx),%edx
  800bd8:	29 d0                	sub    %edx,%eax
}
  800bda:	5d                   	pop    %ebp
  800bdb:	c3                   	ret    

00800bdc <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800bdc:	55                   	push   %ebp
  800bdd:	89 e5                	mov    %esp,%ebp
  800bdf:	53                   	push   %ebx
  800be0:	8b 45 08             	mov    0x8(%ebp),%eax
  800be3:	8b 55 0c             	mov    0xc(%ebp),%edx
  800be6:	89 c3                	mov    %eax,%ebx
  800be8:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800beb:	eb 02                	jmp    800bef <strncmp+0x13>
		n--, p++, q++;
  800bed:	40                   	inc    %eax
  800bee:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800bef:	39 d8                	cmp    %ebx,%eax
  800bf1:	74 14                	je     800c07 <strncmp+0x2b>
  800bf3:	8a 08                	mov    (%eax),%cl
  800bf5:	84 c9                	test   %cl,%cl
  800bf7:	74 04                	je     800bfd <strncmp+0x21>
  800bf9:	3a 0a                	cmp    (%edx),%cl
  800bfb:	74 f0                	je     800bed <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800bfd:	0f b6 00             	movzbl (%eax),%eax
  800c00:	0f b6 12             	movzbl (%edx),%edx
  800c03:	29 d0                	sub    %edx,%eax
  800c05:	eb 05                	jmp    800c0c <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800c07:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800c0c:	5b                   	pop    %ebx
  800c0d:	5d                   	pop    %ebp
  800c0e:	c3                   	ret    

00800c0f <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800c0f:	55                   	push   %ebp
  800c10:	89 e5                	mov    %esp,%ebp
  800c12:	8b 45 08             	mov    0x8(%ebp),%eax
  800c15:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800c18:	eb 05                	jmp    800c1f <strchr+0x10>
		if (*s == c)
  800c1a:	38 ca                	cmp    %cl,%dl
  800c1c:	74 0c                	je     800c2a <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800c1e:	40                   	inc    %eax
  800c1f:	8a 10                	mov    (%eax),%dl
  800c21:	84 d2                	test   %dl,%dl
  800c23:	75 f5                	jne    800c1a <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800c25:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c2a:	5d                   	pop    %ebp
  800c2b:	c3                   	ret    

00800c2c <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800c2c:	55                   	push   %ebp
  800c2d:	89 e5                	mov    %esp,%ebp
  800c2f:	8b 45 08             	mov    0x8(%ebp),%eax
  800c32:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800c35:	eb 05                	jmp    800c3c <strfind+0x10>
		if (*s == c)
  800c37:	38 ca                	cmp    %cl,%dl
  800c39:	74 07                	je     800c42 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800c3b:	40                   	inc    %eax
  800c3c:	8a 10                	mov    (%eax),%dl
  800c3e:	84 d2                	test   %dl,%dl
  800c40:	75 f5                	jne    800c37 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800c42:	5d                   	pop    %ebp
  800c43:	c3                   	ret    

00800c44 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800c44:	55                   	push   %ebp
  800c45:	89 e5                	mov    %esp,%ebp
  800c47:	57                   	push   %edi
  800c48:	56                   	push   %esi
  800c49:	53                   	push   %ebx
  800c4a:	8b 7d 08             	mov    0x8(%ebp),%edi
  800c4d:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800c50:	85 c9                	test   %ecx,%ecx
  800c52:	74 36                	je     800c8a <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800c54:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800c5a:	75 28                	jne    800c84 <memset+0x40>
  800c5c:	f6 c1 03             	test   $0x3,%cl
  800c5f:	75 23                	jne    800c84 <memset+0x40>
		c &= 0xFF;
  800c61:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800c65:	89 d3                	mov    %edx,%ebx
  800c67:	c1 e3 08             	shl    $0x8,%ebx
  800c6a:	89 d6                	mov    %edx,%esi
  800c6c:	c1 e6 18             	shl    $0x18,%esi
  800c6f:	89 d0                	mov    %edx,%eax
  800c71:	c1 e0 10             	shl    $0x10,%eax
  800c74:	09 f0                	or     %esi,%eax
  800c76:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800c78:	89 d8                	mov    %ebx,%eax
  800c7a:	09 d0                	or     %edx,%eax
  800c7c:	c1 e9 02             	shr    $0x2,%ecx
  800c7f:	fc                   	cld    
  800c80:	f3 ab                	rep stos %eax,%es:(%edi)
  800c82:	eb 06                	jmp    800c8a <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800c84:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c87:	fc                   	cld    
  800c88:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800c8a:	89 f8                	mov    %edi,%eax
  800c8c:	5b                   	pop    %ebx
  800c8d:	5e                   	pop    %esi
  800c8e:	5f                   	pop    %edi
  800c8f:	5d                   	pop    %ebp
  800c90:	c3                   	ret    

00800c91 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800c91:	55                   	push   %ebp
  800c92:	89 e5                	mov    %esp,%ebp
  800c94:	57                   	push   %edi
  800c95:	56                   	push   %esi
  800c96:	8b 45 08             	mov    0x8(%ebp),%eax
  800c99:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c9c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800c9f:	39 c6                	cmp    %eax,%esi
  800ca1:	73 33                	jae    800cd6 <memmove+0x45>
  800ca3:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800ca6:	39 d0                	cmp    %edx,%eax
  800ca8:	73 2c                	jae    800cd6 <memmove+0x45>
		s += n;
		d += n;
  800caa:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800cad:	89 d6                	mov    %edx,%esi
  800caf:	09 fe                	or     %edi,%esi
  800cb1:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800cb7:	75 13                	jne    800ccc <memmove+0x3b>
  800cb9:	f6 c1 03             	test   $0x3,%cl
  800cbc:	75 0e                	jne    800ccc <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800cbe:	83 ef 04             	sub    $0x4,%edi
  800cc1:	8d 72 fc             	lea    -0x4(%edx),%esi
  800cc4:	c1 e9 02             	shr    $0x2,%ecx
  800cc7:	fd                   	std    
  800cc8:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800cca:	eb 07                	jmp    800cd3 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800ccc:	4f                   	dec    %edi
  800ccd:	8d 72 ff             	lea    -0x1(%edx),%esi
  800cd0:	fd                   	std    
  800cd1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800cd3:	fc                   	cld    
  800cd4:	eb 1d                	jmp    800cf3 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800cd6:	89 f2                	mov    %esi,%edx
  800cd8:	09 c2                	or     %eax,%edx
  800cda:	f6 c2 03             	test   $0x3,%dl
  800cdd:	75 0f                	jne    800cee <memmove+0x5d>
  800cdf:	f6 c1 03             	test   $0x3,%cl
  800ce2:	75 0a                	jne    800cee <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800ce4:	c1 e9 02             	shr    $0x2,%ecx
  800ce7:	89 c7                	mov    %eax,%edi
  800ce9:	fc                   	cld    
  800cea:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800cec:	eb 05                	jmp    800cf3 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800cee:	89 c7                	mov    %eax,%edi
  800cf0:	fc                   	cld    
  800cf1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800cf3:	5e                   	pop    %esi
  800cf4:	5f                   	pop    %edi
  800cf5:	5d                   	pop    %ebp
  800cf6:	c3                   	ret    

00800cf7 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800cf7:	55                   	push   %ebp
  800cf8:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800cfa:	ff 75 10             	pushl  0x10(%ebp)
  800cfd:	ff 75 0c             	pushl  0xc(%ebp)
  800d00:	ff 75 08             	pushl  0x8(%ebp)
  800d03:	e8 89 ff ff ff       	call   800c91 <memmove>
}
  800d08:	c9                   	leave  
  800d09:	c3                   	ret    

00800d0a <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800d0a:	55                   	push   %ebp
  800d0b:	89 e5                	mov    %esp,%ebp
  800d0d:	56                   	push   %esi
  800d0e:	53                   	push   %ebx
  800d0f:	8b 45 08             	mov    0x8(%ebp),%eax
  800d12:	8b 55 0c             	mov    0xc(%ebp),%edx
  800d15:	89 c6                	mov    %eax,%esi
  800d17:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800d1a:	eb 14                	jmp    800d30 <memcmp+0x26>
		if (*s1 != *s2)
  800d1c:	8a 08                	mov    (%eax),%cl
  800d1e:	8a 1a                	mov    (%edx),%bl
  800d20:	38 d9                	cmp    %bl,%cl
  800d22:	74 0a                	je     800d2e <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800d24:	0f b6 c1             	movzbl %cl,%eax
  800d27:	0f b6 db             	movzbl %bl,%ebx
  800d2a:	29 d8                	sub    %ebx,%eax
  800d2c:	eb 0b                	jmp    800d39 <memcmp+0x2f>
		s1++, s2++;
  800d2e:	40                   	inc    %eax
  800d2f:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800d30:	39 f0                	cmp    %esi,%eax
  800d32:	75 e8                	jne    800d1c <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800d34:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800d39:	5b                   	pop    %ebx
  800d3a:	5e                   	pop    %esi
  800d3b:	5d                   	pop    %ebp
  800d3c:	c3                   	ret    

00800d3d <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800d3d:	55                   	push   %ebp
  800d3e:	89 e5                	mov    %esp,%ebp
  800d40:	53                   	push   %ebx
  800d41:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800d44:	89 c1                	mov    %eax,%ecx
  800d46:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800d49:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800d4d:	eb 08                	jmp    800d57 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800d4f:	0f b6 10             	movzbl (%eax),%edx
  800d52:	39 da                	cmp    %ebx,%edx
  800d54:	74 05                	je     800d5b <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800d56:	40                   	inc    %eax
  800d57:	39 c8                	cmp    %ecx,%eax
  800d59:	72 f4                	jb     800d4f <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800d5b:	5b                   	pop    %ebx
  800d5c:	5d                   	pop    %ebp
  800d5d:	c3                   	ret    

00800d5e <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800d5e:	55                   	push   %ebp
  800d5f:	89 e5                	mov    %esp,%ebp
  800d61:	57                   	push   %edi
  800d62:	56                   	push   %esi
  800d63:	53                   	push   %ebx
  800d64:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d67:	eb 01                	jmp    800d6a <strtol+0xc>
		s++;
  800d69:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d6a:	8a 01                	mov    (%ecx),%al
  800d6c:	3c 20                	cmp    $0x20,%al
  800d6e:	74 f9                	je     800d69 <strtol+0xb>
  800d70:	3c 09                	cmp    $0x9,%al
  800d72:	74 f5                	je     800d69 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800d74:	3c 2b                	cmp    $0x2b,%al
  800d76:	75 08                	jne    800d80 <strtol+0x22>
		s++;
  800d78:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800d79:	bf 00 00 00 00       	mov    $0x0,%edi
  800d7e:	eb 11                	jmp    800d91 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800d80:	3c 2d                	cmp    $0x2d,%al
  800d82:	75 08                	jne    800d8c <strtol+0x2e>
		s++, neg = 1;
  800d84:	41                   	inc    %ecx
  800d85:	bf 01 00 00 00       	mov    $0x1,%edi
  800d8a:	eb 05                	jmp    800d91 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800d8c:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800d91:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d95:	0f 84 87 00 00 00    	je     800e22 <strtol+0xc4>
  800d9b:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800d9f:	75 27                	jne    800dc8 <strtol+0x6a>
  800da1:	80 39 30             	cmpb   $0x30,(%ecx)
  800da4:	75 22                	jne    800dc8 <strtol+0x6a>
  800da6:	e9 88 00 00 00       	jmp    800e33 <strtol+0xd5>
		s += 2, base = 16;
  800dab:	83 c1 02             	add    $0x2,%ecx
  800dae:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800db5:	eb 11                	jmp    800dc8 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800db7:	41                   	inc    %ecx
  800db8:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800dbf:	eb 07                	jmp    800dc8 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800dc1:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800dc8:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800dcd:	8a 11                	mov    (%ecx),%dl
  800dcf:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800dd2:	80 fb 09             	cmp    $0x9,%bl
  800dd5:	77 08                	ja     800ddf <strtol+0x81>
			dig = *s - '0';
  800dd7:	0f be d2             	movsbl %dl,%edx
  800dda:	83 ea 30             	sub    $0x30,%edx
  800ddd:	eb 22                	jmp    800e01 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800ddf:	8d 72 9f             	lea    -0x61(%edx),%esi
  800de2:	89 f3                	mov    %esi,%ebx
  800de4:	80 fb 19             	cmp    $0x19,%bl
  800de7:	77 08                	ja     800df1 <strtol+0x93>
			dig = *s - 'a' + 10;
  800de9:	0f be d2             	movsbl %dl,%edx
  800dec:	83 ea 57             	sub    $0x57,%edx
  800def:	eb 10                	jmp    800e01 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800df1:	8d 72 bf             	lea    -0x41(%edx),%esi
  800df4:	89 f3                	mov    %esi,%ebx
  800df6:	80 fb 19             	cmp    $0x19,%bl
  800df9:	77 14                	ja     800e0f <strtol+0xb1>
			dig = *s - 'A' + 10;
  800dfb:	0f be d2             	movsbl %dl,%edx
  800dfe:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800e01:	3b 55 10             	cmp    0x10(%ebp),%edx
  800e04:	7d 09                	jge    800e0f <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800e06:	41                   	inc    %ecx
  800e07:	0f af 45 10          	imul   0x10(%ebp),%eax
  800e0b:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800e0d:	eb be                	jmp    800dcd <strtol+0x6f>

	if (endptr)
  800e0f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800e13:	74 05                	je     800e1a <strtol+0xbc>
		*endptr = (char *) s;
  800e15:	8b 75 0c             	mov    0xc(%ebp),%esi
  800e18:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800e1a:	85 ff                	test   %edi,%edi
  800e1c:	74 21                	je     800e3f <strtol+0xe1>
  800e1e:	f7 d8                	neg    %eax
  800e20:	eb 1d                	jmp    800e3f <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800e22:	80 39 30             	cmpb   $0x30,(%ecx)
  800e25:	75 9a                	jne    800dc1 <strtol+0x63>
  800e27:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800e2b:	0f 84 7a ff ff ff    	je     800dab <strtol+0x4d>
  800e31:	eb 84                	jmp    800db7 <strtol+0x59>
  800e33:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800e37:	0f 84 6e ff ff ff    	je     800dab <strtol+0x4d>
  800e3d:	eb 89                	jmp    800dc8 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800e3f:	5b                   	pop    %ebx
  800e40:	5e                   	pop    %esi
  800e41:	5f                   	pop    %edi
  800e42:	5d                   	pop    %ebp
  800e43:	c3                   	ret    

00800e44 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800e44:	55                   	push   %ebp
  800e45:	89 e5                	mov    %esp,%ebp
  800e47:	57                   	push   %edi
  800e48:	56                   	push   %esi
  800e49:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e4a:	b8 00 00 00 00       	mov    $0x0,%eax
  800e4f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e52:	8b 55 08             	mov    0x8(%ebp),%edx
  800e55:	89 c3                	mov    %eax,%ebx
  800e57:	89 c7                	mov    %eax,%edi
  800e59:	89 c6                	mov    %eax,%esi
  800e5b:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800e5d:	5b                   	pop    %ebx
  800e5e:	5e                   	pop    %esi
  800e5f:	5f                   	pop    %edi
  800e60:	5d                   	pop    %ebp
  800e61:	c3                   	ret    

00800e62 <sys_cgetc>:

int
sys_cgetc(void)
{
  800e62:	55                   	push   %ebp
  800e63:	89 e5                	mov    %esp,%ebp
  800e65:	57                   	push   %edi
  800e66:	56                   	push   %esi
  800e67:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e68:	ba 00 00 00 00       	mov    $0x0,%edx
  800e6d:	b8 01 00 00 00       	mov    $0x1,%eax
  800e72:	89 d1                	mov    %edx,%ecx
  800e74:	89 d3                	mov    %edx,%ebx
  800e76:	89 d7                	mov    %edx,%edi
  800e78:	89 d6                	mov    %edx,%esi
  800e7a:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800e7c:	5b                   	pop    %ebx
  800e7d:	5e                   	pop    %esi
  800e7e:	5f                   	pop    %edi
  800e7f:	5d                   	pop    %ebp
  800e80:	c3                   	ret    

00800e81 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800e81:	55                   	push   %ebp
  800e82:	89 e5                	mov    %esp,%ebp
  800e84:	57                   	push   %edi
  800e85:	56                   	push   %esi
  800e86:	53                   	push   %ebx
  800e87:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e8a:	b9 00 00 00 00       	mov    $0x0,%ecx
  800e8f:	b8 03 00 00 00       	mov    $0x3,%eax
  800e94:	8b 55 08             	mov    0x8(%ebp),%edx
  800e97:	89 cb                	mov    %ecx,%ebx
  800e99:	89 cf                	mov    %ecx,%edi
  800e9b:	89 ce                	mov    %ecx,%esi
  800e9d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e9f:	85 c0                	test   %eax,%eax
  800ea1:	7e 17                	jle    800eba <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ea3:	83 ec 0c             	sub    $0xc,%esp
  800ea6:	50                   	push   %eax
  800ea7:	6a 03                	push   $0x3
  800ea9:	68 ff 2d 80 00       	push   $0x802dff
  800eae:	6a 23                	push   $0x23
  800eb0:	68 1c 2e 80 00       	push   $0x802e1c
  800eb5:	e8 26 f6 ff ff       	call   8004e0 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800eba:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ebd:	5b                   	pop    %ebx
  800ebe:	5e                   	pop    %esi
  800ebf:	5f                   	pop    %edi
  800ec0:	5d                   	pop    %ebp
  800ec1:	c3                   	ret    

00800ec2 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800ec2:	55                   	push   %ebp
  800ec3:	89 e5                	mov    %esp,%ebp
  800ec5:	57                   	push   %edi
  800ec6:	56                   	push   %esi
  800ec7:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ec8:	ba 00 00 00 00       	mov    $0x0,%edx
  800ecd:	b8 02 00 00 00       	mov    $0x2,%eax
  800ed2:	89 d1                	mov    %edx,%ecx
  800ed4:	89 d3                	mov    %edx,%ebx
  800ed6:	89 d7                	mov    %edx,%edi
  800ed8:	89 d6                	mov    %edx,%esi
  800eda:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800edc:	5b                   	pop    %ebx
  800edd:	5e                   	pop    %esi
  800ede:	5f                   	pop    %edi
  800edf:	5d                   	pop    %ebp
  800ee0:	c3                   	ret    

00800ee1 <sys_yield>:

void
sys_yield(void)
{
  800ee1:	55                   	push   %ebp
  800ee2:	89 e5                	mov    %esp,%ebp
  800ee4:	57                   	push   %edi
  800ee5:	56                   	push   %esi
  800ee6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ee7:	ba 00 00 00 00       	mov    $0x0,%edx
  800eec:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ef1:	89 d1                	mov    %edx,%ecx
  800ef3:	89 d3                	mov    %edx,%ebx
  800ef5:	89 d7                	mov    %edx,%edi
  800ef7:	89 d6                	mov    %edx,%esi
  800ef9:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800efb:	5b                   	pop    %ebx
  800efc:	5e                   	pop    %esi
  800efd:	5f                   	pop    %edi
  800efe:	5d                   	pop    %ebp
  800eff:	c3                   	ret    

00800f00 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800f00:	55                   	push   %ebp
  800f01:	89 e5                	mov    %esp,%ebp
  800f03:	57                   	push   %edi
  800f04:	56                   	push   %esi
  800f05:	53                   	push   %ebx
  800f06:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f09:	be 00 00 00 00       	mov    $0x0,%esi
  800f0e:	b8 04 00 00 00       	mov    $0x4,%eax
  800f13:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f16:	8b 55 08             	mov    0x8(%ebp),%edx
  800f19:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800f1c:	89 f7                	mov    %esi,%edi
  800f1e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f20:	85 c0                	test   %eax,%eax
  800f22:	7e 17                	jle    800f3b <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f24:	83 ec 0c             	sub    $0xc,%esp
  800f27:	50                   	push   %eax
  800f28:	6a 04                	push   $0x4
  800f2a:	68 ff 2d 80 00       	push   $0x802dff
  800f2f:	6a 23                	push   $0x23
  800f31:	68 1c 2e 80 00       	push   $0x802e1c
  800f36:	e8 a5 f5 ff ff       	call   8004e0 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800f3b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f3e:	5b                   	pop    %ebx
  800f3f:	5e                   	pop    %esi
  800f40:	5f                   	pop    %edi
  800f41:	5d                   	pop    %ebp
  800f42:	c3                   	ret    

00800f43 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800f43:	55                   	push   %ebp
  800f44:	89 e5                	mov    %esp,%ebp
  800f46:	57                   	push   %edi
  800f47:	56                   	push   %esi
  800f48:	53                   	push   %ebx
  800f49:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f4c:	b8 05 00 00 00       	mov    $0x5,%eax
  800f51:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f54:	8b 55 08             	mov    0x8(%ebp),%edx
  800f57:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800f5a:	8b 7d 14             	mov    0x14(%ebp),%edi
  800f5d:	8b 75 18             	mov    0x18(%ebp),%esi
  800f60:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f62:	85 c0                	test   %eax,%eax
  800f64:	7e 17                	jle    800f7d <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f66:	83 ec 0c             	sub    $0xc,%esp
  800f69:	50                   	push   %eax
  800f6a:	6a 05                	push   $0x5
  800f6c:	68 ff 2d 80 00       	push   $0x802dff
  800f71:	6a 23                	push   $0x23
  800f73:	68 1c 2e 80 00       	push   $0x802e1c
  800f78:	e8 63 f5 ff ff       	call   8004e0 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800f7d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f80:	5b                   	pop    %ebx
  800f81:	5e                   	pop    %esi
  800f82:	5f                   	pop    %edi
  800f83:	5d                   	pop    %ebp
  800f84:	c3                   	ret    

00800f85 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800f85:	55                   	push   %ebp
  800f86:	89 e5                	mov    %esp,%ebp
  800f88:	57                   	push   %edi
  800f89:	56                   	push   %esi
  800f8a:	53                   	push   %ebx
  800f8b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f8e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800f93:	b8 06 00 00 00       	mov    $0x6,%eax
  800f98:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f9b:	8b 55 08             	mov    0x8(%ebp),%edx
  800f9e:	89 df                	mov    %ebx,%edi
  800fa0:	89 de                	mov    %ebx,%esi
  800fa2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800fa4:	85 c0                	test   %eax,%eax
  800fa6:	7e 17                	jle    800fbf <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800fa8:	83 ec 0c             	sub    $0xc,%esp
  800fab:	50                   	push   %eax
  800fac:	6a 06                	push   $0x6
  800fae:	68 ff 2d 80 00       	push   $0x802dff
  800fb3:	6a 23                	push   $0x23
  800fb5:	68 1c 2e 80 00       	push   $0x802e1c
  800fba:	e8 21 f5 ff ff       	call   8004e0 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800fbf:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fc2:	5b                   	pop    %ebx
  800fc3:	5e                   	pop    %esi
  800fc4:	5f                   	pop    %edi
  800fc5:	5d                   	pop    %ebp
  800fc6:	c3                   	ret    

00800fc7 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800fc7:	55                   	push   %ebp
  800fc8:	89 e5                	mov    %esp,%ebp
  800fca:	57                   	push   %edi
  800fcb:	56                   	push   %esi
  800fcc:	53                   	push   %ebx
  800fcd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fd0:	bb 00 00 00 00       	mov    $0x0,%ebx
  800fd5:	b8 08 00 00 00       	mov    $0x8,%eax
  800fda:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800fdd:	8b 55 08             	mov    0x8(%ebp),%edx
  800fe0:	89 df                	mov    %ebx,%edi
  800fe2:	89 de                	mov    %ebx,%esi
  800fe4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800fe6:	85 c0                	test   %eax,%eax
  800fe8:	7e 17                	jle    801001 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800fea:	83 ec 0c             	sub    $0xc,%esp
  800fed:	50                   	push   %eax
  800fee:	6a 08                	push   $0x8
  800ff0:	68 ff 2d 80 00       	push   $0x802dff
  800ff5:	6a 23                	push   $0x23
  800ff7:	68 1c 2e 80 00       	push   $0x802e1c
  800ffc:	e8 df f4 ff ff       	call   8004e0 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  801001:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801004:	5b                   	pop    %ebx
  801005:	5e                   	pop    %esi
  801006:	5f                   	pop    %edi
  801007:	5d                   	pop    %ebp
  801008:	c3                   	ret    

00801009 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  801009:	55                   	push   %ebp
  80100a:	89 e5                	mov    %esp,%ebp
  80100c:	57                   	push   %edi
  80100d:	56                   	push   %esi
  80100e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80100f:	ba 00 00 00 00       	mov    $0x0,%edx
  801014:	b8 0c 00 00 00       	mov    $0xc,%eax
  801019:	89 d1                	mov    %edx,%ecx
  80101b:	89 d3                	mov    %edx,%ebx
  80101d:	89 d7                	mov    %edx,%edi
  80101f:	89 d6                	mov    %edx,%esi
  801021:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  801023:	5b                   	pop    %ebx
  801024:	5e                   	pop    %esi
  801025:	5f                   	pop    %edi
  801026:	5d                   	pop    %ebp
  801027:	c3                   	ret    

00801028 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  801028:	55                   	push   %ebp
  801029:	89 e5                	mov    %esp,%ebp
  80102b:	57                   	push   %edi
  80102c:	56                   	push   %esi
  80102d:	53                   	push   %ebx
  80102e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801031:	bb 00 00 00 00       	mov    $0x0,%ebx
  801036:	b8 09 00 00 00       	mov    $0x9,%eax
  80103b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80103e:	8b 55 08             	mov    0x8(%ebp),%edx
  801041:	89 df                	mov    %ebx,%edi
  801043:	89 de                	mov    %ebx,%esi
  801045:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801047:	85 c0                	test   %eax,%eax
  801049:	7e 17                	jle    801062 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80104b:	83 ec 0c             	sub    $0xc,%esp
  80104e:	50                   	push   %eax
  80104f:	6a 09                	push   $0x9
  801051:	68 ff 2d 80 00       	push   $0x802dff
  801056:	6a 23                	push   $0x23
  801058:	68 1c 2e 80 00       	push   $0x802e1c
  80105d:	e8 7e f4 ff ff       	call   8004e0 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  801062:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801065:	5b                   	pop    %ebx
  801066:	5e                   	pop    %esi
  801067:	5f                   	pop    %edi
  801068:	5d                   	pop    %ebp
  801069:	c3                   	ret    

0080106a <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80106a:	55                   	push   %ebp
  80106b:	89 e5                	mov    %esp,%ebp
  80106d:	57                   	push   %edi
  80106e:	56                   	push   %esi
  80106f:	53                   	push   %ebx
  801070:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801073:	bb 00 00 00 00       	mov    $0x0,%ebx
  801078:	b8 0a 00 00 00       	mov    $0xa,%eax
  80107d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801080:	8b 55 08             	mov    0x8(%ebp),%edx
  801083:	89 df                	mov    %ebx,%edi
  801085:	89 de                	mov    %ebx,%esi
  801087:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801089:	85 c0                	test   %eax,%eax
  80108b:	7e 17                	jle    8010a4 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80108d:	83 ec 0c             	sub    $0xc,%esp
  801090:	50                   	push   %eax
  801091:	6a 0a                	push   $0xa
  801093:	68 ff 2d 80 00       	push   $0x802dff
  801098:	6a 23                	push   $0x23
  80109a:	68 1c 2e 80 00       	push   $0x802e1c
  80109f:	e8 3c f4 ff ff       	call   8004e0 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8010a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010a7:	5b                   	pop    %ebx
  8010a8:	5e                   	pop    %esi
  8010a9:	5f                   	pop    %edi
  8010aa:	5d                   	pop    %ebp
  8010ab:	c3                   	ret    

008010ac <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8010ac:	55                   	push   %ebp
  8010ad:	89 e5                	mov    %esp,%ebp
  8010af:	57                   	push   %edi
  8010b0:	56                   	push   %esi
  8010b1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8010b2:	be 00 00 00 00       	mov    $0x0,%esi
  8010b7:	b8 0d 00 00 00       	mov    $0xd,%eax
  8010bc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8010bf:	8b 55 08             	mov    0x8(%ebp),%edx
  8010c2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8010c5:	8b 7d 14             	mov    0x14(%ebp),%edi
  8010c8:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8010ca:	5b                   	pop    %ebx
  8010cb:	5e                   	pop    %esi
  8010cc:	5f                   	pop    %edi
  8010cd:	5d                   	pop    %ebp
  8010ce:	c3                   	ret    

008010cf <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8010cf:	55                   	push   %ebp
  8010d0:	89 e5                	mov    %esp,%ebp
  8010d2:	57                   	push   %edi
  8010d3:	56                   	push   %esi
  8010d4:	53                   	push   %ebx
  8010d5:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8010d8:	b9 00 00 00 00       	mov    $0x0,%ecx
  8010dd:	b8 0e 00 00 00       	mov    $0xe,%eax
  8010e2:	8b 55 08             	mov    0x8(%ebp),%edx
  8010e5:	89 cb                	mov    %ecx,%ebx
  8010e7:	89 cf                	mov    %ecx,%edi
  8010e9:	89 ce                	mov    %ecx,%esi
  8010eb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8010ed:	85 c0                	test   %eax,%eax
  8010ef:	7e 17                	jle    801108 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8010f1:	83 ec 0c             	sub    $0xc,%esp
  8010f4:	50                   	push   %eax
  8010f5:	6a 0e                	push   $0xe
  8010f7:	68 ff 2d 80 00       	push   $0x802dff
  8010fc:	6a 23                	push   $0x23
  8010fe:	68 1c 2e 80 00       	push   $0x802e1c
  801103:	e8 d8 f3 ff ff       	call   8004e0 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  801108:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80110b:	5b                   	pop    %ebx
  80110c:	5e                   	pop    %esi
  80110d:	5f                   	pop    %edi
  80110e:	5d                   	pop    %ebp
  80110f:	c3                   	ret    

00801110 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  801110:	55                   	push   %ebp
  801111:	89 e5                	mov    %esp,%ebp
  801113:	53                   	push   %ebx
  801114:	83 ec 04             	sub    $0x4,%esp
  801117:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  80111a:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  80111c:	89 da                	mov    %ebx,%edx
  80111e:	c1 ea 0c             	shr    $0xc,%edx
  801121:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  801128:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  80112c:	74 05                	je     801133 <pgfault+0x23>
  80112e:	f6 c6 08             	test   $0x8,%dh
  801131:	75 14                	jne    801147 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  801133:	83 ec 04             	sub    $0x4,%esp
  801136:	68 2c 2e 80 00       	push   $0x802e2c
  80113b:	6a 26                	push   $0x26
  80113d:	68 90 2e 80 00       	push   $0x802e90
  801142:	e8 99 f3 ff ff       	call   8004e0 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  801147:	83 ec 04             	sub    $0x4,%esp
  80114a:	6a 07                	push   $0x7
  80114c:	68 00 f0 7f 00       	push   $0x7ff000
  801151:	6a 00                	push   $0x0
  801153:	e8 a8 fd ff ff       	call   800f00 <sys_page_alloc>
  801158:	83 c4 10             	add    $0x10,%esp
  80115b:	85 c0                	test   %eax,%eax
  80115d:	74 14                	je     801173 <pgfault+0x63>
        panic("pgfault: no phys mem");
  80115f:	83 ec 04             	sub    $0x4,%esp
  801162:	68 9b 2e 80 00       	push   $0x802e9b
  801167:	6a 32                	push   $0x32
  801169:	68 90 2e 80 00       	push   $0x802e90
  80116e:	e8 6d f3 ff ff       	call   8004e0 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  801173:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  801179:	83 ec 04             	sub    $0x4,%esp
  80117c:	68 00 10 00 00       	push   $0x1000
  801181:	53                   	push   %ebx
  801182:	68 00 f0 7f 00       	push   $0x7ff000
  801187:	e8 05 fb ff ff       	call   800c91 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  80118c:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  801193:	53                   	push   %ebx
  801194:	6a 00                	push   $0x0
  801196:	68 00 f0 7f 00       	push   $0x7ff000
  80119b:	6a 00                	push   $0x0
  80119d:	e8 a1 fd ff ff       	call   800f43 <sys_page_map>
  8011a2:	83 c4 20             	add    $0x20,%esp
  8011a5:	85 c0                	test   %eax,%eax
  8011a7:	74 14                	je     8011bd <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  8011a9:	83 ec 04             	sub    $0x4,%esp
  8011ac:	68 b0 2e 80 00       	push   $0x802eb0
  8011b1:	6a 3f                	push   $0x3f
  8011b3:	68 90 2e 80 00       	push   $0x802e90
  8011b8:	e8 23 f3 ff ff       	call   8004e0 <_panic>


	//panic("pgfault not implemented");
}
  8011bd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8011c0:	c9                   	leave  
  8011c1:	c3                   	ret    

008011c2 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  8011c2:	55                   	push   %ebp
  8011c3:	89 e5                	mov    %esp,%ebp
  8011c5:	57                   	push   %edi
  8011c6:	56                   	push   %esi
  8011c7:	53                   	push   %ebx
  8011c8:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  8011cb:	68 10 11 80 00       	push   $0x801110
  8011d0:	e8 74 13 00 00       	call   802549 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  8011d5:	b8 07 00 00 00       	mov    $0x7,%eax
  8011da:	cd 30                	int    $0x30
  8011dc:	89 45 dc             	mov    %eax,-0x24(%ebp)
  8011df:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  8011e2:	83 c4 10             	add    $0x10,%esp
  8011e5:	85 c0                	test   %eax,%eax
  8011e7:	79 17                	jns    801200 <fork+0x3e>
        panic("fork: cannot create child env");
  8011e9:	83 ec 04             	sub    $0x4,%esp
  8011ec:	68 c3 2e 80 00       	push   $0x802ec3
  8011f1:	68 97 00 00 00       	push   $0x97
  8011f6:	68 90 2e 80 00       	push   $0x802e90
  8011fb:	e8 e0 f2 ff ff       	call   8004e0 <_panic>
    } else if (envid == 0) {
  801200:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  801204:	75 2a                	jne    801230 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  801206:	e8 b7 fc ff ff       	call   800ec2 <sys_getenvid>
  80120b:	25 ff 03 00 00       	and    $0x3ff,%eax
  801210:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  801217:	c1 e0 07             	shl    $0x7,%eax
  80121a:	29 d0                	sub    %edx,%eax
  80121c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801221:	a3 04 50 80 00       	mov    %eax,0x805004
        return 0;
  801226:	b8 00 00 00 00       	mov    $0x0,%eax
  80122b:	e9 94 01 00 00       	jmp    8013c4 <fork+0x202>
  801230:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  801237:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80123a:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  801241:	a8 01                	test   $0x1,%al
  801243:	0f 84 0a 01 00 00    	je     801353 <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  801249:	c1 e7 0a             	shl    $0xa,%edi
  80124c:	be 00 00 00 00       	mov    $0x0,%esi
  801251:	89 fb                	mov    %edi,%ebx
  801253:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  801255:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  80125b:	75 34                	jne    801291 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  80125d:	83 ec 04             	sub    $0x4,%esp
  801260:	6a 07                	push   $0x7
  801262:	68 00 f0 bf ee       	push   $0xeebff000
  801267:	ff 75 e4             	pushl  -0x1c(%ebp)
  80126a:	e8 91 fc ff ff       	call   800f00 <sys_page_alloc>
  80126f:	83 c4 10             	add    $0x10,%esp
  801272:	85 c0                	test   %eax,%eax
  801274:	0f 84 cc 00 00 00    	je     801346 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  80127a:	83 ec 04             	sub    $0x4,%esp
  80127d:	68 e1 2e 80 00       	push   $0x802ee1
  801282:	68 ad 00 00 00       	push   $0xad
  801287:	68 90 2e 80 00       	push   $0x802e90
  80128c:	e8 4f f2 ff ff       	call   8004e0 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  801291:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  801298:	a8 01                	test   $0x1,%al
  80129a:	0f 84 a6 00 00 00    	je     801346 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  8012a0:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  8012a7:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  8012aa:	a9 02 08 00 00       	test   $0x802,%eax
  8012af:	74 62                	je     801313 <fork+0x151>
  8012b1:	f6 c4 04             	test   $0x4,%ah
  8012b4:	75 78                	jne    80132e <fork+0x16c>
        if (sys_page_map(0,
  8012b6:	83 ec 0c             	sub    $0xc,%esp
  8012b9:	68 05 08 00 00       	push   $0x805
  8012be:	53                   	push   %ebx
  8012bf:	ff 75 e4             	pushl  -0x1c(%ebp)
  8012c2:	53                   	push   %ebx
  8012c3:	6a 00                	push   $0x0
  8012c5:	e8 79 fc ff ff       	call   800f43 <sys_page_map>
  8012ca:	83 c4 20             	add    $0x20,%esp
  8012cd:	85 c0                	test   %eax,%eax
  8012cf:	74 14                	je     8012e5 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  8012d1:	83 ec 04             	sub    $0x4,%esp
  8012d4:	68 fc 2e 80 00       	push   $0x802efc
  8012d9:	6a 64                	push   $0x64
  8012db:	68 90 2e 80 00       	push   $0x802e90
  8012e0:	e8 fb f1 ff ff       	call   8004e0 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  8012e5:	83 ec 0c             	sub    $0xc,%esp
  8012e8:	68 05 08 00 00       	push   $0x805
  8012ed:	53                   	push   %ebx
  8012ee:	6a 00                	push   $0x0
  8012f0:	53                   	push   %ebx
  8012f1:	6a 00                	push   $0x0
  8012f3:	e8 4b fc ff ff       	call   800f43 <sys_page_map>
  8012f8:	83 c4 20             	add    $0x20,%esp
  8012fb:	85 c0                	test   %eax,%eax
  8012fd:	74 47                	je     801346 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  8012ff:	83 ec 04             	sub    $0x4,%esp
  801302:	68 13 2f 80 00       	push   $0x802f13
  801307:	6a 6d                	push   $0x6d
  801309:	68 90 2e 80 00       	push   $0x802e90
  80130e:	e8 cd f1 ff ff       	call   8004e0 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  801313:	f6 c4 04             	test   $0x4,%ah
  801316:	75 16                	jne    80132e <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  801318:	83 ec 0c             	sub    $0xc,%esp
  80131b:	6a 05                	push   $0x5
  80131d:	53                   	push   %ebx
  80131e:	ff 75 e4             	pushl  -0x1c(%ebp)
  801321:	53                   	push   %ebx
  801322:	6a 00                	push   $0x0
  801324:	e8 1a fc ff ff       	call   800f43 <sys_page_map>
  801329:	83 c4 20             	add    $0x20,%esp
  80132c:	eb 18                	jmp    801346 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  80132e:	83 ec 0c             	sub    $0xc,%esp
  801331:	25 07 0e 00 00       	and    $0xe07,%eax
  801336:	50                   	push   %eax
  801337:	53                   	push   %ebx
  801338:	ff 75 e4             	pushl  -0x1c(%ebp)
  80133b:	53                   	push   %ebx
  80133c:	6a 00                	push   $0x0
  80133e:	e8 00 fc ff ff       	call   800f43 <sys_page_map>
  801343:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801346:	46                   	inc    %esi
  801347:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  80134d:	0f 85 fe fe ff ff    	jne    801251 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801353:	ff 45 e0             	incl   -0x20(%ebp)
  801356:	8b 45 e0             	mov    -0x20(%ebp),%eax
  801359:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  80135e:	0f 85 d3 fe ff ff    	jne    801237 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  801364:	a1 04 50 80 00       	mov    0x805004,%eax
  801369:	8b 40 64             	mov    0x64(%eax),%eax
  80136c:	83 ec 08             	sub    $0x8,%esp
  80136f:	50                   	push   %eax
  801370:	ff 75 dc             	pushl  -0x24(%ebp)
  801373:	e8 f2 fc ff ff       	call   80106a <sys_env_set_pgfault_upcall>
  801378:	83 c4 10             	add    $0x10,%esp
  80137b:	85 c0                	test   %eax,%eax
  80137d:	74 17                	je     801396 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  80137f:	83 ec 04             	sub    $0x4,%esp
  801382:	68 70 2e 80 00       	push   $0x802e70
  801387:	68 b9 00 00 00       	push   $0xb9
  80138c:	68 90 2e 80 00       	push   $0x802e90
  801391:	e8 4a f1 ff ff       	call   8004e0 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  801396:	83 ec 08             	sub    $0x8,%esp
  801399:	6a 02                	push   $0x2
  80139b:	ff 75 dc             	pushl  -0x24(%ebp)
  80139e:	e8 24 fc ff ff       	call   800fc7 <sys_env_set_status>
  8013a3:	83 c4 10             	add    $0x10,%esp
  8013a6:	85 c0                	test   %eax,%eax
  8013a8:	74 17                	je     8013c1 <fork+0x1ff>
        panic("fork: cannot set env status");
  8013aa:	83 ec 04             	sub    $0x4,%esp
  8013ad:	68 2e 2f 80 00       	push   $0x802f2e
  8013b2:	68 bd 00 00 00       	push   $0xbd
  8013b7:	68 90 2e 80 00       	push   $0x802e90
  8013bc:	e8 1f f1 ff ff       	call   8004e0 <_panic>

    return envid;
  8013c1:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  8013c4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8013c7:	5b                   	pop    %ebx
  8013c8:	5e                   	pop    %esi
  8013c9:	5f                   	pop    %edi
  8013ca:	5d                   	pop    %ebp
  8013cb:	c3                   	ret    

008013cc <sfork>:

// Challenge!
int
sfork(void)
{
  8013cc:	55                   	push   %ebp
  8013cd:	89 e5                	mov    %esp,%ebp
  8013cf:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  8013d2:	68 4a 2f 80 00       	push   $0x802f4a
  8013d7:	68 c8 00 00 00       	push   $0xc8
  8013dc:	68 90 2e 80 00       	push   $0x802e90
  8013e1:	e8 fa f0 ff ff       	call   8004e0 <_panic>

008013e6 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  8013e6:	55                   	push   %ebp
  8013e7:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  8013e9:	8b 45 08             	mov    0x8(%ebp),%eax
  8013ec:	05 00 00 00 30       	add    $0x30000000,%eax
  8013f1:	c1 e8 0c             	shr    $0xc,%eax
}
  8013f4:	5d                   	pop    %ebp
  8013f5:	c3                   	ret    

008013f6 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  8013f6:	55                   	push   %ebp
  8013f7:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  8013f9:	8b 45 08             	mov    0x8(%ebp),%eax
  8013fc:	05 00 00 00 30       	add    $0x30000000,%eax
  801401:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  801406:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  80140b:	5d                   	pop    %ebp
  80140c:	c3                   	ret    

0080140d <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  80140d:	55                   	push   %ebp
  80140e:	89 e5                	mov    %esp,%ebp
  801410:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801413:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  801418:	89 c2                	mov    %eax,%edx
  80141a:	c1 ea 16             	shr    $0x16,%edx
  80141d:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801424:	f6 c2 01             	test   $0x1,%dl
  801427:	74 11                	je     80143a <fd_alloc+0x2d>
  801429:	89 c2                	mov    %eax,%edx
  80142b:	c1 ea 0c             	shr    $0xc,%edx
  80142e:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801435:	f6 c2 01             	test   $0x1,%dl
  801438:	75 09                	jne    801443 <fd_alloc+0x36>
			*fd_store = fd;
  80143a:	89 01                	mov    %eax,(%ecx)
			return 0;
  80143c:	b8 00 00 00 00       	mov    $0x0,%eax
  801441:	eb 17                	jmp    80145a <fd_alloc+0x4d>
  801443:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801448:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  80144d:	75 c9                	jne    801418 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  80144f:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801455:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  80145a:	5d                   	pop    %ebp
  80145b:	c3                   	ret    

0080145c <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  80145c:	55                   	push   %ebp
  80145d:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  80145f:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801463:	77 39                	ja     80149e <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801465:	8b 45 08             	mov    0x8(%ebp),%eax
  801468:	c1 e0 0c             	shl    $0xc,%eax
  80146b:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  801470:	89 c2                	mov    %eax,%edx
  801472:	c1 ea 16             	shr    $0x16,%edx
  801475:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  80147c:	f6 c2 01             	test   $0x1,%dl
  80147f:	74 24                	je     8014a5 <fd_lookup+0x49>
  801481:	89 c2                	mov    %eax,%edx
  801483:	c1 ea 0c             	shr    $0xc,%edx
  801486:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80148d:	f6 c2 01             	test   $0x1,%dl
  801490:	74 1a                	je     8014ac <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  801492:	8b 55 0c             	mov    0xc(%ebp),%edx
  801495:	89 02                	mov    %eax,(%edx)
	return 0;
  801497:	b8 00 00 00 00       	mov    $0x0,%eax
  80149c:	eb 13                	jmp    8014b1 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  80149e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8014a3:	eb 0c                	jmp    8014b1 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8014a5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8014aa:	eb 05                	jmp    8014b1 <fd_lookup+0x55>
  8014ac:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8014b1:	5d                   	pop    %ebp
  8014b2:	c3                   	ret    

008014b3 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8014b3:	55                   	push   %ebp
  8014b4:	89 e5                	mov    %esp,%ebp
  8014b6:	83 ec 08             	sub    $0x8,%esp
  8014b9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8014bc:	ba dc 2f 80 00       	mov    $0x802fdc,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  8014c1:	eb 13                	jmp    8014d6 <dev_lookup+0x23>
  8014c3:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  8014c6:	39 08                	cmp    %ecx,(%eax)
  8014c8:	75 0c                	jne    8014d6 <dev_lookup+0x23>
			*dev = devtab[i];
  8014ca:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8014cd:	89 01                	mov    %eax,(%ecx)
			return 0;
  8014cf:	b8 00 00 00 00       	mov    $0x0,%eax
  8014d4:	eb 2e                	jmp    801504 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  8014d6:	8b 02                	mov    (%edx),%eax
  8014d8:	85 c0                	test   %eax,%eax
  8014da:	75 e7                	jne    8014c3 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  8014dc:	a1 04 50 80 00       	mov    0x805004,%eax
  8014e1:	8b 40 48             	mov    0x48(%eax),%eax
  8014e4:	83 ec 04             	sub    $0x4,%esp
  8014e7:	51                   	push   %ecx
  8014e8:	50                   	push   %eax
  8014e9:	68 60 2f 80 00       	push   $0x802f60
  8014ee:	e8 c5 f0 ff ff       	call   8005b8 <cprintf>
	*dev = 0;
  8014f3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8014f6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  8014fc:	83 c4 10             	add    $0x10,%esp
  8014ff:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801504:	c9                   	leave  
  801505:	c3                   	ret    

00801506 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  801506:	55                   	push   %ebp
  801507:	89 e5                	mov    %esp,%ebp
  801509:	56                   	push   %esi
  80150a:	53                   	push   %ebx
  80150b:	83 ec 10             	sub    $0x10,%esp
  80150e:	8b 75 08             	mov    0x8(%ebp),%esi
  801511:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  801514:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801517:	50                   	push   %eax
  801518:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  80151e:	c1 e8 0c             	shr    $0xc,%eax
  801521:	50                   	push   %eax
  801522:	e8 35 ff ff ff       	call   80145c <fd_lookup>
  801527:	83 c4 08             	add    $0x8,%esp
  80152a:	85 c0                	test   %eax,%eax
  80152c:	78 05                	js     801533 <fd_close+0x2d>
	    || fd != fd2)
  80152e:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  801531:	74 06                	je     801539 <fd_close+0x33>
		return (must_exist ? r : 0);
  801533:	84 db                	test   %bl,%bl
  801535:	74 47                	je     80157e <fd_close+0x78>
  801537:	eb 4a                	jmp    801583 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801539:	83 ec 08             	sub    $0x8,%esp
  80153c:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80153f:	50                   	push   %eax
  801540:	ff 36                	pushl  (%esi)
  801542:	e8 6c ff ff ff       	call   8014b3 <dev_lookup>
  801547:	89 c3                	mov    %eax,%ebx
  801549:	83 c4 10             	add    $0x10,%esp
  80154c:	85 c0                	test   %eax,%eax
  80154e:	78 1c                	js     80156c <fd_close+0x66>
		if (dev->dev_close)
  801550:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801553:	8b 40 10             	mov    0x10(%eax),%eax
  801556:	85 c0                	test   %eax,%eax
  801558:	74 0d                	je     801567 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  80155a:	83 ec 0c             	sub    $0xc,%esp
  80155d:	56                   	push   %esi
  80155e:	ff d0                	call   *%eax
  801560:	89 c3                	mov    %eax,%ebx
  801562:	83 c4 10             	add    $0x10,%esp
  801565:	eb 05                	jmp    80156c <fd_close+0x66>
		else
			r = 0;
  801567:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  80156c:	83 ec 08             	sub    $0x8,%esp
  80156f:	56                   	push   %esi
  801570:	6a 00                	push   $0x0
  801572:	e8 0e fa ff ff       	call   800f85 <sys_page_unmap>
	return r;
  801577:	83 c4 10             	add    $0x10,%esp
  80157a:	89 d8                	mov    %ebx,%eax
  80157c:	eb 05                	jmp    801583 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  80157e:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801583:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801586:	5b                   	pop    %ebx
  801587:	5e                   	pop    %esi
  801588:	5d                   	pop    %ebp
  801589:	c3                   	ret    

0080158a <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  80158a:	55                   	push   %ebp
  80158b:	89 e5                	mov    %esp,%ebp
  80158d:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801590:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801593:	50                   	push   %eax
  801594:	ff 75 08             	pushl  0x8(%ebp)
  801597:	e8 c0 fe ff ff       	call   80145c <fd_lookup>
  80159c:	83 c4 08             	add    $0x8,%esp
  80159f:	85 c0                	test   %eax,%eax
  8015a1:	78 10                	js     8015b3 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8015a3:	83 ec 08             	sub    $0x8,%esp
  8015a6:	6a 01                	push   $0x1
  8015a8:	ff 75 f4             	pushl  -0xc(%ebp)
  8015ab:	e8 56 ff ff ff       	call   801506 <fd_close>
  8015b0:	83 c4 10             	add    $0x10,%esp
}
  8015b3:	c9                   	leave  
  8015b4:	c3                   	ret    

008015b5 <close_all>:

void
close_all(void)
{
  8015b5:	55                   	push   %ebp
  8015b6:	89 e5                	mov    %esp,%ebp
  8015b8:	53                   	push   %ebx
  8015b9:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  8015bc:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  8015c1:	83 ec 0c             	sub    $0xc,%esp
  8015c4:	53                   	push   %ebx
  8015c5:	e8 c0 ff ff ff       	call   80158a <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  8015ca:	43                   	inc    %ebx
  8015cb:	83 c4 10             	add    $0x10,%esp
  8015ce:	83 fb 20             	cmp    $0x20,%ebx
  8015d1:	75 ee                	jne    8015c1 <close_all+0xc>
		close(i);
}
  8015d3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015d6:	c9                   	leave  
  8015d7:	c3                   	ret    

008015d8 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  8015d8:	55                   	push   %ebp
  8015d9:	89 e5                	mov    %esp,%ebp
  8015db:	57                   	push   %edi
  8015dc:	56                   	push   %esi
  8015dd:	53                   	push   %ebx
  8015de:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  8015e1:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8015e4:	50                   	push   %eax
  8015e5:	ff 75 08             	pushl  0x8(%ebp)
  8015e8:	e8 6f fe ff ff       	call   80145c <fd_lookup>
  8015ed:	83 c4 08             	add    $0x8,%esp
  8015f0:	85 c0                	test   %eax,%eax
  8015f2:	0f 88 c2 00 00 00    	js     8016ba <dup+0xe2>
		return r;
	close(newfdnum);
  8015f8:	83 ec 0c             	sub    $0xc,%esp
  8015fb:	ff 75 0c             	pushl  0xc(%ebp)
  8015fe:	e8 87 ff ff ff       	call   80158a <close>

	newfd = INDEX2FD(newfdnum);
  801603:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801606:	c1 e3 0c             	shl    $0xc,%ebx
  801609:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  80160f:	83 c4 04             	add    $0x4,%esp
  801612:	ff 75 e4             	pushl  -0x1c(%ebp)
  801615:	e8 dc fd ff ff       	call   8013f6 <fd2data>
  80161a:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  80161c:	89 1c 24             	mov    %ebx,(%esp)
  80161f:	e8 d2 fd ff ff       	call   8013f6 <fd2data>
  801624:	83 c4 10             	add    $0x10,%esp
  801627:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801629:	89 f0                	mov    %esi,%eax
  80162b:	c1 e8 16             	shr    $0x16,%eax
  80162e:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801635:	a8 01                	test   $0x1,%al
  801637:	74 35                	je     80166e <dup+0x96>
  801639:	89 f0                	mov    %esi,%eax
  80163b:	c1 e8 0c             	shr    $0xc,%eax
  80163e:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801645:	f6 c2 01             	test   $0x1,%dl
  801648:	74 24                	je     80166e <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  80164a:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801651:	83 ec 0c             	sub    $0xc,%esp
  801654:	25 07 0e 00 00       	and    $0xe07,%eax
  801659:	50                   	push   %eax
  80165a:	57                   	push   %edi
  80165b:	6a 00                	push   $0x0
  80165d:	56                   	push   %esi
  80165e:	6a 00                	push   $0x0
  801660:	e8 de f8 ff ff       	call   800f43 <sys_page_map>
  801665:	89 c6                	mov    %eax,%esi
  801667:	83 c4 20             	add    $0x20,%esp
  80166a:	85 c0                	test   %eax,%eax
  80166c:	78 2c                	js     80169a <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  80166e:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801671:	89 d0                	mov    %edx,%eax
  801673:	c1 e8 0c             	shr    $0xc,%eax
  801676:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80167d:	83 ec 0c             	sub    $0xc,%esp
  801680:	25 07 0e 00 00       	and    $0xe07,%eax
  801685:	50                   	push   %eax
  801686:	53                   	push   %ebx
  801687:	6a 00                	push   $0x0
  801689:	52                   	push   %edx
  80168a:	6a 00                	push   $0x0
  80168c:	e8 b2 f8 ff ff       	call   800f43 <sys_page_map>
  801691:	89 c6                	mov    %eax,%esi
  801693:	83 c4 20             	add    $0x20,%esp
  801696:	85 c0                	test   %eax,%eax
  801698:	79 1d                	jns    8016b7 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  80169a:	83 ec 08             	sub    $0x8,%esp
  80169d:	53                   	push   %ebx
  80169e:	6a 00                	push   $0x0
  8016a0:	e8 e0 f8 ff ff       	call   800f85 <sys_page_unmap>
	sys_page_unmap(0, nva);
  8016a5:	83 c4 08             	add    $0x8,%esp
  8016a8:	57                   	push   %edi
  8016a9:	6a 00                	push   $0x0
  8016ab:	e8 d5 f8 ff ff       	call   800f85 <sys_page_unmap>
	return r;
  8016b0:	83 c4 10             	add    $0x10,%esp
  8016b3:	89 f0                	mov    %esi,%eax
  8016b5:	eb 03                	jmp    8016ba <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8016b7:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8016ba:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8016bd:	5b                   	pop    %ebx
  8016be:	5e                   	pop    %esi
  8016bf:	5f                   	pop    %edi
  8016c0:	5d                   	pop    %ebp
  8016c1:	c3                   	ret    

008016c2 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8016c2:	55                   	push   %ebp
  8016c3:	89 e5                	mov    %esp,%ebp
  8016c5:	53                   	push   %ebx
  8016c6:	83 ec 14             	sub    $0x14,%esp
  8016c9:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8016cc:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8016cf:	50                   	push   %eax
  8016d0:	53                   	push   %ebx
  8016d1:	e8 86 fd ff ff       	call   80145c <fd_lookup>
  8016d6:	83 c4 08             	add    $0x8,%esp
  8016d9:	85 c0                	test   %eax,%eax
  8016db:	78 67                	js     801744 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8016dd:	83 ec 08             	sub    $0x8,%esp
  8016e0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8016e3:	50                   	push   %eax
  8016e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8016e7:	ff 30                	pushl  (%eax)
  8016e9:	e8 c5 fd ff ff       	call   8014b3 <dev_lookup>
  8016ee:	83 c4 10             	add    $0x10,%esp
  8016f1:	85 c0                	test   %eax,%eax
  8016f3:	78 4f                	js     801744 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8016f5:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8016f8:	8b 42 08             	mov    0x8(%edx),%eax
  8016fb:	83 e0 03             	and    $0x3,%eax
  8016fe:	83 f8 01             	cmp    $0x1,%eax
  801701:	75 21                	jne    801724 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801703:	a1 04 50 80 00       	mov    0x805004,%eax
  801708:	8b 40 48             	mov    0x48(%eax),%eax
  80170b:	83 ec 04             	sub    $0x4,%esp
  80170e:	53                   	push   %ebx
  80170f:	50                   	push   %eax
  801710:	68 a1 2f 80 00       	push   $0x802fa1
  801715:	e8 9e ee ff ff       	call   8005b8 <cprintf>
		return -E_INVAL;
  80171a:	83 c4 10             	add    $0x10,%esp
  80171d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801722:	eb 20                	jmp    801744 <read+0x82>
	}
	if (!dev->dev_read)
  801724:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801727:	8b 40 08             	mov    0x8(%eax),%eax
  80172a:	85 c0                	test   %eax,%eax
  80172c:	74 11                	je     80173f <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  80172e:	83 ec 04             	sub    $0x4,%esp
  801731:	ff 75 10             	pushl  0x10(%ebp)
  801734:	ff 75 0c             	pushl  0xc(%ebp)
  801737:	52                   	push   %edx
  801738:	ff d0                	call   *%eax
  80173a:	83 c4 10             	add    $0x10,%esp
  80173d:	eb 05                	jmp    801744 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  80173f:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801744:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801747:	c9                   	leave  
  801748:	c3                   	ret    

00801749 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801749:	55                   	push   %ebp
  80174a:	89 e5                	mov    %esp,%ebp
  80174c:	57                   	push   %edi
  80174d:	56                   	push   %esi
  80174e:	53                   	push   %ebx
  80174f:	83 ec 0c             	sub    $0xc,%esp
  801752:	8b 7d 08             	mov    0x8(%ebp),%edi
  801755:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801758:	bb 00 00 00 00       	mov    $0x0,%ebx
  80175d:	eb 21                	jmp    801780 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  80175f:	83 ec 04             	sub    $0x4,%esp
  801762:	89 f0                	mov    %esi,%eax
  801764:	29 d8                	sub    %ebx,%eax
  801766:	50                   	push   %eax
  801767:	89 d8                	mov    %ebx,%eax
  801769:	03 45 0c             	add    0xc(%ebp),%eax
  80176c:	50                   	push   %eax
  80176d:	57                   	push   %edi
  80176e:	e8 4f ff ff ff       	call   8016c2 <read>
		if (m < 0)
  801773:	83 c4 10             	add    $0x10,%esp
  801776:	85 c0                	test   %eax,%eax
  801778:	78 10                	js     80178a <readn+0x41>
			return m;
		if (m == 0)
  80177a:	85 c0                	test   %eax,%eax
  80177c:	74 0a                	je     801788 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80177e:	01 c3                	add    %eax,%ebx
  801780:	39 f3                	cmp    %esi,%ebx
  801782:	72 db                	jb     80175f <readn+0x16>
  801784:	89 d8                	mov    %ebx,%eax
  801786:	eb 02                	jmp    80178a <readn+0x41>
  801788:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  80178a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80178d:	5b                   	pop    %ebx
  80178e:	5e                   	pop    %esi
  80178f:	5f                   	pop    %edi
  801790:	5d                   	pop    %ebp
  801791:	c3                   	ret    

00801792 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801792:	55                   	push   %ebp
  801793:	89 e5                	mov    %esp,%ebp
  801795:	53                   	push   %ebx
  801796:	83 ec 14             	sub    $0x14,%esp
  801799:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80179c:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80179f:	50                   	push   %eax
  8017a0:	53                   	push   %ebx
  8017a1:	e8 b6 fc ff ff       	call   80145c <fd_lookup>
  8017a6:	83 c4 08             	add    $0x8,%esp
  8017a9:	85 c0                	test   %eax,%eax
  8017ab:	78 62                	js     80180f <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8017ad:	83 ec 08             	sub    $0x8,%esp
  8017b0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8017b3:	50                   	push   %eax
  8017b4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8017b7:	ff 30                	pushl  (%eax)
  8017b9:	e8 f5 fc ff ff       	call   8014b3 <dev_lookup>
  8017be:	83 c4 10             	add    $0x10,%esp
  8017c1:	85 c0                	test   %eax,%eax
  8017c3:	78 4a                	js     80180f <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8017c5:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8017c8:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8017cc:	75 21                	jne    8017ef <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  8017ce:	a1 04 50 80 00       	mov    0x805004,%eax
  8017d3:	8b 40 48             	mov    0x48(%eax),%eax
  8017d6:	83 ec 04             	sub    $0x4,%esp
  8017d9:	53                   	push   %ebx
  8017da:	50                   	push   %eax
  8017db:	68 bd 2f 80 00       	push   $0x802fbd
  8017e0:	e8 d3 ed ff ff       	call   8005b8 <cprintf>
		return -E_INVAL;
  8017e5:	83 c4 10             	add    $0x10,%esp
  8017e8:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8017ed:	eb 20                	jmp    80180f <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8017ef:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8017f2:	8b 52 0c             	mov    0xc(%edx),%edx
  8017f5:	85 d2                	test   %edx,%edx
  8017f7:	74 11                	je     80180a <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8017f9:	83 ec 04             	sub    $0x4,%esp
  8017fc:	ff 75 10             	pushl  0x10(%ebp)
  8017ff:	ff 75 0c             	pushl  0xc(%ebp)
  801802:	50                   	push   %eax
  801803:	ff d2                	call   *%edx
  801805:	83 c4 10             	add    $0x10,%esp
  801808:	eb 05                	jmp    80180f <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  80180a:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  80180f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801812:	c9                   	leave  
  801813:	c3                   	ret    

00801814 <seek>:

int
seek(int fdnum, off_t offset)
{
  801814:	55                   	push   %ebp
  801815:	89 e5                	mov    %esp,%ebp
  801817:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80181a:	8d 45 fc             	lea    -0x4(%ebp),%eax
  80181d:	50                   	push   %eax
  80181e:	ff 75 08             	pushl  0x8(%ebp)
  801821:	e8 36 fc ff ff       	call   80145c <fd_lookup>
  801826:	83 c4 08             	add    $0x8,%esp
  801829:	85 c0                	test   %eax,%eax
  80182b:	78 0e                	js     80183b <seek+0x27>
		return r;
	fd->fd_offset = offset;
  80182d:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801830:	8b 55 0c             	mov    0xc(%ebp),%edx
  801833:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801836:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80183b:	c9                   	leave  
  80183c:	c3                   	ret    

0080183d <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  80183d:	55                   	push   %ebp
  80183e:	89 e5                	mov    %esp,%ebp
  801840:	53                   	push   %ebx
  801841:	83 ec 14             	sub    $0x14,%esp
  801844:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801847:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80184a:	50                   	push   %eax
  80184b:	53                   	push   %ebx
  80184c:	e8 0b fc ff ff       	call   80145c <fd_lookup>
  801851:	83 c4 08             	add    $0x8,%esp
  801854:	85 c0                	test   %eax,%eax
  801856:	78 5f                	js     8018b7 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801858:	83 ec 08             	sub    $0x8,%esp
  80185b:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80185e:	50                   	push   %eax
  80185f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801862:	ff 30                	pushl  (%eax)
  801864:	e8 4a fc ff ff       	call   8014b3 <dev_lookup>
  801869:	83 c4 10             	add    $0x10,%esp
  80186c:	85 c0                	test   %eax,%eax
  80186e:	78 47                	js     8018b7 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801870:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801873:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801877:	75 21                	jne    80189a <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801879:	a1 04 50 80 00       	mov    0x805004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80187e:	8b 40 48             	mov    0x48(%eax),%eax
  801881:	83 ec 04             	sub    $0x4,%esp
  801884:	53                   	push   %ebx
  801885:	50                   	push   %eax
  801886:	68 80 2f 80 00       	push   $0x802f80
  80188b:	e8 28 ed ff ff       	call   8005b8 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  801890:	83 c4 10             	add    $0x10,%esp
  801893:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801898:	eb 1d                	jmp    8018b7 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  80189a:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80189d:	8b 52 18             	mov    0x18(%edx),%edx
  8018a0:	85 d2                	test   %edx,%edx
  8018a2:	74 0e                	je     8018b2 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8018a4:	83 ec 08             	sub    $0x8,%esp
  8018a7:	ff 75 0c             	pushl  0xc(%ebp)
  8018aa:	50                   	push   %eax
  8018ab:	ff d2                	call   *%edx
  8018ad:	83 c4 10             	add    $0x10,%esp
  8018b0:	eb 05                	jmp    8018b7 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8018b2:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8018b7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8018ba:	c9                   	leave  
  8018bb:	c3                   	ret    

008018bc <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8018bc:	55                   	push   %ebp
  8018bd:	89 e5                	mov    %esp,%ebp
  8018bf:	53                   	push   %ebx
  8018c0:	83 ec 14             	sub    $0x14,%esp
  8018c3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8018c6:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8018c9:	50                   	push   %eax
  8018ca:	ff 75 08             	pushl  0x8(%ebp)
  8018cd:	e8 8a fb ff ff       	call   80145c <fd_lookup>
  8018d2:	83 c4 08             	add    $0x8,%esp
  8018d5:	85 c0                	test   %eax,%eax
  8018d7:	78 52                	js     80192b <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8018d9:	83 ec 08             	sub    $0x8,%esp
  8018dc:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8018df:	50                   	push   %eax
  8018e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8018e3:	ff 30                	pushl  (%eax)
  8018e5:	e8 c9 fb ff ff       	call   8014b3 <dev_lookup>
  8018ea:	83 c4 10             	add    $0x10,%esp
  8018ed:	85 c0                	test   %eax,%eax
  8018ef:	78 3a                	js     80192b <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8018f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8018f4:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8018f8:	74 2c                	je     801926 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8018fa:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8018fd:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801904:	00 00 00 
	stat->st_isdir = 0;
  801907:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  80190e:	00 00 00 
	stat->st_dev = dev;
  801911:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  801917:	83 ec 08             	sub    $0x8,%esp
  80191a:	53                   	push   %ebx
  80191b:	ff 75 f0             	pushl  -0x10(%ebp)
  80191e:	ff 50 14             	call   *0x14(%eax)
  801921:	83 c4 10             	add    $0x10,%esp
  801924:	eb 05                	jmp    80192b <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801926:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  80192b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80192e:	c9                   	leave  
  80192f:	c3                   	ret    

00801930 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801930:	55                   	push   %ebp
  801931:	89 e5                	mov    %esp,%ebp
  801933:	56                   	push   %esi
  801934:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801935:	83 ec 08             	sub    $0x8,%esp
  801938:	6a 00                	push   $0x0
  80193a:	ff 75 08             	pushl  0x8(%ebp)
  80193d:	e8 e7 01 00 00       	call   801b29 <open>
  801942:	89 c3                	mov    %eax,%ebx
  801944:	83 c4 10             	add    $0x10,%esp
  801947:	85 c0                	test   %eax,%eax
  801949:	78 1d                	js     801968 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  80194b:	83 ec 08             	sub    $0x8,%esp
  80194e:	ff 75 0c             	pushl  0xc(%ebp)
  801951:	50                   	push   %eax
  801952:	e8 65 ff ff ff       	call   8018bc <fstat>
  801957:	89 c6                	mov    %eax,%esi
	close(fd);
  801959:	89 1c 24             	mov    %ebx,(%esp)
  80195c:	e8 29 fc ff ff       	call   80158a <close>
	return r;
  801961:	83 c4 10             	add    $0x10,%esp
  801964:	89 f0                	mov    %esi,%eax
  801966:	eb 00                	jmp    801968 <stat+0x38>
}
  801968:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80196b:	5b                   	pop    %ebx
  80196c:	5e                   	pop    %esi
  80196d:	5d                   	pop    %ebp
  80196e:	c3                   	ret    

0080196f <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  80196f:	55                   	push   %ebp
  801970:	89 e5                	mov    %esp,%ebp
  801972:	56                   	push   %esi
  801973:	53                   	push   %ebx
  801974:	89 c6                	mov    %eax,%esi
  801976:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801978:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  80197f:	75 12                	jne    801993 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801981:	83 ec 0c             	sub    $0xc,%esp
  801984:	6a 01                	push   $0x1
  801986:	e8 fa 0c 00 00       	call   802685 <ipc_find_env>
  80198b:	a3 00 50 80 00       	mov    %eax,0x805000
  801990:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  801993:	6a 07                	push   $0x7
  801995:	68 00 60 80 00       	push   $0x806000
  80199a:	56                   	push   %esi
  80199b:	ff 35 00 50 80 00    	pushl  0x805000
  8019a1:	e8 8a 0c 00 00       	call   802630 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8019a6:	83 c4 0c             	add    $0xc,%esp
  8019a9:	6a 00                	push   $0x0
  8019ab:	53                   	push   %ebx
  8019ac:	6a 00                	push   $0x0
  8019ae:	e8 15 0c 00 00       	call   8025c8 <ipc_recv>
}
  8019b3:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8019b6:	5b                   	pop    %ebx
  8019b7:	5e                   	pop    %esi
  8019b8:	5d                   	pop    %ebp
  8019b9:	c3                   	ret    

008019ba <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8019ba:	55                   	push   %ebp
  8019bb:	89 e5                	mov    %esp,%ebp
  8019bd:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8019c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8019c3:	8b 40 0c             	mov    0xc(%eax),%eax
  8019c6:	a3 00 60 80 00       	mov    %eax,0x806000
	fsipcbuf.set_size.req_size = newsize;
  8019cb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8019ce:	a3 04 60 80 00       	mov    %eax,0x806004
	return fsipc(FSREQ_SET_SIZE, NULL);
  8019d3:	ba 00 00 00 00       	mov    $0x0,%edx
  8019d8:	b8 02 00 00 00       	mov    $0x2,%eax
  8019dd:	e8 8d ff ff ff       	call   80196f <fsipc>
}
  8019e2:	c9                   	leave  
  8019e3:	c3                   	ret    

008019e4 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8019e4:	55                   	push   %ebp
  8019e5:	89 e5                	mov    %esp,%ebp
  8019e7:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8019ea:	8b 45 08             	mov    0x8(%ebp),%eax
  8019ed:	8b 40 0c             	mov    0xc(%eax),%eax
  8019f0:	a3 00 60 80 00       	mov    %eax,0x806000
	return fsipc(FSREQ_FLUSH, NULL);
  8019f5:	ba 00 00 00 00       	mov    $0x0,%edx
  8019fa:	b8 06 00 00 00       	mov    $0x6,%eax
  8019ff:	e8 6b ff ff ff       	call   80196f <fsipc>
}
  801a04:	c9                   	leave  
  801a05:	c3                   	ret    

00801a06 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801a06:	55                   	push   %ebp
  801a07:	89 e5                	mov    %esp,%ebp
  801a09:	53                   	push   %ebx
  801a0a:	83 ec 04             	sub    $0x4,%esp
  801a0d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801a10:	8b 45 08             	mov    0x8(%ebp),%eax
  801a13:	8b 40 0c             	mov    0xc(%eax),%eax
  801a16:	a3 00 60 80 00       	mov    %eax,0x806000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  801a1b:	ba 00 00 00 00       	mov    $0x0,%edx
  801a20:	b8 05 00 00 00       	mov    $0x5,%eax
  801a25:	e8 45 ff ff ff       	call   80196f <fsipc>
  801a2a:	85 c0                	test   %eax,%eax
  801a2c:	78 2c                	js     801a5a <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801a2e:	83 ec 08             	sub    $0x8,%esp
  801a31:	68 00 60 80 00       	push   $0x806000
  801a36:	53                   	push   %ebx
  801a37:	e8 e0 f0 ff ff       	call   800b1c <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801a3c:	a1 80 60 80 00       	mov    0x806080,%eax
  801a41:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801a47:	a1 84 60 80 00       	mov    0x806084,%eax
  801a4c:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801a52:	83 c4 10             	add    $0x10,%esp
  801a55:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801a5a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801a5d:	c9                   	leave  
  801a5e:	c3                   	ret    

00801a5f <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801a5f:	55                   	push   %ebp
  801a60:	89 e5                	mov    %esp,%ebp
  801a62:	83 ec 08             	sub    $0x8,%esp
  801a65:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801a68:	8b 55 08             	mov    0x8(%ebp),%edx
  801a6b:	8b 52 0c             	mov    0xc(%edx),%edx
  801a6e:	89 15 00 60 80 00    	mov    %edx,0x806000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801a74:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801a79:	76 05                	jbe    801a80 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801a7b:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801a80:	a3 04 60 80 00       	mov    %eax,0x806004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801a85:	83 ec 04             	sub    $0x4,%esp
  801a88:	50                   	push   %eax
  801a89:	ff 75 0c             	pushl  0xc(%ebp)
  801a8c:	68 08 60 80 00       	push   $0x806008
  801a91:	e8 fb f1 ff ff       	call   800c91 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801a96:	ba 00 00 00 00       	mov    $0x0,%edx
  801a9b:	b8 04 00 00 00       	mov    $0x4,%eax
  801aa0:	e8 ca fe ff ff       	call   80196f <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801aa5:	c9                   	leave  
  801aa6:	c3                   	ret    

00801aa7 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801aa7:	55                   	push   %ebp
  801aa8:	89 e5                	mov    %esp,%ebp
  801aaa:	56                   	push   %esi
  801aab:	53                   	push   %ebx
  801aac:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  801aaf:	8b 45 08             	mov    0x8(%ebp),%eax
  801ab2:	8b 40 0c             	mov    0xc(%eax),%eax
  801ab5:	a3 00 60 80 00       	mov    %eax,0x806000
	fsipcbuf.read.req_n = n;
  801aba:	89 35 04 60 80 00    	mov    %esi,0x806004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801ac0:	ba 00 00 00 00       	mov    $0x0,%edx
  801ac5:	b8 03 00 00 00       	mov    $0x3,%eax
  801aca:	e8 a0 fe ff ff       	call   80196f <fsipc>
  801acf:	89 c3                	mov    %eax,%ebx
  801ad1:	85 c0                	test   %eax,%eax
  801ad3:	78 4b                	js     801b20 <devfile_read+0x79>
		return r;
	assert(r <= n);
  801ad5:	39 c6                	cmp    %eax,%esi
  801ad7:	73 16                	jae    801aef <devfile_read+0x48>
  801ad9:	68 ec 2f 80 00       	push   $0x802fec
  801ade:	68 f3 2f 80 00       	push   $0x802ff3
  801ae3:	6a 7c                	push   $0x7c
  801ae5:	68 08 30 80 00       	push   $0x803008
  801aea:	e8 f1 e9 ff ff       	call   8004e0 <_panic>
	assert(r <= PGSIZE);
  801aef:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801af4:	7e 16                	jle    801b0c <devfile_read+0x65>
  801af6:	68 13 30 80 00       	push   $0x803013
  801afb:	68 f3 2f 80 00       	push   $0x802ff3
  801b00:	6a 7d                	push   $0x7d
  801b02:	68 08 30 80 00       	push   $0x803008
  801b07:	e8 d4 e9 ff ff       	call   8004e0 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801b0c:	83 ec 04             	sub    $0x4,%esp
  801b0f:	50                   	push   %eax
  801b10:	68 00 60 80 00       	push   $0x806000
  801b15:	ff 75 0c             	pushl  0xc(%ebp)
  801b18:	e8 74 f1 ff ff       	call   800c91 <memmove>
	return r;
  801b1d:	83 c4 10             	add    $0x10,%esp
}
  801b20:	89 d8                	mov    %ebx,%eax
  801b22:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801b25:	5b                   	pop    %ebx
  801b26:	5e                   	pop    %esi
  801b27:	5d                   	pop    %ebp
  801b28:	c3                   	ret    

00801b29 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801b29:	55                   	push   %ebp
  801b2a:	89 e5                	mov    %esp,%ebp
  801b2c:	53                   	push   %ebx
  801b2d:	83 ec 20             	sub    $0x20,%esp
  801b30:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801b33:	53                   	push   %ebx
  801b34:	e8 ae ef ff ff       	call   800ae7 <strlen>
  801b39:	83 c4 10             	add    $0x10,%esp
  801b3c:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801b41:	7f 63                	jg     801ba6 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801b43:	83 ec 0c             	sub    $0xc,%esp
  801b46:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b49:	50                   	push   %eax
  801b4a:	e8 be f8 ff ff       	call   80140d <fd_alloc>
  801b4f:	83 c4 10             	add    $0x10,%esp
  801b52:	85 c0                	test   %eax,%eax
  801b54:	78 55                	js     801bab <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801b56:	83 ec 08             	sub    $0x8,%esp
  801b59:	53                   	push   %ebx
  801b5a:	68 00 60 80 00       	push   $0x806000
  801b5f:	e8 b8 ef ff ff       	call   800b1c <strcpy>
	fsipcbuf.open.req_omode = mode;
  801b64:	8b 45 0c             	mov    0xc(%ebp),%eax
  801b67:	a3 00 64 80 00       	mov    %eax,0x806400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801b6c:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801b6f:	b8 01 00 00 00       	mov    $0x1,%eax
  801b74:	e8 f6 fd ff ff       	call   80196f <fsipc>
  801b79:	89 c3                	mov    %eax,%ebx
  801b7b:	83 c4 10             	add    $0x10,%esp
  801b7e:	85 c0                	test   %eax,%eax
  801b80:	79 14                	jns    801b96 <open+0x6d>
		fd_close(fd, 0);
  801b82:	83 ec 08             	sub    $0x8,%esp
  801b85:	6a 00                	push   $0x0
  801b87:	ff 75 f4             	pushl  -0xc(%ebp)
  801b8a:	e8 77 f9 ff ff       	call   801506 <fd_close>
		return r;
  801b8f:	83 c4 10             	add    $0x10,%esp
  801b92:	89 d8                	mov    %ebx,%eax
  801b94:	eb 15                	jmp    801bab <open+0x82>
	}

	return fd2num(fd);
  801b96:	83 ec 0c             	sub    $0xc,%esp
  801b99:	ff 75 f4             	pushl  -0xc(%ebp)
  801b9c:	e8 45 f8 ff ff       	call   8013e6 <fd2num>
  801ba1:	83 c4 10             	add    $0x10,%esp
  801ba4:	eb 05                	jmp    801bab <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801ba6:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801bab:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801bae:	c9                   	leave  
  801baf:	c3                   	ret    

00801bb0 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801bb0:	55                   	push   %ebp
  801bb1:	89 e5                	mov    %esp,%ebp
  801bb3:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801bb6:	ba 00 00 00 00       	mov    $0x0,%edx
  801bbb:	b8 08 00 00 00       	mov    $0x8,%eax
  801bc0:	e8 aa fd ff ff       	call   80196f <fsipc>
}
  801bc5:	c9                   	leave  
  801bc6:	c3                   	ret    

00801bc7 <spawn>:
// argv: pointer to null-terminated array of pointers to strings,
// 	 which will be passed to the child as its command-line arguments.
// Returns child envid on success, < 0 on failure.
int
spawn(const char *prog, const char **argv)
{
  801bc7:	55                   	push   %ebp
  801bc8:	89 e5                	mov    %esp,%ebp
  801bca:	57                   	push   %edi
  801bcb:	56                   	push   %esi
  801bcc:	53                   	push   %ebx
  801bcd:	81 ec 94 02 00 00    	sub    $0x294,%esp
  801bd3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//   - Call sys_env_set_trapframe(child, &child_tf) to set up the
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
  801bd6:	6a 00                	push   $0x0
  801bd8:	ff 75 08             	pushl  0x8(%ebp)
  801bdb:	e8 49 ff ff ff       	call   801b29 <open>
  801be0:	89 c1                	mov    %eax,%ecx
  801be2:	89 85 8c fd ff ff    	mov    %eax,-0x274(%ebp)
  801be8:	83 c4 10             	add    $0x10,%esp
  801beb:	85 c0                	test   %eax,%eax
  801bed:	0f 88 e2 04 00 00    	js     8020d5 <spawn+0x50e>
		return r;
	fd = r;

	// Read elf header
	elf = (struct Elf*) elf_buf;
	if (readn(fd, elf_buf, sizeof(elf_buf)) != sizeof(elf_buf)
  801bf3:	83 ec 04             	sub    $0x4,%esp
  801bf6:	68 00 02 00 00       	push   $0x200
  801bfb:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  801c01:	50                   	push   %eax
  801c02:	51                   	push   %ecx
  801c03:	e8 41 fb ff ff       	call   801749 <readn>
  801c08:	83 c4 10             	add    $0x10,%esp
  801c0b:	3d 00 02 00 00       	cmp    $0x200,%eax
  801c10:	75 0c                	jne    801c1e <spawn+0x57>
	    || elf->e_magic != ELF_MAGIC) {
  801c12:	81 bd e8 fd ff ff 7f 	cmpl   $0x464c457f,-0x218(%ebp)
  801c19:	45 4c 46 
  801c1c:	74 33                	je     801c51 <spawn+0x8a>
		close(fd);
  801c1e:	83 ec 0c             	sub    $0xc,%esp
  801c21:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801c27:	e8 5e f9 ff ff       	call   80158a <close>
		cprintf("elf magic %08x want %08x\n", elf->e_magic, ELF_MAGIC);
  801c2c:	83 c4 0c             	add    $0xc,%esp
  801c2f:	68 7f 45 4c 46       	push   $0x464c457f
  801c34:	ff b5 e8 fd ff ff    	pushl  -0x218(%ebp)
  801c3a:	68 1f 30 80 00       	push   $0x80301f
  801c3f:	e8 74 e9 ff ff       	call   8005b8 <cprintf>
		return -E_NOT_EXEC;
  801c44:	83 c4 10             	add    $0x10,%esp
  801c47:	bb f2 ff ff ff       	mov    $0xfffffff2,%ebx
  801c4c:	e9 e4 04 00 00       	jmp    802135 <spawn+0x56e>
  801c51:	b8 07 00 00 00       	mov    $0x7,%eax
  801c56:	cd 30                	int    $0x30
  801c58:	89 85 74 fd ff ff    	mov    %eax,-0x28c(%ebp)
  801c5e:	89 85 84 fd ff ff    	mov    %eax,-0x27c(%ebp)
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
  801c64:	85 c0                	test   %eax,%eax
  801c66:	0f 88 71 04 00 00    	js     8020dd <spawn+0x516>
		return r;
	child = r;

	// Set up trap frame, including initial stack.
	child_tf = envs[ENVX(child)].env_tf;
  801c6c:	25 ff 03 00 00       	and    $0x3ff,%eax
  801c71:	89 c6                	mov    %eax,%esi
  801c73:	8d 04 85 00 00 00 00 	lea    0x0(,%eax,4),%eax
  801c7a:	c1 e6 07             	shl    $0x7,%esi
  801c7d:	29 c6                	sub    %eax,%esi
  801c7f:	81 c6 00 00 c0 ee    	add    $0xeec00000,%esi
  801c85:	8d bd a4 fd ff ff    	lea    -0x25c(%ebp),%edi
  801c8b:	b9 11 00 00 00       	mov    $0x11,%ecx
  801c90:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	child_tf.tf_eip = elf->e_entry;
  801c92:	8b 85 00 fe ff ff    	mov    -0x200(%ebp),%eax
  801c98:	89 85 d4 fd ff ff    	mov    %eax,-0x22c(%ebp)
  801c9e:	ba 00 00 00 00       	mov    $0x0,%edx
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  801ca3:	b8 00 00 00 00       	mov    $0x0,%eax
	char *string_store;
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
  801ca8:	bf 00 00 00 00       	mov    $0x0,%edi
  801cad:	89 5d 0c             	mov    %ebx,0xc(%ebp)
  801cb0:	89 c3                	mov    %eax,%ebx
  801cb2:	eb 13                	jmp    801cc7 <spawn+0x100>
	for (argc = 0; argv[argc] != 0; argc++)
		string_size += strlen(argv[argc]) + 1;
  801cb4:	83 ec 0c             	sub    $0xc,%esp
  801cb7:	50                   	push   %eax
  801cb8:	e8 2a ee ff ff       	call   800ae7 <strlen>
  801cbd:	8d 7c 38 01          	lea    0x1(%eax,%edi,1),%edi
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  801cc1:	43                   	inc    %ebx
  801cc2:	89 f2                	mov    %esi,%edx
  801cc4:	83 c4 10             	add    $0x10,%esp
  801cc7:	89 d9                	mov    %ebx,%ecx
  801cc9:	8d 72 04             	lea    0x4(%edx),%esi
  801ccc:	8b 45 0c             	mov    0xc(%ebp),%eax
  801ccf:	8b 44 30 fc          	mov    -0x4(%eax,%esi,1),%eax
  801cd3:	85 c0                	test   %eax,%eax
  801cd5:	75 dd                	jne    801cb4 <spawn+0xed>
  801cd7:	89 9d 90 fd ff ff    	mov    %ebx,-0x270(%ebp)
  801cdd:	89 9d 88 fd ff ff    	mov    %ebx,-0x278(%ebp)
  801ce3:	89 95 80 fd ff ff    	mov    %edx,-0x280(%ebp)
  801ce9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Determine where to place the strings and the argv array.
	// Set up pointers into the temporary page 'UTEMP'; we'll map a page
	// there later, then remap that page into the child environment
	// at (USTACKTOP - PGSIZE).
	// strings is the topmost thing on the stack.
	string_store = (char*) UTEMP + PGSIZE - string_size;
  801cec:	b8 00 10 40 00       	mov    $0x401000,%eax
  801cf1:	29 f8                	sub    %edi,%eax
  801cf3:	89 c7                	mov    %eax,%edi
	// argv is below that.  There's one argument pointer per argument, plus
	// a null pointer.
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));
  801cf5:	89 c2                	mov    %eax,%edx
  801cf7:	83 e2 fc             	and    $0xfffffffc,%edx
  801cfa:	8d 04 8d 04 00 00 00 	lea    0x4(,%ecx,4),%eax
  801d01:	29 c2                	sub    %eax,%edx
  801d03:	89 95 94 fd ff ff    	mov    %edx,-0x26c(%ebp)

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
  801d09:	8d 42 f8             	lea    -0x8(%edx),%eax
  801d0c:	3d ff ff 3f 00       	cmp    $0x3fffff,%eax
  801d11:	0f 86 d6 03 00 00    	jbe    8020ed <spawn+0x526>
		return -E_NO_MEM;

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801d17:	83 ec 04             	sub    $0x4,%esp
  801d1a:	6a 07                	push   $0x7
  801d1c:	68 00 00 40 00       	push   $0x400000
  801d21:	6a 00                	push   $0x0
  801d23:	e8 d8 f1 ff ff       	call   800f00 <sys_page_alloc>
  801d28:	83 c4 10             	add    $0x10,%esp
  801d2b:	85 c0                	test   %eax,%eax
  801d2d:	0f 88 c1 03 00 00    	js     8020f4 <spawn+0x52d>
  801d33:	be 00 00 00 00       	mov    $0x0,%esi
  801d38:	eb 2e                	jmp    801d68 <spawn+0x1a1>
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
		argv_store[i] = UTEMP2USTACK(string_store);
  801d3a:	8d 87 00 d0 7f ee    	lea    -0x11803000(%edi),%eax
  801d40:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  801d46:	89 04 b1             	mov    %eax,(%ecx,%esi,4)
		strcpy(string_store, argv[i]);
  801d49:	83 ec 08             	sub    $0x8,%esp
  801d4c:	ff 34 b3             	pushl  (%ebx,%esi,4)
  801d4f:	57                   	push   %edi
  801d50:	e8 c7 ed ff ff       	call   800b1c <strcpy>
		string_store += strlen(argv[i]) + 1;
  801d55:	83 c4 04             	add    $0x4,%esp
  801d58:	ff 34 b3             	pushl  (%ebx,%esi,4)
  801d5b:	e8 87 ed ff ff       	call   800ae7 <strlen>
  801d60:	8d 7c 07 01          	lea    0x1(%edi,%eax,1),%edi
	//	  (Again, argv should use an address valid in the child's
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
  801d64:	46                   	inc    %esi
  801d65:	83 c4 10             	add    $0x10,%esp
  801d68:	39 b5 90 fd ff ff    	cmp    %esi,-0x270(%ebp)
  801d6e:	7f ca                	jg     801d3a <spawn+0x173>
		argv_store[i] = UTEMP2USTACK(string_store);
		strcpy(string_store, argv[i]);
		string_store += strlen(argv[i]) + 1;
	}
	argv_store[argc] = 0;
  801d70:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801d76:	8b 8d 80 fd ff ff    	mov    -0x280(%ebp),%ecx
  801d7c:	c7 04 08 00 00 00 00 	movl   $0x0,(%eax,%ecx,1)
	assert(string_store == (char*)UTEMP + PGSIZE);
  801d83:	81 ff 00 10 40 00    	cmp    $0x401000,%edi
  801d89:	74 19                	je     801da4 <spawn+0x1dd>
  801d8b:	68 ac 30 80 00       	push   $0x8030ac
  801d90:	68 f3 2f 80 00       	push   $0x802ff3
  801d95:	68 f1 00 00 00       	push   $0xf1
  801d9a:	68 39 30 80 00       	push   $0x803039
  801d9f:	e8 3c e7 ff ff       	call   8004e0 <_panic>

	argv_store[-1] = UTEMP2USTACK(argv_store);
  801da4:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  801daa:	89 c8                	mov    %ecx,%eax
  801dac:	2d 00 30 80 11       	sub    $0x11803000,%eax
  801db1:	89 41 fc             	mov    %eax,-0x4(%ecx)
	argv_store[-2] = argc;
  801db4:	89 c8                	mov    %ecx,%eax
  801db6:	8b 8d 88 fd ff ff    	mov    -0x278(%ebp),%ecx
  801dbc:	89 48 f8             	mov    %ecx,-0x8(%eax)

	*init_esp = UTEMP2USTACK(&argv_store[-2]);
  801dbf:	2d 08 30 80 11       	sub    $0x11803008,%eax
  801dc4:	89 85 e0 fd ff ff    	mov    %eax,-0x220(%ebp)

	// After completing the stack, map it into the child's address space
	// and unmap it from ours!
	if ((r = sys_page_map(0, UTEMP, child, (void*) (USTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W)) < 0)
  801dca:	83 ec 0c             	sub    $0xc,%esp
  801dcd:	6a 07                	push   $0x7
  801dcf:	68 00 d0 bf ee       	push   $0xeebfd000
  801dd4:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801dda:	68 00 00 40 00       	push   $0x400000
  801ddf:	6a 00                	push   $0x0
  801de1:	e8 5d f1 ff ff       	call   800f43 <sys_page_map>
  801de6:	89 c3                	mov    %eax,%ebx
  801de8:	83 c4 20             	add    $0x20,%esp
  801deb:	85 c0                	test   %eax,%eax
  801ded:	0f 88 30 03 00 00    	js     802123 <spawn+0x55c>
		goto error;
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  801df3:	83 ec 08             	sub    $0x8,%esp
  801df6:	68 00 00 40 00       	push   $0x400000
  801dfb:	6a 00                	push   $0x0
  801dfd:	e8 83 f1 ff ff       	call   800f85 <sys_page_unmap>
  801e02:	89 c3                	mov    %eax,%ebx
  801e04:	83 c4 10             	add    $0x10,%esp
  801e07:	85 c0                	test   %eax,%eax
  801e09:	0f 88 14 03 00 00    	js     802123 <spawn+0x55c>

	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
  801e0f:	8b 85 04 fe ff ff    	mov    -0x1fc(%ebp),%eax
  801e15:	8d 84 05 e8 fd ff ff 	lea    -0x218(%ebp,%eax,1),%eax
  801e1c:	89 85 7c fd ff ff    	mov    %eax,-0x284(%ebp)
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  801e22:	c7 85 78 fd ff ff 00 	movl   $0x0,-0x288(%ebp)
  801e29:	00 00 00 
  801e2c:	e9 88 01 00 00       	jmp    801fb9 <spawn+0x3f2>
		if (ph->p_type != ELF_PROG_LOAD)
  801e31:	8b 85 7c fd ff ff    	mov    -0x284(%ebp),%eax
  801e37:	83 38 01             	cmpl   $0x1,(%eax)
  801e3a:	0f 85 6c 01 00 00    	jne    801fac <spawn+0x3e5>
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
  801e40:	89 c1                	mov    %eax,%ecx
  801e42:	8b 40 18             	mov    0x18(%eax),%eax
  801e45:	83 e0 02             	and    $0x2,%eax
			perm |= PTE_W;
  801e48:	83 f8 01             	cmp    $0x1,%eax
  801e4b:	19 c0                	sbb    %eax,%eax
  801e4d:	83 e0 fe             	and    $0xfffffffe,%eax
  801e50:	83 c0 07             	add    $0x7,%eax
  801e53:	89 85 88 fd ff ff    	mov    %eax,-0x278(%ebp)
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
  801e59:	89 c8                	mov    %ecx,%eax
  801e5b:	8b 49 04             	mov    0x4(%ecx),%ecx
  801e5e:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
  801e64:	8b 78 10             	mov    0x10(%eax),%edi
  801e67:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  801e6d:	8b 70 14             	mov    0x14(%eax),%esi
  801e70:	89 f2                	mov    %esi,%edx
  801e72:	89 b5 90 fd ff ff    	mov    %esi,-0x270(%ebp)
  801e78:	8b 70 08             	mov    0x8(%eax),%esi
	int i, r;
	void *blk;

	//cprintf("map_segment %x+%x\n", va, memsz);

	if ((i = PGOFF(va))) {
  801e7b:	89 f0                	mov    %esi,%eax
  801e7d:	25 ff 0f 00 00       	and    $0xfff,%eax
  801e82:	74 1a                	je     801e9e <spawn+0x2d7>
		va -= i;
  801e84:	29 c6                	sub    %eax,%esi
		memsz += i;
  801e86:	01 c2                	add    %eax,%edx
  801e88:	89 95 90 fd ff ff    	mov    %edx,-0x270(%ebp)
		filesz += i;
  801e8e:	01 c7                	add    %eax,%edi
  801e90:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
		fileoffset -= i;
  801e96:	29 c1                	sub    %eax,%ecx
  801e98:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  801e9e:	bb 00 00 00 00       	mov    $0x0,%ebx
  801ea3:	e9 f6 00 00 00       	jmp    801f9e <spawn+0x3d7>
		if (i >= filesz) {
  801ea8:	39 9d 94 fd ff ff    	cmp    %ebx,-0x26c(%ebp)
  801eae:	77 27                	ja     801ed7 <spawn+0x310>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
  801eb0:	83 ec 04             	sub    $0x4,%esp
  801eb3:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801eb9:	56                   	push   %esi
  801eba:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801ec0:	e8 3b f0 ff ff       	call   800f00 <sys_page_alloc>
  801ec5:	83 c4 10             	add    $0x10,%esp
  801ec8:	85 c0                	test   %eax,%eax
  801eca:	0f 89 c2 00 00 00    	jns    801f92 <spawn+0x3cb>
  801ed0:	89 c3                	mov    %eax,%ebx
  801ed2:	e9 2b 02 00 00       	jmp    802102 <spawn+0x53b>
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801ed7:	83 ec 04             	sub    $0x4,%esp
  801eda:	6a 07                	push   $0x7
  801edc:	68 00 00 40 00       	push   $0x400000
  801ee1:	6a 00                	push   $0x0
  801ee3:	e8 18 f0 ff ff       	call   800f00 <sys_page_alloc>
  801ee8:	83 c4 10             	add    $0x10,%esp
  801eeb:	85 c0                	test   %eax,%eax
  801eed:	0f 88 05 02 00 00    	js     8020f8 <spawn+0x531>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801ef3:	83 ec 08             	sub    $0x8,%esp
  801ef6:	8b 85 80 fd ff ff    	mov    -0x280(%ebp),%eax
  801efc:	01 f8                	add    %edi,%eax
  801efe:	50                   	push   %eax
  801eff:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801f05:	e8 0a f9 ff ff       	call   801814 <seek>
  801f0a:	83 c4 10             	add    $0x10,%esp
  801f0d:	85 c0                	test   %eax,%eax
  801f0f:	0f 88 e7 01 00 00    	js     8020fc <spawn+0x535>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801f15:	83 ec 04             	sub    $0x4,%esp
  801f18:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801f1e:	29 f8                	sub    %edi,%eax
  801f20:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801f25:	76 05                	jbe    801f2c <spawn+0x365>
  801f27:	b8 00 10 00 00       	mov    $0x1000,%eax
  801f2c:	50                   	push   %eax
  801f2d:	68 00 00 40 00       	push   $0x400000
  801f32:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801f38:	e8 0c f8 ff ff       	call   801749 <readn>
  801f3d:	83 c4 10             	add    $0x10,%esp
  801f40:	85 c0                	test   %eax,%eax
  801f42:	0f 88 b8 01 00 00    	js     802100 <spawn+0x539>
				return r;
			if ((r = sys_page_map(0, UTEMP, child, (void*) (va + i), perm)) < 0)
  801f48:	83 ec 0c             	sub    $0xc,%esp
  801f4b:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801f51:	56                   	push   %esi
  801f52:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801f58:	68 00 00 40 00       	push   $0x400000
  801f5d:	6a 00                	push   $0x0
  801f5f:	e8 df ef ff ff       	call   800f43 <sys_page_map>
  801f64:	83 c4 20             	add    $0x20,%esp
  801f67:	85 c0                	test   %eax,%eax
  801f69:	79 15                	jns    801f80 <spawn+0x3b9>
				panic("spawn: sys_page_map data: %e", r);
  801f6b:	50                   	push   %eax
  801f6c:	68 45 30 80 00       	push   $0x803045
  801f71:	68 24 01 00 00       	push   $0x124
  801f76:	68 39 30 80 00       	push   $0x803039
  801f7b:	e8 60 e5 ff ff       	call   8004e0 <_panic>
			sys_page_unmap(0, UTEMP);
  801f80:	83 ec 08             	sub    $0x8,%esp
  801f83:	68 00 00 40 00       	push   $0x400000
  801f88:	6a 00                	push   $0x0
  801f8a:	e8 f6 ef ff ff       	call   800f85 <sys_page_unmap>
  801f8f:	83 c4 10             	add    $0x10,%esp
		memsz += i;
		filesz += i;
		fileoffset -= i;
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  801f92:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  801f98:	81 c6 00 10 00 00    	add    $0x1000,%esi
  801f9e:	89 df                	mov    %ebx,%edi
  801fa0:	39 9d 90 fd ff ff    	cmp    %ebx,-0x270(%ebp)
  801fa6:	0f 87 fc fe ff ff    	ja     801ea8 <spawn+0x2e1>
	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  801fac:	ff 85 78 fd ff ff    	incl   -0x288(%ebp)
  801fb2:	83 85 7c fd ff ff 20 	addl   $0x20,-0x284(%ebp)
  801fb9:	0f b7 85 14 fe ff ff 	movzwl -0x1ec(%ebp),%eax
  801fc0:	39 85 78 fd ff ff    	cmp    %eax,-0x288(%ebp)
  801fc6:	0f 8c 65 fe ff ff    	jl     801e31 <spawn+0x26a>
			perm |= PTE_W;
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
				     fd, ph->p_filesz, ph->p_offset, perm)) < 0)
			goto error;
	}
	close(fd);
  801fcc:	83 ec 0c             	sub    $0xc,%esp
  801fcf:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801fd5:	e8 b0 f5 ff ff       	call   80158a <close>
  801fda:	83 c4 10             	add    $0x10,%esp
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801fdd:	bf 00 00 00 00       	mov    $0x0,%edi
  801fe2:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  801fe8:	8b bd 84 fd ff ff    	mov    -0x27c(%ebp),%edi
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  801fee:	8b b5 94 fd ff ff    	mov    -0x26c(%ebp),%esi
  801ff4:	8b 04 b5 00 d0 7b ef 	mov    -0x10843000(,%esi,4),%eax
  801ffb:	a8 01                	test   $0x1,%al
  801ffd:	74 4b                	je     80204a <spawn+0x483>
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
        	/* LAB 4: Your code here.
		    pte_t pte = uvpt[pn];
		    void *va = (void *)(pn << PGSHIFT);
		    int perm = pte & PTE_SYSCALL;*/
		    unsigned pn = (ipd << 10) | ipt;
  801fff:	c1 e6 0a             	shl    $0xa,%esi
  802002:	bb 00 00 00 00       	mov    $0x0,%ebx
  802007:	89 f0                	mov    %esi,%eax
  802009:	09 d8                	or     %ebx,%eax
        	if(!(uvpt[pn] & PTE_P)){ //doesn't exist
  80200b:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  802012:	f6 c2 01             	test   $0x1,%dl
  802015:	74 2a                	je     802041 <spawn+0x47a>
        		continue;
        	}
	        int perm = uvpt[pn] & PTE_SYSCALL;
  802017:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
	        if(perm & PTE_SHARE){
  80201e:	f6 c6 04             	test   $0x4,%dh
  802021:	74 1e                	je     802041 <spawn+0x47a>
			    void *va = (void *)(pn << PGSHIFT);
  802023:	c1 e0 0c             	shl    $0xc,%eax
			    r = sys_page_map(0, va, child, va, perm);
  802026:	83 ec 0c             	sub    $0xc,%esp
  802029:	81 e2 07 0e 00 00    	and    $0xe07,%edx
  80202f:	52                   	push   %edx
  802030:	50                   	push   %eax
  802031:	57                   	push   %edi
  802032:	50                   	push   %eax
  802033:	6a 00                	push   $0x0
  802035:	e8 09 ef ff ff       	call   800f43 <sys_page_map>
			    if (r){
  80203a:	83 c4 20             	add    $0x20,%esp
  80203d:	85 c0                	test   %eax,%eax
  80203f:	75 1e                	jne    80205f <spawn+0x498>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  802041:	43                   	inc    %ebx
  802042:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  802048:	75 bd                	jne    802007 <spawn+0x440>
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80204a:	ff 85 94 fd ff ff    	incl   -0x26c(%ebp)
  802050:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  802056:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  80205b:	75 91                	jne    801fee <spawn+0x427>
  80205d:	eb 19                	jmp    802078 <spawn+0x4b1>
	}
	close(fd);
	fd = -1;

	// Copy shared library state.
	if ((r = copy_shared_pages(child)) < 0)
  80205f:	85 c0                	test   %eax,%eax
  802061:	79 15                	jns    802078 <spawn+0x4b1>
		panic("copy_shared_pages: %e", r);
  802063:	50                   	push   %eax
  802064:	68 62 30 80 00       	push   $0x803062
  802069:	68 82 00 00 00       	push   $0x82
  80206e:	68 39 30 80 00       	push   $0x803039
  802073:	e8 68 e4 ff ff       	call   8004e0 <_panic>

	if ((r = sys_env_set_trapframe(child, &child_tf)) < 0)
  802078:	83 ec 08             	sub    $0x8,%esp
  80207b:	8d 85 a4 fd ff ff    	lea    -0x25c(%ebp),%eax
  802081:	50                   	push   %eax
  802082:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  802088:	e8 9b ef ff ff       	call   801028 <sys_env_set_trapframe>
  80208d:	83 c4 10             	add    $0x10,%esp
  802090:	85 c0                	test   %eax,%eax
  802092:	79 15                	jns    8020a9 <spawn+0x4e2>
		panic("sys_env_set_trapframe: %e", r);
  802094:	50                   	push   %eax
  802095:	68 78 30 80 00       	push   $0x803078
  80209a:	68 85 00 00 00       	push   $0x85
  80209f:	68 39 30 80 00       	push   $0x803039
  8020a4:	e8 37 e4 ff ff       	call   8004e0 <_panic>

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
  8020a9:	83 ec 08             	sub    $0x8,%esp
  8020ac:	6a 02                	push   $0x2
  8020ae:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8020b4:	e8 0e ef ff ff       	call   800fc7 <sys_env_set_status>
  8020b9:	83 c4 10             	add    $0x10,%esp
  8020bc:	85 c0                	test   %eax,%eax
  8020be:	79 25                	jns    8020e5 <spawn+0x51e>
		panic("sys_env_set_status: %e", r);
  8020c0:	50                   	push   %eax
  8020c1:	68 92 30 80 00       	push   $0x803092
  8020c6:	68 88 00 00 00       	push   $0x88
  8020cb:	68 39 30 80 00       	push   $0x803039
  8020d0:	e8 0b e4 ff ff       	call   8004e0 <_panic>
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
		return r;
  8020d5:	8b 9d 8c fd ff ff    	mov    -0x274(%ebp),%ebx
  8020db:	eb 58                	jmp    802135 <spawn+0x56e>
		return -E_NOT_EXEC;
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
		return r;
  8020dd:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  8020e3:	eb 50                	jmp    802135 <spawn+0x56e>
		panic("sys_env_set_trapframe: %e", r);

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
		panic("sys_env_set_status: %e", r);

	return child;
  8020e5:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  8020eb:	eb 48                	jmp    802135 <spawn+0x56e>
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
		return -E_NO_MEM;
  8020ed:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
  8020f2:	eb 41                	jmp    802135 <spawn+0x56e>

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
		return r;
  8020f4:	89 c3                	mov    %eax,%ebx
  8020f6:	eb 3d                	jmp    802135 <spawn+0x56e>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  8020f8:	89 c3                	mov    %eax,%ebx
  8020fa:	eb 06                	jmp    802102 <spawn+0x53b>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  8020fc:	89 c3                	mov    %eax,%ebx
  8020fe:	eb 02                	jmp    802102 <spawn+0x53b>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  802100:	89 c3                	mov    %eax,%ebx
		panic("sys_env_set_status: %e", r);

	return child;

error:
	sys_env_destroy(child);
  802102:	83 ec 0c             	sub    $0xc,%esp
  802105:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  80210b:	e8 71 ed ff ff       	call   800e81 <sys_env_destroy>
	close(fd);
  802110:	83 c4 04             	add    $0x4,%esp
  802113:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  802119:	e8 6c f4 ff ff       	call   80158a <close>
	return r;
  80211e:	83 c4 10             	add    $0x10,%esp
  802121:	eb 12                	jmp    802135 <spawn+0x56e>
		goto error;

	return 0;

error:
	sys_page_unmap(0, UTEMP);
  802123:	83 ec 08             	sub    $0x8,%esp
  802126:	68 00 00 40 00       	push   $0x400000
  80212b:	6a 00                	push   $0x0
  80212d:	e8 53 ee ff ff       	call   800f85 <sys_page_unmap>
  802132:	83 c4 10             	add    $0x10,%esp

error:
	sys_env_destroy(child);
	close(fd);
	return r;
}
  802135:	89 d8                	mov    %ebx,%eax
  802137:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80213a:	5b                   	pop    %ebx
  80213b:	5e                   	pop    %esi
  80213c:	5f                   	pop    %edi
  80213d:	5d                   	pop    %ebp
  80213e:	c3                   	ret    

0080213f <spawnl>:
// Spawn, taking command-line arguments array directly on the stack.
// NOTE: Must have a sentinal of NULL at the end of the args
// (none of the args may be NULL).
int
spawnl(const char *prog, const char *arg0, ...)
{
  80213f:	55                   	push   %ebp
  802140:	89 e5                	mov    %esp,%ebp
  802142:	56                   	push   %esi
  802143:	53                   	push   %ebx
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  802144:	8d 55 10             	lea    0x10(%ebp),%edx
{
	// We calculate argc by advancing the args until we hit NULL.
	// The contract of the function guarantees that the last
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
  802147:	b8 00 00 00 00       	mov    $0x0,%eax
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  80214c:	eb 01                	jmp    80214f <spawnl+0x10>
		argc++;
  80214e:	40                   	inc    %eax
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  80214f:	83 c2 04             	add    $0x4,%edx
  802152:	83 7a fc 00          	cmpl   $0x0,-0x4(%edx)
  802156:	75 f6                	jne    80214e <spawnl+0xf>
		argc++;
	va_end(vl);

	// Now that we have the size of the args, do a second pass
	// and store the values in a VLA, which has the format of argv
	const char *argv[argc+2];
  802158:	8d 14 85 1a 00 00 00 	lea    0x1a(,%eax,4),%edx
  80215f:	83 e2 f0             	and    $0xfffffff0,%edx
  802162:	29 d4                	sub    %edx,%esp
  802164:	8d 54 24 03          	lea    0x3(%esp),%edx
  802168:	c1 ea 02             	shr    $0x2,%edx
  80216b:	8d 34 95 00 00 00 00 	lea    0x0(,%edx,4),%esi
  802172:	89 f3                	mov    %esi,%ebx
	argv[0] = arg0;
  802174:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  802177:	89 0c 95 00 00 00 00 	mov    %ecx,0x0(,%edx,4)
	argv[argc+1] = NULL;
  80217e:	c7 44 86 04 00 00 00 	movl   $0x0,0x4(%esi,%eax,4)
  802185:	00 
  802186:	89 c2                	mov    %eax,%edx

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  802188:	b8 00 00 00 00       	mov    $0x0,%eax
  80218d:	eb 08                	jmp    802197 <spawnl+0x58>
		argv[i+1] = va_arg(vl, const char *);
  80218f:	40                   	inc    %eax
  802190:	8b 4c 85 0c          	mov    0xc(%ebp,%eax,4),%ecx
  802194:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
	argv[0] = arg0;
	argv[argc+1] = NULL;

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  802197:	39 d0                	cmp    %edx,%eax
  802199:	75 f4                	jne    80218f <spawnl+0x50>
		argv[i+1] = va_arg(vl, const char *);
	va_end(vl);
	return spawn(prog, argv);
  80219b:	83 ec 08             	sub    $0x8,%esp
  80219e:	56                   	push   %esi
  80219f:	ff 75 08             	pushl  0x8(%ebp)
  8021a2:	e8 20 fa ff ff       	call   801bc7 <spawn>
}
  8021a7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8021aa:	5b                   	pop    %ebx
  8021ab:	5e                   	pop    %esi
  8021ac:	5d                   	pop    %ebp
  8021ad:	c3                   	ret    

008021ae <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  8021ae:	55                   	push   %ebp
  8021af:	89 e5                	mov    %esp,%ebp
  8021b1:	56                   	push   %esi
  8021b2:	53                   	push   %ebx
  8021b3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  8021b6:	83 ec 0c             	sub    $0xc,%esp
  8021b9:	ff 75 08             	pushl  0x8(%ebp)
  8021bc:	e8 35 f2 ff ff       	call   8013f6 <fd2data>
  8021c1:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  8021c3:	83 c4 08             	add    $0x8,%esp
  8021c6:	68 d2 30 80 00       	push   $0x8030d2
  8021cb:	53                   	push   %ebx
  8021cc:	e8 4b e9 ff ff       	call   800b1c <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  8021d1:	8b 46 04             	mov    0x4(%esi),%eax
  8021d4:	2b 06                	sub    (%esi),%eax
  8021d6:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  8021dc:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8021e3:	00 00 00 
	stat->st_dev = &devpipe;
  8021e6:	c7 83 88 00 00 00 3c 	movl   $0x80403c,0x88(%ebx)
  8021ed:	40 80 00 
	return 0;
}
  8021f0:	b8 00 00 00 00       	mov    $0x0,%eax
  8021f5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8021f8:	5b                   	pop    %ebx
  8021f9:	5e                   	pop    %esi
  8021fa:	5d                   	pop    %ebp
  8021fb:	c3                   	ret    

008021fc <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  8021fc:	55                   	push   %ebp
  8021fd:	89 e5                	mov    %esp,%ebp
  8021ff:	53                   	push   %ebx
  802200:	83 ec 0c             	sub    $0xc,%esp
  802203:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  802206:	53                   	push   %ebx
  802207:	6a 00                	push   $0x0
  802209:	e8 77 ed ff ff       	call   800f85 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  80220e:	89 1c 24             	mov    %ebx,(%esp)
  802211:	e8 e0 f1 ff ff       	call   8013f6 <fd2data>
  802216:	83 c4 08             	add    $0x8,%esp
  802219:	50                   	push   %eax
  80221a:	6a 00                	push   $0x0
  80221c:	e8 64 ed ff ff       	call   800f85 <sys_page_unmap>
}
  802221:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802224:	c9                   	leave  
  802225:	c3                   	ret    

00802226 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  802226:	55                   	push   %ebp
  802227:	89 e5                	mov    %esp,%ebp
  802229:	57                   	push   %edi
  80222a:	56                   	push   %esi
  80222b:	53                   	push   %ebx
  80222c:	83 ec 1c             	sub    $0x1c,%esp
  80222f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  802232:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  802234:	a1 04 50 80 00       	mov    0x805004,%eax
  802239:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  80223c:	83 ec 0c             	sub    $0xc,%esp
  80223f:	ff 75 e0             	pushl  -0x20(%ebp)
  802242:	e8 82 04 00 00       	call   8026c9 <pageref>
  802247:	89 c3                	mov    %eax,%ebx
  802249:	89 3c 24             	mov    %edi,(%esp)
  80224c:	e8 78 04 00 00       	call   8026c9 <pageref>
  802251:	83 c4 10             	add    $0x10,%esp
  802254:	39 c3                	cmp    %eax,%ebx
  802256:	0f 94 c1             	sete   %cl
  802259:	0f b6 c9             	movzbl %cl,%ecx
  80225c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  80225f:	8b 15 04 50 80 00    	mov    0x805004,%edx
  802265:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  802268:	39 ce                	cmp    %ecx,%esi
  80226a:	74 1b                	je     802287 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  80226c:	39 c3                	cmp    %eax,%ebx
  80226e:	75 c4                	jne    802234 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  802270:	8b 42 58             	mov    0x58(%edx),%eax
  802273:	ff 75 e4             	pushl  -0x1c(%ebp)
  802276:	50                   	push   %eax
  802277:	56                   	push   %esi
  802278:	68 d9 30 80 00       	push   $0x8030d9
  80227d:	e8 36 e3 ff ff       	call   8005b8 <cprintf>
  802282:	83 c4 10             	add    $0x10,%esp
  802285:	eb ad                	jmp    802234 <_pipeisclosed+0xe>
	}
}
  802287:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  80228a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80228d:	5b                   	pop    %ebx
  80228e:	5e                   	pop    %esi
  80228f:	5f                   	pop    %edi
  802290:	5d                   	pop    %ebp
  802291:	c3                   	ret    

00802292 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  802292:	55                   	push   %ebp
  802293:	89 e5                	mov    %esp,%ebp
  802295:	57                   	push   %edi
  802296:	56                   	push   %esi
  802297:	53                   	push   %ebx
  802298:	83 ec 18             	sub    $0x18,%esp
  80229b:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  80229e:	56                   	push   %esi
  80229f:	e8 52 f1 ff ff       	call   8013f6 <fd2data>
  8022a4:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8022a6:	83 c4 10             	add    $0x10,%esp
  8022a9:	bf 00 00 00 00       	mov    $0x0,%edi
  8022ae:	eb 3b                	jmp    8022eb <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  8022b0:	89 da                	mov    %ebx,%edx
  8022b2:	89 f0                	mov    %esi,%eax
  8022b4:	e8 6d ff ff ff       	call   802226 <_pipeisclosed>
  8022b9:	85 c0                	test   %eax,%eax
  8022bb:	75 38                	jne    8022f5 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  8022bd:	e8 1f ec ff ff       	call   800ee1 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  8022c2:	8b 53 04             	mov    0x4(%ebx),%edx
  8022c5:	8b 03                	mov    (%ebx),%eax
  8022c7:	83 c0 20             	add    $0x20,%eax
  8022ca:	39 c2                	cmp    %eax,%edx
  8022cc:	73 e2                	jae    8022b0 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  8022ce:	8b 45 0c             	mov    0xc(%ebp),%eax
  8022d1:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  8022d4:	89 d0                	mov    %edx,%eax
  8022d6:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8022db:	79 05                	jns    8022e2 <devpipe_write+0x50>
  8022dd:	48                   	dec    %eax
  8022de:	83 c8 e0             	or     $0xffffffe0,%eax
  8022e1:	40                   	inc    %eax
  8022e2:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  8022e6:	42                   	inc    %edx
  8022e7:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8022ea:	47                   	inc    %edi
  8022eb:	3b 7d 10             	cmp    0x10(%ebp),%edi
  8022ee:	75 d2                	jne    8022c2 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  8022f0:	8b 45 10             	mov    0x10(%ebp),%eax
  8022f3:	eb 05                	jmp    8022fa <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8022f5:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  8022fa:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8022fd:	5b                   	pop    %ebx
  8022fe:	5e                   	pop    %esi
  8022ff:	5f                   	pop    %edi
  802300:	5d                   	pop    %ebp
  802301:	c3                   	ret    

00802302 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  802302:	55                   	push   %ebp
  802303:	89 e5                	mov    %esp,%ebp
  802305:	57                   	push   %edi
  802306:	56                   	push   %esi
  802307:	53                   	push   %ebx
  802308:	83 ec 18             	sub    $0x18,%esp
  80230b:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  80230e:	57                   	push   %edi
  80230f:	e8 e2 f0 ff ff       	call   8013f6 <fd2data>
  802314:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802316:	83 c4 10             	add    $0x10,%esp
  802319:	bb 00 00 00 00       	mov    $0x0,%ebx
  80231e:	eb 3a                	jmp    80235a <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  802320:	85 db                	test   %ebx,%ebx
  802322:	74 04                	je     802328 <devpipe_read+0x26>
				return i;
  802324:	89 d8                	mov    %ebx,%eax
  802326:	eb 41                	jmp    802369 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  802328:	89 f2                	mov    %esi,%edx
  80232a:	89 f8                	mov    %edi,%eax
  80232c:	e8 f5 fe ff ff       	call   802226 <_pipeisclosed>
  802331:	85 c0                	test   %eax,%eax
  802333:	75 2f                	jne    802364 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  802335:	e8 a7 eb ff ff       	call   800ee1 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  80233a:	8b 06                	mov    (%esi),%eax
  80233c:	3b 46 04             	cmp    0x4(%esi),%eax
  80233f:	74 df                	je     802320 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  802341:	25 1f 00 00 80       	and    $0x8000001f,%eax
  802346:	79 05                	jns    80234d <devpipe_read+0x4b>
  802348:	48                   	dec    %eax
  802349:	83 c8 e0             	or     $0xffffffe0,%eax
  80234c:	40                   	inc    %eax
  80234d:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  802351:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  802354:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  802357:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802359:	43                   	inc    %ebx
  80235a:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  80235d:	75 db                	jne    80233a <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  80235f:	8b 45 10             	mov    0x10(%ebp),%eax
  802362:	eb 05                	jmp    802369 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  802364:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  802369:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80236c:	5b                   	pop    %ebx
  80236d:	5e                   	pop    %esi
  80236e:	5f                   	pop    %edi
  80236f:	5d                   	pop    %ebp
  802370:	c3                   	ret    

00802371 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  802371:	55                   	push   %ebp
  802372:	89 e5                	mov    %esp,%ebp
  802374:	56                   	push   %esi
  802375:	53                   	push   %ebx
  802376:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  802379:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80237c:	50                   	push   %eax
  80237d:	e8 8b f0 ff ff       	call   80140d <fd_alloc>
  802382:	83 c4 10             	add    $0x10,%esp
  802385:	85 c0                	test   %eax,%eax
  802387:	0f 88 2a 01 00 00    	js     8024b7 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80238d:	83 ec 04             	sub    $0x4,%esp
  802390:	68 07 04 00 00       	push   $0x407
  802395:	ff 75 f4             	pushl  -0xc(%ebp)
  802398:	6a 00                	push   $0x0
  80239a:	e8 61 eb ff ff       	call   800f00 <sys_page_alloc>
  80239f:	83 c4 10             	add    $0x10,%esp
  8023a2:	85 c0                	test   %eax,%eax
  8023a4:	0f 88 0d 01 00 00    	js     8024b7 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  8023aa:	83 ec 0c             	sub    $0xc,%esp
  8023ad:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8023b0:	50                   	push   %eax
  8023b1:	e8 57 f0 ff ff       	call   80140d <fd_alloc>
  8023b6:	89 c3                	mov    %eax,%ebx
  8023b8:	83 c4 10             	add    $0x10,%esp
  8023bb:	85 c0                	test   %eax,%eax
  8023bd:	0f 88 e2 00 00 00    	js     8024a5 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8023c3:	83 ec 04             	sub    $0x4,%esp
  8023c6:	68 07 04 00 00       	push   $0x407
  8023cb:	ff 75 f0             	pushl  -0x10(%ebp)
  8023ce:	6a 00                	push   $0x0
  8023d0:	e8 2b eb ff ff       	call   800f00 <sys_page_alloc>
  8023d5:	89 c3                	mov    %eax,%ebx
  8023d7:	83 c4 10             	add    $0x10,%esp
  8023da:	85 c0                	test   %eax,%eax
  8023dc:	0f 88 c3 00 00 00    	js     8024a5 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  8023e2:	83 ec 0c             	sub    $0xc,%esp
  8023e5:	ff 75 f4             	pushl  -0xc(%ebp)
  8023e8:	e8 09 f0 ff ff       	call   8013f6 <fd2data>
  8023ed:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8023ef:	83 c4 0c             	add    $0xc,%esp
  8023f2:	68 07 04 00 00       	push   $0x407
  8023f7:	50                   	push   %eax
  8023f8:	6a 00                	push   $0x0
  8023fa:	e8 01 eb ff ff       	call   800f00 <sys_page_alloc>
  8023ff:	89 c3                	mov    %eax,%ebx
  802401:	83 c4 10             	add    $0x10,%esp
  802404:	85 c0                	test   %eax,%eax
  802406:	0f 88 89 00 00 00    	js     802495 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80240c:	83 ec 0c             	sub    $0xc,%esp
  80240f:	ff 75 f0             	pushl  -0x10(%ebp)
  802412:	e8 df ef ff ff       	call   8013f6 <fd2data>
  802417:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  80241e:	50                   	push   %eax
  80241f:	6a 00                	push   $0x0
  802421:	56                   	push   %esi
  802422:	6a 00                	push   $0x0
  802424:	e8 1a eb ff ff       	call   800f43 <sys_page_map>
  802429:	89 c3                	mov    %eax,%ebx
  80242b:	83 c4 20             	add    $0x20,%esp
  80242e:	85 c0                	test   %eax,%eax
  802430:	78 55                	js     802487 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  802432:	8b 15 3c 40 80 00    	mov    0x80403c,%edx
  802438:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80243b:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  80243d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802440:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  802447:	8b 15 3c 40 80 00    	mov    0x80403c,%edx
  80244d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802450:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  802452:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802455:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  80245c:	83 ec 0c             	sub    $0xc,%esp
  80245f:	ff 75 f4             	pushl  -0xc(%ebp)
  802462:	e8 7f ef ff ff       	call   8013e6 <fd2num>
  802467:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80246a:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  80246c:	83 c4 04             	add    $0x4,%esp
  80246f:	ff 75 f0             	pushl  -0x10(%ebp)
  802472:	e8 6f ef ff ff       	call   8013e6 <fd2num>
  802477:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80247a:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  80247d:	83 c4 10             	add    $0x10,%esp
  802480:	b8 00 00 00 00       	mov    $0x0,%eax
  802485:	eb 30                	jmp    8024b7 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  802487:	83 ec 08             	sub    $0x8,%esp
  80248a:	56                   	push   %esi
  80248b:	6a 00                	push   $0x0
  80248d:	e8 f3 ea ff ff       	call   800f85 <sys_page_unmap>
  802492:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  802495:	83 ec 08             	sub    $0x8,%esp
  802498:	ff 75 f0             	pushl  -0x10(%ebp)
  80249b:	6a 00                	push   $0x0
  80249d:	e8 e3 ea ff ff       	call   800f85 <sys_page_unmap>
  8024a2:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  8024a5:	83 ec 08             	sub    $0x8,%esp
  8024a8:	ff 75 f4             	pushl  -0xc(%ebp)
  8024ab:	6a 00                	push   $0x0
  8024ad:	e8 d3 ea ff ff       	call   800f85 <sys_page_unmap>
  8024b2:	83 c4 10             	add    $0x10,%esp
  8024b5:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  8024b7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8024ba:	5b                   	pop    %ebx
  8024bb:	5e                   	pop    %esi
  8024bc:	5d                   	pop    %ebp
  8024bd:	c3                   	ret    

008024be <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  8024be:	55                   	push   %ebp
  8024bf:	89 e5                	mov    %esp,%ebp
  8024c1:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8024c4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8024c7:	50                   	push   %eax
  8024c8:	ff 75 08             	pushl  0x8(%ebp)
  8024cb:	e8 8c ef ff ff       	call   80145c <fd_lookup>
  8024d0:	83 c4 10             	add    $0x10,%esp
  8024d3:	85 c0                	test   %eax,%eax
  8024d5:	78 18                	js     8024ef <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  8024d7:	83 ec 0c             	sub    $0xc,%esp
  8024da:	ff 75 f4             	pushl  -0xc(%ebp)
  8024dd:	e8 14 ef ff ff       	call   8013f6 <fd2data>
	return _pipeisclosed(fd, p);
  8024e2:	89 c2                	mov    %eax,%edx
  8024e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8024e7:	e8 3a fd ff ff       	call   802226 <_pipeisclosed>
  8024ec:	83 c4 10             	add    $0x10,%esp
}
  8024ef:	c9                   	leave  
  8024f0:	c3                   	ret    

008024f1 <wait>:
#include <inc/lib.h>

// Waits until 'envid' exits.
void
wait(envid_t envid)
{
  8024f1:	55                   	push   %ebp
  8024f2:	89 e5                	mov    %esp,%ebp
  8024f4:	56                   	push   %esi
  8024f5:	53                   	push   %ebx
  8024f6:	8b 75 08             	mov    0x8(%ebp),%esi
	const volatile struct Env *e;

	assert(envid != 0);
  8024f9:	85 f6                	test   %esi,%esi
  8024fb:	75 16                	jne    802513 <wait+0x22>
  8024fd:	68 f1 30 80 00       	push   $0x8030f1
  802502:	68 f3 2f 80 00       	push   $0x802ff3
  802507:	6a 09                	push   $0x9
  802509:	68 fc 30 80 00       	push   $0x8030fc
  80250e:	e8 cd df ff ff       	call   8004e0 <_panic>
	e = &envs[ENVX(envid)];
  802513:	89 f3                	mov    %esi,%ebx
  802515:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
	while (e->env_id == envid && e->env_status != ENV_FREE)
  80251b:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  802522:	c1 e3 07             	shl    $0x7,%ebx
  802525:	29 c3                	sub    %eax,%ebx
  802527:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  80252d:	eb 05                	jmp    802534 <wait+0x43>
		sys_yield();
  80252f:	e8 ad e9 ff ff       	call   800ee1 <sys_yield>
{
	const volatile struct Env *e;

	assert(envid != 0);
	e = &envs[ENVX(envid)];
	while (e->env_id == envid && e->env_status != ENV_FREE)
  802534:	8b 43 48             	mov    0x48(%ebx),%eax
  802537:	39 c6                	cmp    %eax,%esi
  802539:	75 07                	jne    802542 <wait+0x51>
  80253b:	8b 43 54             	mov    0x54(%ebx),%eax
  80253e:	85 c0                	test   %eax,%eax
  802540:	75 ed                	jne    80252f <wait+0x3e>
		sys_yield();
}
  802542:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802545:	5b                   	pop    %ebx
  802546:	5e                   	pop    %esi
  802547:	5d                   	pop    %ebp
  802548:	c3                   	ret    

00802549 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  802549:	55                   	push   %ebp
  80254a:	89 e5                	mov    %esp,%ebp
  80254c:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  80254f:	83 3d 00 70 80 00 00 	cmpl   $0x0,0x807000
  802556:	75 3e                	jne    802596 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  802558:	83 ec 04             	sub    $0x4,%esp
  80255b:	6a 07                	push   $0x7
  80255d:	68 00 f0 bf ee       	push   $0xeebff000
  802562:	6a 00                	push   $0x0
  802564:	e8 97 e9 ff ff       	call   800f00 <sys_page_alloc>
  802569:	83 c4 10             	add    $0x10,%esp
  80256c:	85 c0                	test   %eax,%eax
  80256e:	74 14                	je     802584 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  802570:	83 ec 04             	sub    $0x4,%esp
  802573:	68 08 31 80 00       	push   $0x803108
  802578:	6a 23                	push   $0x23
  80257a:	68 2c 31 80 00       	push   $0x80312c
  80257f:	e8 5c df ff ff       	call   8004e0 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  802584:	83 ec 08             	sub    $0x8,%esp
  802587:	68 a0 25 80 00       	push   $0x8025a0
  80258c:	6a 00                	push   $0x0
  80258e:	e8 d7 ea ff ff       	call   80106a <sys_env_set_pgfault_upcall>
  802593:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  802596:	8b 45 08             	mov    0x8(%ebp),%eax
  802599:	a3 00 70 80 00       	mov    %eax,0x807000
}
  80259e:	c9                   	leave  
  80259f:	c3                   	ret    

008025a0 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  8025a0:	54                   	push   %esp
	movl _pgfault_handler, %eax
  8025a1:	a1 00 70 80 00       	mov    0x807000,%eax
	call *%eax
  8025a6:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  8025a8:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  8025ab:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  8025ad:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  8025b1:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  8025b5:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  8025b6:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  8025b9:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  8025bb:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  8025be:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  8025c1:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  8025c2:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8025c5:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8025c6:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8025c7:	c3                   	ret    

008025c8 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  8025c8:	55                   	push   %ebp
  8025c9:	89 e5                	mov    %esp,%ebp
  8025cb:	56                   	push   %esi
  8025cc:	53                   	push   %ebx
  8025cd:	8b 75 08             	mov    0x8(%ebp),%esi
  8025d0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8025d3:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  8025d6:	85 c0                	test   %eax,%eax
  8025d8:	75 05                	jne    8025df <ipc_recv+0x17>
        pg = (void *)UTOP;
  8025da:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  8025df:	83 ec 0c             	sub    $0xc,%esp
  8025e2:	50                   	push   %eax
  8025e3:	e8 e7 ea ff ff       	call   8010cf <sys_ipc_recv>
  8025e8:	83 c4 10             	add    $0x10,%esp
  8025eb:	85 c0                	test   %eax,%eax
  8025ed:	74 16                	je     802605 <ipc_recv+0x3d>
        if (from_env_store)
  8025ef:	85 f6                	test   %esi,%esi
  8025f1:	74 06                	je     8025f9 <ipc_recv+0x31>
            *from_env_store = 0;
  8025f3:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  8025f9:	85 db                	test   %ebx,%ebx
  8025fb:	74 2c                	je     802629 <ipc_recv+0x61>
            *perm_store = 0;
  8025fd:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  802603:	eb 24                	jmp    802629 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  802605:	85 f6                	test   %esi,%esi
  802607:	74 0a                	je     802613 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  802609:	a1 04 50 80 00       	mov    0x805004,%eax
  80260e:	8b 40 74             	mov    0x74(%eax),%eax
  802611:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  802613:	85 db                	test   %ebx,%ebx
  802615:	74 0a                	je     802621 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  802617:	a1 04 50 80 00       	mov    0x805004,%eax
  80261c:	8b 40 78             	mov    0x78(%eax),%eax
  80261f:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  802621:	a1 04 50 80 00       	mov    0x805004,%eax
  802626:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  802629:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80262c:	5b                   	pop    %ebx
  80262d:	5e                   	pop    %esi
  80262e:	5d                   	pop    %ebp
  80262f:	c3                   	ret    

00802630 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  802630:	55                   	push   %ebp
  802631:	89 e5                	mov    %esp,%ebp
  802633:	57                   	push   %edi
  802634:	56                   	push   %esi
  802635:	53                   	push   %ebx
  802636:	83 ec 0c             	sub    $0xc,%esp
  802639:	8b 75 0c             	mov    0xc(%ebp),%esi
  80263c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80263f:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  802642:	85 db                	test   %ebx,%ebx
  802644:	75 0c                	jne    802652 <ipc_send+0x22>
        pg = (void *)UTOP;
  802646:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  80264b:	eb 05                	jmp    802652 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  80264d:	e8 8f e8 ff ff       	call   800ee1 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  802652:	57                   	push   %edi
  802653:	53                   	push   %ebx
  802654:	56                   	push   %esi
  802655:	ff 75 08             	pushl  0x8(%ebp)
  802658:	e8 4f ea ff ff       	call   8010ac <sys_ipc_try_send>
  80265d:	83 c4 10             	add    $0x10,%esp
  802660:	83 f8 f9             	cmp    $0xfffffff9,%eax
  802663:	74 e8                	je     80264d <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  802665:	85 c0                	test   %eax,%eax
  802667:	74 14                	je     80267d <ipc_send+0x4d>
        panic("ipc_send: error");
  802669:	83 ec 04             	sub    $0x4,%esp
  80266c:	68 3a 31 80 00       	push   $0x80313a
  802671:	6a 6a                	push   $0x6a
  802673:	68 4a 31 80 00       	push   $0x80314a
  802678:	e8 63 de ff ff       	call   8004e0 <_panic>
    }
	//panic("ipc_send not implemented");
}
  80267d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802680:	5b                   	pop    %ebx
  802681:	5e                   	pop    %esi
  802682:	5f                   	pop    %edi
  802683:	5d                   	pop    %ebp
  802684:	c3                   	ret    

00802685 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  802685:	55                   	push   %ebp
  802686:	89 e5                	mov    %esp,%ebp
  802688:	53                   	push   %ebx
  802689:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  80268c:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  802691:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  802698:	89 d0                	mov    %edx,%eax
  80269a:	c1 e0 07             	shl    $0x7,%eax
  80269d:	29 d8                	sub    %ebx,%eax
  80269f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8026a4:	8b 40 50             	mov    0x50(%eax),%eax
  8026a7:	39 c8                	cmp    %ecx,%eax
  8026a9:	75 0d                	jne    8026b8 <ipc_find_env+0x33>
			return envs[i].env_id;
  8026ab:	c1 e2 07             	shl    $0x7,%edx
  8026ae:	29 da                	sub    %ebx,%edx
  8026b0:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  8026b6:	eb 0e                	jmp    8026c6 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  8026b8:	42                   	inc    %edx
  8026b9:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  8026bf:	75 d0                	jne    802691 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  8026c1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8026c6:	5b                   	pop    %ebx
  8026c7:	5d                   	pop    %ebp
  8026c8:	c3                   	ret    

008026c9 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  8026c9:	55                   	push   %ebp
  8026ca:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  8026cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8026cf:	c1 e8 16             	shr    $0x16,%eax
  8026d2:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8026d9:	a8 01                	test   $0x1,%al
  8026db:	74 21                	je     8026fe <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  8026dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8026e0:	c1 e8 0c             	shr    $0xc,%eax
  8026e3:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  8026ea:	a8 01                	test   $0x1,%al
  8026ec:	74 17                	je     802705 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  8026ee:	c1 e8 0c             	shr    $0xc,%eax
  8026f1:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  8026f8:	ef 
  8026f9:	0f b7 c0             	movzwl %ax,%eax
  8026fc:	eb 0c                	jmp    80270a <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  8026fe:	b8 00 00 00 00       	mov    $0x0,%eax
  802703:	eb 05                	jmp    80270a <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  802705:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  80270a:	5d                   	pop    %ebp
  80270b:	c3                   	ret    

0080270c <__udivdi3>:
  80270c:	55                   	push   %ebp
  80270d:	57                   	push   %edi
  80270e:	56                   	push   %esi
  80270f:	53                   	push   %ebx
  802710:	83 ec 1c             	sub    $0x1c,%esp
  802713:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  802717:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  80271b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80271f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  802723:	89 ca                	mov    %ecx,%edx
  802725:	89 f8                	mov    %edi,%eax
  802727:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  80272b:	85 f6                	test   %esi,%esi
  80272d:	75 2d                	jne    80275c <__udivdi3+0x50>
  80272f:	39 cf                	cmp    %ecx,%edi
  802731:	77 65                	ja     802798 <__udivdi3+0x8c>
  802733:	89 fd                	mov    %edi,%ebp
  802735:	85 ff                	test   %edi,%edi
  802737:	75 0b                	jne    802744 <__udivdi3+0x38>
  802739:	b8 01 00 00 00       	mov    $0x1,%eax
  80273e:	31 d2                	xor    %edx,%edx
  802740:	f7 f7                	div    %edi
  802742:	89 c5                	mov    %eax,%ebp
  802744:	31 d2                	xor    %edx,%edx
  802746:	89 c8                	mov    %ecx,%eax
  802748:	f7 f5                	div    %ebp
  80274a:	89 c1                	mov    %eax,%ecx
  80274c:	89 d8                	mov    %ebx,%eax
  80274e:	f7 f5                	div    %ebp
  802750:	89 cf                	mov    %ecx,%edi
  802752:	89 fa                	mov    %edi,%edx
  802754:	83 c4 1c             	add    $0x1c,%esp
  802757:	5b                   	pop    %ebx
  802758:	5e                   	pop    %esi
  802759:	5f                   	pop    %edi
  80275a:	5d                   	pop    %ebp
  80275b:	c3                   	ret    
  80275c:	39 ce                	cmp    %ecx,%esi
  80275e:	77 28                	ja     802788 <__udivdi3+0x7c>
  802760:	0f bd fe             	bsr    %esi,%edi
  802763:	83 f7 1f             	xor    $0x1f,%edi
  802766:	75 40                	jne    8027a8 <__udivdi3+0x9c>
  802768:	39 ce                	cmp    %ecx,%esi
  80276a:	72 0a                	jb     802776 <__udivdi3+0x6a>
  80276c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  802770:	0f 87 9e 00 00 00    	ja     802814 <__udivdi3+0x108>
  802776:	b8 01 00 00 00       	mov    $0x1,%eax
  80277b:	89 fa                	mov    %edi,%edx
  80277d:	83 c4 1c             	add    $0x1c,%esp
  802780:	5b                   	pop    %ebx
  802781:	5e                   	pop    %esi
  802782:	5f                   	pop    %edi
  802783:	5d                   	pop    %ebp
  802784:	c3                   	ret    
  802785:	8d 76 00             	lea    0x0(%esi),%esi
  802788:	31 ff                	xor    %edi,%edi
  80278a:	31 c0                	xor    %eax,%eax
  80278c:	89 fa                	mov    %edi,%edx
  80278e:	83 c4 1c             	add    $0x1c,%esp
  802791:	5b                   	pop    %ebx
  802792:	5e                   	pop    %esi
  802793:	5f                   	pop    %edi
  802794:	5d                   	pop    %ebp
  802795:	c3                   	ret    
  802796:	66 90                	xchg   %ax,%ax
  802798:	89 d8                	mov    %ebx,%eax
  80279a:	f7 f7                	div    %edi
  80279c:	31 ff                	xor    %edi,%edi
  80279e:	89 fa                	mov    %edi,%edx
  8027a0:	83 c4 1c             	add    $0x1c,%esp
  8027a3:	5b                   	pop    %ebx
  8027a4:	5e                   	pop    %esi
  8027a5:	5f                   	pop    %edi
  8027a6:	5d                   	pop    %ebp
  8027a7:	c3                   	ret    
  8027a8:	bd 20 00 00 00       	mov    $0x20,%ebp
  8027ad:	89 eb                	mov    %ebp,%ebx
  8027af:	29 fb                	sub    %edi,%ebx
  8027b1:	89 f9                	mov    %edi,%ecx
  8027b3:	d3 e6                	shl    %cl,%esi
  8027b5:	89 c5                	mov    %eax,%ebp
  8027b7:	88 d9                	mov    %bl,%cl
  8027b9:	d3 ed                	shr    %cl,%ebp
  8027bb:	89 e9                	mov    %ebp,%ecx
  8027bd:	09 f1                	or     %esi,%ecx
  8027bf:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8027c3:	89 f9                	mov    %edi,%ecx
  8027c5:	d3 e0                	shl    %cl,%eax
  8027c7:	89 c5                	mov    %eax,%ebp
  8027c9:	89 d6                	mov    %edx,%esi
  8027cb:	88 d9                	mov    %bl,%cl
  8027cd:	d3 ee                	shr    %cl,%esi
  8027cf:	89 f9                	mov    %edi,%ecx
  8027d1:	d3 e2                	shl    %cl,%edx
  8027d3:	8b 44 24 08          	mov    0x8(%esp),%eax
  8027d7:	88 d9                	mov    %bl,%cl
  8027d9:	d3 e8                	shr    %cl,%eax
  8027db:	09 c2                	or     %eax,%edx
  8027dd:	89 d0                	mov    %edx,%eax
  8027df:	89 f2                	mov    %esi,%edx
  8027e1:	f7 74 24 0c          	divl   0xc(%esp)
  8027e5:	89 d6                	mov    %edx,%esi
  8027e7:	89 c3                	mov    %eax,%ebx
  8027e9:	f7 e5                	mul    %ebp
  8027eb:	39 d6                	cmp    %edx,%esi
  8027ed:	72 19                	jb     802808 <__udivdi3+0xfc>
  8027ef:	74 0b                	je     8027fc <__udivdi3+0xf0>
  8027f1:	89 d8                	mov    %ebx,%eax
  8027f3:	31 ff                	xor    %edi,%edi
  8027f5:	e9 58 ff ff ff       	jmp    802752 <__udivdi3+0x46>
  8027fa:	66 90                	xchg   %ax,%ax
  8027fc:	8b 54 24 08          	mov    0x8(%esp),%edx
  802800:	89 f9                	mov    %edi,%ecx
  802802:	d3 e2                	shl    %cl,%edx
  802804:	39 c2                	cmp    %eax,%edx
  802806:	73 e9                	jae    8027f1 <__udivdi3+0xe5>
  802808:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80280b:	31 ff                	xor    %edi,%edi
  80280d:	e9 40 ff ff ff       	jmp    802752 <__udivdi3+0x46>
  802812:	66 90                	xchg   %ax,%ax
  802814:	31 c0                	xor    %eax,%eax
  802816:	e9 37 ff ff ff       	jmp    802752 <__udivdi3+0x46>
  80281b:	90                   	nop

0080281c <__umoddi3>:
  80281c:	55                   	push   %ebp
  80281d:	57                   	push   %edi
  80281e:	56                   	push   %esi
  80281f:	53                   	push   %ebx
  802820:	83 ec 1c             	sub    $0x1c,%esp
  802823:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  802827:	8b 74 24 34          	mov    0x34(%esp),%esi
  80282b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80282f:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  802833:	89 44 24 0c          	mov    %eax,0xc(%esp)
  802837:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80283b:	89 f3                	mov    %esi,%ebx
  80283d:	89 fa                	mov    %edi,%edx
  80283f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  802843:	89 34 24             	mov    %esi,(%esp)
  802846:	85 c0                	test   %eax,%eax
  802848:	75 1a                	jne    802864 <__umoddi3+0x48>
  80284a:	39 f7                	cmp    %esi,%edi
  80284c:	0f 86 a2 00 00 00    	jbe    8028f4 <__umoddi3+0xd8>
  802852:	89 c8                	mov    %ecx,%eax
  802854:	89 f2                	mov    %esi,%edx
  802856:	f7 f7                	div    %edi
  802858:	89 d0                	mov    %edx,%eax
  80285a:	31 d2                	xor    %edx,%edx
  80285c:	83 c4 1c             	add    $0x1c,%esp
  80285f:	5b                   	pop    %ebx
  802860:	5e                   	pop    %esi
  802861:	5f                   	pop    %edi
  802862:	5d                   	pop    %ebp
  802863:	c3                   	ret    
  802864:	39 f0                	cmp    %esi,%eax
  802866:	0f 87 ac 00 00 00    	ja     802918 <__umoddi3+0xfc>
  80286c:	0f bd e8             	bsr    %eax,%ebp
  80286f:	83 f5 1f             	xor    $0x1f,%ebp
  802872:	0f 84 ac 00 00 00    	je     802924 <__umoddi3+0x108>
  802878:	bf 20 00 00 00       	mov    $0x20,%edi
  80287d:	29 ef                	sub    %ebp,%edi
  80287f:	89 fe                	mov    %edi,%esi
  802881:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802885:	89 e9                	mov    %ebp,%ecx
  802887:	d3 e0                	shl    %cl,%eax
  802889:	89 d7                	mov    %edx,%edi
  80288b:	89 f1                	mov    %esi,%ecx
  80288d:	d3 ef                	shr    %cl,%edi
  80288f:	09 c7                	or     %eax,%edi
  802891:	89 e9                	mov    %ebp,%ecx
  802893:	d3 e2                	shl    %cl,%edx
  802895:	89 14 24             	mov    %edx,(%esp)
  802898:	89 d8                	mov    %ebx,%eax
  80289a:	d3 e0                	shl    %cl,%eax
  80289c:	89 c2                	mov    %eax,%edx
  80289e:	8b 44 24 08          	mov    0x8(%esp),%eax
  8028a2:	d3 e0                	shl    %cl,%eax
  8028a4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8028a8:	8b 44 24 08          	mov    0x8(%esp),%eax
  8028ac:	89 f1                	mov    %esi,%ecx
  8028ae:	d3 e8                	shr    %cl,%eax
  8028b0:	09 d0                	or     %edx,%eax
  8028b2:	d3 eb                	shr    %cl,%ebx
  8028b4:	89 da                	mov    %ebx,%edx
  8028b6:	f7 f7                	div    %edi
  8028b8:	89 d3                	mov    %edx,%ebx
  8028ba:	f7 24 24             	mull   (%esp)
  8028bd:	89 c6                	mov    %eax,%esi
  8028bf:	89 d1                	mov    %edx,%ecx
  8028c1:	39 d3                	cmp    %edx,%ebx
  8028c3:	0f 82 87 00 00 00    	jb     802950 <__umoddi3+0x134>
  8028c9:	0f 84 91 00 00 00    	je     802960 <__umoddi3+0x144>
  8028cf:	8b 54 24 04          	mov    0x4(%esp),%edx
  8028d3:	29 f2                	sub    %esi,%edx
  8028d5:	19 cb                	sbb    %ecx,%ebx
  8028d7:	89 d8                	mov    %ebx,%eax
  8028d9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8028dd:	d3 e0                	shl    %cl,%eax
  8028df:	89 e9                	mov    %ebp,%ecx
  8028e1:	d3 ea                	shr    %cl,%edx
  8028e3:	09 d0                	or     %edx,%eax
  8028e5:	89 e9                	mov    %ebp,%ecx
  8028e7:	d3 eb                	shr    %cl,%ebx
  8028e9:	89 da                	mov    %ebx,%edx
  8028eb:	83 c4 1c             	add    $0x1c,%esp
  8028ee:	5b                   	pop    %ebx
  8028ef:	5e                   	pop    %esi
  8028f0:	5f                   	pop    %edi
  8028f1:	5d                   	pop    %ebp
  8028f2:	c3                   	ret    
  8028f3:	90                   	nop
  8028f4:	89 fd                	mov    %edi,%ebp
  8028f6:	85 ff                	test   %edi,%edi
  8028f8:	75 0b                	jne    802905 <__umoddi3+0xe9>
  8028fa:	b8 01 00 00 00       	mov    $0x1,%eax
  8028ff:	31 d2                	xor    %edx,%edx
  802901:	f7 f7                	div    %edi
  802903:	89 c5                	mov    %eax,%ebp
  802905:	89 f0                	mov    %esi,%eax
  802907:	31 d2                	xor    %edx,%edx
  802909:	f7 f5                	div    %ebp
  80290b:	89 c8                	mov    %ecx,%eax
  80290d:	f7 f5                	div    %ebp
  80290f:	89 d0                	mov    %edx,%eax
  802911:	e9 44 ff ff ff       	jmp    80285a <__umoddi3+0x3e>
  802916:	66 90                	xchg   %ax,%ax
  802918:	89 c8                	mov    %ecx,%eax
  80291a:	89 f2                	mov    %esi,%edx
  80291c:	83 c4 1c             	add    $0x1c,%esp
  80291f:	5b                   	pop    %ebx
  802920:	5e                   	pop    %esi
  802921:	5f                   	pop    %edi
  802922:	5d                   	pop    %ebp
  802923:	c3                   	ret    
  802924:	3b 04 24             	cmp    (%esp),%eax
  802927:	72 06                	jb     80292f <__umoddi3+0x113>
  802929:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  80292d:	77 0f                	ja     80293e <__umoddi3+0x122>
  80292f:	89 f2                	mov    %esi,%edx
  802931:	29 f9                	sub    %edi,%ecx
  802933:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  802937:	89 14 24             	mov    %edx,(%esp)
  80293a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80293e:	8b 44 24 04          	mov    0x4(%esp),%eax
  802942:	8b 14 24             	mov    (%esp),%edx
  802945:	83 c4 1c             	add    $0x1c,%esp
  802948:	5b                   	pop    %ebx
  802949:	5e                   	pop    %esi
  80294a:	5f                   	pop    %edi
  80294b:	5d                   	pop    %ebp
  80294c:	c3                   	ret    
  80294d:	8d 76 00             	lea    0x0(%esi),%esi
  802950:	2b 04 24             	sub    (%esp),%eax
  802953:	19 fa                	sbb    %edi,%edx
  802955:	89 d1                	mov    %edx,%ecx
  802957:	89 c6                	mov    %eax,%esi
  802959:	e9 71 ff ff ff       	jmp    8028cf <__umoddi3+0xb3>
  80295e:	66 90                	xchg   %ax,%ax
  802960:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802964:	72 ea                	jb     802950 <__umoddi3+0x134>
  802966:	89 d9                	mov    %ebx,%ecx
  802968:	e9 62 ff ff ff       	jmp    8028cf <__umoddi3+0xb3>
