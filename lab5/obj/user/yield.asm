
obj/user/yield.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 67 00 00 00       	call   800098 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 0c             	sub    $0xc,%esp
	int i;

	cprintf("Hello, I am environment %08x.\n", thisenv->env_id);
  80003a:	a1 04 20 80 00       	mov    0x802004,%eax
  80003f:	8b 40 48             	mov    0x48(%eax),%eax
  800042:	50                   	push   %eax
  800043:	68 a0 0f 80 00       	push   $0x800fa0
  800048:	e8 3e 01 00 00       	call   80018b <cprintf>
  80004d:	83 c4 10             	add    $0x10,%esp
	for (i = 0; i < 5; i++) {
  800050:	bb 00 00 00 00       	mov    $0x0,%ebx
		sys_yield();
  800055:	e8 5a 0a 00 00       	call   800ab4 <sys_yield>
		cprintf("Back in environment %08x, iteration %d.\n",
			thisenv->env_id, i);
  80005a:	a1 04 20 80 00       	mov    0x802004,%eax
	int i;

	cprintf("Hello, I am environment %08x.\n", thisenv->env_id);
	for (i = 0; i < 5; i++) {
		sys_yield();
		cprintf("Back in environment %08x, iteration %d.\n",
  80005f:	8b 40 48             	mov    0x48(%eax),%eax
  800062:	83 ec 04             	sub    $0x4,%esp
  800065:	53                   	push   %ebx
  800066:	50                   	push   %eax
  800067:	68 c0 0f 80 00       	push   $0x800fc0
  80006c:	e8 1a 01 00 00       	call   80018b <cprintf>
umain(int argc, char **argv)
{
	int i;

	cprintf("Hello, I am environment %08x.\n", thisenv->env_id);
	for (i = 0; i < 5; i++) {
  800071:	43                   	inc    %ebx
  800072:	83 c4 10             	add    $0x10,%esp
  800075:	83 fb 05             	cmp    $0x5,%ebx
  800078:	75 db                	jne    800055 <umain+0x22>
		sys_yield();
		cprintf("Back in environment %08x, iteration %d.\n",
			thisenv->env_id, i);
	}
	cprintf("All done in environment %08x.\n", thisenv->env_id);
  80007a:	a1 04 20 80 00       	mov    0x802004,%eax
  80007f:	8b 40 48             	mov    0x48(%eax),%eax
  800082:	83 ec 08             	sub    $0x8,%esp
  800085:	50                   	push   %eax
  800086:	68 ec 0f 80 00       	push   $0x800fec
  80008b:	e8 fb 00 00 00       	call   80018b <cprintf>
}
  800090:	83 c4 10             	add    $0x10,%esp
  800093:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800096:	c9                   	leave  
  800097:	c3                   	ret    

00800098 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800098:	55                   	push   %ebp
  800099:	89 e5                	mov    %esp,%ebp
  80009b:	56                   	push   %esi
  80009c:	53                   	push   %ebx
  80009d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000a0:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000a3:	e8 ed 09 00 00       	call   800a95 <sys_getenvid>
  8000a8:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000ad:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000b4:	c1 e0 07             	shl    $0x7,%eax
  8000b7:	29 d0                	sub    %edx,%eax
  8000b9:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000be:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000c3:	85 db                	test   %ebx,%ebx
  8000c5:	7e 07                	jle    8000ce <libmain+0x36>
		binaryname = argv[0];
  8000c7:	8b 06                	mov    (%esi),%eax
  8000c9:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000ce:	83 ec 08             	sub    $0x8,%esp
  8000d1:	56                   	push   %esi
  8000d2:	53                   	push   %ebx
  8000d3:	e8 5b ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000d8:	e8 0a 00 00 00       	call   8000e7 <exit>
}
  8000dd:	83 c4 10             	add    $0x10,%esp
  8000e0:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000e3:	5b                   	pop    %ebx
  8000e4:	5e                   	pop    %esi
  8000e5:	5d                   	pop    %ebp
  8000e6:	c3                   	ret    

008000e7 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000e7:	55                   	push   %ebp
  8000e8:	89 e5                	mov    %esp,%ebp
  8000ea:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000ed:	6a 00                	push   $0x0
  8000ef:	e8 60 09 00 00       	call   800a54 <sys_env_destroy>
}
  8000f4:	83 c4 10             	add    $0x10,%esp
  8000f7:	c9                   	leave  
  8000f8:	c3                   	ret    

008000f9 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000f9:	55                   	push   %ebp
  8000fa:	89 e5                	mov    %esp,%ebp
  8000fc:	53                   	push   %ebx
  8000fd:	83 ec 04             	sub    $0x4,%esp
  800100:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800103:	8b 13                	mov    (%ebx),%edx
  800105:	8d 42 01             	lea    0x1(%edx),%eax
  800108:	89 03                	mov    %eax,(%ebx)
  80010a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80010d:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800111:	3d ff 00 00 00       	cmp    $0xff,%eax
  800116:	75 1a                	jne    800132 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800118:	83 ec 08             	sub    $0x8,%esp
  80011b:	68 ff 00 00 00       	push   $0xff
  800120:	8d 43 08             	lea    0x8(%ebx),%eax
  800123:	50                   	push   %eax
  800124:	e8 ee 08 00 00       	call   800a17 <sys_cputs>
		b->idx = 0;
  800129:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80012f:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800132:	ff 43 04             	incl   0x4(%ebx)
}
  800135:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800138:	c9                   	leave  
  800139:	c3                   	ret    

0080013a <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80013a:	55                   	push   %ebp
  80013b:	89 e5                	mov    %esp,%ebp
  80013d:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800143:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80014a:	00 00 00 
	b.cnt = 0;
  80014d:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800154:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800157:	ff 75 0c             	pushl  0xc(%ebp)
  80015a:	ff 75 08             	pushl  0x8(%ebp)
  80015d:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800163:	50                   	push   %eax
  800164:	68 f9 00 80 00       	push   $0x8000f9
  800169:	e8 51 01 00 00       	call   8002bf <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80016e:	83 c4 08             	add    $0x8,%esp
  800171:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800177:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80017d:	50                   	push   %eax
  80017e:	e8 94 08 00 00       	call   800a17 <sys_cputs>

	return b.cnt;
}
  800183:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800189:	c9                   	leave  
  80018a:	c3                   	ret    

0080018b <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80018b:	55                   	push   %ebp
  80018c:	89 e5                	mov    %esp,%ebp
  80018e:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800191:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800194:	50                   	push   %eax
  800195:	ff 75 08             	pushl  0x8(%ebp)
  800198:	e8 9d ff ff ff       	call   80013a <vcprintf>
	va_end(ap);

	return cnt;
}
  80019d:	c9                   	leave  
  80019e:	c3                   	ret    

0080019f <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80019f:	55                   	push   %ebp
  8001a0:	89 e5                	mov    %esp,%ebp
  8001a2:	57                   	push   %edi
  8001a3:	56                   	push   %esi
  8001a4:	53                   	push   %ebx
  8001a5:	83 ec 1c             	sub    $0x1c,%esp
  8001a8:	89 c7                	mov    %eax,%edi
  8001aa:	89 d6                	mov    %edx,%esi
  8001ac:	8b 45 08             	mov    0x8(%ebp),%eax
  8001af:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001b2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001b5:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001b8:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001bb:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001c0:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001c3:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001c6:	39 d3                	cmp    %edx,%ebx
  8001c8:	72 05                	jb     8001cf <printnum+0x30>
  8001ca:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001cd:	77 45                	ja     800214 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001cf:	83 ec 0c             	sub    $0xc,%esp
  8001d2:	ff 75 18             	pushl  0x18(%ebp)
  8001d5:	8b 45 14             	mov    0x14(%ebp),%eax
  8001d8:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001db:	53                   	push   %ebx
  8001dc:	ff 75 10             	pushl  0x10(%ebp)
  8001df:	83 ec 08             	sub    $0x8,%esp
  8001e2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001e5:	ff 75 e0             	pushl  -0x20(%ebp)
  8001e8:	ff 75 dc             	pushl  -0x24(%ebp)
  8001eb:	ff 75 d8             	pushl  -0x28(%ebp)
  8001ee:	e8 39 0b 00 00       	call   800d2c <__udivdi3>
  8001f3:	83 c4 18             	add    $0x18,%esp
  8001f6:	52                   	push   %edx
  8001f7:	50                   	push   %eax
  8001f8:	89 f2                	mov    %esi,%edx
  8001fa:	89 f8                	mov    %edi,%eax
  8001fc:	e8 9e ff ff ff       	call   80019f <printnum>
  800201:	83 c4 20             	add    $0x20,%esp
  800204:	eb 16                	jmp    80021c <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800206:	83 ec 08             	sub    $0x8,%esp
  800209:	56                   	push   %esi
  80020a:	ff 75 18             	pushl  0x18(%ebp)
  80020d:	ff d7                	call   *%edi
  80020f:	83 c4 10             	add    $0x10,%esp
  800212:	eb 03                	jmp    800217 <printnum+0x78>
  800214:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800217:	4b                   	dec    %ebx
  800218:	85 db                	test   %ebx,%ebx
  80021a:	7f ea                	jg     800206 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80021c:	83 ec 08             	sub    $0x8,%esp
  80021f:	56                   	push   %esi
  800220:	83 ec 04             	sub    $0x4,%esp
  800223:	ff 75 e4             	pushl  -0x1c(%ebp)
  800226:	ff 75 e0             	pushl  -0x20(%ebp)
  800229:	ff 75 dc             	pushl  -0x24(%ebp)
  80022c:	ff 75 d8             	pushl  -0x28(%ebp)
  80022f:	e8 08 0c 00 00       	call   800e3c <__umoddi3>
  800234:	83 c4 14             	add    $0x14,%esp
  800237:	0f be 80 15 10 80 00 	movsbl 0x801015(%eax),%eax
  80023e:	50                   	push   %eax
  80023f:	ff d7                	call   *%edi
}
  800241:	83 c4 10             	add    $0x10,%esp
  800244:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800247:	5b                   	pop    %ebx
  800248:	5e                   	pop    %esi
  800249:	5f                   	pop    %edi
  80024a:	5d                   	pop    %ebp
  80024b:	c3                   	ret    

0080024c <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80024c:	55                   	push   %ebp
  80024d:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80024f:	83 fa 01             	cmp    $0x1,%edx
  800252:	7e 0e                	jle    800262 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800254:	8b 10                	mov    (%eax),%edx
  800256:	8d 4a 08             	lea    0x8(%edx),%ecx
  800259:	89 08                	mov    %ecx,(%eax)
  80025b:	8b 02                	mov    (%edx),%eax
  80025d:	8b 52 04             	mov    0x4(%edx),%edx
  800260:	eb 22                	jmp    800284 <getuint+0x38>
	else if (lflag)
  800262:	85 d2                	test   %edx,%edx
  800264:	74 10                	je     800276 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800266:	8b 10                	mov    (%eax),%edx
  800268:	8d 4a 04             	lea    0x4(%edx),%ecx
  80026b:	89 08                	mov    %ecx,(%eax)
  80026d:	8b 02                	mov    (%edx),%eax
  80026f:	ba 00 00 00 00       	mov    $0x0,%edx
  800274:	eb 0e                	jmp    800284 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800276:	8b 10                	mov    (%eax),%edx
  800278:	8d 4a 04             	lea    0x4(%edx),%ecx
  80027b:	89 08                	mov    %ecx,(%eax)
  80027d:	8b 02                	mov    (%edx),%eax
  80027f:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800284:	5d                   	pop    %ebp
  800285:	c3                   	ret    

00800286 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800286:	55                   	push   %ebp
  800287:	89 e5                	mov    %esp,%ebp
  800289:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80028c:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80028f:	8b 10                	mov    (%eax),%edx
  800291:	3b 50 04             	cmp    0x4(%eax),%edx
  800294:	73 0a                	jae    8002a0 <sprintputch+0x1a>
		*b->buf++ = ch;
  800296:	8d 4a 01             	lea    0x1(%edx),%ecx
  800299:	89 08                	mov    %ecx,(%eax)
  80029b:	8b 45 08             	mov    0x8(%ebp),%eax
  80029e:	88 02                	mov    %al,(%edx)
}
  8002a0:	5d                   	pop    %ebp
  8002a1:	c3                   	ret    

008002a2 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002a2:	55                   	push   %ebp
  8002a3:	89 e5                	mov    %esp,%ebp
  8002a5:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002a8:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002ab:	50                   	push   %eax
  8002ac:	ff 75 10             	pushl  0x10(%ebp)
  8002af:	ff 75 0c             	pushl  0xc(%ebp)
  8002b2:	ff 75 08             	pushl  0x8(%ebp)
  8002b5:	e8 05 00 00 00       	call   8002bf <vprintfmt>
	va_end(ap);
}
  8002ba:	83 c4 10             	add    $0x10,%esp
  8002bd:	c9                   	leave  
  8002be:	c3                   	ret    

008002bf <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002bf:	55                   	push   %ebp
  8002c0:	89 e5                	mov    %esp,%ebp
  8002c2:	57                   	push   %edi
  8002c3:	56                   	push   %esi
  8002c4:	53                   	push   %ebx
  8002c5:	83 ec 2c             	sub    $0x2c,%esp
  8002c8:	8b 75 08             	mov    0x8(%ebp),%esi
  8002cb:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002ce:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002d1:	eb 12                	jmp    8002e5 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002d3:	85 c0                	test   %eax,%eax
  8002d5:	0f 84 68 03 00 00    	je     800643 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002db:	83 ec 08             	sub    $0x8,%esp
  8002de:	53                   	push   %ebx
  8002df:	50                   	push   %eax
  8002e0:	ff d6                	call   *%esi
  8002e2:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002e5:	47                   	inc    %edi
  8002e6:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002ea:	83 f8 25             	cmp    $0x25,%eax
  8002ed:	75 e4                	jne    8002d3 <vprintfmt+0x14>
  8002ef:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002f3:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002fa:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800301:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800308:	ba 00 00 00 00       	mov    $0x0,%edx
  80030d:	eb 07                	jmp    800316 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80030f:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800312:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800316:	8d 47 01             	lea    0x1(%edi),%eax
  800319:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80031c:	0f b6 0f             	movzbl (%edi),%ecx
  80031f:	8a 07                	mov    (%edi),%al
  800321:	83 e8 23             	sub    $0x23,%eax
  800324:	3c 55                	cmp    $0x55,%al
  800326:	0f 87 fe 02 00 00    	ja     80062a <vprintfmt+0x36b>
  80032c:	0f b6 c0             	movzbl %al,%eax
  80032f:	ff 24 85 60 11 80 00 	jmp    *0x801160(,%eax,4)
  800336:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800339:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80033d:	eb d7                	jmp    800316 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80033f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800342:	b8 00 00 00 00       	mov    $0x0,%eax
  800347:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80034a:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80034d:	01 c0                	add    %eax,%eax
  80034f:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800353:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800356:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800359:	83 fa 09             	cmp    $0x9,%edx
  80035c:	77 34                	ja     800392 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80035e:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80035f:	eb e9                	jmp    80034a <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800361:	8b 45 14             	mov    0x14(%ebp),%eax
  800364:	8d 48 04             	lea    0x4(%eax),%ecx
  800367:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80036a:	8b 00                	mov    (%eax),%eax
  80036c:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80036f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800372:	eb 24                	jmp    800398 <vprintfmt+0xd9>
  800374:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800378:	79 07                	jns    800381 <vprintfmt+0xc2>
  80037a:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800381:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800384:	eb 90                	jmp    800316 <vprintfmt+0x57>
  800386:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800389:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800390:	eb 84                	jmp    800316 <vprintfmt+0x57>
  800392:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800395:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800398:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80039c:	0f 89 74 ff ff ff    	jns    800316 <vprintfmt+0x57>
				width = precision, precision = -1;
  8003a2:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003a5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003a8:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003af:	e9 62 ff ff ff       	jmp    800316 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003b4:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003b8:	e9 59 ff ff ff       	jmp    800316 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003bd:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c0:	8d 50 04             	lea    0x4(%eax),%edx
  8003c3:	89 55 14             	mov    %edx,0x14(%ebp)
  8003c6:	83 ec 08             	sub    $0x8,%esp
  8003c9:	53                   	push   %ebx
  8003ca:	ff 30                	pushl  (%eax)
  8003cc:	ff d6                	call   *%esi
			break;
  8003ce:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003d4:	e9 0c ff ff ff       	jmp    8002e5 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003d9:	8b 45 14             	mov    0x14(%ebp),%eax
  8003dc:	8d 50 04             	lea    0x4(%eax),%edx
  8003df:	89 55 14             	mov    %edx,0x14(%ebp)
  8003e2:	8b 00                	mov    (%eax),%eax
  8003e4:	85 c0                	test   %eax,%eax
  8003e6:	79 02                	jns    8003ea <vprintfmt+0x12b>
  8003e8:	f7 d8                	neg    %eax
  8003ea:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003ec:	83 f8 0f             	cmp    $0xf,%eax
  8003ef:	7f 0b                	jg     8003fc <vprintfmt+0x13d>
  8003f1:	8b 04 85 c0 12 80 00 	mov    0x8012c0(,%eax,4),%eax
  8003f8:	85 c0                	test   %eax,%eax
  8003fa:	75 18                	jne    800414 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003fc:	52                   	push   %edx
  8003fd:	68 2d 10 80 00       	push   $0x80102d
  800402:	53                   	push   %ebx
  800403:	56                   	push   %esi
  800404:	e8 99 fe ff ff       	call   8002a2 <printfmt>
  800409:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040c:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80040f:	e9 d1 fe ff ff       	jmp    8002e5 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800414:	50                   	push   %eax
  800415:	68 36 10 80 00       	push   $0x801036
  80041a:	53                   	push   %ebx
  80041b:	56                   	push   %esi
  80041c:	e8 81 fe ff ff       	call   8002a2 <printfmt>
  800421:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800424:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800427:	e9 b9 fe ff ff       	jmp    8002e5 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80042c:	8b 45 14             	mov    0x14(%ebp),%eax
  80042f:	8d 50 04             	lea    0x4(%eax),%edx
  800432:	89 55 14             	mov    %edx,0x14(%ebp)
  800435:	8b 38                	mov    (%eax),%edi
  800437:	85 ff                	test   %edi,%edi
  800439:	75 05                	jne    800440 <vprintfmt+0x181>
				p = "(null)";
  80043b:	bf 26 10 80 00       	mov    $0x801026,%edi
			if (width > 0 && padc != '-')
  800440:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800444:	0f 8e 90 00 00 00    	jle    8004da <vprintfmt+0x21b>
  80044a:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80044e:	0f 84 8e 00 00 00    	je     8004e2 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800454:	83 ec 08             	sub    $0x8,%esp
  800457:	ff 75 d0             	pushl  -0x30(%ebp)
  80045a:	57                   	push   %edi
  80045b:	e8 70 02 00 00       	call   8006d0 <strnlen>
  800460:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800463:	29 c1                	sub    %eax,%ecx
  800465:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800468:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80046b:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80046f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800472:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800475:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800477:	eb 0d                	jmp    800486 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800479:	83 ec 08             	sub    $0x8,%esp
  80047c:	53                   	push   %ebx
  80047d:	ff 75 e4             	pushl  -0x1c(%ebp)
  800480:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800482:	4f                   	dec    %edi
  800483:	83 c4 10             	add    $0x10,%esp
  800486:	85 ff                	test   %edi,%edi
  800488:	7f ef                	jg     800479 <vprintfmt+0x1ba>
  80048a:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80048d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800490:	89 c8                	mov    %ecx,%eax
  800492:	85 c9                	test   %ecx,%ecx
  800494:	79 05                	jns    80049b <vprintfmt+0x1dc>
  800496:	b8 00 00 00 00       	mov    $0x0,%eax
  80049b:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80049e:	29 c1                	sub    %eax,%ecx
  8004a0:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004a3:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004a9:	eb 3d                	jmp    8004e8 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004ab:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004af:	74 19                	je     8004ca <vprintfmt+0x20b>
  8004b1:	0f be c0             	movsbl %al,%eax
  8004b4:	83 e8 20             	sub    $0x20,%eax
  8004b7:	83 f8 5e             	cmp    $0x5e,%eax
  8004ba:	76 0e                	jbe    8004ca <vprintfmt+0x20b>
					putch('?', putdat);
  8004bc:	83 ec 08             	sub    $0x8,%esp
  8004bf:	53                   	push   %ebx
  8004c0:	6a 3f                	push   $0x3f
  8004c2:	ff 55 08             	call   *0x8(%ebp)
  8004c5:	83 c4 10             	add    $0x10,%esp
  8004c8:	eb 0b                	jmp    8004d5 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004ca:	83 ec 08             	sub    $0x8,%esp
  8004cd:	53                   	push   %ebx
  8004ce:	52                   	push   %edx
  8004cf:	ff 55 08             	call   *0x8(%ebp)
  8004d2:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004d5:	ff 4d e4             	decl   -0x1c(%ebp)
  8004d8:	eb 0e                	jmp    8004e8 <vprintfmt+0x229>
  8004da:	89 75 08             	mov    %esi,0x8(%ebp)
  8004dd:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004e0:	eb 06                	jmp    8004e8 <vprintfmt+0x229>
  8004e2:	89 75 08             	mov    %esi,0x8(%ebp)
  8004e5:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004e8:	47                   	inc    %edi
  8004e9:	8a 47 ff             	mov    -0x1(%edi),%al
  8004ec:	0f be d0             	movsbl %al,%edx
  8004ef:	85 d2                	test   %edx,%edx
  8004f1:	74 1d                	je     800510 <vprintfmt+0x251>
  8004f3:	85 f6                	test   %esi,%esi
  8004f5:	78 b4                	js     8004ab <vprintfmt+0x1ec>
  8004f7:	4e                   	dec    %esi
  8004f8:	79 b1                	jns    8004ab <vprintfmt+0x1ec>
  8004fa:	8b 75 08             	mov    0x8(%ebp),%esi
  8004fd:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800500:	eb 14                	jmp    800516 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800502:	83 ec 08             	sub    $0x8,%esp
  800505:	53                   	push   %ebx
  800506:	6a 20                	push   $0x20
  800508:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80050a:	4f                   	dec    %edi
  80050b:	83 c4 10             	add    $0x10,%esp
  80050e:	eb 06                	jmp    800516 <vprintfmt+0x257>
  800510:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800513:	8b 75 08             	mov    0x8(%ebp),%esi
  800516:	85 ff                	test   %edi,%edi
  800518:	7f e8                	jg     800502 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80051a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80051d:	e9 c3 fd ff ff       	jmp    8002e5 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800522:	83 fa 01             	cmp    $0x1,%edx
  800525:	7e 16                	jle    80053d <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800527:	8b 45 14             	mov    0x14(%ebp),%eax
  80052a:	8d 50 08             	lea    0x8(%eax),%edx
  80052d:	89 55 14             	mov    %edx,0x14(%ebp)
  800530:	8b 50 04             	mov    0x4(%eax),%edx
  800533:	8b 00                	mov    (%eax),%eax
  800535:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800538:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80053b:	eb 32                	jmp    80056f <vprintfmt+0x2b0>
	else if (lflag)
  80053d:	85 d2                	test   %edx,%edx
  80053f:	74 18                	je     800559 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800541:	8b 45 14             	mov    0x14(%ebp),%eax
  800544:	8d 50 04             	lea    0x4(%eax),%edx
  800547:	89 55 14             	mov    %edx,0x14(%ebp)
  80054a:	8b 00                	mov    (%eax),%eax
  80054c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80054f:	89 c1                	mov    %eax,%ecx
  800551:	c1 f9 1f             	sar    $0x1f,%ecx
  800554:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800557:	eb 16                	jmp    80056f <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800559:	8b 45 14             	mov    0x14(%ebp),%eax
  80055c:	8d 50 04             	lea    0x4(%eax),%edx
  80055f:	89 55 14             	mov    %edx,0x14(%ebp)
  800562:	8b 00                	mov    (%eax),%eax
  800564:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800567:	89 c1                	mov    %eax,%ecx
  800569:	c1 f9 1f             	sar    $0x1f,%ecx
  80056c:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80056f:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800572:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800575:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800579:	79 76                	jns    8005f1 <vprintfmt+0x332>
				putch('-', putdat);
  80057b:	83 ec 08             	sub    $0x8,%esp
  80057e:	53                   	push   %ebx
  80057f:	6a 2d                	push   $0x2d
  800581:	ff d6                	call   *%esi
				num = -(long long) num;
  800583:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800586:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800589:	f7 d8                	neg    %eax
  80058b:	83 d2 00             	adc    $0x0,%edx
  80058e:	f7 da                	neg    %edx
  800590:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800593:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800598:	eb 5c                	jmp    8005f6 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80059a:	8d 45 14             	lea    0x14(%ebp),%eax
  80059d:	e8 aa fc ff ff       	call   80024c <getuint>
			base = 10;
  8005a2:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005a7:	eb 4d                	jmp    8005f6 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005a9:	8d 45 14             	lea    0x14(%ebp),%eax
  8005ac:	e8 9b fc ff ff       	call   80024c <getuint>
			base = 8;
  8005b1:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005b6:	eb 3e                	jmp    8005f6 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005b8:	83 ec 08             	sub    $0x8,%esp
  8005bb:	53                   	push   %ebx
  8005bc:	6a 30                	push   $0x30
  8005be:	ff d6                	call   *%esi
			putch('x', putdat);
  8005c0:	83 c4 08             	add    $0x8,%esp
  8005c3:	53                   	push   %ebx
  8005c4:	6a 78                	push   $0x78
  8005c6:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005c8:	8b 45 14             	mov    0x14(%ebp),%eax
  8005cb:	8d 50 04             	lea    0x4(%eax),%edx
  8005ce:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005d1:	8b 00                	mov    (%eax),%eax
  8005d3:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005d8:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005db:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005e0:	eb 14                	jmp    8005f6 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005e2:	8d 45 14             	lea    0x14(%ebp),%eax
  8005e5:	e8 62 fc ff ff       	call   80024c <getuint>
			base = 16;
  8005ea:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005ef:	eb 05                	jmp    8005f6 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005f1:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005f6:	83 ec 0c             	sub    $0xc,%esp
  8005f9:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005fd:	57                   	push   %edi
  8005fe:	ff 75 e4             	pushl  -0x1c(%ebp)
  800601:	51                   	push   %ecx
  800602:	52                   	push   %edx
  800603:	50                   	push   %eax
  800604:	89 da                	mov    %ebx,%edx
  800606:	89 f0                	mov    %esi,%eax
  800608:	e8 92 fb ff ff       	call   80019f <printnum>
			break;
  80060d:	83 c4 20             	add    $0x20,%esp
  800610:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800613:	e9 cd fc ff ff       	jmp    8002e5 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800618:	83 ec 08             	sub    $0x8,%esp
  80061b:	53                   	push   %ebx
  80061c:	51                   	push   %ecx
  80061d:	ff d6                	call   *%esi
			break;
  80061f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800622:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800625:	e9 bb fc ff ff       	jmp    8002e5 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80062a:	83 ec 08             	sub    $0x8,%esp
  80062d:	53                   	push   %ebx
  80062e:	6a 25                	push   $0x25
  800630:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800632:	83 c4 10             	add    $0x10,%esp
  800635:	eb 01                	jmp    800638 <vprintfmt+0x379>
  800637:	4f                   	dec    %edi
  800638:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80063c:	75 f9                	jne    800637 <vprintfmt+0x378>
  80063e:	e9 a2 fc ff ff       	jmp    8002e5 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800643:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800646:	5b                   	pop    %ebx
  800647:	5e                   	pop    %esi
  800648:	5f                   	pop    %edi
  800649:	5d                   	pop    %ebp
  80064a:	c3                   	ret    

0080064b <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80064b:	55                   	push   %ebp
  80064c:	89 e5                	mov    %esp,%ebp
  80064e:	83 ec 18             	sub    $0x18,%esp
  800651:	8b 45 08             	mov    0x8(%ebp),%eax
  800654:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800657:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80065a:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80065e:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800661:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800668:	85 c0                	test   %eax,%eax
  80066a:	74 26                	je     800692 <vsnprintf+0x47>
  80066c:	85 d2                	test   %edx,%edx
  80066e:	7e 29                	jle    800699 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800670:	ff 75 14             	pushl  0x14(%ebp)
  800673:	ff 75 10             	pushl  0x10(%ebp)
  800676:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800679:	50                   	push   %eax
  80067a:	68 86 02 80 00       	push   $0x800286
  80067f:	e8 3b fc ff ff       	call   8002bf <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800684:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800687:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80068a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80068d:	83 c4 10             	add    $0x10,%esp
  800690:	eb 0c                	jmp    80069e <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800692:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800697:	eb 05                	jmp    80069e <vsnprintf+0x53>
  800699:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80069e:	c9                   	leave  
  80069f:	c3                   	ret    

008006a0 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006a0:	55                   	push   %ebp
  8006a1:	89 e5                	mov    %esp,%ebp
  8006a3:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006a6:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006a9:	50                   	push   %eax
  8006aa:	ff 75 10             	pushl  0x10(%ebp)
  8006ad:	ff 75 0c             	pushl  0xc(%ebp)
  8006b0:	ff 75 08             	pushl  0x8(%ebp)
  8006b3:	e8 93 ff ff ff       	call   80064b <vsnprintf>
	va_end(ap);

	return rc;
}
  8006b8:	c9                   	leave  
  8006b9:	c3                   	ret    

008006ba <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006ba:	55                   	push   %ebp
  8006bb:	89 e5                	mov    %esp,%ebp
  8006bd:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006c0:	b8 00 00 00 00       	mov    $0x0,%eax
  8006c5:	eb 01                	jmp    8006c8 <strlen+0xe>
		n++;
  8006c7:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006c8:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006cc:	75 f9                	jne    8006c7 <strlen+0xd>
		n++;
	return n;
}
  8006ce:	5d                   	pop    %ebp
  8006cf:	c3                   	ret    

008006d0 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006d0:	55                   	push   %ebp
  8006d1:	89 e5                	mov    %esp,%ebp
  8006d3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006d6:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006d9:	ba 00 00 00 00       	mov    $0x0,%edx
  8006de:	eb 01                	jmp    8006e1 <strnlen+0x11>
		n++;
  8006e0:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006e1:	39 c2                	cmp    %eax,%edx
  8006e3:	74 08                	je     8006ed <strnlen+0x1d>
  8006e5:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006e9:	75 f5                	jne    8006e0 <strnlen+0x10>
  8006eb:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006ed:	5d                   	pop    %ebp
  8006ee:	c3                   	ret    

008006ef <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006ef:	55                   	push   %ebp
  8006f0:	89 e5                	mov    %esp,%ebp
  8006f2:	53                   	push   %ebx
  8006f3:	8b 45 08             	mov    0x8(%ebp),%eax
  8006f6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006f9:	89 c2                	mov    %eax,%edx
  8006fb:	42                   	inc    %edx
  8006fc:	41                   	inc    %ecx
  8006fd:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800700:	88 5a ff             	mov    %bl,-0x1(%edx)
  800703:	84 db                	test   %bl,%bl
  800705:	75 f4                	jne    8006fb <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800707:	5b                   	pop    %ebx
  800708:	5d                   	pop    %ebp
  800709:	c3                   	ret    

0080070a <strcat>:

char *
strcat(char *dst, const char *src)
{
  80070a:	55                   	push   %ebp
  80070b:	89 e5                	mov    %esp,%ebp
  80070d:	53                   	push   %ebx
  80070e:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800711:	53                   	push   %ebx
  800712:	e8 a3 ff ff ff       	call   8006ba <strlen>
  800717:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80071a:	ff 75 0c             	pushl  0xc(%ebp)
  80071d:	01 d8                	add    %ebx,%eax
  80071f:	50                   	push   %eax
  800720:	e8 ca ff ff ff       	call   8006ef <strcpy>
	return dst;
}
  800725:	89 d8                	mov    %ebx,%eax
  800727:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80072a:	c9                   	leave  
  80072b:	c3                   	ret    

0080072c <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80072c:	55                   	push   %ebp
  80072d:	89 e5                	mov    %esp,%ebp
  80072f:	56                   	push   %esi
  800730:	53                   	push   %ebx
  800731:	8b 75 08             	mov    0x8(%ebp),%esi
  800734:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800737:	89 f3                	mov    %esi,%ebx
  800739:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80073c:	89 f2                	mov    %esi,%edx
  80073e:	eb 0c                	jmp    80074c <strncpy+0x20>
		*dst++ = *src;
  800740:	42                   	inc    %edx
  800741:	8a 01                	mov    (%ecx),%al
  800743:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800746:	80 39 01             	cmpb   $0x1,(%ecx)
  800749:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80074c:	39 da                	cmp    %ebx,%edx
  80074e:	75 f0                	jne    800740 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800750:	89 f0                	mov    %esi,%eax
  800752:	5b                   	pop    %ebx
  800753:	5e                   	pop    %esi
  800754:	5d                   	pop    %ebp
  800755:	c3                   	ret    

00800756 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800756:	55                   	push   %ebp
  800757:	89 e5                	mov    %esp,%ebp
  800759:	56                   	push   %esi
  80075a:	53                   	push   %ebx
  80075b:	8b 75 08             	mov    0x8(%ebp),%esi
  80075e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800761:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800764:	85 c0                	test   %eax,%eax
  800766:	74 1e                	je     800786 <strlcpy+0x30>
  800768:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80076c:	89 f2                	mov    %esi,%edx
  80076e:	eb 05                	jmp    800775 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800770:	42                   	inc    %edx
  800771:	41                   	inc    %ecx
  800772:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800775:	39 c2                	cmp    %eax,%edx
  800777:	74 08                	je     800781 <strlcpy+0x2b>
  800779:	8a 19                	mov    (%ecx),%bl
  80077b:	84 db                	test   %bl,%bl
  80077d:	75 f1                	jne    800770 <strlcpy+0x1a>
  80077f:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800781:	c6 00 00             	movb   $0x0,(%eax)
  800784:	eb 02                	jmp    800788 <strlcpy+0x32>
  800786:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800788:	29 f0                	sub    %esi,%eax
}
  80078a:	5b                   	pop    %ebx
  80078b:	5e                   	pop    %esi
  80078c:	5d                   	pop    %ebp
  80078d:	c3                   	ret    

0080078e <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80078e:	55                   	push   %ebp
  80078f:	89 e5                	mov    %esp,%ebp
  800791:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800794:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800797:	eb 02                	jmp    80079b <strcmp+0xd>
		p++, q++;
  800799:	41                   	inc    %ecx
  80079a:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80079b:	8a 01                	mov    (%ecx),%al
  80079d:	84 c0                	test   %al,%al
  80079f:	74 04                	je     8007a5 <strcmp+0x17>
  8007a1:	3a 02                	cmp    (%edx),%al
  8007a3:	74 f4                	je     800799 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007a5:	0f b6 c0             	movzbl %al,%eax
  8007a8:	0f b6 12             	movzbl (%edx),%edx
  8007ab:	29 d0                	sub    %edx,%eax
}
  8007ad:	5d                   	pop    %ebp
  8007ae:	c3                   	ret    

008007af <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007af:	55                   	push   %ebp
  8007b0:	89 e5                	mov    %esp,%ebp
  8007b2:	53                   	push   %ebx
  8007b3:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007b9:	89 c3                	mov    %eax,%ebx
  8007bb:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007be:	eb 02                	jmp    8007c2 <strncmp+0x13>
		n--, p++, q++;
  8007c0:	40                   	inc    %eax
  8007c1:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007c2:	39 d8                	cmp    %ebx,%eax
  8007c4:	74 14                	je     8007da <strncmp+0x2b>
  8007c6:	8a 08                	mov    (%eax),%cl
  8007c8:	84 c9                	test   %cl,%cl
  8007ca:	74 04                	je     8007d0 <strncmp+0x21>
  8007cc:	3a 0a                	cmp    (%edx),%cl
  8007ce:	74 f0                	je     8007c0 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007d0:	0f b6 00             	movzbl (%eax),%eax
  8007d3:	0f b6 12             	movzbl (%edx),%edx
  8007d6:	29 d0                	sub    %edx,%eax
  8007d8:	eb 05                	jmp    8007df <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007da:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007df:	5b                   	pop    %ebx
  8007e0:	5d                   	pop    %ebp
  8007e1:	c3                   	ret    

008007e2 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007e2:	55                   	push   %ebp
  8007e3:	89 e5                	mov    %esp,%ebp
  8007e5:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e8:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007eb:	eb 05                	jmp    8007f2 <strchr+0x10>
		if (*s == c)
  8007ed:	38 ca                	cmp    %cl,%dl
  8007ef:	74 0c                	je     8007fd <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007f1:	40                   	inc    %eax
  8007f2:	8a 10                	mov    (%eax),%dl
  8007f4:	84 d2                	test   %dl,%dl
  8007f6:	75 f5                	jne    8007ed <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007f8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007fd:	5d                   	pop    %ebp
  8007fe:	c3                   	ret    

008007ff <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007ff:	55                   	push   %ebp
  800800:	89 e5                	mov    %esp,%ebp
  800802:	8b 45 08             	mov    0x8(%ebp),%eax
  800805:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800808:	eb 05                	jmp    80080f <strfind+0x10>
		if (*s == c)
  80080a:	38 ca                	cmp    %cl,%dl
  80080c:	74 07                	je     800815 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80080e:	40                   	inc    %eax
  80080f:	8a 10                	mov    (%eax),%dl
  800811:	84 d2                	test   %dl,%dl
  800813:	75 f5                	jne    80080a <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800815:	5d                   	pop    %ebp
  800816:	c3                   	ret    

00800817 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800817:	55                   	push   %ebp
  800818:	89 e5                	mov    %esp,%ebp
  80081a:	57                   	push   %edi
  80081b:	56                   	push   %esi
  80081c:	53                   	push   %ebx
  80081d:	8b 7d 08             	mov    0x8(%ebp),%edi
  800820:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800823:	85 c9                	test   %ecx,%ecx
  800825:	74 36                	je     80085d <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800827:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80082d:	75 28                	jne    800857 <memset+0x40>
  80082f:	f6 c1 03             	test   $0x3,%cl
  800832:	75 23                	jne    800857 <memset+0x40>
		c &= 0xFF;
  800834:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800838:	89 d3                	mov    %edx,%ebx
  80083a:	c1 e3 08             	shl    $0x8,%ebx
  80083d:	89 d6                	mov    %edx,%esi
  80083f:	c1 e6 18             	shl    $0x18,%esi
  800842:	89 d0                	mov    %edx,%eax
  800844:	c1 e0 10             	shl    $0x10,%eax
  800847:	09 f0                	or     %esi,%eax
  800849:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80084b:	89 d8                	mov    %ebx,%eax
  80084d:	09 d0                	or     %edx,%eax
  80084f:	c1 e9 02             	shr    $0x2,%ecx
  800852:	fc                   	cld    
  800853:	f3 ab                	rep stos %eax,%es:(%edi)
  800855:	eb 06                	jmp    80085d <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800857:	8b 45 0c             	mov    0xc(%ebp),%eax
  80085a:	fc                   	cld    
  80085b:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80085d:	89 f8                	mov    %edi,%eax
  80085f:	5b                   	pop    %ebx
  800860:	5e                   	pop    %esi
  800861:	5f                   	pop    %edi
  800862:	5d                   	pop    %ebp
  800863:	c3                   	ret    

00800864 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800864:	55                   	push   %ebp
  800865:	89 e5                	mov    %esp,%ebp
  800867:	57                   	push   %edi
  800868:	56                   	push   %esi
  800869:	8b 45 08             	mov    0x8(%ebp),%eax
  80086c:	8b 75 0c             	mov    0xc(%ebp),%esi
  80086f:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800872:	39 c6                	cmp    %eax,%esi
  800874:	73 33                	jae    8008a9 <memmove+0x45>
  800876:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800879:	39 d0                	cmp    %edx,%eax
  80087b:	73 2c                	jae    8008a9 <memmove+0x45>
		s += n;
		d += n;
  80087d:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800880:	89 d6                	mov    %edx,%esi
  800882:	09 fe                	or     %edi,%esi
  800884:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80088a:	75 13                	jne    80089f <memmove+0x3b>
  80088c:	f6 c1 03             	test   $0x3,%cl
  80088f:	75 0e                	jne    80089f <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800891:	83 ef 04             	sub    $0x4,%edi
  800894:	8d 72 fc             	lea    -0x4(%edx),%esi
  800897:	c1 e9 02             	shr    $0x2,%ecx
  80089a:	fd                   	std    
  80089b:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80089d:	eb 07                	jmp    8008a6 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80089f:	4f                   	dec    %edi
  8008a0:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008a3:	fd                   	std    
  8008a4:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008a6:	fc                   	cld    
  8008a7:	eb 1d                	jmp    8008c6 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008a9:	89 f2                	mov    %esi,%edx
  8008ab:	09 c2                	or     %eax,%edx
  8008ad:	f6 c2 03             	test   $0x3,%dl
  8008b0:	75 0f                	jne    8008c1 <memmove+0x5d>
  8008b2:	f6 c1 03             	test   $0x3,%cl
  8008b5:	75 0a                	jne    8008c1 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008b7:	c1 e9 02             	shr    $0x2,%ecx
  8008ba:	89 c7                	mov    %eax,%edi
  8008bc:	fc                   	cld    
  8008bd:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008bf:	eb 05                	jmp    8008c6 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008c1:	89 c7                	mov    %eax,%edi
  8008c3:	fc                   	cld    
  8008c4:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008c6:	5e                   	pop    %esi
  8008c7:	5f                   	pop    %edi
  8008c8:	5d                   	pop    %ebp
  8008c9:	c3                   	ret    

008008ca <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008ca:	55                   	push   %ebp
  8008cb:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008cd:	ff 75 10             	pushl  0x10(%ebp)
  8008d0:	ff 75 0c             	pushl  0xc(%ebp)
  8008d3:	ff 75 08             	pushl  0x8(%ebp)
  8008d6:	e8 89 ff ff ff       	call   800864 <memmove>
}
  8008db:	c9                   	leave  
  8008dc:	c3                   	ret    

008008dd <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008dd:	55                   	push   %ebp
  8008de:	89 e5                	mov    %esp,%ebp
  8008e0:	56                   	push   %esi
  8008e1:	53                   	push   %ebx
  8008e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e5:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008e8:	89 c6                	mov    %eax,%esi
  8008ea:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008ed:	eb 14                	jmp    800903 <memcmp+0x26>
		if (*s1 != *s2)
  8008ef:	8a 08                	mov    (%eax),%cl
  8008f1:	8a 1a                	mov    (%edx),%bl
  8008f3:	38 d9                	cmp    %bl,%cl
  8008f5:	74 0a                	je     800901 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008f7:	0f b6 c1             	movzbl %cl,%eax
  8008fa:	0f b6 db             	movzbl %bl,%ebx
  8008fd:	29 d8                	sub    %ebx,%eax
  8008ff:	eb 0b                	jmp    80090c <memcmp+0x2f>
		s1++, s2++;
  800901:	40                   	inc    %eax
  800902:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800903:	39 f0                	cmp    %esi,%eax
  800905:	75 e8                	jne    8008ef <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800907:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80090c:	5b                   	pop    %ebx
  80090d:	5e                   	pop    %esi
  80090e:	5d                   	pop    %ebp
  80090f:	c3                   	ret    

00800910 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800910:	55                   	push   %ebp
  800911:	89 e5                	mov    %esp,%ebp
  800913:	53                   	push   %ebx
  800914:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800917:	89 c1                	mov    %eax,%ecx
  800919:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80091c:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800920:	eb 08                	jmp    80092a <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800922:	0f b6 10             	movzbl (%eax),%edx
  800925:	39 da                	cmp    %ebx,%edx
  800927:	74 05                	je     80092e <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800929:	40                   	inc    %eax
  80092a:	39 c8                	cmp    %ecx,%eax
  80092c:	72 f4                	jb     800922 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80092e:	5b                   	pop    %ebx
  80092f:	5d                   	pop    %ebp
  800930:	c3                   	ret    

00800931 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800931:	55                   	push   %ebp
  800932:	89 e5                	mov    %esp,%ebp
  800934:	57                   	push   %edi
  800935:	56                   	push   %esi
  800936:	53                   	push   %ebx
  800937:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80093a:	eb 01                	jmp    80093d <strtol+0xc>
		s++;
  80093c:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80093d:	8a 01                	mov    (%ecx),%al
  80093f:	3c 20                	cmp    $0x20,%al
  800941:	74 f9                	je     80093c <strtol+0xb>
  800943:	3c 09                	cmp    $0x9,%al
  800945:	74 f5                	je     80093c <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800947:	3c 2b                	cmp    $0x2b,%al
  800949:	75 08                	jne    800953 <strtol+0x22>
		s++;
  80094b:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80094c:	bf 00 00 00 00       	mov    $0x0,%edi
  800951:	eb 11                	jmp    800964 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800953:	3c 2d                	cmp    $0x2d,%al
  800955:	75 08                	jne    80095f <strtol+0x2e>
		s++, neg = 1;
  800957:	41                   	inc    %ecx
  800958:	bf 01 00 00 00       	mov    $0x1,%edi
  80095d:	eb 05                	jmp    800964 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80095f:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800964:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800968:	0f 84 87 00 00 00    	je     8009f5 <strtol+0xc4>
  80096e:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800972:	75 27                	jne    80099b <strtol+0x6a>
  800974:	80 39 30             	cmpb   $0x30,(%ecx)
  800977:	75 22                	jne    80099b <strtol+0x6a>
  800979:	e9 88 00 00 00       	jmp    800a06 <strtol+0xd5>
		s += 2, base = 16;
  80097e:	83 c1 02             	add    $0x2,%ecx
  800981:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800988:	eb 11                	jmp    80099b <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80098a:	41                   	inc    %ecx
  80098b:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800992:	eb 07                	jmp    80099b <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800994:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80099b:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009a0:	8a 11                	mov    (%ecx),%dl
  8009a2:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009a5:	80 fb 09             	cmp    $0x9,%bl
  8009a8:	77 08                	ja     8009b2 <strtol+0x81>
			dig = *s - '0';
  8009aa:	0f be d2             	movsbl %dl,%edx
  8009ad:	83 ea 30             	sub    $0x30,%edx
  8009b0:	eb 22                	jmp    8009d4 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009b2:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009b5:	89 f3                	mov    %esi,%ebx
  8009b7:	80 fb 19             	cmp    $0x19,%bl
  8009ba:	77 08                	ja     8009c4 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009bc:	0f be d2             	movsbl %dl,%edx
  8009bf:	83 ea 57             	sub    $0x57,%edx
  8009c2:	eb 10                	jmp    8009d4 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009c4:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009c7:	89 f3                	mov    %esi,%ebx
  8009c9:	80 fb 19             	cmp    $0x19,%bl
  8009cc:	77 14                	ja     8009e2 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009ce:	0f be d2             	movsbl %dl,%edx
  8009d1:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009d4:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009d7:	7d 09                	jge    8009e2 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009d9:	41                   	inc    %ecx
  8009da:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009de:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009e0:	eb be                	jmp    8009a0 <strtol+0x6f>

	if (endptr)
  8009e2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009e6:	74 05                	je     8009ed <strtol+0xbc>
		*endptr = (char *) s;
  8009e8:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009eb:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009ed:	85 ff                	test   %edi,%edi
  8009ef:	74 21                	je     800a12 <strtol+0xe1>
  8009f1:	f7 d8                	neg    %eax
  8009f3:	eb 1d                	jmp    800a12 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009f5:	80 39 30             	cmpb   $0x30,(%ecx)
  8009f8:	75 9a                	jne    800994 <strtol+0x63>
  8009fa:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009fe:	0f 84 7a ff ff ff    	je     80097e <strtol+0x4d>
  800a04:	eb 84                	jmp    80098a <strtol+0x59>
  800a06:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a0a:	0f 84 6e ff ff ff    	je     80097e <strtol+0x4d>
  800a10:	eb 89                	jmp    80099b <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a12:	5b                   	pop    %ebx
  800a13:	5e                   	pop    %esi
  800a14:	5f                   	pop    %edi
  800a15:	5d                   	pop    %ebp
  800a16:	c3                   	ret    

00800a17 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a17:	55                   	push   %ebp
  800a18:	89 e5                	mov    %esp,%ebp
  800a1a:	57                   	push   %edi
  800a1b:	56                   	push   %esi
  800a1c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a1d:	b8 00 00 00 00       	mov    $0x0,%eax
  800a22:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a25:	8b 55 08             	mov    0x8(%ebp),%edx
  800a28:	89 c3                	mov    %eax,%ebx
  800a2a:	89 c7                	mov    %eax,%edi
  800a2c:	89 c6                	mov    %eax,%esi
  800a2e:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a30:	5b                   	pop    %ebx
  800a31:	5e                   	pop    %esi
  800a32:	5f                   	pop    %edi
  800a33:	5d                   	pop    %ebp
  800a34:	c3                   	ret    

00800a35 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a35:	55                   	push   %ebp
  800a36:	89 e5                	mov    %esp,%ebp
  800a38:	57                   	push   %edi
  800a39:	56                   	push   %esi
  800a3a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a3b:	ba 00 00 00 00       	mov    $0x0,%edx
  800a40:	b8 01 00 00 00       	mov    $0x1,%eax
  800a45:	89 d1                	mov    %edx,%ecx
  800a47:	89 d3                	mov    %edx,%ebx
  800a49:	89 d7                	mov    %edx,%edi
  800a4b:	89 d6                	mov    %edx,%esi
  800a4d:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a4f:	5b                   	pop    %ebx
  800a50:	5e                   	pop    %esi
  800a51:	5f                   	pop    %edi
  800a52:	5d                   	pop    %ebp
  800a53:	c3                   	ret    

00800a54 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a54:	55                   	push   %ebp
  800a55:	89 e5                	mov    %esp,%ebp
  800a57:	57                   	push   %edi
  800a58:	56                   	push   %esi
  800a59:	53                   	push   %ebx
  800a5a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a5d:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a62:	b8 03 00 00 00       	mov    $0x3,%eax
  800a67:	8b 55 08             	mov    0x8(%ebp),%edx
  800a6a:	89 cb                	mov    %ecx,%ebx
  800a6c:	89 cf                	mov    %ecx,%edi
  800a6e:	89 ce                	mov    %ecx,%esi
  800a70:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a72:	85 c0                	test   %eax,%eax
  800a74:	7e 17                	jle    800a8d <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a76:	83 ec 0c             	sub    $0xc,%esp
  800a79:	50                   	push   %eax
  800a7a:	6a 03                	push   $0x3
  800a7c:	68 1f 13 80 00       	push   $0x80131f
  800a81:	6a 23                	push   $0x23
  800a83:	68 3c 13 80 00       	push   $0x80133c
  800a88:	e8 56 02 00 00       	call   800ce3 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a8d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a90:	5b                   	pop    %ebx
  800a91:	5e                   	pop    %esi
  800a92:	5f                   	pop    %edi
  800a93:	5d                   	pop    %ebp
  800a94:	c3                   	ret    

00800a95 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a95:	55                   	push   %ebp
  800a96:	89 e5                	mov    %esp,%ebp
  800a98:	57                   	push   %edi
  800a99:	56                   	push   %esi
  800a9a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a9b:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa0:	b8 02 00 00 00       	mov    $0x2,%eax
  800aa5:	89 d1                	mov    %edx,%ecx
  800aa7:	89 d3                	mov    %edx,%ebx
  800aa9:	89 d7                	mov    %edx,%edi
  800aab:	89 d6                	mov    %edx,%esi
  800aad:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800aaf:	5b                   	pop    %ebx
  800ab0:	5e                   	pop    %esi
  800ab1:	5f                   	pop    %edi
  800ab2:	5d                   	pop    %ebp
  800ab3:	c3                   	ret    

00800ab4 <sys_yield>:

void
sys_yield(void)
{
  800ab4:	55                   	push   %ebp
  800ab5:	89 e5                	mov    %esp,%ebp
  800ab7:	57                   	push   %edi
  800ab8:	56                   	push   %esi
  800ab9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aba:	ba 00 00 00 00       	mov    $0x0,%edx
  800abf:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ac4:	89 d1                	mov    %edx,%ecx
  800ac6:	89 d3                	mov    %edx,%ebx
  800ac8:	89 d7                	mov    %edx,%edi
  800aca:	89 d6                	mov    %edx,%esi
  800acc:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800ace:	5b                   	pop    %ebx
  800acf:	5e                   	pop    %esi
  800ad0:	5f                   	pop    %edi
  800ad1:	5d                   	pop    %ebp
  800ad2:	c3                   	ret    

00800ad3 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800ad3:	55                   	push   %ebp
  800ad4:	89 e5                	mov    %esp,%ebp
  800ad6:	57                   	push   %edi
  800ad7:	56                   	push   %esi
  800ad8:	53                   	push   %ebx
  800ad9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800adc:	be 00 00 00 00       	mov    $0x0,%esi
  800ae1:	b8 04 00 00 00       	mov    $0x4,%eax
  800ae6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ae9:	8b 55 08             	mov    0x8(%ebp),%edx
  800aec:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800aef:	89 f7                	mov    %esi,%edi
  800af1:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800af3:	85 c0                	test   %eax,%eax
  800af5:	7e 17                	jle    800b0e <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800af7:	83 ec 0c             	sub    $0xc,%esp
  800afa:	50                   	push   %eax
  800afb:	6a 04                	push   $0x4
  800afd:	68 1f 13 80 00       	push   $0x80131f
  800b02:	6a 23                	push   $0x23
  800b04:	68 3c 13 80 00       	push   $0x80133c
  800b09:	e8 d5 01 00 00       	call   800ce3 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b0e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b11:	5b                   	pop    %ebx
  800b12:	5e                   	pop    %esi
  800b13:	5f                   	pop    %edi
  800b14:	5d                   	pop    %ebp
  800b15:	c3                   	ret    

00800b16 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b16:	55                   	push   %ebp
  800b17:	89 e5                	mov    %esp,%ebp
  800b19:	57                   	push   %edi
  800b1a:	56                   	push   %esi
  800b1b:	53                   	push   %ebx
  800b1c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1f:	b8 05 00 00 00       	mov    $0x5,%eax
  800b24:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b27:	8b 55 08             	mov    0x8(%ebp),%edx
  800b2a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b2d:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b30:	8b 75 18             	mov    0x18(%ebp),%esi
  800b33:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b35:	85 c0                	test   %eax,%eax
  800b37:	7e 17                	jle    800b50 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b39:	83 ec 0c             	sub    $0xc,%esp
  800b3c:	50                   	push   %eax
  800b3d:	6a 05                	push   $0x5
  800b3f:	68 1f 13 80 00       	push   $0x80131f
  800b44:	6a 23                	push   $0x23
  800b46:	68 3c 13 80 00       	push   $0x80133c
  800b4b:	e8 93 01 00 00       	call   800ce3 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b50:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b53:	5b                   	pop    %ebx
  800b54:	5e                   	pop    %esi
  800b55:	5f                   	pop    %edi
  800b56:	5d                   	pop    %ebp
  800b57:	c3                   	ret    

00800b58 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b58:	55                   	push   %ebp
  800b59:	89 e5                	mov    %esp,%ebp
  800b5b:	57                   	push   %edi
  800b5c:	56                   	push   %esi
  800b5d:	53                   	push   %ebx
  800b5e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b61:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b66:	b8 06 00 00 00       	mov    $0x6,%eax
  800b6b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b6e:	8b 55 08             	mov    0x8(%ebp),%edx
  800b71:	89 df                	mov    %ebx,%edi
  800b73:	89 de                	mov    %ebx,%esi
  800b75:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b77:	85 c0                	test   %eax,%eax
  800b79:	7e 17                	jle    800b92 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b7b:	83 ec 0c             	sub    $0xc,%esp
  800b7e:	50                   	push   %eax
  800b7f:	6a 06                	push   $0x6
  800b81:	68 1f 13 80 00       	push   $0x80131f
  800b86:	6a 23                	push   $0x23
  800b88:	68 3c 13 80 00       	push   $0x80133c
  800b8d:	e8 51 01 00 00       	call   800ce3 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800b92:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b95:	5b                   	pop    %ebx
  800b96:	5e                   	pop    %esi
  800b97:	5f                   	pop    %edi
  800b98:	5d                   	pop    %ebp
  800b99:	c3                   	ret    

00800b9a <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800b9a:	55                   	push   %ebp
  800b9b:	89 e5                	mov    %esp,%ebp
  800b9d:	57                   	push   %edi
  800b9e:	56                   	push   %esi
  800b9f:	53                   	push   %ebx
  800ba0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ba3:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ba8:	b8 08 00 00 00       	mov    $0x8,%eax
  800bad:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bb0:	8b 55 08             	mov    0x8(%ebp),%edx
  800bb3:	89 df                	mov    %ebx,%edi
  800bb5:	89 de                	mov    %ebx,%esi
  800bb7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bb9:	85 c0                	test   %eax,%eax
  800bbb:	7e 17                	jle    800bd4 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bbd:	83 ec 0c             	sub    $0xc,%esp
  800bc0:	50                   	push   %eax
  800bc1:	6a 08                	push   $0x8
  800bc3:	68 1f 13 80 00       	push   $0x80131f
  800bc8:	6a 23                	push   $0x23
  800bca:	68 3c 13 80 00       	push   $0x80133c
  800bcf:	e8 0f 01 00 00       	call   800ce3 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800bd4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bd7:	5b                   	pop    %ebx
  800bd8:	5e                   	pop    %esi
  800bd9:	5f                   	pop    %edi
  800bda:	5d                   	pop    %ebp
  800bdb:	c3                   	ret    

00800bdc <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800bdc:	55                   	push   %ebp
  800bdd:	89 e5                	mov    %esp,%ebp
  800bdf:	57                   	push   %edi
  800be0:	56                   	push   %esi
  800be1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800be2:	ba 00 00 00 00       	mov    $0x0,%edx
  800be7:	b8 0c 00 00 00       	mov    $0xc,%eax
  800bec:	89 d1                	mov    %edx,%ecx
  800bee:	89 d3                	mov    %edx,%ebx
  800bf0:	89 d7                	mov    %edx,%edi
  800bf2:	89 d6                	mov    %edx,%esi
  800bf4:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800bf6:	5b                   	pop    %ebx
  800bf7:	5e                   	pop    %esi
  800bf8:	5f                   	pop    %edi
  800bf9:	5d                   	pop    %ebp
  800bfa:	c3                   	ret    

00800bfb <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800bfb:	55                   	push   %ebp
  800bfc:	89 e5                	mov    %esp,%ebp
  800bfe:	57                   	push   %edi
  800bff:	56                   	push   %esi
  800c00:	53                   	push   %ebx
  800c01:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c04:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c09:	b8 09 00 00 00       	mov    $0x9,%eax
  800c0e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c11:	8b 55 08             	mov    0x8(%ebp),%edx
  800c14:	89 df                	mov    %ebx,%edi
  800c16:	89 de                	mov    %ebx,%esi
  800c18:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c1a:	85 c0                	test   %eax,%eax
  800c1c:	7e 17                	jle    800c35 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c1e:	83 ec 0c             	sub    $0xc,%esp
  800c21:	50                   	push   %eax
  800c22:	6a 09                	push   $0x9
  800c24:	68 1f 13 80 00       	push   $0x80131f
  800c29:	6a 23                	push   $0x23
  800c2b:	68 3c 13 80 00       	push   $0x80133c
  800c30:	e8 ae 00 00 00       	call   800ce3 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c35:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c38:	5b                   	pop    %ebx
  800c39:	5e                   	pop    %esi
  800c3a:	5f                   	pop    %edi
  800c3b:	5d                   	pop    %ebp
  800c3c:	c3                   	ret    

00800c3d <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c3d:	55                   	push   %ebp
  800c3e:	89 e5                	mov    %esp,%ebp
  800c40:	57                   	push   %edi
  800c41:	56                   	push   %esi
  800c42:	53                   	push   %ebx
  800c43:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c46:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c4b:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c50:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c53:	8b 55 08             	mov    0x8(%ebp),%edx
  800c56:	89 df                	mov    %ebx,%edi
  800c58:	89 de                	mov    %ebx,%esi
  800c5a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c5c:	85 c0                	test   %eax,%eax
  800c5e:	7e 17                	jle    800c77 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c60:	83 ec 0c             	sub    $0xc,%esp
  800c63:	50                   	push   %eax
  800c64:	6a 0a                	push   $0xa
  800c66:	68 1f 13 80 00       	push   $0x80131f
  800c6b:	6a 23                	push   $0x23
  800c6d:	68 3c 13 80 00       	push   $0x80133c
  800c72:	e8 6c 00 00 00       	call   800ce3 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800c77:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c7a:	5b                   	pop    %ebx
  800c7b:	5e                   	pop    %esi
  800c7c:	5f                   	pop    %edi
  800c7d:	5d                   	pop    %ebp
  800c7e:	c3                   	ret    

00800c7f <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800c7f:	55                   	push   %ebp
  800c80:	89 e5                	mov    %esp,%ebp
  800c82:	57                   	push   %edi
  800c83:	56                   	push   %esi
  800c84:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c85:	be 00 00 00 00       	mov    $0x0,%esi
  800c8a:	b8 0d 00 00 00       	mov    $0xd,%eax
  800c8f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c92:	8b 55 08             	mov    0x8(%ebp),%edx
  800c95:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c98:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c9b:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800c9d:	5b                   	pop    %ebx
  800c9e:	5e                   	pop    %esi
  800c9f:	5f                   	pop    %edi
  800ca0:	5d                   	pop    %ebp
  800ca1:	c3                   	ret    

00800ca2 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800ca2:	55                   	push   %ebp
  800ca3:	89 e5                	mov    %esp,%ebp
  800ca5:	57                   	push   %edi
  800ca6:	56                   	push   %esi
  800ca7:	53                   	push   %ebx
  800ca8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cab:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cb0:	b8 0e 00 00 00       	mov    $0xe,%eax
  800cb5:	8b 55 08             	mov    0x8(%ebp),%edx
  800cb8:	89 cb                	mov    %ecx,%ebx
  800cba:	89 cf                	mov    %ecx,%edi
  800cbc:	89 ce                	mov    %ecx,%esi
  800cbe:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cc0:	85 c0                	test   %eax,%eax
  800cc2:	7e 17                	jle    800cdb <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cc4:	83 ec 0c             	sub    $0xc,%esp
  800cc7:	50                   	push   %eax
  800cc8:	6a 0e                	push   $0xe
  800cca:	68 1f 13 80 00       	push   $0x80131f
  800ccf:	6a 23                	push   $0x23
  800cd1:	68 3c 13 80 00       	push   $0x80133c
  800cd6:	e8 08 00 00 00       	call   800ce3 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800cdb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cde:	5b                   	pop    %ebx
  800cdf:	5e                   	pop    %esi
  800ce0:	5f                   	pop    %edi
  800ce1:	5d                   	pop    %ebp
  800ce2:	c3                   	ret    

00800ce3 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800ce3:	55                   	push   %ebp
  800ce4:	89 e5                	mov    %esp,%ebp
  800ce6:	56                   	push   %esi
  800ce7:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800ce8:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800ceb:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800cf1:	e8 9f fd ff ff       	call   800a95 <sys_getenvid>
  800cf6:	83 ec 0c             	sub    $0xc,%esp
  800cf9:	ff 75 0c             	pushl  0xc(%ebp)
  800cfc:	ff 75 08             	pushl  0x8(%ebp)
  800cff:	56                   	push   %esi
  800d00:	50                   	push   %eax
  800d01:	68 4c 13 80 00       	push   $0x80134c
  800d06:	e8 80 f4 ff ff       	call   80018b <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800d0b:	83 c4 18             	add    $0x18,%esp
  800d0e:	53                   	push   %ebx
  800d0f:	ff 75 10             	pushl  0x10(%ebp)
  800d12:	e8 23 f4 ff ff       	call   80013a <vcprintf>
	cprintf("\n");
  800d17:	c7 04 24 70 13 80 00 	movl   $0x801370,(%esp)
  800d1e:	e8 68 f4 ff ff       	call   80018b <cprintf>
  800d23:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800d26:	cc                   	int3   
  800d27:	eb fd                	jmp    800d26 <_panic+0x43>
  800d29:	66 90                	xchg   %ax,%ax
  800d2b:	90                   	nop

00800d2c <__udivdi3>:
  800d2c:	55                   	push   %ebp
  800d2d:	57                   	push   %edi
  800d2e:	56                   	push   %esi
  800d2f:	53                   	push   %ebx
  800d30:	83 ec 1c             	sub    $0x1c,%esp
  800d33:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d37:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d3b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d3f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d43:	89 ca                	mov    %ecx,%edx
  800d45:	89 f8                	mov    %edi,%eax
  800d47:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800d4b:	85 f6                	test   %esi,%esi
  800d4d:	75 2d                	jne    800d7c <__udivdi3+0x50>
  800d4f:	39 cf                	cmp    %ecx,%edi
  800d51:	77 65                	ja     800db8 <__udivdi3+0x8c>
  800d53:	89 fd                	mov    %edi,%ebp
  800d55:	85 ff                	test   %edi,%edi
  800d57:	75 0b                	jne    800d64 <__udivdi3+0x38>
  800d59:	b8 01 00 00 00       	mov    $0x1,%eax
  800d5e:	31 d2                	xor    %edx,%edx
  800d60:	f7 f7                	div    %edi
  800d62:	89 c5                	mov    %eax,%ebp
  800d64:	31 d2                	xor    %edx,%edx
  800d66:	89 c8                	mov    %ecx,%eax
  800d68:	f7 f5                	div    %ebp
  800d6a:	89 c1                	mov    %eax,%ecx
  800d6c:	89 d8                	mov    %ebx,%eax
  800d6e:	f7 f5                	div    %ebp
  800d70:	89 cf                	mov    %ecx,%edi
  800d72:	89 fa                	mov    %edi,%edx
  800d74:	83 c4 1c             	add    $0x1c,%esp
  800d77:	5b                   	pop    %ebx
  800d78:	5e                   	pop    %esi
  800d79:	5f                   	pop    %edi
  800d7a:	5d                   	pop    %ebp
  800d7b:	c3                   	ret    
  800d7c:	39 ce                	cmp    %ecx,%esi
  800d7e:	77 28                	ja     800da8 <__udivdi3+0x7c>
  800d80:	0f bd fe             	bsr    %esi,%edi
  800d83:	83 f7 1f             	xor    $0x1f,%edi
  800d86:	75 40                	jne    800dc8 <__udivdi3+0x9c>
  800d88:	39 ce                	cmp    %ecx,%esi
  800d8a:	72 0a                	jb     800d96 <__udivdi3+0x6a>
  800d8c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d90:	0f 87 9e 00 00 00    	ja     800e34 <__udivdi3+0x108>
  800d96:	b8 01 00 00 00       	mov    $0x1,%eax
  800d9b:	89 fa                	mov    %edi,%edx
  800d9d:	83 c4 1c             	add    $0x1c,%esp
  800da0:	5b                   	pop    %ebx
  800da1:	5e                   	pop    %esi
  800da2:	5f                   	pop    %edi
  800da3:	5d                   	pop    %ebp
  800da4:	c3                   	ret    
  800da5:	8d 76 00             	lea    0x0(%esi),%esi
  800da8:	31 ff                	xor    %edi,%edi
  800daa:	31 c0                	xor    %eax,%eax
  800dac:	89 fa                	mov    %edi,%edx
  800dae:	83 c4 1c             	add    $0x1c,%esp
  800db1:	5b                   	pop    %ebx
  800db2:	5e                   	pop    %esi
  800db3:	5f                   	pop    %edi
  800db4:	5d                   	pop    %ebp
  800db5:	c3                   	ret    
  800db6:	66 90                	xchg   %ax,%ax
  800db8:	89 d8                	mov    %ebx,%eax
  800dba:	f7 f7                	div    %edi
  800dbc:	31 ff                	xor    %edi,%edi
  800dbe:	89 fa                	mov    %edi,%edx
  800dc0:	83 c4 1c             	add    $0x1c,%esp
  800dc3:	5b                   	pop    %ebx
  800dc4:	5e                   	pop    %esi
  800dc5:	5f                   	pop    %edi
  800dc6:	5d                   	pop    %ebp
  800dc7:	c3                   	ret    
  800dc8:	bd 20 00 00 00       	mov    $0x20,%ebp
  800dcd:	89 eb                	mov    %ebp,%ebx
  800dcf:	29 fb                	sub    %edi,%ebx
  800dd1:	89 f9                	mov    %edi,%ecx
  800dd3:	d3 e6                	shl    %cl,%esi
  800dd5:	89 c5                	mov    %eax,%ebp
  800dd7:	88 d9                	mov    %bl,%cl
  800dd9:	d3 ed                	shr    %cl,%ebp
  800ddb:	89 e9                	mov    %ebp,%ecx
  800ddd:	09 f1                	or     %esi,%ecx
  800ddf:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800de3:	89 f9                	mov    %edi,%ecx
  800de5:	d3 e0                	shl    %cl,%eax
  800de7:	89 c5                	mov    %eax,%ebp
  800de9:	89 d6                	mov    %edx,%esi
  800deb:	88 d9                	mov    %bl,%cl
  800ded:	d3 ee                	shr    %cl,%esi
  800def:	89 f9                	mov    %edi,%ecx
  800df1:	d3 e2                	shl    %cl,%edx
  800df3:	8b 44 24 08          	mov    0x8(%esp),%eax
  800df7:	88 d9                	mov    %bl,%cl
  800df9:	d3 e8                	shr    %cl,%eax
  800dfb:	09 c2                	or     %eax,%edx
  800dfd:	89 d0                	mov    %edx,%eax
  800dff:	89 f2                	mov    %esi,%edx
  800e01:	f7 74 24 0c          	divl   0xc(%esp)
  800e05:	89 d6                	mov    %edx,%esi
  800e07:	89 c3                	mov    %eax,%ebx
  800e09:	f7 e5                	mul    %ebp
  800e0b:	39 d6                	cmp    %edx,%esi
  800e0d:	72 19                	jb     800e28 <__udivdi3+0xfc>
  800e0f:	74 0b                	je     800e1c <__udivdi3+0xf0>
  800e11:	89 d8                	mov    %ebx,%eax
  800e13:	31 ff                	xor    %edi,%edi
  800e15:	e9 58 ff ff ff       	jmp    800d72 <__udivdi3+0x46>
  800e1a:	66 90                	xchg   %ax,%ax
  800e1c:	8b 54 24 08          	mov    0x8(%esp),%edx
  800e20:	89 f9                	mov    %edi,%ecx
  800e22:	d3 e2                	shl    %cl,%edx
  800e24:	39 c2                	cmp    %eax,%edx
  800e26:	73 e9                	jae    800e11 <__udivdi3+0xe5>
  800e28:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800e2b:	31 ff                	xor    %edi,%edi
  800e2d:	e9 40 ff ff ff       	jmp    800d72 <__udivdi3+0x46>
  800e32:	66 90                	xchg   %ax,%ax
  800e34:	31 c0                	xor    %eax,%eax
  800e36:	e9 37 ff ff ff       	jmp    800d72 <__udivdi3+0x46>
  800e3b:	90                   	nop

00800e3c <__umoddi3>:
  800e3c:	55                   	push   %ebp
  800e3d:	57                   	push   %edi
  800e3e:	56                   	push   %esi
  800e3f:	53                   	push   %ebx
  800e40:	83 ec 1c             	sub    $0x1c,%esp
  800e43:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800e47:	8b 74 24 34          	mov    0x34(%esp),%esi
  800e4b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e4f:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e53:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e57:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e5b:	89 f3                	mov    %esi,%ebx
  800e5d:	89 fa                	mov    %edi,%edx
  800e5f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e63:	89 34 24             	mov    %esi,(%esp)
  800e66:	85 c0                	test   %eax,%eax
  800e68:	75 1a                	jne    800e84 <__umoddi3+0x48>
  800e6a:	39 f7                	cmp    %esi,%edi
  800e6c:	0f 86 a2 00 00 00    	jbe    800f14 <__umoddi3+0xd8>
  800e72:	89 c8                	mov    %ecx,%eax
  800e74:	89 f2                	mov    %esi,%edx
  800e76:	f7 f7                	div    %edi
  800e78:	89 d0                	mov    %edx,%eax
  800e7a:	31 d2                	xor    %edx,%edx
  800e7c:	83 c4 1c             	add    $0x1c,%esp
  800e7f:	5b                   	pop    %ebx
  800e80:	5e                   	pop    %esi
  800e81:	5f                   	pop    %edi
  800e82:	5d                   	pop    %ebp
  800e83:	c3                   	ret    
  800e84:	39 f0                	cmp    %esi,%eax
  800e86:	0f 87 ac 00 00 00    	ja     800f38 <__umoddi3+0xfc>
  800e8c:	0f bd e8             	bsr    %eax,%ebp
  800e8f:	83 f5 1f             	xor    $0x1f,%ebp
  800e92:	0f 84 ac 00 00 00    	je     800f44 <__umoddi3+0x108>
  800e98:	bf 20 00 00 00       	mov    $0x20,%edi
  800e9d:	29 ef                	sub    %ebp,%edi
  800e9f:	89 fe                	mov    %edi,%esi
  800ea1:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800ea5:	89 e9                	mov    %ebp,%ecx
  800ea7:	d3 e0                	shl    %cl,%eax
  800ea9:	89 d7                	mov    %edx,%edi
  800eab:	89 f1                	mov    %esi,%ecx
  800ead:	d3 ef                	shr    %cl,%edi
  800eaf:	09 c7                	or     %eax,%edi
  800eb1:	89 e9                	mov    %ebp,%ecx
  800eb3:	d3 e2                	shl    %cl,%edx
  800eb5:	89 14 24             	mov    %edx,(%esp)
  800eb8:	89 d8                	mov    %ebx,%eax
  800eba:	d3 e0                	shl    %cl,%eax
  800ebc:	89 c2                	mov    %eax,%edx
  800ebe:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ec2:	d3 e0                	shl    %cl,%eax
  800ec4:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ec8:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ecc:	89 f1                	mov    %esi,%ecx
  800ece:	d3 e8                	shr    %cl,%eax
  800ed0:	09 d0                	or     %edx,%eax
  800ed2:	d3 eb                	shr    %cl,%ebx
  800ed4:	89 da                	mov    %ebx,%edx
  800ed6:	f7 f7                	div    %edi
  800ed8:	89 d3                	mov    %edx,%ebx
  800eda:	f7 24 24             	mull   (%esp)
  800edd:	89 c6                	mov    %eax,%esi
  800edf:	89 d1                	mov    %edx,%ecx
  800ee1:	39 d3                	cmp    %edx,%ebx
  800ee3:	0f 82 87 00 00 00    	jb     800f70 <__umoddi3+0x134>
  800ee9:	0f 84 91 00 00 00    	je     800f80 <__umoddi3+0x144>
  800eef:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ef3:	29 f2                	sub    %esi,%edx
  800ef5:	19 cb                	sbb    %ecx,%ebx
  800ef7:	89 d8                	mov    %ebx,%eax
  800ef9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800efd:	d3 e0                	shl    %cl,%eax
  800eff:	89 e9                	mov    %ebp,%ecx
  800f01:	d3 ea                	shr    %cl,%edx
  800f03:	09 d0                	or     %edx,%eax
  800f05:	89 e9                	mov    %ebp,%ecx
  800f07:	d3 eb                	shr    %cl,%ebx
  800f09:	89 da                	mov    %ebx,%edx
  800f0b:	83 c4 1c             	add    $0x1c,%esp
  800f0e:	5b                   	pop    %ebx
  800f0f:	5e                   	pop    %esi
  800f10:	5f                   	pop    %edi
  800f11:	5d                   	pop    %ebp
  800f12:	c3                   	ret    
  800f13:	90                   	nop
  800f14:	89 fd                	mov    %edi,%ebp
  800f16:	85 ff                	test   %edi,%edi
  800f18:	75 0b                	jne    800f25 <__umoddi3+0xe9>
  800f1a:	b8 01 00 00 00       	mov    $0x1,%eax
  800f1f:	31 d2                	xor    %edx,%edx
  800f21:	f7 f7                	div    %edi
  800f23:	89 c5                	mov    %eax,%ebp
  800f25:	89 f0                	mov    %esi,%eax
  800f27:	31 d2                	xor    %edx,%edx
  800f29:	f7 f5                	div    %ebp
  800f2b:	89 c8                	mov    %ecx,%eax
  800f2d:	f7 f5                	div    %ebp
  800f2f:	89 d0                	mov    %edx,%eax
  800f31:	e9 44 ff ff ff       	jmp    800e7a <__umoddi3+0x3e>
  800f36:	66 90                	xchg   %ax,%ax
  800f38:	89 c8                	mov    %ecx,%eax
  800f3a:	89 f2                	mov    %esi,%edx
  800f3c:	83 c4 1c             	add    $0x1c,%esp
  800f3f:	5b                   	pop    %ebx
  800f40:	5e                   	pop    %esi
  800f41:	5f                   	pop    %edi
  800f42:	5d                   	pop    %ebp
  800f43:	c3                   	ret    
  800f44:	3b 04 24             	cmp    (%esp),%eax
  800f47:	72 06                	jb     800f4f <__umoddi3+0x113>
  800f49:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f4d:	77 0f                	ja     800f5e <__umoddi3+0x122>
  800f4f:	89 f2                	mov    %esi,%edx
  800f51:	29 f9                	sub    %edi,%ecx
  800f53:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f57:	89 14 24             	mov    %edx,(%esp)
  800f5a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f5e:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f62:	8b 14 24             	mov    (%esp),%edx
  800f65:	83 c4 1c             	add    $0x1c,%esp
  800f68:	5b                   	pop    %ebx
  800f69:	5e                   	pop    %esi
  800f6a:	5f                   	pop    %edi
  800f6b:	5d                   	pop    %ebp
  800f6c:	c3                   	ret    
  800f6d:	8d 76 00             	lea    0x0(%esi),%esi
  800f70:	2b 04 24             	sub    (%esp),%eax
  800f73:	19 fa                	sbb    %edi,%edx
  800f75:	89 d1                	mov    %edx,%ecx
  800f77:	89 c6                	mov    %eax,%esi
  800f79:	e9 71 ff ff ff       	jmp    800eef <__umoddi3+0xb3>
  800f7e:	66 90                	xchg   %ax,%ax
  800f80:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f84:	72 ea                	jb     800f70 <__umoddi3+0x134>
  800f86:	89 d9                	mov    %ebx,%ecx
  800f88:	e9 62 ff ff ff       	jmp    800eef <__umoddi3+0xb3>
