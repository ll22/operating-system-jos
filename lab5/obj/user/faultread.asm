
obj/user/faultread.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 1d 00 00 00       	call   80004e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	cprintf("I read %08x from location 0!\n", *(unsigned*)0);
  800039:	ff 35 00 00 00 00    	pushl  0x0
  80003f:	68 60 0f 80 00       	push   $0x800f60
  800044:	e8 f8 00 00 00       	call   800141 <cprintf>
}
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	c9                   	leave  
  80004d:	c3                   	ret    

0080004e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004e:	55                   	push   %ebp
  80004f:	89 e5                	mov    %esp,%ebp
  800051:	56                   	push   %esi
  800052:	53                   	push   %ebx
  800053:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800056:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800059:	e8 ed 09 00 00       	call   800a4b <sys_getenvid>
  80005e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800063:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80006a:	c1 e0 07             	shl    $0x7,%eax
  80006d:	29 d0                	sub    %edx,%eax
  80006f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800074:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800079:	85 db                	test   %ebx,%ebx
  80007b:	7e 07                	jle    800084 <libmain+0x36>
		binaryname = argv[0];
  80007d:	8b 06                	mov    (%esi),%eax
  80007f:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800084:	83 ec 08             	sub    $0x8,%esp
  800087:	56                   	push   %esi
  800088:	53                   	push   %ebx
  800089:	e8 a5 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008e:	e8 0a 00 00 00       	call   80009d <exit>
}
  800093:	83 c4 10             	add    $0x10,%esp
  800096:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800099:	5b                   	pop    %ebx
  80009a:	5e                   	pop    %esi
  80009b:	5d                   	pop    %ebp
  80009c:	c3                   	ret    

0080009d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80009d:	55                   	push   %ebp
  80009e:	89 e5                	mov    %esp,%ebp
  8000a0:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000a3:	6a 00                	push   $0x0
  8000a5:	e8 60 09 00 00       	call   800a0a <sys_env_destroy>
}
  8000aa:	83 c4 10             	add    $0x10,%esp
  8000ad:	c9                   	leave  
  8000ae:	c3                   	ret    

008000af <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000af:	55                   	push   %ebp
  8000b0:	89 e5                	mov    %esp,%ebp
  8000b2:	53                   	push   %ebx
  8000b3:	83 ec 04             	sub    $0x4,%esp
  8000b6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000b9:	8b 13                	mov    (%ebx),%edx
  8000bb:	8d 42 01             	lea    0x1(%edx),%eax
  8000be:	89 03                	mov    %eax,(%ebx)
  8000c0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000c3:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000c7:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000cc:	75 1a                	jne    8000e8 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000ce:	83 ec 08             	sub    $0x8,%esp
  8000d1:	68 ff 00 00 00       	push   $0xff
  8000d6:	8d 43 08             	lea    0x8(%ebx),%eax
  8000d9:	50                   	push   %eax
  8000da:	e8 ee 08 00 00       	call   8009cd <sys_cputs>
		b->idx = 0;
  8000df:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8000e5:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8000e8:	ff 43 04             	incl   0x4(%ebx)
}
  8000eb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000ee:	c9                   	leave  
  8000ef:	c3                   	ret    

008000f0 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8000f0:	55                   	push   %ebp
  8000f1:	89 e5                	mov    %esp,%ebp
  8000f3:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8000f9:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800100:	00 00 00 
	b.cnt = 0;
  800103:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80010a:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80010d:	ff 75 0c             	pushl  0xc(%ebp)
  800110:	ff 75 08             	pushl  0x8(%ebp)
  800113:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800119:	50                   	push   %eax
  80011a:	68 af 00 80 00       	push   $0x8000af
  80011f:	e8 51 01 00 00       	call   800275 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800124:	83 c4 08             	add    $0x8,%esp
  800127:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80012d:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800133:	50                   	push   %eax
  800134:	e8 94 08 00 00       	call   8009cd <sys_cputs>

	return b.cnt;
}
  800139:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80013f:	c9                   	leave  
  800140:	c3                   	ret    

00800141 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800141:	55                   	push   %ebp
  800142:	89 e5                	mov    %esp,%ebp
  800144:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800147:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80014a:	50                   	push   %eax
  80014b:	ff 75 08             	pushl  0x8(%ebp)
  80014e:	e8 9d ff ff ff       	call   8000f0 <vcprintf>
	va_end(ap);

	return cnt;
}
  800153:	c9                   	leave  
  800154:	c3                   	ret    

00800155 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800155:	55                   	push   %ebp
  800156:	89 e5                	mov    %esp,%ebp
  800158:	57                   	push   %edi
  800159:	56                   	push   %esi
  80015a:	53                   	push   %ebx
  80015b:	83 ec 1c             	sub    $0x1c,%esp
  80015e:	89 c7                	mov    %eax,%edi
  800160:	89 d6                	mov    %edx,%esi
  800162:	8b 45 08             	mov    0x8(%ebp),%eax
  800165:	8b 55 0c             	mov    0xc(%ebp),%edx
  800168:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80016b:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80016e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800171:	bb 00 00 00 00       	mov    $0x0,%ebx
  800176:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800179:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80017c:	39 d3                	cmp    %edx,%ebx
  80017e:	72 05                	jb     800185 <printnum+0x30>
  800180:	39 45 10             	cmp    %eax,0x10(%ebp)
  800183:	77 45                	ja     8001ca <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800185:	83 ec 0c             	sub    $0xc,%esp
  800188:	ff 75 18             	pushl  0x18(%ebp)
  80018b:	8b 45 14             	mov    0x14(%ebp),%eax
  80018e:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800191:	53                   	push   %ebx
  800192:	ff 75 10             	pushl  0x10(%ebp)
  800195:	83 ec 08             	sub    $0x8,%esp
  800198:	ff 75 e4             	pushl  -0x1c(%ebp)
  80019b:	ff 75 e0             	pushl  -0x20(%ebp)
  80019e:	ff 75 dc             	pushl  -0x24(%ebp)
  8001a1:	ff 75 d8             	pushl  -0x28(%ebp)
  8001a4:	e8 37 0b 00 00       	call   800ce0 <__udivdi3>
  8001a9:	83 c4 18             	add    $0x18,%esp
  8001ac:	52                   	push   %edx
  8001ad:	50                   	push   %eax
  8001ae:	89 f2                	mov    %esi,%edx
  8001b0:	89 f8                	mov    %edi,%eax
  8001b2:	e8 9e ff ff ff       	call   800155 <printnum>
  8001b7:	83 c4 20             	add    $0x20,%esp
  8001ba:	eb 16                	jmp    8001d2 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001bc:	83 ec 08             	sub    $0x8,%esp
  8001bf:	56                   	push   %esi
  8001c0:	ff 75 18             	pushl  0x18(%ebp)
  8001c3:	ff d7                	call   *%edi
  8001c5:	83 c4 10             	add    $0x10,%esp
  8001c8:	eb 03                	jmp    8001cd <printnum+0x78>
  8001ca:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001cd:	4b                   	dec    %ebx
  8001ce:	85 db                	test   %ebx,%ebx
  8001d0:	7f ea                	jg     8001bc <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001d2:	83 ec 08             	sub    $0x8,%esp
  8001d5:	56                   	push   %esi
  8001d6:	83 ec 04             	sub    $0x4,%esp
  8001d9:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001dc:	ff 75 e0             	pushl  -0x20(%ebp)
  8001df:	ff 75 dc             	pushl  -0x24(%ebp)
  8001e2:	ff 75 d8             	pushl  -0x28(%ebp)
  8001e5:	e8 06 0c 00 00       	call   800df0 <__umoddi3>
  8001ea:	83 c4 14             	add    $0x14,%esp
  8001ed:	0f be 80 88 0f 80 00 	movsbl 0x800f88(%eax),%eax
  8001f4:	50                   	push   %eax
  8001f5:	ff d7                	call   *%edi
}
  8001f7:	83 c4 10             	add    $0x10,%esp
  8001fa:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001fd:	5b                   	pop    %ebx
  8001fe:	5e                   	pop    %esi
  8001ff:	5f                   	pop    %edi
  800200:	5d                   	pop    %ebp
  800201:	c3                   	ret    

00800202 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800202:	55                   	push   %ebp
  800203:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800205:	83 fa 01             	cmp    $0x1,%edx
  800208:	7e 0e                	jle    800218 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80020a:	8b 10                	mov    (%eax),%edx
  80020c:	8d 4a 08             	lea    0x8(%edx),%ecx
  80020f:	89 08                	mov    %ecx,(%eax)
  800211:	8b 02                	mov    (%edx),%eax
  800213:	8b 52 04             	mov    0x4(%edx),%edx
  800216:	eb 22                	jmp    80023a <getuint+0x38>
	else if (lflag)
  800218:	85 d2                	test   %edx,%edx
  80021a:	74 10                	je     80022c <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80021c:	8b 10                	mov    (%eax),%edx
  80021e:	8d 4a 04             	lea    0x4(%edx),%ecx
  800221:	89 08                	mov    %ecx,(%eax)
  800223:	8b 02                	mov    (%edx),%eax
  800225:	ba 00 00 00 00       	mov    $0x0,%edx
  80022a:	eb 0e                	jmp    80023a <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80022c:	8b 10                	mov    (%eax),%edx
  80022e:	8d 4a 04             	lea    0x4(%edx),%ecx
  800231:	89 08                	mov    %ecx,(%eax)
  800233:	8b 02                	mov    (%edx),%eax
  800235:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80023a:	5d                   	pop    %ebp
  80023b:	c3                   	ret    

0080023c <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80023c:	55                   	push   %ebp
  80023d:	89 e5                	mov    %esp,%ebp
  80023f:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800242:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800245:	8b 10                	mov    (%eax),%edx
  800247:	3b 50 04             	cmp    0x4(%eax),%edx
  80024a:	73 0a                	jae    800256 <sprintputch+0x1a>
		*b->buf++ = ch;
  80024c:	8d 4a 01             	lea    0x1(%edx),%ecx
  80024f:	89 08                	mov    %ecx,(%eax)
  800251:	8b 45 08             	mov    0x8(%ebp),%eax
  800254:	88 02                	mov    %al,(%edx)
}
  800256:	5d                   	pop    %ebp
  800257:	c3                   	ret    

00800258 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800258:	55                   	push   %ebp
  800259:	89 e5                	mov    %esp,%ebp
  80025b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80025e:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800261:	50                   	push   %eax
  800262:	ff 75 10             	pushl  0x10(%ebp)
  800265:	ff 75 0c             	pushl  0xc(%ebp)
  800268:	ff 75 08             	pushl  0x8(%ebp)
  80026b:	e8 05 00 00 00       	call   800275 <vprintfmt>
	va_end(ap);
}
  800270:	83 c4 10             	add    $0x10,%esp
  800273:	c9                   	leave  
  800274:	c3                   	ret    

00800275 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800275:	55                   	push   %ebp
  800276:	89 e5                	mov    %esp,%ebp
  800278:	57                   	push   %edi
  800279:	56                   	push   %esi
  80027a:	53                   	push   %ebx
  80027b:	83 ec 2c             	sub    $0x2c,%esp
  80027e:	8b 75 08             	mov    0x8(%ebp),%esi
  800281:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800284:	8b 7d 10             	mov    0x10(%ebp),%edi
  800287:	eb 12                	jmp    80029b <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800289:	85 c0                	test   %eax,%eax
  80028b:	0f 84 68 03 00 00    	je     8005f9 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800291:	83 ec 08             	sub    $0x8,%esp
  800294:	53                   	push   %ebx
  800295:	50                   	push   %eax
  800296:	ff d6                	call   *%esi
  800298:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80029b:	47                   	inc    %edi
  80029c:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002a0:	83 f8 25             	cmp    $0x25,%eax
  8002a3:	75 e4                	jne    800289 <vprintfmt+0x14>
  8002a5:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002a9:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002b0:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002b7:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002be:	ba 00 00 00 00       	mov    $0x0,%edx
  8002c3:	eb 07                	jmp    8002cc <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002c5:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002c8:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002cc:	8d 47 01             	lea    0x1(%edi),%eax
  8002cf:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002d2:	0f b6 0f             	movzbl (%edi),%ecx
  8002d5:	8a 07                	mov    (%edi),%al
  8002d7:	83 e8 23             	sub    $0x23,%eax
  8002da:	3c 55                	cmp    $0x55,%al
  8002dc:	0f 87 fe 02 00 00    	ja     8005e0 <vprintfmt+0x36b>
  8002e2:	0f b6 c0             	movzbl %al,%eax
  8002e5:	ff 24 85 c0 10 80 00 	jmp    *0x8010c0(,%eax,4)
  8002ec:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8002ef:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8002f3:	eb d7                	jmp    8002cc <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8002f8:	b8 00 00 00 00       	mov    $0x0,%eax
  8002fd:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800300:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800303:	01 c0                	add    %eax,%eax
  800305:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800309:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80030c:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80030f:	83 fa 09             	cmp    $0x9,%edx
  800312:	77 34                	ja     800348 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800314:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800315:	eb e9                	jmp    800300 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800317:	8b 45 14             	mov    0x14(%ebp),%eax
  80031a:	8d 48 04             	lea    0x4(%eax),%ecx
  80031d:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800320:	8b 00                	mov    (%eax),%eax
  800322:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800325:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800328:	eb 24                	jmp    80034e <vprintfmt+0xd9>
  80032a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80032e:	79 07                	jns    800337 <vprintfmt+0xc2>
  800330:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800337:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80033a:	eb 90                	jmp    8002cc <vprintfmt+0x57>
  80033c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80033f:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800346:	eb 84                	jmp    8002cc <vprintfmt+0x57>
  800348:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80034b:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80034e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800352:	0f 89 74 ff ff ff    	jns    8002cc <vprintfmt+0x57>
				width = precision, precision = -1;
  800358:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80035b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80035e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800365:	e9 62 ff ff ff       	jmp    8002cc <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80036a:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80036b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80036e:	e9 59 ff ff ff       	jmp    8002cc <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800373:	8b 45 14             	mov    0x14(%ebp),%eax
  800376:	8d 50 04             	lea    0x4(%eax),%edx
  800379:	89 55 14             	mov    %edx,0x14(%ebp)
  80037c:	83 ec 08             	sub    $0x8,%esp
  80037f:	53                   	push   %ebx
  800380:	ff 30                	pushl  (%eax)
  800382:	ff d6                	call   *%esi
			break;
  800384:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800387:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80038a:	e9 0c ff ff ff       	jmp    80029b <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80038f:	8b 45 14             	mov    0x14(%ebp),%eax
  800392:	8d 50 04             	lea    0x4(%eax),%edx
  800395:	89 55 14             	mov    %edx,0x14(%ebp)
  800398:	8b 00                	mov    (%eax),%eax
  80039a:	85 c0                	test   %eax,%eax
  80039c:	79 02                	jns    8003a0 <vprintfmt+0x12b>
  80039e:	f7 d8                	neg    %eax
  8003a0:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003a2:	83 f8 0f             	cmp    $0xf,%eax
  8003a5:	7f 0b                	jg     8003b2 <vprintfmt+0x13d>
  8003a7:	8b 04 85 20 12 80 00 	mov    0x801220(,%eax,4),%eax
  8003ae:	85 c0                	test   %eax,%eax
  8003b0:	75 18                	jne    8003ca <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003b2:	52                   	push   %edx
  8003b3:	68 a0 0f 80 00       	push   $0x800fa0
  8003b8:	53                   	push   %ebx
  8003b9:	56                   	push   %esi
  8003ba:	e8 99 fe ff ff       	call   800258 <printfmt>
  8003bf:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c2:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003c5:	e9 d1 fe ff ff       	jmp    80029b <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003ca:	50                   	push   %eax
  8003cb:	68 a9 0f 80 00       	push   $0x800fa9
  8003d0:	53                   	push   %ebx
  8003d1:	56                   	push   %esi
  8003d2:	e8 81 fe ff ff       	call   800258 <printfmt>
  8003d7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003da:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003dd:	e9 b9 fe ff ff       	jmp    80029b <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8003e2:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e5:	8d 50 04             	lea    0x4(%eax),%edx
  8003e8:	89 55 14             	mov    %edx,0x14(%ebp)
  8003eb:	8b 38                	mov    (%eax),%edi
  8003ed:	85 ff                	test   %edi,%edi
  8003ef:	75 05                	jne    8003f6 <vprintfmt+0x181>
				p = "(null)";
  8003f1:	bf 99 0f 80 00       	mov    $0x800f99,%edi
			if (width > 0 && padc != '-')
  8003f6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003fa:	0f 8e 90 00 00 00    	jle    800490 <vprintfmt+0x21b>
  800400:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800404:	0f 84 8e 00 00 00    	je     800498 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80040a:	83 ec 08             	sub    $0x8,%esp
  80040d:	ff 75 d0             	pushl  -0x30(%ebp)
  800410:	57                   	push   %edi
  800411:	e8 70 02 00 00       	call   800686 <strnlen>
  800416:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800419:	29 c1                	sub    %eax,%ecx
  80041b:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80041e:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800421:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800425:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800428:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80042b:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80042d:	eb 0d                	jmp    80043c <vprintfmt+0x1c7>
					putch(padc, putdat);
  80042f:	83 ec 08             	sub    $0x8,%esp
  800432:	53                   	push   %ebx
  800433:	ff 75 e4             	pushl  -0x1c(%ebp)
  800436:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800438:	4f                   	dec    %edi
  800439:	83 c4 10             	add    $0x10,%esp
  80043c:	85 ff                	test   %edi,%edi
  80043e:	7f ef                	jg     80042f <vprintfmt+0x1ba>
  800440:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800443:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800446:	89 c8                	mov    %ecx,%eax
  800448:	85 c9                	test   %ecx,%ecx
  80044a:	79 05                	jns    800451 <vprintfmt+0x1dc>
  80044c:	b8 00 00 00 00       	mov    $0x0,%eax
  800451:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800454:	29 c1                	sub    %eax,%ecx
  800456:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800459:	89 75 08             	mov    %esi,0x8(%ebp)
  80045c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80045f:	eb 3d                	jmp    80049e <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800461:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800465:	74 19                	je     800480 <vprintfmt+0x20b>
  800467:	0f be c0             	movsbl %al,%eax
  80046a:	83 e8 20             	sub    $0x20,%eax
  80046d:	83 f8 5e             	cmp    $0x5e,%eax
  800470:	76 0e                	jbe    800480 <vprintfmt+0x20b>
					putch('?', putdat);
  800472:	83 ec 08             	sub    $0x8,%esp
  800475:	53                   	push   %ebx
  800476:	6a 3f                	push   $0x3f
  800478:	ff 55 08             	call   *0x8(%ebp)
  80047b:	83 c4 10             	add    $0x10,%esp
  80047e:	eb 0b                	jmp    80048b <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800480:	83 ec 08             	sub    $0x8,%esp
  800483:	53                   	push   %ebx
  800484:	52                   	push   %edx
  800485:	ff 55 08             	call   *0x8(%ebp)
  800488:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80048b:	ff 4d e4             	decl   -0x1c(%ebp)
  80048e:	eb 0e                	jmp    80049e <vprintfmt+0x229>
  800490:	89 75 08             	mov    %esi,0x8(%ebp)
  800493:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800496:	eb 06                	jmp    80049e <vprintfmt+0x229>
  800498:	89 75 08             	mov    %esi,0x8(%ebp)
  80049b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80049e:	47                   	inc    %edi
  80049f:	8a 47 ff             	mov    -0x1(%edi),%al
  8004a2:	0f be d0             	movsbl %al,%edx
  8004a5:	85 d2                	test   %edx,%edx
  8004a7:	74 1d                	je     8004c6 <vprintfmt+0x251>
  8004a9:	85 f6                	test   %esi,%esi
  8004ab:	78 b4                	js     800461 <vprintfmt+0x1ec>
  8004ad:	4e                   	dec    %esi
  8004ae:	79 b1                	jns    800461 <vprintfmt+0x1ec>
  8004b0:	8b 75 08             	mov    0x8(%ebp),%esi
  8004b3:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004b6:	eb 14                	jmp    8004cc <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004b8:	83 ec 08             	sub    $0x8,%esp
  8004bb:	53                   	push   %ebx
  8004bc:	6a 20                	push   $0x20
  8004be:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004c0:	4f                   	dec    %edi
  8004c1:	83 c4 10             	add    $0x10,%esp
  8004c4:	eb 06                	jmp    8004cc <vprintfmt+0x257>
  8004c6:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004c9:	8b 75 08             	mov    0x8(%ebp),%esi
  8004cc:	85 ff                	test   %edi,%edi
  8004ce:	7f e8                	jg     8004b8 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004d0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004d3:	e9 c3 fd ff ff       	jmp    80029b <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004d8:	83 fa 01             	cmp    $0x1,%edx
  8004db:	7e 16                	jle    8004f3 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8004dd:	8b 45 14             	mov    0x14(%ebp),%eax
  8004e0:	8d 50 08             	lea    0x8(%eax),%edx
  8004e3:	89 55 14             	mov    %edx,0x14(%ebp)
  8004e6:	8b 50 04             	mov    0x4(%eax),%edx
  8004e9:	8b 00                	mov    (%eax),%eax
  8004eb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004ee:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8004f1:	eb 32                	jmp    800525 <vprintfmt+0x2b0>
	else if (lflag)
  8004f3:	85 d2                	test   %edx,%edx
  8004f5:	74 18                	je     80050f <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8004f7:	8b 45 14             	mov    0x14(%ebp),%eax
  8004fa:	8d 50 04             	lea    0x4(%eax),%edx
  8004fd:	89 55 14             	mov    %edx,0x14(%ebp)
  800500:	8b 00                	mov    (%eax),%eax
  800502:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800505:	89 c1                	mov    %eax,%ecx
  800507:	c1 f9 1f             	sar    $0x1f,%ecx
  80050a:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80050d:	eb 16                	jmp    800525 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80050f:	8b 45 14             	mov    0x14(%ebp),%eax
  800512:	8d 50 04             	lea    0x4(%eax),%edx
  800515:	89 55 14             	mov    %edx,0x14(%ebp)
  800518:	8b 00                	mov    (%eax),%eax
  80051a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80051d:	89 c1                	mov    %eax,%ecx
  80051f:	c1 f9 1f             	sar    $0x1f,%ecx
  800522:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800525:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800528:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80052b:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80052f:	79 76                	jns    8005a7 <vprintfmt+0x332>
				putch('-', putdat);
  800531:	83 ec 08             	sub    $0x8,%esp
  800534:	53                   	push   %ebx
  800535:	6a 2d                	push   $0x2d
  800537:	ff d6                	call   *%esi
				num = -(long long) num;
  800539:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80053c:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80053f:	f7 d8                	neg    %eax
  800541:	83 d2 00             	adc    $0x0,%edx
  800544:	f7 da                	neg    %edx
  800546:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800549:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80054e:	eb 5c                	jmp    8005ac <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800550:	8d 45 14             	lea    0x14(%ebp),%eax
  800553:	e8 aa fc ff ff       	call   800202 <getuint>
			base = 10;
  800558:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80055d:	eb 4d                	jmp    8005ac <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80055f:	8d 45 14             	lea    0x14(%ebp),%eax
  800562:	e8 9b fc ff ff       	call   800202 <getuint>
			base = 8;
  800567:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80056c:	eb 3e                	jmp    8005ac <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80056e:	83 ec 08             	sub    $0x8,%esp
  800571:	53                   	push   %ebx
  800572:	6a 30                	push   $0x30
  800574:	ff d6                	call   *%esi
			putch('x', putdat);
  800576:	83 c4 08             	add    $0x8,%esp
  800579:	53                   	push   %ebx
  80057a:	6a 78                	push   $0x78
  80057c:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80057e:	8b 45 14             	mov    0x14(%ebp),%eax
  800581:	8d 50 04             	lea    0x4(%eax),%edx
  800584:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800587:	8b 00                	mov    (%eax),%eax
  800589:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80058e:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800591:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800596:	eb 14                	jmp    8005ac <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800598:	8d 45 14             	lea    0x14(%ebp),%eax
  80059b:	e8 62 fc ff ff       	call   800202 <getuint>
			base = 16;
  8005a0:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005a5:	eb 05                	jmp    8005ac <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005a7:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005ac:	83 ec 0c             	sub    $0xc,%esp
  8005af:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005b3:	57                   	push   %edi
  8005b4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005b7:	51                   	push   %ecx
  8005b8:	52                   	push   %edx
  8005b9:	50                   	push   %eax
  8005ba:	89 da                	mov    %ebx,%edx
  8005bc:	89 f0                	mov    %esi,%eax
  8005be:	e8 92 fb ff ff       	call   800155 <printnum>
			break;
  8005c3:	83 c4 20             	add    $0x20,%esp
  8005c6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005c9:	e9 cd fc ff ff       	jmp    80029b <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005ce:	83 ec 08             	sub    $0x8,%esp
  8005d1:	53                   	push   %ebx
  8005d2:	51                   	push   %ecx
  8005d3:	ff d6                	call   *%esi
			break;
  8005d5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8005db:	e9 bb fc ff ff       	jmp    80029b <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8005e0:	83 ec 08             	sub    $0x8,%esp
  8005e3:	53                   	push   %ebx
  8005e4:	6a 25                	push   $0x25
  8005e6:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8005e8:	83 c4 10             	add    $0x10,%esp
  8005eb:	eb 01                	jmp    8005ee <vprintfmt+0x379>
  8005ed:	4f                   	dec    %edi
  8005ee:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8005f2:	75 f9                	jne    8005ed <vprintfmt+0x378>
  8005f4:	e9 a2 fc ff ff       	jmp    80029b <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8005f9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8005fc:	5b                   	pop    %ebx
  8005fd:	5e                   	pop    %esi
  8005fe:	5f                   	pop    %edi
  8005ff:	5d                   	pop    %ebp
  800600:	c3                   	ret    

00800601 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800601:	55                   	push   %ebp
  800602:	89 e5                	mov    %esp,%ebp
  800604:	83 ec 18             	sub    $0x18,%esp
  800607:	8b 45 08             	mov    0x8(%ebp),%eax
  80060a:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80060d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800610:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800614:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800617:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80061e:	85 c0                	test   %eax,%eax
  800620:	74 26                	je     800648 <vsnprintf+0x47>
  800622:	85 d2                	test   %edx,%edx
  800624:	7e 29                	jle    80064f <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800626:	ff 75 14             	pushl  0x14(%ebp)
  800629:	ff 75 10             	pushl  0x10(%ebp)
  80062c:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80062f:	50                   	push   %eax
  800630:	68 3c 02 80 00       	push   $0x80023c
  800635:	e8 3b fc ff ff       	call   800275 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80063a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80063d:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800640:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800643:	83 c4 10             	add    $0x10,%esp
  800646:	eb 0c                	jmp    800654 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800648:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80064d:	eb 05                	jmp    800654 <vsnprintf+0x53>
  80064f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800654:	c9                   	leave  
  800655:	c3                   	ret    

00800656 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800656:	55                   	push   %ebp
  800657:	89 e5                	mov    %esp,%ebp
  800659:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80065c:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80065f:	50                   	push   %eax
  800660:	ff 75 10             	pushl  0x10(%ebp)
  800663:	ff 75 0c             	pushl  0xc(%ebp)
  800666:	ff 75 08             	pushl  0x8(%ebp)
  800669:	e8 93 ff ff ff       	call   800601 <vsnprintf>
	va_end(ap);

	return rc;
}
  80066e:	c9                   	leave  
  80066f:	c3                   	ret    

00800670 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800670:	55                   	push   %ebp
  800671:	89 e5                	mov    %esp,%ebp
  800673:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800676:	b8 00 00 00 00       	mov    $0x0,%eax
  80067b:	eb 01                	jmp    80067e <strlen+0xe>
		n++;
  80067d:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80067e:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800682:	75 f9                	jne    80067d <strlen+0xd>
		n++;
	return n;
}
  800684:	5d                   	pop    %ebp
  800685:	c3                   	ret    

00800686 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800686:	55                   	push   %ebp
  800687:	89 e5                	mov    %esp,%ebp
  800689:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80068c:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80068f:	ba 00 00 00 00       	mov    $0x0,%edx
  800694:	eb 01                	jmp    800697 <strnlen+0x11>
		n++;
  800696:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800697:	39 c2                	cmp    %eax,%edx
  800699:	74 08                	je     8006a3 <strnlen+0x1d>
  80069b:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80069f:	75 f5                	jne    800696 <strnlen+0x10>
  8006a1:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006a3:	5d                   	pop    %ebp
  8006a4:	c3                   	ret    

008006a5 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006a5:	55                   	push   %ebp
  8006a6:	89 e5                	mov    %esp,%ebp
  8006a8:	53                   	push   %ebx
  8006a9:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006af:	89 c2                	mov    %eax,%edx
  8006b1:	42                   	inc    %edx
  8006b2:	41                   	inc    %ecx
  8006b3:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006b6:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006b9:	84 db                	test   %bl,%bl
  8006bb:	75 f4                	jne    8006b1 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006bd:	5b                   	pop    %ebx
  8006be:	5d                   	pop    %ebp
  8006bf:	c3                   	ret    

008006c0 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006c0:	55                   	push   %ebp
  8006c1:	89 e5                	mov    %esp,%ebp
  8006c3:	53                   	push   %ebx
  8006c4:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006c7:	53                   	push   %ebx
  8006c8:	e8 a3 ff ff ff       	call   800670 <strlen>
  8006cd:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006d0:	ff 75 0c             	pushl  0xc(%ebp)
  8006d3:	01 d8                	add    %ebx,%eax
  8006d5:	50                   	push   %eax
  8006d6:	e8 ca ff ff ff       	call   8006a5 <strcpy>
	return dst;
}
  8006db:	89 d8                	mov    %ebx,%eax
  8006dd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006e0:	c9                   	leave  
  8006e1:	c3                   	ret    

008006e2 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8006e2:	55                   	push   %ebp
  8006e3:	89 e5                	mov    %esp,%ebp
  8006e5:	56                   	push   %esi
  8006e6:	53                   	push   %ebx
  8006e7:	8b 75 08             	mov    0x8(%ebp),%esi
  8006ea:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006ed:	89 f3                	mov    %esi,%ebx
  8006ef:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8006f2:	89 f2                	mov    %esi,%edx
  8006f4:	eb 0c                	jmp    800702 <strncpy+0x20>
		*dst++ = *src;
  8006f6:	42                   	inc    %edx
  8006f7:	8a 01                	mov    (%ecx),%al
  8006f9:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8006fc:	80 39 01             	cmpb   $0x1,(%ecx)
  8006ff:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800702:	39 da                	cmp    %ebx,%edx
  800704:	75 f0                	jne    8006f6 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800706:	89 f0                	mov    %esi,%eax
  800708:	5b                   	pop    %ebx
  800709:	5e                   	pop    %esi
  80070a:	5d                   	pop    %ebp
  80070b:	c3                   	ret    

0080070c <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80070c:	55                   	push   %ebp
  80070d:	89 e5                	mov    %esp,%ebp
  80070f:	56                   	push   %esi
  800710:	53                   	push   %ebx
  800711:	8b 75 08             	mov    0x8(%ebp),%esi
  800714:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800717:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80071a:	85 c0                	test   %eax,%eax
  80071c:	74 1e                	je     80073c <strlcpy+0x30>
  80071e:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800722:	89 f2                	mov    %esi,%edx
  800724:	eb 05                	jmp    80072b <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800726:	42                   	inc    %edx
  800727:	41                   	inc    %ecx
  800728:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80072b:	39 c2                	cmp    %eax,%edx
  80072d:	74 08                	je     800737 <strlcpy+0x2b>
  80072f:	8a 19                	mov    (%ecx),%bl
  800731:	84 db                	test   %bl,%bl
  800733:	75 f1                	jne    800726 <strlcpy+0x1a>
  800735:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800737:	c6 00 00             	movb   $0x0,(%eax)
  80073a:	eb 02                	jmp    80073e <strlcpy+0x32>
  80073c:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80073e:	29 f0                	sub    %esi,%eax
}
  800740:	5b                   	pop    %ebx
  800741:	5e                   	pop    %esi
  800742:	5d                   	pop    %ebp
  800743:	c3                   	ret    

00800744 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800744:	55                   	push   %ebp
  800745:	89 e5                	mov    %esp,%ebp
  800747:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80074a:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80074d:	eb 02                	jmp    800751 <strcmp+0xd>
		p++, q++;
  80074f:	41                   	inc    %ecx
  800750:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800751:	8a 01                	mov    (%ecx),%al
  800753:	84 c0                	test   %al,%al
  800755:	74 04                	je     80075b <strcmp+0x17>
  800757:	3a 02                	cmp    (%edx),%al
  800759:	74 f4                	je     80074f <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80075b:	0f b6 c0             	movzbl %al,%eax
  80075e:	0f b6 12             	movzbl (%edx),%edx
  800761:	29 d0                	sub    %edx,%eax
}
  800763:	5d                   	pop    %ebp
  800764:	c3                   	ret    

00800765 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800765:	55                   	push   %ebp
  800766:	89 e5                	mov    %esp,%ebp
  800768:	53                   	push   %ebx
  800769:	8b 45 08             	mov    0x8(%ebp),%eax
  80076c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80076f:	89 c3                	mov    %eax,%ebx
  800771:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800774:	eb 02                	jmp    800778 <strncmp+0x13>
		n--, p++, q++;
  800776:	40                   	inc    %eax
  800777:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800778:	39 d8                	cmp    %ebx,%eax
  80077a:	74 14                	je     800790 <strncmp+0x2b>
  80077c:	8a 08                	mov    (%eax),%cl
  80077e:	84 c9                	test   %cl,%cl
  800780:	74 04                	je     800786 <strncmp+0x21>
  800782:	3a 0a                	cmp    (%edx),%cl
  800784:	74 f0                	je     800776 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800786:	0f b6 00             	movzbl (%eax),%eax
  800789:	0f b6 12             	movzbl (%edx),%edx
  80078c:	29 d0                	sub    %edx,%eax
  80078e:	eb 05                	jmp    800795 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800790:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800795:	5b                   	pop    %ebx
  800796:	5d                   	pop    %ebp
  800797:	c3                   	ret    

00800798 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800798:	55                   	push   %ebp
  800799:	89 e5                	mov    %esp,%ebp
  80079b:	8b 45 08             	mov    0x8(%ebp),%eax
  80079e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007a1:	eb 05                	jmp    8007a8 <strchr+0x10>
		if (*s == c)
  8007a3:	38 ca                	cmp    %cl,%dl
  8007a5:	74 0c                	je     8007b3 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007a7:	40                   	inc    %eax
  8007a8:	8a 10                	mov    (%eax),%dl
  8007aa:	84 d2                	test   %dl,%dl
  8007ac:	75 f5                	jne    8007a3 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007ae:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007b3:	5d                   	pop    %ebp
  8007b4:	c3                   	ret    

008007b5 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007b5:	55                   	push   %ebp
  8007b6:	89 e5                	mov    %esp,%ebp
  8007b8:	8b 45 08             	mov    0x8(%ebp),%eax
  8007bb:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007be:	eb 05                	jmp    8007c5 <strfind+0x10>
		if (*s == c)
  8007c0:	38 ca                	cmp    %cl,%dl
  8007c2:	74 07                	je     8007cb <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007c4:	40                   	inc    %eax
  8007c5:	8a 10                	mov    (%eax),%dl
  8007c7:	84 d2                	test   %dl,%dl
  8007c9:	75 f5                	jne    8007c0 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007cb:	5d                   	pop    %ebp
  8007cc:	c3                   	ret    

008007cd <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007cd:	55                   	push   %ebp
  8007ce:	89 e5                	mov    %esp,%ebp
  8007d0:	57                   	push   %edi
  8007d1:	56                   	push   %esi
  8007d2:	53                   	push   %ebx
  8007d3:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007d6:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007d9:	85 c9                	test   %ecx,%ecx
  8007db:	74 36                	je     800813 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8007dd:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8007e3:	75 28                	jne    80080d <memset+0x40>
  8007e5:	f6 c1 03             	test   $0x3,%cl
  8007e8:	75 23                	jne    80080d <memset+0x40>
		c &= 0xFF;
  8007ea:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8007ee:	89 d3                	mov    %edx,%ebx
  8007f0:	c1 e3 08             	shl    $0x8,%ebx
  8007f3:	89 d6                	mov    %edx,%esi
  8007f5:	c1 e6 18             	shl    $0x18,%esi
  8007f8:	89 d0                	mov    %edx,%eax
  8007fa:	c1 e0 10             	shl    $0x10,%eax
  8007fd:	09 f0                	or     %esi,%eax
  8007ff:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800801:	89 d8                	mov    %ebx,%eax
  800803:	09 d0                	or     %edx,%eax
  800805:	c1 e9 02             	shr    $0x2,%ecx
  800808:	fc                   	cld    
  800809:	f3 ab                	rep stos %eax,%es:(%edi)
  80080b:	eb 06                	jmp    800813 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80080d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800810:	fc                   	cld    
  800811:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800813:	89 f8                	mov    %edi,%eax
  800815:	5b                   	pop    %ebx
  800816:	5e                   	pop    %esi
  800817:	5f                   	pop    %edi
  800818:	5d                   	pop    %ebp
  800819:	c3                   	ret    

0080081a <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80081a:	55                   	push   %ebp
  80081b:	89 e5                	mov    %esp,%ebp
  80081d:	57                   	push   %edi
  80081e:	56                   	push   %esi
  80081f:	8b 45 08             	mov    0x8(%ebp),%eax
  800822:	8b 75 0c             	mov    0xc(%ebp),%esi
  800825:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800828:	39 c6                	cmp    %eax,%esi
  80082a:	73 33                	jae    80085f <memmove+0x45>
  80082c:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80082f:	39 d0                	cmp    %edx,%eax
  800831:	73 2c                	jae    80085f <memmove+0x45>
		s += n;
		d += n;
  800833:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800836:	89 d6                	mov    %edx,%esi
  800838:	09 fe                	or     %edi,%esi
  80083a:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800840:	75 13                	jne    800855 <memmove+0x3b>
  800842:	f6 c1 03             	test   $0x3,%cl
  800845:	75 0e                	jne    800855 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800847:	83 ef 04             	sub    $0x4,%edi
  80084a:	8d 72 fc             	lea    -0x4(%edx),%esi
  80084d:	c1 e9 02             	shr    $0x2,%ecx
  800850:	fd                   	std    
  800851:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800853:	eb 07                	jmp    80085c <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800855:	4f                   	dec    %edi
  800856:	8d 72 ff             	lea    -0x1(%edx),%esi
  800859:	fd                   	std    
  80085a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80085c:	fc                   	cld    
  80085d:	eb 1d                	jmp    80087c <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80085f:	89 f2                	mov    %esi,%edx
  800861:	09 c2                	or     %eax,%edx
  800863:	f6 c2 03             	test   $0x3,%dl
  800866:	75 0f                	jne    800877 <memmove+0x5d>
  800868:	f6 c1 03             	test   $0x3,%cl
  80086b:	75 0a                	jne    800877 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80086d:	c1 e9 02             	shr    $0x2,%ecx
  800870:	89 c7                	mov    %eax,%edi
  800872:	fc                   	cld    
  800873:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800875:	eb 05                	jmp    80087c <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800877:	89 c7                	mov    %eax,%edi
  800879:	fc                   	cld    
  80087a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80087c:	5e                   	pop    %esi
  80087d:	5f                   	pop    %edi
  80087e:	5d                   	pop    %ebp
  80087f:	c3                   	ret    

00800880 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800880:	55                   	push   %ebp
  800881:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800883:	ff 75 10             	pushl  0x10(%ebp)
  800886:	ff 75 0c             	pushl  0xc(%ebp)
  800889:	ff 75 08             	pushl  0x8(%ebp)
  80088c:	e8 89 ff ff ff       	call   80081a <memmove>
}
  800891:	c9                   	leave  
  800892:	c3                   	ret    

00800893 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800893:	55                   	push   %ebp
  800894:	89 e5                	mov    %esp,%ebp
  800896:	56                   	push   %esi
  800897:	53                   	push   %ebx
  800898:	8b 45 08             	mov    0x8(%ebp),%eax
  80089b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80089e:	89 c6                	mov    %eax,%esi
  8008a0:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008a3:	eb 14                	jmp    8008b9 <memcmp+0x26>
		if (*s1 != *s2)
  8008a5:	8a 08                	mov    (%eax),%cl
  8008a7:	8a 1a                	mov    (%edx),%bl
  8008a9:	38 d9                	cmp    %bl,%cl
  8008ab:	74 0a                	je     8008b7 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008ad:	0f b6 c1             	movzbl %cl,%eax
  8008b0:	0f b6 db             	movzbl %bl,%ebx
  8008b3:	29 d8                	sub    %ebx,%eax
  8008b5:	eb 0b                	jmp    8008c2 <memcmp+0x2f>
		s1++, s2++;
  8008b7:	40                   	inc    %eax
  8008b8:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008b9:	39 f0                	cmp    %esi,%eax
  8008bb:	75 e8                	jne    8008a5 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008bd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008c2:	5b                   	pop    %ebx
  8008c3:	5e                   	pop    %esi
  8008c4:	5d                   	pop    %ebp
  8008c5:	c3                   	ret    

008008c6 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008c6:	55                   	push   %ebp
  8008c7:	89 e5                	mov    %esp,%ebp
  8008c9:	53                   	push   %ebx
  8008ca:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008cd:	89 c1                	mov    %eax,%ecx
  8008cf:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008d2:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008d6:	eb 08                	jmp    8008e0 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008d8:	0f b6 10             	movzbl (%eax),%edx
  8008db:	39 da                	cmp    %ebx,%edx
  8008dd:	74 05                	je     8008e4 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008df:	40                   	inc    %eax
  8008e0:	39 c8                	cmp    %ecx,%eax
  8008e2:	72 f4                	jb     8008d8 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8008e4:	5b                   	pop    %ebx
  8008e5:	5d                   	pop    %ebp
  8008e6:	c3                   	ret    

008008e7 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8008e7:	55                   	push   %ebp
  8008e8:	89 e5                	mov    %esp,%ebp
  8008ea:	57                   	push   %edi
  8008eb:	56                   	push   %esi
  8008ec:	53                   	push   %ebx
  8008ed:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008f0:	eb 01                	jmp    8008f3 <strtol+0xc>
		s++;
  8008f2:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008f3:	8a 01                	mov    (%ecx),%al
  8008f5:	3c 20                	cmp    $0x20,%al
  8008f7:	74 f9                	je     8008f2 <strtol+0xb>
  8008f9:	3c 09                	cmp    $0x9,%al
  8008fb:	74 f5                	je     8008f2 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8008fd:	3c 2b                	cmp    $0x2b,%al
  8008ff:	75 08                	jne    800909 <strtol+0x22>
		s++;
  800901:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800902:	bf 00 00 00 00       	mov    $0x0,%edi
  800907:	eb 11                	jmp    80091a <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800909:	3c 2d                	cmp    $0x2d,%al
  80090b:	75 08                	jne    800915 <strtol+0x2e>
		s++, neg = 1;
  80090d:	41                   	inc    %ecx
  80090e:	bf 01 00 00 00       	mov    $0x1,%edi
  800913:	eb 05                	jmp    80091a <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800915:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80091a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80091e:	0f 84 87 00 00 00    	je     8009ab <strtol+0xc4>
  800924:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800928:	75 27                	jne    800951 <strtol+0x6a>
  80092a:	80 39 30             	cmpb   $0x30,(%ecx)
  80092d:	75 22                	jne    800951 <strtol+0x6a>
  80092f:	e9 88 00 00 00       	jmp    8009bc <strtol+0xd5>
		s += 2, base = 16;
  800934:	83 c1 02             	add    $0x2,%ecx
  800937:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80093e:	eb 11                	jmp    800951 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800940:	41                   	inc    %ecx
  800941:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800948:	eb 07                	jmp    800951 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  80094a:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800951:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800956:	8a 11                	mov    (%ecx),%dl
  800958:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80095b:	80 fb 09             	cmp    $0x9,%bl
  80095e:	77 08                	ja     800968 <strtol+0x81>
			dig = *s - '0';
  800960:	0f be d2             	movsbl %dl,%edx
  800963:	83 ea 30             	sub    $0x30,%edx
  800966:	eb 22                	jmp    80098a <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800968:	8d 72 9f             	lea    -0x61(%edx),%esi
  80096b:	89 f3                	mov    %esi,%ebx
  80096d:	80 fb 19             	cmp    $0x19,%bl
  800970:	77 08                	ja     80097a <strtol+0x93>
			dig = *s - 'a' + 10;
  800972:	0f be d2             	movsbl %dl,%edx
  800975:	83 ea 57             	sub    $0x57,%edx
  800978:	eb 10                	jmp    80098a <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  80097a:	8d 72 bf             	lea    -0x41(%edx),%esi
  80097d:	89 f3                	mov    %esi,%ebx
  80097f:	80 fb 19             	cmp    $0x19,%bl
  800982:	77 14                	ja     800998 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800984:	0f be d2             	movsbl %dl,%edx
  800987:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  80098a:	3b 55 10             	cmp    0x10(%ebp),%edx
  80098d:	7d 09                	jge    800998 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80098f:	41                   	inc    %ecx
  800990:	0f af 45 10          	imul   0x10(%ebp),%eax
  800994:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800996:	eb be                	jmp    800956 <strtol+0x6f>

	if (endptr)
  800998:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80099c:	74 05                	je     8009a3 <strtol+0xbc>
		*endptr = (char *) s;
  80099e:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009a1:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009a3:	85 ff                	test   %edi,%edi
  8009a5:	74 21                	je     8009c8 <strtol+0xe1>
  8009a7:	f7 d8                	neg    %eax
  8009a9:	eb 1d                	jmp    8009c8 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009ab:	80 39 30             	cmpb   $0x30,(%ecx)
  8009ae:	75 9a                	jne    80094a <strtol+0x63>
  8009b0:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009b4:	0f 84 7a ff ff ff    	je     800934 <strtol+0x4d>
  8009ba:	eb 84                	jmp    800940 <strtol+0x59>
  8009bc:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009c0:	0f 84 6e ff ff ff    	je     800934 <strtol+0x4d>
  8009c6:	eb 89                	jmp    800951 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009c8:	5b                   	pop    %ebx
  8009c9:	5e                   	pop    %esi
  8009ca:	5f                   	pop    %edi
  8009cb:	5d                   	pop    %ebp
  8009cc:	c3                   	ret    

008009cd <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009cd:	55                   	push   %ebp
  8009ce:	89 e5                	mov    %esp,%ebp
  8009d0:	57                   	push   %edi
  8009d1:	56                   	push   %esi
  8009d2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009d3:	b8 00 00 00 00       	mov    $0x0,%eax
  8009d8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009db:	8b 55 08             	mov    0x8(%ebp),%edx
  8009de:	89 c3                	mov    %eax,%ebx
  8009e0:	89 c7                	mov    %eax,%edi
  8009e2:	89 c6                	mov    %eax,%esi
  8009e4:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8009e6:	5b                   	pop    %ebx
  8009e7:	5e                   	pop    %esi
  8009e8:	5f                   	pop    %edi
  8009e9:	5d                   	pop    %ebp
  8009ea:	c3                   	ret    

008009eb <sys_cgetc>:

int
sys_cgetc(void)
{
  8009eb:	55                   	push   %ebp
  8009ec:	89 e5                	mov    %esp,%ebp
  8009ee:	57                   	push   %edi
  8009ef:	56                   	push   %esi
  8009f0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009f1:	ba 00 00 00 00       	mov    $0x0,%edx
  8009f6:	b8 01 00 00 00       	mov    $0x1,%eax
  8009fb:	89 d1                	mov    %edx,%ecx
  8009fd:	89 d3                	mov    %edx,%ebx
  8009ff:	89 d7                	mov    %edx,%edi
  800a01:	89 d6                	mov    %edx,%esi
  800a03:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a05:	5b                   	pop    %ebx
  800a06:	5e                   	pop    %esi
  800a07:	5f                   	pop    %edi
  800a08:	5d                   	pop    %ebp
  800a09:	c3                   	ret    

00800a0a <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a0a:	55                   	push   %ebp
  800a0b:	89 e5                	mov    %esp,%ebp
  800a0d:	57                   	push   %edi
  800a0e:	56                   	push   %esi
  800a0f:	53                   	push   %ebx
  800a10:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a13:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a18:	b8 03 00 00 00       	mov    $0x3,%eax
  800a1d:	8b 55 08             	mov    0x8(%ebp),%edx
  800a20:	89 cb                	mov    %ecx,%ebx
  800a22:	89 cf                	mov    %ecx,%edi
  800a24:	89 ce                	mov    %ecx,%esi
  800a26:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a28:	85 c0                	test   %eax,%eax
  800a2a:	7e 17                	jle    800a43 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a2c:	83 ec 0c             	sub    $0xc,%esp
  800a2f:	50                   	push   %eax
  800a30:	6a 03                	push   $0x3
  800a32:	68 7f 12 80 00       	push   $0x80127f
  800a37:	6a 23                	push   $0x23
  800a39:	68 9c 12 80 00       	push   $0x80129c
  800a3e:	e8 56 02 00 00       	call   800c99 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a43:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a46:	5b                   	pop    %ebx
  800a47:	5e                   	pop    %esi
  800a48:	5f                   	pop    %edi
  800a49:	5d                   	pop    %ebp
  800a4a:	c3                   	ret    

00800a4b <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a4b:	55                   	push   %ebp
  800a4c:	89 e5                	mov    %esp,%ebp
  800a4e:	57                   	push   %edi
  800a4f:	56                   	push   %esi
  800a50:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a51:	ba 00 00 00 00       	mov    $0x0,%edx
  800a56:	b8 02 00 00 00       	mov    $0x2,%eax
  800a5b:	89 d1                	mov    %edx,%ecx
  800a5d:	89 d3                	mov    %edx,%ebx
  800a5f:	89 d7                	mov    %edx,%edi
  800a61:	89 d6                	mov    %edx,%esi
  800a63:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a65:	5b                   	pop    %ebx
  800a66:	5e                   	pop    %esi
  800a67:	5f                   	pop    %edi
  800a68:	5d                   	pop    %ebp
  800a69:	c3                   	ret    

00800a6a <sys_yield>:

void
sys_yield(void)
{
  800a6a:	55                   	push   %ebp
  800a6b:	89 e5                	mov    %esp,%ebp
  800a6d:	57                   	push   %edi
  800a6e:	56                   	push   %esi
  800a6f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a70:	ba 00 00 00 00       	mov    $0x0,%edx
  800a75:	b8 0b 00 00 00       	mov    $0xb,%eax
  800a7a:	89 d1                	mov    %edx,%ecx
  800a7c:	89 d3                	mov    %edx,%ebx
  800a7e:	89 d7                	mov    %edx,%edi
  800a80:	89 d6                	mov    %edx,%esi
  800a82:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800a84:	5b                   	pop    %ebx
  800a85:	5e                   	pop    %esi
  800a86:	5f                   	pop    %edi
  800a87:	5d                   	pop    %ebp
  800a88:	c3                   	ret    

00800a89 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800a89:	55                   	push   %ebp
  800a8a:	89 e5                	mov    %esp,%ebp
  800a8c:	57                   	push   %edi
  800a8d:	56                   	push   %esi
  800a8e:	53                   	push   %ebx
  800a8f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a92:	be 00 00 00 00       	mov    $0x0,%esi
  800a97:	b8 04 00 00 00       	mov    $0x4,%eax
  800a9c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a9f:	8b 55 08             	mov    0x8(%ebp),%edx
  800aa2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800aa5:	89 f7                	mov    %esi,%edi
  800aa7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800aa9:	85 c0                	test   %eax,%eax
  800aab:	7e 17                	jle    800ac4 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800aad:	83 ec 0c             	sub    $0xc,%esp
  800ab0:	50                   	push   %eax
  800ab1:	6a 04                	push   $0x4
  800ab3:	68 7f 12 80 00       	push   $0x80127f
  800ab8:	6a 23                	push   $0x23
  800aba:	68 9c 12 80 00       	push   $0x80129c
  800abf:	e8 d5 01 00 00       	call   800c99 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800ac4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ac7:	5b                   	pop    %ebx
  800ac8:	5e                   	pop    %esi
  800ac9:	5f                   	pop    %edi
  800aca:	5d                   	pop    %ebp
  800acb:	c3                   	ret    

00800acc <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800acc:	55                   	push   %ebp
  800acd:	89 e5                	mov    %esp,%ebp
  800acf:	57                   	push   %edi
  800ad0:	56                   	push   %esi
  800ad1:	53                   	push   %ebx
  800ad2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ad5:	b8 05 00 00 00       	mov    $0x5,%eax
  800ada:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800add:	8b 55 08             	mov    0x8(%ebp),%edx
  800ae0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ae3:	8b 7d 14             	mov    0x14(%ebp),%edi
  800ae6:	8b 75 18             	mov    0x18(%ebp),%esi
  800ae9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800aeb:	85 c0                	test   %eax,%eax
  800aed:	7e 17                	jle    800b06 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800aef:	83 ec 0c             	sub    $0xc,%esp
  800af2:	50                   	push   %eax
  800af3:	6a 05                	push   $0x5
  800af5:	68 7f 12 80 00       	push   $0x80127f
  800afa:	6a 23                	push   $0x23
  800afc:	68 9c 12 80 00       	push   $0x80129c
  800b01:	e8 93 01 00 00       	call   800c99 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b06:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b09:	5b                   	pop    %ebx
  800b0a:	5e                   	pop    %esi
  800b0b:	5f                   	pop    %edi
  800b0c:	5d                   	pop    %ebp
  800b0d:	c3                   	ret    

00800b0e <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b0e:	55                   	push   %ebp
  800b0f:	89 e5                	mov    %esp,%ebp
  800b11:	57                   	push   %edi
  800b12:	56                   	push   %esi
  800b13:	53                   	push   %ebx
  800b14:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b17:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b1c:	b8 06 00 00 00       	mov    $0x6,%eax
  800b21:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b24:	8b 55 08             	mov    0x8(%ebp),%edx
  800b27:	89 df                	mov    %ebx,%edi
  800b29:	89 de                	mov    %ebx,%esi
  800b2b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b2d:	85 c0                	test   %eax,%eax
  800b2f:	7e 17                	jle    800b48 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b31:	83 ec 0c             	sub    $0xc,%esp
  800b34:	50                   	push   %eax
  800b35:	6a 06                	push   $0x6
  800b37:	68 7f 12 80 00       	push   $0x80127f
  800b3c:	6a 23                	push   $0x23
  800b3e:	68 9c 12 80 00       	push   $0x80129c
  800b43:	e8 51 01 00 00       	call   800c99 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800b48:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b4b:	5b                   	pop    %ebx
  800b4c:	5e                   	pop    %esi
  800b4d:	5f                   	pop    %edi
  800b4e:	5d                   	pop    %ebp
  800b4f:	c3                   	ret    

00800b50 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800b50:	55                   	push   %ebp
  800b51:	89 e5                	mov    %esp,%ebp
  800b53:	57                   	push   %edi
  800b54:	56                   	push   %esi
  800b55:	53                   	push   %ebx
  800b56:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b59:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b5e:	b8 08 00 00 00       	mov    $0x8,%eax
  800b63:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b66:	8b 55 08             	mov    0x8(%ebp),%edx
  800b69:	89 df                	mov    %ebx,%edi
  800b6b:	89 de                	mov    %ebx,%esi
  800b6d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b6f:	85 c0                	test   %eax,%eax
  800b71:	7e 17                	jle    800b8a <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b73:	83 ec 0c             	sub    $0xc,%esp
  800b76:	50                   	push   %eax
  800b77:	6a 08                	push   $0x8
  800b79:	68 7f 12 80 00       	push   $0x80127f
  800b7e:	6a 23                	push   $0x23
  800b80:	68 9c 12 80 00       	push   $0x80129c
  800b85:	e8 0f 01 00 00       	call   800c99 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800b8a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b8d:	5b                   	pop    %ebx
  800b8e:	5e                   	pop    %esi
  800b8f:	5f                   	pop    %edi
  800b90:	5d                   	pop    %ebp
  800b91:	c3                   	ret    

00800b92 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800b92:	55                   	push   %ebp
  800b93:	89 e5                	mov    %esp,%ebp
  800b95:	57                   	push   %edi
  800b96:	56                   	push   %esi
  800b97:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b98:	ba 00 00 00 00       	mov    $0x0,%edx
  800b9d:	b8 0c 00 00 00       	mov    $0xc,%eax
  800ba2:	89 d1                	mov    %edx,%ecx
  800ba4:	89 d3                	mov    %edx,%ebx
  800ba6:	89 d7                	mov    %edx,%edi
  800ba8:	89 d6                	mov    %edx,%esi
  800baa:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800bac:	5b                   	pop    %ebx
  800bad:	5e                   	pop    %esi
  800bae:	5f                   	pop    %edi
  800baf:	5d                   	pop    %ebp
  800bb0:	c3                   	ret    

00800bb1 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800bb1:	55                   	push   %ebp
  800bb2:	89 e5                	mov    %esp,%ebp
  800bb4:	57                   	push   %edi
  800bb5:	56                   	push   %esi
  800bb6:	53                   	push   %ebx
  800bb7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bba:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bbf:	b8 09 00 00 00       	mov    $0x9,%eax
  800bc4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bc7:	8b 55 08             	mov    0x8(%ebp),%edx
  800bca:	89 df                	mov    %ebx,%edi
  800bcc:	89 de                	mov    %ebx,%esi
  800bce:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bd0:	85 c0                	test   %eax,%eax
  800bd2:	7e 17                	jle    800beb <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bd4:	83 ec 0c             	sub    $0xc,%esp
  800bd7:	50                   	push   %eax
  800bd8:	6a 09                	push   $0x9
  800bda:	68 7f 12 80 00       	push   $0x80127f
  800bdf:	6a 23                	push   $0x23
  800be1:	68 9c 12 80 00       	push   $0x80129c
  800be6:	e8 ae 00 00 00       	call   800c99 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800beb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bee:	5b                   	pop    %ebx
  800bef:	5e                   	pop    %esi
  800bf0:	5f                   	pop    %edi
  800bf1:	5d                   	pop    %ebp
  800bf2:	c3                   	ret    

00800bf3 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800bf3:	55                   	push   %ebp
  800bf4:	89 e5                	mov    %esp,%ebp
  800bf6:	57                   	push   %edi
  800bf7:	56                   	push   %esi
  800bf8:	53                   	push   %ebx
  800bf9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bfc:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c01:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c06:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c09:	8b 55 08             	mov    0x8(%ebp),%edx
  800c0c:	89 df                	mov    %ebx,%edi
  800c0e:	89 de                	mov    %ebx,%esi
  800c10:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c12:	85 c0                	test   %eax,%eax
  800c14:	7e 17                	jle    800c2d <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c16:	83 ec 0c             	sub    $0xc,%esp
  800c19:	50                   	push   %eax
  800c1a:	6a 0a                	push   $0xa
  800c1c:	68 7f 12 80 00       	push   $0x80127f
  800c21:	6a 23                	push   $0x23
  800c23:	68 9c 12 80 00       	push   $0x80129c
  800c28:	e8 6c 00 00 00       	call   800c99 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800c2d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c30:	5b                   	pop    %ebx
  800c31:	5e                   	pop    %esi
  800c32:	5f                   	pop    %edi
  800c33:	5d                   	pop    %ebp
  800c34:	c3                   	ret    

00800c35 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800c35:	55                   	push   %ebp
  800c36:	89 e5                	mov    %esp,%ebp
  800c38:	57                   	push   %edi
  800c39:	56                   	push   %esi
  800c3a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c3b:	be 00 00 00 00       	mov    $0x0,%esi
  800c40:	b8 0d 00 00 00       	mov    $0xd,%eax
  800c45:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c48:	8b 55 08             	mov    0x8(%ebp),%edx
  800c4b:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c4e:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c51:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800c53:	5b                   	pop    %ebx
  800c54:	5e                   	pop    %esi
  800c55:	5f                   	pop    %edi
  800c56:	5d                   	pop    %ebp
  800c57:	c3                   	ret    

00800c58 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800c58:	55                   	push   %ebp
  800c59:	89 e5                	mov    %esp,%ebp
  800c5b:	57                   	push   %edi
  800c5c:	56                   	push   %esi
  800c5d:	53                   	push   %ebx
  800c5e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c61:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c66:	b8 0e 00 00 00       	mov    $0xe,%eax
  800c6b:	8b 55 08             	mov    0x8(%ebp),%edx
  800c6e:	89 cb                	mov    %ecx,%ebx
  800c70:	89 cf                	mov    %ecx,%edi
  800c72:	89 ce                	mov    %ecx,%esi
  800c74:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c76:	85 c0                	test   %eax,%eax
  800c78:	7e 17                	jle    800c91 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c7a:	83 ec 0c             	sub    $0xc,%esp
  800c7d:	50                   	push   %eax
  800c7e:	6a 0e                	push   $0xe
  800c80:	68 7f 12 80 00       	push   $0x80127f
  800c85:	6a 23                	push   $0x23
  800c87:	68 9c 12 80 00       	push   $0x80129c
  800c8c:	e8 08 00 00 00       	call   800c99 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800c91:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c94:	5b                   	pop    %ebx
  800c95:	5e                   	pop    %esi
  800c96:	5f                   	pop    %edi
  800c97:	5d                   	pop    %ebp
  800c98:	c3                   	ret    

00800c99 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800c99:	55                   	push   %ebp
  800c9a:	89 e5                	mov    %esp,%ebp
  800c9c:	56                   	push   %esi
  800c9d:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800c9e:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800ca1:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800ca7:	e8 9f fd ff ff       	call   800a4b <sys_getenvid>
  800cac:	83 ec 0c             	sub    $0xc,%esp
  800caf:	ff 75 0c             	pushl  0xc(%ebp)
  800cb2:	ff 75 08             	pushl  0x8(%ebp)
  800cb5:	56                   	push   %esi
  800cb6:	50                   	push   %eax
  800cb7:	68 ac 12 80 00       	push   $0x8012ac
  800cbc:	e8 80 f4 ff ff       	call   800141 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800cc1:	83 c4 18             	add    $0x18,%esp
  800cc4:	53                   	push   %ebx
  800cc5:	ff 75 10             	pushl  0x10(%ebp)
  800cc8:	e8 23 f4 ff ff       	call   8000f0 <vcprintf>
	cprintf("\n");
  800ccd:	c7 04 24 7c 0f 80 00 	movl   $0x800f7c,(%esp)
  800cd4:	e8 68 f4 ff ff       	call   800141 <cprintf>
  800cd9:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800cdc:	cc                   	int3   
  800cdd:	eb fd                	jmp    800cdc <_panic+0x43>
  800cdf:	90                   	nop

00800ce0 <__udivdi3>:
  800ce0:	55                   	push   %ebp
  800ce1:	57                   	push   %edi
  800ce2:	56                   	push   %esi
  800ce3:	53                   	push   %ebx
  800ce4:	83 ec 1c             	sub    $0x1c,%esp
  800ce7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ceb:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cef:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cf3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cf7:	89 ca                	mov    %ecx,%edx
  800cf9:	89 f8                	mov    %edi,%eax
  800cfb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cff:	85 f6                	test   %esi,%esi
  800d01:	75 2d                	jne    800d30 <__udivdi3+0x50>
  800d03:	39 cf                	cmp    %ecx,%edi
  800d05:	77 65                	ja     800d6c <__udivdi3+0x8c>
  800d07:	89 fd                	mov    %edi,%ebp
  800d09:	85 ff                	test   %edi,%edi
  800d0b:	75 0b                	jne    800d18 <__udivdi3+0x38>
  800d0d:	b8 01 00 00 00       	mov    $0x1,%eax
  800d12:	31 d2                	xor    %edx,%edx
  800d14:	f7 f7                	div    %edi
  800d16:	89 c5                	mov    %eax,%ebp
  800d18:	31 d2                	xor    %edx,%edx
  800d1a:	89 c8                	mov    %ecx,%eax
  800d1c:	f7 f5                	div    %ebp
  800d1e:	89 c1                	mov    %eax,%ecx
  800d20:	89 d8                	mov    %ebx,%eax
  800d22:	f7 f5                	div    %ebp
  800d24:	89 cf                	mov    %ecx,%edi
  800d26:	89 fa                	mov    %edi,%edx
  800d28:	83 c4 1c             	add    $0x1c,%esp
  800d2b:	5b                   	pop    %ebx
  800d2c:	5e                   	pop    %esi
  800d2d:	5f                   	pop    %edi
  800d2e:	5d                   	pop    %ebp
  800d2f:	c3                   	ret    
  800d30:	39 ce                	cmp    %ecx,%esi
  800d32:	77 28                	ja     800d5c <__udivdi3+0x7c>
  800d34:	0f bd fe             	bsr    %esi,%edi
  800d37:	83 f7 1f             	xor    $0x1f,%edi
  800d3a:	75 40                	jne    800d7c <__udivdi3+0x9c>
  800d3c:	39 ce                	cmp    %ecx,%esi
  800d3e:	72 0a                	jb     800d4a <__udivdi3+0x6a>
  800d40:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d44:	0f 87 9e 00 00 00    	ja     800de8 <__udivdi3+0x108>
  800d4a:	b8 01 00 00 00       	mov    $0x1,%eax
  800d4f:	89 fa                	mov    %edi,%edx
  800d51:	83 c4 1c             	add    $0x1c,%esp
  800d54:	5b                   	pop    %ebx
  800d55:	5e                   	pop    %esi
  800d56:	5f                   	pop    %edi
  800d57:	5d                   	pop    %ebp
  800d58:	c3                   	ret    
  800d59:	8d 76 00             	lea    0x0(%esi),%esi
  800d5c:	31 ff                	xor    %edi,%edi
  800d5e:	31 c0                	xor    %eax,%eax
  800d60:	89 fa                	mov    %edi,%edx
  800d62:	83 c4 1c             	add    $0x1c,%esp
  800d65:	5b                   	pop    %ebx
  800d66:	5e                   	pop    %esi
  800d67:	5f                   	pop    %edi
  800d68:	5d                   	pop    %ebp
  800d69:	c3                   	ret    
  800d6a:	66 90                	xchg   %ax,%ax
  800d6c:	89 d8                	mov    %ebx,%eax
  800d6e:	f7 f7                	div    %edi
  800d70:	31 ff                	xor    %edi,%edi
  800d72:	89 fa                	mov    %edi,%edx
  800d74:	83 c4 1c             	add    $0x1c,%esp
  800d77:	5b                   	pop    %ebx
  800d78:	5e                   	pop    %esi
  800d79:	5f                   	pop    %edi
  800d7a:	5d                   	pop    %ebp
  800d7b:	c3                   	ret    
  800d7c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d81:	89 eb                	mov    %ebp,%ebx
  800d83:	29 fb                	sub    %edi,%ebx
  800d85:	89 f9                	mov    %edi,%ecx
  800d87:	d3 e6                	shl    %cl,%esi
  800d89:	89 c5                	mov    %eax,%ebp
  800d8b:	88 d9                	mov    %bl,%cl
  800d8d:	d3 ed                	shr    %cl,%ebp
  800d8f:	89 e9                	mov    %ebp,%ecx
  800d91:	09 f1                	or     %esi,%ecx
  800d93:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d97:	89 f9                	mov    %edi,%ecx
  800d99:	d3 e0                	shl    %cl,%eax
  800d9b:	89 c5                	mov    %eax,%ebp
  800d9d:	89 d6                	mov    %edx,%esi
  800d9f:	88 d9                	mov    %bl,%cl
  800da1:	d3 ee                	shr    %cl,%esi
  800da3:	89 f9                	mov    %edi,%ecx
  800da5:	d3 e2                	shl    %cl,%edx
  800da7:	8b 44 24 08          	mov    0x8(%esp),%eax
  800dab:	88 d9                	mov    %bl,%cl
  800dad:	d3 e8                	shr    %cl,%eax
  800daf:	09 c2                	or     %eax,%edx
  800db1:	89 d0                	mov    %edx,%eax
  800db3:	89 f2                	mov    %esi,%edx
  800db5:	f7 74 24 0c          	divl   0xc(%esp)
  800db9:	89 d6                	mov    %edx,%esi
  800dbb:	89 c3                	mov    %eax,%ebx
  800dbd:	f7 e5                	mul    %ebp
  800dbf:	39 d6                	cmp    %edx,%esi
  800dc1:	72 19                	jb     800ddc <__udivdi3+0xfc>
  800dc3:	74 0b                	je     800dd0 <__udivdi3+0xf0>
  800dc5:	89 d8                	mov    %ebx,%eax
  800dc7:	31 ff                	xor    %edi,%edi
  800dc9:	e9 58 ff ff ff       	jmp    800d26 <__udivdi3+0x46>
  800dce:	66 90                	xchg   %ax,%ax
  800dd0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dd4:	89 f9                	mov    %edi,%ecx
  800dd6:	d3 e2                	shl    %cl,%edx
  800dd8:	39 c2                	cmp    %eax,%edx
  800dda:	73 e9                	jae    800dc5 <__udivdi3+0xe5>
  800ddc:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ddf:	31 ff                	xor    %edi,%edi
  800de1:	e9 40 ff ff ff       	jmp    800d26 <__udivdi3+0x46>
  800de6:	66 90                	xchg   %ax,%ax
  800de8:	31 c0                	xor    %eax,%eax
  800dea:	e9 37 ff ff ff       	jmp    800d26 <__udivdi3+0x46>
  800def:	90                   	nop

00800df0 <__umoddi3>:
  800df0:	55                   	push   %ebp
  800df1:	57                   	push   %edi
  800df2:	56                   	push   %esi
  800df3:	53                   	push   %ebx
  800df4:	83 ec 1c             	sub    $0x1c,%esp
  800df7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800dfb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dff:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e03:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e07:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e0b:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e0f:	89 f3                	mov    %esi,%ebx
  800e11:	89 fa                	mov    %edi,%edx
  800e13:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e17:	89 34 24             	mov    %esi,(%esp)
  800e1a:	85 c0                	test   %eax,%eax
  800e1c:	75 1a                	jne    800e38 <__umoddi3+0x48>
  800e1e:	39 f7                	cmp    %esi,%edi
  800e20:	0f 86 a2 00 00 00    	jbe    800ec8 <__umoddi3+0xd8>
  800e26:	89 c8                	mov    %ecx,%eax
  800e28:	89 f2                	mov    %esi,%edx
  800e2a:	f7 f7                	div    %edi
  800e2c:	89 d0                	mov    %edx,%eax
  800e2e:	31 d2                	xor    %edx,%edx
  800e30:	83 c4 1c             	add    $0x1c,%esp
  800e33:	5b                   	pop    %ebx
  800e34:	5e                   	pop    %esi
  800e35:	5f                   	pop    %edi
  800e36:	5d                   	pop    %ebp
  800e37:	c3                   	ret    
  800e38:	39 f0                	cmp    %esi,%eax
  800e3a:	0f 87 ac 00 00 00    	ja     800eec <__umoddi3+0xfc>
  800e40:	0f bd e8             	bsr    %eax,%ebp
  800e43:	83 f5 1f             	xor    $0x1f,%ebp
  800e46:	0f 84 ac 00 00 00    	je     800ef8 <__umoddi3+0x108>
  800e4c:	bf 20 00 00 00       	mov    $0x20,%edi
  800e51:	29 ef                	sub    %ebp,%edi
  800e53:	89 fe                	mov    %edi,%esi
  800e55:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e59:	89 e9                	mov    %ebp,%ecx
  800e5b:	d3 e0                	shl    %cl,%eax
  800e5d:	89 d7                	mov    %edx,%edi
  800e5f:	89 f1                	mov    %esi,%ecx
  800e61:	d3 ef                	shr    %cl,%edi
  800e63:	09 c7                	or     %eax,%edi
  800e65:	89 e9                	mov    %ebp,%ecx
  800e67:	d3 e2                	shl    %cl,%edx
  800e69:	89 14 24             	mov    %edx,(%esp)
  800e6c:	89 d8                	mov    %ebx,%eax
  800e6e:	d3 e0                	shl    %cl,%eax
  800e70:	89 c2                	mov    %eax,%edx
  800e72:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e76:	d3 e0                	shl    %cl,%eax
  800e78:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e7c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e80:	89 f1                	mov    %esi,%ecx
  800e82:	d3 e8                	shr    %cl,%eax
  800e84:	09 d0                	or     %edx,%eax
  800e86:	d3 eb                	shr    %cl,%ebx
  800e88:	89 da                	mov    %ebx,%edx
  800e8a:	f7 f7                	div    %edi
  800e8c:	89 d3                	mov    %edx,%ebx
  800e8e:	f7 24 24             	mull   (%esp)
  800e91:	89 c6                	mov    %eax,%esi
  800e93:	89 d1                	mov    %edx,%ecx
  800e95:	39 d3                	cmp    %edx,%ebx
  800e97:	0f 82 87 00 00 00    	jb     800f24 <__umoddi3+0x134>
  800e9d:	0f 84 91 00 00 00    	je     800f34 <__umoddi3+0x144>
  800ea3:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ea7:	29 f2                	sub    %esi,%edx
  800ea9:	19 cb                	sbb    %ecx,%ebx
  800eab:	89 d8                	mov    %ebx,%eax
  800ead:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800eb1:	d3 e0                	shl    %cl,%eax
  800eb3:	89 e9                	mov    %ebp,%ecx
  800eb5:	d3 ea                	shr    %cl,%edx
  800eb7:	09 d0                	or     %edx,%eax
  800eb9:	89 e9                	mov    %ebp,%ecx
  800ebb:	d3 eb                	shr    %cl,%ebx
  800ebd:	89 da                	mov    %ebx,%edx
  800ebf:	83 c4 1c             	add    $0x1c,%esp
  800ec2:	5b                   	pop    %ebx
  800ec3:	5e                   	pop    %esi
  800ec4:	5f                   	pop    %edi
  800ec5:	5d                   	pop    %ebp
  800ec6:	c3                   	ret    
  800ec7:	90                   	nop
  800ec8:	89 fd                	mov    %edi,%ebp
  800eca:	85 ff                	test   %edi,%edi
  800ecc:	75 0b                	jne    800ed9 <__umoddi3+0xe9>
  800ece:	b8 01 00 00 00       	mov    $0x1,%eax
  800ed3:	31 d2                	xor    %edx,%edx
  800ed5:	f7 f7                	div    %edi
  800ed7:	89 c5                	mov    %eax,%ebp
  800ed9:	89 f0                	mov    %esi,%eax
  800edb:	31 d2                	xor    %edx,%edx
  800edd:	f7 f5                	div    %ebp
  800edf:	89 c8                	mov    %ecx,%eax
  800ee1:	f7 f5                	div    %ebp
  800ee3:	89 d0                	mov    %edx,%eax
  800ee5:	e9 44 ff ff ff       	jmp    800e2e <__umoddi3+0x3e>
  800eea:	66 90                	xchg   %ax,%ax
  800eec:	89 c8                	mov    %ecx,%eax
  800eee:	89 f2                	mov    %esi,%edx
  800ef0:	83 c4 1c             	add    $0x1c,%esp
  800ef3:	5b                   	pop    %ebx
  800ef4:	5e                   	pop    %esi
  800ef5:	5f                   	pop    %edi
  800ef6:	5d                   	pop    %ebp
  800ef7:	c3                   	ret    
  800ef8:	3b 04 24             	cmp    (%esp),%eax
  800efb:	72 06                	jb     800f03 <__umoddi3+0x113>
  800efd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f01:	77 0f                	ja     800f12 <__umoddi3+0x122>
  800f03:	89 f2                	mov    %esi,%edx
  800f05:	29 f9                	sub    %edi,%ecx
  800f07:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f0b:	89 14 24             	mov    %edx,(%esp)
  800f0e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f12:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f16:	8b 14 24             	mov    (%esp),%edx
  800f19:	83 c4 1c             	add    $0x1c,%esp
  800f1c:	5b                   	pop    %ebx
  800f1d:	5e                   	pop    %esi
  800f1e:	5f                   	pop    %edi
  800f1f:	5d                   	pop    %ebp
  800f20:	c3                   	ret    
  800f21:	8d 76 00             	lea    0x0(%esi),%esi
  800f24:	2b 04 24             	sub    (%esp),%eax
  800f27:	19 fa                	sbb    %edi,%edx
  800f29:	89 d1                	mov    %edx,%ecx
  800f2b:	89 c6                	mov    %eax,%esi
  800f2d:	e9 71 ff ff ff       	jmp    800ea3 <__umoddi3+0xb3>
  800f32:	66 90                	xchg   %ax,%ax
  800f34:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f38:	72 ea                	jb     800f24 <__umoddi3+0x134>
  800f3a:	89 d9                	mov    %ebx,%ecx
  800f3c:	e9 62 ff ff ff       	jmp    800ea3 <__umoddi3+0xb3>
