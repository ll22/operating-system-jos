
obj/user/testfdsharing.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 87 01 00 00       	call   8001b8 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

char buf[512], buf2[512];

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 14             	sub    $0x14,%esp
	int fd, r, n, n2;

	if ((fd = open("motd", O_RDONLY)) < 0)
  80003c:	6a 00                	push   $0x0
  80003e:	68 40 22 80 00       	push   $0x802240
  800043:	e8 1a 18 00 00       	call   801862 <open>
  800048:	89 c3                	mov    %eax,%ebx
  80004a:	83 c4 10             	add    $0x10,%esp
  80004d:	85 c0                	test   %eax,%eax
  80004f:	79 12                	jns    800063 <umain+0x30>
		panic("open motd: %e", fd);
  800051:	50                   	push   %eax
  800052:	68 45 22 80 00       	push   $0x802245
  800057:	6a 0c                	push   $0xc
  800059:	68 53 22 80 00       	push   $0x802253
  80005e:	e8 b6 01 00 00       	call   800219 <_panic>
	seek(fd, 0);
  800063:	83 ec 08             	sub    $0x8,%esp
  800066:	6a 00                	push   $0x0
  800068:	50                   	push   %eax
  800069:	e8 df 14 00 00       	call   80154d <seek>
	if ((n = readn(fd, buf, sizeof buf)) <= 0)
  80006e:	83 c4 0c             	add    $0xc,%esp
  800071:	68 00 02 00 00       	push   $0x200
  800076:	68 20 42 80 00       	push   $0x804220
  80007b:	53                   	push   %ebx
  80007c:	e8 01 14 00 00       	call   801482 <readn>
  800081:	89 c6                	mov    %eax,%esi
  800083:	83 c4 10             	add    $0x10,%esp
  800086:	85 c0                	test   %eax,%eax
  800088:	7f 12                	jg     80009c <umain+0x69>
		panic("readn: %e", n);
  80008a:	50                   	push   %eax
  80008b:	68 68 22 80 00       	push   $0x802268
  800090:	6a 0f                	push   $0xf
  800092:	68 53 22 80 00       	push   $0x802253
  800097:	e8 7d 01 00 00       	call   800219 <_panic>

	if ((r = fork()) < 0)
  80009c:	e8 5a 0e 00 00       	call   800efb <fork>
  8000a1:	89 c7                	mov    %eax,%edi
  8000a3:	85 c0                	test   %eax,%eax
  8000a5:	79 12                	jns    8000b9 <umain+0x86>
		panic("fork: %e", r);
  8000a7:	50                   	push   %eax
  8000a8:	68 72 22 80 00       	push   $0x802272
  8000ad:	6a 12                	push   $0x12
  8000af:	68 53 22 80 00       	push   $0x802253
  8000b4:	e8 60 01 00 00       	call   800219 <_panic>
	if (r == 0) {
  8000b9:	85 c0                	test   %eax,%eax
  8000bb:	0f 85 9d 00 00 00    	jne    80015e <umain+0x12b>
		seek(fd, 0);
  8000c1:	83 ec 08             	sub    $0x8,%esp
  8000c4:	6a 00                	push   $0x0
  8000c6:	53                   	push   %ebx
  8000c7:	e8 81 14 00 00       	call   80154d <seek>
		cprintf("going to read in child (might page fault if your sharing is buggy)\n");
  8000cc:	c7 04 24 b0 22 80 00 	movl   $0x8022b0,(%esp)
  8000d3:	e8 19 02 00 00       	call   8002f1 <cprintf>
		if ((n2 = readn(fd, buf2, sizeof buf2)) != n)
  8000d8:	83 c4 0c             	add    $0xc,%esp
  8000db:	68 00 02 00 00       	push   $0x200
  8000e0:	68 20 40 80 00       	push   $0x804020
  8000e5:	53                   	push   %ebx
  8000e6:	e8 97 13 00 00       	call   801482 <readn>
  8000eb:	83 c4 10             	add    $0x10,%esp
  8000ee:	39 c6                	cmp    %eax,%esi
  8000f0:	74 16                	je     800108 <umain+0xd5>
			panic("read in parent got %d, read in child got %d", n, n2);
  8000f2:	83 ec 0c             	sub    $0xc,%esp
  8000f5:	50                   	push   %eax
  8000f6:	56                   	push   %esi
  8000f7:	68 f4 22 80 00       	push   $0x8022f4
  8000fc:	6a 17                	push   $0x17
  8000fe:	68 53 22 80 00       	push   $0x802253
  800103:	e8 11 01 00 00       	call   800219 <_panic>
		if (memcmp(buf, buf2, n) != 0)
  800108:	83 ec 04             	sub    $0x4,%esp
  80010b:	56                   	push   %esi
  80010c:	68 20 40 80 00       	push   $0x804020
  800111:	68 20 42 80 00       	push   $0x804220
  800116:	e8 28 09 00 00       	call   800a43 <memcmp>
  80011b:	83 c4 10             	add    $0x10,%esp
  80011e:	85 c0                	test   %eax,%eax
  800120:	74 14                	je     800136 <umain+0x103>
			panic("read in parent got different bytes from read in child");
  800122:	83 ec 04             	sub    $0x4,%esp
  800125:	68 20 23 80 00       	push   $0x802320
  80012a:	6a 19                	push   $0x19
  80012c:	68 53 22 80 00       	push   $0x802253
  800131:	e8 e3 00 00 00       	call   800219 <_panic>
		cprintf("read in child succeeded\n");
  800136:	83 ec 0c             	sub    $0xc,%esp
  800139:	68 7b 22 80 00       	push   $0x80227b
  80013e:	e8 ae 01 00 00       	call   8002f1 <cprintf>
		seek(fd, 0);
  800143:	83 c4 08             	add    $0x8,%esp
  800146:	6a 00                	push   $0x0
  800148:	53                   	push   %ebx
  800149:	e8 ff 13 00 00       	call   80154d <seek>
		close(fd);
  80014e:	89 1c 24             	mov    %ebx,(%esp)
  800151:	e8 6d 11 00 00       	call   8012c3 <close>
		exit();
  800156:	e8 ac 00 00 00       	call   800207 <exit>
  80015b:	83 c4 10             	add    $0x10,%esp
	}
	wait(r);
  80015e:	83 ec 0c             	sub    $0xc,%esp
  800161:	57                   	push   %edi
  800162:	e8 dc 1a 00 00       	call   801c43 <wait>
	if ((n2 = readn(fd, buf2, sizeof buf2)) != n)
  800167:	83 c4 0c             	add    $0xc,%esp
  80016a:	68 00 02 00 00       	push   $0x200
  80016f:	68 20 40 80 00       	push   $0x804020
  800174:	53                   	push   %ebx
  800175:	e8 08 13 00 00       	call   801482 <readn>
  80017a:	83 c4 10             	add    $0x10,%esp
  80017d:	39 c6                	cmp    %eax,%esi
  80017f:	74 16                	je     800197 <umain+0x164>
		panic("read in parent got %d, then got %d", n, n2);
  800181:	83 ec 0c             	sub    $0xc,%esp
  800184:	50                   	push   %eax
  800185:	56                   	push   %esi
  800186:	68 58 23 80 00       	push   $0x802358
  80018b:	6a 21                	push   $0x21
  80018d:	68 53 22 80 00       	push   $0x802253
  800192:	e8 82 00 00 00       	call   800219 <_panic>
	cprintf("read in parent succeeded\n");
  800197:	83 ec 0c             	sub    $0xc,%esp
  80019a:	68 94 22 80 00       	push   $0x802294
  80019f:	e8 4d 01 00 00       	call   8002f1 <cprintf>
	close(fd);
  8001a4:	89 1c 24             	mov    %ebx,(%esp)
  8001a7:	e8 17 11 00 00       	call   8012c3 <close>
#include <inc/types.h>

static inline void
breakpoint(void)
{
	asm volatile("int3");
  8001ac:	cc                   	int3   

	breakpoint();
}
  8001ad:	83 c4 10             	add    $0x10,%esp
  8001b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001b3:	5b                   	pop    %ebx
  8001b4:	5e                   	pop    %esi
  8001b5:	5f                   	pop    %edi
  8001b6:	5d                   	pop    %ebp
  8001b7:	c3                   	ret    

008001b8 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8001b8:	55                   	push   %ebp
  8001b9:	89 e5                	mov    %esp,%ebp
  8001bb:	56                   	push   %esi
  8001bc:	53                   	push   %ebx
  8001bd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8001c0:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8001c3:	e8 33 0a 00 00       	call   800bfb <sys_getenvid>
  8001c8:	25 ff 03 00 00       	and    $0x3ff,%eax
  8001cd:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001d4:	c1 e0 07             	shl    $0x7,%eax
  8001d7:	29 d0                	sub    %edx,%eax
  8001d9:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8001de:	a3 20 44 80 00       	mov    %eax,0x804420
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8001e3:	85 db                	test   %ebx,%ebx
  8001e5:	7e 07                	jle    8001ee <libmain+0x36>
		binaryname = argv[0];
  8001e7:	8b 06                	mov    (%esi),%eax
  8001e9:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  8001ee:	83 ec 08             	sub    $0x8,%esp
  8001f1:	56                   	push   %esi
  8001f2:	53                   	push   %ebx
  8001f3:	e8 3b fe ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8001f8:	e8 0a 00 00 00       	call   800207 <exit>
}
  8001fd:	83 c4 10             	add    $0x10,%esp
  800200:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800203:	5b                   	pop    %ebx
  800204:	5e                   	pop    %esi
  800205:	5d                   	pop    %ebp
  800206:	c3                   	ret    

00800207 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800207:	55                   	push   %ebp
  800208:	89 e5                	mov    %esp,%ebp
  80020a:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80020d:	6a 00                	push   $0x0
  80020f:	e8 a6 09 00 00       	call   800bba <sys_env_destroy>
}
  800214:	83 c4 10             	add    $0x10,%esp
  800217:	c9                   	leave  
  800218:	c3                   	ret    

00800219 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800219:	55                   	push   %ebp
  80021a:	89 e5                	mov    %esp,%ebp
  80021c:	56                   	push   %esi
  80021d:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80021e:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800221:	8b 35 00 30 80 00    	mov    0x803000,%esi
  800227:	e8 cf 09 00 00       	call   800bfb <sys_getenvid>
  80022c:	83 ec 0c             	sub    $0xc,%esp
  80022f:	ff 75 0c             	pushl  0xc(%ebp)
  800232:	ff 75 08             	pushl  0x8(%ebp)
  800235:	56                   	push   %esi
  800236:	50                   	push   %eax
  800237:	68 88 23 80 00       	push   $0x802388
  80023c:	e8 b0 00 00 00       	call   8002f1 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800241:	83 c4 18             	add    $0x18,%esp
  800244:	53                   	push   %ebx
  800245:	ff 75 10             	pushl  0x10(%ebp)
  800248:	e8 53 00 00 00       	call   8002a0 <vcprintf>
	cprintf("\n");
  80024d:	c7 04 24 92 22 80 00 	movl   $0x802292,(%esp)
  800254:	e8 98 00 00 00       	call   8002f1 <cprintf>
  800259:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80025c:	cc                   	int3   
  80025d:	eb fd                	jmp    80025c <_panic+0x43>

0080025f <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80025f:	55                   	push   %ebp
  800260:	89 e5                	mov    %esp,%ebp
  800262:	53                   	push   %ebx
  800263:	83 ec 04             	sub    $0x4,%esp
  800266:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800269:	8b 13                	mov    (%ebx),%edx
  80026b:	8d 42 01             	lea    0x1(%edx),%eax
  80026e:	89 03                	mov    %eax,(%ebx)
  800270:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800273:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800277:	3d ff 00 00 00       	cmp    $0xff,%eax
  80027c:	75 1a                	jne    800298 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80027e:	83 ec 08             	sub    $0x8,%esp
  800281:	68 ff 00 00 00       	push   $0xff
  800286:	8d 43 08             	lea    0x8(%ebx),%eax
  800289:	50                   	push   %eax
  80028a:	e8 ee 08 00 00       	call   800b7d <sys_cputs>
		b->idx = 0;
  80028f:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800295:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800298:	ff 43 04             	incl   0x4(%ebx)
}
  80029b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80029e:	c9                   	leave  
  80029f:	c3                   	ret    

008002a0 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8002a0:	55                   	push   %ebp
  8002a1:	89 e5                	mov    %esp,%ebp
  8002a3:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8002a9:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8002b0:	00 00 00 
	b.cnt = 0;
  8002b3:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8002ba:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8002bd:	ff 75 0c             	pushl  0xc(%ebp)
  8002c0:	ff 75 08             	pushl  0x8(%ebp)
  8002c3:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8002c9:	50                   	push   %eax
  8002ca:	68 5f 02 80 00       	push   $0x80025f
  8002cf:	e8 51 01 00 00       	call   800425 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8002d4:	83 c4 08             	add    $0x8,%esp
  8002d7:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8002dd:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8002e3:	50                   	push   %eax
  8002e4:	e8 94 08 00 00       	call   800b7d <sys_cputs>

	return b.cnt;
}
  8002e9:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8002ef:	c9                   	leave  
  8002f0:	c3                   	ret    

008002f1 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8002f1:	55                   	push   %ebp
  8002f2:	89 e5                	mov    %esp,%ebp
  8002f4:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8002f7:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8002fa:	50                   	push   %eax
  8002fb:	ff 75 08             	pushl  0x8(%ebp)
  8002fe:	e8 9d ff ff ff       	call   8002a0 <vcprintf>
	va_end(ap);

	return cnt;
}
  800303:	c9                   	leave  
  800304:	c3                   	ret    

00800305 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800305:	55                   	push   %ebp
  800306:	89 e5                	mov    %esp,%ebp
  800308:	57                   	push   %edi
  800309:	56                   	push   %esi
  80030a:	53                   	push   %ebx
  80030b:	83 ec 1c             	sub    $0x1c,%esp
  80030e:	89 c7                	mov    %eax,%edi
  800310:	89 d6                	mov    %edx,%esi
  800312:	8b 45 08             	mov    0x8(%ebp),%eax
  800315:	8b 55 0c             	mov    0xc(%ebp),%edx
  800318:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80031b:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80031e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800321:	bb 00 00 00 00       	mov    $0x0,%ebx
  800326:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800329:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80032c:	39 d3                	cmp    %edx,%ebx
  80032e:	72 05                	jb     800335 <printnum+0x30>
  800330:	39 45 10             	cmp    %eax,0x10(%ebp)
  800333:	77 45                	ja     80037a <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800335:	83 ec 0c             	sub    $0xc,%esp
  800338:	ff 75 18             	pushl  0x18(%ebp)
  80033b:	8b 45 14             	mov    0x14(%ebp),%eax
  80033e:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800341:	53                   	push   %ebx
  800342:	ff 75 10             	pushl  0x10(%ebp)
  800345:	83 ec 08             	sub    $0x8,%esp
  800348:	ff 75 e4             	pushl  -0x1c(%ebp)
  80034b:	ff 75 e0             	pushl  -0x20(%ebp)
  80034e:	ff 75 dc             	pushl  -0x24(%ebp)
  800351:	ff 75 d8             	pushl  -0x28(%ebp)
  800354:	e8 83 1c 00 00       	call   801fdc <__udivdi3>
  800359:	83 c4 18             	add    $0x18,%esp
  80035c:	52                   	push   %edx
  80035d:	50                   	push   %eax
  80035e:	89 f2                	mov    %esi,%edx
  800360:	89 f8                	mov    %edi,%eax
  800362:	e8 9e ff ff ff       	call   800305 <printnum>
  800367:	83 c4 20             	add    $0x20,%esp
  80036a:	eb 16                	jmp    800382 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80036c:	83 ec 08             	sub    $0x8,%esp
  80036f:	56                   	push   %esi
  800370:	ff 75 18             	pushl  0x18(%ebp)
  800373:	ff d7                	call   *%edi
  800375:	83 c4 10             	add    $0x10,%esp
  800378:	eb 03                	jmp    80037d <printnum+0x78>
  80037a:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80037d:	4b                   	dec    %ebx
  80037e:	85 db                	test   %ebx,%ebx
  800380:	7f ea                	jg     80036c <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800382:	83 ec 08             	sub    $0x8,%esp
  800385:	56                   	push   %esi
  800386:	83 ec 04             	sub    $0x4,%esp
  800389:	ff 75 e4             	pushl  -0x1c(%ebp)
  80038c:	ff 75 e0             	pushl  -0x20(%ebp)
  80038f:	ff 75 dc             	pushl  -0x24(%ebp)
  800392:	ff 75 d8             	pushl  -0x28(%ebp)
  800395:	e8 52 1d 00 00       	call   8020ec <__umoddi3>
  80039a:	83 c4 14             	add    $0x14,%esp
  80039d:	0f be 80 ab 23 80 00 	movsbl 0x8023ab(%eax),%eax
  8003a4:	50                   	push   %eax
  8003a5:	ff d7                	call   *%edi
}
  8003a7:	83 c4 10             	add    $0x10,%esp
  8003aa:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8003ad:	5b                   	pop    %ebx
  8003ae:	5e                   	pop    %esi
  8003af:	5f                   	pop    %edi
  8003b0:	5d                   	pop    %ebp
  8003b1:	c3                   	ret    

008003b2 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8003b2:	55                   	push   %ebp
  8003b3:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8003b5:	83 fa 01             	cmp    $0x1,%edx
  8003b8:	7e 0e                	jle    8003c8 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8003ba:	8b 10                	mov    (%eax),%edx
  8003bc:	8d 4a 08             	lea    0x8(%edx),%ecx
  8003bf:	89 08                	mov    %ecx,(%eax)
  8003c1:	8b 02                	mov    (%edx),%eax
  8003c3:	8b 52 04             	mov    0x4(%edx),%edx
  8003c6:	eb 22                	jmp    8003ea <getuint+0x38>
	else if (lflag)
  8003c8:	85 d2                	test   %edx,%edx
  8003ca:	74 10                	je     8003dc <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8003cc:	8b 10                	mov    (%eax),%edx
  8003ce:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003d1:	89 08                	mov    %ecx,(%eax)
  8003d3:	8b 02                	mov    (%edx),%eax
  8003d5:	ba 00 00 00 00       	mov    $0x0,%edx
  8003da:	eb 0e                	jmp    8003ea <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8003dc:	8b 10                	mov    (%eax),%edx
  8003de:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003e1:	89 08                	mov    %ecx,(%eax)
  8003e3:	8b 02                	mov    (%edx),%eax
  8003e5:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8003ea:	5d                   	pop    %ebp
  8003eb:	c3                   	ret    

008003ec <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8003ec:	55                   	push   %ebp
  8003ed:	89 e5                	mov    %esp,%ebp
  8003ef:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8003f2:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8003f5:	8b 10                	mov    (%eax),%edx
  8003f7:	3b 50 04             	cmp    0x4(%eax),%edx
  8003fa:	73 0a                	jae    800406 <sprintputch+0x1a>
		*b->buf++ = ch;
  8003fc:	8d 4a 01             	lea    0x1(%edx),%ecx
  8003ff:	89 08                	mov    %ecx,(%eax)
  800401:	8b 45 08             	mov    0x8(%ebp),%eax
  800404:	88 02                	mov    %al,(%edx)
}
  800406:	5d                   	pop    %ebp
  800407:	c3                   	ret    

00800408 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800408:	55                   	push   %ebp
  800409:	89 e5                	mov    %esp,%ebp
  80040b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80040e:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800411:	50                   	push   %eax
  800412:	ff 75 10             	pushl  0x10(%ebp)
  800415:	ff 75 0c             	pushl  0xc(%ebp)
  800418:	ff 75 08             	pushl  0x8(%ebp)
  80041b:	e8 05 00 00 00       	call   800425 <vprintfmt>
	va_end(ap);
}
  800420:	83 c4 10             	add    $0x10,%esp
  800423:	c9                   	leave  
  800424:	c3                   	ret    

00800425 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800425:	55                   	push   %ebp
  800426:	89 e5                	mov    %esp,%ebp
  800428:	57                   	push   %edi
  800429:	56                   	push   %esi
  80042a:	53                   	push   %ebx
  80042b:	83 ec 2c             	sub    $0x2c,%esp
  80042e:	8b 75 08             	mov    0x8(%ebp),%esi
  800431:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800434:	8b 7d 10             	mov    0x10(%ebp),%edi
  800437:	eb 12                	jmp    80044b <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800439:	85 c0                	test   %eax,%eax
  80043b:	0f 84 68 03 00 00    	je     8007a9 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800441:	83 ec 08             	sub    $0x8,%esp
  800444:	53                   	push   %ebx
  800445:	50                   	push   %eax
  800446:	ff d6                	call   *%esi
  800448:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80044b:	47                   	inc    %edi
  80044c:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800450:	83 f8 25             	cmp    $0x25,%eax
  800453:	75 e4                	jne    800439 <vprintfmt+0x14>
  800455:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800459:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800460:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800467:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80046e:	ba 00 00 00 00       	mov    $0x0,%edx
  800473:	eb 07                	jmp    80047c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800475:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800478:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80047c:	8d 47 01             	lea    0x1(%edi),%eax
  80047f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800482:	0f b6 0f             	movzbl (%edi),%ecx
  800485:	8a 07                	mov    (%edi),%al
  800487:	83 e8 23             	sub    $0x23,%eax
  80048a:	3c 55                	cmp    $0x55,%al
  80048c:	0f 87 fe 02 00 00    	ja     800790 <vprintfmt+0x36b>
  800492:	0f b6 c0             	movzbl %al,%eax
  800495:	ff 24 85 e0 24 80 00 	jmp    *0x8024e0(,%eax,4)
  80049c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80049f:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8004a3:	eb d7                	jmp    80047c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004a8:	b8 00 00 00 00       	mov    $0x0,%eax
  8004ad:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8004b0:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8004b3:	01 c0                	add    %eax,%eax
  8004b5:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8004b9:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8004bc:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8004bf:	83 fa 09             	cmp    $0x9,%edx
  8004c2:	77 34                	ja     8004f8 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8004c4:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8004c5:	eb e9                	jmp    8004b0 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8004c7:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ca:	8d 48 04             	lea    0x4(%eax),%ecx
  8004cd:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8004d0:	8b 00                	mov    (%eax),%eax
  8004d2:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004d5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8004d8:	eb 24                	jmp    8004fe <vprintfmt+0xd9>
  8004da:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004de:	79 07                	jns    8004e7 <vprintfmt+0xc2>
  8004e0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004ea:	eb 90                	jmp    80047c <vprintfmt+0x57>
  8004ec:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8004ef:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8004f6:	eb 84                	jmp    80047c <vprintfmt+0x57>
  8004f8:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8004fb:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8004fe:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800502:	0f 89 74 ff ff ff    	jns    80047c <vprintfmt+0x57>
				width = precision, precision = -1;
  800508:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80050b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80050e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800515:	e9 62 ff ff ff       	jmp    80047c <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80051a:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80051b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80051e:	e9 59 ff ff ff       	jmp    80047c <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800523:	8b 45 14             	mov    0x14(%ebp),%eax
  800526:	8d 50 04             	lea    0x4(%eax),%edx
  800529:	89 55 14             	mov    %edx,0x14(%ebp)
  80052c:	83 ec 08             	sub    $0x8,%esp
  80052f:	53                   	push   %ebx
  800530:	ff 30                	pushl  (%eax)
  800532:	ff d6                	call   *%esi
			break;
  800534:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800537:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80053a:	e9 0c ff ff ff       	jmp    80044b <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80053f:	8b 45 14             	mov    0x14(%ebp),%eax
  800542:	8d 50 04             	lea    0x4(%eax),%edx
  800545:	89 55 14             	mov    %edx,0x14(%ebp)
  800548:	8b 00                	mov    (%eax),%eax
  80054a:	85 c0                	test   %eax,%eax
  80054c:	79 02                	jns    800550 <vprintfmt+0x12b>
  80054e:	f7 d8                	neg    %eax
  800550:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800552:	83 f8 0f             	cmp    $0xf,%eax
  800555:	7f 0b                	jg     800562 <vprintfmt+0x13d>
  800557:	8b 04 85 40 26 80 00 	mov    0x802640(,%eax,4),%eax
  80055e:	85 c0                	test   %eax,%eax
  800560:	75 18                	jne    80057a <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800562:	52                   	push   %edx
  800563:	68 c3 23 80 00       	push   $0x8023c3
  800568:	53                   	push   %ebx
  800569:	56                   	push   %esi
  80056a:	e8 99 fe ff ff       	call   800408 <printfmt>
  80056f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800572:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800575:	e9 d1 fe ff ff       	jmp    80044b <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80057a:	50                   	push   %eax
  80057b:	68 a5 28 80 00       	push   $0x8028a5
  800580:	53                   	push   %ebx
  800581:	56                   	push   %esi
  800582:	e8 81 fe ff ff       	call   800408 <printfmt>
  800587:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80058a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80058d:	e9 b9 fe ff ff       	jmp    80044b <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800592:	8b 45 14             	mov    0x14(%ebp),%eax
  800595:	8d 50 04             	lea    0x4(%eax),%edx
  800598:	89 55 14             	mov    %edx,0x14(%ebp)
  80059b:	8b 38                	mov    (%eax),%edi
  80059d:	85 ff                	test   %edi,%edi
  80059f:	75 05                	jne    8005a6 <vprintfmt+0x181>
				p = "(null)";
  8005a1:	bf bc 23 80 00       	mov    $0x8023bc,%edi
			if (width > 0 && padc != '-')
  8005a6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005aa:	0f 8e 90 00 00 00    	jle    800640 <vprintfmt+0x21b>
  8005b0:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8005b4:	0f 84 8e 00 00 00    	je     800648 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8005ba:	83 ec 08             	sub    $0x8,%esp
  8005bd:	ff 75 d0             	pushl  -0x30(%ebp)
  8005c0:	57                   	push   %edi
  8005c1:	e8 70 02 00 00       	call   800836 <strnlen>
  8005c6:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8005c9:	29 c1                	sub    %eax,%ecx
  8005cb:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8005ce:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8005d1:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8005d5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8005d8:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8005db:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005dd:	eb 0d                	jmp    8005ec <vprintfmt+0x1c7>
					putch(padc, putdat);
  8005df:	83 ec 08             	sub    $0x8,%esp
  8005e2:	53                   	push   %ebx
  8005e3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005e6:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005e8:	4f                   	dec    %edi
  8005e9:	83 c4 10             	add    $0x10,%esp
  8005ec:	85 ff                	test   %edi,%edi
  8005ee:	7f ef                	jg     8005df <vprintfmt+0x1ba>
  8005f0:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8005f3:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8005f6:	89 c8                	mov    %ecx,%eax
  8005f8:	85 c9                	test   %ecx,%ecx
  8005fa:	79 05                	jns    800601 <vprintfmt+0x1dc>
  8005fc:	b8 00 00 00 00       	mov    $0x0,%eax
  800601:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800604:	29 c1                	sub    %eax,%ecx
  800606:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800609:	89 75 08             	mov    %esi,0x8(%ebp)
  80060c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80060f:	eb 3d                	jmp    80064e <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800611:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800615:	74 19                	je     800630 <vprintfmt+0x20b>
  800617:	0f be c0             	movsbl %al,%eax
  80061a:	83 e8 20             	sub    $0x20,%eax
  80061d:	83 f8 5e             	cmp    $0x5e,%eax
  800620:	76 0e                	jbe    800630 <vprintfmt+0x20b>
					putch('?', putdat);
  800622:	83 ec 08             	sub    $0x8,%esp
  800625:	53                   	push   %ebx
  800626:	6a 3f                	push   $0x3f
  800628:	ff 55 08             	call   *0x8(%ebp)
  80062b:	83 c4 10             	add    $0x10,%esp
  80062e:	eb 0b                	jmp    80063b <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800630:	83 ec 08             	sub    $0x8,%esp
  800633:	53                   	push   %ebx
  800634:	52                   	push   %edx
  800635:	ff 55 08             	call   *0x8(%ebp)
  800638:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80063b:	ff 4d e4             	decl   -0x1c(%ebp)
  80063e:	eb 0e                	jmp    80064e <vprintfmt+0x229>
  800640:	89 75 08             	mov    %esi,0x8(%ebp)
  800643:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800646:	eb 06                	jmp    80064e <vprintfmt+0x229>
  800648:	89 75 08             	mov    %esi,0x8(%ebp)
  80064b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80064e:	47                   	inc    %edi
  80064f:	8a 47 ff             	mov    -0x1(%edi),%al
  800652:	0f be d0             	movsbl %al,%edx
  800655:	85 d2                	test   %edx,%edx
  800657:	74 1d                	je     800676 <vprintfmt+0x251>
  800659:	85 f6                	test   %esi,%esi
  80065b:	78 b4                	js     800611 <vprintfmt+0x1ec>
  80065d:	4e                   	dec    %esi
  80065e:	79 b1                	jns    800611 <vprintfmt+0x1ec>
  800660:	8b 75 08             	mov    0x8(%ebp),%esi
  800663:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800666:	eb 14                	jmp    80067c <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800668:	83 ec 08             	sub    $0x8,%esp
  80066b:	53                   	push   %ebx
  80066c:	6a 20                	push   $0x20
  80066e:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800670:	4f                   	dec    %edi
  800671:	83 c4 10             	add    $0x10,%esp
  800674:	eb 06                	jmp    80067c <vprintfmt+0x257>
  800676:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800679:	8b 75 08             	mov    0x8(%ebp),%esi
  80067c:	85 ff                	test   %edi,%edi
  80067e:	7f e8                	jg     800668 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800680:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800683:	e9 c3 fd ff ff       	jmp    80044b <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800688:	83 fa 01             	cmp    $0x1,%edx
  80068b:	7e 16                	jle    8006a3 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80068d:	8b 45 14             	mov    0x14(%ebp),%eax
  800690:	8d 50 08             	lea    0x8(%eax),%edx
  800693:	89 55 14             	mov    %edx,0x14(%ebp)
  800696:	8b 50 04             	mov    0x4(%eax),%edx
  800699:	8b 00                	mov    (%eax),%eax
  80069b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80069e:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8006a1:	eb 32                	jmp    8006d5 <vprintfmt+0x2b0>
	else if (lflag)
  8006a3:	85 d2                	test   %edx,%edx
  8006a5:	74 18                	je     8006bf <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8006a7:	8b 45 14             	mov    0x14(%ebp),%eax
  8006aa:	8d 50 04             	lea    0x4(%eax),%edx
  8006ad:	89 55 14             	mov    %edx,0x14(%ebp)
  8006b0:	8b 00                	mov    (%eax),%eax
  8006b2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006b5:	89 c1                	mov    %eax,%ecx
  8006b7:	c1 f9 1f             	sar    $0x1f,%ecx
  8006ba:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8006bd:	eb 16                	jmp    8006d5 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8006bf:	8b 45 14             	mov    0x14(%ebp),%eax
  8006c2:	8d 50 04             	lea    0x4(%eax),%edx
  8006c5:	89 55 14             	mov    %edx,0x14(%ebp)
  8006c8:	8b 00                	mov    (%eax),%eax
  8006ca:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006cd:	89 c1                	mov    %eax,%ecx
  8006cf:	c1 f9 1f             	sar    $0x1f,%ecx
  8006d2:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8006d5:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006d8:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8006db:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8006df:	79 76                	jns    800757 <vprintfmt+0x332>
				putch('-', putdat);
  8006e1:	83 ec 08             	sub    $0x8,%esp
  8006e4:	53                   	push   %ebx
  8006e5:	6a 2d                	push   $0x2d
  8006e7:	ff d6                	call   *%esi
				num = -(long long) num;
  8006e9:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006ec:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8006ef:	f7 d8                	neg    %eax
  8006f1:	83 d2 00             	adc    $0x0,%edx
  8006f4:	f7 da                	neg    %edx
  8006f6:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8006f9:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8006fe:	eb 5c                	jmp    80075c <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800700:	8d 45 14             	lea    0x14(%ebp),%eax
  800703:	e8 aa fc ff ff       	call   8003b2 <getuint>
			base = 10;
  800708:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80070d:	eb 4d                	jmp    80075c <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80070f:	8d 45 14             	lea    0x14(%ebp),%eax
  800712:	e8 9b fc ff ff       	call   8003b2 <getuint>
			base = 8;
  800717:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80071c:	eb 3e                	jmp    80075c <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80071e:	83 ec 08             	sub    $0x8,%esp
  800721:	53                   	push   %ebx
  800722:	6a 30                	push   $0x30
  800724:	ff d6                	call   *%esi
			putch('x', putdat);
  800726:	83 c4 08             	add    $0x8,%esp
  800729:	53                   	push   %ebx
  80072a:	6a 78                	push   $0x78
  80072c:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80072e:	8b 45 14             	mov    0x14(%ebp),%eax
  800731:	8d 50 04             	lea    0x4(%eax),%edx
  800734:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800737:	8b 00                	mov    (%eax),%eax
  800739:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80073e:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800741:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800746:	eb 14                	jmp    80075c <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800748:	8d 45 14             	lea    0x14(%ebp),%eax
  80074b:	e8 62 fc ff ff       	call   8003b2 <getuint>
			base = 16;
  800750:	b9 10 00 00 00       	mov    $0x10,%ecx
  800755:	eb 05                	jmp    80075c <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800757:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80075c:	83 ec 0c             	sub    $0xc,%esp
  80075f:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800763:	57                   	push   %edi
  800764:	ff 75 e4             	pushl  -0x1c(%ebp)
  800767:	51                   	push   %ecx
  800768:	52                   	push   %edx
  800769:	50                   	push   %eax
  80076a:	89 da                	mov    %ebx,%edx
  80076c:	89 f0                	mov    %esi,%eax
  80076e:	e8 92 fb ff ff       	call   800305 <printnum>
			break;
  800773:	83 c4 20             	add    $0x20,%esp
  800776:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800779:	e9 cd fc ff ff       	jmp    80044b <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80077e:	83 ec 08             	sub    $0x8,%esp
  800781:	53                   	push   %ebx
  800782:	51                   	push   %ecx
  800783:	ff d6                	call   *%esi
			break;
  800785:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800788:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80078b:	e9 bb fc ff ff       	jmp    80044b <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800790:	83 ec 08             	sub    $0x8,%esp
  800793:	53                   	push   %ebx
  800794:	6a 25                	push   $0x25
  800796:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800798:	83 c4 10             	add    $0x10,%esp
  80079b:	eb 01                	jmp    80079e <vprintfmt+0x379>
  80079d:	4f                   	dec    %edi
  80079e:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8007a2:	75 f9                	jne    80079d <vprintfmt+0x378>
  8007a4:	e9 a2 fc ff ff       	jmp    80044b <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8007a9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8007ac:	5b                   	pop    %ebx
  8007ad:	5e                   	pop    %esi
  8007ae:	5f                   	pop    %edi
  8007af:	5d                   	pop    %ebp
  8007b0:	c3                   	ret    

008007b1 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8007b1:	55                   	push   %ebp
  8007b2:	89 e5                	mov    %esp,%ebp
  8007b4:	83 ec 18             	sub    $0x18,%esp
  8007b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ba:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8007bd:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8007c0:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8007c4:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8007c7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007ce:	85 c0                	test   %eax,%eax
  8007d0:	74 26                	je     8007f8 <vsnprintf+0x47>
  8007d2:	85 d2                	test   %edx,%edx
  8007d4:	7e 29                	jle    8007ff <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8007d6:	ff 75 14             	pushl  0x14(%ebp)
  8007d9:	ff 75 10             	pushl  0x10(%ebp)
  8007dc:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007df:	50                   	push   %eax
  8007e0:	68 ec 03 80 00       	push   $0x8003ec
  8007e5:	e8 3b fc ff ff       	call   800425 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8007ea:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8007ed:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8007f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8007f3:	83 c4 10             	add    $0x10,%esp
  8007f6:	eb 0c                	jmp    800804 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8007f8:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8007fd:	eb 05                	jmp    800804 <vsnprintf+0x53>
  8007ff:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800804:	c9                   	leave  
  800805:	c3                   	ret    

00800806 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800806:	55                   	push   %ebp
  800807:	89 e5                	mov    %esp,%ebp
  800809:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80080c:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80080f:	50                   	push   %eax
  800810:	ff 75 10             	pushl  0x10(%ebp)
  800813:	ff 75 0c             	pushl  0xc(%ebp)
  800816:	ff 75 08             	pushl  0x8(%ebp)
  800819:	e8 93 ff ff ff       	call   8007b1 <vsnprintf>
	va_end(ap);

	return rc;
}
  80081e:	c9                   	leave  
  80081f:	c3                   	ret    

00800820 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800820:	55                   	push   %ebp
  800821:	89 e5                	mov    %esp,%ebp
  800823:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800826:	b8 00 00 00 00       	mov    $0x0,%eax
  80082b:	eb 01                	jmp    80082e <strlen+0xe>
		n++;
  80082d:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80082e:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800832:	75 f9                	jne    80082d <strlen+0xd>
		n++;
	return n;
}
  800834:	5d                   	pop    %ebp
  800835:	c3                   	ret    

00800836 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800836:	55                   	push   %ebp
  800837:	89 e5                	mov    %esp,%ebp
  800839:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80083c:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80083f:	ba 00 00 00 00       	mov    $0x0,%edx
  800844:	eb 01                	jmp    800847 <strnlen+0x11>
		n++;
  800846:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800847:	39 c2                	cmp    %eax,%edx
  800849:	74 08                	je     800853 <strnlen+0x1d>
  80084b:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80084f:	75 f5                	jne    800846 <strnlen+0x10>
  800851:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800853:	5d                   	pop    %ebp
  800854:	c3                   	ret    

00800855 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800855:	55                   	push   %ebp
  800856:	89 e5                	mov    %esp,%ebp
  800858:	53                   	push   %ebx
  800859:	8b 45 08             	mov    0x8(%ebp),%eax
  80085c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80085f:	89 c2                	mov    %eax,%edx
  800861:	42                   	inc    %edx
  800862:	41                   	inc    %ecx
  800863:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800866:	88 5a ff             	mov    %bl,-0x1(%edx)
  800869:	84 db                	test   %bl,%bl
  80086b:	75 f4                	jne    800861 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80086d:	5b                   	pop    %ebx
  80086e:	5d                   	pop    %ebp
  80086f:	c3                   	ret    

00800870 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800870:	55                   	push   %ebp
  800871:	89 e5                	mov    %esp,%ebp
  800873:	53                   	push   %ebx
  800874:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800877:	53                   	push   %ebx
  800878:	e8 a3 ff ff ff       	call   800820 <strlen>
  80087d:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800880:	ff 75 0c             	pushl  0xc(%ebp)
  800883:	01 d8                	add    %ebx,%eax
  800885:	50                   	push   %eax
  800886:	e8 ca ff ff ff       	call   800855 <strcpy>
	return dst;
}
  80088b:	89 d8                	mov    %ebx,%eax
  80088d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800890:	c9                   	leave  
  800891:	c3                   	ret    

00800892 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800892:	55                   	push   %ebp
  800893:	89 e5                	mov    %esp,%ebp
  800895:	56                   	push   %esi
  800896:	53                   	push   %ebx
  800897:	8b 75 08             	mov    0x8(%ebp),%esi
  80089a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80089d:	89 f3                	mov    %esi,%ebx
  80089f:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008a2:	89 f2                	mov    %esi,%edx
  8008a4:	eb 0c                	jmp    8008b2 <strncpy+0x20>
		*dst++ = *src;
  8008a6:	42                   	inc    %edx
  8008a7:	8a 01                	mov    (%ecx),%al
  8008a9:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8008ac:	80 39 01             	cmpb   $0x1,(%ecx)
  8008af:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008b2:	39 da                	cmp    %ebx,%edx
  8008b4:	75 f0                	jne    8008a6 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8008b6:	89 f0                	mov    %esi,%eax
  8008b8:	5b                   	pop    %ebx
  8008b9:	5e                   	pop    %esi
  8008ba:	5d                   	pop    %ebp
  8008bb:	c3                   	ret    

008008bc <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8008bc:	55                   	push   %ebp
  8008bd:	89 e5                	mov    %esp,%ebp
  8008bf:	56                   	push   %esi
  8008c0:	53                   	push   %ebx
  8008c1:	8b 75 08             	mov    0x8(%ebp),%esi
  8008c4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8008c7:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8008ca:	85 c0                	test   %eax,%eax
  8008cc:	74 1e                	je     8008ec <strlcpy+0x30>
  8008ce:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8008d2:	89 f2                	mov    %esi,%edx
  8008d4:	eb 05                	jmp    8008db <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8008d6:	42                   	inc    %edx
  8008d7:	41                   	inc    %ecx
  8008d8:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8008db:	39 c2                	cmp    %eax,%edx
  8008dd:	74 08                	je     8008e7 <strlcpy+0x2b>
  8008df:	8a 19                	mov    (%ecx),%bl
  8008e1:	84 db                	test   %bl,%bl
  8008e3:	75 f1                	jne    8008d6 <strlcpy+0x1a>
  8008e5:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8008e7:	c6 00 00             	movb   $0x0,(%eax)
  8008ea:	eb 02                	jmp    8008ee <strlcpy+0x32>
  8008ec:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8008ee:	29 f0                	sub    %esi,%eax
}
  8008f0:	5b                   	pop    %ebx
  8008f1:	5e                   	pop    %esi
  8008f2:	5d                   	pop    %ebp
  8008f3:	c3                   	ret    

008008f4 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8008f4:	55                   	push   %ebp
  8008f5:	89 e5                	mov    %esp,%ebp
  8008f7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8008fa:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8008fd:	eb 02                	jmp    800901 <strcmp+0xd>
		p++, q++;
  8008ff:	41                   	inc    %ecx
  800900:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800901:	8a 01                	mov    (%ecx),%al
  800903:	84 c0                	test   %al,%al
  800905:	74 04                	je     80090b <strcmp+0x17>
  800907:	3a 02                	cmp    (%edx),%al
  800909:	74 f4                	je     8008ff <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80090b:	0f b6 c0             	movzbl %al,%eax
  80090e:	0f b6 12             	movzbl (%edx),%edx
  800911:	29 d0                	sub    %edx,%eax
}
  800913:	5d                   	pop    %ebp
  800914:	c3                   	ret    

00800915 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800915:	55                   	push   %ebp
  800916:	89 e5                	mov    %esp,%ebp
  800918:	53                   	push   %ebx
  800919:	8b 45 08             	mov    0x8(%ebp),%eax
  80091c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80091f:	89 c3                	mov    %eax,%ebx
  800921:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800924:	eb 02                	jmp    800928 <strncmp+0x13>
		n--, p++, q++;
  800926:	40                   	inc    %eax
  800927:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800928:	39 d8                	cmp    %ebx,%eax
  80092a:	74 14                	je     800940 <strncmp+0x2b>
  80092c:	8a 08                	mov    (%eax),%cl
  80092e:	84 c9                	test   %cl,%cl
  800930:	74 04                	je     800936 <strncmp+0x21>
  800932:	3a 0a                	cmp    (%edx),%cl
  800934:	74 f0                	je     800926 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800936:	0f b6 00             	movzbl (%eax),%eax
  800939:	0f b6 12             	movzbl (%edx),%edx
  80093c:	29 d0                	sub    %edx,%eax
  80093e:	eb 05                	jmp    800945 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800940:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800945:	5b                   	pop    %ebx
  800946:	5d                   	pop    %ebp
  800947:	c3                   	ret    

00800948 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800948:	55                   	push   %ebp
  800949:	89 e5                	mov    %esp,%ebp
  80094b:	8b 45 08             	mov    0x8(%ebp),%eax
  80094e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800951:	eb 05                	jmp    800958 <strchr+0x10>
		if (*s == c)
  800953:	38 ca                	cmp    %cl,%dl
  800955:	74 0c                	je     800963 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800957:	40                   	inc    %eax
  800958:	8a 10                	mov    (%eax),%dl
  80095a:	84 d2                	test   %dl,%dl
  80095c:	75 f5                	jne    800953 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80095e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800963:	5d                   	pop    %ebp
  800964:	c3                   	ret    

00800965 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800965:	55                   	push   %ebp
  800966:	89 e5                	mov    %esp,%ebp
  800968:	8b 45 08             	mov    0x8(%ebp),%eax
  80096b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80096e:	eb 05                	jmp    800975 <strfind+0x10>
		if (*s == c)
  800970:	38 ca                	cmp    %cl,%dl
  800972:	74 07                	je     80097b <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800974:	40                   	inc    %eax
  800975:	8a 10                	mov    (%eax),%dl
  800977:	84 d2                	test   %dl,%dl
  800979:	75 f5                	jne    800970 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80097b:	5d                   	pop    %ebp
  80097c:	c3                   	ret    

0080097d <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80097d:	55                   	push   %ebp
  80097e:	89 e5                	mov    %esp,%ebp
  800980:	57                   	push   %edi
  800981:	56                   	push   %esi
  800982:	53                   	push   %ebx
  800983:	8b 7d 08             	mov    0x8(%ebp),%edi
  800986:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800989:	85 c9                	test   %ecx,%ecx
  80098b:	74 36                	je     8009c3 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80098d:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800993:	75 28                	jne    8009bd <memset+0x40>
  800995:	f6 c1 03             	test   $0x3,%cl
  800998:	75 23                	jne    8009bd <memset+0x40>
		c &= 0xFF;
  80099a:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80099e:	89 d3                	mov    %edx,%ebx
  8009a0:	c1 e3 08             	shl    $0x8,%ebx
  8009a3:	89 d6                	mov    %edx,%esi
  8009a5:	c1 e6 18             	shl    $0x18,%esi
  8009a8:	89 d0                	mov    %edx,%eax
  8009aa:	c1 e0 10             	shl    $0x10,%eax
  8009ad:	09 f0                	or     %esi,%eax
  8009af:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8009b1:	89 d8                	mov    %ebx,%eax
  8009b3:	09 d0                	or     %edx,%eax
  8009b5:	c1 e9 02             	shr    $0x2,%ecx
  8009b8:	fc                   	cld    
  8009b9:	f3 ab                	rep stos %eax,%es:(%edi)
  8009bb:	eb 06                	jmp    8009c3 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8009bd:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009c0:	fc                   	cld    
  8009c1:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8009c3:	89 f8                	mov    %edi,%eax
  8009c5:	5b                   	pop    %ebx
  8009c6:	5e                   	pop    %esi
  8009c7:	5f                   	pop    %edi
  8009c8:	5d                   	pop    %ebp
  8009c9:	c3                   	ret    

008009ca <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8009ca:	55                   	push   %ebp
  8009cb:	89 e5                	mov    %esp,%ebp
  8009cd:	57                   	push   %edi
  8009ce:	56                   	push   %esi
  8009cf:	8b 45 08             	mov    0x8(%ebp),%eax
  8009d2:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009d5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8009d8:	39 c6                	cmp    %eax,%esi
  8009da:	73 33                	jae    800a0f <memmove+0x45>
  8009dc:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8009df:	39 d0                	cmp    %edx,%eax
  8009e1:	73 2c                	jae    800a0f <memmove+0x45>
		s += n;
		d += n;
  8009e3:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009e6:	89 d6                	mov    %edx,%esi
  8009e8:	09 fe                	or     %edi,%esi
  8009ea:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8009f0:	75 13                	jne    800a05 <memmove+0x3b>
  8009f2:	f6 c1 03             	test   $0x3,%cl
  8009f5:	75 0e                	jne    800a05 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8009f7:	83 ef 04             	sub    $0x4,%edi
  8009fa:	8d 72 fc             	lea    -0x4(%edx),%esi
  8009fd:	c1 e9 02             	shr    $0x2,%ecx
  800a00:	fd                   	std    
  800a01:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a03:	eb 07                	jmp    800a0c <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800a05:	4f                   	dec    %edi
  800a06:	8d 72 ff             	lea    -0x1(%edx),%esi
  800a09:	fd                   	std    
  800a0a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800a0c:	fc                   	cld    
  800a0d:	eb 1d                	jmp    800a2c <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a0f:	89 f2                	mov    %esi,%edx
  800a11:	09 c2                	or     %eax,%edx
  800a13:	f6 c2 03             	test   $0x3,%dl
  800a16:	75 0f                	jne    800a27 <memmove+0x5d>
  800a18:	f6 c1 03             	test   $0x3,%cl
  800a1b:	75 0a                	jne    800a27 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800a1d:	c1 e9 02             	shr    $0x2,%ecx
  800a20:	89 c7                	mov    %eax,%edi
  800a22:	fc                   	cld    
  800a23:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a25:	eb 05                	jmp    800a2c <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800a27:	89 c7                	mov    %eax,%edi
  800a29:	fc                   	cld    
  800a2a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800a2c:	5e                   	pop    %esi
  800a2d:	5f                   	pop    %edi
  800a2e:	5d                   	pop    %ebp
  800a2f:	c3                   	ret    

00800a30 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800a30:	55                   	push   %ebp
  800a31:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800a33:	ff 75 10             	pushl  0x10(%ebp)
  800a36:	ff 75 0c             	pushl  0xc(%ebp)
  800a39:	ff 75 08             	pushl  0x8(%ebp)
  800a3c:	e8 89 ff ff ff       	call   8009ca <memmove>
}
  800a41:	c9                   	leave  
  800a42:	c3                   	ret    

00800a43 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800a43:	55                   	push   %ebp
  800a44:	89 e5                	mov    %esp,%ebp
  800a46:	56                   	push   %esi
  800a47:	53                   	push   %ebx
  800a48:	8b 45 08             	mov    0x8(%ebp),%eax
  800a4b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a4e:	89 c6                	mov    %eax,%esi
  800a50:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a53:	eb 14                	jmp    800a69 <memcmp+0x26>
		if (*s1 != *s2)
  800a55:	8a 08                	mov    (%eax),%cl
  800a57:	8a 1a                	mov    (%edx),%bl
  800a59:	38 d9                	cmp    %bl,%cl
  800a5b:	74 0a                	je     800a67 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800a5d:	0f b6 c1             	movzbl %cl,%eax
  800a60:	0f b6 db             	movzbl %bl,%ebx
  800a63:	29 d8                	sub    %ebx,%eax
  800a65:	eb 0b                	jmp    800a72 <memcmp+0x2f>
		s1++, s2++;
  800a67:	40                   	inc    %eax
  800a68:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a69:	39 f0                	cmp    %esi,%eax
  800a6b:	75 e8                	jne    800a55 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800a6d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a72:	5b                   	pop    %ebx
  800a73:	5e                   	pop    %esi
  800a74:	5d                   	pop    %ebp
  800a75:	c3                   	ret    

00800a76 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800a76:	55                   	push   %ebp
  800a77:	89 e5                	mov    %esp,%ebp
  800a79:	53                   	push   %ebx
  800a7a:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800a7d:	89 c1                	mov    %eax,%ecx
  800a7f:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800a82:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a86:	eb 08                	jmp    800a90 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a88:	0f b6 10             	movzbl (%eax),%edx
  800a8b:	39 da                	cmp    %ebx,%edx
  800a8d:	74 05                	je     800a94 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a8f:	40                   	inc    %eax
  800a90:	39 c8                	cmp    %ecx,%eax
  800a92:	72 f4                	jb     800a88 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800a94:	5b                   	pop    %ebx
  800a95:	5d                   	pop    %ebp
  800a96:	c3                   	ret    

00800a97 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800a97:	55                   	push   %ebp
  800a98:	89 e5                	mov    %esp,%ebp
  800a9a:	57                   	push   %edi
  800a9b:	56                   	push   %esi
  800a9c:	53                   	push   %ebx
  800a9d:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800aa0:	eb 01                	jmp    800aa3 <strtol+0xc>
		s++;
  800aa2:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800aa3:	8a 01                	mov    (%ecx),%al
  800aa5:	3c 20                	cmp    $0x20,%al
  800aa7:	74 f9                	je     800aa2 <strtol+0xb>
  800aa9:	3c 09                	cmp    $0x9,%al
  800aab:	74 f5                	je     800aa2 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800aad:	3c 2b                	cmp    $0x2b,%al
  800aaf:	75 08                	jne    800ab9 <strtol+0x22>
		s++;
  800ab1:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800ab2:	bf 00 00 00 00       	mov    $0x0,%edi
  800ab7:	eb 11                	jmp    800aca <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800ab9:	3c 2d                	cmp    $0x2d,%al
  800abb:	75 08                	jne    800ac5 <strtol+0x2e>
		s++, neg = 1;
  800abd:	41                   	inc    %ecx
  800abe:	bf 01 00 00 00       	mov    $0x1,%edi
  800ac3:	eb 05                	jmp    800aca <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800ac5:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800aca:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800ace:	0f 84 87 00 00 00    	je     800b5b <strtol+0xc4>
  800ad4:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800ad8:	75 27                	jne    800b01 <strtol+0x6a>
  800ada:	80 39 30             	cmpb   $0x30,(%ecx)
  800add:	75 22                	jne    800b01 <strtol+0x6a>
  800adf:	e9 88 00 00 00       	jmp    800b6c <strtol+0xd5>
		s += 2, base = 16;
  800ae4:	83 c1 02             	add    $0x2,%ecx
  800ae7:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800aee:	eb 11                	jmp    800b01 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800af0:	41                   	inc    %ecx
  800af1:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800af8:	eb 07                	jmp    800b01 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800afa:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800b01:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800b06:	8a 11                	mov    (%ecx),%dl
  800b08:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800b0b:	80 fb 09             	cmp    $0x9,%bl
  800b0e:	77 08                	ja     800b18 <strtol+0x81>
			dig = *s - '0';
  800b10:	0f be d2             	movsbl %dl,%edx
  800b13:	83 ea 30             	sub    $0x30,%edx
  800b16:	eb 22                	jmp    800b3a <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800b18:	8d 72 9f             	lea    -0x61(%edx),%esi
  800b1b:	89 f3                	mov    %esi,%ebx
  800b1d:	80 fb 19             	cmp    $0x19,%bl
  800b20:	77 08                	ja     800b2a <strtol+0x93>
			dig = *s - 'a' + 10;
  800b22:	0f be d2             	movsbl %dl,%edx
  800b25:	83 ea 57             	sub    $0x57,%edx
  800b28:	eb 10                	jmp    800b3a <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800b2a:	8d 72 bf             	lea    -0x41(%edx),%esi
  800b2d:	89 f3                	mov    %esi,%ebx
  800b2f:	80 fb 19             	cmp    $0x19,%bl
  800b32:	77 14                	ja     800b48 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800b34:	0f be d2             	movsbl %dl,%edx
  800b37:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800b3a:	3b 55 10             	cmp    0x10(%ebp),%edx
  800b3d:	7d 09                	jge    800b48 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800b3f:	41                   	inc    %ecx
  800b40:	0f af 45 10          	imul   0x10(%ebp),%eax
  800b44:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800b46:	eb be                	jmp    800b06 <strtol+0x6f>

	if (endptr)
  800b48:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800b4c:	74 05                	je     800b53 <strtol+0xbc>
		*endptr = (char *) s;
  800b4e:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b51:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800b53:	85 ff                	test   %edi,%edi
  800b55:	74 21                	je     800b78 <strtol+0xe1>
  800b57:	f7 d8                	neg    %eax
  800b59:	eb 1d                	jmp    800b78 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b5b:	80 39 30             	cmpb   $0x30,(%ecx)
  800b5e:	75 9a                	jne    800afa <strtol+0x63>
  800b60:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b64:	0f 84 7a ff ff ff    	je     800ae4 <strtol+0x4d>
  800b6a:	eb 84                	jmp    800af0 <strtol+0x59>
  800b6c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b70:	0f 84 6e ff ff ff    	je     800ae4 <strtol+0x4d>
  800b76:	eb 89                	jmp    800b01 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800b78:	5b                   	pop    %ebx
  800b79:	5e                   	pop    %esi
  800b7a:	5f                   	pop    %edi
  800b7b:	5d                   	pop    %ebp
  800b7c:	c3                   	ret    

00800b7d <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800b7d:	55                   	push   %ebp
  800b7e:	89 e5                	mov    %esp,%ebp
  800b80:	57                   	push   %edi
  800b81:	56                   	push   %esi
  800b82:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b83:	b8 00 00 00 00       	mov    $0x0,%eax
  800b88:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b8b:	8b 55 08             	mov    0x8(%ebp),%edx
  800b8e:	89 c3                	mov    %eax,%ebx
  800b90:	89 c7                	mov    %eax,%edi
  800b92:	89 c6                	mov    %eax,%esi
  800b94:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800b96:	5b                   	pop    %ebx
  800b97:	5e                   	pop    %esi
  800b98:	5f                   	pop    %edi
  800b99:	5d                   	pop    %ebp
  800b9a:	c3                   	ret    

00800b9b <sys_cgetc>:

int
sys_cgetc(void)
{
  800b9b:	55                   	push   %ebp
  800b9c:	89 e5                	mov    %esp,%ebp
  800b9e:	57                   	push   %edi
  800b9f:	56                   	push   %esi
  800ba0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ba1:	ba 00 00 00 00       	mov    $0x0,%edx
  800ba6:	b8 01 00 00 00       	mov    $0x1,%eax
  800bab:	89 d1                	mov    %edx,%ecx
  800bad:	89 d3                	mov    %edx,%ebx
  800baf:	89 d7                	mov    %edx,%edi
  800bb1:	89 d6                	mov    %edx,%esi
  800bb3:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800bb5:	5b                   	pop    %ebx
  800bb6:	5e                   	pop    %esi
  800bb7:	5f                   	pop    %edi
  800bb8:	5d                   	pop    %ebp
  800bb9:	c3                   	ret    

00800bba <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800bba:	55                   	push   %ebp
  800bbb:	89 e5                	mov    %esp,%ebp
  800bbd:	57                   	push   %edi
  800bbe:	56                   	push   %esi
  800bbf:	53                   	push   %ebx
  800bc0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc3:	b9 00 00 00 00       	mov    $0x0,%ecx
  800bc8:	b8 03 00 00 00       	mov    $0x3,%eax
  800bcd:	8b 55 08             	mov    0x8(%ebp),%edx
  800bd0:	89 cb                	mov    %ecx,%ebx
  800bd2:	89 cf                	mov    %ecx,%edi
  800bd4:	89 ce                	mov    %ecx,%esi
  800bd6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bd8:	85 c0                	test   %eax,%eax
  800bda:	7e 17                	jle    800bf3 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bdc:	83 ec 0c             	sub    $0xc,%esp
  800bdf:	50                   	push   %eax
  800be0:	6a 03                	push   $0x3
  800be2:	68 9f 26 80 00       	push   $0x80269f
  800be7:	6a 23                	push   $0x23
  800be9:	68 bc 26 80 00       	push   $0x8026bc
  800bee:	e8 26 f6 ff ff       	call   800219 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800bf3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf6:	5b                   	pop    %ebx
  800bf7:	5e                   	pop    %esi
  800bf8:	5f                   	pop    %edi
  800bf9:	5d                   	pop    %ebp
  800bfa:	c3                   	ret    

00800bfb <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800bfb:	55                   	push   %ebp
  800bfc:	89 e5                	mov    %esp,%ebp
  800bfe:	57                   	push   %edi
  800bff:	56                   	push   %esi
  800c00:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c01:	ba 00 00 00 00       	mov    $0x0,%edx
  800c06:	b8 02 00 00 00       	mov    $0x2,%eax
  800c0b:	89 d1                	mov    %edx,%ecx
  800c0d:	89 d3                	mov    %edx,%ebx
  800c0f:	89 d7                	mov    %edx,%edi
  800c11:	89 d6                	mov    %edx,%esi
  800c13:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800c15:	5b                   	pop    %ebx
  800c16:	5e                   	pop    %esi
  800c17:	5f                   	pop    %edi
  800c18:	5d                   	pop    %ebp
  800c19:	c3                   	ret    

00800c1a <sys_yield>:

void
sys_yield(void)
{
  800c1a:	55                   	push   %ebp
  800c1b:	89 e5                	mov    %esp,%ebp
  800c1d:	57                   	push   %edi
  800c1e:	56                   	push   %esi
  800c1f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c20:	ba 00 00 00 00       	mov    $0x0,%edx
  800c25:	b8 0b 00 00 00       	mov    $0xb,%eax
  800c2a:	89 d1                	mov    %edx,%ecx
  800c2c:	89 d3                	mov    %edx,%ebx
  800c2e:	89 d7                	mov    %edx,%edi
  800c30:	89 d6                	mov    %edx,%esi
  800c32:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800c34:	5b                   	pop    %ebx
  800c35:	5e                   	pop    %esi
  800c36:	5f                   	pop    %edi
  800c37:	5d                   	pop    %ebp
  800c38:	c3                   	ret    

00800c39 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800c39:	55                   	push   %ebp
  800c3a:	89 e5                	mov    %esp,%ebp
  800c3c:	57                   	push   %edi
  800c3d:	56                   	push   %esi
  800c3e:	53                   	push   %ebx
  800c3f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c42:	be 00 00 00 00       	mov    $0x0,%esi
  800c47:	b8 04 00 00 00       	mov    $0x4,%eax
  800c4c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c4f:	8b 55 08             	mov    0x8(%ebp),%edx
  800c52:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c55:	89 f7                	mov    %esi,%edi
  800c57:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c59:	85 c0                	test   %eax,%eax
  800c5b:	7e 17                	jle    800c74 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c5d:	83 ec 0c             	sub    $0xc,%esp
  800c60:	50                   	push   %eax
  800c61:	6a 04                	push   $0x4
  800c63:	68 9f 26 80 00       	push   $0x80269f
  800c68:	6a 23                	push   $0x23
  800c6a:	68 bc 26 80 00       	push   $0x8026bc
  800c6f:	e8 a5 f5 ff ff       	call   800219 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800c74:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c77:	5b                   	pop    %ebx
  800c78:	5e                   	pop    %esi
  800c79:	5f                   	pop    %edi
  800c7a:	5d                   	pop    %ebp
  800c7b:	c3                   	ret    

00800c7c <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800c7c:	55                   	push   %ebp
  800c7d:	89 e5                	mov    %esp,%ebp
  800c7f:	57                   	push   %edi
  800c80:	56                   	push   %esi
  800c81:	53                   	push   %ebx
  800c82:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c85:	b8 05 00 00 00       	mov    $0x5,%eax
  800c8a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c8d:	8b 55 08             	mov    0x8(%ebp),%edx
  800c90:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c93:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c96:	8b 75 18             	mov    0x18(%ebp),%esi
  800c99:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c9b:	85 c0                	test   %eax,%eax
  800c9d:	7e 17                	jle    800cb6 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c9f:	83 ec 0c             	sub    $0xc,%esp
  800ca2:	50                   	push   %eax
  800ca3:	6a 05                	push   $0x5
  800ca5:	68 9f 26 80 00       	push   $0x80269f
  800caa:	6a 23                	push   $0x23
  800cac:	68 bc 26 80 00       	push   $0x8026bc
  800cb1:	e8 63 f5 ff ff       	call   800219 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800cb6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cb9:	5b                   	pop    %ebx
  800cba:	5e                   	pop    %esi
  800cbb:	5f                   	pop    %edi
  800cbc:	5d                   	pop    %ebp
  800cbd:	c3                   	ret    

00800cbe <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800cbe:	55                   	push   %ebp
  800cbf:	89 e5                	mov    %esp,%ebp
  800cc1:	57                   	push   %edi
  800cc2:	56                   	push   %esi
  800cc3:	53                   	push   %ebx
  800cc4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cc7:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ccc:	b8 06 00 00 00       	mov    $0x6,%eax
  800cd1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cd4:	8b 55 08             	mov    0x8(%ebp),%edx
  800cd7:	89 df                	mov    %ebx,%edi
  800cd9:	89 de                	mov    %ebx,%esi
  800cdb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cdd:	85 c0                	test   %eax,%eax
  800cdf:	7e 17                	jle    800cf8 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ce1:	83 ec 0c             	sub    $0xc,%esp
  800ce4:	50                   	push   %eax
  800ce5:	6a 06                	push   $0x6
  800ce7:	68 9f 26 80 00       	push   $0x80269f
  800cec:	6a 23                	push   $0x23
  800cee:	68 bc 26 80 00       	push   $0x8026bc
  800cf3:	e8 21 f5 ff ff       	call   800219 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800cf8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cfb:	5b                   	pop    %ebx
  800cfc:	5e                   	pop    %esi
  800cfd:	5f                   	pop    %edi
  800cfe:	5d                   	pop    %ebp
  800cff:	c3                   	ret    

00800d00 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800d00:	55                   	push   %ebp
  800d01:	89 e5                	mov    %esp,%ebp
  800d03:	57                   	push   %edi
  800d04:	56                   	push   %esi
  800d05:	53                   	push   %ebx
  800d06:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d09:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d0e:	b8 08 00 00 00       	mov    $0x8,%eax
  800d13:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d16:	8b 55 08             	mov    0x8(%ebp),%edx
  800d19:	89 df                	mov    %ebx,%edi
  800d1b:	89 de                	mov    %ebx,%esi
  800d1d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d1f:	85 c0                	test   %eax,%eax
  800d21:	7e 17                	jle    800d3a <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d23:	83 ec 0c             	sub    $0xc,%esp
  800d26:	50                   	push   %eax
  800d27:	6a 08                	push   $0x8
  800d29:	68 9f 26 80 00       	push   $0x80269f
  800d2e:	6a 23                	push   $0x23
  800d30:	68 bc 26 80 00       	push   $0x8026bc
  800d35:	e8 df f4 ff ff       	call   800219 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800d3a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d3d:	5b                   	pop    %ebx
  800d3e:	5e                   	pop    %esi
  800d3f:	5f                   	pop    %edi
  800d40:	5d                   	pop    %ebp
  800d41:	c3                   	ret    

00800d42 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800d42:	55                   	push   %ebp
  800d43:	89 e5                	mov    %esp,%ebp
  800d45:	57                   	push   %edi
  800d46:	56                   	push   %esi
  800d47:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d48:	ba 00 00 00 00       	mov    $0x0,%edx
  800d4d:	b8 0c 00 00 00       	mov    $0xc,%eax
  800d52:	89 d1                	mov    %edx,%ecx
  800d54:	89 d3                	mov    %edx,%ebx
  800d56:	89 d7                	mov    %edx,%edi
  800d58:	89 d6                	mov    %edx,%esi
  800d5a:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800d5c:	5b                   	pop    %ebx
  800d5d:	5e                   	pop    %esi
  800d5e:	5f                   	pop    %edi
  800d5f:	5d                   	pop    %ebp
  800d60:	c3                   	ret    

00800d61 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800d61:	55                   	push   %ebp
  800d62:	89 e5                	mov    %esp,%ebp
  800d64:	57                   	push   %edi
  800d65:	56                   	push   %esi
  800d66:	53                   	push   %ebx
  800d67:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d6a:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d6f:	b8 09 00 00 00       	mov    $0x9,%eax
  800d74:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d77:	8b 55 08             	mov    0x8(%ebp),%edx
  800d7a:	89 df                	mov    %ebx,%edi
  800d7c:	89 de                	mov    %ebx,%esi
  800d7e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d80:	85 c0                	test   %eax,%eax
  800d82:	7e 17                	jle    800d9b <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d84:	83 ec 0c             	sub    $0xc,%esp
  800d87:	50                   	push   %eax
  800d88:	6a 09                	push   $0x9
  800d8a:	68 9f 26 80 00       	push   $0x80269f
  800d8f:	6a 23                	push   $0x23
  800d91:	68 bc 26 80 00       	push   $0x8026bc
  800d96:	e8 7e f4 ff ff       	call   800219 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800d9b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d9e:	5b                   	pop    %ebx
  800d9f:	5e                   	pop    %esi
  800da0:	5f                   	pop    %edi
  800da1:	5d                   	pop    %ebp
  800da2:	c3                   	ret    

00800da3 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800da3:	55                   	push   %ebp
  800da4:	89 e5                	mov    %esp,%ebp
  800da6:	57                   	push   %edi
  800da7:	56                   	push   %esi
  800da8:	53                   	push   %ebx
  800da9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dac:	bb 00 00 00 00       	mov    $0x0,%ebx
  800db1:	b8 0a 00 00 00       	mov    $0xa,%eax
  800db6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800db9:	8b 55 08             	mov    0x8(%ebp),%edx
  800dbc:	89 df                	mov    %ebx,%edi
  800dbe:	89 de                	mov    %ebx,%esi
  800dc0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800dc2:	85 c0                	test   %eax,%eax
  800dc4:	7e 17                	jle    800ddd <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800dc6:	83 ec 0c             	sub    $0xc,%esp
  800dc9:	50                   	push   %eax
  800dca:	6a 0a                	push   $0xa
  800dcc:	68 9f 26 80 00       	push   $0x80269f
  800dd1:	6a 23                	push   $0x23
  800dd3:	68 bc 26 80 00       	push   $0x8026bc
  800dd8:	e8 3c f4 ff ff       	call   800219 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800ddd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800de0:	5b                   	pop    %ebx
  800de1:	5e                   	pop    %esi
  800de2:	5f                   	pop    %edi
  800de3:	5d                   	pop    %ebp
  800de4:	c3                   	ret    

00800de5 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800de5:	55                   	push   %ebp
  800de6:	89 e5                	mov    %esp,%ebp
  800de8:	57                   	push   %edi
  800de9:	56                   	push   %esi
  800dea:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800deb:	be 00 00 00 00       	mov    $0x0,%esi
  800df0:	b8 0d 00 00 00       	mov    $0xd,%eax
  800df5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800df8:	8b 55 08             	mov    0x8(%ebp),%edx
  800dfb:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800dfe:	8b 7d 14             	mov    0x14(%ebp),%edi
  800e01:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800e03:	5b                   	pop    %ebx
  800e04:	5e                   	pop    %esi
  800e05:	5f                   	pop    %edi
  800e06:	5d                   	pop    %ebp
  800e07:	c3                   	ret    

00800e08 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800e08:	55                   	push   %ebp
  800e09:	89 e5                	mov    %esp,%ebp
  800e0b:	57                   	push   %edi
  800e0c:	56                   	push   %esi
  800e0d:	53                   	push   %ebx
  800e0e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e11:	b9 00 00 00 00       	mov    $0x0,%ecx
  800e16:	b8 0e 00 00 00       	mov    $0xe,%eax
  800e1b:	8b 55 08             	mov    0x8(%ebp),%edx
  800e1e:	89 cb                	mov    %ecx,%ebx
  800e20:	89 cf                	mov    %ecx,%edi
  800e22:	89 ce                	mov    %ecx,%esi
  800e24:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e26:	85 c0                	test   %eax,%eax
  800e28:	7e 17                	jle    800e41 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e2a:	83 ec 0c             	sub    $0xc,%esp
  800e2d:	50                   	push   %eax
  800e2e:	6a 0e                	push   $0xe
  800e30:	68 9f 26 80 00       	push   $0x80269f
  800e35:	6a 23                	push   $0x23
  800e37:	68 bc 26 80 00       	push   $0x8026bc
  800e3c:	e8 d8 f3 ff ff       	call   800219 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800e41:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e44:	5b                   	pop    %ebx
  800e45:	5e                   	pop    %esi
  800e46:	5f                   	pop    %edi
  800e47:	5d                   	pop    %ebp
  800e48:	c3                   	ret    

00800e49 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800e49:	55                   	push   %ebp
  800e4a:	89 e5                	mov    %esp,%ebp
  800e4c:	53                   	push   %ebx
  800e4d:	83 ec 04             	sub    $0x4,%esp
  800e50:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800e53:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800e55:	89 da                	mov    %ebx,%edx
  800e57:	c1 ea 0c             	shr    $0xc,%edx
  800e5a:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800e61:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800e65:	74 05                	je     800e6c <pgfault+0x23>
  800e67:	f6 c6 08             	test   $0x8,%dh
  800e6a:	75 14                	jne    800e80 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800e6c:	83 ec 04             	sub    $0x4,%esp
  800e6f:	68 cc 26 80 00       	push   $0x8026cc
  800e74:	6a 26                	push   $0x26
  800e76:	68 30 27 80 00       	push   $0x802730
  800e7b:	e8 99 f3 ff ff       	call   800219 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800e80:	83 ec 04             	sub    $0x4,%esp
  800e83:	6a 07                	push   $0x7
  800e85:	68 00 f0 7f 00       	push   $0x7ff000
  800e8a:	6a 00                	push   $0x0
  800e8c:	e8 a8 fd ff ff       	call   800c39 <sys_page_alloc>
  800e91:	83 c4 10             	add    $0x10,%esp
  800e94:	85 c0                	test   %eax,%eax
  800e96:	74 14                	je     800eac <pgfault+0x63>
        panic("pgfault: no phys mem");
  800e98:	83 ec 04             	sub    $0x4,%esp
  800e9b:	68 3b 27 80 00       	push   $0x80273b
  800ea0:	6a 32                	push   $0x32
  800ea2:	68 30 27 80 00       	push   $0x802730
  800ea7:	e8 6d f3 ff ff       	call   800219 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800eac:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800eb2:	83 ec 04             	sub    $0x4,%esp
  800eb5:	68 00 10 00 00       	push   $0x1000
  800eba:	53                   	push   %ebx
  800ebb:	68 00 f0 7f 00       	push   $0x7ff000
  800ec0:	e8 05 fb ff ff       	call   8009ca <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800ec5:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800ecc:	53                   	push   %ebx
  800ecd:	6a 00                	push   $0x0
  800ecf:	68 00 f0 7f 00       	push   $0x7ff000
  800ed4:	6a 00                	push   $0x0
  800ed6:	e8 a1 fd ff ff       	call   800c7c <sys_page_map>
  800edb:	83 c4 20             	add    $0x20,%esp
  800ede:	85 c0                	test   %eax,%eax
  800ee0:	74 14                	je     800ef6 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800ee2:	83 ec 04             	sub    $0x4,%esp
  800ee5:	68 50 27 80 00       	push   $0x802750
  800eea:	6a 3f                	push   $0x3f
  800eec:	68 30 27 80 00       	push   $0x802730
  800ef1:	e8 23 f3 ff ff       	call   800219 <_panic>


	//panic("pgfault not implemented");
}
  800ef6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ef9:	c9                   	leave  
  800efa:	c3                   	ret    

00800efb <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800efb:	55                   	push   %ebp
  800efc:	89 e5                	mov    %esp,%ebp
  800efe:	57                   	push   %edi
  800eff:	56                   	push   %esi
  800f00:	53                   	push   %ebx
  800f01:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800f04:	68 49 0e 80 00       	push   $0x800e49
  800f09:	e8 09 0f 00 00       	call   801e17 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800f0e:	b8 07 00 00 00       	mov    $0x7,%eax
  800f13:	cd 30                	int    $0x30
  800f15:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800f18:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800f1b:	83 c4 10             	add    $0x10,%esp
  800f1e:	85 c0                	test   %eax,%eax
  800f20:	79 17                	jns    800f39 <fork+0x3e>
        panic("fork: cannot create child env");
  800f22:	83 ec 04             	sub    $0x4,%esp
  800f25:	68 63 27 80 00       	push   $0x802763
  800f2a:	68 97 00 00 00       	push   $0x97
  800f2f:	68 30 27 80 00       	push   $0x802730
  800f34:	e8 e0 f2 ff ff       	call   800219 <_panic>
    } else if (envid == 0) {
  800f39:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800f3d:	75 2a                	jne    800f69 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800f3f:	e8 b7 fc ff ff       	call   800bfb <sys_getenvid>
  800f44:	25 ff 03 00 00       	and    $0x3ff,%eax
  800f49:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f50:	c1 e0 07             	shl    $0x7,%eax
  800f53:	29 d0                	sub    %edx,%eax
  800f55:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800f5a:	a3 20 44 80 00       	mov    %eax,0x804420
        return 0;
  800f5f:	b8 00 00 00 00       	mov    $0x0,%eax
  800f64:	e9 94 01 00 00       	jmp    8010fd <fork+0x202>
  800f69:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800f70:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800f73:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800f7a:	a8 01                	test   $0x1,%al
  800f7c:	0f 84 0a 01 00 00    	je     80108c <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800f82:	c1 e7 0a             	shl    $0xa,%edi
  800f85:	be 00 00 00 00       	mov    $0x0,%esi
  800f8a:	89 fb                	mov    %edi,%ebx
  800f8c:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800f8e:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800f94:	75 34                	jne    800fca <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800f96:	83 ec 04             	sub    $0x4,%esp
  800f99:	6a 07                	push   $0x7
  800f9b:	68 00 f0 bf ee       	push   $0xeebff000
  800fa0:	ff 75 e4             	pushl  -0x1c(%ebp)
  800fa3:	e8 91 fc ff ff       	call   800c39 <sys_page_alloc>
  800fa8:	83 c4 10             	add    $0x10,%esp
  800fab:	85 c0                	test   %eax,%eax
  800fad:	0f 84 cc 00 00 00    	je     80107f <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800fb3:	83 ec 04             	sub    $0x4,%esp
  800fb6:	68 81 27 80 00       	push   $0x802781
  800fbb:	68 ad 00 00 00       	push   $0xad
  800fc0:	68 30 27 80 00       	push   $0x802730
  800fc5:	e8 4f f2 ff ff       	call   800219 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800fca:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800fd1:	a8 01                	test   $0x1,%al
  800fd3:	0f 84 a6 00 00 00    	je     80107f <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800fd9:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800fe0:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800fe3:	a9 02 08 00 00       	test   $0x802,%eax
  800fe8:	74 62                	je     80104c <fork+0x151>
  800fea:	f6 c4 04             	test   $0x4,%ah
  800fed:	75 78                	jne    801067 <fork+0x16c>
        if (sys_page_map(0,
  800fef:	83 ec 0c             	sub    $0xc,%esp
  800ff2:	68 05 08 00 00       	push   $0x805
  800ff7:	53                   	push   %ebx
  800ff8:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ffb:	53                   	push   %ebx
  800ffc:	6a 00                	push   $0x0
  800ffe:	e8 79 fc ff ff       	call   800c7c <sys_page_map>
  801003:	83 c4 20             	add    $0x20,%esp
  801006:	85 c0                	test   %eax,%eax
  801008:	74 14                	je     80101e <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  80100a:	83 ec 04             	sub    $0x4,%esp
  80100d:	68 9c 27 80 00       	push   $0x80279c
  801012:	6a 64                	push   $0x64
  801014:	68 30 27 80 00       	push   $0x802730
  801019:	e8 fb f1 ff ff       	call   800219 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  80101e:	83 ec 0c             	sub    $0xc,%esp
  801021:	68 05 08 00 00       	push   $0x805
  801026:	53                   	push   %ebx
  801027:	6a 00                	push   $0x0
  801029:	53                   	push   %ebx
  80102a:	6a 00                	push   $0x0
  80102c:	e8 4b fc ff ff       	call   800c7c <sys_page_map>
  801031:	83 c4 20             	add    $0x20,%esp
  801034:	85 c0                	test   %eax,%eax
  801036:	74 47                	je     80107f <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  801038:	83 ec 04             	sub    $0x4,%esp
  80103b:	68 b3 27 80 00       	push   $0x8027b3
  801040:	6a 6d                	push   $0x6d
  801042:	68 30 27 80 00       	push   $0x802730
  801047:	e8 cd f1 ff ff       	call   800219 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  80104c:	f6 c4 04             	test   $0x4,%ah
  80104f:	75 16                	jne    801067 <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  801051:	83 ec 0c             	sub    $0xc,%esp
  801054:	6a 05                	push   $0x5
  801056:	53                   	push   %ebx
  801057:	ff 75 e4             	pushl  -0x1c(%ebp)
  80105a:	53                   	push   %ebx
  80105b:	6a 00                	push   $0x0
  80105d:	e8 1a fc ff ff       	call   800c7c <sys_page_map>
  801062:	83 c4 20             	add    $0x20,%esp
  801065:	eb 18                	jmp    80107f <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  801067:	83 ec 0c             	sub    $0xc,%esp
  80106a:	25 07 0e 00 00       	and    $0xe07,%eax
  80106f:	50                   	push   %eax
  801070:	53                   	push   %ebx
  801071:	ff 75 e4             	pushl  -0x1c(%ebp)
  801074:	53                   	push   %ebx
  801075:	6a 00                	push   $0x0
  801077:	e8 00 fc ff ff       	call   800c7c <sys_page_map>
  80107c:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  80107f:	46                   	inc    %esi
  801080:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  801086:	0f 85 fe fe ff ff    	jne    800f8a <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80108c:	ff 45 e0             	incl   -0x20(%ebp)
  80108f:	8b 45 e0             	mov    -0x20(%ebp),%eax
  801092:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  801097:	0f 85 d3 fe ff ff    	jne    800f70 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  80109d:	a1 20 44 80 00       	mov    0x804420,%eax
  8010a2:	8b 40 64             	mov    0x64(%eax),%eax
  8010a5:	83 ec 08             	sub    $0x8,%esp
  8010a8:	50                   	push   %eax
  8010a9:	ff 75 dc             	pushl  -0x24(%ebp)
  8010ac:	e8 f2 fc ff ff       	call   800da3 <sys_env_set_pgfault_upcall>
  8010b1:	83 c4 10             	add    $0x10,%esp
  8010b4:	85 c0                	test   %eax,%eax
  8010b6:	74 17                	je     8010cf <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  8010b8:	83 ec 04             	sub    $0x4,%esp
  8010bb:	68 10 27 80 00       	push   $0x802710
  8010c0:	68 b9 00 00 00       	push   $0xb9
  8010c5:	68 30 27 80 00       	push   $0x802730
  8010ca:	e8 4a f1 ff ff       	call   800219 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  8010cf:	83 ec 08             	sub    $0x8,%esp
  8010d2:	6a 02                	push   $0x2
  8010d4:	ff 75 dc             	pushl  -0x24(%ebp)
  8010d7:	e8 24 fc ff ff       	call   800d00 <sys_env_set_status>
  8010dc:	83 c4 10             	add    $0x10,%esp
  8010df:	85 c0                	test   %eax,%eax
  8010e1:	74 17                	je     8010fa <fork+0x1ff>
        panic("fork: cannot set env status");
  8010e3:	83 ec 04             	sub    $0x4,%esp
  8010e6:	68 ce 27 80 00       	push   $0x8027ce
  8010eb:	68 bd 00 00 00       	push   $0xbd
  8010f0:	68 30 27 80 00       	push   $0x802730
  8010f5:	e8 1f f1 ff ff       	call   800219 <_panic>

    return envid;
  8010fa:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  8010fd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801100:	5b                   	pop    %ebx
  801101:	5e                   	pop    %esi
  801102:	5f                   	pop    %edi
  801103:	5d                   	pop    %ebp
  801104:	c3                   	ret    

00801105 <sfork>:

// Challenge!
int
sfork(void)
{
  801105:	55                   	push   %ebp
  801106:	89 e5                	mov    %esp,%ebp
  801108:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  80110b:	68 ea 27 80 00       	push   $0x8027ea
  801110:	68 c8 00 00 00       	push   $0xc8
  801115:	68 30 27 80 00       	push   $0x802730
  80111a:	e8 fa f0 ff ff       	call   800219 <_panic>

0080111f <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  80111f:	55                   	push   %ebp
  801120:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  801122:	8b 45 08             	mov    0x8(%ebp),%eax
  801125:	05 00 00 00 30       	add    $0x30000000,%eax
  80112a:	c1 e8 0c             	shr    $0xc,%eax
}
  80112d:	5d                   	pop    %ebp
  80112e:	c3                   	ret    

0080112f <fd2data>:

char*
fd2data(struct Fd *fd)
{
  80112f:	55                   	push   %ebp
  801130:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  801132:	8b 45 08             	mov    0x8(%ebp),%eax
  801135:	05 00 00 00 30       	add    $0x30000000,%eax
  80113a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  80113f:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  801144:	5d                   	pop    %ebp
  801145:	c3                   	ret    

00801146 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  801146:	55                   	push   %ebp
  801147:	89 e5                	mov    %esp,%ebp
  801149:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80114c:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  801151:	89 c2                	mov    %eax,%edx
  801153:	c1 ea 16             	shr    $0x16,%edx
  801156:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  80115d:	f6 c2 01             	test   $0x1,%dl
  801160:	74 11                	je     801173 <fd_alloc+0x2d>
  801162:	89 c2                	mov    %eax,%edx
  801164:	c1 ea 0c             	shr    $0xc,%edx
  801167:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80116e:	f6 c2 01             	test   $0x1,%dl
  801171:	75 09                	jne    80117c <fd_alloc+0x36>
			*fd_store = fd;
  801173:	89 01                	mov    %eax,(%ecx)
			return 0;
  801175:	b8 00 00 00 00       	mov    $0x0,%eax
  80117a:	eb 17                	jmp    801193 <fd_alloc+0x4d>
  80117c:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801181:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801186:	75 c9                	jne    801151 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801188:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  80118e:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801193:	5d                   	pop    %ebp
  801194:	c3                   	ret    

00801195 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  801195:	55                   	push   %ebp
  801196:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  801198:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  80119c:	77 39                	ja     8011d7 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  80119e:	8b 45 08             	mov    0x8(%ebp),%eax
  8011a1:	c1 e0 0c             	shl    $0xc,%eax
  8011a4:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  8011a9:	89 c2                	mov    %eax,%edx
  8011ab:	c1 ea 16             	shr    $0x16,%edx
  8011ae:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8011b5:	f6 c2 01             	test   $0x1,%dl
  8011b8:	74 24                	je     8011de <fd_lookup+0x49>
  8011ba:	89 c2                	mov    %eax,%edx
  8011bc:	c1 ea 0c             	shr    $0xc,%edx
  8011bf:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8011c6:	f6 c2 01             	test   $0x1,%dl
  8011c9:	74 1a                	je     8011e5 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  8011cb:	8b 55 0c             	mov    0xc(%ebp),%edx
  8011ce:	89 02                	mov    %eax,(%edx)
	return 0;
  8011d0:	b8 00 00 00 00       	mov    $0x0,%eax
  8011d5:	eb 13                	jmp    8011ea <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8011d7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011dc:	eb 0c                	jmp    8011ea <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8011de:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011e3:	eb 05                	jmp    8011ea <fd_lookup+0x55>
  8011e5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8011ea:	5d                   	pop    %ebp
  8011eb:	c3                   	ret    

008011ec <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8011ec:	55                   	push   %ebp
  8011ed:	89 e5                	mov    %esp,%ebp
  8011ef:	83 ec 08             	sub    $0x8,%esp
  8011f2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8011f5:	ba 7c 28 80 00       	mov    $0x80287c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  8011fa:	eb 13                	jmp    80120f <dev_lookup+0x23>
  8011fc:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  8011ff:	39 08                	cmp    %ecx,(%eax)
  801201:	75 0c                	jne    80120f <dev_lookup+0x23>
			*dev = devtab[i];
  801203:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801206:	89 01                	mov    %eax,(%ecx)
			return 0;
  801208:	b8 00 00 00 00       	mov    $0x0,%eax
  80120d:	eb 2e                	jmp    80123d <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  80120f:	8b 02                	mov    (%edx),%eax
  801211:	85 c0                	test   %eax,%eax
  801213:	75 e7                	jne    8011fc <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  801215:	a1 20 44 80 00       	mov    0x804420,%eax
  80121a:	8b 40 48             	mov    0x48(%eax),%eax
  80121d:	83 ec 04             	sub    $0x4,%esp
  801220:	51                   	push   %ecx
  801221:	50                   	push   %eax
  801222:	68 00 28 80 00       	push   $0x802800
  801227:	e8 c5 f0 ff ff       	call   8002f1 <cprintf>
	*dev = 0;
  80122c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80122f:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  801235:	83 c4 10             	add    $0x10,%esp
  801238:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  80123d:	c9                   	leave  
  80123e:	c3                   	ret    

0080123f <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  80123f:	55                   	push   %ebp
  801240:	89 e5                	mov    %esp,%ebp
  801242:	56                   	push   %esi
  801243:	53                   	push   %ebx
  801244:	83 ec 10             	sub    $0x10,%esp
  801247:	8b 75 08             	mov    0x8(%ebp),%esi
  80124a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  80124d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801250:	50                   	push   %eax
  801251:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  801257:	c1 e8 0c             	shr    $0xc,%eax
  80125a:	50                   	push   %eax
  80125b:	e8 35 ff ff ff       	call   801195 <fd_lookup>
  801260:	83 c4 08             	add    $0x8,%esp
  801263:	85 c0                	test   %eax,%eax
  801265:	78 05                	js     80126c <fd_close+0x2d>
	    || fd != fd2)
  801267:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  80126a:	74 06                	je     801272 <fd_close+0x33>
		return (must_exist ? r : 0);
  80126c:	84 db                	test   %bl,%bl
  80126e:	74 47                	je     8012b7 <fd_close+0x78>
  801270:	eb 4a                	jmp    8012bc <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801272:	83 ec 08             	sub    $0x8,%esp
  801275:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801278:	50                   	push   %eax
  801279:	ff 36                	pushl  (%esi)
  80127b:	e8 6c ff ff ff       	call   8011ec <dev_lookup>
  801280:	89 c3                	mov    %eax,%ebx
  801282:	83 c4 10             	add    $0x10,%esp
  801285:	85 c0                	test   %eax,%eax
  801287:	78 1c                	js     8012a5 <fd_close+0x66>
		if (dev->dev_close)
  801289:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80128c:	8b 40 10             	mov    0x10(%eax),%eax
  80128f:	85 c0                	test   %eax,%eax
  801291:	74 0d                	je     8012a0 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801293:	83 ec 0c             	sub    $0xc,%esp
  801296:	56                   	push   %esi
  801297:	ff d0                	call   *%eax
  801299:	89 c3                	mov    %eax,%ebx
  80129b:	83 c4 10             	add    $0x10,%esp
  80129e:	eb 05                	jmp    8012a5 <fd_close+0x66>
		else
			r = 0;
  8012a0:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  8012a5:	83 ec 08             	sub    $0x8,%esp
  8012a8:	56                   	push   %esi
  8012a9:	6a 00                	push   $0x0
  8012ab:	e8 0e fa ff ff       	call   800cbe <sys_page_unmap>
	return r;
  8012b0:	83 c4 10             	add    $0x10,%esp
  8012b3:	89 d8                	mov    %ebx,%eax
  8012b5:	eb 05                	jmp    8012bc <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  8012b7:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  8012bc:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8012bf:	5b                   	pop    %ebx
  8012c0:	5e                   	pop    %esi
  8012c1:	5d                   	pop    %ebp
  8012c2:	c3                   	ret    

008012c3 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  8012c3:	55                   	push   %ebp
  8012c4:	89 e5                	mov    %esp,%ebp
  8012c6:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8012c9:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012cc:	50                   	push   %eax
  8012cd:	ff 75 08             	pushl  0x8(%ebp)
  8012d0:	e8 c0 fe ff ff       	call   801195 <fd_lookup>
  8012d5:	83 c4 08             	add    $0x8,%esp
  8012d8:	85 c0                	test   %eax,%eax
  8012da:	78 10                	js     8012ec <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8012dc:	83 ec 08             	sub    $0x8,%esp
  8012df:	6a 01                	push   $0x1
  8012e1:	ff 75 f4             	pushl  -0xc(%ebp)
  8012e4:	e8 56 ff ff ff       	call   80123f <fd_close>
  8012e9:	83 c4 10             	add    $0x10,%esp
}
  8012ec:	c9                   	leave  
  8012ed:	c3                   	ret    

008012ee <close_all>:

void
close_all(void)
{
  8012ee:	55                   	push   %ebp
  8012ef:	89 e5                	mov    %esp,%ebp
  8012f1:	53                   	push   %ebx
  8012f2:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  8012f5:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  8012fa:	83 ec 0c             	sub    $0xc,%esp
  8012fd:	53                   	push   %ebx
  8012fe:	e8 c0 ff ff ff       	call   8012c3 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801303:	43                   	inc    %ebx
  801304:	83 c4 10             	add    $0x10,%esp
  801307:	83 fb 20             	cmp    $0x20,%ebx
  80130a:	75 ee                	jne    8012fa <close_all+0xc>
		close(i);
}
  80130c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80130f:	c9                   	leave  
  801310:	c3                   	ret    

00801311 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801311:	55                   	push   %ebp
  801312:	89 e5                	mov    %esp,%ebp
  801314:	57                   	push   %edi
  801315:	56                   	push   %esi
  801316:	53                   	push   %ebx
  801317:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  80131a:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  80131d:	50                   	push   %eax
  80131e:	ff 75 08             	pushl  0x8(%ebp)
  801321:	e8 6f fe ff ff       	call   801195 <fd_lookup>
  801326:	83 c4 08             	add    $0x8,%esp
  801329:	85 c0                	test   %eax,%eax
  80132b:	0f 88 c2 00 00 00    	js     8013f3 <dup+0xe2>
		return r;
	close(newfdnum);
  801331:	83 ec 0c             	sub    $0xc,%esp
  801334:	ff 75 0c             	pushl  0xc(%ebp)
  801337:	e8 87 ff ff ff       	call   8012c3 <close>

	newfd = INDEX2FD(newfdnum);
  80133c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80133f:	c1 e3 0c             	shl    $0xc,%ebx
  801342:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801348:	83 c4 04             	add    $0x4,%esp
  80134b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80134e:	e8 dc fd ff ff       	call   80112f <fd2data>
  801353:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  801355:	89 1c 24             	mov    %ebx,(%esp)
  801358:	e8 d2 fd ff ff       	call   80112f <fd2data>
  80135d:	83 c4 10             	add    $0x10,%esp
  801360:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801362:	89 f0                	mov    %esi,%eax
  801364:	c1 e8 16             	shr    $0x16,%eax
  801367:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  80136e:	a8 01                	test   $0x1,%al
  801370:	74 35                	je     8013a7 <dup+0x96>
  801372:	89 f0                	mov    %esi,%eax
  801374:	c1 e8 0c             	shr    $0xc,%eax
  801377:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  80137e:	f6 c2 01             	test   $0x1,%dl
  801381:	74 24                	je     8013a7 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801383:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80138a:	83 ec 0c             	sub    $0xc,%esp
  80138d:	25 07 0e 00 00       	and    $0xe07,%eax
  801392:	50                   	push   %eax
  801393:	57                   	push   %edi
  801394:	6a 00                	push   $0x0
  801396:	56                   	push   %esi
  801397:	6a 00                	push   $0x0
  801399:	e8 de f8 ff ff       	call   800c7c <sys_page_map>
  80139e:	89 c6                	mov    %eax,%esi
  8013a0:	83 c4 20             	add    $0x20,%esp
  8013a3:	85 c0                	test   %eax,%eax
  8013a5:	78 2c                	js     8013d3 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8013a7:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8013aa:	89 d0                	mov    %edx,%eax
  8013ac:	c1 e8 0c             	shr    $0xc,%eax
  8013af:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8013b6:	83 ec 0c             	sub    $0xc,%esp
  8013b9:	25 07 0e 00 00       	and    $0xe07,%eax
  8013be:	50                   	push   %eax
  8013bf:	53                   	push   %ebx
  8013c0:	6a 00                	push   $0x0
  8013c2:	52                   	push   %edx
  8013c3:	6a 00                	push   $0x0
  8013c5:	e8 b2 f8 ff ff       	call   800c7c <sys_page_map>
  8013ca:	89 c6                	mov    %eax,%esi
  8013cc:	83 c4 20             	add    $0x20,%esp
  8013cf:	85 c0                	test   %eax,%eax
  8013d1:	79 1d                	jns    8013f0 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8013d3:	83 ec 08             	sub    $0x8,%esp
  8013d6:	53                   	push   %ebx
  8013d7:	6a 00                	push   $0x0
  8013d9:	e8 e0 f8 ff ff       	call   800cbe <sys_page_unmap>
	sys_page_unmap(0, nva);
  8013de:	83 c4 08             	add    $0x8,%esp
  8013e1:	57                   	push   %edi
  8013e2:	6a 00                	push   $0x0
  8013e4:	e8 d5 f8 ff ff       	call   800cbe <sys_page_unmap>
	return r;
  8013e9:	83 c4 10             	add    $0x10,%esp
  8013ec:	89 f0                	mov    %esi,%eax
  8013ee:	eb 03                	jmp    8013f3 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8013f0:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8013f3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8013f6:	5b                   	pop    %ebx
  8013f7:	5e                   	pop    %esi
  8013f8:	5f                   	pop    %edi
  8013f9:	5d                   	pop    %ebp
  8013fa:	c3                   	ret    

008013fb <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8013fb:	55                   	push   %ebp
  8013fc:	89 e5                	mov    %esp,%ebp
  8013fe:	53                   	push   %ebx
  8013ff:	83 ec 14             	sub    $0x14,%esp
  801402:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801405:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801408:	50                   	push   %eax
  801409:	53                   	push   %ebx
  80140a:	e8 86 fd ff ff       	call   801195 <fd_lookup>
  80140f:	83 c4 08             	add    $0x8,%esp
  801412:	85 c0                	test   %eax,%eax
  801414:	78 67                	js     80147d <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801416:	83 ec 08             	sub    $0x8,%esp
  801419:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80141c:	50                   	push   %eax
  80141d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801420:	ff 30                	pushl  (%eax)
  801422:	e8 c5 fd ff ff       	call   8011ec <dev_lookup>
  801427:	83 c4 10             	add    $0x10,%esp
  80142a:	85 c0                	test   %eax,%eax
  80142c:	78 4f                	js     80147d <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  80142e:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801431:	8b 42 08             	mov    0x8(%edx),%eax
  801434:	83 e0 03             	and    $0x3,%eax
  801437:	83 f8 01             	cmp    $0x1,%eax
  80143a:	75 21                	jne    80145d <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  80143c:	a1 20 44 80 00       	mov    0x804420,%eax
  801441:	8b 40 48             	mov    0x48(%eax),%eax
  801444:	83 ec 04             	sub    $0x4,%esp
  801447:	53                   	push   %ebx
  801448:	50                   	push   %eax
  801449:	68 41 28 80 00       	push   $0x802841
  80144e:	e8 9e ee ff ff       	call   8002f1 <cprintf>
		return -E_INVAL;
  801453:	83 c4 10             	add    $0x10,%esp
  801456:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80145b:	eb 20                	jmp    80147d <read+0x82>
	}
	if (!dev->dev_read)
  80145d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801460:	8b 40 08             	mov    0x8(%eax),%eax
  801463:	85 c0                	test   %eax,%eax
  801465:	74 11                	je     801478 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801467:	83 ec 04             	sub    $0x4,%esp
  80146a:	ff 75 10             	pushl  0x10(%ebp)
  80146d:	ff 75 0c             	pushl  0xc(%ebp)
  801470:	52                   	push   %edx
  801471:	ff d0                	call   *%eax
  801473:	83 c4 10             	add    $0x10,%esp
  801476:	eb 05                	jmp    80147d <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801478:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  80147d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801480:	c9                   	leave  
  801481:	c3                   	ret    

00801482 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801482:	55                   	push   %ebp
  801483:	89 e5                	mov    %esp,%ebp
  801485:	57                   	push   %edi
  801486:	56                   	push   %esi
  801487:	53                   	push   %ebx
  801488:	83 ec 0c             	sub    $0xc,%esp
  80148b:	8b 7d 08             	mov    0x8(%ebp),%edi
  80148e:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801491:	bb 00 00 00 00       	mov    $0x0,%ebx
  801496:	eb 21                	jmp    8014b9 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801498:	83 ec 04             	sub    $0x4,%esp
  80149b:	89 f0                	mov    %esi,%eax
  80149d:	29 d8                	sub    %ebx,%eax
  80149f:	50                   	push   %eax
  8014a0:	89 d8                	mov    %ebx,%eax
  8014a2:	03 45 0c             	add    0xc(%ebp),%eax
  8014a5:	50                   	push   %eax
  8014a6:	57                   	push   %edi
  8014a7:	e8 4f ff ff ff       	call   8013fb <read>
		if (m < 0)
  8014ac:	83 c4 10             	add    $0x10,%esp
  8014af:	85 c0                	test   %eax,%eax
  8014b1:	78 10                	js     8014c3 <readn+0x41>
			return m;
		if (m == 0)
  8014b3:	85 c0                	test   %eax,%eax
  8014b5:	74 0a                	je     8014c1 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8014b7:	01 c3                	add    %eax,%ebx
  8014b9:	39 f3                	cmp    %esi,%ebx
  8014bb:	72 db                	jb     801498 <readn+0x16>
  8014bd:	89 d8                	mov    %ebx,%eax
  8014bf:	eb 02                	jmp    8014c3 <readn+0x41>
  8014c1:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  8014c3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8014c6:	5b                   	pop    %ebx
  8014c7:	5e                   	pop    %esi
  8014c8:	5f                   	pop    %edi
  8014c9:	5d                   	pop    %ebp
  8014ca:	c3                   	ret    

008014cb <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8014cb:	55                   	push   %ebp
  8014cc:	89 e5                	mov    %esp,%ebp
  8014ce:	53                   	push   %ebx
  8014cf:	83 ec 14             	sub    $0x14,%esp
  8014d2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8014d5:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8014d8:	50                   	push   %eax
  8014d9:	53                   	push   %ebx
  8014da:	e8 b6 fc ff ff       	call   801195 <fd_lookup>
  8014df:	83 c4 08             	add    $0x8,%esp
  8014e2:	85 c0                	test   %eax,%eax
  8014e4:	78 62                	js     801548 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8014e6:	83 ec 08             	sub    $0x8,%esp
  8014e9:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8014ec:	50                   	push   %eax
  8014ed:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8014f0:	ff 30                	pushl  (%eax)
  8014f2:	e8 f5 fc ff ff       	call   8011ec <dev_lookup>
  8014f7:	83 c4 10             	add    $0x10,%esp
  8014fa:	85 c0                	test   %eax,%eax
  8014fc:	78 4a                	js     801548 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8014fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801501:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801505:	75 21                	jne    801528 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801507:	a1 20 44 80 00       	mov    0x804420,%eax
  80150c:	8b 40 48             	mov    0x48(%eax),%eax
  80150f:	83 ec 04             	sub    $0x4,%esp
  801512:	53                   	push   %ebx
  801513:	50                   	push   %eax
  801514:	68 5d 28 80 00       	push   $0x80285d
  801519:	e8 d3 ed ff ff       	call   8002f1 <cprintf>
		return -E_INVAL;
  80151e:	83 c4 10             	add    $0x10,%esp
  801521:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801526:	eb 20                	jmp    801548 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801528:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80152b:	8b 52 0c             	mov    0xc(%edx),%edx
  80152e:	85 d2                	test   %edx,%edx
  801530:	74 11                	je     801543 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801532:	83 ec 04             	sub    $0x4,%esp
  801535:	ff 75 10             	pushl  0x10(%ebp)
  801538:	ff 75 0c             	pushl  0xc(%ebp)
  80153b:	50                   	push   %eax
  80153c:	ff d2                	call   *%edx
  80153e:	83 c4 10             	add    $0x10,%esp
  801541:	eb 05                	jmp    801548 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801543:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801548:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80154b:	c9                   	leave  
  80154c:	c3                   	ret    

0080154d <seek>:

int
seek(int fdnum, off_t offset)
{
  80154d:	55                   	push   %ebp
  80154e:	89 e5                	mov    %esp,%ebp
  801550:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801553:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801556:	50                   	push   %eax
  801557:	ff 75 08             	pushl  0x8(%ebp)
  80155a:	e8 36 fc ff ff       	call   801195 <fd_lookup>
  80155f:	83 c4 08             	add    $0x8,%esp
  801562:	85 c0                	test   %eax,%eax
  801564:	78 0e                	js     801574 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801566:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801569:	8b 55 0c             	mov    0xc(%ebp),%edx
  80156c:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  80156f:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801574:	c9                   	leave  
  801575:	c3                   	ret    

00801576 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801576:	55                   	push   %ebp
  801577:	89 e5                	mov    %esp,%ebp
  801579:	53                   	push   %ebx
  80157a:	83 ec 14             	sub    $0x14,%esp
  80157d:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801580:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801583:	50                   	push   %eax
  801584:	53                   	push   %ebx
  801585:	e8 0b fc ff ff       	call   801195 <fd_lookup>
  80158a:	83 c4 08             	add    $0x8,%esp
  80158d:	85 c0                	test   %eax,%eax
  80158f:	78 5f                	js     8015f0 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801591:	83 ec 08             	sub    $0x8,%esp
  801594:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801597:	50                   	push   %eax
  801598:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80159b:	ff 30                	pushl  (%eax)
  80159d:	e8 4a fc ff ff       	call   8011ec <dev_lookup>
  8015a2:	83 c4 10             	add    $0x10,%esp
  8015a5:	85 c0                	test   %eax,%eax
  8015a7:	78 47                	js     8015f0 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8015a9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015ac:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8015b0:	75 21                	jne    8015d3 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  8015b2:	a1 20 44 80 00       	mov    0x804420,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  8015b7:	8b 40 48             	mov    0x48(%eax),%eax
  8015ba:	83 ec 04             	sub    $0x4,%esp
  8015bd:	53                   	push   %ebx
  8015be:	50                   	push   %eax
  8015bf:	68 20 28 80 00       	push   $0x802820
  8015c4:	e8 28 ed ff ff       	call   8002f1 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8015c9:	83 c4 10             	add    $0x10,%esp
  8015cc:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8015d1:	eb 1d                	jmp    8015f0 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8015d3:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8015d6:	8b 52 18             	mov    0x18(%edx),%edx
  8015d9:	85 d2                	test   %edx,%edx
  8015db:	74 0e                	je     8015eb <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8015dd:	83 ec 08             	sub    $0x8,%esp
  8015e0:	ff 75 0c             	pushl  0xc(%ebp)
  8015e3:	50                   	push   %eax
  8015e4:	ff d2                	call   *%edx
  8015e6:	83 c4 10             	add    $0x10,%esp
  8015e9:	eb 05                	jmp    8015f0 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8015eb:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8015f0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015f3:	c9                   	leave  
  8015f4:	c3                   	ret    

008015f5 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8015f5:	55                   	push   %ebp
  8015f6:	89 e5                	mov    %esp,%ebp
  8015f8:	53                   	push   %ebx
  8015f9:	83 ec 14             	sub    $0x14,%esp
  8015fc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8015ff:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801602:	50                   	push   %eax
  801603:	ff 75 08             	pushl  0x8(%ebp)
  801606:	e8 8a fb ff ff       	call   801195 <fd_lookup>
  80160b:	83 c4 08             	add    $0x8,%esp
  80160e:	85 c0                	test   %eax,%eax
  801610:	78 52                	js     801664 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801612:	83 ec 08             	sub    $0x8,%esp
  801615:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801618:	50                   	push   %eax
  801619:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80161c:	ff 30                	pushl  (%eax)
  80161e:	e8 c9 fb ff ff       	call   8011ec <dev_lookup>
  801623:	83 c4 10             	add    $0x10,%esp
  801626:	85 c0                	test   %eax,%eax
  801628:	78 3a                	js     801664 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  80162a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80162d:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  801631:	74 2c                	je     80165f <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  801633:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  801636:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  80163d:	00 00 00 
	stat->st_isdir = 0;
  801640:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801647:	00 00 00 
	stat->st_dev = dev;
  80164a:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  801650:	83 ec 08             	sub    $0x8,%esp
  801653:	53                   	push   %ebx
  801654:	ff 75 f0             	pushl  -0x10(%ebp)
  801657:	ff 50 14             	call   *0x14(%eax)
  80165a:	83 c4 10             	add    $0x10,%esp
  80165d:	eb 05                	jmp    801664 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  80165f:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  801664:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801667:	c9                   	leave  
  801668:	c3                   	ret    

00801669 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801669:	55                   	push   %ebp
  80166a:	89 e5                	mov    %esp,%ebp
  80166c:	56                   	push   %esi
  80166d:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  80166e:	83 ec 08             	sub    $0x8,%esp
  801671:	6a 00                	push   $0x0
  801673:	ff 75 08             	pushl  0x8(%ebp)
  801676:	e8 e7 01 00 00       	call   801862 <open>
  80167b:	89 c3                	mov    %eax,%ebx
  80167d:	83 c4 10             	add    $0x10,%esp
  801680:	85 c0                	test   %eax,%eax
  801682:	78 1d                	js     8016a1 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801684:	83 ec 08             	sub    $0x8,%esp
  801687:	ff 75 0c             	pushl  0xc(%ebp)
  80168a:	50                   	push   %eax
  80168b:	e8 65 ff ff ff       	call   8015f5 <fstat>
  801690:	89 c6                	mov    %eax,%esi
	close(fd);
  801692:	89 1c 24             	mov    %ebx,(%esp)
  801695:	e8 29 fc ff ff       	call   8012c3 <close>
	return r;
  80169a:	83 c4 10             	add    $0x10,%esp
  80169d:	89 f0                	mov    %esi,%eax
  80169f:	eb 00                	jmp    8016a1 <stat+0x38>
}
  8016a1:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8016a4:	5b                   	pop    %ebx
  8016a5:	5e                   	pop    %esi
  8016a6:	5d                   	pop    %ebp
  8016a7:	c3                   	ret    

008016a8 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8016a8:	55                   	push   %ebp
  8016a9:	89 e5                	mov    %esp,%ebp
  8016ab:	56                   	push   %esi
  8016ac:	53                   	push   %ebx
  8016ad:	89 c6                	mov    %eax,%esi
  8016af:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8016b1:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  8016b8:	75 12                	jne    8016cc <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  8016ba:	83 ec 0c             	sub    $0xc,%esp
  8016bd:	6a 01                	push   $0x1
  8016bf:	e8 8f 08 00 00       	call   801f53 <ipc_find_env>
  8016c4:	a3 00 40 80 00       	mov    %eax,0x804000
  8016c9:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8016cc:	6a 07                	push   $0x7
  8016ce:	68 00 50 80 00       	push   $0x805000
  8016d3:	56                   	push   %esi
  8016d4:	ff 35 00 40 80 00    	pushl  0x804000
  8016da:	e8 1f 08 00 00       	call   801efe <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8016df:	83 c4 0c             	add    $0xc,%esp
  8016e2:	6a 00                	push   $0x0
  8016e4:	53                   	push   %ebx
  8016e5:	6a 00                	push   $0x0
  8016e7:	e8 aa 07 00 00       	call   801e96 <ipc_recv>
}
  8016ec:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8016ef:	5b                   	pop    %ebx
  8016f0:	5e                   	pop    %esi
  8016f1:	5d                   	pop    %ebp
  8016f2:	c3                   	ret    

008016f3 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8016f3:	55                   	push   %ebp
  8016f4:	89 e5                	mov    %esp,%ebp
  8016f6:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8016f9:	8b 45 08             	mov    0x8(%ebp),%eax
  8016fc:	8b 40 0c             	mov    0xc(%eax),%eax
  8016ff:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  801704:	8b 45 0c             	mov    0xc(%ebp),%eax
  801707:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  80170c:	ba 00 00 00 00       	mov    $0x0,%edx
  801711:	b8 02 00 00 00       	mov    $0x2,%eax
  801716:	e8 8d ff ff ff       	call   8016a8 <fsipc>
}
  80171b:	c9                   	leave  
  80171c:	c3                   	ret    

0080171d <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  80171d:	55                   	push   %ebp
  80171e:	89 e5                	mov    %esp,%ebp
  801720:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  801723:	8b 45 08             	mov    0x8(%ebp),%eax
  801726:	8b 40 0c             	mov    0xc(%eax),%eax
  801729:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  80172e:	ba 00 00 00 00       	mov    $0x0,%edx
  801733:	b8 06 00 00 00       	mov    $0x6,%eax
  801738:	e8 6b ff ff ff       	call   8016a8 <fsipc>
}
  80173d:	c9                   	leave  
  80173e:	c3                   	ret    

0080173f <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  80173f:	55                   	push   %ebp
  801740:	89 e5                	mov    %esp,%ebp
  801742:	53                   	push   %ebx
  801743:	83 ec 04             	sub    $0x4,%esp
  801746:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801749:	8b 45 08             	mov    0x8(%ebp),%eax
  80174c:	8b 40 0c             	mov    0xc(%eax),%eax
  80174f:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  801754:	ba 00 00 00 00       	mov    $0x0,%edx
  801759:	b8 05 00 00 00       	mov    $0x5,%eax
  80175e:	e8 45 ff ff ff       	call   8016a8 <fsipc>
  801763:	85 c0                	test   %eax,%eax
  801765:	78 2c                	js     801793 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801767:	83 ec 08             	sub    $0x8,%esp
  80176a:	68 00 50 80 00       	push   $0x805000
  80176f:	53                   	push   %ebx
  801770:	e8 e0 f0 ff ff       	call   800855 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801775:	a1 80 50 80 00       	mov    0x805080,%eax
  80177a:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801780:	a1 84 50 80 00       	mov    0x805084,%eax
  801785:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  80178b:	83 c4 10             	add    $0x10,%esp
  80178e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801793:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801796:	c9                   	leave  
  801797:	c3                   	ret    

00801798 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801798:	55                   	push   %ebp
  801799:	89 e5                	mov    %esp,%ebp
  80179b:	83 ec 08             	sub    $0x8,%esp
  80179e:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  8017a1:	8b 55 08             	mov    0x8(%ebp),%edx
  8017a4:	8b 52 0c             	mov    0xc(%edx),%edx
  8017a7:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  8017ad:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  8017b2:	76 05                	jbe    8017b9 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  8017b4:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  8017b9:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  8017be:	83 ec 04             	sub    $0x4,%esp
  8017c1:	50                   	push   %eax
  8017c2:	ff 75 0c             	pushl  0xc(%ebp)
  8017c5:	68 08 50 80 00       	push   $0x805008
  8017ca:	e8 fb f1 ff ff       	call   8009ca <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8017cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8017d4:	b8 04 00 00 00       	mov    $0x4,%eax
  8017d9:	e8 ca fe ff ff       	call   8016a8 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8017de:	c9                   	leave  
  8017df:	c3                   	ret    

008017e0 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8017e0:	55                   	push   %ebp
  8017e1:	89 e5                	mov    %esp,%ebp
  8017e3:	56                   	push   %esi
  8017e4:	53                   	push   %ebx
  8017e5:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8017e8:	8b 45 08             	mov    0x8(%ebp),%eax
  8017eb:	8b 40 0c             	mov    0xc(%eax),%eax
  8017ee:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  8017f3:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  8017f9:	ba 00 00 00 00       	mov    $0x0,%edx
  8017fe:	b8 03 00 00 00       	mov    $0x3,%eax
  801803:	e8 a0 fe ff ff       	call   8016a8 <fsipc>
  801808:	89 c3                	mov    %eax,%ebx
  80180a:	85 c0                	test   %eax,%eax
  80180c:	78 4b                	js     801859 <devfile_read+0x79>
		return r;
	assert(r <= n);
  80180e:	39 c6                	cmp    %eax,%esi
  801810:	73 16                	jae    801828 <devfile_read+0x48>
  801812:	68 8c 28 80 00       	push   $0x80288c
  801817:	68 93 28 80 00       	push   $0x802893
  80181c:	6a 7c                	push   $0x7c
  80181e:	68 a8 28 80 00       	push   $0x8028a8
  801823:	e8 f1 e9 ff ff       	call   800219 <_panic>
	assert(r <= PGSIZE);
  801828:	3d 00 10 00 00       	cmp    $0x1000,%eax
  80182d:	7e 16                	jle    801845 <devfile_read+0x65>
  80182f:	68 b3 28 80 00       	push   $0x8028b3
  801834:	68 93 28 80 00       	push   $0x802893
  801839:	6a 7d                	push   $0x7d
  80183b:	68 a8 28 80 00       	push   $0x8028a8
  801840:	e8 d4 e9 ff ff       	call   800219 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801845:	83 ec 04             	sub    $0x4,%esp
  801848:	50                   	push   %eax
  801849:	68 00 50 80 00       	push   $0x805000
  80184e:	ff 75 0c             	pushl  0xc(%ebp)
  801851:	e8 74 f1 ff ff       	call   8009ca <memmove>
	return r;
  801856:	83 c4 10             	add    $0x10,%esp
}
  801859:	89 d8                	mov    %ebx,%eax
  80185b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80185e:	5b                   	pop    %ebx
  80185f:	5e                   	pop    %esi
  801860:	5d                   	pop    %ebp
  801861:	c3                   	ret    

00801862 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801862:	55                   	push   %ebp
  801863:	89 e5                	mov    %esp,%ebp
  801865:	53                   	push   %ebx
  801866:	83 ec 20             	sub    $0x20,%esp
  801869:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  80186c:	53                   	push   %ebx
  80186d:	e8 ae ef ff ff       	call   800820 <strlen>
  801872:	83 c4 10             	add    $0x10,%esp
  801875:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  80187a:	7f 63                	jg     8018df <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  80187c:	83 ec 0c             	sub    $0xc,%esp
  80187f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801882:	50                   	push   %eax
  801883:	e8 be f8 ff ff       	call   801146 <fd_alloc>
  801888:	83 c4 10             	add    $0x10,%esp
  80188b:	85 c0                	test   %eax,%eax
  80188d:	78 55                	js     8018e4 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  80188f:	83 ec 08             	sub    $0x8,%esp
  801892:	53                   	push   %ebx
  801893:	68 00 50 80 00       	push   $0x805000
  801898:	e8 b8 ef ff ff       	call   800855 <strcpy>
	fsipcbuf.open.req_omode = mode;
  80189d:	8b 45 0c             	mov    0xc(%ebp),%eax
  8018a0:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  8018a5:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8018a8:	b8 01 00 00 00       	mov    $0x1,%eax
  8018ad:	e8 f6 fd ff ff       	call   8016a8 <fsipc>
  8018b2:	89 c3                	mov    %eax,%ebx
  8018b4:	83 c4 10             	add    $0x10,%esp
  8018b7:	85 c0                	test   %eax,%eax
  8018b9:	79 14                	jns    8018cf <open+0x6d>
		fd_close(fd, 0);
  8018bb:	83 ec 08             	sub    $0x8,%esp
  8018be:	6a 00                	push   $0x0
  8018c0:	ff 75 f4             	pushl  -0xc(%ebp)
  8018c3:	e8 77 f9 ff ff       	call   80123f <fd_close>
		return r;
  8018c8:	83 c4 10             	add    $0x10,%esp
  8018cb:	89 d8                	mov    %ebx,%eax
  8018cd:	eb 15                	jmp    8018e4 <open+0x82>
	}

	return fd2num(fd);
  8018cf:	83 ec 0c             	sub    $0xc,%esp
  8018d2:	ff 75 f4             	pushl  -0xc(%ebp)
  8018d5:	e8 45 f8 ff ff       	call   80111f <fd2num>
  8018da:	83 c4 10             	add    $0x10,%esp
  8018dd:	eb 05                	jmp    8018e4 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8018df:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8018e4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8018e7:	c9                   	leave  
  8018e8:	c3                   	ret    

008018e9 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8018e9:	55                   	push   %ebp
  8018ea:	89 e5                	mov    %esp,%ebp
  8018ec:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8018ef:	ba 00 00 00 00       	mov    $0x0,%edx
  8018f4:	b8 08 00 00 00       	mov    $0x8,%eax
  8018f9:	e8 aa fd ff ff       	call   8016a8 <fsipc>
}
  8018fe:	c9                   	leave  
  8018ff:	c3                   	ret    

00801900 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801900:	55                   	push   %ebp
  801901:	89 e5                	mov    %esp,%ebp
  801903:	56                   	push   %esi
  801904:	53                   	push   %ebx
  801905:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801908:	83 ec 0c             	sub    $0xc,%esp
  80190b:	ff 75 08             	pushl  0x8(%ebp)
  80190e:	e8 1c f8 ff ff       	call   80112f <fd2data>
  801913:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801915:	83 c4 08             	add    $0x8,%esp
  801918:	68 bf 28 80 00       	push   $0x8028bf
  80191d:	53                   	push   %ebx
  80191e:	e8 32 ef ff ff       	call   800855 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801923:	8b 46 04             	mov    0x4(%esi),%eax
  801926:	2b 06                	sub    (%esi),%eax
  801928:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  80192e:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801935:	00 00 00 
	stat->st_dev = &devpipe;
  801938:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  80193f:	30 80 00 
	return 0;
}
  801942:	b8 00 00 00 00       	mov    $0x0,%eax
  801947:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80194a:	5b                   	pop    %ebx
  80194b:	5e                   	pop    %esi
  80194c:	5d                   	pop    %ebp
  80194d:	c3                   	ret    

0080194e <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  80194e:	55                   	push   %ebp
  80194f:	89 e5                	mov    %esp,%ebp
  801951:	53                   	push   %ebx
  801952:	83 ec 0c             	sub    $0xc,%esp
  801955:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801958:	53                   	push   %ebx
  801959:	6a 00                	push   $0x0
  80195b:	e8 5e f3 ff ff       	call   800cbe <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801960:	89 1c 24             	mov    %ebx,(%esp)
  801963:	e8 c7 f7 ff ff       	call   80112f <fd2data>
  801968:	83 c4 08             	add    $0x8,%esp
  80196b:	50                   	push   %eax
  80196c:	6a 00                	push   $0x0
  80196e:	e8 4b f3 ff ff       	call   800cbe <sys_page_unmap>
}
  801973:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801976:	c9                   	leave  
  801977:	c3                   	ret    

00801978 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801978:	55                   	push   %ebp
  801979:	89 e5                	mov    %esp,%ebp
  80197b:	57                   	push   %edi
  80197c:	56                   	push   %esi
  80197d:	53                   	push   %ebx
  80197e:	83 ec 1c             	sub    $0x1c,%esp
  801981:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801984:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801986:	a1 20 44 80 00       	mov    0x804420,%eax
  80198b:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  80198e:	83 ec 0c             	sub    $0xc,%esp
  801991:	ff 75 e0             	pushl  -0x20(%ebp)
  801994:	e8 fe 05 00 00       	call   801f97 <pageref>
  801999:	89 c3                	mov    %eax,%ebx
  80199b:	89 3c 24             	mov    %edi,(%esp)
  80199e:	e8 f4 05 00 00       	call   801f97 <pageref>
  8019a3:	83 c4 10             	add    $0x10,%esp
  8019a6:	39 c3                	cmp    %eax,%ebx
  8019a8:	0f 94 c1             	sete   %cl
  8019ab:	0f b6 c9             	movzbl %cl,%ecx
  8019ae:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  8019b1:	8b 15 20 44 80 00    	mov    0x804420,%edx
  8019b7:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  8019ba:	39 ce                	cmp    %ecx,%esi
  8019bc:	74 1b                	je     8019d9 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  8019be:	39 c3                	cmp    %eax,%ebx
  8019c0:	75 c4                	jne    801986 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  8019c2:	8b 42 58             	mov    0x58(%edx),%eax
  8019c5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8019c8:	50                   	push   %eax
  8019c9:	56                   	push   %esi
  8019ca:	68 c6 28 80 00       	push   $0x8028c6
  8019cf:	e8 1d e9 ff ff       	call   8002f1 <cprintf>
  8019d4:	83 c4 10             	add    $0x10,%esp
  8019d7:	eb ad                	jmp    801986 <_pipeisclosed+0xe>
	}
}
  8019d9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8019dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8019df:	5b                   	pop    %ebx
  8019e0:	5e                   	pop    %esi
  8019e1:	5f                   	pop    %edi
  8019e2:	5d                   	pop    %ebp
  8019e3:	c3                   	ret    

008019e4 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8019e4:	55                   	push   %ebp
  8019e5:	89 e5                	mov    %esp,%ebp
  8019e7:	57                   	push   %edi
  8019e8:	56                   	push   %esi
  8019e9:	53                   	push   %ebx
  8019ea:	83 ec 18             	sub    $0x18,%esp
  8019ed:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  8019f0:	56                   	push   %esi
  8019f1:	e8 39 f7 ff ff       	call   80112f <fd2data>
  8019f6:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8019f8:	83 c4 10             	add    $0x10,%esp
  8019fb:	bf 00 00 00 00       	mov    $0x0,%edi
  801a00:	eb 3b                	jmp    801a3d <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801a02:	89 da                	mov    %ebx,%edx
  801a04:	89 f0                	mov    %esi,%eax
  801a06:	e8 6d ff ff ff       	call   801978 <_pipeisclosed>
  801a0b:	85 c0                	test   %eax,%eax
  801a0d:	75 38                	jne    801a47 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801a0f:	e8 06 f2 ff ff       	call   800c1a <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801a14:	8b 53 04             	mov    0x4(%ebx),%edx
  801a17:	8b 03                	mov    (%ebx),%eax
  801a19:	83 c0 20             	add    $0x20,%eax
  801a1c:	39 c2                	cmp    %eax,%edx
  801a1e:	73 e2                	jae    801a02 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801a20:	8b 45 0c             	mov    0xc(%ebp),%eax
  801a23:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801a26:	89 d0                	mov    %edx,%eax
  801a28:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801a2d:	79 05                	jns    801a34 <devpipe_write+0x50>
  801a2f:	48                   	dec    %eax
  801a30:	83 c8 e0             	or     $0xffffffe0,%eax
  801a33:	40                   	inc    %eax
  801a34:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801a38:	42                   	inc    %edx
  801a39:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a3c:	47                   	inc    %edi
  801a3d:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801a40:	75 d2                	jne    801a14 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801a42:	8b 45 10             	mov    0x10(%ebp),%eax
  801a45:	eb 05                	jmp    801a4c <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801a47:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801a4c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a4f:	5b                   	pop    %ebx
  801a50:	5e                   	pop    %esi
  801a51:	5f                   	pop    %edi
  801a52:	5d                   	pop    %ebp
  801a53:	c3                   	ret    

00801a54 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801a54:	55                   	push   %ebp
  801a55:	89 e5                	mov    %esp,%ebp
  801a57:	57                   	push   %edi
  801a58:	56                   	push   %esi
  801a59:	53                   	push   %ebx
  801a5a:	83 ec 18             	sub    $0x18,%esp
  801a5d:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801a60:	57                   	push   %edi
  801a61:	e8 c9 f6 ff ff       	call   80112f <fd2data>
  801a66:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a68:	83 c4 10             	add    $0x10,%esp
  801a6b:	bb 00 00 00 00       	mov    $0x0,%ebx
  801a70:	eb 3a                	jmp    801aac <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801a72:	85 db                	test   %ebx,%ebx
  801a74:	74 04                	je     801a7a <devpipe_read+0x26>
				return i;
  801a76:	89 d8                	mov    %ebx,%eax
  801a78:	eb 41                	jmp    801abb <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801a7a:	89 f2                	mov    %esi,%edx
  801a7c:	89 f8                	mov    %edi,%eax
  801a7e:	e8 f5 fe ff ff       	call   801978 <_pipeisclosed>
  801a83:	85 c0                	test   %eax,%eax
  801a85:	75 2f                	jne    801ab6 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801a87:	e8 8e f1 ff ff       	call   800c1a <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801a8c:	8b 06                	mov    (%esi),%eax
  801a8e:	3b 46 04             	cmp    0x4(%esi),%eax
  801a91:	74 df                	je     801a72 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801a93:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801a98:	79 05                	jns    801a9f <devpipe_read+0x4b>
  801a9a:	48                   	dec    %eax
  801a9b:	83 c8 e0             	or     $0xffffffe0,%eax
  801a9e:	40                   	inc    %eax
  801a9f:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801aa3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801aa6:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801aa9:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801aab:	43                   	inc    %ebx
  801aac:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801aaf:	75 db                	jne    801a8c <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801ab1:	8b 45 10             	mov    0x10(%ebp),%eax
  801ab4:	eb 05                	jmp    801abb <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801ab6:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801abb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801abe:	5b                   	pop    %ebx
  801abf:	5e                   	pop    %esi
  801ac0:	5f                   	pop    %edi
  801ac1:	5d                   	pop    %ebp
  801ac2:	c3                   	ret    

00801ac3 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801ac3:	55                   	push   %ebp
  801ac4:	89 e5                	mov    %esp,%ebp
  801ac6:	56                   	push   %esi
  801ac7:	53                   	push   %ebx
  801ac8:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801acb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ace:	50                   	push   %eax
  801acf:	e8 72 f6 ff ff       	call   801146 <fd_alloc>
  801ad4:	83 c4 10             	add    $0x10,%esp
  801ad7:	85 c0                	test   %eax,%eax
  801ad9:	0f 88 2a 01 00 00    	js     801c09 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801adf:	83 ec 04             	sub    $0x4,%esp
  801ae2:	68 07 04 00 00       	push   $0x407
  801ae7:	ff 75 f4             	pushl  -0xc(%ebp)
  801aea:	6a 00                	push   $0x0
  801aec:	e8 48 f1 ff ff       	call   800c39 <sys_page_alloc>
  801af1:	83 c4 10             	add    $0x10,%esp
  801af4:	85 c0                	test   %eax,%eax
  801af6:	0f 88 0d 01 00 00    	js     801c09 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801afc:	83 ec 0c             	sub    $0xc,%esp
  801aff:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801b02:	50                   	push   %eax
  801b03:	e8 3e f6 ff ff       	call   801146 <fd_alloc>
  801b08:	89 c3                	mov    %eax,%ebx
  801b0a:	83 c4 10             	add    $0x10,%esp
  801b0d:	85 c0                	test   %eax,%eax
  801b0f:	0f 88 e2 00 00 00    	js     801bf7 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b15:	83 ec 04             	sub    $0x4,%esp
  801b18:	68 07 04 00 00       	push   $0x407
  801b1d:	ff 75 f0             	pushl  -0x10(%ebp)
  801b20:	6a 00                	push   $0x0
  801b22:	e8 12 f1 ff ff       	call   800c39 <sys_page_alloc>
  801b27:	89 c3                	mov    %eax,%ebx
  801b29:	83 c4 10             	add    $0x10,%esp
  801b2c:	85 c0                	test   %eax,%eax
  801b2e:	0f 88 c3 00 00 00    	js     801bf7 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801b34:	83 ec 0c             	sub    $0xc,%esp
  801b37:	ff 75 f4             	pushl  -0xc(%ebp)
  801b3a:	e8 f0 f5 ff ff       	call   80112f <fd2data>
  801b3f:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b41:	83 c4 0c             	add    $0xc,%esp
  801b44:	68 07 04 00 00       	push   $0x407
  801b49:	50                   	push   %eax
  801b4a:	6a 00                	push   $0x0
  801b4c:	e8 e8 f0 ff ff       	call   800c39 <sys_page_alloc>
  801b51:	89 c3                	mov    %eax,%ebx
  801b53:	83 c4 10             	add    $0x10,%esp
  801b56:	85 c0                	test   %eax,%eax
  801b58:	0f 88 89 00 00 00    	js     801be7 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b5e:	83 ec 0c             	sub    $0xc,%esp
  801b61:	ff 75 f0             	pushl  -0x10(%ebp)
  801b64:	e8 c6 f5 ff ff       	call   80112f <fd2data>
  801b69:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801b70:	50                   	push   %eax
  801b71:	6a 00                	push   $0x0
  801b73:	56                   	push   %esi
  801b74:	6a 00                	push   $0x0
  801b76:	e8 01 f1 ff ff       	call   800c7c <sys_page_map>
  801b7b:	89 c3                	mov    %eax,%ebx
  801b7d:	83 c4 20             	add    $0x20,%esp
  801b80:	85 c0                	test   %eax,%eax
  801b82:	78 55                	js     801bd9 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801b84:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801b8a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b8d:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801b8f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b92:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801b99:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801b9f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801ba2:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801ba4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801ba7:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801bae:	83 ec 0c             	sub    $0xc,%esp
  801bb1:	ff 75 f4             	pushl  -0xc(%ebp)
  801bb4:	e8 66 f5 ff ff       	call   80111f <fd2num>
  801bb9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801bbc:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801bbe:	83 c4 04             	add    $0x4,%esp
  801bc1:	ff 75 f0             	pushl  -0x10(%ebp)
  801bc4:	e8 56 f5 ff ff       	call   80111f <fd2num>
  801bc9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801bcc:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801bcf:	83 c4 10             	add    $0x10,%esp
  801bd2:	b8 00 00 00 00       	mov    $0x0,%eax
  801bd7:	eb 30                	jmp    801c09 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801bd9:	83 ec 08             	sub    $0x8,%esp
  801bdc:	56                   	push   %esi
  801bdd:	6a 00                	push   $0x0
  801bdf:	e8 da f0 ff ff       	call   800cbe <sys_page_unmap>
  801be4:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801be7:	83 ec 08             	sub    $0x8,%esp
  801bea:	ff 75 f0             	pushl  -0x10(%ebp)
  801bed:	6a 00                	push   $0x0
  801bef:	e8 ca f0 ff ff       	call   800cbe <sys_page_unmap>
  801bf4:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801bf7:	83 ec 08             	sub    $0x8,%esp
  801bfa:	ff 75 f4             	pushl  -0xc(%ebp)
  801bfd:	6a 00                	push   $0x0
  801bff:	e8 ba f0 ff ff       	call   800cbe <sys_page_unmap>
  801c04:	83 c4 10             	add    $0x10,%esp
  801c07:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801c09:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c0c:	5b                   	pop    %ebx
  801c0d:	5e                   	pop    %esi
  801c0e:	5d                   	pop    %ebp
  801c0f:	c3                   	ret    

00801c10 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801c10:	55                   	push   %ebp
  801c11:	89 e5                	mov    %esp,%ebp
  801c13:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801c16:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c19:	50                   	push   %eax
  801c1a:	ff 75 08             	pushl  0x8(%ebp)
  801c1d:	e8 73 f5 ff ff       	call   801195 <fd_lookup>
  801c22:	83 c4 10             	add    $0x10,%esp
  801c25:	85 c0                	test   %eax,%eax
  801c27:	78 18                	js     801c41 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801c29:	83 ec 0c             	sub    $0xc,%esp
  801c2c:	ff 75 f4             	pushl  -0xc(%ebp)
  801c2f:	e8 fb f4 ff ff       	call   80112f <fd2data>
	return _pipeisclosed(fd, p);
  801c34:	89 c2                	mov    %eax,%edx
  801c36:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c39:	e8 3a fd ff ff       	call   801978 <_pipeisclosed>
  801c3e:	83 c4 10             	add    $0x10,%esp
}
  801c41:	c9                   	leave  
  801c42:	c3                   	ret    

00801c43 <wait>:
#include <inc/lib.h>

// Waits until 'envid' exits.
void
wait(envid_t envid)
{
  801c43:	55                   	push   %ebp
  801c44:	89 e5                	mov    %esp,%ebp
  801c46:	56                   	push   %esi
  801c47:	53                   	push   %ebx
  801c48:	8b 75 08             	mov    0x8(%ebp),%esi
	const volatile struct Env *e;

	assert(envid != 0);
  801c4b:	85 f6                	test   %esi,%esi
  801c4d:	75 16                	jne    801c65 <wait+0x22>
  801c4f:	68 de 28 80 00       	push   $0x8028de
  801c54:	68 93 28 80 00       	push   $0x802893
  801c59:	6a 09                	push   $0x9
  801c5b:	68 e9 28 80 00       	push   $0x8028e9
  801c60:	e8 b4 e5 ff ff       	call   800219 <_panic>
	e = &envs[ENVX(envid)];
  801c65:	89 f3                	mov    %esi,%ebx
  801c67:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
	while (e->env_id == envid && e->env_status != ENV_FREE)
  801c6d:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  801c74:	c1 e3 07             	shl    $0x7,%ebx
  801c77:	29 c3                	sub    %eax,%ebx
  801c79:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  801c7f:	eb 05                	jmp    801c86 <wait+0x43>
		sys_yield();
  801c81:	e8 94 ef ff ff       	call   800c1a <sys_yield>
{
	const volatile struct Env *e;

	assert(envid != 0);
	e = &envs[ENVX(envid)];
	while (e->env_id == envid && e->env_status != ENV_FREE)
  801c86:	8b 43 48             	mov    0x48(%ebx),%eax
  801c89:	39 c6                	cmp    %eax,%esi
  801c8b:	75 07                	jne    801c94 <wait+0x51>
  801c8d:	8b 43 54             	mov    0x54(%ebx),%eax
  801c90:	85 c0                	test   %eax,%eax
  801c92:	75 ed                	jne    801c81 <wait+0x3e>
		sys_yield();
}
  801c94:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c97:	5b                   	pop    %ebx
  801c98:	5e                   	pop    %esi
  801c99:	5d                   	pop    %ebp
  801c9a:	c3                   	ret    

00801c9b <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801c9b:	55                   	push   %ebp
  801c9c:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801c9e:	b8 00 00 00 00       	mov    $0x0,%eax
  801ca3:	5d                   	pop    %ebp
  801ca4:	c3                   	ret    

00801ca5 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801ca5:	55                   	push   %ebp
  801ca6:	89 e5                	mov    %esp,%ebp
  801ca8:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801cab:	68 f4 28 80 00       	push   $0x8028f4
  801cb0:	ff 75 0c             	pushl  0xc(%ebp)
  801cb3:	e8 9d eb ff ff       	call   800855 <strcpy>
	return 0;
}
  801cb8:	b8 00 00 00 00       	mov    $0x0,%eax
  801cbd:	c9                   	leave  
  801cbe:	c3                   	ret    

00801cbf <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801cbf:	55                   	push   %ebp
  801cc0:	89 e5                	mov    %esp,%ebp
  801cc2:	57                   	push   %edi
  801cc3:	56                   	push   %esi
  801cc4:	53                   	push   %ebx
  801cc5:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801ccb:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801cd0:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801cd6:	eb 2c                	jmp    801d04 <devcons_write+0x45>
		m = n - tot;
  801cd8:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801cdb:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801cdd:	83 fb 7f             	cmp    $0x7f,%ebx
  801ce0:	76 05                	jbe    801ce7 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801ce2:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801ce7:	83 ec 04             	sub    $0x4,%esp
  801cea:	53                   	push   %ebx
  801ceb:	03 45 0c             	add    0xc(%ebp),%eax
  801cee:	50                   	push   %eax
  801cef:	57                   	push   %edi
  801cf0:	e8 d5 ec ff ff       	call   8009ca <memmove>
		sys_cputs(buf, m);
  801cf5:	83 c4 08             	add    $0x8,%esp
  801cf8:	53                   	push   %ebx
  801cf9:	57                   	push   %edi
  801cfa:	e8 7e ee ff ff       	call   800b7d <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801cff:	01 de                	add    %ebx,%esi
  801d01:	83 c4 10             	add    $0x10,%esp
  801d04:	89 f0                	mov    %esi,%eax
  801d06:	3b 75 10             	cmp    0x10(%ebp),%esi
  801d09:	72 cd                	jb     801cd8 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801d0b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d0e:	5b                   	pop    %ebx
  801d0f:	5e                   	pop    %esi
  801d10:	5f                   	pop    %edi
  801d11:	5d                   	pop    %ebp
  801d12:	c3                   	ret    

00801d13 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801d13:	55                   	push   %ebp
  801d14:	89 e5                	mov    %esp,%ebp
  801d16:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801d19:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801d1d:	75 07                	jne    801d26 <devcons_read+0x13>
  801d1f:	eb 23                	jmp    801d44 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801d21:	e8 f4 ee ff ff       	call   800c1a <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801d26:	e8 70 ee ff ff       	call   800b9b <sys_cgetc>
  801d2b:	85 c0                	test   %eax,%eax
  801d2d:	74 f2                	je     801d21 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801d2f:	85 c0                	test   %eax,%eax
  801d31:	78 1d                	js     801d50 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801d33:	83 f8 04             	cmp    $0x4,%eax
  801d36:	74 13                	je     801d4b <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801d38:	8b 55 0c             	mov    0xc(%ebp),%edx
  801d3b:	88 02                	mov    %al,(%edx)
	return 1;
  801d3d:	b8 01 00 00 00       	mov    $0x1,%eax
  801d42:	eb 0c                	jmp    801d50 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801d44:	b8 00 00 00 00       	mov    $0x0,%eax
  801d49:	eb 05                	jmp    801d50 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801d4b:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801d50:	c9                   	leave  
  801d51:	c3                   	ret    

00801d52 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801d52:	55                   	push   %ebp
  801d53:	89 e5                	mov    %esp,%ebp
  801d55:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801d58:	8b 45 08             	mov    0x8(%ebp),%eax
  801d5b:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801d5e:	6a 01                	push   $0x1
  801d60:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801d63:	50                   	push   %eax
  801d64:	e8 14 ee ff ff       	call   800b7d <sys_cputs>
}
  801d69:	83 c4 10             	add    $0x10,%esp
  801d6c:	c9                   	leave  
  801d6d:	c3                   	ret    

00801d6e <getchar>:

int
getchar(void)
{
  801d6e:	55                   	push   %ebp
  801d6f:	89 e5                	mov    %esp,%ebp
  801d71:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801d74:	6a 01                	push   $0x1
  801d76:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801d79:	50                   	push   %eax
  801d7a:	6a 00                	push   $0x0
  801d7c:	e8 7a f6 ff ff       	call   8013fb <read>
	if (r < 0)
  801d81:	83 c4 10             	add    $0x10,%esp
  801d84:	85 c0                	test   %eax,%eax
  801d86:	78 0f                	js     801d97 <getchar+0x29>
		return r;
	if (r < 1)
  801d88:	85 c0                	test   %eax,%eax
  801d8a:	7e 06                	jle    801d92 <getchar+0x24>
		return -E_EOF;
	return c;
  801d8c:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801d90:	eb 05                	jmp    801d97 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801d92:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801d97:	c9                   	leave  
  801d98:	c3                   	ret    

00801d99 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801d99:	55                   	push   %ebp
  801d9a:	89 e5                	mov    %esp,%ebp
  801d9c:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801d9f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801da2:	50                   	push   %eax
  801da3:	ff 75 08             	pushl  0x8(%ebp)
  801da6:	e8 ea f3 ff ff       	call   801195 <fd_lookup>
  801dab:	83 c4 10             	add    $0x10,%esp
  801dae:	85 c0                	test   %eax,%eax
  801db0:	78 11                	js     801dc3 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801db2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801db5:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801dbb:	39 10                	cmp    %edx,(%eax)
  801dbd:	0f 94 c0             	sete   %al
  801dc0:	0f b6 c0             	movzbl %al,%eax
}
  801dc3:	c9                   	leave  
  801dc4:	c3                   	ret    

00801dc5 <opencons>:

int
opencons(void)
{
  801dc5:	55                   	push   %ebp
  801dc6:	89 e5                	mov    %esp,%ebp
  801dc8:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801dcb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801dce:	50                   	push   %eax
  801dcf:	e8 72 f3 ff ff       	call   801146 <fd_alloc>
  801dd4:	83 c4 10             	add    $0x10,%esp
  801dd7:	85 c0                	test   %eax,%eax
  801dd9:	78 3a                	js     801e15 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801ddb:	83 ec 04             	sub    $0x4,%esp
  801dde:	68 07 04 00 00       	push   $0x407
  801de3:	ff 75 f4             	pushl  -0xc(%ebp)
  801de6:	6a 00                	push   $0x0
  801de8:	e8 4c ee ff ff       	call   800c39 <sys_page_alloc>
  801ded:	83 c4 10             	add    $0x10,%esp
  801df0:	85 c0                	test   %eax,%eax
  801df2:	78 21                	js     801e15 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801df4:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801dfa:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801dfd:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801dff:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e02:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801e09:	83 ec 0c             	sub    $0xc,%esp
  801e0c:	50                   	push   %eax
  801e0d:	e8 0d f3 ff ff       	call   80111f <fd2num>
  801e12:	83 c4 10             	add    $0x10,%esp
}
  801e15:	c9                   	leave  
  801e16:	c3                   	ret    

00801e17 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801e17:	55                   	push   %ebp
  801e18:	89 e5                	mov    %esp,%ebp
  801e1a:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801e1d:	83 3d 00 60 80 00 00 	cmpl   $0x0,0x806000
  801e24:	75 3e                	jne    801e64 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801e26:	83 ec 04             	sub    $0x4,%esp
  801e29:	6a 07                	push   $0x7
  801e2b:	68 00 f0 bf ee       	push   $0xeebff000
  801e30:	6a 00                	push   $0x0
  801e32:	e8 02 ee ff ff       	call   800c39 <sys_page_alloc>
  801e37:	83 c4 10             	add    $0x10,%esp
  801e3a:	85 c0                	test   %eax,%eax
  801e3c:	74 14                	je     801e52 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801e3e:	83 ec 04             	sub    $0x4,%esp
  801e41:	68 00 29 80 00       	push   $0x802900
  801e46:	6a 23                	push   $0x23
  801e48:	68 24 29 80 00       	push   $0x802924
  801e4d:	e8 c7 e3 ff ff       	call   800219 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801e52:	83 ec 08             	sub    $0x8,%esp
  801e55:	68 6e 1e 80 00       	push   $0x801e6e
  801e5a:	6a 00                	push   $0x0
  801e5c:	e8 42 ef ff ff       	call   800da3 <sys_env_set_pgfault_upcall>
  801e61:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801e64:	8b 45 08             	mov    0x8(%ebp),%eax
  801e67:	a3 00 60 80 00       	mov    %eax,0x806000
}
  801e6c:	c9                   	leave  
  801e6d:	c3                   	ret    

00801e6e <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801e6e:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801e6f:	a1 00 60 80 00       	mov    0x806000,%eax
	call *%eax
  801e74:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801e76:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801e79:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801e7b:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801e7f:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801e83:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801e84:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801e87:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801e89:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801e8c:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801e8f:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801e90:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  801e93:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801e94:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801e95:	c3                   	ret    

00801e96 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801e96:	55                   	push   %ebp
  801e97:	89 e5                	mov    %esp,%ebp
  801e99:	56                   	push   %esi
  801e9a:	53                   	push   %ebx
  801e9b:	8b 75 08             	mov    0x8(%ebp),%esi
  801e9e:	8b 45 0c             	mov    0xc(%ebp),%eax
  801ea1:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801ea4:	85 c0                	test   %eax,%eax
  801ea6:	75 05                	jne    801ead <ipc_recv+0x17>
        pg = (void *)UTOP;
  801ea8:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801ead:	83 ec 0c             	sub    $0xc,%esp
  801eb0:	50                   	push   %eax
  801eb1:	e8 52 ef ff ff       	call   800e08 <sys_ipc_recv>
  801eb6:	83 c4 10             	add    $0x10,%esp
  801eb9:	85 c0                	test   %eax,%eax
  801ebb:	74 16                	je     801ed3 <ipc_recv+0x3d>
        if (from_env_store)
  801ebd:	85 f6                	test   %esi,%esi
  801ebf:	74 06                	je     801ec7 <ipc_recv+0x31>
            *from_env_store = 0;
  801ec1:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801ec7:	85 db                	test   %ebx,%ebx
  801ec9:	74 2c                	je     801ef7 <ipc_recv+0x61>
            *perm_store = 0;
  801ecb:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801ed1:	eb 24                	jmp    801ef7 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801ed3:	85 f6                	test   %esi,%esi
  801ed5:	74 0a                	je     801ee1 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801ed7:	a1 20 44 80 00       	mov    0x804420,%eax
  801edc:	8b 40 74             	mov    0x74(%eax),%eax
  801edf:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801ee1:	85 db                	test   %ebx,%ebx
  801ee3:	74 0a                	je     801eef <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801ee5:	a1 20 44 80 00       	mov    0x804420,%eax
  801eea:	8b 40 78             	mov    0x78(%eax),%eax
  801eed:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801eef:	a1 20 44 80 00       	mov    0x804420,%eax
  801ef4:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801ef7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801efa:	5b                   	pop    %ebx
  801efb:	5e                   	pop    %esi
  801efc:	5d                   	pop    %ebp
  801efd:	c3                   	ret    

00801efe <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801efe:	55                   	push   %ebp
  801eff:	89 e5                	mov    %esp,%ebp
  801f01:	57                   	push   %edi
  801f02:	56                   	push   %esi
  801f03:	53                   	push   %ebx
  801f04:	83 ec 0c             	sub    $0xc,%esp
  801f07:	8b 75 0c             	mov    0xc(%ebp),%esi
  801f0a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801f0d:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801f10:	85 db                	test   %ebx,%ebx
  801f12:	75 0c                	jne    801f20 <ipc_send+0x22>
        pg = (void *)UTOP;
  801f14:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801f19:	eb 05                	jmp    801f20 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801f1b:	e8 fa ec ff ff       	call   800c1a <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801f20:	57                   	push   %edi
  801f21:	53                   	push   %ebx
  801f22:	56                   	push   %esi
  801f23:	ff 75 08             	pushl  0x8(%ebp)
  801f26:	e8 ba ee ff ff       	call   800de5 <sys_ipc_try_send>
  801f2b:	83 c4 10             	add    $0x10,%esp
  801f2e:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801f31:	74 e8                	je     801f1b <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801f33:	85 c0                	test   %eax,%eax
  801f35:	74 14                	je     801f4b <ipc_send+0x4d>
        panic("ipc_send: error");
  801f37:	83 ec 04             	sub    $0x4,%esp
  801f3a:	68 32 29 80 00       	push   $0x802932
  801f3f:	6a 6a                	push   $0x6a
  801f41:	68 42 29 80 00       	push   $0x802942
  801f46:	e8 ce e2 ff ff       	call   800219 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801f4b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f4e:	5b                   	pop    %ebx
  801f4f:	5e                   	pop    %esi
  801f50:	5f                   	pop    %edi
  801f51:	5d                   	pop    %ebp
  801f52:	c3                   	ret    

00801f53 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801f53:	55                   	push   %ebp
  801f54:	89 e5                	mov    %esp,%ebp
  801f56:	53                   	push   %ebx
  801f57:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801f5a:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801f5f:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801f66:	89 d0                	mov    %edx,%eax
  801f68:	c1 e0 07             	shl    $0x7,%eax
  801f6b:	29 d8                	sub    %ebx,%eax
  801f6d:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801f72:	8b 40 50             	mov    0x50(%eax),%eax
  801f75:	39 c8                	cmp    %ecx,%eax
  801f77:	75 0d                	jne    801f86 <ipc_find_env+0x33>
			return envs[i].env_id;
  801f79:	c1 e2 07             	shl    $0x7,%edx
  801f7c:	29 da                	sub    %ebx,%edx
  801f7e:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801f84:	eb 0e                	jmp    801f94 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801f86:	42                   	inc    %edx
  801f87:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801f8d:	75 d0                	jne    801f5f <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801f8f:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801f94:	5b                   	pop    %ebx
  801f95:	5d                   	pop    %ebp
  801f96:	c3                   	ret    

00801f97 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801f97:	55                   	push   %ebp
  801f98:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801f9a:	8b 45 08             	mov    0x8(%ebp),%eax
  801f9d:	c1 e8 16             	shr    $0x16,%eax
  801fa0:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801fa7:	a8 01                	test   $0x1,%al
  801fa9:	74 21                	je     801fcc <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801fab:	8b 45 08             	mov    0x8(%ebp),%eax
  801fae:	c1 e8 0c             	shr    $0xc,%eax
  801fb1:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801fb8:	a8 01                	test   $0x1,%al
  801fba:	74 17                	je     801fd3 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801fbc:	c1 e8 0c             	shr    $0xc,%eax
  801fbf:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801fc6:	ef 
  801fc7:	0f b7 c0             	movzwl %ax,%eax
  801fca:	eb 0c                	jmp    801fd8 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801fcc:	b8 00 00 00 00       	mov    $0x0,%eax
  801fd1:	eb 05                	jmp    801fd8 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801fd3:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801fd8:	5d                   	pop    %ebp
  801fd9:	c3                   	ret    
  801fda:	66 90                	xchg   %ax,%ax

00801fdc <__udivdi3>:
  801fdc:	55                   	push   %ebp
  801fdd:	57                   	push   %edi
  801fde:	56                   	push   %esi
  801fdf:	53                   	push   %ebx
  801fe0:	83 ec 1c             	sub    $0x1c,%esp
  801fe3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801fe7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801feb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801fef:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801ff3:	89 ca                	mov    %ecx,%edx
  801ff5:	89 f8                	mov    %edi,%eax
  801ff7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801ffb:	85 f6                	test   %esi,%esi
  801ffd:	75 2d                	jne    80202c <__udivdi3+0x50>
  801fff:	39 cf                	cmp    %ecx,%edi
  802001:	77 65                	ja     802068 <__udivdi3+0x8c>
  802003:	89 fd                	mov    %edi,%ebp
  802005:	85 ff                	test   %edi,%edi
  802007:	75 0b                	jne    802014 <__udivdi3+0x38>
  802009:	b8 01 00 00 00       	mov    $0x1,%eax
  80200e:	31 d2                	xor    %edx,%edx
  802010:	f7 f7                	div    %edi
  802012:	89 c5                	mov    %eax,%ebp
  802014:	31 d2                	xor    %edx,%edx
  802016:	89 c8                	mov    %ecx,%eax
  802018:	f7 f5                	div    %ebp
  80201a:	89 c1                	mov    %eax,%ecx
  80201c:	89 d8                	mov    %ebx,%eax
  80201e:	f7 f5                	div    %ebp
  802020:	89 cf                	mov    %ecx,%edi
  802022:	89 fa                	mov    %edi,%edx
  802024:	83 c4 1c             	add    $0x1c,%esp
  802027:	5b                   	pop    %ebx
  802028:	5e                   	pop    %esi
  802029:	5f                   	pop    %edi
  80202a:	5d                   	pop    %ebp
  80202b:	c3                   	ret    
  80202c:	39 ce                	cmp    %ecx,%esi
  80202e:	77 28                	ja     802058 <__udivdi3+0x7c>
  802030:	0f bd fe             	bsr    %esi,%edi
  802033:	83 f7 1f             	xor    $0x1f,%edi
  802036:	75 40                	jne    802078 <__udivdi3+0x9c>
  802038:	39 ce                	cmp    %ecx,%esi
  80203a:	72 0a                	jb     802046 <__udivdi3+0x6a>
  80203c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  802040:	0f 87 9e 00 00 00    	ja     8020e4 <__udivdi3+0x108>
  802046:	b8 01 00 00 00       	mov    $0x1,%eax
  80204b:	89 fa                	mov    %edi,%edx
  80204d:	83 c4 1c             	add    $0x1c,%esp
  802050:	5b                   	pop    %ebx
  802051:	5e                   	pop    %esi
  802052:	5f                   	pop    %edi
  802053:	5d                   	pop    %ebp
  802054:	c3                   	ret    
  802055:	8d 76 00             	lea    0x0(%esi),%esi
  802058:	31 ff                	xor    %edi,%edi
  80205a:	31 c0                	xor    %eax,%eax
  80205c:	89 fa                	mov    %edi,%edx
  80205e:	83 c4 1c             	add    $0x1c,%esp
  802061:	5b                   	pop    %ebx
  802062:	5e                   	pop    %esi
  802063:	5f                   	pop    %edi
  802064:	5d                   	pop    %ebp
  802065:	c3                   	ret    
  802066:	66 90                	xchg   %ax,%ax
  802068:	89 d8                	mov    %ebx,%eax
  80206a:	f7 f7                	div    %edi
  80206c:	31 ff                	xor    %edi,%edi
  80206e:	89 fa                	mov    %edi,%edx
  802070:	83 c4 1c             	add    $0x1c,%esp
  802073:	5b                   	pop    %ebx
  802074:	5e                   	pop    %esi
  802075:	5f                   	pop    %edi
  802076:	5d                   	pop    %ebp
  802077:	c3                   	ret    
  802078:	bd 20 00 00 00       	mov    $0x20,%ebp
  80207d:	89 eb                	mov    %ebp,%ebx
  80207f:	29 fb                	sub    %edi,%ebx
  802081:	89 f9                	mov    %edi,%ecx
  802083:	d3 e6                	shl    %cl,%esi
  802085:	89 c5                	mov    %eax,%ebp
  802087:	88 d9                	mov    %bl,%cl
  802089:	d3 ed                	shr    %cl,%ebp
  80208b:	89 e9                	mov    %ebp,%ecx
  80208d:	09 f1                	or     %esi,%ecx
  80208f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  802093:	89 f9                	mov    %edi,%ecx
  802095:	d3 e0                	shl    %cl,%eax
  802097:	89 c5                	mov    %eax,%ebp
  802099:	89 d6                	mov    %edx,%esi
  80209b:	88 d9                	mov    %bl,%cl
  80209d:	d3 ee                	shr    %cl,%esi
  80209f:	89 f9                	mov    %edi,%ecx
  8020a1:	d3 e2                	shl    %cl,%edx
  8020a3:	8b 44 24 08          	mov    0x8(%esp),%eax
  8020a7:	88 d9                	mov    %bl,%cl
  8020a9:	d3 e8                	shr    %cl,%eax
  8020ab:	09 c2                	or     %eax,%edx
  8020ad:	89 d0                	mov    %edx,%eax
  8020af:	89 f2                	mov    %esi,%edx
  8020b1:	f7 74 24 0c          	divl   0xc(%esp)
  8020b5:	89 d6                	mov    %edx,%esi
  8020b7:	89 c3                	mov    %eax,%ebx
  8020b9:	f7 e5                	mul    %ebp
  8020bb:	39 d6                	cmp    %edx,%esi
  8020bd:	72 19                	jb     8020d8 <__udivdi3+0xfc>
  8020bf:	74 0b                	je     8020cc <__udivdi3+0xf0>
  8020c1:	89 d8                	mov    %ebx,%eax
  8020c3:	31 ff                	xor    %edi,%edi
  8020c5:	e9 58 ff ff ff       	jmp    802022 <__udivdi3+0x46>
  8020ca:	66 90                	xchg   %ax,%ax
  8020cc:	8b 54 24 08          	mov    0x8(%esp),%edx
  8020d0:	89 f9                	mov    %edi,%ecx
  8020d2:	d3 e2                	shl    %cl,%edx
  8020d4:	39 c2                	cmp    %eax,%edx
  8020d6:	73 e9                	jae    8020c1 <__udivdi3+0xe5>
  8020d8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8020db:	31 ff                	xor    %edi,%edi
  8020dd:	e9 40 ff ff ff       	jmp    802022 <__udivdi3+0x46>
  8020e2:	66 90                	xchg   %ax,%ax
  8020e4:	31 c0                	xor    %eax,%eax
  8020e6:	e9 37 ff ff ff       	jmp    802022 <__udivdi3+0x46>
  8020eb:	90                   	nop

008020ec <__umoddi3>:
  8020ec:	55                   	push   %ebp
  8020ed:	57                   	push   %edi
  8020ee:	56                   	push   %esi
  8020ef:	53                   	push   %ebx
  8020f0:	83 ec 1c             	sub    $0x1c,%esp
  8020f3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8020f7:	8b 74 24 34          	mov    0x34(%esp),%esi
  8020fb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8020ff:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  802103:	89 44 24 0c          	mov    %eax,0xc(%esp)
  802107:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80210b:	89 f3                	mov    %esi,%ebx
  80210d:	89 fa                	mov    %edi,%edx
  80210f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  802113:	89 34 24             	mov    %esi,(%esp)
  802116:	85 c0                	test   %eax,%eax
  802118:	75 1a                	jne    802134 <__umoddi3+0x48>
  80211a:	39 f7                	cmp    %esi,%edi
  80211c:	0f 86 a2 00 00 00    	jbe    8021c4 <__umoddi3+0xd8>
  802122:	89 c8                	mov    %ecx,%eax
  802124:	89 f2                	mov    %esi,%edx
  802126:	f7 f7                	div    %edi
  802128:	89 d0                	mov    %edx,%eax
  80212a:	31 d2                	xor    %edx,%edx
  80212c:	83 c4 1c             	add    $0x1c,%esp
  80212f:	5b                   	pop    %ebx
  802130:	5e                   	pop    %esi
  802131:	5f                   	pop    %edi
  802132:	5d                   	pop    %ebp
  802133:	c3                   	ret    
  802134:	39 f0                	cmp    %esi,%eax
  802136:	0f 87 ac 00 00 00    	ja     8021e8 <__umoddi3+0xfc>
  80213c:	0f bd e8             	bsr    %eax,%ebp
  80213f:	83 f5 1f             	xor    $0x1f,%ebp
  802142:	0f 84 ac 00 00 00    	je     8021f4 <__umoddi3+0x108>
  802148:	bf 20 00 00 00       	mov    $0x20,%edi
  80214d:	29 ef                	sub    %ebp,%edi
  80214f:	89 fe                	mov    %edi,%esi
  802151:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802155:	89 e9                	mov    %ebp,%ecx
  802157:	d3 e0                	shl    %cl,%eax
  802159:	89 d7                	mov    %edx,%edi
  80215b:	89 f1                	mov    %esi,%ecx
  80215d:	d3 ef                	shr    %cl,%edi
  80215f:	09 c7                	or     %eax,%edi
  802161:	89 e9                	mov    %ebp,%ecx
  802163:	d3 e2                	shl    %cl,%edx
  802165:	89 14 24             	mov    %edx,(%esp)
  802168:	89 d8                	mov    %ebx,%eax
  80216a:	d3 e0                	shl    %cl,%eax
  80216c:	89 c2                	mov    %eax,%edx
  80216e:	8b 44 24 08          	mov    0x8(%esp),%eax
  802172:	d3 e0                	shl    %cl,%eax
  802174:	89 44 24 04          	mov    %eax,0x4(%esp)
  802178:	8b 44 24 08          	mov    0x8(%esp),%eax
  80217c:	89 f1                	mov    %esi,%ecx
  80217e:	d3 e8                	shr    %cl,%eax
  802180:	09 d0                	or     %edx,%eax
  802182:	d3 eb                	shr    %cl,%ebx
  802184:	89 da                	mov    %ebx,%edx
  802186:	f7 f7                	div    %edi
  802188:	89 d3                	mov    %edx,%ebx
  80218a:	f7 24 24             	mull   (%esp)
  80218d:	89 c6                	mov    %eax,%esi
  80218f:	89 d1                	mov    %edx,%ecx
  802191:	39 d3                	cmp    %edx,%ebx
  802193:	0f 82 87 00 00 00    	jb     802220 <__umoddi3+0x134>
  802199:	0f 84 91 00 00 00    	je     802230 <__umoddi3+0x144>
  80219f:	8b 54 24 04          	mov    0x4(%esp),%edx
  8021a3:	29 f2                	sub    %esi,%edx
  8021a5:	19 cb                	sbb    %ecx,%ebx
  8021a7:	89 d8                	mov    %ebx,%eax
  8021a9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8021ad:	d3 e0                	shl    %cl,%eax
  8021af:	89 e9                	mov    %ebp,%ecx
  8021b1:	d3 ea                	shr    %cl,%edx
  8021b3:	09 d0                	or     %edx,%eax
  8021b5:	89 e9                	mov    %ebp,%ecx
  8021b7:	d3 eb                	shr    %cl,%ebx
  8021b9:	89 da                	mov    %ebx,%edx
  8021bb:	83 c4 1c             	add    $0x1c,%esp
  8021be:	5b                   	pop    %ebx
  8021bf:	5e                   	pop    %esi
  8021c0:	5f                   	pop    %edi
  8021c1:	5d                   	pop    %ebp
  8021c2:	c3                   	ret    
  8021c3:	90                   	nop
  8021c4:	89 fd                	mov    %edi,%ebp
  8021c6:	85 ff                	test   %edi,%edi
  8021c8:	75 0b                	jne    8021d5 <__umoddi3+0xe9>
  8021ca:	b8 01 00 00 00       	mov    $0x1,%eax
  8021cf:	31 d2                	xor    %edx,%edx
  8021d1:	f7 f7                	div    %edi
  8021d3:	89 c5                	mov    %eax,%ebp
  8021d5:	89 f0                	mov    %esi,%eax
  8021d7:	31 d2                	xor    %edx,%edx
  8021d9:	f7 f5                	div    %ebp
  8021db:	89 c8                	mov    %ecx,%eax
  8021dd:	f7 f5                	div    %ebp
  8021df:	89 d0                	mov    %edx,%eax
  8021e1:	e9 44 ff ff ff       	jmp    80212a <__umoddi3+0x3e>
  8021e6:	66 90                	xchg   %ax,%ax
  8021e8:	89 c8                	mov    %ecx,%eax
  8021ea:	89 f2                	mov    %esi,%edx
  8021ec:	83 c4 1c             	add    $0x1c,%esp
  8021ef:	5b                   	pop    %ebx
  8021f0:	5e                   	pop    %esi
  8021f1:	5f                   	pop    %edi
  8021f2:	5d                   	pop    %ebp
  8021f3:	c3                   	ret    
  8021f4:	3b 04 24             	cmp    (%esp),%eax
  8021f7:	72 06                	jb     8021ff <__umoddi3+0x113>
  8021f9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8021fd:	77 0f                	ja     80220e <__umoddi3+0x122>
  8021ff:	89 f2                	mov    %esi,%edx
  802201:	29 f9                	sub    %edi,%ecx
  802203:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  802207:	89 14 24             	mov    %edx,(%esp)
  80220a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80220e:	8b 44 24 04          	mov    0x4(%esp),%eax
  802212:	8b 14 24             	mov    (%esp),%edx
  802215:	83 c4 1c             	add    $0x1c,%esp
  802218:	5b                   	pop    %ebx
  802219:	5e                   	pop    %esi
  80221a:	5f                   	pop    %edi
  80221b:	5d                   	pop    %ebp
  80221c:	c3                   	ret    
  80221d:	8d 76 00             	lea    0x0(%esi),%esi
  802220:	2b 04 24             	sub    (%esp),%eax
  802223:	19 fa                	sbb    %edi,%edx
  802225:	89 d1                	mov    %edx,%ecx
  802227:	89 c6                	mov    %eax,%esi
  802229:	e9 71 ff ff ff       	jmp    80219f <__umoddi3+0xb3>
  80222e:	66 90                	xchg   %ax,%ax
  802230:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802234:	72 ea                	jb     802220 <__umoddi3+0x134>
  802236:	89 d9                	mov    %ebx,%ecx
  802238:	e9 62 ff ff ff       	jmp    80219f <__umoddi3+0xb3>
