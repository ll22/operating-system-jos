
obj/user/testpiperace2.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 95 01 00 00       	call   8001c6 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 38             	sub    $0x38,%esp
	int p[2], r, i;
	struct Fd *fd;
	const volatile struct Env *kid;

	cprintf("testing for pipeisclosed race...\n");
  80003c:	68 00 22 80 00       	push   $0x802200
  800041:	e8 b9 02 00 00       	call   8002ff <cprintf>
	if ((r = pipe(p)) < 0)
  800046:	8d 45 e0             	lea    -0x20(%ebp),%eax
  800049:	89 04 24             	mov    %eax,(%esp)
  80004c:	e8 80 1a 00 00       	call   801ad1 <pipe>
  800051:	83 c4 10             	add    $0x10,%esp
  800054:	85 c0                	test   %eax,%eax
  800056:	79 12                	jns    80006a <umain+0x37>
		panic("pipe: %e", r);
  800058:	50                   	push   %eax
  800059:	68 4e 22 80 00       	push   $0x80224e
  80005e:	6a 0d                	push   $0xd
  800060:	68 57 22 80 00       	push   $0x802257
  800065:	e8 bd 01 00 00       	call   800227 <_panic>
	if ((r = fork()) < 0)
  80006a:	e8 9a 0e 00 00       	call   800f09 <fork>
  80006f:	89 c6                	mov    %eax,%esi
  800071:	85 c0                	test   %eax,%eax
  800073:	79 12                	jns    800087 <umain+0x54>
		panic("fork: %e", r);
  800075:	50                   	push   %eax
  800076:	68 6c 22 80 00       	push   $0x80226c
  80007b:	6a 0f                	push   $0xf
  80007d:	68 57 22 80 00       	push   $0x802257
  800082:	e8 a0 01 00 00       	call   800227 <_panic>
	if (r == 0) {
  800087:	85 c0                	test   %eax,%eax
  800089:	75 66                	jne    8000f1 <umain+0xbe>
		// child just dups and closes repeatedly,
		// yielding so the parent can see
		// the fd state between the two.
		close(p[1]);
  80008b:	83 ec 0c             	sub    $0xc,%esp
  80008e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800091:	e8 3b 12 00 00       	call   8012d1 <close>
  800096:	83 c4 10             	add    $0x10,%esp
		for (i = 0; i < 200; i++) {
  800099:	bb 00 00 00 00       	mov    $0x0,%ebx
			if (i % 10 == 0)
  80009e:	bf 0a 00 00 00       	mov    $0xa,%edi
  8000a3:	89 d8                	mov    %ebx,%eax
  8000a5:	99                   	cltd   
  8000a6:	f7 ff                	idiv   %edi
  8000a8:	85 d2                	test   %edx,%edx
  8000aa:	75 11                	jne    8000bd <umain+0x8a>
				cprintf("%d.", i);
  8000ac:	83 ec 08             	sub    $0x8,%esp
  8000af:	53                   	push   %ebx
  8000b0:	68 75 22 80 00       	push   $0x802275
  8000b5:	e8 45 02 00 00       	call   8002ff <cprintf>
  8000ba:	83 c4 10             	add    $0x10,%esp
			// dup, then close.  yield so that other guy will
			// see us while we're between them.
			dup(p[0], 10);
  8000bd:	83 ec 08             	sub    $0x8,%esp
  8000c0:	6a 0a                	push   $0xa
  8000c2:	ff 75 e0             	pushl  -0x20(%ebp)
  8000c5:	e8 55 12 00 00       	call   80131f <dup>
			sys_yield();
  8000ca:	e8 59 0b 00 00       	call   800c28 <sys_yield>
			close(10);
  8000cf:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  8000d6:	e8 f6 11 00 00       	call   8012d1 <close>
			sys_yield();
  8000db:	e8 48 0b 00 00       	call   800c28 <sys_yield>
	if (r == 0) {
		// child just dups and closes repeatedly,
		// yielding so the parent can see
		// the fd state between the two.
		close(p[1]);
		for (i = 0; i < 200; i++) {
  8000e0:	43                   	inc    %ebx
  8000e1:	83 c4 10             	add    $0x10,%esp
  8000e4:	81 fb c8 00 00 00    	cmp    $0xc8,%ebx
  8000ea:	75 b7                	jne    8000a3 <umain+0x70>
			dup(p[0], 10);
			sys_yield();
			close(10);
			sys_yield();
		}
		exit();
  8000ec:	e8 24 01 00 00       	call   800215 <exit>
	// pageref(p[0]) and gets 3, then it will return true when
	// it shouldn't.
	//
	// So either way, pipeisclosed is going give a wrong answer.
	//
	kid = &envs[ENVX(r)];
  8000f1:	89 f0                	mov    %esi,%eax
  8000f3:	25 ff 03 00 00       	and    $0x3ff,%eax
	while (kid->env_status == ENV_RUNNABLE)
  8000f8:	8d 3c 85 00 00 00 00 	lea    0x0(,%eax,4),%edi
  8000ff:	c1 e0 07             	shl    $0x7,%eax
  800102:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  800105:	eb 2f                	jmp    800136 <umain+0x103>
		if (pipeisclosed(p[0]) != 0) {
  800107:	83 ec 0c             	sub    $0xc,%esp
  80010a:	ff 75 e0             	pushl  -0x20(%ebp)
  80010d:	e8 0c 1b 00 00       	call   801c1e <pipeisclosed>
  800112:	83 c4 10             	add    $0x10,%esp
  800115:	85 c0                	test   %eax,%eax
  800117:	74 28                	je     800141 <umain+0x10e>
			cprintf("\nRACE: pipe appears closed\n");
  800119:	83 ec 0c             	sub    $0xc,%esp
  80011c:	68 79 22 80 00       	push   $0x802279
  800121:	e8 d9 01 00 00       	call   8002ff <cprintf>
			sys_env_destroy(r);
  800126:	89 34 24             	mov    %esi,(%esp)
  800129:	e8 9a 0a 00 00       	call   800bc8 <sys_env_destroy>
			exit();
  80012e:	e8 e2 00 00 00       	call   800215 <exit>
  800133:	83 c4 10             	add    $0x10,%esp
	// it shouldn't.
	//
	// So either way, pipeisclosed is going give a wrong answer.
	//
	kid = &envs[ENVX(r)];
	while (kid->env_status == ENV_RUNNABLE)
  800136:	8b 5d d4             	mov    -0x2c(%ebp),%ebx
  800139:	29 fb                	sub    %edi,%ebx
  80013b:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  800141:	8b 43 54             	mov    0x54(%ebx),%eax
  800144:	83 f8 02             	cmp    $0x2,%eax
  800147:	74 be                	je     800107 <umain+0xd4>
		if (pipeisclosed(p[0]) != 0) {
			cprintf("\nRACE: pipe appears closed\n");
			sys_env_destroy(r);
			exit();
		}
	cprintf("child done with loop\n");
  800149:	83 ec 0c             	sub    $0xc,%esp
  80014c:	68 95 22 80 00       	push   $0x802295
  800151:	e8 a9 01 00 00       	call   8002ff <cprintf>
	if (pipeisclosed(p[0]))
  800156:	83 c4 04             	add    $0x4,%esp
  800159:	ff 75 e0             	pushl  -0x20(%ebp)
  80015c:	e8 bd 1a 00 00       	call   801c1e <pipeisclosed>
  800161:	83 c4 10             	add    $0x10,%esp
  800164:	85 c0                	test   %eax,%eax
  800166:	74 14                	je     80017c <umain+0x149>
		panic("somehow the other end of p[0] got closed!");
  800168:	83 ec 04             	sub    $0x4,%esp
  80016b:	68 24 22 80 00       	push   $0x802224
  800170:	6a 40                	push   $0x40
  800172:	68 57 22 80 00       	push   $0x802257
  800177:	e8 ab 00 00 00       	call   800227 <_panic>
	if ((r = fd_lookup(p[0], &fd)) < 0)
  80017c:	83 ec 08             	sub    $0x8,%esp
  80017f:	8d 45 dc             	lea    -0x24(%ebp),%eax
  800182:	50                   	push   %eax
  800183:	ff 75 e0             	pushl  -0x20(%ebp)
  800186:	e8 18 10 00 00       	call   8011a3 <fd_lookup>
  80018b:	83 c4 10             	add    $0x10,%esp
  80018e:	85 c0                	test   %eax,%eax
  800190:	79 12                	jns    8001a4 <umain+0x171>
		panic("cannot look up p[0]: %e", r);
  800192:	50                   	push   %eax
  800193:	68 ab 22 80 00       	push   $0x8022ab
  800198:	6a 42                	push   $0x42
  80019a:	68 57 22 80 00       	push   $0x802257
  80019f:	e8 83 00 00 00       	call   800227 <_panic>
	(void) fd2data(fd);
  8001a4:	83 ec 0c             	sub    $0xc,%esp
  8001a7:	ff 75 dc             	pushl  -0x24(%ebp)
  8001aa:	e8 8e 0f 00 00       	call   80113d <fd2data>
	cprintf("race didn't happen\n");
  8001af:	c7 04 24 c3 22 80 00 	movl   $0x8022c3,(%esp)
  8001b6:	e8 44 01 00 00       	call   8002ff <cprintf>
}
  8001bb:	83 c4 10             	add    $0x10,%esp
  8001be:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001c1:	5b                   	pop    %ebx
  8001c2:	5e                   	pop    %esi
  8001c3:	5f                   	pop    %edi
  8001c4:	5d                   	pop    %ebp
  8001c5:	c3                   	ret    

008001c6 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8001c6:	55                   	push   %ebp
  8001c7:	89 e5                	mov    %esp,%ebp
  8001c9:	56                   	push   %esi
  8001ca:	53                   	push   %ebx
  8001cb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8001ce:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8001d1:	e8 33 0a 00 00       	call   800c09 <sys_getenvid>
  8001d6:	25 ff 03 00 00       	and    $0x3ff,%eax
  8001db:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001e2:	c1 e0 07             	shl    $0x7,%eax
  8001e5:	29 d0                	sub    %edx,%eax
  8001e7:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8001ec:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8001f1:	85 db                	test   %ebx,%ebx
  8001f3:	7e 07                	jle    8001fc <libmain+0x36>
		binaryname = argv[0];
  8001f5:	8b 06                	mov    (%esi),%eax
  8001f7:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  8001fc:	83 ec 08             	sub    $0x8,%esp
  8001ff:	56                   	push   %esi
  800200:	53                   	push   %ebx
  800201:	e8 2d fe ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800206:	e8 0a 00 00 00       	call   800215 <exit>
}
  80020b:	83 c4 10             	add    $0x10,%esp
  80020e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800211:	5b                   	pop    %ebx
  800212:	5e                   	pop    %esi
  800213:	5d                   	pop    %ebp
  800214:	c3                   	ret    

00800215 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800215:	55                   	push   %ebp
  800216:	89 e5                	mov    %esp,%ebp
  800218:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80021b:	6a 00                	push   $0x0
  80021d:	e8 a6 09 00 00       	call   800bc8 <sys_env_destroy>
}
  800222:	83 c4 10             	add    $0x10,%esp
  800225:	c9                   	leave  
  800226:	c3                   	ret    

00800227 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800227:	55                   	push   %ebp
  800228:	89 e5                	mov    %esp,%ebp
  80022a:	56                   	push   %esi
  80022b:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80022c:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80022f:	8b 35 00 30 80 00    	mov    0x803000,%esi
  800235:	e8 cf 09 00 00       	call   800c09 <sys_getenvid>
  80023a:	83 ec 0c             	sub    $0xc,%esp
  80023d:	ff 75 0c             	pushl  0xc(%ebp)
  800240:	ff 75 08             	pushl  0x8(%ebp)
  800243:	56                   	push   %esi
  800244:	50                   	push   %eax
  800245:	68 e4 22 80 00       	push   $0x8022e4
  80024a:	e8 b0 00 00 00       	call   8002ff <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80024f:	83 c4 18             	add    $0x18,%esp
  800252:	53                   	push   %ebx
  800253:	ff 75 10             	pushl  0x10(%ebp)
  800256:	e8 53 00 00 00       	call   8002ae <vcprintf>
	cprintf("\n");
  80025b:	c7 04 24 37 28 80 00 	movl   $0x802837,(%esp)
  800262:	e8 98 00 00 00       	call   8002ff <cprintf>
  800267:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80026a:	cc                   	int3   
  80026b:	eb fd                	jmp    80026a <_panic+0x43>

0080026d <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80026d:	55                   	push   %ebp
  80026e:	89 e5                	mov    %esp,%ebp
  800270:	53                   	push   %ebx
  800271:	83 ec 04             	sub    $0x4,%esp
  800274:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800277:	8b 13                	mov    (%ebx),%edx
  800279:	8d 42 01             	lea    0x1(%edx),%eax
  80027c:	89 03                	mov    %eax,(%ebx)
  80027e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800281:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800285:	3d ff 00 00 00       	cmp    $0xff,%eax
  80028a:	75 1a                	jne    8002a6 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80028c:	83 ec 08             	sub    $0x8,%esp
  80028f:	68 ff 00 00 00       	push   $0xff
  800294:	8d 43 08             	lea    0x8(%ebx),%eax
  800297:	50                   	push   %eax
  800298:	e8 ee 08 00 00       	call   800b8b <sys_cputs>
		b->idx = 0;
  80029d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8002a3:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8002a6:	ff 43 04             	incl   0x4(%ebx)
}
  8002a9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8002ac:	c9                   	leave  
  8002ad:	c3                   	ret    

008002ae <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8002ae:	55                   	push   %ebp
  8002af:	89 e5                	mov    %esp,%ebp
  8002b1:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8002b7:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8002be:	00 00 00 
	b.cnt = 0;
  8002c1:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8002c8:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8002cb:	ff 75 0c             	pushl  0xc(%ebp)
  8002ce:	ff 75 08             	pushl  0x8(%ebp)
  8002d1:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8002d7:	50                   	push   %eax
  8002d8:	68 6d 02 80 00       	push   $0x80026d
  8002dd:	e8 51 01 00 00       	call   800433 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8002e2:	83 c4 08             	add    $0x8,%esp
  8002e5:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8002eb:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8002f1:	50                   	push   %eax
  8002f2:	e8 94 08 00 00       	call   800b8b <sys_cputs>

	return b.cnt;
}
  8002f7:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8002fd:	c9                   	leave  
  8002fe:	c3                   	ret    

008002ff <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8002ff:	55                   	push   %ebp
  800300:	89 e5                	mov    %esp,%ebp
  800302:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800305:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800308:	50                   	push   %eax
  800309:	ff 75 08             	pushl  0x8(%ebp)
  80030c:	e8 9d ff ff ff       	call   8002ae <vcprintf>
	va_end(ap);

	return cnt;
}
  800311:	c9                   	leave  
  800312:	c3                   	ret    

00800313 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800313:	55                   	push   %ebp
  800314:	89 e5                	mov    %esp,%ebp
  800316:	57                   	push   %edi
  800317:	56                   	push   %esi
  800318:	53                   	push   %ebx
  800319:	83 ec 1c             	sub    $0x1c,%esp
  80031c:	89 c7                	mov    %eax,%edi
  80031e:	89 d6                	mov    %edx,%esi
  800320:	8b 45 08             	mov    0x8(%ebp),%eax
  800323:	8b 55 0c             	mov    0xc(%ebp),%edx
  800326:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800329:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80032c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80032f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800334:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800337:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80033a:	39 d3                	cmp    %edx,%ebx
  80033c:	72 05                	jb     800343 <printnum+0x30>
  80033e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800341:	77 45                	ja     800388 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800343:	83 ec 0c             	sub    $0xc,%esp
  800346:	ff 75 18             	pushl  0x18(%ebp)
  800349:	8b 45 14             	mov    0x14(%ebp),%eax
  80034c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80034f:	53                   	push   %ebx
  800350:	ff 75 10             	pushl  0x10(%ebp)
  800353:	83 ec 08             	sub    $0x8,%esp
  800356:	ff 75 e4             	pushl  -0x1c(%ebp)
  800359:	ff 75 e0             	pushl  -0x20(%ebp)
  80035c:	ff 75 dc             	pushl  -0x24(%ebp)
  80035f:	ff 75 d8             	pushl  -0x28(%ebp)
  800362:	e8 29 1c 00 00       	call   801f90 <__udivdi3>
  800367:	83 c4 18             	add    $0x18,%esp
  80036a:	52                   	push   %edx
  80036b:	50                   	push   %eax
  80036c:	89 f2                	mov    %esi,%edx
  80036e:	89 f8                	mov    %edi,%eax
  800370:	e8 9e ff ff ff       	call   800313 <printnum>
  800375:	83 c4 20             	add    $0x20,%esp
  800378:	eb 16                	jmp    800390 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80037a:	83 ec 08             	sub    $0x8,%esp
  80037d:	56                   	push   %esi
  80037e:	ff 75 18             	pushl  0x18(%ebp)
  800381:	ff d7                	call   *%edi
  800383:	83 c4 10             	add    $0x10,%esp
  800386:	eb 03                	jmp    80038b <printnum+0x78>
  800388:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80038b:	4b                   	dec    %ebx
  80038c:	85 db                	test   %ebx,%ebx
  80038e:	7f ea                	jg     80037a <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800390:	83 ec 08             	sub    $0x8,%esp
  800393:	56                   	push   %esi
  800394:	83 ec 04             	sub    $0x4,%esp
  800397:	ff 75 e4             	pushl  -0x1c(%ebp)
  80039a:	ff 75 e0             	pushl  -0x20(%ebp)
  80039d:	ff 75 dc             	pushl  -0x24(%ebp)
  8003a0:	ff 75 d8             	pushl  -0x28(%ebp)
  8003a3:	e8 f8 1c 00 00       	call   8020a0 <__umoddi3>
  8003a8:	83 c4 14             	add    $0x14,%esp
  8003ab:	0f be 80 07 23 80 00 	movsbl 0x802307(%eax),%eax
  8003b2:	50                   	push   %eax
  8003b3:	ff d7                	call   *%edi
}
  8003b5:	83 c4 10             	add    $0x10,%esp
  8003b8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8003bb:	5b                   	pop    %ebx
  8003bc:	5e                   	pop    %esi
  8003bd:	5f                   	pop    %edi
  8003be:	5d                   	pop    %ebp
  8003bf:	c3                   	ret    

008003c0 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8003c0:	55                   	push   %ebp
  8003c1:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8003c3:	83 fa 01             	cmp    $0x1,%edx
  8003c6:	7e 0e                	jle    8003d6 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8003c8:	8b 10                	mov    (%eax),%edx
  8003ca:	8d 4a 08             	lea    0x8(%edx),%ecx
  8003cd:	89 08                	mov    %ecx,(%eax)
  8003cf:	8b 02                	mov    (%edx),%eax
  8003d1:	8b 52 04             	mov    0x4(%edx),%edx
  8003d4:	eb 22                	jmp    8003f8 <getuint+0x38>
	else if (lflag)
  8003d6:	85 d2                	test   %edx,%edx
  8003d8:	74 10                	je     8003ea <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8003da:	8b 10                	mov    (%eax),%edx
  8003dc:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003df:	89 08                	mov    %ecx,(%eax)
  8003e1:	8b 02                	mov    (%edx),%eax
  8003e3:	ba 00 00 00 00       	mov    $0x0,%edx
  8003e8:	eb 0e                	jmp    8003f8 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8003ea:	8b 10                	mov    (%eax),%edx
  8003ec:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003ef:	89 08                	mov    %ecx,(%eax)
  8003f1:	8b 02                	mov    (%edx),%eax
  8003f3:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8003f8:	5d                   	pop    %ebp
  8003f9:	c3                   	ret    

008003fa <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8003fa:	55                   	push   %ebp
  8003fb:	89 e5                	mov    %esp,%ebp
  8003fd:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800400:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800403:	8b 10                	mov    (%eax),%edx
  800405:	3b 50 04             	cmp    0x4(%eax),%edx
  800408:	73 0a                	jae    800414 <sprintputch+0x1a>
		*b->buf++ = ch;
  80040a:	8d 4a 01             	lea    0x1(%edx),%ecx
  80040d:	89 08                	mov    %ecx,(%eax)
  80040f:	8b 45 08             	mov    0x8(%ebp),%eax
  800412:	88 02                	mov    %al,(%edx)
}
  800414:	5d                   	pop    %ebp
  800415:	c3                   	ret    

00800416 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800416:	55                   	push   %ebp
  800417:	89 e5                	mov    %esp,%ebp
  800419:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80041c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80041f:	50                   	push   %eax
  800420:	ff 75 10             	pushl  0x10(%ebp)
  800423:	ff 75 0c             	pushl  0xc(%ebp)
  800426:	ff 75 08             	pushl  0x8(%ebp)
  800429:	e8 05 00 00 00       	call   800433 <vprintfmt>
	va_end(ap);
}
  80042e:	83 c4 10             	add    $0x10,%esp
  800431:	c9                   	leave  
  800432:	c3                   	ret    

00800433 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800433:	55                   	push   %ebp
  800434:	89 e5                	mov    %esp,%ebp
  800436:	57                   	push   %edi
  800437:	56                   	push   %esi
  800438:	53                   	push   %ebx
  800439:	83 ec 2c             	sub    $0x2c,%esp
  80043c:	8b 75 08             	mov    0x8(%ebp),%esi
  80043f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800442:	8b 7d 10             	mov    0x10(%ebp),%edi
  800445:	eb 12                	jmp    800459 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800447:	85 c0                	test   %eax,%eax
  800449:	0f 84 68 03 00 00    	je     8007b7 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80044f:	83 ec 08             	sub    $0x8,%esp
  800452:	53                   	push   %ebx
  800453:	50                   	push   %eax
  800454:	ff d6                	call   *%esi
  800456:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800459:	47                   	inc    %edi
  80045a:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80045e:	83 f8 25             	cmp    $0x25,%eax
  800461:	75 e4                	jne    800447 <vprintfmt+0x14>
  800463:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800467:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80046e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800475:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80047c:	ba 00 00 00 00       	mov    $0x0,%edx
  800481:	eb 07                	jmp    80048a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800483:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800486:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80048a:	8d 47 01             	lea    0x1(%edi),%eax
  80048d:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800490:	0f b6 0f             	movzbl (%edi),%ecx
  800493:	8a 07                	mov    (%edi),%al
  800495:	83 e8 23             	sub    $0x23,%eax
  800498:	3c 55                	cmp    $0x55,%al
  80049a:	0f 87 fe 02 00 00    	ja     80079e <vprintfmt+0x36b>
  8004a0:	0f b6 c0             	movzbl %al,%eax
  8004a3:	ff 24 85 40 24 80 00 	jmp    *0x802440(,%eax,4)
  8004aa:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8004ad:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8004b1:	eb d7                	jmp    80048a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004b6:	b8 00 00 00 00       	mov    $0x0,%eax
  8004bb:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8004be:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8004c1:	01 c0                	add    %eax,%eax
  8004c3:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8004c7:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8004ca:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8004cd:	83 fa 09             	cmp    $0x9,%edx
  8004d0:	77 34                	ja     800506 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8004d2:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8004d3:	eb e9                	jmp    8004be <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8004d5:	8b 45 14             	mov    0x14(%ebp),%eax
  8004d8:	8d 48 04             	lea    0x4(%eax),%ecx
  8004db:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8004de:	8b 00                	mov    (%eax),%eax
  8004e0:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8004e6:	eb 24                	jmp    80050c <vprintfmt+0xd9>
  8004e8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004ec:	79 07                	jns    8004f5 <vprintfmt+0xc2>
  8004ee:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004f8:	eb 90                	jmp    80048a <vprintfmt+0x57>
  8004fa:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8004fd:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800504:	eb 84                	jmp    80048a <vprintfmt+0x57>
  800506:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800509:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80050c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800510:	0f 89 74 ff ff ff    	jns    80048a <vprintfmt+0x57>
				width = precision, precision = -1;
  800516:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800519:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80051c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800523:	e9 62 ff ff ff       	jmp    80048a <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800528:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800529:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80052c:	e9 59 ff ff ff       	jmp    80048a <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800531:	8b 45 14             	mov    0x14(%ebp),%eax
  800534:	8d 50 04             	lea    0x4(%eax),%edx
  800537:	89 55 14             	mov    %edx,0x14(%ebp)
  80053a:	83 ec 08             	sub    $0x8,%esp
  80053d:	53                   	push   %ebx
  80053e:	ff 30                	pushl  (%eax)
  800540:	ff d6                	call   *%esi
			break;
  800542:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800545:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800548:	e9 0c ff ff ff       	jmp    800459 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80054d:	8b 45 14             	mov    0x14(%ebp),%eax
  800550:	8d 50 04             	lea    0x4(%eax),%edx
  800553:	89 55 14             	mov    %edx,0x14(%ebp)
  800556:	8b 00                	mov    (%eax),%eax
  800558:	85 c0                	test   %eax,%eax
  80055a:	79 02                	jns    80055e <vprintfmt+0x12b>
  80055c:	f7 d8                	neg    %eax
  80055e:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800560:	83 f8 0f             	cmp    $0xf,%eax
  800563:	7f 0b                	jg     800570 <vprintfmt+0x13d>
  800565:	8b 04 85 a0 25 80 00 	mov    0x8025a0(,%eax,4),%eax
  80056c:	85 c0                	test   %eax,%eax
  80056e:	75 18                	jne    800588 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800570:	52                   	push   %edx
  800571:	68 1f 23 80 00       	push   $0x80231f
  800576:	53                   	push   %ebx
  800577:	56                   	push   %esi
  800578:	e8 99 fe ff ff       	call   800416 <printfmt>
  80057d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800580:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800583:	e9 d1 fe ff ff       	jmp    800459 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800588:	50                   	push   %eax
  800589:	68 05 28 80 00       	push   $0x802805
  80058e:	53                   	push   %ebx
  80058f:	56                   	push   %esi
  800590:	e8 81 fe ff ff       	call   800416 <printfmt>
  800595:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800598:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80059b:	e9 b9 fe ff ff       	jmp    800459 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8005a0:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a3:	8d 50 04             	lea    0x4(%eax),%edx
  8005a6:	89 55 14             	mov    %edx,0x14(%ebp)
  8005a9:	8b 38                	mov    (%eax),%edi
  8005ab:	85 ff                	test   %edi,%edi
  8005ad:	75 05                	jne    8005b4 <vprintfmt+0x181>
				p = "(null)";
  8005af:	bf 18 23 80 00       	mov    $0x802318,%edi
			if (width > 0 && padc != '-')
  8005b4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005b8:	0f 8e 90 00 00 00    	jle    80064e <vprintfmt+0x21b>
  8005be:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8005c2:	0f 84 8e 00 00 00    	je     800656 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8005c8:	83 ec 08             	sub    $0x8,%esp
  8005cb:	ff 75 d0             	pushl  -0x30(%ebp)
  8005ce:	57                   	push   %edi
  8005cf:	e8 70 02 00 00       	call   800844 <strnlen>
  8005d4:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8005d7:	29 c1                	sub    %eax,%ecx
  8005d9:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8005dc:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8005df:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8005e3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8005e6:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8005e9:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005eb:	eb 0d                	jmp    8005fa <vprintfmt+0x1c7>
					putch(padc, putdat);
  8005ed:	83 ec 08             	sub    $0x8,%esp
  8005f0:	53                   	push   %ebx
  8005f1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005f4:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005f6:	4f                   	dec    %edi
  8005f7:	83 c4 10             	add    $0x10,%esp
  8005fa:	85 ff                	test   %edi,%edi
  8005fc:	7f ef                	jg     8005ed <vprintfmt+0x1ba>
  8005fe:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800601:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800604:	89 c8                	mov    %ecx,%eax
  800606:	85 c9                	test   %ecx,%ecx
  800608:	79 05                	jns    80060f <vprintfmt+0x1dc>
  80060a:	b8 00 00 00 00       	mov    $0x0,%eax
  80060f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800612:	29 c1                	sub    %eax,%ecx
  800614:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800617:	89 75 08             	mov    %esi,0x8(%ebp)
  80061a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80061d:	eb 3d                	jmp    80065c <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80061f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800623:	74 19                	je     80063e <vprintfmt+0x20b>
  800625:	0f be c0             	movsbl %al,%eax
  800628:	83 e8 20             	sub    $0x20,%eax
  80062b:	83 f8 5e             	cmp    $0x5e,%eax
  80062e:	76 0e                	jbe    80063e <vprintfmt+0x20b>
					putch('?', putdat);
  800630:	83 ec 08             	sub    $0x8,%esp
  800633:	53                   	push   %ebx
  800634:	6a 3f                	push   $0x3f
  800636:	ff 55 08             	call   *0x8(%ebp)
  800639:	83 c4 10             	add    $0x10,%esp
  80063c:	eb 0b                	jmp    800649 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80063e:	83 ec 08             	sub    $0x8,%esp
  800641:	53                   	push   %ebx
  800642:	52                   	push   %edx
  800643:	ff 55 08             	call   *0x8(%ebp)
  800646:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800649:	ff 4d e4             	decl   -0x1c(%ebp)
  80064c:	eb 0e                	jmp    80065c <vprintfmt+0x229>
  80064e:	89 75 08             	mov    %esi,0x8(%ebp)
  800651:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800654:	eb 06                	jmp    80065c <vprintfmt+0x229>
  800656:	89 75 08             	mov    %esi,0x8(%ebp)
  800659:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80065c:	47                   	inc    %edi
  80065d:	8a 47 ff             	mov    -0x1(%edi),%al
  800660:	0f be d0             	movsbl %al,%edx
  800663:	85 d2                	test   %edx,%edx
  800665:	74 1d                	je     800684 <vprintfmt+0x251>
  800667:	85 f6                	test   %esi,%esi
  800669:	78 b4                	js     80061f <vprintfmt+0x1ec>
  80066b:	4e                   	dec    %esi
  80066c:	79 b1                	jns    80061f <vprintfmt+0x1ec>
  80066e:	8b 75 08             	mov    0x8(%ebp),%esi
  800671:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800674:	eb 14                	jmp    80068a <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800676:	83 ec 08             	sub    $0x8,%esp
  800679:	53                   	push   %ebx
  80067a:	6a 20                	push   $0x20
  80067c:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80067e:	4f                   	dec    %edi
  80067f:	83 c4 10             	add    $0x10,%esp
  800682:	eb 06                	jmp    80068a <vprintfmt+0x257>
  800684:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800687:	8b 75 08             	mov    0x8(%ebp),%esi
  80068a:	85 ff                	test   %edi,%edi
  80068c:	7f e8                	jg     800676 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80068e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800691:	e9 c3 fd ff ff       	jmp    800459 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800696:	83 fa 01             	cmp    $0x1,%edx
  800699:	7e 16                	jle    8006b1 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80069b:	8b 45 14             	mov    0x14(%ebp),%eax
  80069e:	8d 50 08             	lea    0x8(%eax),%edx
  8006a1:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a4:	8b 50 04             	mov    0x4(%eax),%edx
  8006a7:	8b 00                	mov    (%eax),%eax
  8006a9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006ac:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8006af:	eb 32                	jmp    8006e3 <vprintfmt+0x2b0>
	else if (lflag)
  8006b1:	85 d2                	test   %edx,%edx
  8006b3:	74 18                	je     8006cd <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8006b5:	8b 45 14             	mov    0x14(%ebp),%eax
  8006b8:	8d 50 04             	lea    0x4(%eax),%edx
  8006bb:	89 55 14             	mov    %edx,0x14(%ebp)
  8006be:	8b 00                	mov    (%eax),%eax
  8006c0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006c3:	89 c1                	mov    %eax,%ecx
  8006c5:	c1 f9 1f             	sar    $0x1f,%ecx
  8006c8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8006cb:	eb 16                	jmp    8006e3 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8006cd:	8b 45 14             	mov    0x14(%ebp),%eax
  8006d0:	8d 50 04             	lea    0x4(%eax),%edx
  8006d3:	89 55 14             	mov    %edx,0x14(%ebp)
  8006d6:	8b 00                	mov    (%eax),%eax
  8006d8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006db:	89 c1                	mov    %eax,%ecx
  8006dd:	c1 f9 1f             	sar    $0x1f,%ecx
  8006e0:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8006e3:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006e6:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8006e9:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8006ed:	79 76                	jns    800765 <vprintfmt+0x332>
				putch('-', putdat);
  8006ef:	83 ec 08             	sub    $0x8,%esp
  8006f2:	53                   	push   %ebx
  8006f3:	6a 2d                	push   $0x2d
  8006f5:	ff d6                	call   *%esi
				num = -(long long) num;
  8006f7:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006fa:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8006fd:	f7 d8                	neg    %eax
  8006ff:	83 d2 00             	adc    $0x0,%edx
  800702:	f7 da                	neg    %edx
  800704:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800707:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80070c:	eb 5c                	jmp    80076a <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80070e:	8d 45 14             	lea    0x14(%ebp),%eax
  800711:	e8 aa fc ff ff       	call   8003c0 <getuint>
			base = 10;
  800716:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80071b:	eb 4d                	jmp    80076a <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80071d:	8d 45 14             	lea    0x14(%ebp),%eax
  800720:	e8 9b fc ff ff       	call   8003c0 <getuint>
			base = 8;
  800725:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80072a:	eb 3e                	jmp    80076a <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80072c:	83 ec 08             	sub    $0x8,%esp
  80072f:	53                   	push   %ebx
  800730:	6a 30                	push   $0x30
  800732:	ff d6                	call   *%esi
			putch('x', putdat);
  800734:	83 c4 08             	add    $0x8,%esp
  800737:	53                   	push   %ebx
  800738:	6a 78                	push   $0x78
  80073a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80073c:	8b 45 14             	mov    0x14(%ebp),%eax
  80073f:	8d 50 04             	lea    0x4(%eax),%edx
  800742:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800745:	8b 00                	mov    (%eax),%eax
  800747:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80074c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80074f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800754:	eb 14                	jmp    80076a <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800756:	8d 45 14             	lea    0x14(%ebp),%eax
  800759:	e8 62 fc ff ff       	call   8003c0 <getuint>
			base = 16;
  80075e:	b9 10 00 00 00       	mov    $0x10,%ecx
  800763:	eb 05                	jmp    80076a <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800765:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80076a:	83 ec 0c             	sub    $0xc,%esp
  80076d:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800771:	57                   	push   %edi
  800772:	ff 75 e4             	pushl  -0x1c(%ebp)
  800775:	51                   	push   %ecx
  800776:	52                   	push   %edx
  800777:	50                   	push   %eax
  800778:	89 da                	mov    %ebx,%edx
  80077a:	89 f0                	mov    %esi,%eax
  80077c:	e8 92 fb ff ff       	call   800313 <printnum>
			break;
  800781:	83 c4 20             	add    $0x20,%esp
  800784:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800787:	e9 cd fc ff ff       	jmp    800459 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80078c:	83 ec 08             	sub    $0x8,%esp
  80078f:	53                   	push   %ebx
  800790:	51                   	push   %ecx
  800791:	ff d6                	call   *%esi
			break;
  800793:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800796:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800799:	e9 bb fc ff ff       	jmp    800459 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80079e:	83 ec 08             	sub    $0x8,%esp
  8007a1:	53                   	push   %ebx
  8007a2:	6a 25                	push   $0x25
  8007a4:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8007a6:	83 c4 10             	add    $0x10,%esp
  8007a9:	eb 01                	jmp    8007ac <vprintfmt+0x379>
  8007ab:	4f                   	dec    %edi
  8007ac:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8007b0:	75 f9                	jne    8007ab <vprintfmt+0x378>
  8007b2:	e9 a2 fc ff ff       	jmp    800459 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8007b7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8007ba:	5b                   	pop    %ebx
  8007bb:	5e                   	pop    %esi
  8007bc:	5f                   	pop    %edi
  8007bd:	5d                   	pop    %ebp
  8007be:	c3                   	ret    

008007bf <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8007bf:	55                   	push   %ebp
  8007c0:	89 e5                	mov    %esp,%ebp
  8007c2:	83 ec 18             	sub    $0x18,%esp
  8007c5:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c8:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8007cb:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8007ce:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8007d2:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8007d5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007dc:	85 c0                	test   %eax,%eax
  8007de:	74 26                	je     800806 <vsnprintf+0x47>
  8007e0:	85 d2                	test   %edx,%edx
  8007e2:	7e 29                	jle    80080d <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8007e4:	ff 75 14             	pushl  0x14(%ebp)
  8007e7:	ff 75 10             	pushl  0x10(%ebp)
  8007ea:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007ed:	50                   	push   %eax
  8007ee:	68 fa 03 80 00       	push   $0x8003fa
  8007f3:	e8 3b fc ff ff       	call   800433 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8007f8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8007fb:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8007fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800801:	83 c4 10             	add    $0x10,%esp
  800804:	eb 0c                	jmp    800812 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800806:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80080b:	eb 05                	jmp    800812 <vsnprintf+0x53>
  80080d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800812:	c9                   	leave  
  800813:	c3                   	ret    

00800814 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800814:	55                   	push   %ebp
  800815:	89 e5                	mov    %esp,%ebp
  800817:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80081a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80081d:	50                   	push   %eax
  80081e:	ff 75 10             	pushl  0x10(%ebp)
  800821:	ff 75 0c             	pushl  0xc(%ebp)
  800824:	ff 75 08             	pushl  0x8(%ebp)
  800827:	e8 93 ff ff ff       	call   8007bf <vsnprintf>
	va_end(ap);

	return rc;
}
  80082c:	c9                   	leave  
  80082d:	c3                   	ret    

0080082e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80082e:	55                   	push   %ebp
  80082f:	89 e5                	mov    %esp,%ebp
  800831:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800834:	b8 00 00 00 00       	mov    $0x0,%eax
  800839:	eb 01                	jmp    80083c <strlen+0xe>
		n++;
  80083b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80083c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800840:	75 f9                	jne    80083b <strlen+0xd>
		n++;
	return n;
}
  800842:	5d                   	pop    %ebp
  800843:	c3                   	ret    

00800844 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800844:	55                   	push   %ebp
  800845:	89 e5                	mov    %esp,%ebp
  800847:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80084a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80084d:	ba 00 00 00 00       	mov    $0x0,%edx
  800852:	eb 01                	jmp    800855 <strnlen+0x11>
		n++;
  800854:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800855:	39 c2                	cmp    %eax,%edx
  800857:	74 08                	je     800861 <strnlen+0x1d>
  800859:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80085d:	75 f5                	jne    800854 <strnlen+0x10>
  80085f:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800861:	5d                   	pop    %ebp
  800862:	c3                   	ret    

00800863 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800863:	55                   	push   %ebp
  800864:	89 e5                	mov    %esp,%ebp
  800866:	53                   	push   %ebx
  800867:	8b 45 08             	mov    0x8(%ebp),%eax
  80086a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80086d:	89 c2                	mov    %eax,%edx
  80086f:	42                   	inc    %edx
  800870:	41                   	inc    %ecx
  800871:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800874:	88 5a ff             	mov    %bl,-0x1(%edx)
  800877:	84 db                	test   %bl,%bl
  800879:	75 f4                	jne    80086f <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80087b:	5b                   	pop    %ebx
  80087c:	5d                   	pop    %ebp
  80087d:	c3                   	ret    

0080087e <strcat>:

char *
strcat(char *dst, const char *src)
{
  80087e:	55                   	push   %ebp
  80087f:	89 e5                	mov    %esp,%ebp
  800881:	53                   	push   %ebx
  800882:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800885:	53                   	push   %ebx
  800886:	e8 a3 ff ff ff       	call   80082e <strlen>
  80088b:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80088e:	ff 75 0c             	pushl  0xc(%ebp)
  800891:	01 d8                	add    %ebx,%eax
  800893:	50                   	push   %eax
  800894:	e8 ca ff ff ff       	call   800863 <strcpy>
	return dst;
}
  800899:	89 d8                	mov    %ebx,%eax
  80089b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80089e:	c9                   	leave  
  80089f:	c3                   	ret    

008008a0 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8008a0:	55                   	push   %ebp
  8008a1:	89 e5                	mov    %esp,%ebp
  8008a3:	56                   	push   %esi
  8008a4:	53                   	push   %ebx
  8008a5:	8b 75 08             	mov    0x8(%ebp),%esi
  8008a8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8008ab:	89 f3                	mov    %esi,%ebx
  8008ad:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008b0:	89 f2                	mov    %esi,%edx
  8008b2:	eb 0c                	jmp    8008c0 <strncpy+0x20>
		*dst++ = *src;
  8008b4:	42                   	inc    %edx
  8008b5:	8a 01                	mov    (%ecx),%al
  8008b7:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8008ba:	80 39 01             	cmpb   $0x1,(%ecx)
  8008bd:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008c0:	39 da                	cmp    %ebx,%edx
  8008c2:	75 f0                	jne    8008b4 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8008c4:	89 f0                	mov    %esi,%eax
  8008c6:	5b                   	pop    %ebx
  8008c7:	5e                   	pop    %esi
  8008c8:	5d                   	pop    %ebp
  8008c9:	c3                   	ret    

008008ca <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8008ca:	55                   	push   %ebp
  8008cb:	89 e5                	mov    %esp,%ebp
  8008cd:	56                   	push   %esi
  8008ce:	53                   	push   %ebx
  8008cf:	8b 75 08             	mov    0x8(%ebp),%esi
  8008d2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8008d5:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8008d8:	85 c0                	test   %eax,%eax
  8008da:	74 1e                	je     8008fa <strlcpy+0x30>
  8008dc:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8008e0:	89 f2                	mov    %esi,%edx
  8008e2:	eb 05                	jmp    8008e9 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8008e4:	42                   	inc    %edx
  8008e5:	41                   	inc    %ecx
  8008e6:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8008e9:	39 c2                	cmp    %eax,%edx
  8008eb:	74 08                	je     8008f5 <strlcpy+0x2b>
  8008ed:	8a 19                	mov    (%ecx),%bl
  8008ef:	84 db                	test   %bl,%bl
  8008f1:	75 f1                	jne    8008e4 <strlcpy+0x1a>
  8008f3:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8008f5:	c6 00 00             	movb   $0x0,(%eax)
  8008f8:	eb 02                	jmp    8008fc <strlcpy+0x32>
  8008fa:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8008fc:	29 f0                	sub    %esi,%eax
}
  8008fe:	5b                   	pop    %ebx
  8008ff:	5e                   	pop    %esi
  800900:	5d                   	pop    %ebp
  800901:	c3                   	ret    

00800902 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800902:	55                   	push   %ebp
  800903:	89 e5                	mov    %esp,%ebp
  800905:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800908:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80090b:	eb 02                	jmp    80090f <strcmp+0xd>
		p++, q++;
  80090d:	41                   	inc    %ecx
  80090e:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80090f:	8a 01                	mov    (%ecx),%al
  800911:	84 c0                	test   %al,%al
  800913:	74 04                	je     800919 <strcmp+0x17>
  800915:	3a 02                	cmp    (%edx),%al
  800917:	74 f4                	je     80090d <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800919:	0f b6 c0             	movzbl %al,%eax
  80091c:	0f b6 12             	movzbl (%edx),%edx
  80091f:	29 d0                	sub    %edx,%eax
}
  800921:	5d                   	pop    %ebp
  800922:	c3                   	ret    

00800923 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800923:	55                   	push   %ebp
  800924:	89 e5                	mov    %esp,%ebp
  800926:	53                   	push   %ebx
  800927:	8b 45 08             	mov    0x8(%ebp),%eax
  80092a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80092d:	89 c3                	mov    %eax,%ebx
  80092f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800932:	eb 02                	jmp    800936 <strncmp+0x13>
		n--, p++, q++;
  800934:	40                   	inc    %eax
  800935:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800936:	39 d8                	cmp    %ebx,%eax
  800938:	74 14                	je     80094e <strncmp+0x2b>
  80093a:	8a 08                	mov    (%eax),%cl
  80093c:	84 c9                	test   %cl,%cl
  80093e:	74 04                	je     800944 <strncmp+0x21>
  800940:	3a 0a                	cmp    (%edx),%cl
  800942:	74 f0                	je     800934 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800944:	0f b6 00             	movzbl (%eax),%eax
  800947:	0f b6 12             	movzbl (%edx),%edx
  80094a:	29 d0                	sub    %edx,%eax
  80094c:	eb 05                	jmp    800953 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80094e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800953:	5b                   	pop    %ebx
  800954:	5d                   	pop    %ebp
  800955:	c3                   	ret    

00800956 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800956:	55                   	push   %ebp
  800957:	89 e5                	mov    %esp,%ebp
  800959:	8b 45 08             	mov    0x8(%ebp),%eax
  80095c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80095f:	eb 05                	jmp    800966 <strchr+0x10>
		if (*s == c)
  800961:	38 ca                	cmp    %cl,%dl
  800963:	74 0c                	je     800971 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800965:	40                   	inc    %eax
  800966:	8a 10                	mov    (%eax),%dl
  800968:	84 d2                	test   %dl,%dl
  80096a:	75 f5                	jne    800961 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80096c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800971:	5d                   	pop    %ebp
  800972:	c3                   	ret    

00800973 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800973:	55                   	push   %ebp
  800974:	89 e5                	mov    %esp,%ebp
  800976:	8b 45 08             	mov    0x8(%ebp),%eax
  800979:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80097c:	eb 05                	jmp    800983 <strfind+0x10>
		if (*s == c)
  80097e:	38 ca                	cmp    %cl,%dl
  800980:	74 07                	je     800989 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800982:	40                   	inc    %eax
  800983:	8a 10                	mov    (%eax),%dl
  800985:	84 d2                	test   %dl,%dl
  800987:	75 f5                	jne    80097e <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800989:	5d                   	pop    %ebp
  80098a:	c3                   	ret    

0080098b <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80098b:	55                   	push   %ebp
  80098c:	89 e5                	mov    %esp,%ebp
  80098e:	57                   	push   %edi
  80098f:	56                   	push   %esi
  800990:	53                   	push   %ebx
  800991:	8b 7d 08             	mov    0x8(%ebp),%edi
  800994:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800997:	85 c9                	test   %ecx,%ecx
  800999:	74 36                	je     8009d1 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80099b:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8009a1:	75 28                	jne    8009cb <memset+0x40>
  8009a3:	f6 c1 03             	test   $0x3,%cl
  8009a6:	75 23                	jne    8009cb <memset+0x40>
		c &= 0xFF;
  8009a8:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8009ac:	89 d3                	mov    %edx,%ebx
  8009ae:	c1 e3 08             	shl    $0x8,%ebx
  8009b1:	89 d6                	mov    %edx,%esi
  8009b3:	c1 e6 18             	shl    $0x18,%esi
  8009b6:	89 d0                	mov    %edx,%eax
  8009b8:	c1 e0 10             	shl    $0x10,%eax
  8009bb:	09 f0                	or     %esi,%eax
  8009bd:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8009bf:	89 d8                	mov    %ebx,%eax
  8009c1:	09 d0                	or     %edx,%eax
  8009c3:	c1 e9 02             	shr    $0x2,%ecx
  8009c6:	fc                   	cld    
  8009c7:	f3 ab                	rep stos %eax,%es:(%edi)
  8009c9:	eb 06                	jmp    8009d1 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8009cb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009ce:	fc                   	cld    
  8009cf:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8009d1:	89 f8                	mov    %edi,%eax
  8009d3:	5b                   	pop    %ebx
  8009d4:	5e                   	pop    %esi
  8009d5:	5f                   	pop    %edi
  8009d6:	5d                   	pop    %ebp
  8009d7:	c3                   	ret    

008009d8 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8009d8:	55                   	push   %ebp
  8009d9:	89 e5                	mov    %esp,%ebp
  8009db:	57                   	push   %edi
  8009dc:	56                   	push   %esi
  8009dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e0:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009e3:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8009e6:	39 c6                	cmp    %eax,%esi
  8009e8:	73 33                	jae    800a1d <memmove+0x45>
  8009ea:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8009ed:	39 d0                	cmp    %edx,%eax
  8009ef:	73 2c                	jae    800a1d <memmove+0x45>
		s += n;
		d += n;
  8009f1:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009f4:	89 d6                	mov    %edx,%esi
  8009f6:	09 fe                	or     %edi,%esi
  8009f8:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8009fe:	75 13                	jne    800a13 <memmove+0x3b>
  800a00:	f6 c1 03             	test   $0x3,%cl
  800a03:	75 0e                	jne    800a13 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800a05:	83 ef 04             	sub    $0x4,%edi
  800a08:	8d 72 fc             	lea    -0x4(%edx),%esi
  800a0b:	c1 e9 02             	shr    $0x2,%ecx
  800a0e:	fd                   	std    
  800a0f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a11:	eb 07                	jmp    800a1a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800a13:	4f                   	dec    %edi
  800a14:	8d 72 ff             	lea    -0x1(%edx),%esi
  800a17:	fd                   	std    
  800a18:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800a1a:	fc                   	cld    
  800a1b:	eb 1d                	jmp    800a3a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a1d:	89 f2                	mov    %esi,%edx
  800a1f:	09 c2                	or     %eax,%edx
  800a21:	f6 c2 03             	test   $0x3,%dl
  800a24:	75 0f                	jne    800a35 <memmove+0x5d>
  800a26:	f6 c1 03             	test   $0x3,%cl
  800a29:	75 0a                	jne    800a35 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800a2b:	c1 e9 02             	shr    $0x2,%ecx
  800a2e:	89 c7                	mov    %eax,%edi
  800a30:	fc                   	cld    
  800a31:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a33:	eb 05                	jmp    800a3a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800a35:	89 c7                	mov    %eax,%edi
  800a37:	fc                   	cld    
  800a38:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800a3a:	5e                   	pop    %esi
  800a3b:	5f                   	pop    %edi
  800a3c:	5d                   	pop    %ebp
  800a3d:	c3                   	ret    

00800a3e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800a3e:	55                   	push   %ebp
  800a3f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800a41:	ff 75 10             	pushl  0x10(%ebp)
  800a44:	ff 75 0c             	pushl  0xc(%ebp)
  800a47:	ff 75 08             	pushl  0x8(%ebp)
  800a4a:	e8 89 ff ff ff       	call   8009d8 <memmove>
}
  800a4f:	c9                   	leave  
  800a50:	c3                   	ret    

00800a51 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800a51:	55                   	push   %ebp
  800a52:	89 e5                	mov    %esp,%ebp
  800a54:	56                   	push   %esi
  800a55:	53                   	push   %ebx
  800a56:	8b 45 08             	mov    0x8(%ebp),%eax
  800a59:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a5c:	89 c6                	mov    %eax,%esi
  800a5e:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a61:	eb 14                	jmp    800a77 <memcmp+0x26>
		if (*s1 != *s2)
  800a63:	8a 08                	mov    (%eax),%cl
  800a65:	8a 1a                	mov    (%edx),%bl
  800a67:	38 d9                	cmp    %bl,%cl
  800a69:	74 0a                	je     800a75 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800a6b:	0f b6 c1             	movzbl %cl,%eax
  800a6e:	0f b6 db             	movzbl %bl,%ebx
  800a71:	29 d8                	sub    %ebx,%eax
  800a73:	eb 0b                	jmp    800a80 <memcmp+0x2f>
		s1++, s2++;
  800a75:	40                   	inc    %eax
  800a76:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a77:	39 f0                	cmp    %esi,%eax
  800a79:	75 e8                	jne    800a63 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800a7b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a80:	5b                   	pop    %ebx
  800a81:	5e                   	pop    %esi
  800a82:	5d                   	pop    %ebp
  800a83:	c3                   	ret    

00800a84 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800a84:	55                   	push   %ebp
  800a85:	89 e5                	mov    %esp,%ebp
  800a87:	53                   	push   %ebx
  800a88:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800a8b:	89 c1                	mov    %eax,%ecx
  800a8d:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800a90:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a94:	eb 08                	jmp    800a9e <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a96:	0f b6 10             	movzbl (%eax),%edx
  800a99:	39 da                	cmp    %ebx,%edx
  800a9b:	74 05                	je     800aa2 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a9d:	40                   	inc    %eax
  800a9e:	39 c8                	cmp    %ecx,%eax
  800aa0:	72 f4                	jb     800a96 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800aa2:	5b                   	pop    %ebx
  800aa3:	5d                   	pop    %ebp
  800aa4:	c3                   	ret    

00800aa5 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800aa5:	55                   	push   %ebp
  800aa6:	89 e5                	mov    %esp,%ebp
  800aa8:	57                   	push   %edi
  800aa9:	56                   	push   %esi
  800aaa:	53                   	push   %ebx
  800aab:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800aae:	eb 01                	jmp    800ab1 <strtol+0xc>
		s++;
  800ab0:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800ab1:	8a 01                	mov    (%ecx),%al
  800ab3:	3c 20                	cmp    $0x20,%al
  800ab5:	74 f9                	je     800ab0 <strtol+0xb>
  800ab7:	3c 09                	cmp    $0x9,%al
  800ab9:	74 f5                	je     800ab0 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800abb:	3c 2b                	cmp    $0x2b,%al
  800abd:	75 08                	jne    800ac7 <strtol+0x22>
		s++;
  800abf:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800ac0:	bf 00 00 00 00       	mov    $0x0,%edi
  800ac5:	eb 11                	jmp    800ad8 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800ac7:	3c 2d                	cmp    $0x2d,%al
  800ac9:	75 08                	jne    800ad3 <strtol+0x2e>
		s++, neg = 1;
  800acb:	41                   	inc    %ecx
  800acc:	bf 01 00 00 00       	mov    $0x1,%edi
  800ad1:	eb 05                	jmp    800ad8 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800ad3:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ad8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800adc:	0f 84 87 00 00 00    	je     800b69 <strtol+0xc4>
  800ae2:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800ae6:	75 27                	jne    800b0f <strtol+0x6a>
  800ae8:	80 39 30             	cmpb   $0x30,(%ecx)
  800aeb:	75 22                	jne    800b0f <strtol+0x6a>
  800aed:	e9 88 00 00 00       	jmp    800b7a <strtol+0xd5>
		s += 2, base = 16;
  800af2:	83 c1 02             	add    $0x2,%ecx
  800af5:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800afc:	eb 11                	jmp    800b0f <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800afe:	41                   	inc    %ecx
  800aff:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800b06:	eb 07                	jmp    800b0f <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800b08:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800b0f:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800b14:	8a 11                	mov    (%ecx),%dl
  800b16:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800b19:	80 fb 09             	cmp    $0x9,%bl
  800b1c:	77 08                	ja     800b26 <strtol+0x81>
			dig = *s - '0';
  800b1e:	0f be d2             	movsbl %dl,%edx
  800b21:	83 ea 30             	sub    $0x30,%edx
  800b24:	eb 22                	jmp    800b48 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800b26:	8d 72 9f             	lea    -0x61(%edx),%esi
  800b29:	89 f3                	mov    %esi,%ebx
  800b2b:	80 fb 19             	cmp    $0x19,%bl
  800b2e:	77 08                	ja     800b38 <strtol+0x93>
			dig = *s - 'a' + 10;
  800b30:	0f be d2             	movsbl %dl,%edx
  800b33:	83 ea 57             	sub    $0x57,%edx
  800b36:	eb 10                	jmp    800b48 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800b38:	8d 72 bf             	lea    -0x41(%edx),%esi
  800b3b:	89 f3                	mov    %esi,%ebx
  800b3d:	80 fb 19             	cmp    $0x19,%bl
  800b40:	77 14                	ja     800b56 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800b42:	0f be d2             	movsbl %dl,%edx
  800b45:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800b48:	3b 55 10             	cmp    0x10(%ebp),%edx
  800b4b:	7d 09                	jge    800b56 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800b4d:	41                   	inc    %ecx
  800b4e:	0f af 45 10          	imul   0x10(%ebp),%eax
  800b52:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800b54:	eb be                	jmp    800b14 <strtol+0x6f>

	if (endptr)
  800b56:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800b5a:	74 05                	je     800b61 <strtol+0xbc>
		*endptr = (char *) s;
  800b5c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b5f:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800b61:	85 ff                	test   %edi,%edi
  800b63:	74 21                	je     800b86 <strtol+0xe1>
  800b65:	f7 d8                	neg    %eax
  800b67:	eb 1d                	jmp    800b86 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b69:	80 39 30             	cmpb   $0x30,(%ecx)
  800b6c:	75 9a                	jne    800b08 <strtol+0x63>
  800b6e:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b72:	0f 84 7a ff ff ff    	je     800af2 <strtol+0x4d>
  800b78:	eb 84                	jmp    800afe <strtol+0x59>
  800b7a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b7e:	0f 84 6e ff ff ff    	je     800af2 <strtol+0x4d>
  800b84:	eb 89                	jmp    800b0f <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800b86:	5b                   	pop    %ebx
  800b87:	5e                   	pop    %esi
  800b88:	5f                   	pop    %edi
  800b89:	5d                   	pop    %ebp
  800b8a:	c3                   	ret    

00800b8b <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800b8b:	55                   	push   %ebp
  800b8c:	89 e5                	mov    %esp,%ebp
  800b8e:	57                   	push   %edi
  800b8f:	56                   	push   %esi
  800b90:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b91:	b8 00 00 00 00       	mov    $0x0,%eax
  800b96:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b99:	8b 55 08             	mov    0x8(%ebp),%edx
  800b9c:	89 c3                	mov    %eax,%ebx
  800b9e:	89 c7                	mov    %eax,%edi
  800ba0:	89 c6                	mov    %eax,%esi
  800ba2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800ba4:	5b                   	pop    %ebx
  800ba5:	5e                   	pop    %esi
  800ba6:	5f                   	pop    %edi
  800ba7:	5d                   	pop    %ebp
  800ba8:	c3                   	ret    

00800ba9 <sys_cgetc>:

int
sys_cgetc(void)
{
  800ba9:	55                   	push   %ebp
  800baa:	89 e5                	mov    %esp,%ebp
  800bac:	57                   	push   %edi
  800bad:	56                   	push   %esi
  800bae:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800baf:	ba 00 00 00 00       	mov    $0x0,%edx
  800bb4:	b8 01 00 00 00       	mov    $0x1,%eax
  800bb9:	89 d1                	mov    %edx,%ecx
  800bbb:	89 d3                	mov    %edx,%ebx
  800bbd:	89 d7                	mov    %edx,%edi
  800bbf:	89 d6                	mov    %edx,%esi
  800bc1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800bc3:	5b                   	pop    %ebx
  800bc4:	5e                   	pop    %esi
  800bc5:	5f                   	pop    %edi
  800bc6:	5d                   	pop    %ebp
  800bc7:	c3                   	ret    

00800bc8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800bc8:	55                   	push   %ebp
  800bc9:	89 e5                	mov    %esp,%ebp
  800bcb:	57                   	push   %edi
  800bcc:	56                   	push   %esi
  800bcd:	53                   	push   %ebx
  800bce:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bd1:	b9 00 00 00 00       	mov    $0x0,%ecx
  800bd6:	b8 03 00 00 00       	mov    $0x3,%eax
  800bdb:	8b 55 08             	mov    0x8(%ebp),%edx
  800bde:	89 cb                	mov    %ecx,%ebx
  800be0:	89 cf                	mov    %ecx,%edi
  800be2:	89 ce                	mov    %ecx,%esi
  800be4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800be6:	85 c0                	test   %eax,%eax
  800be8:	7e 17                	jle    800c01 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bea:	83 ec 0c             	sub    $0xc,%esp
  800bed:	50                   	push   %eax
  800bee:	6a 03                	push   $0x3
  800bf0:	68 ff 25 80 00       	push   $0x8025ff
  800bf5:	6a 23                	push   $0x23
  800bf7:	68 1c 26 80 00       	push   $0x80261c
  800bfc:	e8 26 f6 ff ff       	call   800227 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800c01:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c04:	5b                   	pop    %ebx
  800c05:	5e                   	pop    %esi
  800c06:	5f                   	pop    %edi
  800c07:	5d                   	pop    %ebp
  800c08:	c3                   	ret    

00800c09 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800c09:	55                   	push   %ebp
  800c0a:	89 e5                	mov    %esp,%ebp
  800c0c:	57                   	push   %edi
  800c0d:	56                   	push   %esi
  800c0e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c0f:	ba 00 00 00 00       	mov    $0x0,%edx
  800c14:	b8 02 00 00 00       	mov    $0x2,%eax
  800c19:	89 d1                	mov    %edx,%ecx
  800c1b:	89 d3                	mov    %edx,%ebx
  800c1d:	89 d7                	mov    %edx,%edi
  800c1f:	89 d6                	mov    %edx,%esi
  800c21:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800c23:	5b                   	pop    %ebx
  800c24:	5e                   	pop    %esi
  800c25:	5f                   	pop    %edi
  800c26:	5d                   	pop    %ebp
  800c27:	c3                   	ret    

00800c28 <sys_yield>:

void
sys_yield(void)
{
  800c28:	55                   	push   %ebp
  800c29:	89 e5                	mov    %esp,%ebp
  800c2b:	57                   	push   %edi
  800c2c:	56                   	push   %esi
  800c2d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2e:	ba 00 00 00 00       	mov    $0x0,%edx
  800c33:	b8 0b 00 00 00       	mov    $0xb,%eax
  800c38:	89 d1                	mov    %edx,%ecx
  800c3a:	89 d3                	mov    %edx,%ebx
  800c3c:	89 d7                	mov    %edx,%edi
  800c3e:	89 d6                	mov    %edx,%esi
  800c40:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800c42:	5b                   	pop    %ebx
  800c43:	5e                   	pop    %esi
  800c44:	5f                   	pop    %edi
  800c45:	5d                   	pop    %ebp
  800c46:	c3                   	ret    

00800c47 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800c47:	55                   	push   %ebp
  800c48:	89 e5                	mov    %esp,%ebp
  800c4a:	57                   	push   %edi
  800c4b:	56                   	push   %esi
  800c4c:	53                   	push   %ebx
  800c4d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c50:	be 00 00 00 00       	mov    $0x0,%esi
  800c55:	b8 04 00 00 00       	mov    $0x4,%eax
  800c5a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c5d:	8b 55 08             	mov    0x8(%ebp),%edx
  800c60:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c63:	89 f7                	mov    %esi,%edi
  800c65:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c67:	85 c0                	test   %eax,%eax
  800c69:	7e 17                	jle    800c82 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c6b:	83 ec 0c             	sub    $0xc,%esp
  800c6e:	50                   	push   %eax
  800c6f:	6a 04                	push   $0x4
  800c71:	68 ff 25 80 00       	push   $0x8025ff
  800c76:	6a 23                	push   $0x23
  800c78:	68 1c 26 80 00       	push   $0x80261c
  800c7d:	e8 a5 f5 ff ff       	call   800227 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800c82:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c85:	5b                   	pop    %ebx
  800c86:	5e                   	pop    %esi
  800c87:	5f                   	pop    %edi
  800c88:	5d                   	pop    %ebp
  800c89:	c3                   	ret    

00800c8a <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800c8a:	55                   	push   %ebp
  800c8b:	89 e5                	mov    %esp,%ebp
  800c8d:	57                   	push   %edi
  800c8e:	56                   	push   %esi
  800c8f:	53                   	push   %ebx
  800c90:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c93:	b8 05 00 00 00       	mov    $0x5,%eax
  800c98:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c9b:	8b 55 08             	mov    0x8(%ebp),%edx
  800c9e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ca1:	8b 7d 14             	mov    0x14(%ebp),%edi
  800ca4:	8b 75 18             	mov    0x18(%ebp),%esi
  800ca7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ca9:	85 c0                	test   %eax,%eax
  800cab:	7e 17                	jle    800cc4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cad:	83 ec 0c             	sub    $0xc,%esp
  800cb0:	50                   	push   %eax
  800cb1:	6a 05                	push   $0x5
  800cb3:	68 ff 25 80 00       	push   $0x8025ff
  800cb8:	6a 23                	push   $0x23
  800cba:	68 1c 26 80 00       	push   $0x80261c
  800cbf:	e8 63 f5 ff ff       	call   800227 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800cc4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cc7:	5b                   	pop    %ebx
  800cc8:	5e                   	pop    %esi
  800cc9:	5f                   	pop    %edi
  800cca:	5d                   	pop    %ebp
  800ccb:	c3                   	ret    

00800ccc <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800ccc:	55                   	push   %ebp
  800ccd:	89 e5                	mov    %esp,%ebp
  800ccf:	57                   	push   %edi
  800cd0:	56                   	push   %esi
  800cd1:	53                   	push   %ebx
  800cd2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cd5:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cda:	b8 06 00 00 00       	mov    $0x6,%eax
  800cdf:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ce2:	8b 55 08             	mov    0x8(%ebp),%edx
  800ce5:	89 df                	mov    %ebx,%edi
  800ce7:	89 de                	mov    %ebx,%esi
  800ce9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ceb:	85 c0                	test   %eax,%eax
  800ced:	7e 17                	jle    800d06 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cef:	83 ec 0c             	sub    $0xc,%esp
  800cf2:	50                   	push   %eax
  800cf3:	6a 06                	push   $0x6
  800cf5:	68 ff 25 80 00       	push   $0x8025ff
  800cfa:	6a 23                	push   $0x23
  800cfc:	68 1c 26 80 00       	push   $0x80261c
  800d01:	e8 21 f5 ff ff       	call   800227 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800d06:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d09:	5b                   	pop    %ebx
  800d0a:	5e                   	pop    %esi
  800d0b:	5f                   	pop    %edi
  800d0c:	5d                   	pop    %ebp
  800d0d:	c3                   	ret    

00800d0e <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800d0e:	55                   	push   %ebp
  800d0f:	89 e5                	mov    %esp,%ebp
  800d11:	57                   	push   %edi
  800d12:	56                   	push   %esi
  800d13:	53                   	push   %ebx
  800d14:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d17:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d1c:	b8 08 00 00 00       	mov    $0x8,%eax
  800d21:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d24:	8b 55 08             	mov    0x8(%ebp),%edx
  800d27:	89 df                	mov    %ebx,%edi
  800d29:	89 de                	mov    %ebx,%esi
  800d2b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d2d:	85 c0                	test   %eax,%eax
  800d2f:	7e 17                	jle    800d48 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d31:	83 ec 0c             	sub    $0xc,%esp
  800d34:	50                   	push   %eax
  800d35:	6a 08                	push   $0x8
  800d37:	68 ff 25 80 00       	push   $0x8025ff
  800d3c:	6a 23                	push   $0x23
  800d3e:	68 1c 26 80 00       	push   $0x80261c
  800d43:	e8 df f4 ff ff       	call   800227 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800d48:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d4b:	5b                   	pop    %ebx
  800d4c:	5e                   	pop    %esi
  800d4d:	5f                   	pop    %edi
  800d4e:	5d                   	pop    %ebp
  800d4f:	c3                   	ret    

00800d50 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800d50:	55                   	push   %ebp
  800d51:	89 e5                	mov    %esp,%ebp
  800d53:	57                   	push   %edi
  800d54:	56                   	push   %esi
  800d55:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d56:	ba 00 00 00 00       	mov    $0x0,%edx
  800d5b:	b8 0c 00 00 00       	mov    $0xc,%eax
  800d60:	89 d1                	mov    %edx,%ecx
  800d62:	89 d3                	mov    %edx,%ebx
  800d64:	89 d7                	mov    %edx,%edi
  800d66:	89 d6                	mov    %edx,%esi
  800d68:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800d6a:	5b                   	pop    %ebx
  800d6b:	5e                   	pop    %esi
  800d6c:	5f                   	pop    %edi
  800d6d:	5d                   	pop    %ebp
  800d6e:	c3                   	ret    

00800d6f <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800d6f:	55                   	push   %ebp
  800d70:	89 e5                	mov    %esp,%ebp
  800d72:	57                   	push   %edi
  800d73:	56                   	push   %esi
  800d74:	53                   	push   %ebx
  800d75:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d78:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d7d:	b8 09 00 00 00       	mov    $0x9,%eax
  800d82:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d85:	8b 55 08             	mov    0x8(%ebp),%edx
  800d88:	89 df                	mov    %ebx,%edi
  800d8a:	89 de                	mov    %ebx,%esi
  800d8c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d8e:	85 c0                	test   %eax,%eax
  800d90:	7e 17                	jle    800da9 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d92:	83 ec 0c             	sub    $0xc,%esp
  800d95:	50                   	push   %eax
  800d96:	6a 09                	push   $0x9
  800d98:	68 ff 25 80 00       	push   $0x8025ff
  800d9d:	6a 23                	push   $0x23
  800d9f:	68 1c 26 80 00       	push   $0x80261c
  800da4:	e8 7e f4 ff ff       	call   800227 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800da9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dac:	5b                   	pop    %ebx
  800dad:	5e                   	pop    %esi
  800dae:	5f                   	pop    %edi
  800daf:	5d                   	pop    %ebp
  800db0:	c3                   	ret    

00800db1 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800db1:	55                   	push   %ebp
  800db2:	89 e5                	mov    %esp,%ebp
  800db4:	57                   	push   %edi
  800db5:	56                   	push   %esi
  800db6:	53                   	push   %ebx
  800db7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dba:	bb 00 00 00 00       	mov    $0x0,%ebx
  800dbf:	b8 0a 00 00 00       	mov    $0xa,%eax
  800dc4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800dc7:	8b 55 08             	mov    0x8(%ebp),%edx
  800dca:	89 df                	mov    %ebx,%edi
  800dcc:	89 de                	mov    %ebx,%esi
  800dce:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800dd0:	85 c0                	test   %eax,%eax
  800dd2:	7e 17                	jle    800deb <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800dd4:	83 ec 0c             	sub    $0xc,%esp
  800dd7:	50                   	push   %eax
  800dd8:	6a 0a                	push   $0xa
  800dda:	68 ff 25 80 00       	push   $0x8025ff
  800ddf:	6a 23                	push   $0x23
  800de1:	68 1c 26 80 00       	push   $0x80261c
  800de6:	e8 3c f4 ff ff       	call   800227 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800deb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dee:	5b                   	pop    %ebx
  800def:	5e                   	pop    %esi
  800df0:	5f                   	pop    %edi
  800df1:	5d                   	pop    %ebp
  800df2:	c3                   	ret    

00800df3 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800df3:	55                   	push   %ebp
  800df4:	89 e5                	mov    %esp,%ebp
  800df6:	57                   	push   %edi
  800df7:	56                   	push   %esi
  800df8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800df9:	be 00 00 00 00       	mov    $0x0,%esi
  800dfe:	b8 0d 00 00 00       	mov    $0xd,%eax
  800e03:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e06:	8b 55 08             	mov    0x8(%ebp),%edx
  800e09:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e0c:	8b 7d 14             	mov    0x14(%ebp),%edi
  800e0f:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800e11:	5b                   	pop    %ebx
  800e12:	5e                   	pop    %esi
  800e13:	5f                   	pop    %edi
  800e14:	5d                   	pop    %ebp
  800e15:	c3                   	ret    

00800e16 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800e16:	55                   	push   %ebp
  800e17:	89 e5                	mov    %esp,%ebp
  800e19:	57                   	push   %edi
  800e1a:	56                   	push   %esi
  800e1b:	53                   	push   %ebx
  800e1c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e1f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800e24:	b8 0e 00 00 00       	mov    $0xe,%eax
  800e29:	8b 55 08             	mov    0x8(%ebp),%edx
  800e2c:	89 cb                	mov    %ecx,%ebx
  800e2e:	89 cf                	mov    %ecx,%edi
  800e30:	89 ce                	mov    %ecx,%esi
  800e32:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e34:	85 c0                	test   %eax,%eax
  800e36:	7e 17                	jle    800e4f <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e38:	83 ec 0c             	sub    $0xc,%esp
  800e3b:	50                   	push   %eax
  800e3c:	6a 0e                	push   $0xe
  800e3e:	68 ff 25 80 00       	push   $0x8025ff
  800e43:	6a 23                	push   $0x23
  800e45:	68 1c 26 80 00       	push   $0x80261c
  800e4a:	e8 d8 f3 ff ff       	call   800227 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800e4f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e52:	5b                   	pop    %ebx
  800e53:	5e                   	pop    %esi
  800e54:	5f                   	pop    %edi
  800e55:	5d                   	pop    %ebp
  800e56:	c3                   	ret    

00800e57 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800e57:	55                   	push   %ebp
  800e58:	89 e5                	mov    %esp,%ebp
  800e5a:	53                   	push   %ebx
  800e5b:	83 ec 04             	sub    $0x4,%esp
  800e5e:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800e61:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800e63:	89 da                	mov    %ebx,%edx
  800e65:	c1 ea 0c             	shr    $0xc,%edx
  800e68:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800e6f:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800e73:	74 05                	je     800e7a <pgfault+0x23>
  800e75:	f6 c6 08             	test   $0x8,%dh
  800e78:	75 14                	jne    800e8e <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800e7a:	83 ec 04             	sub    $0x4,%esp
  800e7d:	68 2c 26 80 00       	push   $0x80262c
  800e82:	6a 26                	push   $0x26
  800e84:	68 90 26 80 00       	push   $0x802690
  800e89:	e8 99 f3 ff ff       	call   800227 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800e8e:	83 ec 04             	sub    $0x4,%esp
  800e91:	6a 07                	push   $0x7
  800e93:	68 00 f0 7f 00       	push   $0x7ff000
  800e98:	6a 00                	push   $0x0
  800e9a:	e8 a8 fd ff ff       	call   800c47 <sys_page_alloc>
  800e9f:	83 c4 10             	add    $0x10,%esp
  800ea2:	85 c0                	test   %eax,%eax
  800ea4:	74 14                	je     800eba <pgfault+0x63>
        panic("pgfault: no phys mem");
  800ea6:	83 ec 04             	sub    $0x4,%esp
  800ea9:	68 9b 26 80 00       	push   $0x80269b
  800eae:	6a 32                	push   $0x32
  800eb0:	68 90 26 80 00       	push   $0x802690
  800eb5:	e8 6d f3 ff ff       	call   800227 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800eba:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800ec0:	83 ec 04             	sub    $0x4,%esp
  800ec3:	68 00 10 00 00       	push   $0x1000
  800ec8:	53                   	push   %ebx
  800ec9:	68 00 f0 7f 00       	push   $0x7ff000
  800ece:	e8 05 fb ff ff       	call   8009d8 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800ed3:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800eda:	53                   	push   %ebx
  800edb:	6a 00                	push   $0x0
  800edd:	68 00 f0 7f 00       	push   $0x7ff000
  800ee2:	6a 00                	push   $0x0
  800ee4:	e8 a1 fd ff ff       	call   800c8a <sys_page_map>
  800ee9:	83 c4 20             	add    $0x20,%esp
  800eec:	85 c0                	test   %eax,%eax
  800eee:	74 14                	je     800f04 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800ef0:	83 ec 04             	sub    $0x4,%esp
  800ef3:	68 b0 26 80 00       	push   $0x8026b0
  800ef8:	6a 3f                	push   $0x3f
  800efa:	68 90 26 80 00       	push   $0x802690
  800eff:	e8 23 f3 ff ff       	call   800227 <_panic>


	//panic("pgfault not implemented");
}
  800f04:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800f07:	c9                   	leave  
  800f08:	c3                   	ret    

00800f09 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800f09:	55                   	push   %ebp
  800f0a:	89 e5                	mov    %esp,%ebp
  800f0c:	57                   	push   %edi
  800f0d:	56                   	push   %esi
  800f0e:	53                   	push   %ebx
  800f0f:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800f12:	68 57 0e 80 00       	push   $0x800e57
  800f17:	e8 b1 0e 00 00       	call   801dcd <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800f1c:	b8 07 00 00 00       	mov    $0x7,%eax
  800f21:	cd 30                	int    $0x30
  800f23:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800f26:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800f29:	83 c4 10             	add    $0x10,%esp
  800f2c:	85 c0                	test   %eax,%eax
  800f2e:	79 17                	jns    800f47 <fork+0x3e>
        panic("fork: cannot create child env");
  800f30:	83 ec 04             	sub    $0x4,%esp
  800f33:	68 c3 26 80 00       	push   $0x8026c3
  800f38:	68 97 00 00 00       	push   $0x97
  800f3d:	68 90 26 80 00       	push   $0x802690
  800f42:	e8 e0 f2 ff ff       	call   800227 <_panic>
    } else if (envid == 0) {
  800f47:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800f4b:	75 2a                	jne    800f77 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800f4d:	e8 b7 fc ff ff       	call   800c09 <sys_getenvid>
  800f52:	25 ff 03 00 00       	and    $0x3ff,%eax
  800f57:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f5e:	c1 e0 07             	shl    $0x7,%eax
  800f61:	29 d0                	sub    %edx,%eax
  800f63:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800f68:	a3 04 40 80 00       	mov    %eax,0x804004
        return 0;
  800f6d:	b8 00 00 00 00       	mov    $0x0,%eax
  800f72:	e9 94 01 00 00       	jmp    80110b <fork+0x202>
  800f77:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800f7e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800f81:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800f88:	a8 01                	test   $0x1,%al
  800f8a:	0f 84 0a 01 00 00    	je     80109a <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800f90:	c1 e7 0a             	shl    $0xa,%edi
  800f93:	be 00 00 00 00       	mov    $0x0,%esi
  800f98:	89 fb                	mov    %edi,%ebx
  800f9a:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800f9c:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800fa2:	75 34                	jne    800fd8 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800fa4:	83 ec 04             	sub    $0x4,%esp
  800fa7:	6a 07                	push   $0x7
  800fa9:	68 00 f0 bf ee       	push   $0xeebff000
  800fae:	ff 75 e4             	pushl  -0x1c(%ebp)
  800fb1:	e8 91 fc ff ff       	call   800c47 <sys_page_alloc>
  800fb6:	83 c4 10             	add    $0x10,%esp
  800fb9:	85 c0                	test   %eax,%eax
  800fbb:	0f 84 cc 00 00 00    	je     80108d <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800fc1:	83 ec 04             	sub    $0x4,%esp
  800fc4:	68 e1 26 80 00       	push   $0x8026e1
  800fc9:	68 ad 00 00 00       	push   $0xad
  800fce:	68 90 26 80 00       	push   $0x802690
  800fd3:	e8 4f f2 ff ff       	call   800227 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800fd8:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800fdf:	a8 01                	test   $0x1,%al
  800fe1:	0f 84 a6 00 00 00    	je     80108d <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800fe7:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800fee:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800ff1:	a9 02 08 00 00       	test   $0x802,%eax
  800ff6:	74 62                	je     80105a <fork+0x151>
  800ff8:	f6 c4 04             	test   $0x4,%ah
  800ffb:	75 78                	jne    801075 <fork+0x16c>
        if (sys_page_map(0,
  800ffd:	83 ec 0c             	sub    $0xc,%esp
  801000:	68 05 08 00 00       	push   $0x805
  801005:	53                   	push   %ebx
  801006:	ff 75 e4             	pushl  -0x1c(%ebp)
  801009:	53                   	push   %ebx
  80100a:	6a 00                	push   $0x0
  80100c:	e8 79 fc ff ff       	call   800c8a <sys_page_map>
  801011:	83 c4 20             	add    $0x20,%esp
  801014:	85 c0                	test   %eax,%eax
  801016:	74 14                	je     80102c <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  801018:	83 ec 04             	sub    $0x4,%esp
  80101b:	68 fc 26 80 00       	push   $0x8026fc
  801020:	6a 64                	push   $0x64
  801022:	68 90 26 80 00       	push   $0x802690
  801027:	e8 fb f1 ff ff       	call   800227 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  80102c:	83 ec 0c             	sub    $0xc,%esp
  80102f:	68 05 08 00 00       	push   $0x805
  801034:	53                   	push   %ebx
  801035:	6a 00                	push   $0x0
  801037:	53                   	push   %ebx
  801038:	6a 00                	push   $0x0
  80103a:	e8 4b fc ff ff       	call   800c8a <sys_page_map>
  80103f:	83 c4 20             	add    $0x20,%esp
  801042:	85 c0                	test   %eax,%eax
  801044:	74 47                	je     80108d <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  801046:	83 ec 04             	sub    $0x4,%esp
  801049:	68 13 27 80 00       	push   $0x802713
  80104e:	6a 6d                	push   $0x6d
  801050:	68 90 26 80 00       	push   $0x802690
  801055:	e8 cd f1 ff ff       	call   800227 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  80105a:	f6 c4 04             	test   $0x4,%ah
  80105d:	75 16                	jne    801075 <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  80105f:	83 ec 0c             	sub    $0xc,%esp
  801062:	6a 05                	push   $0x5
  801064:	53                   	push   %ebx
  801065:	ff 75 e4             	pushl  -0x1c(%ebp)
  801068:	53                   	push   %ebx
  801069:	6a 00                	push   $0x0
  80106b:	e8 1a fc ff ff       	call   800c8a <sys_page_map>
  801070:	83 c4 20             	add    $0x20,%esp
  801073:	eb 18                	jmp    80108d <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  801075:	83 ec 0c             	sub    $0xc,%esp
  801078:	25 07 0e 00 00       	and    $0xe07,%eax
  80107d:	50                   	push   %eax
  80107e:	53                   	push   %ebx
  80107f:	ff 75 e4             	pushl  -0x1c(%ebp)
  801082:	53                   	push   %ebx
  801083:	6a 00                	push   $0x0
  801085:	e8 00 fc ff ff       	call   800c8a <sys_page_map>
  80108a:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  80108d:	46                   	inc    %esi
  80108e:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  801094:	0f 85 fe fe ff ff    	jne    800f98 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80109a:	ff 45 e0             	incl   -0x20(%ebp)
  80109d:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8010a0:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  8010a5:	0f 85 d3 fe ff ff    	jne    800f7e <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  8010ab:	a1 04 40 80 00       	mov    0x804004,%eax
  8010b0:	8b 40 64             	mov    0x64(%eax),%eax
  8010b3:	83 ec 08             	sub    $0x8,%esp
  8010b6:	50                   	push   %eax
  8010b7:	ff 75 dc             	pushl  -0x24(%ebp)
  8010ba:	e8 f2 fc ff ff       	call   800db1 <sys_env_set_pgfault_upcall>
  8010bf:	83 c4 10             	add    $0x10,%esp
  8010c2:	85 c0                	test   %eax,%eax
  8010c4:	74 17                	je     8010dd <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  8010c6:	83 ec 04             	sub    $0x4,%esp
  8010c9:	68 70 26 80 00       	push   $0x802670
  8010ce:	68 b9 00 00 00       	push   $0xb9
  8010d3:	68 90 26 80 00       	push   $0x802690
  8010d8:	e8 4a f1 ff ff       	call   800227 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  8010dd:	83 ec 08             	sub    $0x8,%esp
  8010e0:	6a 02                	push   $0x2
  8010e2:	ff 75 dc             	pushl  -0x24(%ebp)
  8010e5:	e8 24 fc ff ff       	call   800d0e <sys_env_set_status>
  8010ea:	83 c4 10             	add    $0x10,%esp
  8010ed:	85 c0                	test   %eax,%eax
  8010ef:	74 17                	je     801108 <fork+0x1ff>
        panic("fork: cannot set env status");
  8010f1:	83 ec 04             	sub    $0x4,%esp
  8010f4:	68 2e 27 80 00       	push   $0x80272e
  8010f9:	68 bd 00 00 00       	push   $0xbd
  8010fe:	68 90 26 80 00       	push   $0x802690
  801103:	e8 1f f1 ff ff       	call   800227 <_panic>

    return envid;
  801108:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  80110b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80110e:	5b                   	pop    %ebx
  80110f:	5e                   	pop    %esi
  801110:	5f                   	pop    %edi
  801111:	5d                   	pop    %ebp
  801112:	c3                   	ret    

00801113 <sfork>:

// Challenge!
int
sfork(void)
{
  801113:	55                   	push   %ebp
  801114:	89 e5                	mov    %esp,%ebp
  801116:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  801119:	68 4a 27 80 00       	push   $0x80274a
  80111e:	68 c8 00 00 00       	push   $0xc8
  801123:	68 90 26 80 00       	push   $0x802690
  801128:	e8 fa f0 ff ff       	call   800227 <_panic>

0080112d <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  80112d:	55                   	push   %ebp
  80112e:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  801130:	8b 45 08             	mov    0x8(%ebp),%eax
  801133:	05 00 00 00 30       	add    $0x30000000,%eax
  801138:	c1 e8 0c             	shr    $0xc,%eax
}
  80113b:	5d                   	pop    %ebp
  80113c:	c3                   	ret    

0080113d <fd2data>:

char*
fd2data(struct Fd *fd)
{
  80113d:	55                   	push   %ebp
  80113e:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  801140:	8b 45 08             	mov    0x8(%ebp),%eax
  801143:	05 00 00 00 30       	add    $0x30000000,%eax
  801148:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  80114d:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  801152:	5d                   	pop    %ebp
  801153:	c3                   	ret    

00801154 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  801154:	55                   	push   %ebp
  801155:	89 e5                	mov    %esp,%ebp
  801157:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80115a:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  80115f:	89 c2                	mov    %eax,%edx
  801161:	c1 ea 16             	shr    $0x16,%edx
  801164:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  80116b:	f6 c2 01             	test   $0x1,%dl
  80116e:	74 11                	je     801181 <fd_alloc+0x2d>
  801170:	89 c2                	mov    %eax,%edx
  801172:	c1 ea 0c             	shr    $0xc,%edx
  801175:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80117c:	f6 c2 01             	test   $0x1,%dl
  80117f:	75 09                	jne    80118a <fd_alloc+0x36>
			*fd_store = fd;
  801181:	89 01                	mov    %eax,(%ecx)
			return 0;
  801183:	b8 00 00 00 00       	mov    $0x0,%eax
  801188:	eb 17                	jmp    8011a1 <fd_alloc+0x4d>
  80118a:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  80118f:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801194:	75 c9                	jne    80115f <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801196:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  80119c:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  8011a1:	5d                   	pop    %ebp
  8011a2:	c3                   	ret    

008011a3 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  8011a3:	55                   	push   %ebp
  8011a4:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  8011a6:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  8011aa:	77 39                	ja     8011e5 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  8011ac:	8b 45 08             	mov    0x8(%ebp),%eax
  8011af:	c1 e0 0c             	shl    $0xc,%eax
  8011b2:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  8011b7:	89 c2                	mov    %eax,%edx
  8011b9:	c1 ea 16             	shr    $0x16,%edx
  8011bc:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8011c3:	f6 c2 01             	test   $0x1,%dl
  8011c6:	74 24                	je     8011ec <fd_lookup+0x49>
  8011c8:	89 c2                	mov    %eax,%edx
  8011ca:	c1 ea 0c             	shr    $0xc,%edx
  8011cd:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8011d4:	f6 c2 01             	test   $0x1,%dl
  8011d7:	74 1a                	je     8011f3 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  8011d9:	8b 55 0c             	mov    0xc(%ebp),%edx
  8011dc:	89 02                	mov    %eax,(%edx)
	return 0;
  8011de:	b8 00 00 00 00       	mov    $0x0,%eax
  8011e3:	eb 13                	jmp    8011f8 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8011e5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011ea:	eb 0c                	jmp    8011f8 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8011ec:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011f1:	eb 05                	jmp    8011f8 <fd_lookup+0x55>
  8011f3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8011f8:	5d                   	pop    %ebp
  8011f9:	c3                   	ret    

008011fa <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8011fa:	55                   	push   %ebp
  8011fb:	89 e5                	mov    %esp,%ebp
  8011fd:	83 ec 08             	sub    $0x8,%esp
  801200:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801203:	ba dc 27 80 00       	mov    $0x8027dc,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801208:	eb 13                	jmp    80121d <dev_lookup+0x23>
  80120a:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  80120d:	39 08                	cmp    %ecx,(%eax)
  80120f:	75 0c                	jne    80121d <dev_lookup+0x23>
			*dev = devtab[i];
  801211:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801214:	89 01                	mov    %eax,(%ecx)
			return 0;
  801216:	b8 00 00 00 00       	mov    $0x0,%eax
  80121b:	eb 2e                	jmp    80124b <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  80121d:	8b 02                	mov    (%edx),%eax
  80121f:	85 c0                	test   %eax,%eax
  801221:	75 e7                	jne    80120a <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  801223:	a1 04 40 80 00       	mov    0x804004,%eax
  801228:	8b 40 48             	mov    0x48(%eax),%eax
  80122b:	83 ec 04             	sub    $0x4,%esp
  80122e:	51                   	push   %ecx
  80122f:	50                   	push   %eax
  801230:	68 60 27 80 00       	push   $0x802760
  801235:	e8 c5 f0 ff ff       	call   8002ff <cprintf>
	*dev = 0;
  80123a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80123d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  801243:	83 c4 10             	add    $0x10,%esp
  801246:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  80124b:	c9                   	leave  
  80124c:	c3                   	ret    

0080124d <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  80124d:	55                   	push   %ebp
  80124e:	89 e5                	mov    %esp,%ebp
  801250:	56                   	push   %esi
  801251:	53                   	push   %ebx
  801252:	83 ec 10             	sub    $0x10,%esp
  801255:	8b 75 08             	mov    0x8(%ebp),%esi
  801258:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  80125b:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80125e:	50                   	push   %eax
  80125f:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  801265:	c1 e8 0c             	shr    $0xc,%eax
  801268:	50                   	push   %eax
  801269:	e8 35 ff ff ff       	call   8011a3 <fd_lookup>
  80126e:	83 c4 08             	add    $0x8,%esp
  801271:	85 c0                	test   %eax,%eax
  801273:	78 05                	js     80127a <fd_close+0x2d>
	    || fd != fd2)
  801275:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  801278:	74 06                	je     801280 <fd_close+0x33>
		return (must_exist ? r : 0);
  80127a:	84 db                	test   %bl,%bl
  80127c:	74 47                	je     8012c5 <fd_close+0x78>
  80127e:	eb 4a                	jmp    8012ca <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801280:	83 ec 08             	sub    $0x8,%esp
  801283:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801286:	50                   	push   %eax
  801287:	ff 36                	pushl  (%esi)
  801289:	e8 6c ff ff ff       	call   8011fa <dev_lookup>
  80128e:	89 c3                	mov    %eax,%ebx
  801290:	83 c4 10             	add    $0x10,%esp
  801293:	85 c0                	test   %eax,%eax
  801295:	78 1c                	js     8012b3 <fd_close+0x66>
		if (dev->dev_close)
  801297:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80129a:	8b 40 10             	mov    0x10(%eax),%eax
  80129d:	85 c0                	test   %eax,%eax
  80129f:	74 0d                	je     8012ae <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  8012a1:	83 ec 0c             	sub    $0xc,%esp
  8012a4:	56                   	push   %esi
  8012a5:	ff d0                	call   *%eax
  8012a7:	89 c3                	mov    %eax,%ebx
  8012a9:	83 c4 10             	add    $0x10,%esp
  8012ac:	eb 05                	jmp    8012b3 <fd_close+0x66>
		else
			r = 0;
  8012ae:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  8012b3:	83 ec 08             	sub    $0x8,%esp
  8012b6:	56                   	push   %esi
  8012b7:	6a 00                	push   $0x0
  8012b9:	e8 0e fa ff ff       	call   800ccc <sys_page_unmap>
	return r;
  8012be:	83 c4 10             	add    $0x10,%esp
  8012c1:	89 d8                	mov    %ebx,%eax
  8012c3:	eb 05                	jmp    8012ca <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  8012c5:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  8012ca:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8012cd:	5b                   	pop    %ebx
  8012ce:	5e                   	pop    %esi
  8012cf:	5d                   	pop    %ebp
  8012d0:	c3                   	ret    

008012d1 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  8012d1:	55                   	push   %ebp
  8012d2:	89 e5                	mov    %esp,%ebp
  8012d4:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8012d7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012da:	50                   	push   %eax
  8012db:	ff 75 08             	pushl  0x8(%ebp)
  8012de:	e8 c0 fe ff ff       	call   8011a3 <fd_lookup>
  8012e3:	83 c4 08             	add    $0x8,%esp
  8012e6:	85 c0                	test   %eax,%eax
  8012e8:	78 10                	js     8012fa <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8012ea:	83 ec 08             	sub    $0x8,%esp
  8012ed:	6a 01                	push   $0x1
  8012ef:	ff 75 f4             	pushl  -0xc(%ebp)
  8012f2:	e8 56 ff ff ff       	call   80124d <fd_close>
  8012f7:	83 c4 10             	add    $0x10,%esp
}
  8012fa:	c9                   	leave  
  8012fb:	c3                   	ret    

008012fc <close_all>:

void
close_all(void)
{
  8012fc:	55                   	push   %ebp
  8012fd:	89 e5                	mov    %esp,%ebp
  8012ff:	53                   	push   %ebx
  801300:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  801303:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801308:	83 ec 0c             	sub    $0xc,%esp
  80130b:	53                   	push   %ebx
  80130c:	e8 c0 ff ff ff       	call   8012d1 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801311:	43                   	inc    %ebx
  801312:	83 c4 10             	add    $0x10,%esp
  801315:	83 fb 20             	cmp    $0x20,%ebx
  801318:	75 ee                	jne    801308 <close_all+0xc>
		close(i);
}
  80131a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80131d:	c9                   	leave  
  80131e:	c3                   	ret    

0080131f <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  80131f:	55                   	push   %ebp
  801320:	89 e5                	mov    %esp,%ebp
  801322:	57                   	push   %edi
  801323:	56                   	push   %esi
  801324:	53                   	push   %ebx
  801325:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801328:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  80132b:	50                   	push   %eax
  80132c:	ff 75 08             	pushl  0x8(%ebp)
  80132f:	e8 6f fe ff ff       	call   8011a3 <fd_lookup>
  801334:	83 c4 08             	add    $0x8,%esp
  801337:	85 c0                	test   %eax,%eax
  801339:	0f 88 c2 00 00 00    	js     801401 <dup+0xe2>
		return r;
	close(newfdnum);
  80133f:	83 ec 0c             	sub    $0xc,%esp
  801342:	ff 75 0c             	pushl  0xc(%ebp)
  801345:	e8 87 ff ff ff       	call   8012d1 <close>

	newfd = INDEX2FD(newfdnum);
  80134a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80134d:	c1 e3 0c             	shl    $0xc,%ebx
  801350:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801356:	83 c4 04             	add    $0x4,%esp
  801359:	ff 75 e4             	pushl  -0x1c(%ebp)
  80135c:	e8 dc fd ff ff       	call   80113d <fd2data>
  801361:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  801363:	89 1c 24             	mov    %ebx,(%esp)
  801366:	e8 d2 fd ff ff       	call   80113d <fd2data>
  80136b:	83 c4 10             	add    $0x10,%esp
  80136e:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801370:	89 f0                	mov    %esi,%eax
  801372:	c1 e8 16             	shr    $0x16,%eax
  801375:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  80137c:	a8 01                	test   $0x1,%al
  80137e:	74 35                	je     8013b5 <dup+0x96>
  801380:	89 f0                	mov    %esi,%eax
  801382:	c1 e8 0c             	shr    $0xc,%eax
  801385:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  80138c:	f6 c2 01             	test   $0x1,%dl
  80138f:	74 24                	je     8013b5 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801391:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801398:	83 ec 0c             	sub    $0xc,%esp
  80139b:	25 07 0e 00 00       	and    $0xe07,%eax
  8013a0:	50                   	push   %eax
  8013a1:	57                   	push   %edi
  8013a2:	6a 00                	push   $0x0
  8013a4:	56                   	push   %esi
  8013a5:	6a 00                	push   $0x0
  8013a7:	e8 de f8 ff ff       	call   800c8a <sys_page_map>
  8013ac:	89 c6                	mov    %eax,%esi
  8013ae:	83 c4 20             	add    $0x20,%esp
  8013b1:	85 c0                	test   %eax,%eax
  8013b3:	78 2c                	js     8013e1 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8013b5:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8013b8:	89 d0                	mov    %edx,%eax
  8013ba:	c1 e8 0c             	shr    $0xc,%eax
  8013bd:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8013c4:	83 ec 0c             	sub    $0xc,%esp
  8013c7:	25 07 0e 00 00       	and    $0xe07,%eax
  8013cc:	50                   	push   %eax
  8013cd:	53                   	push   %ebx
  8013ce:	6a 00                	push   $0x0
  8013d0:	52                   	push   %edx
  8013d1:	6a 00                	push   $0x0
  8013d3:	e8 b2 f8 ff ff       	call   800c8a <sys_page_map>
  8013d8:	89 c6                	mov    %eax,%esi
  8013da:	83 c4 20             	add    $0x20,%esp
  8013dd:	85 c0                	test   %eax,%eax
  8013df:	79 1d                	jns    8013fe <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8013e1:	83 ec 08             	sub    $0x8,%esp
  8013e4:	53                   	push   %ebx
  8013e5:	6a 00                	push   $0x0
  8013e7:	e8 e0 f8 ff ff       	call   800ccc <sys_page_unmap>
	sys_page_unmap(0, nva);
  8013ec:	83 c4 08             	add    $0x8,%esp
  8013ef:	57                   	push   %edi
  8013f0:	6a 00                	push   $0x0
  8013f2:	e8 d5 f8 ff ff       	call   800ccc <sys_page_unmap>
	return r;
  8013f7:	83 c4 10             	add    $0x10,%esp
  8013fa:	89 f0                	mov    %esi,%eax
  8013fc:	eb 03                	jmp    801401 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8013fe:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801401:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801404:	5b                   	pop    %ebx
  801405:	5e                   	pop    %esi
  801406:	5f                   	pop    %edi
  801407:	5d                   	pop    %ebp
  801408:	c3                   	ret    

00801409 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801409:	55                   	push   %ebp
  80140a:	89 e5                	mov    %esp,%ebp
  80140c:	53                   	push   %ebx
  80140d:	83 ec 14             	sub    $0x14,%esp
  801410:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801413:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801416:	50                   	push   %eax
  801417:	53                   	push   %ebx
  801418:	e8 86 fd ff ff       	call   8011a3 <fd_lookup>
  80141d:	83 c4 08             	add    $0x8,%esp
  801420:	85 c0                	test   %eax,%eax
  801422:	78 67                	js     80148b <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801424:	83 ec 08             	sub    $0x8,%esp
  801427:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80142a:	50                   	push   %eax
  80142b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80142e:	ff 30                	pushl  (%eax)
  801430:	e8 c5 fd ff ff       	call   8011fa <dev_lookup>
  801435:	83 c4 10             	add    $0x10,%esp
  801438:	85 c0                	test   %eax,%eax
  80143a:	78 4f                	js     80148b <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  80143c:	8b 55 f0             	mov    -0x10(%ebp),%edx
  80143f:	8b 42 08             	mov    0x8(%edx),%eax
  801442:	83 e0 03             	and    $0x3,%eax
  801445:	83 f8 01             	cmp    $0x1,%eax
  801448:	75 21                	jne    80146b <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  80144a:	a1 04 40 80 00       	mov    0x804004,%eax
  80144f:	8b 40 48             	mov    0x48(%eax),%eax
  801452:	83 ec 04             	sub    $0x4,%esp
  801455:	53                   	push   %ebx
  801456:	50                   	push   %eax
  801457:	68 a1 27 80 00       	push   $0x8027a1
  80145c:	e8 9e ee ff ff       	call   8002ff <cprintf>
		return -E_INVAL;
  801461:	83 c4 10             	add    $0x10,%esp
  801464:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801469:	eb 20                	jmp    80148b <read+0x82>
	}
	if (!dev->dev_read)
  80146b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80146e:	8b 40 08             	mov    0x8(%eax),%eax
  801471:	85 c0                	test   %eax,%eax
  801473:	74 11                	je     801486 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801475:	83 ec 04             	sub    $0x4,%esp
  801478:	ff 75 10             	pushl  0x10(%ebp)
  80147b:	ff 75 0c             	pushl  0xc(%ebp)
  80147e:	52                   	push   %edx
  80147f:	ff d0                	call   *%eax
  801481:	83 c4 10             	add    $0x10,%esp
  801484:	eb 05                	jmp    80148b <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801486:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  80148b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80148e:	c9                   	leave  
  80148f:	c3                   	ret    

00801490 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801490:	55                   	push   %ebp
  801491:	89 e5                	mov    %esp,%ebp
  801493:	57                   	push   %edi
  801494:	56                   	push   %esi
  801495:	53                   	push   %ebx
  801496:	83 ec 0c             	sub    $0xc,%esp
  801499:	8b 7d 08             	mov    0x8(%ebp),%edi
  80149c:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80149f:	bb 00 00 00 00       	mov    $0x0,%ebx
  8014a4:	eb 21                	jmp    8014c7 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  8014a6:	83 ec 04             	sub    $0x4,%esp
  8014a9:	89 f0                	mov    %esi,%eax
  8014ab:	29 d8                	sub    %ebx,%eax
  8014ad:	50                   	push   %eax
  8014ae:	89 d8                	mov    %ebx,%eax
  8014b0:	03 45 0c             	add    0xc(%ebp),%eax
  8014b3:	50                   	push   %eax
  8014b4:	57                   	push   %edi
  8014b5:	e8 4f ff ff ff       	call   801409 <read>
		if (m < 0)
  8014ba:	83 c4 10             	add    $0x10,%esp
  8014bd:	85 c0                	test   %eax,%eax
  8014bf:	78 10                	js     8014d1 <readn+0x41>
			return m;
		if (m == 0)
  8014c1:	85 c0                	test   %eax,%eax
  8014c3:	74 0a                	je     8014cf <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8014c5:	01 c3                	add    %eax,%ebx
  8014c7:	39 f3                	cmp    %esi,%ebx
  8014c9:	72 db                	jb     8014a6 <readn+0x16>
  8014cb:	89 d8                	mov    %ebx,%eax
  8014cd:	eb 02                	jmp    8014d1 <readn+0x41>
  8014cf:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  8014d1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8014d4:	5b                   	pop    %ebx
  8014d5:	5e                   	pop    %esi
  8014d6:	5f                   	pop    %edi
  8014d7:	5d                   	pop    %ebp
  8014d8:	c3                   	ret    

008014d9 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8014d9:	55                   	push   %ebp
  8014da:	89 e5                	mov    %esp,%ebp
  8014dc:	53                   	push   %ebx
  8014dd:	83 ec 14             	sub    $0x14,%esp
  8014e0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8014e3:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8014e6:	50                   	push   %eax
  8014e7:	53                   	push   %ebx
  8014e8:	e8 b6 fc ff ff       	call   8011a3 <fd_lookup>
  8014ed:	83 c4 08             	add    $0x8,%esp
  8014f0:	85 c0                	test   %eax,%eax
  8014f2:	78 62                	js     801556 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8014f4:	83 ec 08             	sub    $0x8,%esp
  8014f7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8014fa:	50                   	push   %eax
  8014fb:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8014fe:	ff 30                	pushl  (%eax)
  801500:	e8 f5 fc ff ff       	call   8011fa <dev_lookup>
  801505:	83 c4 10             	add    $0x10,%esp
  801508:	85 c0                	test   %eax,%eax
  80150a:	78 4a                	js     801556 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80150c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80150f:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801513:	75 21                	jne    801536 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801515:	a1 04 40 80 00       	mov    0x804004,%eax
  80151a:	8b 40 48             	mov    0x48(%eax),%eax
  80151d:	83 ec 04             	sub    $0x4,%esp
  801520:	53                   	push   %ebx
  801521:	50                   	push   %eax
  801522:	68 bd 27 80 00       	push   $0x8027bd
  801527:	e8 d3 ed ff ff       	call   8002ff <cprintf>
		return -E_INVAL;
  80152c:	83 c4 10             	add    $0x10,%esp
  80152f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801534:	eb 20                	jmp    801556 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801536:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801539:	8b 52 0c             	mov    0xc(%edx),%edx
  80153c:	85 d2                	test   %edx,%edx
  80153e:	74 11                	je     801551 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801540:	83 ec 04             	sub    $0x4,%esp
  801543:	ff 75 10             	pushl  0x10(%ebp)
  801546:	ff 75 0c             	pushl  0xc(%ebp)
  801549:	50                   	push   %eax
  80154a:	ff d2                	call   *%edx
  80154c:	83 c4 10             	add    $0x10,%esp
  80154f:	eb 05                	jmp    801556 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801551:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801556:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801559:	c9                   	leave  
  80155a:	c3                   	ret    

0080155b <seek>:

int
seek(int fdnum, off_t offset)
{
  80155b:	55                   	push   %ebp
  80155c:	89 e5                	mov    %esp,%ebp
  80155e:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801561:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801564:	50                   	push   %eax
  801565:	ff 75 08             	pushl  0x8(%ebp)
  801568:	e8 36 fc ff ff       	call   8011a3 <fd_lookup>
  80156d:	83 c4 08             	add    $0x8,%esp
  801570:	85 c0                	test   %eax,%eax
  801572:	78 0e                	js     801582 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801574:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801577:	8b 55 0c             	mov    0xc(%ebp),%edx
  80157a:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  80157d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801582:	c9                   	leave  
  801583:	c3                   	ret    

00801584 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801584:	55                   	push   %ebp
  801585:	89 e5                	mov    %esp,%ebp
  801587:	53                   	push   %ebx
  801588:	83 ec 14             	sub    $0x14,%esp
  80158b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  80158e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801591:	50                   	push   %eax
  801592:	53                   	push   %ebx
  801593:	e8 0b fc ff ff       	call   8011a3 <fd_lookup>
  801598:	83 c4 08             	add    $0x8,%esp
  80159b:	85 c0                	test   %eax,%eax
  80159d:	78 5f                	js     8015fe <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80159f:	83 ec 08             	sub    $0x8,%esp
  8015a2:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8015a5:	50                   	push   %eax
  8015a6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015a9:	ff 30                	pushl  (%eax)
  8015ab:	e8 4a fc ff ff       	call   8011fa <dev_lookup>
  8015b0:	83 c4 10             	add    $0x10,%esp
  8015b3:	85 c0                	test   %eax,%eax
  8015b5:	78 47                	js     8015fe <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8015b7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015ba:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8015be:	75 21                	jne    8015e1 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  8015c0:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  8015c5:	8b 40 48             	mov    0x48(%eax),%eax
  8015c8:	83 ec 04             	sub    $0x4,%esp
  8015cb:	53                   	push   %ebx
  8015cc:	50                   	push   %eax
  8015cd:	68 80 27 80 00       	push   $0x802780
  8015d2:	e8 28 ed ff ff       	call   8002ff <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8015d7:	83 c4 10             	add    $0x10,%esp
  8015da:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8015df:	eb 1d                	jmp    8015fe <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8015e1:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8015e4:	8b 52 18             	mov    0x18(%edx),%edx
  8015e7:	85 d2                	test   %edx,%edx
  8015e9:	74 0e                	je     8015f9 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8015eb:	83 ec 08             	sub    $0x8,%esp
  8015ee:	ff 75 0c             	pushl  0xc(%ebp)
  8015f1:	50                   	push   %eax
  8015f2:	ff d2                	call   *%edx
  8015f4:	83 c4 10             	add    $0x10,%esp
  8015f7:	eb 05                	jmp    8015fe <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8015f9:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8015fe:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801601:	c9                   	leave  
  801602:	c3                   	ret    

00801603 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  801603:	55                   	push   %ebp
  801604:	89 e5                	mov    %esp,%ebp
  801606:	53                   	push   %ebx
  801607:	83 ec 14             	sub    $0x14,%esp
  80160a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80160d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801610:	50                   	push   %eax
  801611:	ff 75 08             	pushl  0x8(%ebp)
  801614:	e8 8a fb ff ff       	call   8011a3 <fd_lookup>
  801619:	83 c4 08             	add    $0x8,%esp
  80161c:	85 c0                	test   %eax,%eax
  80161e:	78 52                	js     801672 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801620:	83 ec 08             	sub    $0x8,%esp
  801623:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801626:	50                   	push   %eax
  801627:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80162a:	ff 30                	pushl  (%eax)
  80162c:	e8 c9 fb ff ff       	call   8011fa <dev_lookup>
  801631:	83 c4 10             	add    $0x10,%esp
  801634:	85 c0                	test   %eax,%eax
  801636:	78 3a                	js     801672 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801638:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80163b:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  80163f:	74 2c                	je     80166d <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  801641:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  801644:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  80164b:	00 00 00 
	stat->st_isdir = 0;
  80164e:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801655:	00 00 00 
	stat->st_dev = dev;
  801658:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  80165e:	83 ec 08             	sub    $0x8,%esp
  801661:	53                   	push   %ebx
  801662:	ff 75 f0             	pushl  -0x10(%ebp)
  801665:	ff 50 14             	call   *0x14(%eax)
  801668:	83 c4 10             	add    $0x10,%esp
  80166b:	eb 05                	jmp    801672 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  80166d:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  801672:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801675:	c9                   	leave  
  801676:	c3                   	ret    

00801677 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801677:	55                   	push   %ebp
  801678:	89 e5                	mov    %esp,%ebp
  80167a:	56                   	push   %esi
  80167b:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  80167c:	83 ec 08             	sub    $0x8,%esp
  80167f:	6a 00                	push   $0x0
  801681:	ff 75 08             	pushl  0x8(%ebp)
  801684:	e8 e7 01 00 00       	call   801870 <open>
  801689:	89 c3                	mov    %eax,%ebx
  80168b:	83 c4 10             	add    $0x10,%esp
  80168e:	85 c0                	test   %eax,%eax
  801690:	78 1d                	js     8016af <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801692:	83 ec 08             	sub    $0x8,%esp
  801695:	ff 75 0c             	pushl  0xc(%ebp)
  801698:	50                   	push   %eax
  801699:	e8 65 ff ff ff       	call   801603 <fstat>
  80169e:	89 c6                	mov    %eax,%esi
	close(fd);
  8016a0:	89 1c 24             	mov    %ebx,(%esp)
  8016a3:	e8 29 fc ff ff       	call   8012d1 <close>
	return r;
  8016a8:	83 c4 10             	add    $0x10,%esp
  8016ab:	89 f0                	mov    %esi,%eax
  8016ad:	eb 00                	jmp    8016af <stat+0x38>
}
  8016af:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8016b2:	5b                   	pop    %ebx
  8016b3:	5e                   	pop    %esi
  8016b4:	5d                   	pop    %ebp
  8016b5:	c3                   	ret    

008016b6 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8016b6:	55                   	push   %ebp
  8016b7:	89 e5                	mov    %esp,%ebp
  8016b9:	56                   	push   %esi
  8016ba:	53                   	push   %ebx
  8016bb:	89 c6                	mov    %eax,%esi
  8016bd:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8016bf:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  8016c6:	75 12                	jne    8016da <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  8016c8:	83 ec 0c             	sub    $0xc,%esp
  8016cb:	6a 01                	push   $0x1
  8016cd:	e8 37 08 00 00       	call   801f09 <ipc_find_env>
  8016d2:	a3 00 40 80 00       	mov    %eax,0x804000
  8016d7:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8016da:	6a 07                	push   $0x7
  8016dc:	68 00 50 80 00       	push   $0x805000
  8016e1:	56                   	push   %esi
  8016e2:	ff 35 00 40 80 00    	pushl  0x804000
  8016e8:	e8 c7 07 00 00       	call   801eb4 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8016ed:	83 c4 0c             	add    $0xc,%esp
  8016f0:	6a 00                	push   $0x0
  8016f2:	53                   	push   %ebx
  8016f3:	6a 00                	push   $0x0
  8016f5:	e8 52 07 00 00       	call   801e4c <ipc_recv>
}
  8016fa:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8016fd:	5b                   	pop    %ebx
  8016fe:	5e                   	pop    %esi
  8016ff:	5d                   	pop    %ebp
  801700:	c3                   	ret    

00801701 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801701:	55                   	push   %ebp
  801702:	89 e5                	mov    %esp,%ebp
  801704:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801707:	8b 45 08             	mov    0x8(%ebp),%eax
  80170a:	8b 40 0c             	mov    0xc(%eax),%eax
  80170d:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  801712:	8b 45 0c             	mov    0xc(%ebp),%eax
  801715:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  80171a:	ba 00 00 00 00       	mov    $0x0,%edx
  80171f:	b8 02 00 00 00       	mov    $0x2,%eax
  801724:	e8 8d ff ff ff       	call   8016b6 <fsipc>
}
  801729:	c9                   	leave  
  80172a:	c3                   	ret    

0080172b <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  80172b:	55                   	push   %ebp
  80172c:	89 e5                	mov    %esp,%ebp
  80172e:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  801731:	8b 45 08             	mov    0x8(%ebp),%eax
  801734:	8b 40 0c             	mov    0xc(%eax),%eax
  801737:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  80173c:	ba 00 00 00 00       	mov    $0x0,%edx
  801741:	b8 06 00 00 00       	mov    $0x6,%eax
  801746:	e8 6b ff ff ff       	call   8016b6 <fsipc>
}
  80174b:	c9                   	leave  
  80174c:	c3                   	ret    

0080174d <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  80174d:	55                   	push   %ebp
  80174e:	89 e5                	mov    %esp,%ebp
  801750:	53                   	push   %ebx
  801751:	83 ec 04             	sub    $0x4,%esp
  801754:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801757:	8b 45 08             	mov    0x8(%ebp),%eax
  80175a:	8b 40 0c             	mov    0xc(%eax),%eax
  80175d:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  801762:	ba 00 00 00 00       	mov    $0x0,%edx
  801767:	b8 05 00 00 00       	mov    $0x5,%eax
  80176c:	e8 45 ff ff ff       	call   8016b6 <fsipc>
  801771:	85 c0                	test   %eax,%eax
  801773:	78 2c                	js     8017a1 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801775:	83 ec 08             	sub    $0x8,%esp
  801778:	68 00 50 80 00       	push   $0x805000
  80177d:	53                   	push   %ebx
  80177e:	e8 e0 f0 ff ff       	call   800863 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801783:	a1 80 50 80 00       	mov    0x805080,%eax
  801788:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  80178e:	a1 84 50 80 00       	mov    0x805084,%eax
  801793:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801799:	83 c4 10             	add    $0x10,%esp
  80179c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8017a1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017a4:	c9                   	leave  
  8017a5:	c3                   	ret    

008017a6 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  8017a6:	55                   	push   %ebp
  8017a7:	89 e5                	mov    %esp,%ebp
  8017a9:	83 ec 08             	sub    $0x8,%esp
  8017ac:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  8017af:	8b 55 08             	mov    0x8(%ebp),%edx
  8017b2:	8b 52 0c             	mov    0xc(%edx),%edx
  8017b5:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  8017bb:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  8017c0:	76 05                	jbe    8017c7 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  8017c2:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  8017c7:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  8017cc:	83 ec 04             	sub    $0x4,%esp
  8017cf:	50                   	push   %eax
  8017d0:	ff 75 0c             	pushl  0xc(%ebp)
  8017d3:	68 08 50 80 00       	push   $0x805008
  8017d8:	e8 fb f1 ff ff       	call   8009d8 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8017dd:	ba 00 00 00 00       	mov    $0x0,%edx
  8017e2:	b8 04 00 00 00       	mov    $0x4,%eax
  8017e7:	e8 ca fe ff ff       	call   8016b6 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8017ec:	c9                   	leave  
  8017ed:	c3                   	ret    

008017ee <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8017ee:	55                   	push   %ebp
  8017ef:	89 e5                	mov    %esp,%ebp
  8017f1:	56                   	push   %esi
  8017f2:	53                   	push   %ebx
  8017f3:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8017f6:	8b 45 08             	mov    0x8(%ebp),%eax
  8017f9:	8b 40 0c             	mov    0xc(%eax),%eax
  8017fc:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801801:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801807:	ba 00 00 00 00       	mov    $0x0,%edx
  80180c:	b8 03 00 00 00       	mov    $0x3,%eax
  801811:	e8 a0 fe ff ff       	call   8016b6 <fsipc>
  801816:	89 c3                	mov    %eax,%ebx
  801818:	85 c0                	test   %eax,%eax
  80181a:	78 4b                	js     801867 <devfile_read+0x79>
		return r;
	assert(r <= n);
  80181c:	39 c6                	cmp    %eax,%esi
  80181e:	73 16                	jae    801836 <devfile_read+0x48>
  801820:	68 ec 27 80 00       	push   $0x8027ec
  801825:	68 f3 27 80 00       	push   $0x8027f3
  80182a:	6a 7c                	push   $0x7c
  80182c:	68 08 28 80 00       	push   $0x802808
  801831:	e8 f1 e9 ff ff       	call   800227 <_panic>
	assert(r <= PGSIZE);
  801836:	3d 00 10 00 00       	cmp    $0x1000,%eax
  80183b:	7e 16                	jle    801853 <devfile_read+0x65>
  80183d:	68 13 28 80 00       	push   $0x802813
  801842:	68 f3 27 80 00       	push   $0x8027f3
  801847:	6a 7d                	push   $0x7d
  801849:	68 08 28 80 00       	push   $0x802808
  80184e:	e8 d4 e9 ff ff       	call   800227 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801853:	83 ec 04             	sub    $0x4,%esp
  801856:	50                   	push   %eax
  801857:	68 00 50 80 00       	push   $0x805000
  80185c:	ff 75 0c             	pushl  0xc(%ebp)
  80185f:	e8 74 f1 ff ff       	call   8009d8 <memmove>
	return r;
  801864:	83 c4 10             	add    $0x10,%esp
}
  801867:	89 d8                	mov    %ebx,%eax
  801869:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80186c:	5b                   	pop    %ebx
  80186d:	5e                   	pop    %esi
  80186e:	5d                   	pop    %ebp
  80186f:	c3                   	ret    

00801870 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801870:	55                   	push   %ebp
  801871:	89 e5                	mov    %esp,%ebp
  801873:	53                   	push   %ebx
  801874:	83 ec 20             	sub    $0x20,%esp
  801877:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  80187a:	53                   	push   %ebx
  80187b:	e8 ae ef ff ff       	call   80082e <strlen>
  801880:	83 c4 10             	add    $0x10,%esp
  801883:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801888:	7f 63                	jg     8018ed <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  80188a:	83 ec 0c             	sub    $0xc,%esp
  80188d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801890:	50                   	push   %eax
  801891:	e8 be f8 ff ff       	call   801154 <fd_alloc>
  801896:	83 c4 10             	add    $0x10,%esp
  801899:	85 c0                	test   %eax,%eax
  80189b:	78 55                	js     8018f2 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  80189d:	83 ec 08             	sub    $0x8,%esp
  8018a0:	53                   	push   %ebx
  8018a1:	68 00 50 80 00       	push   $0x805000
  8018a6:	e8 b8 ef ff ff       	call   800863 <strcpy>
	fsipcbuf.open.req_omode = mode;
  8018ab:	8b 45 0c             	mov    0xc(%ebp),%eax
  8018ae:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  8018b3:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8018b6:	b8 01 00 00 00       	mov    $0x1,%eax
  8018bb:	e8 f6 fd ff ff       	call   8016b6 <fsipc>
  8018c0:	89 c3                	mov    %eax,%ebx
  8018c2:	83 c4 10             	add    $0x10,%esp
  8018c5:	85 c0                	test   %eax,%eax
  8018c7:	79 14                	jns    8018dd <open+0x6d>
		fd_close(fd, 0);
  8018c9:	83 ec 08             	sub    $0x8,%esp
  8018cc:	6a 00                	push   $0x0
  8018ce:	ff 75 f4             	pushl  -0xc(%ebp)
  8018d1:	e8 77 f9 ff ff       	call   80124d <fd_close>
		return r;
  8018d6:	83 c4 10             	add    $0x10,%esp
  8018d9:	89 d8                	mov    %ebx,%eax
  8018db:	eb 15                	jmp    8018f2 <open+0x82>
	}

	return fd2num(fd);
  8018dd:	83 ec 0c             	sub    $0xc,%esp
  8018e0:	ff 75 f4             	pushl  -0xc(%ebp)
  8018e3:	e8 45 f8 ff ff       	call   80112d <fd2num>
  8018e8:	83 c4 10             	add    $0x10,%esp
  8018eb:	eb 05                	jmp    8018f2 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8018ed:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8018f2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8018f5:	c9                   	leave  
  8018f6:	c3                   	ret    

008018f7 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8018f7:	55                   	push   %ebp
  8018f8:	89 e5                	mov    %esp,%ebp
  8018fa:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8018fd:	ba 00 00 00 00       	mov    $0x0,%edx
  801902:	b8 08 00 00 00       	mov    $0x8,%eax
  801907:	e8 aa fd ff ff       	call   8016b6 <fsipc>
}
  80190c:	c9                   	leave  
  80190d:	c3                   	ret    

0080190e <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  80190e:	55                   	push   %ebp
  80190f:	89 e5                	mov    %esp,%ebp
  801911:	56                   	push   %esi
  801912:	53                   	push   %ebx
  801913:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801916:	83 ec 0c             	sub    $0xc,%esp
  801919:	ff 75 08             	pushl  0x8(%ebp)
  80191c:	e8 1c f8 ff ff       	call   80113d <fd2data>
  801921:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801923:	83 c4 08             	add    $0x8,%esp
  801926:	68 1f 28 80 00       	push   $0x80281f
  80192b:	53                   	push   %ebx
  80192c:	e8 32 ef ff ff       	call   800863 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801931:	8b 46 04             	mov    0x4(%esi),%eax
  801934:	2b 06                	sub    (%esi),%eax
  801936:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  80193c:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801943:	00 00 00 
	stat->st_dev = &devpipe;
  801946:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  80194d:	30 80 00 
	return 0;
}
  801950:	b8 00 00 00 00       	mov    $0x0,%eax
  801955:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801958:	5b                   	pop    %ebx
  801959:	5e                   	pop    %esi
  80195a:	5d                   	pop    %ebp
  80195b:	c3                   	ret    

0080195c <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  80195c:	55                   	push   %ebp
  80195d:	89 e5                	mov    %esp,%ebp
  80195f:	53                   	push   %ebx
  801960:	83 ec 0c             	sub    $0xc,%esp
  801963:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801966:	53                   	push   %ebx
  801967:	6a 00                	push   $0x0
  801969:	e8 5e f3 ff ff       	call   800ccc <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  80196e:	89 1c 24             	mov    %ebx,(%esp)
  801971:	e8 c7 f7 ff ff       	call   80113d <fd2data>
  801976:	83 c4 08             	add    $0x8,%esp
  801979:	50                   	push   %eax
  80197a:	6a 00                	push   $0x0
  80197c:	e8 4b f3 ff ff       	call   800ccc <sys_page_unmap>
}
  801981:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801984:	c9                   	leave  
  801985:	c3                   	ret    

00801986 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801986:	55                   	push   %ebp
  801987:	89 e5                	mov    %esp,%ebp
  801989:	57                   	push   %edi
  80198a:	56                   	push   %esi
  80198b:	53                   	push   %ebx
  80198c:	83 ec 1c             	sub    $0x1c,%esp
  80198f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801992:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801994:	a1 04 40 80 00       	mov    0x804004,%eax
  801999:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  80199c:	83 ec 0c             	sub    $0xc,%esp
  80199f:	ff 75 e0             	pushl  -0x20(%ebp)
  8019a2:	e8 a6 05 00 00       	call   801f4d <pageref>
  8019a7:	89 c3                	mov    %eax,%ebx
  8019a9:	89 3c 24             	mov    %edi,(%esp)
  8019ac:	e8 9c 05 00 00       	call   801f4d <pageref>
  8019b1:	83 c4 10             	add    $0x10,%esp
  8019b4:	39 c3                	cmp    %eax,%ebx
  8019b6:	0f 94 c1             	sete   %cl
  8019b9:	0f b6 c9             	movzbl %cl,%ecx
  8019bc:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  8019bf:	8b 15 04 40 80 00    	mov    0x804004,%edx
  8019c5:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  8019c8:	39 ce                	cmp    %ecx,%esi
  8019ca:	74 1b                	je     8019e7 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  8019cc:	39 c3                	cmp    %eax,%ebx
  8019ce:	75 c4                	jne    801994 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  8019d0:	8b 42 58             	mov    0x58(%edx),%eax
  8019d3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8019d6:	50                   	push   %eax
  8019d7:	56                   	push   %esi
  8019d8:	68 26 28 80 00       	push   $0x802826
  8019dd:	e8 1d e9 ff ff       	call   8002ff <cprintf>
  8019e2:	83 c4 10             	add    $0x10,%esp
  8019e5:	eb ad                	jmp    801994 <_pipeisclosed+0xe>
	}
}
  8019e7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8019ea:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8019ed:	5b                   	pop    %ebx
  8019ee:	5e                   	pop    %esi
  8019ef:	5f                   	pop    %edi
  8019f0:	5d                   	pop    %ebp
  8019f1:	c3                   	ret    

008019f2 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8019f2:	55                   	push   %ebp
  8019f3:	89 e5                	mov    %esp,%ebp
  8019f5:	57                   	push   %edi
  8019f6:	56                   	push   %esi
  8019f7:	53                   	push   %ebx
  8019f8:	83 ec 18             	sub    $0x18,%esp
  8019fb:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  8019fe:	56                   	push   %esi
  8019ff:	e8 39 f7 ff ff       	call   80113d <fd2data>
  801a04:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a06:	83 c4 10             	add    $0x10,%esp
  801a09:	bf 00 00 00 00       	mov    $0x0,%edi
  801a0e:	eb 3b                	jmp    801a4b <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801a10:	89 da                	mov    %ebx,%edx
  801a12:	89 f0                	mov    %esi,%eax
  801a14:	e8 6d ff ff ff       	call   801986 <_pipeisclosed>
  801a19:	85 c0                	test   %eax,%eax
  801a1b:	75 38                	jne    801a55 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801a1d:	e8 06 f2 ff ff       	call   800c28 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801a22:	8b 53 04             	mov    0x4(%ebx),%edx
  801a25:	8b 03                	mov    (%ebx),%eax
  801a27:	83 c0 20             	add    $0x20,%eax
  801a2a:	39 c2                	cmp    %eax,%edx
  801a2c:	73 e2                	jae    801a10 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801a2e:	8b 45 0c             	mov    0xc(%ebp),%eax
  801a31:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801a34:	89 d0                	mov    %edx,%eax
  801a36:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801a3b:	79 05                	jns    801a42 <devpipe_write+0x50>
  801a3d:	48                   	dec    %eax
  801a3e:	83 c8 e0             	or     $0xffffffe0,%eax
  801a41:	40                   	inc    %eax
  801a42:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801a46:	42                   	inc    %edx
  801a47:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a4a:	47                   	inc    %edi
  801a4b:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801a4e:	75 d2                	jne    801a22 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801a50:	8b 45 10             	mov    0x10(%ebp),%eax
  801a53:	eb 05                	jmp    801a5a <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801a55:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801a5a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a5d:	5b                   	pop    %ebx
  801a5e:	5e                   	pop    %esi
  801a5f:	5f                   	pop    %edi
  801a60:	5d                   	pop    %ebp
  801a61:	c3                   	ret    

00801a62 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801a62:	55                   	push   %ebp
  801a63:	89 e5                	mov    %esp,%ebp
  801a65:	57                   	push   %edi
  801a66:	56                   	push   %esi
  801a67:	53                   	push   %ebx
  801a68:	83 ec 18             	sub    $0x18,%esp
  801a6b:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801a6e:	57                   	push   %edi
  801a6f:	e8 c9 f6 ff ff       	call   80113d <fd2data>
  801a74:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a76:	83 c4 10             	add    $0x10,%esp
  801a79:	bb 00 00 00 00       	mov    $0x0,%ebx
  801a7e:	eb 3a                	jmp    801aba <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801a80:	85 db                	test   %ebx,%ebx
  801a82:	74 04                	je     801a88 <devpipe_read+0x26>
				return i;
  801a84:	89 d8                	mov    %ebx,%eax
  801a86:	eb 41                	jmp    801ac9 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801a88:	89 f2                	mov    %esi,%edx
  801a8a:	89 f8                	mov    %edi,%eax
  801a8c:	e8 f5 fe ff ff       	call   801986 <_pipeisclosed>
  801a91:	85 c0                	test   %eax,%eax
  801a93:	75 2f                	jne    801ac4 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801a95:	e8 8e f1 ff ff       	call   800c28 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801a9a:	8b 06                	mov    (%esi),%eax
  801a9c:	3b 46 04             	cmp    0x4(%esi),%eax
  801a9f:	74 df                	je     801a80 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801aa1:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801aa6:	79 05                	jns    801aad <devpipe_read+0x4b>
  801aa8:	48                   	dec    %eax
  801aa9:	83 c8 e0             	or     $0xffffffe0,%eax
  801aac:	40                   	inc    %eax
  801aad:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801ab1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801ab4:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801ab7:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801ab9:	43                   	inc    %ebx
  801aba:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801abd:	75 db                	jne    801a9a <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801abf:	8b 45 10             	mov    0x10(%ebp),%eax
  801ac2:	eb 05                	jmp    801ac9 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801ac4:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801ac9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801acc:	5b                   	pop    %ebx
  801acd:	5e                   	pop    %esi
  801ace:	5f                   	pop    %edi
  801acf:	5d                   	pop    %ebp
  801ad0:	c3                   	ret    

00801ad1 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801ad1:	55                   	push   %ebp
  801ad2:	89 e5                	mov    %esp,%ebp
  801ad4:	56                   	push   %esi
  801ad5:	53                   	push   %ebx
  801ad6:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801ad9:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801adc:	50                   	push   %eax
  801add:	e8 72 f6 ff ff       	call   801154 <fd_alloc>
  801ae2:	83 c4 10             	add    $0x10,%esp
  801ae5:	85 c0                	test   %eax,%eax
  801ae7:	0f 88 2a 01 00 00    	js     801c17 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801aed:	83 ec 04             	sub    $0x4,%esp
  801af0:	68 07 04 00 00       	push   $0x407
  801af5:	ff 75 f4             	pushl  -0xc(%ebp)
  801af8:	6a 00                	push   $0x0
  801afa:	e8 48 f1 ff ff       	call   800c47 <sys_page_alloc>
  801aff:	83 c4 10             	add    $0x10,%esp
  801b02:	85 c0                	test   %eax,%eax
  801b04:	0f 88 0d 01 00 00    	js     801c17 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801b0a:	83 ec 0c             	sub    $0xc,%esp
  801b0d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801b10:	50                   	push   %eax
  801b11:	e8 3e f6 ff ff       	call   801154 <fd_alloc>
  801b16:	89 c3                	mov    %eax,%ebx
  801b18:	83 c4 10             	add    $0x10,%esp
  801b1b:	85 c0                	test   %eax,%eax
  801b1d:	0f 88 e2 00 00 00    	js     801c05 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b23:	83 ec 04             	sub    $0x4,%esp
  801b26:	68 07 04 00 00       	push   $0x407
  801b2b:	ff 75 f0             	pushl  -0x10(%ebp)
  801b2e:	6a 00                	push   $0x0
  801b30:	e8 12 f1 ff ff       	call   800c47 <sys_page_alloc>
  801b35:	89 c3                	mov    %eax,%ebx
  801b37:	83 c4 10             	add    $0x10,%esp
  801b3a:	85 c0                	test   %eax,%eax
  801b3c:	0f 88 c3 00 00 00    	js     801c05 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801b42:	83 ec 0c             	sub    $0xc,%esp
  801b45:	ff 75 f4             	pushl  -0xc(%ebp)
  801b48:	e8 f0 f5 ff ff       	call   80113d <fd2data>
  801b4d:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b4f:	83 c4 0c             	add    $0xc,%esp
  801b52:	68 07 04 00 00       	push   $0x407
  801b57:	50                   	push   %eax
  801b58:	6a 00                	push   $0x0
  801b5a:	e8 e8 f0 ff ff       	call   800c47 <sys_page_alloc>
  801b5f:	89 c3                	mov    %eax,%ebx
  801b61:	83 c4 10             	add    $0x10,%esp
  801b64:	85 c0                	test   %eax,%eax
  801b66:	0f 88 89 00 00 00    	js     801bf5 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b6c:	83 ec 0c             	sub    $0xc,%esp
  801b6f:	ff 75 f0             	pushl  -0x10(%ebp)
  801b72:	e8 c6 f5 ff ff       	call   80113d <fd2data>
  801b77:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801b7e:	50                   	push   %eax
  801b7f:	6a 00                	push   $0x0
  801b81:	56                   	push   %esi
  801b82:	6a 00                	push   $0x0
  801b84:	e8 01 f1 ff ff       	call   800c8a <sys_page_map>
  801b89:	89 c3                	mov    %eax,%ebx
  801b8b:	83 c4 20             	add    $0x20,%esp
  801b8e:	85 c0                	test   %eax,%eax
  801b90:	78 55                	js     801be7 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801b92:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801b98:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b9b:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801b9d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ba0:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801ba7:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801bad:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801bb0:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801bb2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801bb5:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801bbc:	83 ec 0c             	sub    $0xc,%esp
  801bbf:	ff 75 f4             	pushl  -0xc(%ebp)
  801bc2:	e8 66 f5 ff ff       	call   80112d <fd2num>
  801bc7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801bca:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801bcc:	83 c4 04             	add    $0x4,%esp
  801bcf:	ff 75 f0             	pushl  -0x10(%ebp)
  801bd2:	e8 56 f5 ff ff       	call   80112d <fd2num>
  801bd7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801bda:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801bdd:	83 c4 10             	add    $0x10,%esp
  801be0:	b8 00 00 00 00       	mov    $0x0,%eax
  801be5:	eb 30                	jmp    801c17 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801be7:	83 ec 08             	sub    $0x8,%esp
  801bea:	56                   	push   %esi
  801beb:	6a 00                	push   $0x0
  801bed:	e8 da f0 ff ff       	call   800ccc <sys_page_unmap>
  801bf2:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801bf5:	83 ec 08             	sub    $0x8,%esp
  801bf8:	ff 75 f0             	pushl  -0x10(%ebp)
  801bfb:	6a 00                	push   $0x0
  801bfd:	e8 ca f0 ff ff       	call   800ccc <sys_page_unmap>
  801c02:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801c05:	83 ec 08             	sub    $0x8,%esp
  801c08:	ff 75 f4             	pushl  -0xc(%ebp)
  801c0b:	6a 00                	push   $0x0
  801c0d:	e8 ba f0 ff ff       	call   800ccc <sys_page_unmap>
  801c12:	83 c4 10             	add    $0x10,%esp
  801c15:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801c17:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c1a:	5b                   	pop    %ebx
  801c1b:	5e                   	pop    %esi
  801c1c:	5d                   	pop    %ebp
  801c1d:	c3                   	ret    

00801c1e <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801c1e:	55                   	push   %ebp
  801c1f:	89 e5                	mov    %esp,%ebp
  801c21:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801c24:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c27:	50                   	push   %eax
  801c28:	ff 75 08             	pushl  0x8(%ebp)
  801c2b:	e8 73 f5 ff ff       	call   8011a3 <fd_lookup>
  801c30:	83 c4 10             	add    $0x10,%esp
  801c33:	85 c0                	test   %eax,%eax
  801c35:	78 18                	js     801c4f <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801c37:	83 ec 0c             	sub    $0xc,%esp
  801c3a:	ff 75 f4             	pushl  -0xc(%ebp)
  801c3d:	e8 fb f4 ff ff       	call   80113d <fd2data>
	return _pipeisclosed(fd, p);
  801c42:	89 c2                	mov    %eax,%edx
  801c44:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c47:	e8 3a fd ff ff       	call   801986 <_pipeisclosed>
  801c4c:	83 c4 10             	add    $0x10,%esp
}
  801c4f:	c9                   	leave  
  801c50:	c3                   	ret    

00801c51 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801c51:	55                   	push   %ebp
  801c52:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801c54:	b8 00 00 00 00       	mov    $0x0,%eax
  801c59:	5d                   	pop    %ebp
  801c5a:	c3                   	ret    

00801c5b <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801c5b:	55                   	push   %ebp
  801c5c:	89 e5                	mov    %esp,%ebp
  801c5e:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801c61:	68 3e 28 80 00       	push   $0x80283e
  801c66:	ff 75 0c             	pushl  0xc(%ebp)
  801c69:	e8 f5 eb ff ff       	call   800863 <strcpy>
	return 0;
}
  801c6e:	b8 00 00 00 00       	mov    $0x0,%eax
  801c73:	c9                   	leave  
  801c74:	c3                   	ret    

00801c75 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801c75:	55                   	push   %ebp
  801c76:	89 e5                	mov    %esp,%ebp
  801c78:	57                   	push   %edi
  801c79:	56                   	push   %esi
  801c7a:	53                   	push   %ebx
  801c7b:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801c81:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801c86:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801c8c:	eb 2c                	jmp    801cba <devcons_write+0x45>
		m = n - tot;
  801c8e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801c91:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801c93:	83 fb 7f             	cmp    $0x7f,%ebx
  801c96:	76 05                	jbe    801c9d <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801c98:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801c9d:	83 ec 04             	sub    $0x4,%esp
  801ca0:	53                   	push   %ebx
  801ca1:	03 45 0c             	add    0xc(%ebp),%eax
  801ca4:	50                   	push   %eax
  801ca5:	57                   	push   %edi
  801ca6:	e8 2d ed ff ff       	call   8009d8 <memmove>
		sys_cputs(buf, m);
  801cab:	83 c4 08             	add    $0x8,%esp
  801cae:	53                   	push   %ebx
  801caf:	57                   	push   %edi
  801cb0:	e8 d6 ee ff ff       	call   800b8b <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801cb5:	01 de                	add    %ebx,%esi
  801cb7:	83 c4 10             	add    $0x10,%esp
  801cba:	89 f0                	mov    %esi,%eax
  801cbc:	3b 75 10             	cmp    0x10(%ebp),%esi
  801cbf:	72 cd                	jb     801c8e <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801cc1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801cc4:	5b                   	pop    %ebx
  801cc5:	5e                   	pop    %esi
  801cc6:	5f                   	pop    %edi
  801cc7:	5d                   	pop    %ebp
  801cc8:	c3                   	ret    

00801cc9 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801cc9:	55                   	push   %ebp
  801cca:	89 e5                	mov    %esp,%ebp
  801ccc:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801ccf:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801cd3:	75 07                	jne    801cdc <devcons_read+0x13>
  801cd5:	eb 23                	jmp    801cfa <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801cd7:	e8 4c ef ff ff       	call   800c28 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801cdc:	e8 c8 ee ff ff       	call   800ba9 <sys_cgetc>
  801ce1:	85 c0                	test   %eax,%eax
  801ce3:	74 f2                	je     801cd7 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801ce5:	85 c0                	test   %eax,%eax
  801ce7:	78 1d                	js     801d06 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801ce9:	83 f8 04             	cmp    $0x4,%eax
  801cec:	74 13                	je     801d01 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801cee:	8b 55 0c             	mov    0xc(%ebp),%edx
  801cf1:	88 02                	mov    %al,(%edx)
	return 1;
  801cf3:	b8 01 00 00 00       	mov    $0x1,%eax
  801cf8:	eb 0c                	jmp    801d06 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801cfa:	b8 00 00 00 00       	mov    $0x0,%eax
  801cff:	eb 05                	jmp    801d06 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801d01:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801d06:	c9                   	leave  
  801d07:	c3                   	ret    

00801d08 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801d08:	55                   	push   %ebp
  801d09:	89 e5                	mov    %esp,%ebp
  801d0b:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801d0e:	8b 45 08             	mov    0x8(%ebp),%eax
  801d11:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801d14:	6a 01                	push   $0x1
  801d16:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801d19:	50                   	push   %eax
  801d1a:	e8 6c ee ff ff       	call   800b8b <sys_cputs>
}
  801d1f:	83 c4 10             	add    $0x10,%esp
  801d22:	c9                   	leave  
  801d23:	c3                   	ret    

00801d24 <getchar>:

int
getchar(void)
{
  801d24:	55                   	push   %ebp
  801d25:	89 e5                	mov    %esp,%ebp
  801d27:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801d2a:	6a 01                	push   $0x1
  801d2c:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801d2f:	50                   	push   %eax
  801d30:	6a 00                	push   $0x0
  801d32:	e8 d2 f6 ff ff       	call   801409 <read>
	if (r < 0)
  801d37:	83 c4 10             	add    $0x10,%esp
  801d3a:	85 c0                	test   %eax,%eax
  801d3c:	78 0f                	js     801d4d <getchar+0x29>
		return r;
	if (r < 1)
  801d3e:	85 c0                	test   %eax,%eax
  801d40:	7e 06                	jle    801d48 <getchar+0x24>
		return -E_EOF;
	return c;
  801d42:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801d46:	eb 05                	jmp    801d4d <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801d48:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801d4d:	c9                   	leave  
  801d4e:	c3                   	ret    

00801d4f <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801d4f:	55                   	push   %ebp
  801d50:	89 e5                	mov    %esp,%ebp
  801d52:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801d55:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801d58:	50                   	push   %eax
  801d59:	ff 75 08             	pushl  0x8(%ebp)
  801d5c:	e8 42 f4 ff ff       	call   8011a3 <fd_lookup>
  801d61:	83 c4 10             	add    $0x10,%esp
  801d64:	85 c0                	test   %eax,%eax
  801d66:	78 11                	js     801d79 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801d68:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801d6b:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801d71:	39 10                	cmp    %edx,(%eax)
  801d73:	0f 94 c0             	sete   %al
  801d76:	0f b6 c0             	movzbl %al,%eax
}
  801d79:	c9                   	leave  
  801d7a:	c3                   	ret    

00801d7b <opencons>:

int
opencons(void)
{
  801d7b:	55                   	push   %ebp
  801d7c:	89 e5                	mov    %esp,%ebp
  801d7e:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801d81:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801d84:	50                   	push   %eax
  801d85:	e8 ca f3 ff ff       	call   801154 <fd_alloc>
  801d8a:	83 c4 10             	add    $0x10,%esp
  801d8d:	85 c0                	test   %eax,%eax
  801d8f:	78 3a                	js     801dcb <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801d91:	83 ec 04             	sub    $0x4,%esp
  801d94:	68 07 04 00 00       	push   $0x407
  801d99:	ff 75 f4             	pushl  -0xc(%ebp)
  801d9c:	6a 00                	push   $0x0
  801d9e:	e8 a4 ee ff ff       	call   800c47 <sys_page_alloc>
  801da3:	83 c4 10             	add    $0x10,%esp
  801da6:	85 c0                	test   %eax,%eax
  801da8:	78 21                	js     801dcb <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801daa:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801db0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801db3:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801db5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801db8:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801dbf:	83 ec 0c             	sub    $0xc,%esp
  801dc2:	50                   	push   %eax
  801dc3:	e8 65 f3 ff ff       	call   80112d <fd2num>
  801dc8:	83 c4 10             	add    $0x10,%esp
}
  801dcb:	c9                   	leave  
  801dcc:	c3                   	ret    

00801dcd <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801dcd:	55                   	push   %ebp
  801dce:	89 e5                	mov    %esp,%ebp
  801dd0:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801dd3:	83 3d 00 60 80 00 00 	cmpl   $0x0,0x806000
  801dda:	75 3e                	jne    801e1a <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801ddc:	83 ec 04             	sub    $0x4,%esp
  801ddf:	6a 07                	push   $0x7
  801de1:	68 00 f0 bf ee       	push   $0xeebff000
  801de6:	6a 00                	push   $0x0
  801de8:	e8 5a ee ff ff       	call   800c47 <sys_page_alloc>
  801ded:	83 c4 10             	add    $0x10,%esp
  801df0:	85 c0                	test   %eax,%eax
  801df2:	74 14                	je     801e08 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801df4:	83 ec 04             	sub    $0x4,%esp
  801df7:	68 4c 28 80 00       	push   $0x80284c
  801dfc:	6a 23                	push   $0x23
  801dfe:	68 70 28 80 00       	push   $0x802870
  801e03:	e8 1f e4 ff ff       	call   800227 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801e08:	83 ec 08             	sub    $0x8,%esp
  801e0b:	68 24 1e 80 00       	push   $0x801e24
  801e10:	6a 00                	push   $0x0
  801e12:	e8 9a ef ff ff       	call   800db1 <sys_env_set_pgfault_upcall>
  801e17:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801e1a:	8b 45 08             	mov    0x8(%ebp),%eax
  801e1d:	a3 00 60 80 00       	mov    %eax,0x806000
}
  801e22:	c9                   	leave  
  801e23:	c3                   	ret    

00801e24 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801e24:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801e25:	a1 00 60 80 00       	mov    0x806000,%eax
	call *%eax
  801e2a:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801e2c:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801e2f:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801e31:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801e35:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801e39:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801e3a:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801e3d:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801e3f:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801e42:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801e45:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801e46:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  801e49:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801e4a:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801e4b:	c3                   	ret    

00801e4c <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801e4c:	55                   	push   %ebp
  801e4d:	89 e5                	mov    %esp,%ebp
  801e4f:	56                   	push   %esi
  801e50:	53                   	push   %ebx
  801e51:	8b 75 08             	mov    0x8(%ebp),%esi
  801e54:	8b 45 0c             	mov    0xc(%ebp),%eax
  801e57:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801e5a:	85 c0                	test   %eax,%eax
  801e5c:	75 05                	jne    801e63 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801e5e:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801e63:	83 ec 0c             	sub    $0xc,%esp
  801e66:	50                   	push   %eax
  801e67:	e8 aa ef ff ff       	call   800e16 <sys_ipc_recv>
  801e6c:	83 c4 10             	add    $0x10,%esp
  801e6f:	85 c0                	test   %eax,%eax
  801e71:	74 16                	je     801e89 <ipc_recv+0x3d>
        if (from_env_store)
  801e73:	85 f6                	test   %esi,%esi
  801e75:	74 06                	je     801e7d <ipc_recv+0x31>
            *from_env_store = 0;
  801e77:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801e7d:	85 db                	test   %ebx,%ebx
  801e7f:	74 2c                	je     801ead <ipc_recv+0x61>
            *perm_store = 0;
  801e81:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801e87:	eb 24                	jmp    801ead <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801e89:	85 f6                	test   %esi,%esi
  801e8b:	74 0a                	je     801e97 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801e8d:	a1 04 40 80 00       	mov    0x804004,%eax
  801e92:	8b 40 74             	mov    0x74(%eax),%eax
  801e95:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801e97:	85 db                	test   %ebx,%ebx
  801e99:	74 0a                	je     801ea5 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801e9b:	a1 04 40 80 00       	mov    0x804004,%eax
  801ea0:	8b 40 78             	mov    0x78(%eax),%eax
  801ea3:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801ea5:	a1 04 40 80 00       	mov    0x804004,%eax
  801eaa:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801ead:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801eb0:	5b                   	pop    %ebx
  801eb1:	5e                   	pop    %esi
  801eb2:	5d                   	pop    %ebp
  801eb3:	c3                   	ret    

00801eb4 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801eb4:	55                   	push   %ebp
  801eb5:	89 e5                	mov    %esp,%ebp
  801eb7:	57                   	push   %edi
  801eb8:	56                   	push   %esi
  801eb9:	53                   	push   %ebx
  801eba:	83 ec 0c             	sub    $0xc,%esp
  801ebd:	8b 75 0c             	mov    0xc(%ebp),%esi
  801ec0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801ec3:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801ec6:	85 db                	test   %ebx,%ebx
  801ec8:	75 0c                	jne    801ed6 <ipc_send+0x22>
        pg = (void *)UTOP;
  801eca:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801ecf:	eb 05                	jmp    801ed6 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801ed1:	e8 52 ed ff ff       	call   800c28 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801ed6:	57                   	push   %edi
  801ed7:	53                   	push   %ebx
  801ed8:	56                   	push   %esi
  801ed9:	ff 75 08             	pushl  0x8(%ebp)
  801edc:	e8 12 ef ff ff       	call   800df3 <sys_ipc_try_send>
  801ee1:	83 c4 10             	add    $0x10,%esp
  801ee4:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801ee7:	74 e8                	je     801ed1 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801ee9:	85 c0                	test   %eax,%eax
  801eeb:	74 14                	je     801f01 <ipc_send+0x4d>
        panic("ipc_send: error");
  801eed:	83 ec 04             	sub    $0x4,%esp
  801ef0:	68 7e 28 80 00       	push   $0x80287e
  801ef5:	6a 6a                	push   $0x6a
  801ef7:	68 8e 28 80 00       	push   $0x80288e
  801efc:	e8 26 e3 ff ff       	call   800227 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801f01:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f04:	5b                   	pop    %ebx
  801f05:	5e                   	pop    %esi
  801f06:	5f                   	pop    %edi
  801f07:	5d                   	pop    %ebp
  801f08:	c3                   	ret    

00801f09 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801f09:	55                   	push   %ebp
  801f0a:	89 e5                	mov    %esp,%ebp
  801f0c:	53                   	push   %ebx
  801f0d:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801f10:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801f15:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801f1c:	89 d0                	mov    %edx,%eax
  801f1e:	c1 e0 07             	shl    $0x7,%eax
  801f21:	29 d8                	sub    %ebx,%eax
  801f23:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801f28:	8b 40 50             	mov    0x50(%eax),%eax
  801f2b:	39 c8                	cmp    %ecx,%eax
  801f2d:	75 0d                	jne    801f3c <ipc_find_env+0x33>
			return envs[i].env_id;
  801f2f:	c1 e2 07             	shl    $0x7,%edx
  801f32:	29 da                	sub    %ebx,%edx
  801f34:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801f3a:	eb 0e                	jmp    801f4a <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801f3c:	42                   	inc    %edx
  801f3d:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801f43:	75 d0                	jne    801f15 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801f45:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801f4a:	5b                   	pop    %ebx
  801f4b:	5d                   	pop    %ebp
  801f4c:	c3                   	ret    

00801f4d <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801f4d:	55                   	push   %ebp
  801f4e:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801f50:	8b 45 08             	mov    0x8(%ebp),%eax
  801f53:	c1 e8 16             	shr    $0x16,%eax
  801f56:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801f5d:	a8 01                	test   $0x1,%al
  801f5f:	74 21                	je     801f82 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801f61:	8b 45 08             	mov    0x8(%ebp),%eax
  801f64:	c1 e8 0c             	shr    $0xc,%eax
  801f67:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801f6e:	a8 01                	test   $0x1,%al
  801f70:	74 17                	je     801f89 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801f72:	c1 e8 0c             	shr    $0xc,%eax
  801f75:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801f7c:	ef 
  801f7d:	0f b7 c0             	movzwl %ax,%eax
  801f80:	eb 0c                	jmp    801f8e <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801f82:	b8 00 00 00 00       	mov    $0x0,%eax
  801f87:	eb 05                	jmp    801f8e <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801f89:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801f8e:	5d                   	pop    %ebp
  801f8f:	c3                   	ret    

00801f90 <__udivdi3>:
  801f90:	55                   	push   %ebp
  801f91:	57                   	push   %edi
  801f92:	56                   	push   %esi
  801f93:	53                   	push   %ebx
  801f94:	83 ec 1c             	sub    $0x1c,%esp
  801f97:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801f9b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801f9f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801fa3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801fa7:	89 ca                	mov    %ecx,%edx
  801fa9:	89 f8                	mov    %edi,%eax
  801fab:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801faf:	85 f6                	test   %esi,%esi
  801fb1:	75 2d                	jne    801fe0 <__udivdi3+0x50>
  801fb3:	39 cf                	cmp    %ecx,%edi
  801fb5:	77 65                	ja     80201c <__udivdi3+0x8c>
  801fb7:	89 fd                	mov    %edi,%ebp
  801fb9:	85 ff                	test   %edi,%edi
  801fbb:	75 0b                	jne    801fc8 <__udivdi3+0x38>
  801fbd:	b8 01 00 00 00       	mov    $0x1,%eax
  801fc2:	31 d2                	xor    %edx,%edx
  801fc4:	f7 f7                	div    %edi
  801fc6:	89 c5                	mov    %eax,%ebp
  801fc8:	31 d2                	xor    %edx,%edx
  801fca:	89 c8                	mov    %ecx,%eax
  801fcc:	f7 f5                	div    %ebp
  801fce:	89 c1                	mov    %eax,%ecx
  801fd0:	89 d8                	mov    %ebx,%eax
  801fd2:	f7 f5                	div    %ebp
  801fd4:	89 cf                	mov    %ecx,%edi
  801fd6:	89 fa                	mov    %edi,%edx
  801fd8:	83 c4 1c             	add    $0x1c,%esp
  801fdb:	5b                   	pop    %ebx
  801fdc:	5e                   	pop    %esi
  801fdd:	5f                   	pop    %edi
  801fde:	5d                   	pop    %ebp
  801fdf:	c3                   	ret    
  801fe0:	39 ce                	cmp    %ecx,%esi
  801fe2:	77 28                	ja     80200c <__udivdi3+0x7c>
  801fe4:	0f bd fe             	bsr    %esi,%edi
  801fe7:	83 f7 1f             	xor    $0x1f,%edi
  801fea:	75 40                	jne    80202c <__udivdi3+0x9c>
  801fec:	39 ce                	cmp    %ecx,%esi
  801fee:	72 0a                	jb     801ffa <__udivdi3+0x6a>
  801ff0:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801ff4:	0f 87 9e 00 00 00    	ja     802098 <__udivdi3+0x108>
  801ffa:	b8 01 00 00 00       	mov    $0x1,%eax
  801fff:	89 fa                	mov    %edi,%edx
  802001:	83 c4 1c             	add    $0x1c,%esp
  802004:	5b                   	pop    %ebx
  802005:	5e                   	pop    %esi
  802006:	5f                   	pop    %edi
  802007:	5d                   	pop    %ebp
  802008:	c3                   	ret    
  802009:	8d 76 00             	lea    0x0(%esi),%esi
  80200c:	31 ff                	xor    %edi,%edi
  80200e:	31 c0                	xor    %eax,%eax
  802010:	89 fa                	mov    %edi,%edx
  802012:	83 c4 1c             	add    $0x1c,%esp
  802015:	5b                   	pop    %ebx
  802016:	5e                   	pop    %esi
  802017:	5f                   	pop    %edi
  802018:	5d                   	pop    %ebp
  802019:	c3                   	ret    
  80201a:	66 90                	xchg   %ax,%ax
  80201c:	89 d8                	mov    %ebx,%eax
  80201e:	f7 f7                	div    %edi
  802020:	31 ff                	xor    %edi,%edi
  802022:	89 fa                	mov    %edi,%edx
  802024:	83 c4 1c             	add    $0x1c,%esp
  802027:	5b                   	pop    %ebx
  802028:	5e                   	pop    %esi
  802029:	5f                   	pop    %edi
  80202a:	5d                   	pop    %ebp
  80202b:	c3                   	ret    
  80202c:	bd 20 00 00 00       	mov    $0x20,%ebp
  802031:	89 eb                	mov    %ebp,%ebx
  802033:	29 fb                	sub    %edi,%ebx
  802035:	89 f9                	mov    %edi,%ecx
  802037:	d3 e6                	shl    %cl,%esi
  802039:	89 c5                	mov    %eax,%ebp
  80203b:	88 d9                	mov    %bl,%cl
  80203d:	d3 ed                	shr    %cl,%ebp
  80203f:	89 e9                	mov    %ebp,%ecx
  802041:	09 f1                	or     %esi,%ecx
  802043:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  802047:	89 f9                	mov    %edi,%ecx
  802049:	d3 e0                	shl    %cl,%eax
  80204b:	89 c5                	mov    %eax,%ebp
  80204d:	89 d6                	mov    %edx,%esi
  80204f:	88 d9                	mov    %bl,%cl
  802051:	d3 ee                	shr    %cl,%esi
  802053:	89 f9                	mov    %edi,%ecx
  802055:	d3 e2                	shl    %cl,%edx
  802057:	8b 44 24 08          	mov    0x8(%esp),%eax
  80205b:	88 d9                	mov    %bl,%cl
  80205d:	d3 e8                	shr    %cl,%eax
  80205f:	09 c2                	or     %eax,%edx
  802061:	89 d0                	mov    %edx,%eax
  802063:	89 f2                	mov    %esi,%edx
  802065:	f7 74 24 0c          	divl   0xc(%esp)
  802069:	89 d6                	mov    %edx,%esi
  80206b:	89 c3                	mov    %eax,%ebx
  80206d:	f7 e5                	mul    %ebp
  80206f:	39 d6                	cmp    %edx,%esi
  802071:	72 19                	jb     80208c <__udivdi3+0xfc>
  802073:	74 0b                	je     802080 <__udivdi3+0xf0>
  802075:	89 d8                	mov    %ebx,%eax
  802077:	31 ff                	xor    %edi,%edi
  802079:	e9 58 ff ff ff       	jmp    801fd6 <__udivdi3+0x46>
  80207e:	66 90                	xchg   %ax,%ax
  802080:	8b 54 24 08          	mov    0x8(%esp),%edx
  802084:	89 f9                	mov    %edi,%ecx
  802086:	d3 e2                	shl    %cl,%edx
  802088:	39 c2                	cmp    %eax,%edx
  80208a:	73 e9                	jae    802075 <__udivdi3+0xe5>
  80208c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80208f:	31 ff                	xor    %edi,%edi
  802091:	e9 40 ff ff ff       	jmp    801fd6 <__udivdi3+0x46>
  802096:	66 90                	xchg   %ax,%ax
  802098:	31 c0                	xor    %eax,%eax
  80209a:	e9 37 ff ff ff       	jmp    801fd6 <__udivdi3+0x46>
  80209f:	90                   	nop

008020a0 <__umoddi3>:
  8020a0:	55                   	push   %ebp
  8020a1:	57                   	push   %edi
  8020a2:	56                   	push   %esi
  8020a3:	53                   	push   %ebx
  8020a4:	83 ec 1c             	sub    $0x1c,%esp
  8020a7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8020ab:	8b 74 24 34          	mov    0x34(%esp),%esi
  8020af:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8020b3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8020b7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8020bb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8020bf:	89 f3                	mov    %esi,%ebx
  8020c1:	89 fa                	mov    %edi,%edx
  8020c3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8020c7:	89 34 24             	mov    %esi,(%esp)
  8020ca:	85 c0                	test   %eax,%eax
  8020cc:	75 1a                	jne    8020e8 <__umoddi3+0x48>
  8020ce:	39 f7                	cmp    %esi,%edi
  8020d0:	0f 86 a2 00 00 00    	jbe    802178 <__umoddi3+0xd8>
  8020d6:	89 c8                	mov    %ecx,%eax
  8020d8:	89 f2                	mov    %esi,%edx
  8020da:	f7 f7                	div    %edi
  8020dc:	89 d0                	mov    %edx,%eax
  8020de:	31 d2                	xor    %edx,%edx
  8020e0:	83 c4 1c             	add    $0x1c,%esp
  8020e3:	5b                   	pop    %ebx
  8020e4:	5e                   	pop    %esi
  8020e5:	5f                   	pop    %edi
  8020e6:	5d                   	pop    %ebp
  8020e7:	c3                   	ret    
  8020e8:	39 f0                	cmp    %esi,%eax
  8020ea:	0f 87 ac 00 00 00    	ja     80219c <__umoddi3+0xfc>
  8020f0:	0f bd e8             	bsr    %eax,%ebp
  8020f3:	83 f5 1f             	xor    $0x1f,%ebp
  8020f6:	0f 84 ac 00 00 00    	je     8021a8 <__umoddi3+0x108>
  8020fc:	bf 20 00 00 00       	mov    $0x20,%edi
  802101:	29 ef                	sub    %ebp,%edi
  802103:	89 fe                	mov    %edi,%esi
  802105:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802109:	89 e9                	mov    %ebp,%ecx
  80210b:	d3 e0                	shl    %cl,%eax
  80210d:	89 d7                	mov    %edx,%edi
  80210f:	89 f1                	mov    %esi,%ecx
  802111:	d3 ef                	shr    %cl,%edi
  802113:	09 c7                	or     %eax,%edi
  802115:	89 e9                	mov    %ebp,%ecx
  802117:	d3 e2                	shl    %cl,%edx
  802119:	89 14 24             	mov    %edx,(%esp)
  80211c:	89 d8                	mov    %ebx,%eax
  80211e:	d3 e0                	shl    %cl,%eax
  802120:	89 c2                	mov    %eax,%edx
  802122:	8b 44 24 08          	mov    0x8(%esp),%eax
  802126:	d3 e0                	shl    %cl,%eax
  802128:	89 44 24 04          	mov    %eax,0x4(%esp)
  80212c:	8b 44 24 08          	mov    0x8(%esp),%eax
  802130:	89 f1                	mov    %esi,%ecx
  802132:	d3 e8                	shr    %cl,%eax
  802134:	09 d0                	or     %edx,%eax
  802136:	d3 eb                	shr    %cl,%ebx
  802138:	89 da                	mov    %ebx,%edx
  80213a:	f7 f7                	div    %edi
  80213c:	89 d3                	mov    %edx,%ebx
  80213e:	f7 24 24             	mull   (%esp)
  802141:	89 c6                	mov    %eax,%esi
  802143:	89 d1                	mov    %edx,%ecx
  802145:	39 d3                	cmp    %edx,%ebx
  802147:	0f 82 87 00 00 00    	jb     8021d4 <__umoddi3+0x134>
  80214d:	0f 84 91 00 00 00    	je     8021e4 <__umoddi3+0x144>
  802153:	8b 54 24 04          	mov    0x4(%esp),%edx
  802157:	29 f2                	sub    %esi,%edx
  802159:	19 cb                	sbb    %ecx,%ebx
  80215b:	89 d8                	mov    %ebx,%eax
  80215d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  802161:	d3 e0                	shl    %cl,%eax
  802163:	89 e9                	mov    %ebp,%ecx
  802165:	d3 ea                	shr    %cl,%edx
  802167:	09 d0                	or     %edx,%eax
  802169:	89 e9                	mov    %ebp,%ecx
  80216b:	d3 eb                	shr    %cl,%ebx
  80216d:	89 da                	mov    %ebx,%edx
  80216f:	83 c4 1c             	add    $0x1c,%esp
  802172:	5b                   	pop    %ebx
  802173:	5e                   	pop    %esi
  802174:	5f                   	pop    %edi
  802175:	5d                   	pop    %ebp
  802176:	c3                   	ret    
  802177:	90                   	nop
  802178:	89 fd                	mov    %edi,%ebp
  80217a:	85 ff                	test   %edi,%edi
  80217c:	75 0b                	jne    802189 <__umoddi3+0xe9>
  80217e:	b8 01 00 00 00       	mov    $0x1,%eax
  802183:	31 d2                	xor    %edx,%edx
  802185:	f7 f7                	div    %edi
  802187:	89 c5                	mov    %eax,%ebp
  802189:	89 f0                	mov    %esi,%eax
  80218b:	31 d2                	xor    %edx,%edx
  80218d:	f7 f5                	div    %ebp
  80218f:	89 c8                	mov    %ecx,%eax
  802191:	f7 f5                	div    %ebp
  802193:	89 d0                	mov    %edx,%eax
  802195:	e9 44 ff ff ff       	jmp    8020de <__umoddi3+0x3e>
  80219a:	66 90                	xchg   %ax,%ax
  80219c:	89 c8                	mov    %ecx,%eax
  80219e:	89 f2                	mov    %esi,%edx
  8021a0:	83 c4 1c             	add    $0x1c,%esp
  8021a3:	5b                   	pop    %ebx
  8021a4:	5e                   	pop    %esi
  8021a5:	5f                   	pop    %edi
  8021a6:	5d                   	pop    %ebp
  8021a7:	c3                   	ret    
  8021a8:	3b 04 24             	cmp    (%esp),%eax
  8021ab:	72 06                	jb     8021b3 <__umoddi3+0x113>
  8021ad:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8021b1:	77 0f                	ja     8021c2 <__umoddi3+0x122>
  8021b3:	89 f2                	mov    %esi,%edx
  8021b5:	29 f9                	sub    %edi,%ecx
  8021b7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8021bb:	89 14 24             	mov    %edx,(%esp)
  8021be:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8021c2:	8b 44 24 04          	mov    0x4(%esp),%eax
  8021c6:	8b 14 24             	mov    (%esp),%edx
  8021c9:	83 c4 1c             	add    $0x1c,%esp
  8021cc:	5b                   	pop    %ebx
  8021cd:	5e                   	pop    %esi
  8021ce:	5f                   	pop    %edi
  8021cf:	5d                   	pop    %ebp
  8021d0:	c3                   	ret    
  8021d1:	8d 76 00             	lea    0x0(%esi),%esi
  8021d4:	2b 04 24             	sub    (%esp),%eax
  8021d7:	19 fa                	sbb    %edi,%edx
  8021d9:	89 d1                	mov    %edx,%ecx
  8021db:	89 c6                	mov    %eax,%esi
  8021dd:	e9 71 ff ff ff       	jmp    802153 <__umoddi3+0xb3>
  8021e2:	66 90                	xchg   %ax,%ax
  8021e4:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8021e8:	72 ea                	jb     8021d4 <__umoddi3+0x134>
  8021ea:	89 d9                	mov    %ebx,%ecx
  8021ec:	e9 62 ff ff ff       	jmp    802153 <__umoddi3+0xb3>
