
obj/user/buggyhello2.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 1d 00 00 00       	call   80004e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

const char *hello = "hello, world\n";

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs(hello, 1024*1024);
  800039:	68 00 00 10 00       	push   $0x100000
  80003e:	ff 35 00 20 80 00    	pushl  0x802000
  800044:	e8 66 00 00 00       	call   8000af <sys_cputs>
}
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	c9                   	leave  
  80004d:	c3                   	ret    

0080004e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004e:	55                   	push   %ebp
  80004f:	89 e5                	mov    %esp,%ebp
  800051:	56                   	push   %esi
  800052:	53                   	push   %ebx
  800053:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800056:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800059:	e8 cf 00 00 00       	call   80012d <sys_getenvid>
  80005e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800063:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80006a:	c1 e0 07             	shl    $0x7,%eax
  80006d:	29 d0                	sub    %edx,%eax
  80006f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800074:	a3 08 20 80 00       	mov    %eax,0x802008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800079:	85 db                	test   %ebx,%ebx
  80007b:	7e 07                	jle    800084 <libmain+0x36>
		binaryname = argv[0];
  80007d:	8b 06                	mov    (%esi),%eax
  80007f:	a3 04 20 80 00       	mov    %eax,0x802004

	// call user main routine
	umain(argc, argv);
  800084:	83 ec 08             	sub    $0x8,%esp
  800087:	56                   	push   %esi
  800088:	53                   	push   %ebx
  800089:	e8 a5 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008e:	e8 0a 00 00 00       	call   80009d <exit>
}
  800093:	83 c4 10             	add    $0x10,%esp
  800096:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800099:	5b                   	pop    %ebx
  80009a:	5e                   	pop    %esi
  80009b:	5d                   	pop    %ebp
  80009c:	c3                   	ret    

0080009d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80009d:	55                   	push   %ebp
  80009e:	89 e5                	mov    %esp,%ebp
  8000a0:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000a3:	6a 00                	push   $0x0
  8000a5:	e8 42 00 00 00       	call   8000ec <sys_env_destroy>
}
  8000aa:	83 c4 10             	add    $0x10,%esp
  8000ad:	c9                   	leave  
  8000ae:	c3                   	ret    

008000af <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000af:	55                   	push   %ebp
  8000b0:	89 e5                	mov    %esp,%ebp
  8000b2:	57                   	push   %edi
  8000b3:	56                   	push   %esi
  8000b4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000b5:	b8 00 00 00 00       	mov    $0x0,%eax
  8000ba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000bd:	8b 55 08             	mov    0x8(%ebp),%edx
  8000c0:	89 c3                	mov    %eax,%ebx
  8000c2:	89 c7                	mov    %eax,%edi
  8000c4:	89 c6                	mov    %eax,%esi
  8000c6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c8:	5b                   	pop    %ebx
  8000c9:	5e                   	pop    %esi
  8000ca:	5f                   	pop    %edi
  8000cb:	5d                   	pop    %ebp
  8000cc:	c3                   	ret    

008000cd <sys_cgetc>:

int
sys_cgetc(void)
{
  8000cd:	55                   	push   %ebp
  8000ce:	89 e5                	mov    %esp,%ebp
  8000d0:	57                   	push   %edi
  8000d1:	56                   	push   %esi
  8000d2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000d3:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d8:	b8 01 00 00 00       	mov    $0x1,%eax
  8000dd:	89 d1                	mov    %edx,%ecx
  8000df:	89 d3                	mov    %edx,%ebx
  8000e1:	89 d7                	mov    %edx,%edi
  8000e3:	89 d6                	mov    %edx,%esi
  8000e5:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e7:	5b                   	pop    %ebx
  8000e8:	5e                   	pop    %esi
  8000e9:	5f                   	pop    %edi
  8000ea:	5d                   	pop    %ebp
  8000eb:	c3                   	ret    

008000ec <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000ec:	55                   	push   %ebp
  8000ed:	89 e5                	mov    %esp,%ebp
  8000ef:	57                   	push   %edi
  8000f0:	56                   	push   %esi
  8000f1:	53                   	push   %ebx
  8000f2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000f5:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000fa:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ff:	8b 55 08             	mov    0x8(%ebp),%edx
  800102:	89 cb                	mov    %ecx,%ebx
  800104:	89 cf                	mov    %ecx,%edi
  800106:	89 ce                	mov    %ecx,%esi
  800108:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80010a:	85 c0                	test   %eax,%eax
  80010c:	7e 17                	jle    800125 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80010e:	83 ec 0c             	sub    $0xc,%esp
  800111:	50                   	push   %eax
  800112:	6a 03                	push   $0x3
  800114:	68 78 0f 80 00       	push   $0x800f78
  800119:	6a 23                	push   $0x23
  80011b:	68 95 0f 80 00       	push   $0x800f95
  800120:	e8 56 02 00 00       	call   80037b <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800125:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800128:	5b                   	pop    %ebx
  800129:	5e                   	pop    %esi
  80012a:	5f                   	pop    %edi
  80012b:	5d                   	pop    %ebp
  80012c:	c3                   	ret    

0080012d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  80012d:	55                   	push   %ebp
  80012e:	89 e5                	mov    %esp,%ebp
  800130:	57                   	push   %edi
  800131:	56                   	push   %esi
  800132:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800133:	ba 00 00 00 00       	mov    $0x0,%edx
  800138:	b8 02 00 00 00       	mov    $0x2,%eax
  80013d:	89 d1                	mov    %edx,%ecx
  80013f:	89 d3                	mov    %edx,%ebx
  800141:	89 d7                	mov    %edx,%edi
  800143:	89 d6                	mov    %edx,%esi
  800145:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800147:	5b                   	pop    %ebx
  800148:	5e                   	pop    %esi
  800149:	5f                   	pop    %edi
  80014a:	5d                   	pop    %ebp
  80014b:	c3                   	ret    

0080014c <sys_yield>:

void
sys_yield(void)
{
  80014c:	55                   	push   %ebp
  80014d:	89 e5                	mov    %esp,%ebp
  80014f:	57                   	push   %edi
  800150:	56                   	push   %esi
  800151:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800152:	ba 00 00 00 00       	mov    $0x0,%edx
  800157:	b8 0b 00 00 00       	mov    $0xb,%eax
  80015c:	89 d1                	mov    %edx,%ecx
  80015e:	89 d3                	mov    %edx,%ebx
  800160:	89 d7                	mov    %edx,%edi
  800162:	89 d6                	mov    %edx,%esi
  800164:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800166:	5b                   	pop    %ebx
  800167:	5e                   	pop    %esi
  800168:	5f                   	pop    %edi
  800169:	5d                   	pop    %ebp
  80016a:	c3                   	ret    

0080016b <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80016b:	55                   	push   %ebp
  80016c:	89 e5                	mov    %esp,%ebp
  80016e:	57                   	push   %edi
  80016f:	56                   	push   %esi
  800170:	53                   	push   %ebx
  800171:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800174:	be 00 00 00 00       	mov    $0x0,%esi
  800179:	b8 04 00 00 00       	mov    $0x4,%eax
  80017e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800181:	8b 55 08             	mov    0x8(%ebp),%edx
  800184:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800187:	89 f7                	mov    %esi,%edi
  800189:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80018b:	85 c0                	test   %eax,%eax
  80018d:	7e 17                	jle    8001a6 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80018f:	83 ec 0c             	sub    $0xc,%esp
  800192:	50                   	push   %eax
  800193:	6a 04                	push   $0x4
  800195:	68 78 0f 80 00       	push   $0x800f78
  80019a:	6a 23                	push   $0x23
  80019c:	68 95 0f 80 00       	push   $0x800f95
  8001a1:	e8 d5 01 00 00       	call   80037b <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001a6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a9:	5b                   	pop    %ebx
  8001aa:	5e                   	pop    %esi
  8001ab:	5f                   	pop    %edi
  8001ac:	5d                   	pop    %ebp
  8001ad:	c3                   	ret    

008001ae <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001ae:	55                   	push   %ebp
  8001af:	89 e5                	mov    %esp,%ebp
  8001b1:	57                   	push   %edi
  8001b2:	56                   	push   %esi
  8001b3:	53                   	push   %ebx
  8001b4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001b7:	b8 05 00 00 00       	mov    $0x5,%eax
  8001bc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001bf:	8b 55 08             	mov    0x8(%ebp),%edx
  8001c2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001c5:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001c8:	8b 75 18             	mov    0x18(%ebp),%esi
  8001cb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001cd:	85 c0                	test   %eax,%eax
  8001cf:	7e 17                	jle    8001e8 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001d1:	83 ec 0c             	sub    $0xc,%esp
  8001d4:	50                   	push   %eax
  8001d5:	6a 05                	push   $0x5
  8001d7:	68 78 0f 80 00       	push   $0x800f78
  8001dc:	6a 23                	push   $0x23
  8001de:	68 95 0f 80 00       	push   $0x800f95
  8001e3:	e8 93 01 00 00       	call   80037b <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001e8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001eb:	5b                   	pop    %ebx
  8001ec:	5e                   	pop    %esi
  8001ed:	5f                   	pop    %edi
  8001ee:	5d                   	pop    %ebp
  8001ef:	c3                   	ret    

008001f0 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001f0:	55                   	push   %ebp
  8001f1:	89 e5                	mov    %esp,%ebp
  8001f3:	57                   	push   %edi
  8001f4:	56                   	push   %esi
  8001f5:	53                   	push   %ebx
  8001f6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001f9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001fe:	b8 06 00 00 00       	mov    $0x6,%eax
  800203:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800206:	8b 55 08             	mov    0x8(%ebp),%edx
  800209:	89 df                	mov    %ebx,%edi
  80020b:	89 de                	mov    %ebx,%esi
  80020d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80020f:	85 c0                	test   %eax,%eax
  800211:	7e 17                	jle    80022a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800213:	83 ec 0c             	sub    $0xc,%esp
  800216:	50                   	push   %eax
  800217:	6a 06                	push   $0x6
  800219:	68 78 0f 80 00       	push   $0x800f78
  80021e:	6a 23                	push   $0x23
  800220:	68 95 0f 80 00       	push   $0x800f95
  800225:	e8 51 01 00 00       	call   80037b <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80022a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80022d:	5b                   	pop    %ebx
  80022e:	5e                   	pop    %esi
  80022f:	5f                   	pop    %edi
  800230:	5d                   	pop    %ebp
  800231:	c3                   	ret    

00800232 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800232:	55                   	push   %ebp
  800233:	89 e5                	mov    %esp,%ebp
  800235:	57                   	push   %edi
  800236:	56                   	push   %esi
  800237:	53                   	push   %ebx
  800238:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80023b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800240:	b8 08 00 00 00       	mov    $0x8,%eax
  800245:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800248:	8b 55 08             	mov    0x8(%ebp),%edx
  80024b:	89 df                	mov    %ebx,%edi
  80024d:	89 de                	mov    %ebx,%esi
  80024f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800251:	85 c0                	test   %eax,%eax
  800253:	7e 17                	jle    80026c <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800255:	83 ec 0c             	sub    $0xc,%esp
  800258:	50                   	push   %eax
  800259:	6a 08                	push   $0x8
  80025b:	68 78 0f 80 00       	push   $0x800f78
  800260:	6a 23                	push   $0x23
  800262:	68 95 0f 80 00       	push   $0x800f95
  800267:	e8 0f 01 00 00       	call   80037b <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  80026c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80026f:	5b                   	pop    %ebx
  800270:	5e                   	pop    %esi
  800271:	5f                   	pop    %edi
  800272:	5d                   	pop    %ebp
  800273:	c3                   	ret    

00800274 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800274:	55                   	push   %ebp
  800275:	89 e5                	mov    %esp,%ebp
  800277:	57                   	push   %edi
  800278:	56                   	push   %esi
  800279:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80027a:	ba 00 00 00 00       	mov    $0x0,%edx
  80027f:	b8 0c 00 00 00       	mov    $0xc,%eax
  800284:	89 d1                	mov    %edx,%ecx
  800286:	89 d3                	mov    %edx,%ebx
  800288:	89 d7                	mov    %edx,%edi
  80028a:	89 d6                	mov    %edx,%esi
  80028c:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80028e:	5b                   	pop    %ebx
  80028f:	5e                   	pop    %esi
  800290:	5f                   	pop    %edi
  800291:	5d                   	pop    %ebp
  800292:	c3                   	ret    

00800293 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800293:	55                   	push   %ebp
  800294:	89 e5                	mov    %esp,%ebp
  800296:	57                   	push   %edi
  800297:	56                   	push   %esi
  800298:	53                   	push   %ebx
  800299:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80029c:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002a1:	b8 09 00 00 00       	mov    $0x9,%eax
  8002a6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002a9:	8b 55 08             	mov    0x8(%ebp),%edx
  8002ac:	89 df                	mov    %ebx,%edi
  8002ae:	89 de                	mov    %ebx,%esi
  8002b0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002b2:	85 c0                	test   %eax,%eax
  8002b4:	7e 17                	jle    8002cd <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002b6:	83 ec 0c             	sub    $0xc,%esp
  8002b9:	50                   	push   %eax
  8002ba:	6a 09                	push   $0x9
  8002bc:	68 78 0f 80 00       	push   $0x800f78
  8002c1:	6a 23                	push   $0x23
  8002c3:	68 95 0f 80 00       	push   $0x800f95
  8002c8:	e8 ae 00 00 00       	call   80037b <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002cd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d0:	5b                   	pop    %ebx
  8002d1:	5e                   	pop    %esi
  8002d2:	5f                   	pop    %edi
  8002d3:	5d                   	pop    %ebp
  8002d4:	c3                   	ret    

008002d5 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002d5:	55                   	push   %ebp
  8002d6:	89 e5                	mov    %esp,%ebp
  8002d8:	57                   	push   %edi
  8002d9:	56                   	push   %esi
  8002da:	53                   	push   %ebx
  8002db:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002de:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002e3:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002e8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002eb:	8b 55 08             	mov    0x8(%ebp),%edx
  8002ee:	89 df                	mov    %ebx,%edi
  8002f0:	89 de                	mov    %ebx,%esi
  8002f2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002f4:	85 c0                	test   %eax,%eax
  8002f6:	7e 17                	jle    80030f <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002f8:	83 ec 0c             	sub    $0xc,%esp
  8002fb:	50                   	push   %eax
  8002fc:	6a 0a                	push   $0xa
  8002fe:	68 78 0f 80 00       	push   $0x800f78
  800303:	6a 23                	push   $0x23
  800305:	68 95 0f 80 00       	push   $0x800f95
  80030a:	e8 6c 00 00 00       	call   80037b <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  80030f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800312:	5b                   	pop    %ebx
  800313:	5e                   	pop    %esi
  800314:	5f                   	pop    %edi
  800315:	5d                   	pop    %ebp
  800316:	c3                   	ret    

00800317 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800317:	55                   	push   %ebp
  800318:	89 e5                	mov    %esp,%ebp
  80031a:	57                   	push   %edi
  80031b:	56                   	push   %esi
  80031c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80031d:	be 00 00 00 00       	mov    $0x0,%esi
  800322:	b8 0d 00 00 00       	mov    $0xd,%eax
  800327:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80032a:	8b 55 08             	mov    0x8(%ebp),%edx
  80032d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800330:	8b 7d 14             	mov    0x14(%ebp),%edi
  800333:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800335:	5b                   	pop    %ebx
  800336:	5e                   	pop    %esi
  800337:	5f                   	pop    %edi
  800338:	5d                   	pop    %ebp
  800339:	c3                   	ret    

0080033a <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  80033a:	55                   	push   %ebp
  80033b:	89 e5                	mov    %esp,%ebp
  80033d:	57                   	push   %edi
  80033e:	56                   	push   %esi
  80033f:	53                   	push   %ebx
  800340:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800343:	b9 00 00 00 00       	mov    $0x0,%ecx
  800348:	b8 0e 00 00 00       	mov    $0xe,%eax
  80034d:	8b 55 08             	mov    0x8(%ebp),%edx
  800350:	89 cb                	mov    %ecx,%ebx
  800352:	89 cf                	mov    %ecx,%edi
  800354:	89 ce                	mov    %ecx,%esi
  800356:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800358:	85 c0                	test   %eax,%eax
  80035a:	7e 17                	jle    800373 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80035c:	83 ec 0c             	sub    $0xc,%esp
  80035f:	50                   	push   %eax
  800360:	6a 0e                	push   $0xe
  800362:	68 78 0f 80 00       	push   $0x800f78
  800367:	6a 23                	push   $0x23
  800369:	68 95 0f 80 00       	push   $0x800f95
  80036e:	e8 08 00 00 00       	call   80037b <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800373:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800376:	5b                   	pop    %ebx
  800377:	5e                   	pop    %esi
  800378:	5f                   	pop    %edi
  800379:	5d                   	pop    %ebp
  80037a:	c3                   	ret    

0080037b <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80037b:	55                   	push   %ebp
  80037c:	89 e5                	mov    %esp,%ebp
  80037e:	56                   	push   %esi
  80037f:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800380:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800383:	8b 35 04 20 80 00    	mov    0x802004,%esi
  800389:	e8 9f fd ff ff       	call   80012d <sys_getenvid>
  80038e:	83 ec 0c             	sub    $0xc,%esp
  800391:	ff 75 0c             	pushl  0xc(%ebp)
  800394:	ff 75 08             	pushl  0x8(%ebp)
  800397:	56                   	push   %esi
  800398:	50                   	push   %eax
  800399:	68 a4 0f 80 00       	push   $0x800fa4
  80039e:	e8 b0 00 00 00       	call   800453 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8003a3:	83 c4 18             	add    $0x18,%esp
  8003a6:	53                   	push   %ebx
  8003a7:	ff 75 10             	pushl  0x10(%ebp)
  8003aa:	e8 53 00 00 00       	call   800402 <vcprintf>
	cprintf("\n");
  8003af:	c7 04 24 6c 0f 80 00 	movl   $0x800f6c,(%esp)
  8003b6:	e8 98 00 00 00       	call   800453 <cprintf>
  8003bb:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003be:	cc                   	int3   
  8003bf:	eb fd                	jmp    8003be <_panic+0x43>

008003c1 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003c1:	55                   	push   %ebp
  8003c2:	89 e5                	mov    %esp,%ebp
  8003c4:	53                   	push   %ebx
  8003c5:	83 ec 04             	sub    $0x4,%esp
  8003c8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003cb:	8b 13                	mov    (%ebx),%edx
  8003cd:	8d 42 01             	lea    0x1(%edx),%eax
  8003d0:	89 03                	mov    %eax,(%ebx)
  8003d2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003d5:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003d9:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003de:	75 1a                	jne    8003fa <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003e0:	83 ec 08             	sub    $0x8,%esp
  8003e3:	68 ff 00 00 00       	push   $0xff
  8003e8:	8d 43 08             	lea    0x8(%ebx),%eax
  8003eb:	50                   	push   %eax
  8003ec:	e8 be fc ff ff       	call   8000af <sys_cputs>
		b->idx = 0;
  8003f1:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003f7:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003fa:	ff 43 04             	incl   0x4(%ebx)
}
  8003fd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800400:	c9                   	leave  
  800401:	c3                   	ret    

00800402 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800402:	55                   	push   %ebp
  800403:	89 e5                	mov    %esp,%ebp
  800405:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80040b:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800412:	00 00 00 
	b.cnt = 0;
  800415:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80041c:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80041f:	ff 75 0c             	pushl  0xc(%ebp)
  800422:	ff 75 08             	pushl  0x8(%ebp)
  800425:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80042b:	50                   	push   %eax
  80042c:	68 c1 03 80 00       	push   $0x8003c1
  800431:	e8 51 01 00 00       	call   800587 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800436:	83 c4 08             	add    $0x8,%esp
  800439:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80043f:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800445:	50                   	push   %eax
  800446:	e8 64 fc ff ff       	call   8000af <sys_cputs>

	return b.cnt;
}
  80044b:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800451:	c9                   	leave  
  800452:	c3                   	ret    

00800453 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800453:	55                   	push   %ebp
  800454:	89 e5                	mov    %esp,%ebp
  800456:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800459:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80045c:	50                   	push   %eax
  80045d:	ff 75 08             	pushl  0x8(%ebp)
  800460:	e8 9d ff ff ff       	call   800402 <vcprintf>
	va_end(ap);

	return cnt;
}
  800465:	c9                   	leave  
  800466:	c3                   	ret    

00800467 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800467:	55                   	push   %ebp
  800468:	89 e5                	mov    %esp,%ebp
  80046a:	57                   	push   %edi
  80046b:	56                   	push   %esi
  80046c:	53                   	push   %ebx
  80046d:	83 ec 1c             	sub    $0x1c,%esp
  800470:	89 c7                	mov    %eax,%edi
  800472:	89 d6                	mov    %edx,%esi
  800474:	8b 45 08             	mov    0x8(%ebp),%eax
  800477:	8b 55 0c             	mov    0xc(%ebp),%edx
  80047a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80047d:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800480:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800483:	bb 00 00 00 00       	mov    $0x0,%ebx
  800488:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80048b:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80048e:	39 d3                	cmp    %edx,%ebx
  800490:	72 05                	jb     800497 <printnum+0x30>
  800492:	39 45 10             	cmp    %eax,0x10(%ebp)
  800495:	77 45                	ja     8004dc <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800497:	83 ec 0c             	sub    $0xc,%esp
  80049a:	ff 75 18             	pushl  0x18(%ebp)
  80049d:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a0:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8004a3:	53                   	push   %ebx
  8004a4:	ff 75 10             	pushl  0x10(%ebp)
  8004a7:	83 ec 08             	sub    $0x8,%esp
  8004aa:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004ad:	ff 75 e0             	pushl  -0x20(%ebp)
  8004b0:	ff 75 dc             	pushl  -0x24(%ebp)
  8004b3:	ff 75 d8             	pushl  -0x28(%ebp)
  8004b6:	e8 25 08 00 00       	call   800ce0 <__udivdi3>
  8004bb:	83 c4 18             	add    $0x18,%esp
  8004be:	52                   	push   %edx
  8004bf:	50                   	push   %eax
  8004c0:	89 f2                	mov    %esi,%edx
  8004c2:	89 f8                	mov    %edi,%eax
  8004c4:	e8 9e ff ff ff       	call   800467 <printnum>
  8004c9:	83 c4 20             	add    $0x20,%esp
  8004cc:	eb 16                	jmp    8004e4 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004ce:	83 ec 08             	sub    $0x8,%esp
  8004d1:	56                   	push   %esi
  8004d2:	ff 75 18             	pushl  0x18(%ebp)
  8004d5:	ff d7                	call   *%edi
  8004d7:	83 c4 10             	add    $0x10,%esp
  8004da:	eb 03                	jmp    8004df <printnum+0x78>
  8004dc:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004df:	4b                   	dec    %ebx
  8004e0:	85 db                	test   %ebx,%ebx
  8004e2:	7f ea                	jg     8004ce <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004e4:	83 ec 08             	sub    $0x8,%esp
  8004e7:	56                   	push   %esi
  8004e8:	83 ec 04             	sub    $0x4,%esp
  8004eb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004ee:	ff 75 e0             	pushl  -0x20(%ebp)
  8004f1:	ff 75 dc             	pushl  -0x24(%ebp)
  8004f4:	ff 75 d8             	pushl  -0x28(%ebp)
  8004f7:	e8 f4 08 00 00       	call   800df0 <__umoddi3>
  8004fc:	83 c4 14             	add    $0x14,%esp
  8004ff:	0f be 80 c7 0f 80 00 	movsbl 0x800fc7(%eax),%eax
  800506:	50                   	push   %eax
  800507:	ff d7                	call   *%edi
}
  800509:	83 c4 10             	add    $0x10,%esp
  80050c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80050f:	5b                   	pop    %ebx
  800510:	5e                   	pop    %esi
  800511:	5f                   	pop    %edi
  800512:	5d                   	pop    %ebp
  800513:	c3                   	ret    

00800514 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800514:	55                   	push   %ebp
  800515:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800517:	83 fa 01             	cmp    $0x1,%edx
  80051a:	7e 0e                	jle    80052a <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80051c:	8b 10                	mov    (%eax),%edx
  80051e:	8d 4a 08             	lea    0x8(%edx),%ecx
  800521:	89 08                	mov    %ecx,(%eax)
  800523:	8b 02                	mov    (%edx),%eax
  800525:	8b 52 04             	mov    0x4(%edx),%edx
  800528:	eb 22                	jmp    80054c <getuint+0x38>
	else if (lflag)
  80052a:	85 d2                	test   %edx,%edx
  80052c:	74 10                	je     80053e <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80052e:	8b 10                	mov    (%eax),%edx
  800530:	8d 4a 04             	lea    0x4(%edx),%ecx
  800533:	89 08                	mov    %ecx,(%eax)
  800535:	8b 02                	mov    (%edx),%eax
  800537:	ba 00 00 00 00       	mov    $0x0,%edx
  80053c:	eb 0e                	jmp    80054c <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80053e:	8b 10                	mov    (%eax),%edx
  800540:	8d 4a 04             	lea    0x4(%edx),%ecx
  800543:	89 08                	mov    %ecx,(%eax)
  800545:	8b 02                	mov    (%edx),%eax
  800547:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80054c:	5d                   	pop    %ebp
  80054d:	c3                   	ret    

0080054e <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80054e:	55                   	push   %ebp
  80054f:	89 e5                	mov    %esp,%ebp
  800551:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800554:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800557:	8b 10                	mov    (%eax),%edx
  800559:	3b 50 04             	cmp    0x4(%eax),%edx
  80055c:	73 0a                	jae    800568 <sprintputch+0x1a>
		*b->buf++ = ch;
  80055e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800561:	89 08                	mov    %ecx,(%eax)
  800563:	8b 45 08             	mov    0x8(%ebp),%eax
  800566:	88 02                	mov    %al,(%edx)
}
  800568:	5d                   	pop    %ebp
  800569:	c3                   	ret    

0080056a <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80056a:	55                   	push   %ebp
  80056b:	89 e5                	mov    %esp,%ebp
  80056d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800570:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800573:	50                   	push   %eax
  800574:	ff 75 10             	pushl  0x10(%ebp)
  800577:	ff 75 0c             	pushl  0xc(%ebp)
  80057a:	ff 75 08             	pushl  0x8(%ebp)
  80057d:	e8 05 00 00 00       	call   800587 <vprintfmt>
	va_end(ap);
}
  800582:	83 c4 10             	add    $0x10,%esp
  800585:	c9                   	leave  
  800586:	c3                   	ret    

00800587 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800587:	55                   	push   %ebp
  800588:	89 e5                	mov    %esp,%ebp
  80058a:	57                   	push   %edi
  80058b:	56                   	push   %esi
  80058c:	53                   	push   %ebx
  80058d:	83 ec 2c             	sub    $0x2c,%esp
  800590:	8b 75 08             	mov    0x8(%ebp),%esi
  800593:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800596:	8b 7d 10             	mov    0x10(%ebp),%edi
  800599:	eb 12                	jmp    8005ad <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80059b:	85 c0                	test   %eax,%eax
  80059d:	0f 84 68 03 00 00    	je     80090b <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8005a3:	83 ec 08             	sub    $0x8,%esp
  8005a6:	53                   	push   %ebx
  8005a7:	50                   	push   %eax
  8005a8:	ff d6                	call   *%esi
  8005aa:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8005ad:	47                   	inc    %edi
  8005ae:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005b2:	83 f8 25             	cmp    $0x25,%eax
  8005b5:	75 e4                	jne    80059b <vprintfmt+0x14>
  8005b7:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005bb:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005c2:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005c9:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005d0:	ba 00 00 00 00       	mov    $0x0,%edx
  8005d5:	eb 07                	jmp    8005de <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d7:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005da:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005de:	8d 47 01             	lea    0x1(%edi),%eax
  8005e1:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005e4:	0f b6 0f             	movzbl (%edi),%ecx
  8005e7:	8a 07                	mov    (%edi),%al
  8005e9:	83 e8 23             	sub    $0x23,%eax
  8005ec:	3c 55                	cmp    $0x55,%al
  8005ee:	0f 87 fe 02 00 00    	ja     8008f2 <vprintfmt+0x36b>
  8005f4:	0f b6 c0             	movzbl %al,%eax
  8005f7:	ff 24 85 00 11 80 00 	jmp    *0x801100(,%eax,4)
  8005fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800601:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800605:	eb d7                	jmp    8005de <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800607:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80060a:	b8 00 00 00 00       	mov    $0x0,%eax
  80060f:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800612:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800615:	01 c0                	add    %eax,%eax
  800617:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80061b:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80061e:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800621:	83 fa 09             	cmp    $0x9,%edx
  800624:	77 34                	ja     80065a <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800626:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800627:	eb e9                	jmp    800612 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800629:	8b 45 14             	mov    0x14(%ebp),%eax
  80062c:	8d 48 04             	lea    0x4(%eax),%ecx
  80062f:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800632:	8b 00                	mov    (%eax),%eax
  800634:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800637:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80063a:	eb 24                	jmp    800660 <vprintfmt+0xd9>
  80063c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800640:	79 07                	jns    800649 <vprintfmt+0xc2>
  800642:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800649:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80064c:	eb 90                	jmp    8005de <vprintfmt+0x57>
  80064e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800651:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800658:	eb 84                	jmp    8005de <vprintfmt+0x57>
  80065a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80065d:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800660:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800664:	0f 89 74 ff ff ff    	jns    8005de <vprintfmt+0x57>
				width = precision, precision = -1;
  80066a:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80066d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800670:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800677:	e9 62 ff ff ff       	jmp    8005de <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80067c:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80067d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800680:	e9 59 ff ff ff       	jmp    8005de <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800685:	8b 45 14             	mov    0x14(%ebp),%eax
  800688:	8d 50 04             	lea    0x4(%eax),%edx
  80068b:	89 55 14             	mov    %edx,0x14(%ebp)
  80068e:	83 ec 08             	sub    $0x8,%esp
  800691:	53                   	push   %ebx
  800692:	ff 30                	pushl  (%eax)
  800694:	ff d6                	call   *%esi
			break;
  800696:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800699:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80069c:	e9 0c ff ff ff       	jmp    8005ad <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8006a1:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a4:	8d 50 04             	lea    0x4(%eax),%edx
  8006a7:	89 55 14             	mov    %edx,0x14(%ebp)
  8006aa:	8b 00                	mov    (%eax),%eax
  8006ac:	85 c0                	test   %eax,%eax
  8006ae:	79 02                	jns    8006b2 <vprintfmt+0x12b>
  8006b0:	f7 d8                	neg    %eax
  8006b2:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006b4:	83 f8 0f             	cmp    $0xf,%eax
  8006b7:	7f 0b                	jg     8006c4 <vprintfmt+0x13d>
  8006b9:	8b 04 85 60 12 80 00 	mov    0x801260(,%eax,4),%eax
  8006c0:	85 c0                	test   %eax,%eax
  8006c2:	75 18                	jne    8006dc <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006c4:	52                   	push   %edx
  8006c5:	68 df 0f 80 00       	push   $0x800fdf
  8006ca:	53                   	push   %ebx
  8006cb:	56                   	push   %esi
  8006cc:	e8 99 fe ff ff       	call   80056a <printfmt>
  8006d1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006d7:	e9 d1 fe ff ff       	jmp    8005ad <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006dc:	50                   	push   %eax
  8006dd:	68 e8 0f 80 00       	push   $0x800fe8
  8006e2:	53                   	push   %ebx
  8006e3:	56                   	push   %esi
  8006e4:	e8 81 fe ff ff       	call   80056a <printfmt>
  8006e9:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006ec:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006ef:	e9 b9 fe ff ff       	jmp    8005ad <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006f4:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f7:	8d 50 04             	lea    0x4(%eax),%edx
  8006fa:	89 55 14             	mov    %edx,0x14(%ebp)
  8006fd:	8b 38                	mov    (%eax),%edi
  8006ff:	85 ff                	test   %edi,%edi
  800701:	75 05                	jne    800708 <vprintfmt+0x181>
				p = "(null)";
  800703:	bf d8 0f 80 00       	mov    $0x800fd8,%edi
			if (width > 0 && padc != '-')
  800708:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80070c:	0f 8e 90 00 00 00    	jle    8007a2 <vprintfmt+0x21b>
  800712:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800716:	0f 84 8e 00 00 00    	je     8007aa <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80071c:	83 ec 08             	sub    $0x8,%esp
  80071f:	ff 75 d0             	pushl  -0x30(%ebp)
  800722:	57                   	push   %edi
  800723:	e8 70 02 00 00       	call   800998 <strnlen>
  800728:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80072b:	29 c1                	sub    %eax,%ecx
  80072d:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800730:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800733:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800737:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80073a:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80073d:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80073f:	eb 0d                	jmp    80074e <vprintfmt+0x1c7>
					putch(padc, putdat);
  800741:	83 ec 08             	sub    $0x8,%esp
  800744:	53                   	push   %ebx
  800745:	ff 75 e4             	pushl  -0x1c(%ebp)
  800748:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80074a:	4f                   	dec    %edi
  80074b:	83 c4 10             	add    $0x10,%esp
  80074e:	85 ff                	test   %edi,%edi
  800750:	7f ef                	jg     800741 <vprintfmt+0x1ba>
  800752:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800755:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800758:	89 c8                	mov    %ecx,%eax
  80075a:	85 c9                	test   %ecx,%ecx
  80075c:	79 05                	jns    800763 <vprintfmt+0x1dc>
  80075e:	b8 00 00 00 00       	mov    $0x0,%eax
  800763:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800766:	29 c1                	sub    %eax,%ecx
  800768:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80076b:	89 75 08             	mov    %esi,0x8(%ebp)
  80076e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800771:	eb 3d                	jmp    8007b0 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800773:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800777:	74 19                	je     800792 <vprintfmt+0x20b>
  800779:	0f be c0             	movsbl %al,%eax
  80077c:	83 e8 20             	sub    $0x20,%eax
  80077f:	83 f8 5e             	cmp    $0x5e,%eax
  800782:	76 0e                	jbe    800792 <vprintfmt+0x20b>
					putch('?', putdat);
  800784:	83 ec 08             	sub    $0x8,%esp
  800787:	53                   	push   %ebx
  800788:	6a 3f                	push   $0x3f
  80078a:	ff 55 08             	call   *0x8(%ebp)
  80078d:	83 c4 10             	add    $0x10,%esp
  800790:	eb 0b                	jmp    80079d <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800792:	83 ec 08             	sub    $0x8,%esp
  800795:	53                   	push   %ebx
  800796:	52                   	push   %edx
  800797:	ff 55 08             	call   *0x8(%ebp)
  80079a:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80079d:	ff 4d e4             	decl   -0x1c(%ebp)
  8007a0:	eb 0e                	jmp    8007b0 <vprintfmt+0x229>
  8007a2:	89 75 08             	mov    %esi,0x8(%ebp)
  8007a5:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007a8:	eb 06                	jmp    8007b0 <vprintfmt+0x229>
  8007aa:	89 75 08             	mov    %esi,0x8(%ebp)
  8007ad:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007b0:	47                   	inc    %edi
  8007b1:	8a 47 ff             	mov    -0x1(%edi),%al
  8007b4:	0f be d0             	movsbl %al,%edx
  8007b7:	85 d2                	test   %edx,%edx
  8007b9:	74 1d                	je     8007d8 <vprintfmt+0x251>
  8007bb:	85 f6                	test   %esi,%esi
  8007bd:	78 b4                	js     800773 <vprintfmt+0x1ec>
  8007bf:	4e                   	dec    %esi
  8007c0:	79 b1                	jns    800773 <vprintfmt+0x1ec>
  8007c2:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007c8:	eb 14                	jmp    8007de <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007ca:	83 ec 08             	sub    $0x8,%esp
  8007cd:	53                   	push   %ebx
  8007ce:	6a 20                	push   $0x20
  8007d0:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007d2:	4f                   	dec    %edi
  8007d3:	83 c4 10             	add    $0x10,%esp
  8007d6:	eb 06                	jmp    8007de <vprintfmt+0x257>
  8007d8:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007db:	8b 75 08             	mov    0x8(%ebp),%esi
  8007de:	85 ff                	test   %edi,%edi
  8007e0:	7f e8                	jg     8007ca <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007e5:	e9 c3 fd ff ff       	jmp    8005ad <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007ea:	83 fa 01             	cmp    $0x1,%edx
  8007ed:	7e 16                	jle    800805 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007ef:	8b 45 14             	mov    0x14(%ebp),%eax
  8007f2:	8d 50 08             	lea    0x8(%eax),%edx
  8007f5:	89 55 14             	mov    %edx,0x14(%ebp)
  8007f8:	8b 50 04             	mov    0x4(%eax),%edx
  8007fb:	8b 00                	mov    (%eax),%eax
  8007fd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800800:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800803:	eb 32                	jmp    800837 <vprintfmt+0x2b0>
	else if (lflag)
  800805:	85 d2                	test   %edx,%edx
  800807:	74 18                	je     800821 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800809:	8b 45 14             	mov    0x14(%ebp),%eax
  80080c:	8d 50 04             	lea    0x4(%eax),%edx
  80080f:	89 55 14             	mov    %edx,0x14(%ebp)
  800812:	8b 00                	mov    (%eax),%eax
  800814:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800817:	89 c1                	mov    %eax,%ecx
  800819:	c1 f9 1f             	sar    $0x1f,%ecx
  80081c:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80081f:	eb 16                	jmp    800837 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800821:	8b 45 14             	mov    0x14(%ebp),%eax
  800824:	8d 50 04             	lea    0x4(%eax),%edx
  800827:	89 55 14             	mov    %edx,0x14(%ebp)
  80082a:	8b 00                	mov    (%eax),%eax
  80082c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80082f:	89 c1                	mov    %eax,%ecx
  800831:	c1 f9 1f             	sar    $0x1f,%ecx
  800834:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800837:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80083a:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80083d:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800841:	79 76                	jns    8008b9 <vprintfmt+0x332>
				putch('-', putdat);
  800843:	83 ec 08             	sub    $0x8,%esp
  800846:	53                   	push   %ebx
  800847:	6a 2d                	push   $0x2d
  800849:	ff d6                	call   *%esi
				num = -(long long) num;
  80084b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80084e:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800851:	f7 d8                	neg    %eax
  800853:	83 d2 00             	adc    $0x0,%edx
  800856:	f7 da                	neg    %edx
  800858:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80085b:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800860:	eb 5c                	jmp    8008be <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800862:	8d 45 14             	lea    0x14(%ebp),%eax
  800865:	e8 aa fc ff ff       	call   800514 <getuint>
			base = 10;
  80086a:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80086f:	eb 4d                	jmp    8008be <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800871:	8d 45 14             	lea    0x14(%ebp),%eax
  800874:	e8 9b fc ff ff       	call   800514 <getuint>
			base = 8;
  800879:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80087e:	eb 3e                	jmp    8008be <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800880:	83 ec 08             	sub    $0x8,%esp
  800883:	53                   	push   %ebx
  800884:	6a 30                	push   $0x30
  800886:	ff d6                	call   *%esi
			putch('x', putdat);
  800888:	83 c4 08             	add    $0x8,%esp
  80088b:	53                   	push   %ebx
  80088c:	6a 78                	push   $0x78
  80088e:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800890:	8b 45 14             	mov    0x14(%ebp),%eax
  800893:	8d 50 04             	lea    0x4(%eax),%edx
  800896:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800899:	8b 00                	mov    (%eax),%eax
  80089b:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8008a0:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8008a3:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8008a8:	eb 14                	jmp    8008be <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8008aa:	8d 45 14             	lea    0x14(%ebp),%eax
  8008ad:	e8 62 fc ff ff       	call   800514 <getuint>
			base = 16;
  8008b2:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008b7:	eb 05                	jmp    8008be <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008b9:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008be:	83 ec 0c             	sub    $0xc,%esp
  8008c1:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008c5:	57                   	push   %edi
  8008c6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008c9:	51                   	push   %ecx
  8008ca:	52                   	push   %edx
  8008cb:	50                   	push   %eax
  8008cc:	89 da                	mov    %ebx,%edx
  8008ce:	89 f0                	mov    %esi,%eax
  8008d0:	e8 92 fb ff ff       	call   800467 <printnum>
			break;
  8008d5:	83 c4 20             	add    $0x20,%esp
  8008d8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008db:	e9 cd fc ff ff       	jmp    8005ad <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008e0:	83 ec 08             	sub    $0x8,%esp
  8008e3:	53                   	push   %ebx
  8008e4:	51                   	push   %ecx
  8008e5:	ff d6                	call   *%esi
			break;
  8008e7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008ea:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008ed:	e9 bb fc ff ff       	jmp    8005ad <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008f2:	83 ec 08             	sub    $0x8,%esp
  8008f5:	53                   	push   %ebx
  8008f6:	6a 25                	push   $0x25
  8008f8:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008fa:	83 c4 10             	add    $0x10,%esp
  8008fd:	eb 01                	jmp    800900 <vprintfmt+0x379>
  8008ff:	4f                   	dec    %edi
  800900:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800904:	75 f9                	jne    8008ff <vprintfmt+0x378>
  800906:	e9 a2 fc ff ff       	jmp    8005ad <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80090b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80090e:	5b                   	pop    %ebx
  80090f:	5e                   	pop    %esi
  800910:	5f                   	pop    %edi
  800911:	5d                   	pop    %ebp
  800912:	c3                   	ret    

00800913 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800913:	55                   	push   %ebp
  800914:	89 e5                	mov    %esp,%ebp
  800916:	83 ec 18             	sub    $0x18,%esp
  800919:	8b 45 08             	mov    0x8(%ebp),%eax
  80091c:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80091f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800922:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800926:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800929:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800930:	85 c0                	test   %eax,%eax
  800932:	74 26                	je     80095a <vsnprintf+0x47>
  800934:	85 d2                	test   %edx,%edx
  800936:	7e 29                	jle    800961 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800938:	ff 75 14             	pushl  0x14(%ebp)
  80093b:	ff 75 10             	pushl  0x10(%ebp)
  80093e:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800941:	50                   	push   %eax
  800942:	68 4e 05 80 00       	push   $0x80054e
  800947:	e8 3b fc ff ff       	call   800587 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80094c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80094f:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800952:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800955:	83 c4 10             	add    $0x10,%esp
  800958:	eb 0c                	jmp    800966 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80095a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80095f:	eb 05                	jmp    800966 <vsnprintf+0x53>
  800961:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800966:	c9                   	leave  
  800967:	c3                   	ret    

00800968 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800968:	55                   	push   %ebp
  800969:	89 e5                	mov    %esp,%ebp
  80096b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80096e:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800971:	50                   	push   %eax
  800972:	ff 75 10             	pushl  0x10(%ebp)
  800975:	ff 75 0c             	pushl  0xc(%ebp)
  800978:	ff 75 08             	pushl  0x8(%ebp)
  80097b:	e8 93 ff ff ff       	call   800913 <vsnprintf>
	va_end(ap);

	return rc;
}
  800980:	c9                   	leave  
  800981:	c3                   	ret    

00800982 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800982:	55                   	push   %ebp
  800983:	89 e5                	mov    %esp,%ebp
  800985:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800988:	b8 00 00 00 00       	mov    $0x0,%eax
  80098d:	eb 01                	jmp    800990 <strlen+0xe>
		n++;
  80098f:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800990:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800994:	75 f9                	jne    80098f <strlen+0xd>
		n++;
	return n;
}
  800996:	5d                   	pop    %ebp
  800997:	c3                   	ret    

00800998 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800998:	55                   	push   %ebp
  800999:	89 e5                	mov    %esp,%ebp
  80099b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80099e:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009a1:	ba 00 00 00 00       	mov    $0x0,%edx
  8009a6:	eb 01                	jmp    8009a9 <strnlen+0x11>
		n++;
  8009a8:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009a9:	39 c2                	cmp    %eax,%edx
  8009ab:	74 08                	je     8009b5 <strnlen+0x1d>
  8009ad:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009b1:	75 f5                	jne    8009a8 <strnlen+0x10>
  8009b3:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009b5:	5d                   	pop    %ebp
  8009b6:	c3                   	ret    

008009b7 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009b7:	55                   	push   %ebp
  8009b8:	89 e5                	mov    %esp,%ebp
  8009ba:	53                   	push   %ebx
  8009bb:	8b 45 08             	mov    0x8(%ebp),%eax
  8009be:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009c1:	89 c2                	mov    %eax,%edx
  8009c3:	42                   	inc    %edx
  8009c4:	41                   	inc    %ecx
  8009c5:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009c8:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009cb:	84 db                	test   %bl,%bl
  8009cd:	75 f4                	jne    8009c3 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009cf:	5b                   	pop    %ebx
  8009d0:	5d                   	pop    %ebp
  8009d1:	c3                   	ret    

008009d2 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009d2:	55                   	push   %ebp
  8009d3:	89 e5                	mov    %esp,%ebp
  8009d5:	53                   	push   %ebx
  8009d6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009d9:	53                   	push   %ebx
  8009da:	e8 a3 ff ff ff       	call   800982 <strlen>
  8009df:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009e2:	ff 75 0c             	pushl  0xc(%ebp)
  8009e5:	01 d8                	add    %ebx,%eax
  8009e7:	50                   	push   %eax
  8009e8:	e8 ca ff ff ff       	call   8009b7 <strcpy>
	return dst;
}
  8009ed:	89 d8                	mov    %ebx,%eax
  8009ef:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009f2:	c9                   	leave  
  8009f3:	c3                   	ret    

008009f4 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009f4:	55                   	push   %ebp
  8009f5:	89 e5                	mov    %esp,%ebp
  8009f7:	56                   	push   %esi
  8009f8:	53                   	push   %ebx
  8009f9:	8b 75 08             	mov    0x8(%ebp),%esi
  8009fc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009ff:	89 f3                	mov    %esi,%ebx
  800a01:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a04:	89 f2                	mov    %esi,%edx
  800a06:	eb 0c                	jmp    800a14 <strncpy+0x20>
		*dst++ = *src;
  800a08:	42                   	inc    %edx
  800a09:	8a 01                	mov    (%ecx),%al
  800a0b:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a0e:	80 39 01             	cmpb   $0x1,(%ecx)
  800a11:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a14:	39 da                	cmp    %ebx,%edx
  800a16:	75 f0                	jne    800a08 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a18:	89 f0                	mov    %esi,%eax
  800a1a:	5b                   	pop    %ebx
  800a1b:	5e                   	pop    %esi
  800a1c:	5d                   	pop    %ebp
  800a1d:	c3                   	ret    

00800a1e <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a1e:	55                   	push   %ebp
  800a1f:	89 e5                	mov    %esp,%ebp
  800a21:	56                   	push   %esi
  800a22:	53                   	push   %ebx
  800a23:	8b 75 08             	mov    0x8(%ebp),%esi
  800a26:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a29:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a2c:	85 c0                	test   %eax,%eax
  800a2e:	74 1e                	je     800a4e <strlcpy+0x30>
  800a30:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a34:	89 f2                	mov    %esi,%edx
  800a36:	eb 05                	jmp    800a3d <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a38:	42                   	inc    %edx
  800a39:	41                   	inc    %ecx
  800a3a:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a3d:	39 c2                	cmp    %eax,%edx
  800a3f:	74 08                	je     800a49 <strlcpy+0x2b>
  800a41:	8a 19                	mov    (%ecx),%bl
  800a43:	84 db                	test   %bl,%bl
  800a45:	75 f1                	jne    800a38 <strlcpy+0x1a>
  800a47:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a49:	c6 00 00             	movb   $0x0,(%eax)
  800a4c:	eb 02                	jmp    800a50 <strlcpy+0x32>
  800a4e:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a50:	29 f0                	sub    %esi,%eax
}
  800a52:	5b                   	pop    %ebx
  800a53:	5e                   	pop    %esi
  800a54:	5d                   	pop    %ebp
  800a55:	c3                   	ret    

00800a56 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a56:	55                   	push   %ebp
  800a57:	89 e5                	mov    %esp,%ebp
  800a59:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a5c:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a5f:	eb 02                	jmp    800a63 <strcmp+0xd>
		p++, q++;
  800a61:	41                   	inc    %ecx
  800a62:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a63:	8a 01                	mov    (%ecx),%al
  800a65:	84 c0                	test   %al,%al
  800a67:	74 04                	je     800a6d <strcmp+0x17>
  800a69:	3a 02                	cmp    (%edx),%al
  800a6b:	74 f4                	je     800a61 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a6d:	0f b6 c0             	movzbl %al,%eax
  800a70:	0f b6 12             	movzbl (%edx),%edx
  800a73:	29 d0                	sub    %edx,%eax
}
  800a75:	5d                   	pop    %ebp
  800a76:	c3                   	ret    

00800a77 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a77:	55                   	push   %ebp
  800a78:	89 e5                	mov    %esp,%ebp
  800a7a:	53                   	push   %ebx
  800a7b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a81:	89 c3                	mov    %eax,%ebx
  800a83:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a86:	eb 02                	jmp    800a8a <strncmp+0x13>
		n--, p++, q++;
  800a88:	40                   	inc    %eax
  800a89:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a8a:	39 d8                	cmp    %ebx,%eax
  800a8c:	74 14                	je     800aa2 <strncmp+0x2b>
  800a8e:	8a 08                	mov    (%eax),%cl
  800a90:	84 c9                	test   %cl,%cl
  800a92:	74 04                	je     800a98 <strncmp+0x21>
  800a94:	3a 0a                	cmp    (%edx),%cl
  800a96:	74 f0                	je     800a88 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a98:	0f b6 00             	movzbl (%eax),%eax
  800a9b:	0f b6 12             	movzbl (%edx),%edx
  800a9e:	29 d0                	sub    %edx,%eax
  800aa0:	eb 05                	jmp    800aa7 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800aa2:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800aa7:	5b                   	pop    %ebx
  800aa8:	5d                   	pop    %ebp
  800aa9:	c3                   	ret    

00800aaa <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800aaa:	55                   	push   %ebp
  800aab:	89 e5                	mov    %esp,%ebp
  800aad:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab0:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ab3:	eb 05                	jmp    800aba <strchr+0x10>
		if (*s == c)
  800ab5:	38 ca                	cmp    %cl,%dl
  800ab7:	74 0c                	je     800ac5 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800ab9:	40                   	inc    %eax
  800aba:	8a 10                	mov    (%eax),%dl
  800abc:	84 d2                	test   %dl,%dl
  800abe:	75 f5                	jne    800ab5 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800ac0:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ac5:	5d                   	pop    %ebp
  800ac6:	c3                   	ret    

00800ac7 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ac7:	55                   	push   %ebp
  800ac8:	89 e5                	mov    %esp,%ebp
  800aca:	8b 45 08             	mov    0x8(%ebp),%eax
  800acd:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ad0:	eb 05                	jmp    800ad7 <strfind+0x10>
		if (*s == c)
  800ad2:	38 ca                	cmp    %cl,%dl
  800ad4:	74 07                	je     800add <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800ad6:	40                   	inc    %eax
  800ad7:	8a 10                	mov    (%eax),%dl
  800ad9:	84 d2                	test   %dl,%dl
  800adb:	75 f5                	jne    800ad2 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800add:	5d                   	pop    %ebp
  800ade:	c3                   	ret    

00800adf <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800adf:	55                   	push   %ebp
  800ae0:	89 e5                	mov    %esp,%ebp
  800ae2:	57                   	push   %edi
  800ae3:	56                   	push   %esi
  800ae4:	53                   	push   %ebx
  800ae5:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ae8:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800aeb:	85 c9                	test   %ecx,%ecx
  800aed:	74 36                	je     800b25 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800aef:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800af5:	75 28                	jne    800b1f <memset+0x40>
  800af7:	f6 c1 03             	test   $0x3,%cl
  800afa:	75 23                	jne    800b1f <memset+0x40>
		c &= 0xFF;
  800afc:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800b00:	89 d3                	mov    %edx,%ebx
  800b02:	c1 e3 08             	shl    $0x8,%ebx
  800b05:	89 d6                	mov    %edx,%esi
  800b07:	c1 e6 18             	shl    $0x18,%esi
  800b0a:	89 d0                	mov    %edx,%eax
  800b0c:	c1 e0 10             	shl    $0x10,%eax
  800b0f:	09 f0                	or     %esi,%eax
  800b11:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b13:	89 d8                	mov    %ebx,%eax
  800b15:	09 d0                	or     %edx,%eax
  800b17:	c1 e9 02             	shr    $0x2,%ecx
  800b1a:	fc                   	cld    
  800b1b:	f3 ab                	rep stos %eax,%es:(%edi)
  800b1d:	eb 06                	jmp    800b25 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b1f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b22:	fc                   	cld    
  800b23:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b25:	89 f8                	mov    %edi,%eax
  800b27:	5b                   	pop    %ebx
  800b28:	5e                   	pop    %esi
  800b29:	5f                   	pop    %edi
  800b2a:	5d                   	pop    %ebp
  800b2b:	c3                   	ret    

00800b2c <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b2c:	55                   	push   %ebp
  800b2d:	89 e5                	mov    %esp,%ebp
  800b2f:	57                   	push   %edi
  800b30:	56                   	push   %esi
  800b31:	8b 45 08             	mov    0x8(%ebp),%eax
  800b34:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b37:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b3a:	39 c6                	cmp    %eax,%esi
  800b3c:	73 33                	jae    800b71 <memmove+0x45>
  800b3e:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b41:	39 d0                	cmp    %edx,%eax
  800b43:	73 2c                	jae    800b71 <memmove+0x45>
		s += n;
		d += n;
  800b45:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b48:	89 d6                	mov    %edx,%esi
  800b4a:	09 fe                	or     %edi,%esi
  800b4c:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b52:	75 13                	jne    800b67 <memmove+0x3b>
  800b54:	f6 c1 03             	test   $0x3,%cl
  800b57:	75 0e                	jne    800b67 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b59:	83 ef 04             	sub    $0x4,%edi
  800b5c:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b5f:	c1 e9 02             	shr    $0x2,%ecx
  800b62:	fd                   	std    
  800b63:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b65:	eb 07                	jmp    800b6e <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b67:	4f                   	dec    %edi
  800b68:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b6b:	fd                   	std    
  800b6c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b6e:	fc                   	cld    
  800b6f:	eb 1d                	jmp    800b8e <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b71:	89 f2                	mov    %esi,%edx
  800b73:	09 c2                	or     %eax,%edx
  800b75:	f6 c2 03             	test   $0x3,%dl
  800b78:	75 0f                	jne    800b89 <memmove+0x5d>
  800b7a:	f6 c1 03             	test   $0x3,%cl
  800b7d:	75 0a                	jne    800b89 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b7f:	c1 e9 02             	shr    $0x2,%ecx
  800b82:	89 c7                	mov    %eax,%edi
  800b84:	fc                   	cld    
  800b85:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b87:	eb 05                	jmp    800b8e <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b89:	89 c7                	mov    %eax,%edi
  800b8b:	fc                   	cld    
  800b8c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b8e:	5e                   	pop    %esi
  800b8f:	5f                   	pop    %edi
  800b90:	5d                   	pop    %ebp
  800b91:	c3                   	ret    

00800b92 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b92:	55                   	push   %ebp
  800b93:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b95:	ff 75 10             	pushl  0x10(%ebp)
  800b98:	ff 75 0c             	pushl  0xc(%ebp)
  800b9b:	ff 75 08             	pushl  0x8(%ebp)
  800b9e:	e8 89 ff ff ff       	call   800b2c <memmove>
}
  800ba3:	c9                   	leave  
  800ba4:	c3                   	ret    

00800ba5 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800ba5:	55                   	push   %ebp
  800ba6:	89 e5                	mov    %esp,%ebp
  800ba8:	56                   	push   %esi
  800ba9:	53                   	push   %ebx
  800baa:	8b 45 08             	mov    0x8(%ebp),%eax
  800bad:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bb0:	89 c6                	mov    %eax,%esi
  800bb2:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bb5:	eb 14                	jmp    800bcb <memcmp+0x26>
		if (*s1 != *s2)
  800bb7:	8a 08                	mov    (%eax),%cl
  800bb9:	8a 1a                	mov    (%edx),%bl
  800bbb:	38 d9                	cmp    %bl,%cl
  800bbd:	74 0a                	je     800bc9 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bbf:	0f b6 c1             	movzbl %cl,%eax
  800bc2:	0f b6 db             	movzbl %bl,%ebx
  800bc5:	29 d8                	sub    %ebx,%eax
  800bc7:	eb 0b                	jmp    800bd4 <memcmp+0x2f>
		s1++, s2++;
  800bc9:	40                   	inc    %eax
  800bca:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bcb:	39 f0                	cmp    %esi,%eax
  800bcd:	75 e8                	jne    800bb7 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bcf:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bd4:	5b                   	pop    %ebx
  800bd5:	5e                   	pop    %esi
  800bd6:	5d                   	pop    %ebp
  800bd7:	c3                   	ret    

00800bd8 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bd8:	55                   	push   %ebp
  800bd9:	89 e5                	mov    %esp,%ebp
  800bdb:	53                   	push   %ebx
  800bdc:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bdf:	89 c1                	mov    %eax,%ecx
  800be1:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800be4:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800be8:	eb 08                	jmp    800bf2 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bea:	0f b6 10             	movzbl (%eax),%edx
  800bed:	39 da                	cmp    %ebx,%edx
  800bef:	74 05                	je     800bf6 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bf1:	40                   	inc    %eax
  800bf2:	39 c8                	cmp    %ecx,%eax
  800bf4:	72 f4                	jb     800bea <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bf6:	5b                   	pop    %ebx
  800bf7:	5d                   	pop    %ebp
  800bf8:	c3                   	ret    

00800bf9 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bf9:	55                   	push   %ebp
  800bfa:	89 e5                	mov    %esp,%ebp
  800bfc:	57                   	push   %edi
  800bfd:	56                   	push   %esi
  800bfe:	53                   	push   %ebx
  800bff:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c02:	eb 01                	jmp    800c05 <strtol+0xc>
		s++;
  800c04:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c05:	8a 01                	mov    (%ecx),%al
  800c07:	3c 20                	cmp    $0x20,%al
  800c09:	74 f9                	je     800c04 <strtol+0xb>
  800c0b:	3c 09                	cmp    $0x9,%al
  800c0d:	74 f5                	je     800c04 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c0f:	3c 2b                	cmp    $0x2b,%al
  800c11:	75 08                	jne    800c1b <strtol+0x22>
		s++;
  800c13:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c14:	bf 00 00 00 00       	mov    $0x0,%edi
  800c19:	eb 11                	jmp    800c2c <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c1b:	3c 2d                	cmp    $0x2d,%al
  800c1d:	75 08                	jne    800c27 <strtol+0x2e>
		s++, neg = 1;
  800c1f:	41                   	inc    %ecx
  800c20:	bf 01 00 00 00       	mov    $0x1,%edi
  800c25:	eb 05                	jmp    800c2c <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c27:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c2c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c30:	0f 84 87 00 00 00    	je     800cbd <strtol+0xc4>
  800c36:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c3a:	75 27                	jne    800c63 <strtol+0x6a>
  800c3c:	80 39 30             	cmpb   $0x30,(%ecx)
  800c3f:	75 22                	jne    800c63 <strtol+0x6a>
  800c41:	e9 88 00 00 00       	jmp    800cce <strtol+0xd5>
		s += 2, base = 16;
  800c46:	83 c1 02             	add    $0x2,%ecx
  800c49:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c50:	eb 11                	jmp    800c63 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c52:	41                   	inc    %ecx
  800c53:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c5a:	eb 07                	jmp    800c63 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c5c:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c63:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c68:	8a 11                	mov    (%ecx),%dl
  800c6a:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c6d:	80 fb 09             	cmp    $0x9,%bl
  800c70:	77 08                	ja     800c7a <strtol+0x81>
			dig = *s - '0';
  800c72:	0f be d2             	movsbl %dl,%edx
  800c75:	83 ea 30             	sub    $0x30,%edx
  800c78:	eb 22                	jmp    800c9c <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c7a:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c7d:	89 f3                	mov    %esi,%ebx
  800c7f:	80 fb 19             	cmp    $0x19,%bl
  800c82:	77 08                	ja     800c8c <strtol+0x93>
			dig = *s - 'a' + 10;
  800c84:	0f be d2             	movsbl %dl,%edx
  800c87:	83 ea 57             	sub    $0x57,%edx
  800c8a:	eb 10                	jmp    800c9c <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c8c:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c8f:	89 f3                	mov    %esi,%ebx
  800c91:	80 fb 19             	cmp    $0x19,%bl
  800c94:	77 14                	ja     800caa <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c96:	0f be d2             	movsbl %dl,%edx
  800c99:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c9c:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c9f:	7d 09                	jge    800caa <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800ca1:	41                   	inc    %ecx
  800ca2:	0f af 45 10          	imul   0x10(%ebp),%eax
  800ca6:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800ca8:	eb be                	jmp    800c68 <strtol+0x6f>

	if (endptr)
  800caa:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800cae:	74 05                	je     800cb5 <strtol+0xbc>
		*endptr = (char *) s;
  800cb0:	8b 75 0c             	mov    0xc(%ebp),%esi
  800cb3:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800cb5:	85 ff                	test   %edi,%edi
  800cb7:	74 21                	je     800cda <strtol+0xe1>
  800cb9:	f7 d8                	neg    %eax
  800cbb:	eb 1d                	jmp    800cda <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800cbd:	80 39 30             	cmpb   $0x30,(%ecx)
  800cc0:	75 9a                	jne    800c5c <strtol+0x63>
  800cc2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cc6:	0f 84 7a ff ff ff    	je     800c46 <strtol+0x4d>
  800ccc:	eb 84                	jmp    800c52 <strtol+0x59>
  800cce:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cd2:	0f 84 6e ff ff ff    	je     800c46 <strtol+0x4d>
  800cd8:	eb 89                	jmp    800c63 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cda:	5b                   	pop    %ebx
  800cdb:	5e                   	pop    %esi
  800cdc:	5f                   	pop    %edi
  800cdd:	5d                   	pop    %ebp
  800cde:	c3                   	ret    
  800cdf:	90                   	nop

00800ce0 <__udivdi3>:
  800ce0:	55                   	push   %ebp
  800ce1:	57                   	push   %edi
  800ce2:	56                   	push   %esi
  800ce3:	53                   	push   %ebx
  800ce4:	83 ec 1c             	sub    $0x1c,%esp
  800ce7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ceb:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cef:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cf3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cf7:	89 ca                	mov    %ecx,%edx
  800cf9:	89 f8                	mov    %edi,%eax
  800cfb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cff:	85 f6                	test   %esi,%esi
  800d01:	75 2d                	jne    800d30 <__udivdi3+0x50>
  800d03:	39 cf                	cmp    %ecx,%edi
  800d05:	77 65                	ja     800d6c <__udivdi3+0x8c>
  800d07:	89 fd                	mov    %edi,%ebp
  800d09:	85 ff                	test   %edi,%edi
  800d0b:	75 0b                	jne    800d18 <__udivdi3+0x38>
  800d0d:	b8 01 00 00 00       	mov    $0x1,%eax
  800d12:	31 d2                	xor    %edx,%edx
  800d14:	f7 f7                	div    %edi
  800d16:	89 c5                	mov    %eax,%ebp
  800d18:	31 d2                	xor    %edx,%edx
  800d1a:	89 c8                	mov    %ecx,%eax
  800d1c:	f7 f5                	div    %ebp
  800d1e:	89 c1                	mov    %eax,%ecx
  800d20:	89 d8                	mov    %ebx,%eax
  800d22:	f7 f5                	div    %ebp
  800d24:	89 cf                	mov    %ecx,%edi
  800d26:	89 fa                	mov    %edi,%edx
  800d28:	83 c4 1c             	add    $0x1c,%esp
  800d2b:	5b                   	pop    %ebx
  800d2c:	5e                   	pop    %esi
  800d2d:	5f                   	pop    %edi
  800d2e:	5d                   	pop    %ebp
  800d2f:	c3                   	ret    
  800d30:	39 ce                	cmp    %ecx,%esi
  800d32:	77 28                	ja     800d5c <__udivdi3+0x7c>
  800d34:	0f bd fe             	bsr    %esi,%edi
  800d37:	83 f7 1f             	xor    $0x1f,%edi
  800d3a:	75 40                	jne    800d7c <__udivdi3+0x9c>
  800d3c:	39 ce                	cmp    %ecx,%esi
  800d3e:	72 0a                	jb     800d4a <__udivdi3+0x6a>
  800d40:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d44:	0f 87 9e 00 00 00    	ja     800de8 <__udivdi3+0x108>
  800d4a:	b8 01 00 00 00       	mov    $0x1,%eax
  800d4f:	89 fa                	mov    %edi,%edx
  800d51:	83 c4 1c             	add    $0x1c,%esp
  800d54:	5b                   	pop    %ebx
  800d55:	5e                   	pop    %esi
  800d56:	5f                   	pop    %edi
  800d57:	5d                   	pop    %ebp
  800d58:	c3                   	ret    
  800d59:	8d 76 00             	lea    0x0(%esi),%esi
  800d5c:	31 ff                	xor    %edi,%edi
  800d5e:	31 c0                	xor    %eax,%eax
  800d60:	89 fa                	mov    %edi,%edx
  800d62:	83 c4 1c             	add    $0x1c,%esp
  800d65:	5b                   	pop    %ebx
  800d66:	5e                   	pop    %esi
  800d67:	5f                   	pop    %edi
  800d68:	5d                   	pop    %ebp
  800d69:	c3                   	ret    
  800d6a:	66 90                	xchg   %ax,%ax
  800d6c:	89 d8                	mov    %ebx,%eax
  800d6e:	f7 f7                	div    %edi
  800d70:	31 ff                	xor    %edi,%edi
  800d72:	89 fa                	mov    %edi,%edx
  800d74:	83 c4 1c             	add    $0x1c,%esp
  800d77:	5b                   	pop    %ebx
  800d78:	5e                   	pop    %esi
  800d79:	5f                   	pop    %edi
  800d7a:	5d                   	pop    %ebp
  800d7b:	c3                   	ret    
  800d7c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d81:	89 eb                	mov    %ebp,%ebx
  800d83:	29 fb                	sub    %edi,%ebx
  800d85:	89 f9                	mov    %edi,%ecx
  800d87:	d3 e6                	shl    %cl,%esi
  800d89:	89 c5                	mov    %eax,%ebp
  800d8b:	88 d9                	mov    %bl,%cl
  800d8d:	d3 ed                	shr    %cl,%ebp
  800d8f:	89 e9                	mov    %ebp,%ecx
  800d91:	09 f1                	or     %esi,%ecx
  800d93:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d97:	89 f9                	mov    %edi,%ecx
  800d99:	d3 e0                	shl    %cl,%eax
  800d9b:	89 c5                	mov    %eax,%ebp
  800d9d:	89 d6                	mov    %edx,%esi
  800d9f:	88 d9                	mov    %bl,%cl
  800da1:	d3 ee                	shr    %cl,%esi
  800da3:	89 f9                	mov    %edi,%ecx
  800da5:	d3 e2                	shl    %cl,%edx
  800da7:	8b 44 24 08          	mov    0x8(%esp),%eax
  800dab:	88 d9                	mov    %bl,%cl
  800dad:	d3 e8                	shr    %cl,%eax
  800daf:	09 c2                	or     %eax,%edx
  800db1:	89 d0                	mov    %edx,%eax
  800db3:	89 f2                	mov    %esi,%edx
  800db5:	f7 74 24 0c          	divl   0xc(%esp)
  800db9:	89 d6                	mov    %edx,%esi
  800dbb:	89 c3                	mov    %eax,%ebx
  800dbd:	f7 e5                	mul    %ebp
  800dbf:	39 d6                	cmp    %edx,%esi
  800dc1:	72 19                	jb     800ddc <__udivdi3+0xfc>
  800dc3:	74 0b                	je     800dd0 <__udivdi3+0xf0>
  800dc5:	89 d8                	mov    %ebx,%eax
  800dc7:	31 ff                	xor    %edi,%edi
  800dc9:	e9 58 ff ff ff       	jmp    800d26 <__udivdi3+0x46>
  800dce:	66 90                	xchg   %ax,%ax
  800dd0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dd4:	89 f9                	mov    %edi,%ecx
  800dd6:	d3 e2                	shl    %cl,%edx
  800dd8:	39 c2                	cmp    %eax,%edx
  800dda:	73 e9                	jae    800dc5 <__udivdi3+0xe5>
  800ddc:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ddf:	31 ff                	xor    %edi,%edi
  800de1:	e9 40 ff ff ff       	jmp    800d26 <__udivdi3+0x46>
  800de6:	66 90                	xchg   %ax,%ax
  800de8:	31 c0                	xor    %eax,%eax
  800dea:	e9 37 ff ff ff       	jmp    800d26 <__udivdi3+0x46>
  800def:	90                   	nop

00800df0 <__umoddi3>:
  800df0:	55                   	push   %ebp
  800df1:	57                   	push   %edi
  800df2:	56                   	push   %esi
  800df3:	53                   	push   %ebx
  800df4:	83 ec 1c             	sub    $0x1c,%esp
  800df7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800dfb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800dff:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e03:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e07:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e0b:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e0f:	89 f3                	mov    %esi,%ebx
  800e11:	89 fa                	mov    %edi,%edx
  800e13:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e17:	89 34 24             	mov    %esi,(%esp)
  800e1a:	85 c0                	test   %eax,%eax
  800e1c:	75 1a                	jne    800e38 <__umoddi3+0x48>
  800e1e:	39 f7                	cmp    %esi,%edi
  800e20:	0f 86 a2 00 00 00    	jbe    800ec8 <__umoddi3+0xd8>
  800e26:	89 c8                	mov    %ecx,%eax
  800e28:	89 f2                	mov    %esi,%edx
  800e2a:	f7 f7                	div    %edi
  800e2c:	89 d0                	mov    %edx,%eax
  800e2e:	31 d2                	xor    %edx,%edx
  800e30:	83 c4 1c             	add    $0x1c,%esp
  800e33:	5b                   	pop    %ebx
  800e34:	5e                   	pop    %esi
  800e35:	5f                   	pop    %edi
  800e36:	5d                   	pop    %ebp
  800e37:	c3                   	ret    
  800e38:	39 f0                	cmp    %esi,%eax
  800e3a:	0f 87 ac 00 00 00    	ja     800eec <__umoddi3+0xfc>
  800e40:	0f bd e8             	bsr    %eax,%ebp
  800e43:	83 f5 1f             	xor    $0x1f,%ebp
  800e46:	0f 84 ac 00 00 00    	je     800ef8 <__umoddi3+0x108>
  800e4c:	bf 20 00 00 00       	mov    $0x20,%edi
  800e51:	29 ef                	sub    %ebp,%edi
  800e53:	89 fe                	mov    %edi,%esi
  800e55:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e59:	89 e9                	mov    %ebp,%ecx
  800e5b:	d3 e0                	shl    %cl,%eax
  800e5d:	89 d7                	mov    %edx,%edi
  800e5f:	89 f1                	mov    %esi,%ecx
  800e61:	d3 ef                	shr    %cl,%edi
  800e63:	09 c7                	or     %eax,%edi
  800e65:	89 e9                	mov    %ebp,%ecx
  800e67:	d3 e2                	shl    %cl,%edx
  800e69:	89 14 24             	mov    %edx,(%esp)
  800e6c:	89 d8                	mov    %ebx,%eax
  800e6e:	d3 e0                	shl    %cl,%eax
  800e70:	89 c2                	mov    %eax,%edx
  800e72:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e76:	d3 e0                	shl    %cl,%eax
  800e78:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e7c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e80:	89 f1                	mov    %esi,%ecx
  800e82:	d3 e8                	shr    %cl,%eax
  800e84:	09 d0                	or     %edx,%eax
  800e86:	d3 eb                	shr    %cl,%ebx
  800e88:	89 da                	mov    %ebx,%edx
  800e8a:	f7 f7                	div    %edi
  800e8c:	89 d3                	mov    %edx,%ebx
  800e8e:	f7 24 24             	mull   (%esp)
  800e91:	89 c6                	mov    %eax,%esi
  800e93:	89 d1                	mov    %edx,%ecx
  800e95:	39 d3                	cmp    %edx,%ebx
  800e97:	0f 82 87 00 00 00    	jb     800f24 <__umoddi3+0x134>
  800e9d:	0f 84 91 00 00 00    	je     800f34 <__umoddi3+0x144>
  800ea3:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ea7:	29 f2                	sub    %esi,%edx
  800ea9:	19 cb                	sbb    %ecx,%ebx
  800eab:	89 d8                	mov    %ebx,%eax
  800ead:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800eb1:	d3 e0                	shl    %cl,%eax
  800eb3:	89 e9                	mov    %ebp,%ecx
  800eb5:	d3 ea                	shr    %cl,%edx
  800eb7:	09 d0                	or     %edx,%eax
  800eb9:	89 e9                	mov    %ebp,%ecx
  800ebb:	d3 eb                	shr    %cl,%ebx
  800ebd:	89 da                	mov    %ebx,%edx
  800ebf:	83 c4 1c             	add    $0x1c,%esp
  800ec2:	5b                   	pop    %ebx
  800ec3:	5e                   	pop    %esi
  800ec4:	5f                   	pop    %edi
  800ec5:	5d                   	pop    %ebp
  800ec6:	c3                   	ret    
  800ec7:	90                   	nop
  800ec8:	89 fd                	mov    %edi,%ebp
  800eca:	85 ff                	test   %edi,%edi
  800ecc:	75 0b                	jne    800ed9 <__umoddi3+0xe9>
  800ece:	b8 01 00 00 00       	mov    $0x1,%eax
  800ed3:	31 d2                	xor    %edx,%edx
  800ed5:	f7 f7                	div    %edi
  800ed7:	89 c5                	mov    %eax,%ebp
  800ed9:	89 f0                	mov    %esi,%eax
  800edb:	31 d2                	xor    %edx,%edx
  800edd:	f7 f5                	div    %ebp
  800edf:	89 c8                	mov    %ecx,%eax
  800ee1:	f7 f5                	div    %ebp
  800ee3:	89 d0                	mov    %edx,%eax
  800ee5:	e9 44 ff ff ff       	jmp    800e2e <__umoddi3+0x3e>
  800eea:	66 90                	xchg   %ax,%ax
  800eec:	89 c8                	mov    %ecx,%eax
  800eee:	89 f2                	mov    %esi,%edx
  800ef0:	83 c4 1c             	add    $0x1c,%esp
  800ef3:	5b                   	pop    %ebx
  800ef4:	5e                   	pop    %esi
  800ef5:	5f                   	pop    %edi
  800ef6:	5d                   	pop    %ebp
  800ef7:	c3                   	ret    
  800ef8:	3b 04 24             	cmp    (%esp),%eax
  800efb:	72 06                	jb     800f03 <__umoddi3+0x113>
  800efd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f01:	77 0f                	ja     800f12 <__umoddi3+0x122>
  800f03:	89 f2                	mov    %esi,%edx
  800f05:	29 f9                	sub    %edi,%ecx
  800f07:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f0b:	89 14 24             	mov    %edx,(%esp)
  800f0e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f12:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f16:	8b 14 24             	mov    (%esp),%edx
  800f19:	83 c4 1c             	add    $0x1c,%esp
  800f1c:	5b                   	pop    %ebx
  800f1d:	5e                   	pop    %esi
  800f1e:	5f                   	pop    %edi
  800f1f:	5d                   	pop    %ebp
  800f20:	c3                   	ret    
  800f21:	8d 76 00             	lea    0x0(%esi),%esi
  800f24:	2b 04 24             	sub    (%esp),%eax
  800f27:	19 fa                	sbb    %edi,%edx
  800f29:	89 d1                	mov    %edx,%ecx
  800f2b:	89 c6                	mov    %eax,%esi
  800f2d:	e9 71 ff ff ff       	jmp    800ea3 <__umoddi3+0xb3>
  800f32:	66 90                	xchg   %ax,%ax
  800f34:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f38:	72 ea                	jb     800f24 <__umoddi3+0x134>
  800f3a:	89 d9                	mov    %ebx,%ecx
  800f3c:	e9 62 ff ff ff       	jmp    800ea3 <__umoddi3+0xb3>
