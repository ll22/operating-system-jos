
obj/user/faultnostack.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 23 00 00 00       	call   800054 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

void _pgfault_upcall();

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_env_set_pgfault_upcall(0, (void*) _pgfault_upcall);
  800039:	68 81 03 80 00       	push   $0x800381
  80003e:	6a 00                	push   $0x0
  800040:	e8 96 02 00 00       	call   8002db <sys_env_set_pgfault_upcall>
	*(int*)0 = 0;
  800045:	c7 05 00 00 00 00 00 	movl   $0x0,0x0
  80004c:	00 00 00 
}
  80004f:	83 c4 10             	add    $0x10,%esp
  800052:	c9                   	leave  
  800053:	c3                   	ret    

00800054 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800054:	55                   	push   %ebp
  800055:	89 e5                	mov    %esp,%ebp
  800057:	56                   	push   %esi
  800058:	53                   	push   %ebx
  800059:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80005c:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80005f:	e8 cf 00 00 00       	call   800133 <sys_getenvid>
  800064:	25 ff 03 00 00       	and    $0x3ff,%eax
  800069:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800070:	c1 e0 07             	shl    $0x7,%eax
  800073:	29 d0                	sub    %edx,%eax
  800075:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80007a:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80007f:	85 db                	test   %ebx,%ebx
  800081:	7e 07                	jle    80008a <libmain+0x36>
		binaryname = argv[0];
  800083:	8b 06                	mov    (%esi),%eax
  800085:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80008a:	83 ec 08             	sub    $0x8,%esp
  80008d:	56                   	push   %esi
  80008e:	53                   	push   %ebx
  80008f:	e8 9f ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800094:	e8 0a 00 00 00       	call   8000a3 <exit>
}
  800099:	83 c4 10             	add    $0x10,%esp
  80009c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80009f:	5b                   	pop    %ebx
  8000a0:	5e                   	pop    %esi
  8000a1:	5d                   	pop    %ebp
  8000a2:	c3                   	ret    

008000a3 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000a3:	55                   	push   %ebp
  8000a4:	89 e5                	mov    %esp,%ebp
  8000a6:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000a9:	6a 00                	push   $0x0
  8000ab:	e8 42 00 00 00       	call   8000f2 <sys_env_destroy>
}
  8000b0:	83 c4 10             	add    $0x10,%esp
  8000b3:	c9                   	leave  
  8000b4:	c3                   	ret    

008000b5 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000b5:	55                   	push   %ebp
  8000b6:	89 e5                	mov    %esp,%ebp
  8000b8:	57                   	push   %edi
  8000b9:	56                   	push   %esi
  8000ba:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000bb:	b8 00 00 00 00       	mov    $0x0,%eax
  8000c0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000c3:	8b 55 08             	mov    0x8(%ebp),%edx
  8000c6:	89 c3                	mov    %eax,%ebx
  8000c8:	89 c7                	mov    %eax,%edi
  8000ca:	89 c6                	mov    %eax,%esi
  8000cc:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000ce:	5b                   	pop    %ebx
  8000cf:	5e                   	pop    %esi
  8000d0:	5f                   	pop    %edi
  8000d1:	5d                   	pop    %ebp
  8000d2:	c3                   	ret    

008000d3 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000d3:	55                   	push   %ebp
  8000d4:	89 e5                	mov    %esp,%ebp
  8000d6:	57                   	push   %edi
  8000d7:	56                   	push   %esi
  8000d8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000d9:	ba 00 00 00 00       	mov    $0x0,%edx
  8000de:	b8 01 00 00 00       	mov    $0x1,%eax
  8000e3:	89 d1                	mov    %edx,%ecx
  8000e5:	89 d3                	mov    %edx,%ebx
  8000e7:	89 d7                	mov    %edx,%edi
  8000e9:	89 d6                	mov    %edx,%esi
  8000eb:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000ed:	5b                   	pop    %ebx
  8000ee:	5e                   	pop    %esi
  8000ef:	5f                   	pop    %edi
  8000f0:	5d                   	pop    %ebp
  8000f1:	c3                   	ret    

008000f2 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000f2:	55                   	push   %ebp
  8000f3:	89 e5                	mov    %esp,%ebp
  8000f5:	57                   	push   %edi
  8000f6:	56                   	push   %esi
  8000f7:	53                   	push   %ebx
  8000f8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000fb:	b9 00 00 00 00       	mov    $0x0,%ecx
  800100:	b8 03 00 00 00       	mov    $0x3,%eax
  800105:	8b 55 08             	mov    0x8(%ebp),%edx
  800108:	89 cb                	mov    %ecx,%ebx
  80010a:	89 cf                	mov    %ecx,%edi
  80010c:	89 ce                	mov    %ecx,%esi
  80010e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800110:	85 c0                	test   %eax,%eax
  800112:	7e 17                	jle    80012b <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800114:	83 ec 0c             	sub    $0xc,%esp
  800117:	50                   	push   %eax
  800118:	6a 03                	push   $0x3
  80011a:	68 ea 0f 80 00       	push   $0x800fea
  80011f:	6a 23                	push   $0x23
  800121:	68 07 10 80 00       	push   $0x801007
  800126:	e8 7e 02 00 00       	call   8003a9 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80012b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80012e:	5b                   	pop    %ebx
  80012f:	5e                   	pop    %esi
  800130:	5f                   	pop    %edi
  800131:	5d                   	pop    %ebp
  800132:	c3                   	ret    

00800133 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800133:	55                   	push   %ebp
  800134:	89 e5                	mov    %esp,%ebp
  800136:	57                   	push   %edi
  800137:	56                   	push   %esi
  800138:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800139:	ba 00 00 00 00       	mov    $0x0,%edx
  80013e:	b8 02 00 00 00       	mov    $0x2,%eax
  800143:	89 d1                	mov    %edx,%ecx
  800145:	89 d3                	mov    %edx,%ebx
  800147:	89 d7                	mov    %edx,%edi
  800149:	89 d6                	mov    %edx,%esi
  80014b:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80014d:	5b                   	pop    %ebx
  80014e:	5e                   	pop    %esi
  80014f:	5f                   	pop    %edi
  800150:	5d                   	pop    %ebp
  800151:	c3                   	ret    

00800152 <sys_yield>:

void
sys_yield(void)
{
  800152:	55                   	push   %ebp
  800153:	89 e5                	mov    %esp,%ebp
  800155:	57                   	push   %edi
  800156:	56                   	push   %esi
  800157:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800158:	ba 00 00 00 00       	mov    $0x0,%edx
  80015d:	b8 0b 00 00 00       	mov    $0xb,%eax
  800162:	89 d1                	mov    %edx,%ecx
  800164:	89 d3                	mov    %edx,%ebx
  800166:	89 d7                	mov    %edx,%edi
  800168:	89 d6                	mov    %edx,%esi
  80016a:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80016c:	5b                   	pop    %ebx
  80016d:	5e                   	pop    %esi
  80016e:	5f                   	pop    %edi
  80016f:	5d                   	pop    %ebp
  800170:	c3                   	ret    

00800171 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800171:	55                   	push   %ebp
  800172:	89 e5                	mov    %esp,%ebp
  800174:	57                   	push   %edi
  800175:	56                   	push   %esi
  800176:	53                   	push   %ebx
  800177:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80017a:	be 00 00 00 00       	mov    $0x0,%esi
  80017f:	b8 04 00 00 00       	mov    $0x4,%eax
  800184:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800187:	8b 55 08             	mov    0x8(%ebp),%edx
  80018a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80018d:	89 f7                	mov    %esi,%edi
  80018f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800191:	85 c0                	test   %eax,%eax
  800193:	7e 17                	jle    8001ac <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800195:	83 ec 0c             	sub    $0xc,%esp
  800198:	50                   	push   %eax
  800199:	6a 04                	push   $0x4
  80019b:	68 ea 0f 80 00       	push   $0x800fea
  8001a0:	6a 23                	push   $0x23
  8001a2:	68 07 10 80 00       	push   $0x801007
  8001a7:	e8 fd 01 00 00       	call   8003a9 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  8001ac:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001af:	5b                   	pop    %ebx
  8001b0:	5e                   	pop    %esi
  8001b1:	5f                   	pop    %edi
  8001b2:	5d                   	pop    %ebp
  8001b3:	c3                   	ret    

008001b4 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001b4:	55                   	push   %ebp
  8001b5:	89 e5                	mov    %esp,%ebp
  8001b7:	57                   	push   %edi
  8001b8:	56                   	push   %esi
  8001b9:	53                   	push   %ebx
  8001ba:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001bd:	b8 05 00 00 00       	mov    $0x5,%eax
  8001c2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001c5:	8b 55 08             	mov    0x8(%ebp),%edx
  8001c8:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001cb:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001ce:	8b 75 18             	mov    0x18(%ebp),%esi
  8001d1:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001d3:	85 c0                	test   %eax,%eax
  8001d5:	7e 17                	jle    8001ee <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001d7:	83 ec 0c             	sub    $0xc,%esp
  8001da:	50                   	push   %eax
  8001db:	6a 05                	push   $0x5
  8001dd:	68 ea 0f 80 00       	push   $0x800fea
  8001e2:	6a 23                	push   $0x23
  8001e4:	68 07 10 80 00       	push   $0x801007
  8001e9:	e8 bb 01 00 00       	call   8003a9 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001ee:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001f1:	5b                   	pop    %ebx
  8001f2:	5e                   	pop    %esi
  8001f3:	5f                   	pop    %edi
  8001f4:	5d                   	pop    %ebp
  8001f5:	c3                   	ret    

008001f6 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001f6:	55                   	push   %ebp
  8001f7:	89 e5                	mov    %esp,%ebp
  8001f9:	57                   	push   %edi
  8001fa:	56                   	push   %esi
  8001fb:	53                   	push   %ebx
  8001fc:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001ff:	bb 00 00 00 00       	mov    $0x0,%ebx
  800204:	b8 06 00 00 00       	mov    $0x6,%eax
  800209:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80020c:	8b 55 08             	mov    0x8(%ebp),%edx
  80020f:	89 df                	mov    %ebx,%edi
  800211:	89 de                	mov    %ebx,%esi
  800213:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800215:	85 c0                	test   %eax,%eax
  800217:	7e 17                	jle    800230 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800219:	83 ec 0c             	sub    $0xc,%esp
  80021c:	50                   	push   %eax
  80021d:	6a 06                	push   $0x6
  80021f:	68 ea 0f 80 00       	push   $0x800fea
  800224:	6a 23                	push   $0x23
  800226:	68 07 10 80 00       	push   $0x801007
  80022b:	e8 79 01 00 00       	call   8003a9 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800230:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800233:	5b                   	pop    %ebx
  800234:	5e                   	pop    %esi
  800235:	5f                   	pop    %edi
  800236:	5d                   	pop    %ebp
  800237:	c3                   	ret    

00800238 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800238:	55                   	push   %ebp
  800239:	89 e5                	mov    %esp,%ebp
  80023b:	57                   	push   %edi
  80023c:	56                   	push   %esi
  80023d:	53                   	push   %ebx
  80023e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800241:	bb 00 00 00 00       	mov    $0x0,%ebx
  800246:	b8 08 00 00 00       	mov    $0x8,%eax
  80024b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80024e:	8b 55 08             	mov    0x8(%ebp),%edx
  800251:	89 df                	mov    %ebx,%edi
  800253:	89 de                	mov    %ebx,%esi
  800255:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800257:	85 c0                	test   %eax,%eax
  800259:	7e 17                	jle    800272 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80025b:	83 ec 0c             	sub    $0xc,%esp
  80025e:	50                   	push   %eax
  80025f:	6a 08                	push   $0x8
  800261:	68 ea 0f 80 00       	push   $0x800fea
  800266:	6a 23                	push   $0x23
  800268:	68 07 10 80 00       	push   $0x801007
  80026d:	e8 37 01 00 00       	call   8003a9 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800272:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800275:	5b                   	pop    %ebx
  800276:	5e                   	pop    %esi
  800277:	5f                   	pop    %edi
  800278:	5d                   	pop    %ebp
  800279:	c3                   	ret    

0080027a <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80027a:	55                   	push   %ebp
  80027b:	89 e5                	mov    %esp,%ebp
  80027d:	57                   	push   %edi
  80027e:	56                   	push   %esi
  80027f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800280:	ba 00 00 00 00       	mov    $0x0,%edx
  800285:	b8 0c 00 00 00       	mov    $0xc,%eax
  80028a:	89 d1                	mov    %edx,%ecx
  80028c:	89 d3                	mov    %edx,%ebx
  80028e:	89 d7                	mov    %edx,%edi
  800290:	89 d6                	mov    %edx,%esi
  800292:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800294:	5b                   	pop    %ebx
  800295:	5e                   	pop    %esi
  800296:	5f                   	pop    %edi
  800297:	5d                   	pop    %ebp
  800298:	c3                   	ret    

00800299 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800299:	55                   	push   %ebp
  80029a:	89 e5                	mov    %esp,%ebp
  80029c:	57                   	push   %edi
  80029d:	56                   	push   %esi
  80029e:	53                   	push   %ebx
  80029f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002a2:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002a7:	b8 09 00 00 00       	mov    $0x9,%eax
  8002ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002af:	8b 55 08             	mov    0x8(%ebp),%edx
  8002b2:	89 df                	mov    %ebx,%edi
  8002b4:	89 de                	mov    %ebx,%esi
  8002b6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002b8:	85 c0                	test   %eax,%eax
  8002ba:	7e 17                	jle    8002d3 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002bc:	83 ec 0c             	sub    $0xc,%esp
  8002bf:	50                   	push   %eax
  8002c0:	6a 09                	push   $0x9
  8002c2:	68 ea 0f 80 00       	push   $0x800fea
  8002c7:	6a 23                	push   $0x23
  8002c9:	68 07 10 80 00       	push   $0x801007
  8002ce:	e8 d6 00 00 00       	call   8003a9 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d6:	5b                   	pop    %ebx
  8002d7:	5e                   	pop    %esi
  8002d8:	5f                   	pop    %edi
  8002d9:	5d                   	pop    %ebp
  8002da:	c3                   	ret    

008002db <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002db:	55                   	push   %ebp
  8002dc:	89 e5                	mov    %esp,%ebp
  8002de:	57                   	push   %edi
  8002df:	56                   	push   %esi
  8002e0:	53                   	push   %ebx
  8002e1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002e4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002e9:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002ee:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002f1:	8b 55 08             	mov    0x8(%ebp),%edx
  8002f4:	89 df                	mov    %ebx,%edi
  8002f6:	89 de                	mov    %ebx,%esi
  8002f8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002fa:	85 c0                	test   %eax,%eax
  8002fc:	7e 17                	jle    800315 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002fe:	83 ec 0c             	sub    $0xc,%esp
  800301:	50                   	push   %eax
  800302:	6a 0a                	push   $0xa
  800304:	68 ea 0f 80 00       	push   $0x800fea
  800309:	6a 23                	push   $0x23
  80030b:	68 07 10 80 00       	push   $0x801007
  800310:	e8 94 00 00 00       	call   8003a9 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800315:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800318:	5b                   	pop    %ebx
  800319:	5e                   	pop    %esi
  80031a:	5f                   	pop    %edi
  80031b:	5d                   	pop    %ebp
  80031c:	c3                   	ret    

0080031d <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  80031d:	55                   	push   %ebp
  80031e:	89 e5                	mov    %esp,%ebp
  800320:	57                   	push   %edi
  800321:	56                   	push   %esi
  800322:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800323:	be 00 00 00 00       	mov    $0x0,%esi
  800328:	b8 0d 00 00 00       	mov    $0xd,%eax
  80032d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800330:	8b 55 08             	mov    0x8(%ebp),%edx
  800333:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800336:	8b 7d 14             	mov    0x14(%ebp),%edi
  800339:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  80033b:	5b                   	pop    %ebx
  80033c:	5e                   	pop    %esi
  80033d:	5f                   	pop    %edi
  80033e:	5d                   	pop    %ebp
  80033f:	c3                   	ret    

00800340 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800340:	55                   	push   %ebp
  800341:	89 e5                	mov    %esp,%ebp
  800343:	57                   	push   %edi
  800344:	56                   	push   %esi
  800345:	53                   	push   %ebx
  800346:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800349:	b9 00 00 00 00       	mov    $0x0,%ecx
  80034e:	b8 0e 00 00 00       	mov    $0xe,%eax
  800353:	8b 55 08             	mov    0x8(%ebp),%edx
  800356:	89 cb                	mov    %ecx,%ebx
  800358:	89 cf                	mov    %ecx,%edi
  80035a:	89 ce                	mov    %ecx,%esi
  80035c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80035e:	85 c0                	test   %eax,%eax
  800360:	7e 17                	jle    800379 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800362:	83 ec 0c             	sub    $0xc,%esp
  800365:	50                   	push   %eax
  800366:	6a 0e                	push   $0xe
  800368:	68 ea 0f 80 00       	push   $0x800fea
  80036d:	6a 23                	push   $0x23
  80036f:	68 07 10 80 00       	push   $0x801007
  800374:	e8 30 00 00 00       	call   8003a9 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800379:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80037c:	5b                   	pop    %ebx
  80037d:	5e                   	pop    %esi
  80037e:	5f                   	pop    %edi
  80037f:	5d                   	pop    %ebp
  800380:	c3                   	ret    

00800381 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  800381:	54                   	push   %esp
	movl _pgfault_handler, %eax
  800382:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  800387:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  800389:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  80038c:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  80038e:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  800392:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  800396:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  800397:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  80039a:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  80039c:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  80039f:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  8003a2:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  8003a3:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8003a6:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8003a7:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8003a8:	c3                   	ret    

008003a9 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8003a9:	55                   	push   %ebp
  8003aa:	89 e5                	mov    %esp,%ebp
  8003ac:	56                   	push   %esi
  8003ad:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8003ae:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8003b1:	8b 35 00 20 80 00    	mov    0x802000,%esi
  8003b7:	e8 77 fd ff ff       	call   800133 <sys_getenvid>
  8003bc:	83 ec 0c             	sub    $0xc,%esp
  8003bf:	ff 75 0c             	pushl  0xc(%ebp)
  8003c2:	ff 75 08             	pushl  0x8(%ebp)
  8003c5:	56                   	push   %esi
  8003c6:	50                   	push   %eax
  8003c7:	68 18 10 80 00       	push   $0x801018
  8003cc:	e8 b0 00 00 00       	call   800481 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8003d1:	83 c4 18             	add    $0x18,%esp
  8003d4:	53                   	push   %ebx
  8003d5:	ff 75 10             	pushl  0x10(%ebp)
  8003d8:	e8 53 00 00 00       	call   800430 <vcprintf>
	cprintf("\n");
  8003dd:	c7 04 24 3b 10 80 00 	movl   $0x80103b,(%esp)
  8003e4:	e8 98 00 00 00       	call   800481 <cprintf>
  8003e9:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003ec:	cc                   	int3   
  8003ed:	eb fd                	jmp    8003ec <_panic+0x43>

008003ef <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003ef:	55                   	push   %ebp
  8003f0:	89 e5                	mov    %esp,%ebp
  8003f2:	53                   	push   %ebx
  8003f3:	83 ec 04             	sub    $0x4,%esp
  8003f6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003f9:	8b 13                	mov    (%ebx),%edx
  8003fb:	8d 42 01             	lea    0x1(%edx),%eax
  8003fe:	89 03                	mov    %eax,(%ebx)
  800400:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800403:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800407:	3d ff 00 00 00       	cmp    $0xff,%eax
  80040c:	75 1a                	jne    800428 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80040e:	83 ec 08             	sub    $0x8,%esp
  800411:	68 ff 00 00 00       	push   $0xff
  800416:	8d 43 08             	lea    0x8(%ebx),%eax
  800419:	50                   	push   %eax
  80041a:	e8 96 fc ff ff       	call   8000b5 <sys_cputs>
		b->idx = 0;
  80041f:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800425:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800428:	ff 43 04             	incl   0x4(%ebx)
}
  80042b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80042e:	c9                   	leave  
  80042f:	c3                   	ret    

00800430 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800430:	55                   	push   %ebp
  800431:	89 e5                	mov    %esp,%ebp
  800433:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800439:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800440:	00 00 00 
	b.cnt = 0;
  800443:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80044a:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80044d:	ff 75 0c             	pushl  0xc(%ebp)
  800450:	ff 75 08             	pushl  0x8(%ebp)
  800453:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800459:	50                   	push   %eax
  80045a:	68 ef 03 80 00       	push   $0x8003ef
  80045f:	e8 51 01 00 00       	call   8005b5 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800464:	83 c4 08             	add    $0x8,%esp
  800467:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80046d:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800473:	50                   	push   %eax
  800474:	e8 3c fc ff ff       	call   8000b5 <sys_cputs>

	return b.cnt;
}
  800479:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80047f:	c9                   	leave  
  800480:	c3                   	ret    

00800481 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800481:	55                   	push   %ebp
  800482:	89 e5                	mov    %esp,%ebp
  800484:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800487:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80048a:	50                   	push   %eax
  80048b:	ff 75 08             	pushl  0x8(%ebp)
  80048e:	e8 9d ff ff ff       	call   800430 <vcprintf>
	va_end(ap);

	return cnt;
}
  800493:	c9                   	leave  
  800494:	c3                   	ret    

00800495 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800495:	55                   	push   %ebp
  800496:	89 e5                	mov    %esp,%ebp
  800498:	57                   	push   %edi
  800499:	56                   	push   %esi
  80049a:	53                   	push   %ebx
  80049b:	83 ec 1c             	sub    $0x1c,%esp
  80049e:	89 c7                	mov    %eax,%edi
  8004a0:	89 d6                	mov    %edx,%esi
  8004a2:	8b 45 08             	mov    0x8(%ebp),%eax
  8004a5:	8b 55 0c             	mov    0xc(%ebp),%edx
  8004a8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004ab:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8004ae:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8004b1:	bb 00 00 00 00       	mov    $0x0,%ebx
  8004b6:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8004b9:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8004bc:	39 d3                	cmp    %edx,%ebx
  8004be:	72 05                	jb     8004c5 <printnum+0x30>
  8004c0:	39 45 10             	cmp    %eax,0x10(%ebp)
  8004c3:	77 45                	ja     80050a <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8004c5:	83 ec 0c             	sub    $0xc,%esp
  8004c8:	ff 75 18             	pushl  0x18(%ebp)
  8004cb:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ce:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8004d1:	53                   	push   %ebx
  8004d2:	ff 75 10             	pushl  0x10(%ebp)
  8004d5:	83 ec 08             	sub    $0x8,%esp
  8004d8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004db:	ff 75 e0             	pushl  -0x20(%ebp)
  8004de:	ff 75 dc             	pushl  -0x24(%ebp)
  8004e1:	ff 75 d8             	pushl  -0x28(%ebp)
  8004e4:	e8 7b 08 00 00       	call   800d64 <__udivdi3>
  8004e9:	83 c4 18             	add    $0x18,%esp
  8004ec:	52                   	push   %edx
  8004ed:	50                   	push   %eax
  8004ee:	89 f2                	mov    %esi,%edx
  8004f0:	89 f8                	mov    %edi,%eax
  8004f2:	e8 9e ff ff ff       	call   800495 <printnum>
  8004f7:	83 c4 20             	add    $0x20,%esp
  8004fa:	eb 16                	jmp    800512 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004fc:	83 ec 08             	sub    $0x8,%esp
  8004ff:	56                   	push   %esi
  800500:	ff 75 18             	pushl  0x18(%ebp)
  800503:	ff d7                	call   *%edi
  800505:	83 c4 10             	add    $0x10,%esp
  800508:	eb 03                	jmp    80050d <printnum+0x78>
  80050a:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80050d:	4b                   	dec    %ebx
  80050e:	85 db                	test   %ebx,%ebx
  800510:	7f ea                	jg     8004fc <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800512:	83 ec 08             	sub    $0x8,%esp
  800515:	56                   	push   %esi
  800516:	83 ec 04             	sub    $0x4,%esp
  800519:	ff 75 e4             	pushl  -0x1c(%ebp)
  80051c:	ff 75 e0             	pushl  -0x20(%ebp)
  80051f:	ff 75 dc             	pushl  -0x24(%ebp)
  800522:	ff 75 d8             	pushl  -0x28(%ebp)
  800525:	e8 4a 09 00 00       	call   800e74 <__umoddi3>
  80052a:	83 c4 14             	add    $0x14,%esp
  80052d:	0f be 80 3d 10 80 00 	movsbl 0x80103d(%eax),%eax
  800534:	50                   	push   %eax
  800535:	ff d7                	call   *%edi
}
  800537:	83 c4 10             	add    $0x10,%esp
  80053a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80053d:	5b                   	pop    %ebx
  80053e:	5e                   	pop    %esi
  80053f:	5f                   	pop    %edi
  800540:	5d                   	pop    %ebp
  800541:	c3                   	ret    

00800542 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800542:	55                   	push   %ebp
  800543:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800545:	83 fa 01             	cmp    $0x1,%edx
  800548:	7e 0e                	jle    800558 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80054a:	8b 10                	mov    (%eax),%edx
  80054c:	8d 4a 08             	lea    0x8(%edx),%ecx
  80054f:	89 08                	mov    %ecx,(%eax)
  800551:	8b 02                	mov    (%edx),%eax
  800553:	8b 52 04             	mov    0x4(%edx),%edx
  800556:	eb 22                	jmp    80057a <getuint+0x38>
	else if (lflag)
  800558:	85 d2                	test   %edx,%edx
  80055a:	74 10                	je     80056c <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80055c:	8b 10                	mov    (%eax),%edx
  80055e:	8d 4a 04             	lea    0x4(%edx),%ecx
  800561:	89 08                	mov    %ecx,(%eax)
  800563:	8b 02                	mov    (%edx),%eax
  800565:	ba 00 00 00 00       	mov    $0x0,%edx
  80056a:	eb 0e                	jmp    80057a <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80056c:	8b 10                	mov    (%eax),%edx
  80056e:	8d 4a 04             	lea    0x4(%edx),%ecx
  800571:	89 08                	mov    %ecx,(%eax)
  800573:	8b 02                	mov    (%edx),%eax
  800575:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80057a:	5d                   	pop    %ebp
  80057b:	c3                   	ret    

0080057c <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80057c:	55                   	push   %ebp
  80057d:	89 e5                	mov    %esp,%ebp
  80057f:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800582:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800585:	8b 10                	mov    (%eax),%edx
  800587:	3b 50 04             	cmp    0x4(%eax),%edx
  80058a:	73 0a                	jae    800596 <sprintputch+0x1a>
		*b->buf++ = ch;
  80058c:	8d 4a 01             	lea    0x1(%edx),%ecx
  80058f:	89 08                	mov    %ecx,(%eax)
  800591:	8b 45 08             	mov    0x8(%ebp),%eax
  800594:	88 02                	mov    %al,(%edx)
}
  800596:	5d                   	pop    %ebp
  800597:	c3                   	ret    

00800598 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800598:	55                   	push   %ebp
  800599:	89 e5                	mov    %esp,%ebp
  80059b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80059e:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8005a1:	50                   	push   %eax
  8005a2:	ff 75 10             	pushl  0x10(%ebp)
  8005a5:	ff 75 0c             	pushl  0xc(%ebp)
  8005a8:	ff 75 08             	pushl  0x8(%ebp)
  8005ab:	e8 05 00 00 00       	call   8005b5 <vprintfmt>
	va_end(ap);
}
  8005b0:	83 c4 10             	add    $0x10,%esp
  8005b3:	c9                   	leave  
  8005b4:	c3                   	ret    

008005b5 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8005b5:	55                   	push   %ebp
  8005b6:	89 e5                	mov    %esp,%ebp
  8005b8:	57                   	push   %edi
  8005b9:	56                   	push   %esi
  8005ba:	53                   	push   %ebx
  8005bb:	83 ec 2c             	sub    $0x2c,%esp
  8005be:	8b 75 08             	mov    0x8(%ebp),%esi
  8005c1:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8005c4:	8b 7d 10             	mov    0x10(%ebp),%edi
  8005c7:	eb 12                	jmp    8005db <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8005c9:	85 c0                	test   %eax,%eax
  8005cb:	0f 84 68 03 00 00    	je     800939 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8005d1:	83 ec 08             	sub    $0x8,%esp
  8005d4:	53                   	push   %ebx
  8005d5:	50                   	push   %eax
  8005d6:	ff d6                	call   *%esi
  8005d8:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8005db:	47                   	inc    %edi
  8005dc:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005e0:	83 f8 25             	cmp    $0x25,%eax
  8005e3:	75 e4                	jne    8005c9 <vprintfmt+0x14>
  8005e5:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005e9:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005f0:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005f7:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005fe:	ba 00 00 00 00       	mov    $0x0,%edx
  800603:	eb 07                	jmp    80060c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800605:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800608:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80060c:	8d 47 01             	lea    0x1(%edi),%eax
  80060f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800612:	0f b6 0f             	movzbl (%edi),%ecx
  800615:	8a 07                	mov    (%edi),%al
  800617:	83 e8 23             	sub    $0x23,%eax
  80061a:	3c 55                	cmp    $0x55,%al
  80061c:	0f 87 fe 02 00 00    	ja     800920 <vprintfmt+0x36b>
  800622:	0f b6 c0             	movzbl %al,%eax
  800625:	ff 24 85 80 11 80 00 	jmp    *0x801180(,%eax,4)
  80062c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80062f:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800633:	eb d7                	jmp    80060c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800635:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800638:	b8 00 00 00 00       	mov    $0x0,%eax
  80063d:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800640:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800643:	01 c0                	add    %eax,%eax
  800645:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800649:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80064c:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80064f:	83 fa 09             	cmp    $0x9,%edx
  800652:	77 34                	ja     800688 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800654:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800655:	eb e9                	jmp    800640 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800657:	8b 45 14             	mov    0x14(%ebp),%eax
  80065a:	8d 48 04             	lea    0x4(%eax),%ecx
  80065d:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800660:	8b 00                	mov    (%eax),%eax
  800662:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800665:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800668:	eb 24                	jmp    80068e <vprintfmt+0xd9>
  80066a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80066e:	79 07                	jns    800677 <vprintfmt+0xc2>
  800670:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800677:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80067a:	eb 90                	jmp    80060c <vprintfmt+0x57>
  80067c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80067f:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800686:	eb 84                	jmp    80060c <vprintfmt+0x57>
  800688:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80068b:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80068e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800692:	0f 89 74 ff ff ff    	jns    80060c <vprintfmt+0x57>
				width = precision, precision = -1;
  800698:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80069b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80069e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8006a5:	e9 62 ff ff ff       	jmp    80060c <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8006aa:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006ab:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8006ae:	e9 59 ff ff ff       	jmp    80060c <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8006b3:	8b 45 14             	mov    0x14(%ebp),%eax
  8006b6:	8d 50 04             	lea    0x4(%eax),%edx
  8006b9:	89 55 14             	mov    %edx,0x14(%ebp)
  8006bc:	83 ec 08             	sub    $0x8,%esp
  8006bf:	53                   	push   %ebx
  8006c0:	ff 30                	pushl  (%eax)
  8006c2:	ff d6                	call   *%esi
			break;
  8006c4:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8006ca:	e9 0c ff ff ff       	jmp    8005db <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8006cf:	8b 45 14             	mov    0x14(%ebp),%eax
  8006d2:	8d 50 04             	lea    0x4(%eax),%edx
  8006d5:	89 55 14             	mov    %edx,0x14(%ebp)
  8006d8:	8b 00                	mov    (%eax),%eax
  8006da:	85 c0                	test   %eax,%eax
  8006dc:	79 02                	jns    8006e0 <vprintfmt+0x12b>
  8006de:	f7 d8                	neg    %eax
  8006e0:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006e2:	83 f8 0f             	cmp    $0xf,%eax
  8006e5:	7f 0b                	jg     8006f2 <vprintfmt+0x13d>
  8006e7:	8b 04 85 e0 12 80 00 	mov    0x8012e0(,%eax,4),%eax
  8006ee:	85 c0                	test   %eax,%eax
  8006f0:	75 18                	jne    80070a <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006f2:	52                   	push   %edx
  8006f3:	68 55 10 80 00       	push   $0x801055
  8006f8:	53                   	push   %ebx
  8006f9:	56                   	push   %esi
  8006fa:	e8 99 fe ff ff       	call   800598 <printfmt>
  8006ff:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800702:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800705:	e9 d1 fe ff ff       	jmp    8005db <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80070a:	50                   	push   %eax
  80070b:	68 5e 10 80 00       	push   $0x80105e
  800710:	53                   	push   %ebx
  800711:	56                   	push   %esi
  800712:	e8 81 fe ff ff       	call   800598 <printfmt>
  800717:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80071a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80071d:	e9 b9 fe ff ff       	jmp    8005db <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800722:	8b 45 14             	mov    0x14(%ebp),%eax
  800725:	8d 50 04             	lea    0x4(%eax),%edx
  800728:	89 55 14             	mov    %edx,0x14(%ebp)
  80072b:	8b 38                	mov    (%eax),%edi
  80072d:	85 ff                	test   %edi,%edi
  80072f:	75 05                	jne    800736 <vprintfmt+0x181>
				p = "(null)";
  800731:	bf 4e 10 80 00       	mov    $0x80104e,%edi
			if (width > 0 && padc != '-')
  800736:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80073a:	0f 8e 90 00 00 00    	jle    8007d0 <vprintfmt+0x21b>
  800740:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800744:	0f 84 8e 00 00 00    	je     8007d8 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80074a:	83 ec 08             	sub    $0x8,%esp
  80074d:	ff 75 d0             	pushl  -0x30(%ebp)
  800750:	57                   	push   %edi
  800751:	e8 70 02 00 00       	call   8009c6 <strnlen>
  800756:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800759:	29 c1                	sub    %eax,%ecx
  80075b:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80075e:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800761:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800765:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800768:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80076b:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80076d:	eb 0d                	jmp    80077c <vprintfmt+0x1c7>
					putch(padc, putdat);
  80076f:	83 ec 08             	sub    $0x8,%esp
  800772:	53                   	push   %ebx
  800773:	ff 75 e4             	pushl  -0x1c(%ebp)
  800776:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800778:	4f                   	dec    %edi
  800779:	83 c4 10             	add    $0x10,%esp
  80077c:	85 ff                	test   %edi,%edi
  80077e:	7f ef                	jg     80076f <vprintfmt+0x1ba>
  800780:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800783:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800786:	89 c8                	mov    %ecx,%eax
  800788:	85 c9                	test   %ecx,%ecx
  80078a:	79 05                	jns    800791 <vprintfmt+0x1dc>
  80078c:	b8 00 00 00 00       	mov    $0x0,%eax
  800791:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800794:	29 c1                	sub    %eax,%ecx
  800796:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800799:	89 75 08             	mov    %esi,0x8(%ebp)
  80079c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80079f:	eb 3d                	jmp    8007de <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8007a1:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8007a5:	74 19                	je     8007c0 <vprintfmt+0x20b>
  8007a7:	0f be c0             	movsbl %al,%eax
  8007aa:	83 e8 20             	sub    $0x20,%eax
  8007ad:	83 f8 5e             	cmp    $0x5e,%eax
  8007b0:	76 0e                	jbe    8007c0 <vprintfmt+0x20b>
					putch('?', putdat);
  8007b2:	83 ec 08             	sub    $0x8,%esp
  8007b5:	53                   	push   %ebx
  8007b6:	6a 3f                	push   $0x3f
  8007b8:	ff 55 08             	call   *0x8(%ebp)
  8007bb:	83 c4 10             	add    $0x10,%esp
  8007be:	eb 0b                	jmp    8007cb <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8007c0:	83 ec 08             	sub    $0x8,%esp
  8007c3:	53                   	push   %ebx
  8007c4:	52                   	push   %edx
  8007c5:	ff 55 08             	call   *0x8(%ebp)
  8007c8:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8007cb:	ff 4d e4             	decl   -0x1c(%ebp)
  8007ce:	eb 0e                	jmp    8007de <vprintfmt+0x229>
  8007d0:	89 75 08             	mov    %esi,0x8(%ebp)
  8007d3:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007d6:	eb 06                	jmp    8007de <vprintfmt+0x229>
  8007d8:	89 75 08             	mov    %esi,0x8(%ebp)
  8007db:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007de:	47                   	inc    %edi
  8007df:	8a 47 ff             	mov    -0x1(%edi),%al
  8007e2:	0f be d0             	movsbl %al,%edx
  8007e5:	85 d2                	test   %edx,%edx
  8007e7:	74 1d                	je     800806 <vprintfmt+0x251>
  8007e9:	85 f6                	test   %esi,%esi
  8007eb:	78 b4                	js     8007a1 <vprintfmt+0x1ec>
  8007ed:	4e                   	dec    %esi
  8007ee:	79 b1                	jns    8007a1 <vprintfmt+0x1ec>
  8007f0:	8b 75 08             	mov    0x8(%ebp),%esi
  8007f3:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007f6:	eb 14                	jmp    80080c <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007f8:	83 ec 08             	sub    $0x8,%esp
  8007fb:	53                   	push   %ebx
  8007fc:	6a 20                	push   $0x20
  8007fe:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800800:	4f                   	dec    %edi
  800801:	83 c4 10             	add    $0x10,%esp
  800804:	eb 06                	jmp    80080c <vprintfmt+0x257>
  800806:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800809:	8b 75 08             	mov    0x8(%ebp),%esi
  80080c:	85 ff                	test   %edi,%edi
  80080e:	7f e8                	jg     8007f8 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800810:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800813:	e9 c3 fd ff ff       	jmp    8005db <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800818:	83 fa 01             	cmp    $0x1,%edx
  80081b:	7e 16                	jle    800833 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80081d:	8b 45 14             	mov    0x14(%ebp),%eax
  800820:	8d 50 08             	lea    0x8(%eax),%edx
  800823:	89 55 14             	mov    %edx,0x14(%ebp)
  800826:	8b 50 04             	mov    0x4(%eax),%edx
  800829:	8b 00                	mov    (%eax),%eax
  80082b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80082e:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800831:	eb 32                	jmp    800865 <vprintfmt+0x2b0>
	else if (lflag)
  800833:	85 d2                	test   %edx,%edx
  800835:	74 18                	je     80084f <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800837:	8b 45 14             	mov    0x14(%ebp),%eax
  80083a:	8d 50 04             	lea    0x4(%eax),%edx
  80083d:	89 55 14             	mov    %edx,0x14(%ebp)
  800840:	8b 00                	mov    (%eax),%eax
  800842:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800845:	89 c1                	mov    %eax,%ecx
  800847:	c1 f9 1f             	sar    $0x1f,%ecx
  80084a:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80084d:	eb 16                	jmp    800865 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80084f:	8b 45 14             	mov    0x14(%ebp),%eax
  800852:	8d 50 04             	lea    0x4(%eax),%edx
  800855:	89 55 14             	mov    %edx,0x14(%ebp)
  800858:	8b 00                	mov    (%eax),%eax
  80085a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80085d:	89 c1                	mov    %eax,%ecx
  80085f:	c1 f9 1f             	sar    $0x1f,%ecx
  800862:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800865:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800868:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80086b:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80086f:	79 76                	jns    8008e7 <vprintfmt+0x332>
				putch('-', putdat);
  800871:	83 ec 08             	sub    $0x8,%esp
  800874:	53                   	push   %ebx
  800875:	6a 2d                	push   $0x2d
  800877:	ff d6                	call   *%esi
				num = -(long long) num;
  800879:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80087c:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80087f:	f7 d8                	neg    %eax
  800881:	83 d2 00             	adc    $0x0,%edx
  800884:	f7 da                	neg    %edx
  800886:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800889:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80088e:	eb 5c                	jmp    8008ec <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800890:	8d 45 14             	lea    0x14(%ebp),%eax
  800893:	e8 aa fc ff ff       	call   800542 <getuint>
			base = 10;
  800898:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80089d:	eb 4d                	jmp    8008ec <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80089f:	8d 45 14             	lea    0x14(%ebp),%eax
  8008a2:	e8 9b fc ff ff       	call   800542 <getuint>
			base = 8;
  8008a7:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8008ac:	eb 3e                	jmp    8008ec <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8008ae:	83 ec 08             	sub    $0x8,%esp
  8008b1:	53                   	push   %ebx
  8008b2:	6a 30                	push   $0x30
  8008b4:	ff d6                	call   *%esi
			putch('x', putdat);
  8008b6:	83 c4 08             	add    $0x8,%esp
  8008b9:	53                   	push   %ebx
  8008ba:	6a 78                	push   $0x78
  8008bc:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8008be:	8b 45 14             	mov    0x14(%ebp),%eax
  8008c1:	8d 50 04             	lea    0x4(%eax),%edx
  8008c4:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8008c7:	8b 00                	mov    (%eax),%eax
  8008c9:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8008ce:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8008d1:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8008d6:	eb 14                	jmp    8008ec <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8008d8:	8d 45 14             	lea    0x14(%ebp),%eax
  8008db:	e8 62 fc ff ff       	call   800542 <getuint>
			base = 16;
  8008e0:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008e5:	eb 05                	jmp    8008ec <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008e7:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008ec:	83 ec 0c             	sub    $0xc,%esp
  8008ef:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008f3:	57                   	push   %edi
  8008f4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008f7:	51                   	push   %ecx
  8008f8:	52                   	push   %edx
  8008f9:	50                   	push   %eax
  8008fa:	89 da                	mov    %ebx,%edx
  8008fc:	89 f0                	mov    %esi,%eax
  8008fe:	e8 92 fb ff ff       	call   800495 <printnum>
			break;
  800903:	83 c4 20             	add    $0x20,%esp
  800906:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800909:	e9 cd fc ff ff       	jmp    8005db <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80090e:	83 ec 08             	sub    $0x8,%esp
  800911:	53                   	push   %ebx
  800912:	51                   	push   %ecx
  800913:	ff d6                	call   *%esi
			break;
  800915:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800918:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80091b:	e9 bb fc ff ff       	jmp    8005db <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800920:	83 ec 08             	sub    $0x8,%esp
  800923:	53                   	push   %ebx
  800924:	6a 25                	push   $0x25
  800926:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800928:	83 c4 10             	add    $0x10,%esp
  80092b:	eb 01                	jmp    80092e <vprintfmt+0x379>
  80092d:	4f                   	dec    %edi
  80092e:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800932:	75 f9                	jne    80092d <vprintfmt+0x378>
  800934:	e9 a2 fc ff ff       	jmp    8005db <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800939:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80093c:	5b                   	pop    %ebx
  80093d:	5e                   	pop    %esi
  80093e:	5f                   	pop    %edi
  80093f:	5d                   	pop    %ebp
  800940:	c3                   	ret    

00800941 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800941:	55                   	push   %ebp
  800942:	89 e5                	mov    %esp,%ebp
  800944:	83 ec 18             	sub    $0x18,%esp
  800947:	8b 45 08             	mov    0x8(%ebp),%eax
  80094a:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80094d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800950:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800954:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800957:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80095e:	85 c0                	test   %eax,%eax
  800960:	74 26                	je     800988 <vsnprintf+0x47>
  800962:	85 d2                	test   %edx,%edx
  800964:	7e 29                	jle    80098f <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800966:	ff 75 14             	pushl  0x14(%ebp)
  800969:	ff 75 10             	pushl  0x10(%ebp)
  80096c:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80096f:	50                   	push   %eax
  800970:	68 7c 05 80 00       	push   $0x80057c
  800975:	e8 3b fc ff ff       	call   8005b5 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80097a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80097d:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800980:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800983:	83 c4 10             	add    $0x10,%esp
  800986:	eb 0c                	jmp    800994 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800988:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80098d:	eb 05                	jmp    800994 <vsnprintf+0x53>
  80098f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800994:	c9                   	leave  
  800995:	c3                   	ret    

00800996 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800996:	55                   	push   %ebp
  800997:	89 e5                	mov    %esp,%ebp
  800999:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80099c:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80099f:	50                   	push   %eax
  8009a0:	ff 75 10             	pushl  0x10(%ebp)
  8009a3:	ff 75 0c             	pushl  0xc(%ebp)
  8009a6:	ff 75 08             	pushl  0x8(%ebp)
  8009a9:	e8 93 ff ff ff       	call   800941 <vsnprintf>
	va_end(ap);

	return rc;
}
  8009ae:	c9                   	leave  
  8009af:	c3                   	ret    

008009b0 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8009b0:	55                   	push   %ebp
  8009b1:	89 e5                	mov    %esp,%ebp
  8009b3:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8009b6:	b8 00 00 00 00       	mov    $0x0,%eax
  8009bb:	eb 01                	jmp    8009be <strlen+0xe>
		n++;
  8009bd:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8009be:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8009c2:	75 f9                	jne    8009bd <strlen+0xd>
		n++;
	return n;
}
  8009c4:	5d                   	pop    %ebp
  8009c5:	c3                   	ret    

008009c6 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8009c6:	55                   	push   %ebp
  8009c7:	89 e5                	mov    %esp,%ebp
  8009c9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8009cc:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8009d4:	eb 01                	jmp    8009d7 <strnlen+0x11>
		n++;
  8009d6:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009d7:	39 c2                	cmp    %eax,%edx
  8009d9:	74 08                	je     8009e3 <strnlen+0x1d>
  8009db:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009df:	75 f5                	jne    8009d6 <strnlen+0x10>
  8009e1:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009e3:	5d                   	pop    %ebp
  8009e4:	c3                   	ret    

008009e5 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009e5:	55                   	push   %ebp
  8009e6:	89 e5                	mov    %esp,%ebp
  8009e8:	53                   	push   %ebx
  8009e9:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ec:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009ef:	89 c2                	mov    %eax,%edx
  8009f1:	42                   	inc    %edx
  8009f2:	41                   	inc    %ecx
  8009f3:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009f6:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009f9:	84 db                	test   %bl,%bl
  8009fb:	75 f4                	jne    8009f1 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009fd:	5b                   	pop    %ebx
  8009fe:	5d                   	pop    %ebp
  8009ff:	c3                   	ret    

00800a00 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800a00:	55                   	push   %ebp
  800a01:	89 e5                	mov    %esp,%ebp
  800a03:	53                   	push   %ebx
  800a04:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800a07:	53                   	push   %ebx
  800a08:	e8 a3 ff ff ff       	call   8009b0 <strlen>
  800a0d:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800a10:	ff 75 0c             	pushl  0xc(%ebp)
  800a13:	01 d8                	add    %ebx,%eax
  800a15:	50                   	push   %eax
  800a16:	e8 ca ff ff ff       	call   8009e5 <strcpy>
	return dst;
}
  800a1b:	89 d8                	mov    %ebx,%eax
  800a1d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800a20:	c9                   	leave  
  800a21:	c3                   	ret    

00800a22 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800a22:	55                   	push   %ebp
  800a23:	89 e5                	mov    %esp,%ebp
  800a25:	56                   	push   %esi
  800a26:	53                   	push   %ebx
  800a27:	8b 75 08             	mov    0x8(%ebp),%esi
  800a2a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a2d:	89 f3                	mov    %esi,%ebx
  800a2f:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a32:	89 f2                	mov    %esi,%edx
  800a34:	eb 0c                	jmp    800a42 <strncpy+0x20>
		*dst++ = *src;
  800a36:	42                   	inc    %edx
  800a37:	8a 01                	mov    (%ecx),%al
  800a39:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a3c:	80 39 01             	cmpb   $0x1,(%ecx)
  800a3f:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a42:	39 da                	cmp    %ebx,%edx
  800a44:	75 f0                	jne    800a36 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a46:	89 f0                	mov    %esi,%eax
  800a48:	5b                   	pop    %ebx
  800a49:	5e                   	pop    %esi
  800a4a:	5d                   	pop    %ebp
  800a4b:	c3                   	ret    

00800a4c <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a4c:	55                   	push   %ebp
  800a4d:	89 e5                	mov    %esp,%ebp
  800a4f:	56                   	push   %esi
  800a50:	53                   	push   %ebx
  800a51:	8b 75 08             	mov    0x8(%ebp),%esi
  800a54:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a57:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a5a:	85 c0                	test   %eax,%eax
  800a5c:	74 1e                	je     800a7c <strlcpy+0x30>
  800a5e:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a62:	89 f2                	mov    %esi,%edx
  800a64:	eb 05                	jmp    800a6b <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a66:	42                   	inc    %edx
  800a67:	41                   	inc    %ecx
  800a68:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a6b:	39 c2                	cmp    %eax,%edx
  800a6d:	74 08                	je     800a77 <strlcpy+0x2b>
  800a6f:	8a 19                	mov    (%ecx),%bl
  800a71:	84 db                	test   %bl,%bl
  800a73:	75 f1                	jne    800a66 <strlcpy+0x1a>
  800a75:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a77:	c6 00 00             	movb   $0x0,(%eax)
  800a7a:	eb 02                	jmp    800a7e <strlcpy+0x32>
  800a7c:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a7e:	29 f0                	sub    %esi,%eax
}
  800a80:	5b                   	pop    %ebx
  800a81:	5e                   	pop    %esi
  800a82:	5d                   	pop    %ebp
  800a83:	c3                   	ret    

00800a84 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a84:	55                   	push   %ebp
  800a85:	89 e5                	mov    %esp,%ebp
  800a87:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a8a:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a8d:	eb 02                	jmp    800a91 <strcmp+0xd>
		p++, q++;
  800a8f:	41                   	inc    %ecx
  800a90:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a91:	8a 01                	mov    (%ecx),%al
  800a93:	84 c0                	test   %al,%al
  800a95:	74 04                	je     800a9b <strcmp+0x17>
  800a97:	3a 02                	cmp    (%edx),%al
  800a99:	74 f4                	je     800a8f <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a9b:	0f b6 c0             	movzbl %al,%eax
  800a9e:	0f b6 12             	movzbl (%edx),%edx
  800aa1:	29 d0                	sub    %edx,%eax
}
  800aa3:	5d                   	pop    %ebp
  800aa4:	c3                   	ret    

00800aa5 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800aa5:	55                   	push   %ebp
  800aa6:	89 e5                	mov    %esp,%ebp
  800aa8:	53                   	push   %ebx
  800aa9:	8b 45 08             	mov    0x8(%ebp),%eax
  800aac:	8b 55 0c             	mov    0xc(%ebp),%edx
  800aaf:	89 c3                	mov    %eax,%ebx
  800ab1:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800ab4:	eb 02                	jmp    800ab8 <strncmp+0x13>
		n--, p++, q++;
  800ab6:	40                   	inc    %eax
  800ab7:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800ab8:	39 d8                	cmp    %ebx,%eax
  800aba:	74 14                	je     800ad0 <strncmp+0x2b>
  800abc:	8a 08                	mov    (%eax),%cl
  800abe:	84 c9                	test   %cl,%cl
  800ac0:	74 04                	je     800ac6 <strncmp+0x21>
  800ac2:	3a 0a                	cmp    (%edx),%cl
  800ac4:	74 f0                	je     800ab6 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800ac6:	0f b6 00             	movzbl (%eax),%eax
  800ac9:	0f b6 12             	movzbl (%edx),%edx
  800acc:	29 d0                	sub    %edx,%eax
  800ace:	eb 05                	jmp    800ad5 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800ad0:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800ad5:	5b                   	pop    %ebx
  800ad6:	5d                   	pop    %ebp
  800ad7:	c3                   	ret    

00800ad8 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800ad8:	55                   	push   %ebp
  800ad9:	89 e5                	mov    %esp,%ebp
  800adb:	8b 45 08             	mov    0x8(%ebp),%eax
  800ade:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ae1:	eb 05                	jmp    800ae8 <strchr+0x10>
		if (*s == c)
  800ae3:	38 ca                	cmp    %cl,%dl
  800ae5:	74 0c                	je     800af3 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800ae7:	40                   	inc    %eax
  800ae8:	8a 10                	mov    (%eax),%dl
  800aea:	84 d2                	test   %dl,%dl
  800aec:	75 f5                	jne    800ae3 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800aee:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800af3:	5d                   	pop    %ebp
  800af4:	c3                   	ret    

00800af5 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800af5:	55                   	push   %ebp
  800af6:	89 e5                	mov    %esp,%ebp
  800af8:	8b 45 08             	mov    0x8(%ebp),%eax
  800afb:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800afe:	eb 05                	jmp    800b05 <strfind+0x10>
		if (*s == c)
  800b00:	38 ca                	cmp    %cl,%dl
  800b02:	74 07                	je     800b0b <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800b04:	40                   	inc    %eax
  800b05:	8a 10                	mov    (%eax),%dl
  800b07:	84 d2                	test   %dl,%dl
  800b09:	75 f5                	jne    800b00 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800b0b:	5d                   	pop    %ebp
  800b0c:	c3                   	ret    

00800b0d <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800b0d:	55                   	push   %ebp
  800b0e:	89 e5                	mov    %esp,%ebp
  800b10:	57                   	push   %edi
  800b11:	56                   	push   %esi
  800b12:	53                   	push   %ebx
  800b13:	8b 7d 08             	mov    0x8(%ebp),%edi
  800b16:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800b19:	85 c9                	test   %ecx,%ecx
  800b1b:	74 36                	je     800b53 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800b1d:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800b23:	75 28                	jne    800b4d <memset+0x40>
  800b25:	f6 c1 03             	test   $0x3,%cl
  800b28:	75 23                	jne    800b4d <memset+0x40>
		c &= 0xFF;
  800b2a:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800b2e:	89 d3                	mov    %edx,%ebx
  800b30:	c1 e3 08             	shl    $0x8,%ebx
  800b33:	89 d6                	mov    %edx,%esi
  800b35:	c1 e6 18             	shl    $0x18,%esi
  800b38:	89 d0                	mov    %edx,%eax
  800b3a:	c1 e0 10             	shl    $0x10,%eax
  800b3d:	09 f0                	or     %esi,%eax
  800b3f:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b41:	89 d8                	mov    %ebx,%eax
  800b43:	09 d0                	or     %edx,%eax
  800b45:	c1 e9 02             	shr    $0x2,%ecx
  800b48:	fc                   	cld    
  800b49:	f3 ab                	rep stos %eax,%es:(%edi)
  800b4b:	eb 06                	jmp    800b53 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b4d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b50:	fc                   	cld    
  800b51:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b53:	89 f8                	mov    %edi,%eax
  800b55:	5b                   	pop    %ebx
  800b56:	5e                   	pop    %esi
  800b57:	5f                   	pop    %edi
  800b58:	5d                   	pop    %ebp
  800b59:	c3                   	ret    

00800b5a <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b5a:	55                   	push   %ebp
  800b5b:	89 e5                	mov    %esp,%ebp
  800b5d:	57                   	push   %edi
  800b5e:	56                   	push   %esi
  800b5f:	8b 45 08             	mov    0x8(%ebp),%eax
  800b62:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b65:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b68:	39 c6                	cmp    %eax,%esi
  800b6a:	73 33                	jae    800b9f <memmove+0x45>
  800b6c:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b6f:	39 d0                	cmp    %edx,%eax
  800b71:	73 2c                	jae    800b9f <memmove+0x45>
		s += n;
		d += n;
  800b73:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b76:	89 d6                	mov    %edx,%esi
  800b78:	09 fe                	or     %edi,%esi
  800b7a:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b80:	75 13                	jne    800b95 <memmove+0x3b>
  800b82:	f6 c1 03             	test   $0x3,%cl
  800b85:	75 0e                	jne    800b95 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b87:	83 ef 04             	sub    $0x4,%edi
  800b8a:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b8d:	c1 e9 02             	shr    $0x2,%ecx
  800b90:	fd                   	std    
  800b91:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b93:	eb 07                	jmp    800b9c <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b95:	4f                   	dec    %edi
  800b96:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b99:	fd                   	std    
  800b9a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b9c:	fc                   	cld    
  800b9d:	eb 1d                	jmp    800bbc <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b9f:	89 f2                	mov    %esi,%edx
  800ba1:	09 c2                	or     %eax,%edx
  800ba3:	f6 c2 03             	test   $0x3,%dl
  800ba6:	75 0f                	jne    800bb7 <memmove+0x5d>
  800ba8:	f6 c1 03             	test   $0x3,%cl
  800bab:	75 0a                	jne    800bb7 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800bad:	c1 e9 02             	shr    $0x2,%ecx
  800bb0:	89 c7                	mov    %eax,%edi
  800bb2:	fc                   	cld    
  800bb3:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800bb5:	eb 05                	jmp    800bbc <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800bb7:	89 c7                	mov    %eax,%edi
  800bb9:	fc                   	cld    
  800bba:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800bbc:	5e                   	pop    %esi
  800bbd:	5f                   	pop    %edi
  800bbe:	5d                   	pop    %ebp
  800bbf:	c3                   	ret    

00800bc0 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800bc0:	55                   	push   %ebp
  800bc1:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800bc3:	ff 75 10             	pushl  0x10(%ebp)
  800bc6:	ff 75 0c             	pushl  0xc(%ebp)
  800bc9:	ff 75 08             	pushl  0x8(%ebp)
  800bcc:	e8 89 ff ff ff       	call   800b5a <memmove>
}
  800bd1:	c9                   	leave  
  800bd2:	c3                   	ret    

00800bd3 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800bd3:	55                   	push   %ebp
  800bd4:	89 e5                	mov    %esp,%ebp
  800bd6:	56                   	push   %esi
  800bd7:	53                   	push   %ebx
  800bd8:	8b 45 08             	mov    0x8(%ebp),%eax
  800bdb:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bde:	89 c6                	mov    %eax,%esi
  800be0:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800be3:	eb 14                	jmp    800bf9 <memcmp+0x26>
		if (*s1 != *s2)
  800be5:	8a 08                	mov    (%eax),%cl
  800be7:	8a 1a                	mov    (%edx),%bl
  800be9:	38 d9                	cmp    %bl,%cl
  800beb:	74 0a                	je     800bf7 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bed:	0f b6 c1             	movzbl %cl,%eax
  800bf0:	0f b6 db             	movzbl %bl,%ebx
  800bf3:	29 d8                	sub    %ebx,%eax
  800bf5:	eb 0b                	jmp    800c02 <memcmp+0x2f>
		s1++, s2++;
  800bf7:	40                   	inc    %eax
  800bf8:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bf9:	39 f0                	cmp    %esi,%eax
  800bfb:	75 e8                	jne    800be5 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bfd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c02:	5b                   	pop    %ebx
  800c03:	5e                   	pop    %esi
  800c04:	5d                   	pop    %ebp
  800c05:	c3                   	ret    

00800c06 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800c06:	55                   	push   %ebp
  800c07:	89 e5                	mov    %esp,%ebp
  800c09:	53                   	push   %ebx
  800c0a:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800c0d:	89 c1                	mov    %eax,%ecx
  800c0f:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800c12:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c16:	eb 08                	jmp    800c20 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800c18:	0f b6 10             	movzbl (%eax),%edx
  800c1b:	39 da                	cmp    %ebx,%edx
  800c1d:	74 05                	je     800c24 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c1f:	40                   	inc    %eax
  800c20:	39 c8                	cmp    %ecx,%eax
  800c22:	72 f4                	jb     800c18 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800c24:	5b                   	pop    %ebx
  800c25:	5d                   	pop    %ebp
  800c26:	c3                   	ret    

00800c27 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800c27:	55                   	push   %ebp
  800c28:	89 e5                	mov    %esp,%ebp
  800c2a:	57                   	push   %edi
  800c2b:	56                   	push   %esi
  800c2c:	53                   	push   %ebx
  800c2d:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c30:	eb 01                	jmp    800c33 <strtol+0xc>
		s++;
  800c32:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c33:	8a 01                	mov    (%ecx),%al
  800c35:	3c 20                	cmp    $0x20,%al
  800c37:	74 f9                	je     800c32 <strtol+0xb>
  800c39:	3c 09                	cmp    $0x9,%al
  800c3b:	74 f5                	je     800c32 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c3d:	3c 2b                	cmp    $0x2b,%al
  800c3f:	75 08                	jne    800c49 <strtol+0x22>
		s++;
  800c41:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c42:	bf 00 00 00 00       	mov    $0x0,%edi
  800c47:	eb 11                	jmp    800c5a <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c49:	3c 2d                	cmp    $0x2d,%al
  800c4b:	75 08                	jne    800c55 <strtol+0x2e>
		s++, neg = 1;
  800c4d:	41                   	inc    %ecx
  800c4e:	bf 01 00 00 00       	mov    $0x1,%edi
  800c53:	eb 05                	jmp    800c5a <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c55:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c5a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c5e:	0f 84 87 00 00 00    	je     800ceb <strtol+0xc4>
  800c64:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c68:	75 27                	jne    800c91 <strtol+0x6a>
  800c6a:	80 39 30             	cmpb   $0x30,(%ecx)
  800c6d:	75 22                	jne    800c91 <strtol+0x6a>
  800c6f:	e9 88 00 00 00       	jmp    800cfc <strtol+0xd5>
		s += 2, base = 16;
  800c74:	83 c1 02             	add    $0x2,%ecx
  800c77:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c7e:	eb 11                	jmp    800c91 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c80:	41                   	inc    %ecx
  800c81:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c88:	eb 07                	jmp    800c91 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c8a:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c91:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c96:	8a 11                	mov    (%ecx),%dl
  800c98:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c9b:	80 fb 09             	cmp    $0x9,%bl
  800c9e:	77 08                	ja     800ca8 <strtol+0x81>
			dig = *s - '0';
  800ca0:	0f be d2             	movsbl %dl,%edx
  800ca3:	83 ea 30             	sub    $0x30,%edx
  800ca6:	eb 22                	jmp    800cca <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800ca8:	8d 72 9f             	lea    -0x61(%edx),%esi
  800cab:	89 f3                	mov    %esi,%ebx
  800cad:	80 fb 19             	cmp    $0x19,%bl
  800cb0:	77 08                	ja     800cba <strtol+0x93>
			dig = *s - 'a' + 10;
  800cb2:	0f be d2             	movsbl %dl,%edx
  800cb5:	83 ea 57             	sub    $0x57,%edx
  800cb8:	eb 10                	jmp    800cca <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800cba:	8d 72 bf             	lea    -0x41(%edx),%esi
  800cbd:	89 f3                	mov    %esi,%ebx
  800cbf:	80 fb 19             	cmp    $0x19,%bl
  800cc2:	77 14                	ja     800cd8 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800cc4:	0f be d2             	movsbl %dl,%edx
  800cc7:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800cca:	3b 55 10             	cmp    0x10(%ebp),%edx
  800ccd:	7d 09                	jge    800cd8 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800ccf:	41                   	inc    %ecx
  800cd0:	0f af 45 10          	imul   0x10(%ebp),%eax
  800cd4:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800cd6:	eb be                	jmp    800c96 <strtol+0x6f>

	if (endptr)
  800cd8:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800cdc:	74 05                	je     800ce3 <strtol+0xbc>
		*endptr = (char *) s;
  800cde:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ce1:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ce3:	85 ff                	test   %edi,%edi
  800ce5:	74 21                	je     800d08 <strtol+0xe1>
  800ce7:	f7 d8                	neg    %eax
  800ce9:	eb 1d                	jmp    800d08 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ceb:	80 39 30             	cmpb   $0x30,(%ecx)
  800cee:	75 9a                	jne    800c8a <strtol+0x63>
  800cf0:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cf4:	0f 84 7a ff ff ff    	je     800c74 <strtol+0x4d>
  800cfa:	eb 84                	jmp    800c80 <strtol+0x59>
  800cfc:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800d00:	0f 84 6e ff ff ff    	je     800c74 <strtol+0x4d>
  800d06:	eb 89                	jmp    800c91 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800d08:	5b                   	pop    %ebx
  800d09:	5e                   	pop    %esi
  800d0a:	5f                   	pop    %edi
  800d0b:	5d                   	pop    %ebp
  800d0c:	c3                   	ret    

00800d0d <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  800d0d:	55                   	push   %ebp
  800d0e:	89 e5                	mov    %esp,%ebp
  800d10:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  800d13:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  800d1a:	75 3e                	jne    800d5a <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  800d1c:	83 ec 04             	sub    $0x4,%esp
  800d1f:	6a 07                	push   $0x7
  800d21:	68 00 f0 bf ee       	push   $0xeebff000
  800d26:	6a 00                	push   $0x0
  800d28:	e8 44 f4 ff ff       	call   800171 <sys_page_alloc>
  800d2d:	83 c4 10             	add    $0x10,%esp
  800d30:	85 c0                	test   %eax,%eax
  800d32:	74 14                	je     800d48 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  800d34:	83 ec 04             	sub    $0x4,%esp
  800d37:	68 40 13 80 00       	push   $0x801340
  800d3c:	6a 23                	push   $0x23
  800d3e:	68 64 13 80 00       	push   $0x801364
  800d43:	e8 61 f6 ff ff       	call   8003a9 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  800d48:	83 ec 08             	sub    $0x8,%esp
  800d4b:	68 81 03 80 00       	push   $0x800381
  800d50:	6a 00                	push   $0x0
  800d52:	e8 84 f5 ff ff       	call   8002db <sys_env_set_pgfault_upcall>
  800d57:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  800d5a:	8b 45 08             	mov    0x8(%ebp),%eax
  800d5d:	a3 08 20 80 00       	mov    %eax,0x802008
}
  800d62:	c9                   	leave  
  800d63:	c3                   	ret    

00800d64 <__udivdi3>:
  800d64:	55                   	push   %ebp
  800d65:	57                   	push   %edi
  800d66:	56                   	push   %esi
  800d67:	53                   	push   %ebx
  800d68:	83 ec 1c             	sub    $0x1c,%esp
  800d6b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d6f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d73:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d77:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d7b:	89 ca                	mov    %ecx,%edx
  800d7d:	89 f8                	mov    %edi,%eax
  800d7f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800d83:	85 f6                	test   %esi,%esi
  800d85:	75 2d                	jne    800db4 <__udivdi3+0x50>
  800d87:	39 cf                	cmp    %ecx,%edi
  800d89:	77 65                	ja     800df0 <__udivdi3+0x8c>
  800d8b:	89 fd                	mov    %edi,%ebp
  800d8d:	85 ff                	test   %edi,%edi
  800d8f:	75 0b                	jne    800d9c <__udivdi3+0x38>
  800d91:	b8 01 00 00 00       	mov    $0x1,%eax
  800d96:	31 d2                	xor    %edx,%edx
  800d98:	f7 f7                	div    %edi
  800d9a:	89 c5                	mov    %eax,%ebp
  800d9c:	31 d2                	xor    %edx,%edx
  800d9e:	89 c8                	mov    %ecx,%eax
  800da0:	f7 f5                	div    %ebp
  800da2:	89 c1                	mov    %eax,%ecx
  800da4:	89 d8                	mov    %ebx,%eax
  800da6:	f7 f5                	div    %ebp
  800da8:	89 cf                	mov    %ecx,%edi
  800daa:	89 fa                	mov    %edi,%edx
  800dac:	83 c4 1c             	add    $0x1c,%esp
  800daf:	5b                   	pop    %ebx
  800db0:	5e                   	pop    %esi
  800db1:	5f                   	pop    %edi
  800db2:	5d                   	pop    %ebp
  800db3:	c3                   	ret    
  800db4:	39 ce                	cmp    %ecx,%esi
  800db6:	77 28                	ja     800de0 <__udivdi3+0x7c>
  800db8:	0f bd fe             	bsr    %esi,%edi
  800dbb:	83 f7 1f             	xor    $0x1f,%edi
  800dbe:	75 40                	jne    800e00 <__udivdi3+0x9c>
  800dc0:	39 ce                	cmp    %ecx,%esi
  800dc2:	72 0a                	jb     800dce <__udivdi3+0x6a>
  800dc4:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800dc8:	0f 87 9e 00 00 00    	ja     800e6c <__udivdi3+0x108>
  800dce:	b8 01 00 00 00       	mov    $0x1,%eax
  800dd3:	89 fa                	mov    %edi,%edx
  800dd5:	83 c4 1c             	add    $0x1c,%esp
  800dd8:	5b                   	pop    %ebx
  800dd9:	5e                   	pop    %esi
  800dda:	5f                   	pop    %edi
  800ddb:	5d                   	pop    %ebp
  800ddc:	c3                   	ret    
  800ddd:	8d 76 00             	lea    0x0(%esi),%esi
  800de0:	31 ff                	xor    %edi,%edi
  800de2:	31 c0                	xor    %eax,%eax
  800de4:	89 fa                	mov    %edi,%edx
  800de6:	83 c4 1c             	add    $0x1c,%esp
  800de9:	5b                   	pop    %ebx
  800dea:	5e                   	pop    %esi
  800deb:	5f                   	pop    %edi
  800dec:	5d                   	pop    %ebp
  800ded:	c3                   	ret    
  800dee:	66 90                	xchg   %ax,%ax
  800df0:	89 d8                	mov    %ebx,%eax
  800df2:	f7 f7                	div    %edi
  800df4:	31 ff                	xor    %edi,%edi
  800df6:	89 fa                	mov    %edi,%edx
  800df8:	83 c4 1c             	add    $0x1c,%esp
  800dfb:	5b                   	pop    %ebx
  800dfc:	5e                   	pop    %esi
  800dfd:	5f                   	pop    %edi
  800dfe:	5d                   	pop    %ebp
  800dff:	c3                   	ret    
  800e00:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e05:	89 eb                	mov    %ebp,%ebx
  800e07:	29 fb                	sub    %edi,%ebx
  800e09:	89 f9                	mov    %edi,%ecx
  800e0b:	d3 e6                	shl    %cl,%esi
  800e0d:	89 c5                	mov    %eax,%ebp
  800e0f:	88 d9                	mov    %bl,%cl
  800e11:	d3 ed                	shr    %cl,%ebp
  800e13:	89 e9                	mov    %ebp,%ecx
  800e15:	09 f1                	or     %esi,%ecx
  800e17:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800e1b:	89 f9                	mov    %edi,%ecx
  800e1d:	d3 e0                	shl    %cl,%eax
  800e1f:	89 c5                	mov    %eax,%ebp
  800e21:	89 d6                	mov    %edx,%esi
  800e23:	88 d9                	mov    %bl,%cl
  800e25:	d3 ee                	shr    %cl,%esi
  800e27:	89 f9                	mov    %edi,%ecx
  800e29:	d3 e2                	shl    %cl,%edx
  800e2b:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e2f:	88 d9                	mov    %bl,%cl
  800e31:	d3 e8                	shr    %cl,%eax
  800e33:	09 c2                	or     %eax,%edx
  800e35:	89 d0                	mov    %edx,%eax
  800e37:	89 f2                	mov    %esi,%edx
  800e39:	f7 74 24 0c          	divl   0xc(%esp)
  800e3d:	89 d6                	mov    %edx,%esi
  800e3f:	89 c3                	mov    %eax,%ebx
  800e41:	f7 e5                	mul    %ebp
  800e43:	39 d6                	cmp    %edx,%esi
  800e45:	72 19                	jb     800e60 <__udivdi3+0xfc>
  800e47:	74 0b                	je     800e54 <__udivdi3+0xf0>
  800e49:	89 d8                	mov    %ebx,%eax
  800e4b:	31 ff                	xor    %edi,%edi
  800e4d:	e9 58 ff ff ff       	jmp    800daa <__udivdi3+0x46>
  800e52:	66 90                	xchg   %ax,%ax
  800e54:	8b 54 24 08          	mov    0x8(%esp),%edx
  800e58:	89 f9                	mov    %edi,%ecx
  800e5a:	d3 e2                	shl    %cl,%edx
  800e5c:	39 c2                	cmp    %eax,%edx
  800e5e:	73 e9                	jae    800e49 <__udivdi3+0xe5>
  800e60:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800e63:	31 ff                	xor    %edi,%edi
  800e65:	e9 40 ff ff ff       	jmp    800daa <__udivdi3+0x46>
  800e6a:	66 90                	xchg   %ax,%ax
  800e6c:	31 c0                	xor    %eax,%eax
  800e6e:	e9 37 ff ff ff       	jmp    800daa <__udivdi3+0x46>
  800e73:	90                   	nop

00800e74 <__umoddi3>:
  800e74:	55                   	push   %ebp
  800e75:	57                   	push   %edi
  800e76:	56                   	push   %esi
  800e77:	53                   	push   %ebx
  800e78:	83 ec 1c             	sub    $0x1c,%esp
  800e7b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800e7f:	8b 74 24 34          	mov    0x34(%esp),%esi
  800e83:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e87:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e8b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e8f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e93:	89 f3                	mov    %esi,%ebx
  800e95:	89 fa                	mov    %edi,%edx
  800e97:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e9b:	89 34 24             	mov    %esi,(%esp)
  800e9e:	85 c0                	test   %eax,%eax
  800ea0:	75 1a                	jne    800ebc <__umoddi3+0x48>
  800ea2:	39 f7                	cmp    %esi,%edi
  800ea4:	0f 86 a2 00 00 00    	jbe    800f4c <__umoddi3+0xd8>
  800eaa:	89 c8                	mov    %ecx,%eax
  800eac:	89 f2                	mov    %esi,%edx
  800eae:	f7 f7                	div    %edi
  800eb0:	89 d0                	mov    %edx,%eax
  800eb2:	31 d2                	xor    %edx,%edx
  800eb4:	83 c4 1c             	add    $0x1c,%esp
  800eb7:	5b                   	pop    %ebx
  800eb8:	5e                   	pop    %esi
  800eb9:	5f                   	pop    %edi
  800eba:	5d                   	pop    %ebp
  800ebb:	c3                   	ret    
  800ebc:	39 f0                	cmp    %esi,%eax
  800ebe:	0f 87 ac 00 00 00    	ja     800f70 <__umoddi3+0xfc>
  800ec4:	0f bd e8             	bsr    %eax,%ebp
  800ec7:	83 f5 1f             	xor    $0x1f,%ebp
  800eca:	0f 84 ac 00 00 00    	je     800f7c <__umoddi3+0x108>
  800ed0:	bf 20 00 00 00       	mov    $0x20,%edi
  800ed5:	29 ef                	sub    %ebp,%edi
  800ed7:	89 fe                	mov    %edi,%esi
  800ed9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800edd:	89 e9                	mov    %ebp,%ecx
  800edf:	d3 e0                	shl    %cl,%eax
  800ee1:	89 d7                	mov    %edx,%edi
  800ee3:	89 f1                	mov    %esi,%ecx
  800ee5:	d3 ef                	shr    %cl,%edi
  800ee7:	09 c7                	or     %eax,%edi
  800ee9:	89 e9                	mov    %ebp,%ecx
  800eeb:	d3 e2                	shl    %cl,%edx
  800eed:	89 14 24             	mov    %edx,(%esp)
  800ef0:	89 d8                	mov    %ebx,%eax
  800ef2:	d3 e0                	shl    %cl,%eax
  800ef4:	89 c2                	mov    %eax,%edx
  800ef6:	8b 44 24 08          	mov    0x8(%esp),%eax
  800efa:	d3 e0                	shl    %cl,%eax
  800efc:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f00:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f04:	89 f1                	mov    %esi,%ecx
  800f06:	d3 e8                	shr    %cl,%eax
  800f08:	09 d0                	or     %edx,%eax
  800f0a:	d3 eb                	shr    %cl,%ebx
  800f0c:	89 da                	mov    %ebx,%edx
  800f0e:	f7 f7                	div    %edi
  800f10:	89 d3                	mov    %edx,%ebx
  800f12:	f7 24 24             	mull   (%esp)
  800f15:	89 c6                	mov    %eax,%esi
  800f17:	89 d1                	mov    %edx,%ecx
  800f19:	39 d3                	cmp    %edx,%ebx
  800f1b:	0f 82 87 00 00 00    	jb     800fa8 <__umoddi3+0x134>
  800f21:	0f 84 91 00 00 00    	je     800fb8 <__umoddi3+0x144>
  800f27:	8b 54 24 04          	mov    0x4(%esp),%edx
  800f2b:	29 f2                	sub    %esi,%edx
  800f2d:	19 cb                	sbb    %ecx,%ebx
  800f2f:	89 d8                	mov    %ebx,%eax
  800f31:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800f35:	d3 e0                	shl    %cl,%eax
  800f37:	89 e9                	mov    %ebp,%ecx
  800f39:	d3 ea                	shr    %cl,%edx
  800f3b:	09 d0                	or     %edx,%eax
  800f3d:	89 e9                	mov    %ebp,%ecx
  800f3f:	d3 eb                	shr    %cl,%ebx
  800f41:	89 da                	mov    %ebx,%edx
  800f43:	83 c4 1c             	add    $0x1c,%esp
  800f46:	5b                   	pop    %ebx
  800f47:	5e                   	pop    %esi
  800f48:	5f                   	pop    %edi
  800f49:	5d                   	pop    %ebp
  800f4a:	c3                   	ret    
  800f4b:	90                   	nop
  800f4c:	89 fd                	mov    %edi,%ebp
  800f4e:	85 ff                	test   %edi,%edi
  800f50:	75 0b                	jne    800f5d <__umoddi3+0xe9>
  800f52:	b8 01 00 00 00       	mov    $0x1,%eax
  800f57:	31 d2                	xor    %edx,%edx
  800f59:	f7 f7                	div    %edi
  800f5b:	89 c5                	mov    %eax,%ebp
  800f5d:	89 f0                	mov    %esi,%eax
  800f5f:	31 d2                	xor    %edx,%edx
  800f61:	f7 f5                	div    %ebp
  800f63:	89 c8                	mov    %ecx,%eax
  800f65:	f7 f5                	div    %ebp
  800f67:	89 d0                	mov    %edx,%eax
  800f69:	e9 44 ff ff ff       	jmp    800eb2 <__umoddi3+0x3e>
  800f6e:	66 90                	xchg   %ax,%ax
  800f70:	89 c8                	mov    %ecx,%eax
  800f72:	89 f2                	mov    %esi,%edx
  800f74:	83 c4 1c             	add    $0x1c,%esp
  800f77:	5b                   	pop    %ebx
  800f78:	5e                   	pop    %esi
  800f79:	5f                   	pop    %edi
  800f7a:	5d                   	pop    %ebp
  800f7b:	c3                   	ret    
  800f7c:	3b 04 24             	cmp    (%esp),%eax
  800f7f:	72 06                	jb     800f87 <__umoddi3+0x113>
  800f81:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f85:	77 0f                	ja     800f96 <__umoddi3+0x122>
  800f87:	89 f2                	mov    %esi,%edx
  800f89:	29 f9                	sub    %edi,%ecx
  800f8b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f8f:	89 14 24             	mov    %edx,(%esp)
  800f92:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f96:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f9a:	8b 14 24             	mov    (%esp),%edx
  800f9d:	83 c4 1c             	add    $0x1c,%esp
  800fa0:	5b                   	pop    %ebx
  800fa1:	5e                   	pop    %esi
  800fa2:	5f                   	pop    %edi
  800fa3:	5d                   	pop    %ebp
  800fa4:	c3                   	ret    
  800fa5:	8d 76 00             	lea    0x0(%esi),%esi
  800fa8:	2b 04 24             	sub    (%esp),%eax
  800fab:	19 fa                	sbb    %edi,%edx
  800fad:	89 d1                	mov    %edx,%ecx
  800faf:	89 c6                	mov    %eax,%esi
  800fb1:	e9 71 ff ff ff       	jmp    800f27 <__umoddi3+0xb3>
  800fb6:	66 90                	xchg   %ax,%ax
  800fb8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800fbc:	72 ea                	jb     800fa8 <__umoddi3+0x134>
  800fbe:	89 d9                	mov    %ebx,%ecx
  800fc0:	e9 62 ff ff ff       	jmp    800f27 <__umoddi3+0xb3>
