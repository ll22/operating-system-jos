
obj/user/testkbd.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 32 02 00 00       	call   800263 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 04             	sub    $0x4,%esp
  80003a:	bb 0a 00 00 00       	mov    $0xa,%ebx
	int i, r;

	// Spin for a bit to let the console quiet
	for (i = 0; i < 10; ++i)
		sys_yield();
  80003f:	e8 87 0d 00 00       	call   800dcb <sys_yield>
umain(int argc, char **argv)
{
	int i, r;

	// Spin for a bit to let the console quiet
	for (i = 0; i < 10; ++i)
  800044:	4b                   	dec    %ebx
  800045:	75 f8                	jne    80003f <umain+0xc>
		sys_yield();

	close(0);
  800047:	83 ec 0c             	sub    $0xc,%esp
  80004a:	6a 00                	push   $0x0
  80004c:	e8 4d 11 00 00       	call   80119e <close>
	if ((r = opencons()) < 0)
  800051:	e8 bb 01 00 00       	call   800211 <opencons>
  800056:	83 c4 10             	add    $0x10,%esp
  800059:	85 c0                	test   %eax,%eax
  80005b:	79 12                	jns    80006f <umain+0x3c>
		panic("opencons: %e", r);
  80005d:	50                   	push   %eax
  80005e:	68 e0 1f 80 00       	push   $0x801fe0
  800063:	6a 0f                	push   $0xf
  800065:	68 ed 1f 80 00       	push   $0x801fed
  80006a:	e8 55 02 00 00       	call   8002c4 <_panic>
	if (r != 0)
  80006f:	85 c0                	test   %eax,%eax
  800071:	74 12                	je     800085 <umain+0x52>
		panic("first opencons used fd %d", r);
  800073:	50                   	push   %eax
  800074:	68 fc 1f 80 00       	push   $0x801ffc
  800079:	6a 11                	push   $0x11
  80007b:	68 ed 1f 80 00       	push   $0x801fed
  800080:	e8 3f 02 00 00       	call   8002c4 <_panic>
	if ((r = dup(0, 1)) < 0)
  800085:	83 ec 08             	sub    $0x8,%esp
  800088:	6a 01                	push   $0x1
  80008a:	6a 00                	push   $0x0
  80008c:	e8 5b 11 00 00       	call   8011ec <dup>
  800091:	83 c4 10             	add    $0x10,%esp
  800094:	85 c0                	test   %eax,%eax
  800096:	79 12                	jns    8000aa <umain+0x77>
		panic("dup: %e", r);
  800098:	50                   	push   %eax
  800099:	68 16 20 80 00       	push   $0x802016
  80009e:	6a 13                	push   $0x13
  8000a0:	68 ed 1f 80 00       	push   $0x801fed
  8000a5:	e8 1a 02 00 00       	call   8002c4 <_panic>

	for(;;){
		char *buf;

		buf = readline("Type a line: ");
  8000aa:	83 ec 0c             	sub    $0xc,%esp
  8000ad:	68 1e 20 80 00       	push   $0x80201e
  8000b2:	e8 14 08 00 00       	call   8008cb <readline>
		if (buf != NULL)
  8000b7:	83 c4 10             	add    $0x10,%esp
  8000ba:	85 c0                	test   %eax,%eax
  8000bc:	74 15                	je     8000d3 <umain+0xa0>
			fprintf(1, "%s\n", buf);
  8000be:	83 ec 04             	sub    $0x4,%esp
  8000c1:	50                   	push   %eax
  8000c2:	68 2c 20 80 00       	push   $0x80202c
  8000c7:	6a 01                	push   $0x1
  8000c9:	e8 f1 17 00 00       	call   8018bf <fprintf>
  8000ce:	83 c4 10             	add    $0x10,%esp
  8000d1:	eb d7                	jmp    8000aa <umain+0x77>
		else
			fprintf(1, "(end of file received)\n");
  8000d3:	83 ec 08             	sub    $0x8,%esp
  8000d6:	68 30 20 80 00       	push   $0x802030
  8000db:	6a 01                	push   $0x1
  8000dd:	e8 dd 17 00 00       	call   8018bf <fprintf>
  8000e2:	83 c4 10             	add    $0x10,%esp
  8000e5:	eb c3                	jmp    8000aa <umain+0x77>

008000e7 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  8000e7:	55                   	push   %ebp
  8000e8:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  8000ea:	b8 00 00 00 00       	mov    $0x0,%eax
  8000ef:	5d                   	pop    %ebp
  8000f0:	c3                   	ret    

008000f1 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  8000f1:	55                   	push   %ebp
  8000f2:	89 e5                	mov    %esp,%ebp
  8000f4:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  8000f7:	68 48 20 80 00       	push   $0x802048
  8000fc:	ff 75 0c             	pushl  0xc(%ebp)
  8000ff:	e8 02 09 00 00       	call   800a06 <strcpy>
	return 0;
}
  800104:	b8 00 00 00 00       	mov    $0x0,%eax
  800109:	c9                   	leave  
  80010a:	c3                   	ret    

0080010b <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  80010b:	55                   	push   %ebp
  80010c:	89 e5                	mov    %esp,%ebp
  80010e:	57                   	push   %edi
  80010f:	56                   	push   %esi
  800110:	53                   	push   %ebx
  800111:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800117:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  80011c:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800122:	eb 2c                	jmp    800150 <devcons_write+0x45>
		m = n - tot;
  800124:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800127:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  800129:	83 fb 7f             	cmp    $0x7f,%ebx
  80012c:	76 05                	jbe    800133 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  80012e:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  800133:	83 ec 04             	sub    $0x4,%esp
  800136:	53                   	push   %ebx
  800137:	03 45 0c             	add    0xc(%ebp),%eax
  80013a:	50                   	push   %eax
  80013b:	57                   	push   %edi
  80013c:	e8 3a 0a 00 00       	call   800b7b <memmove>
		sys_cputs(buf, m);
  800141:	83 c4 08             	add    $0x8,%esp
  800144:	53                   	push   %ebx
  800145:	57                   	push   %edi
  800146:	e8 e3 0b 00 00       	call   800d2e <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  80014b:	01 de                	add    %ebx,%esi
  80014d:	83 c4 10             	add    $0x10,%esp
  800150:	89 f0                	mov    %esi,%eax
  800152:	3b 75 10             	cmp    0x10(%ebp),%esi
  800155:	72 cd                	jb     800124 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  800157:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80015a:	5b                   	pop    %ebx
  80015b:	5e                   	pop    %esi
  80015c:	5f                   	pop    %edi
  80015d:	5d                   	pop    %ebp
  80015e:	c3                   	ret    

0080015f <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  80015f:	55                   	push   %ebp
  800160:	89 e5                	mov    %esp,%ebp
  800162:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  800165:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800169:	75 07                	jne    800172 <devcons_read+0x13>
  80016b:	eb 23                	jmp    800190 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  80016d:	e8 59 0c 00 00       	call   800dcb <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  800172:	e8 d5 0b 00 00       	call   800d4c <sys_cgetc>
  800177:	85 c0                	test   %eax,%eax
  800179:	74 f2                	je     80016d <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  80017b:	85 c0                	test   %eax,%eax
  80017d:	78 1d                	js     80019c <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  80017f:	83 f8 04             	cmp    $0x4,%eax
  800182:	74 13                	je     800197 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  800184:	8b 55 0c             	mov    0xc(%ebp),%edx
  800187:	88 02                	mov    %al,(%edx)
	return 1;
  800189:	b8 01 00 00 00       	mov    $0x1,%eax
  80018e:	eb 0c                	jmp    80019c <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  800190:	b8 00 00 00 00       	mov    $0x0,%eax
  800195:	eb 05                	jmp    80019c <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  800197:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  80019c:	c9                   	leave  
  80019d:	c3                   	ret    

0080019e <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  80019e:	55                   	push   %ebp
  80019f:	89 e5                	mov    %esp,%ebp
  8001a1:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  8001a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8001a7:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  8001aa:	6a 01                	push   $0x1
  8001ac:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8001af:	50                   	push   %eax
  8001b0:	e8 79 0b 00 00       	call   800d2e <sys_cputs>
}
  8001b5:	83 c4 10             	add    $0x10,%esp
  8001b8:	c9                   	leave  
  8001b9:	c3                   	ret    

008001ba <getchar>:

int
getchar(void)
{
  8001ba:	55                   	push   %ebp
  8001bb:	89 e5                	mov    %esp,%ebp
  8001bd:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  8001c0:	6a 01                	push   $0x1
  8001c2:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8001c5:	50                   	push   %eax
  8001c6:	6a 00                	push   $0x0
  8001c8:	e8 09 11 00 00       	call   8012d6 <read>
	if (r < 0)
  8001cd:	83 c4 10             	add    $0x10,%esp
  8001d0:	85 c0                	test   %eax,%eax
  8001d2:	78 0f                	js     8001e3 <getchar+0x29>
		return r;
	if (r < 1)
  8001d4:	85 c0                	test   %eax,%eax
  8001d6:	7e 06                	jle    8001de <getchar+0x24>
		return -E_EOF;
	return c;
  8001d8:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  8001dc:	eb 05                	jmp    8001e3 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  8001de:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  8001e3:	c9                   	leave  
  8001e4:	c3                   	ret    

008001e5 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  8001e5:	55                   	push   %ebp
  8001e6:	89 e5                	mov    %esp,%ebp
  8001e8:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8001eb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8001ee:	50                   	push   %eax
  8001ef:	ff 75 08             	pushl  0x8(%ebp)
  8001f2:	e8 79 0e 00 00       	call   801070 <fd_lookup>
  8001f7:	83 c4 10             	add    $0x10,%esp
  8001fa:	85 c0                	test   %eax,%eax
  8001fc:	78 11                	js     80020f <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  8001fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800201:	8b 15 00 30 80 00    	mov    0x803000,%edx
  800207:	39 10                	cmp    %edx,(%eax)
  800209:	0f 94 c0             	sete   %al
  80020c:	0f b6 c0             	movzbl %al,%eax
}
  80020f:	c9                   	leave  
  800210:	c3                   	ret    

00800211 <opencons>:

int
opencons(void)
{
  800211:	55                   	push   %ebp
  800212:	89 e5                	mov    %esp,%ebp
  800214:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  800217:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80021a:	50                   	push   %eax
  80021b:	e8 01 0e 00 00       	call   801021 <fd_alloc>
  800220:	83 c4 10             	add    $0x10,%esp
  800223:	85 c0                	test   %eax,%eax
  800225:	78 3a                	js     800261 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  800227:	83 ec 04             	sub    $0x4,%esp
  80022a:	68 07 04 00 00       	push   $0x407
  80022f:	ff 75 f4             	pushl  -0xc(%ebp)
  800232:	6a 00                	push   $0x0
  800234:	e8 b1 0b 00 00       	call   800dea <sys_page_alloc>
  800239:	83 c4 10             	add    $0x10,%esp
  80023c:	85 c0                	test   %eax,%eax
  80023e:	78 21                	js     800261 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  800240:	8b 15 00 30 80 00    	mov    0x803000,%edx
  800246:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800249:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  80024b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80024e:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  800255:	83 ec 0c             	sub    $0xc,%esp
  800258:	50                   	push   %eax
  800259:	e8 9c 0d 00 00       	call   800ffa <fd2num>
  80025e:	83 c4 10             	add    $0x10,%esp
}
  800261:	c9                   	leave  
  800262:	c3                   	ret    

00800263 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800263:	55                   	push   %ebp
  800264:	89 e5                	mov    %esp,%ebp
  800266:	56                   	push   %esi
  800267:	53                   	push   %ebx
  800268:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80026b:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80026e:	e8 39 0b 00 00       	call   800dac <sys_getenvid>
  800273:	25 ff 03 00 00       	and    $0x3ff,%eax
  800278:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80027f:	c1 e0 07             	shl    $0x7,%eax
  800282:	29 d0                	sub    %edx,%eax
  800284:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800289:	a3 04 44 80 00       	mov    %eax,0x804404
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80028e:	85 db                	test   %ebx,%ebx
  800290:	7e 07                	jle    800299 <libmain+0x36>
		binaryname = argv[0];
  800292:	8b 06                	mov    (%esi),%eax
  800294:	a3 1c 30 80 00       	mov    %eax,0x80301c

	// call user main routine
	umain(argc, argv);
  800299:	83 ec 08             	sub    $0x8,%esp
  80029c:	56                   	push   %esi
  80029d:	53                   	push   %ebx
  80029e:	e8 90 fd ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8002a3:	e8 0a 00 00 00       	call   8002b2 <exit>
}
  8002a8:	83 c4 10             	add    $0x10,%esp
  8002ab:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8002ae:	5b                   	pop    %ebx
  8002af:	5e                   	pop    %esi
  8002b0:	5d                   	pop    %ebp
  8002b1:	c3                   	ret    

008002b2 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8002b2:	55                   	push   %ebp
  8002b3:	89 e5                	mov    %esp,%ebp
  8002b5:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8002b8:	6a 00                	push   $0x0
  8002ba:	e8 ac 0a 00 00       	call   800d6b <sys_env_destroy>
}
  8002bf:	83 c4 10             	add    $0x10,%esp
  8002c2:	c9                   	leave  
  8002c3:	c3                   	ret    

008002c4 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8002c4:	55                   	push   %ebp
  8002c5:	89 e5                	mov    %esp,%ebp
  8002c7:	56                   	push   %esi
  8002c8:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8002c9:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8002cc:	8b 35 1c 30 80 00    	mov    0x80301c,%esi
  8002d2:	e8 d5 0a 00 00       	call   800dac <sys_getenvid>
  8002d7:	83 ec 0c             	sub    $0xc,%esp
  8002da:	ff 75 0c             	pushl  0xc(%ebp)
  8002dd:	ff 75 08             	pushl  0x8(%ebp)
  8002e0:	56                   	push   %esi
  8002e1:	50                   	push   %eax
  8002e2:	68 60 20 80 00       	push   $0x802060
  8002e7:	e8 b0 00 00 00       	call   80039c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8002ec:	83 c4 18             	add    $0x18,%esp
  8002ef:	53                   	push   %ebx
  8002f0:	ff 75 10             	pushl  0x10(%ebp)
  8002f3:	e8 53 00 00 00       	call   80034b <vcprintf>
	cprintf("\n");
  8002f8:	c7 04 24 46 20 80 00 	movl   $0x802046,(%esp)
  8002ff:	e8 98 00 00 00       	call   80039c <cprintf>
  800304:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800307:	cc                   	int3   
  800308:	eb fd                	jmp    800307 <_panic+0x43>

0080030a <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80030a:	55                   	push   %ebp
  80030b:	89 e5                	mov    %esp,%ebp
  80030d:	53                   	push   %ebx
  80030e:	83 ec 04             	sub    $0x4,%esp
  800311:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800314:	8b 13                	mov    (%ebx),%edx
  800316:	8d 42 01             	lea    0x1(%edx),%eax
  800319:	89 03                	mov    %eax,(%ebx)
  80031b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80031e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800322:	3d ff 00 00 00       	cmp    $0xff,%eax
  800327:	75 1a                	jne    800343 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800329:	83 ec 08             	sub    $0x8,%esp
  80032c:	68 ff 00 00 00       	push   $0xff
  800331:	8d 43 08             	lea    0x8(%ebx),%eax
  800334:	50                   	push   %eax
  800335:	e8 f4 09 00 00       	call   800d2e <sys_cputs>
		b->idx = 0;
  80033a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800340:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800343:	ff 43 04             	incl   0x4(%ebx)
}
  800346:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800349:	c9                   	leave  
  80034a:	c3                   	ret    

0080034b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80034b:	55                   	push   %ebp
  80034c:	89 e5                	mov    %esp,%ebp
  80034e:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800354:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80035b:	00 00 00 
	b.cnt = 0;
  80035e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800365:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800368:	ff 75 0c             	pushl  0xc(%ebp)
  80036b:	ff 75 08             	pushl  0x8(%ebp)
  80036e:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800374:	50                   	push   %eax
  800375:	68 0a 03 80 00       	push   $0x80030a
  80037a:	e8 51 01 00 00       	call   8004d0 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80037f:	83 c4 08             	add    $0x8,%esp
  800382:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800388:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80038e:	50                   	push   %eax
  80038f:	e8 9a 09 00 00       	call   800d2e <sys_cputs>

	return b.cnt;
}
  800394:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80039a:	c9                   	leave  
  80039b:	c3                   	ret    

0080039c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80039c:	55                   	push   %ebp
  80039d:	89 e5                	mov    %esp,%ebp
  80039f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8003a2:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8003a5:	50                   	push   %eax
  8003a6:	ff 75 08             	pushl  0x8(%ebp)
  8003a9:	e8 9d ff ff ff       	call   80034b <vcprintf>
	va_end(ap);

	return cnt;
}
  8003ae:	c9                   	leave  
  8003af:	c3                   	ret    

008003b0 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8003b0:	55                   	push   %ebp
  8003b1:	89 e5                	mov    %esp,%ebp
  8003b3:	57                   	push   %edi
  8003b4:	56                   	push   %esi
  8003b5:	53                   	push   %ebx
  8003b6:	83 ec 1c             	sub    $0x1c,%esp
  8003b9:	89 c7                	mov    %eax,%edi
  8003bb:	89 d6                	mov    %edx,%esi
  8003bd:	8b 45 08             	mov    0x8(%ebp),%eax
  8003c0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8003c3:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8003c6:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8003c9:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8003cc:	bb 00 00 00 00       	mov    $0x0,%ebx
  8003d1:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8003d4:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8003d7:	39 d3                	cmp    %edx,%ebx
  8003d9:	72 05                	jb     8003e0 <printnum+0x30>
  8003db:	39 45 10             	cmp    %eax,0x10(%ebp)
  8003de:	77 45                	ja     800425 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8003e0:	83 ec 0c             	sub    $0xc,%esp
  8003e3:	ff 75 18             	pushl  0x18(%ebp)
  8003e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e9:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8003ec:	53                   	push   %ebx
  8003ed:	ff 75 10             	pushl  0x10(%ebp)
  8003f0:	83 ec 08             	sub    $0x8,%esp
  8003f3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8003f6:	ff 75 e0             	pushl  -0x20(%ebp)
  8003f9:	ff 75 dc             	pushl  -0x24(%ebp)
  8003fc:	ff 75 d8             	pushl  -0x28(%ebp)
  8003ff:	e8 70 19 00 00       	call   801d74 <__udivdi3>
  800404:	83 c4 18             	add    $0x18,%esp
  800407:	52                   	push   %edx
  800408:	50                   	push   %eax
  800409:	89 f2                	mov    %esi,%edx
  80040b:	89 f8                	mov    %edi,%eax
  80040d:	e8 9e ff ff ff       	call   8003b0 <printnum>
  800412:	83 c4 20             	add    $0x20,%esp
  800415:	eb 16                	jmp    80042d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800417:	83 ec 08             	sub    $0x8,%esp
  80041a:	56                   	push   %esi
  80041b:	ff 75 18             	pushl  0x18(%ebp)
  80041e:	ff d7                	call   *%edi
  800420:	83 c4 10             	add    $0x10,%esp
  800423:	eb 03                	jmp    800428 <printnum+0x78>
  800425:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800428:	4b                   	dec    %ebx
  800429:	85 db                	test   %ebx,%ebx
  80042b:	7f ea                	jg     800417 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80042d:	83 ec 08             	sub    $0x8,%esp
  800430:	56                   	push   %esi
  800431:	83 ec 04             	sub    $0x4,%esp
  800434:	ff 75 e4             	pushl  -0x1c(%ebp)
  800437:	ff 75 e0             	pushl  -0x20(%ebp)
  80043a:	ff 75 dc             	pushl  -0x24(%ebp)
  80043d:	ff 75 d8             	pushl  -0x28(%ebp)
  800440:	e8 3f 1a 00 00       	call   801e84 <__umoddi3>
  800445:	83 c4 14             	add    $0x14,%esp
  800448:	0f be 80 83 20 80 00 	movsbl 0x802083(%eax),%eax
  80044f:	50                   	push   %eax
  800450:	ff d7                	call   *%edi
}
  800452:	83 c4 10             	add    $0x10,%esp
  800455:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800458:	5b                   	pop    %ebx
  800459:	5e                   	pop    %esi
  80045a:	5f                   	pop    %edi
  80045b:	5d                   	pop    %ebp
  80045c:	c3                   	ret    

0080045d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80045d:	55                   	push   %ebp
  80045e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800460:	83 fa 01             	cmp    $0x1,%edx
  800463:	7e 0e                	jle    800473 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800465:	8b 10                	mov    (%eax),%edx
  800467:	8d 4a 08             	lea    0x8(%edx),%ecx
  80046a:	89 08                	mov    %ecx,(%eax)
  80046c:	8b 02                	mov    (%edx),%eax
  80046e:	8b 52 04             	mov    0x4(%edx),%edx
  800471:	eb 22                	jmp    800495 <getuint+0x38>
	else if (lflag)
  800473:	85 d2                	test   %edx,%edx
  800475:	74 10                	je     800487 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800477:	8b 10                	mov    (%eax),%edx
  800479:	8d 4a 04             	lea    0x4(%edx),%ecx
  80047c:	89 08                	mov    %ecx,(%eax)
  80047e:	8b 02                	mov    (%edx),%eax
  800480:	ba 00 00 00 00       	mov    $0x0,%edx
  800485:	eb 0e                	jmp    800495 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800487:	8b 10                	mov    (%eax),%edx
  800489:	8d 4a 04             	lea    0x4(%edx),%ecx
  80048c:	89 08                	mov    %ecx,(%eax)
  80048e:	8b 02                	mov    (%edx),%eax
  800490:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800495:	5d                   	pop    %ebp
  800496:	c3                   	ret    

00800497 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800497:	55                   	push   %ebp
  800498:	89 e5                	mov    %esp,%ebp
  80049a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80049d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8004a0:	8b 10                	mov    (%eax),%edx
  8004a2:	3b 50 04             	cmp    0x4(%eax),%edx
  8004a5:	73 0a                	jae    8004b1 <sprintputch+0x1a>
		*b->buf++ = ch;
  8004a7:	8d 4a 01             	lea    0x1(%edx),%ecx
  8004aa:	89 08                	mov    %ecx,(%eax)
  8004ac:	8b 45 08             	mov    0x8(%ebp),%eax
  8004af:	88 02                	mov    %al,(%edx)
}
  8004b1:	5d                   	pop    %ebp
  8004b2:	c3                   	ret    

008004b3 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8004b3:	55                   	push   %ebp
  8004b4:	89 e5                	mov    %esp,%ebp
  8004b6:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8004b9:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8004bc:	50                   	push   %eax
  8004bd:	ff 75 10             	pushl  0x10(%ebp)
  8004c0:	ff 75 0c             	pushl  0xc(%ebp)
  8004c3:	ff 75 08             	pushl  0x8(%ebp)
  8004c6:	e8 05 00 00 00       	call   8004d0 <vprintfmt>
	va_end(ap);
}
  8004cb:	83 c4 10             	add    $0x10,%esp
  8004ce:	c9                   	leave  
  8004cf:	c3                   	ret    

008004d0 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8004d0:	55                   	push   %ebp
  8004d1:	89 e5                	mov    %esp,%ebp
  8004d3:	57                   	push   %edi
  8004d4:	56                   	push   %esi
  8004d5:	53                   	push   %ebx
  8004d6:	83 ec 2c             	sub    $0x2c,%esp
  8004d9:	8b 75 08             	mov    0x8(%ebp),%esi
  8004dc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8004df:	8b 7d 10             	mov    0x10(%ebp),%edi
  8004e2:	eb 12                	jmp    8004f6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8004e4:	85 c0                	test   %eax,%eax
  8004e6:	0f 84 68 03 00 00    	je     800854 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8004ec:	83 ec 08             	sub    $0x8,%esp
  8004ef:	53                   	push   %ebx
  8004f0:	50                   	push   %eax
  8004f1:	ff d6                	call   *%esi
  8004f3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004f6:	47                   	inc    %edi
  8004f7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8004fb:	83 f8 25             	cmp    $0x25,%eax
  8004fe:	75 e4                	jne    8004e4 <vprintfmt+0x14>
  800500:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800504:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80050b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800512:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800519:	ba 00 00 00 00       	mov    $0x0,%edx
  80051e:	eb 07                	jmp    800527 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800520:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800523:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800527:	8d 47 01             	lea    0x1(%edi),%eax
  80052a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80052d:	0f b6 0f             	movzbl (%edi),%ecx
  800530:	8a 07                	mov    (%edi),%al
  800532:	83 e8 23             	sub    $0x23,%eax
  800535:	3c 55                	cmp    $0x55,%al
  800537:	0f 87 fe 02 00 00    	ja     80083b <vprintfmt+0x36b>
  80053d:	0f b6 c0             	movzbl %al,%eax
  800540:	ff 24 85 c0 21 80 00 	jmp    *0x8021c0(,%eax,4)
  800547:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80054a:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80054e:	eb d7                	jmp    800527 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800550:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800553:	b8 00 00 00 00       	mov    $0x0,%eax
  800558:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80055b:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80055e:	01 c0                	add    %eax,%eax
  800560:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800564:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800567:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80056a:	83 fa 09             	cmp    $0x9,%edx
  80056d:	77 34                	ja     8005a3 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80056f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800570:	eb e9                	jmp    80055b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800572:	8b 45 14             	mov    0x14(%ebp),%eax
  800575:	8d 48 04             	lea    0x4(%eax),%ecx
  800578:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80057b:	8b 00                	mov    (%eax),%eax
  80057d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800580:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800583:	eb 24                	jmp    8005a9 <vprintfmt+0xd9>
  800585:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800589:	79 07                	jns    800592 <vprintfmt+0xc2>
  80058b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800592:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800595:	eb 90                	jmp    800527 <vprintfmt+0x57>
  800597:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80059a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8005a1:	eb 84                	jmp    800527 <vprintfmt+0x57>
  8005a3:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8005a6:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8005a9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005ad:	0f 89 74 ff ff ff    	jns    800527 <vprintfmt+0x57>
				width = precision, precision = -1;
  8005b3:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8005b6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8005b9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005c0:	e9 62 ff ff ff       	jmp    800527 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8005c5:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8005c9:	e9 59 ff ff ff       	jmp    800527 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8005ce:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d1:	8d 50 04             	lea    0x4(%eax),%edx
  8005d4:	89 55 14             	mov    %edx,0x14(%ebp)
  8005d7:	83 ec 08             	sub    $0x8,%esp
  8005da:	53                   	push   %ebx
  8005db:	ff 30                	pushl  (%eax)
  8005dd:	ff d6                	call   *%esi
			break;
  8005df:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8005e5:	e9 0c ff ff ff       	jmp    8004f6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8005ea:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ed:	8d 50 04             	lea    0x4(%eax),%edx
  8005f0:	89 55 14             	mov    %edx,0x14(%ebp)
  8005f3:	8b 00                	mov    (%eax),%eax
  8005f5:	85 c0                	test   %eax,%eax
  8005f7:	79 02                	jns    8005fb <vprintfmt+0x12b>
  8005f9:	f7 d8                	neg    %eax
  8005fb:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8005fd:	83 f8 0f             	cmp    $0xf,%eax
  800600:	7f 0b                	jg     80060d <vprintfmt+0x13d>
  800602:	8b 04 85 20 23 80 00 	mov    0x802320(,%eax,4),%eax
  800609:	85 c0                	test   %eax,%eax
  80060b:	75 18                	jne    800625 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80060d:	52                   	push   %edx
  80060e:	68 9b 20 80 00       	push   $0x80209b
  800613:	53                   	push   %ebx
  800614:	56                   	push   %esi
  800615:	e8 99 fe ff ff       	call   8004b3 <printfmt>
  80061a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80061d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800620:	e9 d1 fe ff ff       	jmp    8004f6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800625:	50                   	push   %eax
  800626:	68 65 24 80 00       	push   $0x802465
  80062b:	53                   	push   %ebx
  80062c:	56                   	push   %esi
  80062d:	e8 81 fe ff ff       	call   8004b3 <printfmt>
  800632:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800635:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800638:	e9 b9 fe ff ff       	jmp    8004f6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80063d:	8b 45 14             	mov    0x14(%ebp),%eax
  800640:	8d 50 04             	lea    0x4(%eax),%edx
  800643:	89 55 14             	mov    %edx,0x14(%ebp)
  800646:	8b 38                	mov    (%eax),%edi
  800648:	85 ff                	test   %edi,%edi
  80064a:	75 05                	jne    800651 <vprintfmt+0x181>
				p = "(null)";
  80064c:	bf 94 20 80 00       	mov    $0x802094,%edi
			if (width > 0 && padc != '-')
  800651:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800655:	0f 8e 90 00 00 00    	jle    8006eb <vprintfmt+0x21b>
  80065b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80065f:	0f 84 8e 00 00 00    	je     8006f3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800665:	83 ec 08             	sub    $0x8,%esp
  800668:	ff 75 d0             	pushl  -0x30(%ebp)
  80066b:	57                   	push   %edi
  80066c:	e8 76 03 00 00       	call   8009e7 <strnlen>
  800671:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800674:	29 c1                	sub    %eax,%ecx
  800676:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800679:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80067c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800680:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800683:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800686:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800688:	eb 0d                	jmp    800697 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80068a:	83 ec 08             	sub    $0x8,%esp
  80068d:	53                   	push   %ebx
  80068e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800691:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800693:	4f                   	dec    %edi
  800694:	83 c4 10             	add    $0x10,%esp
  800697:	85 ff                	test   %edi,%edi
  800699:	7f ef                	jg     80068a <vprintfmt+0x1ba>
  80069b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80069e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8006a1:	89 c8                	mov    %ecx,%eax
  8006a3:	85 c9                	test   %ecx,%ecx
  8006a5:	79 05                	jns    8006ac <vprintfmt+0x1dc>
  8006a7:	b8 00 00 00 00       	mov    $0x0,%eax
  8006ac:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8006af:	29 c1                	sub    %eax,%ecx
  8006b1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8006b4:	89 75 08             	mov    %esi,0x8(%ebp)
  8006b7:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8006ba:	eb 3d                	jmp    8006f9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8006bc:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8006c0:	74 19                	je     8006db <vprintfmt+0x20b>
  8006c2:	0f be c0             	movsbl %al,%eax
  8006c5:	83 e8 20             	sub    $0x20,%eax
  8006c8:	83 f8 5e             	cmp    $0x5e,%eax
  8006cb:	76 0e                	jbe    8006db <vprintfmt+0x20b>
					putch('?', putdat);
  8006cd:	83 ec 08             	sub    $0x8,%esp
  8006d0:	53                   	push   %ebx
  8006d1:	6a 3f                	push   $0x3f
  8006d3:	ff 55 08             	call   *0x8(%ebp)
  8006d6:	83 c4 10             	add    $0x10,%esp
  8006d9:	eb 0b                	jmp    8006e6 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8006db:	83 ec 08             	sub    $0x8,%esp
  8006de:	53                   	push   %ebx
  8006df:	52                   	push   %edx
  8006e0:	ff 55 08             	call   *0x8(%ebp)
  8006e3:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8006e6:	ff 4d e4             	decl   -0x1c(%ebp)
  8006e9:	eb 0e                	jmp    8006f9 <vprintfmt+0x229>
  8006eb:	89 75 08             	mov    %esi,0x8(%ebp)
  8006ee:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8006f1:	eb 06                	jmp    8006f9 <vprintfmt+0x229>
  8006f3:	89 75 08             	mov    %esi,0x8(%ebp)
  8006f6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8006f9:	47                   	inc    %edi
  8006fa:	8a 47 ff             	mov    -0x1(%edi),%al
  8006fd:	0f be d0             	movsbl %al,%edx
  800700:	85 d2                	test   %edx,%edx
  800702:	74 1d                	je     800721 <vprintfmt+0x251>
  800704:	85 f6                	test   %esi,%esi
  800706:	78 b4                	js     8006bc <vprintfmt+0x1ec>
  800708:	4e                   	dec    %esi
  800709:	79 b1                	jns    8006bc <vprintfmt+0x1ec>
  80070b:	8b 75 08             	mov    0x8(%ebp),%esi
  80070e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800711:	eb 14                	jmp    800727 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800713:	83 ec 08             	sub    $0x8,%esp
  800716:	53                   	push   %ebx
  800717:	6a 20                	push   $0x20
  800719:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80071b:	4f                   	dec    %edi
  80071c:	83 c4 10             	add    $0x10,%esp
  80071f:	eb 06                	jmp    800727 <vprintfmt+0x257>
  800721:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800724:	8b 75 08             	mov    0x8(%ebp),%esi
  800727:	85 ff                	test   %edi,%edi
  800729:	7f e8                	jg     800713 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80072b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80072e:	e9 c3 fd ff ff       	jmp    8004f6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800733:	83 fa 01             	cmp    $0x1,%edx
  800736:	7e 16                	jle    80074e <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800738:	8b 45 14             	mov    0x14(%ebp),%eax
  80073b:	8d 50 08             	lea    0x8(%eax),%edx
  80073e:	89 55 14             	mov    %edx,0x14(%ebp)
  800741:	8b 50 04             	mov    0x4(%eax),%edx
  800744:	8b 00                	mov    (%eax),%eax
  800746:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800749:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80074c:	eb 32                	jmp    800780 <vprintfmt+0x2b0>
	else if (lflag)
  80074e:	85 d2                	test   %edx,%edx
  800750:	74 18                	je     80076a <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800752:	8b 45 14             	mov    0x14(%ebp),%eax
  800755:	8d 50 04             	lea    0x4(%eax),%edx
  800758:	89 55 14             	mov    %edx,0x14(%ebp)
  80075b:	8b 00                	mov    (%eax),%eax
  80075d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800760:	89 c1                	mov    %eax,%ecx
  800762:	c1 f9 1f             	sar    $0x1f,%ecx
  800765:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800768:	eb 16                	jmp    800780 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80076a:	8b 45 14             	mov    0x14(%ebp),%eax
  80076d:	8d 50 04             	lea    0x4(%eax),%edx
  800770:	89 55 14             	mov    %edx,0x14(%ebp)
  800773:	8b 00                	mov    (%eax),%eax
  800775:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800778:	89 c1                	mov    %eax,%ecx
  80077a:	c1 f9 1f             	sar    $0x1f,%ecx
  80077d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800780:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800783:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800786:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80078a:	79 76                	jns    800802 <vprintfmt+0x332>
				putch('-', putdat);
  80078c:	83 ec 08             	sub    $0x8,%esp
  80078f:	53                   	push   %ebx
  800790:	6a 2d                	push   $0x2d
  800792:	ff d6                	call   *%esi
				num = -(long long) num;
  800794:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800797:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80079a:	f7 d8                	neg    %eax
  80079c:	83 d2 00             	adc    $0x0,%edx
  80079f:	f7 da                	neg    %edx
  8007a1:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8007a4:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8007a9:	eb 5c                	jmp    800807 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8007ab:	8d 45 14             	lea    0x14(%ebp),%eax
  8007ae:	e8 aa fc ff ff       	call   80045d <getuint>
			base = 10;
  8007b3:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8007b8:	eb 4d                	jmp    800807 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8007ba:	8d 45 14             	lea    0x14(%ebp),%eax
  8007bd:	e8 9b fc ff ff       	call   80045d <getuint>
			base = 8;
  8007c2:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8007c7:	eb 3e                	jmp    800807 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8007c9:	83 ec 08             	sub    $0x8,%esp
  8007cc:	53                   	push   %ebx
  8007cd:	6a 30                	push   $0x30
  8007cf:	ff d6                	call   *%esi
			putch('x', putdat);
  8007d1:	83 c4 08             	add    $0x8,%esp
  8007d4:	53                   	push   %ebx
  8007d5:	6a 78                	push   $0x78
  8007d7:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8007d9:	8b 45 14             	mov    0x14(%ebp),%eax
  8007dc:	8d 50 04             	lea    0x4(%eax),%edx
  8007df:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8007e2:	8b 00                	mov    (%eax),%eax
  8007e4:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8007e9:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8007ec:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8007f1:	eb 14                	jmp    800807 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8007f3:	8d 45 14             	lea    0x14(%ebp),%eax
  8007f6:	e8 62 fc ff ff       	call   80045d <getuint>
			base = 16;
  8007fb:	b9 10 00 00 00       	mov    $0x10,%ecx
  800800:	eb 05                	jmp    800807 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800802:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800807:	83 ec 0c             	sub    $0xc,%esp
  80080a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80080e:	57                   	push   %edi
  80080f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800812:	51                   	push   %ecx
  800813:	52                   	push   %edx
  800814:	50                   	push   %eax
  800815:	89 da                	mov    %ebx,%edx
  800817:	89 f0                	mov    %esi,%eax
  800819:	e8 92 fb ff ff       	call   8003b0 <printnum>
			break;
  80081e:	83 c4 20             	add    $0x20,%esp
  800821:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800824:	e9 cd fc ff ff       	jmp    8004f6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800829:	83 ec 08             	sub    $0x8,%esp
  80082c:	53                   	push   %ebx
  80082d:	51                   	push   %ecx
  80082e:	ff d6                	call   *%esi
			break;
  800830:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800833:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800836:	e9 bb fc ff ff       	jmp    8004f6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80083b:	83 ec 08             	sub    $0x8,%esp
  80083e:	53                   	push   %ebx
  80083f:	6a 25                	push   $0x25
  800841:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800843:	83 c4 10             	add    $0x10,%esp
  800846:	eb 01                	jmp    800849 <vprintfmt+0x379>
  800848:	4f                   	dec    %edi
  800849:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80084d:	75 f9                	jne    800848 <vprintfmt+0x378>
  80084f:	e9 a2 fc ff ff       	jmp    8004f6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800854:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800857:	5b                   	pop    %ebx
  800858:	5e                   	pop    %esi
  800859:	5f                   	pop    %edi
  80085a:	5d                   	pop    %ebp
  80085b:	c3                   	ret    

0080085c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80085c:	55                   	push   %ebp
  80085d:	89 e5                	mov    %esp,%ebp
  80085f:	83 ec 18             	sub    $0x18,%esp
  800862:	8b 45 08             	mov    0x8(%ebp),%eax
  800865:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800868:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80086b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80086f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800872:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800879:	85 c0                	test   %eax,%eax
  80087b:	74 26                	je     8008a3 <vsnprintf+0x47>
  80087d:	85 d2                	test   %edx,%edx
  80087f:	7e 29                	jle    8008aa <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800881:	ff 75 14             	pushl  0x14(%ebp)
  800884:	ff 75 10             	pushl  0x10(%ebp)
  800887:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80088a:	50                   	push   %eax
  80088b:	68 97 04 80 00       	push   $0x800497
  800890:	e8 3b fc ff ff       	call   8004d0 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800895:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800898:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80089b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80089e:	83 c4 10             	add    $0x10,%esp
  8008a1:	eb 0c                	jmp    8008af <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8008a3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8008a8:	eb 05                	jmp    8008af <vsnprintf+0x53>
  8008aa:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8008af:	c9                   	leave  
  8008b0:	c3                   	ret    

008008b1 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8008b1:	55                   	push   %ebp
  8008b2:	89 e5                	mov    %esp,%ebp
  8008b4:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8008b7:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8008ba:	50                   	push   %eax
  8008bb:	ff 75 10             	pushl  0x10(%ebp)
  8008be:	ff 75 0c             	pushl  0xc(%ebp)
  8008c1:	ff 75 08             	pushl  0x8(%ebp)
  8008c4:	e8 93 ff ff ff       	call   80085c <vsnprintf>
	va_end(ap);

	return rc;
}
  8008c9:	c9                   	leave  
  8008ca:	c3                   	ret    

008008cb <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
  8008cb:	55                   	push   %ebp
  8008cc:	89 e5                	mov    %esp,%ebp
  8008ce:	57                   	push   %edi
  8008cf:	56                   	push   %esi
  8008d0:	53                   	push   %ebx
  8008d1:	83 ec 0c             	sub    $0xc,%esp
  8008d4:	8b 45 08             	mov    0x8(%ebp),%eax

#if JOS_KERNEL
	if (prompt != NULL)
		cprintf("%s", prompt);
#else
	if (prompt != NULL)
  8008d7:	85 c0                	test   %eax,%eax
  8008d9:	74 13                	je     8008ee <readline+0x23>
		fprintf(1, "%s", prompt);
  8008db:	83 ec 04             	sub    $0x4,%esp
  8008de:	50                   	push   %eax
  8008df:	68 65 24 80 00       	push   $0x802465
  8008e4:	6a 01                	push   $0x1
  8008e6:	e8 d4 0f 00 00       	call   8018bf <fprintf>
  8008eb:	83 c4 10             	add    $0x10,%esp
#endif

	i = 0;
	echoing = iscons(0);
  8008ee:	83 ec 0c             	sub    $0xc,%esp
  8008f1:	6a 00                	push   $0x0
  8008f3:	e8 ed f8 ff ff       	call   8001e5 <iscons>
  8008f8:	89 c7                	mov    %eax,%edi
  8008fa:	83 c4 10             	add    $0x10,%esp
#else
	if (prompt != NULL)
		fprintf(1, "%s", prompt);
#endif

	i = 0;
  8008fd:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
  800902:	e8 b3 f8 ff ff       	call   8001ba <getchar>
  800907:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
  800909:	85 c0                	test   %eax,%eax
  80090b:	79 24                	jns    800931 <readline+0x66>
			if (c != -E_EOF)
  80090d:	83 f8 f8             	cmp    $0xfffffff8,%eax
  800910:	0f 84 90 00 00 00    	je     8009a6 <readline+0xdb>
				cprintf("read error: %e\n", c);
  800916:	83 ec 08             	sub    $0x8,%esp
  800919:	50                   	push   %eax
  80091a:	68 7f 23 80 00       	push   $0x80237f
  80091f:	e8 78 fa ff ff       	call   80039c <cprintf>
  800924:	83 c4 10             	add    $0x10,%esp
			return NULL;
  800927:	b8 00 00 00 00       	mov    $0x0,%eax
  80092c:	e9 98 00 00 00       	jmp    8009c9 <readline+0xfe>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
  800931:	83 f8 08             	cmp    $0x8,%eax
  800934:	74 7d                	je     8009b3 <readline+0xe8>
  800936:	83 f8 7f             	cmp    $0x7f,%eax
  800939:	75 16                	jne    800951 <readline+0x86>
  80093b:	eb 70                	jmp    8009ad <readline+0xe2>
			if (echoing)
  80093d:	85 ff                	test   %edi,%edi
  80093f:	74 0d                	je     80094e <readline+0x83>
				cputchar('\b');
  800941:	83 ec 0c             	sub    $0xc,%esp
  800944:	6a 08                	push   $0x8
  800946:	e8 53 f8 ff ff       	call   80019e <cputchar>
  80094b:	83 c4 10             	add    $0x10,%esp
			i--;
  80094e:	4e                   	dec    %esi
  80094f:	eb b1                	jmp    800902 <readline+0x37>
		} else if (c >= ' ' && i < BUFLEN-1) {
  800951:	83 f8 1f             	cmp    $0x1f,%eax
  800954:	7e 23                	jle    800979 <readline+0xae>
  800956:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
  80095c:	7f 1b                	jg     800979 <readline+0xae>
			if (echoing)
  80095e:	85 ff                	test   %edi,%edi
  800960:	74 0c                	je     80096e <readline+0xa3>
				cputchar(c);
  800962:	83 ec 0c             	sub    $0xc,%esp
  800965:	53                   	push   %ebx
  800966:	e8 33 f8 ff ff       	call   80019e <cputchar>
  80096b:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
  80096e:	88 9e 00 40 80 00    	mov    %bl,0x804000(%esi)
  800974:	8d 76 01             	lea    0x1(%esi),%esi
  800977:	eb 89                	jmp    800902 <readline+0x37>
		} else if (c == '\n' || c == '\r') {
  800979:	83 fb 0a             	cmp    $0xa,%ebx
  80097c:	74 09                	je     800987 <readline+0xbc>
  80097e:	83 fb 0d             	cmp    $0xd,%ebx
  800981:	0f 85 7b ff ff ff    	jne    800902 <readline+0x37>
			if (echoing)
  800987:	85 ff                	test   %edi,%edi
  800989:	74 0d                	je     800998 <readline+0xcd>
				cputchar('\n');
  80098b:	83 ec 0c             	sub    $0xc,%esp
  80098e:	6a 0a                	push   $0xa
  800990:	e8 09 f8 ff ff       	call   80019e <cputchar>
  800995:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
  800998:	c6 86 00 40 80 00 00 	movb   $0x0,0x804000(%esi)
			return buf;
  80099f:	b8 00 40 80 00       	mov    $0x804000,%eax
  8009a4:	eb 23                	jmp    8009c9 <readline+0xfe>
	while (1) {
		c = getchar();
		if (c < 0) {
			if (c != -E_EOF)
				cprintf("read error: %e\n", c);
			return NULL;
  8009a6:	b8 00 00 00 00       	mov    $0x0,%eax
  8009ab:	eb 1c                	jmp    8009c9 <readline+0xfe>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
  8009ad:	85 f6                	test   %esi,%esi
  8009af:	7f 8c                	jg     80093d <readline+0x72>
  8009b1:	eb 09                	jmp    8009bc <readline+0xf1>
  8009b3:	85 f6                	test   %esi,%esi
  8009b5:	7f 86                	jg     80093d <readline+0x72>
  8009b7:	e9 46 ff ff ff       	jmp    800902 <readline+0x37>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
  8009bc:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
  8009c2:	7e 9a                	jle    80095e <readline+0x93>
  8009c4:	e9 39 ff ff ff       	jmp    800902 <readline+0x37>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
  8009c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8009cc:	5b                   	pop    %ebx
  8009cd:	5e                   	pop    %esi
  8009ce:	5f                   	pop    %edi
  8009cf:	5d                   	pop    %ebp
  8009d0:	c3                   	ret    

008009d1 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8009d1:	55                   	push   %ebp
  8009d2:	89 e5                	mov    %esp,%ebp
  8009d4:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8009d7:	b8 00 00 00 00       	mov    $0x0,%eax
  8009dc:	eb 01                	jmp    8009df <strlen+0xe>
		n++;
  8009de:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8009df:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8009e3:	75 f9                	jne    8009de <strlen+0xd>
		n++;
	return n;
}
  8009e5:	5d                   	pop    %ebp
  8009e6:	c3                   	ret    

008009e7 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8009e7:	55                   	push   %ebp
  8009e8:	89 e5                	mov    %esp,%ebp
  8009ea:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8009ed:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009f0:	ba 00 00 00 00       	mov    $0x0,%edx
  8009f5:	eb 01                	jmp    8009f8 <strnlen+0x11>
		n++;
  8009f7:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009f8:	39 c2                	cmp    %eax,%edx
  8009fa:	74 08                	je     800a04 <strnlen+0x1d>
  8009fc:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800a00:	75 f5                	jne    8009f7 <strnlen+0x10>
  800a02:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800a04:	5d                   	pop    %ebp
  800a05:	c3                   	ret    

00800a06 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800a06:	55                   	push   %ebp
  800a07:	89 e5                	mov    %esp,%ebp
  800a09:	53                   	push   %ebx
  800a0a:	8b 45 08             	mov    0x8(%ebp),%eax
  800a0d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800a10:	89 c2                	mov    %eax,%edx
  800a12:	42                   	inc    %edx
  800a13:	41                   	inc    %ecx
  800a14:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800a17:	88 5a ff             	mov    %bl,-0x1(%edx)
  800a1a:	84 db                	test   %bl,%bl
  800a1c:	75 f4                	jne    800a12 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800a1e:	5b                   	pop    %ebx
  800a1f:	5d                   	pop    %ebp
  800a20:	c3                   	ret    

00800a21 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800a21:	55                   	push   %ebp
  800a22:	89 e5                	mov    %esp,%ebp
  800a24:	53                   	push   %ebx
  800a25:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800a28:	53                   	push   %ebx
  800a29:	e8 a3 ff ff ff       	call   8009d1 <strlen>
  800a2e:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800a31:	ff 75 0c             	pushl  0xc(%ebp)
  800a34:	01 d8                	add    %ebx,%eax
  800a36:	50                   	push   %eax
  800a37:	e8 ca ff ff ff       	call   800a06 <strcpy>
	return dst;
}
  800a3c:	89 d8                	mov    %ebx,%eax
  800a3e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800a41:	c9                   	leave  
  800a42:	c3                   	ret    

00800a43 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800a43:	55                   	push   %ebp
  800a44:	89 e5                	mov    %esp,%ebp
  800a46:	56                   	push   %esi
  800a47:	53                   	push   %ebx
  800a48:	8b 75 08             	mov    0x8(%ebp),%esi
  800a4b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a4e:	89 f3                	mov    %esi,%ebx
  800a50:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a53:	89 f2                	mov    %esi,%edx
  800a55:	eb 0c                	jmp    800a63 <strncpy+0x20>
		*dst++ = *src;
  800a57:	42                   	inc    %edx
  800a58:	8a 01                	mov    (%ecx),%al
  800a5a:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a5d:	80 39 01             	cmpb   $0x1,(%ecx)
  800a60:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a63:	39 da                	cmp    %ebx,%edx
  800a65:	75 f0                	jne    800a57 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a67:	89 f0                	mov    %esi,%eax
  800a69:	5b                   	pop    %ebx
  800a6a:	5e                   	pop    %esi
  800a6b:	5d                   	pop    %ebp
  800a6c:	c3                   	ret    

00800a6d <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a6d:	55                   	push   %ebp
  800a6e:	89 e5                	mov    %esp,%ebp
  800a70:	56                   	push   %esi
  800a71:	53                   	push   %ebx
  800a72:	8b 75 08             	mov    0x8(%ebp),%esi
  800a75:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a78:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a7b:	85 c0                	test   %eax,%eax
  800a7d:	74 1e                	je     800a9d <strlcpy+0x30>
  800a7f:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a83:	89 f2                	mov    %esi,%edx
  800a85:	eb 05                	jmp    800a8c <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a87:	42                   	inc    %edx
  800a88:	41                   	inc    %ecx
  800a89:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a8c:	39 c2                	cmp    %eax,%edx
  800a8e:	74 08                	je     800a98 <strlcpy+0x2b>
  800a90:	8a 19                	mov    (%ecx),%bl
  800a92:	84 db                	test   %bl,%bl
  800a94:	75 f1                	jne    800a87 <strlcpy+0x1a>
  800a96:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a98:	c6 00 00             	movb   $0x0,(%eax)
  800a9b:	eb 02                	jmp    800a9f <strlcpy+0x32>
  800a9d:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a9f:	29 f0                	sub    %esi,%eax
}
  800aa1:	5b                   	pop    %ebx
  800aa2:	5e                   	pop    %esi
  800aa3:	5d                   	pop    %ebp
  800aa4:	c3                   	ret    

00800aa5 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800aa5:	55                   	push   %ebp
  800aa6:	89 e5                	mov    %esp,%ebp
  800aa8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800aab:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800aae:	eb 02                	jmp    800ab2 <strcmp+0xd>
		p++, q++;
  800ab0:	41                   	inc    %ecx
  800ab1:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800ab2:	8a 01                	mov    (%ecx),%al
  800ab4:	84 c0                	test   %al,%al
  800ab6:	74 04                	je     800abc <strcmp+0x17>
  800ab8:	3a 02                	cmp    (%edx),%al
  800aba:	74 f4                	je     800ab0 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800abc:	0f b6 c0             	movzbl %al,%eax
  800abf:	0f b6 12             	movzbl (%edx),%edx
  800ac2:	29 d0                	sub    %edx,%eax
}
  800ac4:	5d                   	pop    %ebp
  800ac5:	c3                   	ret    

00800ac6 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800ac6:	55                   	push   %ebp
  800ac7:	89 e5                	mov    %esp,%ebp
  800ac9:	53                   	push   %ebx
  800aca:	8b 45 08             	mov    0x8(%ebp),%eax
  800acd:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ad0:	89 c3                	mov    %eax,%ebx
  800ad2:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800ad5:	eb 02                	jmp    800ad9 <strncmp+0x13>
		n--, p++, q++;
  800ad7:	40                   	inc    %eax
  800ad8:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800ad9:	39 d8                	cmp    %ebx,%eax
  800adb:	74 14                	je     800af1 <strncmp+0x2b>
  800add:	8a 08                	mov    (%eax),%cl
  800adf:	84 c9                	test   %cl,%cl
  800ae1:	74 04                	je     800ae7 <strncmp+0x21>
  800ae3:	3a 0a                	cmp    (%edx),%cl
  800ae5:	74 f0                	je     800ad7 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800ae7:	0f b6 00             	movzbl (%eax),%eax
  800aea:	0f b6 12             	movzbl (%edx),%edx
  800aed:	29 d0                	sub    %edx,%eax
  800aef:	eb 05                	jmp    800af6 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800af1:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800af6:	5b                   	pop    %ebx
  800af7:	5d                   	pop    %ebp
  800af8:	c3                   	ret    

00800af9 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800af9:	55                   	push   %ebp
  800afa:	89 e5                	mov    %esp,%ebp
  800afc:	8b 45 08             	mov    0x8(%ebp),%eax
  800aff:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800b02:	eb 05                	jmp    800b09 <strchr+0x10>
		if (*s == c)
  800b04:	38 ca                	cmp    %cl,%dl
  800b06:	74 0c                	je     800b14 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800b08:	40                   	inc    %eax
  800b09:	8a 10                	mov    (%eax),%dl
  800b0b:	84 d2                	test   %dl,%dl
  800b0d:	75 f5                	jne    800b04 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800b0f:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b14:	5d                   	pop    %ebp
  800b15:	c3                   	ret    

00800b16 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800b16:	55                   	push   %ebp
  800b17:	89 e5                	mov    %esp,%ebp
  800b19:	8b 45 08             	mov    0x8(%ebp),%eax
  800b1c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800b1f:	eb 05                	jmp    800b26 <strfind+0x10>
		if (*s == c)
  800b21:	38 ca                	cmp    %cl,%dl
  800b23:	74 07                	je     800b2c <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800b25:	40                   	inc    %eax
  800b26:	8a 10                	mov    (%eax),%dl
  800b28:	84 d2                	test   %dl,%dl
  800b2a:	75 f5                	jne    800b21 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800b2c:	5d                   	pop    %ebp
  800b2d:	c3                   	ret    

00800b2e <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800b2e:	55                   	push   %ebp
  800b2f:	89 e5                	mov    %esp,%ebp
  800b31:	57                   	push   %edi
  800b32:	56                   	push   %esi
  800b33:	53                   	push   %ebx
  800b34:	8b 7d 08             	mov    0x8(%ebp),%edi
  800b37:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800b3a:	85 c9                	test   %ecx,%ecx
  800b3c:	74 36                	je     800b74 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800b3e:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800b44:	75 28                	jne    800b6e <memset+0x40>
  800b46:	f6 c1 03             	test   $0x3,%cl
  800b49:	75 23                	jne    800b6e <memset+0x40>
		c &= 0xFF;
  800b4b:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800b4f:	89 d3                	mov    %edx,%ebx
  800b51:	c1 e3 08             	shl    $0x8,%ebx
  800b54:	89 d6                	mov    %edx,%esi
  800b56:	c1 e6 18             	shl    $0x18,%esi
  800b59:	89 d0                	mov    %edx,%eax
  800b5b:	c1 e0 10             	shl    $0x10,%eax
  800b5e:	09 f0                	or     %esi,%eax
  800b60:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b62:	89 d8                	mov    %ebx,%eax
  800b64:	09 d0                	or     %edx,%eax
  800b66:	c1 e9 02             	shr    $0x2,%ecx
  800b69:	fc                   	cld    
  800b6a:	f3 ab                	rep stos %eax,%es:(%edi)
  800b6c:	eb 06                	jmp    800b74 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b6e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b71:	fc                   	cld    
  800b72:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b74:	89 f8                	mov    %edi,%eax
  800b76:	5b                   	pop    %ebx
  800b77:	5e                   	pop    %esi
  800b78:	5f                   	pop    %edi
  800b79:	5d                   	pop    %ebp
  800b7a:	c3                   	ret    

00800b7b <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b7b:	55                   	push   %ebp
  800b7c:	89 e5                	mov    %esp,%ebp
  800b7e:	57                   	push   %edi
  800b7f:	56                   	push   %esi
  800b80:	8b 45 08             	mov    0x8(%ebp),%eax
  800b83:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b86:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b89:	39 c6                	cmp    %eax,%esi
  800b8b:	73 33                	jae    800bc0 <memmove+0x45>
  800b8d:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b90:	39 d0                	cmp    %edx,%eax
  800b92:	73 2c                	jae    800bc0 <memmove+0x45>
		s += n;
		d += n;
  800b94:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b97:	89 d6                	mov    %edx,%esi
  800b99:	09 fe                	or     %edi,%esi
  800b9b:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800ba1:	75 13                	jne    800bb6 <memmove+0x3b>
  800ba3:	f6 c1 03             	test   $0x3,%cl
  800ba6:	75 0e                	jne    800bb6 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800ba8:	83 ef 04             	sub    $0x4,%edi
  800bab:	8d 72 fc             	lea    -0x4(%edx),%esi
  800bae:	c1 e9 02             	shr    $0x2,%ecx
  800bb1:	fd                   	std    
  800bb2:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800bb4:	eb 07                	jmp    800bbd <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800bb6:	4f                   	dec    %edi
  800bb7:	8d 72 ff             	lea    -0x1(%edx),%esi
  800bba:	fd                   	std    
  800bbb:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800bbd:	fc                   	cld    
  800bbe:	eb 1d                	jmp    800bdd <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800bc0:	89 f2                	mov    %esi,%edx
  800bc2:	09 c2                	or     %eax,%edx
  800bc4:	f6 c2 03             	test   $0x3,%dl
  800bc7:	75 0f                	jne    800bd8 <memmove+0x5d>
  800bc9:	f6 c1 03             	test   $0x3,%cl
  800bcc:	75 0a                	jne    800bd8 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800bce:	c1 e9 02             	shr    $0x2,%ecx
  800bd1:	89 c7                	mov    %eax,%edi
  800bd3:	fc                   	cld    
  800bd4:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800bd6:	eb 05                	jmp    800bdd <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800bd8:	89 c7                	mov    %eax,%edi
  800bda:	fc                   	cld    
  800bdb:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800bdd:	5e                   	pop    %esi
  800bde:	5f                   	pop    %edi
  800bdf:	5d                   	pop    %ebp
  800be0:	c3                   	ret    

00800be1 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800be1:	55                   	push   %ebp
  800be2:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800be4:	ff 75 10             	pushl  0x10(%ebp)
  800be7:	ff 75 0c             	pushl  0xc(%ebp)
  800bea:	ff 75 08             	pushl  0x8(%ebp)
  800bed:	e8 89 ff ff ff       	call   800b7b <memmove>
}
  800bf2:	c9                   	leave  
  800bf3:	c3                   	ret    

00800bf4 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800bf4:	55                   	push   %ebp
  800bf5:	89 e5                	mov    %esp,%ebp
  800bf7:	56                   	push   %esi
  800bf8:	53                   	push   %ebx
  800bf9:	8b 45 08             	mov    0x8(%ebp),%eax
  800bfc:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bff:	89 c6                	mov    %eax,%esi
  800c01:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800c04:	eb 14                	jmp    800c1a <memcmp+0x26>
		if (*s1 != *s2)
  800c06:	8a 08                	mov    (%eax),%cl
  800c08:	8a 1a                	mov    (%edx),%bl
  800c0a:	38 d9                	cmp    %bl,%cl
  800c0c:	74 0a                	je     800c18 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800c0e:	0f b6 c1             	movzbl %cl,%eax
  800c11:	0f b6 db             	movzbl %bl,%ebx
  800c14:	29 d8                	sub    %ebx,%eax
  800c16:	eb 0b                	jmp    800c23 <memcmp+0x2f>
		s1++, s2++;
  800c18:	40                   	inc    %eax
  800c19:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800c1a:	39 f0                	cmp    %esi,%eax
  800c1c:	75 e8                	jne    800c06 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800c1e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c23:	5b                   	pop    %ebx
  800c24:	5e                   	pop    %esi
  800c25:	5d                   	pop    %ebp
  800c26:	c3                   	ret    

00800c27 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800c27:	55                   	push   %ebp
  800c28:	89 e5                	mov    %esp,%ebp
  800c2a:	53                   	push   %ebx
  800c2b:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800c2e:	89 c1                	mov    %eax,%ecx
  800c30:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800c33:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c37:	eb 08                	jmp    800c41 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800c39:	0f b6 10             	movzbl (%eax),%edx
  800c3c:	39 da                	cmp    %ebx,%edx
  800c3e:	74 05                	je     800c45 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c40:	40                   	inc    %eax
  800c41:	39 c8                	cmp    %ecx,%eax
  800c43:	72 f4                	jb     800c39 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800c45:	5b                   	pop    %ebx
  800c46:	5d                   	pop    %ebp
  800c47:	c3                   	ret    

00800c48 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800c48:	55                   	push   %ebp
  800c49:	89 e5                	mov    %esp,%ebp
  800c4b:	57                   	push   %edi
  800c4c:	56                   	push   %esi
  800c4d:	53                   	push   %ebx
  800c4e:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c51:	eb 01                	jmp    800c54 <strtol+0xc>
		s++;
  800c53:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c54:	8a 01                	mov    (%ecx),%al
  800c56:	3c 20                	cmp    $0x20,%al
  800c58:	74 f9                	je     800c53 <strtol+0xb>
  800c5a:	3c 09                	cmp    $0x9,%al
  800c5c:	74 f5                	je     800c53 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c5e:	3c 2b                	cmp    $0x2b,%al
  800c60:	75 08                	jne    800c6a <strtol+0x22>
		s++;
  800c62:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c63:	bf 00 00 00 00       	mov    $0x0,%edi
  800c68:	eb 11                	jmp    800c7b <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c6a:	3c 2d                	cmp    $0x2d,%al
  800c6c:	75 08                	jne    800c76 <strtol+0x2e>
		s++, neg = 1;
  800c6e:	41                   	inc    %ecx
  800c6f:	bf 01 00 00 00       	mov    $0x1,%edi
  800c74:	eb 05                	jmp    800c7b <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c76:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c7b:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c7f:	0f 84 87 00 00 00    	je     800d0c <strtol+0xc4>
  800c85:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c89:	75 27                	jne    800cb2 <strtol+0x6a>
  800c8b:	80 39 30             	cmpb   $0x30,(%ecx)
  800c8e:	75 22                	jne    800cb2 <strtol+0x6a>
  800c90:	e9 88 00 00 00       	jmp    800d1d <strtol+0xd5>
		s += 2, base = 16;
  800c95:	83 c1 02             	add    $0x2,%ecx
  800c98:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c9f:	eb 11                	jmp    800cb2 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800ca1:	41                   	inc    %ecx
  800ca2:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800ca9:	eb 07                	jmp    800cb2 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800cab:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800cb2:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800cb7:	8a 11                	mov    (%ecx),%dl
  800cb9:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800cbc:	80 fb 09             	cmp    $0x9,%bl
  800cbf:	77 08                	ja     800cc9 <strtol+0x81>
			dig = *s - '0';
  800cc1:	0f be d2             	movsbl %dl,%edx
  800cc4:	83 ea 30             	sub    $0x30,%edx
  800cc7:	eb 22                	jmp    800ceb <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800cc9:	8d 72 9f             	lea    -0x61(%edx),%esi
  800ccc:	89 f3                	mov    %esi,%ebx
  800cce:	80 fb 19             	cmp    $0x19,%bl
  800cd1:	77 08                	ja     800cdb <strtol+0x93>
			dig = *s - 'a' + 10;
  800cd3:	0f be d2             	movsbl %dl,%edx
  800cd6:	83 ea 57             	sub    $0x57,%edx
  800cd9:	eb 10                	jmp    800ceb <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800cdb:	8d 72 bf             	lea    -0x41(%edx),%esi
  800cde:	89 f3                	mov    %esi,%ebx
  800ce0:	80 fb 19             	cmp    $0x19,%bl
  800ce3:	77 14                	ja     800cf9 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800ce5:	0f be d2             	movsbl %dl,%edx
  800ce8:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800ceb:	3b 55 10             	cmp    0x10(%ebp),%edx
  800cee:	7d 09                	jge    800cf9 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800cf0:	41                   	inc    %ecx
  800cf1:	0f af 45 10          	imul   0x10(%ebp),%eax
  800cf5:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800cf7:	eb be                	jmp    800cb7 <strtol+0x6f>

	if (endptr)
  800cf9:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800cfd:	74 05                	je     800d04 <strtol+0xbc>
		*endptr = (char *) s;
  800cff:	8b 75 0c             	mov    0xc(%ebp),%esi
  800d02:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800d04:	85 ff                	test   %edi,%edi
  800d06:	74 21                	je     800d29 <strtol+0xe1>
  800d08:	f7 d8                	neg    %eax
  800d0a:	eb 1d                	jmp    800d29 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800d0c:	80 39 30             	cmpb   $0x30,(%ecx)
  800d0f:	75 9a                	jne    800cab <strtol+0x63>
  800d11:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800d15:	0f 84 7a ff ff ff    	je     800c95 <strtol+0x4d>
  800d1b:	eb 84                	jmp    800ca1 <strtol+0x59>
  800d1d:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800d21:	0f 84 6e ff ff ff    	je     800c95 <strtol+0x4d>
  800d27:	eb 89                	jmp    800cb2 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800d29:	5b                   	pop    %ebx
  800d2a:	5e                   	pop    %esi
  800d2b:	5f                   	pop    %edi
  800d2c:	5d                   	pop    %ebp
  800d2d:	c3                   	ret    

00800d2e <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800d2e:	55                   	push   %ebp
  800d2f:	89 e5                	mov    %esp,%ebp
  800d31:	57                   	push   %edi
  800d32:	56                   	push   %esi
  800d33:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d34:	b8 00 00 00 00       	mov    $0x0,%eax
  800d39:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d3c:	8b 55 08             	mov    0x8(%ebp),%edx
  800d3f:	89 c3                	mov    %eax,%ebx
  800d41:	89 c7                	mov    %eax,%edi
  800d43:	89 c6                	mov    %eax,%esi
  800d45:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800d47:	5b                   	pop    %ebx
  800d48:	5e                   	pop    %esi
  800d49:	5f                   	pop    %edi
  800d4a:	5d                   	pop    %ebp
  800d4b:	c3                   	ret    

00800d4c <sys_cgetc>:

int
sys_cgetc(void)
{
  800d4c:	55                   	push   %ebp
  800d4d:	89 e5                	mov    %esp,%ebp
  800d4f:	57                   	push   %edi
  800d50:	56                   	push   %esi
  800d51:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d52:	ba 00 00 00 00       	mov    $0x0,%edx
  800d57:	b8 01 00 00 00       	mov    $0x1,%eax
  800d5c:	89 d1                	mov    %edx,%ecx
  800d5e:	89 d3                	mov    %edx,%ebx
  800d60:	89 d7                	mov    %edx,%edi
  800d62:	89 d6                	mov    %edx,%esi
  800d64:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800d66:	5b                   	pop    %ebx
  800d67:	5e                   	pop    %esi
  800d68:	5f                   	pop    %edi
  800d69:	5d                   	pop    %ebp
  800d6a:	c3                   	ret    

00800d6b <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800d6b:	55                   	push   %ebp
  800d6c:	89 e5                	mov    %esp,%ebp
  800d6e:	57                   	push   %edi
  800d6f:	56                   	push   %esi
  800d70:	53                   	push   %ebx
  800d71:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d74:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d79:	b8 03 00 00 00       	mov    $0x3,%eax
  800d7e:	8b 55 08             	mov    0x8(%ebp),%edx
  800d81:	89 cb                	mov    %ecx,%ebx
  800d83:	89 cf                	mov    %ecx,%edi
  800d85:	89 ce                	mov    %ecx,%esi
  800d87:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d89:	85 c0                	test   %eax,%eax
  800d8b:	7e 17                	jle    800da4 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d8d:	83 ec 0c             	sub    $0xc,%esp
  800d90:	50                   	push   %eax
  800d91:	6a 03                	push   $0x3
  800d93:	68 8f 23 80 00       	push   $0x80238f
  800d98:	6a 23                	push   $0x23
  800d9a:	68 ac 23 80 00       	push   $0x8023ac
  800d9f:	e8 20 f5 ff ff       	call   8002c4 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800da4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800da7:	5b                   	pop    %ebx
  800da8:	5e                   	pop    %esi
  800da9:	5f                   	pop    %edi
  800daa:	5d                   	pop    %ebp
  800dab:	c3                   	ret    

00800dac <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800dac:	55                   	push   %ebp
  800dad:	89 e5                	mov    %esp,%ebp
  800daf:	57                   	push   %edi
  800db0:	56                   	push   %esi
  800db1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800db2:	ba 00 00 00 00       	mov    $0x0,%edx
  800db7:	b8 02 00 00 00       	mov    $0x2,%eax
  800dbc:	89 d1                	mov    %edx,%ecx
  800dbe:	89 d3                	mov    %edx,%ebx
  800dc0:	89 d7                	mov    %edx,%edi
  800dc2:	89 d6                	mov    %edx,%esi
  800dc4:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800dc6:	5b                   	pop    %ebx
  800dc7:	5e                   	pop    %esi
  800dc8:	5f                   	pop    %edi
  800dc9:	5d                   	pop    %ebp
  800dca:	c3                   	ret    

00800dcb <sys_yield>:

void
sys_yield(void)
{
  800dcb:	55                   	push   %ebp
  800dcc:	89 e5                	mov    %esp,%ebp
  800dce:	57                   	push   %edi
  800dcf:	56                   	push   %esi
  800dd0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dd1:	ba 00 00 00 00       	mov    $0x0,%edx
  800dd6:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ddb:	89 d1                	mov    %edx,%ecx
  800ddd:	89 d3                	mov    %edx,%ebx
  800ddf:	89 d7                	mov    %edx,%edi
  800de1:	89 d6                	mov    %edx,%esi
  800de3:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800de5:	5b                   	pop    %ebx
  800de6:	5e                   	pop    %esi
  800de7:	5f                   	pop    %edi
  800de8:	5d                   	pop    %ebp
  800de9:	c3                   	ret    

00800dea <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800dea:	55                   	push   %ebp
  800deb:	89 e5                	mov    %esp,%ebp
  800ded:	57                   	push   %edi
  800dee:	56                   	push   %esi
  800def:	53                   	push   %ebx
  800df0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800df3:	be 00 00 00 00       	mov    $0x0,%esi
  800df8:	b8 04 00 00 00       	mov    $0x4,%eax
  800dfd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e00:	8b 55 08             	mov    0x8(%ebp),%edx
  800e03:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e06:	89 f7                	mov    %esi,%edi
  800e08:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e0a:	85 c0                	test   %eax,%eax
  800e0c:	7e 17                	jle    800e25 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e0e:	83 ec 0c             	sub    $0xc,%esp
  800e11:	50                   	push   %eax
  800e12:	6a 04                	push   $0x4
  800e14:	68 8f 23 80 00       	push   $0x80238f
  800e19:	6a 23                	push   $0x23
  800e1b:	68 ac 23 80 00       	push   $0x8023ac
  800e20:	e8 9f f4 ff ff       	call   8002c4 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800e25:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e28:	5b                   	pop    %ebx
  800e29:	5e                   	pop    %esi
  800e2a:	5f                   	pop    %edi
  800e2b:	5d                   	pop    %ebp
  800e2c:	c3                   	ret    

00800e2d <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800e2d:	55                   	push   %ebp
  800e2e:	89 e5                	mov    %esp,%ebp
  800e30:	57                   	push   %edi
  800e31:	56                   	push   %esi
  800e32:	53                   	push   %ebx
  800e33:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e36:	b8 05 00 00 00       	mov    $0x5,%eax
  800e3b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e3e:	8b 55 08             	mov    0x8(%ebp),%edx
  800e41:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e44:	8b 7d 14             	mov    0x14(%ebp),%edi
  800e47:	8b 75 18             	mov    0x18(%ebp),%esi
  800e4a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e4c:	85 c0                	test   %eax,%eax
  800e4e:	7e 17                	jle    800e67 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e50:	83 ec 0c             	sub    $0xc,%esp
  800e53:	50                   	push   %eax
  800e54:	6a 05                	push   $0x5
  800e56:	68 8f 23 80 00       	push   $0x80238f
  800e5b:	6a 23                	push   $0x23
  800e5d:	68 ac 23 80 00       	push   $0x8023ac
  800e62:	e8 5d f4 ff ff       	call   8002c4 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800e67:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e6a:	5b                   	pop    %ebx
  800e6b:	5e                   	pop    %esi
  800e6c:	5f                   	pop    %edi
  800e6d:	5d                   	pop    %ebp
  800e6e:	c3                   	ret    

00800e6f <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800e6f:	55                   	push   %ebp
  800e70:	89 e5                	mov    %esp,%ebp
  800e72:	57                   	push   %edi
  800e73:	56                   	push   %esi
  800e74:	53                   	push   %ebx
  800e75:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e78:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e7d:	b8 06 00 00 00       	mov    $0x6,%eax
  800e82:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e85:	8b 55 08             	mov    0x8(%ebp),%edx
  800e88:	89 df                	mov    %ebx,%edi
  800e8a:	89 de                	mov    %ebx,%esi
  800e8c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e8e:	85 c0                	test   %eax,%eax
  800e90:	7e 17                	jle    800ea9 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e92:	83 ec 0c             	sub    $0xc,%esp
  800e95:	50                   	push   %eax
  800e96:	6a 06                	push   $0x6
  800e98:	68 8f 23 80 00       	push   $0x80238f
  800e9d:	6a 23                	push   $0x23
  800e9f:	68 ac 23 80 00       	push   $0x8023ac
  800ea4:	e8 1b f4 ff ff       	call   8002c4 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800ea9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800eac:	5b                   	pop    %ebx
  800ead:	5e                   	pop    %esi
  800eae:	5f                   	pop    %edi
  800eaf:	5d                   	pop    %ebp
  800eb0:	c3                   	ret    

00800eb1 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800eb1:	55                   	push   %ebp
  800eb2:	89 e5                	mov    %esp,%ebp
  800eb4:	57                   	push   %edi
  800eb5:	56                   	push   %esi
  800eb6:	53                   	push   %ebx
  800eb7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800eba:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ebf:	b8 08 00 00 00       	mov    $0x8,%eax
  800ec4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ec7:	8b 55 08             	mov    0x8(%ebp),%edx
  800eca:	89 df                	mov    %ebx,%edi
  800ecc:	89 de                	mov    %ebx,%esi
  800ece:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ed0:	85 c0                	test   %eax,%eax
  800ed2:	7e 17                	jle    800eeb <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ed4:	83 ec 0c             	sub    $0xc,%esp
  800ed7:	50                   	push   %eax
  800ed8:	6a 08                	push   $0x8
  800eda:	68 8f 23 80 00       	push   $0x80238f
  800edf:	6a 23                	push   $0x23
  800ee1:	68 ac 23 80 00       	push   $0x8023ac
  800ee6:	e8 d9 f3 ff ff       	call   8002c4 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800eeb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800eee:	5b                   	pop    %ebx
  800eef:	5e                   	pop    %esi
  800ef0:	5f                   	pop    %edi
  800ef1:	5d                   	pop    %ebp
  800ef2:	c3                   	ret    

00800ef3 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800ef3:	55                   	push   %ebp
  800ef4:	89 e5                	mov    %esp,%ebp
  800ef6:	57                   	push   %edi
  800ef7:	56                   	push   %esi
  800ef8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ef9:	ba 00 00 00 00       	mov    $0x0,%edx
  800efe:	b8 0c 00 00 00       	mov    $0xc,%eax
  800f03:	89 d1                	mov    %edx,%ecx
  800f05:	89 d3                	mov    %edx,%ebx
  800f07:	89 d7                	mov    %edx,%edi
  800f09:	89 d6                	mov    %edx,%esi
  800f0b:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800f0d:	5b                   	pop    %ebx
  800f0e:	5e                   	pop    %esi
  800f0f:	5f                   	pop    %edi
  800f10:	5d                   	pop    %ebp
  800f11:	c3                   	ret    

00800f12 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800f12:	55                   	push   %ebp
  800f13:	89 e5                	mov    %esp,%ebp
  800f15:	57                   	push   %edi
  800f16:	56                   	push   %esi
  800f17:	53                   	push   %ebx
  800f18:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f1b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800f20:	b8 09 00 00 00       	mov    $0x9,%eax
  800f25:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f28:	8b 55 08             	mov    0x8(%ebp),%edx
  800f2b:	89 df                	mov    %ebx,%edi
  800f2d:	89 de                	mov    %ebx,%esi
  800f2f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f31:	85 c0                	test   %eax,%eax
  800f33:	7e 17                	jle    800f4c <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f35:	83 ec 0c             	sub    $0xc,%esp
  800f38:	50                   	push   %eax
  800f39:	6a 09                	push   $0x9
  800f3b:	68 8f 23 80 00       	push   $0x80238f
  800f40:	6a 23                	push   $0x23
  800f42:	68 ac 23 80 00       	push   $0x8023ac
  800f47:	e8 78 f3 ff ff       	call   8002c4 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800f4c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f4f:	5b                   	pop    %ebx
  800f50:	5e                   	pop    %esi
  800f51:	5f                   	pop    %edi
  800f52:	5d                   	pop    %ebp
  800f53:	c3                   	ret    

00800f54 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800f54:	55                   	push   %ebp
  800f55:	89 e5                	mov    %esp,%ebp
  800f57:	57                   	push   %edi
  800f58:	56                   	push   %esi
  800f59:	53                   	push   %ebx
  800f5a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f5d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800f62:	b8 0a 00 00 00       	mov    $0xa,%eax
  800f67:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f6a:	8b 55 08             	mov    0x8(%ebp),%edx
  800f6d:	89 df                	mov    %ebx,%edi
  800f6f:	89 de                	mov    %ebx,%esi
  800f71:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f73:	85 c0                	test   %eax,%eax
  800f75:	7e 17                	jle    800f8e <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f77:	83 ec 0c             	sub    $0xc,%esp
  800f7a:	50                   	push   %eax
  800f7b:	6a 0a                	push   $0xa
  800f7d:	68 8f 23 80 00       	push   $0x80238f
  800f82:	6a 23                	push   $0x23
  800f84:	68 ac 23 80 00       	push   $0x8023ac
  800f89:	e8 36 f3 ff ff       	call   8002c4 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800f8e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f91:	5b                   	pop    %ebx
  800f92:	5e                   	pop    %esi
  800f93:	5f                   	pop    %edi
  800f94:	5d                   	pop    %ebp
  800f95:	c3                   	ret    

00800f96 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800f96:	55                   	push   %ebp
  800f97:	89 e5                	mov    %esp,%ebp
  800f99:	57                   	push   %edi
  800f9a:	56                   	push   %esi
  800f9b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f9c:	be 00 00 00 00       	mov    $0x0,%esi
  800fa1:	b8 0d 00 00 00       	mov    $0xd,%eax
  800fa6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800fa9:	8b 55 08             	mov    0x8(%ebp),%edx
  800fac:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800faf:	8b 7d 14             	mov    0x14(%ebp),%edi
  800fb2:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800fb4:	5b                   	pop    %ebx
  800fb5:	5e                   	pop    %esi
  800fb6:	5f                   	pop    %edi
  800fb7:	5d                   	pop    %ebp
  800fb8:	c3                   	ret    

00800fb9 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800fb9:	55                   	push   %ebp
  800fba:	89 e5                	mov    %esp,%ebp
  800fbc:	57                   	push   %edi
  800fbd:	56                   	push   %esi
  800fbe:	53                   	push   %ebx
  800fbf:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fc2:	b9 00 00 00 00       	mov    $0x0,%ecx
  800fc7:	b8 0e 00 00 00       	mov    $0xe,%eax
  800fcc:	8b 55 08             	mov    0x8(%ebp),%edx
  800fcf:	89 cb                	mov    %ecx,%ebx
  800fd1:	89 cf                	mov    %ecx,%edi
  800fd3:	89 ce                	mov    %ecx,%esi
  800fd5:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800fd7:	85 c0                	test   %eax,%eax
  800fd9:	7e 17                	jle    800ff2 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800fdb:	83 ec 0c             	sub    $0xc,%esp
  800fde:	50                   	push   %eax
  800fdf:	6a 0e                	push   $0xe
  800fe1:	68 8f 23 80 00       	push   $0x80238f
  800fe6:	6a 23                	push   $0x23
  800fe8:	68 ac 23 80 00       	push   $0x8023ac
  800fed:	e8 d2 f2 ff ff       	call   8002c4 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800ff2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ff5:	5b                   	pop    %ebx
  800ff6:	5e                   	pop    %esi
  800ff7:	5f                   	pop    %edi
  800ff8:	5d                   	pop    %ebp
  800ff9:	c3                   	ret    

00800ffa <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800ffa:	55                   	push   %ebp
  800ffb:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  800ffd:	8b 45 08             	mov    0x8(%ebp),%eax
  801000:	05 00 00 00 30       	add    $0x30000000,%eax
  801005:	c1 e8 0c             	shr    $0xc,%eax
}
  801008:	5d                   	pop    %ebp
  801009:	c3                   	ret    

0080100a <fd2data>:

char*
fd2data(struct Fd *fd)
{
  80100a:	55                   	push   %ebp
  80100b:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  80100d:	8b 45 08             	mov    0x8(%ebp),%eax
  801010:	05 00 00 00 30       	add    $0x30000000,%eax
  801015:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  80101a:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  80101f:	5d                   	pop    %ebp
  801020:	c3                   	ret    

00801021 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  801021:	55                   	push   %ebp
  801022:	89 e5                	mov    %esp,%ebp
  801024:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801027:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  80102c:	89 c2                	mov    %eax,%edx
  80102e:	c1 ea 16             	shr    $0x16,%edx
  801031:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801038:	f6 c2 01             	test   $0x1,%dl
  80103b:	74 11                	je     80104e <fd_alloc+0x2d>
  80103d:	89 c2                	mov    %eax,%edx
  80103f:	c1 ea 0c             	shr    $0xc,%edx
  801042:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801049:	f6 c2 01             	test   $0x1,%dl
  80104c:	75 09                	jne    801057 <fd_alloc+0x36>
			*fd_store = fd;
  80104e:	89 01                	mov    %eax,(%ecx)
			return 0;
  801050:	b8 00 00 00 00       	mov    $0x0,%eax
  801055:	eb 17                	jmp    80106e <fd_alloc+0x4d>
  801057:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  80105c:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801061:	75 c9                	jne    80102c <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801063:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801069:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  80106e:	5d                   	pop    %ebp
  80106f:	c3                   	ret    

00801070 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  801070:	55                   	push   %ebp
  801071:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  801073:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801077:	77 39                	ja     8010b2 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801079:	8b 45 08             	mov    0x8(%ebp),%eax
  80107c:	c1 e0 0c             	shl    $0xc,%eax
  80107f:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  801084:	89 c2                	mov    %eax,%edx
  801086:	c1 ea 16             	shr    $0x16,%edx
  801089:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801090:	f6 c2 01             	test   $0x1,%dl
  801093:	74 24                	je     8010b9 <fd_lookup+0x49>
  801095:	89 c2                	mov    %eax,%edx
  801097:	c1 ea 0c             	shr    $0xc,%edx
  80109a:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8010a1:	f6 c2 01             	test   $0x1,%dl
  8010a4:	74 1a                	je     8010c0 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  8010a6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8010a9:	89 02                	mov    %eax,(%edx)
	return 0;
  8010ab:	b8 00 00 00 00       	mov    $0x0,%eax
  8010b0:	eb 13                	jmp    8010c5 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8010b2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8010b7:	eb 0c                	jmp    8010c5 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8010b9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8010be:	eb 05                	jmp    8010c5 <fd_lookup+0x55>
  8010c0:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8010c5:	5d                   	pop    %ebp
  8010c6:	c3                   	ret    

008010c7 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8010c7:	55                   	push   %ebp
  8010c8:	89 e5                	mov    %esp,%ebp
  8010ca:	83 ec 08             	sub    $0x8,%esp
  8010cd:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8010d0:	ba 3c 24 80 00       	mov    $0x80243c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  8010d5:	eb 13                	jmp    8010ea <dev_lookup+0x23>
  8010d7:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  8010da:	39 08                	cmp    %ecx,(%eax)
  8010dc:	75 0c                	jne    8010ea <dev_lookup+0x23>
			*dev = devtab[i];
  8010de:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8010e1:	89 01                	mov    %eax,(%ecx)
			return 0;
  8010e3:	b8 00 00 00 00       	mov    $0x0,%eax
  8010e8:	eb 2e                	jmp    801118 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  8010ea:	8b 02                	mov    (%edx),%eax
  8010ec:	85 c0                	test   %eax,%eax
  8010ee:	75 e7                	jne    8010d7 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  8010f0:	a1 04 44 80 00       	mov    0x804404,%eax
  8010f5:	8b 40 48             	mov    0x48(%eax),%eax
  8010f8:	83 ec 04             	sub    $0x4,%esp
  8010fb:	51                   	push   %ecx
  8010fc:	50                   	push   %eax
  8010fd:	68 bc 23 80 00       	push   $0x8023bc
  801102:	e8 95 f2 ff ff       	call   80039c <cprintf>
	*dev = 0;
  801107:	8b 45 0c             	mov    0xc(%ebp),%eax
  80110a:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  801110:	83 c4 10             	add    $0x10,%esp
  801113:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801118:	c9                   	leave  
  801119:	c3                   	ret    

0080111a <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  80111a:	55                   	push   %ebp
  80111b:	89 e5                	mov    %esp,%ebp
  80111d:	56                   	push   %esi
  80111e:	53                   	push   %ebx
  80111f:	83 ec 10             	sub    $0x10,%esp
  801122:	8b 75 08             	mov    0x8(%ebp),%esi
  801125:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  801128:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80112b:	50                   	push   %eax
  80112c:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  801132:	c1 e8 0c             	shr    $0xc,%eax
  801135:	50                   	push   %eax
  801136:	e8 35 ff ff ff       	call   801070 <fd_lookup>
  80113b:	83 c4 08             	add    $0x8,%esp
  80113e:	85 c0                	test   %eax,%eax
  801140:	78 05                	js     801147 <fd_close+0x2d>
	    || fd != fd2)
  801142:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  801145:	74 06                	je     80114d <fd_close+0x33>
		return (must_exist ? r : 0);
  801147:	84 db                	test   %bl,%bl
  801149:	74 47                	je     801192 <fd_close+0x78>
  80114b:	eb 4a                	jmp    801197 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  80114d:	83 ec 08             	sub    $0x8,%esp
  801150:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801153:	50                   	push   %eax
  801154:	ff 36                	pushl  (%esi)
  801156:	e8 6c ff ff ff       	call   8010c7 <dev_lookup>
  80115b:	89 c3                	mov    %eax,%ebx
  80115d:	83 c4 10             	add    $0x10,%esp
  801160:	85 c0                	test   %eax,%eax
  801162:	78 1c                	js     801180 <fd_close+0x66>
		if (dev->dev_close)
  801164:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801167:	8b 40 10             	mov    0x10(%eax),%eax
  80116a:	85 c0                	test   %eax,%eax
  80116c:	74 0d                	je     80117b <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  80116e:	83 ec 0c             	sub    $0xc,%esp
  801171:	56                   	push   %esi
  801172:	ff d0                	call   *%eax
  801174:	89 c3                	mov    %eax,%ebx
  801176:	83 c4 10             	add    $0x10,%esp
  801179:	eb 05                	jmp    801180 <fd_close+0x66>
		else
			r = 0;
  80117b:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  801180:	83 ec 08             	sub    $0x8,%esp
  801183:	56                   	push   %esi
  801184:	6a 00                	push   $0x0
  801186:	e8 e4 fc ff ff       	call   800e6f <sys_page_unmap>
	return r;
  80118b:	83 c4 10             	add    $0x10,%esp
  80118e:	89 d8                	mov    %ebx,%eax
  801190:	eb 05                	jmp    801197 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  801192:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801197:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80119a:	5b                   	pop    %ebx
  80119b:	5e                   	pop    %esi
  80119c:	5d                   	pop    %ebp
  80119d:	c3                   	ret    

0080119e <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  80119e:	55                   	push   %ebp
  80119f:	89 e5                	mov    %esp,%ebp
  8011a1:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8011a4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8011a7:	50                   	push   %eax
  8011a8:	ff 75 08             	pushl  0x8(%ebp)
  8011ab:	e8 c0 fe ff ff       	call   801070 <fd_lookup>
  8011b0:	83 c4 08             	add    $0x8,%esp
  8011b3:	85 c0                	test   %eax,%eax
  8011b5:	78 10                	js     8011c7 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8011b7:	83 ec 08             	sub    $0x8,%esp
  8011ba:	6a 01                	push   $0x1
  8011bc:	ff 75 f4             	pushl  -0xc(%ebp)
  8011bf:	e8 56 ff ff ff       	call   80111a <fd_close>
  8011c4:	83 c4 10             	add    $0x10,%esp
}
  8011c7:	c9                   	leave  
  8011c8:	c3                   	ret    

008011c9 <close_all>:

void
close_all(void)
{
  8011c9:	55                   	push   %ebp
  8011ca:	89 e5                	mov    %esp,%ebp
  8011cc:	53                   	push   %ebx
  8011cd:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  8011d0:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  8011d5:	83 ec 0c             	sub    $0xc,%esp
  8011d8:	53                   	push   %ebx
  8011d9:	e8 c0 ff ff ff       	call   80119e <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  8011de:	43                   	inc    %ebx
  8011df:	83 c4 10             	add    $0x10,%esp
  8011e2:	83 fb 20             	cmp    $0x20,%ebx
  8011e5:	75 ee                	jne    8011d5 <close_all+0xc>
		close(i);
}
  8011e7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8011ea:	c9                   	leave  
  8011eb:	c3                   	ret    

008011ec <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  8011ec:	55                   	push   %ebp
  8011ed:	89 e5                	mov    %esp,%ebp
  8011ef:	57                   	push   %edi
  8011f0:	56                   	push   %esi
  8011f1:	53                   	push   %ebx
  8011f2:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  8011f5:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8011f8:	50                   	push   %eax
  8011f9:	ff 75 08             	pushl  0x8(%ebp)
  8011fc:	e8 6f fe ff ff       	call   801070 <fd_lookup>
  801201:	83 c4 08             	add    $0x8,%esp
  801204:	85 c0                	test   %eax,%eax
  801206:	0f 88 c2 00 00 00    	js     8012ce <dup+0xe2>
		return r;
	close(newfdnum);
  80120c:	83 ec 0c             	sub    $0xc,%esp
  80120f:	ff 75 0c             	pushl  0xc(%ebp)
  801212:	e8 87 ff ff ff       	call   80119e <close>

	newfd = INDEX2FD(newfdnum);
  801217:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80121a:	c1 e3 0c             	shl    $0xc,%ebx
  80121d:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801223:	83 c4 04             	add    $0x4,%esp
  801226:	ff 75 e4             	pushl  -0x1c(%ebp)
  801229:	e8 dc fd ff ff       	call   80100a <fd2data>
  80122e:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  801230:	89 1c 24             	mov    %ebx,(%esp)
  801233:	e8 d2 fd ff ff       	call   80100a <fd2data>
  801238:	83 c4 10             	add    $0x10,%esp
  80123b:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  80123d:	89 f0                	mov    %esi,%eax
  80123f:	c1 e8 16             	shr    $0x16,%eax
  801242:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801249:	a8 01                	test   $0x1,%al
  80124b:	74 35                	je     801282 <dup+0x96>
  80124d:	89 f0                	mov    %esi,%eax
  80124f:	c1 e8 0c             	shr    $0xc,%eax
  801252:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801259:	f6 c2 01             	test   $0x1,%dl
  80125c:	74 24                	je     801282 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  80125e:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801265:	83 ec 0c             	sub    $0xc,%esp
  801268:	25 07 0e 00 00       	and    $0xe07,%eax
  80126d:	50                   	push   %eax
  80126e:	57                   	push   %edi
  80126f:	6a 00                	push   $0x0
  801271:	56                   	push   %esi
  801272:	6a 00                	push   $0x0
  801274:	e8 b4 fb ff ff       	call   800e2d <sys_page_map>
  801279:	89 c6                	mov    %eax,%esi
  80127b:	83 c4 20             	add    $0x20,%esp
  80127e:	85 c0                	test   %eax,%eax
  801280:	78 2c                	js     8012ae <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  801282:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801285:	89 d0                	mov    %edx,%eax
  801287:	c1 e8 0c             	shr    $0xc,%eax
  80128a:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801291:	83 ec 0c             	sub    $0xc,%esp
  801294:	25 07 0e 00 00       	and    $0xe07,%eax
  801299:	50                   	push   %eax
  80129a:	53                   	push   %ebx
  80129b:	6a 00                	push   $0x0
  80129d:	52                   	push   %edx
  80129e:	6a 00                	push   $0x0
  8012a0:	e8 88 fb ff ff       	call   800e2d <sys_page_map>
  8012a5:	89 c6                	mov    %eax,%esi
  8012a7:	83 c4 20             	add    $0x20,%esp
  8012aa:	85 c0                	test   %eax,%eax
  8012ac:	79 1d                	jns    8012cb <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8012ae:	83 ec 08             	sub    $0x8,%esp
  8012b1:	53                   	push   %ebx
  8012b2:	6a 00                	push   $0x0
  8012b4:	e8 b6 fb ff ff       	call   800e6f <sys_page_unmap>
	sys_page_unmap(0, nva);
  8012b9:	83 c4 08             	add    $0x8,%esp
  8012bc:	57                   	push   %edi
  8012bd:	6a 00                	push   $0x0
  8012bf:	e8 ab fb ff ff       	call   800e6f <sys_page_unmap>
	return r;
  8012c4:	83 c4 10             	add    $0x10,%esp
  8012c7:	89 f0                	mov    %esi,%eax
  8012c9:	eb 03                	jmp    8012ce <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8012cb:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8012ce:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8012d1:	5b                   	pop    %ebx
  8012d2:	5e                   	pop    %esi
  8012d3:	5f                   	pop    %edi
  8012d4:	5d                   	pop    %ebp
  8012d5:	c3                   	ret    

008012d6 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8012d6:	55                   	push   %ebp
  8012d7:	89 e5                	mov    %esp,%ebp
  8012d9:	53                   	push   %ebx
  8012da:	83 ec 14             	sub    $0x14,%esp
  8012dd:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8012e0:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8012e3:	50                   	push   %eax
  8012e4:	53                   	push   %ebx
  8012e5:	e8 86 fd ff ff       	call   801070 <fd_lookup>
  8012ea:	83 c4 08             	add    $0x8,%esp
  8012ed:	85 c0                	test   %eax,%eax
  8012ef:	78 67                	js     801358 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8012f1:	83 ec 08             	sub    $0x8,%esp
  8012f4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012f7:	50                   	push   %eax
  8012f8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8012fb:	ff 30                	pushl  (%eax)
  8012fd:	e8 c5 fd ff ff       	call   8010c7 <dev_lookup>
  801302:	83 c4 10             	add    $0x10,%esp
  801305:	85 c0                	test   %eax,%eax
  801307:	78 4f                	js     801358 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801309:	8b 55 f0             	mov    -0x10(%ebp),%edx
  80130c:	8b 42 08             	mov    0x8(%edx),%eax
  80130f:	83 e0 03             	and    $0x3,%eax
  801312:	83 f8 01             	cmp    $0x1,%eax
  801315:	75 21                	jne    801338 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801317:	a1 04 44 80 00       	mov    0x804404,%eax
  80131c:	8b 40 48             	mov    0x48(%eax),%eax
  80131f:	83 ec 04             	sub    $0x4,%esp
  801322:	53                   	push   %ebx
  801323:	50                   	push   %eax
  801324:	68 00 24 80 00       	push   $0x802400
  801329:	e8 6e f0 ff ff       	call   80039c <cprintf>
		return -E_INVAL;
  80132e:	83 c4 10             	add    $0x10,%esp
  801331:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801336:	eb 20                	jmp    801358 <read+0x82>
	}
	if (!dev->dev_read)
  801338:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80133b:	8b 40 08             	mov    0x8(%eax),%eax
  80133e:	85 c0                	test   %eax,%eax
  801340:	74 11                	je     801353 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801342:	83 ec 04             	sub    $0x4,%esp
  801345:	ff 75 10             	pushl  0x10(%ebp)
  801348:	ff 75 0c             	pushl  0xc(%ebp)
  80134b:	52                   	push   %edx
  80134c:	ff d0                	call   *%eax
  80134e:	83 c4 10             	add    $0x10,%esp
  801351:	eb 05                	jmp    801358 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801353:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801358:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80135b:	c9                   	leave  
  80135c:	c3                   	ret    

0080135d <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  80135d:	55                   	push   %ebp
  80135e:	89 e5                	mov    %esp,%ebp
  801360:	57                   	push   %edi
  801361:	56                   	push   %esi
  801362:	53                   	push   %ebx
  801363:	83 ec 0c             	sub    $0xc,%esp
  801366:	8b 7d 08             	mov    0x8(%ebp),%edi
  801369:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80136c:	bb 00 00 00 00       	mov    $0x0,%ebx
  801371:	eb 21                	jmp    801394 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801373:	83 ec 04             	sub    $0x4,%esp
  801376:	89 f0                	mov    %esi,%eax
  801378:	29 d8                	sub    %ebx,%eax
  80137a:	50                   	push   %eax
  80137b:	89 d8                	mov    %ebx,%eax
  80137d:	03 45 0c             	add    0xc(%ebp),%eax
  801380:	50                   	push   %eax
  801381:	57                   	push   %edi
  801382:	e8 4f ff ff ff       	call   8012d6 <read>
		if (m < 0)
  801387:	83 c4 10             	add    $0x10,%esp
  80138a:	85 c0                	test   %eax,%eax
  80138c:	78 10                	js     80139e <readn+0x41>
			return m;
		if (m == 0)
  80138e:	85 c0                	test   %eax,%eax
  801390:	74 0a                	je     80139c <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801392:	01 c3                	add    %eax,%ebx
  801394:	39 f3                	cmp    %esi,%ebx
  801396:	72 db                	jb     801373 <readn+0x16>
  801398:	89 d8                	mov    %ebx,%eax
  80139a:	eb 02                	jmp    80139e <readn+0x41>
  80139c:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  80139e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8013a1:	5b                   	pop    %ebx
  8013a2:	5e                   	pop    %esi
  8013a3:	5f                   	pop    %edi
  8013a4:	5d                   	pop    %ebp
  8013a5:	c3                   	ret    

008013a6 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8013a6:	55                   	push   %ebp
  8013a7:	89 e5                	mov    %esp,%ebp
  8013a9:	53                   	push   %ebx
  8013aa:	83 ec 14             	sub    $0x14,%esp
  8013ad:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8013b0:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8013b3:	50                   	push   %eax
  8013b4:	53                   	push   %ebx
  8013b5:	e8 b6 fc ff ff       	call   801070 <fd_lookup>
  8013ba:	83 c4 08             	add    $0x8,%esp
  8013bd:	85 c0                	test   %eax,%eax
  8013bf:	78 62                	js     801423 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8013c1:	83 ec 08             	sub    $0x8,%esp
  8013c4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013c7:	50                   	push   %eax
  8013c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8013cb:	ff 30                	pushl  (%eax)
  8013cd:	e8 f5 fc ff ff       	call   8010c7 <dev_lookup>
  8013d2:	83 c4 10             	add    $0x10,%esp
  8013d5:	85 c0                	test   %eax,%eax
  8013d7:	78 4a                	js     801423 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8013d9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8013dc:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8013e0:	75 21                	jne    801403 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  8013e2:	a1 04 44 80 00       	mov    0x804404,%eax
  8013e7:	8b 40 48             	mov    0x48(%eax),%eax
  8013ea:	83 ec 04             	sub    $0x4,%esp
  8013ed:	53                   	push   %ebx
  8013ee:	50                   	push   %eax
  8013ef:	68 1c 24 80 00       	push   $0x80241c
  8013f4:	e8 a3 ef ff ff       	call   80039c <cprintf>
		return -E_INVAL;
  8013f9:	83 c4 10             	add    $0x10,%esp
  8013fc:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801401:	eb 20                	jmp    801423 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801403:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801406:	8b 52 0c             	mov    0xc(%edx),%edx
  801409:	85 d2                	test   %edx,%edx
  80140b:	74 11                	je     80141e <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  80140d:	83 ec 04             	sub    $0x4,%esp
  801410:	ff 75 10             	pushl  0x10(%ebp)
  801413:	ff 75 0c             	pushl  0xc(%ebp)
  801416:	50                   	push   %eax
  801417:	ff d2                	call   *%edx
  801419:	83 c4 10             	add    $0x10,%esp
  80141c:	eb 05                	jmp    801423 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  80141e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801423:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801426:	c9                   	leave  
  801427:	c3                   	ret    

00801428 <seek>:

int
seek(int fdnum, off_t offset)
{
  801428:	55                   	push   %ebp
  801429:	89 e5                	mov    %esp,%ebp
  80142b:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80142e:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801431:	50                   	push   %eax
  801432:	ff 75 08             	pushl  0x8(%ebp)
  801435:	e8 36 fc ff ff       	call   801070 <fd_lookup>
  80143a:	83 c4 08             	add    $0x8,%esp
  80143d:	85 c0                	test   %eax,%eax
  80143f:	78 0e                	js     80144f <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801441:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801444:	8b 55 0c             	mov    0xc(%ebp),%edx
  801447:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  80144a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80144f:	c9                   	leave  
  801450:	c3                   	ret    

00801451 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801451:	55                   	push   %ebp
  801452:	89 e5                	mov    %esp,%ebp
  801454:	53                   	push   %ebx
  801455:	83 ec 14             	sub    $0x14,%esp
  801458:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  80145b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80145e:	50                   	push   %eax
  80145f:	53                   	push   %ebx
  801460:	e8 0b fc ff ff       	call   801070 <fd_lookup>
  801465:	83 c4 08             	add    $0x8,%esp
  801468:	85 c0                	test   %eax,%eax
  80146a:	78 5f                	js     8014cb <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80146c:	83 ec 08             	sub    $0x8,%esp
  80146f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801472:	50                   	push   %eax
  801473:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801476:	ff 30                	pushl  (%eax)
  801478:	e8 4a fc ff ff       	call   8010c7 <dev_lookup>
  80147d:	83 c4 10             	add    $0x10,%esp
  801480:	85 c0                	test   %eax,%eax
  801482:	78 47                	js     8014cb <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801484:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801487:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80148b:	75 21                	jne    8014ae <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  80148d:	a1 04 44 80 00       	mov    0x804404,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  801492:	8b 40 48             	mov    0x48(%eax),%eax
  801495:	83 ec 04             	sub    $0x4,%esp
  801498:	53                   	push   %ebx
  801499:	50                   	push   %eax
  80149a:	68 dc 23 80 00       	push   $0x8023dc
  80149f:	e8 f8 ee ff ff       	call   80039c <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8014a4:	83 c4 10             	add    $0x10,%esp
  8014a7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8014ac:	eb 1d                	jmp    8014cb <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8014ae:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8014b1:	8b 52 18             	mov    0x18(%edx),%edx
  8014b4:	85 d2                	test   %edx,%edx
  8014b6:	74 0e                	je     8014c6 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8014b8:	83 ec 08             	sub    $0x8,%esp
  8014bb:	ff 75 0c             	pushl  0xc(%ebp)
  8014be:	50                   	push   %eax
  8014bf:	ff d2                	call   *%edx
  8014c1:	83 c4 10             	add    $0x10,%esp
  8014c4:	eb 05                	jmp    8014cb <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8014c6:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8014cb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8014ce:	c9                   	leave  
  8014cf:	c3                   	ret    

008014d0 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8014d0:	55                   	push   %ebp
  8014d1:	89 e5                	mov    %esp,%ebp
  8014d3:	53                   	push   %ebx
  8014d4:	83 ec 14             	sub    $0x14,%esp
  8014d7:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8014da:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8014dd:	50                   	push   %eax
  8014de:	ff 75 08             	pushl  0x8(%ebp)
  8014e1:	e8 8a fb ff ff       	call   801070 <fd_lookup>
  8014e6:	83 c4 08             	add    $0x8,%esp
  8014e9:	85 c0                	test   %eax,%eax
  8014eb:	78 52                	js     80153f <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8014ed:	83 ec 08             	sub    $0x8,%esp
  8014f0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8014f3:	50                   	push   %eax
  8014f4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8014f7:	ff 30                	pushl  (%eax)
  8014f9:	e8 c9 fb ff ff       	call   8010c7 <dev_lookup>
  8014fe:	83 c4 10             	add    $0x10,%esp
  801501:	85 c0                	test   %eax,%eax
  801503:	78 3a                	js     80153f <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801505:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801508:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  80150c:	74 2c                	je     80153a <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  80150e:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  801511:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801518:	00 00 00 
	stat->st_isdir = 0;
  80151b:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801522:	00 00 00 
	stat->st_dev = dev;
  801525:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  80152b:	83 ec 08             	sub    $0x8,%esp
  80152e:	53                   	push   %ebx
  80152f:	ff 75 f0             	pushl  -0x10(%ebp)
  801532:	ff 50 14             	call   *0x14(%eax)
  801535:	83 c4 10             	add    $0x10,%esp
  801538:	eb 05                	jmp    80153f <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  80153a:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  80153f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801542:	c9                   	leave  
  801543:	c3                   	ret    

00801544 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801544:	55                   	push   %ebp
  801545:	89 e5                	mov    %esp,%ebp
  801547:	56                   	push   %esi
  801548:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801549:	83 ec 08             	sub    $0x8,%esp
  80154c:	6a 00                	push   $0x0
  80154e:	ff 75 08             	pushl  0x8(%ebp)
  801551:	e8 e7 01 00 00       	call   80173d <open>
  801556:	89 c3                	mov    %eax,%ebx
  801558:	83 c4 10             	add    $0x10,%esp
  80155b:	85 c0                	test   %eax,%eax
  80155d:	78 1d                	js     80157c <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  80155f:	83 ec 08             	sub    $0x8,%esp
  801562:	ff 75 0c             	pushl  0xc(%ebp)
  801565:	50                   	push   %eax
  801566:	e8 65 ff ff ff       	call   8014d0 <fstat>
  80156b:	89 c6                	mov    %eax,%esi
	close(fd);
  80156d:	89 1c 24             	mov    %ebx,(%esp)
  801570:	e8 29 fc ff ff       	call   80119e <close>
	return r;
  801575:	83 c4 10             	add    $0x10,%esp
  801578:	89 f0                	mov    %esi,%eax
  80157a:	eb 00                	jmp    80157c <stat+0x38>
}
  80157c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80157f:	5b                   	pop    %ebx
  801580:	5e                   	pop    %esi
  801581:	5d                   	pop    %ebp
  801582:	c3                   	ret    

00801583 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  801583:	55                   	push   %ebp
  801584:	89 e5                	mov    %esp,%ebp
  801586:	56                   	push   %esi
  801587:	53                   	push   %ebx
  801588:	89 c6                	mov    %eax,%esi
  80158a:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  80158c:	83 3d 00 44 80 00 00 	cmpl   $0x0,0x804400
  801593:	75 12                	jne    8015a7 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801595:	83 ec 0c             	sub    $0xc,%esp
  801598:	6a 01                	push   $0x1
  80159a:	e8 4d 07 00 00       	call   801cec <ipc_find_env>
  80159f:	a3 00 44 80 00       	mov    %eax,0x804400
  8015a4:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8015a7:	6a 07                	push   $0x7
  8015a9:	68 00 50 80 00       	push   $0x805000
  8015ae:	56                   	push   %esi
  8015af:	ff 35 00 44 80 00    	pushl  0x804400
  8015b5:	e8 dd 06 00 00       	call   801c97 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8015ba:	83 c4 0c             	add    $0xc,%esp
  8015bd:	6a 00                	push   $0x0
  8015bf:	53                   	push   %ebx
  8015c0:	6a 00                	push   $0x0
  8015c2:	e8 68 06 00 00       	call   801c2f <ipc_recv>
}
  8015c7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8015ca:	5b                   	pop    %ebx
  8015cb:	5e                   	pop    %esi
  8015cc:	5d                   	pop    %ebp
  8015cd:	c3                   	ret    

008015ce <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8015ce:	55                   	push   %ebp
  8015cf:	89 e5                	mov    %esp,%ebp
  8015d1:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8015d4:	8b 45 08             	mov    0x8(%ebp),%eax
  8015d7:	8b 40 0c             	mov    0xc(%eax),%eax
  8015da:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  8015df:	8b 45 0c             	mov    0xc(%ebp),%eax
  8015e2:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  8015e7:	ba 00 00 00 00       	mov    $0x0,%edx
  8015ec:	b8 02 00 00 00       	mov    $0x2,%eax
  8015f1:	e8 8d ff ff ff       	call   801583 <fsipc>
}
  8015f6:	c9                   	leave  
  8015f7:	c3                   	ret    

008015f8 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8015f8:	55                   	push   %ebp
  8015f9:	89 e5                	mov    %esp,%ebp
  8015fb:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8015fe:	8b 45 08             	mov    0x8(%ebp),%eax
  801601:	8b 40 0c             	mov    0xc(%eax),%eax
  801604:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  801609:	ba 00 00 00 00       	mov    $0x0,%edx
  80160e:	b8 06 00 00 00       	mov    $0x6,%eax
  801613:	e8 6b ff ff ff       	call   801583 <fsipc>
}
  801618:	c9                   	leave  
  801619:	c3                   	ret    

0080161a <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  80161a:	55                   	push   %ebp
  80161b:	89 e5                	mov    %esp,%ebp
  80161d:	53                   	push   %ebx
  80161e:	83 ec 04             	sub    $0x4,%esp
  801621:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801624:	8b 45 08             	mov    0x8(%ebp),%eax
  801627:	8b 40 0c             	mov    0xc(%eax),%eax
  80162a:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  80162f:	ba 00 00 00 00       	mov    $0x0,%edx
  801634:	b8 05 00 00 00       	mov    $0x5,%eax
  801639:	e8 45 ff ff ff       	call   801583 <fsipc>
  80163e:	85 c0                	test   %eax,%eax
  801640:	78 2c                	js     80166e <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801642:	83 ec 08             	sub    $0x8,%esp
  801645:	68 00 50 80 00       	push   $0x805000
  80164a:	53                   	push   %ebx
  80164b:	e8 b6 f3 ff ff       	call   800a06 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801650:	a1 80 50 80 00       	mov    0x805080,%eax
  801655:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  80165b:	a1 84 50 80 00       	mov    0x805084,%eax
  801660:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801666:	83 c4 10             	add    $0x10,%esp
  801669:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80166e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801671:	c9                   	leave  
  801672:	c3                   	ret    

00801673 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801673:	55                   	push   %ebp
  801674:	89 e5                	mov    %esp,%ebp
  801676:	83 ec 08             	sub    $0x8,%esp
  801679:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  80167c:	8b 55 08             	mov    0x8(%ebp),%edx
  80167f:	8b 52 0c             	mov    0xc(%edx),%edx
  801682:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801688:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  80168d:	76 05                	jbe    801694 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  80168f:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801694:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801699:	83 ec 04             	sub    $0x4,%esp
  80169c:	50                   	push   %eax
  80169d:	ff 75 0c             	pushl  0xc(%ebp)
  8016a0:	68 08 50 80 00       	push   $0x805008
  8016a5:	e8 d1 f4 ff ff       	call   800b7b <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8016aa:	ba 00 00 00 00       	mov    $0x0,%edx
  8016af:	b8 04 00 00 00       	mov    $0x4,%eax
  8016b4:	e8 ca fe ff ff       	call   801583 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8016b9:	c9                   	leave  
  8016ba:	c3                   	ret    

008016bb <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8016bb:	55                   	push   %ebp
  8016bc:	89 e5                	mov    %esp,%ebp
  8016be:	56                   	push   %esi
  8016bf:	53                   	push   %ebx
  8016c0:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8016c3:	8b 45 08             	mov    0x8(%ebp),%eax
  8016c6:	8b 40 0c             	mov    0xc(%eax),%eax
  8016c9:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  8016ce:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  8016d4:	ba 00 00 00 00       	mov    $0x0,%edx
  8016d9:	b8 03 00 00 00       	mov    $0x3,%eax
  8016de:	e8 a0 fe ff ff       	call   801583 <fsipc>
  8016e3:	89 c3                	mov    %eax,%ebx
  8016e5:	85 c0                	test   %eax,%eax
  8016e7:	78 4b                	js     801734 <devfile_read+0x79>
		return r;
	assert(r <= n);
  8016e9:	39 c6                	cmp    %eax,%esi
  8016eb:	73 16                	jae    801703 <devfile_read+0x48>
  8016ed:	68 4c 24 80 00       	push   $0x80244c
  8016f2:	68 53 24 80 00       	push   $0x802453
  8016f7:	6a 7c                	push   $0x7c
  8016f9:	68 68 24 80 00       	push   $0x802468
  8016fe:	e8 c1 eb ff ff       	call   8002c4 <_panic>
	assert(r <= PGSIZE);
  801703:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801708:	7e 16                	jle    801720 <devfile_read+0x65>
  80170a:	68 73 24 80 00       	push   $0x802473
  80170f:	68 53 24 80 00       	push   $0x802453
  801714:	6a 7d                	push   $0x7d
  801716:	68 68 24 80 00       	push   $0x802468
  80171b:	e8 a4 eb ff ff       	call   8002c4 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801720:	83 ec 04             	sub    $0x4,%esp
  801723:	50                   	push   %eax
  801724:	68 00 50 80 00       	push   $0x805000
  801729:	ff 75 0c             	pushl  0xc(%ebp)
  80172c:	e8 4a f4 ff ff       	call   800b7b <memmove>
	return r;
  801731:	83 c4 10             	add    $0x10,%esp
}
  801734:	89 d8                	mov    %ebx,%eax
  801736:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801739:	5b                   	pop    %ebx
  80173a:	5e                   	pop    %esi
  80173b:	5d                   	pop    %ebp
  80173c:	c3                   	ret    

0080173d <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  80173d:	55                   	push   %ebp
  80173e:	89 e5                	mov    %esp,%ebp
  801740:	53                   	push   %ebx
  801741:	83 ec 20             	sub    $0x20,%esp
  801744:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801747:	53                   	push   %ebx
  801748:	e8 84 f2 ff ff       	call   8009d1 <strlen>
  80174d:	83 c4 10             	add    $0x10,%esp
  801750:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801755:	7f 63                	jg     8017ba <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801757:	83 ec 0c             	sub    $0xc,%esp
  80175a:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80175d:	50                   	push   %eax
  80175e:	e8 be f8 ff ff       	call   801021 <fd_alloc>
  801763:	83 c4 10             	add    $0x10,%esp
  801766:	85 c0                	test   %eax,%eax
  801768:	78 55                	js     8017bf <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  80176a:	83 ec 08             	sub    $0x8,%esp
  80176d:	53                   	push   %ebx
  80176e:	68 00 50 80 00       	push   $0x805000
  801773:	e8 8e f2 ff ff       	call   800a06 <strcpy>
	fsipcbuf.open.req_omode = mode;
  801778:	8b 45 0c             	mov    0xc(%ebp),%eax
  80177b:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801780:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801783:	b8 01 00 00 00       	mov    $0x1,%eax
  801788:	e8 f6 fd ff ff       	call   801583 <fsipc>
  80178d:	89 c3                	mov    %eax,%ebx
  80178f:	83 c4 10             	add    $0x10,%esp
  801792:	85 c0                	test   %eax,%eax
  801794:	79 14                	jns    8017aa <open+0x6d>
		fd_close(fd, 0);
  801796:	83 ec 08             	sub    $0x8,%esp
  801799:	6a 00                	push   $0x0
  80179b:	ff 75 f4             	pushl  -0xc(%ebp)
  80179e:	e8 77 f9 ff ff       	call   80111a <fd_close>
		return r;
  8017a3:	83 c4 10             	add    $0x10,%esp
  8017a6:	89 d8                	mov    %ebx,%eax
  8017a8:	eb 15                	jmp    8017bf <open+0x82>
	}

	return fd2num(fd);
  8017aa:	83 ec 0c             	sub    $0xc,%esp
  8017ad:	ff 75 f4             	pushl  -0xc(%ebp)
  8017b0:	e8 45 f8 ff ff       	call   800ffa <fd2num>
  8017b5:	83 c4 10             	add    $0x10,%esp
  8017b8:	eb 05                	jmp    8017bf <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8017ba:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8017bf:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017c2:	c9                   	leave  
  8017c3:	c3                   	ret    

008017c4 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8017c4:	55                   	push   %ebp
  8017c5:	89 e5                	mov    %esp,%ebp
  8017c7:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8017ca:	ba 00 00 00 00       	mov    $0x0,%edx
  8017cf:	b8 08 00 00 00       	mov    $0x8,%eax
  8017d4:	e8 aa fd ff ff       	call   801583 <fsipc>
}
  8017d9:	c9                   	leave  
  8017da:	c3                   	ret    

008017db <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  8017db:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  8017df:	7e 38                	jle    801819 <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  8017e1:	55                   	push   %ebp
  8017e2:	89 e5                	mov    %esp,%ebp
  8017e4:	53                   	push   %ebx
  8017e5:	83 ec 08             	sub    $0x8,%esp
  8017e8:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  8017ea:	ff 70 04             	pushl  0x4(%eax)
  8017ed:	8d 40 10             	lea    0x10(%eax),%eax
  8017f0:	50                   	push   %eax
  8017f1:	ff 33                	pushl  (%ebx)
  8017f3:	e8 ae fb ff ff       	call   8013a6 <write>
		if (result > 0)
  8017f8:	83 c4 10             	add    $0x10,%esp
  8017fb:	85 c0                	test   %eax,%eax
  8017fd:	7e 03                	jle    801802 <writebuf+0x27>
			b->result += result;
  8017ff:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  801802:	3b 43 04             	cmp    0x4(%ebx),%eax
  801805:	74 0e                	je     801815 <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  801807:	89 c2                	mov    %eax,%edx
  801809:	85 c0                	test   %eax,%eax
  80180b:	7e 05                	jle    801812 <writebuf+0x37>
  80180d:	ba 00 00 00 00       	mov    $0x0,%edx
  801812:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  801815:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801818:	c9                   	leave  
  801819:	c3                   	ret    

0080181a <putch>:

static void
putch(int ch, void *thunk)
{
  80181a:	55                   	push   %ebp
  80181b:	89 e5                	mov    %esp,%ebp
  80181d:	53                   	push   %ebx
  80181e:	83 ec 04             	sub    $0x4,%esp
  801821:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  801824:	8b 53 04             	mov    0x4(%ebx),%edx
  801827:	8d 42 01             	lea    0x1(%edx),%eax
  80182a:	89 43 04             	mov    %eax,0x4(%ebx)
  80182d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801830:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  801834:	3d 00 01 00 00       	cmp    $0x100,%eax
  801839:	75 0e                	jne    801849 <putch+0x2f>
		writebuf(b);
  80183b:	89 d8                	mov    %ebx,%eax
  80183d:	e8 99 ff ff ff       	call   8017db <writebuf>
		b->idx = 0;
  801842:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  801849:	83 c4 04             	add    $0x4,%esp
  80184c:	5b                   	pop    %ebx
  80184d:	5d                   	pop    %ebp
  80184e:	c3                   	ret    

0080184f <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  80184f:	55                   	push   %ebp
  801850:	89 e5                	mov    %esp,%ebp
  801852:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  801858:	8b 45 08             	mov    0x8(%ebp),%eax
  80185b:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  801861:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  801868:	00 00 00 
	b.result = 0;
  80186b:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  801872:	00 00 00 
	b.error = 1;
  801875:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  80187c:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  80187f:	ff 75 10             	pushl  0x10(%ebp)
  801882:	ff 75 0c             	pushl  0xc(%ebp)
  801885:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  80188b:	50                   	push   %eax
  80188c:	68 1a 18 80 00       	push   $0x80181a
  801891:	e8 3a ec ff ff       	call   8004d0 <vprintfmt>
	if (b.idx > 0)
  801896:	83 c4 10             	add    $0x10,%esp
  801899:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  8018a0:	7e 0b                	jle    8018ad <vfprintf+0x5e>
		writebuf(&b);
  8018a2:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  8018a8:	e8 2e ff ff ff       	call   8017db <writebuf>

	return (b.result ? b.result : b.error);
  8018ad:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8018b3:	85 c0                	test   %eax,%eax
  8018b5:	75 06                	jne    8018bd <vfprintf+0x6e>
  8018b7:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8018bd:	c9                   	leave  
  8018be:	c3                   	ret    

008018bf <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  8018bf:	55                   	push   %ebp
  8018c0:	89 e5                	mov    %esp,%ebp
  8018c2:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8018c5:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  8018c8:	50                   	push   %eax
  8018c9:	ff 75 0c             	pushl  0xc(%ebp)
  8018cc:	ff 75 08             	pushl  0x8(%ebp)
  8018cf:	e8 7b ff ff ff       	call   80184f <vfprintf>
	va_end(ap);

	return cnt;
}
  8018d4:	c9                   	leave  
  8018d5:	c3                   	ret    

008018d6 <printf>:

int
printf(const char *fmt, ...)
{
  8018d6:	55                   	push   %ebp
  8018d7:	89 e5                	mov    %esp,%ebp
  8018d9:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8018dc:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  8018df:	50                   	push   %eax
  8018e0:	ff 75 08             	pushl  0x8(%ebp)
  8018e3:	6a 01                	push   $0x1
  8018e5:	e8 65 ff ff ff       	call   80184f <vfprintf>
	va_end(ap);

	return cnt;
}
  8018ea:	c9                   	leave  
  8018eb:	c3                   	ret    

008018ec <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  8018ec:	55                   	push   %ebp
  8018ed:	89 e5                	mov    %esp,%ebp
  8018ef:	56                   	push   %esi
  8018f0:	53                   	push   %ebx
  8018f1:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  8018f4:	83 ec 0c             	sub    $0xc,%esp
  8018f7:	ff 75 08             	pushl  0x8(%ebp)
  8018fa:	e8 0b f7 ff ff       	call   80100a <fd2data>
  8018ff:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801901:	83 c4 08             	add    $0x8,%esp
  801904:	68 7f 24 80 00       	push   $0x80247f
  801909:	53                   	push   %ebx
  80190a:	e8 f7 f0 ff ff       	call   800a06 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  80190f:	8b 46 04             	mov    0x4(%esi),%eax
  801912:	2b 06                	sub    (%esi),%eax
  801914:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  80191a:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801921:	00 00 00 
	stat->st_dev = &devpipe;
  801924:	c7 83 88 00 00 00 3c 	movl   $0x80303c,0x88(%ebx)
  80192b:	30 80 00 
	return 0;
}
  80192e:	b8 00 00 00 00       	mov    $0x0,%eax
  801933:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801936:	5b                   	pop    %ebx
  801937:	5e                   	pop    %esi
  801938:	5d                   	pop    %ebp
  801939:	c3                   	ret    

0080193a <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  80193a:	55                   	push   %ebp
  80193b:	89 e5                	mov    %esp,%ebp
  80193d:	53                   	push   %ebx
  80193e:	83 ec 0c             	sub    $0xc,%esp
  801941:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801944:	53                   	push   %ebx
  801945:	6a 00                	push   $0x0
  801947:	e8 23 f5 ff ff       	call   800e6f <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  80194c:	89 1c 24             	mov    %ebx,(%esp)
  80194f:	e8 b6 f6 ff ff       	call   80100a <fd2data>
  801954:	83 c4 08             	add    $0x8,%esp
  801957:	50                   	push   %eax
  801958:	6a 00                	push   $0x0
  80195a:	e8 10 f5 ff ff       	call   800e6f <sys_page_unmap>
}
  80195f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801962:	c9                   	leave  
  801963:	c3                   	ret    

00801964 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801964:	55                   	push   %ebp
  801965:	89 e5                	mov    %esp,%ebp
  801967:	57                   	push   %edi
  801968:	56                   	push   %esi
  801969:	53                   	push   %ebx
  80196a:	83 ec 1c             	sub    $0x1c,%esp
  80196d:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801970:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801972:	a1 04 44 80 00       	mov    0x804404,%eax
  801977:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  80197a:	83 ec 0c             	sub    $0xc,%esp
  80197d:	ff 75 e0             	pushl  -0x20(%ebp)
  801980:	e8 ab 03 00 00       	call   801d30 <pageref>
  801985:	89 c3                	mov    %eax,%ebx
  801987:	89 3c 24             	mov    %edi,(%esp)
  80198a:	e8 a1 03 00 00       	call   801d30 <pageref>
  80198f:	83 c4 10             	add    $0x10,%esp
  801992:	39 c3                	cmp    %eax,%ebx
  801994:	0f 94 c1             	sete   %cl
  801997:	0f b6 c9             	movzbl %cl,%ecx
  80199a:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  80199d:	8b 15 04 44 80 00    	mov    0x804404,%edx
  8019a3:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  8019a6:	39 ce                	cmp    %ecx,%esi
  8019a8:	74 1b                	je     8019c5 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  8019aa:	39 c3                	cmp    %eax,%ebx
  8019ac:	75 c4                	jne    801972 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  8019ae:	8b 42 58             	mov    0x58(%edx),%eax
  8019b1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8019b4:	50                   	push   %eax
  8019b5:	56                   	push   %esi
  8019b6:	68 86 24 80 00       	push   $0x802486
  8019bb:	e8 dc e9 ff ff       	call   80039c <cprintf>
  8019c0:	83 c4 10             	add    $0x10,%esp
  8019c3:	eb ad                	jmp    801972 <_pipeisclosed+0xe>
	}
}
  8019c5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8019c8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8019cb:	5b                   	pop    %ebx
  8019cc:	5e                   	pop    %esi
  8019cd:	5f                   	pop    %edi
  8019ce:	5d                   	pop    %ebp
  8019cf:	c3                   	ret    

008019d0 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8019d0:	55                   	push   %ebp
  8019d1:	89 e5                	mov    %esp,%ebp
  8019d3:	57                   	push   %edi
  8019d4:	56                   	push   %esi
  8019d5:	53                   	push   %ebx
  8019d6:	83 ec 18             	sub    $0x18,%esp
  8019d9:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  8019dc:	56                   	push   %esi
  8019dd:	e8 28 f6 ff ff       	call   80100a <fd2data>
  8019e2:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8019e4:	83 c4 10             	add    $0x10,%esp
  8019e7:	bf 00 00 00 00       	mov    $0x0,%edi
  8019ec:	eb 3b                	jmp    801a29 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  8019ee:	89 da                	mov    %ebx,%edx
  8019f0:	89 f0                	mov    %esi,%eax
  8019f2:	e8 6d ff ff ff       	call   801964 <_pipeisclosed>
  8019f7:	85 c0                	test   %eax,%eax
  8019f9:	75 38                	jne    801a33 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  8019fb:	e8 cb f3 ff ff       	call   800dcb <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801a00:	8b 53 04             	mov    0x4(%ebx),%edx
  801a03:	8b 03                	mov    (%ebx),%eax
  801a05:	83 c0 20             	add    $0x20,%eax
  801a08:	39 c2                	cmp    %eax,%edx
  801a0a:	73 e2                	jae    8019ee <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801a0c:	8b 45 0c             	mov    0xc(%ebp),%eax
  801a0f:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801a12:	89 d0                	mov    %edx,%eax
  801a14:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801a19:	79 05                	jns    801a20 <devpipe_write+0x50>
  801a1b:	48                   	dec    %eax
  801a1c:	83 c8 e0             	or     $0xffffffe0,%eax
  801a1f:	40                   	inc    %eax
  801a20:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801a24:	42                   	inc    %edx
  801a25:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a28:	47                   	inc    %edi
  801a29:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801a2c:	75 d2                	jne    801a00 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801a2e:	8b 45 10             	mov    0x10(%ebp),%eax
  801a31:	eb 05                	jmp    801a38 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801a33:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801a38:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a3b:	5b                   	pop    %ebx
  801a3c:	5e                   	pop    %esi
  801a3d:	5f                   	pop    %edi
  801a3e:	5d                   	pop    %ebp
  801a3f:	c3                   	ret    

00801a40 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801a40:	55                   	push   %ebp
  801a41:	89 e5                	mov    %esp,%ebp
  801a43:	57                   	push   %edi
  801a44:	56                   	push   %esi
  801a45:	53                   	push   %ebx
  801a46:	83 ec 18             	sub    $0x18,%esp
  801a49:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801a4c:	57                   	push   %edi
  801a4d:	e8 b8 f5 ff ff       	call   80100a <fd2data>
  801a52:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a54:	83 c4 10             	add    $0x10,%esp
  801a57:	bb 00 00 00 00       	mov    $0x0,%ebx
  801a5c:	eb 3a                	jmp    801a98 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801a5e:	85 db                	test   %ebx,%ebx
  801a60:	74 04                	je     801a66 <devpipe_read+0x26>
				return i;
  801a62:	89 d8                	mov    %ebx,%eax
  801a64:	eb 41                	jmp    801aa7 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801a66:	89 f2                	mov    %esi,%edx
  801a68:	89 f8                	mov    %edi,%eax
  801a6a:	e8 f5 fe ff ff       	call   801964 <_pipeisclosed>
  801a6f:	85 c0                	test   %eax,%eax
  801a71:	75 2f                	jne    801aa2 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801a73:	e8 53 f3 ff ff       	call   800dcb <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801a78:	8b 06                	mov    (%esi),%eax
  801a7a:	3b 46 04             	cmp    0x4(%esi),%eax
  801a7d:	74 df                	je     801a5e <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801a7f:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801a84:	79 05                	jns    801a8b <devpipe_read+0x4b>
  801a86:	48                   	dec    %eax
  801a87:	83 c8 e0             	or     $0xffffffe0,%eax
  801a8a:	40                   	inc    %eax
  801a8b:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801a8f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801a92:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801a95:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a97:	43                   	inc    %ebx
  801a98:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801a9b:	75 db                	jne    801a78 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801a9d:	8b 45 10             	mov    0x10(%ebp),%eax
  801aa0:	eb 05                	jmp    801aa7 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801aa2:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801aa7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801aaa:	5b                   	pop    %ebx
  801aab:	5e                   	pop    %esi
  801aac:	5f                   	pop    %edi
  801aad:	5d                   	pop    %ebp
  801aae:	c3                   	ret    

00801aaf <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801aaf:	55                   	push   %ebp
  801ab0:	89 e5                	mov    %esp,%ebp
  801ab2:	56                   	push   %esi
  801ab3:	53                   	push   %ebx
  801ab4:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801ab7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801aba:	50                   	push   %eax
  801abb:	e8 61 f5 ff ff       	call   801021 <fd_alloc>
  801ac0:	83 c4 10             	add    $0x10,%esp
  801ac3:	85 c0                	test   %eax,%eax
  801ac5:	0f 88 2a 01 00 00    	js     801bf5 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801acb:	83 ec 04             	sub    $0x4,%esp
  801ace:	68 07 04 00 00       	push   $0x407
  801ad3:	ff 75 f4             	pushl  -0xc(%ebp)
  801ad6:	6a 00                	push   $0x0
  801ad8:	e8 0d f3 ff ff       	call   800dea <sys_page_alloc>
  801add:	83 c4 10             	add    $0x10,%esp
  801ae0:	85 c0                	test   %eax,%eax
  801ae2:	0f 88 0d 01 00 00    	js     801bf5 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801ae8:	83 ec 0c             	sub    $0xc,%esp
  801aeb:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801aee:	50                   	push   %eax
  801aef:	e8 2d f5 ff ff       	call   801021 <fd_alloc>
  801af4:	89 c3                	mov    %eax,%ebx
  801af6:	83 c4 10             	add    $0x10,%esp
  801af9:	85 c0                	test   %eax,%eax
  801afb:	0f 88 e2 00 00 00    	js     801be3 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b01:	83 ec 04             	sub    $0x4,%esp
  801b04:	68 07 04 00 00       	push   $0x407
  801b09:	ff 75 f0             	pushl  -0x10(%ebp)
  801b0c:	6a 00                	push   $0x0
  801b0e:	e8 d7 f2 ff ff       	call   800dea <sys_page_alloc>
  801b13:	89 c3                	mov    %eax,%ebx
  801b15:	83 c4 10             	add    $0x10,%esp
  801b18:	85 c0                	test   %eax,%eax
  801b1a:	0f 88 c3 00 00 00    	js     801be3 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801b20:	83 ec 0c             	sub    $0xc,%esp
  801b23:	ff 75 f4             	pushl  -0xc(%ebp)
  801b26:	e8 df f4 ff ff       	call   80100a <fd2data>
  801b2b:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b2d:	83 c4 0c             	add    $0xc,%esp
  801b30:	68 07 04 00 00       	push   $0x407
  801b35:	50                   	push   %eax
  801b36:	6a 00                	push   $0x0
  801b38:	e8 ad f2 ff ff       	call   800dea <sys_page_alloc>
  801b3d:	89 c3                	mov    %eax,%ebx
  801b3f:	83 c4 10             	add    $0x10,%esp
  801b42:	85 c0                	test   %eax,%eax
  801b44:	0f 88 89 00 00 00    	js     801bd3 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b4a:	83 ec 0c             	sub    $0xc,%esp
  801b4d:	ff 75 f0             	pushl  -0x10(%ebp)
  801b50:	e8 b5 f4 ff ff       	call   80100a <fd2data>
  801b55:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801b5c:	50                   	push   %eax
  801b5d:	6a 00                	push   $0x0
  801b5f:	56                   	push   %esi
  801b60:	6a 00                	push   $0x0
  801b62:	e8 c6 f2 ff ff       	call   800e2d <sys_page_map>
  801b67:	89 c3                	mov    %eax,%ebx
  801b69:	83 c4 20             	add    $0x20,%esp
  801b6c:	85 c0                	test   %eax,%eax
  801b6e:	78 55                	js     801bc5 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801b70:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801b76:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b79:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801b7b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b7e:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801b85:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801b8b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801b8e:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801b90:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801b93:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801b9a:	83 ec 0c             	sub    $0xc,%esp
  801b9d:	ff 75 f4             	pushl  -0xc(%ebp)
  801ba0:	e8 55 f4 ff ff       	call   800ffa <fd2num>
  801ba5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801ba8:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801baa:	83 c4 04             	add    $0x4,%esp
  801bad:	ff 75 f0             	pushl  -0x10(%ebp)
  801bb0:	e8 45 f4 ff ff       	call   800ffa <fd2num>
  801bb5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801bb8:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801bbb:	83 c4 10             	add    $0x10,%esp
  801bbe:	b8 00 00 00 00       	mov    $0x0,%eax
  801bc3:	eb 30                	jmp    801bf5 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801bc5:	83 ec 08             	sub    $0x8,%esp
  801bc8:	56                   	push   %esi
  801bc9:	6a 00                	push   $0x0
  801bcb:	e8 9f f2 ff ff       	call   800e6f <sys_page_unmap>
  801bd0:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801bd3:	83 ec 08             	sub    $0x8,%esp
  801bd6:	ff 75 f0             	pushl  -0x10(%ebp)
  801bd9:	6a 00                	push   $0x0
  801bdb:	e8 8f f2 ff ff       	call   800e6f <sys_page_unmap>
  801be0:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801be3:	83 ec 08             	sub    $0x8,%esp
  801be6:	ff 75 f4             	pushl  -0xc(%ebp)
  801be9:	6a 00                	push   $0x0
  801beb:	e8 7f f2 ff ff       	call   800e6f <sys_page_unmap>
  801bf0:	83 c4 10             	add    $0x10,%esp
  801bf3:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801bf5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801bf8:	5b                   	pop    %ebx
  801bf9:	5e                   	pop    %esi
  801bfa:	5d                   	pop    %ebp
  801bfb:	c3                   	ret    

00801bfc <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801bfc:	55                   	push   %ebp
  801bfd:	89 e5                	mov    %esp,%ebp
  801bff:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801c02:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c05:	50                   	push   %eax
  801c06:	ff 75 08             	pushl  0x8(%ebp)
  801c09:	e8 62 f4 ff ff       	call   801070 <fd_lookup>
  801c0e:	83 c4 10             	add    $0x10,%esp
  801c11:	85 c0                	test   %eax,%eax
  801c13:	78 18                	js     801c2d <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801c15:	83 ec 0c             	sub    $0xc,%esp
  801c18:	ff 75 f4             	pushl  -0xc(%ebp)
  801c1b:	e8 ea f3 ff ff       	call   80100a <fd2data>
	return _pipeisclosed(fd, p);
  801c20:	89 c2                	mov    %eax,%edx
  801c22:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c25:	e8 3a fd ff ff       	call   801964 <_pipeisclosed>
  801c2a:	83 c4 10             	add    $0x10,%esp
}
  801c2d:	c9                   	leave  
  801c2e:	c3                   	ret    

00801c2f <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801c2f:	55                   	push   %ebp
  801c30:	89 e5                	mov    %esp,%ebp
  801c32:	56                   	push   %esi
  801c33:	53                   	push   %ebx
  801c34:	8b 75 08             	mov    0x8(%ebp),%esi
  801c37:	8b 45 0c             	mov    0xc(%ebp),%eax
  801c3a:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801c3d:	85 c0                	test   %eax,%eax
  801c3f:	75 05                	jne    801c46 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801c41:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801c46:	83 ec 0c             	sub    $0xc,%esp
  801c49:	50                   	push   %eax
  801c4a:	e8 6a f3 ff ff       	call   800fb9 <sys_ipc_recv>
  801c4f:	83 c4 10             	add    $0x10,%esp
  801c52:	85 c0                	test   %eax,%eax
  801c54:	74 16                	je     801c6c <ipc_recv+0x3d>
        if (from_env_store)
  801c56:	85 f6                	test   %esi,%esi
  801c58:	74 06                	je     801c60 <ipc_recv+0x31>
            *from_env_store = 0;
  801c5a:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801c60:	85 db                	test   %ebx,%ebx
  801c62:	74 2c                	je     801c90 <ipc_recv+0x61>
            *perm_store = 0;
  801c64:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801c6a:	eb 24                	jmp    801c90 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801c6c:	85 f6                	test   %esi,%esi
  801c6e:	74 0a                	je     801c7a <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801c70:	a1 04 44 80 00       	mov    0x804404,%eax
  801c75:	8b 40 74             	mov    0x74(%eax),%eax
  801c78:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801c7a:	85 db                	test   %ebx,%ebx
  801c7c:	74 0a                	je     801c88 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801c7e:	a1 04 44 80 00       	mov    0x804404,%eax
  801c83:	8b 40 78             	mov    0x78(%eax),%eax
  801c86:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801c88:	a1 04 44 80 00       	mov    0x804404,%eax
  801c8d:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801c90:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c93:	5b                   	pop    %ebx
  801c94:	5e                   	pop    %esi
  801c95:	5d                   	pop    %ebp
  801c96:	c3                   	ret    

00801c97 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801c97:	55                   	push   %ebp
  801c98:	89 e5                	mov    %esp,%ebp
  801c9a:	57                   	push   %edi
  801c9b:	56                   	push   %esi
  801c9c:	53                   	push   %ebx
  801c9d:	83 ec 0c             	sub    $0xc,%esp
  801ca0:	8b 75 0c             	mov    0xc(%ebp),%esi
  801ca3:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801ca6:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801ca9:	85 db                	test   %ebx,%ebx
  801cab:	75 0c                	jne    801cb9 <ipc_send+0x22>
        pg = (void *)UTOP;
  801cad:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801cb2:	eb 05                	jmp    801cb9 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801cb4:	e8 12 f1 ff ff       	call   800dcb <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801cb9:	57                   	push   %edi
  801cba:	53                   	push   %ebx
  801cbb:	56                   	push   %esi
  801cbc:	ff 75 08             	pushl  0x8(%ebp)
  801cbf:	e8 d2 f2 ff ff       	call   800f96 <sys_ipc_try_send>
  801cc4:	83 c4 10             	add    $0x10,%esp
  801cc7:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801cca:	74 e8                	je     801cb4 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801ccc:	85 c0                	test   %eax,%eax
  801cce:	74 14                	je     801ce4 <ipc_send+0x4d>
        panic("ipc_send: error");
  801cd0:	83 ec 04             	sub    $0x4,%esp
  801cd3:	68 9e 24 80 00       	push   $0x80249e
  801cd8:	6a 6a                	push   $0x6a
  801cda:	68 ae 24 80 00       	push   $0x8024ae
  801cdf:	e8 e0 e5 ff ff       	call   8002c4 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801ce4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801ce7:	5b                   	pop    %ebx
  801ce8:	5e                   	pop    %esi
  801ce9:	5f                   	pop    %edi
  801cea:	5d                   	pop    %ebp
  801ceb:	c3                   	ret    

00801cec <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801cec:	55                   	push   %ebp
  801ced:	89 e5                	mov    %esp,%ebp
  801cef:	53                   	push   %ebx
  801cf0:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801cf3:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801cf8:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801cff:	89 d0                	mov    %edx,%eax
  801d01:	c1 e0 07             	shl    $0x7,%eax
  801d04:	29 d8                	sub    %ebx,%eax
  801d06:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801d0b:	8b 40 50             	mov    0x50(%eax),%eax
  801d0e:	39 c8                	cmp    %ecx,%eax
  801d10:	75 0d                	jne    801d1f <ipc_find_env+0x33>
			return envs[i].env_id;
  801d12:	c1 e2 07             	shl    $0x7,%edx
  801d15:	29 da                	sub    %ebx,%edx
  801d17:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801d1d:	eb 0e                	jmp    801d2d <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801d1f:	42                   	inc    %edx
  801d20:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801d26:	75 d0                	jne    801cf8 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801d28:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801d2d:	5b                   	pop    %ebx
  801d2e:	5d                   	pop    %ebp
  801d2f:	c3                   	ret    

00801d30 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801d30:	55                   	push   %ebp
  801d31:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801d33:	8b 45 08             	mov    0x8(%ebp),%eax
  801d36:	c1 e8 16             	shr    $0x16,%eax
  801d39:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801d40:	a8 01                	test   $0x1,%al
  801d42:	74 21                	je     801d65 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801d44:	8b 45 08             	mov    0x8(%ebp),%eax
  801d47:	c1 e8 0c             	shr    $0xc,%eax
  801d4a:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801d51:	a8 01                	test   $0x1,%al
  801d53:	74 17                	je     801d6c <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801d55:	c1 e8 0c             	shr    $0xc,%eax
  801d58:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801d5f:	ef 
  801d60:	0f b7 c0             	movzwl %ax,%eax
  801d63:	eb 0c                	jmp    801d71 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801d65:	b8 00 00 00 00       	mov    $0x0,%eax
  801d6a:	eb 05                	jmp    801d71 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801d6c:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801d71:	5d                   	pop    %ebp
  801d72:	c3                   	ret    
  801d73:	90                   	nop

00801d74 <__udivdi3>:
  801d74:	55                   	push   %ebp
  801d75:	57                   	push   %edi
  801d76:	56                   	push   %esi
  801d77:	53                   	push   %ebx
  801d78:	83 ec 1c             	sub    $0x1c,%esp
  801d7b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801d7f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801d83:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801d87:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801d8b:	89 ca                	mov    %ecx,%edx
  801d8d:	89 f8                	mov    %edi,%eax
  801d8f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801d93:	85 f6                	test   %esi,%esi
  801d95:	75 2d                	jne    801dc4 <__udivdi3+0x50>
  801d97:	39 cf                	cmp    %ecx,%edi
  801d99:	77 65                	ja     801e00 <__udivdi3+0x8c>
  801d9b:	89 fd                	mov    %edi,%ebp
  801d9d:	85 ff                	test   %edi,%edi
  801d9f:	75 0b                	jne    801dac <__udivdi3+0x38>
  801da1:	b8 01 00 00 00       	mov    $0x1,%eax
  801da6:	31 d2                	xor    %edx,%edx
  801da8:	f7 f7                	div    %edi
  801daa:	89 c5                	mov    %eax,%ebp
  801dac:	31 d2                	xor    %edx,%edx
  801dae:	89 c8                	mov    %ecx,%eax
  801db0:	f7 f5                	div    %ebp
  801db2:	89 c1                	mov    %eax,%ecx
  801db4:	89 d8                	mov    %ebx,%eax
  801db6:	f7 f5                	div    %ebp
  801db8:	89 cf                	mov    %ecx,%edi
  801dba:	89 fa                	mov    %edi,%edx
  801dbc:	83 c4 1c             	add    $0x1c,%esp
  801dbf:	5b                   	pop    %ebx
  801dc0:	5e                   	pop    %esi
  801dc1:	5f                   	pop    %edi
  801dc2:	5d                   	pop    %ebp
  801dc3:	c3                   	ret    
  801dc4:	39 ce                	cmp    %ecx,%esi
  801dc6:	77 28                	ja     801df0 <__udivdi3+0x7c>
  801dc8:	0f bd fe             	bsr    %esi,%edi
  801dcb:	83 f7 1f             	xor    $0x1f,%edi
  801dce:	75 40                	jne    801e10 <__udivdi3+0x9c>
  801dd0:	39 ce                	cmp    %ecx,%esi
  801dd2:	72 0a                	jb     801dde <__udivdi3+0x6a>
  801dd4:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801dd8:	0f 87 9e 00 00 00    	ja     801e7c <__udivdi3+0x108>
  801dde:	b8 01 00 00 00       	mov    $0x1,%eax
  801de3:	89 fa                	mov    %edi,%edx
  801de5:	83 c4 1c             	add    $0x1c,%esp
  801de8:	5b                   	pop    %ebx
  801de9:	5e                   	pop    %esi
  801dea:	5f                   	pop    %edi
  801deb:	5d                   	pop    %ebp
  801dec:	c3                   	ret    
  801ded:	8d 76 00             	lea    0x0(%esi),%esi
  801df0:	31 ff                	xor    %edi,%edi
  801df2:	31 c0                	xor    %eax,%eax
  801df4:	89 fa                	mov    %edi,%edx
  801df6:	83 c4 1c             	add    $0x1c,%esp
  801df9:	5b                   	pop    %ebx
  801dfa:	5e                   	pop    %esi
  801dfb:	5f                   	pop    %edi
  801dfc:	5d                   	pop    %ebp
  801dfd:	c3                   	ret    
  801dfe:	66 90                	xchg   %ax,%ax
  801e00:	89 d8                	mov    %ebx,%eax
  801e02:	f7 f7                	div    %edi
  801e04:	31 ff                	xor    %edi,%edi
  801e06:	89 fa                	mov    %edi,%edx
  801e08:	83 c4 1c             	add    $0x1c,%esp
  801e0b:	5b                   	pop    %ebx
  801e0c:	5e                   	pop    %esi
  801e0d:	5f                   	pop    %edi
  801e0e:	5d                   	pop    %ebp
  801e0f:	c3                   	ret    
  801e10:	bd 20 00 00 00       	mov    $0x20,%ebp
  801e15:	89 eb                	mov    %ebp,%ebx
  801e17:	29 fb                	sub    %edi,%ebx
  801e19:	89 f9                	mov    %edi,%ecx
  801e1b:	d3 e6                	shl    %cl,%esi
  801e1d:	89 c5                	mov    %eax,%ebp
  801e1f:	88 d9                	mov    %bl,%cl
  801e21:	d3 ed                	shr    %cl,%ebp
  801e23:	89 e9                	mov    %ebp,%ecx
  801e25:	09 f1                	or     %esi,%ecx
  801e27:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801e2b:	89 f9                	mov    %edi,%ecx
  801e2d:	d3 e0                	shl    %cl,%eax
  801e2f:	89 c5                	mov    %eax,%ebp
  801e31:	89 d6                	mov    %edx,%esi
  801e33:	88 d9                	mov    %bl,%cl
  801e35:	d3 ee                	shr    %cl,%esi
  801e37:	89 f9                	mov    %edi,%ecx
  801e39:	d3 e2                	shl    %cl,%edx
  801e3b:	8b 44 24 08          	mov    0x8(%esp),%eax
  801e3f:	88 d9                	mov    %bl,%cl
  801e41:	d3 e8                	shr    %cl,%eax
  801e43:	09 c2                	or     %eax,%edx
  801e45:	89 d0                	mov    %edx,%eax
  801e47:	89 f2                	mov    %esi,%edx
  801e49:	f7 74 24 0c          	divl   0xc(%esp)
  801e4d:	89 d6                	mov    %edx,%esi
  801e4f:	89 c3                	mov    %eax,%ebx
  801e51:	f7 e5                	mul    %ebp
  801e53:	39 d6                	cmp    %edx,%esi
  801e55:	72 19                	jb     801e70 <__udivdi3+0xfc>
  801e57:	74 0b                	je     801e64 <__udivdi3+0xf0>
  801e59:	89 d8                	mov    %ebx,%eax
  801e5b:	31 ff                	xor    %edi,%edi
  801e5d:	e9 58 ff ff ff       	jmp    801dba <__udivdi3+0x46>
  801e62:	66 90                	xchg   %ax,%ax
  801e64:	8b 54 24 08          	mov    0x8(%esp),%edx
  801e68:	89 f9                	mov    %edi,%ecx
  801e6a:	d3 e2                	shl    %cl,%edx
  801e6c:	39 c2                	cmp    %eax,%edx
  801e6e:	73 e9                	jae    801e59 <__udivdi3+0xe5>
  801e70:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801e73:	31 ff                	xor    %edi,%edi
  801e75:	e9 40 ff ff ff       	jmp    801dba <__udivdi3+0x46>
  801e7a:	66 90                	xchg   %ax,%ax
  801e7c:	31 c0                	xor    %eax,%eax
  801e7e:	e9 37 ff ff ff       	jmp    801dba <__udivdi3+0x46>
  801e83:	90                   	nop

00801e84 <__umoddi3>:
  801e84:	55                   	push   %ebp
  801e85:	57                   	push   %edi
  801e86:	56                   	push   %esi
  801e87:	53                   	push   %ebx
  801e88:	83 ec 1c             	sub    $0x1c,%esp
  801e8b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  801e8f:	8b 74 24 34          	mov    0x34(%esp),%esi
  801e93:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801e97:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801e9b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  801e9f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801ea3:	89 f3                	mov    %esi,%ebx
  801ea5:	89 fa                	mov    %edi,%edx
  801ea7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801eab:	89 34 24             	mov    %esi,(%esp)
  801eae:	85 c0                	test   %eax,%eax
  801eb0:	75 1a                	jne    801ecc <__umoddi3+0x48>
  801eb2:	39 f7                	cmp    %esi,%edi
  801eb4:	0f 86 a2 00 00 00    	jbe    801f5c <__umoddi3+0xd8>
  801eba:	89 c8                	mov    %ecx,%eax
  801ebc:	89 f2                	mov    %esi,%edx
  801ebe:	f7 f7                	div    %edi
  801ec0:	89 d0                	mov    %edx,%eax
  801ec2:	31 d2                	xor    %edx,%edx
  801ec4:	83 c4 1c             	add    $0x1c,%esp
  801ec7:	5b                   	pop    %ebx
  801ec8:	5e                   	pop    %esi
  801ec9:	5f                   	pop    %edi
  801eca:	5d                   	pop    %ebp
  801ecb:	c3                   	ret    
  801ecc:	39 f0                	cmp    %esi,%eax
  801ece:	0f 87 ac 00 00 00    	ja     801f80 <__umoddi3+0xfc>
  801ed4:	0f bd e8             	bsr    %eax,%ebp
  801ed7:	83 f5 1f             	xor    $0x1f,%ebp
  801eda:	0f 84 ac 00 00 00    	je     801f8c <__umoddi3+0x108>
  801ee0:	bf 20 00 00 00       	mov    $0x20,%edi
  801ee5:	29 ef                	sub    %ebp,%edi
  801ee7:	89 fe                	mov    %edi,%esi
  801ee9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801eed:	89 e9                	mov    %ebp,%ecx
  801eef:	d3 e0                	shl    %cl,%eax
  801ef1:	89 d7                	mov    %edx,%edi
  801ef3:	89 f1                	mov    %esi,%ecx
  801ef5:	d3 ef                	shr    %cl,%edi
  801ef7:	09 c7                	or     %eax,%edi
  801ef9:	89 e9                	mov    %ebp,%ecx
  801efb:	d3 e2                	shl    %cl,%edx
  801efd:	89 14 24             	mov    %edx,(%esp)
  801f00:	89 d8                	mov    %ebx,%eax
  801f02:	d3 e0                	shl    %cl,%eax
  801f04:	89 c2                	mov    %eax,%edx
  801f06:	8b 44 24 08          	mov    0x8(%esp),%eax
  801f0a:	d3 e0                	shl    %cl,%eax
  801f0c:	89 44 24 04          	mov    %eax,0x4(%esp)
  801f10:	8b 44 24 08          	mov    0x8(%esp),%eax
  801f14:	89 f1                	mov    %esi,%ecx
  801f16:	d3 e8                	shr    %cl,%eax
  801f18:	09 d0                	or     %edx,%eax
  801f1a:	d3 eb                	shr    %cl,%ebx
  801f1c:	89 da                	mov    %ebx,%edx
  801f1e:	f7 f7                	div    %edi
  801f20:	89 d3                	mov    %edx,%ebx
  801f22:	f7 24 24             	mull   (%esp)
  801f25:	89 c6                	mov    %eax,%esi
  801f27:	89 d1                	mov    %edx,%ecx
  801f29:	39 d3                	cmp    %edx,%ebx
  801f2b:	0f 82 87 00 00 00    	jb     801fb8 <__umoddi3+0x134>
  801f31:	0f 84 91 00 00 00    	je     801fc8 <__umoddi3+0x144>
  801f37:	8b 54 24 04          	mov    0x4(%esp),%edx
  801f3b:	29 f2                	sub    %esi,%edx
  801f3d:	19 cb                	sbb    %ecx,%ebx
  801f3f:	89 d8                	mov    %ebx,%eax
  801f41:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801f45:	d3 e0                	shl    %cl,%eax
  801f47:	89 e9                	mov    %ebp,%ecx
  801f49:	d3 ea                	shr    %cl,%edx
  801f4b:	09 d0                	or     %edx,%eax
  801f4d:	89 e9                	mov    %ebp,%ecx
  801f4f:	d3 eb                	shr    %cl,%ebx
  801f51:	89 da                	mov    %ebx,%edx
  801f53:	83 c4 1c             	add    $0x1c,%esp
  801f56:	5b                   	pop    %ebx
  801f57:	5e                   	pop    %esi
  801f58:	5f                   	pop    %edi
  801f59:	5d                   	pop    %ebp
  801f5a:	c3                   	ret    
  801f5b:	90                   	nop
  801f5c:	89 fd                	mov    %edi,%ebp
  801f5e:	85 ff                	test   %edi,%edi
  801f60:	75 0b                	jne    801f6d <__umoddi3+0xe9>
  801f62:	b8 01 00 00 00       	mov    $0x1,%eax
  801f67:	31 d2                	xor    %edx,%edx
  801f69:	f7 f7                	div    %edi
  801f6b:	89 c5                	mov    %eax,%ebp
  801f6d:	89 f0                	mov    %esi,%eax
  801f6f:	31 d2                	xor    %edx,%edx
  801f71:	f7 f5                	div    %ebp
  801f73:	89 c8                	mov    %ecx,%eax
  801f75:	f7 f5                	div    %ebp
  801f77:	89 d0                	mov    %edx,%eax
  801f79:	e9 44 ff ff ff       	jmp    801ec2 <__umoddi3+0x3e>
  801f7e:	66 90                	xchg   %ax,%ax
  801f80:	89 c8                	mov    %ecx,%eax
  801f82:	89 f2                	mov    %esi,%edx
  801f84:	83 c4 1c             	add    $0x1c,%esp
  801f87:	5b                   	pop    %ebx
  801f88:	5e                   	pop    %esi
  801f89:	5f                   	pop    %edi
  801f8a:	5d                   	pop    %ebp
  801f8b:	c3                   	ret    
  801f8c:	3b 04 24             	cmp    (%esp),%eax
  801f8f:	72 06                	jb     801f97 <__umoddi3+0x113>
  801f91:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801f95:	77 0f                	ja     801fa6 <__umoddi3+0x122>
  801f97:	89 f2                	mov    %esi,%edx
  801f99:	29 f9                	sub    %edi,%ecx
  801f9b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  801f9f:	89 14 24             	mov    %edx,(%esp)
  801fa2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801fa6:	8b 44 24 04          	mov    0x4(%esp),%eax
  801faa:	8b 14 24             	mov    (%esp),%edx
  801fad:	83 c4 1c             	add    $0x1c,%esp
  801fb0:	5b                   	pop    %ebx
  801fb1:	5e                   	pop    %esi
  801fb2:	5f                   	pop    %edi
  801fb3:	5d                   	pop    %ebp
  801fb4:	c3                   	ret    
  801fb5:	8d 76 00             	lea    0x0(%esi),%esi
  801fb8:	2b 04 24             	sub    (%esp),%eax
  801fbb:	19 fa                	sbb    %edi,%edx
  801fbd:	89 d1                	mov    %edx,%ecx
  801fbf:	89 c6                	mov    %eax,%esi
  801fc1:	e9 71 ff ff ff       	jmp    801f37 <__umoddi3+0xb3>
  801fc6:	66 90                	xchg   %ax,%ax
  801fc8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801fcc:	72 ea                	jb     801fb8 <__umoddi3+0x134>
  801fce:	89 d9                	mov    %ebx,%ecx
  801fd0:	e9 62 ff ff ff       	jmp    801f37 <__umoddi3+0xb3>
