
obj/user/init.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 63 03 00 00       	call   800394 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <sum>:

char bss[6000];

int
sum(const char *s, int n)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	8b 75 08             	mov    0x8(%ebp),%esi
  80003b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int i, tot = 0;
  80003e:	b8 00 00 00 00       	mov    $0x0,%eax
	for (i = 0; i < n; i++)
  800043:	ba 00 00 00 00       	mov    $0x0,%edx
  800048:	eb 0a                	jmp    800054 <sum+0x21>
		tot ^= i * s[i];
  80004a:	0f be 0c 16          	movsbl (%esi,%edx,1),%ecx
  80004e:	0f af ca             	imul   %edx,%ecx
  800051:	31 c8                	xor    %ecx,%eax

int
sum(const char *s, int n)
{
	int i, tot = 0;
	for (i = 0; i < n; i++)
  800053:	42                   	inc    %edx
  800054:	39 da                	cmp    %ebx,%edx
  800056:	7c f2                	jl     80004a <sum+0x17>
		tot ^= i * s[i];
	return tot;
}
  800058:	5b                   	pop    %ebx
  800059:	5e                   	pop    %esi
  80005a:	5d                   	pop    %ebp
  80005b:	c3                   	ret    

0080005c <umain>:

void
umain(int argc, char **argv)
{
  80005c:	55                   	push   %ebp
  80005d:	89 e5                	mov    %esp,%ebp
  80005f:	57                   	push   %edi
  800060:	56                   	push   %esi
  800061:	53                   	push   %ebx
  800062:	81 ec 18 01 00 00    	sub    $0x118,%esp
  800068:	8b 7d 0c             	mov    0xc(%ebp),%edi
	int i, r, x, want;
	char args[256];

	cprintf("init: running\n");
  80006b:	68 40 25 80 00       	push   $0x802540
  800070:	e8 58 04 00 00       	call   8004cd <cprintf>

	want = 0xf989e;
	if ((x = sum((char*)&data, sizeof data)) != want)
  800075:	83 c4 08             	add    $0x8,%esp
  800078:	68 70 17 00 00       	push   $0x1770
  80007d:	68 00 30 80 00       	push   $0x803000
  800082:	e8 ac ff ff ff       	call   800033 <sum>
  800087:	83 c4 10             	add    $0x10,%esp
  80008a:	3d 9e 98 0f 00       	cmp    $0xf989e,%eax
  80008f:	74 18                	je     8000a9 <umain+0x4d>
		cprintf("init: data is not initialized: got sum %08x wanted %08x\n",
  800091:	83 ec 04             	sub    $0x4,%esp
  800094:	68 9e 98 0f 00       	push   $0xf989e
  800099:	50                   	push   %eax
  80009a:	68 08 26 80 00       	push   $0x802608
  80009f:	e8 29 04 00 00       	call   8004cd <cprintf>
  8000a4:	83 c4 10             	add    $0x10,%esp
  8000a7:	eb 10                	jmp    8000b9 <umain+0x5d>
			x, want);
	else
		cprintf("init: data seems okay\n");
  8000a9:	83 ec 0c             	sub    $0xc,%esp
  8000ac:	68 4f 25 80 00       	push   $0x80254f
  8000b1:	e8 17 04 00 00       	call   8004cd <cprintf>
  8000b6:	83 c4 10             	add    $0x10,%esp
	if ((x = sum(bss, sizeof bss)) != 0)
  8000b9:	83 ec 08             	sub    $0x8,%esp
  8000bc:	68 70 17 00 00       	push   $0x1770
  8000c1:	68 20 50 80 00       	push   $0x805020
  8000c6:	e8 68 ff ff ff       	call   800033 <sum>
  8000cb:	83 c4 10             	add    $0x10,%esp
  8000ce:	85 c0                	test   %eax,%eax
  8000d0:	74 13                	je     8000e5 <umain+0x89>
		cprintf("bss is not initialized: wanted sum 0 got %08x\n", x);
  8000d2:	83 ec 08             	sub    $0x8,%esp
  8000d5:	50                   	push   %eax
  8000d6:	68 44 26 80 00       	push   $0x802644
  8000db:	e8 ed 03 00 00       	call   8004cd <cprintf>
  8000e0:	83 c4 10             	add    $0x10,%esp
  8000e3:	eb 10                	jmp    8000f5 <umain+0x99>
	else
		cprintf("init: bss seems okay\n");
  8000e5:	83 ec 0c             	sub    $0xc,%esp
  8000e8:	68 66 25 80 00       	push   $0x802566
  8000ed:	e8 db 03 00 00       	call   8004cd <cprintf>
  8000f2:	83 c4 10             	add    $0x10,%esp

	// output in one syscall per line to avoid output interleaving 
	strcat(args, "init: args:");
  8000f5:	83 ec 08             	sub    $0x8,%esp
  8000f8:	68 7c 25 80 00       	push   $0x80257c
  8000fd:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  800103:	50                   	push   %eax
  800104:	e8 43 09 00 00       	call   800a4c <strcat>
	for (i = 0; i < argc; i++) {
  800109:	83 c4 10             	add    $0x10,%esp
  80010c:	bb 00 00 00 00       	mov    $0x0,%ebx
		strcat(args, " '");
  800111:	8d b5 e8 fe ff ff    	lea    -0x118(%ebp),%esi
	else
		cprintf("init: bss seems okay\n");

	// output in one syscall per line to avoid output interleaving 
	strcat(args, "init: args:");
	for (i = 0; i < argc; i++) {
  800117:	eb 2c                	jmp    800145 <umain+0xe9>
		strcat(args, " '");
  800119:	83 ec 08             	sub    $0x8,%esp
  80011c:	68 88 25 80 00       	push   $0x802588
  800121:	56                   	push   %esi
  800122:	e8 25 09 00 00       	call   800a4c <strcat>
		strcat(args, argv[i]);
  800127:	83 c4 08             	add    $0x8,%esp
  80012a:	ff 34 9f             	pushl  (%edi,%ebx,4)
  80012d:	56                   	push   %esi
  80012e:	e8 19 09 00 00       	call   800a4c <strcat>
		strcat(args, "'");
  800133:	83 c4 08             	add    $0x8,%esp
  800136:	68 89 25 80 00       	push   $0x802589
  80013b:	56                   	push   %esi
  80013c:	e8 0b 09 00 00       	call   800a4c <strcat>
	else
		cprintf("init: bss seems okay\n");

	// output in one syscall per line to avoid output interleaving 
	strcat(args, "init: args:");
	for (i = 0; i < argc; i++) {
  800141:	43                   	inc    %ebx
  800142:	83 c4 10             	add    $0x10,%esp
  800145:	3b 5d 08             	cmp    0x8(%ebp),%ebx
  800148:	7c cf                	jl     800119 <umain+0xbd>
		strcat(args, " '");
		strcat(args, argv[i]);
		strcat(args, "'");
	}
	cprintf("%s\n", args);
  80014a:	83 ec 08             	sub    $0x8,%esp
  80014d:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  800153:	50                   	push   %eax
  800154:	68 8b 25 80 00       	push   $0x80258b
  800159:	e8 6f 03 00 00       	call   8004cd <cprintf>

	cprintf("init: running sh\n");
  80015e:	c7 04 24 8f 25 80 00 	movl   $0x80258f,(%esp)
  800165:	e8 63 03 00 00       	call   8004cd <cprintf>

	// being run directly from kernel, so no file descriptors open yet
	close(0);
  80016a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800171:	e8 53 10 00 00       	call   8011c9 <close>
	if ((r = opencons()) < 0)
  800176:	e8 c7 01 00 00       	call   800342 <opencons>
  80017b:	83 c4 10             	add    $0x10,%esp
  80017e:	85 c0                	test   %eax,%eax
  800180:	79 12                	jns    800194 <umain+0x138>
		panic("opencons: %e", r);
  800182:	50                   	push   %eax
  800183:	68 a1 25 80 00       	push   $0x8025a1
  800188:	6a 37                	push   $0x37
  80018a:	68 ae 25 80 00       	push   $0x8025ae
  80018f:	e8 61 02 00 00       	call   8003f5 <_panic>
	if (r != 0)
  800194:	85 c0                	test   %eax,%eax
  800196:	74 12                	je     8001aa <umain+0x14e>
		panic("first opencons used fd %d", r);
  800198:	50                   	push   %eax
  800199:	68 ba 25 80 00       	push   $0x8025ba
  80019e:	6a 39                	push   $0x39
  8001a0:	68 ae 25 80 00       	push   $0x8025ae
  8001a5:	e8 4b 02 00 00       	call   8003f5 <_panic>
	if ((r = dup(0, 1)) < 0)
  8001aa:	83 ec 08             	sub    $0x8,%esp
  8001ad:	6a 01                	push   $0x1
  8001af:	6a 00                	push   $0x0
  8001b1:	e8 61 10 00 00       	call   801217 <dup>
  8001b6:	83 c4 10             	add    $0x10,%esp
  8001b9:	85 c0                	test   %eax,%eax
  8001bb:	79 12                	jns    8001cf <umain+0x173>
		panic("dup: %e", r);
  8001bd:	50                   	push   %eax
  8001be:	68 d4 25 80 00       	push   $0x8025d4
  8001c3:	6a 3b                	push   $0x3b
  8001c5:	68 ae 25 80 00       	push   $0x8025ae
  8001ca:	e8 26 02 00 00       	call   8003f5 <_panic>
	while (1) {
		cprintf("init: starting sh\n");
  8001cf:	83 ec 0c             	sub    $0xc,%esp
  8001d2:	68 dc 25 80 00       	push   $0x8025dc
  8001d7:	e8 f1 02 00 00       	call   8004cd <cprintf>
		r = spawnl("/sh", "sh", (char*)0);
  8001dc:	83 c4 0c             	add    $0xc,%esp
  8001df:	6a 00                	push   $0x0
  8001e1:	68 f0 25 80 00       	push   $0x8025f0
  8001e6:	68 ef 25 80 00       	push   $0x8025ef
  8001eb:	e8 8e 1b 00 00       	call   801d7e <spawnl>
		if (r < 0) {
  8001f0:	83 c4 10             	add    $0x10,%esp
  8001f3:	85 c0                	test   %eax,%eax
  8001f5:	79 13                	jns    80020a <umain+0x1ae>
			cprintf("init: spawn sh: %e\n", r);
  8001f7:	83 ec 08             	sub    $0x8,%esp
  8001fa:	50                   	push   %eax
  8001fb:	68 f3 25 80 00       	push   $0x8025f3
  800200:	e8 c8 02 00 00       	call   8004cd <cprintf>
			continue;
  800205:	83 c4 10             	add    $0x10,%esp
  800208:	eb c5                	jmp    8001cf <umain+0x173>
		}
		wait(r);
  80020a:	83 ec 0c             	sub    $0xc,%esp
  80020d:	50                   	push   %eax
  80020e:	e8 1d 1f 00 00       	call   802130 <wait>
  800213:	83 c4 10             	add    $0x10,%esp
  800216:	eb b7                	jmp    8001cf <umain+0x173>

00800218 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  800218:	55                   	push   %ebp
  800219:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  80021b:	b8 00 00 00 00       	mov    $0x0,%eax
  800220:	5d                   	pop    %ebp
  800221:	c3                   	ret    

00800222 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  800222:	55                   	push   %ebp
  800223:	89 e5                	mov    %esp,%ebp
  800225:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  800228:	68 73 26 80 00       	push   $0x802673
  80022d:	ff 75 0c             	pushl  0xc(%ebp)
  800230:	e8 fc 07 00 00       	call   800a31 <strcpy>
	return 0;
}
  800235:	b8 00 00 00 00       	mov    $0x0,%eax
  80023a:	c9                   	leave  
  80023b:	c3                   	ret    

0080023c <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  80023c:	55                   	push   %ebp
  80023d:	89 e5                	mov    %esp,%ebp
  80023f:	57                   	push   %edi
  800240:	56                   	push   %esi
  800241:	53                   	push   %ebx
  800242:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800248:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  80024d:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800253:	eb 2c                	jmp    800281 <devcons_write+0x45>
		m = n - tot;
  800255:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800258:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  80025a:	83 fb 7f             	cmp    $0x7f,%ebx
  80025d:	76 05                	jbe    800264 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  80025f:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  800264:	83 ec 04             	sub    $0x4,%esp
  800267:	53                   	push   %ebx
  800268:	03 45 0c             	add    0xc(%ebp),%eax
  80026b:	50                   	push   %eax
  80026c:	57                   	push   %edi
  80026d:	e8 34 09 00 00       	call   800ba6 <memmove>
		sys_cputs(buf, m);
  800272:	83 c4 08             	add    $0x8,%esp
  800275:	53                   	push   %ebx
  800276:	57                   	push   %edi
  800277:	e8 dd 0a 00 00       	call   800d59 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  80027c:	01 de                	add    %ebx,%esi
  80027e:	83 c4 10             	add    $0x10,%esp
  800281:	89 f0                	mov    %esi,%eax
  800283:	3b 75 10             	cmp    0x10(%ebp),%esi
  800286:	72 cd                	jb     800255 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  800288:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80028b:	5b                   	pop    %ebx
  80028c:	5e                   	pop    %esi
  80028d:	5f                   	pop    %edi
  80028e:	5d                   	pop    %ebp
  80028f:	c3                   	ret    

00800290 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  800290:	55                   	push   %ebp
  800291:	89 e5                	mov    %esp,%ebp
  800293:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  800296:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80029a:	75 07                	jne    8002a3 <devcons_read+0x13>
  80029c:	eb 23                	jmp    8002c1 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  80029e:	e8 53 0b 00 00       	call   800df6 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  8002a3:	e8 cf 0a 00 00       	call   800d77 <sys_cgetc>
  8002a8:	85 c0                	test   %eax,%eax
  8002aa:	74 f2                	je     80029e <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  8002ac:	85 c0                	test   %eax,%eax
  8002ae:	78 1d                	js     8002cd <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  8002b0:	83 f8 04             	cmp    $0x4,%eax
  8002b3:	74 13                	je     8002c8 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  8002b5:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002b8:	88 02                	mov    %al,(%edx)
	return 1;
  8002ba:	b8 01 00 00 00       	mov    $0x1,%eax
  8002bf:	eb 0c                	jmp    8002cd <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  8002c1:	b8 00 00 00 00       	mov    $0x0,%eax
  8002c6:	eb 05                	jmp    8002cd <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  8002c8:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  8002cd:	c9                   	leave  
  8002ce:	c3                   	ret    

008002cf <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  8002cf:	55                   	push   %ebp
  8002d0:	89 e5                	mov    %esp,%ebp
  8002d2:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  8002d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8002d8:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  8002db:	6a 01                	push   $0x1
  8002dd:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8002e0:	50                   	push   %eax
  8002e1:	e8 73 0a 00 00       	call   800d59 <sys_cputs>
}
  8002e6:	83 c4 10             	add    $0x10,%esp
  8002e9:	c9                   	leave  
  8002ea:	c3                   	ret    

008002eb <getchar>:

int
getchar(void)
{
  8002eb:	55                   	push   %ebp
  8002ec:	89 e5                	mov    %esp,%ebp
  8002ee:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  8002f1:	6a 01                	push   $0x1
  8002f3:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8002f6:	50                   	push   %eax
  8002f7:	6a 00                	push   $0x0
  8002f9:	e8 03 10 00 00       	call   801301 <read>
	if (r < 0)
  8002fe:	83 c4 10             	add    $0x10,%esp
  800301:	85 c0                	test   %eax,%eax
  800303:	78 0f                	js     800314 <getchar+0x29>
		return r;
	if (r < 1)
  800305:	85 c0                	test   %eax,%eax
  800307:	7e 06                	jle    80030f <getchar+0x24>
		return -E_EOF;
	return c;
  800309:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  80030d:	eb 05                	jmp    800314 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  80030f:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  800314:	c9                   	leave  
  800315:	c3                   	ret    

00800316 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  800316:	55                   	push   %ebp
  800317:	89 e5                	mov    %esp,%ebp
  800319:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80031c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80031f:	50                   	push   %eax
  800320:	ff 75 08             	pushl  0x8(%ebp)
  800323:	e8 73 0d 00 00       	call   80109b <fd_lookup>
  800328:	83 c4 10             	add    $0x10,%esp
  80032b:	85 c0                	test   %eax,%eax
  80032d:	78 11                	js     800340 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  80032f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800332:	8b 15 70 47 80 00    	mov    0x804770,%edx
  800338:	39 10                	cmp    %edx,(%eax)
  80033a:	0f 94 c0             	sete   %al
  80033d:	0f b6 c0             	movzbl %al,%eax
}
  800340:	c9                   	leave  
  800341:	c3                   	ret    

00800342 <opencons>:

int
opencons(void)
{
  800342:	55                   	push   %ebp
  800343:	89 e5                	mov    %esp,%ebp
  800345:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  800348:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80034b:	50                   	push   %eax
  80034c:	e8 fb 0c 00 00       	call   80104c <fd_alloc>
  800351:	83 c4 10             	add    $0x10,%esp
  800354:	85 c0                	test   %eax,%eax
  800356:	78 3a                	js     800392 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  800358:	83 ec 04             	sub    $0x4,%esp
  80035b:	68 07 04 00 00       	push   $0x407
  800360:	ff 75 f4             	pushl  -0xc(%ebp)
  800363:	6a 00                	push   $0x0
  800365:	e8 ab 0a 00 00       	call   800e15 <sys_page_alloc>
  80036a:	83 c4 10             	add    $0x10,%esp
  80036d:	85 c0                	test   %eax,%eax
  80036f:	78 21                	js     800392 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  800371:	8b 15 70 47 80 00    	mov    0x804770,%edx
  800377:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80037a:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  80037c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80037f:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  800386:	83 ec 0c             	sub    $0xc,%esp
  800389:	50                   	push   %eax
  80038a:	e8 96 0c 00 00       	call   801025 <fd2num>
  80038f:	83 c4 10             	add    $0x10,%esp
}
  800392:	c9                   	leave  
  800393:	c3                   	ret    

00800394 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800394:	55                   	push   %ebp
  800395:	89 e5                	mov    %esp,%ebp
  800397:	56                   	push   %esi
  800398:	53                   	push   %ebx
  800399:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80039c:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80039f:	e8 33 0a 00 00       	call   800dd7 <sys_getenvid>
  8003a4:	25 ff 03 00 00       	and    $0x3ff,%eax
  8003a9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8003b0:	c1 e0 07             	shl    $0x7,%eax
  8003b3:	29 d0                	sub    %edx,%eax
  8003b5:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8003ba:	a3 90 67 80 00       	mov    %eax,0x806790
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8003bf:	85 db                	test   %ebx,%ebx
  8003c1:	7e 07                	jle    8003ca <libmain+0x36>
		binaryname = argv[0];
  8003c3:	8b 06                	mov    (%esi),%eax
  8003c5:	a3 8c 47 80 00       	mov    %eax,0x80478c

	// call user main routine
	umain(argc, argv);
  8003ca:	83 ec 08             	sub    $0x8,%esp
  8003cd:	56                   	push   %esi
  8003ce:	53                   	push   %ebx
  8003cf:	e8 88 fc ff ff       	call   80005c <umain>

	// exit gracefully
	exit();
  8003d4:	e8 0a 00 00 00       	call   8003e3 <exit>
}
  8003d9:	83 c4 10             	add    $0x10,%esp
  8003dc:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8003df:	5b                   	pop    %ebx
  8003e0:	5e                   	pop    %esi
  8003e1:	5d                   	pop    %ebp
  8003e2:	c3                   	ret    

008003e3 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8003e3:	55                   	push   %ebp
  8003e4:	89 e5                	mov    %esp,%ebp
  8003e6:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8003e9:	6a 00                	push   $0x0
  8003eb:	e8 a6 09 00 00       	call   800d96 <sys_env_destroy>
}
  8003f0:	83 c4 10             	add    $0x10,%esp
  8003f3:	c9                   	leave  
  8003f4:	c3                   	ret    

008003f5 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8003f5:	55                   	push   %ebp
  8003f6:	89 e5                	mov    %esp,%ebp
  8003f8:	56                   	push   %esi
  8003f9:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8003fa:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8003fd:	8b 35 8c 47 80 00    	mov    0x80478c,%esi
  800403:	e8 cf 09 00 00       	call   800dd7 <sys_getenvid>
  800408:	83 ec 0c             	sub    $0xc,%esp
  80040b:	ff 75 0c             	pushl  0xc(%ebp)
  80040e:	ff 75 08             	pushl  0x8(%ebp)
  800411:	56                   	push   %esi
  800412:	50                   	push   %eax
  800413:	68 8c 26 80 00       	push   $0x80268c
  800418:	e8 b0 00 00 00       	call   8004cd <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80041d:	83 c4 18             	add    $0x18,%esp
  800420:	53                   	push   %ebx
  800421:	ff 75 10             	pushl  0x10(%ebp)
  800424:	e8 53 00 00 00       	call   80047c <vcprintf>
	cprintf("\n");
  800429:	c7 04 24 78 2b 80 00 	movl   $0x802b78,(%esp)
  800430:	e8 98 00 00 00       	call   8004cd <cprintf>
  800435:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800438:	cc                   	int3   
  800439:	eb fd                	jmp    800438 <_panic+0x43>

0080043b <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80043b:	55                   	push   %ebp
  80043c:	89 e5                	mov    %esp,%ebp
  80043e:	53                   	push   %ebx
  80043f:	83 ec 04             	sub    $0x4,%esp
  800442:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800445:	8b 13                	mov    (%ebx),%edx
  800447:	8d 42 01             	lea    0x1(%edx),%eax
  80044a:	89 03                	mov    %eax,(%ebx)
  80044c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80044f:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800453:	3d ff 00 00 00       	cmp    $0xff,%eax
  800458:	75 1a                	jne    800474 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80045a:	83 ec 08             	sub    $0x8,%esp
  80045d:	68 ff 00 00 00       	push   $0xff
  800462:	8d 43 08             	lea    0x8(%ebx),%eax
  800465:	50                   	push   %eax
  800466:	e8 ee 08 00 00       	call   800d59 <sys_cputs>
		b->idx = 0;
  80046b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800471:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800474:	ff 43 04             	incl   0x4(%ebx)
}
  800477:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80047a:	c9                   	leave  
  80047b:	c3                   	ret    

0080047c <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80047c:	55                   	push   %ebp
  80047d:	89 e5                	mov    %esp,%ebp
  80047f:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800485:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80048c:	00 00 00 
	b.cnt = 0;
  80048f:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800496:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800499:	ff 75 0c             	pushl  0xc(%ebp)
  80049c:	ff 75 08             	pushl  0x8(%ebp)
  80049f:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8004a5:	50                   	push   %eax
  8004a6:	68 3b 04 80 00       	push   $0x80043b
  8004ab:	e8 51 01 00 00       	call   800601 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8004b0:	83 c4 08             	add    $0x8,%esp
  8004b3:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8004b9:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8004bf:	50                   	push   %eax
  8004c0:	e8 94 08 00 00       	call   800d59 <sys_cputs>

	return b.cnt;
}
  8004c5:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8004cb:	c9                   	leave  
  8004cc:	c3                   	ret    

008004cd <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8004cd:	55                   	push   %ebp
  8004ce:	89 e5                	mov    %esp,%ebp
  8004d0:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8004d3:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8004d6:	50                   	push   %eax
  8004d7:	ff 75 08             	pushl  0x8(%ebp)
  8004da:	e8 9d ff ff ff       	call   80047c <vcprintf>
	va_end(ap);

	return cnt;
}
  8004df:	c9                   	leave  
  8004e0:	c3                   	ret    

008004e1 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8004e1:	55                   	push   %ebp
  8004e2:	89 e5                	mov    %esp,%ebp
  8004e4:	57                   	push   %edi
  8004e5:	56                   	push   %esi
  8004e6:	53                   	push   %ebx
  8004e7:	83 ec 1c             	sub    $0x1c,%esp
  8004ea:	89 c7                	mov    %eax,%edi
  8004ec:	89 d6                	mov    %edx,%esi
  8004ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8004f1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8004f4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004f7:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8004fa:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8004fd:	bb 00 00 00 00       	mov    $0x0,%ebx
  800502:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800505:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800508:	39 d3                	cmp    %edx,%ebx
  80050a:	72 05                	jb     800511 <printnum+0x30>
  80050c:	39 45 10             	cmp    %eax,0x10(%ebp)
  80050f:	77 45                	ja     800556 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800511:	83 ec 0c             	sub    $0xc,%esp
  800514:	ff 75 18             	pushl  0x18(%ebp)
  800517:	8b 45 14             	mov    0x14(%ebp),%eax
  80051a:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80051d:	53                   	push   %ebx
  80051e:	ff 75 10             	pushl  0x10(%ebp)
  800521:	83 ec 08             	sub    $0x8,%esp
  800524:	ff 75 e4             	pushl  -0x1c(%ebp)
  800527:	ff 75 e0             	pushl  -0x20(%ebp)
  80052a:	ff 75 dc             	pushl  -0x24(%ebp)
  80052d:	ff 75 d8             	pushl  -0x28(%ebp)
  800530:	e8 97 1d 00 00       	call   8022cc <__udivdi3>
  800535:	83 c4 18             	add    $0x18,%esp
  800538:	52                   	push   %edx
  800539:	50                   	push   %eax
  80053a:	89 f2                	mov    %esi,%edx
  80053c:	89 f8                	mov    %edi,%eax
  80053e:	e8 9e ff ff ff       	call   8004e1 <printnum>
  800543:	83 c4 20             	add    $0x20,%esp
  800546:	eb 16                	jmp    80055e <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800548:	83 ec 08             	sub    $0x8,%esp
  80054b:	56                   	push   %esi
  80054c:	ff 75 18             	pushl  0x18(%ebp)
  80054f:	ff d7                	call   *%edi
  800551:	83 c4 10             	add    $0x10,%esp
  800554:	eb 03                	jmp    800559 <printnum+0x78>
  800556:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800559:	4b                   	dec    %ebx
  80055a:	85 db                	test   %ebx,%ebx
  80055c:	7f ea                	jg     800548 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80055e:	83 ec 08             	sub    $0x8,%esp
  800561:	56                   	push   %esi
  800562:	83 ec 04             	sub    $0x4,%esp
  800565:	ff 75 e4             	pushl  -0x1c(%ebp)
  800568:	ff 75 e0             	pushl  -0x20(%ebp)
  80056b:	ff 75 dc             	pushl  -0x24(%ebp)
  80056e:	ff 75 d8             	pushl  -0x28(%ebp)
  800571:	e8 66 1e 00 00       	call   8023dc <__umoddi3>
  800576:	83 c4 14             	add    $0x14,%esp
  800579:	0f be 80 af 26 80 00 	movsbl 0x8026af(%eax),%eax
  800580:	50                   	push   %eax
  800581:	ff d7                	call   *%edi
}
  800583:	83 c4 10             	add    $0x10,%esp
  800586:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800589:	5b                   	pop    %ebx
  80058a:	5e                   	pop    %esi
  80058b:	5f                   	pop    %edi
  80058c:	5d                   	pop    %ebp
  80058d:	c3                   	ret    

0080058e <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80058e:	55                   	push   %ebp
  80058f:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800591:	83 fa 01             	cmp    $0x1,%edx
  800594:	7e 0e                	jle    8005a4 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800596:	8b 10                	mov    (%eax),%edx
  800598:	8d 4a 08             	lea    0x8(%edx),%ecx
  80059b:	89 08                	mov    %ecx,(%eax)
  80059d:	8b 02                	mov    (%edx),%eax
  80059f:	8b 52 04             	mov    0x4(%edx),%edx
  8005a2:	eb 22                	jmp    8005c6 <getuint+0x38>
	else if (lflag)
  8005a4:	85 d2                	test   %edx,%edx
  8005a6:	74 10                	je     8005b8 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8005a8:	8b 10                	mov    (%eax),%edx
  8005aa:	8d 4a 04             	lea    0x4(%edx),%ecx
  8005ad:	89 08                	mov    %ecx,(%eax)
  8005af:	8b 02                	mov    (%edx),%eax
  8005b1:	ba 00 00 00 00       	mov    $0x0,%edx
  8005b6:	eb 0e                	jmp    8005c6 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8005b8:	8b 10                	mov    (%eax),%edx
  8005ba:	8d 4a 04             	lea    0x4(%edx),%ecx
  8005bd:	89 08                	mov    %ecx,(%eax)
  8005bf:	8b 02                	mov    (%edx),%eax
  8005c1:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8005c6:	5d                   	pop    %ebp
  8005c7:	c3                   	ret    

008005c8 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8005c8:	55                   	push   %ebp
  8005c9:	89 e5                	mov    %esp,%ebp
  8005cb:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8005ce:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8005d1:	8b 10                	mov    (%eax),%edx
  8005d3:	3b 50 04             	cmp    0x4(%eax),%edx
  8005d6:	73 0a                	jae    8005e2 <sprintputch+0x1a>
		*b->buf++ = ch;
  8005d8:	8d 4a 01             	lea    0x1(%edx),%ecx
  8005db:	89 08                	mov    %ecx,(%eax)
  8005dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8005e0:	88 02                	mov    %al,(%edx)
}
  8005e2:	5d                   	pop    %ebp
  8005e3:	c3                   	ret    

008005e4 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8005e4:	55                   	push   %ebp
  8005e5:	89 e5                	mov    %esp,%ebp
  8005e7:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8005ea:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8005ed:	50                   	push   %eax
  8005ee:	ff 75 10             	pushl  0x10(%ebp)
  8005f1:	ff 75 0c             	pushl  0xc(%ebp)
  8005f4:	ff 75 08             	pushl  0x8(%ebp)
  8005f7:	e8 05 00 00 00       	call   800601 <vprintfmt>
	va_end(ap);
}
  8005fc:	83 c4 10             	add    $0x10,%esp
  8005ff:	c9                   	leave  
  800600:	c3                   	ret    

00800601 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800601:	55                   	push   %ebp
  800602:	89 e5                	mov    %esp,%ebp
  800604:	57                   	push   %edi
  800605:	56                   	push   %esi
  800606:	53                   	push   %ebx
  800607:	83 ec 2c             	sub    $0x2c,%esp
  80060a:	8b 75 08             	mov    0x8(%ebp),%esi
  80060d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800610:	8b 7d 10             	mov    0x10(%ebp),%edi
  800613:	eb 12                	jmp    800627 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800615:	85 c0                	test   %eax,%eax
  800617:	0f 84 68 03 00 00    	je     800985 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80061d:	83 ec 08             	sub    $0x8,%esp
  800620:	53                   	push   %ebx
  800621:	50                   	push   %eax
  800622:	ff d6                	call   *%esi
  800624:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800627:	47                   	inc    %edi
  800628:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80062c:	83 f8 25             	cmp    $0x25,%eax
  80062f:	75 e4                	jne    800615 <vprintfmt+0x14>
  800631:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800635:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80063c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800643:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80064a:	ba 00 00 00 00       	mov    $0x0,%edx
  80064f:	eb 07                	jmp    800658 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800651:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800654:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800658:	8d 47 01             	lea    0x1(%edi),%eax
  80065b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80065e:	0f b6 0f             	movzbl (%edi),%ecx
  800661:	8a 07                	mov    (%edi),%al
  800663:	83 e8 23             	sub    $0x23,%eax
  800666:	3c 55                	cmp    $0x55,%al
  800668:	0f 87 fe 02 00 00    	ja     80096c <vprintfmt+0x36b>
  80066e:	0f b6 c0             	movzbl %al,%eax
  800671:	ff 24 85 00 28 80 00 	jmp    *0x802800(,%eax,4)
  800678:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80067b:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80067f:	eb d7                	jmp    800658 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800681:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800684:	b8 00 00 00 00       	mov    $0x0,%eax
  800689:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80068c:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80068f:	01 c0                	add    %eax,%eax
  800691:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800695:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800698:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80069b:	83 fa 09             	cmp    $0x9,%edx
  80069e:	77 34                	ja     8006d4 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8006a0:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8006a1:	eb e9                	jmp    80068c <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8006a3:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a6:	8d 48 04             	lea    0x4(%eax),%ecx
  8006a9:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8006ac:	8b 00                	mov    (%eax),%eax
  8006ae:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006b1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8006b4:	eb 24                	jmp    8006da <vprintfmt+0xd9>
  8006b6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006ba:	79 07                	jns    8006c3 <vprintfmt+0xc2>
  8006bc:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006c6:	eb 90                	jmp    800658 <vprintfmt+0x57>
  8006c8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8006cb:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8006d2:	eb 84                	jmp    800658 <vprintfmt+0x57>
  8006d4:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8006d7:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8006da:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006de:	0f 89 74 ff ff ff    	jns    800658 <vprintfmt+0x57>
				width = precision, precision = -1;
  8006e4:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8006e7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006ea:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8006f1:	e9 62 ff ff ff       	jmp    800658 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8006f6:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006f7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8006fa:	e9 59 ff ff ff       	jmp    800658 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8006ff:	8b 45 14             	mov    0x14(%ebp),%eax
  800702:	8d 50 04             	lea    0x4(%eax),%edx
  800705:	89 55 14             	mov    %edx,0x14(%ebp)
  800708:	83 ec 08             	sub    $0x8,%esp
  80070b:	53                   	push   %ebx
  80070c:	ff 30                	pushl  (%eax)
  80070e:	ff d6                	call   *%esi
			break;
  800710:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800713:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800716:	e9 0c ff ff ff       	jmp    800627 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80071b:	8b 45 14             	mov    0x14(%ebp),%eax
  80071e:	8d 50 04             	lea    0x4(%eax),%edx
  800721:	89 55 14             	mov    %edx,0x14(%ebp)
  800724:	8b 00                	mov    (%eax),%eax
  800726:	85 c0                	test   %eax,%eax
  800728:	79 02                	jns    80072c <vprintfmt+0x12b>
  80072a:	f7 d8                	neg    %eax
  80072c:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80072e:	83 f8 0f             	cmp    $0xf,%eax
  800731:	7f 0b                	jg     80073e <vprintfmt+0x13d>
  800733:	8b 04 85 60 29 80 00 	mov    0x802960(,%eax,4),%eax
  80073a:	85 c0                	test   %eax,%eax
  80073c:	75 18                	jne    800756 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80073e:	52                   	push   %edx
  80073f:	68 c7 26 80 00       	push   $0x8026c7
  800744:	53                   	push   %ebx
  800745:	56                   	push   %esi
  800746:	e8 99 fe ff ff       	call   8005e4 <printfmt>
  80074b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80074e:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800751:	e9 d1 fe ff ff       	jmp    800627 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800756:	50                   	push   %eax
  800757:	68 91 2a 80 00       	push   $0x802a91
  80075c:	53                   	push   %ebx
  80075d:	56                   	push   %esi
  80075e:	e8 81 fe ff ff       	call   8005e4 <printfmt>
  800763:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800766:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800769:	e9 b9 fe ff ff       	jmp    800627 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80076e:	8b 45 14             	mov    0x14(%ebp),%eax
  800771:	8d 50 04             	lea    0x4(%eax),%edx
  800774:	89 55 14             	mov    %edx,0x14(%ebp)
  800777:	8b 38                	mov    (%eax),%edi
  800779:	85 ff                	test   %edi,%edi
  80077b:	75 05                	jne    800782 <vprintfmt+0x181>
				p = "(null)";
  80077d:	bf c0 26 80 00       	mov    $0x8026c0,%edi
			if (width > 0 && padc != '-')
  800782:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800786:	0f 8e 90 00 00 00    	jle    80081c <vprintfmt+0x21b>
  80078c:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800790:	0f 84 8e 00 00 00    	je     800824 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800796:	83 ec 08             	sub    $0x8,%esp
  800799:	ff 75 d0             	pushl  -0x30(%ebp)
  80079c:	57                   	push   %edi
  80079d:	e8 70 02 00 00       	call   800a12 <strnlen>
  8007a2:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8007a5:	29 c1                	sub    %eax,%ecx
  8007a7:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8007aa:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8007ad:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8007b1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8007b4:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8007b7:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8007b9:	eb 0d                	jmp    8007c8 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8007bb:	83 ec 08             	sub    $0x8,%esp
  8007be:	53                   	push   %ebx
  8007bf:	ff 75 e4             	pushl  -0x1c(%ebp)
  8007c2:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8007c4:	4f                   	dec    %edi
  8007c5:	83 c4 10             	add    $0x10,%esp
  8007c8:	85 ff                	test   %edi,%edi
  8007ca:	7f ef                	jg     8007bb <vprintfmt+0x1ba>
  8007cc:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8007cf:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8007d2:	89 c8                	mov    %ecx,%eax
  8007d4:	85 c9                	test   %ecx,%ecx
  8007d6:	79 05                	jns    8007dd <vprintfmt+0x1dc>
  8007d8:	b8 00 00 00 00       	mov    $0x0,%eax
  8007dd:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8007e0:	29 c1                	sub    %eax,%ecx
  8007e2:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8007e5:	89 75 08             	mov    %esi,0x8(%ebp)
  8007e8:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007eb:	eb 3d                	jmp    80082a <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8007ed:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8007f1:	74 19                	je     80080c <vprintfmt+0x20b>
  8007f3:	0f be c0             	movsbl %al,%eax
  8007f6:	83 e8 20             	sub    $0x20,%eax
  8007f9:	83 f8 5e             	cmp    $0x5e,%eax
  8007fc:	76 0e                	jbe    80080c <vprintfmt+0x20b>
					putch('?', putdat);
  8007fe:	83 ec 08             	sub    $0x8,%esp
  800801:	53                   	push   %ebx
  800802:	6a 3f                	push   $0x3f
  800804:	ff 55 08             	call   *0x8(%ebp)
  800807:	83 c4 10             	add    $0x10,%esp
  80080a:	eb 0b                	jmp    800817 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80080c:	83 ec 08             	sub    $0x8,%esp
  80080f:	53                   	push   %ebx
  800810:	52                   	push   %edx
  800811:	ff 55 08             	call   *0x8(%ebp)
  800814:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800817:	ff 4d e4             	decl   -0x1c(%ebp)
  80081a:	eb 0e                	jmp    80082a <vprintfmt+0x229>
  80081c:	89 75 08             	mov    %esi,0x8(%ebp)
  80081f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800822:	eb 06                	jmp    80082a <vprintfmt+0x229>
  800824:	89 75 08             	mov    %esi,0x8(%ebp)
  800827:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80082a:	47                   	inc    %edi
  80082b:	8a 47 ff             	mov    -0x1(%edi),%al
  80082e:	0f be d0             	movsbl %al,%edx
  800831:	85 d2                	test   %edx,%edx
  800833:	74 1d                	je     800852 <vprintfmt+0x251>
  800835:	85 f6                	test   %esi,%esi
  800837:	78 b4                	js     8007ed <vprintfmt+0x1ec>
  800839:	4e                   	dec    %esi
  80083a:	79 b1                	jns    8007ed <vprintfmt+0x1ec>
  80083c:	8b 75 08             	mov    0x8(%ebp),%esi
  80083f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800842:	eb 14                	jmp    800858 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800844:	83 ec 08             	sub    $0x8,%esp
  800847:	53                   	push   %ebx
  800848:	6a 20                	push   $0x20
  80084a:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80084c:	4f                   	dec    %edi
  80084d:	83 c4 10             	add    $0x10,%esp
  800850:	eb 06                	jmp    800858 <vprintfmt+0x257>
  800852:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800855:	8b 75 08             	mov    0x8(%ebp),%esi
  800858:	85 ff                	test   %edi,%edi
  80085a:	7f e8                	jg     800844 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80085c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80085f:	e9 c3 fd ff ff       	jmp    800627 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800864:	83 fa 01             	cmp    $0x1,%edx
  800867:	7e 16                	jle    80087f <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800869:	8b 45 14             	mov    0x14(%ebp),%eax
  80086c:	8d 50 08             	lea    0x8(%eax),%edx
  80086f:	89 55 14             	mov    %edx,0x14(%ebp)
  800872:	8b 50 04             	mov    0x4(%eax),%edx
  800875:	8b 00                	mov    (%eax),%eax
  800877:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80087a:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80087d:	eb 32                	jmp    8008b1 <vprintfmt+0x2b0>
	else if (lflag)
  80087f:	85 d2                	test   %edx,%edx
  800881:	74 18                	je     80089b <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800883:	8b 45 14             	mov    0x14(%ebp),%eax
  800886:	8d 50 04             	lea    0x4(%eax),%edx
  800889:	89 55 14             	mov    %edx,0x14(%ebp)
  80088c:	8b 00                	mov    (%eax),%eax
  80088e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800891:	89 c1                	mov    %eax,%ecx
  800893:	c1 f9 1f             	sar    $0x1f,%ecx
  800896:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800899:	eb 16                	jmp    8008b1 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80089b:	8b 45 14             	mov    0x14(%ebp),%eax
  80089e:	8d 50 04             	lea    0x4(%eax),%edx
  8008a1:	89 55 14             	mov    %edx,0x14(%ebp)
  8008a4:	8b 00                	mov    (%eax),%eax
  8008a6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8008a9:	89 c1                	mov    %eax,%ecx
  8008ab:	c1 f9 1f             	sar    $0x1f,%ecx
  8008ae:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8008b1:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8008b4:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8008b7:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8008bb:	79 76                	jns    800933 <vprintfmt+0x332>
				putch('-', putdat);
  8008bd:	83 ec 08             	sub    $0x8,%esp
  8008c0:	53                   	push   %ebx
  8008c1:	6a 2d                	push   $0x2d
  8008c3:	ff d6                	call   *%esi
				num = -(long long) num;
  8008c5:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8008c8:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8008cb:	f7 d8                	neg    %eax
  8008cd:	83 d2 00             	adc    $0x0,%edx
  8008d0:	f7 da                	neg    %edx
  8008d2:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8008d5:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8008da:	eb 5c                	jmp    800938 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8008dc:	8d 45 14             	lea    0x14(%ebp),%eax
  8008df:	e8 aa fc ff ff       	call   80058e <getuint>
			base = 10;
  8008e4:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8008e9:	eb 4d                	jmp    800938 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8008eb:	8d 45 14             	lea    0x14(%ebp),%eax
  8008ee:	e8 9b fc ff ff       	call   80058e <getuint>
			base = 8;
  8008f3:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8008f8:	eb 3e                	jmp    800938 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8008fa:	83 ec 08             	sub    $0x8,%esp
  8008fd:	53                   	push   %ebx
  8008fe:	6a 30                	push   $0x30
  800900:	ff d6                	call   *%esi
			putch('x', putdat);
  800902:	83 c4 08             	add    $0x8,%esp
  800905:	53                   	push   %ebx
  800906:	6a 78                	push   $0x78
  800908:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80090a:	8b 45 14             	mov    0x14(%ebp),%eax
  80090d:	8d 50 04             	lea    0x4(%eax),%edx
  800910:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800913:	8b 00                	mov    (%eax),%eax
  800915:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80091a:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80091d:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800922:	eb 14                	jmp    800938 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800924:	8d 45 14             	lea    0x14(%ebp),%eax
  800927:	e8 62 fc ff ff       	call   80058e <getuint>
			base = 16;
  80092c:	b9 10 00 00 00       	mov    $0x10,%ecx
  800931:	eb 05                	jmp    800938 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800933:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800938:	83 ec 0c             	sub    $0xc,%esp
  80093b:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80093f:	57                   	push   %edi
  800940:	ff 75 e4             	pushl  -0x1c(%ebp)
  800943:	51                   	push   %ecx
  800944:	52                   	push   %edx
  800945:	50                   	push   %eax
  800946:	89 da                	mov    %ebx,%edx
  800948:	89 f0                	mov    %esi,%eax
  80094a:	e8 92 fb ff ff       	call   8004e1 <printnum>
			break;
  80094f:	83 c4 20             	add    $0x20,%esp
  800952:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800955:	e9 cd fc ff ff       	jmp    800627 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80095a:	83 ec 08             	sub    $0x8,%esp
  80095d:	53                   	push   %ebx
  80095e:	51                   	push   %ecx
  80095f:	ff d6                	call   *%esi
			break;
  800961:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800964:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800967:	e9 bb fc ff ff       	jmp    800627 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80096c:	83 ec 08             	sub    $0x8,%esp
  80096f:	53                   	push   %ebx
  800970:	6a 25                	push   $0x25
  800972:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800974:	83 c4 10             	add    $0x10,%esp
  800977:	eb 01                	jmp    80097a <vprintfmt+0x379>
  800979:	4f                   	dec    %edi
  80097a:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80097e:	75 f9                	jne    800979 <vprintfmt+0x378>
  800980:	e9 a2 fc ff ff       	jmp    800627 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800985:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800988:	5b                   	pop    %ebx
  800989:	5e                   	pop    %esi
  80098a:	5f                   	pop    %edi
  80098b:	5d                   	pop    %ebp
  80098c:	c3                   	ret    

0080098d <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80098d:	55                   	push   %ebp
  80098e:	89 e5                	mov    %esp,%ebp
  800990:	83 ec 18             	sub    $0x18,%esp
  800993:	8b 45 08             	mov    0x8(%ebp),%eax
  800996:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800999:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80099c:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8009a0:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8009a3:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8009aa:	85 c0                	test   %eax,%eax
  8009ac:	74 26                	je     8009d4 <vsnprintf+0x47>
  8009ae:	85 d2                	test   %edx,%edx
  8009b0:	7e 29                	jle    8009db <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8009b2:	ff 75 14             	pushl  0x14(%ebp)
  8009b5:	ff 75 10             	pushl  0x10(%ebp)
  8009b8:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8009bb:	50                   	push   %eax
  8009bc:	68 c8 05 80 00       	push   $0x8005c8
  8009c1:	e8 3b fc ff ff       	call   800601 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8009c6:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8009c9:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8009cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8009cf:	83 c4 10             	add    $0x10,%esp
  8009d2:	eb 0c                	jmp    8009e0 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8009d4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8009d9:	eb 05                	jmp    8009e0 <vsnprintf+0x53>
  8009db:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8009e0:	c9                   	leave  
  8009e1:	c3                   	ret    

008009e2 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8009e2:	55                   	push   %ebp
  8009e3:	89 e5                	mov    %esp,%ebp
  8009e5:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8009e8:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8009eb:	50                   	push   %eax
  8009ec:	ff 75 10             	pushl  0x10(%ebp)
  8009ef:	ff 75 0c             	pushl  0xc(%ebp)
  8009f2:	ff 75 08             	pushl  0x8(%ebp)
  8009f5:	e8 93 ff ff ff       	call   80098d <vsnprintf>
	va_end(ap);

	return rc;
}
  8009fa:	c9                   	leave  
  8009fb:	c3                   	ret    

008009fc <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8009fc:	55                   	push   %ebp
  8009fd:	89 e5                	mov    %esp,%ebp
  8009ff:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800a02:	b8 00 00 00 00       	mov    $0x0,%eax
  800a07:	eb 01                	jmp    800a0a <strlen+0xe>
		n++;
  800a09:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800a0a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800a0e:	75 f9                	jne    800a09 <strlen+0xd>
		n++;
	return n;
}
  800a10:	5d                   	pop    %ebp
  800a11:	c3                   	ret    

00800a12 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800a12:	55                   	push   %ebp
  800a13:	89 e5                	mov    %esp,%ebp
  800a15:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a18:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800a1b:	ba 00 00 00 00       	mov    $0x0,%edx
  800a20:	eb 01                	jmp    800a23 <strnlen+0x11>
		n++;
  800a22:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800a23:	39 c2                	cmp    %eax,%edx
  800a25:	74 08                	je     800a2f <strnlen+0x1d>
  800a27:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800a2b:	75 f5                	jne    800a22 <strnlen+0x10>
  800a2d:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800a2f:	5d                   	pop    %ebp
  800a30:	c3                   	ret    

00800a31 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800a31:	55                   	push   %ebp
  800a32:	89 e5                	mov    %esp,%ebp
  800a34:	53                   	push   %ebx
  800a35:	8b 45 08             	mov    0x8(%ebp),%eax
  800a38:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800a3b:	89 c2                	mov    %eax,%edx
  800a3d:	42                   	inc    %edx
  800a3e:	41                   	inc    %ecx
  800a3f:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800a42:	88 5a ff             	mov    %bl,-0x1(%edx)
  800a45:	84 db                	test   %bl,%bl
  800a47:	75 f4                	jne    800a3d <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800a49:	5b                   	pop    %ebx
  800a4a:	5d                   	pop    %ebp
  800a4b:	c3                   	ret    

00800a4c <strcat>:

char *
strcat(char *dst, const char *src)
{
  800a4c:	55                   	push   %ebp
  800a4d:	89 e5                	mov    %esp,%ebp
  800a4f:	53                   	push   %ebx
  800a50:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800a53:	53                   	push   %ebx
  800a54:	e8 a3 ff ff ff       	call   8009fc <strlen>
  800a59:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800a5c:	ff 75 0c             	pushl  0xc(%ebp)
  800a5f:	01 d8                	add    %ebx,%eax
  800a61:	50                   	push   %eax
  800a62:	e8 ca ff ff ff       	call   800a31 <strcpy>
	return dst;
}
  800a67:	89 d8                	mov    %ebx,%eax
  800a69:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800a6c:	c9                   	leave  
  800a6d:	c3                   	ret    

00800a6e <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800a6e:	55                   	push   %ebp
  800a6f:	89 e5                	mov    %esp,%ebp
  800a71:	56                   	push   %esi
  800a72:	53                   	push   %ebx
  800a73:	8b 75 08             	mov    0x8(%ebp),%esi
  800a76:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a79:	89 f3                	mov    %esi,%ebx
  800a7b:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a7e:	89 f2                	mov    %esi,%edx
  800a80:	eb 0c                	jmp    800a8e <strncpy+0x20>
		*dst++ = *src;
  800a82:	42                   	inc    %edx
  800a83:	8a 01                	mov    (%ecx),%al
  800a85:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a88:	80 39 01             	cmpb   $0x1,(%ecx)
  800a8b:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a8e:	39 da                	cmp    %ebx,%edx
  800a90:	75 f0                	jne    800a82 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a92:	89 f0                	mov    %esi,%eax
  800a94:	5b                   	pop    %ebx
  800a95:	5e                   	pop    %esi
  800a96:	5d                   	pop    %ebp
  800a97:	c3                   	ret    

00800a98 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a98:	55                   	push   %ebp
  800a99:	89 e5                	mov    %esp,%ebp
  800a9b:	56                   	push   %esi
  800a9c:	53                   	push   %ebx
  800a9d:	8b 75 08             	mov    0x8(%ebp),%esi
  800aa0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800aa3:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800aa6:	85 c0                	test   %eax,%eax
  800aa8:	74 1e                	je     800ac8 <strlcpy+0x30>
  800aaa:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800aae:	89 f2                	mov    %esi,%edx
  800ab0:	eb 05                	jmp    800ab7 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800ab2:	42                   	inc    %edx
  800ab3:	41                   	inc    %ecx
  800ab4:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800ab7:	39 c2                	cmp    %eax,%edx
  800ab9:	74 08                	je     800ac3 <strlcpy+0x2b>
  800abb:	8a 19                	mov    (%ecx),%bl
  800abd:	84 db                	test   %bl,%bl
  800abf:	75 f1                	jne    800ab2 <strlcpy+0x1a>
  800ac1:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800ac3:	c6 00 00             	movb   $0x0,(%eax)
  800ac6:	eb 02                	jmp    800aca <strlcpy+0x32>
  800ac8:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800aca:	29 f0                	sub    %esi,%eax
}
  800acc:	5b                   	pop    %ebx
  800acd:	5e                   	pop    %esi
  800ace:	5d                   	pop    %ebp
  800acf:	c3                   	ret    

00800ad0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800ad0:	55                   	push   %ebp
  800ad1:	89 e5                	mov    %esp,%ebp
  800ad3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800ad6:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800ad9:	eb 02                	jmp    800add <strcmp+0xd>
		p++, q++;
  800adb:	41                   	inc    %ecx
  800adc:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800add:	8a 01                	mov    (%ecx),%al
  800adf:	84 c0                	test   %al,%al
  800ae1:	74 04                	je     800ae7 <strcmp+0x17>
  800ae3:	3a 02                	cmp    (%edx),%al
  800ae5:	74 f4                	je     800adb <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800ae7:	0f b6 c0             	movzbl %al,%eax
  800aea:	0f b6 12             	movzbl (%edx),%edx
  800aed:	29 d0                	sub    %edx,%eax
}
  800aef:	5d                   	pop    %ebp
  800af0:	c3                   	ret    

00800af1 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800af1:	55                   	push   %ebp
  800af2:	89 e5                	mov    %esp,%ebp
  800af4:	53                   	push   %ebx
  800af5:	8b 45 08             	mov    0x8(%ebp),%eax
  800af8:	8b 55 0c             	mov    0xc(%ebp),%edx
  800afb:	89 c3                	mov    %eax,%ebx
  800afd:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800b00:	eb 02                	jmp    800b04 <strncmp+0x13>
		n--, p++, q++;
  800b02:	40                   	inc    %eax
  800b03:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800b04:	39 d8                	cmp    %ebx,%eax
  800b06:	74 14                	je     800b1c <strncmp+0x2b>
  800b08:	8a 08                	mov    (%eax),%cl
  800b0a:	84 c9                	test   %cl,%cl
  800b0c:	74 04                	je     800b12 <strncmp+0x21>
  800b0e:	3a 0a                	cmp    (%edx),%cl
  800b10:	74 f0                	je     800b02 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800b12:	0f b6 00             	movzbl (%eax),%eax
  800b15:	0f b6 12             	movzbl (%edx),%edx
  800b18:	29 d0                	sub    %edx,%eax
  800b1a:	eb 05                	jmp    800b21 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800b1c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800b21:	5b                   	pop    %ebx
  800b22:	5d                   	pop    %ebp
  800b23:	c3                   	ret    

00800b24 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800b24:	55                   	push   %ebp
  800b25:	89 e5                	mov    %esp,%ebp
  800b27:	8b 45 08             	mov    0x8(%ebp),%eax
  800b2a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800b2d:	eb 05                	jmp    800b34 <strchr+0x10>
		if (*s == c)
  800b2f:	38 ca                	cmp    %cl,%dl
  800b31:	74 0c                	je     800b3f <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800b33:	40                   	inc    %eax
  800b34:	8a 10                	mov    (%eax),%dl
  800b36:	84 d2                	test   %dl,%dl
  800b38:	75 f5                	jne    800b2f <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800b3a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b3f:	5d                   	pop    %ebp
  800b40:	c3                   	ret    

00800b41 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800b41:	55                   	push   %ebp
  800b42:	89 e5                	mov    %esp,%ebp
  800b44:	8b 45 08             	mov    0x8(%ebp),%eax
  800b47:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800b4a:	eb 05                	jmp    800b51 <strfind+0x10>
		if (*s == c)
  800b4c:	38 ca                	cmp    %cl,%dl
  800b4e:	74 07                	je     800b57 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800b50:	40                   	inc    %eax
  800b51:	8a 10                	mov    (%eax),%dl
  800b53:	84 d2                	test   %dl,%dl
  800b55:	75 f5                	jne    800b4c <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800b57:	5d                   	pop    %ebp
  800b58:	c3                   	ret    

00800b59 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800b59:	55                   	push   %ebp
  800b5a:	89 e5                	mov    %esp,%ebp
  800b5c:	57                   	push   %edi
  800b5d:	56                   	push   %esi
  800b5e:	53                   	push   %ebx
  800b5f:	8b 7d 08             	mov    0x8(%ebp),%edi
  800b62:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800b65:	85 c9                	test   %ecx,%ecx
  800b67:	74 36                	je     800b9f <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800b69:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800b6f:	75 28                	jne    800b99 <memset+0x40>
  800b71:	f6 c1 03             	test   $0x3,%cl
  800b74:	75 23                	jne    800b99 <memset+0x40>
		c &= 0xFF;
  800b76:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800b7a:	89 d3                	mov    %edx,%ebx
  800b7c:	c1 e3 08             	shl    $0x8,%ebx
  800b7f:	89 d6                	mov    %edx,%esi
  800b81:	c1 e6 18             	shl    $0x18,%esi
  800b84:	89 d0                	mov    %edx,%eax
  800b86:	c1 e0 10             	shl    $0x10,%eax
  800b89:	09 f0                	or     %esi,%eax
  800b8b:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b8d:	89 d8                	mov    %ebx,%eax
  800b8f:	09 d0                	or     %edx,%eax
  800b91:	c1 e9 02             	shr    $0x2,%ecx
  800b94:	fc                   	cld    
  800b95:	f3 ab                	rep stos %eax,%es:(%edi)
  800b97:	eb 06                	jmp    800b9f <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b99:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b9c:	fc                   	cld    
  800b9d:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b9f:	89 f8                	mov    %edi,%eax
  800ba1:	5b                   	pop    %ebx
  800ba2:	5e                   	pop    %esi
  800ba3:	5f                   	pop    %edi
  800ba4:	5d                   	pop    %ebp
  800ba5:	c3                   	ret    

00800ba6 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ba6:	55                   	push   %ebp
  800ba7:	89 e5                	mov    %esp,%ebp
  800ba9:	57                   	push   %edi
  800baa:	56                   	push   %esi
  800bab:	8b 45 08             	mov    0x8(%ebp),%eax
  800bae:	8b 75 0c             	mov    0xc(%ebp),%esi
  800bb1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800bb4:	39 c6                	cmp    %eax,%esi
  800bb6:	73 33                	jae    800beb <memmove+0x45>
  800bb8:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800bbb:	39 d0                	cmp    %edx,%eax
  800bbd:	73 2c                	jae    800beb <memmove+0x45>
		s += n;
		d += n;
  800bbf:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800bc2:	89 d6                	mov    %edx,%esi
  800bc4:	09 fe                	or     %edi,%esi
  800bc6:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800bcc:	75 13                	jne    800be1 <memmove+0x3b>
  800bce:	f6 c1 03             	test   $0x3,%cl
  800bd1:	75 0e                	jne    800be1 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800bd3:	83 ef 04             	sub    $0x4,%edi
  800bd6:	8d 72 fc             	lea    -0x4(%edx),%esi
  800bd9:	c1 e9 02             	shr    $0x2,%ecx
  800bdc:	fd                   	std    
  800bdd:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800bdf:	eb 07                	jmp    800be8 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800be1:	4f                   	dec    %edi
  800be2:	8d 72 ff             	lea    -0x1(%edx),%esi
  800be5:	fd                   	std    
  800be6:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800be8:	fc                   	cld    
  800be9:	eb 1d                	jmp    800c08 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800beb:	89 f2                	mov    %esi,%edx
  800bed:	09 c2                	or     %eax,%edx
  800bef:	f6 c2 03             	test   $0x3,%dl
  800bf2:	75 0f                	jne    800c03 <memmove+0x5d>
  800bf4:	f6 c1 03             	test   $0x3,%cl
  800bf7:	75 0a                	jne    800c03 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800bf9:	c1 e9 02             	shr    $0x2,%ecx
  800bfc:	89 c7                	mov    %eax,%edi
  800bfe:	fc                   	cld    
  800bff:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800c01:	eb 05                	jmp    800c08 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800c03:	89 c7                	mov    %eax,%edi
  800c05:	fc                   	cld    
  800c06:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800c08:	5e                   	pop    %esi
  800c09:	5f                   	pop    %edi
  800c0a:	5d                   	pop    %ebp
  800c0b:	c3                   	ret    

00800c0c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800c0c:	55                   	push   %ebp
  800c0d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800c0f:	ff 75 10             	pushl  0x10(%ebp)
  800c12:	ff 75 0c             	pushl  0xc(%ebp)
  800c15:	ff 75 08             	pushl  0x8(%ebp)
  800c18:	e8 89 ff ff ff       	call   800ba6 <memmove>
}
  800c1d:	c9                   	leave  
  800c1e:	c3                   	ret    

00800c1f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800c1f:	55                   	push   %ebp
  800c20:	89 e5                	mov    %esp,%ebp
  800c22:	56                   	push   %esi
  800c23:	53                   	push   %ebx
  800c24:	8b 45 08             	mov    0x8(%ebp),%eax
  800c27:	8b 55 0c             	mov    0xc(%ebp),%edx
  800c2a:	89 c6                	mov    %eax,%esi
  800c2c:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800c2f:	eb 14                	jmp    800c45 <memcmp+0x26>
		if (*s1 != *s2)
  800c31:	8a 08                	mov    (%eax),%cl
  800c33:	8a 1a                	mov    (%edx),%bl
  800c35:	38 d9                	cmp    %bl,%cl
  800c37:	74 0a                	je     800c43 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800c39:	0f b6 c1             	movzbl %cl,%eax
  800c3c:	0f b6 db             	movzbl %bl,%ebx
  800c3f:	29 d8                	sub    %ebx,%eax
  800c41:	eb 0b                	jmp    800c4e <memcmp+0x2f>
		s1++, s2++;
  800c43:	40                   	inc    %eax
  800c44:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800c45:	39 f0                	cmp    %esi,%eax
  800c47:	75 e8                	jne    800c31 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800c49:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c4e:	5b                   	pop    %ebx
  800c4f:	5e                   	pop    %esi
  800c50:	5d                   	pop    %ebp
  800c51:	c3                   	ret    

00800c52 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800c52:	55                   	push   %ebp
  800c53:	89 e5                	mov    %esp,%ebp
  800c55:	53                   	push   %ebx
  800c56:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800c59:	89 c1                	mov    %eax,%ecx
  800c5b:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800c5e:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c62:	eb 08                	jmp    800c6c <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800c64:	0f b6 10             	movzbl (%eax),%edx
  800c67:	39 da                	cmp    %ebx,%edx
  800c69:	74 05                	je     800c70 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c6b:	40                   	inc    %eax
  800c6c:	39 c8                	cmp    %ecx,%eax
  800c6e:	72 f4                	jb     800c64 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800c70:	5b                   	pop    %ebx
  800c71:	5d                   	pop    %ebp
  800c72:	c3                   	ret    

00800c73 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800c73:	55                   	push   %ebp
  800c74:	89 e5                	mov    %esp,%ebp
  800c76:	57                   	push   %edi
  800c77:	56                   	push   %esi
  800c78:	53                   	push   %ebx
  800c79:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c7c:	eb 01                	jmp    800c7f <strtol+0xc>
		s++;
  800c7e:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c7f:	8a 01                	mov    (%ecx),%al
  800c81:	3c 20                	cmp    $0x20,%al
  800c83:	74 f9                	je     800c7e <strtol+0xb>
  800c85:	3c 09                	cmp    $0x9,%al
  800c87:	74 f5                	je     800c7e <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c89:	3c 2b                	cmp    $0x2b,%al
  800c8b:	75 08                	jne    800c95 <strtol+0x22>
		s++;
  800c8d:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c8e:	bf 00 00 00 00       	mov    $0x0,%edi
  800c93:	eb 11                	jmp    800ca6 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c95:	3c 2d                	cmp    $0x2d,%al
  800c97:	75 08                	jne    800ca1 <strtol+0x2e>
		s++, neg = 1;
  800c99:	41                   	inc    %ecx
  800c9a:	bf 01 00 00 00       	mov    $0x1,%edi
  800c9f:	eb 05                	jmp    800ca6 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800ca1:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ca6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800caa:	0f 84 87 00 00 00    	je     800d37 <strtol+0xc4>
  800cb0:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800cb4:	75 27                	jne    800cdd <strtol+0x6a>
  800cb6:	80 39 30             	cmpb   $0x30,(%ecx)
  800cb9:	75 22                	jne    800cdd <strtol+0x6a>
  800cbb:	e9 88 00 00 00       	jmp    800d48 <strtol+0xd5>
		s += 2, base = 16;
  800cc0:	83 c1 02             	add    $0x2,%ecx
  800cc3:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800cca:	eb 11                	jmp    800cdd <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800ccc:	41                   	inc    %ecx
  800ccd:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800cd4:	eb 07                	jmp    800cdd <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800cd6:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800cdd:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800ce2:	8a 11                	mov    (%ecx),%dl
  800ce4:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800ce7:	80 fb 09             	cmp    $0x9,%bl
  800cea:	77 08                	ja     800cf4 <strtol+0x81>
			dig = *s - '0';
  800cec:	0f be d2             	movsbl %dl,%edx
  800cef:	83 ea 30             	sub    $0x30,%edx
  800cf2:	eb 22                	jmp    800d16 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800cf4:	8d 72 9f             	lea    -0x61(%edx),%esi
  800cf7:	89 f3                	mov    %esi,%ebx
  800cf9:	80 fb 19             	cmp    $0x19,%bl
  800cfc:	77 08                	ja     800d06 <strtol+0x93>
			dig = *s - 'a' + 10;
  800cfe:	0f be d2             	movsbl %dl,%edx
  800d01:	83 ea 57             	sub    $0x57,%edx
  800d04:	eb 10                	jmp    800d16 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800d06:	8d 72 bf             	lea    -0x41(%edx),%esi
  800d09:	89 f3                	mov    %esi,%ebx
  800d0b:	80 fb 19             	cmp    $0x19,%bl
  800d0e:	77 14                	ja     800d24 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800d10:	0f be d2             	movsbl %dl,%edx
  800d13:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800d16:	3b 55 10             	cmp    0x10(%ebp),%edx
  800d19:	7d 09                	jge    800d24 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800d1b:	41                   	inc    %ecx
  800d1c:	0f af 45 10          	imul   0x10(%ebp),%eax
  800d20:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800d22:	eb be                	jmp    800ce2 <strtol+0x6f>

	if (endptr)
  800d24:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800d28:	74 05                	je     800d2f <strtol+0xbc>
		*endptr = (char *) s;
  800d2a:	8b 75 0c             	mov    0xc(%ebp),%esi
  800d2d:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800d2f:	85 ff                	test   %edi,%edi
  800d31:	74 21                	je     800d54 <strtol+0xe1>
  800d33:	f7 d8                	neg    %eax
  800d35:	eb 1d                	jmp    800d54 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800d37:	80 39 30             	cmpb   $0x30,(%ecx)
  800d3a:	75 9a                	jne    800cd6 <strtol+0x63>
  800d3c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800d40:	0f 84 7a ff ff ff    	je     800cc0 <strtol+0x4d>
  800d46:	eb 84                	jmp    800ccc <strtol+0x59>
  800d48:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800d4c:	0f 84 6e ff ff ff    	je     800cc0 <strtol+0x4d>
  800d52:	eb 89                	jmp    800cdd <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800d54:	5b                   	pop    %ebx
  800d55:	5e                   	pop    %esi
  800d56:	5f                   	pop    %edi
  800d57:	5d                   	pop    %ebp
  800d58:	c3                   	ret    

00800d59 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800d59:	55                   	push   %ebp
  800d5a:	89 e5                	mov    %esp,%ebp
  800d5c:	57                   	push   %edi
  800d5d:	56                   	push   %esi
  800d5e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d5f:	b8 00 00 00 00       	mov    $0x0,%eax
  800d64:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d67:	8b 55 08             	mov    0x8(%ebp),%edx
  800d6a:	89 c3                	mov    %eax,%ebx
  800d6c:	89 c7                	mov    %eax,%edi
  800d6e:	89 c6                	mov    %eax,%esi
  800d70:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800d72:	5b                   	pop    %ebx
  800d73:	5e                   	pop    %esi
  800d74:	5f                   	pop    %edi
  800d75:	5d                   	pop    %ebp
  800d76:	c3                   	ret    

00800d77 <sys_cgetc>:

int
sys_cgetc(void)
{
  800d77:	55                   	push   %ebp
  800d78:	89 e5                	mov    %esp,%ebp
  800d7a:	57                   	push   %edi
  800d7b:	56                   	push   %esi
  800d7c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d7d:	ba 00 00 00 00       	mov    $0x0,%edx
  800d82:	b8 01 00 00 00       	mov    $0x1,%eax
  800d87:	89 d1                	mov    %edx,%ecx
  800d89:	89 d3                	mov    %edx,%ebx
  800d8b:	89 d7                	mov    %edx,%edi
  800d8d:	89 d6                	mov    %edx,%esi
  800d8f:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800d91:	5b                   	pop    %ebx
  800d92:	5e                   	pop    %esi
  800d93:	5f                   	pop    %edi
  800d94:	5d                   	pop    %ebp
  800d95:	c3                   	ret    

00800d96 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800d96:	55                   	push   %ebp
  800d97:	89 e5                	mov    %esp,%ebp
  800d99:	57                   	push   %edi
  800d9a:	56                   	push   %esi
  800d9b:	53                   	push   %ebx
  800d9c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d9f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800da4:	b8 03 00 00 00       	mov    $0x3,%eax
  800da9:	8b 55 08             	mov    0x8(%ebp),%edx
  800dac:	89 cb                	mov    %ecx,%ebx
  800dae:	89 cf                	mov    %ecx,%edi
  800db0:	89 ce                	mov    %ecx,%esi
  800db2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800db4:	85 c0                	test   %eax,%eax
  800db6:	7e 17                	jle    800dcf <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800db8:	83 ec 0c             	sub    $0xc,%esp
  800dbb:	50                   	push   %eax
  800dbc:	6a 03                	push   $0x3
  800dbe:	68 bf 29 80 00       	push   $0x8029bf
  800dc3:	6a 23                	push   $0x23
  800dc5:	68 dc 29 80 00       	push   $0x8029dc
  800dca:	e8 26 f6 ff ff       	call   8003f5 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800dcf:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dd2:	5b                   	pop    %ebx
  800dd3:	5e                   	pop    %esi
  800dd4:	5f                   	pop    %edi
  800dd5:	5d                   	pop    %ebp
  800dd6:	c3                   	ret    

00800dd7 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800dd7:	55                   	push   %ebp
  800dd8:	89 e5                	mov    %esp,%ebp
  800dda:	57                   	push   %edi
  800ddb:	56                   	push   %esi
  800ddc:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ddd:	ba 00 00 00 00       	mov    $0x0,%edx
  800de2:	b8 02 00 00 00       	mov    $0x2,%eax
  800de7:	89 d1                	mov    %edx,%ecx
  800de9:	89 d3                	mov    %edx,%ebx
  800deb:	89 d7                	mov    %edx,%edi
  800ded:	89 d6                	mov    %edx,%esi
  800def:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800df1:	5b                   	pop    %ebx
  800df2:	5e                   	pop    %esi
  800df3:	5f                   	pop    %edi
  800df4:	5d                   	pop    %ebp
  800df5:	c3                   	ret    

00800df6 <sys_yield>:

void
sys_yield(void)
{
  800df6:	55                   	push   %ebp
  800df7:	89 e5                	mov    %esp,%ebp
  800df9:	57                   	push   %edi
  800dfa:	56                   	push   %esi
  800dfb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dfc:	ba 00 00 00 00       	mov    $0x0,%edx
  800e01:	b8 0b 00 00 00       	mov    $0xb,%eax
  800e06:	89 d1                	mov    %edx,%ecx
  800e08:	89 d3                	mov    %edx,%ebx
  800e0a:	89 d7                	mov    %edx,%edi
  800e0c:	89 d6                	mov    %edx,%esi
  800e0e:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800e10:	5b                   	pop    %ebx
  800e11:	5e                   	pop    %esi
  800e12:	5f                   	pop    %edi
  800e13:	5d                   	pop    %ebp
  800e14:	c3                   	ret    

00800e15 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800e15:	55                   	push   %ebp
  800e16:	89 e5                	mov    %esp,%ebp
  800e18:	57                   	push   %edi
  800e19:	56                   	push   %esi
  800e1a:	53                   	push   %ebx
  800e1b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e1e:	be 00 00 00 00       	mov    $0x0,%esi
  800e23:	b8 04 00 00 00       	mov    $0x4,%eax
  800e28:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e2b:	8b 55 08             	mov    0x8(%ebp),%edx
  800e2e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e31:	89 f7                	mov    %esi,%edi
  800e33:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e35:	85 c0                	test   %eax,%eax
  800e37:	7e 17                	jle    800e50 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e39:	83 ec 0c             	sub    $0xc,%esp
  800e3c:	50                   	push   %eax
  800e3d:	6a 04                	push   $0x4
  800e3f:	68 bf 29 80 00       	push   $0x8029bf
  800e44:	6a 23                	push   $0x23
  800e46:	68 dc 29 80 00       	push   $0x8029dc
  800e4b:	e8 a5 f5 ff ff       	call   8003f5 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800e50:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e53:	5b                   	pop    %ebx
  800e54:	5e                   	pop    %esi
  800e55:	5f                   	pop    %edi
  800e56:	5d                   	pop    %ebp
  800e57:	c3                   	ret    

00800e58 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800e58:	55                   	push   %ebp
  800e59:	89 e5                	mov    %esp,%ebp
  800e5b:	57                   	push   %edi
  800e5c:	56                   	push   %esi
  800e5d:	53                   	push   %ebx
  800e5e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e61:	b8 05 00 00 00       	mov    $0x5,%eax
  800e66:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e69:	8b 55 08             	mov    0x8(%ebp),%edx
  800e6c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e6f:	8b 7d 14             	mov    0x14(%ebp),%edi
  800e72:	8b 75 18             	mov    0x18(%ebp),%esi
  800e75:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e77:	85 c0                	test   %eax,%eax
  800e79:	7e 17                	jle    800e92 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e7b:	83 ec 0c             	sub    $0xc,%esp
  800e7e:	50                   	push   %eax
  800e7f:	6a 05                	push   $0x5
  800e81:	68 bf 29 80 00       	push   $0x8029bf
  800e86:	6a 23                	push   $0x23
  800e88:	68 dc 29 80 00       	push   $0x8029dc
  800e8d:	e8 63 f5 ff ff       	call   8003f5 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800e92:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e95:	5b                   	pop    %ebx
  800e96:	5e                   	pop    %esi
  800e97:	5f                   	pop    %edi
  800e98:	5d                   	pop    %ebp
  800e99:	c3                   	ret    

00800e9a <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800e9a:	55                   	push   %ebp
  800e9b:	89 e5                	mov    %esp,%ebp
  800e9d:	57                   	push   %edi
  800e9e:	56                   	push   %esi
  800e9f:	53                   	push   %ebx
  800ea0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ea3:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ea8:	b8 06 00 00 00       	mov    $0x6,%eax
  800ead:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800eb0:	8b 55 08             	mov    0x8(%ebp),%edx
  800eb3:	89 df                	mov    %ebx,%edi
  800eb5:	89 de                	mov    %ebx,%esi
  800eb7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800eb9:	85 c0                	test   %eax,%eax
  800ebb:	7e 17                	jle    800ed4 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ebd:	83 ec 0c             	sub    $0xc,%esp
  800ec0:	50                   	push   %eax
  800ec1:	6a 06                	push   $0x6
  800ec3:	68 bf 29 80 00       	push   $0x8029bf
  800ec8:	6a 23                	push   $0x23
  800eca:	68 dc 29 80 00       	push   $0x8029dc
  800ecf:	e8 21 f5 ff ff       	call   8003f5 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800ed4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ed7:	5b                   	pop    %ebx
  800ed8:	5e                   	pop    %esi
  800ed9:	5f                   	pop    %edi
  800eda:	5d                   	pop    %ebp
  800edb:	c3                   	ret    

00800edc <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800edc:	55                   	push   %ebp
  800edd:	89 e5                	mov    %esp,%ebp
  800edf:	57                   	push   %edi
  800ee0:	56                   	push   %esi
  800ee1:	53                   	push   %ebx
  800ee2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ee5:	bb 00 00 00 00       	mov    $0x0,%ebx
  800eea:	b8 08 00 00 00       	mov    $0x8,%eax
  800eef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ef2:	8b 55 08             	mov    0x8(%ebp),%edx
  800ef5:	89 df                	mov    %ebx,%edi
  800ef7:	89 de                	mov    %ebx,%esi
  800ef9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800efb:	85 c0                	test   %eax,%eax
  800efd:	7e 17                	jle    800f16 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800eff:	83 ec 0c             	sub    $0xc,%esp
  800f02:	50                   	push   %eax
  800f03:	6a 08                	push   $0x8
  800f05:	68 bf 29 80 00       	push   $0x8029bf
  800f0a:	6a 23                	push   $0x23
  800f0c:	68 dc 29 80 00       	push   $0x8029dc
  800f11:	e8 df f4 ff ff       	call   8003f5 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800f16:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f19:	5b                   	pop    %ebx
  800f1a:	5e                   	pop    %esi
  800f1b:	5f                   	pop    %edi
  800f1c:	5d                   	pop    %ebp
  800f1d:	c3                   	ret    

00800f1e <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800f1e:	55                   	push   %ebp
  800f1f:	89 e5                	mov    %esp,%ebp
  800f21:	57                   	push   %edi
  800f22:	56                   	push   %esi
  800f23:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f24:	ba 00 00 00 00       	mov    $0x0,%edx
  800f29:	b8 0c 00 00 00       	mov    $0xc,%eax
  800f2e:	89 d1                	mov    %edx,%ecx
  800f30:	89 d3                	mov    %edx,%ebx
  800f32:	89 d7                	mov    %edx,%edi
  800f34:	89 d6                	mov    %edx,%esi
  800f36:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800f38:	5b                   	pop    %ebx
  800f39:	5e                   	pop    %esi
  800f3a:	5f                   	pop    %edi
  800f3b:	5d                   	pop    %ebp
  800f3c:	c3                   	ret    

00800f3d <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800f3d:	55                   	push   %ebp
  800f3e:	89 e5                	mov    %esp,%ebp
  800f40:	57                   	push   %edi
  800f41:	56                   	push   %esi
  800f42:	53                   	push   %ebx
  800f43:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f46:	bb 00 00 00 00       	mov    $0x0,%ebx
  800f4b:	b8 09 00 00 00       	mov    $0x9,%eax
  800f50:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f53:	8b 55 08             	mov    0x8(%ebp),%edx
  800f56:	89 df                	mov    %ebx,%edi
  800f58:	89 de                	mov    %ebx,%esi
  800f5a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f5c:	85 c0                	test   %eax,%eax
  800f5e:	7e 17                	jle    800f77 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f60:	83 ec 0c             	sub    $0xc,%esp
  800f63:	50                   	push   %eax
  800f64:	6a 09                	push   $0x9
  800f66:	68 bf 29 80 00       	push   $0x8029bf
  800f6b:	6a 23                	push   $0x23
  800f6d:	68 dc 29 80 00       	push   $0x8029dc
  800f72:	e8 7e f4 ff ff       	call   8003f5 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800f77:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f7a:	5b                   	pop    %ebx
  800f7b:	5e                   	pop    %esi
  800f7c:	5f                   	pop    %edi
  800f7d:	5d                   	pop    %ebp
  800f7e:	c3                   	ret    

00800f7f <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800f7f:	55                   	push   %ebp
  800f80:	89 e5                	mov    %esp,%ebp
  800f82:	57                   	push   %edi
  800f83:	56                   	push   %esi
  800f84:	53                   	push   %ebx
  800f85:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f88:	bb 00 00 00 00       	mov    $0x0,%ebx
  800f8d:	b8 0a 00 00 00       	mov    $0xa,%eax
  800f92:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f95:	8b 55 08             	mov    0x8(%ebp),%edx
  800f98:	89 df                	mov    %ebx,%edi
  800f9a:	89 de                	mov    %ebx,%esi
  800f9c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f9e:	85 c0                	test   %eax,%eax
  800fa0:	7e 17                	jle    800fb9 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800fa2:	83 ec 0c             	sub    $0xc,%esp
  800fa5:	50                   	push   %eax
  800fa6:	6a 0a                	push   $0xa
  800fa8:	68 bf 29 80 00       	push   $0x8029bf
  800fad:	6a 23                	push   $0x23
  800faf:	68 dc 29 80 00       	push   $0x8029dc
  800fb4:	e8 3c f4 ff ff       	call   8003f5 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800fb9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fbc:	5b                   	pop    %ebx
  800fbd:	5e                   	pop    %esi
  800fbe:	5f                   	pop    %edi
  800fbf:	5d                   	pop    %ebp
  800fc0:	c3                   	ret    

00800fc1 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800fc1:	55                   	push   %ebp
  800fc2:	89 e5                	mov    %esp,%ebp
  800fc4:	57                   	push   %edi
  800fc5:	56                   	push   %esi
  800fc6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fc7:	be 00 00 00 00       	mov    $0x0,%esi
  800fcc:	b8 0d 00 00 00       	mov    $0xd,%eax
  800fd1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800fd4:	8b 55 08             	mov    0x8(%ebp),%edx
  800fd7:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800fda:	8b 7d 14             	mov    0x14(%ebp),%edi
  800fdd:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800fdf:	5b                   	pop    %ebx
  800fe0:	5e                   	pop    %esi
  800fe1:	5f                   	pop    %edi
  800fe2:	5d                   	pop    %ebp
  800fe3:	c3                   	ret    

00800fe4 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800fe4:	55                   	push   %ebp
  800fe5:	89 e5                	mov    %esp,%ebp
  800fe7:	57                   	push   %edi
  800fe8:	56                   	push   %esi
  800fe9:	53                   	push   %ebx
  800fea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fed:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ff2:	b8 0e 00 00 00       	mov    $0xe,%eax
  800ff7:	8b 55 08             	mov    0x8(%ebp),%edx
  800ffa:	89 cb                	mov    %ecx,%ebx
  800ffc:	89 cf                	mov    %ecx,%edi
  800ffe:	89 ce                	mov    %ecx,%esi
  801000:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801002:	85 c0                	test   %eax,%eax
  801004:	7e 17                	jle    80101d <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  801006:	83 ec 0c             	sub    $0xc,%esp
  801009:	50                   	push   %eax
  80100a:	6a 0e                	push   $0xe
  80100c:	68 bf 29 80 00       	push   $0x8029bf
  801011:	6a 23                	push   $0x23
  801013:	68 dc 29 80 00       	push   $0x8029dc
  801018:	e8 d8 f3 ff ff       	call   8003f5 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80101d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801020:	5b                   	pop    %ebx
  801021:	5e                   	pop    %esi
  801022:	5f                   	pop    %edi
  801023:	5d                   	pop    %ebp
  801024:	c3                   	ret    

00801025 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  801025:	55                   	push   %ebp
  801026:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  801028:	8b 45 08             	mov    0x8(%ebp),%eax
  80102b:	05 00 00 00 30       	add    $0x30000000,%eax
  801030:	c1 e8 0c             	shr    $0xc,%eax
}
  801033:	5d                   	pop    %ebp
  801034:	c3                   	ret    

00801035 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  801035:	55                   	push   %ebp
  801036:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  801038:	8b 45 08             	mov    0x8(%ebp),%eax
  80103b:	05 00 00 00 30       	add    $0x30000000,%eax
  801040:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  801045:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  80104a:	5d                   	pop    %ebp
  80104b:	c3                   	ret    

0080104c <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  80104c:	55                   	push   %ebp
  80104d:	89 e5                	mov    %esp,%ebp
  80104f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801052:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  801057:	89 c2                	mov    %eax,%edx
  801059:	c1 ea 16             	shr    $0x16,%edx
  80105c:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801063:	f6 c2 01             	test   $0x1,%dl
  801066:	74 11                	je     801079 <fd_alloc+0x2d>
  801068:	89 c2                	mov    %eax,%edx
  80106a:	c1 ea 0c             	shr    $0xc,%edx
  80106d:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801074:	f6 c2 01             	test   $0x1,%dl
  801077:	75 09                	jne    801082 <fd_alloc+0x36>
			*fd_store = fd;
  801079:	89 01                	mov    %eax,(%ecx)
			return 0;
  80107b:	b8 00 00 00 00       	mov    $0x0,%eax
  801080:	eb 17                	jmp    801099 <fd_alloc+0x4d>
  801082:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801087:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  80108c:	75 c9                	jne    801057 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  80108e:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801094:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801099:	5d                   	pop    %ebp
  80109a:	c3                   	ret    

0080109b <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  80109b:	55                   	push   %ebp
  80109c:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  80109e:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  8010a2:	77 39                	ja     8010dd <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  8010a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8010a7:	c1 e0 0c             	shl    $0xc,%eax
  8010aa:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  8010af:	89 c2                	mov    %eax,%edx
  8010b1:	c1 ea 16             	shr    $0x16,%edx
  8010b4:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8010bb:	f6 c2 01             	test   $0x1,%dl
  8010be:	74 24                	je     8010e4 <fd_lookup+0x49>
  8010c0:	89 c2                	mov    %eax,%edx
  8010c2:	c1 ea 0c             	shr    $0xc,%edx
  8010c5:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8010cc:	f6 c2 01             	test   $0x1,%dl
  8010cf:	74 1a                	je     8010eb <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  8010d1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8010d4:	89 02                	mov    %eax,(%edx)
	return 0;
  8010d6:	b8 00 00 00 00       	mov    $0x0,%eax
  8010db:	eb 13                	jmp    8010f0 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8010dd:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8010e2:	eb 0c                	jmp    8010f0 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8010e4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8010e9:	eb 05                	jmp    8010f0 <fd_lookup+0x55>
  8010eb:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8010f0:	5d                   	pop    %ebp
  8010f1:	c3                   	ret    

008010f2 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8010f2:	55                   	push   %ebp
  8010f3:	89 e5                	mov    %esp,%ebp
  8010f5:	83 ec 08             	sub    $0x8,%esp
  8010f8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8010fb:	ba 68 2a 80 00       	mov    $0x802a68,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801100:	eb 13                	jmp    801115 <dev_lookup+0x23>
  801102:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  801105:	39 08                	cmp    %ecx,(%eax)
  801107:	75 0c                	jne    801115 <dev_lookup+0x23>
			*dev = devtab[i];
  801109:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80110c:	89 01                	mov    %eax,(%ecx)
			return 0;
  80110e:	b8 00 00 00 00       	mov    $0x0,%eax
  801113:	eb 2e                	jmp    801143 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801115:	8b 02                	mov    (%edx),%eax
  801117:	85 c0                	test   %eax,%eax
  801119:	75 e7                	jne    801102 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  80111b:	a1 90 67 80 00       	mov    0x806790,%eax
  801120:	8b 40 48             	mov    0x48(%eax),%eax
  801123:	83 ec 04             	sub    $0x4,%esp
  801126:	51                   	push   %ecx
  801127:	50                   	push   %eax
  801128:	68 ec 29 80 00       	push   $0x8029ec
  80112d:	e8 9b f3 ff ff       	call   8004cd <cprintf>
	*dev = 0;
  801132:	8b 45 0c             	mov    0xc(%ebp),%eax
  801135:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  80113b:	83 c4 10             	add    $0x10,%esp
  80113e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801143:	c9                   	leave  
  801144:	c3                   	ret    

00801145 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  801145:	55                   	push   %ebp
  801146:	89 e5                	mov    %esp,%ebp
  801148:	56                   	push   %esi
  801149:	53                   	push   %ebx
  80114a:	83 ec 10             	sub    $0x10,%esp
  80114d:	8b 75 08             	mov    0x8(%ebp),%esi
  801150:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  801153:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801156:	50                   	push   %eax
  801157:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  80115d:	c1 e8 0c             	shr    $0xc,%eax
  801160:	50                   	push   %eax
  801161:	e8 35 ff ff ff       	call   80109b <fd_lookup>
  801166:	83 c4 08             	add    $0x8,%esp
  801169:	85 c0                	test   %eax,%eax
  80116b:	78 05                	js     801172 <fd_close+0x2d>
	    || fd != fd2)
  80116d:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  801170:	74 06                	je     801178 <fd_close+0x33>
		return (must_exist ? r : 0);
  801172:	84 db                	test   %bl,%bl
  801174:	74 47                	je     8011bd <fd_close+0x78>
  801176:	eb 4a                	jmp    8011c2 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801178:	83 ec 08             	sub    $0x8,%esp
  80117b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80117e:	50                   	push   %eax
  80117f:	ff 36                	pushl  (%esi)
  801181:	e8 6c ff ff ff       	call   8010f2 <dev_lookup>
  801186:	89 c3                	mov    %eax,%ebx
  801188:	83 c4 10             	add    $0x10,%esp
  80118b:	85 c0                	test   %eax,%eax
  80118d:	78 1c                	js     8011ab <fd_close+0x66>
		if (dev->dev_close)
  80118f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801192:	8b 40 10             	mov    0x10(%eax),%eax
  801195:	85 c0                	test   %eax,%eax
  801197:	74 0d                	je     8011a6 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801199:	83 ec 0c             	sub    $0xc,%esp
  80119c:	56                   	push   %esi
  80119d:	ff d0                	call   *%eax
  80119f:	89 c3                	mov    %eax,%ebx
  8011a1:	83 c4 10             	add    $0x10,%esp
  8011a4:	eb 05                	jmp    8011ab <fd_close+0x66>
		else
			r = 0;
  8011a6:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  8011ab:	83 ec 08             	sub    $0x8,%esp
  8011ae:	56                   	push   %esi
  8011af:	6a 00                	push   $0x0
  8011b1:	e8 e4 fc ff ff       	call   800e9a <sys_page_unmap>
	return r;
  8011b6:	83 c4 10             	add    $0x10,%esp
  8011b9:	89 d8                	mov    %ebx,%eax
  8011bb:	eb 05                	jmp    8011c2 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  8011bd:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  8011c2:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8011c5:	5b                   	pop    %ebx
  8011c6:	5e                   	pop    %esi
  8011c7:	5d                   	pop    %ebp
  8011c8:	c3                   	ret    

008011c9 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  8011c9:	55                   	push   %ebp
  8011ca:	89 e5                	mov    %esp,%ebp
  8011cc:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8011cf:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8011d2:	50                   	push   %eax
  8011d3:	ff 75 08             	pushl  0x8(%ebp)
  8011d6:	e8 c0 fe ff ff       	call   80109b <fd_lookup>
  8011db:	83 c4 08             	add    $0x8,%esp
  8011de:	85 c0                	test   %eax,%eax
  8011e0:	78 10                	js     8011f2 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8011e2:	83 ec 08             	sub    $0x8,%esp
  8011e5:	6a 01                	push   $0x1
  8011e7:	ff 75 f4             	pushl  -0xc(%ebp)
  8011ea:	e8 56 ff ff ff       	call   801145 <fd_close>
  8011ef:	83 c4 10             	add    $0x10,%esp
}
  8011f2:	c9                   	leave  
  8011f3:	c3                   	ret    

008011f4 <close_all>:

void
close_all(void)
{
  8011f4:	55                   	push   %ebp
  8011f5:	89 e5                	mov    %esp,%ebp
  8011f7:	53                   	push   %ebx
  8011f8:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  8011fb:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801200:	83 ec 0c             	sub    $0xc,%esp
  801203:	53                   	push   %ebx
  801204:	e8 c0 ff ff ff       	call   8011c9 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801209:	43                   	inc    %ebx
  80120a:	83 c4 10             	add    $0x10,%esp
  80120d:	83 fb 20             	cmp    $0x20,%ebx
  801210:	75 ee                	jne    801200 <close_all+0xc>
		close(i);
}
  801212:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801215:	c9                   	leave  
  801216:	c3                   	ret    

00801217 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801217:	55                   	push   %ebp
  801218:	89 e5                	mov    %esp,%ebp
  80121a:	57                   	push   %edi
  80121b:	56                   	push   %esi
  80121c:	53                   	push   %ebx
  80121d:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801220:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801223:	50                   	push   %eax
  801224:	ff 75 08             	pushl  0x8(%ebp)
  801227:	e8 6f fe ff ff       	call   80109b <fd_lookup>
  80122c:	83 c4 08             	add    $0x8,%esp
  80122f:	85 c0                	test   %eax,%eax
  801231:	0f 88 c2 00 00 00    	js     8012f9 <dup+0xe2>
		return r;
	close(newfdnum);
  801237:	83 ec 0c             	sub    $0xc,%esp
  80123a:	ff 75 0c             	pushl  0xc(%ebp)
  80123d:	e8 87 ff ff ff       	call   8011c9 <close>

	newfd = INDEX2FD(newfdnum);
  801242:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801245:	c1 e3 0c             	shl    $0xc,%ebx
  801248:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  80124e:	83 c4 04             	add    $0x4,%esp
  801251:	ff 75 e4             	pushl  -0x1c(%ebp)
  801254:	e8 dc fd ff ff       	call   801035 <fd2data>
  801259:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  80125b:	89 1c 24             	mov    %ebx,(%esp)
  80125e:	e8 d2 fd ff ff       	call   801035 <fd2data>
  801263:	83 c4 10             	add    $0x10,%esp
  801266:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801268:	89 f0                	mov    %esi,%eax
  80126a:	c1 e8 16             	shr    $0x16,%eax
  80126d:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801274:	a8 01                	test   $0x1,%al
  801276:	74 35                	je     8012ad <dup+0x96>
  801278:	89 f0                	mov    %esi,%eax
  80127a:	c1 e8 0c             	shr    $0xc,%eax
  80127d:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801284:	f6 c2 01             	test   $0x1,%dl
  801287:	74 24                	je     8012ad <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801289:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801290:	83 ec 0c             	sub    $0xc,%esp
  801293:	25 07 0e 00 00       	and    $0xe07,%eax
  801298:	50                   	push   %eax
  801299:	57                   	push   %edi
  80129a:	6a 00                	push   $0x0
  80129c:	56                   	push   %esi
  80129d:	6a 00                	push   $0x0
  80129f:	e8 b4 fb ff ff       	call   800e58 <sys_page_map>
  8012a4:	89 c6                	mov    %eax,%esi
  8012a6:	83 c4 20             	add    $0x20,%esp
  8012a9:	85 c0                	test   %eax,%eax
  8012ab:	78 2c                	js     8012d9 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8012ad:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8012b0:	89 d0                	mov    %edx,%eax
  8012b2:	c1 e8 0c             	shr    $0xc,%eax
  8012b5:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8012bc:	83 ec 0c             	sub    $0xc,%esp
  8012bf:	25 07 0e 00 00       	and    $0xe07,%eax
  8012c4:	50                   	push   %eax
  8012c5:	53                   	push   %ebx
  8012c6:	6a 00                	push   $0x0
  8012c8:	52                   	push   %edx
  8012c9:	6a 00                	push   $0x0
  8012cb:	e8 88 fb ff ff       	call   800e58 <sys_page_map>
  8012d0:	89 c6                	mov    %eax,%esi
  8012d2:	83 c4 20             	add    $0x20,%esp
  8012d5:	85 c0                	test   %eax,%eax
  8012d7:	79 1d                	jns    8012f6 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8012d9:	83 ec 08             	sub    $0x8,%esp
  8012dc:	53                   	push   %ebx
  8012dd:	6a 00                	push   $0x0
  8012df:	e8 b6 fb ff ff       	call   800e9a <sys_page_unmap>
	sys_page_unmap(0, nva);
  8012e4:	83 c4 08             	add    $0x8,%esp
  8012e7:	57                   	push   %edi
  8012e8:	6a 00                	push   $0x0
  8012ea:	e8 ab fb ff ff       	call   800e9a <sys_page_unmap>
	return r;
  8012ef:	83 c4 10             	add    $0x10,%esp
  8012f2:	89 f0                	mov    %esi,%eax
  8012f4:	eb 03                	jmp    8012f9 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8012f6:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8012f9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8012fc:	5b                   	pop    %ebx
  8012fd:	5e                   	pop    %esi
  8012fe:	5f                   	pop    %edi
  8012ff:	5d                   	pop    %ebp
  801300:	c3                   	ret    

00801301 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801301:	55                   	push   %ebp
  801302:	89 e5                	mov    %esp,%ebp
  801304:	53                   	push   %ebx
  801305:	83 ec 14             	sub    $0x14,%esp
  801308:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80130b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80130e:	50                   	push   %eax
  80130f:	53                   	push   %ebx
  801310:	e8 86 fd ff ff       	call   80109b <fd_lookup>
  801315:	83 c4 08             	add    $0x8,%esp
  801318:	85 c0                	test   %eax,%eax
  80131a:	78 67                	js     801383 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80131c:	83 ec 08             	sub    $0x8,%esp
  80131f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801322:	50                   	push   %eax
  801323:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801326:	ff 30                	pushl  (%eax)
  801328:	e8 c5 fd ff ff       	call   8010f2 <dev_lookup>
  80132d:	83 c4 10             	add    $0x10,%esp
  801330:	85 c0                	test   %eax,%eax
  801332:	78 4f                	js     801383 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801334:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801337:	8b 42 08             	mov    0x8(%edx),%eax
  80133a:	83 e0 03             	and    $0x3,%eax
  80133d:	83 f8 01             	cmp    $0x1,%eax
  801340:	75 21                	jne    801363 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801342:	a1 90 67 80 00       	mov    0x806790,%eax
  801347:	8b 40 48             	mov    0x48(%eax),%eax
  80134a:	83 ec 04             	sub    $0x4,%esp
  80134d:	53                   	push   %ebx
  80134e:	50                   	push   %eax
  80134f:	68 2d 2a 80 00       	push   $0x802a2d
  801354:	e8 74 f1 ff ff       	call   8004cd <cprintf>
		return -E_INVAL;
  801359:	83 c4 10             	add    $0x10,%esp
  80135c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801361:	eb 20                	jmp    801383 <read+0x82>
	}
	if (!dev->dev_read)
  801363:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801366:	8b 40 08             	mov    0x8(%eax),%eax
  801369:	85 c0                	test   %eax,%eax
  80136b:	74 11                	je     80137e <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  80136d:	83 ec 04             	sub    $0x4,%esp
  801370:	ff 75 10             	pushl  0x10(%ebp)
  801373:	ff 75 0c             	pushl  0xc(%ebp)
  801376:	52                   	push   %edx
  801377:	ff d0                	call   *%eax
  801379:	83 c4 10             	add    $0x10,%esp
  80137c:	eb 05                	jmp    801383 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  80137e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801383:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801386:	c9                   	leave  
  801387:	c3                   	ret    

00801388 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801388:	55                   	push   %ebp
  801389:	89 e5                	mov    %esp,%ebp
  80138b:	57                   	push   %edi
  80138c:	56                   	push   %esi
  80138d:	53                   	push   %ebx
  80138e:	83 ec 0c             	sub    $0xc,%esp
  801391:	8b 7d 08             	mov    0x8(%ebp),%edi
  801394:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801397:	bb 00 00 00 00       	mov    $0x0,%ebx
  80139c:	eb 21                	jmp    8013bf <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  80139e:	83 ec 04             	sub    $0x4,%esp
  8013a1:	89 f0                	mov    %esi,%eax
  8013a3:	29 d8                	sub    %ebx,%eax
  8013a5:	50                   	push   %eax
  8013a6:	89 d8                	mov    %ebx,%eax
  8013a8:	03 45 0c             	add    0xc(%ebp),%eax
  8013ab:	50                   	push   %eax
  8013ac:	57                   	push   %edi
  8013ad:	e8 4f ff ff ff       	call   801301 <read>
		if (m < 0)
  8013b2:	83 c4 10             	add    $0x10,%esp
  8013b5:	85 c0                	test   %eax,%eax
  8013b7:	78 10                	js     8013c9 <readn+0x41>
			return m;
		if (m == 0)
  8013b9:	85 c0                	test   %eax,%eax
  8013bb:	74 0a                	je     8013c7 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8013bd:	01 c3                	add    %eax,%ebx
  8013bf:	39 f3                	cmp    %esi,%ebx
  8013c1:	72 db                	jb     80139e <readn+0x16>
  8013c3:	89 d8                	mov    %ebx,%eax
  8013c5:	eb 02                	jmp    8013c9 <readn+0x41>
  8013c7:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  8013c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8013cc:	5b                   	pop    %ebx
  8013cd:	5e                   	pop    %esi
  8013ce:	5f                   	pop    %edi
  8013cf:	5d                   	pop    %ebp
  8013d0:	c3                   	ret    

008013d1 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8013d1:	55                   	push   %ebp
  8013d2:	89 e5                	mov    %esp,%ebp
  8013d4:	53                   	push   %ebx
  8013d5:	83 ec 14             	sub    $0x14,%esp
  8013d8:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8013db:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8013de:	50                   	push   %eax
  8013df:	53                   	push   %ebx
  8013e0:	e8 b6 fc ff ff       	call   80109b <fd_lookup>
  8013e5:	83 c4 08             	add    $0x8,%esp
  8013e8:	85 c0                	test   %eax,%eax
  8013ea:	78 62                	js     80144e <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8013ec:	83 ec 08             	sub    $0x8,%esp
  8013ef:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013f2:	50                   	push   %eax
  8013f3:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8013f6:	ff 30                	pushl  (%eax)
  8013f8:	e8 f5 fc ff ff       	call   8010f2 <dev_lookup>
  8013fd:	83 c4 10             	add    $0x10,%esp
  801400:	85 c0                	test   %eax,%eax
  801402:	78 4a                	js     80144e <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801404:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801407:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80140b:	75 21                	jne    80142e <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  80140d:	a1 90 67 80 00       	mov    0x806790,%eax
  801412:	8b 40 48             	mov    0x48(%eax),%eax
  801415:	83 ec 04             	sub    $0x4,%esp
  801418:	53                   	push   %ebx
  801419:	50                   	push   %eax
  80141a:	68 49 2a 80 00       	push   $0x802a49
  80141f:	e8 a9 f0 ff ff       	call   8004cd <cprintf>
		return -E_INVAL;
  801424:	83 c4 10             	add    $0x10,%esp
  801427:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80142c:	eb 20                	jmp    80144e <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  80142e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801431:	8b 52 0c             	mov    0xc(%edx),%edx
  801434:	85 d2                	test   %edx,%edx
  801436:	74 11                	je     801449 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801438:	83 ec 04             	sub    $0x4,%esp
  80143b:	ff 75 10             	pushl  0x10(%ebp)
  80143e:	ff 75 0c             	pushl  0xc(%ebp)
  801441:	50                   	push   %eax
  801442:	ff d2                	call   *%edx
  801444:	83 c4 10             	add    $0x10,%esp
  801447:	eb 05                	jmp    80144e <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801449:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  80144e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801451:	c9                   	leave  
  801452:	c3                   	ret    

00801453 <seek>:

int
seek(int fdnum, off_t offset)
{
  801453:	55                   	push   %ebp
  801454:	89 e5                	mov    %esp,%ebp
  801456:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801459:	8d 45 fc             	lea    -0x4(%ebp),%eax
  80145c:	50                   	push   %eax
  80145d:	ff 75 08             	pushl  0x8(%ebp)
  801460:	e8 36 fc ff ff       	call   80109b <fd_lookup>
  801465:	83 c4 08             	add    $0x8,%esp
  801468:	85 c0                	test   %eax,%eax
  80146a:	78 0e                	js     80147a <seek+0x27>
		return r;
	fd->fd_offset = offset;
  80146c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  80146f:	8b 55 0c             	mov    0xc(%ebp),%edx
  801472:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801475:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80147a:	c9                   	leave  
  80147b:	c3                   	ret    

0080147c <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  80147c:	55                   	push   %ebp
  80147d:	89 e5                	mov    %esp,%ebp
  80147f:	53                   	push   %ebx
  801480:	83 ec 14             	sub    $0x14,%esp
  801483:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801486:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801489:	50                   	push   %eax
  80148a:	53                   	push   %ebx
  80148b:	e8 0b fc ff ff       	call   80109b <fd_lookup>
  801490:	83 c4 08             	add    $0x8,%esp
  801493:	85 c0                	test   %eax,%eax
  801495:	78 5f                	js     8014f6 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801497:	83 ec 08             	sub    $0x8,%esp
  80149a:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80149d:	50                   	push   %eax
  80149e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8014a1:	ff 30                	pushl  (%eax)
  8014a3:	e8 4a fc ff ff       	call   8010f2 <dev_lookup>
  8014a8:	83 c4 10             	add    $0x10,%esp
  8014ab:	85 c0                	test   %eax,%eax
  8014ad:	78 47                	js     8014f6 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8014af:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8014b2:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8014b6:	75 21                	jne    8014d9 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  8014b8:	a1 90 67 80 00       	mov    0x806790,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  8014bd:	8b 40 48             	mov    0x48(%eax),%eax
  8014c0:	83 ec 04             	sub    $0x4,%esp
  8014c3:	53                   	push   %ebx
  8014c4:	50                   	push   %eax
  8014c5:	68 0c 2a 80 00       	push   $0x802a0c
  8014ca:	e8 fe ef ff ff       	call   8004cd <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8014cf:	83 c4 10             	add    $0x10,%esp
  8014d2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8014d7:	eb 1d                	jmp    8014f6 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8014d9:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8014dc:	8b 52 18             	mov    0x18(%edx),%edx
  8014df:	85 d2                	test   %edx,%edx
  8014e1:	74 0e                	je     8014f1 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8014e3:	83 ec 08             	sub    $0x8,%esp
  8014e6:	ff 75 0c             	pushl  0xc(%ebp)
  8014e9:	50                   	push   %eax
  8014ea:	ff d2                	call   *%edx
  8014ec:	83 c4 10             	add    $0x10,%esp
  8014ef:	eb 05                	jmp    8014f6 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8014f1:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8014f6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8014f9:	c9                   	leave  
  8014fa:	c3                   	ret    

008014fb <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8014fb:	55                   	push   %ebp
  8014fc:	89 e5                	mov    %esp,%ebp
  8014fe:	53                   	push   %ebx
  8014ff:	83 ec 14             	sub    $0x14,%esp
  801502:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801505:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801508:	50                   	push   %eax
  801509:	ff 75 08             	pushl  0x8(%ebp)
  80150c:	e8 8a fb ff ff       	call   80109b <fd_lookup>
  801511:	83 c4 08             	add    $0x8,%esp
  801514:	85 c0                	test   %eax,%eax
  801516:	78 52                	js     80156a <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801518:	83 ec 08             	sub    $0x8,%esp
  80151b:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80151e:	50                   	push   %eax
  80151f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801522:	ff 30                	pushl  (%eax)
  801524:	e8 c9 fb ff ff       	call   8010f2 <dev_lookup>
  801529:	83 c4 10             	add    $0x10,%esp
  80152c:	85 c0                	test   %eax,%eax
  80152e:	78 3a                	js     80156a <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801530:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801533:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  801537:	74 2c                	je     801565 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  801539:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  80153c:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801543:	00 00 00 
	stat->st_isdir = 0;
  801546:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  80154d:	00 00 00 
	stat->st_dev = dev;
  801550:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  801556:	83 ec 08             	sub    $0x8,%esp
  801559:	53                   	push   %ebx
  80155a:	ff 75 f0             	pushl  -0x10(%ebp)
  80155d:	ff 50 14             	call   *0x14(%eax)
  801560:	83 c4 10             	add    $0x10,%esp
  801563:	eb 05                	jmp    80156a <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801565:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  80156a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80156d:	c9                   	leave  
  80156e:	c3                   	ret    

0080156f <stat>:

int
stat(const char *path, struct Stat *stat)
{
  80156f:	55                   	push   %ebp
  801570:	89 e5                	mov    %esp,%ebp
  801572:	56                   	push   %esi
  801573:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801574:	83 ec 08             	sub    $0x8,%esp
  801577:	6a 00                	push   $0x0
  801579:	ff 75 08             	pushl  0x8(%ebp)
  80157c:	e8 e7 01 00 00       	call   801768 <open>
  801581:	89 c3                	mov    %eax,%ebx
  801583:	83 c4 10             	add    $0x10,%esp
  801586:	85 c0                	test   %eax,%eax
  801588:	78 1d                	js     8015a7 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  80158a:	83 ec 08             	sub    $0x8,%esp
  80158d:	ff 75 0c             	pushl  0xc(%ebp)
  801590:	50                   	push   %eax
  801591:	e8 65 ff ff ff       	call   8014fb <fstat>
  801596:	89 c6                	mov    %eax,%esi
	close(fd);
  801598:	89 1c 24             	mov    %ebx,(%esp)
  80159b:	e8 29 fc ff ff       	call   8011c9 <close>
	return r;
  8015a0:	83 c4 10             	add    $0x10,%esp
  8015a3:	89 f0                	mov    %esi,%eax
  8015a5:	eb 00                	jmp    8015a7 <stat+0x38>
}
  8015a7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8015aa:	5b                   	pop    %ebx
  8015ab:	5e                   	pop    %esi
  8015ac:	5d                   	pop    %ebp
  8015ad:	c3                   	ret    

008015ae <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8015ae:	55                   	push   %ebp
  8015af:	89 e5                	mov    %esp,%ebp
  8015b1:	56                   	push   %esi
  8015b2:	53                   	push   %ebx
  8015b3:	89 c6                	mov    %eax,%esi
  8015b5:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8015b7:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8015be:	75 12                	jne    8015d2 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  8015c0:	83 ec 0c             	sub    $0xc,%esp
  8015c3:	6a 01                	push   $0x1
  8015c5:	e8 7b 0c 00 00       	call   802245 <ipc_find_env>
  8015ca:	a3 00 50 80 00       	mov    %eax,0x805000
  8015cf:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8015d2:	6a 07                	push   $0x7
  8015d4:	68 00 70 80 00       	push   $0x807000
  8015d9:	56                   	push   %esi
  8015da:	ff 35 00 50 80 00    	pushl  0x805000
  8015e0:	e8 0b 0c 00 00       	call   8021f0 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8015e5:	83 c4 0c             	add    $0xc,%esp
  8015e8:	6a 00                	push   $0x0
  8015ea:	53                   	push   %ebx
  8015eb:	6a 00                	push   $0x0
  8015ed:	e8 96 0b 00 00       	call   802188 <ipc_recv>
}
  8015f2:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8015f5:	5b                   	pop    %ebx
  8015f6:	5e                   	pop    %esi
  8015f7:	5d                   	pop    %ebp
  8015f8:	c3                   	ret    

008015f9 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8015f9:	55                   	push   %ebp
  8015fa:	89 e5                	mov    %esp,%ebp
  8015fc:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8015ff:	8b 45 08             	mov    0x8(%ebp),%eax
  801602:	8b 40 0c             	mov    0xc(%eax),%eax
  801605:	a3 00 70 80 00       	mov    %eax,0x807000
	fsipcbuf.set_size.req_size = newsize;
  80160a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80160d:	a3 04 70 80 00       	mov    %eax,0x807004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801612:	ba 00 00 00 00       	mov    $0x0,%edx
  801617:	b8 02 00 00 00       	mov    $0x2,%eax
  80161c:	e8 8d ff ff ff       	call   8015ae <fsipc>
}
  801621:	c9                   	leave  
  801622:	c3                   	ret    

00801623 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  801623:	55                   	push   %ebp
  801624:	89 e5                	mov    %esp,%ebp
  801626:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  801629:	8b 45 08             	mov    0x8(%ebp),%eax
  80162c:	8b 40 0c             	mov    0xc(%eax),%eax
  80162f:	a3 00 70 80 00       	mov    %eax,0x807000
	return fsipc(FSREQ_FLUSH, NULL);
  801634:	ba 00 00 00 00       	mov    $0x0,%edx
  801639:	b8 06 00 00 00       	mov    $0x6,%eax
  80163e:	e8 6b ff ff ff       	call   8015ae <fsipc>
}
  801643:	c9                   	leave  
  801644:	c3                   	ret    

00801645 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801645:	55                   	push   %ebp
  801646:	89 e5                	mov    %esp,%ebp
  801648:	53                   	push   %ebx
  801649:	83 ec 04             	sub    $0x4,%esp
  80164c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  80164f:	8b 45 08             	mov    0x8(%ebp),%eax
  801652:	8b 40 0c             	mov    0xc(%eax),%eax
  801655:	a3 00 70 80 00       	mov    %eax,0x807000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  80165a:	ba 00 00 00 00       	mov    $0x0,%edx
  80165f:	b8 05 00 00 00       	mov    $0x5,%eax
  801664:	e8 45 ff ff ff       	call   8015ae <fsipc>
  801669:	85 c0                	test   %eax,%eax
  80166b:	78 2c                	js     801699 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  80166d:	83 ec 08             	sub    $0x8,%esp
  801670:	68 00 70 80 00       	push   $0x807000
  801675:	53                   	push   %ebx
  801676:	e8 b6 f3 ff ff       	call   800a31 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  80167b:	a1 80 70 80 00       	mov    0x807080,%eax
  801680:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801686:	a1 84 70 80 00       	mov    0x807084,%eax
  80168b:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801691:	83 c4 10             	add    $0x10,%esp
  801694:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801699:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80169c:	c9                   	leave  
  80169d:	c3                   	ret    

0080169e <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  80169e:	55                   	push   %ebp
  80169f:	89 e5                	mov    %esp,%ebp
  8016a1:	83 ec 08             	sub    $0x8,%esp
  8016a4:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  8016a7:	8b 55 08             	mov    0x8(%ebp),%edx
  8016aa:	8b 52 0c             	mov    0xc(%edx),%edx
  8016ad:	89 15 00 70 80 00    	mov    %edx,0x807000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  8016b3:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  8016b8:	76 05                	jbe    8016bf <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  8016ba:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  8016bf:	a3 04 70 80 00       	mov    %eax,0x807004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  8016c4:	83 ec 04             	sub    $0x4,%esp
  8016c7:	50                   	push   %eax
  8016c8:	ff 75 0c             	pushl  0xc(%ebp)
  8016cb:	68 08 70 80 00       	push   $0x807008
  8016d0:	e8 d1 f4 ff ff       	call   800ba6 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8016d5:	ba 00 00 00 00       	mov    $0x0,%edx
  8016da:	b8 04 00 00 00       	mov    $0x4,%eax
  8016df:	e8 ca fe ff ff       	call   8015ae <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8016e4:	c9                   	leave  
  8016e5:	c3                   	ret    

008016e6 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8016e6:	55                   	push   %ebp
  8016e7:	89 e5                	mov    %esp,%ebp
  8016e9:	56                   	push   %esi
  8016ea:	53                   	push   %ebx
  8016eb:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8016ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8016f1:	8b 40 0c             	mov    0xc(%eax),%eax
  8016f4:	a3 00 70 80 00       	mov    %eax,0x807000
	fsipcbuf.read.req_n = n;
  8016f9:	89 35 04 70 80 00    	mov    %esi,0x807004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  8016ff:	ba 00 00 00 00       	mov    $0x0,%edx
  801704:	b8 03 00 00 00       	mov    $0x3,%eax
  801709:	e8 a0 fe ff ff       	call   8015ae <fsipc>
  80170e:	89 c3                	mov    %eax,%ebx
  801710:	85 c0                	test   %eax,%eax
  801712:	78 4b                	js     80175f <devfile_read+0x79>
		return r;
	assert(r <= n);
  801714:	39 c6                	cmp    %eax,%esi
  801716:	73 16                	jae    80172e <devfile_read+0x48>
  801718:	68 78 2a 80 00       	push   $0x802a78
  80171d:	68 7f 2a 80 00       	push   $0x802a7f
  801722:	6a 7c                	push   $0x7c
  801724:	68 94 2a 80 00       	push   $0x802a94
  801729:	e8 c7 ec ff ff       	call   8003f5 <_panic>
	assert(r <= PGSIZE);
  80172e:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801733:	7e 16                	jle    80174b <devfile_read+0x65>
  801735:	68 9f 2a 80 00       	push   $0x802a9f
  80173a:	68 7f 2a 80 00       	push   $0x802a7f
  80173f:	6a 7d                	push   $0x7d
  801741:	68 94 2a 80 00       	push   $0x802a94
  801746:	e8 aa ec ff ff       	call   8003f5 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  80174b:	83 ec 04             	sub    $0x4,%esp
  80174e:	50                   	push   %eax
  80174f:	68 00 70 80 00       	push   $0x807000
  801754:	ff 75 0c             	pushl  0xc(%ebp)
  801757:	e8 4a f4 ff ff       	call   800ba6 <memmove>
	return r;
  80175c:	83 c4 10             	add    $0x10,%esp
}
  80175f:	89 d8                	mov    %ebx,%eax
  801761:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801764:	5b                   	pop    %ebx
  801765:	5e                   	pop    %esi
  801766:	5d                   	pop    %ebp
  801767:	c3                   	ret    

00801768 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801768:	55                   	push   %ebp
  801769:	89 e5                	mov    %esp,%ebp
  80176b:	53                   	push   %ebx
  80176c:	83 ec 20             	sub    $0x20,%esp
  80176f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801772:	53                   	push   %ebx
  801773:	e8 84 f2 ff ff       	call   8009fc <strlen>
  801778:	83 c4 10             	add    $0x10,%esp
  80177b:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801780:	7f 63                	jg     8017e5 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801782:	83 ec 0c             	sub    $0xc,%esp
  801785:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801788:	50                   	push   %eax
  801789:	e8 be f8 ff ff       	call   80104c <fd_alloc>
  80178e:	83 c4 10             	add    $0x10,%esp
  801791:	85 c0                	test   %eax,%eax
  801793:	78 55                	js     8017ea <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801795:	83 ec 08             	sub    $0x8,%esp
  801798:	53                   	push   %ebx
  801799:	68 00 70 80 00       	push   $0x807000
  80179e:	e8 8e f2 ff ff       	call   800a31 <strcpy>
	fsipcbuf.open.req_omode = mode;
  8017a3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8017a6:	a3 00 74 80 00       	mov    %eax,0x807400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  8017ab:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8017ae:	b8 01 00 00 00       	mov    $0x1,%eax
  8017b3:	e8 f6 fd ff ff       	call   8015ae <fsipc>
  8017b8:	89 c3                	mov    %eax,%ebx
  8017ba:	83 c4 10             	add    $0x10,%esp
  8017bd:	85 c0                	test   %eax,%eax
  8017bf:	79 14                	jns    8017d5 <open+0x6d>
		fd_close(fd, 0);
  8017c1:	83 ec 08             	sub    $0x8,%esp
  8017c4:	6a 00                	push   $0x0
  8017c6:	ff 75 f4             	pushl  -0xc(%ebp)
  8017c9:	e8 77 f9 ff ff       	call   801145 <fd_close>
		return r;
  8017ce:	83 c4 10             	add    $0x10,%esp
  8017d1:	89 d8                	mov    %ebx,%eax
  8017d3:	eb 15                	jmp    8017ea <open+0x82>
	}

	return fd2num(fd);
  8017d5:	83 ec 0c             	sub    $0xc,%esp
  8017d8:	ff 75 f4             	pushl  -0xc(%ebp)
  8017db:	e8 45 f8 ff ff       	call   801025 <fd2num>
  8017e0:	83 c4 10             	add    $0x10,%esp
  8017e3:	eb 05                	jmp    8017ea <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8017e5:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8017ea:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017ed:	c9                   	leave  
  8017ee:	c3                   	ret    

008017ef <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8017ef:	55                   	push   %ebp
  8017f0:	89 e5                	mov    %esp,%ebp
  8017f2:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8017f5:	ba 00 00 00 00       	mov    $0x0,%edx
  8017fa:	b8 08 00 00 00       	mov    $0x8,%eax
  8017ff:	e8 aa fd ff ff       	call   8015ae <fsipc>
}
  801804:	c9                   	leave  
  801805:	c3                   	ret    

00801806 <spawn>:
// argv: pointer to null-terminated array of pointers to strings,
// 	 which will be passed to the child as its command-line arguments.
// Returns child envid on success, < 0 on failure.
int
spawn(const char *prog, const char **argv)
{
  801806:	55                   	push   %ebp
  801807:	89 e5                	mov    %esp,%ebp
  801809:	57                   	push   %edi
  80180a:	56                   	push   %esi
  80180b:	53                   	push   %ebx
  80180c:	81 ec 94 02 00 00    	sub    $0x294,%esp
  801812:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//   - Call sys_env_set_trapframe(child, &child_tf) to set up the
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
  801815:	6a 00                	push   $0x0
  801817:	ff 75 08             	pushl  0x8(%ebp)
  80181a:	e8 49 ff ff ff       	call   801768 <open>
  80181f:	89 c1                	mov    %eax,%ecx
  801821:	89 85 8c fd ff ff    	mov    %eax,-0x274(%ebp)
  801827:	83 c4 10             	add    $0x10,%esp
  80182a:	85 c0                	test   %eax,%eax
  80182c:	0f 88 e2 04 00 00    	js     801d14 <spawn+0x50e>
		return r;
	fd = r;

	// Read elf header
	elf = (struct Elf*) elf_buf;
	if (readn(fd, elf_buf, sizeof(elf_buf)) != sizeof(elf_buf)
  801832:	83 ec 04             	sub    $0x4,%esp
  801835:	68 00 02 00 00       	push   $0x200
  80183a:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  801840:	50                   	push   %eax
  801841:	51                   	push   %ecx
  801842:	e8 41 fb ff ff       	call   801388 <readn>
  801847:	83 c4 10             	add    $0x10,%esp
  80184a:	3d 00 02 00 00       	cmp    $0x200,%eax
  80184f:	75 0c                	jne    80185d <spawn+0x57>
	    || elf->e_magic != ELF_MAGIC) {
  801851:	81 bd e8 fd ff ff 7f 	cmpl   $0x464c457f,-0x218(%ebp)
  801858:	45 4c 46 
  80185b:	74 33                	je     801890 <spawn+0x8a>
		close(fd);
  80185d:	83 ec 0c             	sub    $0xc,%esp
  801860:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801866:	e8 5e f9 ff ff       	call   8011c9 <close>
		cprintf("elf magic %08x want %08x\n", elf->e_magic, ELF_MAGIC);
  80186b:	83 c4 0c             	add    $0xc,%esp
  80186e:	68 7f 45 4c 46       	push   $0x464c457f
  801873:	ff b5 e8 fd ff ff    	pushl  -0x218(%ebp)
  801879:	68 ab 2a 80 00       	push   $0x802aab
  80187e:	e8 4a ec ff ff       	call   8004cd <cprintf>
		return -E_NOT_EXEC;
  801883:	83 c4 10             	add    $0x10,%esp
  801886:	bb f2 ff ff ff       	mov    $0xfffffff2,%ebx
  80188b:	e9 e4 04 00 00       	jmp    801d74 <spawn+0x56e>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  801890:	b8 07 00 00 00       	mov    $0x7,%eax
  801895:	cd 30                	int    $0x30
  801897:	89 85 74 fd ff ff    	mov    %eax,-0x28c(%ebp)
  80189d:	89 85 84 fd ff ff    	mov    %eax,-0x27c(%ebp)
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
  8018a3:	85 c0                	test   %eax,%eax
  8018a5:	0f 88 71 04 00 00    	js     801d1c <spawn+0x516>
		return r;
	child = r;

	// Set up trap frame, including initial stack.
	child_tf = envs[ENVX(child)].env_tf;
  8018ab:	25 ff 03 00 00       	and    $0x3ff,%eax
  8018b0:	89 c6                	mov    %eax,%esi
  8018b2:	8d 04 85 00 00 00 00 	lea    0x0(,%eax,4),%eax
  8018b9:	c1 e6 07             	shl    $0x7,%esi
  8018bc:	29 c6                	sub    %eax,%esi
  8018be:	81 c6 00 00 c0 ee    	add    $0xeec00000,%esi
  8018c4:	8d bd a4 fd ff ff    	lea    -0x25c(%ebp),%edi
  8018ca:	b9 11 00 00 00       	mov    $0x11,%ecx
  8018cf:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	child_tf.tf_eip = elf->e_entry;
  8018d1:	8b 85 00 fe ff ff    	mov    -0x200(%ebp),%eax
  8018d7:	89 85 d4 fd ff ff    	mov    %eax,-0x22c(%ebp)
  8018dd:	ba 00 00 00 00       	mov    $0x0,%edx
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  8018e2:	b8 00 00 00 00       	mov    $0x0,%eax
	char *string_store;
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
  8018e7:	bf 00 00 00 00       	mov    $0x0,%edi
  8018ec:	89 5d 0c             	mov    %ebx,0xc(%ebp)
  8018ef:	89 c3                	mov    %eax,%ebx
  8018f1:	eb 13                	jmp    801906 <spawn+0x100>
	for (argc = 0; argv[argc] != 0; argc++)
		string_size += strlen(argv[argc]) + 1;
  8018f3:	83 ec 0c             	sub    $0xc,%esp
  8018f6:	50                   	push   %eax
  8018f7:	e8 00 f1 ff ff       	call   8009fc <strlen>
  8018fc:	8d 7c 38 01          	lea    0x1(%eax,%edi,1),%edi
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  801900:	43                   	inc    %ebx
  801901:	89 f2                	mov    %esi,%edx
  801903:	83 c4 10             	add    $0x10,%esp
  801906:	89 d9                	mov    %ebx,%ecx
  801908:	8d 72 04             	lea    0x4(%edx),%esi
  80190b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80190e:	8b 44 30 fc          	mov    -0x4(%eax,%esi,1),%eax
  801912:	85 c0                	test   %eax,%eax
  801914:	75 dd                	jne    8018f3 <spawn+0xed>
  801916:	89 9d 90 fd ff ff    	mov    %ebx,-0x270(%ebp)
  80191c:	89 9d 88 fd ff ff    	mov    %ebx,-0x278(%ebp)
  801922:	89 95 80 fd ff ff    	mov    %edx,-0x280(%ebp)
  801928:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Determine where to place the strings and the argv array.
	// Set up pointers into the temporary page 'UTEMP'; we'll map a page
	// there later, then remap that page into the child environment
	// at (USTACKTOP - PGSIZE).
	// strings is the topmost thing on the stack.
	string_store = (char*) UTEMP + PGSIZE - string_size;
  80192b:	b8 00 10 40 00       	mov    $0x401000,%eax
  801930:	29 f8                	sub    %edi,%eax
  801932:	89 c7                	mov    %eax,%edi
	// argv is below that.  There's one argument pointer per argument, plus
	// a null pointer.
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));
  801934:	89 c2                	mov    %eax,%edx
  801936:	83 e2 fc             	and    $0xfffffffc,%edx
  801939:	8d 04 8d 04 00 00 00 	lea    0x4(,%ecx,4),%eax
  801940:	29 c2                	sub    %eax,%edx
  801942:	89 95 94 fd ff ff    	mov    %edx,-0x26c(%ebp)

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
  801948:	8d 42 f8             	lea    -0x8(%edx),%eax
  80194b:	3d ff ff 3f 00       	cmp    $0x3fffff,%eax
  801950:	0f 86 d6 03 00 00    	jbe    801d2c <spawn+0x526>
		return -E_NO_MEM;

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801956:	83 ec 04             	sub    $0x4,%esp
  801959:	6a 07                	push   $0x7
  80195b:	68 00 00 40 00       	push   $0x400000
  801960:	6a 00                	push   $0x0
  801962:	e8 ae f4 ff ff       	call   800e15 <sys_page_alloc>
  801967:	83 c4 10             	add    $0x10,%esp
  80196a:	85 c0                	test   %eax,%eax
  80196c:	0f 88 c1 03 00 00    	js     801d33 <spawn+0x52d>
  801972:	be 00 00 00 00       	mov    $0x0,%esi
  801977:	eb 2e                	jmp    8019a7 <spawn+0x1a1>
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
		argv_store[i] = UTEMP2USTACK(string_store);
  801979:	8d 87 00 d0 7f ee    	lea    -0x11803000(%edi),%eax
  80197f:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  801985:	89 04 b1             	mov    %eax,(%ecx,%esi,4)
		strcpy(string_store, argv[i]);
  801988:	83 ec 08             	sub    $0x8,%esp
  80198b:	ff 34 b3             	pushl  (%ebx,%esi,4)
  80198e:	57                   	push   %edi
  80198f:	e8 9d f0 ff ff       	call   800a31 <strcpy>
		string_store += strlen(argv[i]) + 1;
  801994:	83 c4 04             	add    $0x4,%esp
  801997:	ff 34 b3             	pushl  (%ebx,%esi,4)
  80199a:	e8 5d f0 ff ff       	call   8009fc <strlen>
  80199f:	8d 7c 07 01          	lea    0x1(%edi,%eax,1),%edi
	//	  (Again, argv should use an address valid in the child's
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
  8019a3:	46                   	inc    %esi
  8019a4:	83 c4 10             	add    $0x10,%esp
  8019a7:	39 b5 90 fd ff ff    	cmp    %esi,-0x270(%ebp)
  8019ad:	7f ca                	jg     801979 <spawn+0x173>
		argv_store[i] = UTEMP2USTACK(string_store);
		strcpy(string_store, argv[i]);
		string_store += strlen(argv[i]) + 1;
	}
	argv_store[argc] = 0;
  8019af:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  8019b5:	8b 8d 80 fd ff ff    	mov    -0x280(%ebp),%ecx
  8019bb:	c7 04 08 00 00 00 00 	movl   $0x0,(%eax,%ecx,1)
	assert(string_store == (char*)UTEMP + PGSIZE);
  8019c2:	81 ff 00 10 40 00    	cmp    $0x401000,%edi
  8019c8:	74 19                	je     8019e3 <spawn+0x1dd>
  8019ca:	68 38 2b 80 00       	push   $0x802b38
  8019cf:	68 7f 2a 80 00       	push   $0x802a7f
  8019d4:	68 f1 00 00 00       	push   $0xf1
  8019d9:	68 c5 2a 80 00       	push   $0x802ac5
  8019de:	e8 12 ea ff ff       	call   8003f5 <_panic>

	argv_store[-1] = UTEMP2USTACK(argv_store);
  8019e3:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  8019e9:	89 c8                	mov    %ecx,%eax
  8019eb:	2d 00 30 80 11       	sub    $0x11803000,%eax
  8019f0:	89 41 fc             	mov    %eax,-0x4(%ecx)
	argv_store[-2] = argc;
  8019f3:	89 c8                	mov    %ecx,%eax
  8019f5:	8b 8d 88 fd ff ff    	mov    -0x278(%ebp),%ecx
  8019fb:	89 48 f8             	mov    %ecx,-0x8(%eax)

	*init_esp = UTEMP2USTACK(&argv_store[-2]);
  8019fe:	2d 08 30 80 11       	sub    $0x11803008,%eax
  801a03:	89 85 e0 fd ff ff    	mov    %eax,-0x220(%ebp)

	// After completing the stack, map it into the child's address space
	// and unmap it from ours!
	if ((r = sys_page_map(0, UTEMP, child, (void*) (USTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W)) < 0)
  801a09:	83 ec 0c             	sub    $0xc,%esp
  801a0c:	6a 07                	push   $0x7
  801a0e:	68 00 d0 bf ee       	push   $0xeebfd000
  801a13:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801a19:	68 00 00 40 00       	push   $0x400000
  801a1e:	6a 00                	push   $0x0
  801a20:	e8 33 f4 ff ff       	call   800e58 <sys_page_map>
  801a25:	89 c3                	mov    %eax,%ebx
  801a27:	83 c4 20             	add    $0x20,%esp
  801a2a:	85 c0                	test   %eax,%eax
  801a2c:	0f 88 30 03 00 00    	js     801d62 <spawn+0x55c>
		goto error;
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  801a32:	83 ec 08             	sub    $0x8,%esp
  801a35:	68 00 00 40 00       	push   $0x400000
  801a3a:	6a 00                	push   $0x0
  801a3c:	e8 59 f4 ff ff       	call   800e9a <sys_page_unmap>
  801a41:	89 c3                	mov    %eax,%ebx
  801a43:	83 c4 10             	add    $0x10,%esp
  801a46:	85 c0                	test   %eax,%eax
  801a48:	0f 88 14 03 00 00    	js     801d62 <spawn+0x55c>

	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
  801a4e:	8b 85 04 fe ff ff    	mov    -0x1fc(%ebp),%eax
  801a54:	8d 84 05 e8 fd ff ff 	lea    -0x218(%ebp,%eax,1),%eax
  801a5b:	89 85 7c fd ff ff    	mov    %eax,-0x284(%ebp)
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  801a61:	c7 85 78 fd ff ff 00 	movl   $0x0,-0x288(%ebp)
  801a68:	00 00 00 
  801a6b:	e9 88 01 00 00       	jmp    801bf8 <spawn+0x3f2>
		if (ph->p_type != ELF_PROG_LOAD)
  801a70:	8b 85 7c fd ff ff    	mov    -0x284(%ebp),%eax
  801a76:	83 38 01             	cmpl   $0x1,(%eax)
  801a79:	0f 85 6c 01 00 00    	jne    801beb <spawn+0x3e5>
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
  801a7f:	89 c1                	mov    %eax,%ecx
  801a81:	8b 40 18             	mov    0x18(%eax),%eax
  801a84:	83 e0 02             	and    $0x2,%eax
			perm |= PTE_W;
  801a87:	83 f8 01             	cmp    $0x1,%eax
  801a8a:	19 c0                	sbb    %eax,%eax
  801a8c:	83 e0 fe             	and    $0xfffffffe,%eax
  801a8f:	83 c0 07             	add    $0x7,%eax
  801a92:	89 85 88 fd ff ff    	mov    %eax,-0x278(%ebp)
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
  801a98:	89 c8                	mov    %ecx,%eax
  801a9a:	8b 49 04             	mov    0x4(%ecx),%ecx
  801a9d:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
  801aa3:	8b 78 10             	mov    0x10(%eax),%edi
  801aa6:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  801aac:	8b 70 14             	mov    0x14(%eax),%esi
  801aaf:	89 f2                	mov    %esi,%edx
  801ab1:	89 b5 90 fd ff ff    	mov    %esi,-0x270(%ebp)
  801ab7:	8b 70 08             	mov    0x8(%eax),%esi
	int i, r;
	void *blk;

	//cprintf("map_segment %x+%x\n", va, memsz);

	if ((i = PGOFF(va))) {
  801aba:	89 f0                	mov    %esi,%eax
  801abc:	25 ff 0f 00 00       	and    $0xfff,%eax
  801ac1:	74 1a                	je     801add <spawn+0x2d7>
		va -= i;
  801ac3:	29 c6                	sub    %eax,%esi
		memsz += i;
  801ac5:	01 c2                	add    %eax,%edx
  801ac7:	89 95 90 fd ff ff    	mov    %edx,-0x270(%ebp)
		filesz += i;
  801acd:	01 c7                	add    %eax,%edi
  801acf:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
		fileoffset -= i;
  801ad5:	29 c1                	sub    %eax,%ecx
  801ad7:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  801add:	bb 00 00 00 00       	mov    $0x0,%ebx
  801ae2:	e9 f6 00 00 00       	jmp    801bdd <spawn+0x3d7>
		if (i >= filesz) {
  801ae7:	39 9d 94 fd ff ff    	cmp    %ebx,-0x26c(%ebp)
  801aed:	77 27                	ja     801b16 <spawn+0x310>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
  801aef:	83 ec 04             	sub    $0x4,%esp
  801af2:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801af8:	56                   	push   %esi
  801af9:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801aff:	e8 11 f3 ff ff       	call   800e15 <sys_page_alloc>
  801b04:	83 c4 10             	add    $0x10,%esp
  801b07:	85 c0                	test   %eax,%eax
  801b09:	0f 89 c2 00 00 00    	jns    801bd1 <spawn+0x3cb>
  801b0f:	89 c3                	mov    %eax,%ebx
  801b11:	e9 2b 02 00 00       	jmp    801d41 <spawn+0x53b>
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801b16:	83 ec 04             	sub    $0x4,%esp
  801b19:	6a 07                	push   $0x7
  801b1b:	68 00 00 40 00       	push   $0x400000
  801b20:	6a 00                	push   $0x0
  801b22:	e8 ee f2 ff ff       	call   800e15 <sys_page_alloc>
  801b27:	83 c4 10             	add    $0x10,%esp
  801b2a:	85 c0                	test   %eax,%eax
  801b2c:	0f 88 05 02 00 00    	js     801d37 <spawn+0x531>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801b32:	83 ec 08             	sub    $0x8,%esp
  801b35:	8b 85 80 fd ff ff    	mov    -0x280(%ebp),%eax
  801b3b:	01 f8                	add    %edi,%eax
  801b3d:	50                   	push   %eax
  801b3e:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801b44:	e8 0a f9 ff ff       	call   801453 <seek>
  801b49:	83 c4 10             	add    $0x10,%esp
  801b4c:	85 c0                	test   %eax,%eax
  801b4e:	0f 88 e7 01 00 00    	js     801d3b <spawn+0x535>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801b54:	83 ec 04             	sub    $0x4,%esp
  801b57:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801b5d:	29 f8                	sub    %edi,%eax
  801b5f:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801b64:	76 05                	jbe    801b6b <spawn+0x365>
  801b66:	b8 00 10 00 00       	mov    $0x1000,%eax
  801b6b:	50                   	push   %eax
  801b6c:	68 00 00 40 00       	push   $0x400000
  801b71:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801b77:	e8 0c f8 ff ff       	call   801388 <readn>
  801b7c:	83 c4 10             	add    $0x10,%esp
  801b7f:	85 c0                	test   %eax,%eax
  801b81:	0f 88 b8 01 00 00    	js     801d3f <spawn+0x539>
				return r;
			if ((r = sys_page_map(0, UTEMP, child, (void*) (va + i), perm)) < 0)
  801b87:	83 ec 0c             	sub    $0xc,%esp
  801b8a:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801b90:	56                   	push   %esi
  801b91:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801b97:	68 00 00 40 00       	push   $0x400000
  801b9c:	6a 00                	push   $0x0
  801b9e:	e8 b5 f2 ff ff       	call   800e58 <sys_page_map>
  801ba3:	83 c4 20             	add    $0x20,%esp
  801ba6:	85 c0                	test   %eax,%eax
  801ba8:	79 15                	jns    801bbf <spawn+0x3b9>
				panic("spawn: sys_page_map data: %e", r);
  801baa:	50                   	push   %eax
  801bab:	68 d1 2a 80 00       	push   $0x802ad1
  801bb0:	68 24 01 00 00       	push   $0x124
  801bb5:	68 c5 2a 80 00       	push   $0x802ac5
  801bba:	e8 36 e8 ff ff       	call   8003f5 <_panic>
			sys_page_unmap(0, UTEMP);
  801bbf:	83 ec 08             	sub    $0x8,%esp
  801bc2:	68 00 00 40 00       	push   $0x400000
  801bc7:	6a 00                	push   $0x0
  801bc9:	e8 cc f2 ff ff       	call   800e9a <sys_page_unmap>
  801bce:	83 c4 10             	add    $0x10,%esp
		memsz += i;
		filesz += i;
		fileoffset -= i;
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  801bd1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  801bd7:	81 c6 00 10 00 00    	add    $0x1000,%esi
  801bdd:	89 df                	mov    %ebx,%edi
  801bdf:	39 9d 90 fd ff ff    	cmp    %ebx,-0x270(%ebp)
  801be5:	0f 87 fc fe ff ff    	ja     801ae7 <spawn+0x2e1>
	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  801beb:	ff 85 78 fd ff ff    	incl   -0x288(%ebp)
  801bf1:	83 85 7c fd ff ff 20 	addl   $0x20,-0x284(%ebp)
  801bf8:	0f b7 85 14 fe ff ff 	movzwl -0x1ec(%ebp),%eax
  801bff:	39 85 78 fd ff ff    	cmp    %eax,-0x288(%ebp)
  801c05:	0f 8c 65 fe ff ff    	jl     801a70 <spawn+0x26a>
			perm |= PTE_W;
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
				     fd, ph->p_filesz, ph->p_offset, perm)) < 0)
			goto error;
	}
	close(fd);
  801c0b:	83 ec 0c             	sub    $0xc,%esp
  801c0e:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801c14:	e8 b0 f5 ff ff       	call   8011c9 <close>
  801c19:	83 c4 10             	add    $0x10,%esp
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801c1c:	bf 00 00 00 00       	mov    $0x0,%edi
  801c21:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  801c27:	8b bd 84 fd ff ff    	mov    -0x27c(%ebp),%edi
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  801c2d:	8b b5 94 fd ff ff    	mov    -0x26c(%ebp),%esi
  801c33:	8b 04 b5 00 d0 7b ef 	mov    -0x10843000(,%esi,4),%eax
  801c3a:	a8 01                	test   $0x1,%al
  801c3c:	74 4b                	je     801c89 <spawn+0x483>
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
        	/* LAB 4: Your code here.
		    pte_t pte = uvpt[pn];
		    void *va = (void *)(pn << PGSHIFT);
		    int perm = pte & PTE_SYSCALL;*/
		    unsigned pn = (ipd << 10) | ipt;
  801c3e:	c1 e6 0a             	shl    $0xa,%esi
  801c41:	bb 00 00 00 00       	mov    $0x0,%ebx
  801c46:	89 f0                	mov    %esi,%eax
  801c48:	09 d8                	or     %ebx,%eax
        	if(!(uvpt[pn] & PTE_P)){ //doesn't exist
  801c4a:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801c51:	f6 c2 01             	test   $0x1,%dl
  801c54:	74 2a                	je     801c80 <spawn+0x47a>
        		continue;
        	}
	        int perm = uvpt[pn] & PTE_SYSCALL;
  801c56:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
	        if(perm & PTE_SHARE){
  801c5d:	f6 c6 04             	test   $0x4,%dh
  801c60:	74 1e                	je     801c80 <spawn+0x47a>
			    void *va = (void *)(pn << PGSHIFT);
  801c62:	c1 e0 0c             	shl    $0xc,%eax
			    r = sys_page_map(0, va, child, va, perm);
  801c65:	83 ec 0c             	sub    $0xc,%esp
  801c68:	81 e2 07 0e 00 00    	and    $0xe07,%edx
  801c6e:	52                   	push   %edx
  801c6f:	50                   	push   %eax
  801c70:	57                   	push   %edi
  801c71:	50                   	push   %eax
  801c72:	6a 00                	push   $0x0
  801c74:	e8 df f1 ff ff       	call   800e58 <sys_page_map>
			    if (r){
  801c79:	83 c4 20             	add    $0x20,%esp
  801c7c:	85 c0                	test   %eax,%eax
  801c7e:	75 1e                	jne    801c9e <spawn+0x498>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801c80:	43                   	inc    %ebx
  801c81:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  801c87:	75 bd                	jne    801c46 <spawn+0x440>
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801c89:	ff 85 94 fd ff ff    	incl   -0x26c(%ebp)
  801c8f:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801c95:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  801c9a:	75 91                	jne    801c2d <spawn+0x427>
  801c9c:	eb 19                	jmp    801cb7 <spawn+0x4b1>
	}
	close(fd);
	fd = -1;

	// Copy shared library state.
	if ((r = copy_shared_pages(child)) < 0)
  801c9e:	85 c0                	test   %eax,%eax
  801ca0:	79 15                	jns    801cb7 <spawn+0x4b1>
		panic("copy_shared_pages: %e", r);
  801ca2:	50                   	push   %eax
  801ca3:	68 ee 2a 80 00       	push   $0x802aee
  801ca8:	68 82 00 00 00       	push   $0x82
  801cad:	68 c5 2a 80 00       	push   $0x802ac5
  801cb2:	e8 3e e7 ff ff       	call   8003f5 <_panic>

	if ((r = sys_env_set_trapframe(child, &child_tf)) < 0)
  801cb7:	83 ec 08             	sub    $0x8,%esp
  801cba:	8d 85 a4 fd ff ff    	lea    -0x25c(%ebp),%eax
  801cc0:	50                   	push   %eax
  801cc1:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801cc7:	e8 71 f2 ff ff       	call   800f3d <sys_env_set_trapframe>
  801ccc:	83 c4 10             	add    $0x10,%esp
  801ccf:	85 c0                	test   %eax,%eax
  801cd1:	79 15                	jns    801ce8 <spawn+0x4e2>
		panic("sys_env_set_trapframe: %e", r);
  801cd3:	50                   	push   %eax
  801cd4:	68 04 2b 80 00       	push   $0x802b04
  801cd9:	68 85 00 00 00       	push   $0x85
  801cde:	68 c5 2a 80 00       	push   $0x802ac5
  801ce3:	e8 0d e7 ff ff       	call   8003f5 <_panic>

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
  801ce8:	83 ec 08             	sub    $0x8,%esp
  801ceb:	6a 02                	push   $0x2
  801ced:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801cf3:	e8 e4 f1 ff ff       	call   800edc <sys_env_set_status>
  801cf8:	83 c4 10             	add    $0x10,%esp
  801cfb:	85 c0                	test   %eax,%eax
  801cfd:	79 25                	jns    801d24 <spawn+0x51e>
		panic("sys_env_set_status: %e", r);
  801cff:	50                   	push   %eax
  801d00:	68 1e 2b 80 00       	push   $0x802b1e
  801d05:	68 88 00 00 00       	push   $0x88
  801d0a:	68 c5 2a 80 00       	push   $0x802ac5
  801d0f:	e8 e1 e6 ff ff       	call   8003f5 <_panic>
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
		return r;
  801d14:	8b 9d 8c fd ff ff    	mov    -0x274(%ebp),%ebx
  801d1a:	eb 58                	jmp    801d74 <spawn+0x56e>
		return -E_NOT_EXEC;
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
		return r;
  801d1c:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801d22:	eb 50                	jmp    801d74 <spawn+0x56e>
		panic("sys_env_set_trapframe: %e", r);

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
		panic("sys_env_set_status: %e", r);

	return child;
  801d24:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801d2a:	eb 48                	jmp    801d74 <spawn+0x56e>
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
		return -E_NO_MEM;
  801d2c:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
  801d31:	eb 41                	jmp    801d74 <spawn+0x56e>

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
		return r;
  801d33:	89 c3                	mov    %eax,%ebx
  801d35:	eb 3d                	jmp    801d74 <spawn+0x56e>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801d37:	89 c3                	mov    %eax,%ebx
  801d39:	eb 06                	jmp    801d41 <spawn+0x53b>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801d3b:	89 c3                	mov    %eax,%ebx
  801d3d:	eb 02                	jmp    801d41 <spawn+0x53b>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801d3f:	89 c3                	mov    %eax,%ebx
		panic("sys_env_set_status: %e", r);

	return child;

error:
	sys_env_destroy(child);
  801d41:	83 ec 0c             	sub    $0xc,%esp
  801d44:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801d4a:	e8 47 f0 ff ff       	call   800d96 <sys_env_destroy>
	close(fd);
  801d4f:	83 c4 04             	add    $0x4,%esp
  801d52:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801d58:	e8 6c f4 ff ff       	call   8011c9 <close>
	return r;
  801d5d:	83 c4 10             	add    $0x10,%esp
  801d60:	eb 12                	jmp    801d74 <spawn+0x56e>
		goto error;

	return 0;

error:
	sys_page_unmap(0, UTEMP);
  801d62:	83 ec 08             	sub    $0x8,%esp
  801d65:	68 00 00 40 00       	push   $0x400000
  801d6a:	6a 00                	push   $0x0
  801d6c:	e8 29 f1 ff ff       	call   800e9a <sys_page_unmap>
  801d71:	83 c4 10             	add    $0x10,%esp

error:
	sys_env_destroy(child);
	close(fd);
	return r;
}
  801d74:	89 d8                	mov    %ebx,%eax
  801d76:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d79:	5b                   	pop    %ebx
  801d7a:	5e                   	pop    %esi
  801d7b:	5f                   	pop    %edi
  801d7c:	5d                   	pop    %ebp
  801d7d:	c3                   	ret    

00801d7e <spawnl>:
// Spawn, taking command-line arguments array directly on the stack.
// NOTE: Must have a sentinal of NULL at the end of the args
// (none of the args may be NULL).
int
spawnl(const char *prog, const char *arg0, ...)
{
  801d7e:	55                   	push   %ebp
  801d7f:	89 e5                	mov    %esp,%ebp
  801d81:	56                   	push   %esi
  801d82:	53                   	push   %ebx
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801d83:	8d 55 10             	lea    0x10(%ebp),%edx
{
	// We calculate argc by advancing the args until we hit NULL.
	// The contract of the function guarantees that the last
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
  801d86:	b8 00 00 00 00       	mov    $0x0,%eax
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801d8b:	eb 01                	jmp    801d8e <spawnl+0x10>
		argc++;
  801d8d:	40                   	inc    %eax
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801d8e:	83 c2 04             	add    $0x4,%edx
  801d91:	83 7a fc 00          	cmpl   $0x0,-0x4(%edx)
  801d95:	75 f6                	jne    801d8d <spawnl+0xf>
		argc++;
	va_end(vl);

	// Now that we have the size of the args, do a second pass
	// and store the values in a VLA, which has the format of argv
	const char *argv[argc+2];
  801d97:	8d 14 85 1a 00 00 00 	lea    0x1a(,%eax,4),%edx
  801d9e:	83 e2 f0             	and    $0xfffffff0,%edx
  801da1:	29 d4                	sub    %edx,%esp
  801da3:	8d 54 24 03          	lea    0x3(%esp),%edx
  801da7:	c1 ea 02             	shr    $0x2,%edx
  801daa:	8d 34 95 00 00 00 00 	lea    0x0(,%edx,4),%esi
  801db1:	89 f3                	mov    %esi,%ebx
	argv[0] = arg0;
  801db3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801db6:	89 0c 95 00 00 00 00 	mov    %ecx,0x0(,%edx,4)
	argv[argc+1] = NULL;
  801dbd:	c7 44 86 04 00 00 00 	movl   $0x0,0x4(%esi,%eax,4)
  801dc4:	00 
  801dc5:	89 c2                	mov    %eax,%edx

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  801dc7:	b8 00 00 00 00       	mov    $0x0,%eax
  801dcc:	eb 08                	jmp    801dd6 <spawnl+0x58>
		argv[i+1] = va_arg(vl, const char *);
  801dce:	40                   	inc    %eax
  801dcf:	8b 4c 85 0c          	mov    0xc(%ebp,%eax,4),%ecx
  801dd3:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
	argv[0] = arg0;
	argv[argc+1] = NULL;

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  801dd6:	39 d0                	cmp    %edx,%eax
  801dd8:	75 f4                	jne    801dce <spawnl+0x50>
		argv[i+1] = va_arg(vl, const char *);
	va_end(vl);
	return spawn(prog, argv);
  801dda:	83 ec 08             	sub    $0x8,%esp
  801ddd:	56                   	push   %esi
  801dde:	ff 75 08             	pushl  0x8(%ebp)
  801de1:	e8 20 fa ff ff       	call   801806 <spawn>
}
  801de6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801de9:	5b                   	pop    %ebx
  801dea:	5e                   	pop    %esi
  801deb:	5d                   	pop    %ebp
  801dec:	c3                   	ret    

00801ded <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801ded:	55                   	push   %ebp
  801dee:	89 e5                	mov    %esp,%ebp
  801df0:	56                   	push   %esi
  801df1:	53                   	push   %ebx
  801df2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801df5:	83 ec 0c             	sub    $0xc,%esp
  801df8:	ff 75 08             	pushl  0x8(%ebp)
  801dfb:	e8 35 f2 ff ff       	call   801035 <fd2data>
  801e00:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801e02:	83 c4 08             	add    $0x8,%esp
  801e05:	68 60 2b 80 00       	push   $0x802b60
  801e0a:	53                   	push   %ebx
  801e0b:	e8 21 ec ff ff       	call   800a31 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801e10:	8b 46 04             	mov    0x4(%esi),%eax
  801e13:	2b 06                	sub    (%esi),%eax
  801e15:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801e1b:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801e22:	00 00 00 
	stat->st_dev = &devpipe;
  801e25:	c7 83 88 00 00 00 ac 	movl   $0x8047ac,0x88(%ebx)
  801e2c:	47 80 00 
	return 0;
}
  801e2f:	b8 00 00 00 00       	mov    $0x0,%eax
  801e34:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801e37:	5b                   	pop    %ebx
  801e38:	5e                   	pop    %esi
  801e39:	5d                   	pop    %ebp
  801e3a:	c3                   	ret    

00801e3b <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801e3b:	55                   	push   %ebp
  801e3c:	89 e5                	mov    %esp,%ebp
  801e3e:	53                   	push   %ebx
  801e3f:	83 ec 0c             	sub    $0xc,%esp
  801e42:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801e45:	53                   	push   %ebx
  801e46:	6a 00                	push   $0x0
  801e48:	e8 4d f0 ff ff       	call   800e9a <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801e4d:	89 1c 24             	mov    %ebx,(%esp)
  801e50:	e8 e0 f1 ff ff       	call   801035 <fd2data>
  801e55:	83 c4 08             	add    $0x8,%esp
  801e58:	50                   	push   %eax
  801e59:	6a 00                	push   $0x0
  801e5b:	e8 3a f0 ff ff       	call   800e9a <sys_page_unmap>
}
  801e60:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801e63:	c9                   	leave  
  801e64:	c3                   	ret    

00801e65 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801e65:	55                   	push   %ebp
  801e66:	89 e5                	mov    %esp,%ebp
  801e68:	57                   	push   %edi
  801e69:	56                   	push   %esi
  801e6a:	53                   	push   %ebx
  801e6b:	83 ec 1c             	sub    $0x1c,%esp
  801e6e:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801e71:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801e73:	a1 90 67 80 00       	mov    0x806790,%eax
  801e78:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801e7b:	83 ec 0c             	sub    $0xc,%esp
  801e7e:	ff 75 e0             	pushl  -0x20(%ebp)
  801e81:	e8 03 04 00 00       	call   802289 <pageref>
  801e86:	89 c3                	mov    %eax,%ebx
  801e88:	89 3c 24             	mov    %edi,(%esp)
  801e8b:	e8 f9 03 00 00       	call   802289 <pageref>
  801e90:	83 c4 10             	add    $0x10,%esp
  801e93:	39 c3                	cmp    %eax,%ebx
  801e95:	0f 94 c1             	sete   %cl
  801e98:	0f b6 c9             	movzbl %cl,%ecx
  801e9b:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801e9e:	8b 15 90 67 80 00    	mov    0x806790,%edx
  801ea4:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801ea7:	39 ce                	cmp    %ecx,%esi
  801ea9:	74 1b                	je     801ec6 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801eab:	39 c3                	cmp    %eax,%ebx
  801ead:	75 c4                	jne    801e73 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801eaf:	8b 42 58             	mov    0x58(%edx),%eax
  801eb2:	ff 75 e4             	pushl  -0x1c(%ebp)
  801eb5:	50                   	push   %eax
  801eb6:	56                   	push   %esi
  801eb7:	68 67 2b 80 00       	push   $0x802b67
  801ebc:	e8 0c e6 ff ff       	call   8004cd <cprintf>
  801ec1:	83 c4 10             	add    $0x10,%esp
  801ec4:	eb ad                	jmp    801e73 <_pipeisclosed+0xe>
	}
}
  801ec6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801ec9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801ecc:	5b                   	pop    %ebx
  801ecd:	5e                   	pop    %esi
  801ece:	5f                   	pop    %edi
  801ecf:	5d                   	pop    %ebp
  801ed0:	c3                   	ret    

00801ed1 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801ed1:	55                   	push   %ebp
  801ed2:	89 e5                	mov    %esp,%ebp
  801ed4:	57                   	push   %edi
  801ed5:	56                   	push   %esi
  801ed6:	53                   	push   %ebx
  801ed7:	83 ec 18             	sub    $0x18,%esp
  801eda:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801edd:	56                   	push   %esi
  801ede:	e8 52 f1 ff ff       	call   801035 <fd2data>
  801ee3:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801ee5:	83 c4 10             	add    $0x10,%esp
  801ee8:	bf 00 00 00 00       	mov    $0x0,%edi
  801eed:	eb 3b                	jmp    801f2a <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801eef:	89 da                	mov    %ebx,%edx
  801ef1:	89 f0                	mov    %esi,%eax
  801ef3:	e8 6d ff ff ff       	call   801e65 <_pipeisclosed>
  801ef8:	85 c0                	test   %eax,%eax
  801efa:	75 38                	jne    801f34 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801efc:	e8 f5 ee ff ff       	call   800df6 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801f01:	8b 53 04             	mov    0x4(%ebx),%edx
  801f04:	8b 03                	mov    (%ebx),%eax
  801f06:	83 c0 20             	add    $0x20,%eax
  801f09:	39 c2                	cmp    %eax,%edx
  801f0b:	73 e2                	jae    801eef <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801f0d:	8b 45 0c             	mov    0xc(%ebp),%eax
  801f10:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801f13:	89 d0                	mov    %edx,%eax
  801f15:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801f1a:	79 05                	jns    801f21 <devpipe_write+0x50>
  801f1c:	48                   	dec    %eax
  801f1d:	83 c8 e0             	or     $0xffffffe0,%eax
  801f20:	40                   	inc    %eax
  801f21:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801f25:	42                   	inc    %edx
  801f26:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801f29:	47                   	inc    %edi
  801f2a:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801f2d:	75 d2                	jne    801f01 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801f2f:	8b 45 10             	mov    0x10(%ebp),%eax
  801f32:	eb 05                	jmp    801f39 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801f34:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801f39:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f3c:	5b                   	pop    %ebx
  801f3d:	5e                   	pop    %esi
  801f3e:	5f                   	pop    %edi
  801f3f:	5d                   	pop    %ebp
  801f40:	c3                   	ret    

00801f41 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801f41:	55                   	push   %ebp
  801f42:	89 e5                	mov    %esp,%ebp
  801f44:	57                   	push   %edi
  801f45:	56                   	push   %esi
  801f46:	53                   	push   %ebx
  801f47:	83 ec 18             	sub    $0x18,%esp
  801f4a:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801f4d:	57                   	push   %edi
  801f4e:	e8 e2 f0 ff ff       	call   801035 <fd2data>
  801f53:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801f55:	83 c4 10             	add    $0x10,%esp
  801f58:	bb 00 00 00 00       	mov    $0x0,%ebx
  801f5d:	eb 3a                	jmp    801f99 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801f5f:	85 db                	test   %ebx,%ebx
  801f61:	74 04                	je     801f67 <devpipe_read+0x26>
				return i;
  801f63:	89 d8                	mov    %ebx,%eax
  801f65:	eb 41                	jmp    801fa8 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801f67:	89 f2                	mov    %esi,%edx
  801f69:	89 f8                	mov    %edi,%eax
  801f6b:	e8 f5 fe ff ff       	call   801e65 <_pipeisclosed>
  801f70:	85 c0                	test   %eax,%eax
  801f72:	75 2f                	jne    801fa3 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801f74:	e8 7d ee ff ff       	call   800df6 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801f79:	8b 06                	mov    (%esi),%eax
  801f7b:	3b 46 04             	cmp    0x4(%esi),%eax
  801f7e:	74 df                	je     801f5f <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801f80:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801f85:	79 05                	jns    801f8c <devpipe_read+0x4b>
  801f87:	48                   	dec    %eax
  801f88:	83 c8 e0             	or     $0xffffffe0,%eax
  801f8b:	40                   	inc    %eax
  801f8c:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801f90:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801f93:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801f96:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801f98:	43                   	inc    %ebx
  801f99:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801f9c:	75 db                	jne    801f79 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801f9e:	8b 45 10             	mov    0x10(%ebp),%eax
  801fa1:	eb 05                	jmp    801fa8 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801fa3:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801fa8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801fab:	5b                   	pop    %ebx
  801fac:	5e                   	pop    %esi
  801fad:	5f                   	pop    %edi
  801fae:	5d                   	pop    %ebp
  801faf:	c3                   	ret    

00801fb0 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801fb0:	55                   	push   %ebp
  801fb1:	89 e5                	mov    %esp,%ebp
  801fb3:	56                   	push   %esi
  801fb4:	53                   	push   %ebx
  801fb5:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801fb8:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801fbb:	50                   	push   %eax
  801fbc:	e8 8b f0 ff ff       	call   80104c <fd_alloc>
  801fc1:	83 c4 10             	add    $0x10,%esp
  801fc4:	85 c0                	test   %eax,%eax
  801fc6:	0f 88 2a 01 00 00    	js     8020f6 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801fcc:	83 ec 04             	sub    $0x4,%esp
  801fcf:	68 07 04 00 00       	push   $0x407
  801fd4:	ff 75 f4             	pushl  -0xc(%ebp)
  801fd7:	6a 00                	push   $0x0
  801fd9:	e8 37 ee ff ff       	call   800e15 <sys_page_alloc>
  801fde:	83 c4 10             	add    $0x10,%esp
  801fe1:	85 c0                	test   %eax,%eax
  801fe3:	0f 88 0d 01 00 00    	js     8020f6 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801fe9:	83 ec 0c             	sub    $0xc,%esp
  801fec:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801fef:	50                   	push   %eax
  801ff0:	e8 57 f0 ff ff       	call   80104c <fd_alloc>
  801ff5:	89 c3                	mov    %eax,%ebx
  801ff7:	83 c4 10             	add    $0x10,%esp
  801ffa:	85 c0                	test   %eax,%eax
  801ffc:	0f 88 e2 00 00 00    	js     8020e4 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  802002:	83 ec 04             	sub    $0x4,%esp
  802005:	68 07 04 00 00       	push   $0x407
  80200a:	ff 75 f0             	pushl  -0x10(%ebp)
  80200d:	6a 00                	push   $0x0
  80200f:	e8 01 ee ff ff       	call   800e15 <sys_page_alloc>
  802014:	89 c3                	mov    %eax,%ebx
  802016:	83 c4 10             	add    $0x10,%esp
  802019:	85 c0                	test   %eax,%eax
  80201b:	0f 88 c3 00 00 00    	js     8020e4 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  802021:	83 ec 0c             	sub    $0xc,%esp
  802024:	ff 75 f4             	pushl  -0xc(%ebp)
  802027:	e8 09 f0 ff ff       	call   801035 <fd2data>
  80202c:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80202e:	83 c4 0c             	add    $0xc,%esp
  802031:	68 07 04 00 00       	push   $0x407
  802036:	50                   	push   %eax
  802037:	6a 00                	push   $0x0
  802039:	e8 d7 ed ff ff       	call   800e15 <sys_page_alloc>
  80203e:	89 c3                	mov    %eax,%ebx
  802040:	83 c4 10             	add    $0x10,%esp
  802043:	85 c0                	test   %eax,%eax
  802045:	0f 88 89 00 00 00    	js     8020d4 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80204b:	83 ec 0c             	sub    $0xc,%esp
  80204e:	ff 75 f0             	pushl  -0x10(%ebp)
  802051:	e8 df ef ff ff       	call   801035 <fd2data>
  802056:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  80205d:	50                   	push   %eax
  80205e:	6a 00                	push   $0x0
  802060:	56                   	push   %esi
  802061:	6a 00                	push   $0x0
  802063:	e8 f0 ed ff ff       	call   800e58 <sys_page_map>
  802068:	89 c3                	mov    %eax,%ebx
  80206a:	83 c4 20             	add    $0x20,%esp
  80206d:	85 c0                	test   %eax,%eax
  80206f:	78 55                	js     8020c6 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  802071:	8b 15 ac 47 80 00    	mov    0x8047ac,%edx
  802077:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80207a:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  80207c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80207f:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  802086:	8b 15 ac 47 80 00    	mov    0x8047ac,%edx
  80208c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80208f:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  802091:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802094:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  80209b:	83 ec 0c             	sub    $0xc,%esp
  80209e:	ff 75 f4             	pushl  -0xc(%ebp)
  8020a1:	e8 7f ef ff ff       	call   801025 <fd2num>
  8020a6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8020a9:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  8020ab:	83 c4 04             	add    $0x4,%esp
  8020ae:	ff 75 f0             	pushl  -0x10(%ebp)
  8020b1:	e8 6f ef ff ff       	call   801025 <fd2num>
  8020b6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8020b9:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  8020bc:	83 c4 10             	add    $0x10,%esp
  8020bf:	b8 00 00 00 00       	mov    $0x0,%eax
  8020c4:	eb 30                	jmp    8020f6 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  8020c6:	83 ec 08             	sub    $0x8,%esp
  8020c9:	56                   	push   %esi
  8020ca:	6a 00                	push   $0x0
  8020cc:	e8 c9 ed ff ff       	call   800e9a <sys_page_unmap>
  8020d1:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  8020d4:	83 ec 08             	sub    $0x8,%esp
  8020d7:	ff 75 f0             	pushl  -0x10(%ebp)
  8020da:	6a 00                	push   $0x0
  8020dc:	e8 b9 ed ff ff       	call   800e9a <sys_page_unmap>
  8020e1:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  8020e4:	83 ec 08             	sub    $0x8,%esp
  8020e7:	ff 75 f4             	pushl  -0xc(%ebp)
  8020ea:	6a 00                	push   $0x0
  8020ec:	e8 a9 ed ff ff       	call   800e9a <sys_page_unmap>
  8020f1:	83 c4 10             	add    $0x10,%esp
  8020f4:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  8020f6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8020f9:	5b                   	pop    %ebx
  8020fa:	5e                   	pop    %esi
  8020fb:	5d                   	pop    %ebp
  8020fc:	c3                   	ret    

008020fd <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  8020fd:	55                   	push   %ebp
  8020fe:	89 e5                	mov    %esp,%ebp
  802100:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  802103:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802106:	50                   	push   %eax
  802107:	ff 75 08             	pushl  0x8(%ebp)
  80210a:	e8 8c ef ff ff       	call   80109b <fd_lookup>
  80210f:	83 c4 10             	add    $0x10,%esp
  802112:	85 c0                	test   %eax,%eax
  802114:	78 18                	js     80212e <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  802116:	83 ec 0c             	sub    $0xc,%esp
  802119:	ff 75 f4             	pushl  -0xc(%ebp)
  80211c:	e8 14 ef ff ff       	call   801035 <fd2data>
	return _pipeisclosed(fd, p);
  802121:	89 c2                	mov    %eax,%edx
  802123:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802126:	e8 3a fd ff ff       	call   801e65 <_pipeisclosed>
  80212b:	83 c4 10             	add    $0x10,%esp
}
  80212e:	c9                   	leave  
  80212f:	c3                   	ret    

00802130 <wait>:
#include <inc/lib.h>

// Waits until 'envid' exits.
void
wait(envid_t envid)
{
  802130:	55                   	push   %ebp
  802131:	89 e5                	mov    %esp,%ebp
  802133:	56                   	push   %esi
  802134:	53                   	push   %ebx
  802135:	8b 75 08             	mov    0x8(%ebp),%esi
	const volatile struct Env *e;

	assert(envid != 0);
  802138:	85 f6                	test   %esi,%esi
  80213a:	75 16                	jne    802152 <wait+0x22>
  80213c:	68 7f 2b 80 00       	push   $0x802b7f
  802141:	68 7f 2a 80 00       	push   $0x802a7f
  802146:	6a 09                	push   $0x9
  802148:	68 8a 2b 80 00       	push   $0x802b8a
  80214d:	e8 a3 e2 ff ff       	call   8003f5 <_panic>
	e = &envs[ENVX(envid)];
  802152:	89 f3                	mov    %esi,%ebx
  802154:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
	while (e->env_id == envid && e->env_status != ENV_FREE)
  80215a:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  802161:	c1 e3 07             	shl    $0x7,%ebx
  802164:	29 c3                	sub    %eax,%ebx
  802166:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  80216c:	eb 05                	jmp    802173 <wait+0x43>
		sys_yield();
  80216e:	e8 83 ec ff ff       	call   800df6 <sys_yield>
{
	const volatile struct Env *e;

	assert(envid != 0);
	e = &envs[ENVX(envid)];
	while (e->env_id == envid && e->env_status != ENV_FREE)
  802173:	8b 43 48             	mov    0x48(%ebx),%eax
  802176:	39 c6                	cmp    %eax,%esi
  802178:	75 07                	jne    802181 <wait+0x51>
  80217a:	8b 43 54             	mov    0x54(%ebx),%eax
  80217d:	85 c0                	test   %eax,%eax
  80217f:	75 ed                	jne    80216e <wait+0x3e>
		sys_yield();
}
  802181:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802184:	5b                   	pop    %ebx
  802185:	5e                   	pop    %esi
  802186:	5d                   	pop    %ebp
  802187:	c3                   	ret    

00802188 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  802188:	55                   	push   %ebp
  802189:	89 e5                	mov    %esp,%ebp
  80218b:	56                   	push   %esi
  80218c:	53                   	push   %ebx
  80218d:	8b 75 08             	mov    0x8(%ebp),%esi
  802190:	8b 45 0c             	mov    0xc(%ebp),%eax
  802193:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  802196:	85 c0                	test   %eax,%eax
  802198:	75 05                	jne    80219f <ipc_recv+0x17>
        pg = (void *)UTOP;
  80219a:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  80219f:	83 ec 0c             	sub    $0xc,%esp
  8021a2:	50                   	push   %eax
  8021a3:	e8 3c ee ff ff       	call   800fe4 <sys_ipc_recv>
  8021a8:	83 c4 10             	add    $0x10,%esp
  8021ab:	85 c0                	test   %eax,%eax
  8021ad:	74 16                	je     8021c5 <ipc_recv+0x3d>
        if (from_env_store)
  8021af:	85 f6                	test   %esi,%esi
  8021b1:	74 06                	je     8021b9 <ipc_recv+0x31>
            *from_env_store = 0;
  8021b3:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  8021b9:	85 db                	test   %ebx,%ebx
  8021bb:	74 2c                	je     8021e9 <ipc_recv+0x61>
            *perm_store = 0;
  8021bd:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8021c3:	eb 24                	jmp    8021e9 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  8021c5:	85 f6                	test   %esi,%esi
  8021c7:	74 0a                	je     8021d3 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  8021c9:	a1 90 67 80 00       	mov    0x806790,%eax
  8021ce:	8b 40 74             	mov    0x74(%eax),%eax
  8021d1:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  8021d3:	85 db                	test   %ebx,%ebx
  8021d5:	74 0a                	je     8021e1 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  8021d7:	a1 90 67 80 00       	mov    0x806790,%eax
  8021dc:	8b 40 78             	mov    0x78(%eax),%eax
  8021df:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  8021e1:	a1 90 67 80 00       	mov    0x806790,%eax
  8021e6:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  8021e9:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8021ec:	5b                   	pop    %ebx
  8021ed:	5e                   	pop    %esi
  8021ee:	5d                   	pop    %ebp
  8021ef:	c3                   	ret    

008021f0 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  8021f0:	55                   	push   %ebp
  8021f1:	89 e5                	mov    %esp,%ebp
  8021f3:	57                   	push   %edi
  8021f4:	56                   	push   %esi
  8021f5:	53                   	push   %ebx
  8021f6:	83 ec 0c             	sub    $0xc,%esp
  8021f9:	8b 75 0c             	mov    0xc(%ebp),%esi
  8021fc:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8021ff:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  802202:	85 db                	test   %ebx,%ebx
  802204:	75 0c                	jne    802212 <ipc_send+0x22>
        pg = (void *)UTOP;
  802206:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  80220b:	eb 05                	jmp    802212 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  80220d:	e8 e4 eb ff ff       	call   800df6 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  802212:	57                   	push   %edi
  802213:	53                   	push   %ebx
  802214:	56                   	push   %esi
  802215:	ff 75 08             	pushl  0x8(%ebp)
  802218:	e8 a4 ed ff ff       	call   800fc1 <sys_ipc_try_send>
  80221d:	83 c4 10             	add    $0x10,%esp
  802220:	83 f8 f9             	cmp    $0xfffffff9,%eax
  802223:	74 e8                	je     80220d <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  802225:	85 c0                	test   %eax,%eax
  802227:	74 14                	je     80223d <ipc_send+0x4d>
        panic("ipc_send: error");
  802229:	83 ec 04             	sub    $0x4,%esp
  80222c:	68 95 2b 80 00       	push   $0x802b95
  802231:	6a 6a                	push   $0x6a
  802233:	68 a5 2b 80 00       	push   $0x802ba5
  802238:	e8 b8 e1 ff ff       	call   8003f5 <_panic>
    }
	//panic("ipc_send not implemented");
}
  80223d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802240:	5b                   	pop    %ebx
  802241:	5e                   	pop    %esi
  802242:	5f                   	pop    %edi
  802243:	5d                   	pop    %ebp
  802244:	c3                   	ret    

00802245 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  802245:	55                   	push   %ebp
  802246:	89 e5                	mov    %esp,%ebp
  802248:	53                   	push   %ebx
  802249:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  80224c:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  802251:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  802258:	89 d0                	mov    %edx,%eax
  80225a:	c1 e0 07             	shl    $0x7,%eax
  80225d:	29 d8                	sub    %ebx,%eax
  80225f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  802264:	8b 40 50             	mov    0x50(%eax),%eax
  802267:	39 c8                	cmp    %ecx,%eax
  802269:	75 0d                	jne    802278 <ipc_find_env+0x33>
			return envs[i].env_id;
  80226b:	c1 e2 07             	shl    $0x7,%edx
  80226e:	29 da                	sub    %ebx,%edx
  802270:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  802276:	eb 0e                	jmp    802286 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  802278:	42                   	inc    %edx
  802279:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  80227f:	75 d0                	jne    802251 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  802281:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802286:	5b                   	pop    %ebx
  802287:	5d                   	pop    %ebp
  802288:	c3                   	ret    

00802289 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802289:	55                   	push   %ebp
  80228a:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  80228c:	8b 45 08             	mov    0x8(%ebp),%eax
  80228f:	c1 e8 16             	shr    $0x16,%eax
  802292:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  802299:	a8 01                	test   $0x1,%al
  80229b:	74 21                	je     8022be <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  80229d:	8b 45 08             	mov    0x8(%ebp),%eax
  8022a0:	c1 e8 0c             	shr    $0xc,%eax
  8022a3:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  8022aa:	a8 01                	test   $0x1,%al
  8022ac:	74 17                	je     8022c5 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  8022ae:	c1 e8 0c             	shr    $0xc,%eax
  8022b1:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  8022b8:	ef 
  8022b9:	0f b7 c0             	movzwl %ax,%eax
  8022bc:	eb 0c                	jmp    8022ca <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  8022be:	b8 00 00 00 00       	mov    $0x0,%eax
  8022c3:	eb 05                	jmp    8022ca <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  8022c5:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  8022ca:	5d                   	pop    %ebp
  8022cb:	c3                   	ret    

008022cc <__udivdi3>:
  8022cc:	55                   	push   %ebp
  8022cd:	57                   	push   %edi
  8022ce:	56                   	push   %esi
  8022cf:	53                   	push   %ebx
  8022d0:	83 ec 1c             	sub    $0x1c,%esp
  8022d3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8022d7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8022db:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8022df:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8022e3:	89 ca                	mov    %ecx,%edx
  8022e5:	89 f8                	mov    %edi,%eax
  8022e7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8022eb:	85 f6                	test   %esi,%esi
  8022ed:	75 2d                	jne    80231c <__udivdi3+0x50>
  8022ef:	39 cf                	cmp    %ecx,%edi
  8022f1:	77 65                	ja     802358 <__udivdi3+0x8c>
  8022f3:	89 fd                	mov    %edi,%ebp
  8022f5:	85 ff                	test   %edi,%edi
  8022f7:	75 0b                	jne    802304 <__udivdi3+0x38>
  8022f9:	b8 01 00 00 00       	mov    $0x1,%eax
  8022fe:	31 d2                	xor    %edx,%edx
  802300:	f7 f7                	div    %edi
  802302:	89 c5                	mov    %eax,%ebp
  802304:	31 d2                	xor    %edx,%edx
  802306:	89 c8                	mov    %ecx,%eax
  802308:	f7 f5                	div    %ebp
  80230a:	89 c1                	mov    %eax,%ecx
  80230c:	89 d8                	mov    %ebx,%eax
  80230e:	f7 f5                	div    %ebp
  802310:	89 cf                	mov    %ecx,%edi
  802312:	89 fa                	mov    %edi,%edx
  802314:	83 c4 1c             	add    $0x1c,%esp
  802317:	5b                   	pop    %ebx
  802318:	5e                   	pop    %esi
  802319:	5f                   	pop    %edi
  80231a:	5d                   	pop    %ebp
  80231b:	c3                   	ret    
  80231c:	39 ce                	cmp    %ecx,%esi
  80231e:	77 28                	ja     802348 <__udivdi3+0x7c>
  802320:	0f bd fe             	bsr    %esi,%edi
  802323:	83 f7 1f             	xor    $0x1f,%edi
  802326:	75 40                	jne    802368 <__udivdi3+0x9c>
  802328:	39 ce                	cmp    %ecx,%esi
  80232a:	72 0a                	jb     802336 <__udivdi3+0x6a>
  80232c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  802330:	0f 87 9e 00 00 00    	ja     8023d4 <__udivdi3+0x108>
  802336:	b8 01 00 00 00       	mov    $0x1,%eax
  80233b:	89 fa                	mov    %edi,%edx
  80233d:	83 c4 1c             	add    $0x1c,%esp
  802340:	5b                   	pop    %ebx
  802341:	5e                   	pop    %esi
  802342:	5f                   	pop    %edi
  802343:	5d                   	pop    %ebp
  802344:	c3                   	ret    
  802345:	8d 76 00             	lea    0x0(%esi),%esi
  802348:	31 ff                	xor    %edi,%edi
  80234a:	31 c0                	xor    %eax,%eax
  80234c:	89 fa                	mov    %edi,%edx
  80234e:	83 c4 1c             	add    $0x1c,%esp
  802351:	5b                   	pop    %ebx
  802352:	5e                   	pop    %esi
  802353:	5f                   	pop    %edi
  802354:	5d                   	pop    %ebp
  802355:	c3                   	ret    
  802356:	66 90                	xchg   %ax,%ax
  802358:	89 d8                	mov    %ebx,%eax
  80235a:	f7 f7                	div    %edi
  80235c:	31 ff                	xor    %edi,%edi
  80235e:	89 fa                	mov    %edi,%edx
  802360:	83 c4 1c             	add    $0x1c,%esp
  802363:	5b                   	pop    %ebx
  802364:	5e                   	pop    %esi
  802365:	5f                   	pop    %edi
  802366:	5d                   	pop    %ebp
  802367:	c3                   	ret    
  802368:	bd 20 00 00 00       	mov    $0x20,%ebp
  80236d:	89 eb                	mov    %ebp,%ebx
  80236f:	29 fb                	sub    %edi,%ebx
  802371:	89 f9                	mov    %edi,%ecx
  802373:	d3 e6                	shl    %cl,%esi
  802375:	89 c5                	mov    %eax,%ebp
  802377:	88 d9                	mov    %bl,%cl
  802379:	d3 ed                	shr    %cl,%ebp
  80237b:	89 e9                	mov    %ebp,%ecx
  80237d:	09 f1                	or     %esi,%ecx
  80237f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  802383:	89 f9                	mov    %edi,%ecx
  802385:	d3 e0                	shl    %cl,%eax
  802387:	89 c5                	mov    %eax,%ebp
  802389:	89 d6                	mov    %edx,%esi
  80238b:	88 d9                	mov    %bl,%cl
  80238d:	d3 ee                	shr    %cl,%esi
  80238f:	89 f9                	mov    %edi,%ecx
  802391:	d3 e2                	shl    %cl,%edx
  802393:	8b 44 24 08          	mov    0x8(%esp),%eax
  802397:	88 d9                	mov    %bl,%cl
  802399:	d3 e8                	shr    %cl,%eax
  80239b:	09 c2                	or     %eax,%edx
  80239d:	89 d0                	mov    %edx,%eax
  80239f:	89 f2                	mov    %esi,%edx
  8023a1:	f7 74 24 0c          	divl   0xc(%esp)
  8023a5:	89 d6                	mov    %edx,%esi
  8023a7:	89 c3                	mov    %eax,%ebx
  8023a9:	f7 e5                	mul    %ebp
  8023ab:	39 d6                	cmp    %edx,%esi
  8023ad:	72 19                	jb     8023c8 <__udivdi3+0xfc>
  8023af:	74 0b                	je     8023bc <__udivdi3+0xf0>
  8023b1:	89 d8                	mov    %ebx,%eax
  8023b3:	31 ff                	xor    %edi,%edi
  8023b5:	e9 58 ff ff ff       	jmp    802312 <__udivdi3+0x46>
  8023ba:	66 90                	xchg   %ax,%ax
  8023bc:	8b 54 24 08          	mov    0x8(%esp),%edx
  8023c0:	89 f9                	mov    %edi,%ecx
  8023c2:	d3 e2                	shl    %cl,%edx
  8023c4:	39 c2                	cmp    %eax,%edx
  8023c6:	73 e9                	jae    8023b1 <__udivdi3+0xe5>
  8023c8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8023cb:	31 ff                	xor    %edi,%edi
  8023cd:	e9 40 ff ff ff       	jmp    802312 <__udivdi3+0x46>
  8023d2:	66 90                	xchg   %ax,%ax
  8023d4:	31 c0                	xor    %eax,%eax
  8023d6:	e9 37 ff ff ff       	jmp    802312 <__udivdi3+0x46>
  8023db:	90                   	nop

008023dc <__umoddi3>:
  8023dc:	55                   	push   %ebp
  8023dd:	57                   	push   %edi
  8023de:	56                   	push   %esi
  8023df:	53                   	push   %ebx
  8023e0:	83 ec 1c             	sub    $0x1c,%esp
  8023e3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8023e7:	8b 74 24 34          	mov    0x34(%esp),%esi
  8023eb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8023ef:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8023f3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8023f7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8023fb:	89 f3                	mov    %esi,%ebx
  8023fd:	89 fa                	mov    %edi,%edx
  8023ff:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  802403:	89 34 24             	mov    %esi,(%esp)
  802406:	85 c0                	test   %eax,%eax
  802408:	75 1a                	jne    802424 <__umoddi3+0x48>
  80240a:	39 f7                	cmp    %esi,%edi
  80240c:	0f 86 a2 00 00 00    	jbe    8024b4 <__umoddi3+0xd8>
  802412:	89 c8                	mov    %ecx,%eax
  802414:	89 f2                	mov    %esi,%edx
  802416:	f7 f7                	div    %edi
  802418:	89 d0                	mov    %edx,%eax
  80241a:	31 d2                	xor    %edx,%edx
  80241c:	83 c4 1c             	add    $0x1c,%esp
  80241f:	5b                   	pop    %ebx
  802420:	5e                   	pop    %esi
  802421:	5f                   	pop    %edi
  802422:	5d                   	pop    %ebp
  802423:	c3                   	ret    
  802424:	39 f0                	cmp    %esi,%eax
  802426:	0f 87 ac 00 00 00    	ja     8024d8 <__umoddi3+0xfc>
  80242c:	0f bd e8             	bsr    %eax,%ebp
  80242f:	83 f5 1f             	xor    $0x1f,%ebp
  802432:	0f 84 ac 00 00 00    	je     8024e4 <__umoddi3+0x108>
  802438:	bf 20 00 00 00       	mov    $0x20,%edi
  80243d:	29 ef                	sub    %ebp,%edi
  80243f:	89 fe                	mov    %edi,%esi
  802441:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802445:	89 e9                	mov    %ebp,%ecx
  802447:	d3 e0                	shl    %cl,%eax
  802449:	89 d7                	mov    %edx,%edi
  80244b:	89 f1                	mov    %esi,%ecx
  80244d:	d3 ef                	shr    %cl,%edi
  80244f:	09 c7                	or     %eax,%edi
  802451:	89 e9                	mov    %ebp,%ecx
  802453:	d3 e2                	shl    %cl,%edx
  802455:	89 14 24             	mov    %edx,(%esp)
  802458:	89 d8                	mov    %ebx,%eax
  80245a:	d3 e0                	shl    %cl,%eax
  80245c:	89 c2                	mov    %eax,%edx
  80245e:	8b 44 24 08          	mov    0x8(%esp),%eax
  802462:	d3 e0                	shl    %cl,%eax
  802464:	89 44 24 04          	mov    %eax,0x4(%esp)
  802468:	8b 44 24 08          	mov    0x8(%esp),%eax
  80246c:	89 f1                	mov    %esi,%ecx
  80246e:	d3 e8                	shr    %cl,%eax
  802470:	09 d0                	or     %edx,%eax
  802472:	d3 eb                	shr    %cl,%ebx
  802474:	89 da                	mov    %ebx,%edx
  802476:	f7 f7                	div    %edi
  802478:	89 d3                	mov    %edx,%ebx
  80247a:	f7 24 24             	mull   (%esp)
  80247d:	89 c6                	mov    %eax,%esi
  80247f:	89 d1                	mov    %edx,%ecx
  802481:	39 d3                	cmp    %edx,%ebx
  802483:	0f 82 87 00 00 00    	jb     802510 <__umoddi3+0x134>
  802489:	0f 84 91 00 00 00    	je     802520 <__umoddi3+0x144>
  80248f:	8b 54 24 04          	mov    0x4(%esp),%edx
  802493:	29 f2                	sub    %esi,%edx
  802495:	19 cb                	sbb    %ecx,%ebx
  802497:	89 d8                	mov    %ebx,%eax
  802499:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  80249d:	d3 e0                	shl    %cl,%eax
  80249f:	89 e9                	mov    %ebp,%ecx
  8024a1:	d3 ea                	shr    %cl,%edx
  8024a3:	09 d0                	or     %edx,%eax
  8024a5:	89 e9                	mov    %ebp,%ecx
  8024a7:	d3 eb                	shr    %cl,%ebx
  8024a9:	89 da                	mov    %ebx,%edx
  8024ab:	83 c4 1c             	add    $0x1c,%esp
  8024ae:	5b                   	pop    %ebx
  8024af:	5e                   	pop    %esi
  8024b0:	5f                   	pop    %edi
  8024b1:	5d                   	pop    %ebp
  8024b2:	c3                   	ret    
  8024b3:	90                   	nop
  8024b4:	89 fd                	mov    %edi,%ebp
  8024b6:	85 ff                	test   %edi,%edi
  8024b8:	75 0b                	jne    8024c5 <__umoddi3+0xe9>
  8024ba:	b8 01 00 00 00       	mov    $0x1,%eax
  8024bf:	31 d2                	xor    %edx,%edx
  8024c1:	f7 f7                	div    %edi
  8024c3:	89 c5                	mov    %eax,%ebp
  8024c5:	89 f0                	mov    %esi,%eax
  8024c7:	31 d2                	xor    %edx,%edx
  8024c9:	f7 f5                	div    %ebp
  8024cb:	89 c8                	mov    %ecx,%eax
  8024cd:	f7 f5                	div    %ebp
  8024cf:	89 d0                	mov    %edx,%eax
  8024d1:	e9 44 ff ff ff       	jmp    80241a <__umoddi3+0x3e>
  8024d6:	66 90                	xchg   %ax,%ax
  8024d8:	89 c8                	mov    %ecx,%eax
  8024da:	89 f2                	mov    %esi,%edx
  8024dc:	83 c4 1c             	add    $0x1c,%esp
  8024df:	5b                   	pop    %ebx
  8024e0:	5e                   	pop    %esi
  8024e1:	5f                   	pop    %edi
  8024e2:	5d                   	pop    %ebp
  8024e3:	c3                   	ret    
  8024e4:	3b 04 24             	cmp    (%esp),%eax
  8024e7:	72 06                	jb     8024ef <__umoddi3+0x113>
  8024e9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8024ed:	77 0f                	ja     8024fe <__umoddi3+0x122>
  8024ef:	89 f2                	mov    %esi,%edx
  8024f1:	29 f9                	sub    %edi,%ecx
  8024f3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8024f7:	89 14 24             	mov    %edx,(%esp)
  8024fa:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8024fe:	8b 44 24 04          	mov    0x4(%esp),%eax
  802502:	8b 14 24             	mov    (%esp),%edx
  802505:	83 c4 1c             	add    $0x1c,%esp
  802508:	5b                   	pop    %ebx
  802509:	5e                   	pop    %esi
  80250a:	5f                   	pop    %edi
  80250b:	5d                   	pop    %ebp
  80250c:	c3                   	ret    
  80250d:	8d 76 00             	lea    0x0(%esi),%esi
  802510:	2b 04 24             	sub    (%esp),%eax
  802513:	19 fa                	sbb    %edi,%edx
  802515:	89 d1                	mov    %edx,%ecx
  802517:	89 c6                	mov    %eax,%esi
  802519:	e9 71 ff ff ff       	jmp    80248f <__umoddi3+0xb3>
  80251e:	66 90                	xchg   %ax,%ax
  802520:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802524:	72 ea                	jb     802510 <__umoddi3+0x134>
  802526:	89 d9                	mov    %ebx,%ecx
  802528:	e9 62 ff ff ff       	jmp    80248f <__umoddi3+0xb3>
