
obj/user/badsegment.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 0d 00 00 00       	call   80003e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	// Try to load the kernel's TSS selector into the DS register.
	asm volatile("movw $0x28,%ax; movw %ax,%ds");
  800036:	66 b8 28 00          	mov    $0x28,%ax
  80003a:	8e d8                	mov    %eax,%ds
}
  80003c:	5d                   	pop    %ebp
  80003d:	c3                   	ret    

0080003e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003e:	55                   	push   %ebp
  80003f:	89 e5                	mov    %esp,%ebp
  800041:	56                   	push   %esi
  800042:	53                   	push   %ebx
  800043:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800046:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800049:	e8 cf 00 00 00       	call   80011d <sys_getenvid>
  80004e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800053:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80005a:	c1 e0 07             	shl    $0x7,%eax
  80005d:	29 d0                	sub    %edx,%eax
  80005f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800064:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800069:	85 db                	test   %ebx,%ebx
  80006b:	7e 07                	jle    800074 <libmain+0x36>
		binaryname = argv[0];
  80006d:	8b 06                	mov    (%esi),%eax
  80006f:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800074:	83 ec 08             	sub    $0x8,%esp
  800077:	56                   	push   %esi
  800078:	53                   	push   %ebx
  800079:	e8 b5 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80007e:	e8 0a 00 00 00       	call   80008d <exit>
}
  800083:	83 c4 10             	add    $0x10,%esp
  800086:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800089:	5b                   	pop    %ebx
  80008a:	5e                   	pop    %esi
  80008b:	5d                   	pop    %ebp
  80008c:	c3                   	ret    

0080008d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80008d:	55                   	push   %ebp
  80008e:	89 e5                	mov    %esp,%ebp
  800090:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800093:	6a 00                	push   $0x0
  800095:	e8 42 00 00 00       	call   8000dc <sys_env_destroy>
}
  80009a:	83 c4 10             	add    $0x10,%esp
  80009d:	c9                   	leave  
  80009e:	c3                   	ret    

0080009f <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009f:	55                   	push   %ebp
  8000a0:	89 e5                	mov    %esp,%ebp
  8000a2:	57                   	push   %edi
  8000a3:	56                   	push   %esi
  8000a4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a5:	b8 00 00 00 00       	mov    $0x0,%eax
  8000aa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000ad:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b0:	89 c3                	mov    %eax,%ebx
  8000b2:	89 c7                	mov    %eax,%edi
  8000b4:	89 c6                	mov    %eax,%esi
  8000b6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b8:	5b                   	pop    %ebx
  8000b9:	5e                   	pop    %esi
  8000ba:	5f                   	pop    %edi
  8000bb:	5d                   	pop    %ebp
  8000bc:	c3                   	ret    

008000bd <sys_cgetc>:

int
sys_cgetc(void)
{
  8000bd:	55                   	push   %ebp
  8000be:	89 e5                	mov    %esp,%ebp
  8000c0:	57                   	push   %edi
  8000c1:	56                   	push   %esi
  8000c2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000c3:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c8:	b8 01 00 00 00       	mov    $0x1,%eax
  8000cd:	89 d1                	mov    %edx,%ecx
  8000cf:	89 d3                	mov    %edx,%ebx
  8000d1:	89 d7                	mov    %edx,%edi
  8000d3:	89 d6                	mov    %edx,%esi
  8000d5:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d7:	5b                   	pop    %ebx
  8000d8:	5e                   	pop    %esi
  8000d9:	5f                   	pop    %edi
  8000da:	5d                   	pop    %ebp
  8000db:	c3                   	ret    

008000dc <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000dc:	55                   	push   %ebp
  8000dd:	89 e5                	mov    %esp,%ebp
  8000df:	57                   	push   %edi
  8000e0:	56                   	push   %esi
  8000e1:	53                   	push   %ebx
  8000e2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e5:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000ea:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ef:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f2:	89 cb                	mov    %ecx,%ebx
  8000f4:	89 cf                	mov    %ecx,%edi
  8000f6:	89 ce                	mov    %ecx,%esi
  8000f8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000fa:	85 c0                	test   %eax,%eax
  8000fc:	7e 17                	jle    800115 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000fe:	83 ec 0c             	sub    $0xc,%esp
  800101:	50                   	push   %eax
  800102:	6a 03                	push   $0x3
  800104:	68 4a 0f 80 00       	push   $0x800f4a
  800109:	6a 23                	push   $0x23
  80010b:	68 67 0f 80 00       	push   $0x800f67
  800110:	e8 56 02 00 00       	call   80036b <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800115:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800118:	5b                   	pop    %ebx
  800119:	5e                   	pop    %esi
  80011a:	5f                   	pop    %edi
  80011b:	5d                   	pop    %ebp
  80011c:	c3                   	ret    

0080011d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  80011d:	55                   	push   %ebp
  80011e:	89 e5                	mov    %esp,%ebp
  800120:	57                   	push   %edi
  800121:	56                   	push   %esi
  800122:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800123:	ba 00 00 00 00       	mov    $0x0,%edx
  800128:	b8 02 00 00 00       	mov    $0x2,%eax
  80012d:	89 d1                	mov    %edx,%ecx
  80012f:	89 d3                	mov    %edx,%ebx
  800131:	89 d7                	mov    %edx,%edi
  800133:	89 d6                	mov    %edx,%esi
  800135:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800137:	5b                   	pop    %ebx
  800138:	5e                   	pop    %esi
  800139:	5f                   	pop    %edi
  80013a:	5d                   	pop    %ebp
  80013b:	c3                   	ret    

0080013c <sys_yield>:

void
sys_yield(void)
{
  80013c:	55                   	push   %ebp
  80013d:	89 e5                	mov    %esp,%ebp
  80013f:	57                   	push   %edi
  800140:	56                   	push   %esi
  800141:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800142:	ba 00 00 00 00       	mov    $0x0,%edx
  800147:	b8 0b 00 00 00       	mov    $0xb,%eax
  80014c:	89 d1                	mov    %edx,%ecx
  80014e:	89 d3                	mov    %edx,%ebx
  800150:	89 d7                	mov    %edx,%edi
  800152:	89 d6                	mov    %edx,%esi
  800154:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800156:	5b                   	pop    %ebx
  800157:	5e                   	pop    %esi
  800158:	5f                   	pop    %edi
  800159:	5d                   	pop    %ebp
  80015a:	c3                   	ret    

0080015b <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  80015b:	55                   	push   %ebp
  80015c:	89 e5                	mov    %esp,%ebp
  80015e:	57                   	push   %edi
  80015f:	56                   	push   %esi
  800160:	53                   	push   %ebx
  800161:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800164:	be 00 00 00 00       	mov    $0x0,%esi
  800169:	b8 04 00 00 00       	mov    $0x4,%eax
  80016e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800171:	8b 55 08             	mov    0x8(%ebp),%edx
  800174:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800177:	89 f7                	mov    %esi,%edi
  800179:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80017b:	85 c0                	test   %eax,%eax
  80017d:	7e 17                	jle    800196 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80017f:	83 ec 0c             	sub    $0xc,%esp
  800182:	50                   	push   %eax
  800183:	6a 04                	push   $0x4
  800185:	68 4a 0f 80 00       	push   $0x800f4a
  80018a:	6a 23                	push   $0x23
  80018c:	68 67 0f 80 00       	push   $0x800f67
  800191:	e8 d5 01 00 00       	call   80036b <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800196:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800199:	5b                   	pop    %ebx
  80019a:	5e                   	pop    %esi
  80019b:	5f                   	pop    %edi
  80019c:	5d                   	pop    %ebp
  80019d:	c3                   	ret    

0080019e <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  80019e:	55                   	push   %ebp
  80019f:	89 e5                	mov    %esp,%ebp
  8001a1:	57                   	push   %edi
  8001a2:	56                   	push   %esi
  8001a3:	53                   	push   %ebx
  8001a4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001a7:	b8 05 00 00 00       	mov    $0x5,%eax
  8001ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001af:	8b 55 08             	mov    0x8(%ebp),%edx
  8001b2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b5:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001b8:	8b 75 18             	mov    0x18(%ebp),%esi
  8001bb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001bd:	85 c0                	test   %eax,%eax
  8001bf:	7e 17                	jle    8001d8 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001c1:	83 ec 0c             	sub    $0xc,%esp
  8001c4:	50                   	push   %eax
  8001c5:	6a 05                	push   $0x5
  8001c7:	68 4a 0f 80 00       	push   $0x800f4a
  8001cc:	6a 23                	push   $0x23
  8001ce:	68 67 0f 80 00       	push   $0x800f67
  8001d3:	e8 93 01 00 00       	call   80036b <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001d8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001db:	5b                   	pop    %ebx
  8001dc:	5e                   	pop    %esi
  8001dd:	5f                   	pop    %edi
  8001de:	5d                   	pop    %ebp
  8001df:	c3                   	ret    

008001e0 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001e0:	55                   	push   %ebp
  8001e1:	89 e5                	mov    %esp,%ebp
  8001e3:	57                   	push   %edi
  8001e4:	56                   	push   %esi
  8001e5:	53                   	push   %ebx
  8001e6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001e9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001ee:	b8 06 00 00 00       	mov    $0x6,%eax
  8001f3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001f6:	8b 55 08             	mov    0x8(%ebp),%edx
  8001f9:	89 df                	mov    %ebx,%edi
  8001fb:	89 de                	mov    %ebx,%esi
  8001fd:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001ff:	85 c0                	test   %eax,%eax
  800201:	7e 17                	jle    80021a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800203:	83 ec 0c             	sub    $0xc,%esp
  800206:	50                   	push   %eax
  800207:	6a 06                	push   $0x6
  800209:	68 4a 0f 80 00       	push   $0x800f4a
  80020e:	6a 23                	push   $0x23
  800210:	68 67 0f 80 00       	push   $0x800f67
  800215:	e8 51 01 00 00       	call   80036b <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  80021a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80021d:	5b                   	pop    %ebx
  80021e:	5e                   	pop    %esi
  80021f:	5f                   	pop    %edi
  800220:	5d                   	pop    %ebp
  800221:	c3                   	ret    

00800222 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800222:	55                   	push   %ebp
  800223:	89 e5                	mov    %esp,%ebp
  800225:	57                   	push   %edi
  800226:	56                   	push   %esi
  800227:	53                   	push   %ebx
  800228:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80022b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800230:	b8 08 00 00 00       	mov    $0x8,%eax
  800235:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800238:	8b 55 08             	mov    0x8(%ebp),%edx
  80023b:	89 df                	mov    %ebx,%edi
  80023d:	89 de                	mov    %ebx,%esi
  80023f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800241:	85 c0                	test   %eax,%eax
  800243:	7e 17                	jle    80025c <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800245:	83 ec 0c             	sub    $0xc,%esp
  800248:	50                   	push   %eax
  800249:	6a 08                	push   $0x8
  80024b:	68 4a 0f 80 00       	push   $0x800f4a
  800250:	6a 23                	push   $0x23
  800252:	68 67 0f 80 00       	push   $0x800f67
  800257:	e8 0f 01 00 00       	call   80036b <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  80025c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80025f:	5b                   	pop    %ebx
  800260:	5e                   	pop    %esi
  800261:	5f                   	pop    %edi
  800262:	5d                   	pop    %ebp
  800263:	c3                   	ret    

00800264 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800264:	55                   	push   %ebp
  800265:	89 e5                	mov    %esp,%ebp
  800267:	57                   	push   %edi
  800268:	56                   	push   %esi
  800269:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80026a:	ba 00 00 00 00       	mov    $0x0,%edx
  80026f:	b8 0c 00 00 00       	mov    $0xc,%eax
  800274:	89 d1                	mov    %edx,%ecx
  800276:	89 d3                	mov    %edx,%ebx
  800278:	89 d7                	mov    %edx,%edi
  80027a:	89 d6                	mov    %edx,%esi
  80027c:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80027e:	5b                   	pop    %ebx
  80027f:	5e                   	pop    %esi
  800280:	5f                   	pop    %edi
  800281:	5d                   	pop    %ebp
  800282:	c3                   	ret    

00800283 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800283:	55                   	push   %ebp
  800284:	89 e5                	mov    %esp,%ebp
  800286:	57                   	push   %edi
  800287:	56                   	push   %esi
  800288:	53                   	push   %ebx
  800289:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80028c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800291:	b8 09 00 00 00       	mov    $0x9,%eax
  800296:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800299:	8b 55 08             	mov    0x8(%ebp),%edx
  80029c:	89 df                	mov    %ebx,%edi
  80029e:	89 de                	mov    %ebx,%esi
  8002a0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002a2:	85 c0                	test   %eax,%eax
  8002a4:	7e 17                	jle    8002bd <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002a6:	83 ec 0c             	sub    $0xc,%esp
  8002a9:	50                   	push   %eax
  8002aa:	6a 09                	push   $0x9
  8002ac:	68 4a 0f 80 00       	push   $0x800f4a
  8002b1:	6a 23                	push   $0x23
  8002b3:	68 67 0f 80 00       	push   $0x800f67
  8002b8:	e8 ae 00 00 00       	call   80036b <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c0:	5b                   	pop    %ebx
  8002c1:	5e                   	pop    %esi
  8002c2:	5f                   	pop    %edi
  8002c3:	5d                   	pop    %ebp
  8002c4:	c3                   	ret    

008002c5 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002c5:	55                   	push   %ebp
  8002c6:	89 e5                	mov    %esp,%ebp
  8002c8:	57                   	push   %edi
  8002c9:	56                   	push   %esi
  8002ca:	53                   	push   %ebx
  8002cb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002ce:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002d3:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002d8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002db:	8b 55 08             	mov    0x8(%ebp),%edx
  8002de:	89 df                	mov    %ebx,%edi
  8002e0:	89 de                	mov    %ebx,%esi
  8002e2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002e4:	85 c0                	test   %eax,%eax
  8002e6:	7e 17                	jle    8002ff <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002e8:	83 ec 0c             	sub    $0xc,%esp
  8002eb:	50                   	push   %eax
  8002ec:	6a 0a                	push   $0xa
  8002ee:	68 4a 0f 80 00       	push   $0x800f4a
  8002f3:	6a 23                	push   $0x23
  8002f5:	68 67 0f 80 00       	push   $0x800f67
  8002fa:	e8 6c 00 00 00       	call   80036b <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800302:	5b                   	pop    %ebx
  800303:	5e                   	pop    %esi
  800304:	5f                   	pop    %edi
  800305:	5d                   	pop    %ebp
  800306:	c3                   	ret    

00800307 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800307:	55                   	push   %ebp
  800308:	89 e5                	mov    %esp,%ebp
  80030a:	57                   	push   %edi
  80030b:	56                   	push   %esi
  80030c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80030d:	be 00 00 00 00       	mov    $0x0,%esi
  800312:	b8 0d 00 00 00       	mov    $0xd,%eax
  800317:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80031a:	8b 55 08             	mov    0x8(%ebp),%edx
  80031d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800320:	8b 7d 14             	mov    0x14(%ebp),%edi
  800323:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800325:	5b                   	pop    %ebx
  800326:	5e                   	pop    %esi
  800327:	5f                   	pop    %edi
  800328:	5d                   	pop    %ebp
  800329:	c3                   	ret    

0080032a <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  80032a:	55                   	push   %ebp
  80032b:	89 e5                	mov    %esp,%ebp
  80032d:	57                   	push   %edi
  80032e:	56                   	push   %esi
  80032f:	53                   	push   %ebx
  800330:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800333:	b9 00 00 00 00       	mov    $0x0,%ecx
  800338:	b8 0e 00 00 00       	mov    $0xe,%eax
  80033d:	8b 55 08             	mov    0x8(%ebp),%edx
  800340:	89 cb                	mov    %ecx,%ebx
  800342:	89 cf                	mov    %ecx,%edi
  800344:	89 ce                	mov    %ecx,%esi
  800346:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800348:	85 c0                	test   %eax,%eax
  80034a:	7e 17                	jle    800363 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80034c:	83 ec 0c             	sub    $0xc,%esp
  80034f:	50                   	push   %eax
  800350:	6a 0e                	push   $0xe
  800352:	68 4a 0f 80 00       	push   $0x800f4a
  800357:	6a 23                	push   $0x23
  800359:	68 67 0f 80 00       	push   $0x800f67
  80035e:	e8 08 00 00 00       	call   80036b <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800363:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800366:	5b                   	pop    %ebx
  800367:	5e                   	pop    %esi
  800368:	5f                   	pop    %edi
  800369:	5d                   	pop    %ebp
  80036a:	c3                   	ret    

0080036b <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80036b:	55                   	push   %ebp
  80036c:	89 e5                	mov    %esp,%ebp
  80036e:	56                   	push   %esi
  80036f:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800370:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800373:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800379:	e8 9f fd ff ff       	call   80011d <sys_getenvid>
  80037e:	83 ec 0c             	sub    $0xc,%esp
  800381:	ff 75 0c             	pushl  0xc(%ebp)
  800384:	ff 75 08             	pushl  0x8(%ebp)
  800387:	56                   	push   %esi
  800388:	50                   	push   %eax
  800389:	68 78 0f 80 00       	push   $0x800f78
  80038e:	e8 b0 00 00 00       	call   800443 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800393:	83 c4 18             	add    $0x18,%esp
  800396:	53                   	push   %ebx
  800397:	ff 75 10             	pushl  0x10(%ebp)
  80039a:	e8 53 00 00 00       	call   8003f2 <vcprintf>
	cprintf("\n");
  80039f:	c7 04 24 9b 0f 80 00 	movl   $0x800f9b,(%esp)
  8003a6:	e8 98 00 00 00       	call   800443 <cprintf>
  8003ab:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003ae:	cc                   	int3   
  8003af:	eb fd                	jmp    8003ae <_panic+0x43>

008003b1 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003b1:	55                   	push   %ebp
  8003b2:	89 e5                	mov    %esp,%ebp
  8003b4:	53                   	push   %ebx
  8003b5:	83 ec 04             	sub    $0x4,%esp
  8003b8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003bb:	8b 13                	mov    (%ebx),%edx
  8003bd:	8d 42 01             	lea    0x1(%edx),%eax
  8003c0:	89 03                	mov    %eax,(%ebx)
  8003c2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003c5:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003c9:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003ce:	75 1a                	jne    8003ea <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003d0:	83 ec 08             	sub    $0x8,%esp
  8003d3:	68 ff 00 00 00       	push   $0xff
  8003d8:	8d 43 08             	lea    0x8(%ebx),%eax
  8003db:	50                   	push   %eax
  8003dc:	e8 be fc ff ff       	call   80009f <sys_cputs>
		b->idx = 0;
  8003e1:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003e7:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003ea:	ff 43 04             	incl   0x4(%ebx)
}
  8003ed:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003f0:	c9                   	leave  
  8003f1:	c3                   	ret    

008003f2 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003f2:	55                   	push   %ebp
  8003f3:	89 e5                	mov    %esp,%ebp
  8003f5:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003fb:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800402:	00 00 00 
	b.cnt = 0;
  800405:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80040c:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80040f:	ff 75 0c             	pushl  0xc(%ebp)
  800412:	ff 75 08             	pushl  0x8(%ebp)
  800415:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80041b:	50                   	push   %eax
  80041c:	68 b1 03 80 00       	push   $0x8003b1
  800421:	e8 51 01 00 00       	call   800577 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800426:	83 c4 08             	add    $0x8,%esp
  800429:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80042f:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800435:	50                   	push   %eax
  800436:	e8 64 fc ff ff       	call   80009f <sys_cputs>

	return b.cnt;
}
  80043b:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800441:	c9                   	leave  
  800442:	c3                   	ret    

00800443 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800443:	55                   	push   %ebp
  800444:	89 e5                	mov    %esp,%ebp
  800446:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800449:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80044c:	50                   	push   %eax
  80044d:	ff 75 08             	pushl  0x8(%ebp)
  800450:	e8 9d ff ff ff       	call   8003f2 <vcprintf>
	va_end(ap);

	return cnt;
}
  800455:	c9                   	leave  
  800456:	c3                   	ret    

00800457 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800457:	55                   	push   %ebp
  800458:	89 e5                	mov    %esp,%ebp
  80045a:	57                   	push   %edi
  80045b:	56                   	push   %esi
  80045c:	53                   	push   %ebx
  80045d:	83 ec 1c             	sub    $0x1c,%esp
  800460:	89 c7                	mov    %eax,%edi
  800462:	89 d6                	mov    %edx,%esi
  800464:	8b 45 08             	mov    0x8(%ebp),%eax
  800467:	8b 55 0c             	mov    0xc(%ebp),%edx
  80046a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80046d:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800470:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800473:	bb 00 00 00 00       	mov    $0x0,%ebx
  800478:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80047b:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80047e:	39 d3                	cmp    %edx,%ebx
  800480:	72 05                	jb     800487 <printnum+0x30>
  800482:	39 45 10             	cmp    %eax,0x10(%ebp)
  800485:	77 45                	ja     8004cc <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800487:	83 ec 0c             	sub    $0xc,%esp
  80048a:	ff 75 18             	pushl  0x18(%ebp)
  80048d:	8b 45 14             	mov    0x14(%ebp),%eax
  800490:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800493:	53                   	push   %ebx
  800494:	ff 75 10             	pushl  0x10(%ebp)
  800497:	83 ec 08             	sub    $0x8,%esp
  80049a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80049d:	ff 75 e0             	pushl  -0x20(%ebp)
  8004a0:	ff 75 dc             	pushl  -0x24(%ebp)
  8004a3:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a6:	e8 25 08 00 00       	call   800cd0 <__udivdi3>
  8004ab:	83 c4 18             	add    $0x18,%esp
  8004ae:	52                   	push   %edx
  8004af:	50                   	push   %eax
  8004b0:	89 f2                	mov    %esi,%edx
  8004b2:	89 f8                	mov    %edi,%eax
  8004b4:	e8 9e ff ff ff       	call   800457 <printnum>
  8004b9:	83 c4 20             	add    $0x20,%esp
  8004bc:	eb 16                	jmp    8004d4 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004be:	83 ec 08             	sub    $0x8,%esp
  8004c1:	56                   	push   %esi
  8004c2:	ff 75 18             	pushl  0x18(%ebp)
  8004c5:	ff d7                	call   *%edi
  8004c7:	83 c4 10             	add    $0x10,%esp
  8004ca:	eb 03                	jmp    8004cf <printnum+0x78>
  8004cc:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004cf:	4b                   	dec    %ebx
  8004d0:	85 db                	test   %ebx,%ebx
  8004d2:	7f ea                	jg     8004be <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004d4:	83 ec 08             	sub    $0x8,%esp
  8004d7:	56                   	push   %esi
  8004d8:	83 ec 04             	sub    $0x4,%esp
  8004db:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004de:	ff 75 e0             	pushl  -0x20(%ebp)
  8004e1:	ff 75 dc             	pushl  -0x24(%ebp)
  8004e4:	ff 75 d8             	pushl  -0x28(%ebp)
  8004e7:	e8 f4 08 00 00       	call   800de0 <__umoddi3>
  8004ec:	83 c4 14             	add    $0x14,%esp
  8004ef:	0f be 80 9d 0f 80 00 	movsbl 0x800f9d(%eax),%eax
  8004f6:	50                   	push   %eax
  8004f7:	ff d7                	call   *%edi
}
  8004f9:	83 c4 10             	add    $0x10,%esp
  8004fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004ff:	5b                   	pop    %ebx
  800500:	5e                   	pop    %esi
  800501:	5f                   	pop    %edi
  800502:	5d                   	pop    %ebp
  800503:	c3                   	ret    

00800504 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800504:	55                   	push   %ebp
  800505:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800507:	83 fa 01             	cmp    $0x1,%edx
  80050a:	7e 0e                	jle    80051a <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80050c:	8b 10                	mov    (%eax),%edx
  80050e:	8d 4a 08             	lea    0x8(%edx),%ecx
  800511:	89 08                	mov    %ecx,(%eax)
  800513:	8b 02                	mov    (%edx),%eax
  800515:	8b 52 04             	mov    0x4(%edx),%edx
  800518:	eb 22                	jmp    80053c <getuint+0x38>
	else if (lflag)
  80051a:	85 d2                	test   %edx,%edx
  80051c:	74 10                	je     80052e <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80051e:	8b 10                	mov    (%eax),%edx
  800520:	8d 4a 04             	lea    0x4(%edx),%ecx
  800523:	89 08                	mov    %ecx,(%eax)
  800525:	8b 02                	mov    (%edx),%eax
  800527:	ba 00 00 00 00       	mov    $0x0,%edx
  80052c:	eb 0e                	jmp    80053c <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80052e:	8b 10                	mov    (%eax),%edx
  800530:	8d 4a 04             	lea    0x4(%edx),%ecx
  800533:	89 08                	mov    %ecx,(%eax)
  800535:	8b 02                	mov    (%edx),%eax
  800537:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80053c:	5d                   	pop    %ebp
  80053d:	c3                   	ret    

0080053e <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80053e:	55                   	push   %ebp
  80053f:	89 e5                	mov    %esp,%ebp
  800541:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800544:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800547:	8b 10                	mov    (%eax),%edx
  800549:	3b 50 04             	cmp    0x4(%eax),%edx
  80054c:	73 0a                	jae    800558 <sprintputch+0x1a>
		*b->buf++ = ch;
  80054e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800551:	89 08                	mov    %ecx,(%eax)
  800553:	8b 45 08             	mov    0x8(%ebp),%eax
  800556:	88 02                	mov    %al,(%edx)
}
  800558:	5d                   	pop    %ebp
  800559:	c3                   	ret    

0080055a <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80055a:	55                   	push   %ebp
  80055b:	89 e5                	mov    %esp,%ebp
  80055d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800560:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800563:	50                   	push   %eax
  800564:	ff 75 10             	pushl  0x10(%ebp)
  800567:	ff 75 0c             	pushl  0xc(%ebp)
  80056a:	ff 75 08             	pushl  0x8(%ebp)
  80056d:	e8 05 00 00 00       	call   800577 <vprintfmt>
	va_end(ap);
}
  800572:	83 c4 10             	add    $0x10,%esp
  800575:	c9                   	leave  
  800576:	c3                   	ret    

00800577 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800577:	55                   	push   %ebp
  800578:	89 e5                	mov    %esp,%ebp
  80057a:	57                   	push   %edi
  80057b:	56                   	push   %esi
  80057c:	53                   	push   %ebx
  80057d:	83 ec 2c             	sub    $0x2c,%esp
  800580:	8b 75 08             	mov    0x8(%ebp),%esi
  800583:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800586:	8b 7d 10             	mov    0x10(%ebp),%edi
  800589:	eb 12                	jmp    80059d <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80058b:	85 c0                	test   %eax,%eax
  80058d:	0f 84 68 03 00 00    	je     8008fb <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800593:	83 ec 08             	sub    $0x8,%esp
  800596:	53                   	push   %ebx
  800597:	50                   	push   %eax
  800598:	ff d6                	call   *%esi
  80059a:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80059d:	47                   	inc    %edi
  80059e:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005a2:	83 f8 25             	cmp    $0x25,%eax
  8005a5:	75 e4                	jne    80058b <vprintfmt+0x14>
  8005a7:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005ab:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005b2:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005b9:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005c0:	ba 00 00 00 00       	mov    $0x0,%edx
  8005c5:	eb 07                	jmp    8005ce <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c7:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005ca:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ce:	8d 47 01             	lea    0x1(%edi),%eax
  8005d1:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005d4:	0f b6 0f             	movzbl (%edi),%ecx
  8005d7:	8a 07                	mov    (%edi),%al
  8005d9:	83 e8 23             	sub    $0x23,%eax
  8005dc:	3c 55                	cmp    $0x55,%al
  8005de:	0f 87 fe 02 00 00    	ja     8008e2 <vprintfmt+0x36b>
  8005e4:	0f b6 c0             	movzbl %al,%eax
  8005e7:	ff 24 85 e0 10 80 00 	jmp    *0x8010e0(,%eax,4)
  8005ee:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005f1:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005f5:	eb d7                	jmp    8005ce <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fa:	b8 00 00 00 00       	mov    $0x0,%eax
  8005ff:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800602:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800605:	01 c0                	add    %eax,%eax
  800607:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80060b:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80060e:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800611:	83 fa 09             	cmp    $0x9,%edx
  800614:	77 34                	ja     80064a <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800616:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800617:	eb e9                	jmp    800602 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800619:	8b 45 14             	mov    0x14(%ebp),%eax
  80061c:	8d 48 04             	lea    0x4(%eax),%ecx
  80061f:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800622:	8b 00                	mov    (%eax),%eax
  800624:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800627:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80062a:	eb 24                	jmp    800650 <vprintfmt+0xd9>
  80062c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800630:	79 07                	jns    800639 <vprintfmt+0xc2>
  800632:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800639:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80063c:	eb 90                	jmp    8005ce <vprintfmt+0x57>
  80063e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800641:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800648:	eb 84                	jmp    8005ce <vprintfmt+0x57>
  80064a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80064d:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800650:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800654:	0f 89 74 ff ff ff    	jns    8005ce <vprintfmt+0x57>
				width = precision, precision = -1;
  80065a:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80065d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800660:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800667:	e9 62 ff ff ff       	jmp    8005ce <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80066c:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80066d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800670:	e9 59 ff ff ff       	jmp    8005ce <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800675:	8b 45 14             	mov    0x14(%ebp),%eax
  800678:	8d 50 04             	lea    0x4(%eax),%edx
  80067b:	89 55 14             	mov    %edx,0x14(%ebp)
  80067e:	83 ec 08             	sub    $0x8,%esp
  800681:	53                   	push   %ebx
  800682:	ff 30                	pushl  (%eax)
  800684:	ff d6                	call   *%esi
			break;
  800686:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800689:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80068c:	e9 0c ff ff ff       	jmp    80059d <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800691:	8b 45 14             	mov    0x14(%ebp),%eax
  800694:	8d 50 04             	lea    0x4(%eax),%edx
  800697:	89 55 14             	mov    %edx,0x14(%ebp)
  80069a:	8b 00                	mov    (%eax),%eax
  80069c:	85 c0                	test   %eax,%eax
  80069e:	79 02                	jns    8006a2 <vprintfmt+0x12b>
  8006a0:	f7 d8                	neg    %eax
  8006a2:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006a4:	83 f8 0f             	cmp    $0xf,%eax
  8006a7:	7f 0b                	jg     8006b4 <vprintfmt+0x13d>
  8006a9:	8b 04 85 40 12 80 00 	mov    0x801240(,%eax,4),%eax
  8006b0:	85 c0                	test   %eax,%eax
  8006b2:	75 18                	jne    8006cc <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006b4:	52                   	push   %edx
  8006b5:	68 b5 0f 80 00       	push   $0x800fb5
  8006ba:	53                   	push   %ebx
  8006bb:	56                   	push   %esi
  8006bc:	e8 99 fe ff ff       	call   80055a <printfmt>
  8006c1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c4:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006c7:	e9 d1 fe ff ff       	jmp    80059d <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006cc:	50                   	push   %eax
  8006cd:	68 be 0f 80 00       	push   $0x800fbe
  8006d2:	53                   	push   %ebx
  8006d3:	56                   	push   %esi
  8006d4:	e8 81 fe ff ff       	call   80055a <printfmt>
  8006d9:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006dc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006df:	e9 b9 fe ff ff       	jmp    80059d <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006e4:	8b 45 14             	mov    0x14(%ebp),%eax
  8006e7:	8d 50 04             	lea    0x4(%eax),%edx
  8006ea:	89 55 14             	mov    %edx,0x14(%ebp)
  8006ed:	8b 38                	mov    (%eax),%edi
  8006ef:	85 ff                	test   %edi,%edi
  8006f1:	75 05                	jne    8006f8 <vprintfmt+0x181>
				p = "(null)";
  8006f3:	bf ae 0f 80 00       	mov    $0x800fae,%edi
			if (width > 0 && padc != '-')
  8006f8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006fc:	0f 8e 90 00 00 00    	jle    800792 <vprintfmt+0x21b>
  800702:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800706:	0f 84 8e 00 00 00    	je     80079a <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80070c:	83 ec 08             	sub    $0x8,%esp
  80070f:	ff 75 d0             	pushl  -0x30(%ebp)
  800712:	57                   	push   %edi
  800713:	e8 70 02 00 00       	call   800988 <strnlen>
  800718:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80071b:	29 c1                	sub    %eax,%ecx
  80071d:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800720:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800723:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800727:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80072a:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80072d:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80072f:	eb 0d                	jmp    80073e <vprintfmt+0x1c7>
					putch(padc, putdat);
  800731:	83 ec 08             	sub    $0x8,%esp
  800734:	53                   	push   %ebx
  800735:	ff 75 e4             	pushl  -0x1c(%ebp)
  800738:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80073a:	4f                   	dec    %edi
  80073b:	83 c4 10             	add    $0x10,%esp
  80073e:	85 ff                	test   %edi,%edi
  800740:	7f ef                	jg     800731 <vprintfmt+0x1ba>
  800742:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800745:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800748:	89 c8                	mov    %ecx,%eax
  80074a:	85 c9                	test   %ecx,%ecx
  80074c:	79 05                	jns    800753 <vprintfmt+0x1dc>
  80074e:	b8 00 00 00 00       	mov    $0x0,%eax
  800753:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800756:	29 c1                	sub    %eax,%ecx
  800758:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80075b:	89 75 08             	mov    %esi,0x8(%ebp)
  80075e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800761:	eb 3d                	jmp    8007a0 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800763:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800767:	74 19                	je     800782 <vprintfmt+0x20b>
  800769:	0f be c0             	movsbl %al,%eax
  80076c:	83 e8 20             	sub    $0x20,%eax
  80076f:	83 f8 5e             	cmp    $0x5e,%eax
  800772:	76 0e                	jbe    800782 <vprintfmt+0x20b>
					putch('?', putdat);
  800774:	83 ec 08             	sub    $0x8,%esp
  800777:	53                   	push   %ebx
  800778:	6a 3f                	push   $0x3f
  80077a:	ff 55 08             	call   *0x8(%ebp)
  80077d:	83 c4 10             	add    $0x10,%esp
  800780:	eb 0b                	jmp    80078d <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800782:	83 ec 08             	sub    $0x8,%esp
  800785:	53                   	push   %ebx
  800786:	52                   	push   %edx
  800787:	ff 55 08             	call   *0x8(%ebp)
  80078a:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80078d:	ff 4d e4             	decl   -0x1c(%ebp)
  800790:	eb 0e                	jmp    8007a0 <vprintfmt+0x229>
  800792:	89 75 08             	mov    %esi,0x8(%ebp)
  800795:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800798:	eb 06                	jmp    8007a0 <vprintfmt+0x229>
  80079a:	89 75 08             	mov    %esi,0x8(%ebp)
  80079d:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007a0:	47                   	inc    %edi
  8007a1:	8a 47 ff             	mov    -0x1(%edi),%al
  8007a4:	0f be d0             	movsbl %al,%edx
  8007a7:	85 d2                	test   %edx,%edx
  8007a9:	74 1d                	je     8007c8 <vprintfmt+0x251>
  8007ab:	85 f6                	test   %esi,%esi
  8007ad:	78 b4                	js     800763 <vprintfmt+0x1ec>
  8007af:	4e                   	dec    %esi
  8007b0:	79 b1                	jns    800763 <vprintfmt+0x1ec>
  8007b2:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007b8:	eb 14                	jmp    8007ce <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007ba:	83 ec 08             	sub    $0x8,%esp
  8007bd:	53                   	push   %ebx
  8007be:	6a 20                	push   $0x20
  8007c0:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007c2:	4f                   	dec    %edi
  8007c3:	83 c4 10             	add    $0x10,%esp
  8007c6:	eb 06                	jmp    8007ce <vprintfmt+0x257>
  8007c8:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007cb:	8b 75 08             	mov    0x8(%ebp),%esi
  8007ce:	85 ff                	test   %edi,%edi
  8007d0:	7f e8                	jg     8007ba <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007d2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007d5:	e9 c3 fd ff ff       	jmp    80059d <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007da:	83 fa 01             	cmp    $0x1,%edx
  8007dd:	7e 16                	jle    8007f5 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007df:	8b 45 14             	mov    0x14(%ebp),%eax
  8007e2:	8d 50 08             	lea    0x8(%eax),%edx
  8007e5:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e8:	8b 50 04             	mov    0x4(%eax),%edx
  8007eb:	8b 00                	mov    (%eax),%eax
  8007ed:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007f0:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007f3:	eb 32                	jmp    800827 <vprintfmt+0x2b0>
	else if (lflag)
  8007f5:	85 d2                	test   %edx,%edx
  8007f7:	74 18                	je     800811 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007f9:	8b 45 14             	mov    0x14(%ebp),%eax
  8007fc:	8d 50 04             	lea    0x4(%eax),%edx
  8007ff:	89 55 14             	mov    %edx,0x14(%ebp)
  800802:	8b 00                	mov    (%eax),%eax
  800804:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800807:	89 c1                	mov    %eax,%ecx
  800809:	c1 f9 1f             	sar    $0x1f,%ecx
  80080c:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80080f:	eb 16                	jmp    800827 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800811:	8b 45 14             	mov    0x14(%ebp),%eax
  800814:	8d 50 04             	lea    0x4(%eax),%edx
  800817:	89 55 14             	mov    %edx,0x14(%ebp)
  80081a:	8b 00                	mov    (%eax),%eax
  80081c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80081f:	89 c1                	mov    %eax,%ecx
  800821:	c1 f9 1f             	sar    $0x1f,%ecx
  800824:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800827:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80082a:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80082d:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800831:	79 76                	jns    8008a9 <vprintfmt+0x332>
				putch('-', putdat);
  800833:	83 ec 08             	sub    $0x8,%esp
  800836:	53                   	push   %ebx
  800837:	6a 2d                	push   $0x2d
  800839:	ff d6                	call   *%esi
				num = -(long long) num;
  80083b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80083e:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800841:	f7 d8                	neg    %eax
  800843:	83 d2 00             	adc    $0x0,%edx
  800846:	f7 da                	neg    %edx
  800848:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80084b:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800850:	eb 5c                	jmp    8008ae <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800852:	8d 45 14             	lea    0x14(%ebp),%eax
  800855:	e8 aa fc ff ff       	call   800504 <getuint>
			base = 10;
  80085a:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80085f:	eb 4d                	jmp    8008ae <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800861:	8d 45 14             	lea    0x14(%ebp),%eax
  800864:	e8 9b fc ff ff       	call   800504 <getuint>
			base = 8;
  800869:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80086e:	eb 3e                	jmp    8008ae <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800870:	83 ec 08             	sub    $0x8,%esp
  800873:	53                   	push   %ebx
  800874:	6a 30                	push   $0x30
  800876:	ff d6                	call   *%esi
			putch('x', putdat);
  800878:	83 c4 08             	add    $0x8,%esp
  80087b:	53                   	push   %ebx
  80087c:	6a 78                	push   $0x78
  80087e:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800880:	8b 45 14             	mov    0x14(%ebp),%eax
  800883:	8d 50 04             	lea    0x4(%eax),%edx
  800886:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800889:	8b 00                	mov    (%eax),%eax
  80088b:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800890:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800893:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800898:	eb 14                	jmp    8008ae <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80089a:	8d 45 14             	lea    0x14(%ebp),%eax
  80089d:	e8 62 fc ff ff       	call   800504 <getuint>
			base = 16;
  8008a2:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008a7:	eb 05                	jmp    8008ae <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008a9:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008ae:	83 ec 0c             	sub    $0xc,%esp
  8008b1:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008b5:	57                   	push   %edi
  8008b6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008b9:	51                   	push   %ecx
  8008ba:	52                   	push   %edx
  8008bb:	50                   	push   %eax
  8008bc:	89 da                	mov    %ebx,%edx
  8008be:	89 f0                	mov    %esi,%eax
  8008c0:	e8 92 fb ff ff       	call   800457 <printnum>
			break;
  8008c5:	83 c4 20             	add    $0x20,%esp
  8008c8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008cb:	e9 cd fc ff ff       	jmp    80059d <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008d0:	83 ec 08             	sub    $0x8,%esp
  8008d3:	53                   	push   %ebx
  8008d4:	51                   	push   %ecx
  8008d5:	ff d6                	call   *%esi
			break;
  8008d7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008da:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008dd:	e9 bb fc ff ff       	jmp    80059d <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008e2:	83 ec 08             	sub    $0x8,%esp
  8008e5:	53                   	push   %ebx
  8008e6:	6a 25                	push   $0x25
  8008e8:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008ea:	83 c4 10             	add    $0x10,%esp
  8008ed:	eb 01                	jmp    8008f0 <vprintfmt+0x379>
  8008ef:	4f                   	dec    %edi
  8008f0:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008f4:	75 f9                	jne    8008ef <vprintfmt+0x378>
  8008f6:	e9 a2 fc ff ff       	jmp    80059d <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008fb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008fe:	5b                   	pop    %ebx
  8008ff:	5e                   	pop    %esi
  800900:	5f                   	pop    %edi
  800901:	5d                   	pop    %ebp
  800902:	c3                   	ret    

00800903 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800903:	55                   	push   %ebp
  800904:	89 e5                	mov    %esp,%ebp
  800906:	83 ec 18             	sub    $0x18,%esp
  800909:	8b 45 08             	mov    0x8(%ebp),%eax
  80090c:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80090f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800912:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800916:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800919:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800920:	85 c0                	test   %eax,%eax
  800922:	74 26                	je     80094a <vsnprintf+0x47>
  800924:	85 d2                	test   %edx,%edx
  800926:	7e 29                	jle    800951 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800928:	ff 75 14             	pushl  0x14(%ebp)
  80092b:	ff 75 10             	pushl  0x10(%ebp)
  80092e:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800931:	50                   	push   %eax
  800932:	68 3e 05 80 00       	push   $0x80053e
  800937:	e8 3b fc ff ff       	call   800577 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80093c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80093f:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800942:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800945:	83 c4 10             	add    $0x10,%esp
  800948:	eb 0c                	jmp    800956 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80094a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80094f:	eb 05                	jmp    800956 <vsnprintf+0x53>
  800951:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800956:	c9                   	leave  
  800957:	c3                   	ret    

00800958 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800958:	55                   	push   %ebp
  800959:	89 e5                	mov    %esp,%ebp
  80095b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80095e:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800961:	50                   	push   %eax
  800962:	ff 75 10             	pushl  0x10(%ebp)
  800965:	ff 75 0c             	pushl  0xc(%ebp)
  800968:	ff 75 08             	pushl  0x8(%ebp)
  80096b:	e8 93 ff ff ff       	call   800903 <vsnprintf>
	va_end(ap);

	return rc;
}
  800970:	c9                   	leave  
  800971:	c3                   	ret    

00800972 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800972:	55                   	push   %ebp
  800973:	89 e5                	mov    %esp,%ebp
  800975:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800978:	b8 00 00 00 00       	mov    $0x0,%eax
  80097d:	eb 01                	jmp    800980 <strlen+0xe>
		n++;
  80097f:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800980:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800984:	75 f9                	jne    80097f <strlen+0xd>
		n++;
	return n;
}
  800986:	5d                   	pop    %ebp
  800987:	c3                   	ret    

00800988 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800988:	55                   	push   %ebp
  800989:	89 e5                	mov    %esp,%ebp
  80098b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80098e:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800991:	ba 00 00 00 00       	mov    $0x0,%edx
  800996:	eb 01                	jmp    800999 <strnlen+0x11>
		n++;
  800998:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800999:	39 c2                	cmp    %eax,%edx
  80099b:	74 08                	je     8009a5 <strnlen+0x1d>
  80099d:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009a1:	75 f5                	jne    800998 <strnlen+0x10>
  8009a3:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009a5:	5d                   	pop    %ebp
  8009a6:	c3                   	ret    

008009a7 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009a7:	55                   	push   %ebp
  8009a8:	89 e5                	mov    %esp,%ebp
  8009aa:	53                   	push   %ebx
  8009ab:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ae:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009b1:	89 c2                	mov    %eax,%edx
  8009b3:	42                   	inc    %edx
  8009b4:	41                   	inc    %ecx
  8009b5:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009b8:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009bb:	84 db                	test   %bl,%bl
  8009bd:	75 f4                	jne    8009b3 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009bf:	5b                   	pop    %ebx
  8009c0:	5d                   	pop    %ebp
  8009c1:	c3                   	ret    

008009c2 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009c2:	55                   	push   %ebp
  8009c3:	89 e5                	mov    %esp,%ebp
  8009c5:	53                   	push   %ebx
  8009c6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009c9:	53                   	push   %ebx
  8009ca:	e8 a3 ff ff ff       	call   800972 <strlen>
  8009cf:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009d2:	ff 75 0c             	pushl  0xc(%ebp)
  8009d5:	01 d8                	add    %ebx,%eax
  8009d7:	50                   	push   %eax
  8009d8:	e8 ca ff ff ff       	call   8009a7 <strcpy>
	return dst;
}
  8009dd:	89 d8                	mov    %ebx,%eax
  8009df:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009e2:	c9                   	leave  
  8009e3:	c3                   	ret    

008009e4 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009e4:	55                   	push   %ebp
  8009e5:	89 e5                	mov    %esp,%ebp
  8009e7:	56                   	push   %esi
  8009e8:	53                   	push   %ebx
  8009e9:	8b 75 08             	mov    0x8(%ebp),%esi
  8009ec:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009ef:	89 f3                	mov    %esi,%ebx
  8009f1:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009f4:	89 f2                	mov    %esi,%edx
  8009f6:	eb 0c                	jmp    800a04 <strncpy+0x20>
		*dst++ = *src;
  8009f8:	42                   	inc    %edx
  8009f9:	8a 01                	mov    (%ecx),%al
  8009fb:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009fe:	80 39 01             	cmpb   $0x1,(%ecx)
  800a01:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a04:	39 da                	cmp    %ebx,%edx
  800a06:	75 f0                	jne    8009f8 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a08:	89 f0                	mov    %esi,%eax
  800a0a:	5b                   	pop    %ebx
  800a0b:	5e                   	pop    %esi
  800a0c:	5d                   	pop    %ebp
  800a0d:	c3                   	ret    

00800a0e <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a0e:	55                   	push   %ebp
  800a0f:	89 e5                	mov    %esp,%ebp
  800a11:	56                   	push   %esi
  800a12:	53                   	push   %ebx
  800a13:	8b 75 08             	mov    0x8(%ebp),%esi
  800a16:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a19:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a1c:	85 c0                	test   %eax,%eax
  800a1e:	74 1e                	je     800a3e <strlcpy+0x30>
  800a20:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a24:	89 f2                	mov    %esi,%edx
  800a26:	eb 05                	jmp    800a2d <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a28:	42                   	inc    %edx
  800a29:	41                   	inc    %ecx
  800a2a:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a2d:	39 c2                	cmp    %eax,%edx
  800a2f:	74 08                	je     800a39 <strlcpy+0x2b>
  800a31:	8a 19                	mov    (%ecx),%bl
  800a33:	84 db                	test   %bl,%bl
  800a35:	75 f1                	jne    800a28 <strlcpy+0x1a>
  800a37:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a39:	c6 00 00             	movb   $0x0,(%eax)
  800a3c:	eb 02                	jmp    800a40 <strlcpy+0x32>
  800a3e:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a40:	29 f0                	sub    %esi,%eax
}
  800a42:	5b                   	pop    %ebx
  800a43:	5e                   	pop    %esi
  800a44:	5d                   	pop    %ebp
  800a45:	c3                   	ret    

00800a46 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a46:	55                   	push   %ebp
  800a47:	89 e5                	mov    %esp,%ebp
  800a49:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a4c:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a4f:	eb 02                	jmp    800a53 <strcmp+0xd>
		p++, q++;
  800a51:	41                   	inc    %ecx
  800a52:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a53:	8a 01                	mov    (%ecx),%al
  800a55:	84 c0                	test   %al,%al
  800a57:	74 04                	je     800a5d <strcmp+0x17>
  800a59:	3a 02                	cmp    (%edx),%al
  800a5b:	74 f4                	je     800a51 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a5d:	0f b6 c0             	movzbl %al,%eax
  800a60:	0f b6 12             	movzbl (%edx),%edx
  800a63:	29 d0                	sub    %edx,%eax
}
  800a65:	5d                   	pop    %ebp
  800a66:	c3                   	ret    

00800a67 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a67:	55                   	push   %ebp
  800a68:	89 e5                	mov    %esp,%ebp
  800a6a:	53                   	push   %ebx
  800a6b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a6e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a71:	89 c3                	mov    %eax,%ebx
  800a73:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a76:	eb 02                	jmp    800a7a <strncmp+0x13>
		n--, p++, q++;
  800a78:	40                   	inc    %eax
  800a79:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a7a:	39 d8                	cmp    %ebx,%eax
  800a7c:	74 14                	je     800a92 <strncmp+0x2b>
  800a7e:	8a 08                	mov    (%eax),%cl
  800a80:	84 c9                	test   %cl,%cl
  800a82:	74 04                	je     800a88 <strncmp+0x21>
  800a84:	3a 0a                	cmp    (%edx),%cl
  800a86:	74 f0                	je     800a78 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a88:	0f b6 00             	movzbl (%eax),%eax
  800a8b:	0f b6 12             	movzbl (%edx),%edx
  800a8e:	29 d0                	sub    %edx,%eax
  800a90:	eb 05                	jmp    800a97 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a92:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a97:	5b                   	pop    %ebx
  800a98:	5d                   	pop    %ebp
  800a99:	c3                   	ret    

00800a9a <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a9a:	55                   	push   %ebp
  800a9b:	89 e5                	mov    %esp,%ebp
  800a9d:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa0:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800aa3:	eb 05                	jmp    800aaa <strchr+0x10>
		if (*s == c)
  800aa5:	38 ca                	cmp    %cl,%dl
  800aa7:	74 0c                	je     800ab5 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800aa9:	40                   	inc    %eax
  800aaa:	8a 10                	mov    (%eax),%dl
  800aac:	84 d2                	test   %dl,%dl
  800aae:	75 f5                	jne    800aa5 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800ab0:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ab5:	5d                   	pop    %ebp
  800ab6:	c3                   	ret    

00800ab7 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ab7:	55                   	push   %ebp
  800ab8:	89 e5                	mov    %esp,%ebp
  800aba:	8b 45 08             	mov    0x8(%ebp),%eax
  800abd:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ac0:	eb 05                	jmp    800ac7 <strfind+0x10>
		if (*s == c)
  800ac2:	38 ca                	cmp    %cl,%dl
  800ac4:	74 07                	je     800acd <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800ac6:	40                   	inc    %eax
  800ac7:	8a 10                	mov    (%eax),%dl
  800ac9:	84 d2                	test   %dl,%dl
  800acb:	75 f5                	jne    800ac2 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800acd:	5d                   	pop    %ebp
  800ace:	c3                   	ret    

00800acf <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800acf:	55                   	push   %ebp
  800ad0:	89 e5                	mov    %esp,%ebp
  800ad2:	57                   	push   %edi
  800ad3:	56                   	push   %esi
  800ad4:	53                   	push   %ebx
  800ad5:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ad8:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800adb:	85 c9                	test   %ecx,%ecx
  800add:	74 36                	je     800b15 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800adf:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800ae5:	75 28                	jne    800b0f <memset+0x40>
  800ae7:	f6 c1 03             	test   $0x3,%cl
  800aea:	75 23                	jne    800b0f <memset+0x40>
		c &= 0xFF;
  800aec:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800af0:	89 d3                	mov    %edx,%ebx
  800af2:	c1 e3 08             	shl    $0x8,%ebx
  800af5:	89 d6                	mov    %edx,%esi
  800af7:	c1 e6 18             	shl    $0x18,%esi
  800afa:	89 d0                	mov    %edx,%eax
  800afc:	c1 e0 10             	shl    $0x10,%eax
  800aff:	09 f0                	or     %esi,%eax
  800b01:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b03:	89 d8                	mov    %ebx,%eax
  800b05:	09 d0                	or     %edx,%eax
  800b07:	c1 e9 02             	shr    $0x2,%ecx
  800b0a:	fc                   	cld    
  800b0b:	f3 ab                	rep stos %eax,%es:(%edi)
  800b0d:	eb 06                	jmp    800b15 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b0f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b12:	fc                   	cld    
  800b13:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b15:	89 f8                	mov    %edi,%eax
  800b17:	5b                   	pop    %ebx
  800b18:	5e                   	pop    %esi
  800b19:	5f                   	pop    %edi
  800b1a:	5d                   	pop    %ebp
  800b1b:	c3                   	ret    

00800b1c <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b1c:	55                   	push   %ebp
  800b1d:	89 e5                	mov    %esp,%ebp
  800b1f:	57                   	push   %edi
  800b20:	56                   	push   %esi
  800b21:	8b 45 08             	mov    0x8(%ebp),%eax
  800b24:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b27:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b2a:	39 c6                	cmp    %eax,%esi
  800b2c:	73 33                	jae    800b61 <memmove+0x45>
  800b2e:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b31:	39 d0                	cmp    %edx,%eax
  800b33:	73 2c                	jae    800b61 <memmove+0x45>
		s += n;
		d += n;
  800b35:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b38:	89 d6                	mov    %edx,%esi
  800b3a:	09 fe                	or     %edi,%esi
  800b3c:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b42:	75 13                	jne    800b57 <memmove+0x3b>
  800b44:	f6 c1 03             	test   $0x3,%cl
  800b47:	75 0e                	jne    800b57 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b49:	83 ef 04             	sub    $0x4,%edi
  800b4c:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b4f:	c1 e9 02             	shr    $0x2,%ecx
  800b52:	fd                   	std    
  800b53:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b55:	eb 07                	jmp    800b5e <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b57:	4f                   	dec    %edi
  800b58:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b5b:	fd                   	std    
  800b5c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b5e:	fc                   	cld    
  800b5f:	eb 1d                	jmp    800b7e <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b61:	89 f2                	mov    %esi,%edx
  800b63:	09 c2                	or     %eax,%edx
  800b65:	f6 c2 03             	test   $0x3,%dl
  800b68:	75 0f                	jne    800b79 <memmove+0x5d>
  800b6a:	f6 c1 03             	test   $0x3,%cl
  800b6d:	75 0a                	jne    800b79 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b6f:	c1 e9 02             	shr    $0x2,%ecx
  800b72:	89 c7                	mov    %eax,%edi
  800b74:	fc                   	cld    
  800b75:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b77:	eb 05                	jmp    800b7e <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b79:	89 c7                	mov    %eax,%edi
  800b7b:	fc                   	cld    
  800b7c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b7e:	5e                   	pop    %esi
  800b7f:	5f                   	pop    %edi
  800b80:	5d                   	pop    %ebp
  800b81:	c3                   	ret    

00800b82 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b82:	55                   	push   %ebp
  800b83:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b85:	ff 75 10             	pushl  0x10(%ebp)
  800b88:	ff 75 0c             	pushl  0xc(%ebp)
  800b8b:	ff 75 08             	pushl  0x8(%ebp)
  800b8e:	e8 89 ff ff ff       	call   800b1c <memmove>
}
  800b93:	c9                   	leave  
  800b94:	c3                   	ret    

00800b95 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b95:	55                   	push   %ebp
  800b96:	89 e5                	mov    %esp,%ebp
  800b98:	56                   	push   %esi
  800b99:	53                   	push   %ebx
  800b9a:	8b 45 08             	mov    0x8(%ebp),%eax
  800b9d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ba0:	89 c6                	mov    %eax,%esi
  800ba2:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ba5:	eb 14                	jmp    800bbb <memcmp+0x26>
		if (*s1 != *s2)
  800ba7:	8a 08                	mov    (%eax),%cl
  800ba9:	8a 1a                	mov    (%edx),%bl
  800bab:	38 d9                	cmp    %bl,%cl
  800bad:	74 0a                	je     800bb9 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800baf:	0f b6 c1             	movzbl %cl,%eax
  800bb2:	0f b6 db             	movzbl %bl,%ebx
  800bb5:	29 d8                	sub    %ebx,%eax
  800bb7:	eb 0b                	jmp    800bc4 <memcmp+0x2f>
		s1++, s2++;
  800bb9:	40                   	inc    %eax
  800bba:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bbb:	39 f0                	cmp    %esi,%eax
  800bbd:	75 e8                	jne    800ba7 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bbf:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bc4:	5b                   	pop    %ebx
  800bc5:	5e                   	pop    %esi
  800bc6:	5d                   	pop    %ebp
  800bc7:	c3                   	ret    

00800bc8 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bc8:	55                   	push   %ebp
  800bc9:	89 e5                	mov    %esp,%ebp
  800bcb:	53                   	push   %ebx
  800bcc:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bcf:	89 c1                	mov    %eax,%ecx
  800bd1:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bd4:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bd8:	eb 08                	jmp    800be2 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bda:	0f b6 10             	movzbl (%eax),%edx
  800bdd:	39 da                	cmp    %ebx,%edx
  800bdf:	74 05                	je     800be6 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800be1:	40                   	inc    %eax
  800be2:	39 c8                	cmp    %ecx,%eax
  800be4:	72 f4                	jb     800bda <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800be6:	5b                   	pop    %ebx
  800be7:	5d                   	pop    %ebp
  800be8:	c3                   	ret    

00800be9 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800be9:	55                   	push   %ebp
  800bea:	89 e5                	mov    %esp,%ebp
  800bec:	57                   	push   %edi
  800bed:	56                   	push   %esi
  800bee:	53                   	push   %ebx
  800bef:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf2:	eb 01                	jmp    800bf5 <strtol+0xc>
		s++;
  800bf4:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf5:	8a 01                	mov    (%ecx),%al
  800bf7:	3c 20                	cmp    $0x20,%al
  800bf9:	74 f9                	je     800bf4 <strtol+0xb>
  800bfb:	3c 09                	cmp    $0x9,%al
  800bfd:	74 f5                	je     800bf4 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bff:	3c 2b                	cmp    $0x2b,%al
  800c01:	75 08                	jne    800c0b <strtol+0x22>
		s++;
  800c03:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c04:	bf 00 00 00 00       	mov    $0x0,%edi
  800c09:	eb 11                	jmp    800c1c <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c0b:	3c 2d                	cmp    $0x2d,%al
  800c0d:	75 08                	jne    800c17 <strtol+0x2e>
		s++, neg = 1;
  800c0f:	41                   	inc    %ecx
  800c10:	bf 01 00 00 00       	mov    $0x1,%edi
  800c15:	eb 05                	jmp    800c1c <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c17:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c1c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c20:	0f 84 87 00 00 00    	je     800cad <strtol+0xc4>
  800c26:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c2a:	75 27                	jne    800c53 <strtol+0x6a>
  800c2c:	80 39 30             	cmpb   $0x30,(%ecx)
  800c2f:	75 22                	jne    800c53 <strtol+0x6a>
  800c31:	e9 88 00 00 00       	jmp    800cbe <strtol+0xd5>
		s += 2, base = 16;
  800c36:	83 c1 02             	add    $0x2,%ecx
  800c39:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c40:	eb 11                	jmp    800c53 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c42:	41                   	inc    %ecx
  800c43:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c4a:	eb 07                	jmp    800c53 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c4c:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c53:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c58:	8a 11                	mov    (%ecx),%dl
  800c5a:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c5d:	80 fb 09             	cmp    $0x9,%bl
  800c60:	77 08                	ja     800c6a <strtol+0x81>
			dig = *s - '0';
  800c62:	0f be d2             	movsbl %dl,%edx
  800c65:	83 ea 30             	sub    $0x30,%edx
  800c68:	eb 22                	jmp    800c8c <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c6a:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c6d:	89 f3                	mov    %esi,%ebx
  800c6f:	80 fb 19             	cmp    $0x19,%bl
  800c72:	77 08                	ja     800c7c <strtol+0x93>
			dig = *s - 'a' + 10;
  800c74:	0f be d2             	movsbl %dl,%edx
  800c77:	83 ea 57             	sub    $0x57,%edx
  800c7a:	eb 10                	jmp    800c8c <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c7c:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c7f:	89 f3                	mov    %esi,%ebx
  800c81:	80 fb 19             	cmp    $0x19,%bl
  800c84:	77 14                	ja     800c9a <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c86:	0f be d2             	movsbl %dl,%edx
  800c89:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c8c:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c8f:	7d 09                	jge    800c9a <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c91:	41                   	inc    %ecx
  800c92:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c96:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c98:	eb be                	jmp    800c58 <strtol+0x6f>

	if (endptr)
  800c9a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c9e:	74 05                	je     800ca5 <strtol+0xbc>
		*endptr = (char *) s;
  800ca0:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ca3:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ca5:	85 ff                	test   %edi,%edi
  800ca7:	74 21                	je     800cca <strtol+0xe1>
  800ca9:	f7 d8                	neg    %eax
  800cab:	eb 1d                	jmp    800cca <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800cad:	80 39 30             	cmpb   $0x30,(%ecx)
  800cb0:	75 9a                	jne    800c4c <strtol+0x63>
  800cb2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cb6:	0f 84 7a ff ff ff    	je     800c36 <strtol+0x4d>
  800cbc:	eb 84                	jmp    800c42 <strtol+0x59>
  800cbe:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cc2:	0f 84 6e ff ff ff    	je     800c36 <strtol+0x4d>
  800cc8:	eb 89                	jmp    800c53 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cca:	5b                   	pop    %ebx
  800ccb:	5e                   	pop    %esi
  800ccc:	5f                   	pop    %edi
  800ccd:	5d                   	pop    %ebp
  800cce:	c3                   	ret    
  800ccf:	90                   	nop

00800cd0 <__udivdi3>:
  800cd0:	55                   	push   %ebp
  800cd1:	57                   	push   %edi
  800cd2:	56                   	push   %esi
  800cd3:	53                   	push   %ebx
  800cd4:	83 ec 1c             	sub    $0x1c,%esp
  800cd7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cdb:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cdf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ce3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ce7:	89 ca                	mov    %ecx,%edx
  800ce9:	89 f8                	mov    %edi,%eax
  800ceb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cef:	85 f6                	test   %esi,%esi
  800cf1:	75 2d                	jne    800d20 <__udivdi3+0x50>
  800cf3:	39 cf                	cmp    %ecx,%edi
  800cf5:	77 65                	ja     800d5c <__udivdi3+0x8c>
  800cf7:	89 fd                	mov    %edi,%ebp
  800cf9:	85 ff                	test   %edi,%edi
  800cfb:	75 0b                	jne    800d08 <__udivdi3+0x38>
  800cfd:	b8 01 00 00 00       	mov    $0x1,%eax
  800d02:	31 d2                	xor    %edx,%edx
  800d04:	f7 f7                	div    %edi
  800d06:	89 c5                	mov    %eax,%ebp
  800d08:	31 d2                	xor    %edx,%edx
  800d0a:	89 c8                	mov    %ecx,%eax
  800d0c:	f7 f5                	div    %ebp
  800d0e:	89 c1                	mov    %eax,%ecx
  800d10:	89 d8                	mov    %ebx,%eax
  800d12:	f7 f5                	div    %ebp
  800d14:	89 cf                	mov    %ecx,%edi
  800d16:	89 fa                	mov    %edi,%edx
  800d18:	83 c4 1c             	add    $0x1c,%esp
  800d1b:	5b                   	pop    %ebx
  800d1c:	5e                   	pop    %esi
  800d1d:	5f                   	pop    %edi
  800d1e:	5d                   	pop    %ebp
  800d1f:	c3                   	ret    
  800d20:	39 ce                	cmp    %ecx,%esi
  800d22:	77 28                	ja     800d4c <__udivdi3+0x7c>
  800d24:	0f bd fe             	bsr    %esi,%edi
  800d27:	83 f7 1f             	xor    $0x1f,%edi
  800d2a:	75 40                	jne    800d6c <__udivdi3+0x9c>
  800d2c:	39 ce                	cmp    %ecx,%esi
  800d2e:	72 0a                	jb     800d3a <__udivdi3+0x6a>
  800d30:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d34:	0f 87 9e 00 00 00    	ja     800dd8 <__udivdi3+0x108>
  800d3a:	b8 01 00 00 00       	mov    $0x1,%eax
  800d3f:	89 fa                	mov    %edi,%edx
  800d41:	83 c4 1c             	add    $0x1c,%esp
  800d44:	5b                   	pop    %ebx
  800d45:	5e                   	pop    %esi
  800d46:	5f                   	pop    %edi
  800d47:	5d                   	pop    %ebp
  800d48:	c3                   	ret    
  800d49:	8d 76 00             	lea    0x0(%esi),%esi
  800d4c:	31 ff                	xor    %edi,%edi
  800d4e:	31 c0                	xor    %eax,%eax
  800d50:	89 fa                	mov    %edi,%edx
  800d52:	83 c4 1c             	add    $0x1c,%esp
  800d55:	5b                   	pop    %ebx
  800d56:	5e                   	pop    %esi
  800d57:	5f                   	pop    %edi
  800d58:	5d                   	pop    %ebp
  800d59:	c3                   	ret    
  800d5a:	66 90                	xchg   %ax,%ax
  800d5c:	89 d8                	mov    %ebx,%eax
  800d5e:	f7 f7                	div    %edi
  800d60:	31 ff                	xor    %edi,%edi
  800d62:	89 fa                	mov    %edi,%edx
  800d64:	83 c4 1c             	add    $0x1c,%esp
  800d67:	5b                   	pop    %ebx
  800d68:	5e                   	pop    %esi
  800d69:	5f                   	pop    %edi
  800d6a:	5d                   	pop    %ebp
  800d6b:	c3                   	ret    
  800d6c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d71:	89 eb                	mov    %ebp,%ebx
  800d73:	29 fb                	sub    %edi,%ebx
  800d75:	89 f9                	mov    %edi,%ecx
  800d77:	d3 e6                	shl    %cl,%esi
  800d79:	89 c5                	mov    %eax,%ebp
  800d7b:	88 d9                	mov    %bl,%cl
  800d7d:	d3 ed                	shr    %cl,%ebp
  800d7f:	89 e9                	mov    %ebp,%ecx
  800d81:	09 f1                	or     %esi,%ecx
  800d83:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d87:	89 f9                	mov    %edi,%ecx
  800d89:	d3 e0                	shl    %cl,%eax
  800d8b:	89 c5                	mov    %eax,%ebp
  800d8d:	89 d6                	mov    %edx,%esi
  800d8f:	88 d9                	mov    %bl,%cl
  800d91:	d3 ee                	shr    %cl,%esi
  800d93:	89 f9                	mov    %edi,%ecx
  800d95:	d3 e2                	shl    %cl,%edx
  800d97:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d9b:	88 d9                	mov    %bl,%cl
  800d9d:	d3 e8                	shr    %cl,%eax
  800d9f:	09 c2                	or     %eax,%edx
  800da1:	89 d0                	mov    %edx,%eax
  800da3:	89 f2                	mov    %esi,%edx
  800da5:	f7 74 24 0c          	divl   0xc(%esp)
  800da9:	89 d6                	mov    %edx,%esi
  800dab:	89 c3                	mov    %eax,%ebx
  800dad:	f7 e5                	mul    %ebp
  800daf:	39 d6                	cmp    %edx,%esi
  800db1:	72 19                	jb     800dcc <__udivdi3+0xfc>
  800db3:	74 0b                	je     800dc0 <__udivdi3+0xf0>
  800db5:	89 d8                	mov    %ebx,%eax
  800db7:	31 ff                	xor    %edi,%edi
  800db9:	e9 58 ff ff ff       	jmp    800d16 <__udivdi3+0x46>
  800dbe:	66 90                	xchg   %ax,%ax
  800dc0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dc4:	89 f9                	mov    %edi,%ecx
  800dc6:	d3 e2                	shl    %cl,%edx
  800dc8:	39 c2                	cmp    %eax,%edx
  800dca:	73 e9                	jae    800db5 <__udivdi3+0xe5>
  800dcc:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800dcf:	31 ff                	xor    %edi,%edi
  800dd1:	e9 40 ff ff ff       	jmp    800d16 <__udivdi3+0x46>
  800dd6:	66 90                	xchg   %ax,%ax
  800dd8:	31 c0                	xor    %eax,%eax
  800dda:	e9 37 ff ff ff       	jmp    800d16 <__udivdi3+0x46>
  800ddf:	90                   	nop

00800de0 <__umoddi3>:
  800de0:	55                   	push   %ebp
  800de1:	57                   	push   %edi
  800de2:	56                   	push   %esi
  800de3:	53                   	push   %ebx
  800de4:	83 ec 1c             	sub    $0x1c,%esp
  800de7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800deb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800def:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800df3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800df7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800dfb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dff:	89 f3                	mov    %esi,%ebx
  800e01:	89 fa                	mov    %edi,%edx
  800e03:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e07:	89 34 24             	mov    %esi,(%esp)
  800e0a:	85 c0                	test   %eax,%eax
  800e0c:	75 1a                	jne    800e28 <__umoddi3+0x48>
  800e0e:	39 f7                	cmp    %esi,%edi
  800e10:	0f 86 a2 00 00 00    	jbe    800eb8 <__umoddi3+0xd8>
  800e16:	89 c8                	mov    %ecx,%eax
  800e18:	89 f2                	mov    %esi,%edx
  800e1a:	f7 f7                	div    %edi
  800e1c:	89 d0                	mov    %edx,%eax
  800e1e:	31 d2                	xor    %edx,%edx
  800e20:	83 c4 1c             	add    $0x1c,%esp
  800e23:	5b                   	pop    %ebx
  800e24:	5e                   	pop    %esi
  800e25:	5f                   	pop    %edi
  800e26:	5d                   	pop    %ebp
  800e27:	c3                   	ret    
  800e28:	39 f0                	cmp    %esi,%eax
  800e2a:	0f 87 ac 00 00 00    	ja     800edc <__umoddi3+0xfc>
  800e30:	0f bd e8             	bsr    %eax,%ebp
  800e33:	83 f5 1f             	xor    $0x1f,%ebp
  800e36:	0f 84 ac 00 00 00    	je     800ee8 <__umoddi3+0x108>
  800e3c:	bf 20 00 00 00       	mov    $0x20,%edi
  800e41:	29 ef                	sub    %ebp,%edi
  800e43:	89 fe                	mov    %edi,%esi
  800e45:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e49:	89 e9                	mov    %ebp,%ecx
  800e4b:	d3 e0                	shl    %cl,%eax
  800e4d:	89 d7                	mov    %edx,%edi
  800e4f:	89 f1                	mov    %esi,%ecx
  800e51:	d3 ef                	shr    %cl,%edi
  800e53:	09 c7                	or     %eax,%edi
  800e55:	89 e9                	mov    %ebp,%ecx
  800e57:	d3 e2                	shl    %cl,%edx
  800e59:	89 14 24             	mov    %edx,(%esp)
  800e5c:	89 d8                	mov    %ebx,%eax
  800e5e:	d3 e0                	shl    %cl,%eax
  800e60:	89 c2                	mov    %eax,%edx
  800e62:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e66:	d3 e0                	shl    %cl,%eax
  800e68:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e6c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e70:	89 f1                	mov    %esi,%ecx
  800e72:	d3 e8                	shr    %cl,%eax
  800e74:	09 d0                	or     %edx,%eax
  800e76:	d3 eb                	shr    %cl,%ebx
  800e78:	89 da                	mov    %ebx,%edx
  800e7a:	f7 f7                	div    %edi
  800e7c:	89 d3                	mov    %edx,%ebx
  800e7e:	f7 24 24             	mull   (%esp)
  800e81:	89 c6                	mov    %eax,%esi
  800e83:	89 d1                	mov    %edx,%ecx
  800e85:	39 d3                	cmp    %edx,%ebx
  800e87:	0f 82 87 00 00 00    	jb     800f14 <__umoddi3+0x134>
  800e8d:	0f 84 91 00 00 00    	je     800f24 <__umoddi3+0x144>
  800e93:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e97:	29 f2                	sub    %esi,%edx
  800e99:	19 cb                	sbb    %ecx,%ebx
  800e9b:	89 d8                	mov    %ebx,%eax
  800e9d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ea1:	d3 e0                	shl    %cl,%eax
  800ea3:	89 e9                	mov    %ebp,%ecx
  800ea5:	d3 ea                	shr    %cl,%edx
  800ea7:	09 d0                	or     %edx,%eax
  800ea9:	89 e9                	mov    %ebp,%ecx
  800eab:	d3 eb                	shr    %cl,%ebx
  800ead:	89 da                	mov    %ebx,%edx
  800eaf:	83 c4 1c             	add    $0x1c,%esp
  800eb2:	5b                   	pop    %ebx
  800eb3:	5e                   	pop    %esi
  800eb4:	5f                   	pop    %edi
  800eb5:	5d                   	pop    %ebp
  800eb6:	c3                   	ret    
  800eb7:	90                   	nop
  800eb8:	89 fd                	mov    %edi,%ebp
  800eba:	85 ff                	test   %edi,%edi
  800ebc:	75 0b                	jne    800ec9 <__umoddi3+0xe9>
  800ebe:	b8 01 00 00 00       	mov    $0x1,%eax
  800ec3:	31 d2                	xor    %edx,%edx
  800ec5:	f7 f7                	div    %edi
  800ec7:	89 c5                	mov    %eax,%ebp
  800ec9:	89 f0                	mov    %esi,%eax
  800ecb:	31 d2                	xor    %edx,%edx
  800ecd:	f7 f5                	div    %ebp
  800ecf:	89 c8                	mov    %ecx,%eax
  800ed1:	f7 f5                	div    %ebp
  800ed3:	89 d0                	mov    %edx,%eax
  800ed5:	e9 44 ff ff ff       	jmp    800e1e <__umoddi3+0x3e>
  800eda:	66 90                	xchg   %ax,%ax
  800edc:	89 c8                	mov    %ecx,%eax
  800ede:	89 f2                	mov    %esi,%edx
  800ee0:	83 c4 1c             	add    $0x1c,%esp
  800ee3:	5b                   	pop    %ebx
  800ee4:	5e                   	pop    %esi
  800ee5:	5f                   	pop    %edi
  800ee6:	5d                   	pop    %ebp
  800ee7:	c3                   	ret    
  800ee8:	3b 04 24             	cmp    (%esp),%eax
  800eeb:	72 06                	jb     800ef3 <__umoddi3+0x113>
  800eed:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ef1:	77 0f                	ja     800f02 <__umoddi3+0x122>
  800ef3:	89 f2                	mov    %esi,%edx
  800ef5:	29 f9                	sub    %edi,%ecx
  800ef7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800efb:	89 14 24             	mov    %edx,(%esp)
  800efe:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f02:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f06:	8b 14 24             	mov    (%esp),%edx
  800f09:	83 c4 1c             	add    $0x1c,%esp
  800f0c:	5b                   	pop    %ebx
  800f0d:	5e                   	pop    %esi
  800f0e:	5f                   	pop    %edi
  800f0f:	5d                   	pop    %ebp
  800f10:	c3                   	ret    
  800f11:	8d 76 00             	lea    0x0(%esi),%esi
  800f14:	2b 04 24             	sub    (%esp),%eax
  800f17:	19 fa                	sbb    %edi,%edx
  800f19:	89 d1                	mov    %edx,%ecx
  800f1b:	89 c6                	mov    %eax,%esi
  800f1d:	e9 71 ff ff ff       	jmp    800e93 <__umoddi3+0xb3>
  800f22:	66 90                	xchg   %ax,%ax
  800f24:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f28:	72 ea                	jb     800f14 <__umoddi3+0x134>
  800f2a:	89 d9                	mov    %ebx,%ecx
  800f2c:	e9 62 ff ff ff       	jmp    800e93 <__umoddi3+0xb3>
