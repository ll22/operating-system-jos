
obj/user/faultdie.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 4f 00 00 00       	call   800080 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <handler>:

#include <inc/lib.h>

void
handler(struct UTrapframe *utf)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 0c             	sub    $0xc,%esp
  800039:	8b 55 08             	mov    0x8(%ebp),%edx
	void *addr = (void*)utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	cprintf("i faulted at va %x, err %x\n", addr, err & 7);
  80003c:	8b 42 04             	mov    0x4(%edx),%eax
  80003f:	83 e0 07             	and    $0x7,%eax
  800042:	50                   	push   %eax
  800043:	ff 32                	pushl  (%edx)
  800045:	68 00 10 80 00       	push   $0x801000
  80004a:	e8 24 01 00 00       	call   800173 <cprintf>
	sys_env_destroy(sys_getenvid());
  80004f:	e8 29 0a 00 00       	call   800a7d <sys_getenvid>
  800054:	89 04 24             	mov    %eax,(%esp)
  800057:	e8 e0 09 00 00       	call   800a3c <sys_env_destroy>
}
  80005c:	83 c4 10             	add    $0x10,%esp
  80005f:	c9                   	leave  
  800060:	c3                   	ret    

00800061 <umain>:

void
umain(int argc, char **argv)
{
  800061:	55                   	push   %ebp
  800062:	89 e5                	mov    %esp,%ebp
  800064:	83 ec 14             	sub    $0x14,%esp
	set_pgfault_handler(handler);
  800067:	68 33 00 80 00       	push   $0x800033
  80006c:	e8 5a 0c 00 00       	call   800ccb <set_pgfault_handler>
	*(int*)0xDeadBeef = 0;
  800071:	c7 05 ef be ad de 00 	movl   $0x0,0xdeadbeef
  800078:	00 00 00 
}
  80007b:	83 c4 10             	add    $0x10,%esp
  80007e:	c9                   	leave  
  80007f:	c3                   	ret    

00800080 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800080:	55                   	push   %ebp
  800081:	89 e5                	mov    %esp,%ebp
  800083:	56                   	push   %esi
  800084:	53                   	push   %ebx
  800085:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800088:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80008b:	e8 ed 09 00 00       	call   800a7d <sys_getenvid>
  800090:	25 ff 03 00 00       	and    $0x3ff,%eax
  800095:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80009c:	c1 e0 07             	shl    $0x7,%eax
  80009f:	29 d0                	sub    %edx,%eax
  8000a1:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000a6:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000ab:	85 db                	test   %ebx,%ebx
  8000ad:	7e 07                	jle    8000b6 <libmain+0x36>
		binaryname = argv[0];
  8000af:	8b 06                	mov    (%esi),%eax
  8000b1:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000b6:	83 ec 08             	sub    $0x8,%esp
  8000b9:	56                   	push   %esi
  8000ba:	53                   	push   %ebx
  8000bb:	e8 a1 ff ff ff       	call   800061 <umain>

	// exit gracefully
	exit();
  8000c0:	e8 0a 00 00 00       	call   8000cf <exit>
}
  8000c5:	83 c4 10             	add    $0x10,%esp
  8000c8:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000cb:	5b                   	pop    %ebx
  8000cc:	5e                   	pop    %esi
  8000cd:	5d                   	pop    %ebp
  8000ce:	c3                   	ret    

008000cf <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000cf:	55                   	push   %ebp
  8000d0:	89 e5                	mov    %esp,%ebp
  8000d2:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000d5:	6a 00                	push   $0x0
  8000d7:	e8 60 09 00 00       	call   800a3c <sys_env_destroy>
}
  8000dc:	83 c4 10             	add    $0x10,%esp
  8000df:	c9                   	leave  
  8000e0:	c3                   	ret    

008000e1 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000e1:	55                   	push   %ebp
  8000e2:	89 e5                	mov    %esp,%ebp
  8000e4:	53                   	push   %ebx
  8000e5:	83 ec 04             	sub    $0x4,%esp
  8000e8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000eb:	8b 13                	mov    (%ebx),%edx
  8000ed:	8d 42 01             	lea    0x1(%edx),%eax
  8000f0:	89 03                	mov    %eax,(%ebx)
  8000f2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000f5:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000f9:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000fe:	75 1a                	jne    80011a <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800100:	83 ec 08             	sub    $0x8,%esp
  800103:	68 ff 00 00 00       	push   $0xff
  800108:	8d 43 08             	lea    0x8(%ebx),%eax
  80010b:	50                   	push   %eax
  80010c:	e8 ee 08 00 00       	call   8009ff <sys_cputs>
		b->idx = 0;
  800111:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800117:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80011a:	ff 43 04             	incl   0x4(%ebx)
}
  80011d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800120:	c9                   	leave  
  800121:	c3                   	ret    

00800122 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800122:	55                   	push   %ebp
  800123:	89 e5                	mov    %esp,%ebp
  800125:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80012b:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800132:	00 00 00 
	b.cnt = 0;
  800135:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80013c:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80013f:	ff 75 0c             	pushl  0xc(%ebp)
  800142:	ff 75 08             	pushl  0x8(%ebp)
  800145:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80014b:	50                   	push   %eax
  80014c:	68 e1 00 80 00       	push   $0x8000e1
  800151:	e8 51 01 00 00       	call   8002a7 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800156:	83 c4 08             	add    $0x8,%esp
  800159:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80015f:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800165:	50                   	push   %eax
  800166:	e8 94 08 00 00       	call   8009ff <sys_cputs>

	return b.cnt;
}
  80016b:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800171:	c9                   	leave  
  800172:	c3                   	ret    

00800173 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800173:	55                   	push   %ebp
  800174:	89 e5                	mov    %esp,%ebp
  800176:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800179:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80017c:	50                   	push   %eax
  80017d:	ff 75 08             	pushl  0x8(%ebp)
  800180:	e8 9d ff ff ff       	call   800122 <vcprintf>
	va_end(ap);

	return cnt;
}
  800185:	c9                   	leave  
  800186:	c3                   	ret    

00800187 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800187:	55                   	push   %ebp
  800188:	89 e5                	mov    %esp,%ebp
  80018a:	57                   	push   %edi
  80018b:	56                   	push   %esi
  80018c:	53                   	push   %ebx
  80018d:	83 ec 1c             	sub    $0x1c,%esp
  800190:	89 c7                	mov    %eax,%edi
  800192:	89 d6                	mov    %edx,%esi
  800194:	8b 45 08             	mov    0x8(%ebp),%eax
  800197:	8b 55 0c             	mov    0xc(%ebp),%edx
  80019a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80019d:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001a0:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001a3:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001a8:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001ab:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001ae:	39 d3                	cmp    %edx,%ebx
  8001b0:	72 05                	jb     8001b7 <printnum+0x30>
  8001b2:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001b5:	77 45                	ja     8001fc <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001b7:	83 ec 0c             	sub    $0xc,%esp
  8001ba:	ff 75 18             	pushl  0x18(%ebp)
  8001bd:	8b 45 14             	mov    0x14(%ebp),%eax
  8001c0:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001c3:	53                   	push   %ebx
  8001c4:	ff 75 10             	pushl  0x10(%ebp)
  8001c7:	83 ec 08             	sub    $0x8,%esp
  8001ca:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001cd:	ff 75 e0             	pushl  -0x20(%ebp)
  8001d0:	ff 75 dc             	pushl  -0x24(%ebp)
  8001d3:	ff 75 d8             	pushl  -0x28(%ebp)
  8001d6:	e8 b5 0b 00 00       	call   800d90 <__udivdi3>
  8001db:	83 c4 18             	add    $0x18,%esp
  8001de:	52                   	push   %edx
  8001df:	50                   	push   %eax
  8001e0:	89 f2                	mov    %esi,%edx
  8001e2:	89 f8                	mov    %edi,%eax
  8001e4:	e8 9e ff ff ff       	call   800187 <printnum>
  8001e9:	83 c4 20             	add    $0x20,%esp
  8001ec:	eb 16                	jmp    800204 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001ee:	83 ec 08             	sub    $0x8,%esp
  8001f1:	56                   	push   %esi
  8001f2:	ff 75 18             	pushl  0x18(%ebp)
  8001f5:	ff d7                	call   *%edi
  8001f7:	83 c4 10             	add    $0x10,%esp
  8001fa:	eb 03                	jmp    8001ff <printnum+0x78>
  8001fc:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001ff:	4b                   	dec    %ebx
  800200:	85 db                	test   %ebx,%ebx
  800202:	7f ea                	jg     8001ee <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800204:	83 ec 08             	sub    $0x8,%esp
  800207:	56                   	push   %esi
  800208:	83 ec 04             	sub    $0x4,%esp
  80020b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80020e:	ff 75 e0             	pushl  -0x20(%ebp)
  800211:	ff 75 dc             	pushl  -0x24(%ebp)
  800214:	ff 75 d8             	pushl  -0x28(%ebp)
  800217:	e8 84 0c 00 00       	call   800ea0 <__umoddi3>
  80021c:	83 c4 14             	add    $0x14,%esp
  80021f:	0f be 80 26 10 80 00 	movsbl 0x801026(%eax),%eax
  800226:	50                   	push   %eax
  800227:	ff d7                	call   *%edi
}
  800229:	83 c4 10             	add    $0x10,%esp
  80022c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80022f:	5b                   	pop    %ebx
  800230:	5e                   	pop    %esi
  800231:	5f                   	pop    %edi
  800232:	5d                   	pop    %ebp
  800233:	c3                   	ret    

00800234 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800234:	55                   	push   %ebp
  800235:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800237:	83 fa 01             	cmp    $0x1,%edx
  80023a:	7e 0e                	jle    80024a <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80023c:	8b 10                	mov    (%eax),%edx
  80023e:	8d 4a 08             	lea    0x8(%edx),%ecx
  800241:	89 08                	mov    %ecx,(%eax)
  800243:	8b 02                	mov    (%edx),%eax
  800245:	8b 52 04             	mov    0x4(%edx),%edx
  800248:	eb 22                	jmp    80026c <getuint+0x38>
	else if (lflag)
  80024a:	85 d2                	test   %edx,%edx
  80024c:	74 10                	je     80025e <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80024e:	8b 10                	mov    (%eax),%edx
  800250:	8d 4a 04             	lea    0x4(%edx),%ecx
  800253:	89 08                	mov    %ecx,(%eax)
  800255:	8b 02                	mov    (%edx),%eax
  800257:	ba 00 00 00 00       	mov    $0x0,%edx
  80025c:	eb 0e                	jmp    80026c <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80025e:	8b 10                	mov    (%eax),%edx
  800260:	8d 4a 04             	lea    0x4(%edx),%ecx
  800263:	89 08                	mov    %ecx,(%eax)
  800265:	8b 02                	mov    (%edx),%eax
  800267:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80026c:	5d                   	pop    %ebp
  80026d:	c3                   	ret    

0080026e <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80026e:	55                   	push   %ebp
  80026f:	89 e5                	mov    %esp,%ebp
  800271:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800274:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800277:	8b 10                	mov    (%eax),%edx
  800279:	3b 50 04             	cmp    0x4(%eax),%edx
  80027c:	73 0a                	jae    800288 <sprintputch+0x1a>
		*b->buf++ = ch;
  80027e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800281:	89 08                	mov    %ecx,(%eax)
  800283:	8b 45 08             	mov    0x8(%ebp),%eax
  800286:	88 02                	mov    %al,(%edx)
}
  800288:	5d                   	pop    %ebp
  800289:	c3                   	ret    

0080028a <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80028a:	55                   	push   %ebp
  80028b:	89 e5                	mov    %esp,%ebp
  80028d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800290:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800293:	50                   	push   %eax
  800294:	ff 75 10             	pushl  0x10(%ebp)
  800297:	ff 75 0c             	pushl  0xc(%ebp)
  80029a:	ff 75 08             	pushl  0x8(%ebp)
  80029d:	e8 05 00 00 00       	call   8002a7 <vprintfmt>
	va_end(ap);
}
  8002a2:	83 c4 10             	add    $0x10,%esp
  8002a5:	c9                   	leave  
  8002a6:	c3                   	ret    

008002a7 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002a7:	55                   	push   %ebp
  8002a8:	89 e5                	mov    %esp,%ebp
  8002aa:	57                   	push   %edi
  8002ab:	56                   	push   %esi
  8002ac:	53                   	push   %ebx
  8002ad:	83 ec 2c             	sub    $0x2c,%esp
  8002b0:	8b 75 08             	mov    0x8(%ebp),%esi
  8002b3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002b6:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002b9:	eb 12                	jmp    8002cd <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002bb:	85 c0                	test   %eax,%eax
  8002bd:	0f 84 68 03 00 00    	je     80062b <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002c3:	83 ec 08             	sub    $0x8,%esp
  8002c6:	53                   	push   %ebx
  8002c7:	50                   	push   %eax
  8002c8:	ff d6                	call   *%esi
  8002ca:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002cd:	47                   	inc    %edi
  8002ce:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002d2:	83 f8 25             	cmp    $0x25,%eax
  8002d5:	75 e4                	jne    8002bb <vprintfmt+0x14>
  8002d7:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002db:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002e2:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002e9:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002f0:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f5:	eb 07                	jmp    8002fe <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f7:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002fa:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002fe:	8d 47 01             	lea    0x1(%edi),%eax
  800301:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800304:	0f b6 0f             	movzbl (%edi),%ecx
  800307:	8a 07                	mov    (%edi),%al
  800309:	83 e8 23             	sub    $0x23,%eax
  80030c:	3c 55                	cmp    $0x55,%al
  80030e:	0f 87 fe 02 00 00    	ja     800612 <vprintfmt+0x36b>
  800314:	0f b6 c0             	movzbl %al,%eax
  800317:	ff 24 85 60 11 80 00 	jmp    *0x801160(,%eax,4)
  80031e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800321:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800325:	eb d7                	jmp    8002fe <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800327:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80032a:	b8 00 00 00 00       	mov    $0x0,%eax
  80032f:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800332:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800335:	01 c0                	add    %eax,%eax
  800337:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80033b:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80033e:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800341:	83 fa 09             	cmp    $0x9,%edx
  800344:	77 34                	ja     80037a <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800346:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800347:	eb e9                	jmp    800332 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800349:	8b 45 14             	mov    0x14(%ebp),%eax
  80034c:	8d 48 04             	lea    0x4(%eax),%ecx
  80034f:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800352:	8b 00                	mov    (%eax),%eax
  800354:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800357:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80035a:	eb 24                	jmp    800380 <vprintfmt+0xd9>
  80035c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800360:	79 07                	jns    800369 <vprintfmt+0xc2>
  800362:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800369:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80036c:	eb 90                	jmp    8002fe <vprintfmt+0x57>
  80036e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800371:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800378:	eb 84                	jmp    8002fe <vprintfmt+0x57>
  80037a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80037d:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800380:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800384:	0f 89 74 ff ff ff    	jns    8002fe <vprintfmt+0x57>
				width = precision, precision = -1;
  80038a:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80038d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800390:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800397:	e9 62 ff ff ff       	jmp    8002fe <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80039c:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003a0:	e9 59 ff ff ff       	jmp    8002fe <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003a5:	8b 45 14             	mov    0x14(%ebp),%eax
  8003a8:	8d 50 04             	lea    0x4(%eax),%edx
  8003ab:	89 55 14             	mov    %edx,0x14(%ebp)
  8003ae:	83 ec 08             	sub    $0x8,%esp
  8003b1:	53                   	push   %ebx
  8003b2:	ff 30                	pushl  (%eax)
  8003b4:	ff d6                	call   *%esi
			break;
  8003b6:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003bc:	e9 0c ff ff ff       	jmp    8002cd <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003c1:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c4:	8d 50 04             	lea    0x4(%eax),%edx
  8003c7:	89 55 14             	mov    %edx,0x14(%ebp)
  8003ca:	8b 00                	mov    (%eax),%eax
  8003cc:	85 c0                	test   %eax,%eax
  8003ce:	79 02                	jns    8003d2 <vprintfmt+0x12b>
  8003d0:	f7 d8                	neg    %eax
  8003d2:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003d4:	83 f8 0f             	cmp    $0xf,%eax
  8003d7:	7f 0b                	jg     8003e4 <vprintfmt+0x13d>
  8003d9:	8b 04 85 c0 12 80 00 	mov    0x8012c0(,%eax,4),%eax
  8003e0:	85 c0                	test   %eax,%eax
  8003e2:	75 18                	jne    8003fc <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003e4:	52                   	push   %edx
  8003e5:	68 3e 10 80 00       	push   $0x80103e
  8003ea:	53                   	push   %ebx
  8003eb:	56                   	push   %esi
  8003ec:	e8 99 fe ff ff       	call   80028a <printfmt>
  8003f1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f4:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003f7:	e9 d1 fe ff ff       	jmp    8002cd <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003fc:	50                   	push   %eax
  8003fd:	68 47 10 80 00       	push   $0x801047
  800402:	53                   	push   %ebx
  800403:	56                   	push   %esi
  800404:	e8 81 fe ff ff       	call   80028a <printfmt>
  800409:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80040f:	e9 b9 fe ff ff       	jmp    8002cd <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800414:	8b 45 14             	mov    0x14(%ebp),%eax
  800417:	8d 50 04             	lea    0x4(%eax),%edx
  80041a:	89 55 14             	mov    %edx,0x14(%ebp)
  80041d:	8b 38                	mov    (%eax),%edi
  80041f:	85 ff                	test   %edi,%edi
  800421:	75 05                	jne    800428 <vprintfmt+0x181>
				p = "(null)";
  800423:	bf 37 10 80 00       	mov    $0x801037,%edi
			if (width > 0 && padc != '-')
  800428:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80042c:	0f 8e 90 00 00 00    	jle    8004c2 <vprintfmt+0x21b>
  800432:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800436:	0f 84 8e 00 00 00    	je     8004ca <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80043c:	83 ec 08             	sub    $0x8,%esp
  80043f:	ff 75 d0             	pushl  -0x30(%ebp)
  800442:	57                   	push   %edi
  800443:	e8 70 02 00 00       	call   8006b8 <strnlen>
  800448:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80044b:	29 c1                	sub    %eax,%ecx
  80044d:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800450:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800453:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800457:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80045a:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80045d:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80045f:	eb 0d                	jmp    80046e <vprintfmt+0x1c7>
					putch(padc, putdat);
  800461:	83 ec 08             	sub    $0x8,%esp
  800464:	53                   	push   %ebx
  800465:	ff 75 e4             	pushl  -0x1c(%ebp)
  800468:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80046a:	4f                   	dec    %edi
  80046b:	83 c4 10             	add    $0x10,%esp
  80046e:	85 ff                	test   %edi,%edi
  800470:	7f ef                	jg     800461 <vprintfmt+0x1ba>
  800472:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800475:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800478:	89 c8                	mov    %ecx,%eax
  80047a:	85 c9                	test   %ecx,%ecx
  80047c:	79 05                	jns    800483 <vprintfmt+0x1dc>
  80047e:	b8 00 00 00 00       	mov    $0x0,%eax
  800483:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800486:	29 c1                	sub    %eax,%ecx
  800488:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80048b:	89 75 08             	mov    %esi,0x8(%ebp)
  80048e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800491:	eb 3d                	jmp    8004d0 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800493:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800497:	74 19                	je     8004b2 <vprintfmt+0x20b>
  800499:	0f be c0             	movsbl %al,%eax
  80049c:	83 e8 20             	sub    $0x20,%eax
  80049f:	83 f8 5e             	cmp    $0x5e,%eax
  8004a2:	76 0e                	jbe    8004b2 <vprintfmt+0x20b>
					putch('?', putdat);
  8004a4:	83 ec 08             	sub    $0x8,%esp
  8004a7:	53                   	push   %ebx
  8004a8:	6a 3f                	push   $0x3f
  8004aa:	ff 55 08             	call   *0x8(%ebp)
  8004ad:	83 c4 10             	add    $0x10,%esp
  8004b0:	eb 0b                	jmp    8004bd <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004b2:	83 ec 08             	sub    $0x8,%esp
  8004b5:	53                   	push   %ebx
  8004b6:	52                   	push   %edx
  8004b7:	ff 55 08             	call   *0x8(%ebp)
  8004ba:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004bd:	ff 4d e4             	decl   -0x1c(%ebp)
  8004c0:	eb 0e                	jmp    8004d0 <vprintfmt+0x229>
  8004c2:	89 75 08             	mov    %esi,0x8(%ebp)
  8004c5:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004c8:	eb 06                	jmp    8004d0 <vprintfmt+0x229>
  8004ca:	89 75 08             	mov    %esi,0x8(%ebp)
  8004cd:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d0:	47                   	inc    %edi
  8004d1:	8a 47 ff             	mov    -0x1(%edi),%al
  8004d4:	0f be d0             	movsbl %al,%edx
  8004d7:	85 d2                	test   %edx,%edx
  8004d9:	74 1d                	je     8004f8 <vprintfmt+0x251>
  8004db:	85 f6                	test   %esi,%esi
  8004dd:	78 b4                	js     800493 <vprintfmt+0x1ec>
  8004df:	4e                   	dec    %esi
  8004e0:	79 b1                	jns    800493 <vprintfmt+0x1ec>
  8004e2:	8b 75 08             	mov    0x8(%ebp),%esi
  8004e5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004e8:	eb 14                	jmp    8004fe <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004ea:	83 ec 08             	sub    $0x8,%esp
  8004ed:	53                   	push   %ebx
  8004ee:	6a 20                	push   $0x20
  8004f0:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004f2:	4f                   	dec    %edi
  8004f3:	83 c4 10             	add    $0x10,%esp
  8004f6:	eb 06                	jmp    8004fe <vprintfmt+0x257>
  8004f8:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004fb:	8b 75 08             	mov    0x8(%ebp),%esi
  8004fe:	85 ff                	test   %edi,%edi
  800500:	7f e8                	jg     8004ea <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800502:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800505:	e9 c3 fd ff ff       	jmp    8002cd <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80050a:	83 fa 01             	cmp    $0x1,%edx
  80050d:	7e 16                	jle    800525 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80050f:	8b 45 14             	mov    0x14(%ebp),%eax
  800512:	8d 50 08             	lea    0x8(%eax),%edx
  800515:	89 55 14             	mov    %edx,0x14(%ebp)
  800518:	8b 50 04             	mov    0x4(%eax),%edx
  80051b:	8b 00                	mov    (%eax),%eax
  80051d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800520:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800523:	eb 32                	jmp    800557 <vprintfmt+0x2b0>
	else if (lflag)
  800525:	85 d2                	test   %edx,%edx
  800527:	74 18                	je     800541 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800529:	8b 45 14             	mov    0x14(%ebp),%eax
  80052c:	8d 50 04             	lea    0x4(%eax),%edx
  80052f:	89 55 14             	mov    %edx,0x14(%ebp)
  800532:	8b 00                	mov    (%eax),%eax
  800534:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800537:	89 c1                	mov    %eax,%ecx
  800539:	c1 f9 1f             	sar    $0x1f,%ecx
  80053c:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80053f:	eb 16                	jmp    800557 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800541:	8b 45 14             	mov    0x14(%ebp),%eax
  800544:	8d 50 04             	lea    0x4(%eax),%edx
  800547:	89 55 14             	mov    %edx,0x14(%ebp)
  80054a:	8b 00                	mov    (%eax),%eax
  80054c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80054f:	89 c1                	mov    %eax,%ecx
  800551:	c1 f9 1f             	sar    $0x1f,%ecx
  800554:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800557:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80055a:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80055d:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800561:	79 76                	jns    8005d9 <vprintfmt+0x332>
				putch('-', putdat);
  800563:	83 ec 08             	sub    $0x8,%esp
  800566:	53                   	push   %ebx
  800567:	6a 2d                	push   $0x2d
  800569:	ff d6                	call   *%esi
				num = -(long long) num;
  80056b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80056e:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800571:	f7 d8                	neg    %eax
  800573:	83 d2 00             	adc    $0x0,%edx
  800576:	f7 da                	neg    %edx
  800578:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80057b:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800580:	eb 5c                	jmp    8005de <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800582:	8d 45 14             	lea    0x14(%ebp),%eax
  800585:	e8 aa fc ff ff       	call   800234 <getuint>
			base = 10;
  80058a:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80058f:	eb 4d                	jmp    8005de <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800591:	8d 45 14             	lea    0x14(%ebp),%eax
  800594:	e8 9b fc ff ff       	call   800234 <getuint>
			base = 8;
  800599:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80059e:	eb 3e                	jmp    8005de <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005a0:	83 ec 08             	sub    $0x8,%esp
  8005a3:	53                   	push   %ebx
  8005a4:	6a 30                	push   $0x30
  8005a6:	ff d6                	call   *%esi
			putch('x', putdat);
  8005a8:	83 c4 08             	add    $0x8,%esp
  8005ab:	53                   	push   %ebx
  8005ac:	6a 78                	push   $0x78
  8005ae:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005b0:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b3:	8d 50 04             	lea    0x4(%eax),%edx
  8005b6:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005b9:	8b 00                	mov    (%eax),%eax
  8005bb:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005c0:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005c3:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005c8:	eb 14                	jmp    8005de <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005ca:	8d 45 14             	lea    0x14(%ebp),%eax
  8005cd:	e8 62 fc ff ff       	call   800234 <getuint>
			base = 16;
  8005d2:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005d7:	eb 05                	jmp    8005de <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005d9:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005de:	83 ec 0c             	sub    $0xc,%esp
  8005e1:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005e5:	57                   	push   %edi
  8005e6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005e9:	51                   	push   %ecx
  8005ea:	52                   	push   %edx
  8005eb:	50                   	push   %eax
  8005ec:	89 da                	mov    %ebx,%edx
  8005ee:	89 f0                	mov    %esi,%eax
  8005f0:	e8 92 fb ff ff       	call   800187 <printnum>
			break;
  8005f5:	83 c4 20             	add    $0x20,%esp
  8005f8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fb:	e9 cd fc ff ff       	jmp    8002cd <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800600:	83 ec 08             	sub    $0x8,%esp
  800603:	53                   	push   %ebx
  800604:	51                   	push   %ecx
  800605:	ff d6                	call   *%esi
			break;
  800607:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80060a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80060d:	e9 bb fc ff ff       	jmp    8002cd <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800612:	83 ec 08             	sub    $0x8,%esp
  800615:	53                   	push   %ebx
  800616:	6a 25                	push   $0x25
  800618:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80061a:	83 c4 10             	add    $0x10,%esp
  80061d:	eb 01                	jmp    800620 <vprintfmt+0x379>
  80061f:	4f                   	dec    %edi
  800620:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800624:	75 f9                	jne    80061f <vprintfmt+0x378>
  800626:	e9 a2 fc ff ff       	jmp    8002cd <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80062b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80062e:	5b                   	pop    %ebx
  80062f:	5e                   	pop    %esi
  800630:	5f                   	pop    %edi
  800631:	5d                   	pop    %ebp
  800632:	c3                   	ret    

00800633 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800633:	55                   	push   %ebp
  800634:	89 e5                	mov    %esp,%ebp
  800636:	83 ec 18             	sub    $0x18,%esp
  800639:	8b 45 08             	mov    0x8(%ebp),%eax
  80063c:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80063f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800642:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800646:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800649:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800650:	85 c0                	test   %eax,%eax
  800652:	74 26                	je     80067a <vsnprintf+0x47>
  800654:	85 d2                	test   %edx,%edx
  800656:	7e 29                	jle    800681 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800658:	ff 75 14             	pushl  0x14(%ebp)
  80065b:	ff 75 10             	pushl  0x10(%ebp)
  80065e:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800661:	50                   	push   %eax
  800662:	68 6e 02 80 00       	push   $0x80026e
  800667:	e8 3b fc ff ff       	call   8002a7 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80066c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80066f:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800672:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800675:	83 c4 10             	add    $0x10,%esp
  800678:	eb 0c                	jmp    800686 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80067a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80067f:	eb 05                	jmp    800686 <vsnprintf+0x53>
  800681:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800686:	c9                   	leave  
  800687:	c3                   	ret    

00800688 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800688:	55                   	push   %ebp
  800689:	89 e5                	mov    %esp,%ebp
  80068b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80068e:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800691:	50                   	push   %eax
  800692:	ff 75 10             	pushl  0x10(%ebp)
  800695:	ff 75 0c             	pushl  0xc(%ebp)
  800698:	ff 75 08             	pushl  0x8(%ebp)
  80069b:	e8 93 ff ff ff       	call   800633 <vsnprintf>
	va_end(ap);

	return rc;
}
  8006a0:	c9                   	leave  
  8006a1:	c3                   	ret    

008006a2 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006a2:	55                   	push   %ebp
  8006a3:	89 e5                	mov    %esp,%ebp
  8006a5:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006a8:	b8 00 00 00 00       	mov    $0x0,%eax
  8006ad:	eb 01                	jmp    8006b0 <strlen+0xe>
		n++;
  8006af:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006b0:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006b4:	75 f9                	jne    8006af <strlen+0xd>
		n++;
	return n;
}
  8006b6:	5d                   	pop    %ebp
  8006b7:	c3                   	ret    

008006b8 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006b8:	55                   	push   %ebp
  8006b9:	89 e5                	mov    %esp,%ebp
  8006bb:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006be:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006c1:	ba 00 00 00 00       	mov    $0x0,%edx
  8006c6:	eb 01                	jmp    8006c9 <strnlen+0x11>
		n++;
  8006c8:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006c9:	39 c2                	cmp    %eax,%edx
  8006cb:	74 08                	je     8006d5 <strnlen+0x1d>
  8006cd:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006d1:	75 f5                	jne    8006c8 <strnlen+0x10>
  8006d3:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006d5:	5d                   	pop    %ebp
  8006d6:	c3                   	ret    

008006d7 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006d7:	55                   	push   %ebp
  8006d8:	89 e5                	mov    %esp,%ebp
  8006da:	53                   	push   %ebx
  8006db:	8b 45 08             	mov    0x8(%ebp),%eax
  8006de:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006e1:	89 c2                	mov    %eax,%edx
  8006e3:	42                   	inc    %edx
  8006e4:	41                   	inc    %ecx
  8006e5:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006e8:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006eb:	84 db                	test   %bl,%bl
  8006ed:	75 f4                	jne    8006e3 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006ef:	5b                   	pop    %ebx
  8006f0:	5d                   	pop    %ebp
  8006f1:	c3                   	ret    

008006f2 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006f2:	55                   	push   %ebp
  8006f3:	89 e5                	mov    %esp,%ebp
  8006f5:	53                   	push   %ebx
  8006f6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006f9:	53                   	push   %ebx
  8006fa:	e8 a3 ff ff ff       	call   8006a2 <strlen>
  8006ff:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800702:	ff 75 0c             	pushl  0xc(%ebp)
  800705:	01 d8                	add    %ebx,%eax
  800707:	50                   	push   %eax
  800708:	e8 ca ff ff ff       	call   8006d7 <strcpy>
	return dst;
}
  80070d:	89 d8                	mov    %ebx,%eax
  80070f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800712:	c9                   	leave  
  800713:	c3                   	ret    

00800714 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800714:	55                   	push   %ebp
  800715:	89 e5                	mov    %esp,%ebp
  800717:	56                   	push   %esi
  800718:	53                   	push   %ebx
  800719:	8b 75 08             	mov    0x8(%ebp),%esi
  80071c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80071f:	89 f3                	mov    %esi,%ebx
  800721:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800724:	89 f2                	mov    %esi,%edx
  800726:	eb 0c                	jmp    800734 <strncpy+0x20>
		*dst++ = *src;
  800728:	42                   	inc    %edx
  800729:	8a 01                	mov    (%ecx),%al
  80072b:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80072e:	80 39 01             	cmpb   $0x1,(%ecx)
  800731:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800734:	39 da                	cmp    %ebx,%edx
  800736:	75 f0                	jne    800728 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800738:	89 f0                	mov    %esi,%eax
  80073a:	5b                   	pop    %ebx
  80073b:	5e                   	pop    %esi
  80073c:	5d                   	pop    %ebp
  80073d:	c3                   	ret    

0080073e <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80073e:	55                   	push   %ebp
  80073f:	89 e5                	mov    %esp,%ebp
  800741:	56                   	push   %esi
  800742:	53                   	push   %ebx
  800743:	8b 75 08             	mov    0x8(%ebp),%esi
  800746:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800749:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80074c:	85 c0                	test   %eax,%eax
  80074e:	74 1e                	je     80076e <strlcpy+0x30>
  800750:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800754:	89 f2                	mov    %esi,%edx
  800756:	eb 05                	jmp    80075d <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800758:	42                   	inc    %edx
  800759:	41                   	inc    %ecx
  80075a:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80075d:	39 c2                	cmp    %eax,%edx
  80075f:	74 08                	je     800769 <strlcpy+0x2b>
  800761:	8a 19                	mov    (%ecx),%bl
  800763:	84 db                	test   %bl,%bl
  800765:	75 f1                	jne    800758 <strlcpy+0x1a>
  800767:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800769:	c6 00 00             	movb   $0x0,(%eax)
  80076c:	eb 02                	jmp    800770 <strlcpy+0x32>
  80076e:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800770:	29 f0                	sub    %esi,%eax
}
  800772:	5b                   	pop    %ebx
  800773:	5e                   	pop    %esi
  800774:	5d                   	pop    %ebp
  800775:	c3                   	ret    

00800776 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800776:	55                   	push   %ebp
  800777:	89 e5                	mov    %esp,%ebp
  800779:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80077c:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80077f:	eb 02                	jmp    800783 <strcmp+0xd>
		p++, q++;
  800781:	41                   	inc    %ecx
  800782:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800783:	8a 01                	mov    (%ecx),%al
  800785:	84 c0                	test   %al,%al
  800787:	74 04                	je     80078d <strcmp+0x17>
  800789:	3a 02                	cmp    (%edx),%al
  80078b:	74 f4                	je     800781 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80078d:	0f b6 c0             	movzbl %al,%eax
  800790:	0f b6 12             	movzbl (%edx),%edx
  800793:	29 d0                	sub    %edx,%eax
}
  800795:	5d                   	pop    %ebp
  800796:	c3                   	ret    

00800797 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800797:	55                   	push   %ebp
  800798:	89 e5                	mov    %esp,%ebp
  80079a:	53                   	push   %ebx
  80079b:	8b 45 08             	mov    0x8(%ebp),%eax
  80079e:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007a1:	89 c3                	mov    %eax,%ebx
  8007a3:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007a6:	eb 02                	jmp    8007aa <strncmp+0x13>
		n--, p++, q++;
  8007a8:	40                   	inc    %eax
  8007a9:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007aa:	39 d8                	cmp    %ebx,%eax
  8007ac:	74 14                	je     8007c2 <strncmp+0x2b>
  8007ae:	8a 08                	mov    (%eax),%cl
  8007b0:	84 c9                	test   %cl,%cl
  8007b2:	74 04                	je     8007b8 <strncmp+0x21>
  8007b4:	3a 0a                	cmp    (%edx),%cl
  8007b6:	74 f0                	je     8007a8 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007b8:	0f b6 00             	movzbl (%eax),%eax
  8007bb:	0f b6 12             	movzbl (%edx),%edx
  8007be:	29 d0                	sub    %edx,%eax
  8007c0:	eb 05                	jmp    8007c7 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007c2:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007c7:	5b                   	pop    %ebx
  8007c8:	5d                   	pop    %ebp
  8007c9:	c3                   	ret    

008007ca <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007ca:	55                   	push   %ebp
  8007cb:	89 e5                	mov    %esp,%ebp
  8007cd:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d0:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007d3:	eb 05                	jmp    8007da <strchr+0x10>
		if (*s == c)
  8007d5:	38 ca                	cmp    %cl,%dl
  8007d7:	74 0c                	je     8007e5 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007d9:	40                   	inc    %eax
  8007da:	8a 10                	mov    (%eax),%dl
  8007dc:	84 d2                	test   %dl,%dl
  8007de:	75 f5                	jne    8007d5 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007e0:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007e5:	5d                   	pop    %ebp
  8007e6:	c3                   	ret    

008007e7 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007e7:	55                   	push   %ebp
  8007e8:	89 e5                	mov    %esp,%ebp
  8007ea:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ed:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007f0:	eb 05                	jmp    8007f7 <strfind+0x10>
		if (*s == c)
  8007f2:	38 ca                	cmp    %cl,%dl
  8007f4:	74 07                	je     8007fd <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007f6:	40                   	inc    %eax
  8007f7:	8a 10                	mov    (%eax),%dl
  8007f9:	84 d2                	test   %dl,%dl
  8007fb:	75 f5                	jne    8007f2 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007fd:	5d                   	pop    %ebp
  8007fe:	c3                   	ret    

008007ff <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007ff:	55                   	push   %ebp
  800800:	89 e5                	mov    %esp,%ebp
  800802:	57                   	push   %edi
  800803:	56                   	push   %esi
  800804:	53                   	push   %ebx
  800805:	8b 7d 08             	mov    0x8(%ebp),%edi
  800808:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80080b:	85 c9                	test   %ecx,%ecx
  80080d:	74 36                	je     800845 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80080f:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800815:	75 28                	jne    80083f <memset+0x40>
  800817:	f6 c1 03             	test   $0x3,%cl
  80081a:	75 23                	jne    80083f <memset+0x40>
		c &= 0xFF;
  80081c:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800820:	89 d3                	mov    %edx,%ebx
  800822:	c1 e3 08             	shl    $0x8,%ebx
  800825:	89 d6                	mov    %edx,%esi
  800827:	c1 e6 18             	shl    $0x18,%esi
  80082a:	89 d0                	mov    %edx,%eax
  80082c:	c1 e0 10             	shl    $0x10,%eax
  80082f:	09 f0                	or     %esi,%eax
  800831:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800833:	89 d8                	mov    %ebx,%eax
  800835:	09 d0                	or     %edx,%eax
  800837:	c1 e9 02             	shr    $0x2,%ecx
  80083a:	fc                   	cld    
  80083b:	f3 ab                	rep stos %eax,%es:(%edi)
  80083d:	eb 06                	jmp    800845 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80083f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800842:	fc                   	cld    
  800843:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800845:	89 f8                	mov    %edi,%eax
  800847:	5b                   	pop    %ebx
  800848:	5e                   	pop    %esi
  800849:	5f                   	pop    %edi
  80084a:	5d                   	pop    %ebp
  80084b:	c3                   	ret    

0080084c <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80084c:	55                   	push   %ebp
  80084d:	89 e5                	mov    %esp,%ebp
  80084f:	57                   	push   %edi
  800850:	56                   	push   %esi
  800851:	8b 45 08             	mov    0x8(%ebp),%eax
  800854:	8b 75 0c             	mov    0xc(%ebp),%esi
  800857:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80085a:	39 c6                	cmp    %eax,%esi
  80085c:	73 33                	jae    800891 <memmove+0x45>
  80085e:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800861:	39 d0                	cmp    %edx,%eax
  800863:	73 2c                	jae    800891 <memmove+0x45>
		s += n;
		d += n;
  800865:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800868:	89 d6                	mov    %edx,%esi
  80086a:	09 fe                	or     %edi,%esi
  80086c:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800872:	75 13                	jne    800887 <memmove+0x3b>
  800874:	f6 c1 03             	test   $0x3,%cl
  800877:	75 0e                	jne    800887 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800879:	83 ef 04             	sub    $0x4,%edi
  80087c:	8d 72 fc             	lea    -0x4(%edx),%esi
  80087f:	c1 e9 02             	shr    $0x2,%ecx
  800882:	fd                   	std    
  800883:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800885:	eb 07                	jmp    80088e <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800887:	4f                   	dec    %edi
  800888:	8d 72 ff             	lea    -0x1(%edx),%esi
  80088b:	fd                   	std    
  80088c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80088e:	fc                   	cld    
  80088f:	eb 1d                	jmp    8008ae <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800891:	89 f2                	mov    %esi,%edx
  800893:	09 c2                	or     %eax,%edx
  800895:	f6 c2 03             	test   $0x3,%dl
  800898:	75 0f                	jne    8008a9 <memmove+0x5d>
  80089a:	f6 c1 03             	test   $0x3,%cl
  80089d:	75 0a                	jne    8008a9 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80089f:	c1 e9 02             	shr    $0x2,%ecx
  8008a2:	89 c7                	mov    %eax,%edi
  8008a4:	fc                   	cld    
  8008a5:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008a7:	eb 05                	jmp    8008ae <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008a9:	89 c7                	mov    %eax,%edi
  8008ab:	fc                   	cld    
  8008ac:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008ae:	5e                   	pop    %esi
  8008af:	5f                   	pop    %edi
  8008b0:	5d                   	pop    %ebp
  8008b1:	c3                   	ret    

008008b2 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008b2:	55                   	push   %ebp
  8008b3:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008b5:	ff 75 10             	pushl  0x10(%ebp)
  8008b8:	ff 75 0c             	pushl  0xc(%ebp)
  8008bb:	ff 75 08             	pushl  0x8(%ebp)
  8008be:	e8 89 ff ff ff       	call   80084c <memmove>
}
  8008c3:	c9                   	leave  
  8008c4:	c3                   	ret    

008008c5 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008c5:	55                   	push   %ebp
  8008c6:	89 e5                	mov    %esp,%ebp
  8008c8:	56                   	push   %esi
  8008c9:	53                   	push   %ebx
  8008ca:	8b 45 08             	mov    0x8(%ebp),%eax
  8008cd:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008d0:	89 c6                	mov    %eax,%esi
  8008d2:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008d5:	eb 14                	jmp    8008eb <memcmp+0x26>
		if (*s1 != *s2)
  8008d7:	8a 08                	mov    (%eax),%cl
  8008d9:	8a 1a                	mov    (%edx),%bl
  8008db:	38 d9                	cmp    %bl,%cl
  8008dd:	74 0a                	je     8008e9 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008df:	0f b6 c1             	movzbl %cl,%eax
  8008e2:	0f b6 db             	movzbl %bl,%ebx
  8008e5:	29 d8                	sub    %ebx,%eax
  8008e7:	eb 0b                	jmp    8008f4 <memcmp+0x2f>
		s1++, s2++;
  8008e9:	40                   	inc    %eax
  8008ea:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008eb:	39 f0                	cmp    %esi,%eax
  8008ed:	75 e8                	jne    8008d7 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008ef:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008f4:	5b                   	pop    %ebx
  8008f5:	5e                   	pop    %esi
  8008f6:	5d                   	pop    %ebp
  8008f7:	c3                   	ret    

008008f8 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008f8:	55                   	push   %ebp
  8008f9:	89 e5                	mov    %esp,%ebp
  8008fb:	53                   	push   %ebx
  8008fc:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008ff:	89 c1                	mov    %eax,%ecx
  800901:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800904:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800908:	eb 08                	jmp    800912 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80090a:	0f b6 10             	movzbl (%eax),%edx
  80090d:	39 da                	cmp    %ebx,%edx
  80090f:	74 05                	je     800916 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800911:	40                   	inc    %eax
  800912:	39 c8                	cmp    %ecx,%eax
  800914:	72 f4                	jb     80090a <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800916:	5b                   	pop    %ebx
  800917:	5d                   	pop    %ebp
  800918:	c3                   	ret    

00800919 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800919:	55                   	push   %ebp
  80091a:	89 e5                	mov    %esp,%ebp
  80091c:	57                   	push   %edi
  80091d:	56                   	push   %esi
  80091e:	53                   	push   %ebx
  80091f:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800922:	eb 01                	jmp    800925 <strtol+0xc>
		s++;
  800924:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800925:	8a 01                	mov    (%ecx),%al
  800927:	3c 20                	cmp    $0x20,%al
  800929:	74 f9                	je     800924 <strtol+0xb>
  80092b:	3c 09                	cmp    $0x9,%al
  80092d:	74 f5                	je     800924 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80092f:	3c 2b                	cmp    $0x2b,%al
  800931:	75 08                	jne    80093b <strtol+0x22>
		s++;
  800933:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800934:	bf 00 00 00 00       	mov    $0x0,%edi
  800939:	eb 11                	jmp    80094c <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80093b:	3c 2d                	cmp    $0x2d,%al
  80093d:	75 08                	jne    800947 <strtol+0x2e>
		s++, neg = 1;
  80093f:	41                   	inc    %ecx
  800940:	bf 01 00 00 00       	mov    $0x1,%edi
  800945:	eb 05                	jmp    80094c <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800947:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80094c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800950:	0f 84 87 00 00 00    	je     8009dd <strtol+0xc4>
  800956:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80095a:	75 27                	jne    800983 <strtol+0x6a>
  80095c:	80 39 30             	cmpb   $0x30,(%ecx)
  80095f:	75 22                	jne    800983 <strtol+0x6a>
  800961:	e9 88 00 00 00       	jmp    8009ee <strtol+0xd5>
		s += 2, base = 16;
  800966:	83 c1 02             	add    $0x2,%ecx
  800969:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800970:	eb 11                	jmp    800983 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800972:	41                   	inc    %ecx
  800973:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80097a:	eb 07                	jmp    800983 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  80097c:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800983:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800988:	8a 11                	mov    (%ecx),%dl
  80098a:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80098d:	80 fb 09             	cmp    $0x9,%bl
  800990:	77 08                	ja     80099a <strtol+0x81>
			dig = *s - '0';
  800992:	0f be d2             	movsbl %dl,%edx
  800995:	83 ea 30             	sub    $0x30,%edx
  800998:	eb 22                	jmp    8009bc <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80099a:	8d 72 9f             	lea    -0x61(%edx),%esi
  80099d:	89 f3                	mov    %esi,%ebx
  80099f:	80 fb 19             	cmp    $0x19,%bl
  8009a2:	77 08                	ja     8009ac <strtol+0x93>
			dig = *s - 'a' + 10;
  8009a4:	0f be d2             	movsbl %dl,%edx
  8009a7:	83 ea 57             	sub    $0x57,%edx
  8009aa:	eb 10                	jmp    8009bc <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009ac:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009af:	89 f3                	mov    %esi,%ebx
  8009b1:	80 fb 19             	cmp    $0x19,%bl
  8009b4:	77 14                	ja     8009ca <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009b6:	0f be d2             	movsbl %dl,%edx
  8009b9:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009bc:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009bf:	7d 09                	jge    8009ca <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009c1:	41                   	inc    %ecx
  8009c2:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009c6:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009c8:	eb be                	jmp    800988 <strtol+0x6f>

	if (endptr)
  8009ca:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009ce:	74 05                	je     8009d5 <strtol+0xbc>
		*endptr = (char *) s;
  8009d0:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009d3:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009d5:	85 ff                	test   %edi,%edi
  8009d7:	74 21                	je     8009fa <strtol+0xe1>
  8009d9:	f7 d8                	neg    %eax
  8009db:	eb 1d                	jmp    8009fa <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009dd:	80 39 30             	cmpb   $0x30,(%ecx)
  8009e0:	75 9a                	jne    80097c <strtol+0x63>
  8009e2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009e6:	0f 84 7a ff ff ff    	je     800966 <strtol+0x4d>
  8009ec:	eb 84                	jmp    800972 <strtol+0x59>
  8009ee:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009f2:	0f 84 6e ff ff ff    	je     800966 <strtol+0x4d>
  8009f8:	eb 89                	jmp    800983 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009fa:	5b                   	pop    %ebx
  8009fb:	5e                   	pop    %esi
  8009fc:	5f                   	pop    %edi
  8009fd:	5d                   	pop    %ebp
  8009fe:	c3                   	ret    

008009ff <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009ff:	55                   	push   %ebp
  800a00:	89 e5                	mov    %esp,%ebp
  800a02:	57                   	push   %edi
  800a03:	56                   	push   %esi
  800a04:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a05:	b8 00 00 00 00       	mov    $0x0,%eax
  800a0a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a0d:	8b 55 08             	mov    0x8(%ebp),%edx
  800a10:	89 c3                	mov    %eax,%ebx
  800a12:	89 c7                	mov    %eax,%edi
  800a14:	89 c6                	mov    %eax,%esi
  800a16:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a18:	5b                   	pop    %ebx
  800a19:	5e                   	pop    %esi
  800a1a:	5f                   	pop    %edi
  800a1b:	5d                   	pop    %ebp
  800a1c:	c3                   	ret    

00800a1d <sys_cgetc>:

int
sys_cgetc(void)
{
  800a1d:	55                   	push   %ebp
  800a1e:	89 e5                	mov    %esp,%ebp
  800a20:	57                   	push   %edi
  800a21:	56                   	push   %esi
  800a22:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a23:	ba 00 00 00 00       	mov    $0x0,%edx
  800a28:	b8 01 00 00 00       	mov    $0x1,%eax
  800a2d:	89 d1                	mov    %edx,%ecx
  800a2f:	89 d3                	mov    %edx,%ebx
  800a31:	89 d7                	mov    %edx,%edi
  800a33:	89 d6                	mov    %edx,%esi
  800a35:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a37:	5b                   	pop    %ebx
  800a38:	5e                   	pop    %esi
  800a39:	5f                   	pop    %edi
  800a3a:	5d                   	pop    %ebp
  800a3b:	c3                   	ret    

00800a3c <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a3c:	55                   	push   %ebp
  800a3d:	89 e5                	mov    %esp,%ebp
  800a3f:	57                   	push   %edi
  800a40:	56                   	push   %esi
  800a41:	53                   	push   %ebx
  800a42:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a45:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a4a:	b8 03 00 00 00       	mov    $0x3,%eax
  800a4f:	8b 55 08             	mov    0x8(%ebp),%edx
  800a52:	89 cb                	mov    %ecx,%ebx
  800a54:	89 cf                	mov    %ecx,%edi
  800a56:	89 ce                	mov    %ecx,%esi
  800a58:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a5a:	85 c0                	test   %eax,%eax
  800a5c:	7e 17                	jle    800a75 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a5e:	83 ec 0c             	sub    $0xc,%esp
  800a61:	50                   	push   %eax
  800a62:	6a 03                	push   $0x3
  800a64:	68 1f 13 80 00       	push   $0x80131f
  800a69:	6a 23                	push   $0x23
  800a6b:	68 3c 13 80 00       	push   $0x80133c
  800a70:	e8 d5 02 00 00       	call   800d4a <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a75:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a78:	5b                   	pop    %ebx
  800a79:	5e                   	pop    %esi
  800a7a:	5f                   	pop    %edi
  800a7b:	5d                   	pop    %ebp
  800a7c:	c3                   	ret    

00800a7d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a7d:	55                   	push   %ebp
  800a7e:	89 e5                	mov    %esp,%ebp
  800a80:	57                   	push   %edi
  800a81:	56                   	push   %esi
  800a82:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a83:	ba 00 00 00 00       	mov    $0x0,%edx
  800a88:	b8 02 00 00 00       	mov    $0x2,%eax
  800a8d:	89 d1                	mov    %edx,%ecx
  800a8f:	89 d3                	mov    %edx,%ebx
  800a91:	89 d7                	mov    %edx,%edi
  800a93:	89 d6                	mov    %edx,%esi
  800a95:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a97:	5b                   	pop    %ebx
  800a98:	5e                   	pop    %esi
  800a99:	5f                   	pop    %edi
  800a9a:	5d                   	pop    %ebp
  800a9b:	c3                   	ret    

00800a9c <sys_yield>:

void
sys_yield(void)
{
  800a9c:	55                   	push   %ebp
  800a9d:	89 e5                	mov    %esp,%ebp
  800a9f:	57                   	push   %edi
  800aa0:	56                   	push   %esi
  800aa1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aa2:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa7:	b8 0b 00 00 00       	mov    $0xb,%eax
  800aac:	89 d1                	mov    %edx,%ecx
  800aae:	89 d3                	mov    %edx,%ebx
  800ab0:	89 d7                	mov    %edx,%edi
  800ab2:	89 d6                	mov    %edx,%esi
  800ab4:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800ab6:	5b                   	pop    %ebx
  800ab7:	5e                   	pop    %esi
  800ab8:	5f                   	pop    %edi
  800ab9:	5d                   	pop    %ebp
  800aba:	c3                   	ret    

00800abb <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800abb:	55                   	push   %ebp
  800abc:	89 e5                	mov    %esp,%ebp
  800abe:	57                   	push   %edi
  800abf:	56                   	push   %esi
  800ac0:	53                   	push   %ebx
  800ac1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac4:	be 00 00 00 00       	mov    $0x0,%esi
  800ac9:	b8 04 00 00 00       	mov    $0x4,%eax
  800ace:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ad1:	8b 55 08             	mov    0x8(%ebp),%edx
  800ad4:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ad7:	89 f7                	mov    %esi,%edi
  800ad9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800adb:	85 c0                	test   %eax,%eax
  800add:	7e 17                	jle    800af6 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800adf:	83 ec 0c             	sub    $0xc,%esp
  800ae2:	50                   	push   %eax
  800ae3:	6a 04                	push   $0x4
  800ae5:	68 1f 13 80 00       	push   $0x80131f
  800aea:	6a 23                	push   $0x23
  800aec:	68 3c 13 80 00       	push   $0x80133c
  800af1:	e8 54 02 00 00       	call   800d4a <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800af6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800af9:	5b                   	pop    %ebx
  800afa:	5e                   	pop    %esi
  800afb:	5f                   	pop    %edi
  800afc:	5d                   	pop    %ebp
  800afd:	c3                   	ret    

00800afe <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800afe:	55                   	push   %ebp
  800aff:	89 e5                	mov    %esp,%ebp
  800b01:	57                   	push   %edi
  800b02:	56                   	push   %esi
  800b03:	53                   	push   %ebx
  800b04:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b07:	b8 05 00 00 00       	mov    $0x5,%eax
  800b0c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b0f:	8b 55 08             	mov    0x8(%ebp),%edx
  800b12:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b15:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b18:	8b 75 18             	mov    0x18(%ebp),%esi
  800b1b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b1d:	85 c0                	test   %eax,%eax
  800b1f:	7e 17                	jle    800b38 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b21:	83 ec 0c             	sub    $0xc,%esp
  800b24:	50                   	push   %eax
  800b25:	6a 05                	push   $0x5
  800b27:	68 1f 13 80 00       	push   $0x80131f
  800b2c:	6a 23                	push   $0x23
  800b2e:	68 3c 13 80 00       	push   $0x80133c
  800b33:	e8 12 02 00 00       	call   800d4a <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b38:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b3b:	5b                   	pop    %ebx
  800b3c:	5e                   	pop    %esi
  800b3d:	5f                   	pop    %edi
  800b3e:	5d                   	pop    %ebp
  800b3f:	c3                   	ret    

00800b40 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b40:	55                   	push   %ebp
  800b41:	89 e5                	mov    %esp,%ebp
  800b43:	57                   	push   %edi
  800b44:	56                   	push   %esi
  800b45:	53                   	push   %ebx
  800b46:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b49:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b4e:	b8 06 00 00 00       	mov    $0x6,%eax
  800b53:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b56:	8b 55 08             	mov    0x8(%ebp),%edx
  800b59:	89 df                	mov    %ebx,%edi
  800b5b:	89 de                	mov    %ebx,%esi
  800b5d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b5f:	85 c0                	test   %eax,%eax
  800b61:	7e 17                	jle    800b7a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b63:	83 ec 0c             	sub    $0xc,%esp
  800b66:	50                   	push   %eax
  800b67:	6a 06                	push   $0x6
  800b69:	68 1f 13 80 00       	push   $0x80131f
  800b6e:	6a 23                	push   $0x23
  800b70:	68 3c 13 80 00       	push   $0x80133c
  800b75:	e8 d0 01 00 00       	call   800d4a <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800b7a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b7d:	5b                   	pop    %ebx
  800b7e:	5e                   	pop    %esi
  800b7f:	5f                   	pop    %edi
  800b80:	5d                   	pop    %ebp
  800b81:	c3                   	ret    

00800b82 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800b82:	55                   	push   %ebp
  800b83:	89 e5                	mov    %esp,%ebp
  800b85:	57                   	push   %edi
  800b86:	56                   	push   %esi
  800b87:	53                   	push   %ebx
  800b88:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b8b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b90:	b8 08 00 00 00       	mov    $0x8,%eax
  800b95:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b98:	8b 55 08             	mov    0x8(%ebp),%edx
  800b9b:	89 df                	mov    %ebx,%edi
  800b9d:	89 de                	mov    %ebx,%esi
  800b9f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ba1:	85 c0                	test   %eax,%eax
  800ba3:	7e 17                	jle    800bbc <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ba5:	83 ec 0c             	sub    $0xc,%esp
  800ba8:	50                   	push   %eax
  800ba9:	6a 08                	push   $0x8
  800bab:	68 1f 13 80 00       	push   $0x80131f
  800bb0:	6a 23                	push   $0x23
  800bb2:	68 3c 13 80 00       	push   $0x80133c
  800bb7:	e8 8e 01 00 00       	call   800d4a <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800bbc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bbf:	5b                   	pop    %ebx
  800bc0:	5e                   	pop    %esi
  800bc1:	5f                   	pop    %edi
  800bc2:	5d                   	pop    %ebp
  800bc3:	c3                   	ret    

00800bc4 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800bc4:	55                   	push   %ebp
  800bc5:	89 e5                	mov    %esp,%ebp
  800bc7:	57                   	push   %edi
  800bc8:	56                   	push   %esi
  800bc9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bca:	ba 00 00 00 00       	mov    $0x0,%edx
  800bcf:	b8 0c 00 00 00       	mov    $0xc,%eax
  800bd4:	89 d1                	mov    %edx,%ecx
  800bd6:	89 d3                	mov    %edx,%ebx
  800bd8:	89 d7                	mov    %edx,%edi
  800bda:	89 d6                	mov    %edx,%esi
  800bdc:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800bde:	5b                   	pop    %ebx
  800bdf:	5e                   	pop    %esi
  800be0:	5f                   	pop    %edi
  800be1:	5d                   	pop    %ebp
  800be2:	c3                   	ret    

00800be3 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800be3:	55                   	push   %ebp
  800be4:	89 e5                	mov    %esp,%ebp
  800be6:	57                   	push   %edi
  800be7:	56                   	push   %esi
  800be8:	53                   	push   %ebx
  800be9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bec:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bf1:	b8 09 00 00 00       	mov    $0x9,%eax
  800bf6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bf9:	8b 55 08             	mov    0x8(%ebp),%edx
  800bfc:	89 df                	mov    %ebx,%edi
  800bfe:	89 de                	mov    %ebx,%esi
  800c00:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c02:	85 c0                	test   %eax,%eax
  800c04:	7e 17                	jle    800c1d <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c06:	83 ec 0c             	sub    $0xc,%esp
  800c09:	50                   	push   %eax
  800c0a:	6a 09                	push   $0x9
  800c0c:	68 1f 13 80 00       	push   $0x80131f
  800c11:	6a 23                	push   $0x23
  800c13:	68 3c 13 80 00       	push   $0x80133c
  800c18:	e8 2d 01 00 00       	call   800d4a <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c1d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c20:	5b                   	pop    %ebx
  800c21:	5e                   	pop    %esi
  800c22:	5f                   	pop    %edi
  800c23:	5d                   	pop    %ebp
  800c24:	c3                   	ret    

00800c25 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c25:	55                   	push   %ebp
  800c26:	89 e5                	mov    %esp,%ebp
  800c28:	57                   	push   %edi
  800c29:	56                   	push   %esi
  800c2a:	53                   	push   %ebx
  800c2b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c33:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c38:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c3b:	8b 55 08             	mov    0x8(%ebp),%edx
  800c3e:	89 df                	mov    %ebx,%edi
  800c40:	89 de                	mov    %ebx,%esi
  800c42:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c44:	85 c0                	test   %eax,%eax
  800c46:	7e 17                	jle    800c5f <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c48:	83 ec 0c             	sub    $0xc,%esp
  800c4b:	50                   	push   %eax
  800c4c:	6a 0a                	push   $0xa
  800c4e:	68 1f 13 80 00       	push   $0x80131f
  800c53:	6a 23                	push   $0x23
  800c55:	68 3c 13 80 00       	push   $0x80133c
  800c5a:	e8 eb 00 00 00       	call   800d4a <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800c5f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c62:	5b                   	pop    %ebx
  800c63:	5e                   	pop    %esi
  800c64:	5f                   	pop    %edi
  800c65:	5d                   	pop    %ebp
  800c66:	c3                   	ret    

00800c67 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800c67:	55                   	push   %ebp
  800c68:	89 e5                	mov    %esp,%ebp
  800c6a:	57                   	push   %edi
  800c6b:	56                   	push   %esi
  800c6c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c6d:	be 00 00 00 00       	mov    $0x0,%esi
  800c72:	b8 0d 00 00 00       	mov    $0xd,%eax
  800c77:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c7a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c7d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c80:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c83:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800c85:	5b                   	pop    %ebx
  800c86:	5e                   	pop    %esi
  800c87:	5f                   	pop    %edi
  800c88:	5d                   	pop    %ebp
  800c89:	c3                   	ret    

00800c8a <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800c8a:	55                   	push   %ebp
  800c8b:	89 e5                	mov    %esp,%ebp
  800c8d:	57                   	push   %edi
  800c8e:	56                   	push   %esi
  800c8f:	53                   	push   %ebx
  800c90:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c93:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c98:	b8 0e 00 00 00       	mov    $0xe,%eax
  800c9d:	8b 55 08             	mov    0x8(%ebp),%edx
  800ca0:	89 cb                	mov    %ecx,%ebx
  800ca2:	89 cf                	mov    %ecx,%edi
  800ca4:	89 ce                	mov    %ecx,%esi
  800ca6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ca8:	85 c0                	test   %eax,%eax
  800caa:	7e 17                	jle    800cc3 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cac:	83 ec 0c             	sub    $0xc,%esp
  800caf:	50                   	push   %eax
  800cb0:	6a 0e                	push   $0xe
  800cb2:	68 1f 13 80 00       	push   $0x80131f
  800cb7:	6a 23                	push   $0x23
  800cb9:	68 3c 13 80 00       	push   $0x80133c
  800cbe:	e8 87 00 00 00       	call   800d4a <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800cc3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cc6:	5b                   	pop    %ebx
  800cc7:	5e                   	pop    %esi
  800cc8:	5f                   	pop    %edi
  800cc9:	5d                   	pop    %ebp
  800cca:	c3                   	ret    

00800ccb <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  800ccb:	55                   	push   %ebp
  800ccc:	89 e5                	mov    %esp,%ebp
  800cce:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  800cd1:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  800cd8:	75 3e                	jne    800d18 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  800cda:	83 ec 04             	sub    $0x4,%esp
  800cdd:	6a 07                	push   $0x7
  800cdf:	68 00 f0 bf ee       	push   $0xeebff000
  800ce4:	6a 00                	push   $0x0
  800ce6:	e8 d0 fd ff ff       	call   800abb <sys_page_alloc>
  800ceb:	83 c4 10             	add    $0x10,%esp
  800cee:	85 c0                	test   %eax,%eax
  800cf0:	74 14                	je     800d06 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  800cf2:	83 ec 04             	sub    $0x4,%esp
  800cf5:	68 4c 13 80 00       	push   $0x80134c
  800cfa:	6a 23                	push   $0x23
  800cfc:	68 6d 13 80 00       	push   $0x80136d
  800d01:	e8 44 00 00 00       	call   800d4a <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  800d06:	83 ec 08             	sub    $0x8,%esp
  800d09:	68 22 0d 80 00       	push   $0x800d22
  800d0e:	6a 00                	push   $0x0
  800d10:	e8 10 ff ff ff       	call   800c25 <sys_env_set_pgfault_upcall>
  800d15:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  800d18:	8b 45 08             	mov    0x8(%ebp),%eax
  800d1b:	a3 08 20 80 00       	mov    %eax,0x802008
}
  800d20:	c9                   	leave  
  800d21:	c3                   	ret    

00800d22 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  800d22:	54                   	push   %esp
	movl _pgfault_handler, %eax
  800d23:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  800d28:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  800d2a:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  800d2d:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  800d2f:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  800d33:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  800d37:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  800d38:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  800d3b:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  800d3d:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  800d40:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  800d43:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  800d44:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  800d47:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  800d48:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  800d49:	c3                   	ret    

00800d4a <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800d4a:	55                   	push   %ebp
  800d4b:	89 e5                	mov    %esp,%ebp
  800d4d:	56                   	push   %esi
  800d4e:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800d4f:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800d52:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800d58:	e8 20 fd ff ff       	call   800a7d <sys_getenvid>
  800d5d:	83 ec 0c             	sub    $0xc,%esp
  800d60:	ff 75 0c             	pushl  0xc(%ebp)
  800d63:	ff 75 08             	pushl  0x8(%ebp)
  800d66:	56                   	push   %esi
  800d67:	50                   	push   %eax
  800d68:	68 7c 13 80 00       	push   $0x80137c
  800d6d:	e8 01 f4 ff ff       	call   800173 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800d72:	83 c4 18             	add    $0x18,%esp
  800d75:	53                   	push   %ebx
  800d76:	ff 75 10             	pushl  0x10(%ebp)
  800d79:	e8 a4 f3 ff ff       	call   800122 <vcprintf>
	cprintf("\n");
  800d7e:	c7 04 24 1a 10 80 00 	movl   $0x80101a,(%esp)
  800d85:	e8 e9 f3 ff ff       	call   800173 <cprintf>
  800d8a:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800d8d:	cc                   	int3   
  800d8e:	eb fd                	jmp    800d8d <_panic+0x43>

00800d90 <__udivdi3>:
  800d90:	55                   	push   %ebp
  800d91:	57                   	push   %edi
  800d92:	56                   	push   %esi
  800d93:	53                   	push   %ebx
  800d94:	83 ec 1c             	sub    $0x1c,%esp
  800d97:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d9b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d9f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800da3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800da7:	89 ca                	mov    %ecx,%edx
  800da9:	89 f8                	mov    %edi,%eax
  800dab:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800daf:	85 f6                	test   %esi,%esi
  800db1:	75 2d                	jne    800de0 <__udivdi3+0x50>
  800db3:	39 cf                	cmp    %ecx,%edi
  800db5:	77 65                	ja     800e1c <__udivdi3+0x8c>
  800db7:	89 fd                	mov    %edi,%ebp
  800db9:	85 ff                	test   %edi,%edi
  800dbb:	75 0b                	jne    800dc8 <__udivdi3+0x38>
  800dbd:	b8 01 00 00 00       	mov    $0x1,%eax
  800dc2:	31 d2                	xor    %edx,%edx
  800dc4:	f7 f7                	div    %edi
  800dc6:	89 c5                	mov    %eax,%ebp
  800dc8:	31 d2                	xor    %edx,%edx
  800dca:	89 c8                	mov    %ecx,%eax
  800dcc:	f7 f5                	div    %ebp
  800dce:	89 c1                	mov    %eax,%ecx
  800dd0:	89 d8                	mov    %ebx,%eax
  800dd2:	f7 f5                	div    %ebp
  800dd4:	89 cf                	mov    %ecx,%edi
  800dd6:	89 fa                	mov    %edi,%edx
  800dd8:	83 c4 1c             	add    $0x1c,%esp
  800ddb:	5b                   	pop    %ebx
  800ddc:	5e                   	pop    %esi
  800ddd:	5f                   	pop    %edi
  800dde:	5d                   	pop    %ebp
  800ddf:	c3                   	ret    
  800de0:	39 ce                	cmp    %ecx,%esi
  800de2:	77 28                	ja     800e0c <__udivdi3+0x7c>
  800de4:	0f bd fe             	bsr    %esi,%edi
  800de7:	83 f7 1f             	xor    $0x1f,%edi
  800dea:	75 40                	jne    800e2c <__udivdi3+0x9c>
  800dec:	39 ce                	cmp    %ecx,%esi
  800dee:	72 0a                	jb     800dfa <__udivdi3+0x6a>
  800df0:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800df4:	0f 87 9e 00 00 00    	ja     800e98 <__udivdi3+0x108>
  800dfa:	b8 01 00 00 00       	mov    $0x1,%eax
  800dff:	89 fa                	mov    %edi,%edx
  800e01:	83 c4 1c             	add    $0x1c,%esp
  800e04:	5b                   	pop    %ebx
  800e05:	5e                   	pop    %esi
  800e06:	5f                   	pop    %edi
  800e07:	5d                   	pop    %ebp
  800e08:	c3                   	ret    
  800e09:	8d 76 00             	lea    0x0(%esi),%esi
  800e0c:	31 ff                	xor    %edi,%edi
  800e0e:	31 c0                	xor    %eax,%eax
  800e10:	89 fa                	mov    %edi,%edx
  800e12:	83 c4 1c             	add    $0x1c,%esp
  800e15:	5b                   	pop    %ebx
  800e16:	5e                   	pop    %esi
  800e17:	5f                   	pop    %edi
  800e18:	5d                   	pop    %ebp
  800e19:	c3                   	ret    
  800e1a:	66 90                	xchg   %ax,%ax
  800e1c:	89 d8                	mov    %ebx,%eax
  800e1e:	f7 f7                	div    %edi
  800e20:	31 ff                	xor    %edi,%edi
  800e22:	89 fa                	mov    %edi,%edx
  800e24:	83 c4 1c             	add    $0x1c,%esp
  800e27:	5b                   	pop    %ebx
  800e28:	5e                   	pop    %esi
  800e29:	5f                   	pop    %edi
  800e2a:	5d                   	pop    %ebp
  800e2b:	c3                   	ret    
  800e2c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e31:	89 eb                	mov    %ebp,%ebx
  800e33:	29 fb                	sub    %edi,%ebx
  800e35:	89 f9                	mov    %edi,%ecx
  800e37:	d3 e6                	shl    %cl,%esi
  800e39:	89 c5                	mov    %eax,%ebp
  800e3b:	88 d9                	mov    %bl,%cl
  800e3d:	d3 ed                	shr    %cl,%ebp
  800e3f:	89 e9                	mov    %ebp,%ecx
  800e41:	09 f1                	or     %esi,%ecx
  800e43:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800e47:	89 f9                	mov    %edi,%ecx
  800e49:	d3 e0                	shl    %cl,%eax
  800e4b:	89 c5                	mov    %eax,%ebp
  800e4d:	89 d6                	mov    %edx,%esi
  800e4f:	88 d9                	mov    %bl,%cl
  800e51:	d3 ee                	shr    %cl,%esi
  800e53:	89 f9                	mov    %edi,%ecx
  800e55:	d3 e2                	shl    %cl,%edx
  800e57:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e5b:	88 d9                	mov    %bl,%cl
  800e5d:	d3 e8                	shr    %cl,%eax
  800e5f:	09 c2                	or     %eax,%edx
  800e61:	89 d0                	mov    %edx,%eax
  800e63:	89 f2                	mov    %esi,%edx
  800e65:	f7 74 24 0c          	divl   0xc(%esp)
  800e69:	89 d6                	mov    %edx,%esi
  800e6b:	89 c3                	mov    %eax,%ebx
  800e6d:	f7 e5                	mul    %ebp
  800e6f:	39 d6                	cmp    %edx,%esi
  800e71:	72 19                	jb     800e8c <__udivdi3+0xfc>
  800e73:	74 0b                	je     800e80 <__udivdi3+0xf0>
  800e75:	89 d8                	mov    %ebx,%eax
  800e77:	31 ff                	xor    %edi,%edi
  800e79:	e9 58 ff ff ff       	jmp    800dd6 <__udivdi3+0x46>
  800e7e:	66 90                	xchg   %ax,%ax
  800e80:	8b 54 24 08          	mov    0x8(%esp),%edx
  800e84:	89 f9                	mov    %edi,%ecx
  800e86:	d3 e2                	shl    %cl,%edx
  800e88:	39 c2                	cmp    %eax,%edx
  800e8a:	73 e9                	jae    800e75 <__udivdi3+0xe5>
  800e8c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800e8f:	31 ff                	xor    %edi,%edi
  800e91:	e9 40 ff ff ff       	jmp    800dd6 <__udivdi3+0x46>
  800e96:	66 90                	xchg   %ax,%ax
  800e98:	31 c0                	xor    %eax,%eax
  800e9a:	e9 37 ff ff ff       	jmp    800dd6 <__udivdi3+0x46>
  800e9f:	90                   	nop

00800ea0 <__umoddi3>:
  800ea0:	55                   	push   %ebp
  800ea1:	57                   	push   %edi
  800ea2:	56                   	push   %esi
  800ea3:	53                   	push   %ebx
  800ea4:	83 ec 1c             	sub    $0x1c,%esp
  800ea7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800eab:	8b 74 24 34          	mov    0x34(%esp),%esi
  800eaf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800eb3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800eb7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800ebb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800ebf:	89 f3                	mov    %esi,%ebx
  800ec1:	89 fa                	mov    %edi,%edx
  800ec3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ec7:	89 34 24             	mov    %esi,(%esp)
  800eca:	85 c0                	test   %eax,%eax
  800ecc:	75 1a                	jne    800ee8 <__umoddi3+0x48>
  800ece:	39 f7                	cmp    %esi,%edi
  800ed0:	0f 86 a2 00 00 00    	jbe    800f78 <__umoddi3+0xd8>
  800ed6:	89 c8                	mov    %ecx,%eax
  800ed8:	89 f2                	mov    %esi,%edx
  800eda:	f7 f7                	div    %edi
  800edc:	89 d0                	mov    %edx,%eax
  800ede:	31 d2                	xor    %edx,%edx
  800ee0:	83 c4 1c             	add    $0x1c,%esp
  800ee3:	5b                   	pop    %ebx
  800ee4:	5e                   	pop    %esi
  800ee5:	5f                   	pop    %edi
  800ee6:	5d                   	pop    %ebp
  800ee7:	c3                   	ret    
  800ee8:	39 f0                	cmp    %esi,%eax
  800eea:	0f 87 ac 00 00 00    	ja     800f9c <__umoddi3+0xfc>
  800ef0:	0f bd e8             	bsr    %eax,%ebp
  800ef3:	83 f5 1f             	xor    $0x1f,%ebp
  800ef6:	0f 84 ac 00 00 00    	je     800fa8 <__umoddi3+0x108>
  800efc:	bf 20 00 00 00       	mov    $0x20,%edi
  800f01:	29 ef                	sub    %ebp,%edi
  800f03:	89 fe                	mov    %edi,%esi
  800f05:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800f09:	89 e9                	mov    %ebp,%ecx
  800f0b:	d3 e0                	shl    %cl,%eax
  800f0d:	89 d7                	mov    %edx,%edi
  800f0f:	89 f1                	mov    %esi,%ecx
  800f11:	d3 ef                	shr    %cl,%edi
  800f13:	09 c7                	or     %eax,%edi
  800f15:	89 e9                	mov    %ebp,%ecx
  800f17:	d3 e2                	shl    %cl,%edx
  800f19:	89 14 24             	mov    %edx,(%esp)
  800f1c:	89 d8                	mov    %ebx,%eax
  800f1e:	d3 e0                	shl    %cl,%eax
  800f20:	89 c2                	mov    %eax,%edx
  800f22:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f26:	d3 e0                	shl    %cl,%eax
  800f28:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f2c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f30:	89 f1                	mov    %esi,%ecx
  800f32:	d3 e8                	shr    %cl,%eax
  800f34:	09 d0                	or     %edx,%eax
  800f36:	d3 eb                	shr    %cl,%ebx
  800f38:	89 da                	mov    %ebx,%edx
  800f3a:	f7 f7                	div    %edi
  800f3c:	89 d3                	mov    %edx,%ebx
  800f3e:	f7 24 24             	mull   (%esp)
  800f41:	89 c6                	mov    %eax,%esi
  800f43:	89 d1                	mov    %edx,%ecx
  800f45:	39 d3                	cmp    %edx,%ebx
  800f47:	0f 82 87 00 00 00    	jb     800fd4 <__umoddi3+0x134>
  800f4d:	0f 84 91 00 00 00    	je     800fe4 <__umoddi3+0x144>
  800f53:	8b 54 24 04          	mov    0x4(%esp),%edx
  800f57:	29 f2                	sub    %esi,%edx
  800f59:	19 cb                	sbb    %ecx,%ebx
  800f5b:	89 d8                	mov    %ebx,%eax
  800f5d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800f61:	d3 e0                	shl    %cl,%eax
  800f63:	89 e9                	mov    %ebp,%ecx
  800f65:	d3 ea                	shr    %cl,%edx
  800f67:	09 d0                	or     %edx,%eax
  800f69:	89 e9                	mov    %ebp,%ecx
  800f6b:	d3 eb                	shr    %cl,%ebx
  800f6d:	89 da                	mov    %ebx,%edx
  800f6f:	83 c4 1c             	add    $0x1c,%esp
  800f72:	5b                   	pop    %ebx
  800f73:	5e                   	pop    %esi
  800f74:	5f                   	pop    %edi
  800f75:	5d                   	pop    %ebp
  800f76:	c3                   	ret    
  800f77:	90                   	nop
  800f78:	89 fd                	mov    %edi,%ebp
  800f7a:	85 ff                	test   %edi,%edi
  800f7c:	75 0b                	jne    800f89 <__umoddi3+0xe9>
  800f7e:	b8 01 00 00 00       	mov    $0x1,%eax
  800f83:	31 d2                	xor    %edx,%edx
  800f85:	f7 f7                	div    %edi
  800f87:	89 c5                	mov    %eax,%ebp
  800f89:	89 f0                	mov    %esi,%eax
  800f8b:	31 d2                	xor    %edx,%edx
  800f8d:	f7 f5                	div    %ebp
  800f8f:	89 c8                	mov    %ecx,%eax
  800f91:	f7 f5                	div    %ebp
  800f93:	89 d0                	mov    %edx,%eax
  800f95:	e9 44 ff ff ff       	jmp    800ede <__umoddi3+0x3e>
  800f9a:	66 90                	xchg   %ax,%ax
  800f9c:	89 c8                	mov    %ecx,%eax
  800f9e:	89 f2                	mov    %esi,%edx
  800fa0:	83 c4 1c             	add    $0x1c,%esp
  800fa3:	5b                   	pop    %ebx
  800fa4:	5e                   	pop    %esi
  800fa5:	5f                   	pop    %edi
  800fa6:	5d                   	pop    %ebp
  800fa7:	c3                   	ret    
  800fa8:	3b 04 24             	cmp    (%esp),%eax
  800fab:	72 06                	jb     800fb3 <__umoddi3+0x113>
  800fad:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800fb1:	77 0f                	ja     800fc2 <__umoddi3+0x122>
  800fb3:	89 f2                	mov    %esi,%edx
  800fb5:	29 f9                	sub    %edi,%ecx
  800fb7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800fbb:	89 14 24             	mov    %edx,(%esp)
  800fbe:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800fc2:	8b 44 24 04          	mov    0x4(%esp),%eax
  800fc6:	8b 14 24             	mov    (%esp),%edx
  800fc9:	83 c4 1c             	add    $0x1c,%esp
  800fcc:	5b                   	pop    %ebx
  800fcd:	5e                   	pop    %esi
  800fce:	5f                   	pop    %edi
  800fcf:	5d                   	pop    %ebp
  800fd0:	c3                   	ret    
  800fd1:	8d 76 00             	lea    0x0(%esi),%esi
  800fd4:	2b 04 24             	sub    (%esp),%eax
  800fd7:	19 fa                	sbb    %edi,%edx
  800fd9:	89 d1                	mov    %edx,%ecx
  800fdb:	89 c6                	mov    %eax,%esi
  800fdd:	e9 71 ff ff ff       	jmp    800f53 <__umoddi3+0xb3>
  800fe2:	66 90                	xchg   %ax,%ax
  800fe4:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800fe8:	72 ea                	jb     800fd4 <__umoddi3+0x134>
  800fea:	89 d9                	mov    %ebx,%ecx
  800fec:	e9 62 ff ff ff       	jmp    800f53 <__umoddi3+0xb3>
