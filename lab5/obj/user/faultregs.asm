
obj/user/faultregs.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 31 05 00 00       	call   800562 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <check_regs>:
static struct regs before, during, after;

static void
check_regs(struct regs* a, const char *an, struct regs* b, const char *bn,
	   const char *testname)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 0c             	sub    $0xc,%esp
  80003c:	89 c6                	mov    %eax,%esi
  80003e:	89 cb                	mov    %ecx,%ebx
	int mismatch = 0;

	cprintf("%-6s %-8s %-8s\n", "", an, bn);
  800040:	ff 75 08             	pushl  0x8(%ebp)
  800043:	52                   	push   %edx
  800044:	68 11 15 80 00       	push   $0x801511
  800049:	68 e0 14 80 00       	push   $0x8014e0
  80004e:	e8 48 06 00 00       	call   80069b <cprintf>
			cprintf("MISMATCH\n");				\
			mismatch = 1;					\
		}							\
	} while (0)

	CHECK(edi, regs.reg_edi);
  800053:	ff 33                	pushl  (%ebx)
  800055:	ff 36                	pushl  (%esi)
  800057:	68 f0 14 80 00       	push   $0x8014f0
  80005c:	68 f4 14 80 00       	push   $0x8014f4
  800061:	e8 35 06 00 00       	call   80069b <cprintf>
  800066:	83 c4 20             	add    $0x20,%esp
  800069:	8b 03                	mov    (%ebx),%eax
  80006b:	39 06                	cmp    %eax,(%esi)
  80006d:	75 17                	jne    800086 <check_regs+0x53>
  80006f:	83 ec 0c             	sub    $0xc,%esp
  800072:	68 04 15 80 00       	push   $0x801504
  800077:	e8 1f 06 00 00       	call   80069b <cprintf>
  80007c:	83 c4 10             	add    $0x10,%esp

static void
check_regs(struct regs* a, const char *an, struct regs* b, const char *bn,
	   const char *testname)
{
	int mismatch = 0;
  80007f:	bf 00 00 00 00       	mov    $0x0,%edi
  800084:	eb 15                	jmp    80009b <check_regs+0x68>
			cprintf("MISMATCH\n");				\
			mismatch = 1;					\
		}							\
	} while (0)

	CHECK(edi, regs.reg_edi);
  800086:	83 ec 0c             	sub    $0xc,%esp
  800089:	68 08 15 80 00       	push   $0x801508
  80008e:	e8 08 06 00 00       	call   80069b <cprintf>
  800093:	83 c4 10             	add    $0x10,%esp
  800096:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(esi, regs.reg_esi);
  80009b:	ff 73 04             	pushl  0x4(%ebx)
  80009e:	ff 76 04             	pushl  0x4(%esi)
  8000a1:	68 12 15 80 00       	push   $0x801512
  8000a6:	68 f4 14 80 00       	push   $0x8014f4
  8000ab:	e8 eb 05 00 00       	call   80069b <cprintf>
  8000b0:	83 c4 10             	add    $0x10,%esp
  8000b3:	8b 43 04             	mov    0x4(%ebx),%eax
  8000b6:	39 46 04             	cmp    %eax,0x4(%esi)
  8000b9:	75 12                	jne    8000cd <check_regs+0x9a>
  8000bb:	83 ec 0c             	sub    $0xc,%esp
  8000be:	68 04 15 80 00       	push   $0x801504
  8000c3:	e8 d3 05 00 00       	call   80069b <cprintf>
  8000c8:	83 c4 10             	add    $0x10,%esp
  8000cb:	eb 15                	jmp    8000e2 <check_regs+0xaf>
  8000cd:	83 ec 0c             	sub    $0xc,%esp
  8000d0:	68 08 15 80 00       	push   $0x801508
  8000d5:	e8 c1 05 00 00       	call   80069b <cprintf>
  8000da:	83 c4 10             	add    $0x10,%esp
  8000dd:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(ebp, regs.reg_ebp);
  8000e2:	ff 73 08             	pushl  0x8(%ebx)
  8000e5:	ff 76 08             	pushl  0x8(%esi)
  8000e8:	68 16 15 80 00       	push   $0x801516
  8000ed:	68 f4 14 80 00       	push   $0x8014f4
  8000f2:	e8 a4 05 00 00       	call   80069b <cprintf>
  8000f7:	83 c4 10             	add    $0x10,%esp
  8000fa:	8b 43 08             	mov    0x8(%ebx),%eax
  8000fd:	39 46 08             	cmp    %eax,0x8(%esi)
  800100:	75 12                	jne    800114 <check_regs+0xe1>
  800102:	83 ec 0c             	sub    $0xc,%esp
  800105:	68 04 15 80 00       	push   $0x801504
  80010a:	e8 8c 05 00 00       	call   80069b <cprintf>
  80010f:	83 c4 10             	add    $0x10,%esp
  800112:	eb 15                	jmp    800129 <check_regs+0xf6>
  800114:	83 ec 0c             	sub    $0xc,%esp
  800117:	68 08 15 80 00       	push   $0x801508
  80011c:	e8 7a 05 00 00       	call   80069b <cprintf>
  800121:	83 c4 10             	add    $0x10,%esp
  800124:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(ebx, regs.reg_ebx);
  800129:	ff 73 10             	pushl  0x10(%ebx)
  80012c:	ff 76 10             	pushl  0x10(%esi)
  80012f:	68 1a 15 80 00       	push   $0x80151a
  800134:	68 f4 14 80 00       	push   $0x8014f4
  800139:	e8 5d 05 00 00       	call   80069b <cprintf>
  80013e:	83 c4 10             	add    $0x10,%esp
  800141:	8b 43 10             	mov    0x10(%ebx),%eax
  800144:	39 46 10             	cmp    %eax,0x10(%esi)
  800147:	75 12                	jne    80015b <check_regs+0x128>
  800149:	83 ec 0c             	sub    $0xc,%esp
  80014c:	68 04 15 80 00       	push   $0x801504
  800151:	e8 45 05 00 00       	call   80069b <cprintf>
  800156:	83 c4 10             	add    $0x10,%esp
  800159:	eb 15                	jmp    800170 <check_regs+0x13d>
  80015b:	83 ec 0c             	sub    $0xc,%esp
  80015e:	68 08 15 80 00       	push   $0x801508
  800163:	e8 33 05 00 00       	call   80069b <cprintf>
  800168:	83 c4 10             	add    $0x10,%esp
  80016b:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(edx, regs.reg_edx);
  800170:	ff 73 14             	pushl  0x14(%ebx)
  800173:	ff 76 14             	pushl  0x14(%esi)
  800176:	68 1e 15 80 00       	push   $0x80151e
  80017b:	68 f4 14 80 00       	push   $0x8014f4
  800180:	e8 16 05 00 00       	call   80069b <cprintf>
  800185:	83 c4 10             	add    $0x10,%esp
  800188:	8b 43 14             	mov    0x14(%ebx),%eax
  80018b:	39 46 14             	cmp    %eax,0x14(%esi)
  80018e:	75 12                	jne    8001a2 <check_regs+0x16f>
  800190:	83 ec 0c             	sub    $0xc,%esp
  800193:	68 04 15 80 00       	push   $0x801504
  800198:	e8 fe 04 00 00       	call   80069b <cprintf>
  80019d:	83 c4 10             	add    $0x10,%esp
  8001a0:	eb 15                	jmp    8001b7 <check_regs+0x184>
  8001a2:	83 ec 0c             	sub    $0xc,%esp
  8001a5:	68 08 15 80 00       	push   $0x801508
  8001aa:	e8 ec 04 00 00       	call   80069b <cprintf>
  8001af:	83 c4 10             	add    $0x10,%esp
  8001b2:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(ecx, regs.reg_ecx);
  8001b7:	ff 73 18             	pushl  0x18(%ebx)
  8001ba:	ff 76 18             	pushl  0x18(%esi)
  8001bd:	68 22 15 80 00       	push   $0x801522
  8001c2:	68 f4 14 80 00       	push   $0x8014f4
  8001c7:	e8 cf 04 00 00       	call   80069b <cprintf>
  8001cc:	83 c4 10             	add    $0x10,%esp
  8001cf:	8b 43 18             	mov    0x18(%ebx),%eax
  8001d2:	39 46 18             	cmp    %eax,0x18(%esi)
  8001d5:	75 12                	jne    8001e9 <check_regs+0x1b6>
  8001d7:	83 ec 0c             	sub    $0xc,%esp
  8001da:	68 04 15 80 00       	push   $0x801504
  8001df:	e8 b7 04 00 00       	call   80069b <cprintf>
  8001e4:	83 c4 10             	add    $0x10,%esp
  8001e7:	eb 15                	jmp    8001fe <check_regs+0x1cb>
  8001e9:	83 ec 0c             	sub    $0xc,%esp
  8001ec:	68 08 15 80 00       	push   $0x801508
  8001f1:	e8 a5 04 00 00       	call   80069b <cprintf>
  8001f6:	83 c4 10             	add    $0x10,%esp
  8001f9:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(eax, regs.reg_eax);
  8001fe:	ff 73 1c             	pushl  0x1c(%ebx)
  800201:	ff 76 1c             	pushl  0x1c(%esi)
  800204:	68 26 15 80 00       	push   $0x801526
  800209:	68 f4 14 80 00       	push   $0x8014f4
  80020e:	e8 88 04 00 00       	call   80069b <cprintf>
  800213:	83 c4 10             	add    $0x10,%esp
  800216:	8b 43 1c             	mov    0x1c(%ebx),%eax
  800219:	39 46 1c             	cmp    %eax,0x1c(%esi)
  80021c:	75 12                	jne    800230 <check_regs+0x1fd>
  80021e:	83 ec 0c             	sub    $0xc,%esp
  800221:	68 04 15 80 00       	push   $0x801504
  800226:	e8 70 04 00 00       	call   80069b <cprintf>
  80022b:	83 c4 10             	add    $0x10,%esp
  80022e:	eb 15                	jmp    800245 <check_regs+0x212>
  800230:	83 ec 0c             	sub    $0xc,%esp
  800233:	68 08 15 80 00       	push   $0x801508
  800238:	e8 5e 04 00 00       	call   80069b <cprintf>
  80023d:	83 c4 10             	add    $0x10,%esp
  800240:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(eip, eip);
  800245:	ff 73 20             	pushl  0x20(%ebx)
  800248:	ff 76 20             	pushl  0x20(%esi)
  80024b:	68 2a 15 80 00       	push   $0x80152a
  800250:	68 f4 14 80 00       	push   $0x8014f4
  800255:	e8 41 04 00 00       	call   80069b <cprintf>
  80025a:	83 c4 10             	add    $0x10,%esp
  80025d:	8b 43 20             	mov    0x20(%ebx),%eax
  800260:	39 46 20             	cmp    %eax,0x20(%esi)
  800263:	75 12                	jne    800277 <check_regs+0x244>
  800265:	83 ec 0c             	sub    $0xc,%esp
  800268:	68 04 15 80 00       	push   $0x801504
  80026d:	e8 29 04 00 00       	call   80069b <cprintf>
  800272:	83 c4 10             	add    $0x10,%esp
  800275:	eb 15                	jmp    80028c <check_regs+0x259>
  800277:	83 ec 0c             	sub    $0xc,%esp
  80027a:	68 08 15 80 00       	push   $0x801508
  80027f:	e8 17 04 00 00       	call   80069b <cprintf>
  800284:	83 c4 10             	add    $0x10,%esp
  800287:	bf 01 00 00 00       	mov    $0x1,%edi
	CHECK(eflags, eflags);
  80028c:	ff 73 24             	pushl  0x24(%ebx)
  80028f:	ff 76 24             	pushl  0x24(%esi)
  800292:	68 2e 15 80 00       	push   $0x80152e
  800297:	68 f4 14 80 00       	push   $0x8014f4
  80029c:	e8 fa 03 00 00       	call   80069b <cprintf>
  8002a1:	83 c4 10             	add    $0x10,%esp
  8002a4:	8b 43 24             	mov    0x24(%ebx),%eax
  8002a7:	39 46 24             	cmp    %eax,0x24(%esi)
  8002aa:	75 2f                	jne    8002db <check_regs+0x2a8>
  8002ac:	83 ec 0c             	sub    $0xc,%esp
  8002af:	68 04 15 80 00       	push   $0x801504
  8002b4:	e8 e2 03 00 00       	call   80069b <cprintf>
	CHECK(esp, esp);
  8002b9:	ff 73 28             	pushl  0x28(%ebx)
  8002bc:	ff 76 28             	pushl  0x28(%esi)
  8002bf:	68 35 15 80 00       	push   $0x801535
  8002c4:	68 f4 14 80 00       	push   $0x8014f4
  8002c9:	e8 cd 03 00 00       	call   80069b <cprintf>
  8002ce:	83 c4 20             	add    $0x20,%esp
  8002d1:	8b 43 28             	mov    0x28(%ebx),%eax
  8002d4:	39 46 28             	cmp    %eax,0x28(%esi)
  8002d7:	74 31                	je     80030a <check_regs+0x2d7>
  8002d9:	eb 55                	jmp    800330 <check_regs+0x2fd>
	CHECK(ebx, regs.reg_ebx);
	CHECK(edx, regs.reg_edx);
	CHECK(ecx, regs.reg_ecx);
	CHECK(eax, regs.reg_eax);
	CHECK(eip, eip);
	CHECK(eflags, eflags);
  8002db:	83 ec 0c             	sub    $0xc,%esp
  8002de:	68 08 15 80 00       	push   $0x801508
  8002e3:	e8 b3 03 00 00       	call   80069b <cprintf>
	CHECK(esp, esp);
  8002e8:	ff 73 28             	pushl  0x28(%ebx)
  8002eb:	ff 76 28             	pushl  0x28(%esi)
  8002ee:	68 35 15 80 00       	push   $0x801535
  8002f3:	68 f4 14 80 00       	push   $0x8014f4
  8002f8:	e8 9e 03 00 00       	call   80069b <cprintf>
  8002fd:	83 c4 20             	add    $0x20,%esp
  800300:	8b 43 28             	mov    0x28(%ebx),%eax
  800303:	39 46 28             	cmp    %eax,0x28(%esi)
  800306:	75 28                	jne    800330 <check_regs+0x2fd>
  800308:	eb 6c                	jmp    800376 <check_regs+0x343>
  80030a:	83 ec 0c             	sub    $0xc,%esp
  80030d:	68 04 15 80 00       	push   $0x801504
  800312:	e8 84 03 00 00       	call   80069b <cprintf>

#undef CHECK

	cprintf("Registers %s ", testname);
  800317:	83 c4 08             	add    $0x8,%esp
  80031a:	ff 75 0c             	pushl  0xc(%ebp)
  80031d:	68 39 15 80 00       	push   $0x801539
  800322:	e8 74 03 00 00       	call   80069b <cprintf>
	if (!mismatch)
  800327:	83 c4 10             	add    $0x10,%esp
  80032a:	85 ff                	test   %edi,%edi
  80032c:	74 24                	je     800352 <check_regs+0x31f>
  80032e:	eb 34                	jmp    800364 <check_regs+0x331>
	CHECK(edx, regs.reg_edx);
	CHECK(ecx, regs.reg_ecx);
	CHECK(eax, regs.reg_eax);
	CHECK(eip, eip);
	CHECK(eflags, eflags);
	CHECK(esp, esp);
  800330:	83 ec 0c             	sub    $0xc,%esp
  800333:	68 08 15 80 00       	push   $0x801508
  800338:	e8 5e 03 00 00       	call   80069b <cprintf>

#undef CHECK

	cprintf("Registers %s ", testname);
  80033d:	83 c4 08             	add    $0x8,%esp
  800340:	ff 75 0c             	pushl  0xc(%ebp)
  800343:	68 39 15 80 00       	push   $0x801539
  800348:	e8 4e 03 00 00       	call   80069b <cprintf>
  80034d:	83 c4 10             	add    $0x10,%esp
  800350:	eb 12                	jmp    800364 <check_regs+0x331>
	if (!mismatch)
		cprintf("OK\n");
  800352:	83 ec 0c             	sub    $0xc,%esp
  800355:	68 04 15 80 00       	push   $0x801504
  80035a:	e8 3c 03 00 00       	call   80069b <cprintf>
  80035f:	83 c4 10             	add    $0x10,%esp
  800362:	eb 34                	jmp    800398 <check_regs+0x365>
	else
		cprintf("MISMATCH\n");
  800364:	83 ec 0c             	sub    $0xc,%esp
  800367:	68 08 15 80 00       	push   $0x801508
  80036c:	e8 2a 03 00 00       	call   80069b <cprintf>
  800371:	83 c4 10             	add    $0x10,%esp
}
  800374:	eb 22                	jmp    800398 <check_regs+0x365>
	CHECK(edx, regs.reg_edx);
	CHECK(ecx, regs.reg_ecx);
	CHECK(eax, regs.reg_eax);
	CHECK(eip, eip);
	CHECK(eflags, eflags);
	CHECK(esp, esp);
  800376:	83 ec 0c             	sub    $0xc,%esp
  800379:	68 04 15 80 00       	push   $0x801504
  80037e:	e8 18 03 00 00       	call   80069b <cprintf>

#undef CHECK

	cprintf("Registers %s ", testname);
  800383:	83 c4 08             	add    $0x8,%esp
  800386:	ff 75 0c             	pushl  0xc(%ebp)
  800389:	68 39 15 80 00       	push   $0x801539
  80038e:	e8 08 03 00 00       	call   80069b <cprintf>
  800393:	83 c4 10             	add    $0x10,%esp
  800396:	eb cc                	jmp    800364 <check_regs+0x331>
	if (!mismatch)
		cprintf("OK\n");
	else
		cprintf("MISMATCH\n");
}
  800398:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80039b:	5b                   	pop    %ebx
  80039c:	5e                   	pop    %esi
  80039d:	5f                   	pop    %edi
  80039e:	5d                   	pop    %ebp
  80039f:	c3                   	ret    

008003a0 <pgfault>:

static void
pgfault(struct UTrapframe *utf)
{
  8003a0:	55                   	push   %ebp
  8003a1:	89 e5                	mov    %esp,%ebp
  8003a3:	57                   	push   %edi
  8003a4:	56                   	push   %esi
  8003a5:	8b 45 08             	mov    0x8(%ebp),%eax
	int r;

	if (utf->utf_fault_va != (uint32_t)UTEMP)
  8003a8:	8b 10                	mov    (%eax),%edx
  8003aa:	81 fa 00 00 40 00    	cmp    $0x400000,%edx
  8003b0:	74 18                	je     8003ca <pgfault+0x2a>
		panic("pgfault expected at UTEMP, got 0x%08x (eip %08x)",
  8003b2:	83 ec 0c             	sub    $0xc,%esp
  8003b5:	ff 70 28             	pushl  0x28(%eax)
  8003b8:	52                   	push   %edx
  8003b9:	68 a0 15 80 00       	push   $0x8015a0
  8003be:	6a 51                	push   $0x51
  8003c0:	68 47 15 80 00       	push   $0x801547
  8003c5:	e8 f9 01 00 00       	call   8005c3 <_panic>
		      utf->utf_fault_va, utf->utf_eip);

	// Check registers in UTrapframe
	during.regs = utf->utf_regs;
  8003ca:	bf 60 20 80 00       	mov    $0x802060,%edi
  8003cf:	8d 70 08             	lea    0x8(%eax),%esi
  8003d2:	b9 08 00 00 00       	mov    $0x8,%ecx
  8003d7:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	during.eip = utf->utf_eip;
  8003d9:	8b 50 28             	mov    0x28(%eax),%edx
  8003dc:	89 15 80 20 80 00    	mov    %edx,0x802080
	during.eflags = utf->utf_eflags & ~FL_RF;
  8003e2:	8b 50 2c             	mov    0x2c(%eax),%edx
  8003e5:	81 e2 ff ff fe ff    	and    $0xfffeffff,%edx
  8003eb:	89 15 84 20 80 00    	mov    %edx,0x802084
	during.esp = utf->utf_esp;
  8003f1:	8b 40 30             	mov    0x30(%eax),%eax
  8003f4:	a3 88 20 80 00       	mov    %eax,0x802088
	check_regs(&before, "before", &during, "during", "in UTrapframe");
  8003f9:	83 ec 08             	sub    $0x8,%esp
  8003fc:	68 5f 15 80 00       	push   $0x80155f
  800401:	68 6d 15 80 00       	push   $0x80156d
  800406:	b9 60 20 80 00       	mov    $0x802060,%ecx
  80040b:	ba 58 15 80 00       	mov    $0x801558,%edx
  800410:	b8 a0 20 80 00       	mov    $0x8020a0,%eax
  800415:	e8 19 fc ff ff       	call   800033 <check_regs>

	// Map UTEMP so the write succeeds
	if ((r = sys_page_alloc(0, UTEMP, PTE_U|PTE_P|PTE_W)) < 0)
  80041a:	83 c4 0c             	add    $0xc,%esp
  80041d:	6a 07                	push   $0x7
  80041f:	68 00 00 40 00       	push   $0x400000
  800424:	6a 00                	push   $0x0
  800426:	e8 b8 0b 00 00       	call   800fe3 <sys_page_alloc>
  80042b:	83 c4 10             	add    $0x10,%esp
  80042e:	85 c0                	test   %eax,%eax
  800430:	79 12                	jns    800444 <pgfault+0xa4>
		panic("sys_page_alloc: %e", r);
  800432:	50                   	push   %eax
  800433:	68 74 15 80 00       	push   $0x801574
  800438:	6a 5c                	push   $0x5c
  80043a:	68 47 15 80 00       	push   $0x801547
  80043f:	e8 7f 01 00 00       	call   8005c3 <_panic>
}
  800444:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800447:	5e                   	pop    %esi
  800448:	5f                   	pop    %edi
  800449:	5d                   	pop    %ebp
  80044a:	c3                   	ret    

0080044b <umain>:

void
umain(int argc, char **argv)
{
  80044b:	55                   	push   %ebp
  80044c:	89 e5                	mov    %esp,%ebp
  80044e:	83 ec 14             	sub    $0x14,%esp
	set_pgfault_handler(pgfault);
  800451:	68 a0 03 80 00       	push   $0x8003a0
  800456:	e8 98 0d 00 00       	call   8011f3 <set_pgfault_handler>

	asm volatile(
  80045b:	50                   	push   %eax
  80045c:	9c                   	pushf  
  80045d:	58                   	pop    %eax
  80045e:	0d d5 08 00 00       	or     $0x8d5,%eax
  800463:	50                   	push   %eax
  800464:	9d                   	popf   
  800465:	a3 c4 20 80 00       	mov    %eax,0x8020c4
  80046a:	8d 05 a5 04 80 00    	lea    0x8004a5,%eax
  800470:	a3 c0 20 80 00       	mov    %eax,0x8020c0
  800475:	58                   	pop    %eax
  800476:	89 3d a0 20 80 00    	mov    %edi,0x8020a0
  80047c:	89 35 a4 20 80 00    	mov    %esi,0x8020a4
  800482:	89 2d a8 20 80 00    	mov    %ebp,0x8020a8
  800488:	89 1d b0 20 80 00    	mov    %ebx,0x8020b0
  80048e:	89 15 b4 20 80 00    	mov    %edx,0x8020b4
  800494:	89 0d b8 20 80 00    	mov    %ecx,0x8020b8
  80049a:	a3 bc 20 80 00       	mov    %eax,0x8020bc
  80049f:	89 25 c8 20 80 00    	mov    %esp,0x8020c8
  8004a5:	c7 05 00 00 40 00 2a 	movl   $0x2a,0x400000
  8004ac:	00 00 00 
  8004af:	89 3d 20 20 80 00    	mov    %edi,0x802020
  8004b5:	89 35 24 20 80 00    	mov    %esi,0x802024
  8004bb:	89 2d 28 20 80 00    	mov    %ebp,0x802028
  8004c1:	89 1d 30 20 80 00    	mov    %ebx,0x802030
  8004c7:	89 15 34 20 80 00    	mov    %edx,0x802034
  8004cd:	89 0d 38 20 80 00    	mov    %ecx,0x802038
  8004d3:	a3 3c 20 80 00       	mov    %eax,0x80203c
  8004d8:	89 25 48 20 80 00    	mov    %esp,0x802048
  8004de:	8b 3d a0 20 80 00    	mov    0x8020a0,%edi
  8004e4:	8b 35 a4 20 80 00    	mov    0x8020a4,%esi
  8004ea:	8b 2d a8 20 80 00    	mov    0x8020a8,%ebp
  8004f0:	8b 1d b0 20 80 00    	mov    0x8020b0,%ebx
  8004f6:	8b 15 b4 20 80 00    	mov    0x8020b4,%edx
  8004fc:	8b 0d b8 20 80 00    	mov    0x8020b8,%ecx
  800502:	a1 bc 20 80 00       	mov    0x8020bc,%eax
  800507:	8b 25 c8 20 80 00    	mov    0x8020c8,%esp
  80050d:	50                   	push   %eax
  80050e:	9c                   	pushf  
  80050f:	58                   	pop    %eax
  800510:	a3 44 20 80 00       	mov    %eax,0x802044
  800515:	58                   	pop    %eax
		: : "m" (before), "m" (after) : "memory", "cc");

	// Check UTEMP to roughly determine that EIP was restored
	// correctly (of course, we probably wouldn't get this far if
	// it weren't)
	if (*(int*)UTEMP != 42)
  800516:	83 c4 10             	add    $0x10,%esp
  800519:	83 3d 00 00 40 00 2a 	cmpl   $0x2a,0x400000
  800520:	74 10                	je     800532 <umain+0xe7>
		cprintf("EIP after page-fault MISMATCH\n");
  800522:	83 ec 0c             	sub    $0xc,%esp
  800525:	68 d4 15 80 00       	push   $0x8015d4
  80052a:	e8 6c 01 00 00       	call   80069b <cprintf>
  80052f:	83 c4 10             	add    $0x10,%esp
	after.eip = before.eip;
  800532:	a1 c0 20 80 00       	mov    0x8020c0,%eax
  800537:	a3 40 20 80 00       	mov    %eax,0x802040

	check_regs(&before, "before", &after, "after", "after page-fault");
  80053c:	83 ec 08             	sub    $0x8,%esp
  80053f:	68 87 15 80 00       	push   $0x801587
  800544:	68 98 15 80 00       	push   $0x801598
  800549:	b9 20 20 80 00       	mov    $0x802020,%ecx
  80054e:	ba 58 15 80 00       	mov    $0x801558,%edx
  800553:	b8 a0 20 80 00       	mov    $0x8020a0,%eax
  800558:	e8 d6 fa ff ff       	call   800033 <check_regs>
}
  80055d:	83 c4 10             	add    $0x10,%esp
  800560:	c9                   	leave  
  800561:	c3                   	ret    

00800562 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800562:	55                   	push   %ebp
  800563:	89 e5                	mov    %esp,%ebp
  800565:	56                   	push   %esi
  800566:	53                   	push   %ebx
  800567:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80056a:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80056d:	e8 33 0a 00 00       	call   800fa5 <sys_getenvid>
  800572:	25 ff 03 00 00       	and    $0x3ff,%eax
  800577:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80057e:	c1 e0 07             	shl    $0x7,%eax
  800581:	29 d0                	sub    %edx,%eax
  800583:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800588:	a3 cc 20 80 00       	mov    %eax,0x8020cc
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80058d:	85 db                	test   %ebx,%ebx
  80058f:	7e 07                	jle    800598 <libmain+0x36>
		binaryname = argv[0];
  800591:	8b 06                	mov    (%esi),%eax
  800593:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800598:	83 ec 08             	sub    $0x8,%esp
  80059b:	56                   	push   %esi
  80059c:	53                   	push   %ebx
  80059d:	e8 a9 fe ff ff       	call   80044b <umain>

	// exit gracefully
	exit();
  8005a2:	e8 0a 00 00 00       	call   8005b1 <exit>
}
  8005a7:	83 c4 10             	add    $0x10,%esp
  8005aa:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8005ad:	5b                   	pop    %ebx
  8005ae:	5e                   	pop    %esi
  8005af:	5d                   	pop    %ebp
  8005b0:	c3                   	ret    

008005b1 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8005b1:	55                   	push   %ebp
  8005b2:	89 e5                	mov    %esp,%ebp
  8005b4:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8005b7:	6a 00                	push   $0x0
  8005b9:	e8 a6 09 00 00       	call   800f64 <sys_env_destroy>
}
  8005be:	83 c4 10             	add    $0x10,%esp
  8005c1:	c9                   	leave  
  8005c2:	c3                   	ret    

008005c3 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8005c3:	55                   	push   %ebp
  8005c4:	89 e5                	mov    %esp,%ebp
  8005c6:	56                   	push   %esi
  8005c7:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8005c8:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8005cb:	8b 35 00 20 80 00    	mov    0x802000,%esi
  8005d1:	e8 cf 09 00 00       	call   800fa5 <sys_getenvid>
  8005d6:	83 ec 0c             	sub    $0xc,%esp
  8005d9:	ff 75 0c             	pushl  0xc(%ebp)
  8005dc:	ff 75 08             	pushl  0x8(%ebp)
  8005df:	56                   	push   %esi
  8005e0:	50                   	push   %eax
  8005e1:	68 00 16 80 00       	push   $0x801600
  8005e6:	e8 b0 00 00 00       	call   80069b <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8005eb:	83 c4 18             	add    $0x18,%esp
  8005ee:	53                   	push   %ebx
  8005ef:	ff 75 10             	pushl  0x10(%ebp)
  8005f2:	e8 53 00 00 00       	call   80064a <vcprintf>
	cprintf("\n");
  8005f7:	c7 04 24 10 15 80 00 	movl   $0x801510,(%esp)
  8005fe:	e8 98 00 00 00       	call   80069b <cprintf>
  800603:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800606:	cc                   	int3   
  800607:	eb fd                	jmp    800606 <_panic+0x43>

00800609 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800609:	55                   	push   %ebp
  80060a:	89 e5                	mov    %esp,%ebp
  80060c:	53                   	push   %ebx
  80060d:	83 ec 04             	sub    $0x4,%esp
  800610:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800613:	8b 13                	mov    (%ebx),%edx
  800615:	8d 42 01             	lea    0x1(%edx),%eax
  800618:	89 03                	mov    %eax,(%ebx)
  80061a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80061d:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800621:	3d ff 00 00 00       	cmp    $0xff,%eax
  800626:	75 1a                	jne    800642 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800628:	83 ec 08             	sub    $0x8,%esp
  80062b:	68 ff 00 00 00       	push   $0xff
  800630:	8d 43 08             	lea    0x8(%ebx),%eax
  800633:	50                   	push   %eax
  800634:	e8 ee 08 00 00       	call   800f27 <sys_cputs>
		b->idx = 0;
  800639:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80063f:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800642:	ff 43 04             	incl   0x4(%ebx)
}
  800645:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800648:	c9                   	leave  
  800649:	c3                   	ret    

0080064a <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80064a:	55                   	push   %ebp
  80064b:	89 e5                	mov    %esp,%ebp
  80064d:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800653:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80065a:	00 00 00 
	b.cnt = 0;
  80065d:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800664:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800667:	ff 75 0c             	pushl  0xc(%ebp)
  80066a:	ff 75 08             	pushl  0x8(%ebp)
  80066d:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800673:	50                   	push   %eax
  800674:	68 09 06 80 00       	push   $0x800609
  800679:	e8 51 01 00 00       	call   8007cf <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80067e:	83 c4 08             	add    $0x8,%esp
  800681:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800687:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80068d:	50                   	push   %eax
  80068e:	e8 94 08 00 00       	call   800f27 <sys_cputs>

	return b.cnt;
}
  800693:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800699:	c9                   	leave  
  80069a:	c3                   	ret    

0080069b <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80069b:	55                   	push   %ebp
  80069c:	89 e5                	mov    %esp,%ebp
  80069e:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8006a1:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8006a4:	50                   	push   %eax
  8006a5:	ff 75 08             	pushl  0x8(%ebp)
  8006a8:	e8 9d ff ff ff       	call   80064a <vcprintf>
	va_end(ap);

	return cnt;
}
  8006ad:	c9                   	leave  
  8006ae:	c3                   	ret    

008006af <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8006af:	55                   	push   %ebp
  8006b0:	89 e5                	mov    %esp,%ebp
  8006b2:	57                   	push   %edi
  8006b3:	56                   	push   %esi
  8006b4:	53                   	push   %ebx
  8006b5:	83 ec 1c             	sub    $0x1c,%esp
  8006b8:	89 c7                	mov    %eax,%edi
  8006ba:	89 d6                	mov    %edx,%esi
  8006bc:	8b 45 08             	mov    0x8(%ebp),%eax
  8006bf:	8b 55 0c             	mov    0xc(%ebp),%edx
  8006c2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006c5:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8006c8:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8006cb:	bb 00 00 00 00       	mov    $0x0,%ebx
  8006d0:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8006d3:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8006d6:	39 d3                	cmp    %edx,%ebx
  8006d8:	72 05                	jb     8006df <printnum+0x30>
  8006da:	39 45 10             	cmp    %eax,0x10(%ebp)
  8006dd:	77 45                	ja     800724 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8006df:	83 ec 0c             	sub    $0xc,%esp
  8006e2:	ff 75 18             	pushl  0x18(%ebp)
  8006e5:	8b 45 14             	mov    0x14(%ebp),%eax
  8006e8:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8006eb:	53                   	push   %ebx
  8006ec:	ff 75 10             	pushl  0x10(%ebp)
  8006ef:	83 ec 08             	sub    $0x8,%esp
  8006f2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006f5:	ff 75 e0             	pushl  -0x20(%ebp)
  8006f8:	ff 75 dc             	pushl  -0x24(%ebp)
  8006fb:	ff 75 d8             	pushl  -0x28(%ebp)
  8006fe:	e8 71 0b 00 00       	call   801274 <__udivdi3>
  800703:	83 c4 18             	add    $0x18,%esp
  800706:	52                   	push   %edx
  800707:	50                   	push   %eax
  800708:	89 f2                	mov    %esi,%edx
  80070a:	89 f8                	mov    %edi,%eax
  80070c:	e8 9e ff ff ff       	call   8006af <printnum>
  800711:	83 c4 20             	add    $0x20,%esp
  800714:	eb 16                	jmp    80072c <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800716:	83 ec 08             	sub    $0x8,%esp
  800719:	56                   	push   %esi
  80071a:	ff 75 18             	pushl  0x18(%ebp)
  80071d:	ff d7                	call   *%edi
  80071f:	83 c4 10             	add    $0x10,%esp
  800722:	eb 03                	jmp    800727 <printnum+0x78>
  800724:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800727:	4b                   	dec    %ebx
  800728:	85 db                	test   %ebx,%ebx
  80072a:	7f ea                	jg     800716 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80072c:	83 ec 08             	sub    $0x8,%esp
  80072f:	56                   	push   %esi
  800730:	83 ec 04             	sub    $0x4,%esp
  800733:	ff 75 e4             	pushl  -0x1c(%ebp)
  800736:	ff 75 e0             	pushl  -0x20(%ebp)
  800739:	ff 75 dc             	pushl  -0x24(%ebp)
  80073c:	ff 75 d8             	pushl  -0x28(%ebp)
  80073f:	e8 40 0c 00 00       	call   801384 <__umoddi3>
  800744:	83 c4 14             	add    $0x14,%esp
  800747:	0f be 80 23 16 80 00 	movsbl 0x801623(%eax),%eax
  80074e:	50                   	push   %eax
  80074f:	ff d7                	call   *%edi
}
  800751:	83 c4 10             	add    $0x10,%esp
  800754:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800757:	5b                   	pop    %ebx
  800758:	5e                   	pop    %esi
  800759:	5f                   	pop    %edi
  80075a:	5d                   	pop    %ebp
  80075b:	c3                   	ret    

0080075c <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80075c:	55                   	push   %ebp
  80075d:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80075f:	83 fa 01             	cmp    $0x1,%edx
  800762:	7e 0e                	jle    800772 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800764:	8b 10                	mov    (%eax),%edx
  800766:	8d 4a 08             	lea    0x8(%edx),%ecx
  800769:	89 08                	mov    %ecx,(%eax)
  80076b:	8b 02                	mov    (%edx),%eax
  80076d:	8b 52 04             	mov    0x4(%edx),%edx
  800770:	eb 22                	jmp    800794 <getuint+0x38>
	else if (lflag)
  800772:	85 d2                	test   %edx,%edx
  800774:	74 10                	je     800786 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800776:	8b 10                	mov    (%eax),%edx
  800778:	8d 4a 04             	lea    0x4(%edx),%ecx
  80077b:	89 08                	mov    %ecx,(%eax)
  80077d:	8b 02                	mov    (%edx),%eax
  80077f:	ba 00 00 00 00       	mov    $0x0,%edx
  800784:	eb 0e                	jmp    800794 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800786:	8b 10                	mov    (%eax),%edx
  800788:	8d 4a 04             	lea    0x4(%edx),%ecx
  80078b:	89 08                	mov    %ecx,(%eax)
  80078d:	8b 02                	mov    (%edx),%eax
  80078f:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800794:	5d                   	pop    %ebp
  800795:	c3                   	ret    

00800796 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800796:	55                   	push   %ebp
  800797:	89 e5                	mov    %esp,%ebp
  800799:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80079c:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80079f:	8b 10                	mov    (%eax),%edx
  8007a1:	3b 50 04             	cmp    0x4(%eax),%edx
  8007a4:	73 0a                	jae    8007b0 <sprintputch+0x1a>
		*b->buf++ = ch;
  8007a6:	8d 4a 01             	lea    0x1(%edx),%ecx
  8007a9:	89 08                	mov    %ecx,(%eax)
  8007ab:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ae:	88 02                	mov    %al,(%edx)
}
  8007b0:	5d                   	pop    %ebp
  8007b1:	c3                   	ret    

008007b2 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8007b2:	55                   	push   %ebp
  8007b3:	89 e5                	mov    %esp,%ebp
  8007b5:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8007b8:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8007bb:	50                   	push   %eax
  8007bc:	ff 75 10             	pushl  0x10(%ebp)
  8007bf:	ff 75 0c             	pushl  0xc(%ebp)
  8007c2:	ff 75 08             	pushl  0x8(%ebp)
  8007c5:	e8 05 00 00 00       	call   8007cf <vprintfmt>
	va_end(ap);
}
  8007ca:	83 c4 10             	add    $0x10,%esp
  8007cd:	c9                   	leave  
  8007ce:	c3                   	ret    

008007cf <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8007cf:	55                   	push   %ebp
  8007d0:	89 e5                	mov    %esp,%ebp
  8007d2:	57                   	push   %edi
  8007d3:	56                   	push   %esi
  8007d4:	53                   	push   %ebx
  8007d5:	83 ec 2c             	sub    $0x2c,%esp
  8007d8:	8b 75 08             	mov    0x8(%ebp),%esi
  8007db:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8007de:	8b 7d 10             	mov    0x10(%ebp),%edi
  8007e1:	eb 12                	jmp    8007f5 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8007e3:	85 c0                	test   %eax,%eax
  8007e5:	0f 84 68 03 00 00    	je     800b53 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8007eb:	83 ec 08             	sub    $0x8,%esp
  8007ee:	53                   	push   %ebx
  8007ef:	50                   	push   %eax
  8007f0:	ff d6                	call   *%esi
  8007f2:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8007f5:	47                   	inc    %edi
  8007f6:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8007fa:	83 f8 25             	cmp    $0x25,%eax
  8007fd:	75 e4                	jne    8007e3 <vprintfmt+0x14>
  8007ff:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800803:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80080a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800811:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800818:	ba 00 00 00 00       	mov    $0x0,%edx
  80081d:	eb 07                	jmp    800826 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80081f:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800822:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800826:	8d 47 01             	lea    0x1(%edi),%eax
  800829:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80082c:	0f b6 0f             	movzbl (%edi),%ecx
  80082f:	8a 07                	mov    (%edi),%al
  800831:	83 e8 23             	sub    $0x23,%eax
  800834:	3c 55                	cmp    $0x55,%al
  800836:	0f 87 fe 02 00 00    	ja     800b3a <vprintfmt+0x36b>
  80083c:	0f b6 c0             	movzbl %al,%eax
  80083f:	ff 24 85 60 17 80 00 	jmp    *0x801760(,%eax,4)
  800846:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800849:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80084d:	eb d7                	jmp    800826 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80084f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800852:	b8 00 00 00 00       	mov    $0x0,%eax
  800857:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80085a:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80085d:	01 c0                	add    %eax,%eax
  80085f:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800863:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800866:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800869:	83 fa 09             	cmp    $0x9,%edx
  80086c:	77 34                	ja     8008a2 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80086e:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80086f:	eb e9                	jmp    80085a <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800871:	8b 45 14             	mov    0x14(%ebp),%eax
  800874:	8d 48 04             	lea    0x4(%eax),%ecx
  800877:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80087a:	8b 00                	mov    (%eax),%eax
  80087c:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80087f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800882:	eb 24                	jmp    8008a8 <vprintfmt+0xd9>
  800884:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800888:	79 07                	jns    800891 <vprintfmt+0xc2>
  80088a:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800891:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800894:	eb 90                	jmp    800826 <vprintfmt+0x57>
  800896:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800899:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8008a0:	eb 84                	jmp    800826 <vprintfmt+0x57>
  8008a2:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8008a5:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8008a8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8008ac:	0f 89 74 ff ff ff    	jns    800826 <vprintfmt+0x57>
				width = precision, precision = -1;
  8008b2:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8008b5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8008b8:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8008bf:	e9 62 ff ff ff       	jmp    800826 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8008c4:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008c5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8008c8:	e9 59 ff ff ff       	jmp    800826 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8008cd:	8b 45 14             	mov    0x14(%ebp),%eax
  8008d0:	8d 50 04             	lea    0x4(%eax),%edx
  8008d3:	89 55 14             	mov    %edx,0x14(%ebp)
  8008d6:	83 ec 08             	sub    $0x8,%esp
  8008d9:	53                   	push   %ebx
  8008da:	ff 30                	pushl  (%eax)
  8008dc:	ff d6                	call   *%esi
			break;
  8008de:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008e1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8008e4:	e9 0c ff ff ff       	jmp    8007f5 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8008e9:	8b 45 14             	mov    0x14(%ebp),%eax
  8008ec:	8d 50 04             	lea    0x4(%eax),%edx
  8008ef:	89 55 14             	mov    %edx,0x14(%ebp)
  8008f2:	8b 00                	mov    (%eax),%eax
  8008f4:	85 c0                	test   %eax,%eax
  8008f6:	79 02                	jns    8008fa <vprintfmt+0x12b>
  8008f8:	f7 d8                	neg    %eax
  8008fa:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8008fc:	83 f8 0f             	cmp    $0xf,%eax
  8008ff:	7f 0b                	jg     80090c <vprintfmt+0x13d>
  800901:	8b 04 85 c0 18 80 00 	mov    0x8018c0(,%eax,4),%eax
  800908:	85 c0                	test   %eax,%eax
  80090a:	75 18                	jne    800924 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80090c:	52                   	push   %edx
  80090d:	68 3b 16 80 00       	push   $0x80163b
  800912:	53                   	push   %ebx
  800913:	56                   	push   %esi
  800914:	e8 99 fe ff ff       	call   8007b2 <printfmt>
  800919:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80091c:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80091f:	e9 d1 fe ff ff       	jmp    8007f5 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800924:	50                   	push   %eax
  800925:	68 44 16 80 00       	push   $0x801644
  80092a:	53                   	push   %ebx
  80092b:	56                   	push   %esi
  80092c:	e8 81 fe ff ff       	call   8007b2 <printfmt>
  800931:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800934:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800937:	e9 b9 fe ff ff       	jmp    8007f5 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80093c:	8b 45 14             	mov    0x14(%ebp),%eax
  80093f:	8d 50 04             	lea    0x4(%eax),%edx
  800942:	89 55 14             	mov    %edx,0x14(%ebp)
  800945:	8b 38                	mov    (%eax),%edi
  800947:	85 ff                	test   %edi,%edi
  800949:	75 05                	jne    800950 <vprintfmt+0x181>
				p = "(null)";
  80094b:	bf 34 16 80 00       	mov    $0x801634,%edi
			if (width > 0 && padc != '-')
  800950:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800954:	0f 8e 90 00 00 00    	jle    8009ea <vprintfmt+0x21b>
  80095a:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80095e:	0f 84 8e 00 00 00    	je     8009f2 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800964:	83 ec 08             	sub    $0x8,%esp
  800967:	ff 75 d0             	pushl  -0x30(%ebp)
  80096a:	57                   	push   %edi
  80096b:	e8 70 02 00 00       	call   800be0 <strnlen>
  800970:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800973:	29 c1                	sub    %eax,%ecx
  800975:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800978:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80097b:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80097f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800982:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800985:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800987:	eb 0d                	jmp    800996 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800989:	83 ec 08             	sub    $0x8,%esp
  80098c:	53                   	push   %ebx
  80098d:	ff 75 e4             	pushl  -0x1c(%ebp)
  800990:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800992:	4f                   	dec    %edi
  800993:	83 c4 10             	add    $0x10,%esp
  800996:	85 ff                	test   %edi,%edi
  800998:	7f ef                	jg     800989 <vprintfmt+0x1ba>
  80099a:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80099d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8009a0:	89 c8                	mov    %ecx,%eax
  8009a2:	85 c9                	test   %ecx,%ecx
  8009a4:	79 05                	jns    8009ab <vprintfmt+0x1dc>
  8009a6:	b8 00 00 00 00       	mov    $0x0,%eax
  8009ab:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8009ae:	29 c1                	sub    %eax,%ecx
  8009b0:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8009b3:	89 75 08             	mov    %esi,0x8(%ebp)
  8009b6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8009b9:	eb 3d                	jmp    8009f8 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8009bb:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8009bf:	74 19                	je     8009da <vprintfmt+0x20b>
  8009c1:	0f be c0             	movsbl %al,%eax
  8009c4:	83 e8 20             	sub    $0x20,%eax
  8009c7:	83 f8 5e             	cmp    $0x5e,%eax
  8009ca:	76 0e                	jbe    8009da <vprintfmt+0x20b>
					putch('?', putdat);
  8009cc:	83 ec 08             	sub    $0x8,%esp
  8009cf:	53                   	push   %ebx
  8009d0:	6a 3f                	push   $0x3f
  8009d2:	ff 55 08             	call   *0x8(%ebp)
  8009d5:	83 c4 10             	add    $0x10,%esp
  8009d8:	eb 0b                	jmp    8009e5 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8009da:	83 ec 08             	sub    $0x8,%esp
  8009dd:	53                   	push   %ebx
  8009de:	52                   	push   %edx
  8009df:	ff 55 08             	call   *0x8(%ebp)
  8009e2:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8009e5:	ff 4d e4             	decl   -0x1c(%ebp)
  8009e8:	eb 0e                	jmp    8009f8 <vprintfmt+0x229>
  8009ea:	89 75 08             	mov    %esi,0x8(%ebp)
  8009ed:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8009f0:	eb 06                	jmp    8009f8 <vprintfmt+0x229>
  8009f2:	89 75 08             	mov    %esi,0x8(%ebp)
  8009f5:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8009f8:	47                   	inc    %edi
  8009f9:	8a 47 ff             	mov    -0x1(%edi),%al
  8009fc:	0f be d0             	movsbl %al,%edx
  8009ff:	85 d2                	test   %edx,%edx
  800a01:	74 1d                	je     800a20 <vprintfmt+0x251>
  800a03:	85 f6                	test   %esi,%esi
  800a05:	78 b4                	js     8009bb <vprintfmt+0x1ec>
  800a07:	4e                   	dec    %esi
  800a08:	79 b1                	jns    8009bb <vprintfmt+0x1ec>
  800a0a:	8b 75 08             	mov    0x8(%ebp),%esi
  800a0d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800a10:	eb 14                	jmp    800a26 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800a12:	83 ec 08             	sub    $0x8,%esp
  800a15:	53                   	push   %ebx
  800a16:	6a 20                	push   $0x20
  800a18:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800a1a:	4f                   	dec    %edi
  800a1b:	83 c4 10             	add    $0x10,%esp
  800a1e:	eb 06                	jmp    800a26 <vprintfmt+0x257>
  800a20:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800a23:	8b 75 08             	mov    0x8(%ebp),%esi
  800a26:	85 ff                	test   %edi,%edi
  800a28:	7f e8                	jg     800a12 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800a2a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800a2d:	e9 c3 fd ff ff       	jmp    8007f5 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800a32:	83 fa 01             	cmp    $0x1,%edx
  800a35:	7e 16                	jle    800a4d <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800a37:	8b 45 14             	mov    0x14(%ebp),%eax
  800a3a:	8d 50 08             	lea    0x8(%eax),%edx
  800a3d:	89 55 14             	mov    %edx,0x14(%ebp)
  800a40:	8b 50 04             	mov    0x4(%eax),%edx
  800a43:	8b 00                	mov    (%eax),%eax
  800a45:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800a48:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800a4b:	eb 32                	jmp    800a7f <vprintfmt+0x2b0>
	else if (lflag)
  800a4d:	85 d2                	test   %edx,%edx
  800a4f:	74 18                	je     800a69 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800a51:	8b 45 14             	mov    0x14(%ebp),%eax
  800a54:	8d 50 04             	lea    0x4(%eax),%edx
  800a57:	89 55 14             	mov    %edx,0x14(%ebp)
  800a5a:	8b 00                	mov    (%eax),%eax
  800a5c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800a5f:	89 c1                	mov    %eax,%ecx
  800a61:	c1 f9 1f             	sar    $0x1f,%ecx
  800a64:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800a67:	eb 16                	jmp    800a7f <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800a69:	8b 45 14             	mov    0x14(%ebp),%eax
  800a6c:	8d 50 04             	lea    0x4(%eax),%edx
  800a6f:	89 55 14             	mov    %edx,0x14(%ebp)
  800a72:	8b 00                	mov    (%eax),%eax
  800a74:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800a77:	89 c1                	mov    %eax,%ecx
  800a79:	c1 f9 1f             	sar    $0x1f,%ecx
  800a7c:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800a7f:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800a82:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800a85:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800a89:	79 76                	jns    800b01 <vprintfmt+0x332>
				putch('-', putdat);
  800a8b:	83 ec 08             	sub    $0x8,%esp
  800a8e:	53                   	push   %ebx
  800a8f:	6a 2d                	push   $0x2d
  800a91:	ff d6                	call   *%esi
				num = -(long long) num;
  800a93:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800a96:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800a99:	f7 d8                	neg    %eax
  800a9b:	83 d2 00             	adc    $0x0,%edx
  800a9e:	f7 da                	neg    %edx
  800aa0:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800aa3:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800aa8:	eb 5c                	jmp    800b06 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800aaa:	8d 45 14             	lea    0x14(%ebp),%eax
  800aad:	e8 aa fc ff ff       	call   80075c <getuint>
			base = 10;
  800ab2:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800ab7:	eb 4d                	jmp    800b06 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800ab9:	8d 45 14             	lea    0x14(%ebp),%eax
  800abc:	e8 9b fc ff ff       	call   80075c <getuint>
			base = 8;
  800ac1:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800ac6:	eb 3e                	jmp    800b06 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800ac8:	83 ec 08             	sub    $0x8,%esp
  800acb:	53                   	push   %ebx
  800acc:	6a 30                	push   $0x30
  800ace:	ff d6                	call   *%esi
			putch('x', putdat);
  800ad0:	83 c4 08             	add    $0x8,%esp
  800ad3:	53                   	push   %ebx
  800ad4:	6a 78                	push   $0x78
  800ad6:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800ad8:	8b 45 14             	mov    0x14(%ebp),%eax
  800adb:	8d 50 04             	lea    0x4(%eax),%edx
  800ade:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800ae1:	8b 00                	mov    (%eax),%eax
  800ae3:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800ae8:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800aeb:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800af0:	eb 14                	jmp    800b06 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800af2:	8d 45 14             	lea    0x14(%ebp),%eax
  800af5:	e8 62 fc ff ff       	call   80075c <getuint>
			base = 16;
  800afa:	b9 10 00 00 00       	mov    $0x10,%ecx
  800aff:	eb 05                	jmp    800b06 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800b01:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800b06:	83 ec 0c             	sub    $0xc,%esp
  800b09:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800b0d:	57                   	push   %edi
  800b0e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800b11:	51                   	push   %ecx
  800b12:	52                   	push   %edx
  800b13:	50                   	push   %eax
  800b14:	89 da                	mov    %ebx,%edx
  800b16:	89 f0                	mov    %esi,%eax
  800b18:	e8 92 fb ff ff       	call   8006af <printnum>
			break;
  800b1d:	83 c4 20             	add    $0x20,%esp
  800b20:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800b23:	e9 cd fc ff ff       	jmp    8007f5 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800b28:	83 ec 08             	sub    $0x8,%esp
  800b2b:	53                   	push   %ebx
  800b2c:	51                   	push   %ecx
  800b2d:	ff d6                	call   *%esi
			break;
  800b2f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800b32:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800b35:	e9 bb fc ff ff       	jmp    8007f5 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800b3a:	83 ec 08             	sub    $0x8,%esp
  800b3d:	53                   	push   %ebx
  800b3e:	6a 25                	push   $0x25
  800b40:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800b42:	83 c4 10             	add    $0x10,%esp
  800b45:	eb 01                	jmp    800b48 <vprintfmt+0x379>
  800b47:	4f                   	dec    %edi
  800b48:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800b4c:	75 f9                	jne    800b47 <vprintfmt+0x378>
  800b4e:	e9 a2 fc ff ff       	jmp    8007f5 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800b53:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b56:	5b                   	pop    %ebx
  800b57:	5e                   	pop    %esi
  800b58:	5f                   	pop    %edi
  800b59:	5d                   	pop    %ebp
  800b5a:	c3                   	ret    

00800b5b <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800b5b:	55                   	push   %ebp
  800b5c:	89 e5                	mov    %esp,%ebp
  800b5e:	83 ec 18             	sub    $0x18,%esp
  800b61:	8b 45 08             	mov    0x8(%ebp),%eax
  800b64:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800b67:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800b6a:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800b6e:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800b71:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800b78:	85 c0                	test   %eax,%eax
  800b7a:	74 26                	je     800ba2 <vsnprintf+0x47>
  800b7c:	85 d2                	test   %edx,%edx
  800b7e:	7e 29                	jle    800ba9 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800b80:	ff 75 14             	pushl  0x14(%ebp)
  800b83:	ff 75 10             	pushl  0x10(%ebp)
  800b86:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800b89:	50                   	push   %eax
  800b8a:	68 96 07 80 00       	push   $0x800796
  800b8f:	e8 3b fc ff ff       	call   8007cf <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800b94:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800b97:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800b9a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800b9d:	83 c4 10             	add    $0x10,%esp
  800ba0:	eb 0c                	jmp    800bae <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800ba2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800ba7:	eb 05                	jmp    800bae <vsnprintf+0x53>
  800ba9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800bae:	c9                   	leave  
  800baf:	c3                   	ret    

00800bb0 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800bb0:	55                   	push   %ebp
  800bb1:	89 e5                	mov    %esp,%ebp
  800bb3:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800bb6:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800bb9:	50                   	push   %eax
  800bba:	ff 75 10             	pushl  0x10(%ebp)
  800bbd:	ff 75 0c             	pushl  0xc(%ebp)
  800bc0:	ff 75 08             	pushl  0x8(%ebp)
  800bc3:	e8 93 ff ff ff       	call   800b5b <vsnprintf>
	va_end(ap);

	return rc;
}
  800bc8:	c9                   	leave  
  800bc9:	c3                   	ret    

00800bca <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800bca:	55                   	push   %ebp
  800bcb:	89 e5                	mov    %esp,%ebp
  800bcd:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800bd0:	b8 00 00 00 00       	mov    $0x0,%eax
  800bd5:	eb 01                	jmp    800bd8 <strlen+0xe>
		n++;
  800bd7:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800bd8:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800bdc:	75 f9                	jne    800bd7 <strlen+0xd>
		n++;
	return n;
}
  800bde:	5d                   	pop    %ebp
  800bdf:	c3                   	ret    

00800be0 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800be0:	55                   	push   %ebp
  800be1:	89 e5                	mov    %esp,%ebp
  800be3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800be6:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800be9:	ba 00 00 00 00       	mov    $0x0,%edx
  800bee:	eb 01                	jmp    800bf1 <strnlen+0x11>
		n++;
  800bf0:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800bf1:	39 c2                	cmp    %eax,%edx
  800bf3:	74 08                	je     800bfd <strnlen+0x1d>
  800bf5:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800bf9:	75 f5                	jne    800bf0 <strnlen+0x10>
  800bfb:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800bfd:	5d                   	pop    %ebp
  800bfe:	c3                   	ret    

00800bff <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800bff:	55                   	push   %ebp
  800c00:	89 e5                	mov    %esp,%ebp
  800c02:	53                   	push   %ebx
  800c03:	8b 45 08             	mov    0x8(%ebp),%eax
  800c06:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800c09:	89 c2                	mov    %eax,%edx
  800c0b:	42                   	inc    %edx
  800c0c:	41                   	inc    %ecx
  800c0d:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800c10:	88 5a ff             	mov    %bl,-0x1(%edx)
  800c13:	84 db                	test   %bl,%bl
  800c15:	75 f4                	jne    800c0b <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800c17:	5b                   	pop    %ebx
  800c18:	5d                   	pop    %ebp
  800c19:	c3                   	ret    

00800c1a <strcat>:

char *
strcat(char *dst, const char *src)
{
  800c1a:	55                   	push   %ebp
  800c1b:	89 e5                	mov    %esp,%ebp
  800c1d:	53                   	push   %ebx
  800c1e:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800c21:	53                   	push   %ebx
  800c22:	e8 a3 ff ff ff       	call   800bca <strlen>
  800c27:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800c2a:	ff 75 0c             	pushl  0xc(%ebp)
  800c2d:	01 d8                	add    %ebx,%eax
  800c2f:	50                   	push   %eax
  800c30:	e8 ca ff ff ff       	call   800bff <strcpy>
	return dst;
}
  800c35:	89 d8                	mov    %ebx,%eax
  800c37:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800c3a:	c9                   	leave  
  800c3b:	c3                   	ret    

00800c3c <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800c3c:	55                   	push   %ebp
  800c3d:	89 e5                	mov    %esp,%ebp
  800c3f:	56                   	push   %esi
  800c40:	53                   	push   %ebx
  800c41:	8b 75 08             	mov    0x8(%ebp),%esi
  800c44:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c47:	89 f3                	mov    %esi,%ebx
  800c49:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800c4c:	89 f2                	mov    %esi,%edx
  800c4e:	eb 0c                	jmp    800c5c <strncpy+0x20>
		*dst++ = *src;
  800c50:	42                   	inc    %edx
  800c51:	8a 01                	mov    (%ecx),%al
  800c53:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800c56:	80 39 01             	cmpb   $0x1,(%ecx)
  800c59:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800c5c:	39 da                	cmp    %ebx,%edx
  800c5e:	75 f0                	jne    800c50 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800c60:	89 f0                	mov    %esi,%eax
  800c62:	5b                   	pop    %ebx
  800c63:	5e                   	pop    %esi
  800c64:	5d                   	pop    %ebp
  800c65:	c3                   	ret    

00800c66 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800c66:	55                   	push   %ebp
  800c67:	89 e5                	mov    %esp,%ebp
  800c69:	56                   	push   %esi
  800c6a:	53                   	push   %ebx
  800c6b:	8b 75 08             	mov    0x8(%ebp),%esi
  800c6e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c71:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800c74:	85 c0                	test   %eax,%eax
  800c76:	74 1e                	je     800c96 <strlcpy+0x30>
  800c78:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800c7c:	89 f2                	mov    %esi,%edx
  800c7e:	eb 05                	jmp    800c85 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800c80:	42                   	inc    %edx
  800c81:	41                   	inc    %ecx
  800c82:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800c85:	39 c2                	cmp    %eax,%edx
  800c87:	74 08                	je     800c91 <strlcpy+0x2b>
  800c89:	8a 19                	mov    (%ecx),%bl
  800c8b:	84 db                	test   %bl,%bl
  800c8d:	75 f1                	jne    800c80 <strlcpy+0x1a>
  800c8f:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800c91:	c6 00 00             	movb   $0x0,(%eax)
  800c94:	eb 02                	jmp    800c98 <strlcpy+0x32>
  800c96:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800c98:	29 f0                	sub    %esi,%eax
}
  800c9a:	5b                   	pop    %ebx
  800c9b:	5e                   	pop    %esi
  800c9c:	5d                   	pop    %ebp
  800c9d:	c3                   	ret    

00800c9e <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800c9e:	55                   	push   %ebp
  800c9f:	89 e5                	mov    %esp,%ebp
  800ca1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800ca4:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800ca7:	eb 02                	jmp    800cab <strcmp+0xd>
		p++, q++;
  800ca9:	41                   	inc    %ecx
  800caa:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800cab:	8a 01                	mov    (%ecx),%al
  800cad:	84 c0                	test   %al,%al
  800caf:	74 04                	je     800cb5 <strcmp+0x17>
  800cb1:	3a 02                	cmp    (%edx),%al
  800cb3:	74 f4                	je     800ca9 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800cb5:	0f b6 c0             	movzbl %al,%eax
  800cb8:	0f b6 12             	movzbl (%edx),%edx
  800cbb:	29 d0                	sub    %edx,%eax
}
  800cbd:	5d                   	pop    %ebp
  800cbe:	c3                   	ret    

00800cbf <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800cbf:	55                   	push   %ebp
  800cc0:	89 e5                	mov    %esp,%ebp
  800cc2:	53                   	push   %ebx
  800cc3:	8b 45 08             	mov    0x8(%ebp),%eax
  800cc6:	8b 55 0c             	mov    0xc(%ebp),%edx
  800cc9:	89 c3                	mov    %eax,%ebx
  800ccb:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800cce:	eb 02                	jmp    800cd2 <strncmp+0x13>
		n--, p++, q++;
  800cd0:	40                   	inc    %eax
  800cd1:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800cd2:	39 d8                	cmp    %ebx,%eax
  800cd4:	74 14                	je     800cea <strncmp+0x2b>
  800cd6:	8a 08                	mov    (%eax),%cl
  800cd8:	84 c9                	test   %cl,%cl
  800cda:	74 04                	je     800ce0 <strncmp+0x21>
  800cdc:	3a 0a                	cmp    (%edx),%cl
  800cde:	74 f0                	je     800cd0 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800ce0:	0f b6 00             	movzbl (%eax),%eax
  800ce3:	0f b6 12             	movzbl (%edx),%edx
  800ce6:	29 d0                	sub    %edx,%eax
  800ce8:	eb 05                	jmp    800cef <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800cea:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800cef:	5b                   	pop    %ebx
  800cf0:	5d                   	pop    %ebp
  800cf1:	c3                   	ret    

00800cf2 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800cf2:	55                   	push   %ebp
  800cf3:	89 e5                	mov    %esp,%ebp
  800cf5:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf8:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800cfb:	eb 05                	jmp    800d02 <strchr+0x10>
		if (*s == c)
  800cfd:	38 ca                	cmp    %cl,%dl
  800cff:	74 0c                	je     800d0d <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800d01:	40                   	inc    %eax
  800d02:	8a 10                	mov    (%eax),%dl
  800d04:	84 d2                	test   %dl,%dl
  800d06:	75 f5                	jne    800cfd <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800d08:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800d0d:	5d                   	pop    %ebp
  800d0e:	c3                   	ret    

00800d0f <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800d0f:	55                   	push   %ebp
  800d10:	89 e5                	mov    %esp,%ebp
  800d12:	8b 45 08             	mov    0x8(%ebp),%eax
  800d15:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800d18:	eb 05                	jmp    800d1f <strfind+0x10>
		if (*s == c)
  800d1a:	38 ca                	cmp    %cl,%dl
  800d1c:	74 07                	je     800d25 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800d1e:	40                   	inc    %eax
  800d1f:	8a 10                	mov    (%eax),%dl
  800d21:	84 d2                	test   %dl,%dl
  800d23:	75 f5                	jne    800d1a <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800d25:	5d                   	pop    %ebp
  800d26:	c3                   	ret    

00800d27 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800d27:	55                   	push   %ebp
  800d28:	89 e5                	mov    %esp,%ebp
  800d2a:	57                   	push   %edi
  800d2b:	56                   	push   %esi
  800d2c:	53                   	push   %ebx
  800d2d:	8b 7d 08             	mov    0x8(%ebp),%edi
  800d30:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800d33:	85 c9                	test   %ecx,%ecx
  800d35:	74 36                	je     800d6d <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800d37:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800d3d:	75 28                	jne    800d67 <memset+0x40>
  800d3f:	f6 c1 03             	test   $0x3,%cl
  800d42:	75 23                	jne    800d67 <memset+0x40>
		c &= 0xFF;
  800d44:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800d48:	89 d3                	mov    %edx,%ebx
  800d4a:	c1 e3 08             	shl    $0x8,%ebx
  800d4d:	89 d6                	mov    %edx,%esi
  800d4f:	c1 e6 18             	shl    $0x18,%esi
  800d52:	89 d0                	mov    %edx,%eax
  800d54:	c1 e0 10             	shl    $0x10,%eax
  800d57:	09 f0                	or     %esi,%eax
  800d59:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800d5b:	89 d8                	mov    %ebx,%eax
  800d5d:	09 d0                	or     %edx,%eax
  800d5f:	c1 e9 02             	shr    $0x2,%ecx
  800d62:	fc                   	cld    
  800d63:	f3 ab                	rep stos %eax,%es:(%edi)
  800d65:	eb 06                	jmp    800d6d <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800d67:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d6a:	fc                   	cld    
  800d6b:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800d6d:	89 f8                	mov    %edi,%eax
  800d6f:	5b                   	pop    %ebx
  800d70:	5e                   	pop    %esi
  800d71:	5f                   	pop    %edi
  800d72:	5d                   	pop    %ebp
  800d73:	c3                   	ret    

00800d74 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800d74:	55                   	push   %ebp
  800d75:	89 e5                	mov    %esp,%ebp
  800d77:	57                   	push   %edi
  800d78:	56                   	push   %esi
  800d79:	8b 45 08             	mov    0x8(%ebp),%eax
  800d7c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800d7f:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800d82:	39 c6                	cmp    %eax,%esi
  800d84:	73 33                	jae    800db9 <memmove+0x45>
  800d86:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800d89:	39 d0                	cmp    %edx,%eax
  800d8b:	73 2c                	jae    800db9 <memmove+0x45>
		s += n;
		d += n;
  800d8d:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800d90:	89 d6                	mov    %edx,%esi
  800d92:	09 fe                	or     %edi,%esi
  800d94:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800d9a:	75 13                	jne    800daf <memmove+0x3b>
  800d9c:	f6 c1 03             	test   $0x3,%cl
  800d9f:	75 0e                	jne    800daf <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800da1:	83 ef 04             	sub    $0x4,%edi
  800da4:	8d 72 fc             	lea    -0x4(%edx),%esi
  800da7:	c1 e9 02             	shr    $0x2,%ecx
  800daa:	fd                   	std    
  800dab:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800dad:	eb 07                	jmp    800db6 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800daf:	4f                   	dec    %edi
  800db0:	8d 72 ff             	lea    -0x1(%edx),%esi
  800db3:	fd                   	std    
  800db4:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800db6:	fc                   	cld    
  800db7:	eb 1d                	jmp    800dd6 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800db9:	89 f2                	mov    %esi,%edx
  800dbb:	09 c2                	or     %eax,%edx
  800dbd:	f6 c2 03             	test   $0x3,%dl
  800dc0:	75 0f                	jne    800dd1 <memmove+0x5d>
  800dc2:	f6 c1 03             	test   $0x3,%cl
  800dc5:	75 0a                	jne    800dd1 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800dc7:	c1 e9 02             	shr    $0x2,%ecx
  800dca:	89 c7                	mov    %eax,%edi
  800dcc:	fc                   	cld    
  800dcd:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800dcf:	eb 05                	jmp    800dd6 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800dd1:	89 c7                	mov    %eax,%edi
  800dd3:	fc                   	cld    
  800dd4:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800dd6:	5e                   	pop    %esi
  800dd7:	5f                   	pop    %edi
  800dd8:	5d                   	pop    %ebp
  800dd9:	c3                   	ret    

00800dda <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800dda:	55                   	push   %ebp
  800ddb:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800ddd:	ff 75 10             	pushl  0x10(%ebp)
  800de0:	ff 75 0c             	pushl  0xc(%ebp)
  800de3:	ff 75 08             	pushl  0x8(%ebp)
  800de6:	e8 89 ff ff ff       	call   800d74 <memmove>
}
  800deb:	c9                   	leave  
  800dec:	c3                   	ret    

00800ded <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800ded:	55                   	push   %ebp
  800dee:	89 e5                	mov    %esp,%ebp
  800df0:	56                   	push   %esi
  800df1:	53                   	push   %ebx
  800df2:	8b 45 08             	mov    0x8(%ebp),%eax
  800df5:	8b 55 0c             	mov    0xc(%ebp),%edx
  800df8:	89 c6                	mov    %eax,%esi
  800dfa:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800dfd:	eb 14                	jmp    800e13 <memcmp+0x26>
		if (*s1 != *s2)
  800dff:	8a 08                	mov    (%eax),%cl
  800e01:	8a 1a                	mov    (%edx),%bl
  800e03:	38 d9                	cmp    %bl,%cl
  800e05:	74 0a                	je     800e11 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800e07:	0f b6 c1             	movzbl %cl,%eax
  800e0a:	0f b6 db             	movzbl %bl,%ebx
  800e0d:	29 d8                	sub    %ebx,%eax
  800e0f:	eb 0b                	jmp    800e1c <memcmp+0x2f>
		s1++, s2++;
  800e11:	40                   	inc    %eax
  800e12:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800e13:	39 f0                	cmp    %esi,%eax
  800e15:	75 e8                	jne    800dff <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800e17:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800e1c:	5b                   	pop    %ebx
  800e1d:	5e                   	pop    %esi
  800e1e:	5d                   	pop    %ebp
  800e1f:	c3                   	ret    

00800e20 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800e20:	55                   	push   %ebp
  800e21:	89 e5                	mov    %esp,%ebp
  800e23:	53                   	push   %ebx
  800e24:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800e27:	89 c1                	mov    %eax,%ecx
  800e29:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800e2c:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800e30:	eb 08                	jmp    800e3a <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800e32:	0f b6 10             	movzbl (%eax),%edx
  800e35:	39 da                	cmp    %ebx,%edx
  800e37:	74 05                	je     800e3e <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800e39:	40                   	inc    %eax
  800e3a:	39 c8                	cmp    %ecx,%eax
  800e3c:	72 f4                	jb     800e32 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800e3e:	5b                   	pop    %ebx
  800e3f:	5d                   	pop    %ebp
  800e40:	c3                   	ret    

00800e41 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800e41:	55                   	push   %ebp
  800e42:	89 e5                	mov    %esp,%ebp
  800e44:	57                   	push   %edi
  800e45:	56                   	push   %esi
  800e46:	53                   	push   %ebx
  800e47:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800e4a:	eb 01                	jmp    800e4d <strtol+0xc>
		s++;
  800e4c:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800e4d:	8a 01                	mov    (%ecx),%al
  800e4f:	3c 20                	cmp    $0x20,%al
  800e51:	74 f9                	je     800e4c <strtol+0xb>
  800e53:	3c 09                	cmp    $0x9,%al
  800e55:	74 f5                	je     800e4c <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800e57:	3c 2b                	cmp    $0x2b,%al
  800e59:	75 08                	jne    800e63 <strtol+0x22>
		s++;
  800e5b:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800e5c:	bf 00 00 00 00       	mov    $0x0,%edi
  800e61:	eb 11                	jmp    800e74 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800e63:	3c 2d                	cmp    $0x2d,%al
  800e65:	75 08                	jne    800e6f <strtol+0x2e>
		s++, neg = 1;
  800e67:	41                   	inc    %ecx
  800e68:	bf 01 00 00 00       	mov    $0x1,%edi
  800e6d:	eb 05                	jmp    800e74 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800e6f:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800e74:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800e78:	0f 84 87 00 00 00    	je     800f05 <strtol+0xc4>
  800e7e:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800e82:	75 27                	jne    800eab <strtol+0x6a>
  800e84:	80 39 30             	cmpb   $0x30,(%ecx)
  800e87:	75 22                	jne    800eab <strtol+0x6a>
  800e89:	e9 88 00 00 00       	jmp    800f16 <strtol+0xd5>
		s += 2, base = 16;
  800e8e:	83 c1 02             	add    $0x2,%ecx
  800e91:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800e98:	eb 11                	jmp    800eab <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800e9a:	41                   	inc    %ecx
  800e9b:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800ea2:	eb 07                	jmp    800eab <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800ea4:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800eab:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800eb0:	8a 11                	mov    (%ecx),%dl
  800eb2:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800eb5:	80 fb 09             	cmp    $0x9,%bl
  800eb8:	77 08                	ja     800ec2 <strtol+0x81>
			dig = *s - '0';
  800eba:	0f be d2             	movsbl %dl,%edx
  800ebd:	83 ea 30             	sub    $0x30,%edx
  800ec0:	eb 22                	jmp    800ee4 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800ec2:	8d 72 9f             	lea    -0x61(%edx),%esi
  800ec5:	89 f3                	mov    %esi,%ebx
  800ec7:	80 fb 19             	cmp    $0x19,%bl
  800eca:	77 08                	ja     800ed4 <strtol+0x93>
			dig = *s - 'a' + 10;
  800ecc:	0f be d2             	movsbl %dl,%edx
  800ecf:	83 ea 57             	sub    $0x57,%edx
  800ed2:	eb 10                	jmp    800ee4 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800ed4:	8d 72 bf             	lea    -0x41(%edx),%esi
  800ed7:	89 f3                	mov    %esi,%ebx
  800ed9:	80 fb 19             	cmp    $0x19,%bl
  800edc:	77 14                	ja     800ef2 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800ede:	0f be d2             	movsbl %dl,%edx
  800ee1:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800ee4:	3b 55 10             	cmp    0x10(%ebp),%edx
  800ee7:	7d 09                	jge    800ef2 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800ee9:	41                   	inc    %ecx
  800eea:	0f af 45 10          	imul   0x10(%ebp),%eax
  800eee:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800ef0:	eb be                	jmp    800eb0 <strtol+0x6f>

	if (endptr)
  800ef2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ef6:	74 05                	je     800efd <strtol+0xbc>
		*endptr = (char *) s;
  800ef8:	8b 75 0c             	mov    0xc(%ebp),%esi
  800efb:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800efd:	85 ff                	test   %edi,%edi
  800eff:	74 21                	je     800f22 <strtol+0xe1>
  800f01:	f7 d8                	neg    %eax
  800f03:	eb 1d                	jmp    800f22 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800f05:	80 39 30             	cmpb   $0x30,(%ecx)
  800f08:	75 9a                	jne    800ea4 <strtol+0x63>
  800f0a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800f0e:	0f 84 7a ff ff ff    	je     800e8e <strtol+0x4d>
  800f14:	eb 84                	jmp    800e9a <strtol+0x59>
  800f16:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800f1a:	0f 84 6e ff ff ff    	je     800e8e <strtol+0x4d>
  800f20:	eb 89                	jmp    800eab <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800f22:	5b                   	pop    %ebx
  800f23:	5e                   	pop    %esi
  800f24:	5f                   	pop    %edi
  800f25:	5d                   	pop    %ebp
  800f26:	c3                   	ret    

00800f27 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800f27:	55                   	push   %ebp
  800f28:	89 e5                	mov    %esp,%ebp
  800f2a:	57                   	push   %edi
  800f2b:	56                   	push   %esi
  800f2c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f2d:	b8 00 00 00 00       	mov    $0x0,%eax
  800f32:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f35:	8b 55 08             	mov    0x8(%ebp),%edx
  800f38:	89 c3                	mov    %eax,%ebx
  800f3a:	89 c7                	mov    %eax,%edi
  800f3c:	89 c6                	mov    %eax,%esi
  800f3e:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800f40:	5b                   	pop    %ebx
  800f41:	5e                   	pop    %esi
  800f42:	5f                   	pop    %edi
  800f43:	5d                   	pop    %ebp
  800f44:	c3                   	ret    

00800f45 <sys_cgetc>:

int
sys_cgetc(void)
{
  800f45:	55                   	push   %ebp
  800f46:	89 e5                	mov    %esp,%ebp
  800f48:	57                   	push   %edi
  800f49:	56                   	push   %esi
  800f4a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f4b:	ba 00 00 00 00       	mov    $0x0,%edx
  800f50:	b8 01 00 00 00       	mov    $0x1,%eax
  800f55:	89 d1                	mov    %edx,%ecx
  800f57:	89 d3                	mov    %edx,%ebx
  800f59:	89 d7                	mov    %edx,%edi
  800f5b:	89 d6                	mov    %edx,%esi
  800f5d:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800f5f:	5b                   	pop    %ebx
  800f60:	5e                   	pop    %esi
  800f61:	5f                   	pop    %edi
  800f62:	5d                   	pop    %ebp
  800f63:	c3                   	ret    

00800f64 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800f64:	55                   	push   %ebp
  800f65:	89 e5                	mov    %esp,%ebp
  800f67:	57                   	push   %edi
  800f68:	56                   	push   %esi
  800f69:	53                   	push   %ebx
  800f6a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f6d:	b9 00 00 00 00       	mov    $0x0,%ecx
  800f72:	b8 03 00 00 00       	mov    $0x3,%eax
  800f77:	8b 55 08             	mov    0x8(%ebp),%edx
  800f7a:	89 cb                	mov    %ecx,%ebx
  800f7c:	89 cf                	mov    %ecx,%edi
  800f7e:	89 ce                	mov    %ecx,%esi
  800f80:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f82:	85 c0                	test   %eax,%eax
  800f84:	7e 17                	jle    800f9d <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f86:	83 ec 0c             	sub    $0xc,%esp
  800f89:	50                   	push   %eax
  800f8a:	6a 03                	push   $0x3
  800f8c:	68 1f 19 80 00       	push   $0x80191f
  800f91:	6a 23                	push   $0x23
  800f93:	68 3c 19 80 00       	push   $0x80193c
  800f98:	e8 26 f6 ff ff       	call   8005c3 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800f9d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fa0:	5b                   	pop    %ebx
  800fa1:	5e                   	pop    %esi
  800fa2:	5f                   	pop    %edi
  800fa3:	5d                   	pop    %ebp
  800fa4:	c3                   	ret    

00800fa5 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800fa5:	55                   	push   %ebp
  800fa6:	89 e5                	mov    %esp,%ebp
  800fa8:	57                   	push   %edi
  800fa9:	56                   	push   %esi
  800faa:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fab:	ba 00 00 00 00       	mov    $0x0,%edx
  800fb0:	b8 02 00 00 00       	mov    $0x2,%eax
  800fb5:	89 d1                	mov    %edx,%ecx
  800fb7:	89 d3                	mov    %edx,%ebx
  800fb9:	89 d7                	mov    %edx,%edi
  800fbb:	89 d6                	mov    %edx,%esi
  800fbd:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800fbf:	5b                   	pop    %ebx
  800fc0:	5e                   	pop    %esi
  800fc1:	5f                   	pop    %edi
  800fc2:	5d                   	pop    %ebp
  800fc3:	c3                   	ret    

00800fc4 <sys_yield>:

void
sys_yield(void)
{
  800fc4:	55                   	push   %ebp
  800fc5:	89 e5                	mov    %esp,%ebp
  800fc7:	57                   	push   %edi
  800fc8:	56                   	push   %esi
  800fc9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fca:	ba 00 00 00 00       	mov    $0x0,%edx
  800fcf:	b8 0b 00 00 00       	mov    $0xb,%eax
  800fd4:	89 d1                	mov    %edx,%ecx
  800fd6:	89 d3                	mov    %edx,%ebx
  800fd8:	89 d7                	mov    %edx,%edi
  800fda:	89 d6                	mov    %edx,%esi
  800fdc:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800fde:	5b                   	pop    %ebx
  800fdf:	5e                   	pop    %esi
  800fe0:	5f                   	pop    %edi
  800fe1:	5d                   	pop    %ebp
  800fe2:	c3                   	ret    

00800fe3 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800fe3:	55                   	push   %ebp
  800fe4:	89 e5                	mov    %esp,%ebp
  800fe6:	57                   	push   %edi
  800fe7:	56                   	push   %esi
  800fe8:	53                   	push   %ebx
  800fe9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fec:	be 00 00 00 00       	mov    $0x0,%esi
  800ff1:	b8 04 00 00 00       	mov    $0x4,%eax
  800ff6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ff9:	8b 55 08             	mov    0x8(%ebp),%edx
  800ffc:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800fff:	89 f7                	mov    %esi,%edi
  801001:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801003:	85 c0                	test   %eax,%eax
  801005:	7e 17                	jle    80101e <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  801007:	83 ec 0c             	sub    $0xc,%esp
  80100a:	50                   	push   %eax
  80100b:	6a 04                	push   $0x4
  80100d:	68 1f 19 80 00       	push   $0x80191f
  801012:	6a 23                	push   $0x23
  801014:	68 3c 19 80 00       	push   $0x80193c
  801019:	e8 a5 f5 ff ff       	call   8005c3 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  80101e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801021:	5b                   	pop    %ebx
  801022:	5e                   	pop    %esi
  801023:	5f                   	pop    %edi
  801024:	5d                   	pop    %ebp
  801025:	c3                   	ret    

00801026 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  801026:	55                   	push   %ebp
  801027:	89 e5                	mov    %esp,%ebp
  801029:	57                   	push   %edi
  80102a:	56                   	push   %esi
  80102b:	53                   	push   %ebx
  80102c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80102f:	b8 05 00 00 00       	mov    $0x5,%eax
  801034:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801037:	8b 55 08             	mov    0x8(%ebp),%edx
  80103a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80103d:	8b 7d 14             	mov    0x14(%ebp),%edi
  801040:	8b 75 18             	mov    0x18(%ebp),%esi
  801043:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801045:	85 c0                	test   %eax,%eax
  801047:	7e 17                	jle    801060 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  801049:	83 ec 0c             	sub    $0xc,%esp
  80104c:	50                   	push   %eax
  80104d:	6a 05                	push   $0x5
  80104f:	68 1f 19 80 00       	push   $0x80191f
  801054:	6a 23                	push   $0x23
  801056:	68 3c 19 80 00       	push   $0x80193c
  80105b:	e8 63 f5 ff ff       	call   8005c3 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  801060:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801063:	5b                   	pop    %ebx
  801064:	5e                   	pop    %esi
  801065:	5f                   	pop    %edi
  801066:	5d                   	pop    %ebp
  801067:	c3                   	ret    

00801068 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  801068:	55                   	push   %ebp
  801069:	89 e5                	mov    %esp,%ebp
  80106b:	57                   	push   %edi
  80106c:	56                   	push   %esi
  80106d:	53                   	push   %ebx
  80106e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801071:	bb 00 00 00 00       	mov    $0x0,%ebx
  801076:	b8 06 00 00 00       	mov    $0x6,%eax
  80107b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80107e:	8b 55 08             	mov    0x8(%ebp),%edx
  801081:	89 df                	mov    %ebx,%edi
  801083:	89 de                	mov    %ebx,%esi
  801085:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801087:	85 c0                	test   %eax,%eax
  801089:	7e 17                	jle    8010a2 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80108b:	83 ec 0c             	sub    $0xc,%esp
  80108e:	50                   	push   %eax
  80108f:	6a 06                	push   $0x6
  801091:	68 1f 19 80 00       	push   $0x80191f
  801096:	6a 23                	push   $0x23
  801098:	68 3c 19 80 00       	push   $0x80193c
  80109d:	e8 21 f5 ff ff       	call   8005c3 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  8010a2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010a5:	5b                   	pop    %ebx
  8010a6:	5e                   	pop    %esi
  8010a7:	5f                   	pop    %edi
  8010a8:	5d                   	pop    %ebp
  8010a9:	c3                   	ret    

008010aa <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  8010aa:	55                   	push   %ebp
  8010ab:	89 e5                	mov    %esp,%ebp
  8010ad:	57                   	push   %edi
  8010ae:	56                   	push   %esi
  8010af:	53                   	push   %ebx
  8010b0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8010b3:	bb 00 00 00 00       	mov    $0x0,%ebx
  8010b8:	b8 08 00 00 00       	mov    $0x8,%eax
  8010bd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8010c0:	8b 55 08             	mov    0x8(%ebp),%edx
  8010c3:	89 df                	mov    %ebx,%edi
  8010c5:	89 de                	mov    %ebx,%esi
  8010c7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8010c9:	85 c0                	test   %eax,%eax
  8010cb:	7e 17                	jle    8010e4 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8010cd:	83 ec 0c             	sub    $0xc,%esp
  8010d0:	50                   	push   %eax
  8010d1:	6a 08                	push   $0x8
  8010d3:	68 1f 19 80 00       	push   $0x80191f
  8010d8:	6a 23                	push   $0x23
  8010da:	68 3c 19 80 00       	push   $0x80193c
  8010df:	e8 df f4 ff ff       	call   8005c3 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  8010e4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010e7:	5b                   	pop    %ebx
  8010e8:	5e                   	pop    %esi
  8010e9:	5f                   	pop    %edi
  8010ea:	5d                   	pop    %ebp
  8010eb:	c3                   	ret    

008010ec <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  8010ec:	55                   	push   %ebp
  8010ed:	89 e5                	mov    %esp,%ebp
  8010ef:	57                   	push   %edi
  8010f0:	56                   	push   %esi
  8010f1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8010f2:	ba 00 00 00 00       	mov    $0x0,%edx
  8010f7:	b8 0c 00 00 00       	mov    $0xc,%eax
  8010fc:	89 d1                	mov    %edx,%ecx
  8010fe:	89 d3                	mov    %edx,%ebx
  801100:	89 d7                	mov    %edx,%edi
  801102:	89 d6                	mov    %edx,%esi
  801104:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  801106:	5b                   	pop    %ebx
  801107:	5e                   	pop    %esi
  801108:	5f                   	pop    %edi
  801109:	5d                   	pop    %ebp
  80110a:	c3                   	ret    

0080110b <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  80110b:	55                   	push   %ebp
  80110c:	89 e5                	mov    %esp,%ebp
  80110e:	57                   	push   %edi
  80110f:	56                   	push   %esi
  801110:	53                   	push   %ebx
  801111:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801114:	bb 00 00 00 00       	mov    $0x0,%ebx
  801119:	b8 09 00 00 00       	mov    $0x9,%eax
  80111e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801121:	8b 55 08             	mov    0x8(%ebp),%edx
  801124:	89 df                	mov    %ebx,%edi
  801126:	89 de                	mov    %ebx,%esi
  801128:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80112a:	85 c0                	test   %eax,%eax
  80112c:	7e 17                	jle    801145 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80112e:	83 ec 0c             	sub    $0xc,%esp
  801131:	50                   	push   %eax
  801132:	6a 09                	push   $0x9
  801134:	68 1f 19 80 00       	push   $0x80191f
  801139:	6a 23                	push   $0x23
  80113b:	68 3c 19 80 00       	push   $0x80193c
  801140:	e8 7e f4 ff ff       	call   8005c3 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  801145:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801148:	5b                   	pop    %ebx
  801149:	5e                   	pop    %esi
  80114a:	5f                   	pop    %edi
  80114b:	5d                   	pop    %ebp
  80114c:	c3                   	ret    

0080114d <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80114d:	55                   	push   %ebp
  80114e:	89 e5                	mov    %esp,%ebp
  801150:	57                   	push   %edi
  801151:	56                   	push   %esi
  801152:	53                   	push   %ebx
  801153:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801156:	bb 00 00 00 00       	mov    $0x0,%ebx
  80115b:	b8 0a 00 00 00       	mov    $0xa,%eax
  801160:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801163:	8b 55 08             	mov    0x8(%ebp),%edx
  801166:	89 df                	mov    %ebx,%edi
  801168:	89 de                	mov    %ebx,%esi
  80116a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80116c:	85 c0                	test   %eax,%eax
  80116e:	7e 17                	jle    801187 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  801170:	83 ec 0c             	sub    $0xc,%esp
  801173:	50                   	push   %eax
  801174:	6a 0a                	push   $0xa
  801176:	68 1f 19 80 00       	push   $0x80191f
  80117b:	6a 23                	push   $0x23
  80117d:	68 3c 19 80 00       	push   $0x80193c
  801182:	e8 3c f4 ff ff       	call   8005c3 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  801187:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80118a:	5b                   	pop    %ebx
  80118b:	5e                   	pop    %esi
  80118c:	5f                   	pop    %edi
  80118d:	5d                   	pop    %ebp
  80118e:	c3                   	ret    

0080118f <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  80118f:	55                   	push   %ebp
  801190:	89 e5                	mov    %esp,%ebp
  801192:	57                   	push   %edi
  801193:	56                   	push   %esi
  801194:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801195:	be 00 00 00 00       	mov    $0x0,%esi
  80119a:	b8 0d 00 00 00       	mov    $0xd,%eax
  80119f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8011a2:	8b 55 08             	mov    0x8(%ebp),%edx
  8011a5:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8011a8:	8b 7d 14             	mov    0x14(%ebp),%edi
  8011ab:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8011ad:	5b                   	pop    %ebx
  8011ae:	5e                   	pop    %esi
  8011af:	5f                   	pop    %edi
  8011b0:	5d                   	pop    %ebp
  8011b1:	c3                   	ret    

008011b2 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  8011b2:	55                   	push   %ebp
  8011b3:	89 e5                	mov    %esp,%ebp
  8011b5:	57                   	push   %edi
  8011b6:	56                   	push   %esi
  8011b7:	53                   	push   %ebx
  8011b8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8011bb:	b9 00 00 00 00       	mov    $0x0,%ecx
  8011c0:	b8 0e 00 00 00       	mov    $0xe,%eax
  8011c5:	8b 55 08             	mov    0x8(%ebp),%edx
  8011c8:	89 cb                	mov    %ecx,%ebx
  8011ca:	89 cf                	mov    %ecx,%edi
  8011cc:	89 ce                	mov    %ecx,%esi
  8011ce:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8011d0:	85 c0                	test   %eax,%eax
  8011d2:	7e 17                	jle    8011eb <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8011d4:	83 ec 0c             	sub    $0xc,%esp
  8011d7:	50                   	push   %eax
  8011d8:	6a 0e                	push   $0xe
  8011da:	68 1f 19 80 00       	push   $0x80191f
  8011df:	6a 23                	push   $0x23
  8011e1:	68 3c 19 80 00       	push   $0x80193c
  8011e6:	e8 d8 f3 ff ff       	call   8005c3 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  8011eb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8011ee:	5b                   	pop    %ebx
  8011ef:	5e                   	pop    %esi
  8011f0:	5f                   	pop    %edi
  8011f1:	5d                   	pop    %ebp
  8011f2:	c3                   	ret    

008011f3 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  8011f3:	55                   	push   %ebp
  8011f4:	89 e5                	mov    %esp,%ebp
  8011f6:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  8011f9:	83 3d d0 20 80 00 00 	cmpl   $0x0,0x8020d0
  801200:	75 3e                	jne    801240 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801202:	83 ec 04             	sub    $0x4,%esp
  801205:	6a 07                	push   $0x7
  801207:	68 00 f0 bf ee       	push   $0xeebff000
  80120c:	6a 00                	push   $0x0
  80120e:	e8 d0 fd ff ff       	call   800fe3 <sys_page_alloc>
  801213:	83 c4 10             	add    $0x10,%esp
  801216:	85 c0                	test   %eax,%eax
  801218:	74 14                	je     80122e <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  80121a:	83 ec 04             	sub    $0x4,%esp
  80121d:	68 4c 19 80 00       	push   $0x80194c
  801222:	6a 23                	push   $0x23
  801224:	68 70 19 80 00       	push   $0x801970
  801229:	e8 95 f3 ff ff       	call   8005c3 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  80122e:	83 ec 08             	sub    $0x8,%esp
  801231:	68 4a 12 80 00       	push   $0x80124a
  801236:	6a 00                	push   $0x0
  801238:	e8 10 ff ff ff       	call   80114d <sys_env_set_pgfault_upcall>
  80123d:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801240:	8b 45 08             	mov    0x8(%ebp),%eax
  801243:	a3 d0 20 80 00       	mov    %eax,0x8020d0
}
  801248:	c9                   	leave  
  801249:	c3                   	ret    

0080124a <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  80124a:	54                   	push   %esp
	movl _pgfault_handler, %eax
  80124b:	a1 d0 20 80 00       	mov    0x8020d0,%eax
	call *%eax
  801250:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801252:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801255:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801257:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  80125b:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  80125f:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801260:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801263:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801265:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801268:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  80126b:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  80126c:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  80126f:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801270:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801271:	c3                   	ret    
  801272:	66 90                	xchg   %ax,%ax

00801274 <__udivdi3>:
  801274:	55                   	push   %ebp
  801275:	57                   	push   %edi
  801276:	56                   	push   %esi
  801277:	53                   	push   %ebx
  801278:	83 ec 1c             	sub    $0x1c,%esp
  80127b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  80127f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801283:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801287:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  80128b:	89 ca                	mov    %ecx,%edx
  80128d:	89 f8                	mov    %edi,%eax
  80128f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801293:	85 f6                	test   %esi,%esi
  801295:	75 2d                	jne    8012c4 <__udivdi3+0x50>
  801297:	39 cf                	cmp    %ecx,%edi
  801299:	77 65                	ja     801300 <__udivdi3+0x8c>
  80129b:	89 fd                	mov    %edi,%ebp
  80129d:	85 ff                	test   %edi,%edi
  80129f:	75 0b                	jne    8012ac <__udivdi3+0x38>
  8012a1:	b8 01 00 00 00       	mov    $0x1,%eax
  8012a6:	31 d2                	xor    %edx,%edx
  8012a8:	f7 f7                	div    %edi
  8012aa:	89 c5                	mov    %eax,%ebp
  8012ac:	31 d2                	xor    %edx,%edx
  8012ae:	89 c8                	mov    %ecx,%eax
  8012b0:	f7 f5                	div    %ebp
  8012b2:	89 c1                	mov    %eax,%ecx
  8012b4:	89 d8                	mov    %ebx,%eax
  8012b6:	f7 f5                	div    %ebp
  8012b8:	89 cf                	mov    %ecx,%edi
  8012ba:	89 fa                	mov    %edi,%edx
  8012bc:	83 c4 1c             	add    $0x1c,%esp
  8012bf:	5b                   	pop    %ebx
  8012c0:	5e                   	pop    %esi
  8012c1:	5f                   	pop    %edi
  8012c2:	5d                   	pop    %ebp
  8012c3:	c3                   	ret    
  8012c4:	39 ce                	cmp    %ecx,%esi
  8012c6:	77 28                	ja     8012f0 <__udivdi3+0x7c>
  8012c8:	0f bd fe             	bsr    %esi,%edi
  8012cb:	83 f7 1f             	xor    $0x1f,%edi
  8012ce:	75 40                	jne    801310 <__udivdi3+0x9c>
  8012d0:	39 ce                	cmp    %ecx,%esi
  8012d2:	72 0a                	jb     8012de <__udivdi3+0x6a>
  8012d4:	3b 44 24 08          	cmp    0x8(%esp),%eax
  8012d8:	0f 87 9e 00 00 00    	ja     80137c <__udivdi3+0x108>
  8012de:	b8 01 00 00 00       	mov    $0x1,%eax
  8012e3:	89 fa                	mov    %edi,%edx
  8012e5:	83 c4 1c             	add    $0x1c,%esp
  8012e8:	5b                   	pop    %ebx
  8012e9:	5e                   	pop    %esi
  8012ea:	5f                   	pop    %edi
  8012eb:	5d                   	pop    %ebp
  8012ec:	c3                   	ret    
  8012ed:	8d 76 00             	lea    0x0(%esi),%esi
  8012f0:	31 ff                	xor    %edi,%edi
  8012f2:	31 c0                	xor    %eax,%eax
  8012f4:	89 fa                	mov    %edi,%edx
  8012f6:	83 c4 1c             	add    $0x1c,%esp
  8012f9:	5b                   	pop    %ebx
  8012fa:	5e                   	pop    %esi
  8012fb:	5f                   	pop    %edi
  8012fc:	5d                   	pop    %ebp
  8012fd:	c3                   	ret    
  8012fe:	66 90                	xchg   %ax,%ax
  801300:	89 d8                	mov    %ebx,%eax
  801302:	f7 f7                	div    %edi
  801304:	31 ff                	xor    %edi,%edi
  801306:	89 fa                	mov    %edi,%edx
  801308:	83 c4 1c             	add    $0x1c,%esp
  80130b:	5b                   	pop    %ebx
  80130c:	5e                   	pop    %esi
  80130d:	5f                   	pop    %edi
  80130e:	5d                   	pop    %ebp
  80130f:	c3                   	ret    
  801310:	bd 20 00 00 00       	mov    $0x20,%ebp
  801315:	89 eb                	mov    %ebp,%ebx
  801317:	29 fb                	sub    %edi,%ebx
  801319:	89 f9                	mov    %edi,%ecx
  80131b:	d3 e6                	shl    %cl,%esi
  80131d:	89 c5                	mov    %eax,%ebp
  80131f:	88 d9                	mov    %bl,%cl
  801321:	d3 ed                	shr    %cl,%ebp
  801323:	89 e9                	mov    %ebp,%ecx
  801325:	09 f1                	or     %esi,%ecx
  801327:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80132b:	89 f9                	mov    %edi,%ecx
  80132d:	d3 e0                	shl    %cl,%eax
  80132f:	89 c5                	mov    %eax,%ebp
  801331:	89 d6                	mov    %edx,%esi
  801333:	88 d9                	mov    %bl,%cl
  801335:	d3 ee                	shr    %cl,%esi
  801337:	89 f9                	mov    %edi,%ecx
  801339:	d3 e2                	shl    %cl,%edx
  80133b:	8b 44 24 08          	mov    0x8(%esp),%eax
  80133f:	88 d9                	mov    %bl,%cl
  801341:	d3 e8                	shr    %cl,%eax
  801343:	09 c2                	or     %eax,%edx
  801345:	89 d0                	mov    %edx,%eax
  801347:	89 f2                	mov    %esi,%edx
  801349:	f7 74 24 0c          	divl   0xc(%esp)
  80134d:	89 d6                	mov    %edx,%esi
  80134f:	89 c3                	mov    %eax,%ebx
  801351:	f7 e5                	mul    %ebp
  801353:	39 d6                	cmp    %edx,%esi
  801355:	72 19                	jb     801370 <__udivdi3+0xfc>
  801357:	74 0b                	je     801364 <__udivdi3+0xf0>
  801359:	89 d8                	mov    %ebx,%eax
  80135b:	31 ff                	xor    %edi,%edi
  80135d:	e9 58 ff ff ff       	jmp    8012ba <__udivdi3+0x46>
  801362:	66 90                	xchg   %ax,%ax
  801364:	8b 54 24 08          	mov    0x8(%esp),%edx
  801368:	89 f9                	mov    %edi,%ecx
  80136a:	d3 e2                	shl    %cl,%edx
  80136c:	39 c2                	cmp    %eax,%edx
  80136e:	73 e9                	jae    801359 <__udivdi3+0xe5>
  801370:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801373:	31 ff                	xor    %edi,%edi
  801375:	e9 40 ff ff ff       	jmp    8012ba <__udivdi3+0x46>
  80137a:	66 90                	xchg   %ax,%ax
  80137c:	31 c0                	xor    %eax,%eax
  80137e:	e9 37 ff ff ff       	jmp    8012ba <__udivdi3+0x46>
  801383:	90                   	nop

00801384 <__umoddi3>:
  801384:	55                   	push   %ebp
  801385:	57                   	push   %edi
  801386:	56                   	push   %esi
  801387:	53                   	push   %ebx
  801388:	83 ec 1c             	sub    $0x1c,%esp
  80138b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  80138f:	8b 74 24 34          	mov    0x34(%esp),%esi
  801393:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801397:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  80139b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80139f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8013a3:	89 f3                	mov    %esi,%ebx
  8013a5:	89 fa                	mov    %edi,%edx
  8013a7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8013ab:	89 34 24             	mov    %esi,(%esp)
  8013ae:	85 c0                	test   %eax,%eax
  8013b0:	75 1a                	jne    8013cc <__umoddi3+0x48>
  8013b2:	39 f7                	cmp    %esi,%edi
  8013b4:	0f 86 a2 00 00 00    	jbe    80145c <__umoddi3+0xd8>
  8013ba:	89 c8                	mov    %ecx,%eax
  8013bc:	89 f2                	mov    %esi,%edx
  8013be:	f7 f7                	div    %edi
  8013c0:	89 d0                	mov    %edx,%eax
  8013c2:	31 d2                	xor    %edx,%edx
  8013c4:	83 c4 1c             	add    $0x1c,%esp
  8013c7:	5b                   	pop    %ebx
  8013c8:	5e                   	pop    %esi
  8013c9:	5f                   	pop    %edi
  8013ca:	5d                   	pop    %ebp
  8013cb:	c3                   	ret    
  8013cc:	39 f0                	cmp    %esi,%eax
  8013ce:	0f 87 ac 00 00 00    	ja     801480 <__umoddi3+0xfc>
  8013d4:	0f bd e8             	bsr    %eax,%ebp
  8013d7:	83 f5 1f             	xor    $0x1f,%ebp
  8013da:	0f 84 ac 00 00 00    	je     80148c <__umoddi3+0x108>
  8013e0:	bf 20 00 00 00       	mov    $0x20,%edi
  8013e5:	29 ef                	sub    %ebp,%edi
  8013e7:	89 fe                	mov    %edi,%esi
  8013e9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8013ed:	89 e9                	mov    %ebp,%ecx
  8013ef:	d3 e0                	shl    %cl,%eax
  8013f1:	89 d7                	mov    %edx,%edi
  8013f3:	89 f1                	mov    %esi,%ecx
  8013f5:	d3 ef                	shr    %cl,%edi
  8013f7:	09 c7                	or     %eax,%edi
  8013f9:	89 e9                	mov    %ebp,%ecx
  8013fb:	d3 e2                	shl    %cl,%edx
  8013fd:	89 14 24             	mov    %edx,(%esp)
  801400:	89 d8                	mov    %ebx,%eax
  801402:	d3 e0                	shl    %cl,%eax
  801404:	89 c2                	mov    %eax,%edx
  801406:	8b 44 24 08          	mov    0x8(%esp),%eax
  80140a:	d3 e0                	shl    %cl,%eax
  80140c:	89 44 24 04          	mov    %eax,0x4(%esp)
  801410:	8b 44 24 08          	mov    0x8(%esp),%eax
  801414:	89 f1                	mov    %esi,%ecx
  801416:	d3 e8                	shr    %cl,%eax
  801418:	09 d0                	or     %edx,%eax
  80141a:	d3 eb                	shr    %cl,%ebx
  80141c:	89 da                	mov    %ebx,%edx
  80141e:	f7 f7                	div    %edi
  801420:	89 d3                	mov    %edx,%ebx
  801422:	f7 24 24             	mull   (%esp)
  801425:	89 c6                	mov    %eax,%esi
  801427:	89 d1                	mov    %edx,%ecx
  801429:	39 d3                	cmp    %edx,%ebx
  80142b:	0f 82 87 00 00 00    	jb     8014b8 <__umoddi3+0x134>
  801431:	0f 84 91 00 00 00    	je     8014c8 <__umoddi3+0x144>
  801437:	8b 54 24 04          	mov    0x4(%esp),%edx
  80143b:	29 f2                	sub    %esi,%edx
  80143d:	19 cb                	sbb    %ecx,%ebx
  80143f:	89 d8                	mov    %ebx,%eax
  801441:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801445:	d3 e0                	shl    %cl,%eax
  801447:	89 e9                	mov    %ebp,%ecx
  801449:	d3 ea                	shr    %cl,%edx
  80144b:	09 d0                	or     %edx,%eax
  80144d:	89 e9                	mov    %ebp,%ecx
  80144f:	d3 eb                	shr    %cl,%ebx
  801451:	89 da                	mov    %ebx,%edx
  801453:	83 c4 1c             	add    $0x1c,%esp
  801456:	5b                   	pop    %ebx
  801457:	5e                   	pop    %esi
  801458:	5f                   	pop    %edi
  801459:	5d                   	pop    %ebp
  80145a:	c3                   	ret    
  80145b:	90                   	nop
  80145c:	89 fd                	mov    %edi,%ebp
  80145e:	85 ff                	test   %edi,%edi
  801460:	75 0b                	jne    80146d <__umoddi3+0xe9>
  801462:	b8 01 00 00 00       	mov    $0x1,%eax
  801467:	31 d2                	xor    %edx,%edx
  801469:	f7 f7                	div    %edi
  80146b:	89 c5                	mov    %eax,%ebp
  80146d:	89 f0                	mov    %esi,%eax
  80146f:	31 d2                	xor    %edx,%edx
  801471:	f7 f5                	div    %ebp
  801473:	89 c8                	mov    %ecx,%eax
  801475:	f7 f5                	div    %ebp
  801477:	89 d0                	mov    %edx,%eax
  801479:	e9 44 ff ff ff       	jmp    8013c2 <__umoddi3+0x3e>
  80147e:	66 90                	xchg   %ax,%ax
  801480:	89 c8                	mov    %ecx,%eax
  801482:	89 f2                	mov    %esi,%edx
  801484:	83 c4 1c             	add    $0x1c,%esp
  801487:	5b                   	pop    %ebx
  801488:	5e                   	pop    %esi
  801489:	5f                   	pop    %edi
  80148a:	5d                   	pop    %ebp
  80148b:	c3                   	ret    
  80148c:	3b 04 24             	cmp    (%esp),%eax
  80148f:	72 06                	jb     801497 <__umoddi3+0x113>
  801491:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801495:	77 0f                	ja     8014a6 <__umoddi3+0x122>
  801497:	89 f2                	mov    %esi,%edx
  801499:	29 f9                	sub    %edi,%ecx
  80149b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  80149f:	89 14 24             	mov    %edx,(%esp)
  8014a2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8014a6:	8b 44 24 04          	mov    0x4(%esp),%eax
  8014aa:	8b 14 24             	mov    (%esp),%edx
  8014ad:	83 c4 1c             	add    $0x1c,%esp
  8014b0:	5b                   	pop    %ebx
  8014b1:	5e                   	pop    %esi
  8014b2:	5f                   	pop    %edi
  8014b3:	5d                   	pop    %ebp
  8014b4:	c3                   	ret    
  8014b5:	8d 76 00             	lea    0x0(%esi),%esi
  8014b8:	2b 04 24             	sub    (%esp),%eax
  8014bb:	19 fa                	sbb    %edi,%edx
  8014bd:	89 d1                	mov    %edx,%ecx
  8014bf:	89 c6                	mov    %eax,%esi
  8014c1:	e9 71 ff ff ff       	jmp    801437 <__umoddi3+0xb3>
  8014c6:	66 90                	xchg   %ax,%ax
  8014c8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8014cc:	72 ea                	jb     8014b8 <__umoddi3+0x134>
  8014ce:	89 d9                	mov    %ebx,%ecx
  8014d0:	e9 62 ff ff ff       	jmp    801437 <__umoddi3+0xb3>
