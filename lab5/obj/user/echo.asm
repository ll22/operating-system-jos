
obj/user/echo.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 ad 00 00 00       	call   8000de <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 1c             	sub    $0x1c,%esp
  80003c:	8b 7d 08             	mov    0x8(%ebp),%edi
  80003f:	8b 75 0c             	mov    0xc(%ebp),%esi
	int i, nflag;

	nflag = 0;
	if (argc > 1 && strcmp(argv[1], "-n") == 0) {
  800042:	83 ff 01             	cmp    $0x1,%edi
  800045:	7e 24                	jle    80006b <umain+0x38>
  800047:	83 ec 08             	sub    $0x8,%esp
  80004a:	68 c0 1d 80 00       	push   $0x801dc0
  80004f:	ff 76 04             	pushl  0x4(%esi)
  800052:	e8 bc 01 00 00       	call   800213 <strcmp>
  800057:	83 c4 10             	add    $0x10,%esp
  80005a:	85 c0                	test   %eax,%eax
  80005c:	75 16                	jne    800074 <umain+0x41>
		nflag = 1;
		argc--;
  80005e:	4f                   	dec    %edi
		argv++;
  80005f:	83 c6 04             	add    $0x4,%esi
{
	int i, nflag;

	nflag = 0;
	if (argc > 1 && strcmp(argv[1], "-n") == 0) {
		nflag = 1;
  800062:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
  800069:	eb 10                	jmp    80007b <umain+0x48>
void
umain(int argc, char **argv)
{
	int i, nflag;

	nflag = 0;
  80006b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  800072:	eb 07                	jmp    80007b <umain+0x48>
  800074:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	if (argc > 1 && strcmp(argv[1], "-n") == 0) {
		nflag = 1;
		argc--;
		argv++;
	}
	for (i = 1; i < argc; i++) {
  80007b:	bb 01 00 00 00       	mov    $0x1,%ebx
  800080:	eb 36                	jmp    8000b8 <umain+0x85>
		if (i > 1)
  800082:	83 fb 01             	cmp    $0x1,%ebx
  800085:	7e 14                	jle    80009b <umain+0x68>
			write(1, " ", 1);
  800087:	83 ec 04             	sub    $0x4,%esp
  80008a:	6a 01                	push   $0x1
  80008c:	68 c3 1d 80 00       	push   $0x801dc3
  800091:	6a 01                	push   $0x1
  800093:	e8 7c 0a 00 00       	call   800b14 <write>
  800098:	83 c4 10             	add    $0x10,%esp
		write(1, argv[i], strlen(argv[i]));
  80009b:	83 ec 0c             	sub    $0xc,%esp
  80009e:	ff 34 9e             	pushl  (%esi,%ebx,4)
  8000a1:	e8 99 00 00 00       	call   80013f <strlen>
  8000a6:	83 c4 0c             	add    $0xc,%esp
  8000a9:	50                   	push   %eax
  8000aa:	ff 34 9e             	pushl  (%esi,%ebx,4)
  8000ad:	6a 01                	push   $0x1
  8000af:	e8 60 0a 00 00       	call   800b14 <write>
	if (argc > 1 && strcmp(argv[1], "-n") == 0) {
		nflag = 1;
		argc--;
		argv++;
	}
	for (i = 1; i < argc; i++) {
  8000b4:	43                   	inc    %ebx
  8000b5:	83 c4 10             	add    $0x10,%esp
  8000b8:	39 df                	cmp    %ebx,%edi
  8000ba:	7f c6                	jg     800082 <umain+0x4f>
		if (i > 1)
			write(1, " ", 1);
		write(1, argv[i], strlen(argv[i]));
	}
	if (!nflag)
  8000bc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8000c0:	75 14                	jne    8000d6 <umain+0xa3>
		write(1, "\n", 1);
  8000c2:	83 ec 04             	sub    $0x4,%esp
  8000c5:	6a 01                	push   $0x1
  8000c7:	68 d3 1e 80 00       	push   $0x801ed3
  8000cc:	6a 01                	push   $0x1
  8000ce:	e8 41 0a 00 00       	call   800b14 <write>
  8000d3:	83 c4 10             	add    $0x10,%esp
}
  8000d6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000d9:	5b                   	pop    %ebx
  8000da:	5e                   	pop    %esi
  8000db:	5f                   	pop    %edi
  8000dc:	5d                   	pop    %ebp
  8000dd:	c3                   	ret    

008000de <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000de:	55                   	push   %ebp
  8000df:	89 e5                	mov    %esp,%ebp
  8000e1:	56                   	push   %esi
  8000e2:	53                   	push   %ebx
  8000e3:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000e6:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000e9:	e8 2c 04 00 00       	call   80051a <sys_getenvid>
  8000ee:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000f3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000fa:	c1 e0 07             	shl    $0x7,%eax
  8000fd:	29 d0                	sub    %edx,%eax
  8000ff:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800104:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800109:	85 db                	test   %ebx,%ebx
  80010b:	7e 07                	jle    800114 <libmain+0x36>
		binaryname = argv[0];
  80010d:	8b 06                	mov    (%esi),%eax
  80010f:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  800114:	83 ec 08             	sub    $0x8,%esp
  800117:	56                   	push   %esi
  800118:	53                   	push   %ebx
  800119:	e8 15 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80011e:	e8 0a 00 00 00       	call   80012d <exit>
}
  800123:	83 c4 10             	add    $0x10,%esp
  800126:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800129:	5b                   	pop    %ebx
  80012a:	5e                   	pop    %esi
  80012b:	5d                   	pop    %ebp
  80012c:	c3                   	ret    

0080012d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80012d:	55                   	push   %ebp
  80012e:	89 e5                	mov    %esp,%ebp
  800130:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800133:	6a 00                	push   $0x0
  800135:	e8 9f 03 00 00       	call   8004d9 <sys_env_destroy>
}
  80013a:	83 c4 10             	add    $0x10,%esp
  80013d:	c9                   	leave  
  80013e:	c3                   	ret    

0080013f <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80013f:	55                   	push   %ebp
  800140:	89 e5                	mov    %esp,%ebp
  800142:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800145:	b8 00 00 00 00       	mov    $0x0,%eax
  80014a:	eb 01                	jmp    80014d <strlen+0xe>
		n++;
  80014c:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80014d:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800151:	75 f9                	jne    80014c <strlen+0xd>
		n++;
	return n;
}
  800153:	5d                   	pop    %ebp
  800154:	c3                   	ret    

00800155 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800155:	55                   	push   %ebp
  800156:	89 e5                	mov    %esp,%ebp
  800158:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80015b:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80015e:	ba 00 00 00 00       	mov    $0x0,%edx
  800163:	eb 01                	jmp    800166 <strnlen+0x11>
		n++;
  800165:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800166:	39 c2                	cmp    %eax,%edx
  800168:	74 08                	je     800172 <strnlen+0x1d>
  80016a:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80016e:	75 f5                	jne    800165 <strnlen+0x10>
  800170:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800172:	5d                   	pop    %ebp
  800173:	c3                   	ret    

00800174 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800174:	55                   	push   %ebp
  800175:	89 e5                	mov    %esp,%ebp
  800177:	53                   	push   %ebx
  800178:	8b 45 08             	mov    0x8(%ebp),%eax
  80017b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80017e:	89 c2                	mov    %eax,%edx
  800180:	42                   	inc    %edx
  800181:	41                   	inc    %ecx
  800182:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800185:	88 5a ff             	mov    %bl,-0x1(%edx)
  800188:	84 db                	test   %bl,%bl
  80018a:	75 f4                	jne    800180 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80018c:	5b                   	pop    %ebx
  80018d:	5d                   	pop    %ebp
  80018e:	c3                   	ret    

0080018f <strcat>:

char *
strcat(char *dst, const char *src)
{
  80018f:	55                   	push   %ebp
  800190:	89 e5                	mov    %esp,%ebp
  800192:	53                   	push   %ebx
  800193:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800196:	53                   	push   %ebx
  800197:	e8 a3 ff ff ff       	call   80013f <strlen>
  80019c:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80019f:	ff 75 0c             	pushl  0xc(%ebp)
  8001a2:	01 d8                	add    %ebx,%eax
  8001a4:	50                   	push   %eax
  8001a5:	e8 ca ff ff ff       	call   800174 <strcpy>
	return dst;
}
  8001aa:	89 d8                	mov    %ebx,%eax
  8001ac:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001af:	c9                   	leave  
  8001b0:	c3                   	ret    

008001b1 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8001b1:	55                   	push   %ebp
  8001b2:	89 e5                	mov    %esp,%ebp
  8001b4:	56                   	push   %esi
  8001b5:	53                   	push   %ebx
  8001b6:	8b 75 08             	mov    0x8(%ebp),%esi
  8001b9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001bc:	89 f3                	mov    %esi,%ebx
  8001be:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8001c1:	89 f2                	mov    %esi,%edx
  8001c3:	eb 0c                	jmp    8001d1 <strncpy+0x20>
		*dst++ = *src;
  8001c5:	42                   	inc    %edx
  8001c6:	8a 01                	mov    (%ecx),%al
  8001c8:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8001cb:	80 39 01             	cmpb   $0x1,(%ecx)
  8001ce:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8001d1:	39 da                	cmp    %ebx,%edx
  8001d3:	75 f0                	jne    8001c5 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8001d5:	89 f0                	mov    %esi,%eax
  8001d7:	5b                   	pop    %ebx
  8001d8:	5e                   	pop    %esi
  8001d9:	5d                   	pop    %ebp
  8001da:	c3                   	ret    

008001db <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8001db:	55                   	push   %ebp
  8001dc:	89 e5                	mov    %esp,%ebp
  8001de:	56                   	push   %esi
  8001df:	53                   	push   %ebx
  8001e0:	8b 75 08             	mov    0x8(%ebp),%esi
  8001e3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001e6:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8001e9:	85 c0                	test   %eax,%eax
  8001eb:	74 1e                	je     80020b <strlcpy+0x30>
  8001ed:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8001f1:	89 f2                	mov    %esi,%edx
  8001f3:	eb 05                	jmp    8001fa <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8001f5:	42                   	inc    %edx
  8001f6:	41                   	inc    %ecx
  8001f7:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8001fa:	39 c2                	cmp    %eax,%edx
  8001fc:	74 08                	je     800206 <strlcpy+0x2b>
  8001fe:	8a 19                	mov    (%ecx),%bl
  800200:	84 db                	test   %bl,%bl
  800202:	75 f1                	jne    8001f5 <strlcpy+0x1a>
  800204:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800206:	c6 00 00             	movb   $0x0,(%eax)
  800209:	eb 02                	jmp    80020d <strlcpy+0x32>
  80020b:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80020d:	29 f0                	sub    %esi,%eax
}
  80020f:	5b                   	pop    %ebx
  800210:	5e                   	pop    %esi
  800211:	5d                   	pop    %ebp
  800212:	c3                   	ret    

00800213 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800213:	55                   	push   %ebp
  800214:	89 e5                	mov    %esp,%ebp
  800216:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800219:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80021c:	eb 02                	jmp    800220 <strcmp+0xd>
		p++, q++;
  80021e:	41                   	inc    %ecx
  80021f:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800220:	8a 01                	mov    (%ecx),%al
  800222:	84 c0                	test   %al,%al
  800224:	74 04                	je     80022a <strcmp+0x17>
  800226:	3a 02                	cmp    (%edx),%al
  800228:	74 f4                	je     80021e <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80022a:	0f b6 c0             	movzbl %al,%eax
  80022d:	0f b6 12             	movzbl (%edx),%edx
  800230:	29 d0                	sub    %edx,%eax
}
  800232:	5d                   	pop    %ebp
  800233:	c3                   	ret    

00800234 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800234:	55                   	push   %ebp
  800235:	89 e5                	mov    %esp,%ebp
  800237:	53                   	push   %ebx
  800238:	8b 45 08             	mov    0x8(%ebp),%eax
  80023b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80023e:	89 c3                	mov    %eax,%ebx
  800240:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800243:	eb 02                	jmp    800247 <strncmp+0x13>
		n--, p++, q++;
  800245:	40                   	inc    %eax
  800246:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800247:	39 d8                	cmp    %ebx,%eax
  800249:	74 14                	je     80025f <strncmp+0x2b>
  80024b:	8a 08                	mov    (%eax),%cl
  80024d:	84 c9                	test   %cl,%cl
  80024f:	74 04                	je     800255 <strncmp+0x21>
  800251:	3a 0a                	cmp    (%edx),%cl
  800253:	74 f0                	je     800245 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800255:	0f b6 00             	movzbl (%eax),%eax
  800258:	0f b6 12             	movzbl (%edx),%edx
  80025b:	29 d0                	sub    %edx,%eax
  80025d:	eb 05                	jmp    800264 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80025f:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800264:	5b                   	pop    %ebx
  800265:	5d                   	pop    %ebp
  800266:	c3                   	ret    

00800267 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800267:	55                   	push   %ebp
  800268:	89 e5                	mov    %esp,%ebp
  80026a:	8b 45 08             	mov    0x8(%ebp),%eax
  80026d:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800270:	eb 05                	jmp    800277 <strchr+0x10>
		if (*s == c)
  800272:	38 ca                	cmp    %cl,%dl
  800274:	74 0c                	je     800282 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800276:	40                   	inc    %eax
  800277:	8a 10                	mov    (%eax),%dl
  800279:	84 d2                	test   %dl,%dl
  80027b:	75 f5                	jne    800272 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80027d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800282:	5d                   	pop    %ebp
  800283:	c3                   	ret    

00800284 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800284:	55                   	push   %ebp
  800285:	89 e5                	mov    %esp,%ebp
  800287:	8b 45 08             	mov    0x8(%ebp),%eax
  80028a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80028d:	eb 05                	jmp    800294 <strfind+0x10>
		if (*s == c)
  80028f:	38 ca                	cmp    %cl,%dl
  800291:	74 07                	je     80029a <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800293:	40                   	inc    %eax
  800294:	8a 10                	mov    (%eax),%dl
  800296:	84 d2                	test   %dl,%dl
  800298:	75 f5                	jne    80028f <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80029a:	5d                   	pop    %ebp
  80029b:	c3                   	ret    

0080029c <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80029c:	55                   	push   %ebp
  80029d:	89 e5                	mov    %esp,%ebp
  80029f:	57                   	push   %edi
  8002a0:	56                   	push   %esi
  8002a1:	53                   	push   %ebx
  8002a2:	8b 7d 08             	mov    0x8(%ebp),%edi
  8002a5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8002a8:	85 c9                	test   %ecx,%ecx
  8002aa:	74 36                	je     8002e2 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8002ac:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8002b2:	75 28                	jne    8002dc <memset+0x40>
  8002b4:	f6 c1 03             	test   $0x3,%cl
  8002b7:	75 23                	jne    8002dc <memset+0x40>
		c &= 0xFF;
  8002b9:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8002bd:	89 d3                	mov    %edx,%ebx
  8002bf:	c1 e3 08             	shl    $0x8,%ebx
  8002c2:	89 d6                	mov    %edx,%esi
  8002c4:	c1 e6 18             	shl    $0x18,%esi
  8002c7:	89 d0                	mov    %edx,%eax
  8002c9:	c1 e0 10             	shl    $0x10,%eax
  8002cc:	09 f0                	or     %esi,%eax
  8002ce:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8002d0:	89 d8                	mov    %ebx,%eax
  8002d2:	09 d0                	or     %edx,%eax
  8002d4:	c1 e9 02             	shr    $0x2,%ecx
  8002d7:	fc                   	cld    
  8002d8:	f3 ab                	rep stos %eax,%es:(%edi)
  8002da:	eb 06                	jmp    8002e2 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8002dc:	8b 45 0c             	mov    0xc(%ebp),%eax
  8002df:	fc                   	cld    
  8002e0:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8002e2:	89 f8                	mov    %edi,%eax
  8002e4:	5b                   	pop    %ebx
  8002e5:	5e                   	pop    %esi
  8002e6:	5f                   	pop    %edi
  8002e7:	5d                   	pop    %ebp
  8002e8:	c3                   	ret    

008002e9 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8002e9:	55                   	push   %ebp
  8002ea:	89 e5                	mov    %esp,%ebp
  8002ec:	57                   	push   %edi
  8002ed:	56                   	push   %esi
  8002ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f1:	8b 75 0c             	mov    0xc(%ebp),%esi
  8002f4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8002f7:	39 c6                	cmp    %eax,%esi
  8002f9:	73 33                	jae    80032e <memmove+0x45>
  8002fb:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8002fe:	39 d0                	cmp    %edx,%eax
  800300:	73 2c                	jae    80032e <memmove+0x45>
		s += n;
		d += n;
  800302:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800305:	89 d6                	mov    %edx,%esi
  800307:	09 fe                	or     %edi,%esi
  800309:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80030f:	75 13                	jne    800324 <memmove+0x3b>
  800311:	f6 c1 03             	test   $0x3,%cl
  800314:	75 0e                	jne    800324 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800316:	83 ef 04             	sub    $0x4,%edi
  800319:	8d 72 fc             	lea    -0x4(%edx),%esi
  80031c:	c1 e9 02             	shr    $0x2,%ecx
  80031f:	fd                   	std    
  800320:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800322:	eb 07                	jmp    80032b <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800324:	4f                   	dec    %edi
  800325:	8d 72 ff             	lea    -0x1(%edx),%esi
  800328:	fd                   	std    
  800329:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80032b:	fc                   	cld    
  80032c:	eb 1d                	jmp    80034b <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80032e:	89 f2                	mov    %esi,%edx
  800330:	09 c2                	or     %eax,%edx
  800332:	f6 c2 03             	test   $0x3,%dl
  800335:	75 0f                	jne    800346 <memmove+0x5d>
  800337:	f6 c1 03             	test   $0x3,%cl
  80033a:	75 0a                	jne    800346 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80033c:	c1 e9 02             	shr    $0x2,%ecx
  80033f:	89 c7                	mov    %eax,%edi
  800341:	fc                   	cld    
  800342:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800344:	eb 05                	jmp    80034b <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800346:	89 c7                	mov    %eax,%edi
  800348:	fc                   	cld    
  800349:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80034b:	5e                   	pop    %esi
  80034c:	5f                   	pop    %edi
  80034d:	5d                   	pop    %ebp
  80034e:	c3                   	ret    

0080034f <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80034f:	55                   	push   %ebp
  800350:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800352:	ff 75 10             	pushl  0x10(%ebp)
  800355:	ff 75 0c             	pushl  0xc(%ebp)
  800358:	ff 75 08             	pushl  0x8(%ebp)
  80035b:	e8 89 ff ff ff       	call   8002e9 <memmove>
}
  800360:	c9                   	leave  
  800361:	c3                   	ret    

00800362 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800362:	55                   	push   %ebp
  800363:	89 e5                	mov    %esp,%ebp
  800365:	56                   	push   %esi
  800366:	53                   	push   %ebx
  800367:	8b 45 08             	mov    0x8(%ebp),%eax
  80036a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80036d:	89 c6                	mov    %eax,%esi
  80036f:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800372:	eb 14                	jmp    800388 <memcmp+0x26>
		if (*s1 != *s2)
  800374:	8a 08                	mov    (%eax),%cl
  800376:	8a 1a                	mov    (%edx),%bl
  800378:	38 d9                	cmp    %bl,%cl
  80037a:	74 0a                	je     800386 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80037c:	0f b6 c1             	movzbl %cl,%eax
  80037f:	0f b6 db             	movzbl %bl,%ebx
  800382:	29 d8                	sub    %ebx,%eax
  800384:	eb 0b                	jmp    800391 <memcmp+0x2f>
		s1++, s2++;
  800386:	40                   	inc    %eax
  800387:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800388:	39 f0                	cmp    %esi,%eax
  80038a:	75 e8                	jne    800374 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80038c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800391:	5b                   	pop    %ebx
  800392:	5e                   	pop    %esi
  800393:	5d                   	pop    %ebp
  800394:	c3                   	ret    

00800395 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800395:	55                   	push   %ebp
  800396:	89 e5                	mov    %esp,%ebp
  800398:	53                   	push   %ebx
  800399:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80039c:	89 c1                	mov    %eax,%ecx
  80039e:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8003a1:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8003a5:	eb 08                	jmp    8003af <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8003a7:	0f b6 10             	movzbl (%eax),%edx
  8003aa:	39 da                	cmp    %ebx,%edx
  8003ac:	74 05                	je     8003b3 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8003ae:	40                   	inc    %eax
  8003af:	39 c8                	cmp    %ecx,%eax
  8003b1:	72 f4                	jb     8003a7 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8003b3:	5b                   	pop    %ebx
  8003b4:	5d                   	pop    %ebp
  8003b5:	c3                   	ret    

008003b6 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8003b6:	55                   	push   %ebp
  8003b7:	89 e5                	mov    %esp,%ebp
  8003b9:	57                   	push   %edi
  8003ba:	56                   	push   %esi
  8003bb:	53                   	push   %ebx
  8003bc:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8003bf:	eb 01                	jmp    8003c2 <strtol+0xc>
		s++;
  8003c1:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8003c2:	8a 01                	mov    (%ecx),%al
  8003c4:	3c 20                	cmp    $0x20,%al
  8003c6:	74 f9                	je     8003c1 <strtol+0xb>
  8003c8:	3c 09                	cmp    $0x9,%al
  8003ca:	74 f5                	je     8003c1 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8003cc:	3c 2b                	cmp    $0x2b,%al
  8003ce:	75 08                	jne    8003d8 <strtol+0x22>
		s++;
  8003d0:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8003d1:	bf 00 00 00 00       	mov    $0x0,%edi
  8003d6:	eb 11                	jmp    8003e9 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8003d8:	3c 2d                	cmp    $0x2d,%al
  8003da:	75 08                	jne    8003e4 <strtol+0x2e>
		s++, neg = 1;
  8003dc:	41                   	inc    %ecx
  8003dd:	bf 01 00 00 00       	mov    $0x1,%edi
  8003e2:	eb 05                	jmp    8003e9 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8003e4:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8003e9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8003ed:	0f 84 87 00 00 00    	je     80047a <strtol+0xc4>
  8003f3:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8003f7:	75 27                	jne    800420 <strtol+0x6a>
  8003f9:	80 39 30             	cmpb   $0x30,(%ecx)
  8003fc:	75 22                	jne    800420 <strtol+0x6a>
  8003fe:	e9 88 00 00 00       	jmp    80048b <strtol+0xd5>
		s += 2, base = 16;
  800403:	83 c1 02             	add    $0x2,%ecx
  800406:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80040d:	eb 11                	jmp    800420 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80040f:	41                   	inc    %ecx
  800410:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800417:	eb 07                	jmp    800420 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800419:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800420:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800425:	8a 11                	mov    (%ecx),%dl
  800427:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80042a:	80 fb 09             	cmp    $0x9,%bl
  80042d:	77 08                	ja     800437 <strtol+0x81>
			dig = *s - '0';
  80042f:	0f be d2             	movsbl %dl,%edx
  800432:	83 ea 30             	sub    $0x30,%edx
  800435:	eb 22                	jmp    800459 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800437:	8d 72 9f             	lea    -0x61(%edx),%esi
  80043a:	89 f3                	mov    %esi,%ebx
  80043c:	80 fb 19             	cmp    $0x19,%bl
  80043f:	77 08                	ja     800449 <strtol+0x93>
			dig = *s - 'a' + 10;
  800441:	0f be d2             	movsbl %dl,%edx
  800444:	83 ea 57             	sub    $0x57,%edx
  800447:	eb 10                	jmp    800459 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800449:	8d 72 bf             	lea    -0x41(%edx),%esi
  80044c:	89 f3                	mov    %esi,%ebx
  80044e:	80 fb 19             	cmp    $0x19,%bl
  800451:	77 14                	ja     800467 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800453:	0f be d2             	movsbl %dl,%edx
  800456:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800459:	3b 55 10             	cmp    0x10(%ebp),%edx
  80045c:	7d 09                	jge    800467 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80045e:	41                   	inc    %ecx
  80045f:	0f af 45 10          	imul   0x10(%ebp),%eax
  800463:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800465:	eb be                	jmp    800425 <strtol+0x6f>

	if (endptr)
  800467:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80046b:	74 05                	je     800472 <strtol+0xbc>
		*endptr = (char *) s;
  80046d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800470:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800472:	85 ff                	test   %edi,%edi
  800474:	74 21                	je     800497 <strtol+0xe1>
  800476:	f7 d8                	neg    %eax
  800478:	eb 1d                	jmp    800497 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80047a:	80 39 30             	cmpb   $0x30,(%ecx)
  80047d:	75 9a                	jne    800419 <strtol+0x63>
  80047f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800483:	0f 84 7a ff ff ff    	je     800403 <strtol+0x4d>
  800489:	eb 84                	jmp    80040f <strtol+0x59>
  80048b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  80048f:	0f 84 6e ff ff ff    	je     800403 <strtol+0x4d>
  800495:	eb 89                	jmp    800420 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800497:	5b                   	pop    %ebx
  800498:	5e                   	pop    %esi
  800499:	5f                   	pop    %edi
  80049a:	5d                   	pop    %ebp
  80049b:	c3                   	ret    

0080049c <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80049c:	55                   	push   %ebp
  80049d:	89 e5                	mov    %esp,%ebp
  80049f:	57                   	push   %edi
  8004a0:	56                   	push   %esi
  8004a1:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8004a2:	b8 00 00 00 00       	mov    $0x0,%eax
  8004a7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8004aa:	8b 55 08             	mov    0x8(%ebp),%edx
  8004ad:	89 c3                	mov    %eax,%ebx
  8004af:	89 c7                	mov    %eax,%edi
  8004b1:	89 c6                	mov    %eax,%esi
  8004b3:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8004b5:	5b                   	pop    %ebx
  8004b6:	5e                   	pop    %esi
  8004b7:	5f                   	pop    %edi
  8004b8:	5d                   	pop    %ebp
  8004b9:	c3                   	ret    

008004ba <sys_cgetc>:

int
sys_cgetc(void)
{
  8004ba:	55                   	push   %ebp
  8004bb:	89 e5                	mov    %esp,%ebp
  8004bd:	57                   	push   %edi
  8004be:	56                   	push   %esi
  8004bf:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8004c0:	ba 00 00 00 00       	mov    $0x0,%edx
  8004c5:	b8 01 00 00 00       	mov    $0x1,%eax
  8004ca:	89 d1                	mov    %edx,%ecx
  8004cc:	89 d3                	mov    %edx,%ebx
  8004ce:	89 d7                	mov    %edx,%edi
  8004d0:	89 d6                	mov    %edx,%esi
  8004d2:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8004d4:	5b                   	pop    %ebx
  8004d5:	5e                   	pop    %esi
  8004d6:	5f                   	pop    %edi
  8004d7:	5d                   	pop    %ebp
  8004d8:	c3                   	ret    

008004d9 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8004d9:	55                   	push   %ebp
  8004da:	89 e5                	mov    %esp,%ebp
  8004dc:	57                   	push   %edi
  8004dd:	56                   	push   %esi
  8004de:	53                   	push   %ebx
  8004df:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8004e2:	b9 00 00 00 00       	mov    $0x0,%ecx
  8004e7:	b8 03 00 00 00       	mov    $0x3,%eax
  8004ec:	8b 55 08             	mov    0x8(%ebp),%edx
  8004ef:	89 cb                	mov    %ecx,%ebx
  8004f1:	89 cf                	mov    %ecx,%edi
  8004f3:	89 ce                	mov    %ecx,%esi
  8004f5:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8004f7:	85 c0                	test   %eax,%eax
  8004f9:	7e 17                	jle    800512 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8004fb:	83 ec 0c             	sub    $0xc,%esp
  8004fe:	50                   	push   %eax
  8004ff:	6a 03                	push   $0x3
  800501:	68 cf 1d 80 00       	push   $0x801dcf
  800506:	6a 23                	push   $0x23
  800508:	68 ec 1d 80 00       	push   $0x801dec
  80050d:	e8 f6 0e 00 00       	call   801408 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800512:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800515:	5b                   	pop    %ebx
  800516:	5e                   	pop    %esi
  800517:	5f                   	pop    %edi
  800518:	5d                   	pop    %ebp
  800519:	c3                   	ret    

0080051a <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  80051a:	55                   	push   %ebp
  80051b:	89 e5                	mov    %esp,%ebp
  80051d:	57                   	push   %edi
  80051e:	56                   	push   %esi
  80051f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800520:	ba 00 00 00 00       	mov    $0x0,%edx
  800525:	b8 02 00 00 00       	mov    $0x2,%eax
  80052a:	89 d1                	mov    %edx,%ecx
  80052c:	89 d3                	mov    %edx,%ebx
  80052e:	89 d7                	mov    %edx,%edi
  800530:	89 d6                	mov    %edx,%esi
  800532:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800534:	5b                   	pop    %ebx
  800535:	5e                   	pop    %esi
  800536:	5f                   	pop    %edi
  800537:	5d                   	pop    %ebp
  800538:	c3                   	ret    

00800539 <sys_yield>:

void
sys_yield(void)
{
  800539:	55                   	push   %ebp
  80053a:	89 e5                	mov    %esp,%ebp
  80053c:	57                   	push   %edi
  80053d:	56                   	push   %esi
  80053e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80053f:	ba 00 00 00 00       	mov    $0x0,%edx
  800544:	b8 0b 00 00 00       	mov    $0xb,%eax
  800549:	89 d1                	mov    %edx,%ecx
  80054b:	89 d3                	mov    %edx,%ebx
  80054d:	89 d7                	mov    %edx,%edi
  80054f:	89 d6                	mov    %edx,%esi
  800551:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800553:	5b                   	pop    %ebx
  800554:	5e                   	pop    %esi
  800555:	5f                   	pop    %edi
  800556:	5d                   	pop    %ebp
  800557:	c3                   	ret    

00800558 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800558:	55                   	push   %ebp
  800559:	89 e5                	mov    %esp,%ebp
  80055b:	57                   	push   %edi
  80055c:	56                   	push   %esi
  80055d:	53                   	push   %ebx
  80055e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800561:	be 00 00 00 00       	mov    $0x0,%esi
  800566:	b8 04 00 00 00       	mov    $0x4,%eax
  80056b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80056e:	8b 55 08             	mov    0x8(%ebp),%edx
  800571:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800574:	89 f7                	mov    %esi,%edi
  800576:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800578:	85 c0                	test   %eax,%eax
  80057a:	7e 17                	jle    800593 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80057c:	83 ec 0c             	sub    $0xc,%esp
  80057f:	50                   	push   %eax
  800580:	6a 04                	push   $0x4
  800582:	68 cf 1d 80 00       	push   $0x801dcf
  800587:	6a 23                	push   $0x23
  800589:	68 ec 1d 80 00       	push   $0x801dec
  80058e:	e8 75 0e 00 00       	call   801408 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800593:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800596:	5b                   	pop    %ebx
  800597:	5e                   	pop    %esi
  800598:	5f                   	pop    %edi
  800599:	5d                   	pop    %ebp
  80059a:	c3                   	ret    

0080059b <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  80059b:	55                   	push   %ebp
  80059c:	89 e5                	mov    %esp,%ebp
  80059e:	57                   	push   %edi
  80059f:	56                   	push   %esi
  8005a0:	53                   	push   %ebx
  8005a1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8005a4:	b8 05 00 00 00       	mov    $0x5,%eax
  8005a9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8005ac:	8b 55 08             	mov    0x8(%ebp),%edx
  8005af:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8005b2:	8b 7d 14             	mov    0x14(%ebp),%edi
  8005b5:	8b 75 18             	mov    0x18(%ebp),%esi
  8005b8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8005ba:	85 c0                	test   %eax,%eax
  8005bc:	7e 17                	jle    8005d5 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8005be:	83 ec 0c             	sub    $0xc,%esp
  8005c1:	50                   	push   %eax
  8005c2:	6a 05                	push   $0x5
  8005c4:	68 cf 1d 80 00       	push   $0x801dcf
  8005c9:	6a 23                	push   $0x23
  8005cb:	68 ec 1d 80 00       	push   $0x801dec
  8005d0:	e8 33 0e 00 00       	call   801408 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8005d5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8005d8:	5b                   	pop    %ebx
  8005d9:	5e                   	pop    %esi
  8005da:	5f                   	pop    %edi
  8005db:	5d                   	pop    %ebp
  8005dc:	c3                   	ret    

008005dd <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8005dd:	55                   	push   %ebp
  8005de:	89 e5                	mov    %esp,%ebp
  8005e0:	57                   	push   %edi
  8005e1:	56                   	push   %esi
  8005e2:	53                   	push   %ebx
  8005e3:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8005e6:	bb 00 00 00 00       	mov    $0x0,%ebx
  8005eb:	b8 06 00 00 00       	mov    $0x6,%eax
  8005f0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8005f3:	8b 55 08             	mov    0x8(%ebp),%edx
  8005f6:	89 df                	mov    %ebx,%edi
  8005f8:	89 de                	mov    %ebx,%esi
  8005fa:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8005fc:	85 c0                	test   %eax,%eax
  8005fe:	7e 17                	jle    800617 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800600:	83 ec 0c             	sub    $0xc,%esp
  800603:	50                   	push   %eax
  800604:	6a 06                	push   $0x6
  800606:	68 cf 1d 80 00       	push   $0x801dcf
  80060b:	6a 23                	push   $0x23
  80060d:	68 ec 1d 80 00       	push   $0x801dec
  800612:	e8 f1 0d 00 00       	call   801408 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800617:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80061a:	5b                   	pop    %ebx
  80061b:	5e                   	pop    %esi
  80061c:	5f                   	pop    %edi
  80061d:	5d                   	pop    %ebp
  80061e:	c3                   	ret    

0080061f <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80061f:	55                   	push   %ebp
  800620:	89 e5                	mov    %esp,%ebp
  800622:	57                   	push   %edi
  800623:	56                   	push   %esi
  800624:	53                   	push   %ebx
  800625:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800628:	bb 00 00 00 00       	mov    $0x0,%ebx
  80062d:	b8 08 00 00 00       	mov    $0x8,%eax
  800632:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800635:	8b 55 08             	mov    0x8(%ebp),%edx
  800638:	89 df                	mov    %ebx,%edi
  80063a:	89 de                	mov    %ebx,%esi
  80063c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80063e:	85 c0                	test   %eax,%eax
  800640:	7e 17                	jle    800659 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800642:	83 ec 0c             	sub    $0xc,%esp
  800645:	50                   	push   %eax
  800646:	6a 08                	push   $0x8
  800648:	68 cf 1d 80 00       	push   $0x801dcf
  80064d:	6a 23                	push   $0x23
  80064f:	68 ec 1d 80 00       	push   $0x801dec
  800654:	e8 af 0d 00 00       	call   801408 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800659:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80065c:	5b                   	pop    %ebx
  80065d:	5e                   	pop    %esi
  80065e:	5f                   	pop    %edi
  80065f:	5d                   	pop    %ebp
  800660:	c3                   	ret    

00800661 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800661:	55                   	push   %ebp
  800662:	89 e5                	mov    %esp,%ebp
  800664:	57                   	push   %edi
  800665:	56                   	push   %esi
  800666:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800667:	ba 00 00 00 00       	mov    $0x0,%edx
  80066c:	b8 0c 00 00 00       	mov    $0xc,%eax
  800671:	89 d1                	mov    %edx,%ecx
  800673:	89 d3                	mov    %edx,%ebx
  800675:	89 d7                	mov    %edx,%edi
  800677:	89 d6                	mov    %edx,%esi
  800679:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  80067b:	5b                   	pop    %ebx
  80067c:	5e                   	pop    %esi
  80067d:	5f                   	pop    %edi
  80067e:	5d                   	pop    %ebp
  80067f:	c3                   	ret    

00800680 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800680:	55                   	push   %ebp
  800681:	89 e5                	mov    %esp,%ebp
  800683:	57                   	push   %edi
  800684:	56                   	push   %esi
  800685:	53                   	push   %ebx
  800686:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800689:	bb 00 00 00 00       	mov    $0x0,%ebx
  80068e:	b8 09 00 00 00       	mov    $0x9,%eax
  800693:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800696:	8b 55 08             	mov    0x8(%ebp),%edx
  800699:	89 df                	mov    %ebx,%edi
  80069b:	89 de                	mov    %ebx,%esi
  80069d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80069f:	85 c0                	test   %eax,%eax
  8006a1:	7e 17                	jle    8006ba <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8006a3:	83 ec 0c             	sub    $0xc,%esp
  8006a6:	50                   	push   %eax
  8006a7:	6a 09                	push   $0x9
  8006a9:	68 cf 1d 80 00       	push   $0x801dcf
  8006ae:	6a 23                	push   $0x23
  8006b0:	68 ec 1d 80 00       	push   $0x801dec
  8006b5:	e8 4e 0d 00 00       	call   801408 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8006ba:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006bd:	5b                   	pop    %ebx
  8006be:	5e                   	pop    %esi
  8006bf:	5f                   	pop    %edi
  8006c0:	5d                   	pop    %ebp
  8006c1:	c3                   	ret    

008006c2 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8006c2:	55                   	push   %ebp
  8006c3:	89 e5                	mov    %esp,%ebp
  8006c5:	57                   	push   %edi
  8006c6:	56                   	push   %esi
  8006c7:	53                   	push   %ebx
  8006c8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8006cb:	bb 00 00 00 00       	mov    $0x0,%ebx
  8006d0:	b8 0a 00 00 00       	mov    $0xa,%eax
  8006d5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006d8:	8b 55 08             	mov    0x8(%ebp),%edx
  8006db:	89 df                	mov    %ebx,%edi
  8006dd:	89 de                	mov    %ebx,%esi
  8006df:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8006e1:	85 c0                	test   %eax,%eax
  8006e3:	7e 17                	jle    8006fc <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8006e5:	83 ec 0c             	sub    $0xc,%esp
  8006e8:	50                   	push   %eax
  8006e9:	6a 0a                	push   $0xa
  8006eb:	68 cf 1d 80 00       	push   $0x801dcf
  8006f0:	6a 23                	push   $0x23
  8006f2:	68 ec 1d 80 00       	push   $0x801dec
  8006f7:	e8 0c 0d 00 00       	call   801408 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8006fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006ff:	5b                   	pop    %ebx
  800700:	5e                   	pop    %esi
  800701:	5f                   	pop    %edi
  800702:	5d                   	pop    %ebp
  800703:	c3                   	ret    

00800704 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800704:	55                   	push   %ebp
  800705:	89 e5                	mov    %esp,%ebp
  800707:	57                   	push   %edi
  800708:	56                   	push   %esi
  800709:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80070a:	be 00 00 00 00       	mov    $0x0,%esi
  80070f:	b8 0d 00 00 00       	mov    $0xd,%eax
  800714:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800717:	8b 55 08             	mov    0x8(%ebp),%edx
  80071a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80071d:	8b 7d 14             	mov    0x14(%ebp),%edi
  800720:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800722:	5b                   	pop    %ebx
  800723:	5e                   	pop    %esi
  800724:	5f                   	pop    %edi
  800725:	5d                   	pop    %ebp
  800726:	c3                   	ret    

00800727 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800727:	55                   	push   %ebp
  800728:	89 e5                	mov    %esp,%ebp
  80072a:	57                   	push   %edi
  80072b:	56                   	push   %esi
  80072c:	53                   	push   %ebx
  80072d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800730:	b9 00 00 00 00       	mov    $0x0,%ecx
  800735:	b8 0e 00 00 00       	mov    $0xe,%eax
  80073a:	8b 55 08             	mov    0x8(%ebp),%edx
  80073d:	89 cb                	mov    %ecx,%ebx
  80073f:	89 cf                	mov    %ecx,%edi
  800741:	89 ce                	mov    %ecx,%esi
  800743:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800745:	85 c0                	test   %eax,%eax
  800747:	7e 17                	jle    800760 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800749:	83 ec 0c             	sub    $0xc,%esp
  80074c:	50                   	push   %eax
  80074d:	6a 0e                	push   $0xe
  80074f:	68 cf 1d 80 00       	push   $0x801dcf
  800754:	6a 23                	push   $0x23
  800756:	68 ec 1d 80 00       	push   $0x801dec
  80075b:	e8 a8 0c 00 00       	call   801408 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800760:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800763:	5b                   	pop    %ebx
  800764:	5e                   	pop    %esi
  800765:	5f                   	pop    %edi
  800766:	5d                   	pop    %ebp
  800767:	c3                   	ret    

00800768 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800768:	55                   	push   %ebp
  800769:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  80076b:	8b 45 08             	mov    0x8(%ebp),%eax
  80076e:	05 00 00 00 30       	add    $0x30000000,%eax
  800773:	c1 e8 0c             	shr    $0xc,%eax
}
  800776:	5d                   	pop    %ebp
  800777:	c3                   	ret    

00800778 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  800778:	55                   	push   %ebp
  800779:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  80077b:	8b 45 08             	mov    0x8(%ebp),%eax
  80077e:	05 00 00 00 30       	add    $0x30000000,%eax
  800783:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800788:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  80078d:	5d                   	pop    %ebp
  80078e:	c3                   	ret    

0080078f <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  80078f:	55                   	push   %ebp
  800790:	89 e5                	mov    %esp,%ebp
  800792:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800795:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  80079a:	89 c2                	mov    %eax,%edx
  80079c:	c1 ea 16             	shr    $0x16,%edx
  80079f:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8007a6:	f6 c2 01             	test   $0x1,%dl
  8007a9:	74 11                	je     8007bc <fd_alloc+0x2d>
  8007ab:	89 c2                	mov    %eax,%edx
  8007ad:	c1 ea 0c             	shr    $0xc,%edx
  8007b0:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8007b7:	f6 c2 01             	test   $0x1,%dl
  8007ba:	75 09                	jne    8007c5 <fd_alloc+0x36>
			*fd_store = fd;
  8007bc:	89 01                	mov    %eax,(%ecx)
			return 0;
  8007be:	b8 00 00 00 00       	mov    $0x0,%eax
  8007c3:	eb 17                	jmp    8007dc <fd_alloc+0x4d>
  8007c5:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  8007ca:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  8007cf:	75 c9                	jne    80079a <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  8007d1:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  8007d7:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  8007dc:	5d                   	pop    %ebp
  8007dd:	c3                   	ret    

008007de <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  8007de:	55                   	push   %ebp
  8007df:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  8007e1:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  8007e5:	77 39                	ja     800820 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  8007e7:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ea:	c1 e0 0c             	shl    $0xc,%eax
  8007ed:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  8007f2:	89 c2                	mov    %eax,%edx
  8007f4:	c1 ea 16             	shr    $0x16,%edx
  8007f7:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8007fe:	f6 c2 01             	test   $0x1,%dl
  800801:	74 24                	je     800827 <fd_lookup+0x49>
  800803:	89 c2                	mov    %eax,%edx
  800805:	c1 ea 0c             	shr    $0xc,%edx
  800808:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80080f:	f6 c2 01             	test   $0x1,%dl
  800812:	74 1a                	je     80082e <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  800814:	8b 55 0c             	mov    0xc(%ebp),%edx
  800817:	89 02                	mov    %eax,(%edx)
	return 0;
  800819:	b8 00 00 00 00       	mov    $0x0,%eax
  80081e:	eb 13                	jmp    800833 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800820:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800825:	eb 0c                	jmp    800833 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800827:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80082c:	eb 05                	jmp    800833 <fd_lookup+0x55>
  80082e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  800833:	5d                   	pop    %ebp
  800834:	c3                   	ret    

00800835 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  800835:	55                   	push   %ebp
  800836:	89 e5                	mov    %esp,%ebp
  800838:	83 ec 08             	sub    $0x8,%esp
  80083b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80083e:	ba 78 1e 80 00       	mov    $0x801e78,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  800843:	eb 13                	jmp    800858 <dev_lookup+0x23>
  800845:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  800848:	39 08                	cmp    %ecx,(%eax)
  80084a:	75 0c                	jne    800858 <dev_lookup+0x23>
			*dev = devtab[i];
  80084c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80084f:	89 01                	mov    %eax,(%ecx)
			return 0;
  800851:	b8 00 00 00 00       	mov    $0x0,%eax
  800856:	eb 2e                	jmp    800886 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  800858:	8b 02                	mov    (%edx),%eax
  80085a:	85 c0                	test   %eax,%eax
  80085c:	75 e7                	jne    800845 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  80085e:	a1 04 40 80 00       	mov    0x804004,%eax
  800863:	8b 40 48             	mov    0x48(%eax),%eax
  800866:	83 ec 04             	sub    $0x4,%esp
  800869:	51                   	push   %ecx
  80086a:	50                   	push   %eax
  80086b:	68 fc 1d 80 00       	push   $0x801dfc
  800870:	e8 6b 0c 00 00       	call   8014e0 <cprintf>
	*dev = 0;
  800875:	8b 45 0c             	mov    0xc(%ebp),%eax
  800878:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  80087e:	83 c4 10             	add    $0x10,%esp
  800881:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  800886:	c9                   	leave  
  800887:	c3                   	ret    

00800888 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  800888:	55                   	push   %ebp
  800889:	89 e5                	mov    %esp,%ebp
  80088b:	56                   	push   %esi
  80088c:	53                   	push   %ebx
  80088d:	83 ec 10             	sub    $0x10,%esp
  800890:	8b 75 08             	mov    0x8(%ebp),%esi
  800893:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  800896:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800899:	50                   	push   %eax
  80089a:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  8008a0:	c1 e8 0c             	shr    $0xc,%eax
  8008a3:	50                   	push   %eax
  8008a4:	e8 35 ff ff ff       	call   8007de <fd_lookup>
  8008a9:	83 c4 08             	add    $0x8,%esp
  8008ac:	85 c0                	test   %eax,%eax
  8008ae:	78 05                	js     8008b5 <fd_close+0x2d>
	    || fd != fd2)
  8008b0:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  8008b3:	74 06                	je     8008bb <fd_close+0x33>
		return (must_exist ? r : 0);
  8008b5:	84 db                	test   %bl,%bl
  8008b7:	74 47                	je     800900 <fd_close+0x78>
  8008b9:	eb 4a                	jmp    800905 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  8008bb:	83 ec 08             	sub    $0x8,%esp
  8008be:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8008c1:	50                   	push   %eax
  8008c2:	ff 36                	pushl  (%esi)
  8008c4:	e8 6c ff ff ff       	call   800835 <dev_lookup>
  8008c9:	89 c3                	mov    %eax,%ebx
  8008cb:	83 c4 10             	add    $0x10,%esp
  8008ce:	85 c0                	test   %eax,%eax
  8008d0:	78 1c                	js     8008ee <fd_close+0x66>
		if (dev->dev_close)
  8008d2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8008d5:	8b 40 10             	mov    0x10(%eax),%eax
  8008d8:	85 c0                	test   %eax,%eax
  8008da:	74 0d                	je     8008e9 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  8008dc:	83 ec 0c             	sub    $0xc,%esp
  8008df:	56                   	push   %esi
  8008e0:	ff d0                	call   *%eax
  8008e2:	89 c3                	mov    %eax,%ebx
  8008e4:	83 c4 10             	add    $0x10,%esp
  8008e7:	eb 05                	jmp    8008ee <fd_close+0x66>
		else
			r = 0;
  8008e9:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  8008ee:	83 ec 08             	sub    $0x8,%esp
  8008f1:	56                   	push   %esi
  8008f2:	6a 00                	push   $0x0
  8008f4:	e8 e4 fc ff ff       	call   8005dd <sys_page_unmap>
	return r;
  8008f9:	83 c4 10             	add    $0x10,%esp
  8008fc:	89 d8                	mov    %ebx,%eax
  8008fe:	eb 05                	jmp    800905 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  800900:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  800905:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800908:	5b                   	pop    %ebx
  800909:	5e                   	pop    %esi
  80090a:	5d                   	pop    %ebp
  80090b:	c3                   	ret    

0080090c <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  80090c:	55                   	push   %ebp
  80090d:	89 e5                	mov    %esp,%ebp
  80090f:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800912:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800915:	50                   	push   %eax
  800916:	ff 75 08             	pushl  0x8(%ebp)
  800919:	e8 c0 fe ff ff       	call   8007de <fd_lookup>
  80091e:	83 c4 08             	add    $0x8,%esp
  800921:	85 c0                	test   %eax,%eax
  800923:	78 10                	js     800935 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  800925:	83 ec 08             	sub    $0x8,%esp
  800928:	6a 01                	push   $0x1
  80092a:	ff 75 f4             	pushl  -0xc(%ebp)
  80092d:	e8 56 ff ff ff       	call   800888 <fd_close>
  800932:	83 c4 10             	add    $0x10,%esp
}
  800935:	c9                   	leave  
  800936:	c3                   	ret    

00800937 <close_all>:

void
close_all(void)
{
  800937:	55                   	push   %ebp
  800938:	89 e5                	mov    %esp,%ebp
  80093a:	53                   	push   %ebx
  80093b:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  80093e:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  800943:	83 ec 0c             	sub    $0xc,%esp
  800946:	53                   	push   %ebx
  800947:	e8 c0 ff ff ff       	call   80090c <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  80094c:	43                   	inc    %ebx
  80094d:	83 c4 10             	add    $0x10,%esp
  800950:	83 fb 20             	cmp    $0x20,%ebx
  800953:	75 ee                	jne    800943 <close_all+0xc>
		close(i);
}
  800955:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800958:	c9                   	leave  
  800959:	c3                   	ret    

0080095a <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  80095a:	55                   	push   %ebp
  80095b:	89 e5                	mov    %esp,%ebp
  80095d:	57                   	push   %edi
  80095e:	56                   	push   %esi
  80095f:	53                   	push   %ebx
  800960:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  800963:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  800966:	50                   	push   %eax
  800967:	ff 75 08             	pushl  0x8(%ebp)
  80096a:	e8 6f fe ff ff       	call   8007de <fd_lookup>
  80096f:	83 c4 08             	add    $0x8,%esp
  800972:	85 c0                	test   %eax,%eax
  800974:	0f 88 c2 00 00 00    	js     800a3c <dup+0xe2>
		return r;
	close(newfdnum);
  80097a:	83 ec 0c             	sub    $0xc,%esp
  80097d:	ff 75 0c             	pushl  0xc(%ebp)
  800980:	e8 87 ff ff ff       	call   80090c <close>

	newfd = INDEX2FD(newfdnum);
  800985:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800988:	c1 e3 0c             	shl    $0xc,%ebx
  80098b:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  800991:	83 c4 04             	add    $0x4,%esp
  800994:	ff 75 e4             	pushl  -0x1c(%ebp)
  800997:	e8 dc fd ff ff       	call   800778 <fd2data>
  80099c:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  80099e:	89 1c 24             	mov    %ebx,(%esp)
  8009a1:	e8 d2 fd ff ff       	call   800778 <fd2data>
  8009a6:	83 c4 10             	add    $0x10,%esp
  8009a9:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8009ab:	89 f0                	mov    %esi,%eax
  8009ad:	c1 e8 16             	shr    $0x16,%eax
  8009b0:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8009b7:	a8 01                	test   $0x1,%al
  8009b9:	74 35                	je     8009f0 <dup+0x96>
  8009bb:	89 f0                	mov    %esi,%eax
  8009bd:	c1 e8 0c             	shr    $0xc,%eax
  8009c0:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  8009c7:	f6 c2 01             	test   $0x1,%dl
  8009ca:	74 24                	je     8009f0 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  8009cc:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8009d3:	83 ec 0c             	sub    $0xc,%esp
  8009d6:	25 07 0e 00 00       	and    $0xe07,%eax
  8009db:	50                   	push   %eax
  8009dc:	57                   	push   %edi
  8009dd:	6a 00                	push   $0x0
  8009df:	56                   	push   %esi
  8009e0:	6a 00                	push   $0x0
  8009e2:	e8 b4 fb ff ff       	call   80059b <sys_page_map>
  8009e7:	89 c6                	mov    %eax,%esi
  8009e9:	83 c4 20             	add    $0x20,%esp
  8009ec:	85 c0                	test   %eax,%eax
  8009ee:	78 2c                	js     800a1c <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8009f0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8009f3:	89 d0                	mov    %edx,%eax
  8009f5:	c1 e8 0c             	shr    $0xc,%eax
  8009f8:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8009ff:	83 ec 0c             	sub    $0xc,%esp
  800a02:	25 07 0e 00 00       	and    $0xe07,%eax
  800a07:	50                   	push   %eax
  800a08:	53                   	push   %ebx
  800a09:	6a 00                	push   $0x0
  800a0b:	52                   	push   %edx
  800a0c:	6a 00                	push   $0x0
  800a0e:	e8 88 fb ff ff       	call   80059b <sys_page_map>
  800a13:	89 c6                	mov    %eax,%esi
  800a15:	83 c4 20             	add    $0x20,%esp
  800a18:	85 c0                	test   %eax,%eax
  800a1a:	79 1d                	jns    800a39 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  800a1c:	83 ec 08             	sub    $0x8,%esp
  800a1f:	53                   	push   %ebx
  800a20:	6a 00                	push   $0x0
  800a22:	e8 b6 fb ff ff       	call   8005dd <sys_page_unmap>
	sys_page_unmap(0, nva);
  800a27:	83 c4 08             	add    $0x8,%esp
  800a2a:	57                   	push   %edi
  800a2b:	6a 00                	push   $0x0
  800a2d:	e8 ab fb ff ff       	call   8005dd <sys_page_unmap>
	return r;
  800a32:	83 c4 10             	add    $0x10,%esp
  800a35:	89 f0                	mov    %esi,%eax
  800a37:	eb 03                	jmp    800a3c <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  800a39:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  800a3c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a3f:	5b                   	pop    %ebx
  800a40:	5e                   	pop    %esi
  800a41:	5f                   	pop    %edi
  800a42:	5d                   	pop    %ebp
  800a43:	c3                   	ret    

00800a44 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  800a44:	55                   	push   %ebp
  800a45:	89 e5                	mov    %esp,%ebp
  800a47:	53                   	push   %ebx
  800a48:	83 ec 14             	sub    $0x14,%esp
  800a4b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  800a4e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800a51:	50                   	push   %eax
  800a52:	53                   	push   %ebx
  800a53:	e8 86 fd ff ff       	call   8007de <fd_lookup>
  800a58:	83 c4 08             	add    $0x8,%esp
  800a5b:	85 c0                	test   %eax,%eax
  800a5d:	78 67                	js     800ac6 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  800a5f:	83 ec 08             	sub    $0x8,%esp
  800a62:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800a65:	50                   	push   %eax
  800a66:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800a69:	ff 30                	pushl  (%eax)
  800a6b:	e8 c5 fd ff ff       	call   800835 <dev_lookup>
  800a70:	83 c4 10             	add    $0x10,%esp
  800a73:	85 c0                	test   %eax,%eax
  800a75:	78 4f                	js     800ac6 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  800a77:	8b 55 f0             	mov    -0x10(%ebp),%edx
  800a7a:	8b 42 08             	mov    0x8(%edx),%eax
  800a7d:	83 e0 03             	and    $0x3,%eax
  800a80:	83 f8 01             	cmp    $0x1,%eax
  800a83:	75 21                	jne    800aa6 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  800a85:	a1 04 40 80 00       	mov    0x804004,%eax
  800a8a:	8b 40 48             	mov    0x48(%eax),%eax
  800a8d:	83 ec 04             	sub    $0x4,%esp
  800a90:	53                   	push   %ebx
  800a91:	50                   	push   %eax
  800a92:	68 3d 1e 80 00       	push   $0x801e3d
  800a97:	e8 44 0a 00 00       	call   8014e0 <cprintf>
		return -E_INVAL;
  800a9c:	83 c4 10             	add    $0x10,%esp
  800a9f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800aa4:	eb 20                	jmp    800ac6 <read+0x82>
	}
	if (!dev->dev_read)
  800aa6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800aa9:	8b 40 08             	mov    0x8(%eax),%eax
  800aac:	85 c0                	test   %eax,%eax
  800aae:	74 11                	je     800ac1 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  800ab0:	83 ec 04             	sub    $0x4,%esp
  800ab3:	ff 75 10             	pushl  0x10(%ebp)
  800ab6:	ff 75 0c             	pushl  0xc(%ebp)
  800ab9:	52                   	push   %edx
  800aba:	ff d0                	call   *%eax
  800abc:	83 c4 10             	add    $0x10,%esp
  800abf:	eb 05                	jmp    800ac6 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  800ac1:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  800ac6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ac9:	c9                   	leave  
  800aca:	c3                   	ret    

00800acb <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  800acb:	55                   	push   %ebp
  800acc:	89 e5                	mov    %esp,%ebp
  800ace:	57                   	push   %edi
  800acf:	56                   	push   %esi
  800ad0:	53                   	push   %ebx
  800ad1:	83 ec 0c             	sub    $0xc,%esp
  800ad4:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ad7:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  800ada:	bb 00 00 00 00       	mov    $0x0,%ebx
  800adf:	eb 21                	jmp    800b02 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  800ae1:	83 ec 04             	sub    $0x4,%esp
  800ae4:	89 f0                	mov    %esi,%eax
  800ae6:	29 d8                	sub    %ebx,%eax
  800ae8:	50                   	push   %eax
  800ae9:	89 d8                	mov    %ebx,%eax
  800aeb:	03 45 0c             	add    0xc(%ebp),%eax
  800aee:	50                   	push   %eax
  800aef:	57                   	push   %edi
  800af0:	e8 4f ff ff ff       	call   800a44 <read>
		if (m < 0)
  800af5:	83 c4 10             	add    $0x10,%esp
  800af8:	85 c0                	test   %eax,%eax
  800afa:	78 10                	js     800b0c <readn+0x41>
			return m;
		if (m == 0)
  800afc:	85 c0                	test   %eax,%eax
  800afe:	74 0a                	je     800b0a <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  800b00:	01 c3                	add    %eax,%ebx
  800b02:	39 f3                	cmp    %esi,%ebx
  800b04:	72 db                	jb     800ae1 <readn+0x16>
  800b06:	89 d8                	mov    %ebx,%eax
  800b08:	eb 02                	jmp    800b0c <readn+0x41>
  800b0a:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  800b0c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b0f:	5b                   	pop    %ebx
  800b10:	5e                   	pop    %esi
  800b11:	5f                   	pop    %edi
  800b12:	5d                   	pop    %ebp
  800b13:	c3                   	ret    

00800b14 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  800b14:	55                   	push   %ebp
  800b15:	89 e5                	mov    %esp,%ebp
  800b17:	53                   	push   %ebx
  800b18:	83 ec 14             	sub    $0x14,%esp
  800b1b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  800b1e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800b21:	50                   	push   %eax
  800b22:	53                   	push   %ebx
  800b23:	e8 b6 fc ff ff       	call   8007de <fd_lookup>
  800b28:	83 c4 08             	add    $0x8,%esp
  800b2b:	85 c0                	test   %eax,%eax
  800b2d:	78 62                	js     800b91 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  800b2f:	83 ec 08             	sub    $0x8,%esp
  800b32:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800b35:	50                   	push   %eax
  800b36:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800b39:	ff 30                	pushl  (%eax)
  800b3b:	e8 f5 fc ff ff       	call   800835 <dev_lookup>
  800b40:	83 c4 10             	add    $0x10,%esp
  800b43:	85 c0                	test   %eax,%eax
  800b45:	78 4a                	js     800b91 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  800b47:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800b4a:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  800b4e:	75 21                	jne    800b71 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  800b50:	a1 04 40 80 00       	mov    0x804004,%eax
  800b55:	8b 40 48             	mov    0x48(%eax),%eax
  800b58:	83 ec 04             	sub    $0x4,%esp
  800b5b:	53                   	push   %ebx
  800b5c:	50                   	push   %eax
  800b5d:	68 59 1e 80 00       	push   $0x801e59
  800b62:	e8 79 09 00 00       	call   8014e0 <cprintf>
		return -E_INVAL;
  800b67:	83 c4 10             	add    $0x10,%esp
  800b6a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800b6f:	eb 20                	jmp    800b91 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  800b71:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800b74:	8b 52 0c             	mov    0xc(%edx),%edx
  800b77:	85 d2                	test   %edx,%edx
  800b79:	74 11                	je     800b8c <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  800b7b:	83 ec 04             	sub    $0x4,%esp
  800b7e:	ff 75 10             	pushl  0x10(%ebp)
  800b81:	ff 75 0c             	pushl  0xc(%ebp)
  800b84:	50                   	push   %eax
  800b85:	ff d2                	call   *%edx
  800b87:	83 c4 10             	add    $0x10,%esp
  800b8a:	eb 05                	jmp    800b91 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  800b8c:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  800b91:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800b94:	c9                   	leave  
  800b95:	c3                   	ret    

00800b96 <seek>:

int
seek(int fdnum, off_t offset)
{
  800b96:	55                   	push   %ebp
  800b97:	89 e5                	mov    %esp,%ebp
  800b99:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800b9c:	8d 45 fc             	lea    -0x4(%ebp),%eax
  800b9f:	50                   	push   %eax
  800ba0:	ff 75 08             	pushl  0x8(%ebp)
  800ba3:	e8 36 fc ff ff       	call   8007de <fd_lookup>
  800ba8:	83 c4 08             	add    $0x8,%esp
  800bab:	85 c0                	test   %eax,%eax
  800bad:	78 0e                	js     800bbd <seek+0x27>
		return r;
	fd->fd_offset = offset;
  800baf:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bb2:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bb5:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  800bb8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bbd:	c9                   	leave  
  800bbe:	c3                   	ret    

00800bbf <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  800bbf:	55                   	push   %ebp
  800bc0:	89 e5                	mov    %esp,%ebp
  800bc2:	53                   	push   %ebx
  800bc3:	83 ec 14             	sub    $0x14,%esp
  800bc6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  800bc9:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800bcc:	50                   	push   %eax
  800bcd:	53                   	push   %ebx
  800bce:	e8 0b fc ff ff       	call   8007de <fd_lookup>
  800bd3:	83 c4 08             	add    $0x8,%esp
  800bd6:	85 c0                	test   %eax,%eax
  800bd8:	78 5f                	js     800c39 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  800bda:	83 ec 08             	sub    $0x8,%esp
  800bdd:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800be0:	50                   	push   %eax
  800be1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800be4:	ff 30                	pushl  (%eax)
  800be6:	e8 4a fc ff ff       	call   800835 <dev_lookup>
  800beb:	83 c4 10             	add    $0x10,%esp
  800bee:	85 c0                	test   %eax,%eax
  800bf0:	78 47                	js     800c39 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  800bf2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800bf5:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  800bf9:	75 21                	jne    800c1c <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  800bfb:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  800c00:	8b 40 48             	mov    0x48(%eax),%eax
  800c03:	83 ec 04             	sub    $0x4,%esp
  800c06:	53                   	push   %ebx
  800c07:	50                   	push   %eax
  800c08:	68 1c 1e 80 00       	push   $0x801e1c
  800c0d:	e8 ce 08 00 00       	call   8014e0 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  800c12:	83 c4 10             	add    $0x10,%esp
  800c15:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800c1a:	eb 1d                	jmp    800c39 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  800c1c:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800c1f:	8b 52 18             	mov    0x18(%edx),%edx
  800c22:	85 d2                	test   %edx,%edx
  800c24:	74 0e                	je     800c34 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  800c26:	83 ec 08             	sub    $0x8,%esp
  800c29:	ff 75 0c             	pushl  0xc(%ebp)
  800c2c:	50                   	push   %eax
  800c2d:	ff d2                	call   *%edx
  800c2f:	83 c4 10             	add    $0x10,%esp
  800c32:	eb 05                	jmp    800c39 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  800c34:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  800c39:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800c3c:	c9                   	leave  
  800c3d:	c3                   	ret    

00800c3e <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  800c3e:	55                   	push   %ebp
  800c3f:	89 e5                	mov    %esp,%ebp
  800c41:	53                   	push   %ebx
  800c42:	83 ec 14             	sub    $0x14,%esp
  800c45:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  800c48:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800c4b:	50                   	push   %eax
  800c4c:	ff 75 08             	pushl  0x8(%ebp)
  800c4f:	e8 8a fb ff ff       	call   8007de <fd_lookup>
  800c54:	83 c4 08             	add    $0x8,%esp
  800c57:	85 c0                	test   %eax,%eax
  800c59:	78 52                	js     800cad <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  800c5b:	83 ec 08             	sub    $0x8,%esp
  800c5e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800c61:	50                   	push   %eax
  800c62:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800c65:	ff 30                	pushl  (%eax)
  800c67:	e8 c9 fb ff ff       	call   800835 <dev_lookup>
  800c6c:	83 c4 10             	add    $0x10,%esp
  800c6f:	85 c0                	test   %eax,%eax
  800c71:	78 3a                	js     800cad <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  800c73:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800c76:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  800c7a:	74 2c                	je     800ca8 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  800c7c:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  800c7f:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  800c86:	00 00 00 
	stat->st_isdir = 0;
  800c89:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  800c90:	00 00 00 
	stat->st_dev = dev;
  800c93:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  800c99:	83 ec 08             	sub    $0x8,%esp
  800c9c:	53                   	push   %ebx
  800c9d:	ff 75 f0             	pushl  -0x10(%ebp)
  800ca0:	ff 50 14             	call   *0x14(%eax)
  800ca3:	83 c4 10             	add    $0x10,%esp
  800ca6:	eb 05                	jmp    800cad <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  800ca8:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  800cad:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800cb0:	c9                   	leave  
  800cb1:	c3                   	ret    

00800cb2 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  800cb2:	55                   	push   %ebp
  800cb3:	89 e5                	mov    %esp,%ebp
  800cb5:	56                   	push   %esi
  800cb6:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  800cb7:	83 ec 08             	sub    $0x8,%esp
  800cba:	6a 00                	push   $0x0
  800cbc:	ff 75 08             	pushl  0x8(%ebp)
  800cbf:	e8 e7 01 00 00       	call   800eab <open>
  800cc4:	89 c3                	mov    %eax,%ebx
  800cc6:	83 c4 10             	add    $0x10,%esp
  800cc9:	85 c0                	test   %eax,%eax
  800ccb:	78 1d                	js     800cea <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  800ccd:	83 ec 08             	sub    $0x8,%esp
  800cd0:	ff 75 0c             	pushl  0xc(%ebp)
  800cd3:	50                   	push   %eax
  800cd4:	e8 65 ff ff ff       	call   800c3e <fstat>
  800cd9:	89 c6                	mov    %eax,%esi
	close(fd);
  800cdb:	89 1c 24             	mov    %ebx,(%esp)
  800cde:	e8 29 fc ff ff       	call   80090c <close>
	return r;
  800ce3:	83 c4 10             	add    $0x10,%esp
  800ce6:	89 f0                	mov    %esi,%eax
  800ce8:	eb 00                	jmp    800cea <stat+0x38>
}
  800cea:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800ced:	5b                   	pop    %ebx
  800cee:	5e                   	pop    %esi
  800cef:	5d                   	pop    %ebp
  800cf0:	c3                   	ret    

00800cf1 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  800cf1:	55                   	push   %ebp
  800cf2:	89 e5                	mov    %esp,%ebp
  800cf4:	56                   	push   %esi
  800cf5:	53                   	push   %ebx
  800cf6:	89 c6                	mov    %eax,%esi
  800cf8:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  800cfa:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  800d01:	75 12                	jne    800d15 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  800d03:	83 ec 0c             	sub    $0xc,%esp
  800d06:	6a 01                	push   $0x1
  800d08:	e8 bf 0d 00 00       	call   801acc <ipc_find_env>
  800d0d:	a3 00 40 80 00       	mov    %eax,0x804000
  800d12:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  800d15:	6a 07                	push   $0x7
  800d17:	68 00 50 80 00       	push   $0x805000
  800d1c:	56                   	push   %esi
  800d1d:	ff 35 00 40 80 00    	pushl  0x804000
  800d23:	e8 4f 0d 00 00       	call   801a77 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  800d28:	83 c4 0c             	add    $0xc,%esp
  800d2b:	6a 00                	push   $0x0
  800d2d:	53                   	push   %ebx
  800d2e:	6a 00                	push   $0x0
  800d30:	e8 da 0c 00 00       	call   801a0f <ipc_recv>
}
  800d35:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800d38:	5b                   	pop    %ebx
  800d39:	5e                   	pop    %esi
  800d3a:	5d                   	pop    %ebp
  800d3b:	c3                   	ret    

00800d3c <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  800d3c:	55                   	push   %ebp
  800d3d:	89 e5                	mov    %esp,%ebp
  800d3f:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  800d42:	8b 45 08             	mov    0x8(%ebp),%eax
  800d45:	8b 40 0c             	mov    0xc(%eax),%eax
  800d48:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  800d4d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d50:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  800d55:	ba 00 00 00 00       	mov    $0x0,%edx
  800d5a:	b8 02 00 00 00       	mov    $0x2,%eax
  800d5f:	e8 8d ff ff ff       	call   800cf1 <fsipc>
}
  800d64:	c9                   	leave  
  800d65:	c3                   	ret    

00800d66 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  800d66:	55                   	push   %ebp
  800d67:	89 e5                	mov    %esp,%ebp
  800d69:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  800d6c:	8b 45 08             	mov    0x8(%ebp),%eax
  800d6f:	8b 40 0c             	mov    0xc(%eax),%eax
  800d72:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  800d77:	ba 00 00 00 00       	mov    $0x0,%edx
  800d7c:	b8 06 00 00 00       	mov    $0x6,%eax
  800d81:	e8 6b ff ff ff       	call   800cf1 <fsipc>
}
  800d86:	c9                   	leave  
  800d87:	c3                   	ret    

00800d88 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  800d88:	55                   	push   %ebp
  800d89:	89 e5                	mov    %esp,%ebp
  800d8b:	53                   	push   %ebx
  800d8c:	83 ec 04             	sub    $0x4,%esp
  800d8f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  800d92:	8b 45 08             	mov    0x8(%ebp),%eax
  800d95:	8b 40 0c             	mov    0xc(%eax),%eax
  800d98:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  800d9d:	ba 00 00 00 00       	mov    $0x0,%edx
  800da2:	b8 05 00 00 00       	mov    $0x5,%eax
  800da7:	e8 45 ff ff ff       	call   800cf1 <fsipc>
  800dac:	85 c0                	test   %eax,%eax
  800dae:	78 2c                	js     800ddc <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  800db0:	83 ec 08             	sub    $0x8,%esp
  800db3:	68 00 50 80 00       	push   $0x805000
  800db8:	53                   	push   %ebx
  800db9:	e8 b6 f3 ff ff       	call   800174 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  800dbe:	a1 80 50 80 00       	mov    0x805080,%eax
  800dc3:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  800dc9:	a1 84 50 80 00       	mov    0x805084,%eax
  800dce:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  800dd4:	83 c4 10             	add    $0x10,%esp
  800dd7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ddc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ddf:	c9                   	leave  
  800de0:	c3                   	ret    

00800de1 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  800de1:	55                   	push   %ebp
  800de2:	89 e5                	mov    %esp,%ebp
  800de4:	83 ec 08             	sub    $0x8,%esp
  800de7:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  800dea:	8b 55 08             	mov    0x8(%ebp),%edx
  800ded:	8b 52 0c             	mov    0xc(%edx),%edx
  800df0:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  800df6:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  800dfb:	76 05                	jbe    800e02 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  800dfd:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  800e02:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  800e07:	83 ec 04             	sub    $0x4,%esp
  800e0a:	50                   	push   %eax
  800e0b:	ff 75 0c             	pushl  0xc(%ebp)
  800e0e:	68 08 50 80 00       	push   $0x805008
  800e13:	e8 d1 f4 ff ff       	call   8002e9 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  800e18:	ba 00 00 00 00       	mov    $0x0,%edx
  800e1d:	b8 04 00 00 00       	mov    $0x4,%eax
  800e22:	e8 ca fe ff ff       	call   800cf1 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  800e27:	c9                   	leave  
  800e28:	c3                   	ret    

00800e29 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  800e29:	55                   	push   %ebp
  800e2a:	89 e5                	mov    %esp,%ebp
  800e2c:	56                   	push   %esi
  800e2d:	53                   	push   %ebx
  800e2e:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  800e31:	8b 45 08             	mov    0x8(%ebp),%eax
  800e34:	8b 40 0c             	mov    0xc(%eax),%eax
  800e37:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  800e3c:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  800e42:	ba 00 00 00 00       	mov    $0x0,%edx
  800e47:	b8 03 00 00 00       	mov    $0x3,%eax
  800e4c:	e8 a0 fe ff ff       	call   800cf1 <fsipc>
  800e51:	89 c3                	mov    %eax,%ebx
  800e53:	85 c0                	test   %eax,%eax
  800e55:	78 4b                	js     800ea2 <devfile_read+0x79>
		return r;
	assert(r <= n);
  800e57:	39 c6                	cmp    %eax,%esi
  800e59:	73 16                	jae    800e71 <devfile_read+0x48>
  800e5b:	68 88 1e 80 00       	push   $0x801e88
  800e60:	68 8f 1e 80 00       	push   $0x801e8f
  800e65:	6a 7c                	push   $0x7c
  800e67:	68 a4 1e 80 00       	push   $0x801ea4
  800e6c:	e8 97 05 00 00       	call   801408 <_panic>
	assert(r <= PGSIZE);
  800e71:	3d 00 10 00 00       	cmp    $0x1000,%eax
  800e76:	7e 16                	jle    800e8e <devfile_read+0x65>
  800e78:	68 af 1e 80 00       	push   $0x801eaf
  800e7d:	68 8f 1e 80 00       	push   $0x801e8f
  800e82:	6a 7d                	push   $0x7d
  800e84:	68 a4 1e 80 00       	push   $0x801ea4
  800e89:	e8 7a 05 00 00       	call   801408 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  800e8e:	83 ec 04             	sub    $0x4,%esp
  800e91:	50                   	push   %eax
  800e92:	68 00 50 80 00       	push   $0x805000
  800e97:	ff 75 0c             	pushl  0xc(%ebp)
  800e9a:	e8 4a f4 ff ff       	call   8002e9 <memmove>
	return r;
  800e9f:	83 c4 10             	add    $0x10,%esp
}
  800ea2:	89 d8                	mov    %ebx,%eax
  800ea4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800ea7:	5b                   	pop    %ebx
  800ea8:	5e                   	pop    %esi
  800ea9:	5d                   	pop    %ebp
  800eaa:	c3                   	ret    

00800eab <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  800eab:	55                   	push   %ebp
  800eac:	89 e5                	mov    %esp,%ebp
  800eae:	53                   	push   %ebx
  800eaf:	83 ec 20             	sub    $0x20,%esp
  800eb2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  800eb5:	53                   	push   %ebx
  800eb6:	e8 84 f2 ff ff       	call   80013f <strlen>
  800ebb:	83 c4 10             	add    $0x10,%esp
  800ebe:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  800ec3:	7f 63                	jg     800f28 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  800ec5:	83 ec 0c             	sub    $0xc,%esp
  800ec8:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800ecb:	50                   	push   %eax
  800ecc:	e8 be f8 ff ff       	call   80078f <fd_alloc>
  800ed1:	83 c4 10             	add    $0x10,%esp
  800ed4:	85 c0                	test   %eax,%eax
  800ed6:	78 55                	js     800f2d <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  800ed8:	83 ec 08             	sub    $0x8,%esp
  800edb:	53                   	push   %ebx
  800edc:	68 00 50 80 00       	push   $0x805000
  800ee1:	e8 8e f2 ff ff       	call   800174 <strcpy>
	fsipcbuf.open.req_omode = mode;
  800ee6:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ee9:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  800eee:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800ef1:	b8 01 00 00 00       	mov    $0x1,%eax
  800ef6:	e8 f6 fd ff ff       	call   800cf1 <fsipc>
  800efb:	89 c3                	mov    %eax,%ebx
  800efd:	83 c4 10             	add    $0x10,%esp
  800f00:	85 c0                	test   %eax,%eax
  800f02:	79 14                	jns    800f18 <open+0x6d>
		fd_close(fd, 0);
  800f04:	83 ec 08             	sub    $0x8,%esp
  800f07:	6a 00                	push   $0x0
  800f09:	ff 75 f4             	pushl  -0xc(%ebp)
  800f0c:	e8 77 f9 ff ff       	call   800888 <fd_close>
		return r;
  800f11:	83 c4 10             	add    $0x10,%esp
  800f14:	89 d8                	mov    %ebx,%eax
  800f16:	eb 15                	jmp    800f2d <open+0x82>
	}

	return fd2num(fd);
  800f18:	83 ec 0c             	sub    $0xc,%esp
  800f1b:	ff 75 f4             	pushl  -0xc(%ebp)
  800f1e:	e8 45 f8 ff ff       	call   800768 <fd2num>
  800f23:	83 c4 10             	add    $0x10,%esp
  800f26:	eb 05                	jmp    800f2d <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  800f28:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  800f2d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800f30:	c9                   	leave  
  800f31:	c3                   	ret    

00800f32 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  800f32:	55                   	push   %ebp
  800f33:	89 e5                	mov    %esp,%ebp
  800f35:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  800f38:	ba 00 00 00 00       	mov    $0x0,%edx
  800f3d:	b8 08 00 00 00       	mov    $0x8,%eax
  800f42:	e8 aa fd ff ff       	call   800cf1 <fsipc>
}
  800f47:	c9                   	leave  
  800f48:	c3                   	ret    

00800f49 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  800f49:	55                   	push   %ebp
  800f4a:	89 e5                	mov    %esp,%ebp
  800f4c:	56                   	push   %esi
  800f4d:	53                   	push   %ebx
  800f4e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  800f51:	83 ec 0c             	sub    $0xc,%esp
  800f54:	ff 75 08             	pushl  0x8(%ebp)
  800f57:	e8 1c f8 ff ff       	call   800778 <fd2data>
  800f5c:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  800f5e:	83 c4 08             	add    $0x8,%esp
  800f61:	68 bb 1e 80 00       	push   $0x801ebb
  800f66:	53                   	push   %ebx
  800f67:	e8 08 f2 ff ff       	call   800174 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  800f6c:	8b 46 04             	mov    0x4(%esi),%eax
  800f6f:	2b 06                	sub    (%esi),%eax
  800f71:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  800f77:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  800f7e:	00 00 00 
	stat->st_dev = &devpipe;
  800f81:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  800f88:	30 80 00 
	return 0;
}
  800f8b:	b8 00 00 00 00       	mov    $0x0,%eax
  800f90:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800f93:	5b                   	pop    %ebx
  800f94:	5e                   	pop    %esi
  800f95:	5d                   	pop    %ebp
  800f96:	c3                   	ret    

00800f97 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  800f97:	55                   	push   %ebp
  800f98:	89 e5                	mov    %esp,%ebp
  800f9a:	53                   	push   %ebx
  800f9b:	83 ec 0c             	sub    $0xc,%esp
  800f9e:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  800fa1:	53                   	push   %ebx
  800fa2:	6a 00                	push   $0x0
  800fa4:	e8 34 f6 ff ff       	call   8005dd <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  800fa9:	89 1c 24             	mov    %ebx,(%esp)
  800fac:	e8 c7 f7 ff ff       	call   800778 <fd2data>
  800fb1:	83 c4 08             	add    $0x8,%esp
  800fb4:	50                   	push   %eax
  800fb5:	6a 00                	push   $0x0
  800fb7:	e8 21 f6 ff ff       	call   8005dd <sys_page_unmap>
}
  800fbc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800fbf:	c9                   	leave  
  800fc0:	c3                   	ret    

00800fc1 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  800fc1:	55                   	push   %ebp
  800fc2:	89 e5                	mov    %esp,%ebp
  800fc4:	57                   	push   %edi
  800fc5:	56                   	push   %esi
  800fc6:	53                   	push   %ebx
  800fc7:	83 ec 1c             	sub    $0x1c,%esp
  800fca:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800fcd:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  800fcf:	a1 04 40 80 00       	mov    0x804004,%eax
  800fd4:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  800fd7:	83 ec 0c             	sub    $0xc,%esp
  800fda:	ff 75 e0             	pushl  -0x20(%ebp)
  800fdd:	e8 2e 0b 00 00       	call   801b10 <pageref>
  800fe2:	89 c3                	mov    %eax,%ebx
  800fe4:	89 3c 24             	mov    %edi,(%esp)
  800fe7:	e8 24 0b 00 00       	call   801b10 <pageref>
  800fec:	83 c4 10             	add    $0x10,%esp
  800fef:	39 c3                	cmp    %eax,%ebx
  800ff1:	0f 94 c1             	sete   %cl
  800ff4:	0f b6 c9             	movzbl %cl,%ecx
  800ff7:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  800ffa:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801000:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801003:	39 ce                	cmp    %ecx,%esi
  801005:	74 1b                	je     801022 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801007:	39 c3                	cmp    %eax,%ebx
  801009:	75 c4                	jne    800fcf <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  80100b:	8b 42 58             	mov    0x58(%edx),%eax
  80100e:	ff 75 e4             	pushl  -0x1c(%ebp)
  801011:	50                   	push   %eax
  801012:	56                   	push   %esi
  801013:	68 c2 1e 80 00       	push   $0x801ec2
  801018:	e8 c3 04 00 00       	call   8014e0 <cprintf>
  80101d:	83 c4 10             	add    $0x10,%esp
  801020:	eb ad                	jmp    800fcf <_pipeisclosed+0xe>
	}
}
  801022:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801025:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801028:	5b                   	pop    %ebx
  801029:	5e                   	pop    %esi
  80102a:	5f                   	pop    %edi
  80102b:	5d                   	pop    %ebp
  80102c:	c3                   	ret    

0080102d <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  80102d:	55                   	push   %ebp
  80102e:	89 e5                	mov    %esp,%ebp
  801030:	57                   	push   %edi
  801031:	56                   	push   %esi
  801032:	53                   	push   %ebx
  801033:	83 ec 18             	sub    $0x18,%esp
  801036:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801039:	56                   	push   %esi
  80103a:	e8 39 f7 ff ff       	call   800778 <fd2data>
  80103f:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801041:	83 c4 10             	add    $0x10,%esp
  801044:	bf 00 00 00 00       	mov    $0x0,%edi
  801049:	eb 3b                	jmp    801086 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  80104b:	89 da                	mov    %ebx,%edx
  80104d:	89 f0                	mov    %esi,%eax
  80104f:	e8 6d ff ff ff       	call   800fc1 <_pipeisclosed>
  801054:	85 c0                	test   %eax,%eax
  801056:	75 38                	jne    801090 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801058:	e8 dc f4 ff ff       	call   800539 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  80105d:	8b 53 04             	mov    0x4(%ebx),%edx
  801060:	8b 03                	mov    (%ebx),%eax
  801062:	83 c0 20             	add    $0x20,%eax
  801065:	39 c2                	cmp    %eax,%edx
  801067:	73 e2                	jae    80104b <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801069:	8b 45 0c             	mov    0xc(%ebp),%eax
  80106c:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  80106f:	89 d0                	mov    %edx,%eax
  801071:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801076:	79 05                	jns    80107d <devpipe_write+0x50>
  801078:	48                   	dec    %eax
  801079:	83 c8 e0             	or     $0xffffffe0,%eax
  80107c:	40                   	inc    %eax
  80107d:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801081:	42                   	inc    %edx
  801082:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801085:	47                   	inc    %edi
  801086:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801089:	75 d2                	jne    80105d <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  80108b:	8b 45 10             	mov    0x10(%ebp),%eax
  80108e:	eb 05                	jmp    801095 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801090:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801095:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801098:	5b                   	pop    %ebx
  801099:	5e                   	pop    %esi
  80109a:	5f                   	pop    %edi
  80109b:	5d                   	pop    %ebp
  80109c:	c3                   	ret    

0080109d <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  80109d:	55                   	push   %ebp
  80109e:	89 e5                	mov    %esp,%ebp
  8010a0:	57                   	push   %edi
  8010a1:	56                   	push   %esi
  8010a2:	53                   	push   %ebx
  8010a3:	83 ec 18             	sub    $0x18,%esp
  8010a6:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  8010a9:	57                   	push   %edi
  8010aa:	e8 c9 f6 ff ff       	call   800778 <fd2data>
  8010af:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8010b1:	83 c4 10             	add    $0x10,%esp
  8010b4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8010b9:	eb 3a                	jmp    8010f5 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  8010bb:	85 db                	test   %ebx,%ebx
  8010bd:	74 04                	je     8010c3 <devpipe_read+0x26>
				return i;
  8010bf:	89 d8                	mov    %ebx,%eax
  8010c1:	eb 41                	jmp    801104 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  8010c3:	89 f2                	mov    %esi,%edx
  8010c5:	89 f8                	mov    %edi,%eax
  8010c7:	e8 f5 fe ff ff       	call   800fc1 <_pipeisclosed>
  8010cc:	85 c0                	test   %eax,%eax
  8010ce:	75 2f                	jne    8010ff <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  8010d0:	e8 64 f4 ff ff       	call   800539 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  8010d5:	8b 06                	mov    (%esi),%eax
  8010d7:	3b 46 04             	cmp    0x4(%esi),%eax
  8010da:	74 df                	je     8010bb <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  8010dc:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8010e1:	79 05                	jns    8010e8 <devpipe_read+0x4b>
  8010e3:	48                   	dec    %eax
  8010e4:	83 c8 e0             	or     $0xffffffe0,%eax
  8010e7:	40                   	inc    %eax
  8010e8:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  8010ec:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8010ef:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  8010f2:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8010f4:	43                   	inc    %ebx
  8010f5:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  8010f8:	75 db                	jne    8010d5 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  8010fa:	8b 45 10             	mov    0x10(%ebp),%eax
  8010fd:	eb 05                	jmp    801104 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8010ff:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801104:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801107:	5b                   	pop    %ebx
  801108:	5e                   	pop    %esi
  801109:	5f                   	pop    %edi
  80110a:	5d                   	pop    %ebp
  80110b:	c3                   	ret    

0080110c <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  80110c:	55                   	push   %ebp
  80110d:	89 e5                	mov    %esp,%ebp
  80110f:	56                   	push   %esi
  801110:	53                   	push   %ebx
  801111:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801114:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801117:	50                   	push   %eax
  801118:	e8 72 f6 ff ff       	call   80078f <fd_alloc>
  80111d:	83 c4 10             	add    $0x10,%esp
  801120:	85 c0                	test   %eax,%eax
  801122:	0f 88 2a 01 00 00    	js     801252 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801128:	83 ec 04             	sub    $0x4,%esp
  80112b:	68 07 04 00 00       	push   $0x407
  801130:	ff 75 f4             	pushl  -0xc(%ebp)
  801133:	6a 00                	push   $0x0
  801135:	e8 1e f4 ff ff       	call   800558 <sys_page_alloc>
  80113a:	83 c4 10             	add    $0x10,%esp
  80113d:	85 c0                	test   %eax,%eax
  80113f:	0f 88 0d 01 00 00    	js     801252 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801145:	83 ec 0c             	sub    $0xc,%esp
  801148:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80114b:	50                   	push   %eax
  80114c:	e8 3e f6 ff ff       	call   80078f <fd_alloc>
  801151:	89 c3                	mov    %eax,%ebx
  801153:	83 c4 10             	add    $0x10,%esp
  801156:	85 c0                	test   %eax,%eax
  801158:	0f 88 e2 00 00 00    	js     801240 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80115e:	83 ec 04             	sub    $0x4,%esp
  801161:	68 07 04 00 00       	push   $0x407
  801166:	ff 75 f0             	pushl  -0x10(%ebp)
  801169:	6a 00                	push   $0x0
  80116b:	e8 e8 f3 ff ff       	call   800558 <sys_page_alloc>
  801170:	89 c3                	mov    %eax,%ebx
  801172:	83 c4 10             	add    $0x10,%esp
  801175:	85 c0                	test   %eax,%eax
  801177:	0f 88 c3 00 00 00    	js     801240 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  80117d:	83 ec 0c             	sub    $0xc,%esp
  801180:	ff 75 f4             	pushl  -0xc(%ebp)
  801183:	e8 f0 f5 ff ff       	call   800778 <fd2data>
  801188:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80118a:	83 c4 0c             	add    $0xc,%esp
  80118d:	68 07 04 00 00       	push   $0x407
  801192:	50                   	push   %eax
  801193:	6a 00                	push   $0x0
  801195:	e8 be f3 ff ff       	call   800558 <sys_page_alloc>
  80119a:	89 c3                	mov    %eax,%ebx
  80119c:	83 c4 10             	add    $0x10,%esp
  80119f:	85 c0                	test   %eax,%eax
  8011a1:	0f 88 89 00 00 00    	js     801230 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8011a7:	83 ec 0c             	sub    $0xc,%esp
  8011aa:	ff 75 f0             	pushl  -0x10(%ebp)
  8011ad:	e8 c6 f5 ff ff       	call   800778 <fd2data>
  8011b2:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  8011b9:	50                   	push   %eax
  8011ba:	6a 00                	push   $0x0
  8011bc:	56                   	push   %esi
  8011bd:	6a 00                	push   $0x0
  8011bf:	e8 d7 f3 ff ff       	call   80059b <sys_page_map>
  8011c4:	89 c3                	mov    %eax,%ebx
  8011c6:	83 c4 20             	add    $0x20,%esp
  8011c9:	85 c0                	test   %eax,%eax
  8011cb:	78 55                	js     801222 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  8011cd:	8b 15 20 30 80 00    	mov    0x803020,%edx
  8011d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8011d6:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  8011d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8011db:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  8011e2:	8b 15 20 30 80 00    	mov    0x803020,%edx
  8011e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011eb:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  8011ed:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011f0:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  8011f7:	83 ec 0c             	sub    $0xc,%esp
  8011fa:	ff 75 f4             	pushl  -0xc(%ebp)
  8011fd:	e8 66 f5 ff ff       	call   800768 <fd2num>
  801202:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801205:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801207:	83 c4 04             	add    $0x4,%esp
  80120a:	ff 75 f0             	pushl  -0x10(%ebp)
  80120d:	e8 56 f5 ff ff       	call   800768 <fd2num>
  801212:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801215:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801218:	83 c4 10             	add    $0x10,%esp
  80121b:	b8 00 00 00 00       	mov    $0x0,%eax
  801220:	eb 30                	jmp    801252 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801222:	83 ec 08             	sub    $0x8,%esp
  801225:	56                   	push   %esi
  801226:	6a 00                	push   $0x0
  801228:	e8 b0 f3 ff ff       	call   8005dd <sys_page_unmap>
  80122d:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801230:	83 ec 08             	sub    $0x8,%esp
  801233:	ff 75 f0             	pushl  -0x10(%ebp)
  801236:	6a 00                	push   $0x0
  801238:	e8 a0 f3 ff ff       	call   8005dd <sys_page_unmap>
  80123d:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801240:	83 ec 08             	sub    $0x8,%esp
  801243:	ff 75 f4             	pushl  -0xc(%ebp)
  801246:	6a 00                	push   $0x0
  801248:	e8 90 f3 ff ff       	call   8005dd <sys_page_unmap>
  80124d:	83 c4 10             	add    $0x10,%esp
  801250:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801252:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801255:	5b                   	pop    %ebx
  801256:	5e                   	pop    %esi
  801257:	5d                   	pop    %ebp
  801258:	c3                   	ret    

00801259 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801259:	55                   	push   %ebp
  80125a:	89 e5                	mov    %esp,%ebp
  80125c:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80125f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801262:	50                   	push   %eax
  801263:	ff 75 08             	pushl  0x8(%ebp)
  801266:	e8 73 f5 ff ff       	call   8007de <fd_lookup>
  80126b:	83 c4 10             	add    $0x10,%esp
  80126e:	85 c0                	test   %eax,%eax
  801270:	78 18                	js     80128a <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801272:	83 ec 0c             	sub    $0xc,%esp
  801275:	ff 75 f4             	pushl  -0xc(%ebp)
  801278:	e8 fb f4 ff ff       	call   800778 <fd2data>
	return _pipeisclosed(fd, p);
  80127d:	89 c2                	mov    %eax,%edx
  80127f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801282:	e8 3a fd ff ff       	call   800fc1 <_pipeisclosed>
  801287:	83 c4 10             	add    $0x10,%esp
}
  80128a:	c9                   	leave  
  80128b:	c3                   	ret    

0080128c <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  80128c:	55                   	push   %ebp
  80128d:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  80128f:	b8 00 00 00 00       	mov    $0x0,%eax
  801294:	5d                   	pop    %ebp
  801295:	c3                   	ret    

00801296 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801296:	55                   	push   %ebp
  801297:	89 e5                	mov    %esp,%ebp
  801299:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  80129c:	68 da 1e 80 00       	push   $0x801eda
  8012a1:	ff 75 0c             	pushl  0xc(%ebp)
  8012a4:	e8 cb ee ff ff       	call   800174 <strcpy>
	return 0;
}
  8012a9:	b8 00 00 00 00       	mov    $0x0,%eax
  8012ae:	c9                   	leave  
  8012af:	c3                   	ret    

008012b0 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8012b0:	55                   	push   %ebp
  8012b1:	89 e5                	mov    %esp,%ebp
  8012b3:	57                   	push   %edi
  8012b4:	56                   	push   %esi
  8012b5:	53                   	push   %ebx
  8012b6:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  8012bc:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  8012c1:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  8012c7:	eb 2c                	jmp    8012f5 <devcons_write+0x45>
		m = n - tot;
  8012c9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8012cc:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  8012ce:	83 fb 7f             	cmp    $0x7f,%ebx
  8012d1:	76 05                	jbe    8012d8 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  8012d3:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  8012d8:	83 ec 04             	sub    $0x4,%esp
  8012db:	53                   	push   %ebx
  8012dc:	03 45 0c             	add    0xc(%ebp),%eax
  8012df:	50                   	push   %eax
  8012e0:	57                   	push   %edi
  8012e1:	e8 03 f0 ff ff       	call   8002e9 <memmove>
		sys_cputs(buf, m);
  8012e6:	83 c4 08             	add    $0x8,%esp
  8012e9:	53                   	push   %ebx
  8012ea:	57                   	push   %edi
  8012eb:	e8 ac f1 ff ff       	call   80049c <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  8012f0:	01 de                	add    %ebx,%esi
  8012f2:	83 c4 10             	add    $0x10,%esp
  8012f5:	89 f0                	mov    %esi,%eax
  8012f7:	3b 75 10             	cmp    0x10(%ebp),%esi
  8012fa:	72 cd                	jb     8012c9 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  8012fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8012ff:	5b                   	pop    %ebx
  801300:	5e                   	pop    %esi
  801301:	5f                   	pop    %edi
  801302:	5d                   	pop    %ebp
  801303:	c3                   	ret    

00801304 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801304:	55                   	push   %ebp
  801305:	89 e5                	mov    %esp,%ebp
  801307:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  80130a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80130e:	75 07                	jne    801317 <devcons_read+0x13>
  801310:	eb 23                	jmp    801335 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801312:	e8 22 f2 ff ff       	call   800539 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801317:	e8 9e f1 ff ff       	call   8004ba <sys_cgetc>
  80131c:	85 c0                	test   %eax,%eax
  80131e:	74 f2                	je     801312 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801320:	85 c0                	test   %eax,%eax
  801322:	78 1d                	js     801341 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801324:	83 f8 04             	cmp    $0x4,%eax
  801327:	74 13                	je     80133c <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801329:	8b 55 0c             	mov    0xc(%ebp),%edx
  80132c:	88 02                	mov    %al,(%edx)
	return 1;
  80132e:	b8 01 00 00 00       	mov    $0x1,%eax
  801333:	eb 0c                	jmp    801341 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801335:	b8 00 00 00 00       	mov    $0x0,%eax
  80133a:	eb 05                	jmp    801341 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  80133c:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801341:	c9                   	leave  
  801342:	c3                   	ret    

00801343 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801343:	55                   	push   %ebp
  801344:	89 e5                	mov    %esp,%ebp
  801346:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801349:	8b 45 08             	mov    0x8(%ebp),%eax
  80134c:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  80134f:	6a 01                	push   $0x1
  801351:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801354:	50                   	push   %eax
  801355:	e8 42 f1 ff ff       	call   80049c <sys_cputs>
}
  80135a:	83 c4 10             	add    $0x10,%esp
  80135d:	c9                   	leave  
  80135e:	c3                   	ret    

0080135f <getchar>:

int
getchar(void)
{
  80135f:	55                   	push   %ebp
  801360:	89 e5                	mov    %esp,%ebp
  801362:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801365:	6a 01                	push   $0x1
  801367:	8d 45 f7             	lea    -0x9(%ebp),%eax
  80136a:	50                   	push   %eax
  80136b:	6a 00                	push   $0x0
  80136d:	e8 d2 f6 ff ff       	call   800a44 <read>
	if (r < 0)
  801372:	83 c4 10             	add    $0x10,%esp
  801375:	85 c0                	test   %eax,%eax
  801377:	78 0f                	js     801388 <getchar+0x29>
		return r;
	if (r < 1)
  801379:	85 c0                	test   %eax,%eax
  80137b:	7e 06                	jle    801383 <getchar+0x24>
		return -E_EOF;
	return c;
  80137d:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801381:	eb 05                	jmp    801388 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801383:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801388:	c9                   	leave  
  801389:	c3                   	ret    

0080138a <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  80138a:	55                   	push   %ebp
  80138b:	89 e5                	mov    %esp,%ebp
  80138d:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801390:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801393:	50                   	push   %eax
  801394:	ff 75 08             	pushl  0x8(%ebp)
  801397:	e8 42 f4 ff ff       	call   8007de <fd_lookup>
  80139c:	83 c4 10             	add    $0x10,%esp
  80139f:	85 c0                	test   %eax,%eax
  8013a1:	78 11                	js     8013b4 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  8013a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8013a6:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  8013ac:	39 10                	cmp    %edx,(%eax)
  8013ae:	0f 94 c0             	sete   %al
  8013b1:	0f b6 c0             	movzbl %al,%eax
}
  8013b4:	c9                   	leave  
  8013b5:	c3                   	ret    

008013b6 <opencons>:

int
opencons(void)
{
  8013b6:	55                   	push   %ebp
  8013b7:	89 e5                	mov    %esp,%ebp
  8013b9:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  8013bc:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013bf:	50                   	push   %eax
  8013c0:	e8 ca f3 ff ff       	call   80078f <fd_alloc>
  8013c5:	83 c4 10             	add    $0x10,%esp
  8013c8:	85 c0                	test   %eax,%eax
  8013ca:	78 3a                	js     801406 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  8013cc:	83 ec 04             	sub    $0x4,%esp
  8013cf:	68 07 04 00 00       	push   $0x407
  8013d4:	ff 75 f4             	pushl  -0xc(%ebp)
  8013d7:	6a 00                	push   $0x0
  8013d9:	e8 7a f1 ff ff       	call   800558 <sys_page_alloc>
  8013de:	83 c4 10             	add    $0x10,%esp
  8013e1:	85 c0                	test   %eax,%eax
  8013e3:	78 21                	js     801406 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  8013e5:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  8013eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8013ee:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  8013f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8013f3:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  8013fa:	83 ec 0c             	sub    $0xc,%esp
  8013fd:	50                   	push   %eax
  8013fe:	e8 65 f3 ff ff       	call   800768 <fd2num>
  801403:	83 c4 10             	add    $0x10,%esp
}
  801406:	c9                   	leave  
  801407:	c3                   	ret    

00801408 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  801408:	55                   	push   %ebp
  801409:	89 e5                	mov    %esp,%ebp
  80140b:	56                   	push   %esi
  80140c:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80140d:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  801410:	8b 35 00 30 80 00    	mov    0x803000,%esi
  801416:	e8 ff f0 ff ff       	call   80051a <sys_getenvid>
  80141b:	83 ec 0c             	sub    $0xc,%esp
  80141e:	ff 75 0c             	pushl  0xc(%ebp)
  801421:	ff 75 08             	pushl  0x8(%ebp)
  801424:	56                   	push   %esi
  801425:	50                   	push   %eax
  801426:	68 e8 1e 80 00       	push   $0x801ee8
  80142b:	e8 b0 00 00 00       	call   8014e0 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  801430:	83 c4 18             	add    $0x18,%esp
  801433:	53                   	push   %ebx
  801434:	ff 75 10             	pushl  0x10(%ebp)
  801437:	e8 53 00 00 00       	call   80148f <vcprintf>
	cprintf("\n");
  80143c:	c7 04 24 d3 1e 80 00 	movl   $0x801ed3,(%esp)
  801443:	e8 98 00 00 00       	call   8014e0 <cprintf>
  801448:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80144b:	cc                   	int3   
  80144c:	eb fd                	jmp    80144b <_panic+0x43>

0080144e <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80144e:	55                   	push   %ebp
  80144f:	89 e5                	mov    %esp,%ebp
  801451:	53                   	push   %ebx
  801452:	83 ec 04             	sub    $0x4,%esp
  801455:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  801458:	8b 13                	mov    (%ebx),%edx
  80145a:	8d 42 01             	lea    0x1(%edx),%eax
  80145d:	89 03                	mov    %eax,(%ebx)
  80145f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801462:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  801466:	3d ff 00 00 00       	cmp    $0xff,%eax
  80146b:	75 1a                	jne    801487 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80146d:	83 ec 08             	sub    $0x8,%esp
  801470:	68 ff 00 00 00       	push   $0xff
  801475:	8d 43 08             	lea    0x8(%ebx),%eax
  801478:	50                   	push   %eax
  801479:	e8 1e f0 ff ff       	call   80049c <sys_cputs>
		b->idx = 0;
  80147e:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801484:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  801487:	ff 43 04             	incl   0x4(%ebx)
}
  80148a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80148d:	c9                   	leave  
  80148e:	c3                   	ret    

0080148f <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80148f:	55                   	push   %ebp
  801490:	89 e5                	mov    %esp,%ebp
  801492:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  801498:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80149f:	00 00 00 
	b.cnt = 0;
  8014a2:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8014a9:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8014ac:	ff 75 0c             	pushl  0xc(%ebp)
  8014af:	ff 75 08             	pushl  0x8(%ebp)
  8014b2:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8014b8:	50                   	push   %eax
  8014b9:	68 4e 14 80 00       	push   $0x80144e
  8014be:	e8 51 01 00 00       	call   801614 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8014c3:	83 c4 08             	add    $0x8,%esp
  8014c6:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8014cc:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8014d2:	50                   	push   %eax
  8014d3:	e8 c4 ef ff ff       	call   80049c <sys_cputs>

	return b.cnt;
}
  8014d8:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8014de:	c9                   	leave  
  8014df:	c3                   	ret    

008014e0 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8014e0:	55                   	push   %ebp
  8014e1:	89 e5                	mov    %esp,%ebp
  8014e3:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8014e6:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8014e9:	50                   	push   %eax
  8014ea:	ff 75 08             	pushl  0x8(%ebp)
  8014ed:	e8 9d ff ff ff       	call   80148f <vcprintf>
	va_end(ap);

	return cnt;
}
  8014f2:	c9                   	leave  
  8014f3:	c3                   	ret    

008014f4 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8014f4:	55                   	push   %ebp
  8014f5:	89 e5                	mov    %esp,%ebp
  8014f7:	57                   	push   %edi
  8014f8:	56                   	push   %esi
  8014f9:	53                   	push   %ebx
  8014fa:	83 ec 1c             	sub    $0x1c,%esp
  8014fd:	89 c7                	mov    %eax,%edi
  8014ff:	89 d6                	mov    %edx,%esi
  801501:	8b 45 08             	mov    0x8(%ebp),%eax
  801504:	8b 55 0c             	mov    0xc(%ebp),%edx
  801507:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80150a:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80150d:	8b 4d 10             	mov    0x10(%ebp),%ecx
  801510:	bb 00 00 00 00       	mov    $0x0,%ebx
  801515:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  801518:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80151b:	39 d3                	cmp    %edx,%ebx
  80151d:	72 05                	jb     801524 <printnum+0x30>
  80151f:	39 45 10             	cmp    %eax,0x10(%ebp)
  801522:	77 45                	ja     801569 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  801524:	83 ec 0c             	sub    $0xc,%esp
  801527:	ff 75 18             	pushl  0x18(%ebp)
  80152a:	8b 45 14             	mov    0x14(%ebp),%eax
  80152d:	8d 58 ff             	lea    -0x1(%eax),%ebx
  801530:	53                   	push   %ebx
  801531:	ff 75 10             	pushl  0x10(%ebp)
  801534:	83 ec 08             	sub    $0x8,%esp
  801537:	ff 75 e4             	pushl  -0x1c(%ebp)
  80153a:	ff 75 e0             	pushl  -0x20(%ebp)
  80153d:	ff 75 dc             	pushl  -0x24(%ebp)
  801540:	ff 75 d8             	pushl  -0x28(%ebp)
  801543:	e8 0c 06 00 00       	call   801b54 <__udivdi3>
  801548:	83 c4 18             	add    $0x18,%esp
  80154b:	52                   	push   %edx
  80154c:	50                   	push   %eax
  80154d:	89 f2                	mov    %esi,%edx
  80154f:	89 f8                	mov    %edi,%eax
  801551:	e8 9e ff ff ff       	call   8014f4 <printnum>
  801556:	83 c4 20             	add    $0x20,%esp
  801559:	eb 16                	jmp    801571 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80155b:	83 ec 08             	sub    $0x8,%esp
  80155e:	56                   	push   %esi
  80155f:	ff 75 18             	pushl  0x18(%ebp)
  801562:	ff d7                	call   *%edi
  801564:	83 c4 10             	add    $0x10,%esp
  801567:	eb 03                	jmp    80156c <printnum+0x78>
  801569:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80156c:	4b                   	dec    %ebx
  80156d:	85 db                	test   %ebx,%ebx
  80156f:	7f ea                	jg     80155b <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  801571:	83 ec 08             	sub    $0x8,%esp
  801574:	56                   	push   %esi
  801575:	83 ec 04             	sub    $0x4,%esp
  801578:	ff 75 e4             	pushl  -0x1c(%ebp)
  80157b:	ff 75 e0             	pushl  -0x20(%ebp)
  80157e:	ff 75 dc             	pushl  -0x24(%ebp)
  801581:	ff 75 d8             	pushl  -0x28(%ebp)
  801584:	e8 db 06 00 00       	call   801c64 <__umoddi3>
  801589:	83 c4 14             	add    $0x14,%esp
  80158c:	0f be 80 0b 1f 80 00 	movsbl 0x801f0b(%eax),%eax
  801593:	50                   	push   %eax
  801594:	ff d7                	call   *%edi
}
  801596:	83 c4 10             	add    $0x10,%esp
  801599:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80159c:	5b                   	pop    %ebx
  80159d:	5e                   	pop    %esi
  80159e:	5f                   	pop    %edi
  80159f:	5d                   	pop    %ebp
  8015a0:	c3                   	ret    

008015a1 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8015a1:	55                   	push   %ebp
  8015a2:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8015a4:	83 fa 01             	cmp    $0x1,%edx
  8015a7:	7e 0e                	jle    8015b7 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8015a9:	8b 10                	mov    (%eax),%edx
  8015ab:	8d 4a 08             	lea    0x8(%edx),%ecx
  8015ae:	89 08                	mov    %ecx,(%eax)
  8015b0:	8b 02                	mov    (%edx),%eax
  8015b2:	8b 52 04             	mov    0x4(%edx),%edx
  8015b5:	eb 22                	jmp    8015d9 <getuint+0x38>
	else if (lflag)
  8015b7:	85 d2                	test   %edx,%edx
  8015b9:	74 10                	je     8015cb <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8015bb:	8b 10                	mov    (%eax),%edx
  8015bd:	8d 4a 04             	lea    0x4(%edx),%ecx
  8015c0:	89 08                	mov    %ecx,(%eax)
  8015c2:	8b 02                	mov    (%edx),%eax
  8015c4:	ba 00 00 00 00       	mov    $0x0,%edx
  8015c9:	eb 0e                	jmp    8015d9 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8015cb:	8b 10                	mov    (%eax),%edx
  8015cd:	8d 4a 04             	lea    0x4(%edx),%ecx
  8015d0:	89 08                	mov    %ecx,(%eax)
  8015d2:	8b 02                	mov    (%edx),%eax
  8015d4:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8015d9:	5d                   	pop    %ebp
  8015da:	c3                   	ret    

008015db <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8015db:	55                   	push   %ebp
  8015dc:	89 e5                	mov    %esp,%ebp
  8015de:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8015e1:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8015e4:	8b 10                	mov    (%eax),%edx
  8015e6:	3b 50 04             	cmp    0x4(%eax),%edx
  8015e9:	73 0a                	jae    8015f5 <sprintputch+0x1a>
		*b->buf++ = ch;
  8015eb:	8d 4a 01             	lea    0x1(%edx),%ecx
  8015ee:	89 08                	mov    %ecx,(%eax)
  8015f0:	8b 45 08             	mov    0x8(%ebp),%eax
  8015f3:	88 02                	mov    %al,(%edx)
}
  8015f5:	5d                   	pop    %ebp
  8015f6:	c3                   	ret    

008015f7 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8015f7:	55                   	push   %ebp
  8015f8:	89 e5                	mov    %esp,%ebp
  8015fa:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8015fd:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  801600:	50                   	push   %eax
  801601:	ff 75 10             	pushl  0x10(%ebp)
  801604:	ff 75 0c             	pushl  0xc(%ebp)
  801607:	ff 75 08             	pushl  0x8(%ebp)
  80160a:	e8 05 00 00 00       	call   801614 <vprintfmt>
	va_end(ap);
}
  80160f:	83 c4 10             	add    $0x10,%esp
  801612:	c9                   	leave  
  801613:	c3                   	ret    

00801614 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  801614:	55                   	push   %ebp
  801615:	89 e5                	mov    %esp,%ebp
  801617:	57                   	push   %edi
  801618:	56                   	push   %esi
  801619:	53                   	push   %ebx
  80161a:	83 ec 2c             	sub    $0x2c,%esp
  80161d:	8b 75 08             	mov    0x8(%ebp),%esi
  801620:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801623:	8b 7d 10             	mov    0x10(%ebp),%edi
  801626:	eb 12                	jmp    80163a <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  801628:	85 c0                	test   %eax,%eax
  80162a:	0f 84 68 03 00 00    	je     801998 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  801630:	83 ec 08             	sub    $0x8,%esp
  801633:	53                   	push   %ebx
  801634:	50                   	push   %eax
  801635:	ff d6                	call   *%esi
  801637:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80163a:	47                   	inc    %edi
  80163b:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80163f:	83 f8 25             	cmp    $0x25,%eax
  801642:	75 e4                	jne    801628 <vprintfmt+0x14>
  801644:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  801648:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80164f:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  801656:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80165d:	ba 00 00 00 00       	mov    $0x0,%edx
  801662:	eb 07                	jmp    80166b <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801664:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  801667:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80166b:	8d 47 01             	lea    0x1(%edi),%eax
  80166e:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801671:	0f b6 0f             	movzbl (%edi),%ecx
  801674:	8a 07                	mov    (%edi),%al
  801676:	83 e8 23             	sub    $0x23,%eax
  801679:	3c 55                	cmp    $0x55,%al
  80167b:	0f 87 fe 02 00 00    	ja     80197f <vprintfmt+0x36b>
  801681:	0f b6 c0             	movzbl %al,%eax
  801684:	ff 24 85 40 20 80 00 	jmp    *0x802040(,%eax,4)
  80168b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80168e:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  801692:	eb d7                	jmp    80166b <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801694:	8b 7d e0             	mov    -0x20(%ebp),%edi
  801697:	b8 00 00 00 00       	mov    $0x0,%eax
  80169c:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80169f:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8016a2:	01 c0                	add    %eax,%eax
  8016a4:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8016a8:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8016ab:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8016ae:	83 fa 09             	cmp    $0x9,%edx
  8016b1:	77 34                	ja     8016e7 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8016b3:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8016b4:	eb e9                	jmp    80169f <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8016b6:	8b 45 14             	mov    0x14(%ebp),%eax
  8016b9:	8d 48 04             	lea    0x4(%eax),%ecx
  8016bc:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8016bf:	8b 00                	mov    (%eax),%eax
  8016c1:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8016c4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8016c7:	eb 24                	jmp    8016ed <vprintfmt+0xd9>
  8016c9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8016cd:	79 07                	jns    8016d6 <vprintfmt+0xc2>
  8016cf:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8016d6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8016d9:	eb 90                	jmp    80166b <vprintfmt+0x57>
  8016db:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8016de:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8016e5:	eb 84                	jmp    80166b <vprintfmt+0x57>
  8016e7:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8016ea:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8016ed:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8016f1:	0f 89 74 ff ff ff    	jns    80166b <vprintfmt+0x57>
				width = precision, precision = -1;
  8016f7:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8016fa:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8016fd:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  801704:	e9 62 ff ff ff       	jmp    80166b <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  801709:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80170a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80170d:	e9 59 ff ff ff       	jmp    80166b <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  801712:	8b 45 14             	mov    0x14(%ebp),%eax
  801715:	8d 50 04             	lea    0x4(%eax),%edx
  801718:	89 55 14             	mov    %edx,0x14(%ebp)
  80171b:	83 ec 08             	sub    $0x8,%esp
  80171e:	53                   	push   %ebx
  80171f:	ff 30                	pushl  (%eax)
  801721:	ff d6                	call   *%esi
			break;
  801723:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801726:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  801729:	e9 0c ff ff ff       	jmp    80163a <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80172e:	8b 45 14             	mov    0x14(%ebp),%eax
  801731:	8d 50 04             	lea    0x4(%eax),%edx
  801734:	89 55 14             	mov    %edx,0x14(%ebp)
  801737:	8b 00                	mov    (%eax),%eax
  801739:	85 c0                	test   %eax,%eax
  80173b:	79 02                	jns    80173f <vprintfmt+0x12b>
  80173d:	f7 d8                	neg    %eax
  80173f:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  801741:	83 f8 0f             	cmp    $0xf,%eax
  801744:	7f 0b                	jg     801751 <vprintfmt+0x13d>
  801746:	8b 04 85 a0 21 80 00 	mov    0x8021a0(,%eax,4),%eax
  80174d:	85 c0                	test   %eax,%eax
  80174f:	75 18                	jne    801769 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  801751:	52                   	push   %edx
  801752:	68 23 1f 80 00       	push   $0x801f23
  801757:	53                   	push   %ebx
  801758:	56                   	push   %esi
  801759:	e8 99 fe ff ff       	call   8015f7 <printfmt>
  80175e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801761:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  801764:	e9 d1 fe ff ff       	jmp    80163a <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  801769:	50                   	push   %eax
  80176a:	68 a1 1e 80 00       	push   $0x801ea1
  80176f:	53                   	push   %ebx
  801770:	56                   	push   %esi
  801771:	e8 81 fe ff ff       	call   8015f7 <printfmt>
  801776:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801779:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80177c:	e9 b9 fe ff ff       	jmp    80163a <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  801781:	8b 45 14             	mov    0x14(%ebp),%eax
  801784:	8d 50 04             	lea    0x4(%eax),%edx
  801787:	89 55 14             	mov    %edx,0x14(%ebp)
  80178a:	8b 38                	mov    (%eax),%edi
  80178c:	85 ff                	test   %edi,%edi
  80178e:	75 05                	jne    801795 <vprintfmt+0x181>
				p = "(null)";
  801790:	bf 1c 1f 80 00       	mov    $0x801f1c,%edi
			if (width > 0 && padc != '-')
  801795:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  801799:	0f 8e 90 00 00 00    	jle    80182f <vprintfmt+0x21b>
  80179f:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8017a3:	0f 84 8e 00 00 00    	je     801837 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8017a9:	83 ec 08             	sub    $0x8,%esp
  8017ac:	ff 75 d0             	pushl  -0x30(%ebp)
  8017af:	57                   	push   %edi
  8017b0:	e8 a0 e9 ff ff       	call   800155 <strnlen>
  8017b5:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8017b8:	29 c1                	sub    %eax,%ecx
  8017ba:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8017bd:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8017c0:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8017c4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8017c7:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8017ca:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8017cc:	eb 0d                	jmp    8017db <vprintfmt+0x1c7>
					putch(padc, putdat);
  8017ce:	83 ec 08             	sub    $0x8,%esp
  8017d1:	53                   	push   %ebx
  8017d2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8017d5:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8017d7:	4f                   	dec    %edi
  8017d8:	83 c4 10             	add    $0x10,%esp
  8017db:	85 ff                	test   %edi,%edi
  8017dd:	7f ef                	jg     8017ce <vprintfmt+0x1ba>
  8017df:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8017e2:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8017e5:	89 c8                	mov    %ecx,%eax
  8017e7:	85 c9                	test   %ecx,%ecx
  8017e9:	79 05                	jns    8017f0 <vprintfmt+0x1dc>
  8017eb:	b8 00 00 00 00       	mov    $0x0,%eax
  8017f0:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8017f3:	29 c1                	sub    %eax,%ecx
  8017f5:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8017f8:	89 75 08             	mov    %esi,0x8(%ebp)
  8017fb:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8017fe:	eb 3d                	jmp    80183d <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  801800:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  801804:	74 19                	je     80181f <vprintfmt+0x20b>
  801806:	0f be c0             	movsbl %al,%eax
  801809:	83 e8 20             	sub    $0x20,%eax
  80180c:	83 f8 5e             	cmp    $0x5e,%eax
  80180f:	76 0e                	jbe    80181f <vprintfmt+0x20b>
					putch('?', putdat);
  801811:	83 ec 08             	sub    $0x8,%esp
  801814:	53                   	push   %ebx
  801815:	6a 3f                	push   $0x3f
  801817:	ff 55 08             	call   *0x8(%ebp)
  80181a:	83 c4 10             	add    $0x10,%esp
  80181d:	eb 0b                	jmp    80182a <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80181f:	83 ec 08             	sub    $0x8,%esp
  801822:	53                   	push   %ebx
  801823:	52                   	push   %edx
  801824:	ff 55 08             	call   *0x8(%ebp)
  801827:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80182a:	ff 4d e4             	decl   -0x1c(%ebp)
  80182d:	eb 0e                	jmp    80183d <vprintfmt+0x229>
  80182f:	89 75 08             	mov    %esi,0x8(%ebp)
  801832:	8b 75 d0             	mov    -0x30(%ebp),%esi
  801835:	eb 06                	jmp    80183d <vprintfmt+0x229>
  801837:	89 75 08             	mov    %esi,0x8(%ebp)
  80183a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80183d:	47                   	inc    %edi
  80183e:	8a 47 ff             	mov    -0x1(%edi),%al
  801841:	0f be d0             	movsbl %al,%edx
  801844:	85 d2                	test   %edx,%edx
  801846:	74 1d                	je     801865 <vprintfmt+0x251>
  801848:	85 f6                	test   %esi,%esi
  80184a:	78 b4                	js     801800 <vprintfmt+0x1ec>
  80184c:	4e                   	dec    %esi
  80184d:	79 b1                	jns    801800 <vprintfmt+0x1ec>
  80184f:	8b 75 08             	mov    0x8(%ebp),%esi
  801852:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  801855:	eb 14                	jmp    80186b <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  801857:	83 ec 08             	sub    $0x8,%esp
  80185a:	53                   	push   %ebx
  80185b:	6a 20                	push   $0x20
  80185d:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80185f:	4f                   	dec    %edi
  801860:	83 c4 10             	add    $0x10,%esp
  801863:	eb 06                	jmp    80186b <vprintfmt+0x257>
  801865:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  801868:	8b 75 08             	mov    0x8(%ebp),%esi
  80186b:	85 ff                	test   %edi,%edi
  80186d:	7f e8                	jg     801857 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80186f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  801872:	e9 c3 fd ff ff       	jmp    80163a <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  801877:	83 fa 01             	cmp    $0x1,%edx
  80187a:	7e 16                	jle    801892 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80187c:	8b 45 14             	mov    0x14(%ebp),%eax
  80187f:	8d 50 08             	lea    0x8(%eax),%edx
  801882:	89 55 14             	mov    %edx,0x14(%ebp)
  801885:	8b 50 04             	mov    0x4(%eax),%edx
  801888:	8b 00                	mov    (%eax),%eax
  80188a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80188d:	89 55 dc             	mov    %edx,-0x24(%ebp)
  801890:	eb 32                	jmp    8018c4 <vprintfmt+0x2b0>
	else if (lflag)
  801892:	85 d2                	test   %edx,%edx
  801894:	74 18                	je     8018ae <vprintfmt+0x29a>
		return va_arg(*ap, long);
  801896:	8b 45 14             	mov    0x14(%ebp),%eax
  801899:	8d 50 04             	lea    0x4(%eax),%edx
  80189c:	89 55 14             	mov    %edx,0x14(%ebp)
  80189f:	8b 00                	mov    (%eax),%eax
  8018a1:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8018a4:	89 c1                	mov    %eax,%ecx
  8018a6:	c1 f9 1f             	sar    $0x1f,%ecx
  8018a9:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8018ac:	eb 16                	jmp    8018c4 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8018ae:	8b 45 14             	mov    0x14(%ebp),%eax
  8018b1:	8d 50 04             	lea    0x4(%eax),%edx
  8018b4:	89 55 14             	mov    %edx,0x14(%ebp)
  8018b7:	8b 00                	mov    (%eax),%eax
  8018b9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8018bc:	89 c1                	mov    %eax,%ecx
  8018be:	c1 f9 1f             	sar    $0x1f,%ecx
  8018c1:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8018c4:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8018c7:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8018ca:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8018ce:	79 76                	jns    801946 <vprintfmt+0x332>
				putch('-', putdat);
  8018d0:	83 ec 08             	sub    $0x8,%esp
  8018d3:	53                   	push   %ebx
  8018d4:	6a 2d                	push   $0x2d
  8018d6:	ff d6                	call   *%esi
				num = -(long long) num;
  8018d8:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8018db:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8018de:	f7 d8                	neg    %eax
  8018e0:	83 d2 00             	adc    $0x0,%edx
  8018e3:	f7 da                	neg    %edx
  8018e5:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8018e8:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8018ed:	eb 5c                	jmp    80194b <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8018ef:	8d 45 14             	lea    0x14(%ebp),%eax
  8018f2:	e8 aa fc ff ff       	call   8015a1 <getuint>
			base = 10;
  8018f7:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8018fc:	eb 4d                	jmp    80194b <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8018fe:	8d 45 14             	lea    0x14(%ebp),%eax
  801901:	e8 9b fc ff ff       	call   8015a1 <getuint>
			base = 8;
  801906:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80190b:	eb 3e                	jmp    80194b <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80190d:	83 ec 08             	sub    $0x8,%esp
  801910:	53                   	push   %ebx
  801911:	6a 30                	push   $0x30
  801913:	ff d6                	call   *%esi
			putch('x', putdat);
  801915:	83 c4 08             	add    $0x8,%esp
  801918:	53                   	push   %ebx
  801919:	6a 78                	push   $0x78
  80191b:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80191d:	8b 45 14             	mov    0x14(%ebp),%eax
  801920:	8d 50 04             	lea    0x4(%eax),%edx
  801923:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  801926:	8b 00                	mov    (%eax),%eax
  801928:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80192d:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  801930:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  801935:	eb 14                	jmp    80194b <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  801937:	8d 45 14             	lea    0x14(%ebp),%eax
  80193a:	e8 62 fc ff ff       	call   8015a1 <getuint>
			base = 16;
  80193f:	b9 10 00 00 00       	mov    $0x10,%ecx
  801944:	eb 05                	jmp    80194b <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  801946:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80194b:	83 ec 0c             	sub    $0xc,%esp
  80194e:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  801952:	57                   	push   %edi
  801953:	ff 75 e4             	pushl  -0x1c(%ebp)
  801956:	51                   	push   %ecx
  801957:	52                   	push   %edx
  801958:	50                   	push   %eax
  801959:	89 da                	mov    %ebx,%edx
  80195b:	89 f0                	mov    %esi,%eax
  80195d:	e8 92 fb ff ff       	call   8014f4 <printnum>
			break;
  801962:	83 c4 20             	add    $0x20,%esp
  801965:	8b 7d e0             	mov    -0x20(%ebp),%edi
  801968:	e9 cd fc ff ff       	jmp    80163a <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80196d:	83 ec 08             	sub    $0x8,%esp
  801970:	53                   	push   %ebx
  801971:	51                   	push   %ecx
  801972:	ff d6                	call   *%esi
			break;
  801974:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  801977:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80197a:	e9 bb fc ff ff       	jmp    80163a <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80197f:	83 ec 08             	sub    $0x8,%esp
  801982:	53                   	push   %ebx
  801983:	6a 25                	push   $0x25
  801985:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  801987:	83 c4 10             	add    $0x10,%esp
  80198a:	eb 01                	jmp    80198d <vprintfmt+0x379>
  80198c:	4f                   	dec    %edi
  80198d:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  801991:	75 f9                	jne    80198c <vprintfmt+0x378>
  801993:	e9 a2 fc ff ff       	jmp    80163a <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  801998:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80199b:	5b                   	pop    %ebx
  80199c:	5e                   	pop    %esi
  80199d:	5f                   	pop    %edi
  80199e:	5d                   	pop    %ebp
  80199f:	c3                   	ret    

008019a0 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8019a0:	55                   	push   %ebp
  8019a1:	89 e5                	mov    %esp,%ebp
  8019a3:	83 ec 18             	sub    $0x18,%esp
  8019a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8019a9:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8019ac:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8019af:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8019b3:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8019b6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8019bd:	85 c0                	test   %eax,%eax
  8019bf:	74 26                	je     8019e7 <vsnprintf+0x47>
  8019c1:	85 d2                	test   %edx,%edx
  8019c3:	7e 29                	jle    8019ee <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8019c5:	ff 75 14             	pushl  0x14(%ebp)
  8019c8:	ff 75 10             	pushl  0x10(%ebp)
  8019cb:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8019ce:	50                   	push   %eax
  8019cf:	68 db 15 80 00       	push   $0x8015db
  8019d4:	e8 3b fc ff ff       	call   801614 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8019d9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8019dc:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8019df:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8019e2:	83 c4 10             	add    $0x10,%esp
  8019e5:	eb 0c                	jmp    8019f3 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8019e7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8019ec:	eb 05                	jmp    8019f3 <vsnprintf+0x53>
  8019ee:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8019f3:	c9                   	leave  
  8019f4:	c3                   	ret    

008019f5 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8019f5:	55                   	push   %ebp
  8019f6:	89 e5                	mov    %esp,%ebp
  8019f8:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8019fb:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8019fe:	50                   	push   %eax
  8019ff:	ff 75 10             	pushl  0x10(%ebp)
  801a02:	ff 75 0c             	pushl  0xc(%ebp)
  801a05:	ff 75 08             	pushl  0x8(%ebp)
  801a08:	e8 93 ff ff ff       	call   8019a0 <vsnprintf>
	va_end(ap);

	return rc;
}
  801a0d:	c9                   	leave  
  801a0e:	c3                   	ret    

00801a0f <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801a0f:	55                   	push   %ebp
  801a10:	89 e5                	mov    %esp,%ebp
  801a12:	56                   	push   %esi
  801a13:	53                   	push   %ebx
  801a14:	8b 75 08             	mov    0x8(%ebp),%esi
  801a17:	8b 45 0c             	mov    0xc(%ebp),%eax
  801a1a:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801a1d:	85 c0                	test   %eax,%eax
  801a1f:	75 05                	jne    801a26 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801a21:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801a26:	83 ec 0c             	sub    $0xc,%esp
  801a29:	50                   	push   %eax
  801a2a:	e8 f8 ec ff ff       	call   800727 <sys_ipc_recv>
  801a2f:	83 c4 10             	add    $0x10,%esp
  801a32:	85 c0                	test   %eax,%eax
  801a34:	74 16                	je     801a4c <ipc_recv+0x3d>
        if (from_env_store)
  801a36:	85 f6                	test   %esi,%esi
  801a38:	74 06                	je     801a40 <ipc_recv+0x31>
            *from_env_store = 0;
  801a3a:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801a40:	85 db                	test   %ebx,%ebx
  801a42:	74 2c                	je     801a70 <ipc_recv+0x61>
            *perm_store = 0;
  801a44:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801a4a:	eb 24                	jmp    801a70 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801a4c:	85 f6                	test   %esi,%esi
  801a4e:	74 0a                	je     801a5a <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801a50:	a1 04 40 80 00       	mov    0x804004,%eax
  801a55:	8b 40 74             	mov    0x74(%eax),%eax
  801a58:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801a5a:	85 db                	test   %ebx,%ebx
  801a5c:	74 0a                	je     801a68 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801a5e:	a1 04 40 80 00       	mov    0x804004,%eax
  801a63:	8b 40 78             	mov    0x78(%eax),%eax
  801a66:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801a68:	a1 04 40 80 00       	mov    0x804004,%eax
  801a6d:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801a70:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801a73:	5b                   	pop    %ebx
  801a74:	5e                   	pop    %esi
  801a75:	5d                   	pop    %ebp
  801a76:	c3                   	ret    

00801a77 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801a77:	55                   	push   %ebp
  801a78:	89 e5                	mov    %esp,%ebp
  801a7a:	57                   	push   %edi
  801a7b:	56                   	push   %esi
  801a7c:	53                   	push   %ebx
  801a7d:	83 ec 0c             	sub    $0xc,%esp
  801a80:	8b 75 0c             	mov    0xc(%ebp),%esi
  801a83:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801a86:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801a89:	85 db                	test   %ebx,%ebx
  801a8b:	75 0c                	jne    801a99 <ipc_send+0x22>
        pg = (void *)UTOP;
  801a8d:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801a92:	eb 05                	jmp    801a99 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801a94:	e8 a0 ea ff ff       	call   800539 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801a99:	57                   	push   %edi
  801a9a:	53                   	push   %ebx
  801a9b:	56                   	push   %esi
  801a9c:	ff 75 08             	pushl  0x8(%ebp)
  801a9f:	e8 60 ec ff ff       	call   800704 <sys_ipc_try_send>
  801aa4:	83 c4 10             	add    $0x10,%esp
  801aa7:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801aaa:	74 e8                	je     801a94 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801aac:	85 c0                	test   %eax,%eax
  801aae:	74 14                	je     801ac4 <ipc_send+0x4d>
        panic("ipc_send: error");
  801ab0:	83 ec 04             	sub    $0x4,%esp
  801ab3:	68 00 22 80 00       	push   $0x802200
  801ab8:	6a 6a                	push   $0x6a
  801aba:	68 10 22 80 00       	push   $0x802210
  801abf:	e8 44 f9 ff ff       	call   801408 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801ac4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801ac7:	5b                   	pop    %ebx
  801ac8:	5e                   	pop    %esi
  801ac9:	5f                   	pop    %edi
  801aca:	5d                   	pop    %ebp
  801acb:	c3                   	ret    

00801acc <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801acc:	55                   	push   %ebp
  801acd:	89 e5                	mov    %esp,%ebp
  801acf:	53                   	push   %ebx
  801ad0:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801ad3:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801ad8:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801adf:	89 d0                	mov    %edx,%eax
  801ae1:	c1 e0 07             	shl    $0x7,%eax
  801ae4:	29 d8                	sub    %ebx,%eax
  801ae6:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801aeb:	8b 40 50             	mov    0x50(%eax),%eax
  801aee:	39 c8                	cmp    %ecx,%eax
  801af0:	75 0d                	jne    801aff <ipc_find_env+0x33>
			return envs[i].env_id;
  801af2:	c1 e2 07             	shl    $0x7,%edx
  801af5:	29 da                	sub    %ebx,%edx
  801af7:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801afd:	eb 0e                	jmp    801b0d <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801aff:	42                   	inc    %edx
  801b00:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801b06:	75 d0                	jne    801ad8 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801b08:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801b0d:	5b                   	pop    %ebx
  801b0e:	5d                   	pop    %ebp
  801b0f:	c3                   	ret    

00801b10 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801b10:	55                   	push   %ebp
  801b11:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801b13:	8b 45 08             	mov    0x8(%ebp),%eax
  801b16:	c1 e8 16             	shr    $0x16,%eax
  801b19:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801b20:	a8 01                	test   $0x1,%al
  801b22:	74 21                	je     801b45 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801b24:	8b 45 08             	mov    0x8(%ebp),%eax
  801b27:	c1 e8 0c             	shr    $0xc,%eax
  801b2a:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801b31:	a8 01                	test   $0x1,%al
  801b33:	74 17                	je     801b4c <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801b35:	c1 e8 0c             	shr    $0xc,%eax
  801b38:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801b3f:	ef 
  801b40:	0f b7 c0             	movzwl %ax,%eax
  801b43:	eb 0c                	jmp    801b51 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801b45:	b8 00 00 00 00       	mov    $0x0,%eax
  801b4a:	eb 05                	jmp    801b51 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801b4c:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801b51:	5d                   	pop    %ebp
  801b52:	c3                   	ret    
  801b53:	90                   	nop

00801b54 <__udivdi3>:
  801b54:	55                   	push   %ebp
  801b55:	57                   	push   %edi
  801b56:	56                   	push   %esi
  801b57:	53                   	push   %ebx
  801b58:	83 ec 1c             	sub    $0x1c,%esp
  801b5b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801b5f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801b63:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801b67:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801b6b:	89 ca                	mov    %ecx,%edx
  801b6d:	89 f8                	mov    %edi,%eax
  801b6f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801b73:	85 f6                	test   %esi,%esi
  801b75:	75 2d                	jne    801ba4 <__udivdi3+0x50>
  801b77:	39 cf                	cmp    %ecx,%edi
  801b79:	77 65                	ja     801be0 <__udivdi3+0x8c>
  801b7b:	89 fd                	mov    %edi,%ebp
  801b7d:	85 ff                	test   %edi,%edi
  801b7f:	75 0b                	jne    801b8c <__udivdi3+0x38>
  801b81:	b8 01 00 00 00       	mov    $0x1,%eax
  801b86:	31 d2                	xor    %edx,%edx
  801b88:	f7 f7                	div    %edi
  801b8a:	89 c5                	mov    %eax,%ebp
  801b8c:	31 d2                	xor    %edx,%edx
  801b8e:	89 c8                	mov    %ecx,%eax
  801b90:	f7 f5                	div    %ebp
  801b92:	89 c1                	mov    %eax,%ecx
  801b94:	89 d8                	mov    %ebx,%eax
  801b96:	f7 f5                	div    %ebp
  801b98:	89 cf                	mov    %ecx,%edi
  801b9a:	89 fa                	mov    %edi,%edx
  801b9c:	83 c4 1c             	add    $0x1c,%esp
  801b9f:	5b                   	pop    %ebx
  801ba0:	5e                   	pop    %esi
  801ba1:	5f                   	pop    %edi
  801ba2:	5d                   	pop    %ebp
  801ba3:	c3                   	ret    
  801ba4:	39 ce                	cmp    %ecx,%esi
  801ba6:	77 28                	ja     801bd0 <__udivdi3+0x7c>
  801ba8:	0f bd fe             	bsr    %esi,%edi
  801bab:	83 f7 1f             	xor    $0x1f,%edi
  801bae:	75 40                	jne    801bf0 <__udivdi3+0x9c>
  801bb0:	39 ce                	cmp    %ecx,%esi
  801bb2:	72 0a                	jb     801bbe <__udivdi3+0x6a>
  801bb4:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801bb8:	0f 87 9e 00 00 00    	ja     801c5c <__udivdi3+0x108>
  801bbe:	b8 01 00 00 00       	mov    $0x1,%eax
  801bc3:	89 fa                	mov    %edi,%edx
  801bc5:	83 c4 1c             	add    $0x1c,%esp
  801bc8:	5b                   	pop    %ebx
  801bc9:	5e                   	pop    %esi
  801bca:	5f                   	pop    %edi
  801bcb:	5d                   	pop    %ebp
  801bcc:	c3                   	ret    
  801bcd:	8d 76 00             	lea    0x0(%esi),%esi
  801bd0:	31 ff                	xor    %edi,%edi
  801bd2:	31 c0                	xor    %eax,%eax
  801bd4:	89 fa                	mov    %edi,%edx
  801bd6:	83 c4 1c             	add    $0x1c,%esp
  801bd9:	5b                   	pop    %ebx
  801bda:	5e                   	pop    %esi
  801bdb:	5f                   	pop    %edi
  801bdc:	5d                   	pop    %ebp
  801bdd:	c3                   	ret    
  801bde:	66 90                	xchg   %ax,%ax
  801be0:	89 d8                	mov    %ebx,%eax
  801be2:	f7 f7                	div    %edi
  801be4:	31 ff                	xor    %edi,%edi
  801be6:	89 fa                	mov    %edi,%edx
  801be8:	83 c4 1c             	add    $0x1c,%esp
  801beb:	5b                   	pop    %ebx
  801bec:	5e                   	pop    %esi
  801bed:	5f                   	pop    %edi
  801bee:	5d                   	pop    %ebp
  801bef:	c3                   	ret    
  801bf0:	bd 20 00 00 00       	mov    $0x20,%ebp
  801bf5:	89 eb                	mov    %ebp,%ebx
  801bf7:	29 fb                	sub    %edi,%ebx
  801bf9:	89 f9                	mov    %edi,%ecx
  801bfb:	d3 e6                	shl    %cl,%esi
  801bfd:	89 c5                	mov    %eax,%ebp
  801bff:	88 d9                	mov    %bl,%cl
  801c01:	d3 ed                	shr    %cl,%ebp
  801c03:	89 e9                	mov    %ebp,%ecx
  801c05:	09 f1                	or     %esi,%ecx
  801c07:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801c0b:	89 f9                	mov    %edi,%ecx
  801c0d:	d3 e0                	shl    %cl,%eax
  801c0f:	89 c5                	mov    %eax,%ebp
  801c11:	89 d6                	mov    %edx,%esi
  801c13:	88 d9                	mov    %bl,%cl
  801c15:	d3 ee                	shr    %cl,%esi
  801c17:	89 f9                	mov    %edi,%ecx
  801c19:	d3 e2                	shl    %cl,%edx
  801c1b:	8b 44 24 08          	mov    0x8(%esp),%eax
  801c1f:	88 d9                	mov    %bl,%cl
  801c21:	d3 e8                	shr    %cl,%eax
  801c23:	09 c2                	or     %eax,%edx
  801c25:	89 d0                	mov    %edx,%eax
  801c27:	89 f2                	mov    %esi,%edx
  801c29:	f7 74 24 0c          	divl   0xc(%esp)
  801c2d:	89 d6                	mov    %edx,%esi
  801c2f:	89 c3                	mov    %eax,%ebx
  801c31:	f7 e5                	mul    %ebp
  801c33:	39 d6                	cmp    %edx,%esi
  801c35:	72 19                	jb     801c50 <__udivdi3+0xfc>
  801c37:	74 0b                	je     801c44 <__udivdi3+0xf0>
  801c39:	89 d8                	mov    %ebx,%eax
  801c3b:	31 ff                	xor    %edi,%edi
  801c3d:	e9 58 ff ff ff       	jmp    801b9a <__udivdi3+0x46>
  801c42:	66 90                	xchg   %ax,%ax
  801c44:	8b 54 24 08          	mov    0x8(%esp),%edx
  801c48:	89 f9                	mov    %edi,%ecx
  801c4a:	d3 e2                	shl    %cl,%edx
  801c4c:	39 c2                	cmp    %eax,%edx
  801c4e:	73 e9                	jae    801c39 <__udivdi3+0xe5>
  801c50:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801c53:	31 ff                	xor    %edi,%edi
  801c55:	e9 40 ff ff ff       	jmp    801b9a <__udivdi3+0x46>
  801c5a:	66 90                	xchg   %ax,%ax
  801c5c:	31 c0                	xor    %eax,%eax
  801c5e:	e9 37 ff ff ff       	jmp    801b9a <__udivdi3+0x46>
  801c63:	90                   	nop

00801c64 <__umoddi3>:
  801c64:	55                   	push   %ebp
  801c65:	57                   	push   %edi
  801c66:	56                   	push   %esi
  801c67:	53                   	push   %ebx
  801c68:	83 ec 1c             	sub    $0x1c,%esp
  801c6b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  801c6f:	8b 74 24 34          	mov    0x34(%esp),%esi
  801c73:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801c77:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801c7b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  801c7f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801c83:	89 f3                	mov    %esi,%ebx
  801c85:	89 fa                	mov    %edi,%edx
  801c87:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801c8b:	89 34 24             	mov    %esi,(%esp)
  801c8e:	85 c0                	test   %eax,%eax
  801c90:	75 1a                	jne    801cac <__umoddi3+0x48>
  801c92:	39 f7                	cmp    %esi,%edi
  801c94:	0f 86 a2 00 00 00    	jbe    801d3c <__umoddi3+0xd8>
  801c9a:	89 c8                	mov    %ecx,%eax
  801c9c:	89 f2                	mov    %esi,%edx
  801c9e:	f7 f7                	div    %edi
  801ca0:	89 d0                	mov    %edx,%eax
  801ca2:	31 d2                	xor    %edx,%edx
  801ca4:	83 c4 1c             	add    $0x1c,%esp
  801ca7:	5b                   	pop    %ebx
  801ca8:	5e                   	pop    %esi
  801ca9:	5f                   	pop    %edi
  801caa:	5d                   	pop    %ebp
  801cab:	c3                   	ret    
  801cac:	39 f0                	cmp    %esi,%eax
  801cae:	0f 87 ac 00 00 00    	ja     801d60 <__umoddi3+0xfc>
  801cb4:	0f bd e8             	bsr    %eax,%ebp
  801cb7:	83 f5 1f             	xor    $0x1f,%ebp
  801cba:	0f 84 ac 00 00 00    	je     801d6c <__umoddi3+0x108>
  801cc0:	bf 20 00 00 00       	mov    $0x20,%edi
  801cc5:	29 ef                	sub    %ebp,%edi
  801cc7:	89 fe                	mov    %edi,%esi
  801cc9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801ccd:	89 e9                	mov    %ebp,%ecx
  801ccf:	d3 e0                	shl    %cl,%eax
  801cd1:	89 d7                	mov    %edx,%edi
  801cd3:	89 f1                	mov    %esi,%ecx
  801cd5:	d3 ef                	shr    %cl,%edi
  801cd7:	09 c7                	or     %eax,%edi
  801cd9:	89 e9                	mov    %ebp,%ecx
  801cdb:	d3 e2                	shl    %cl,%edx
  801cdd:	89 14 24             	mov    %edx,(%esp)
  801ce0:	89 d8                	mov    %ebx,%eax
  801ce2:	d3 e0                	shl    %cl,%eax
  801ce4:	89 c2                	mov    %eax,%edx
  801ce6:	8b 44 24 08          	mov    0x8(%esp),%eax
  801cea:	d3 e0                	shl    %cl,%eax
  801cec:	89 44 24 04          	mov    %eax,0x4(%esp)
  801cf0:	8b 44 24 08          	mov    0x8(%esp),%eax
  801cf4:	89 f1                	mov    %esi,%ecx
  801cf6:	d3 e8                	shr    %cl,%eax
  801cf8:	09 d0                	or     %edx,%eax
  801cfa:	d3 eb                	shr    %cl,%ebx
  801cfc:	89 da                	mov    %ebx,%edx
  801cfe:	f7 f7                	div    %edi
  801d00:	89 d3                	mov    %edx,%ebx
  801d02:	f7 24 24             	mull   (%esp)
  801d05:	89 c6                	mov    %eax,%esi
  801d07:	89 d1                	mov    %edx,%ecx
  801d09:	39 d3                	cmp    %edx,%ebx
  801d0b:	0f 82 87 00 00 00    	jb     801d98 <__umoddi3+0x134>
  801d11:	0f 84 91 00 00 00    	je     801da8 <__umoddi3+0x144>
  801d17:	8b 54 24 04          	mov    0x4(%esp),%edx
  801d1b:	29 f2                	sub    %esi,%edx
  801d1d:	19 cb                	sbb    %ecx,%ebx
  801d1f:	89 d8                	mov    %ebx,%eax
  801d21:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801d25:	d3 e0                	shl    %cl,%eax
  801d27:	89 e9                	mov    %ebp,%ecx
  801d29:	d3 ea                	shr    %cl,%edx
  801d2b:	09 d0                	or     %edx,%eax
  801d2d:	89 e9                	mov    %ebp,%ecx
  801d2f:	d3 eb                	shr    %cl,%ebx
  801d31:	89 da                	mov    %ebx,%edx
  801d33:	83 c4 1c             	add    $0x1c,%esp
  801d36:	5b                   	pop    %ebx
  801d37:	5e                   	pop    %esi
  801d38:	5f                   	pop    %edi
  801d39:	5d                   	pop    %ebp
  801d3a:	c3                   	ret    
  801d3b:	90                   	nop
  801d3c:	89 fd                	mov    %edi,%ebp
  801d3e:	85 ff                	test   %edi,%edi
  801d40:	75 0b                	jne    801d4d <__umoddi3+0xe9>
  801d42:	b8 01 00 00 00       	mov    $0x1,%eax
  801d47:	31 d2                	xor    %edx,%edx
  801d49:	f7 f7                	div    %edi
  801d4b:	89 c5                	mov    %eax,%ebp
  801d4d:	89 f0                	mov    %esi,%eax
  801d4f:	31 d2                	xor    %edx,%edx
  801d51:	f7 f5                	div    %ebp
  801d53:	89 c8                	mov    %ecx,%eax
  801d55:	f7 f5                	div    %ebp
  801d57:	89 d0                	mov    %edx,%eax
  801d59:	e9 44 ff ff ff       	jmp    801ca2 <__umoddi3+0x3e>
  801d5e:	66 90                	xchg   %ax,%ax
  801d60:	89 c8                	mov    %ecx,%eax
  801d62:	89 f2                	mov    %esi,%edx
  801d64:	83 c4 1c             	add    $0x1c,%esp
  801d67:	5b                   	pop    %ebx
  801d68:	5e                   	pop    %esi
  801d69:	5f                   	pop    %edi
  801d6a:	5d                   	pop    %ebp
  801d6b:	c3                   	ret    
  801d6c:	3b 04 24             	cmp    (%esp),%eax
  801d6f:	72 06                	jb     801d77 <__umoddi3+0x113>
  801d71:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801d75:	77 0f                	ja     801d86 <__umoddi3+0x122>
  801d77:	89 f2                	mov    %esi,%edx
  801d79:	29 f9                	sub    %edi,%ecx
  801d7b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  801d7f:	89 14 24             	mov    %edx,(%esp)
  801d82:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801d86:	8b 44 24 04          	mov    0x4(%esp),%eax
  801d8a:	8b 14 24             	mov    (%esp),%edx
  801d8d:	83 c4 1c             	add    $0x1c,%esp
  801d90:	5b                   	pop    %ebx
  801d91:	5e                   	pop    %esi
  801d92:	5f                   	pop    %edi
  801d93:	5d                   	pop    %ebp
  801d94:	c3                   	ret    
  801d95:	8d 76 00             	lea    0x0(%esi),%esi
  801d98:	2b 04 24             	sub    (%esp),%eax
  801d9b:	19 fa                	sbb    %edi,%edx
  801d9d:	89 d1                	mov    %edx,%ecx
  801d9f:	89 c6                	mov    %eax,%esi
  801da1:	e9 71 ff ff ff       	jmp    801d17 <__umoddi3+0xb3>
  801da6:	66 90                	xchg   %ax,%ax
  801da8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801dac:	72 ea                	jb     801d98 <__umoddi3+0x134>
  801dae:	89 d9                	mov    %ebx,%ecx
  801db0:	e9 62 ff ff ff       	jmp    801d17 <__umoddi3+0xb3>
