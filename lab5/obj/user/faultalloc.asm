
obj/user/faultalloc.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 99 00 00 00       	call   8000ca <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <handler>:

#include <inc/lib.h>

void
handler(struct UTrapframe *utf)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 0c             	sub    $0xc,%esp
	int r;
	void *addr = (void*)utf->utf_fault_va;
  80003a:	8b 45 08             	mov    0x8(%ebp),%eax
  80003d:	8b 18                	mov    (%eax),%ebx

	cprintf("fault %x\n", addr);
  80003f:	53                   	push   %ebx
  800040:	68 40 10 80 00       	push   $0x801040
  800045:	e8 b9 01 00 00       	call   800203 <cprintf>
	if ((r = sys_page_alloc(0, ROUNDDOWN(addr, PGSIZE),
  80004a:	83 c4 0c             	add    $0xc,%esp
  80004d:	6a 07                	push   $0x7
  80004f:	89 d8                	mov    %ebx,%eax
  800051:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800056:	50                   	push   %eax
  800057:	6a 00                	push   $0x0
  800059:	e8 ed 0a 00 00       	call   800b4b <sys_page_alloc>
  80005e:	83 c4 10             	add    $0x10,%esp
  800061:	85 c0                	test   %eax,%eax
  800063:	79 16                	jns    80007b <handler+0x48>
				PTE_P|PTE_U|PTE_W)) < 0)
		panic("allocating at %x in page fault handler: %e", addr, r);
  800065:	83 ec 0c             	sub    $0xc,%esp
  800068:	50                   	push   %eax
  800069:	53                   	push   %ebx
  80006a:	68 60 10 80 00       	push   $0x801060
  80006f:	6a 0e                	push   $0xe
  800071:	68 4a 10 80 00       	push   $0x80104a
  800076:	e8 b0 00 00 00       	call   80012b <_panic>
	snprintf((char*) addr, 100, "this string was faulted in at %x", addr);
  80007b:	53                   	push   %ebx
  80007c:	68 8c 10 80 00       	push   $0x80108c
  800081:	6a 64                	push   $0x64
  800083:	53                   	push   %ebx
  800084:	e8 8f 06 00 00       	call   800718 <snprintf>
}
  800089:	83 c4 10             	add    $0x10,%esp
  80008c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80008f:	c9                   	leave  
  800090:	c3                   	ret    

00800091 <umain>:

void
umain(int argc, char **argv)
{
  800091:	55                   	push   %ebp
  800092:	89 e5                	mov    %esp,%ebp
  800094:	83 ec 14             	sub    $0x14,%esp
	set_pgfault_handler(handler);
  800097:	68 33 00 80 00       	push   $0x800033
  80009c:	e8 ba 0c 00 00       	call   800d5b <set_pgfault_handler>
	cprintf("%s\n", (char*)0xDeadBeef);
  8000a1:	83 c4 08             	add    $0x8,%esp
  8000a4:	68 ef be ad de       	push   $0xdeadbeef
  8000a9:	68 5c 10 80 00       	push   $0x80105c
  8000ae:	e8 50 01 00 00       	call   800203 <cprintf>
	cprintf("%s\n", (char*)0xCafeBffe);
  8000b3:	83 c4 08             	add    $0x8,%esp
  8000b6:	68 fe bf fe ca       	push   $0xcafebffe
  8000bb:	68 5c 10 80 00       	push   $0x80105c
  8000c0:	e8 3e 01 00 00       	call   800203 <cprintf>
}
  8000c5:	83 c4 10             	add    $0x10,%esp
  8000c8:	c9                   	leave  
  8000c9:	c3                   	ret    

008000ca <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000ca:	55                   	push   %ebp
  8000cb:	89 e5                	mov    %esp,%ebp
  8000cd:	56                   	push   %esi
  8000ce:	53                   	push   %ebx
  8000cf:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000d2:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000d5:	e8 33 0a 00 00       	call   800b0d <sys_getenvid>
  8000da:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000df:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000e6:	c1 e0 07             	shl    $0x7,%eax
  8000e9:	29 d0                	sub    %edx,%eax
  8000eb:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000f0:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000f5:	85 db                	test   %ebx,%ebx
  8000f7:	7e 07                	jle    800100 <libmain+0x36>
		binaryname = argv[0];
  8000f9:	8b 06                	mov    (%esi),%eax
  8000fb:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800100:	83 ec 08             	sub    $0x8,%esp
  800103:	56                   	push   %esi
  800104:	53                   	push   %ebx
  800105:	e8 87 ff ff ff       	call   800091 <umain>

	// exit gracefully
	exit();
  80010a:	e8 0a 00 00 00       	call   800119 <exit>
}
  80010f:	83 c4 10             	add    $0x10,%esp
  800112:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800115:	5b                   	pop    %ebx
  800116:	5e                   	pop    %esi
  800117:	5d                   	pop    %ebp
  800118:	c3                   	ret    

00800119 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800119:	55                   	push   %ebp
  80011a:	89 e5                	mov    %esp,%ebp
  80011c:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80011f:	6a 00                	push   $0x0
  800121:	e8 a6 09 00 00       	call   800acc <sys_env_destroy>
}
  800126:	83 c4 10             	add    $0x10,%esp
  800129:	c9                   	leave  
  80012a:	c3                   	ret    

0080012b <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80012b:	55                   	push   %ebp
  80012c:	89 e5                	mov    %esp,%ebp
  80012e:	56                   	push   %esi
  80012f:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800130:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800133:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800139:	e8 cf 09 00 00       	call   800b0d <sys_getenvid>
  80013e:	83 ec 0c             	sub    $0xc,%esp
  800141:	ff 75 0c             	pushl  0xc(%ebp)
  800144:	ff 75 08             	pushl  0x8(%ebp)
  800147:	56                   	push   %esi
  800148:	50                   	push   %eax
  800149:	68 b8 10 80 00       	push   $0x8010b8
  80014e:	e8 b0 00 00 00       	call   800203 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800153:	83 c4 18             	add    $0x18,%esp
  800156:	53                   	push   %ebx
  800157:	ff 75 10             	pushl  0x10(%ebp)
  80015a:	e8 53 00 00 00       	call   8001b2 <vcprintf>
	cprintf("\n");
  80015f:	c7 04 24 5e 10 80 00 	movl   $0x80105e,(%esp)
  800166:	e8 98 00 00 00       	call   800203 <cprintf>
  80016b:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80016e:	cc                   	int3   
  80016f:	eb fd                	jmp    80016e <_panic+0x43>

00800171 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800171:	55                   	push   %ebp
  800172:	89 e5                	mov    %esp,%ebp
  800174:	53                   	push   %ebx
  800175:	83 ec 04             	sub    $0x4,%esp
  800178:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80017b:	8b 13                	mov    (%ebx),%edx
  80017d:	8d 42 01             	lea    0x1(%edx),%eax
  800180:	89 03                	mov    %eax,(%ebx)
  800182:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800185:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800189:	3d ff 00 00 00       	cmp    $0xff,%eax
  80018e:	75 1a                	jne    8001aa <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800190:	83 ec 08             	sub    $0x8,%esp
  800193:	68 ff 00 00 00       	push   $0xff
  800198:	8d 43 08             	lea    0x8(%ebx),%eax
  80019b:	50                   	push   %eax
  80019c:	e8 ee 08 00 00       	call   800a8f <sys_cputs>
		b->idx = 0;
  8001a1:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001a7:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001aa:	ff 43 04             	incl   0x4(%ebx)
}
  8001ad:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001b0:	c9                   	leave  
  8001b1:	c3                   	ret    

008001b2 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001b2:	55                   	push   %ebp
  8001b3:	89 e5                	mov    %esp,%ebp
  8001b5:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001bb:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001c2:	00 00 00 
	b.cnt = 0;
  8001c5:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001cc:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001cf:	ff 75 0c             	pushl  0xc(%ebp)
  8001d2:	ff 75 08             	pushl  0x8(%ebp)
  8001d5:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001db:	50                   	push   %eax
  8001dc:	68 71 01 80 00       	push   $0x800171
  8001e1:	e8 51 01 00 00       	call   800337 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001e6:	83 c4 08             	add    $0x8,%esp
  8001e9:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001ef:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001f5:	50                   	push   %eax
  8001f6:	e8 94 08 00 00       	call   800a8f <sys_cputs>

	return b.cnt;
}
  8001fb:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800201:	c9                   	leave  
  800202:	c3                   	ret    

00800203 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800203:	55                   	push   %ebp
  800204:	89 e5                	mov    %esp,%ebp
  800206:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800209:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80020c:	50                   	push   %eax
  80020d:	ff 75 08             	pushl  0x8(%ebp)
  800210:	e8 9d ff ff ff       	call   8001b2 <vcprintf>
	va_end(ap);

	return cnt;
}
  800215:	c9                   	leave  
  800216:	c3                   	ret    

00800217 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800217:	55                   	push   %ebp
  800218:	89 e5                	mov    %esp,%ebp
  80021a:	57                   	push   %edi
  80021b:	56                   	push   %esi
  80021c:	53                   	push   %ebx
  80021d:	83 ec 1c             	sub    $0x1c,%esp
  800220:	89 c7                	mov    %eax,%edi
  800222:	89 d6                	mov    %edx,%esi
  800224:	8b 45 08             	mov    0x8(%ebp),%eax
  800227:	8b 55 0c             	mov    0xc(%ebp),%edx
  80022a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80022d:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800230:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800233:	bb 00 00 00 00       	mov    $0x0,%ebx
  800238:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80023b:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80023e:	39 d3                	cmp    %edx,%ebx
  800240:	72 05                	jb     800247 <printnum+0x30>
  800242:	39 45 10             	cmp    %eax,0x10(%ebp)
  800245:	77 45                	ja     80028c <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800247:	83 ec 0c             	sub    $0xc,%esp
  80024a:	ff 75 18             	pushl  0x18(%ebp)
  80024d:	8b 45 14             	mov    0x14(%ebp),%eax
  800250:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800253:	53                   	push   %ebx
  800254:	ff 75 10             	pushl  0x10(%ebp)
  800257:	83 ec 08             	sub    $0x8,%esp
  80025a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80025d:	ff 75 e0             	pushl  -0x20(%ebp)
  800260:	ff 75 dc             	pushl  -0x24(%ebp)
  800263:	ff 75 d8             	pushl  -0x28(%ebp)
  800266:	e8 71 0b 00 00       	call   800ddc <__udivdi3>
  80026b:	83 c4 18             	add    $0x18,%esp
  80026e:	52                   	push   %edx
  80026f:	50                   	push   %eax
  800270:	89 f2                	mov    %esi,%edx
  800272:	89 f8                	mov    %edi,%eax
  800274:	e8 9e ff ff ff       	call   800217 <printnum>
  800279:	83 c4 20             	add    $0x20,%esp
  80027c:	eb 16                	jmp    800294 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80027e:	83 ec 08             	sub    $0x8,%esp
  800281:	56                   	push   %esi
  800282:	ff 75 18             	pushl  0x18(%ebp)
  800285:	ff d7                	call   *%edi
  800287:	83 c4 10             	add    $0x10,%esp
  80028a:	eb 03                	jmp    80028f <printnum+0x78>
  80028c:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80028f:	4b                   	dec    %ebx
  800290:	85 db                	test   %ebx,%ebx
  800292:	7f ea                	jg     80027e <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800294:	83 ec 08             	sub    $0x8,%esp
  800297:	56                   	push   %esi
  800298:	83 ec 04             	sub    $0x4,%esp
  80029b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80029e:	ff 75 e0             	pushl  -0x20(%ebp)
  8002a1:	ff 75 dc             	pushl  -0x24(%ebp)
  8002a4:	ff 75 d8             	pushl  -0x28(%ebp)
  8002a7:	e8 40 0c 00 00       	call   800eec <__umoddi3>
  8002ac:	83 c4 14             	add    $0x14,%esp
  8002af:	0f be 80 db 10 80 00 	movsbl 0x8010db(%eax),%eax
  8002b6:	50                   	push   %eax
  8002b7:	ff d7                	call   *%edi
}
  8002b9:	83 c4 10             	add    $0x10,%esp
  8002bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002bf:	5b                   	pop    %ebx
  8002c0:	5e                   	pop    %esi
  8002c1:	5f                   	pop    %edi
  8002c2:	5d                   	pop    %ebp
  8002c3:	c3                   	ret    

008002c4 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002c4:	55                   	push   %ebp
  8002c5:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002c7:	83 fa 01             	cmp    $0x1,%edx
  8002ca:	7e 0e                	jle    8002da <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002cc:	8b 10                	mov    (%eax),%edx
  8002ce:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002d1:	89 08                	mov    %ecx,(%eax)
  8002d3:	8b 02                	mov    (%edx),%eax
  8002d5:	8b 52 04             	mov    0x4(%edx),%edx
  8002d8:	eb 22                	jmp    8002fc <getuint+0x38>
	else if (lflag)
  8002da:	85 d2                	test   %edx,%edx
  8002dc:	74 10                	je     8002ee <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002de:	8b 10                	mov    (%eax),%edx
  8002e0:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002e3:	89 08                	mov    %ecx,(%eax)
  8002e5:	8b 02                	mov    (%edx),%eax
  8002e7:	ba 00 00 00 00       	mov    $0x0,%edx
  8002ec:	eb 0e                	jmp    8002fc <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002ee:	8b 10                	mov    (%eax),%edx
  8002f0:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002f3:	89 08                	mov    %ecx,(%eax)
  8002f5:	8b 02                	mov    (%edx),%eax
  8002f7:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002fc:	5d                   	pop    %ebp
  8002fd:	c3                   	ret    

008002fe <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002fe:	55                   	push   %ebp
  8002ff:	89 e5                	mov    %esp,%ebp
  800301:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800304:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800307:	8b 10                	mov    (%eax),%edx
  800309:	3b 50 04             	cmp    0x4(%eax),%edx
  80030c:	73 0a                	jae    800318 <sprintputch+0x1a>
		*b->buf++ = ch;
  80030e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800311:	89 08                	mov    %ecx,(%eax)
  800313:	8b 45 08             	mov    0x8(%ebp),%eax
  800316:	88 02                	mov    %al,(%edx)
}
  800318:	5d                   	pop    %ebp
  800319:	c3                   	ret    

0080031a <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80031a:	55                   	push   %ebp
  80031b:	89 e5                	mov    %esp,%ebp
  80031d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800320:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800323:	50                   	push   %eax
  800324:	ff 75 10             	pushl  0x10(%ebp)
  800327:	ff 75 0c             	pushl  0xc(%ebp)
  80032a:	ff 75 08             	pushl  0x8(%ebp)
  80032d:	e8 05 00 00 00       	call   800337 <vprintfmt>
	va_end(ap);
}
  800332:	83 c4 10             	add    $0x10,%esp
  800335:	c9                   	leave  
  800336:	c3                   	ret    

00800337 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800337:	55                   	push   %ebp
  800338:	89 e5                	mov    %esp,%ebp
  80033a:	57                   	push   %edi
  80033b:	56                   	push   %esi
  80033c:	53                   	push   %ebx
  80033d:	83 ec 2c             	sub    $0x2c,%esp
  800340:	8b 75 08             	mov    0x8(%ebp),%esi
  800343:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800346:	8b 7d 10             	mov    0x10(%ebp),%edi
  800349:	eb 12                	jmp    80035d <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80034b:	85 c0                	test   %eax,%eax
  80034d:	0f 84 68 03 00 00    	je     8006bb <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800353:	83 ec 08             	sub    $0x8,%esp
  800356:	53                   	push   %ebx
  800357:	50                   	push   %eax
  800358:	ff d6                	call   *%esi
  80035a:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80035d:	47                   	inc    %edi
  80035e:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800362:	83 f8 25             	cmp    $0x25,%eax
  800365:	75 e4                	jne    80034b <vprintfmt+0x14>
  800367:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80036b:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800372:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800379:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800380:	ba 00 00 00 00       	mov    $0x0,%edx
  800385:	eb 07                	jmp    80038e <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800387:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80038a:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80038e:	8d 47 01             	lea    0x1(%edi),%eax
  800391:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800394:	0f b6 0f             	movzbl (%edi),%ecx
  800397:	8a 07                	mov    (%edi),%al
  800399:	83 e8 23             	sub    $0x23,%eax
  80039c:	3c 55                	cmp    $0x55,%al
  80039e:	0f 87 fe 02 00 00    	ja     8006a2 <vprintfmt+0x36b>
  8003a4:	0f b6 c0             	movzbl %al,%eax
  8003a7:	ff 24 85 20 12 80 00 	jmp    *0x801220(,%eax,4)
  8003ae:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003b1:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003b5:	eb d7                	jmp    80038e <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003ba:	b8 00 00 00 00       	mov    $0x0,%eax
  8003bf:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003c2:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003c5:	01 c0                	add    %eax,%eax
  8003c7:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003cb:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003ce:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003d1:	83 fa 09             	cmp    $0x9,%edx
  8003d4:	77 34                	ja     80040a <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003d6:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003d7:	eb e9                	jmp    8003c2 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003d9:	8b 45 14             	mov    0x14(%ebp),%eax
  8003dc:	8d 48 04             	lea    0x4(%eax),%ecx
  8003df:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003e2:	8b 00                	mov    (%eax),%eax
  8003e4:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003ea:	eb 24                	jmp    800410 <vprintfmt+0xd9>
  8003ec:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003f0:	79 07                	jns    8003f9 <vprintfmt+0xc2>
  8003f2:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003fc:	eb 90                	jmp    80038e <vprintfmt+0x57>
  8003fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800401:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800408:	eb 84                	jmp    80038e <vprintfmt+0x57>
  80040a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80040d:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800410:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800414:	0f 89 74 ff ff ff    	jns    80038e <vprintfmt+0x57>
				width = precision, precision = -1;
  80041a:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80041d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800420:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800427:	e9 62 ff ff ff       	jmp    80038e <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80042c:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80042d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800430:	e9 59 ff ff ff       	jmp    80038e <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800435:	8b 45 14             	mov    0x14(%ebp),%eax
  800438:	8d 50 04             	lea    0x4(%eax),%edx
  80043b:	89 55 14             	mov    %edx,0x14(%ebp)
  80043e:	83 ec 08             	sub    $0x8,%esp
  800441:	53                   	push   %ebx
  800442:	ff 30                	pushl  (%eax)
  800444:	ff d6                	call   *%esi
			break;
  800446:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800449:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80044c:	e9 0c ff ff ff       	jmp    80035d <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800451:	8b 45 14             	mov    0x14(%ebp),%eax
  800454:	8d 50 04             	lea    0x4(%eax),%edx
  800457:	89 55 14             	mov    %edx,0x14(%ebp)
  80045a:	8b 00                	mov    (%eax),%eax
  80045c:	85 c0                	test   %eax,%eax
  80045e:	79 02                	jns    800462 <vprintfmt+0x12b>
  800460:	f7 d8                	neg    %eax
  800462:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800464:	83 f8 0f             	cmp    $0xf,%eax
  800467:	7f 0b                	jg     800474 <vprintfmt+0x13d>
  800469:	8b 04 85 80 13 80 00 	mov    0x801380(,%eax,4),%eax
  800470:	85 c0                	test   %eax,%eax
  800472:	75 18                	jne    80048c <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800474:	52                   	push   %edx
  800475:	68 f3 10 80 00       	push   $0x8010f3
  80047a:	53                   	push   %ebx
  80047b:	56                   	push   %esi
  80047c:	e8 99 fe ff ff       	call   80031a <printfmt>
  800481:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800484:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800487:	e9 d1 fe ff ff       	jmp    80035d <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80048c:	50                   	push   %eax
  80048d:	68 fc 10 80 00       	push   $0x8010fc
  800492:	53                   	push   %ebx
  800493:	56                   	push   %esi
  800494:	e8 81 fe ff ff       	call   80031a <printfmt>
  800499:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80049c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80049f:	e9 b9 fe ff ff       	jmp    80035d <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004a4:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a7:	8d 50 04             	lea    0x4(%eax),%edx
  8004aa:	89 55 14             	mov    %edx,0x14(%ebp)
  8004ad:	8b 38                	mov    (%eax),%edi
  8004af:	85 ff                	test   %edi,%edi
  8004b1:	75 05                	jne    8004b8 <vprintfmt+0x181>
				p = "(null)";
  8004b3:	bf ec 10 80 00       	mov    $0x8010ec,%edi
			if (width > 0 && padc != '-')
  8004b8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004bc:	0f 8e 90 00 00 00    	jle    800552 <vprintfmt+0x21b>
  8004c2:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004c6:	0f 84 8e 00 00 00    	je     80055a <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004cc:	83 ec 08             	sub    $0x8,%esp
  8004cf:	ff 75 d0             	pushl  -0x30(%ebp)
  8004d2:	57                   	push   %edi
  8004d3:	e8 70 02 00 00       	call   800748 <strnlen>
  8004d8:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004db:	29 c1                	sub    %eax,%ecx
  8004dd:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004e0:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004e3:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004e7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004ea:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004ed:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004ef:	eb 0d                	jmp    8004fe <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004f1:	83 ec 08             	sub    $0x8,%esp
  8004f4:	53                   	push   %ebx
  8004f5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004f8:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004fa:	4f                   	dec    %edi
  8004fb:	83 c4 10             	add    $0x10,%esp
  8004fe:	85 ff                	test   %edi,%edi
  800500:	7f ef                	jg     8004f1 <vprintfmt+0x1ba>
  800502:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800505:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800508:	89 c8                	mov    %ecx,%eax
  80050a:	85 c9                	test   %ecx,%ecx
  80050c:	79 05                	jns    800513 <vprintfmt+0x1dc>
  80050e:	b8 00 00 00 00       	mov    $0x0,%eax
  800513:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800516:	29 c1                	sub    %eax,%ecx
  800518:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80051b:	89 75 08             	mov    %esi,0x8(%ebp)
  80051e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800521:	eb 3d                	jmp    800560 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800523:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800527:	74 19                	je     800542 <vprintfmt+0x20b>
  800529:	0f be c0             	movsbl %al,%eax
  80052c:	83 e8 20             	sub    $0x20,%eax
  80052f:	83 f8 5e             	cmp    $0x5e,%eax
  800532:	76 0e                	jbe    800542 <vprintfmt+0x20b>
					putch('?', putdat);
  800534:	83 ec 08             	sub    $0x8,%esp
  800537:	53                   	push   %ebx
  800538:	6a 3f                	push   $0x3f
  80053a:	ff 55 08             	call   *0x8(%ebp)
  80053d:	83 c4 10             	add    $0x10,%esp
  800540:	eb 0b                	jmp    80054d <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800542:	83 ec 08             	sub    $0x8,%esp
  800545:	53                   	push   %ebx
  800546:	52                   	push   %edx
  800547:	ff 55 08             	call   *0x8(%ebp)
  80054a:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80054d:	ff 4d e4             	decl   -0x1c(%ebp)
  800550:	eb 0e                	jmp    800560 <vprintfmt+0x229>
  800552:	89 75 08             	mov    %esi,0x8(%ebp)
  800555:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800558:	eb 06                	jmp    800560 <vprintfmt+0x229>
  80055a:	89 75 08             	mov    %esi,0x8(%ebp)
  80055d:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800560:	47                   	inc    %edi
  800561:	8a 47 ff             	mov    -0x1(%edi),%al
  800564:	0f be d0             	movsbl %al,%edx
  800567:	85 d2                	test   %edx,%edx
  800569:	74 1d                	je     800588 <vprintfmt+0x251>
  80056b:	85 f6                	test   %esi,%esi
  80056d:	78 b4                	js     800523 <vprintfmt+0x1ec>
  80056f:	4e                   	dec    %esi
  800570:	79 b1                	jns    800523 <vprintfmt+0x1ec>
  800572:	8b 75 08             	mov    0x8(%ebp),%esi
  800575:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800578:	eb 14                	jmp    80058e <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80057a:	83 ec 08             	sub    $0x8,%esp
  80057d:	53                   	push   %ebx
  80057e:	6a 20                	push   $0x20
  800580:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800582:	4f                   	dec    %edi
  800583:	83 c4 10             	add    $0x10,%esp
  800586:	eb 06                	jmp    80058e <vprintfmt+0x257>
  800588:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80058b:	8b 75 08             	mov    0x8(%ebp),%esi
  80058e:	85 ff                	test   %edi,%edi
  800590:	7f e8                	jg     80057a <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800592:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800595:	e9 c3 fd ff ff       	jmp    80035d <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80059a:	83 fa 01             	cmp    $0x1,%edx
  80059d:	7e 16                	jle    8005b5 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80059f:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a2:	8d 50 08             	lea    0x8(%eax),%edx
  8005a5:	89 55 14             	mov    %edx,0x14(%ebp)
  8005a8:	8b 50 04             	mov    0x4(%eax),%edx
  8005ab:	8b 00                	mov    (%eax),%eax
  8005ad:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005b0:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005b3:	eb 32                	jmp    8005e7 <vprintfmt+0x2b0>
	else if (lflag)
  8005b5:	85 d2                	test   %edx,%edx
  8005b7:	74 18                	je     8005d1 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005b9:	8b 45 14             	mov    0x14(%ebp),%eax
  8005bc:	8d 50 04             	lea    0x4(%eax),%edx
  8005bf:	89 55 14             	mov    %edx,0x14(%ebp)
  8005c2:	8b 00                	mov    (%eax),%eax
  8005c4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005c7:	89 c1                	mov    %eax,%ecx
  8005c9:	c1 f9 1f             	sar    $0x1f,%ecx
  8005cc:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005cf:	eb 16                	jmp    8005e7 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005d1:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d4:	8d 50 04             	lea    0x4(%eax),%edx
  8005d7:	89 55 14             	mov    %edx,0x14(%ebp)
  8005da:	8b 00                	mov    (%eax),%eax
  8005dc:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005df:	89 c1                	mov    %eax,%ecx
  8005e1:	c1 f9 1f             	sar    $0x1f,%ecx
  8005e4:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005e7:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005ea:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005ed:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005f1:	79 76                	jns    800669 <vprintfmt+0x332>
				putch('-', putdat);
  8005f3:	83 ec 08             	sub    $0x8,%esp
  8005f6:	53                   	push   %ebx
  8005f7:	6a 2d                	push   $0x2d
  8005f9:	ff d6                	call   *%esi
				num = -(long long) num;
  8005fb:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005fe:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800601:	f7 d8                	neg    %eax
  800603:	83 d2 00             	adc    $0x0,%edx
  800606:	f7 da                	neg    %edx
  800608:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80060b:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800610:	eb 5c                	jmp    80066e <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800612:	8d 45 14             	lea    0x14(%ebp),%eax
  800615:	e8 aa fc ff ff       	call   8002c4 <getuint>
			base = 10;
  80061a:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80061f:	eb 4d                	jmp    80066e <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800621:	8d 45 14             	lea    0x14(%ebp),%eax
  800624:	e8 9b fc ff ff       	call   8002c4 <getuint>
			base = 8;
  800629:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80062e:	eb 3e                	jmp    80066e <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800630:	83 ec 08             	sub    $0x8,%esp
  800633:	53                   	push   %ebx
  800634:	6a 30                	push   $0x30
  800636:	ff d6                	call   *%esi
			putch('x', putdat);
  800638:	83 c4 08             	add    $0x8,%esp
  80063b:	53                   	push   %ebx
  80063c:	6a 78                	push   $0x78
  80063e:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800640:	8b 45 14             	mov    0x14(%ebp),%eax
  800643:	8d 50 04             	lea    0x4(%eax),%edx
  800646:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800649:	8b 00                	mov    (%eax),%eax
  80064b:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800650:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800653:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800658:	eb 14                	jmp    80066e <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80065a:	8d 45 14             	lea    0x14(%ebp),%eax
  80065d:	e8 62 fc ff ff       	call   8002c4 <getuint>
			base = 16;
  800662:	b9 10 00 00 00       	mov    $0x10,%ecx
  800667:	eb 05                	jmp    80066e <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800669:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80066e:	83 ec 0c             	sub    $0xc,%esp
  800671:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800675:	57                   	push   %edi
  800676:	ff 75 e4             	pushl  -0x1c(%ebp)
  800679:	51                   	push   %ecx
  80067a:	52                   	push   %edx
  80067b:	50                   	push   %eax
  80067c:	89 da                	mov    %ebx,%edx
  80067e:	89 f0                	mov    %esi,%eax
  800680:	e8 92 fb ff ff       	call   800217 <printnum>
			break;
  800685:	83 c4 20             	add    $0x20,%esp
  800688:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80068b:	e9 cd fc ff ff       	jmp    80035d <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800690:	83 ec 08             	sub    $0x8,%esp
  800693:	53                   	push   %ebx
  800694:	51                   	push   %ecx
  800695:	ff d6                	call   *%esi
			break;
  800697:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80069a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80069d:	e9 bb fc ff ff       	jmp    80035d <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006a2:	83 ec 08             	sub    $0x8,%esp
  8006a5:	53                   	push   %ebx
  8006a6:	6a 25                	push   $0x25
  8006a8:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006aa:	83 c4 10             	add    $0x10,%esp
  8006ad:	eb 01                	jmp    8006b0 <vprintfmt+0x379>
  8006af:	4f                   	dec    %edi
  8006b0:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006b4:	75 f9                	jne    8006af <vprintfmt+0x378>
  8006b6:	e9 a2 fc ff ff       	jmp    80035d <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006bb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006be:	5b                   	pop    %ebx
  8006bf:	5e                   	pop    %esi
  8006c0:	5f                   	pop    %edi
  8006c1:	5d                   	pop    %ebp
  8006c2:	c3                   	ret    

008006c3 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006c3:	55                   	push   %ebp
  8006c4:	89 e5                	mov    %esp,%ebp
  8006c6:	83 ec 18             	sub    $0x18,%esp
  8006c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8006cc:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006cf:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006d2:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006d6:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006d9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006e0:	85 c0                	test   %eax,%eax
  8006e2:	74 26                	je     80070a <vsnprintf+0x47>
  8006e4:	85 d2                	test   %edx,%edx
  8006e6:	7e 29                	jle    800711 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006e8:	ff 75 14             	pushl  0x14(%ebp)
  8006eb:	ff 75 10             	pushl  0x10(%ebp)
  8006ee:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006f1:	50                   	push   %eax
  8006f2:	68 fe 02 80 00       	push   $0x8002fe
  8006f7:	e8 3b fc ff ff       	call   800337 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006ff:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800702:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800705:	83 c4 10             	add    $0x10,%esp
  800708:	eb 0c                	jmp    800716 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80070a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80070f:	eb 05                	jmp    800716 <vsnprintf+0x53>
  800711:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800716:	c9                   	leave  
  800717:	c3                   	ret    

00800718 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800718:	55                   	push   %ebp
  800719:	89 e5                	mov    %esp,%ebp
  80071b:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80071e:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800721:	50                   	push   %eax
  800722:	ff 75 10             	pushl  0x10(%ebp)
  800725:	ff 75 0c             	pushl  0xc(%ebp)
  800728:	ff 75 08             	pushl  0x8(%ebp)
  80072b:	e8 93 ff ff ff       	call   8006c3 <vsnprintf>
	va_end(ap);

	return rc;
}
  800730:	c9                   	leave  
  800731:	c3                   	ret    

00800732 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800732:	55                   	push   %ebp
  800733:	89 e5                	mov    %esp,%ebp
  800735:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800738:	b8 00 00 00 00       	mov    $0x0,%eax
  80073d:	eb 01                	jmp    800740 <strlen+0xe>
		n++;
  80073f:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800740:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800744:	75 f9                	jne    80073f <strlen+0xd>
		n++;
	return n;
}
  800746:	5d                   	pop    %ebp
  800747:	c3                   	ret    

00800748 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800748:	55                   	push   %ebp
  800749:	89 e5                	mov    %esp,%ebp
  80074b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80074e:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800751:	ba 00 00 00 00       	mov    $0x0,%edx
  800756:	eb 01                	jmp    800759 <strnlen+0x11>
		n++;
  800758:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800759:	39 c2                	cmp    %eax,%edx
  80075b:	74 08                	je     800765 <strnlen+0x1d>
  80075d:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800761:	75 f5                	jne    800758 <strnlen+0x10>
  800763:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800765:	5d                   	pop    %ebp
  800766:	c3                   	ret    

00800767 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800767:	55                   	push   %ebp
  800768:	89 e5                	mov    %esp,%ebp
  80076a:	53                   	push   %ebx
  80076b:	8b 45 08             	mov    0x8(%ebp),%eax
  80076e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800771:	89 c2                	mov    %eax,%edx
  800773:	42                   	inc    %edx
  800774:	41                   	inc    %ecx
  800775:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800778:	88 5a ff             	mov    %bl,-0x1(%edx)
  80077b:	84 db                	test   %bl,%bl
  80077d:	75 f4                	jne    800773 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80077f:	5b                   	pop    %ebx
  800780:	5d                   	pop    %ebp
  800781:	c3                   	ret    

00800782 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800782:	55                   	push   %ebp
  800783:	89 e5                	mov    %esp,%ebp
  800785:	53                   	push   %ebx
  800786:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800789:	53                   	push   %ebx
  80078a:	e8 a3 ff ff ff       	call   800732 <strlen>
  80078f:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800792:	ff 75 0c             	pushl  0xc(%ebp)
  800795:	01 d8                	add    %ebx,%eax
  800797:	50                   	push   %eax
  800798:	e8 ca ff ff ff       	call   800767 <strcpy>
	return dst;
}
  80079d:	89 d8                	mov    %ebx,%eax
  80079f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007a2:	c9                   	leave  
  8007a3:	c3                   	ret    

008007a4 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007a4:	55                   	push   %ebp
  8007a5:	89 e5                	mov    %esp,%ebp
  8007a7:	56                   	push   %esi
  8007a8:	53                   	push   %ebx
  8007a9:	8b 75 08             	mov    0x8(%ebp),%esi
  8007ac:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007af:	89 f3                	mov    %esi,%ebx
  8007b1:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007b4:	89 f2                	mov    %esi,%edx
  8007b6:	eb 0c                	jmp    8007c4 <strncpy+0x20>
		*dst++ = *src;
  8007b8:	42                   	inc    %edx
  8007b9:	8a 01                	mov    (%ecx),%al
  8007bb:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007be:	80 39 01             	cmpb   $0x1,(%ecx)
  8007c1:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007c4:	39 da                	cmp    %ebx,%edx
  8007c6:	75 f0                	jne    8007b8 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007c8:	89 f0                	mov    %esi,%eax
  8007ca:	5b                   	pop    %ebx
  8007cb:	5e                   	pop    %esi
  8007cc:	5d                   	pop    %ebp
  8007cd:	c3                   	ret    

008007ce <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007ce:	55                   	push   %ebp
  8007cf:	89 e5                	mov    %esp,%ebp
  8007d1:	56                   	push   %esi
  8007d2:	53                   	push   %ebx
  8007d3:	8b 75 08             	mov    0x8(%ebp),%esi
  8007d6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007d9:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007dc:	85 c0                	test   %eax,%eax
  8007de:	74 1e                	je     8007fe <strlcpy+0x30>
  8007e0:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007e4:	89 f2                	mov    %esi,%edx
  8007e6:	eb 05                	jmp    8007ed <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007e8:	42                   	inc    %edx
  8007e9:	41                   	inc    %ecx
  8007ea:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007ed:	39 c2                	cmp    %eax,%edx
  8007ef:	74 08                	je     8007f9 <strlcpy+0x2b>
  8007f1:	8a 19                	mov    (%ecx),%bl
  8007f3:	84 db                	test   %bl,%bl
  8007f5:	75 f1                	jne    8007e8 <strlcpy+0x1a>
  8007f7:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007f9:	c6 00 00             	movb   $0x0,(%eax)
  8007fc:	eb 02                	jmp    800800 <strlcpy+0x32>
  8007fe:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800800:	29 f0                	sub    %esi,%eax
}
  800802:	5b                   	pop    %ebx
  800803:	5e                   	pop    %esi
  800804:	5d                   	pop    %ebp
  800805:	c3                   	ret    

00800806 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800806:	55                   	push   %ebp
  800807:	89 e5                	mov    %esp,%ebp
  800809:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80080c:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80080f:	eb 02                	jmp    800813 <strcmp+0xd>
		p++, q++;
  800811:	41                   	inc    %ecx
  800812:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800813:	8a 01                	mov    (%ecx),%al
  800815:	84 c0                	test   %al,%al
  800817:	74 04                	je     80081d <strcmp+0x17>
  800819:	3a 02                	cmp    (%edx),%al
  80081b:	74 f4                	je     800811 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80081d:	0f b6 c0             	movzbl %al,%eax
  800820:	0f b6 12             	movzbl (%edx),%edx
  800823:	29 d0                	sub    %edx,%eax
}
  800825:	5d                   	pop    %ebp
  800826:	c3                   	ret    

00800827 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800827:	55                   	push   %ebp
  800828:	89 e5                	mov    %esp,%ebp
  80082a:	53                   	push   %ebx
  80082b:	8b 45 08             	mov    0x8(%ebp),%eax
  80082e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800831:	89 c3                	mov    %eax,%ebx
  800833:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800836:	eb 02                	jmp    80083a <strncmp+0x13>
		n--, p++, q++;
  800838:	40                   	inc    %eax
  800839:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80083a:	39 d8                	cmp    %ebx,%eax
  80083c:	74 14                	je     800852 <strncmp+0x2b>
  80083e:	8a 08                	mov    (%eax),%cl
  800840:	84 c9                	test   %cl,%cl
  800842:	74 04                	je     800848 <strncmp+0x21>
  800844:	3a 0a                	cmp    (%edx),%cl
  800846:	74 f0                	je     800838 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800848:	0f b6 00             	movzbl (%eax),%eax
  80084b:	0f b6 12             	movzbl (%edx),%edx
  80084e:	29 d0                	sub    %edx,%eax
  800850:	eb 05                	jmp    800857 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800852:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800857:	5b                   	pop    %ebx
  800858:	5d                   	pop    %ebp
  800859:	c3                   	ret    

0080085a <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80085a:	55                   	push   %ebp
  80085b:	89 e5                	mov    %esp,%ebp
  80085d:	8b 45 08             	mov    0x8(%ebp),%eax
  800860:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800863:	eb 05                	jmp    80086a <strchr+0x10>
		if (*s == c)
  800865:	38 ca                	cmp    %cl,%dl
  800867:	74 0c                	je     800875 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800869:	40                   	inc    %eax
  80086a:	8a 10                	mov    (%eax),%dl
  80086c:	84 d2                	test   %dl,%dl
  80086e:	75 f5                	jne    800865 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800870:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800875:	5d                   	pop    %ebp
  800876:	c3                   	ret    

00800877 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800877:	55                   	push   %ebp
  800878:	89 e5                	mov    %esp,%ebp
  80087a:	8b 45 08             	mov    0x8(%ebp),%eax
  80087d:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800880:	eb 05                	jmp    800887 <strfind+0x10>
		if (*s == c)
  800882:	38 ca                	cmp    %cl,%dl
  800884:	74 07                	je     80088d <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800886:	40                   	inc    %eax
  800887:	8a 10                	mov    (%eax),%dl
  800889:	84 d2                	test   %dl,%dl
  80088b:	75 f5                	jne    800882 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80088d:	5d                   	pop    %ebp
  80088e:	c3                   	ret    

0080088f <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80088f:	55                   	push   %ebp
  800890:	89 e5                	mov    %esp,%ebp
  800892:	57                   	push   %edi
  800893:	56                   	push   %esi
  800894:	53                   	push   %ebx
  800895:	8b 7d 08             	mov    0x8(%ebp),%edi
  800898:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80089b:	85 c9                	test   %ecx,%ecx
  80089d:	74 36                	je     8008d5 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80089f:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008a5:	75 28                	jne    8008cf <memset+0x40>
  8008a7:	f6 c1 03             	test   $0x3,%cl
  8008aa:	75 23                	jne    8008cf <memset+0x40>
		c &= 0xFF;
  8008ac:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008b0:	89 d3                	mov    %edx,%ebx
  8008b2:	c1 e3 08             	shl    $0x8,%ebx
  8008b5:	89 d6                	mov    %edx,%esi
  8008b7:	c1 e6 18             	shl    $0x18,%esi
  8008ba:	89 d0                	mov    %edx,%eax
  8008bc:	c1 e0 10             	shl    $0x10,%eax
  8008bf:	09 f0                	or     %esi,%eax
  8008c1:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008c3:	89 d8                	mov    %ebx,%eax
  8008c5:	09 d0                	or     %edx,%eax
  8008c7:	c1 e9 02             	shr    $0x2,%ecx
  8008ca:	fc                   	cld    
  8008cb:	f3 ab                	rep stos %eax,%es:(%edi)
  8008cd:	eb 06                	jmp    8008d5 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008cf:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008d2:	fc                   	cld    
  8008d3:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008d5:	89 f8                	mov    %edi,%eax
  8008d7:	5b                   	pop    %ebx
  8008d8:	5e                   	pop    %esi
  8008d9:	5f                   	pop    %edi
  8008da:	5d                   	pop    %ebp
  8008db:	c3                   	ret    

008008dc <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008dc:	55                   	push   %ebp
  8008dd:	89 e5                	mov    %esp,%ebp
  8008df:	57                   	push   %edi
  8008e0:	56                   	push   %esi
  8008e1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e4:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008e7:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008ea:	39 c6                	cmp    %eax,%esi
  8008ec:	73 33                	jae    800921 <memmove+0x45>
  8008ee:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008f1:	39 d0                	cmp    %edx,%eax
  8008f3:	73 2c                	jae    800921 <memmove+0x45>
		s += n;
		d += n;
  8008f5:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008f8:	89 d6                	mov    %edx,%esi
  8008fa:	09 fe                	or     %edi,%esi
  8008fc:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800902:	75 13                	jne    800917 <memmove+0x3b>
  800904:	f6 c1 03             	test   $0x3,%cl
  800907:	75 0e                	jne    800917 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800909:	83 ef 04             	sub    $0x4,%edi
  80090c:	8d 72 fc             	lea    -0x4(%edx),%esi
  80090f:	c1 e9 02             	shr    $0x2,%ecx
  800912:	fd                   	std    
  800913:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800915:	eb 07                	jmp    80091e <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800917:	4f                   	dec    %edi
  800918:	8d 72 ff             	lea    -0x1(%edx),%esi
  80091b:	fd                   	std    
  80091c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80091e:	fc                   	cld    
  80091f:	eb 1d                	jmp    80093e <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800921:	89 f2                	mov    %esi,%edx
  800923:	09 c2                	or     %eax,%edx
  800925:	f6 c2 03             	test   $0x3,%dl
  800928:	75 0f                	jne    800939 <memmove+0x5d>
  80092a:	f6 c1 03             	test   $0x3,%cl
  80092d:	75 0a                	jne    800939 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80092f:	c1 e9 02             	shr    $0x2,%ecx
  800932:	89 c7                	mov    %eax,%edi
  800934:	fc                   	cld    
  800935:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800937:	eb 05                	jmp    80093e <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800939:	89 c7                	mov    %eax,%edi
  80093b:	fc                   	cld    
  80093c:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80093e:	5e                   	pop    %esi
  80093f:	5f                   	pop    %edi
  800940:	5d                   	pop    %ebp
  800941:	c3                   	ret    

00800942 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800942:	55                   	push   %ebp
  800943:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800945:	ff 75 10             	pushl  0x10(%ebp)
  800948:	ff 75 0c             	pushl  0xc(%ebp)
  80094b:	ff 75 08             	pushl  0x8(%ebp)
  80094e:	e8 89 ff ff ff       	call   8008dc <memmove>
}
  800953:	c9                   	leave  
  800954:	c3                   	ret    

00800955 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800955:	55                   	push   %ebp
  800956:	89 e5                	mov    %esp,%ebp
  800958:	56                   	push   %esi
  800959:	53                   	push   %ebx
  80095a:	8b 45 08             	mov    0x8(%ebp),%eax
  80095d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800960:	89 c6                	mov    %eax,%esi
  800962:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800965:	eb 14                	jmp    80097b <memcmp+0x26>
		if (*s1 != *s2)
  800967:	8a 08                	mov    (%eax),%cl
  800969:	8a 1a                	mov    (%edx),%bl
  80096b:	38 d9                	cmp    %bl,%cl
  80096d:	74 0a                	je     800979 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80096f:	0f b6 c1             	movzbl %cl,%eax
  800972:	0f b6 db             	movzbl %bl,%ebx
  800975:	29 d8                	sub    %ebx,%eax
  800977:	eb 0b                	jmp    800984 <memcmp+0x2f>
		s1++, s2++;
  800979:	40                   	inc    %eax
  80097a:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80097b:	39 f0                	cmp    %esi,%eax
  80097d:	75 e8                	jne    800967 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80097f:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800984:	5b                   	pop    %ebx
  800985:	5e                   	pop    %esi
  800986:	5d                   	pop    %ebp
  800987:	c3                   	ret    

00800988 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800988:	55                   	push   %ebp
  800989:	89 e5                	mov    %esp,%ebp
  80098b:	53                   	push   %ebx
  80098c:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80098f:	89 c1                	mov    %eax,%ecx
  800991:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800994:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800998:	eb 08                	jmp    8009a2 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80099a:	0f b6 10             	movzbl (%eax),%edx
  80099d:	39 da                	cmp    %ebx,%edx
  80099f:	74 05                	je     8009a6 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a1:	40                   	inc    %eax
  8009a2:	39 c8                	cmp    %ecx,%eax
  8009a4:	72 f4                	jb     80099a <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009a6:	5b                   	pop    %ebx
  8009a7:	5d                   	pop    %ebp
  8009a8:	c3                   	ret    

008009a9 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009a9:	55                   	push   %ebp
  8009aa:	89 e5                	mov    %esp,%ebp
  8009ac:	57                   	push   %edi
  8009ad:	56                   	push   %esi
  8009ae:	53                   	push   %ebx
  8009af:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009b2:	eb 01                	jmp    8009b5 <strtol+0xc>
		s++;
  8009b4:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009b5:	8a 01                	mov    (%ecx),%al
  8009b7:	3c 20                	cmp    $0x20,%al
  8009b9:	74 f9                	je     8009b4 <strtol+0xb>
  8009bb:	3c 09                	cmp    $0x9,%al
  8009bd:	74 f5                	je     8009b4 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009bf:	3c 2b                	cmp    $0x2b,%al
  8009c1:	75 08                	jne    8009cb <strtol+0x22>
		s++;
  8009c3:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009c4:	bf 00 00 00 00       	mov    $0x0,%edi
  8009c9:	eb 11                	jmp    8009dc <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009cb:	3c 2d                	cmp    $0x2d,%al
  8009cd:	75 08                	jne    8009d7 <strtol+0x2e>
		s++, neg = 1;
  8009cf:	41                   	inc    %ecx
  8009d0:	bf 01 00 00 00       	mov    $0x1,%edi
  8009d5:	eb 05                	jmp    8009dc <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009d7:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009dc:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009e0:	0f 84 87 00 00 00    	je     800a6d <strtol+0xc4>
  8009e6:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009ea:	75 27                	jne    800a13 <strtol+0x6a>
  8009ec:	80 39 30             	cmpb   $0x30,(%ecx)
  8009ef:	75 22                	jne    800a13 <strtol+0x6a>
  8009f1:	e9 88 00 00 00       	jmp    800a7e <strtol+0xd5>
		s += 2, base = 16;
  8009f6:	83 c1 02             	add    $0x2,%ecx
  8009f9:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a00:	eb 11                	jmp    800a13 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a02:	41                   	inc    %ecx
  800a03:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a0a:	eb 07                	jmp    800a13 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a0c:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a13:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a18:	8a 11                	mov    (%ecx),%dl
  800a1a:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a1d:	80 fb 09             	cmp    $0x9,%bl
  800a20:	77 08                	ja     800a2a <strtol+0x81>
			dig = *s - '0';
  800a22:	0f be d2             	movsbl %dl,%edx
  800a25:	83 ea 30             	sub    $0x30,%edx
  800a28:	eb 22                	jmp    800a4c <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a2a:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a2d:	89 f3                	mov    %esi,%ebx
  800a2f:	80 fb 19             	cmp    $0x19,%bl
  800a32:	77 08                	ja     800a3c <strtol+0x93>
			dig = *s - 'a' + 10;
  800a34:	0f be d2             	movsbl %dl,%edx
  800a37:	83 ea 57             	sub    $0x57,%edx
  800a3a:	eb 10                	jmp    800a4c <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a3c:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a3f:	89 f3                	mov    %esi,%ebx
  800a41:	80 fb 19             	cmp    $0x19,%bl
  800a44:	77 14                	ja     800a5a <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a46:	0f be d2             	movsbl %dl,%edx
  800a49:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a4c:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a4f:	7d 09                	jge    800a5a <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a51:	41                   	inc    %ecx
  800a52:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a56:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a58:	eb be                	jmp    800a18 <strtol+0x6f>

	if (endptr)
  800a5a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a5e:	74 05                	je     800a65 <strtol+0xbc>
		*endptr = (char *) s;
  800a60:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a63:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a65:	85 ff                	test   %edi,%edi
  800a67:	74 21                	je     800a8a <strtol+0xe1>
  800a69:	f7 d8                	neg    %eax
  800a6b:	eb 1d                	jmp    800a8a <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a6d:	80 39 30             	cmpb   $0x30,(%ecx)
  800a70:	75 9a                	jne    800a0c <strtol+0x63>
  800a72:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a76:	0f 84 7a ff ff ff    	je     8009f6 <strtol+0x4d>
  800a7c:	eb 84                	jmp    800a02 <strtol+0x59>
  800a7e:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a82:	0f 84 6e ff ff ff    	je     8009f6 <strtol+0x4d>
  800a88:	eb 89                	jmp    800a13 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a8a:	5b                   	pop    %ebx
  800a8b:	5e                   	pop    %esi
  800a8c:	5f                   	pop    %edi
  800a8d:	5d                   	pop    %ebp
  800a8e:	c3                   	ret    

00800a8f <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a8f:	55                   	push   %ebp
  800a90:	89 e5                	mov    %esp,%ebp
  800a92:	57                   	push   %edi
  800a93:	56                   	push   %esi
  800a94:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a95:	b8 00 00 00 00       	mov    $0x0,%eax
  800a9a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a9d:	8b 55 08             	mov    0x8(%ebp),%edx
  800aa0:	89 c3                	mov    %eax,%ebx
  800aa2:	89 c7                	mov    %eax,%edi
  800aa4:	89 c6                	mov    %eax,%esi
  800aa6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800aa8:	5b                   	pop    %ebx
  800aa9:	5e                   	pop    %esi
  800aaa:	5f                   	pop    %edi
  800aab:	5d                   	pop    %ebp
  800aac:	c3                   	ret    

00800aad <sys_cgetc>:

int
sys_cgetc(void)
{
  800aad:	55                   	push   %ebp
  800aae:	89 e5                	mov    %esp,%ebp
  800ab0:	57                   	push   %edi
  800ab1:	56                   	push   %esi
  800ab2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ab3:	ba 00 00 00 00       	mov    $0x0,%edx
  800ab8:	b8 01 00 00 00       	mov    $0x1,%eax
  800abd:	89 d1                	mov    %edx,%ecx
  800abf:	89 d3                	mov    %edx,%ebx
  800ac1:	89 d7                	mov    %edx,%edi
  800ac3:	89 d6                	mov    %edx,%esi
  800ac5:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800ac7:	5b                   	pop    %ebx
  800ac8:	5e                   	pop    %esi
  800ac9:	5f                   	pop    %edi
  800aca:	5d                   	pop    %ebp
  800acb:	c3                   	ret    

00800acc <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800acc:	55                   	push   %ebp
  800acd:	89 e5                	mov    %esp,%ebp
  800acf:	57                   	push   %edi
  800ad0:	56                   	push   %esi
  800ad1:	53                   	push   %ebx
  800ad2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ad5:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ada:	b8 03 00 00 00       	mov    $0x3,%eax
  800adf:	8b 55 08             	mov    0x8(%ebp),%edx
  800ae2:	89 cb                	mov    %ecx,%ebx
  800ae4:	89 cf                	mov    %ecx,%edi
  800ae6:	89 ce                	mov    %ecx,%esi
  800ae8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800aea:	85 c0                	test   %eax,%eax
  800aec:	7e 17                	jle    800b05 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800aee:	83 ec 0c             	sub    $0xc,%esp
  800af1:	50                   	push   %eax
  800af2:	6a 03                	push   $0x3
  800af4:	68 df 13 80 00       	push   $0x8013df
  800af9:	6a 23                	push   $0x23
  800afb:	68 fc 13 80 00       	push   $0x8013fc
  800b00:	e8 26 f6 ff ff       	call   80012b <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b05:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b08:	5b                   	pop    %ebx
  800b09:	5e                   	pop    %esi
  800b0a:	5f                   	pop    %edi
  800b0b:	5d                   	pop    %ebp
  800b0c:	c3                   	ret    

00800b0d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b0d:	55                   	push   %ebp
  800b0e:	89 e5                	mov    %esp,%ebp
  800b10:	57                   	push   %edi
  800b11:	56                   	push   %esi
  800b12:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b13:	ba 00 00 00 00       	mov    $0x0,%edx
  800b18:	b8 02 00 00 00       	mov    $0x2,%eax
  800b1d:	89 d1                	mov    %edx,%ecx
  800b1f:	89 d3                	mov    %edx,%ebx
  800b21:	89 d7                	mov    %edx,%edi
  800b23:	89 d6                	mov    %edx,%esi
  800b25:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b27:	5b                   	pop    %ebx
  800b28:	5e                   	pop    %esi
  800b29:	5f                   	pop    %edi
  800b2a:	5d                   	pop    %ebp
  800b2b:	c3                   	ret    

00800b2c <sys_yield>:

void
sys_yield(void)
{
  800b2c:	55                   	push   %ebp
  800b2d:	89 e5                	mov    %esp,%ebp
  800b2f:	57                   	push   %edi
  800b30:	56                   	push   %esi
  800b31:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b32:	ba 00 00 00 00       	mov    $0x0,%edx
  800b37:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b3c:	89 d1                	mov    %edx,%ecx
  800b3e:	89 d3                	mov    %edx,%ebx
  800b40:	89 d7                	mov    %edx,%edi
  800b42:	89 d6                	mov    %edx,%esi
  800b44:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b46:	5b                   	pop    %ebx
  800b47:	5e                   	pop    %esi
  800b48:	5f                   	pop    %edi
  800b49:	5d                   	pop    %ebp
  800b4a:	c3                   	ret    

00800b4b <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b4b:	55                   	push   %ebp
  800b4c:	89 e5                	mov    %esp,%ebp
  800b4e:	57                   	push   %edi
  800b4f:	56                   	push   %esi
  800b50:	53                   	push   %ebx
  800b51:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b54:	be 00 00 00 00       	mov    $0x0,%esi
  800b59:	b8 04 00 00 00       	mov    $0x4,%eax
  800b5e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b61:	8b 55 08             	mov    0x8(%ebp),%edx
  800b64:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b67:	89 f7                	mov    %esi,%edi
  800b69:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b6b:	85 c0                	test   %eax,%eax
  800b6d:	7e 17                	jle    800b86 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b6f:	83 ec 0c             	sub    $0xc,%esp
  800b72:	50                   	push   %eax
  800b73:	6a 04                	push   $0x4
  800b75:	68 df 13 80 00       	push   $0x8013df
  800b7a:	6a 23                	push   $0x23
  800b7c:	68 fc 13 80 00       	push   $0x8013fc
  800b81:	e8 a5 f5 ff ff       	call   80012b <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b86:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b89:	5b                   	pop    %ebx
  800b8a:	5e                   	pop    %esi
  800b8b:	5f                   	pop    %edi
  800b8c:	5d                   	pop    %ebp
  800b8d:	c3                   	ret    

00800b8e <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b8e:	55                   	push   %ebp
  800b8f:	89 e5                	mov    %esp,%ebp
  800b91:	57                   	push   %edi
  800b92:	56                   	push   %esi
  800b93:	53                   	push   %ebx
  800b94:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b97:	b8 05 00 00 00       	mov    $0x5,%eax
  800b9c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b9f:	8b 55 08             	mov    0x8(%ebp),%edx
  800ba2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ba5:	8b 7d 14             	mov    0x14(%ebp),%edi
  800ba8:	8b 75 18             	mov    0x18(%ebp),%esi
  800bab:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bad:	85 c0                	test   %eax,%eax
  800baf:	7e 17                	jle    800bc8 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bb1:	83 ec 0c             	sub    $0xc,%esp
  800bb4:	50                   	push   %eax
  800bb5:	6a 05                	push   $0x5
  800bb7:	68 df 13 80 00       	push   $0x8013df
  800bbc:	6a 23                	push   $0x23
  800bbe:	68 fc 13 80 00       	push   $0x8013fc
  800bc3:	e8 63 f5 ff ff       	call   80012b <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bc8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bcb:	5b                   	pop    %ebx
  800bcc:	5e                   	pop    %esi
  800bcd:	5f                   	pop    %edi
  800bce:	5d                   	pop    %ebp
  800bcf:	c3                   	ret    

00800bd0 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bd0:	55                   	push   %ebp
  800bd1:	89 e5                	mov    %esp,%ebp
  800bd3:	57                   	push   %edi
  800bd4:	56                   	push   %esi
  800bd5:	53                   	push   %ebx
  800bd6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bd9:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bde:	b8 06 00 00 00       	mov    $0x6,%eax
  800be3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800be6:	8b 55 08             	mov    0x8(%ebp),%edx
  800be9:	89 df                	mov    %ebx,%edi
  800beb:	89 de                	mov    %ebx,%esi
  800bed:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bef:	85 c0                	test   %eax,%eax
  800bf1:	7e 17                	jle    800c0a <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bf3:	83 ec 0c             	sub    $0xc,%esp
  800bf6:	50                   	push   %eax
  800bf7:	6a 06                	push   $0x6
  800bf9:	68 df 13 80 00       	push   $0x8013df
  800bfe:	6a 23                	push   $0x23
  800c00:	68 fc 13 80 00       	push   $0x8013fc
  800c05:	e8 21 f5 ff ff       	call   80012b <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c0a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c0d:	5b                   	pop    %ebx
  800c0e:	5e                   	pop    %esi
  800c0f:	5f                   	pop    %edi
  800c10:	5d                   	pop    %ebp
  800c11:	c3                   	ret    

00800c12 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c12:	55                   	push   %ebp
  800c13:	89 e5                	mov    %esp,%ebp
  800c15:	57                   	push   %edi
  800c16:	56                   	push   %esi
  800c17:	53                   	push   %ebx
  800c18:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c1b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c20:	b8 08 00 00 00       	mov    $0x8,%eax
  800c25:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c28:	8b 55 08             	mov    0x8(%ebp),%edx
  800c2b:	89 df                	mov    %ebx,%edi
  800c2d:	89 de                	mov    %ebx,%esi
  800c2f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c31:	85 c0                	test   %eax,%eax
  800c33:	7e 17                	jle    800c4c <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c35:	83 ec 0c             	sub    $0xc,%esp
  800c38:	50                   	push   %eax
  800c39:	6a 08                	push   $0x8
  800c3b:	68 df 13 80 00       	push   $0x8013df
  800c40:	6a 23                	push   $0x23
  800c42:	68 fc 13 80 00       	push   $0x8013fc
  800c47:	e8 df f4 ff ff       	call   80012b <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c4c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c4f:	5b                   	pop    %ebx
  800c50:	5e                   	pop    %esi
  800c51:	5f                   	pop    %edi
  800c52:	5d                   	pop    %ebp
  800c53:	c3                   	ret    

00800c54 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c54:	55                   	push   %ebp
  800c55:	89 e5                	mov    %esp,%ebp
  800c57:	57                   	push   %edi
  800c58:	56                   	push   %esi
  800c59:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c5a:	ba 00 00 00 00       	mov    $0x0,%edx
  800c5f:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c64:	89 d1                	mov    %edx,%ecx
  800c66:	89 d3                	mov    %edx,%ebx
  800c68:	89 d7                	mov    %edx,%edi
  800c6a:	89 d6                	mov    %edx,%esi
  800c6c:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c6e:	5b                   	pop    %ebx
  800c6f:	5e                   	pop    %esi
  800c70:	5f                   	pop    %edi
  800c71:	5d                   	pop    %ebp
  800c72:	c3                   	ret    

00800c73 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c73:	55                   	push   %ebp
  800c74:	89 e5                	mov    %esp,%ebp
  800c76:	57                   	push   %edi
  800c77:	56                   	push   %esi
  800c78:	53                   	push   %ebx
  800c79:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c7c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c81:	b8 09 00 00 00       	mov    $0x9,%eax
  800c86:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c89:	8b 55 08             	mov    0x8(%ebp),%edx
  800c8c:	89 df                	mov    %ebx,%edi
  800c8e:	89 de                	mov    %ebx,%esi
  800c90:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c92:	85 c0                	test   %eax,%eax
  800c94:	7e 17                	jle    800cad <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c96:	83 ec 0c             	sub    $0xc,%esp
  800c99:	50                   	push   %eax
  800c9a:	6a 09                	push   $0x9
  800c9c:	68 df 13 80 00       	push   $0x8013df
  800ca1:	6a 23                	push   $0x23
  800ca3:	68 fc 13 80 00       	push   $0x8013fc
  800ca8:	e8 7e f4 ff ff       	call   80012b <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800cad:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cb0:	5b                   	pop    %ebx
  800cb1:	5e                   	pop    %esi
  800cb2:	5f                   	pop    %edi
  800cb3:	5d                   	pop    %ebp
  800cb4:	c3                   	ret    

00800cb5 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800cb5:	55                   	push   %ebp
  800cb6:	89 e5                	mov    %esp,%ebp
  800cb8:	57                   	push   %edi
  800cb9:	56                   	push   %esi
  800cba:	53                   	push   %ebx
  800cbb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cbe:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cc3:	b8 0a 00 00 00       	mov    $0xa,%eax
  800cc8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ccb:	8b 55 08             	mov    0x8(%ebp),%edx
  800cce:	89 df                	mov    %ebx,%edi
  800cd0:	89 de                	mov    %ebx,%esi
  800cd2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cd4:	85 c0                	test   %eax,%eax
  800cd6:	7e 17                	jle    800cef <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cd8:	83 ec 0c             	sub    $0xc,%esp
  800cdb:	50                   	push   %eax
  800cdc:	6a 0a                	push   $0xa
  800cde:	68 df 13 80 00       	push   $0x8013df
  800ce3:	6a 23                	push   $0x23
  800ce5:	68 fc 13 80 00       	push   $0x8013fc
  800cea:	e8 3c f4 ff ff       	call   80012b <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cef:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cf2:	5b                   	pop    %ebx
  800cf3:	5e                   	pop    %esi
  800cf4:	5f                   	pop    %edi
  800cf5:	5d                   	pop    %ebp
  800cf6:	c3                   	ret    

00800cf7 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800cf7:	55                   	push   %ebp
  800cf8:	89 e5                	mov    %esp,%ebp
  800cfa:	57                   	push   %edi
  800cfb:	56                   	push   %esi
  800cfc:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cfd:	be 00 00 00 00       	mov    $0x0,%esi
  800d02:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d07:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d0a:	8b 55 08             	mov    0x8(%ebp),%edx
  800d0d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d10:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d13:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d15:	5b                   	pop    %ebx
  800d16:	5e                   	pop    %esi
  800d17:	5f                   	pop    %edi
  800d18:	5d                   	pop    %ebp
  800d19:	c3                   	ret    

00800d1a <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d1a:	55                   	push   %ebp
  800d1b:	89 e5                	mov    %esp,%ebp
  800d1d:	57                   	push   %edi
  800d1e:	56                   	push   %esi
  800d1f:	53                   	push   %ebx
  800d20:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d23:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d28:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d2d:	8b 55 08             	mov    0x8(%ebp),%edx
  800d30:	89 cb                	mov    %ecx,%ebx
  800d32:	89 cf                	mov    %ecx,%edi
  800d34:	89 ce                	mov    %ecx,%esi
  800d36:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d38:	85 c0                	test   %eax,%eax
  800d3a:	7e 17                	jle    800d53 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d3c:	83 ec 0c             	sub    $0xc,%esp
  800d3f:	50                   	push   %eax
  800d40:	6a 0e                	push   $0xe
  800d42:	68 df 13 80 00       	push   $0x8013df
  800d47:	6a 23                	push   $0x23
  800d49:	68 fc 13 80 00       	push   $0x8013fc
  800d4e:	e8 d8 f3 ff ff       	call   80012b <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d53:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d56:	5b                   	pop    %ebx
  800d57:	5e                   	pop    %esi
  800d58:	5f                   	pop    %edi
  800d59:	5d                   	pop    %ebp
  800d5a:	c3                   	ret    

00800d5b <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  800d5b:	55                   	push   %ebp
  800d5c:	89 e5                	mov    %esp,%ebp
  800d5e:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  800d61:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  800d68:	75 3e                	jne    800da8 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  800d6a:	83 ec 04             	sub    $0x4,%esp
  800d6d:	6a 07                	push   $0x7
  800d6f:	68 00 f0 bf ee       	push   $0xeebff000
  800d74:	6a 00                	push   $0x0
  800d76:	e8 d0 fd ff ff       	call   800b4b <sys_page_alloc>
  800d7b:	83 c4 10             	add    $0x10,%esp
  800d7e:	85 c0                	test   %eax,%eax
  800d80:	74 14                	je     800d96 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  800d82:	83 ec 04             	sub    $0x4,%esp
  800d85:	68 0c 14 80 00       	push   $0x80140c
  800d8a:	6a 23                	push   $0x23
  800d8c:	68 30 14 80 00       	push   $0x801430
  800d91:	e8 95 f3 ff ff       	call   80012b <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  800d96:	83 ec 08             	sub    $0x8,%esp
  800d99:	68 b2 0d 80 00       	push   $0x800db2
  800d9e:	6a 00                	push   $0x0
  800da0:	e8 10 ff ff ff       	call   800cb5 <sys_env_set_pgfault_upcall>
  800da5:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  800da8:	8b 45 08             	mov    0x8(%ebp),%eax
  800dab:	a3 08 20 80 00       	mov    %eax,0x802008
}
  800db0:	c9                   	leave  
  800db1:	c3                   	ret    

00800db2 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  800db2:	54                   	push   %esp
	movl _pgfault_handler, %eax
  800db3:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  800db8:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  800dba:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  800dbd:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  800dbf:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  800dc3:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  800dc7:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  800dc8:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  800dcb:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  800dcd:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  800dd0:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  800dd3:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  800dd4:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  800dd7:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  800dd8:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  800dd9:	c3                   	ret    
  800dda:	66 90                	xchg   %ax,%ax

00800ddc <__udivdi3>:
  800ddc:	55                   	push   %ebp
  800ddd:	57                   	push   %edi
  800dde:	56                   	push   %esi
  800ddf:	53                   	push   %ebx
  800de0:	83 ec 1c             	sub    $0x1c,%esp
  800de3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800de7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800deb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800def:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800df3:	89 ca                	mov    %ecx,%edx
  800df5:	89 f8                	mov    %edi,%eax
  800df7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800dfb:	85 f6                	test   %esi,%esi
  800dfd:	75 2d                	jne    800e2c <__udivdi3+0x50>
  800dff:	39 cf                	cmp    %ecx,%edi
  800e01:	77 65                	ja     800e68 <__udivdi3+0x8c>
  800e03:	89 fd                	mov    %edi,%ebp
  800e05:	85 ff                	test   %edi,%edi
  800e07:	75 0b                	jne    800e14 <__udivdi3+0x38>
  800e09:	b8 01 00 00 00       	mov    $0x1,%eax
  800e0e:	31 d2                	xor    %edx,%edx
  800e10:	f7 f7                	div    %edi
  800e12:	89 c5                	mov    %eax,%ebp
  800e14:	31 d2                	xor    %edx,%edx
  800e16:	89 c8                	mov    %ecx,%eax
  800e18:	f7 f5                	div    %ebp
  800e1a:	89 c1                	mov    %eax,%ecx
  800e1c:	89 d8                	mov    %ebx,%eax
  800e1e:	f7 f5                	div    %ebp
  800e20:	89 cf                	mov    %ecx,%edi
  800e22:	89 fa                	mov    %edi,%edx
  800e24:	83 c4 1c             	add    $0x1c,%esp
  800e27:	5b                   	pop    %ebx
  800e28:	5e                   	pop    %esi
  800e29:	5f                   	pop    %edi
  800e2a:	5d                   	pop    %ebp
  800e2b:	c3                   	ret    
  800e2c:	39 ce                	cmp    %ecx,%esi
  800e2e:	77 28                	ja     800e58 <__udivdi3+0x7c>
  800e30:	0f bd fe             	bsr    %esi,%edi
  800e33:	83 f7 1f             	xor    $0x1f,%edi
  800e36:	75 40                	jne    800e78 <__udivdi3+0x9c>
  800e38:	39 ce                	cmp    %ecx,%esi
  800e3a:	72 0a                	jb     800e46 <__udivdi3+0x6a>
  800e3c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800e40:	0f 87 9e 00 00 00    	ja     800ee4 <__udivdi3+0x108>
  800e46:	b8 01 00 00 00       	mov    $0x1,%eax
  800e4b:	89 fa                	mov    %edi,%edx
  800e4d:	83 c4 1c             	add    $0x1c,%esp
  800e50:	5b                   	pop    %ebx
  800e51:	5e                   	pop    %esi
  800e52:	5f                   	pop    %edi
  800e53:	5d                   	pop    %ebp
  800e54:	c3                   	ret    
  800e55:	8d 76 00             	lea    0x0(%esi),%esi
  800e58:	31 ff                	xor    %edi,%edi
  800e5a:	31 c0                	xor    %eax,%eax
  800e5c:	89 fa                	mov    %edi,%edx
  800e5e:	83 c4 1c             	add    $0x1c,%esp
  800e61:	5b                   	pop    %ebx
  800e62:	5e                   	pop    %esi
  800e63:	5f                   	pop    %edi
  800e64:	5d                   	pop    %ebp
  800e65:	c3                   	ret    
  800e66:	66 90                	xchg   %ax,%ax
  800e68:	89 d8                	mov    %ebx,%eax
  800e6a:	f7 f7                	div    %edi
  800e6c:	31 ff                	xor    %edi,%edi
  800e6e:	89 fa                	mov    %edi,%edx
  800e70:	83 c4 1c             	add    $0x1c,%esp
  800e73:	5b                   	pop    %ebx
  800e74:	5e                   	pop    %esi
  800e75:	5f                   	pop    %edi
  800e76:	5d                   	pop    %ebp
  800e77:	c3                   	ret    
  800e78:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e7d:	89 eb                	mov    %ebp,%ebx
  800e7f:	29 fb                	sub    %edi,%ebx
  800e81:	89 f9                	mov    %edi,%ecx
  800e83:	d3 e6                	shl    %cl,%esi
  800e85:	89 c5                	mov    %eax,%ebp
  800e87:	88 d9                	mov    %bl,%cl
  800e89:	d3 ed                	shr    %cl,%ebp
  800e8b:	89 e9                	mov    %ebp,%ecx
  800e8d:	09 f1                	or     %esi,%ecx
  800e8f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800e93:	89 f9                	mov    %edi,%ecx
  800e95:	d3 e0                	shl    %cl,%eax
  800e97:	89 c5                	mov    %eax,%ebp
  800e99:	89 d6                	mov    %edx,%esi
  800e9b:	88 d9                	mov    %bl,%cl
  800e9d:	d3 ee                	shr    %cl,%esi
  800e9f:	89 f9                	mov    %edi,%ecx
  800ea1:	d3 e2                	shl    %cl,%edx
  800ea3:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ea7:	88 d9                	mov    %bl,%cl
  800ea9:	d3 e8                	shr    %cl,%eax
  800eab:	09 c2                	or     %eax,%edx
  800ead:	89 d0                	mov    %edx,%eax
  800eaf:	89 f2                	mov    %esi,%edx
  800eb1:	f7 74 24 0c          	divl   0xc(%esp)
  800eb5:	89 d6                	mov    %edx,%esi
  800eb7:	89 c3                	mov    %eax,%ebx
  800eb9:	f7 e5                	mul    %ebp
  800ebb:	39 d6                	cmp    %edx,%esi
  800ebd:	72 19                	jb     800ed8 <__udivdi3+0xfc>
  800ebf:	74 0b                	je     800ecc <__udivdi3+0xf0>
  800ec1:	89 d8                	mov    %ebx,%eax
  800ec3:	31 ff                	xor    %edi,%edi
  800ec5:	e9 58 ff ff ff       	jmp    800e22 <__udivdi3+0x46>
  800eca:	66 90                	xchg   %ax,%ax
  800ecc:	8b 54 24 08          	mov    0x8(%esp),%edx
  800ed0:	89 f9                	mov    %edi,%ecx
  800ed2:	d3 e2                	shl    %cl,%edx
  800ed4:	39 c2                	cmp    %eax,%edx
  800ed6:	73 e9                	jae    800ec1 <__udivdi3+0xe5>
  800ed8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800edb:	31 ff                	xor    %edi,%edi
  800edd:	e9 40 ff ff ff       	jmp    800e22 <__udivdi3+0x46>
  800ee2:	66 90                	xchg   %ax,%ax
  800ee4:	31 c0                	xor    %eax,%eax
  800ee6:	e9 37 ff ff ff       	jmp    800e22 <__udivdi3+0x46>
  800eeb:	90                   	nop

00800eec <__umoddi3>:
  800eec:	55                   	push   %ebp
  800eed:	57                   	push   %edi
  800eee:	56                   	push   %esi
  800eef:	53                   	push   %ebx
  800ef0:	83 ec 1c             	sub    $0x1c,%esp
  800ef3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800ef7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800efb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800eff:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800f03:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800f07:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800f0b:	89 f3                	mov    %esi,%ebx
  800f0d:	89 fa                	mov    %edi,%edx
  800f0f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f13:	89 34 24             	mov    %esi,(%esp)
  800f16:	85 c0                	test   %eax,%eax
  800f18:	75 1a                	jne    800f34 <__umoddi3+0x48>
  800f1a:	39 f7                	cmp    %esi,%edi
  800f1c:	0f 86 a2 00 00 00    	jbe    800fc4 <__umoddi3+0xd8>
  800f22:	89 c8                	mov    %ecx,%eax
  800f24:	89 f2                	mov    %esi,%edx
  800f26:	f7 f7                	div    %edi
  800f28:	89 d0                	mov    %edx,%eax
  800f2a:	31 d2                	xor    %edx,%edx
  800f2c:	83 c4 1c             	add    $0x1c,%esp
  800f2f:	5b                   	pop    %ebx
  800f30:	5e                   	pop    %esi
  800f31:	5f                   	pop    %edi
  800f32:	5d                   	pop    %ebp
  800f33:	c3                   	ret    
  800f34:	39 f0                	cmp    %esi,%eax
  800f36:	0f 87 ac 00 00 00    	ja     800fe8 <__umoddi3+0xfc>
  800f3c:	0f bd e8             	bsr    %eax,%ebp
  800f3f:	83 f5 1f             	xor    $0x1f,%ebp
  800f42:	0f 84 ac 00 00 00    	je     800ff4 <__umoddi3+0x108>
  800f48:	bf 20 00 00 00       	mov    $0x20,%edi
  800f4d:	29 ef                	sub    %ebp,%edi
  800f4f:	89 fe                	mov    %edi,%esi
  800f51:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800f55:	89 e9                	mov    %ebp,%ecx
  800f57:	d3 e0                	shl    %cl,%eax
  800f59:	89 d7                	mov    %edx,%edi
  800f5b:	89 f1                	mov    %esi,%ecx
  800f5d:	d3 ef                	shr    %cl,%edi
  800f5f:	09 c7                	or     %eax,%edi
  800f61:	89 e9                	mov    %ebp,%ecx
  800f63:	d3 e2                	shl    %cl,%edx
  800f65:	89 14 24             	mov    %edx,(%esp)
  800f68:	89 d8                	mov    %ebx,%eax
  800f6a:	d3 e0                	shl    %cl,%eax
  800f6c:	89 c2                	mov    %eax,%edx
  800f6e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f72:	d3 e0                	shl    %cl,%eax
  800f74:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f78:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f7c:	89 f1                	mov    %esi,%ecx
  800f7e:	d3 e8                	shr    %cl,%eax
  800f80:	09 d0                	or     %edx,%eax
  800f82:	d3 eb                	shr    %cl,%ebx
  800f84:	89 da                	mov    %ebx,%edx
  800f86:	f7 f7                	div    %edi
  800f88:	89 d3                	mov    %edx,%ebx
  800f8a:	f7 24 24             	mull   (%esp)
  800f8d:	89 c6                	mov    %eax,%esi
  800f8f:	89 d1                	mov    %edx,%ecx
  800f91:	39 d3                	cmp    %edx,%ebx
  800f93:	0f 82 87 00 00 00    	jb     801020 <__umoddi3+0x134>
  800f99:	0f 84 91 00 00 00    	je     801030 <__umoddi3+0x144>
  800f9f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800fa3:	29 f2                	sub    %esi,%edx
  800fa5:	19 cb                	sbb    %ecx,%ebx
  800fa7:	89 d8                	mov    %ebx,%eax
  800fa9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800fad:	d3 e0                	shl    %cl,%eax
  800faf:	89 e9                	mov    %ebp,%ecx
  800fb1:	d3 ea                	shr    %cl,%edx
  800fb3:	09 d0                	or     %edx,%eax
  800fb5:	89 e9                	mov    %ebp,%ecx
  800fb7:	d3 eb                	shr    %cl,%ebx
  800fb9:	89 da                	mov    %ebx,%edx
  800fbb:	83 c4 1c             	add    $0x1c,%esp
  800fbe:	5b                   	pop    %ebx
  800fbf:	5e                   	pop    %esi
  800fc0:	5f                   	pop    %edi
  800fc1:	5d                   	pop    %ebp
  800fc2:	c3                   	ret    
  800fc3:	90                   	nop
  800fc4:	89 fd                	mov    %edi,%ebp
  800fc6:	85 ff                	test   %edi,%edi
  800fc8:	75 0b                	jne    800fd5 <__umoddi3+0xe9>
  800fca:	b8 01 00 00 00       	mov    $0x1,%eax
  800fcf:	31 d2                	xor    %edx,%edx
  800fd1:	f7 f7                	div    %edi
  800fd3:	89 c5                	mov    %eax,%ebp
  800fd5:	89 f0                	mov    %esi,%eax
  800fd7:	31 d2                	xor    %edx,%edx
  800fd9:	f7 f5                	div    %ebp
  800fdb:	89 c8                	mov    %ecx,%eax
  800fdd:	f7 f5                	div    %ebp
  800fdf:	89 d0                	mov    %edx,%eax
  800fe1:	e9 44 ff ff ff       	jmp    800f2a <__umoddi3+0x3e>
  800fe6:	66 90                	xchg   %ax,%ax
  800fe8:	89 c8                	mov    %ecx,%eax
  800fea:	89 f2                	mov    %esi,%edx
  800fec:	83 c4 1c             	add    $0x1c,%esp
  800fef:	5b                   	pop    %ebx
  800ff0:	5e                   	pop    %esi
  800ff1:	5f                   	pop    %edi
  800ff2:	5d                   	pop    %ebp
  800ff3:	c3                   	ret    
  800ff4:	3b 04 24             	cmp    (%esp),%eax
  800ff7:	72 06                	jb     800fff <__umoddi3+0x113>
  800ff9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ffd:	77 0f                	ja     80100e <__umoddi3+0x122>
  800fff:	89 f2                	mov    %esi,%edx
  801001:	29 f9                	sub    %edi,%ecx
  801003:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  801007:	89 14 24             	mov    %edx,(%esp)
  80100a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80100e:	8b 44 24 04          	mov    0x4(%esp),%eax
  801012:	8b 14 24             	mov    (%esp),%edx
  801015:	83 c4 1c             	add    $0x1c,%esp
  801018:	5b                   	pop    %ebx
  801019:	5e                   	pop    %esi
  80101a:	5f                   	pop    %edi
  80101b:	5d                   	pop    %ebp
  80101c:	c3                   	ret    
  80101d:	8d 76 00             	lea    0x0(%esi),%esi
  801020:	2b 04 24             	sub    (%esp),%eax
  801023:	19 fa                	sbb    %edi,%edx
  801025:	89 d1                	mov    %edx,%ecx
  801027:	89 c6                	mov    %eax,%esi
  801029:	e9 71 ff ff ff       	jmp    800f9f <__umoddi3+0xb3>
  80102e:	66 90                	xchg   %ax,%ax
  801030:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801034:	72 ea                	jb     801020 <__umoddi3+0x134>
  801036:	89 d9                	mov    %ebx,%ecx
  801038:	e9 62 ff ff ff       	jmp    800f9f <__umoddi3+0xb3>
