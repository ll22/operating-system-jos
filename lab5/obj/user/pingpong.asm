
obj/user/pingpong.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 8b 00 00 00       	call   8000bc <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 1c             	sub    $0x1c,%esp
	envid_t who;

	if ((who = fork()) != 0) {
  80003c:	e8 78 0d 00 00       	call   800db9 <fork>
  800041:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800044:	85 c0                	test   %eax,%eax
  800046:	74 27                	je     80006f <umain+0x3c>
  800048:	89 c3                	mov    %eax,%ebx
		// get the ball rolling
		cprintf("send 0 from %x to %x\n", sys_getenvid(), who);
  80004a:	e8 6a 0a 00 00       	call   800ab9 <sys_getenvid>
  80004f:	83 ec 04             	sub    $0x4,%esp
  800052:	53                   	push   %ebx
  800053:	50                   	push   %eax
  800054:	68 20 14 80 00       	push   $0x801420
  800059:	e8 51 01 00 00       	call   8001af <cprintf>
		ipc_send(who, 0, 0, 0);
  80005e:	6a 00                	push   $0x0
  800060:	6a 00                	push   $0x0
  800062:	6a 00                	push   $0x0
  800064:	ff 75 e4             	pushl  -0x1c(%ebp)
  800067:	e8 d9 0f 00 00       	call   801045 <ipc_send>
  80006c:	83 c4 20             	add    $0x20,%esp
	}

	while (1) {
		uint32_t i = ipc_recv(&who, 0, 0);
  80006f:	8d 75 e4             	lea    -0x1c(%ebp),%esi
  800072:	83 ec 04             	sub    $0x4,%esp
  800075:	6a 00                	push   $0x0
  800077:	6a 00                	push   $0x0
  800079:	56                   	push   %esi
  80007a:	e8 5e 0f 00 00       	call   800fdd <ipc_recv>
  80007f:	89 c3                	mov    %eax,%ebx
		cprintf("%x got %d from %x\n", sys_getenvid(), i, who);
  800081:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800084:	e8 30 0a 00 00       	call   800ab9 <sys_getenvid>
  800089:	57                   	push   %edi
  80008a:	53                   	push   %ebx
  80008b:	50                   	push   %eax
  80008c:	68 36 14 80 00       	push   $0x801436
  800091:	e8 19 01 00 00       	call   8001af <cprintf>
		if (i == 10)
  800096:	83 c4 20             	add    $0x20,%esp
  800099:	83 fb 0a             	cmp    $0xa,%ebx
  80009c:	74 16                	je     8000b4 <umain+0x81>
			return;
		i++;
  80009e:	43                   	inc    %ebx
		ipc_send(who, i, 0, 0);
  80009f:	6a 00                	push   $0x0
  8000a1:	6a 00                	push   $0x0
  8000a3:	53                   	push   %ebx
  8000a4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8000a7:	e8 99 0f 00 00       	call   801045 <ipc_send>
		if (i == 10)
  8000ac:	83 c4 10             	add    $0x10,%esp
  8000af:	83 fb 0a             	cmp    $0xa,%ebx
  8000b2:	75 be                	jne    800072 <umain+0x3f>
			return;
	}

}
  8000b4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000b7:	5b                   	pop    %ebx
  8000b8:	5e                   	pop    %esi
  8000b9:	5f                   	pop    %edi
  8000ba:	5d                   	pop    %ebp
  8000bb:	c3                   	ret    

008000bc <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000bc:	55                   	push   %ebp
  8000bd:	89 e5                	mov    %esp,%ebp
  8000bf:	56                   	push   %esi
  8000c0:	53                   	push   %ebx
  8000c1:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000c4:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000c7:	e8 ed 09 00 00       	call   800ab9 <sys_getenvid>
  8000cc:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000d1:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000d8:	c1 e0 07             	shl    $0x7,%eax
  8000db:	29 d0                	sub    %edx,%eax
  8000dd:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000e2:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000e7:	85 db                	test   %ebx,%ebx
  8000e9:	7e 07                	jle    8000f2 <libmain+0x36>
		binaryname = argv[0];
  8000eb:	8b 06                	mov    (%esi),%eax
  8000ed:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000f2:	83 ec 08             	sub    $0x8,%esp
  8000f5:	56                   	push   %esi
  8000f6:	53                   	push   %ebx
  8000f7:	e8 37 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000fc:	e8 0a 00 00 00       	call   80010b <exit>
}
  800101:	83 c4 10             	add    $0x10,%esp
  800104:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800107:	5b                   	pop    %ebx
  800108:	5e                   	pop    %esi
  800109:	5d                   	pop    %ebp
  80010a:	c3                   	ret    

0080010b <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80010b:	55                   	push   %ebp
  80010c:	89 e5                	mov    %esp,%ebp
  80010e:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800111:	6a 00                	push   $0x0
  800113:	e8 60 09 00 00       	call   800a78 <sys_env_destroy>
}
  800118:	83 c4 10             	add    $0x10,%esp
  80011b:	c9                   	leave  
  80011c:	c3                   	ret    

0080011d <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80011d:	55                   	push   %ebp
  80011e:	89 e5                	mov    %esp,%ebp
  800120:	53                   	push   %ebx
  800121:	83 ec 04             	sub    $0x4,%esp
  800124:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800127:	8b 13                	mov    (%ebx),%edx
  800129:	8d 42 01             	lea    0x1(%edx),%eax
  80012c:	89 03                	mov    %eax,(%ebx)
  80012e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800131:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800135:	3d ff 00 00 00       	cmp    $0xff,%eax
  80013a:	75 1a                	jne    800156 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80013c:	83 ec 08             	sub    $0x8,%esp
  80013f:	68 ff 00 00 00       	push   $0xff
  800144:	8d 43 08             	lea    0x8(%ebx),%eax
  800147:	50                   	push   %eax
  800148:	e8 ee 08 00 00       	call   800a3b <sys_cputs>
		b->idx = 0;
  80014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800153:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800156:	ff 43 04             	incl   0x4(%ebx)
}
  800159:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80015c:	c9                   	leave  
  80015d:	c3                   	ret    

0080015e <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80015e:	55                   	push   %ebp
  80015f:	89 e5                	mov    %esp,%ebp
  800161:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800167:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80016e:	00 00 00 
	b.cnt = 0;
  800171:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800178:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80017b:	ff 75 0c             	pushl  0xc(%ebp)
  80017e:	ff 75 08             	pushl  0x8(%ebp)
  800181:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800187:	50                   	push   %eax
  800188:	68 1d 01 80 00       	push   $0x80011d
  80018d:	e8 51 01 00 00       	call   8002e3 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800192:	83 c4 08             	add    $0x8,%esp
  800195:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80019b:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001a1:	50                   	push   %eax
  8001a2:	e8 94 08 00 00       	call   800a3b <sys_cputs>

	return b.cnt;
}
  8001a7:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001ad:	c9                   	leave  
  8001ae:	c3                   	ret    

008001af <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001af:	55                   	push   %ebp
  8001b0:	89 e5                	mov    %esp,%ebp
  8001b2:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001b5:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8001b8:	50                   	push   %eax
  8001b9:	ff 75 08             	pushl  0x8(%ebp)
  8001bc:	e8 9d ff ff ff       	call   80015e <vcprintf>
	va_end(ap);

	return cnt;
}
  8001c1:	c9                   	leave  
  8001c2:	c3                   	ret    

008001c3 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001c3:	55                   	push   %ebp
  8001c4:	89 e5                	mov    %esp,%ebp
  8001c6:	57                   	push   %edi
  8001c7:	56                   	push   %esi
  8001c8:	53                   	push   %ebx
  8001c9:	83 ec 1c             	sub    $0x1c,%esp
  8001cc:	89 c7                	mov    %eax,%edi
  8001ce:	89 d6                	mov    %edx,%esi
  8001d0:	8b 45 08             	mov    0x8(%ebp),%eax
  8001d3:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001d6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001d9:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001dc:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001df:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001e4:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001e7:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001ea:	39 d3                	cmp    %edx,%ebx
  8001ec:	72 05                	jb     8001f3 <printnum+0x30>
  8001ee:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001f1:	77 45                	ja     800238 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001f3:	83 ec 0c             	sub    $0xc,%esp
  8001f6:	ff 75 18             	pushl  0x18(%ebp)
  8001f9:	8b 45 14             	mov    0x14(%ebp),%eax
  8001fc:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001ff:	53                   	push   %ebx
  800200:	ff 75 10             	pushl  0x10(%ebp)
  800203:	83 ec 08             	sub    $0x8,%esp
  800206:	ff 75 e4             	pushl  -0x1c(%ebp)
  800209:	ff 75 e0             	pushl  -0x20(%ebp)
  80020c:	ff 75 dc             	pushl  -0x24(%ebp)
  80020f:	ff 75 d8             	pushl  -0x28(%ebp)
  800212:	e8 8d 0f 00 00       	call   8011a4 <__udivdi3>
  800217:	83 c4 18             	add    $0x18,%esp
  80021a:	52                   	push   %edx
  80021b:	50                   	push   %eax
  80021c:	89 f2                	mov    %esi,%edx
  80021e:	89 f8                	mov    %edi,%eax
  800220:	e8 9e ff ff ff       	call   8001c3 <printnum>
  800225:	83 c4 20             	add    $0x20,%esp
  800228:	eb 16                	jmp    800240 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80022a:	83 ec 08             	sub    $0x8,%esp
  80022d:	56                   	push   %esi
  80022e:	ff 75 18             	pushl  0x18(%ebp)
  800231:	ff d7                	call   *%edi
  800233:	83 c4 10             	add    $0x10,%esp
  800236:	eb 03                	jmp    80023b <printnum+0x78>
  800238:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80023b:	4b                   	dec    %ebx
  80023c:	85 db                	test   %ebx,%ebx
  80023e:	7f ea                	jg     80022a <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800240:	83 ec 08             	sub    $0x8,%esp
  800243:	56                   	push   %esi
  800244:	83 ec 04             	sub    $0x4,%esp
  800247:	ff 75 e4             	pushl  -0x1c(%ebp)
  80024a:	ff 75 e0             	pushl  -0x20(%ebp)
  80024d:	ff 75 dc             	pushl  -0x24(%ebp)
  800250:	ff 75 d8             	pushl  -0x28(%ebp)
  800253:	e8 5c 10 00 00       	call   8012b4 <__umoddi3>
  800258:	83 c4 14             	add    $0x14,%esp
  80025b:	0f be 80 53 14 80 00 	movsbl 0x801453(%eax),%eax
  800262:	50                   	push   %eax
  800263:	ff d7                	call   *%edi
}
  800265:	83 c4 10             	add    $0x10,%esp
  800268:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80026b:	5b                   	pop    %ebx
  80026c:	5e                   	pop    %esi
  80026d:	5f                   	pop    %edi
  80026e:	5d                   	pop    %ebp
  80026f:	c3                   	ret    

00800270 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800270:	55                   	push   %ebp
  800271:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800273:	83 fa 01             	cmp    $0x1,%edx
  800276:	7e 0e                	jle    800286 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800278:	8b 10                	mov    (%eax),%edx
  80027a:	8d 4a 08             	lea    0x8(%edx),%ecx
  80027d:	89 08                	mov    %ecx,(%eax)
  80027f:	8b 02                	mov    (%edx),%eax
  800281:	8b 52 04             	mov    0x4(%edx),%edx
  800284:	eb 22                	jmp    8002a8 <getuint+0x38>
	else if (lflag)
  800286:	85 d2                	test   %edx,%edx
  800288:	74 10                	je     80029a <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80028a:	8b 10                	mov    (%eax),%edx
  80028c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80028f:	89 08                	mov    %ecx,(%eax)
  800291:	8b 02                	mov    (%edx),%eax
  800293:	ba 00 00 00 00       	mov    $0x0,%edx
  800298:	eb 0e                	jmp    8002a8 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80029a:	8b 10                	mov    (%eax),%edx
  80029c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80029f:	89 08                	mov    %ecx,(%eax)
  8002a1:	8b 02                	mov    (%edx),%eax
  8002a3:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002a8:	5d                   	pop    %ebp
  8002a9:	c3                   	ret    

008002aa <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002aa:	55                   	push   %ebp
  8002ab:	89 e5                	mov    %esp,%ebp
  8002ad:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002b0:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8002b3:	8b 10                	mov    (%eax),%edx
  8002b5:	3b 50 04             	cmp    0x4(%eax),%edx
  8002b8:	73 0a                	jae    8002c4 <sprintputch+0x1a>
		*b->buf++ = ch;
  8002ba:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002bd:	89 08                	mov    %ecx,(%eax)
  8002bf:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c2:	88 02                	mov    %al,(%edx)
}
  8002c4:	5d                   	pop    %ebp
  8002c5:	c3                   	ret    

008002c6 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002c6:	55                   	push   %ebp
  8002c7:	89 e5                	mov    %esp,%ebp
  8002c9:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002cc:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002cf:	50                   	push   %eax
  8002d0:	ff 75 10             	pushl  0x10(%ebp)
  8002d3:	ff 75 0c             	pushl  0xc(%ebp)
  8002d6:	ff 75 08             	pushl  0x8(%ebp)
  8002d9:	e8 05 00 00 00       	call   8002e3 <vprintfmt>
	va_end(ap);
}
  8002de:	83 c4 10             	add    $0x10,%esp
  8002e1:	c9                   	leave  
  8002e2:	c3                   	ret    

008002e3 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002e3:	55                   	push   %ebp
  8002e4:	89 e5                	mov    %esp,%ebp
  8002e6:	57                   	push   %edi
  8002e7:	56                   	push   %esi
  8002e8:	53                   	push   %ebx
  8002e9:	83 ec 2c             	sub    $0x2c,%esp
  8002ec:	8b 75 08             	mov    0x8(%ebp),%esi
  8002ef:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002f2:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002f5:	eb 12                	jmp    800309 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002f7:	85 c0                	test   %eax,%eax
  8002f9:	0f 84 68 03 00 00    	je     800667 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002ff:	83 ec 08             	sub    $0x8,%esp
  800302:	53                   	push   %ebx
  800303:	50                   	push   %eax
  800304:	ff d6                	call   *%esi
  800306:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800309:	47                   	inc    %edi
  80030a:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80030e:	83 f8 25             	cmp    $0x25,%eax
  800311:	75 e4                	jne    8002f7 <vprintfmt+0x14>
  800313:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800317:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80031e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800325:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80032c:	ba 00 00 00 00       	mov    $0x0,%edx
  800331:	eb 07                	jmp    80033a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800333:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800336:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80033a:	8d 47 01             	lea    0x1(%edi),%eax
  80033d:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800340:	0f b6 0f             	movzbl (%edi),%ecx
  800343:	8a 07                	mov    (%edi),%al
  800345:	83 e8 23             	sub    $0x23,%eax
  800348:	3c 55                	cmp    $0x55,%al
  80034a:	0f 87 fe 02 00 00    	ja     80064e <vprintfmt+0x36b>
  800350:	0f b6 c0             	movzbl %al,%eax
  800353:	ff 24 85 a0 15 80 00 	jmp    *0x8015a0(,%eax,4)
  80035a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80035d:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800361:	eb d7                	jmp    80033a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800363:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800366:	b8 00 00 00 00       	mov    $0x0,%eax
  80036b:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80036e:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800371:	01 c0                	add    %eax,%eax
  800373:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800377:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80037a:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80037d:	83 fa 09             	cmp    $0x9,%edx
  800380:	77 34                	ja     8003b6 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800382:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800383:	eb e9                	jmp    80036e <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800385:	8b 45 14             	mov    0x14(%ebp),%eax
  800388:	8d 48 04             	lea    0x4(%eax),%ecx
  80038b:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80038e:	8b 00                	mov    (%eax),%eax
  800390:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800393:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800396:	eb 24                	jmp    8003bc <vprintfmt+0xd9>
  800398:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80039c:	79 07                	jns    8003a5 <vprintfmt+0xc2>
  80039e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003a8:	eb 90                	jmp    80033a <vprintfmt+0x57>
  8003aa:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003ad:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8003b4:	eb 84                	jmp    80033a <vprintfmt+0x57>
  8003b6:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003b9:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003bc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003c0:	0f 89 74 ff ff ff    	jns    80033a <vprintfmt+0x57>
				width = precision, precision = -1;
  8003c6:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003c9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003cc:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003d3:	e9 62 ff ff ff       	jmp    80033a <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003d8:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003dc:	e9 59 ff ff ff       	jmp    80033a <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003e1:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e4:	8d 50 04             	lea    0x4(%eax),%edx
  8003e7:	89 55 14             	mov    %edx,0x14(%ebp)
  8003ea:	83 ec 08             	sub    $0x8,%esp
  8003ed:	53                   	push   %ebx
  8003ee:	ff 30                	pushl  (%eax)
  8003f0:	ff d6                	call   *%esi
			break;
  8003f2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003f8:	e9 0c ff ff ff       	jmp    800309 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003fd:	8b 45 14             	mov    0x14(%ebp),%eax
  800400:	8d 50 04             	lea    0x4(%eax),%edx
  800403:	89 55 14             	mov    %edx,0x14(%ebp)
  800406:	8b 00                	mov    (%eax),%eax
  800408:	85 c0                	test   %eax,%eax
  80040a:	79 02                	jns    80040e <vprintfmt+0x12b>
  80040c:	f7 d8                	neg    %eax
  80040e:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800410:	83 f8 0f             	cmp    $0xf,%eax
  800413:	7f 0b                	jg     800420 <vprintfmt+0x13d>
  800415:	8b 04 85 00 17 80 00 	mov    0x801700(,%eax,4),%eax
  80041c:	85 c0                	test   %eax,%eax
  80041e:	75 18                	jne    800438 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800420:	52                   	push   %edx
  800421:	68 6b 14 80 00       	push   $0x80146b
  800426:	53                   	push   %ebx
  800427:	56                   	push   %esi
  800428:	e8 99 fe ff ff       	call   8002c6 <printfmt>
  80042d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800430:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800433:	e9 d1 fe ff ff       	jmp    800309 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800438:	50                   	push   %eax
  800439:	68 74 14 80 00       	push   $0x801474
  80043e:	53                   	push   %ebx
  80043f:	56                   	push   %esi
  800440:	e8 81 fe ff ff       	call   8002c6 <printfmt>
  800445:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800448:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80044b:	e9 b9 fe ff ff       	jmp    800309 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800450:	8b 45 14             	mov    0x14(%ebp),%eax
  800453:	8d 50 04             	lea    0x4(%eax),%edx
  800456:	89 55 14             	mov    %edx,0x14(%ebp)
  800459:	8b 38                	mov    (%eax),%edi
  80045b:	85 ff                	test   %edi,%edi
  80045d:	75 05                	jne    800464 <vprintfmt+0x181>
				p = "(null)";
  80045f:	bf 64 14 80 00       	mov    $0x801464,%edi
			if (width > 0 && padc != '-')
  800464:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800468:	0f 8e 90 00 00 00    	jle    8004fe <vprintfmt+0x21b>
  80046e:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800472:	0f 84 8e 00 00 00    	je     800506 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800478:	83 ec 08             	sub    $0x8,%esp
  80047b:	ff 75 d0             	pushl  -0x30(%ebp)
  80047e:	57                   	push   %edi
  80047f:	e8 70 02 00 00       	call   8006f4 <strnlen>
  800484:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800487:	29 c1                	sub    %eax,%ecx
  800489:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80048c:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80048f:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800493:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800496:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800499:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80049b:	eb 0d                	jmp    8004aa <vprintfmt+0x1c7>
					putch(padc, putdat);
  80049d:	83 ec 08             	sub    $0x8,%esp
  8004a0:	53                   	push   %ebx
  8004a1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a4:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004a6:	4f                   	dec    %edi
  8004a7:	83 c4 10             	add    $0x10,%esp
  8004aa:	85 ff                	test   %edi,%edi
  8004ac:	7f ef                	jg     80049d <vprintfmt+0x1ba>
  8004ae:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8004b1:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004b4:	89 c8                	mov    %ecx,%eax
  8004b6:	85 c9                	test   %ecx,%ecx
  8004b8:	79 05                	jns    8004bf <vprintfmt+0x1dc>
  8004ba:	b8 00 00 00 00       	mov    $0x0,%eax
  8004bf:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004c2:	29 c1                	sub    %eax,%ecx
  8004c4:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004c7:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ca:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004cd:	eb 3d                	jmp    80050c <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004cf:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004d3:	74 19                	je     8004ee <vprintfmt+0x20b>
  8004d5:	0f be c0             	movsbl %al,%eax
  8004d8:	83 e8 20             	sub    $0x20,%eax
  8004db:	83 f8 5e             	cmp    $0x5e,%eax
  8004de:	76 0e                	jbe    8004ee <vprintfmt+0x20b>
					putch('?', putdat);
  8004e0:	83 ec 08             	sub    $0x8,%esp
  8004e3:	53                   	push   %ebx
  8004e4:	6a 3f                	push   $0x3f
  8004e6:	ff 55 08             	call   *0x8(%ebp)
  8004e9:	83 c4 10             	add    $0x10,%esp
  8004ec:	eb 0b                	jmp    8004f9 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004ee:	83 ec 08             	sub    $0x8,%esp
  8004f1:	53                   	push   %ebx
  8004f2:	52                   	push   %edx
  8004f3:	ff 55 08             	call   *0x8(%ebp)
  8004f6:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004f9:	ff 4d e4             	decl   -0x1c(%ebp)
  8004fc:	eb 0e                	jmp    80050c <vprintfmt+0x229>
  8004fe:	89 75 08             	mov    %esi,0x8(%ebp)
  800501:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800504:	eb 06                	jmp    80050c <vprintfmt+0x229>
  800506:	89 75 08             	mov    %esi,0x8(%ebp)
  800509:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80050c:	47                   	inc    %edi
  80050d:	8a 47 ff             	mov    -0x1(%edi),%al
  800510:	0f be d0             	movsbl %al,%edx
  800513:	85 d2                	test   %edx,%edx
  800515:	74 1d                	je     800534 <vprintfmt+0x251>
  800517:	85 f6                	test   %esi,%esi
  800519:	78 b4                	js     8004cf <vprintfmt+0x1ec>
  80051b:	4e                   	dec    %esi
  80051c:	79 b1                	jns    8004cf <vprintfmt+0x1ec>
  80051e:	8b 75 08             	mov    0x8(%ebp),%esi
  800521:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800524:	eb 14                	jmp    80053a <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800526:	83 ec 08             	sub    $0x8,%esp
  800529:	53                   	push   %ebx
  80052a:	6a 20                	push   $0x20
  80052c:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80052e:	4f                   	dec    %edi
  80052f:	83 c4 10             	add    $0x10,%esp
  800532:	eb 06                	jmp    80053a <vprintfmt+0x257>
  800534:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800537:	8b 75 08             	mov    0x8(%ebp),%esi
  80053a:	85 ff                	test   %edi,%edi
  80053c:	7f e8                	jg     800526 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80053e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800541:	e9 c3 fd ff ff       	jmp    800309 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800546:	83 fa 01             	cmp    $0x1,%edx
  800549:	7e 16                	jle    800561 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80054b:	8b 45 14             	mov    0x14(%ebp),%eax
  80054e:	8d 50 08             	lea    0x8(%eax),%edx
  800551:	89 55 14             	mov    %edx,0x14(%ebp)
  800554:	8b 50 04             	mov    0x4(%eax),%edx
  800557:	8b 00                	mov    (%eax),%eax
  800559:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80055c:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80055f:	eb 32                	jmp    800593 <vprintfmt+0x2b0>
	else if (lflag)
  800561:	85 d2                	test   %edx,%edx
  800563:	74 18                	je     80057d <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800565:	8b 45 14             	mov    0x14(%ebp),%eax
  800568:	8d 50 04             	lea    0x4(%eax),%edx
  80056b:	89 55 14             	mov    %edx,0x14(%ebp)
  80056e:	8b 00                	mov    (%eax),%eax
  800570:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800573:	89 c1                	mov    %eax,%ecx
  800575:	c1 f9 1f             	sar    $0x1f,%ecx
  800578:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80057b:	eb 16                	jmp    800593 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80057d:	8b 45 14             	mov    0x14(%ebp),%eax
  800580:	8d 50 04             	lea    0x4(%eax),%edx
  800583:	89 55 14             	mov    %edx,0x14(%ebp)
  800586:	8b 00                	mov    (%eax),%eax
  800588:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80058b:	89 c1                	mov    %eax,%ecx
  80058d:	c1 f9 1f             	sar    $0x1f,%ecx
  800590:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800593:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800596:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800599:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80059d:	79 76                	jns    800615 <vprintfmt+0x332>
				putch('-', putdat);
  80059f:	83 ec 08             	sub    $0x8,%esp
  8005a2:	53                   	push   %ebx
  8005a3:	6a 2d                	push   $0x2d
  8005a5:	ff d6                	call   *%esi
				num = -(long long) num;
  8005a7:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005aa:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005ad:	f7 d8                	neg    %eax
  8005af:	83 d2 00             	adc    $0x0,%edx
  8005b2:	f7 da                	neg    %edx
  8005b4:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8005b7:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005bc:	eb 5c                	jmp    80061a <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005be:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c1:	e8 aa fc ff ff       	call   800270 <getuint>
			base = 10;
  8005c6:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005cb:	eb 4d                	jmp    80061a <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005cd:	8d 45 14             	lea    0x14(%ebp),%eax
  8005d0:	e8 9b fc ff ff       	call   800270 <getuint>
			base = 8;
  8005d5:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005da:	eb 3e                	jmp    80061a <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005dc:	83 ec 08             	sub    $0x8,%esp
  8005df:	53                   	push   %ebx
  8005e0:	6a 30                	push   $0x30
  8005e2:	ff d6                	call   *%esi
			putch('x', putdat);
  8005e4:	83 c4 08             	add    $0x8,%esp
  8005e7:	53                   	push   %ebx
  8005e8:	6a 78                	push   $0x78
  8005ea:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005ec:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ef:	8d 50 04             	lea    0x4(%eax),%edx
  8005f2:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005f5:	8b 00                	mov    (%eax),%eax
  8005f7:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005fc:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005ff:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800604:	eb 14                	jmp    80061a <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800606:	8d 45 14             	lea    0x14(%ebp),%eax
  800609:	e8 62 fc ff ff       	call   800270 <getuint>
			base = 16;
  80060e:	b9 10 00 00 00       	mov    $0x10,%ecx
  800613:	eb 05                	jmp    80061a <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800615:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80061a:	83 ec 0c             	sub    $0xc,%esp
  80061d:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800621:	57                   	push   %edi
  800622:	ff 75 e4             	pushl  -0x1c(%ebp)
  800625:	51                   	push   %ecx
  800626:	52                   	push   %edx
  800627:	50                   	push   %eax
  800628:	89 da                	mov    %ebx,%edx
  80062a:	89 f0                	mov    %esi,%eax
  80062c:	e8 92 fb ff ff       	call   8001c3 <printnum>
			break;
  800631:	83 c4 20             	add    $0x20,%esp
  800634:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800637:	e9 cd fc ff ff       	jmp    800309 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80063c:	83 ec 08             	sub    $0x8,%esp
  80063f:	53                   	push   %ebx
  800640:	51                   	push   %ecx
  800641:	ff d6                	call   *%esi
			break;
  800643:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800646:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800649:	e9 bb fc ff ff       	jmp    800309 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80064e:	83 ec 08             	sub    $0x8,%esp
  800651:	53                   	push   %ebx
  800652:	6a 25                	push   $0x25
  800654:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800656:	83 c4 10             	add    $0x10,%esp
  800659:	eb 01                	jmp    80065c <vprintfmt+0x379>
  80065b:	4f                   	dec    %edi
  80065c:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800660:	75 f9                	jne    80065b <vprintfmt+0x378>
  800662:	e9 a2 fc ff ff       	jmp    800309 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800667:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80066a:	5b                   	pop    %ebx
  80066b:	5e                   	pop    %esi
  80066c:	5f                   	pop    %edi
  80066d:	5d                   	pop    %ebp
  80066e:	c3                   	ret    

0080066f <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80066f:	55                   	push   %ebp
  800670:	89 e5                	mov    %esp,%ebp
  800672:	83 ec 18             	sub    $0x18,%esp
  800675:	8b 45 08             	mov    0x8(%ebp),%eax
  800678:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80067b:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80067e:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800682:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800685:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80068c:	85 c0                	test   %eax,%eax
  80068e:	74 26                	je     8006b6 <vsnprintf+0x47>
  800690:	85 d2                	test   %edx,%edx
  800692:	7e 29                	jle    8006bd <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800694:	ff 75 14             	pushl  0x14(%ebp)
  800697:	ff 75 10             	pushl  0x10(%ebp)
  80069a:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80069d:	50                   	push   %eax
  80069e:	68 aa 02 80 00       	push   $0x8002aa
  8006a3:	e8 3b fc ff ff       	call   8002e3 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006ab:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8006b1:	83 c4 10             	add    $0x10,%esp
  8006b4:	eb 0c                	jmp    8006c2 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8006b6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006bb:	eb 05                	jmp    8006c2 <vsnprintf+0x53>
  8006bd:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8006c2:	c9                   	leave  
  8006c3:	c3                   	ret    

008006c4 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006c4:	55                   	push   %ebp
  8006c5:	89 e5                	mov    %esp,%ebp
  8006c7:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006ca:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006cd:	50                   	push   %eax
  8006ce:	ff 75 10             	pushl  0x10(%ebp)
  8006d1:	ff 75 0c             	pushl  0xc(%ebp)
  8006d4:	ff 75 08             	pushl  0x8(%ebp)
  8006d7:	e8 93 ff ff ff       	call   80066f <vsnprintf>
	va_end(ap);

	return rc;
}
  8006dc:	c9                   	leave  
  8006dd:	c3                   	ret    

008006de <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006de:	55                   	push   %ebp
  8006df:	89 e5                	mov    %esp,%ebp
  8006e1:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006e4:	b8 00 00 00 00       	mov    $0x0,%eax
  8006e9:	eb 01                	jmp    8006ec <strlen+0xe>
		n++;
  8006eb:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006ec:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006f0:	75 f9                	jne    8006eb <strlen+0xd>
		n++;
	return n;
}
  8006f2:	5d                   	pop    %ebp
  8006f3:	c3                   	ret    

008006f4 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006f4:	55                   	push   %ebp
  8006f5:	89 e5                	mov    %esp,%ebp
  8006f7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006fa:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006fd:	ba 00 00 00 00       	mov    $0x0,%edx
  800702:	eb 01                	jmp    800705 <strnlen+0x11>
		n++;
  800704:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800705:	39 c2                	cmp    %eax,%edx
  800707:	74 08                	je     800711 <strnlen+0x1d>
  800709:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80070d:	75 f5                	jne    800704 <strnlen+0x10>
  80070f:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800711:	5d                   	pop    %ebp
  800712:	c3                   	ret    

00800713 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800713:	55                   	push   %ebp
  800714:	89 e5                	mov    %esp,%ebp
  800716:	53                   	push   %ebx
  800717:	8b 45 08             	mov    0x8(%ebp),%eax
  80071a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80071d:	89 c2                	mov    %eax,%edx
  80071f:	42                   	inc    %edx
  800720:	41                   	inc    %ecx
  800721:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800724:	88 5a ff             	mov    %bl,-0x1(%edx)
  800727:	84 db                	test   %bl,%bl
  800729:	75 f4                	jne    80071f <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80072b:	5b                   	pop    %ebx
  80072c:	5d                   	pop    %ebp
  80072d:	c3                   	ret    

0080072e <strcat>:

char *
strcat(char *dst, const char *src)
{
  80072e:	55                   	push   %ebp
  80072f:	89 e5                	mov    %esp,%ebp
  800731:	53                   	push   %ebx
  800732:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800735:	53                   	push   %ebx
  800736:	e8 a3 ff ff ff       	call   8006de <strlen>
  80073b:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80073e:	ff 75 0c             	pushl  0xc(%ebp)
  800741:	01 d8                	add    %ebx,%eax
  800743:	50                   	push   %eax
  800744:	e8 ca ff ff ff       	call   800713 <strcpy>
	return dst;
}
  800749:	89 d8                	mov    %ebx,%eax
  80074b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80074e:	c9                   	leave  
  80074f:	c3                   	ret    

00800750 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800750:	55                   	push   %ebp
  800751:	89 e5                	mov    %esp,%ebp
  800753:	56                   	push   %esi
  800754:	53                   	push   %ebx
  800755:	8b 75 08             	mov    0x8(%ebp),%esi
  800758:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80075b:	89 f3                	mov    %esi,%ebx
  80075d:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800760:	89 f2                	mov    %esi,%edx
  800762:	eb 0c                	jmp    800770 <strncpy+0x20>
		*dst++ = *src;
  800764:	42                   	inc    %edx
  800765:	8a 01                	mov    (%ecx),%al
  800767:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80076a:	80 39 01             	cmpb   $0x1,(%ecx)
  80076d:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800770:	39 da                	cmp    %ebx,%edx
  800772:	75 f0                	jne    800764 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800774:	89 f0                	mov    %esi,%eax
  800776:	5b                   	pop    %ebx
  800777:	5e                   	pop    %esi
  800778:	5d                   	pop    %ebp
  800779:	c3                   	ret    

0080077a <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80077a:	55                   	push   %ebp
  80077b:	89 e5                	mov    %esp,%ebp
  80077d:	56                   	push   %esi
  80077e:	53                   	push   %ebx
  80077f:	8b 75 08             	mov    0x8(%ebp),%esi
  800782:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800785:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800788:	85 c0                	test   %eax,%eax
  80078a:	74 1e                	je     8007aa <strlcpy+0x30>
  80078c:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800790:	89 f2                	mov    %esi,%edx
  800792:	eb 05                	jmp    800799 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800794:	42                   	inc    %edx
  800795:	41                   	inc    %ecx
  800796:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800799:	39 c2                	cmp    %eax,%edx
  80079b:	74 08                	je     8007a5 <strlcpy+0x2b>
  80079d:	8a 19                	mov    (%ecx),%bl
  80079f:	84 db                	test   %bl,%bl
  8007a1:	75 f1                	jne    800794 <strlcpy+0x1a>
  8007a3:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007a5:	c6 00 00             	movb   $0x0,(%eax)
  8007a8:	eb 02                	jmp    8007ac <strlcpy+0x32>
  8007aa:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007ac:	29 f0                	sub    %esi,%eax
}
  8007ae:	5b                   	pop    %ebx
  8007af:	5e                   	pop    %esi
  8007b0:	5d                   	pop    %ebp
  8007b1:	c3                   	ret    

008007b2 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8007b2:	55                   	push   %ebp
  8007b3:	89 e5                	mov    %esp,%ebp
  8007b5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007b8:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007bb:	eb 02                	jmp    8007bf <strcmp+0xd>
		p++, q++;
  8007bd:	41                   	inc    %ecx
  8007be:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007bf:	8a 01                	mov    (%ecx),%al
  8007c1:	84 c0                	test   %al,%al
  8007c3:	74 04                	je     8007c9 <strcmp+0x17>
  8007c5:	3a 02                	cmp    (%edx),%al
  8007c7:	74 f4                	je     8007bd <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007c9:	0f b6 c0             	movzbl %al,%eax
  8007cc:	0f b6 12             	movzbl (%edx),%edx
  8007cf:	29 d0                	sub    %edx,%eax
}
  8007d1:	5d                   	pop    %ebp
  8007d2:	c3                   	ret    

008007d3 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007d3:	55                   	push   %ebp
  8007d4:	89 e5                	mov    %esp,%ebp
  8007d6:	53                   	push   %ebx
  8007d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8007da:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007dd:	89 c3                	mov    %eax,%ebx
  8007df:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007e2:	eb 02                	jmp    8007e6 <strncmp+0x13>
		n--, p++, q++;
  8007e4:	40                   	inc    %eax
  8007e5:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007e6:	39 d8                	cmp    %ebx,%eax
  8007e8:	74 14                	je     8007fe <strncmp+0x2b>
  8007ea:	8a 08                	mov    (%eax),%cl
  8007ec:	84 c9                	test   %cl,%cl
  8007ee:	74 04                	je     8007f4 <strncmp+0x21>
  8007f0:	3a 0a                	cmp    (%edx),%cl
  8007f2:	74 f0                	je     8007e4 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007f4:	0f b6 00             	movzbl (%eax),%eax
  8007f7:	0f b6 12             	movzbl (%edx),%edx
  8007fa:	29 d0                	sub    %edx,%eax
  8007fc:	eb 05                	jmp    800803 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007fe:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800803:	5b                   	pop    %ebx
  800804:	5d                   	pop    %ebp
  800805:	c3                   	ret    

00800806 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800806:	55                   	push   %ebp
  800807:	89 e5                	mov    %esp,%ebp
  800809:	8b 45 08             	mov    0x8(%ebp),%eax
  80080c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80080f:	eb 05                	jmp    800816 <strchr+0x10>
		if (*s == c)
  800811:	38 ca                	cmp    %cl,%dl
  800813:	74 0c                	je     800821 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800815:	40                   	inc    %eax
  800816:	8a 10                	mov    (%eax),%dl
  800818:	84 d2                	test   %dl,%dl
  80081a:	75 f5                	jne    800811 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80081c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800821:	5d                   	pop    %ebp
  800822:	c3                   	ret    

00800823 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800823:	55                   	push   %ebp
  800824:	89 e5                	mov    %esp,%ebp
  800826:	8b 45 08             	mov    0x8(%ebp),%eax
  800829:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80082c:	eb 05                	jmp    800833 <strfind+0x10>
		if (*s == c)
  80082e:	38 ca                	cmp    %cl,%dl
  800830:	74 07                	je     800839 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800832:	40                   	inc    %eax
  800833:	8a 10                	mov    (%eax),%dl
  800835:	84 d2                	test   %dl,%dl
  800837:	75 f5                	jne    80082e <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800839:	5d                   	pop    %ebp
  80083a:	c3                   	ret    

0080083b <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80083b:	55                   	push   %ebp
  80083c:	89 e5                	mov    %esp,%ebp
  80083e:	57                   	push   %edi
  80083f:	56                   	push   %esi
  800840:	53                   	push   %ebx
  800841:	8b 7d 08             	mov    0x8(%ebp),%edi
  800844:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800847:	85 c9                	test   %ecx,%ecx
  800849:	74 36                	je     800881 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80084b:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800851:	75 28                	jne    80087b <memset+0x40>
  800853:	f6 c1 03             	test   $0x3,%cl
  800856:	75 23                	jne    80087b <memset+0x40>
		c &= 0xFF;
  800858:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80085c:	89 d3                	mov    %edx,%ebx
  80085e:	c1 e3 08             	shl    $0x8,%ebx
  800861:	89 d6                	mov    %edx,%esi
  800863:	c1 e6 18             	shl    $0x18,%esi
  800866:	89 d0                	mov    %edx,%eax
  800868:	c1 e0 10             	shl    $0x10,%eax
  80086b:	09 f0                	or     %esi,%eax
  80086d:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80086f:	89 d8                	mov    %ebx,%eax
  800871:	09 d0                	or     %edx,%eax
  800873:	c1 e9 02             	shr    $0x2,%ecx
  800876:	fc                   	cld    
  800877:	f3 ab                	rep stos %eax,%es:(%edi)
  800879:	eb 06                	jmp    800881 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80087b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80087e:	fc                   	cld    
  80087f:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800881:	89 f8                	mov    %edi,%eax
  800883:	5b                   	pop    %ebx
  800884:	5e                   	pop    %esi
  800885:	5f                   	pop    %edi
  800886:	5d                   	pop    %ebp
  800887:	c3                   	ret    

00800888 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800888:	55                   	push   %ebp
  800889:	89 e5                	mov    %esp,%ebp
  80088b:	57                   	push   %edi
  80088c:	56                   	push   %esi
  80088d:	8b 45 08             	mov    0x8(%ebp),%eax
  800890:	8b 75 0c             	mov    0xc(%ebp),%esi
  800893:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800896:	39 c6                	cmp    %eax,%esi
  800898:	73 33                	jae    8008cd <memmove+0x45>
  80089a:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80089d:	39 d0                	cmp    %edx,%eax
  80089f:	73 2c                	jae    8008cd <memmove+0x45>
		s += n;
		d += n;
  8008a1:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008a4:	89 d6                	mov    %edx,%esi
  8008a6:	09 fe                	or     %edi,%esi
  8008a8:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008ae:	75 13                	jne    8008c3 <memmove+0x3b>
  8008b0:	f6 c1 03             	test   $0x3,%cl
  8008b3:	75 0e                	jne    8008c3 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8008b5:	83 ef 04             	sub    $0x4,%edi
  8008b8:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008bb:	c1 e9 02             	shr    $0x2,%ecx
  8008be:	fd                   	std    
  8008bf:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008c1:	eb 07                	jmp    8008ca <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008c3:	4f                   	dec    %edi
  8008c4:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008c7:	fd                   	std    
  8008c8:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008ca:	fc                   	cld    
  8008cb:	eb 1d                	jmp    8008ea <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008cd:	89 f2                	mov    %esi,%edx
  8008cf:	09 c2                	or     %eax,%edx
  8008d1:	f6 c2 03             	test   $0x3,%dl
  8008d4:	75 0f                	jne    8008e5 <memmove+0x5d>
  8008d6:	f6 c1 03             	test   $0x3,%cl
  8008d9:	75 0a                	jne    8008e5 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008db:	c1 e9 02             	shr    $0x2,%ecx
  8008de:	89 c7                	mov    %eax,%edi
  8008e0:	fc                   	cld    
  8008e1:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008e3:	eb 05                	jmp    8008ea <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008e5:	89 c7                	mov    %eax,%edi
  8008e7:	fc                   	cld    
  8008e8:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008ea:	5e                   	pop    %esi
  8008eb:	5f                   	pop    %edi
  8008ec:	5d                   	pop    %ebp
  8008ed:	c3                   	ret    

008008ee <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008ee:	55                   	push   %ebp
  8008ef:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008f1:	ff 75 10             	pushl  0x10(%ebp)
  8008f4:	ff 75 0c             	pushl  0xc(%ebp)
  8008f7:	ff 75 08             	pushl  0x8(%ebp)
  8008fa:	e8 89 ff ff ff       	call   800888 <memmove>
}
  8008ff:	c9                   	leave  
  800900:	c3                   	ret    

00800901 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800901:	55                   	push   %ebp
  800902:	89 e5                	mov    %esp,%ebp
  800904:	56                   	push   %esi
  800905:	53                   	push   %ebx
  800906:	8b 45 08             	mov    0x8(%ebp),%eax
  800909:	8b 55 0c             	mov    0xc(%ebp),%edx
  80090c:	89 c6                	mov    %eax,%esi
  80090e:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800911:	eb 14                	jmp    800927 <memcmp+0x26>
		if (*s1 != *s2)
  800913:	8a 08                	mov    (%eax),%cl
  800915:	8a 1a                	mov    (%edx),%bl
  800917:	38 d9                	cmp    %bl,%cl
  800919:	74 0a                	je     800925 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80091b:	0f b6 c1             	movzbl %cl,%eax
  80091e:	0f b6 db             	movzbl %bl,%ebx
  800921:	29 d8                	sub    %ebx,%eax
  800923:	eb 0b                	jmp    800930 <memcmp+0x2f>
		s1++, s2++;
  800925:	40                   	inc    %eax
  800926:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800927:	39 f0                	cmp    %esi,%eax
  800929:	75 e8                	jne    800913 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80092b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800930:	5b                   	pop    %ebx
  800931:	5e                   	pop    %esi
  800932:	5d                   	pop    %ebp
  800933:	c3                   	ret    

00800934 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800934:	55                   	push   %ebp
  800935:	89 e5                	mov    %esp,%ebp
  800937:	53                   	push   %ebx
  800938:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80093b:	89 c1                	mov    %eax,%ecx
  80093d:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800940:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800944:	eb 08                	jmp    80094e <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800946:	0f b6 10             	movzbl (%eax),%edx
  800949:	39 da                	cmp    %ebx,%edx
  80094b:	74 05                	je     800952 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80094d:	40                   	inc    %eax
  80094e:	39 c8                	cmp    %ecx,%eax
  800950:	72 f4                	jb     800946 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800952:	5b                   	pop    %ebx
  800953:	5d                   	pop    %ebp
  800954:	c3                   	ret    

00800955 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800955:	55                   	push   %ebp
  800956:	89 e5                	mov    %esp,%ebp
  800958:	57                   	push   %edi
  800959:	56                   	push   %esi
  80095a:	53                   	push   %ebx
  80095b:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80095e:	eb 01                	jmp    800961 <strtol+0xc>
		s++;
  800960:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800961:	8a 01                	mov    (%ecx),%al
  800963:	3c 20                	cmp    $0x20,%al
  800965:	74 f9                	je     800960 <strtol+0xb>
  800967:	3c 09                	cmp    $0x9,%al
  800969:	74 f5                	je     800960 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80096b:	3c 2b                	cmp    $0x2b,%al
  80096d:	75 08                	jne    800977 <strtol+0x22>
		s++;
  80096f:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800970:	bf 00 00 00 00       	mov    $0x0,%edi
  800975:	eb 11                	jmp    800988 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800977:	3c 2d                	cmp    $0x2d,%al
  800979:	75 08                	jne    800983 <strtol+0x2e>
		s++, neg = 1;
  80097b:	41                   	inc    %ecx
  80097c:	bf 01 00 00 00       	mov    $0x1,%edi
  800981:	eb 05                	jmp    800988 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800983:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800988:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80098c:	0f 84 87 00 00 00    	je     800a19 <strtol+0xc4>
  800992:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800996:	75 27                	jne    8009bf <strtol+0x6a>
  800998:	80 39 30             	cmpb   $0x30,(%ecx)
  80099b:	75 22                	jne    8009bf <strtol+0x6a>
  80099d:	e9 88 00 00 00       	jmp    800a2a <strtol+0xd5>
		s += 2, base = 16;
  8009a2:	83 c1 02             	add    $0x2,%ecx
  8009a5:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009ac:	eb 11                	jmp    8009bf <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009ae:	41                   	inc    %ecx
  8009af:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8009b6:	eb 07                	jmp    8009bf <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8009b8:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009bf:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009c4:	8a 11                	mov    (%ecx),%dl
  8009c6:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009c9:	80 fb 09             	cmp    $0x9,%bl
  8009cc:	77 08                	ja     8009d6 <strtol+0x81>
			dig = *s - '0';
  8009ce:	0f be d2             	movsbl %dl,%edx
  8009d1:	83 ea 30             	sub    $0x30,%edx
  8009d4:	eb 22                	jmp    8009f8 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009d6:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009d9:	89 f3                	mov    %esi,%ebx
  8009db:	80 fb 19             	cmp    $0x19,%bl
  8009de:	77 08                	ja     8009e8 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009e0:	0f be d2             	movsbl %dl,%edx
  8009e3:	83 ea 57             	sub    $0x57,%edx
  8009e6:	eb 10                	jmp    8009f8 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009e8:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009eb:	89 f3                	mov    %esi,%ebx
  8009ed:	80 fb 19             	cmp    $0x19,%bl
  8009f0:	77 14                	ja     800a06 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009f2:	0f be d2             	movsbl %dl,%edx
  8009f5:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009f8:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009fb:	7d 09                	jge    800a06 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009fd:	41                   	inc    %ecx
  8009fe:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a02:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a04:	eb be                	jmp    8009c4 <strtol+0x6f>

	if (endptr)
  800a06:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a0a:	74 05                	je     800a11 <strtol+0xbc>
		*endptr = (char *) s;
  800a0c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a0f:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a11:	85 ff                	test   %edi,%edi
  800a13:	74 21                	je     800a36 <strtol+0xe1>
  800a15:	f7 d8                	neg    %eax
  800a17:	eb 1d                	jmp    800a36 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a19:	80 39 30             	cmpb   $0x30,(%ecx)
  800a1c:	75 9a                	jne    8009b8 <strtol+0x63>
  800a1e:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a22:	0f 84 7a ff ff ff    	je     8009a2 <strtol+0x4d>
  800a28:	eb 84                	jmp    8009ae <strtol+0x59>
  800a2a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a2e:	0f 84 6e ff ff ff    	je     8009a2 <strtol+0x4d>
  800a34:	eb 89                	jmp    8009bf <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a36:	5b                   	pop    %ebx
  800a37:	5e                   	pop    %esi
  800a38:	5f                   	pop    %edi
  800a39:	5d                   	pop    %ebp
  800a3a:	c3                   	ret    

00800a3b <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a3b:	55                   	push   %ebp
  800a3c:	89 e5                	mov    %esp,%ebp
  800a3e:	57                   	push   %edi
  800a3f:	56                   	push   %esi
  800a40:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a41:	b8 00 00 00 00       	mov    $0x0,%eax
  800a46:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a49:	8b 55 08             	mov    0x8(%ebp),%edx
  800a4c:	89 c3                	mov    %eax,%ebx
  800a4e:	89 c7                	mov    %eax,%edi
  800a50:	89 c6                	mov    %eax,%esi
  800a52:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a54:	5b                   	pop    %ebx
  800a55:	5e                   	pop    %esi
  800a56:	5f                   	pop    %edi
  800a57:	5d                   	pop    %ebp
  800a58:	c3                   	ret    

00800a59 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a59:	55                   	push   %ebp
  800a5a:	89 e5                	mov    %esp,%ebp
  800a5c:	57                   	push   %edi
  800a5d:	56                   	push   %esi
  800a5e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a5f:	ba 00 00 00 00       	mov    $0x0,%edx
  800a64:	b8 01 00 00 00       	mov    $0x1,%eax
  800a69:	89 d1                	mov    %edx,%ecx
  800a6b:	89 d3                	mov    %edx,%ebx
  800a6d:	89 d7                	mov    %edx,%edi
  800a6f:	89 d6                	mov    %edx,%esi
  800a71:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a73:	5b                   	pop    %ebx
  800a74:	5e                   	pop    %esi
  800a75:	5f                   	pop    %edi
  800a76:	5d                   	pop    %ebp
  800a77:	c3                   	ret    

00800a78 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a78:	55                   	push   %ebp
  800a79:	89 e5                	mov    %esp,%ebp
  800a7b:	57                   	push   %edi
  800a7c:	56                   	push   %esi
  800a7d:	53                   	push   %ebx
  800a7e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a81:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a86:	b8 03 00 00 00       	mov    $0x3,%eax
  800a8b:	8b 55 08             	mov    0x8(%ebp),%edx
  800a8e:	89 cb                	mov    %ecx,%ebx
  800a90:	89 cf                	mov    %ecx,%edi
  800a92:	89 ce                	mov    %ecx,%esi
  800a94:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a96:	85 c0                	test   %eax,%eax
  800a98:	7e 17                	jle    800ab1 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a9a:	83 ec 0c             	sub    $0xc,%esp
  800a9d:	50                   	push   %eax
  800a9e:	6a 03                	push   $0x3
  800aa0:	68 5f 17 80 00       	push   $0x80175f
  800aa5:	6a 23                	push   $0x23
  800aa7:	68 7c 17 80 00       	push   $0x80177c
  800aac:	e8 2d 06 00 00       	call   8010de <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800ab1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ab4:	5b                   	pop    %ebx
  800ab5:	5e                   	pop    %esi
  800ab6:	5f                   	pop    %edi
  800ab7:	5d                   	pop    %ebp
  800ab8:	c3                   	ret    

00800ab9 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800ab9:	55                   	push   %ebp
  800aba:	89 e5                	mov    %esp,%ebp
  800abc:	57                   	push   %edi
  800abd:	56                   	push   %esi
  800abe:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800abf:	ba 00 00 00 00       	mov    $0x0,%edx
  800ac4:	b8 02 00 00 00       	mov    $0x2,%eax
  800ac9:	89 d1                	mov    %edx,%ecx
  800acb:	89 d3                	mov    %edx,%ebx
  800acd:	89 d7                	mov    %edx,%edi
  800acf:	89 d6                	mov    %edx,%esi
  800ad1:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800ad3:	5b                   	pop    %ebx
  800ad4:	5e                   	pop    %esi
  800ad5:	5f                   	pop    %edi
  800ad6:	5d                   	pop    %ebp
  800ad7:	c3                   	ret    

00800ad8 <sys_yield>:

void
sys_yield(void)
{
  800ad8:	55                   	push   %ebp
  800ad9:	89 e5                	mov    %esp,%ebp
  800adb:	57                   	push   %edi
  800adc:	56                   	push   %esi
  800add:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ade:	ba 00 00 00 00       	mov    $0x0,%edx
  800ae3:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ae8:	89 d1                	mov    %edx,%ecx
  800aea:	89 d3                	mov    %edx,%ebx
  800aec:	89 d7                	mov    %edx,%edi
  800aee:	89 d6                	mov    %edx,%esi
  800af0:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800af2:	5b                   	pop    %ebx
  800af3:	5e                   	pop    %esi
  800af4:	5f                   	pop    %edi
  800af5:	5d                   	pop    %ebp
  800af6:	c3                   	ret    

00800af7 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800af7:	55                   	push   %ebp
  800af8:	89 e5                	mov    %esp,%ebp
  800afa:	57                   	push   %edi
  800afb:	56                   	push   %esi
  800afc:	53                   	push   %ebx
  800afd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b00:	be 00 00 00 00       	mov    $0x0,%esi
  800b05:	b8 04 00 00 00       	mov    $0x4,%eax
  800b0a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b0d:	8b 55 08             	mov    0x8(%ebp),%edx
  800b10:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b13:	89 f7                	mov    %esi,%edi
  800b15:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b17:	85 c0                	test   %eax,%eax
  800b19:	7e 17                	jle    800b32 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b1b:	83 ec 0c             	sub    $0xc,%esp
  800b1e:	50                   	push   %eax
  800b1f:	6a 04                	push   $0x4
  800b21:	68 5f 17 80 00       	push   $0x80175f
  800b26:	6a 23                	push   $0x23
  800b28:	68 7c 17 80 00       	push   $0x80177c
  800b2d:	e8 ac 05 00 00       	call   8010de <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b32:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b35:	5b                   	pop    %ebx
  800b36:	5e                   	pop    %esi
  800b37:	5f                   	pop    %edi
  800b38:	5d                   	pop    %ebp
  800b39:	c3                   	ret    

00800b3a <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b3a:	55                   	push   %ebp
  800b3b:	89 e5                	mov    %esp,%ebp
  800b3d:	57                   	push   %edi
  800b3e:	56                   	push   %esi
  800b3f:	53                   	push   %ebx
  800b40:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b43:	b8 05 00 00 00       	mov    $0x5,%eax
  800b48:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b4b:	8b 55 08             	mov    0x8(%ebp),%edx
  800b4e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b51:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b54:	8b 75 18             	mov    0x18(%ebp),%esi
  800b57:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b59:	85 c0                	test   %eax,%eax
  800b5b:	7e 17                	jle    800b74 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b5d:	83 ec 0c             	sub    $0xc,%esp
  800b60:	50                   	push   %eax
  800b61:	6a 05                	push   $0x5
  800b63:	68 5f 17 80 00       	push   $0x80175f
  800b68:	6a 23                	push   $0x23
  800b6a:	68 7c 17 80 00       	push   $0x80177c
  800b6f:	e8 6a 05 00 00       	call   8010de <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b74:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b77:	5b                   	pop    %ebx
  800b78:	5e                   	pop    %esi
  800b79:	5f                   	pop    %edi
  800b7a:	5d                   	pop    %ebp
  800b7b:	c3                   	ret    

00800b7c <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b7c:	55                   	push   %ebp
  800b7d:	89 e5                	mov    %esp,%ebp
  800b7f:	57                   	push   %edi
  800b80:	56                   	push   %esi
  800b81:	53                   	push   %ebx
  800b82:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b85:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b8a:	b8 06 00 00 00       	mov    $0x6,%eax
  800b8f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b92:	8b 55 08             	mov    0x8(%ebp),%edx
  800b95:	89 df                	mov    %ebx,%edi
  800b97:	89 de                	mov    %ebx,%esi
  800b99:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b9b:	85 c0                	test   %eax,%eax
  800b9d:	7e 17                	jle    800bb6 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b9f:	83 ec 0c             	sub    $0xc,%esp
  800ba2:	50                   	push   %eax
  800ba3:	6a 06                	push   $0x6
  800ba5:	68 5f 17 80 00       	push   $0x80175f
  800baa:	6a 23                	push   $0x23
  800bac:	68 7c 17 80 00       	push   $0x80177c
  800bb1:	e8 28 05 00 00       	call   8010de <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800bb6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bb9:	5b                   	pop    %ebx
  800bba:	5e                   	pop    %esi
  800bbb:	5f                   	pop    %edi
  800bbc:	5d                   	pop    %ebp
  800bbd:	c3                   	ret    

00800bbe <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800bbe:	55                   	push   %ebp
  800bbf:	89 e5                	mov    %esp,%ebp
  800bc1:	57                   	push   %edi
  800bc2:	56                   	push   %esi
  800bc3:	53                   	push   %ebx
  800bc4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc7:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bcc:	b8 08 00 00 00       	mov    $0x8,%eax
  800bd1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bd4:	8b 55 08             	mov    0x8(%ebp),%edx
  800bd7:	89 df                	mov    %ebx,%edi
  800bd9:	89 de                	mov    %ebx,%esi
  800bdb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bdd:	85 c0                	test   %eax,%eax
  800bdf:	7e 17                	jle    800bf8 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800be1:	83 ec 0c             	sub    $0xc,%esp
  800be4:	50                   	push   %eax
  800be5:	6a 08                	push   $0x8
  800be7:	68 5f 17 80 00       	push   $0x80175f
  800bec:	6a 23                	push   $0x23
  800bee:	68 7c 17 80 00       	push   $0x80177c
  800bf3:	e8 e6 04 00 00       	call   8010de <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800bf8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bfb:	5b                   	pop    %ebx
  800bfc:	5e                   	pop    %esi
  800bfd:	5f                   	pop    %edi
  800bfe:	5d                   	pop    %ebp
  800bff:	c3                   	ret    

00800c00 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c00:	55                   	push   %ebp
  800c01:	89 e5                	mov    %esp,%ebp
  800c03:	57                   	push   %edi
  800c04:	56                   	push   %esi
  800c05:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c06:	ba 00 00 00 00       	mov    $0x0,%edx
  800c0b:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c10:	89 d1                	mov    %edx,%ecx
  800c12:	89 d3                	mov    %edx,%ebx
  800c14:	89 d7                	mov    %edx,%edi
  800c16:	89 d6                	mov    %edx,%esi
  800c18:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c1a:	5b                   	pop    %ebx
  800c1b:	5e                   	pop    %esi
  800c1c:	5f                   	pop    %edi
  800c1d:	5d                   	pop    %ebp
  800c1e:	c3                   	ret    

00800c1f <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c1f:	55                   	push   %ebp
  800c20:	89 e5                	mov    %esp,%ebp
  800c22:	57                   	push   %edi
  800c23:	56                   	push   %esi
  800c24:	53                   	push   %ebx
  800c25:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c28:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c2d:	b8 09 00 00 00       	mov    $0x9,%eax
  800c32:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c35:	8b 55 08             	mov    0x8(%ebp),%edx
  800c38:	89 df                	mov    %ebx,%edi
  800c3a:	89 de                	mov    %ebx,%esi
  800c3c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c3e:	85 c0                	test   %eax,%eax
  800c40:	7e 17                	jle    800c59 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c42:	83 ec 0c             	sub    $0xc,%esp
  800c45:	50                   	push   %eax
  800c46:	6a 09                	push   $0x9
  800c48:	68 5f 17 80 00       	push   $0x80175f
  800c4d:	6a 23                	push   $0x23
  800c4f:	68 7c 17 80 00       	push   $0x80177c
  800c54:	e8 85 04 00 00       	call   8010de <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c59:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c5c:	5b                   	pop    %ebx
  800c5d:	5e                   	pop    %esi
  800c5e:	5f                   	pop    %edi
  800c5f:	5d                   	pop    %ebp
  800c60:	c3                   	ret    

00800c61 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c61:	55                   	push   %ebp
  800c62:	89 e5                	mov    %esp,%ebp
  800c64:	57                   	push   %edi
  800c65:	56                   	push   %esi
  800c66:	53                   	push   %ebx
  800c67:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c6a:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c6f:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c74:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c77:	8b 55 08             	mov    0x8(%ebp),%edx
  800c7a:	89 df                	mov    %ebx,%edi
  800c7c:	89 de                	mov    %ebx,%esi
  800c7e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c80:	85 c0                	test   %eax,%eax
  800c82:	7e 17                	jle    800c9b <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c84:	83 ec 0c             	sub    $0xc,%esp
  800c87:	50                   	push   %eax
  800c88:	6a 0a                	push   $0xa
  800c8a:	68 5f 17 80 00       	push   $0x80175f
  800c8f:	6a 23                	push   $0x23
  800c91:	68 7c 17 80 00       	push   $0x80177c
  800c96:	e8 43 04 00 00       	call   8010de <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800c9b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c9e:	5b                   	pop    %ebx
  800c9f:	5e                   	pop    %esi
  800ca0:	5f                   	pop    %edi
  800ca1:	5d                   	pop    %ebp
  800ca2:	c3                   	ret    

00800ca3 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800ca3:	55                   	push   %ebp
  800ca4:	89 e5                	mov    %esp,%ebp
  800ca6:	57                   	push   %edi
  800ca7:	56                   	push   %esi
  800ca8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ca9:	be 00 00 00 00       	mov    $0x0,%esi
  800cae:	b8 0d 00 00 00       	mov    $0xd,%eax
  800cb3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cb6:	8b 55 08             	mov    0x8(%ebp),%edx
  800cb9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cbc:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cbf:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800cc1:	5b                   	pop    %ebx
  800cc2:	5e                   	pop    %esi
  800cc3:	5f                   	pop    %edi
  800cc4:	5d                   	pop    %ebp
  800cc5:	c3                   	ret    

00800cc6 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800cc6:	55                   	push   %ebp
  800cc7:	89 e5                	mov    %esp,%ebp
  800cc9:	57                   	push   %edi
  800cca:	56                   	push   %esi
  800ccb:	53                   	push   %ebx
  800ccc:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ccf:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cd4:	b8 0e 00 00 00       	mov    $0xe,%eax
  800cd9:	8b 55 08             	mov    0x8(%ebp),%edx
  800cdc:	89 cb                	mov    %ecx,%ebx
  800cde:	89 cf                	mov    %ecx,%edi
  800ce0:	89 ce                	mov    %ecx,%esi
  800ce2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ce4:	85 c0                	test   %eax,%eax
  800ce6:	7e 17                	jle    800cff <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ce8:	83 ec 0c             	sub    $0xc,%esp
  800ceb:	50                   	push   %eax
  800cec:	6a 0e                	push   $0xe
  800cee:	68 5f 17 80 00       	push   $0x80175f
  800cf3:	6a 23                	push   $0x23
  800cf5:	68 7c 17 80 00       	push   $0x80177c
  800cfa:	e8 df 03 00 00       	call   8010de <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800cff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d02:	5b                   	pop    %ebx
  800d03:	5e                   	pop    %esi
  800d04:	5f                   	pop    %edi
  800d05:	5d                   	pop    %ebp
  800d06:	c3                   	ret    

00800d07 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800d07:	55                   	push   %ebp
  800d08:	89 e5                	mov    %esp,%ebp
  800d0a:	53                   	push   %ebx
  800d0b:	83 ec 04             	sub    $0x4,%esp
  800d0e:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800d11:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800d13:	89 da                	mov    %ebx,%edx
  800d15:	c1 ea 0c             	shr    $0xc,%edx
  800d18:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800d1f:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800d23:	74 05                	je     800d2a <pgfault+0x23>
  800d25:	f6 c6 08             	test   $0x8,%dh
  800d28:	75 14                	jne    800d3e <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800d2a:	83 ec 04             	sub    $0x4,%esp
  800d2d:	68 8c 17 80 00       	push   $0x80178c
  800d32:	6a 26                	push   $0x26
  800d34:	68 f0 17 80 00       	push   $0x8017f0
  800d39:	e8 a0 03 00 00       	call   8010de <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800d3e:	83 ec 04             	sub    $0x4,%esp
  800d41:	6a 07                	push   $0x7
  800d43:	68 00 f0 7f 00       	push   $0x7ff000
  800d48:	6a 00                	push   $0x0
  800d4a:	e8 a8 fd ff ff       	call   800af7 <sys_page_alloc>
  800d4f:	83 c4 10             	add    $0x10,%esp
  800d52:	85 c0                	test   %eax,%eax
  800d54:	74 14                	je     800d6a <pgfault+0x63>
        panic("pgfault: no phys mem");
  800d56:	83 ec 04             	sub    $0x4,%esp
  800d59:	68 fb 17 80 00       	push   $0x8017fb
  800d5e:	6a 32                	push   $0x32
  800d60:	68 f0 17 80 00       	push   $0x8017f0
  800d65:	e8 74 03 00 00       	call   8010de <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800d6a:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800d70:	83 ec 04             	sub    $0x4,%esp
  800d73:	68 00 10 00 00       	push   $0x1000
  800d78:	53                   	push   %ebx
  800d79:	68 00 f0 7f 00       	push   $0x7ff000
  800d7e:	e8 05 fb ff ff       	call   800888 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800d83:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800d8a:	53                   	push   %ebx
  800d8b:	6a 00                	push   $0x0
  800d8d:	68 00 f0 7f 00       	push   $0x7ff000
  800d92:	6a 00                	push   $0x0
  800d94:	e8 a1 fd ff ff       	call   800b3a <sys_page_map>
  800d99:	83 c4 20             	add    $0x20,%esp
  800d9c:	85 c0                	test   %eax,%eax
  800d9e:	74 14                	je     800db4 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800da0:	83 ec 04             	sub    $0x4,%esp
  800da3:	68 10 18 80 00       	push   $0x801810
  800da8:	6a 3f                	push   $0x3f
  800daa:	68 f0 17 80 00       	push   $0x8017f0
  800daf:	e8 2a 03 00 00       	call   8010de <_panic>


	//panic("pgfault not implemented");
}
  800db4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800db7:	c9                   	leave  
  800db8:	c3                   	ret    

00800db9 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800db9:	55                   	push   %ebp
  800dba:	89 e5                	mov    %esp,%ebp
  800dbc:	57                   	push   %edi
  800dbd:	56                   	push   %esi
  800dbe:	53                   	push   %ebx
  800dbf:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800dc2:	68 07 0d 80 00       	push   $0x800d07
  800dc7:	e8 58 03 00 00       	call   801124 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800dcc:	b8 07 00 00 00       	mov    $0x7,%eax
  800dd1:	cd 30                	int    $0x30
  800dd3:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800dd6:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800dd9:	83 c4 10             	add    $0x10,%esp
  800ddc:	85 c0                	test   %eax,%eax
  800dde:	79 17                	jns    800df7 <fork+0x3e>
        panic("fork: cannot create child env");
  800de0:	83 ec 04             	sub    $0x4,%esp
  800de3:	68 23 18 80 00       	push   $0x801823
  800de8:	68 97 00 00 00       	push   $0x97
  800ded:	68 f0 17 80 00       	push   $0x8017f0
  800df2:	e8 e7 02 00 00       	call   8010de <_panic>
    } else if (envid == 0) {
  800df7:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800dfb:	75 2a                	jne    800e27 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800dfd:	e8 b7 fc ff ff       	call   800ab9 <sys_getenvid>
  800e02:	25 ff 03 00 00       	and    $0x3ff,%eax
  800e07:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e0e:	c1 e0 07             	shl    $0x7,%eax
  800e11:	29 d0                	sub    %edx,%eax
  800e13:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800e18:	a3 04 20 80 00       	mov    %eax,0x802004
        return 0;
  800e1d:	b8 00 00 00 00       	mov    $0x0,%eax
  800e22:	e9 94 01 00 00       	jmp    800fbb <fork+0x202>
  800e27:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800e2e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800e31:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800e38:	a8 01                	test   $0x1,%al
  800e3a:	0f 84 0a 01 00 00    	je     800f4a <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800e40:	c1 e7 0a             	shl    $0xa,%edi
  800e43:	be 00 00 00 00       	mov    $0x0,%esi
  800e48:	89 fb                	mov    %edi,%ebx
  800e4a:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800e4c:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800e52:	75 34                	jne    800e88 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800e54:	83 ec 04             	sub    $0x4,%esp
  800e57:	6a 07                	push   $0x7
  800e59:	68 00 f0 bf ee       	push   $0xeebff000
  800e5e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800e61:	e8 91 fc ff ff       	call   800af7 <sys_page_alloc>
  800e66:	83 c4 10             	add    $0x10,%esp
  800e69:	85 c0                	test   %eax,%eax
  800e6b:	0f 84 cc 00 00 00    	je     800f3d <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800e71:	83 ec 04             	sub    $0x4,%esp
  800e74:	68 41 18 80 00       	push   $0x801841
  800e79:	68 ad 00 00 00       	push   $0xad
  800e7e:	68 f0 17 80 00       	push   $0x8017f0
  800e83:	e8 56 02 00 00       	call   8010de <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800e88:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800e8f:	a8 01                	test   $0x1,%al
  800e91:	0f 84 a6 00 00 00    	je     800f3d <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800e97:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800e9e:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800ea1:	a9 02 08 00 00       	test   $0x802,%eax
  800ea6:	74 62                	je     800f0a <fork+0x151>
  800ea8:	f6 c4 04             	test   $0x4,%ah
  800eab:	75 78                	jne    800f25 <fork+0x16c>
        if (sys_page_map(0,
  800ead:	83 ec 0c             	sub    $0xc,%esp
  800eb0:	68 05 08 00 00       	push   $0x805
  800eb5:	53                   	push   %ebx
  800eb6:	ff 75 e4             	pushl  -0x1c(%ebp)
  800eb9:	53                   	push   %ebx
  800eba:	6a 00                	push   $0x0
  800ebc:	e8 79 fc ff ff       	call   800b3a <sys_page_map>
  800ec1:	83 c4 20             	add    $0x20,%esp
  800ec4:	85 c0                	test   %eax,%eax
  800ec6:	74 14                	je     800edc <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800ec8:	83 ec 04             	sub    $0x4,%esp
  800ecb:	68 5c 18 80 00       	push   $0x80185c
  800ed0:	6a 64                	push   $0x64
  800ed2:	68 f0 17 80 00       	push   $0x8017f0
  800ed7:	e8 02 02 00 00       	call   8010de <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800edc:	83 ec 0c             	sub    $0xc,%esp
  800edf:	68 05 08 00 00       	push   $0x805
  800ee4:	53                   	push   %ebx
  800ee5:	6a 00                	push   $0x0
  800ee7:	53                   	push   %ebx
  800ee8:	6a 00                	push   $0x0
  800eea:	e8 4b fc ff ff       	call   800b3a <sys_page_map>
  800eef:	83 c4 20             	add    $0x20,%esp
  800ef2:	85 c0                	test   %eax,%eax
  800ef4:	74 47                	je     800f3d <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  800ef6:	83 ec 04             	sub    $0x4,%esp
  800ef9:	68 73 18 80 00       	push   $0x801873
  800efe:	6a 6d                	push   $0x6d
  800f00:	68 f0 17 80 00       	push   $0x8017f0
  800f05:	e8 d4 01 00 00       	call   8010de <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  800f0a:	f6 c4 04             	test   $0x4,%ah
  800f0d:	75 16                	jne    800f25 <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  800f0f:	83 ec 0c             	sub    $0xc,%esp
  800f12:	6a 05                	push   $0x5
  800f14:	53                   	push   %ebx
  800f15:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f18:	53                   	push   %ebx
  800f19:	6a 00                	push   $0x0
  800f1b:	e8 1a fc ff ff       	call   800b3a <sys_page_map>
  800f20:	83 c4 20             	add    $0x20,%esp
  800f23:	eb 18                	jmp    800f3d <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  800f25:	83 ec 0c             	sub    $0xc,%esp
  800f28:	25 07 0e 00 00       	and    $0xe07,%eax
  800f2d:	50                   	push   %eax
  800f2e:	53                   	push   %ebx
  800f2f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f32:	53                   	push   %ebx
  800f33:	6a 00                	push   $0x0
  800f35:	e8 00 fc ff ff       	call   800b3a <sys_page_map>
  800f3a:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  800f3d:	46                   	inc    %esi
  800f3e:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  800f44:	0f 85 fe fe ff ff    	jne    800e48 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  800f4a:	ff 45 e0             	incl   -0x20(%ebp)
  800f4d:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800f50:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  800f55:	0f 85 d3 fe ff ff    	jne    800e2e <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  800f5b:	a1 04 20 80 00       	mov    0x802004,%eax
  800f60:	8b 40 64             	mov    0x64(%eax),%eax
  800f63:	83 ec 08             	sub    $0x8,%esp
  800f66:	50                   	push   %eax
  800f67:	ff 75 dc             	pushl  -0x24(%ebp)
  800f6a:	e8 f2 fc ff ff       	call   800c61 <sys_env_set_pgfault_upcall>
  800f6f:	83 c4 10             	add    $0x10,%esp
  800f72:	85 c0                	test   %eax,%eax
  800f74:	74 17                	je     800f8d <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  800f76:	83 ec 04             	sub    $0x4,%esp
  800f79:	68 d0 17 80 00       	push   $0x8017d0
  800f7e:	68 b9 00 00 00       	push   $0xb9
  800f83:	68 f0 17 80 00       	push   $0x8017f0
  800f88:	e8 51 01 00 00       	call   8010de <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  800f8d:	83 ec 08             	sub    $0x8,%esp
  800f90:	6a 02                	push   $0x2
  800f92:	ff 75 dc             	pushl  -0x24(%ebp)
  800f95:	e8 24 fc ff ff       	call   800bbe <sys_env_set_status>
  800f9a:	83 c4 10             	add    $0x10,%esp
  800f9d:	85 c0                	test   %eax,%eax
  800f9f:	74 17                	je     800fb8 <fork+0x1ff>
        panic("fork: cannot set env status");
  800fa1:	83 ec 04             	sub    $0x4,%esp
  800fa4:	68 8e 18 80 00       	push   $0x80188e
  800fa9:	68 bd 00 00 00       	push   $0xbd
  800fae:	68 f0 17 80 00       	push   $0x8017f0
  800fb3:	e8 26 01 00 00       	call   8010de <_panic>

    return envid;
  800fb8:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  800fbb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fbe:	5b                   	pop    %ebx
  800fbf:	5e                   	pop    %esi
  800fc0:	5f                   	pop    %edi
  800fc1:	5d                   	pop    %ebp
  800fc2:	c3                   	ret    

00800fc3 <sfork>:

// Challenge!
int
sfork(void)
{
  800fc3:	55                   	push   %ebp
  800fc4:	89 e5                	mov    %esp,%ebp
  800fc6:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  800fc9:	68 aa 18 80 00       	push   $0x8018aa
  800fce:	68 c8 00 00 00       	push   $0xc8
  800fd3:	68 f0 17 80 00       	push   $0x8017f0
  800fd8:	e8 01 01 00 00       	call   8010de <_panic>

00800fdd <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  800fdd:	55                   	push   %ebp
  800fde:	89 e5                	mov    %esp,%ebp
  800fe0:	56                   	push   %esi
  800fe1:	53                   	push   %ebx
  800fe2:	8b 75 08             	mov    0x8(%ebp),%esi
  800fe5:	8b 45 0c             	mov    0xc(%ebp),%eax
  800fe8:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  800feb:	85 c0                	test   %eax,%eax
  800fed:	75 05                	jne    800ff4 <ipc_recv+0x17>
        pg = (void *)UTOP;
  800fef:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  800ff4:	83 ec 0c             	sub    $0xc,%esp
  800ff7:	50                   	push   %eax
  800ff8:	e8 c9 fc ff ff       	call   800cc6 <sys_ipc_recv>
  800ffd:	83 c4 10             	add    $0x10,%esp
  801000:	85 c0                	test   %eax,%eax
  801002:	74 16                	je     80101a <ipc_recv+0x3d>
        if (from_env_store)
  801004:	85 f6                	test   %esi,%esi
  801006:	74 06                	je     80100e <ipc_recv+0x31>
            *from_env_store = 0;
  801008:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  80100e:	85 db                	test   %ebx,%ebx
  801010:	74 2c                	je     80103e <ipc_recv+0x61>
            *perm_store = 0;
  801012:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801018:	eb 24                	jmp    80103e <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  80101a:	85 f6                	test   %esi,%esi
  80101c:	74 0a                	je     801028 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  80101e:	a1 04 20 80 00       	mov    0x802004,%eax
  801023:	8b 40 74             	mov    0x74(%eax),%eax
  801026:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801028:	85 db                	test   %ebx,%ebx
  80102a:	74 0a                	je     801036 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  80102c:	a1 04 20 80 00       	mov    0x802004,%eax
  801031:	8b 40 78             	mov    0x78(%eax),%eax
  801034:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801036:	a1 04 20 80 00       	mov    0x802004,%eax
  80103b:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  80103e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801041:	5b                   	pop    %ebx
  801042:	5e                   	pop    %esi
  801043:	5d                   	pop    %ebp
  801044:	c3                   	ret    

00801045 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801045:	55                   	push   %ebp
  801046:	89 e5                	mov    %esp,%ebp
  801048:	57                   	push   %edi
  801049:	56                   	push   %esi
  80104a:	53                   	push   %ebx
  80104b:	83 ec 0c             	sub    $0xc,%esp
  80104e:	8b 75 0c             	mov    0xc(%ebp),%esi
  801051:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801054:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801057:	85 db                	test   %ebx,%ebx
  801059:	75 0c                	jne    801067 <ipc_send+0x22>
        pg = (void *)UTOP;
  80105b:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801060:	eb 05                	jmp    801067 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801062:	e8 71 fa ff ff       	call   800ad8 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801067:	57                   	push   %edi
  801068:	53                   	push   %ebx
  801069:	56                   	push   %esi
  80106a:	ff 75 08             	pushl  0x8(%ebp)
  80106d:	e8 31 fc ff ff       	call   800ca3 <sys_ipc_try_send>
  801072:	83 c4 10             	add    $0x10,%esp
  801075:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801078:	74 e8                	je     801062 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  80107a:	85 c0                	test   %eax,%eax
  80107c:	74 14                	je     801092 <ipc_send+0x4d>
        panic("ipc_send: error");
  80107e:	83 ec 04             	sub    $0x4,%esp
  801081:	68 c0 18 80 00       	push   $0x8018c0
  801086:	6a 6a                	push   $0x6a
  801088:	68 d0 18 80 00       	push   $0x8018d0
  80108d:	e8 4c 00 00 00       	call   8010de <_panic>
    }
	//panic("ipc_send not implemented");
}
  801092:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801095:	5b                   	pop    %ebx
  801096:	5e                   	pop    %esi
  801097:	5f                   	pop    %edi
  801098:	5d                   	pop    %ebp
  801099:	c3                   	ret    

0080109a <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  80109a:	55                   	push   %ebp
  80109b:	89 e5                	mov    %esp,%ebp
  80109d:	53                   	push   %ebx
  80109e:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  8010a1:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  8010a6:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  8010ad:	89 d0                	mov    %edx,%eax
  8010af:	c1 e0 07             	shl    $0x7,%eax
  8010b2:	29 d8                	sub    %ebx,%eax
  8010b4:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8010b9:	8b 40 50             	mov    0x50(%eax),%eax
  8010bc:	39 c8                	cmp    %ecx,%eax
  8010be:	75 0d                	jne    8010cd <ipc_find_env+0x33>
			return envs[i].env_id;
  8010c0:	c1 e2 07             	shl    $0x7,%edx
  8010c3:	29 da                	sub    %ebx,%edx
  8010c5:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  8010cb:	eb 0e                	jmp    8010db <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  8010cd:	42                   	inc    %edx
  8010ce:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  8010d4:	75 d0                	jne    8010a6 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  8010d6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8010db:	5b                   	pop    %ebx
  8010dc:	5d                   	pop    %ebp
  8010dd:	c3                   	ret    

008010de <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8010de:	55                   	push   %ebp
  8010df:	89 e5                	mov    %esp,%ebp
  8010e1:	56                   	push   %esi
  8010e2:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8010e3:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8010e6:	8b 35 00 20 80 00    	mov    0x802000,%esi
  8010ec:	e8 c8 f9 ff ff       	call   800ab9 <sys_getenvid>
  8010f1:	83 ec 0c             	sub    $0xc,%esp
  8010f4:	ff 75 0c             	pushl  0xc(%ebp)
  8010f7:	ff 75 08             	pushl  0x8(%ebp)
  8010fa:	56                   	push   %esi
  8010fb:	50                   	push   %eax
  8010fc:	68 dc 18 80 00       	push   $0x8018dc
  801101:	e8 a9 f0 ff ff       	call   8001af <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  801106:	83 c4 18             	add    $0x18,%esp
  801109:	53                   	push   %ebx
  80110a:	ff 75 10             	pushl  0x10(%ebp)
  80110d:	e8 4c f0 ff ff       	call   80015e <vcprintf>
	cprintf("\n");
  801112:	c7 04 24 47 14 80 00 	movl   $0x801447,(%esp)
  801119:	e8 91 f0 ff ff       	call   8001af <cprintf>
  80111e:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801121:	cc                   	int3   
  801122:	eb fd                	jmp    801121 <_panic+0x43>

00801124 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801124:	55                   	push   %ebp
  801125:	89 e5                	mov    %esp,%ebp
  801127:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  80112a:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  801131:	75 3e                	jne    801171 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801133:	83 ec 04             	sub    $0x4,%esp
  801136:	6a 07                	push   $0x7
  801138:	68 00 f0 bf ee       	push   $0xeebff000
  80113d:	6a 00                	push   $0x0
  80113f:	e8 b3 f9 ff ff       	call   800af7 <sys_page_alloc>
  801144:	83 c4 10             	add    $0x10,%esp
  801147:	85 c0                	test   %eax,%eax
  801149:	74 14                	je     80115f <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  80114b:	83 ec 04             	sub    $0x4,%esp
  80114e:	68 00 19 80 00       	push   $0x801900
  801153:	6a 23                	push   $0x23
  801155:	68 24 19 80 00       	push   $0x801924
  80115a:	e8 7f ff ff ff       	call   8010de <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  80115f:	83 ec 08             	sub    $0x8,%esp
  801162:	68 7b 11 80 00       	push   $0x80117b
  801167:	6a 00                	push   $0x0
  801169:	e8 f3 fa ff ff       	call   800c61 <sys_env_set_pgfault_upcall>
  80116e:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801171:	8b 45 08             	mov    0x8(%ebp),%eax
  801174:	a3 08 20 80 00       	mov    %eax,0x802008
}
  801179:	c9                   	leave  
  80117a:	c3                   	ret    

0080117b <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  80117b:	54                   	push   %esp
	movl _pgfault_handler, %eax
  80117c:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  801181:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801183:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801186:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801188:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  80118c:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801190:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801191:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801194:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801196:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801199:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  80119c:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  80119d:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8011a0:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8011a1:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8011a2:	c3                   	ret    
  8011a3:	90                   	nop

008011a4 <__udivdi3>:
  8011a4:	55                   	push   %ebp
  8011a5:	57                   	push   %edi
  8011a6:	56                   	push   %esi
  8011a7:	53                   	push   %ebx
  8011a8:	83 ec 1c             	sub    $0x1c,%esp
  8011ab:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8011af:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8011b3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8011b7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8011bb:	89 ca                	mov    %ecx,%edx
  8011bd:	89 f8                	mov    %edi,%eax
  8011bf:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8011c3:	85 f6                	test   %esi,%esi
  8011c5:	75 2d                	jne    8011f4 <__udivdi3+0x50>
  8011c7:	39 cf                	cmp    %ecx,%edi
  8011c9:	77 65                	ja     801230 <__udivdi3+0x8c>
  8011cb:	89 fd                	mov    %edi,%ebp
  8011cd:	85 ff                	test   %edi,%edi
  8011cf:	75 0b                	jne    8011dc <__udivdi3+0x38>
  8011d1:	b8 01 00 00 00       	mov    $0x1,%eax
  8011d6:	31 d2                	xor    %edx,%edx
  8011d8:	f7 f7                	div    %edi
  8011da:	89 c5                	mov    %eax,%ebp
  8011dc:	31 d2                	xor    %edx,%edx
  8011de:	89 c8                	mov    %ecx,%eax
  8011e0:	f7 f5                	div    %ebp
  8011e2:	89 c1                	mov    %eax,%ecx
  8011e4:	89 d8                	mov    %ebx,%eax
  8011e6:	f7 f5                	div    %ebp
  8011e8:	89 cf                	mov    %ecx,%edi
  8011ea:	89 fa                	mov    %edi,%edx
  8011ec:	83 c4 1c             	add    $0x1c,%esp
  8011ef:	5b                   	pop    %ebx
  8011f0:	5e                   	pop    %esi
  8011f1:	5f                   	pop    %edi
  8011f2:	5d                   	pop    %ebp
  8011f3:	c3                   	ret    
  8011f4:	39 ce                	cmp    %ecx,%esi
  8011f6:	77 28                	ja     801220 <__udivdi3+0x7c>
  8011f8:	0f bd fe             	bsr    %esi,%edi
  8011fb:	83 f7 1f             	xor    $0x1f,%edi
  8011fe:	75 40                	jne    801240 <__udivdi3+0x9c>
  801200:	39 ce                	cmp    %ecx,%esi
  801202:	72 0a                	jb     80120e <__udivdi3+0x6a>
  801204:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801208:	0f 87 9e 00 00 00    	ja     8012ac <__udivdi3+0x108>
  80120e:	b8 01 00 00 00       	mov    $0x1,%eax
  801213:	89 fa                	mov    %edi,%edx
  801215:	83 c4 1c             	add    $0x1c,%esp
  801218:	5b                   	pop    %ebx
  801219:	5e                   	pop    %esi
  80121a:	5f                   	pop    %edi
  80121b:	5d                   	pop    %ebp
  80121c:	c3                   	ret    
  80121d:	8d 76 00             	lea    0x0(%esi),%esi
  801220:	31 ff                	xor    %edi,%edi
  801222:	31 c0                	xor    %eax,%eax
  801224:	89 fa                	mov    %edi,%edx
  801226:	83 c4 1c             	add    $0x1c,%esp
  801229:	5b                   	pop    %ebx
  80122a:	5e                   	pop    %esi
  80122b:	5f                   	pop    %edi
  80122c:	5d                   	pop    %ebp
  80122d:	c3                   	ret    
  80122e:	66 90                	xchg   %ax,%ax
  801230:	89 d8                	mov    %ebx,%eax
  801232:	f7 f7                	div    %edi
  801234:	31 ff                	xor    %edi,%edi
  801236:	89 fa                	mov    %edi,%edx
  801238:	83 c4 1c             	add    $0x1c,%esp
  80123b:	5b                   	pop    %ebx
  80123c:	5e                   	pop    %esi
  80123d:	5f                   	pop    %edi
  80123e:	5d                   	pop    %ebp
  80123f:	c3                   	ret    
  801240:	bd 20 00 00 00       	mov    $0x20,%ebp
  801245:	89 eb                	mov    %ebp,%ebx
  801247:	29 fb                	sub    %edi,%ebx
  801249:	89 f9                	mov    %edi,%ecx
  80124b:	d3 e6                	shl    %cl,%esi
  80124d:	89 c5                	mov    %eax,%ebp
  80124f:	88 d9                	mov    %bl,%cl
  801251:	d3 ed                	shr    %cl,%ebp
  801253:	89 e9                	mov    %ebp,%ecx
  801255:	09 f1                	or     %esi,%ecx
  801257:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80125b:	89 f9                	mov    %edi,%ecx
  80125d:	d3 e0                	shl    %cl,%eax
  80125f:	89 c5                	mov    %eax,%ebp
  801261:	89 d6                	mov    %edx,%esi
  801263:	88 d9                	mov    %bl,%cl
  801265:	d3 ee                	shr    %cl,%esi
  801267:	89 f9                	mov    %edi,%ecx
  801269:	d3 e2                	shl    %cl,%edx
  80126b:	8b 44 24 08          	mov    0x8(%esp),%eax
  80126f:	88 d9                	mov    %bl,%cl
  801271:	d3 e8                	shr    %cl,%eax
  801273:	09 c2                	or     %eax,%edx
  801275:	89 d0                	mov    %edx,%eax
  801277:	89 f2                	mov    %esi,%edx
  801279:	f7 74 24 0c          	divl   0xc(%esp)
  80127d:	89 d6                	mov    %edx,%esi
  80127f:	89 c3                	mov    %eax,%ebx
  801281:	f7 e5                	mul    %ebp
  801283:	39 d6                	cmp    %edx,%esi
  801285:	72 19                	jb     8012a0 <__udivdi3+0xfc>
  801287:	74 0b                	je     801294 <__udivdi3+0xf0>
  801289:	89 d8                	mov    %ebx,%eax
  80128b:	31 ff                	xor    %edi,%edi
  80128d:	e9 58 ff ff ff       	jmp    8011ea <__udivdi3+0x46>
  801292:	66 90                	xchg   %ax,%ax
  801294:	8b 54 24 08          	mov    0x8(%esp),%edx
  801298:	89 f9                	mov    %edi,%ecx
  80129a:	d3 e2                	shl    %cl,%edx
  80129c:	39 c2                	cmp    %eax,%edx
  80129e:	73 e9                	jae    801289 <__udivdi3+0xe5>
  8012a0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8012a3:	31 ff                	xor    %edi,%edi
  8012a5:	e9 40 ff ff ff       	jmp    8011ea <__udivdi3+0x46>
  8012aa:	66 90                	xchg   %ax,%ax
  8012ac:	31 c0                	xor    %eax,%eax
  8012ae:	e9 37 ff ff ff       	jmp    8011ea <__udivdi3+0x46>
  8012b3:	90                   	nop

008012b4 <__umoddi3>:
  8012b4:	55                   	push   %ebp
  8012b5:	57                   	push   %edi
  8012b6:	56                   	push   %esi
  8012b7:	53                   	push   %ebx
  8012b8:	83 ec 1c             	sub    $0x1c,%esp
  8012bb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8012bf:	8b 74 24 34          	mov    0x34(%esp),%esi
  8012c3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8012c7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8012cb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8012cf:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8012d3:	89 f3                	mov    %esi,%ebx
  8012d5:	89 fa                	mov    %edi,%edx
  8012d7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8012db:	89 34 24             	mov    %esi,(%esp)
  8012de:	85 c0                	test   %eax,%eax
  8012e0:	75 1a                	jne    8012fc <__umoddi3+0x48>
  8012e2:	39 f7                	cmp    %esi,%edi
  8012e4:	0f 86 a2 00 00 00    	jbe    80138c <__umoddi3+0xd8>
  8012ea:	89 c8                	mov    %ecx,%eax
  8012ec:	89 f2                	mov    %esi,%edx
  8012ee:	f7 f7                	div    %edi
  8012f0:	89 d0                	mov    %edx,%eax
  8012f2:	31 d2                	xor    %edx,%edx
  8012f4:	83 c4 1c             	add    $0x1c,%esp
  8012f7:	5b                   	pop    %ebx
  8012f8:	5e                   	pop    %esi
  8012f9:	5f                   	pop    %edi
  8012fa:	5d                   	pop    %ebp
  8012fb:	c3                   	ret    
  8012fc:	39 f0                	cmp    %esi,%eax
  8012fe:	0f 87 ac 00 00 00    	ja     8013b0 <__umoddi3+0xfc>
  801304:	0f bd e8             	bsr    %eax,%ebp
  801307:	83 f5 1f             	xor    $0x1f,%ebp
  80130a:	0f 84 ac 00 00 00    	je     8013bc <__umoddi3+0x108>
  801310:	bf 20 00 00 00       	mov    $0x20,%edi
  801315:	29 ef                	sub    %ebp,%edi
  801317:	89 fe                	mov    %edi,%esi
  801319:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80131d:	89 e9                	mov    %ebp,%ecx
  80131f:	d3 e0                	shl    %cl,%eax
  801321:	89 d7                	mov    %edx,%edi
  801323:	89 f1                	mov    %esi,%ecx
  801325:	d3 ef                	shr    %cl,%edi
  801327:	09 c7                	or     %eax,%edi
  801329:	89 e9                	mov    %ebp,%ecx
  80132b:	d3 e2                	shl    %cl,%edx
  80132d:	89 14 24             	mov    %edx,(%esp)
  801330:	89 d8                	mov    %ebx,%eax
  801332:	d3 e0                	shl    %cl,%eax
  801334:	89 c2                	mov    %eax,%edx
  801336:	8b 44 24 08          	mov    0x8(%esp),%eax
  80133a:	d3 e0                	shl    %cl,%eax
  80133c:	89 44 24 04          	mov    %eax,0x4(%esp)
  801340:	8b 44 24 08          	mov    0x8(%esp),%eax
  801344:	89 f1                	mov    %esi,%ecx
  801346:	d3 e8                	shr    %cl,%eax
  801348:	09 d0                	or     %edx,%eax
  80134a:	d3 eb                	shr    %cl,%ebx
  80134c:	89 da                	mov    %ebx,%edx
  80134e:	f7 f7                	div    %edi
  801350:	89 d3                	mov    %edx,%ebx
  801352:	f7 24 24             	mull   (%esp)
  801355:	89 c6                	mov    %eax,%esi
  801357:	89 d1                	mov    %edx,%ecx
  801359:	39 d3                	cmp    %edx,%ebx
  80135b:	0f 82 87 00 00 00    	jb     8013e8 <__umoddi3+0x134>
  801361:	0f 84 91 00 00 00    	je     8013f8 <__umoddi3+0x144>
  801367:	8b 54 24 04          	mov    0x4(%esp),%edx
  80136b:	29 f2                	sub    %esi,%edx
  80136d:	19 cb                	sbb    %ecx,%ebx
  80136f:	89 d8                	mov    %ebx,%eax
  801371:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801375:	d3 e0                	shl    %cl,%eax
  801377:	89 e9                	mov    %ebp,%ecx
  801379:	d3 ea                	shr    %cl,%edx
  80137b:	09 d0                	or     %edx,%eax
  80137d:	89 e9                	mov    %ebp,%ecx
  80137f:	d3 eb                	shr    %cl,%ebx
  801381:	89 da                	mov    %ebx,%edx
  801383:	83 c4 1c             	add    $0x1c,%esp
  801386:	5b                   	pop    %ebx
  801387:	5e                   	pop    %esi
  801388:	5f                   	pop    %edi
  801389:	5d                   	pop    %ebp
  80138a:	c3                   	ret    
  80138b:	90                   	nop
  80138c:	89 fd                	mov    %edi,%ebp
  80138e:	85 ff                	test   %edi,%edi
  801390:	75 0b                	jne    80139d <__umoddi3+0xe9>
  801392:	b8 01 00 00 00       	mov    $0x1,%eax
  801397:	31 d2                	xor    %edx,%edx
  801399:	f7 f7                	div    %edi
  80139b:	89 c5                	mov    %eax,%ebp
  80139d:	89 f0                	mov    %esi,%eax
  80139f:	31 d2                	xor    %edx,%edx
  8013a1:	f7 f5                	div    %ebp
  8013a3:	89 c8                	mov    %ecx,%eax
  8013a5:	f7 f5                	div    %ebp
  8013a7:	89 d0                	mov    %edx,%eax
  8013a9:	e9 44 ff ff ff       	jmp    8012f2 <__umoddi3+0x3e>
  8013ae:	66 90                	xchg   %ax,%ax
  8013b0:	89 c8                	mov    %ecx,%eax
  8013b2:	89 f2                	mov    %esi,%edx
  8013b4:	83 c4 1c             	add    $0x1c,%esp
  8013b7:	5b                   	pop    %ebx
  8013b8:	5e                   	pop    %esi
  8013b9:	5f                   	pop    %edi
  8013ba:	5d                   	pop    %ebp
  8013bb:	c3                   	ret    
  8013bc:	3b 04 24             	cmp    (%esp),%eax
  8013bf:	72 06                	jb     8013c7 <__umoddi3+0x113>
  8013c1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8013c5:	77 0f                	ja     8013d6 <__umoddi3+0x122>
  8013c7:	89 f2                	mov    %esi,%edx
  8013c9:	29 f9                	sub    %edi,%ecx
  8013cb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8013cf:	89 14 24             	mov    %edx,(%esp)
  8013d2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8013d6:	8b 44 24 04          	mov    0x4(%esp),%eax
  8013da:	8b 14 24             	mov    (%esp),%edx
  8013dd:	83 c4 1c             	add    $0x1c,%esp
  8013e0:	5b                   	pop    %ebx
  8013e1:	5e                   	pop    %esi
  8013e2:	5f                   	pop    %edi
  8013e3:	5d                   	pop    %ebp
  8013e4:	c3                   	ret    
  8013e5:	8d 76 00             	lea    0x0(%esi),%esi
  8013e8:	2b 04 24             	sub    (%esp),%eax
  8013eb:	19 fa                	sbb    %edi,%edx
  8013ed:	89 d1                	mov    %edx,%ecx
  8013ef:	89 c6                	mov    %eax,%esi
  8013f1:	e9 71 ff ff ff       	jmp    801367 <__umoddi3+0xb3>
  8013f6:	66 90                	xchg   %ax,%ax
  8013f8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8013fc:	72 ea                	jb     8013e8 <__umoddi3+0x134>
  8013fe:	89 d9                	mov    %ebx,%ecx
  801400:	e9 62 ff ff ff       	jmp    801367 <__umoddi3+0xb3>
