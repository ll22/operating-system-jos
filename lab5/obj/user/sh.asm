
obj/user/sh.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 7d 09 00 00       	call   8009ae <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <_gettoken>:
#define WHITESPACE " \t\r\n"
#define SYMBOLS "<|>&;()"

int
_gettoken(char *s, char **p1, char **p2)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 0c             	sub    $0xc,%esp
  80003c:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80003f:	8b 7d 0c             	mov    0xc(%ebp),%edi
	int t;

	if (s == 0) {
  800042:	85 db                	test   %ebx,%ebx
  800044:	75 27                	jne    80006d <_gettoken+0x3a>
		if (debug > 1)
  800046:	83 3d 00 50 80 00 01 	cmpl   $0x1,0x805000
  80004d:	0f 8e 2c 01 00 00    	jle    80017f <_gettoken+0x14c>
			cprintf("GETTOKEN NULL\n");
  800053:	83 ec 0c             	sub    $0xc,%esp
  800056:	68 20 32 80 00       	push   $0x803220
  80005b:	e8 87 0a 00 00       	call   800ae7 <cprintf>
  800060:	83 c4 10             	add    $0x10,%esp
		return 0;
  800063:	b8 00 00 00 00       	mov    $0x0,%eax
  800068:	e9 29 01 00 00       	jmp    800196 <_gettoken+0x163>
	}

	if (debug > 1)
  80006d:	83 3d 00 50 80 00 01 	cmpl   $0x1,0x805000
  800074:	7e 11                	jle    800087 <_gettoken+0x54>
		cprintf("GETTOKEN: %s\n", s);
  800076:	83 ec 08             	sub    $0x8,%esp
  800079:	53                   	push   %ebx
  80007a:	68 2f 32 80 00       	push   $0x80322f
  80007f:	e8 63 0a 00 00       	call   800ae7 <cprintf>
  800084:	83 c4 10             	add    $0x10,%esp

	*p1 = 0;
  800087:	c7 07 00 00 00 00    	movl   $0x0,(%edi)
	*p2 = 0;
  80008d:	8b 45 10             	mov    0x10(%ebp),%eax
  800090:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

	while (strchr(WHITESPACE, *s))
  800096:	eb 05                	jmp    80009d <_gettoken+0x6a>
		*s++ = 0;
  800098:	43                   	inc    %ebx
  800099:	c6 43 ff 00          	movb   $0x0,-0x1(%ebx)
		cprintf("GETTOKEN: %s\n", s);

	*p1 = 0;
	*p2 = 0;

	while (strchr(WHITESPACE, *s))
  80009d:	83 ec 08             	sub    $0x8,%esp
  8000a0:	0f be 03             	movsbl (%ebx),%eax
  8000a3:	50                   	push   %eax
  8000a4:	68 3d 32 80 00       	push   $0x80323d
  8000a9:	e8 96 11 00 00       	call   801244 <strchr>
  8000ae:	83 c4 10             	add    $0x10,%esp
  8000b1:	85 c0                	test   %eax,%eax
  8000b3:	75 e3                	jne    800098 <_gettoken+0x65>
		*s++ = 0;
	if (*s == 0) {
  8000b5:	8a 03                	mov    (%ebx),%al
  8000b7:	84 c0                	test   %al,%al
  8000b9:	75 27                	jne    8000e2 <_gettoken+0xaf>
		if (debug > 1)
  8000bb:	83 3d 00 50 80 00 01 	cmpl   $0x1,0x805000
  8000c2:	0f 8e be 00 00 00    	jle    800186 <_gettoken+0x153>
			cprintf("EOL\n");
  8000c8:	83 ec 0c             	sub    $0xc,%esp
  8000cb:	68 42 32 80 00       	push   $0x803242
  8000d0:	e8 12 0a 00 00       	call   800ae7 <cprintf>
  8000d5:	83 c4 10             	add    $0x10,%esp
		return 0;
  8000d8:	b8 00 00 00 00       	mov    $0x0,%eax
  8000dd:	e9 b4 00 00 00       	jmp    800196 <_gettoken+0x163>
	}
	if (strchr(SYMBOLS, *s)) {
  8000e2:	83 ec 08             	sub    $0x8,%esp
  8000e5:	0f be c0             	movsbl %al,%eax
  8000e8:	50                   	push   %eax
  8000e9:	68 53 32 80 00       	push   $0x803253
  8000ee:	e8 51 11 00 00       	call   801244 <strchr>
  8000f3:	83 c4 10             	add    $0x10,%esp
  8000f6:	85 c0                	test   %eax,%eax
  8000f8:	74 2c                	je     800126 <_gettoken+0xf3>
		t = *s;
  8000fa:	0f be 33             	movsbl (%ebx),%esi
		*p1 = s;
  8000fd:	89 1f                	mov    %ebx,(%edi)
		*s++ = 0;
  8000ff:	c6 03 00             	movb   $0x0,(%ebx)
		*p2 = s;
  800102:	43                   	inc    %ebx
  800103:	8b 45 10             	mov    0x10(%ebp),%eax
  800106:	89 18                	mov    %ebx,(%eax)
		if (debug > 1)
  800108:	83 3d 00 50 80 00 01 	cmpl   $0x1,0x805000
  80010f:	7e 7c                	jle    80018d <_gettoken+0x15a>
			cprintf("TOK %c\n", t);
  800111:	83 ec 08             	sub    $0x8,%esp
  800114:	56                   	push   %esi
  800115:	68 47 32 80 00       	push   $0x803247
  80011a:	e8 c8 09 00 00       	call   800ae7 <cprintf>
  80011f:	83 c4 10             	add    $0x10,%esp
		return t;
  800122:	89 f0                	mov    %esi,%eax
  800124:	eb 70                	jmp    800196 <_gettoken+0x163>
	}
	*p1 = s;
  800126:	89 1f                	mov    %ebx,(%edi)
	while (*s && !strchr(WHITESPACE SYMBOLS, *s))
  800128:	eb 01                	jmp    80012b <_gettoken+0xf8>
		s++;
  80012a:	43                   	inc    %ebx
		if (debug > 1)
			cprintf("TOK %c\n", t);
		return t;
	}
	*p1 = s;
	while (*s && !strchr(WHITESPACE SYMBOLS, *s))
  80012b:	8a 03                	mov    (%ebx),%al
  80012d:	84 c0                	test   %al,%al
  80012f:	74 18                	je     800149 <_gettoken+0x116>
  800131:	83 ec 08             	sub    $0x8,%esp
  800134:	0f be c0             	movsbl %al,%eax
  800137:	50                   	push   %eax
  800138:	68 4f 32 80 00       	push   $0x80324f
  80013d:	e8 02 11 00 00       	call   801244 <strchr>
  800142:	83 c4 10             	add    $0x10,%esp
  800145:	85 c0                	test   %eax,%eax
  800147:	74 e1                	je     80012a <_gettoken+0xf7>
		s++;
	*p2 = s;
  800149:	8b 45 10             	mov    0x10(%ebp),%eax
  80014c:	89 18                	mov    %ebx,(%eax)
	if (debug > 1) {
  80014e:	83 3d 00 50 80 00 01 	cmpl   $0x1,0x805000
  800155:	7e 3a                	jle    800191 <_gettoken+0x15e>
		t = **p2;
  800157:	0f b6 33             	movzbl (%ebx),%esi
		**p2 = 0;
  80015a:	c6 03 00             	movb   $0x0,(%ebx)
		cprintf("WORD: %s\n", *p1);
  80015d:	83 ec 08             	sub    $0x8,%esp
  800160:	ff 37                	pushl  (%edi)
  800162:	68 5b 32 80 00       	push   $0x80325b
  800167:	e8 7b 09 00 00       	call   800ae7 <cprintf>
		**p2 = t;
  80016c:	8b 45 10             	mov    0x10(%ebp),%eax
  80016f:	8b 00                	mov    (%eax),%eax
  800171:	89 f2                	mov    %esi,%edx
  800173:	88 10                	mov    %dl,(%eax)
  800175:	83 c4 10             	add    $0x10,%esp
	}
	return 'w';
  800178:	b8 77 00 00 00       	mov    $0x77,%eax
  80017d:	eb 17                	jmp    800196 <_gettoken+0x163>
	int t;

	if (s == 0) {
		if (debug > 1)
			cprintf("GETTOKEN NULL\n");
		return 0;
  80017f:	b8 00 00 00 00       	mov    $0x0,%eax
  800184:	eb 10                	jmp    800196 <_gettoken+0x163>
	while (strchr(WHITESPACE, *s))
		*s++ = 0;
	if (*s == 0) {
		if (debug > 1)
			cprintf("EOL\n");
		return 0;
  800186:	b8 00 00 00 00       	mov    $0x0,%eax
  80018b:	eb 09                	jmp    800196 <_gettoken+0x163>
		*p1 = s;
		*s++ = 0;
		*p2 = s;
		if (debug > 1)
			cprintf("TOK %c\n", t);
		return t;
  80018d:	89 f0                	mov    %esi,%eax
  80018f:	eb 05                	jmp    800196 <_gettoken+0x163>
		t = **p2;
		**p2 = 0;
		cprintf("WORD: %s\n", *p1);
		**p2 = t;
	}
	return 'w';
  800191:	b8 77 00 00 00       	mov    $0x77,%eax
}
  800196:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800199:	5b                   	pop    %ebx
  80019a:	5e                   	pop    %esi
  80019b:	5f                   	pop    %edi
  80019c:	5d                   	pop    %ebp
  80019d:	c3                   	ret    

0080019e <gettoken>:

int
gettoken(char *s, char **p1)
{
  80019e:	55                   	push   %ebp
  80019f:	89 e5                	mov    %esp,%ebp
  8001a1:	83 ec 08             	sub    $0x8,%esp
  8001a4:	8b 45 08             	mov    0x8(%ebp),%eax
	static int c, nc;
	static char* np1, *np2;

	if (s) {
  8001a7:	85 c0                	test   %eax,%eax
  8001a9:	74 22                	je     8001cd <gettoken+0x2f>
		nc = _gettoken(s, &np1, &np2);
  8001ab:	83 ec 04             	sub    $0x4,%esp
  8001ae:	68 0c 50 80 00       	push   $0x80500c
  8001b3:	68 10 50 80 00       	push   $0x805010
  8001b8:	50                   	push   %eax
  8001b9:	e8 75 fe ff ff       	call   800033 <_gettoken>
  8001be:	a3 08 50 80 00       	mov    %eax,0x805008
		return 0;
  8001c3:	83 c4 10             	add    $0x10,%esp
  8001c6:	b8 00 00 00 00       	mov    $0x0,%eax
  8001cb:	eb 3a                	jmp    800207 <gettoken+0x69>
	}
	c = nc;
  8001cd:	a1 08 50 80 00       	mov    0x805008,%eax
  8001d2:	a3 04 50 80 00       	mov    %eax,0x805004
	*p1 = np1;
  8001d7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8001da:	8b 15 10 50 80 00    	mov    0x805010,%edx
  8001e0:	89 10                	mov    %edx,(%eax)
	nc = _gettoken(np2, &np1, &np2);
  8001e2:	83 ec 04             	sub    $0x4,%esp
  8001e5:	68 0c 50 80 00       	push   $0x80500c
  8001ea:	68 10 50 80 00       	push   $0x805010
  8001ef:	ff 35 0c 50 80 00    	pushl  0x80500c
  8001f5:	e8 39 fe ff ff       	call   800033 <_gettoken>
  8001fa:	a3 08 50 80 00       	mov    %eax,0x805008
	return c;
  8001ff:	a1 04 50 80 00       	mov    0x805004,%eax
  800204:	83 c4 10             	add    $0x10,%esp
}
  800207:	c9                   	leave  
  800208:	c3                   	ret    

00800209 <runcmd>:
// runcmd() is called in a forked child,
// so it's OK to manipulate file descriptor state.
#define MAXARGS 16
void
runcmd(char* s)
{
  800209:	55                   	push   %ebp
  80020a:	89 e5                	mov    %esp,%ebp
  80020c:	57                   	push   %edi
  80020d:	56                   	push   %esi
  80020e:	53                   	push   %ebx
  80020f:	81 ec 64 04 00 00    	sub    $0x464,%esp
	char *argv[MAXARGS], *t, argv0buf[BUFSIZ];
	int argc, c, i, r, p[2], fd, pipe_child;

	pipe_child = 0;
	gettoken(s, 0);
  800215:	6a 00                	push   $0x0
  800217:	ff 75 08             	pushl  0x8(%ebp)
  80021a:	e8 7f ff ff ff       	call   80019e <gettoken>
  80021f:	83 c4 10             	add    $0x10,%esp

again:
	argc = 0;
	while (1) {
		switch ((c = gettoken(0, &t))) {
  800222:	8d 5d a4             	lea    -0x5c(%ebp),%ebx

	pipe_child = 0;
	gettoken(s, 0);

again:
	argc = 0;
  800225:	be 00 00 00 00       	mov    $0x0,%esi
	while (1) {
		switch ((c = gettoken(0, &t))) {
  80022a:	83 ec 08             	sub    $0x8,%esp
  80022d:	53                   	push   %ebx
  80022e:	6a 00                	push   $0x0
  800230:	e8 69 ff ff ff       	call   80019e <gettoken>
  800235:	83 c4 10             	add    $0x10,%esp
  800238:	83 f8 3e             	cmp    $0x3e,%eax
  80023b:	0f 84 cc 00 00 00    	je     80030d <runcmd+0x104>
  800241:	83 f8 3e             	cmp    $0x3e,%eax
  800244:	7f 12                	jg     800258 <runcmd+0x4f>
  800246:	85 c0                	test   %eax,%eax
  800248:	0f 84 3b 02 00 00    	je     800489 <runcmd+0x280>
  80024e:	83 f8 3c             	cmp    $0x3c,%eax
  800251:	74 3e                	je     800291 <runcmd+0x88>
  800253:	e9 1f 02 00 00       	jmp    800477 <runcmd+0x26e>
  800258:	83 f8 77             	cmp    $0x77,%eax
  80025b:	74 0e                	je     80026b <runcmd+0x62>
  80025d:	83 f8 7c             	cmp    $0x7c,%eax
  800260:	0f 84 25 01 00 00    	je     80038b <runcmd+0x182>
  800266:	e9 0c 02 00 00       	jmp    800477 <runcmd+0x26e>

		case 'w':	// Add an argument
			if (argc == MAXARGS) {
  80026b:	83 fe 10             	cmp    $0x10,%esi
  80026e:	75 15                	jne    800285 <runcmd+0x7c>
				cprintf("too many arguments\n");
  800270:	83 ec 0c             	sub    $0xc,%esp
  800273:	68 65 32 80 00       	push   $0x803265
  800278:	e8 6a 08 00 00       	call   800ae7 <cprintf>
				exit();
  80027d:	e8 7b 07 00 00       	call   8009fd <exit>
  800282:	83 c4 10             	add    $0x10,%esp
			}
			argv[argc++] = t;
  800285:	8b 45 a4             	mov    -0x5c(%ebp),%eax
  800288:	89 44 b5 a8          	mov    %eax,-0x58(%ebp,%esi,4)
  80028c:	8d 76 01             	lea    0x1(%esi),%esi
			break;
  80028f:	eb 99                	jmp    80022a <runcmd+0x21>

		case '<':	// Input redirection
			// Grab the filename from the argument list
			if (gettoken(0, &t) != 'w') {
  800291:	83 ec 08             	sub    $0x8,%esp
  800294:	53                   	push   %ebx
  800295:	6a 00                	push   $0x0
  800297:	e8 02 ff ff ff       	call   80019e <gettoken>
  80029c:	83 c4 10             	add    $0x10,%esp
  80029f:	83 f8 77             	cmp    $0x77,%eax
  8002a2:	74 15                	je     8002b9 <runcmd+0xb0>
				cprintf("syntax error: < not followed by word\n");
  8002a4:	83 ec 0c             	sub    $0xc,%esp
  8002a7:	68 b8 33 80 00       	push   $0x8033b8
  8002ac:	e8 36 08 00 00       	call   800ae7 <cprintf>
				exit();
  8002b1:	e8 47 07 00 00       	call   8009fd <exit>
  8002b6:	83 c4 10             	add    $0x10,%esp
			// then check whether 'fd' is 0.
			// If not, dup 'fd' onto file descriptor 0,
			// then close the original 'fd'.

			// LAB 5: Your code here.
			if ((fd = open(t, O_RDONLY)) < 0) {
  8002b9:	83 ec 08             	sub    $0x8,%esp
  8002bc:	6a 00                	push   $0x0
  8002be:	ff 75 a4             	pushl  -0x5c(%ebp)
  8002c1:	e8 ef 1f 00 00       	call   8022b5 <open>
  8002c6:	89 c7                	mov    %eax,%edi
  8002c8:	83 c4 10             	add    $0x10,%esp
  8002cb:	85 c0                	test   %eax,%eax
  8002cd:	79 1b                	jns    8002ea <runcmd+0xe1>
				cprintf("open %s for read: %e", t, fd);
  8002cf:	83 ec 04             	sub    $0x4,%esp
  8002d2:	50                   	push   %eax
  8002d3:	ff 75 a4             	pushl  -0x5c(%ebp)
  8002d6:	68 79 32 80 00       	push   $0x803279
  8002db:	e8 07 08 00 00       	call   800ae7 <cprintf>
				exit();
  8002e0:	e8 18 07 00 00       	call   8009fd <exit>
  8002e5:	83 c4 10             	add    $0x10,%esp
  8002e8:	eb 08                	jmp    8002f2 <runcmd+0xe9>
			}
			if (fd != 0) {
  8002ea:	85 c0                	test   %eax,%eax
  8002ec:	0f 84 38 ff ff ff    	je     80022a <runcmd+0x21>
				dup(fd, 0);
  8002f2:	83 ec 08             	sub    $0x8,%esp
  8002f5:	6a 00                	push   $0x0
  8002f7:	57                   	push   %edi
  8002f8:	e8 67 1a 00 00       	call   801d64 <dup>
				close(fd);
  8002fd:	89 3c 24             	mov    %edi,(%esp)
  800300:	e8 11 1a 00 00       	call   801d16 <close>
  800305:	83 c4 10             	add    $0x10,%esp
  800308:	e9 1d ff ff ff       	jmp    80022a <runcmd+0x21>
			//panic("< redirection not implemented");
			break;

		case '>':	// Output redirection
			// Grab the filename from the argument list
			if (gettoken(0, &t) != 'w') {
  80030d:	83 ec 08             	sub    $0x8,%esp
  800310:	53                   	push   %ebx
  800311:	6a 00                	push   $0x0
  800313:	e8 86 fe ff ff       	call   80019e <gettoken>
  800318:	83 c4 10             	add    $0x10,%esp
  80031b:	83 f8 77             	cmp    $0x77,%eax
  80031e:	74 15                	je     800335 <runcmd+0x12c>
				cprintf("syntax error: > not followed by word\n");
  800320:	83 ec 0c             	sub    $0xc,%esp
  800323:	68 e0 33 80 00       	push   $0x8033e0
  800328:	e8 ba 07 00 00       	call   800ae7 <cprintf>
				exit();
  80032d:	e8 cb 06 00 00       	call   8009fd <exit>
  800332:	83 c4 10             	add    $0x10,%esp
			}
			if ((fd = open(t, O_WRONLY|O_CREAT|O_TRUNC)) < 0) {
  800335:	83 ec 08             	sub    $0x8,%esp
  800338:	68 01 03 00 00       	push   $0x301
  80033d:	ff 75 a4             	pushl  -0x5c(%ebp)
  800340:	e8 70 1f 00 00       	call   8022b5 <open>
  800345:	89 c7                	mov    %eax,%edi
  800347:	83 c4 10             	add    $0x10,%esp
  80034a:	85 c0                	test   %eax,%eax
  80034c:	79 19                	jns    800367 <runcmd+0x15e>
				cprintf("open %s for write: %e", t, fd);
  80034e:	83 ec 04             	sub    $0x4,%esp
  800351:	50                   	push   %eax
  800352:	ff 75 a4             	pushl  -0x5c(%ebp)
  800355:	68 8e 32 80 00       	push   $0x80328e
  80035a:	e8 88 07 00 00       	call   800ae7 <cprintf>
				exit();
  80035f:	e8 99 06 00 00       	call   8009fd <exit>
  800364:	83 c4 10             	add    $0x10,%esp
			}
			if (fd != 1) {
  800367:	83 ff 01             	cmp    $0x1,%edi
  80036a:	0f 84 ba fe ff ff    	je     80022a <runcmd+0x21>
				dup(fd, 1);
  800370:	83 ec 08             	sub    $0x8,%esp
  800373:	6a 01                	push   $0x1
  800375:	57                   	push   %edi
  800376:	e8 e9 19 00 00       	call   801d64 <dup>
				close(fd);
  80037b:	89 3c 24             	mov    %edi,(%esp)
  80037e:	e8 93 19 00 00       	call   801d16 <close>
  800383:	83 c4 10             	add    $0x10,%esp
  800386:	e9 9f fe ff ff       	jmp    80022a <runcmd+0x21>
			}
			break;

		case '|':	// Pipe
			if ((r = pipe(p)) < 0) {
  80038b:	83 ec 0c             	sub    $0xc,%esp
  80038e:	8d 85 9c fb ff ff    	lea    -0x464(%ebp),%eax
  800394:	50                   	push   %eax
  800395:	e8 74 28 00 00       	call   802c0e <pipe>
  80039a:	83 c4 10             	add    $0x10,%esp
  80039d:	85 c0                	test   %eax,%eax
  80039f:	79 16                	jns    8003b7 <runcmd+0x1ae>
				cprintf("pipe: %e", r);
  8003a1:	83 ec 08             	sub    $0x8,%esp
  8003a4:	50                   	push   %eax
  8003a5:	68 a4 32 80 00       	push   $0x8032a4
  8003aa:	e8 38 07 00 00       	call   800ae7 <cprintf>
				exit();
  8003af:	e8 49 06 00 00       	call   8009fd <exit>
  8003b4:	83 c4 10             	add    $0x10,%esp
			}
			if (debug)
  8003b7:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8003be:	74 1c                	je     8003dc <runcmd+0x1d3>
				cprintf("PIPE: %d %d\n", p[0], p[1]);
  8003c0:	83 ec 04             	sub    $0x4,%esp
  8003c3:	ff b5 a0 fb ff ff    	pushl  -0x460(%ebp)
  8003c9:	ff b5 9c fb ff ff    	pushl  -0x464(%ebp)
  8003cf:	68 ad 32 80 00       	push   $0x8032ad
  8003d4:	e8 0e 07 00 00       	call   800ae7 <cprintf>
  8003d9:	83 c4 10             	add    $0x10,%esp
			if ((r = fork()) < 0) {
  8003dc:	e8 16 14 00 00       	call   8017f7 <fork>
  8003e1:	89 c7                	mov    %eax,%edi
  8003e3:	85 c0                	test   %eax,%eax
  8003e5:	79 16                	jns    8003fd <runcmd+0x1f4>
				cprintf("fork: %e", r);
  8003e7:	83 ec 08             	sub    $0x8,%esp
  8003ea:	50                   	push   %eax
  8003eb:	68 ba 32 80 00       	push   $0x8032ba
  8003f0:	e8 f2 06 00 00       	call   800ae7 <cprintf>
				exit();
  8003f5:	e8 03 06 00 00       	call   8009fd <exit>
  8003fa:	83 c4 10             	add    $0x10,%esp
			}
			if (r == 0) {
  8003fd:	85 ff                	test   %edi,%edi
  8003ff:	75 3c                	jne    80043d <runcmd+0x234>
				if (p[0] != 0) {
  800401:	8b 85 9c fb ff ff    	mov    -0x464(%ebp),%eax
  800407:	85 c0                	test   %eax,%eax
  800409:	74 1c                	je     800427 <runcmd+0x21e>
					dup(p[0], 0);
  80040b:	83 ec 08             	sub    $0x8,%esp
  80040e:	6a 00                	push   $0x0
  800410:	50                   	push   %eax
  800411:	e8 4e 19 00 00       	call   801d64 <dup>
					close(p[0]);
  800416:	83 c4 04             	add    $0x4,%esp
  800419:	ff b5 9c fb ff ff    	pushl  -0x464(%ebp)
  80041f:	e8 f2 18 00 00       	call   801d16 <close>
  800424:	83 c4 10             	add    $0x10,%esp
				}
				close(p[1]);
  800427:	83 ec 0c             	sub    $0xc,%esp
  80042a:	ff b5 a0 fb ff ff    	pushl  -0x460(%ebp)
  800430:	e8 e1 18 00 00       	call   801d16 <close>
				goto again;
  800435:	83 c4 10             	add    $0x10,%esp
  800438:	e9 e8 fd ff ff       	jmp    800225 <runcmd+0x1c>
			} else {
				pipe_child = r;
				if (p[1] != 1) {
  80043d:	8b 85 a0 fb ff ff    	mov    -0x460(%ebp),%eax
  800443:	83 f8 01             	cmp    $0x1,%eax
  800446:	74 1c                	je     800464 <runcmd+0x25b>
					dup(p[1], 1);
  800448:	83 ec 08             	sub    $0x8,%esp
  80044b:	6a 01                	push   $0x1
  80044d:	50                   	push   %eax
  80044e:	e8 11 19 00 00       	call   801d64 <dup>
					close(p[1]);
  800453:	83 c4 04             	add    $0x4,%esp
  800456:	ff b5 a0 fb ff ff    	pushl  -0x460(%ebp)
  80045c:	e8 b5 18 00 00       	call   801d16 <close>
  800461:	83 c4 10             	add    $0x10,%esp
				}
				close(p[0]);
  800464:	83 ec 0c             	sub    $0xc,%esp
  800467:	ff b5 9c fb ff ff    	pushl  -0x464(%ebp)
  80046d:	e8 a4 18 00 00       	call   801d16 <close>
				goto runit;
  800472:	83 c4 10             	add    $0x10,%esp
  800475:	eb 17                	jmp    80048e <runcmd+0x285>
		case 0:		// String is complete
			// Run the current command!
			goto runit;

		default:
			panic("bad return %d from gettoken", c);
  800477:	50                   	push   %eax
  800478:	68 c3 32 80 00       	push   $0x8032c3
  80047d:	6a 79                	push   $0x79
  80047f:	68 df 32 80 00       	push   $0x8032df
  800484:	e8 86 05 00 00       	call   800a0f <_panic>
runcmd(char* s)
{
	char *argv[MAXARGS], *t, argv0buf[BUFSIZ];
	int argc, c, i, r, p[2], fd, pipe_child;

	pipe_child = 0;
  800489:	bf 00 00 00 00       	mov    $0x0,%edi
		}
	}

runit:
	// Return immediately if command line was empty.
	if(argc == 0) {
  80048e:	85 f6                	test   %esi,%esi
  800490:	75 22                	jne    8004b4 <runcmd+0x2ab>
		if (debug)
  800492:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  800499:	0f 84 96 01 00 00    	je     800635 <runcmd+0x42c>
			cprintf("EMPTY COMMAND\n");
  80049f:	83 ec 0c             	sub    $0xc,%esp
  8004a2:	68 e9 32 80 00       	push   $0x8032e9
  8004a7:	e8 3b 06 00 00       	call   800ae7 <cprintf>
  8004ac:	83 c4 10             	add    $0x10,%esp
  8004af:	e9 81 01 00 00       	jmp    800635 <runcmd+0x42c>

	// Clean up command line.
	// Read all commands from the filesystem: add an initial '/' to
	// the command name.
	// This essentially acts like 'PATH=/'.
	if (argv[0][0] != '/') {
  8004b4:	8b 45 a8             	mov    -0x58(%ebp),%eax
  8004b7:	80 38 2f             	cmpb   $0x2f,(%eax)
  8004ba:	74 23                	je     8004df <runcmd+0x2d6>
		argv0buf[0] = '/';
  8004bc:	c6 85 a4 fb ff ff 2f 	movb   $0x2f,-0x45c(%ebp)
		strcpy(argv0buf + 1, argv[0]);
  8004c3:	83 ec 08             	sub    $0x8,%esp
  8004c6:	50                   	push   %eax
  8004c7:	8d 9d a4 fb ff ff    	lea    -0x45c(%ebp),%ebx
  8004cd:	8d 85 a5 fb ff ff    	lea    -0x45b(%ebp),%eax
  8004d3:	50                   	push   %eax
  8004d4:	e8 78 0c 00 00       	call   801151 <strcpy>
		argv[0] = argv0buf;
  8004d9:	89 5d a8             	mov    %ebx,-0x58(%ebp)
  8004dc:	83 c4 10             	add    $0x10,%esp
	}
	argv[argc] = 0;
  8004df:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
  8004e6:	00 

	// Print the command.
	if (debug) {
  8004e7:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8004ee:	74 49                	je     800539 <runcmd+0x330>
		cprintf("[%08x] SPAWN:", thisenv->env_id);
  8004f0:	a1 24 54 80 00       	mov    0x805424,%eax
  8004f5:	8b 40 48             	mov    0x48(%eax),%eax
  8004f8:	83 ec 08             	sub    $0x8,%esp
  8004fb:	50                   	push   %eax
  8004fc:	68 f8 32 80 00       	push   $0x8032f8
  800501:	e8 e1 05 00 00       	call   800ae7 <cprintf>
  800506:	8d 5d a8             	lea    -0x58(%ebp),%ebx
		for (i = 0; argv[i]; i++)
  800509:	83 c4 10             	add    $0x10,%esp
  80050c:	eb 11                	jmp    80051f <runcmd+0x316>
			cprintf(" %s", argv[i]);
  80050e:	83 ec 08             	sub    $0x8,%esp
  800511:	50                   	push   %eax
  800512:	68 80 33 80 00       	push   $0x803380
  800517:	e8 cb 05 00 00       	call   800ae7 <cprintf>
  80051c:	83 c4 10             	add    $0x10,%esp
  80051f:	83 c3 04             	add    $0x4,%ebx
	argv[argc] = 0;

	// Print the command.
	if (debug) {
		cprintf("[%08x] SPAWN:", thisenv->env_id);
		for (i = 0; argv[i]; i++)
  800522:	8b 43 fc             	mov    -0x4(%ebx),%eax
  800525:	85 c0                	test   %eax,%eax
  800527:	75 e5                	jne    80050e <runcmd+0x305>
			cprintf(" %s", argv[i]);
		cprintf("\n");
  800529:	83 ec 0c             	sub    $0xc,%esp
  80052c:	68 40 32 80 00       	push   $0x803240
  800531:	e8 b1 05 00 00       	call   800ae7 <cprintf>
  800536:	83 c4 10             	add    $0x10,%esp
	}

	// Spawn the command!
	if ((r = spawn(argv[0], (const char**) argv)) < 0)
  800539:	83 ec 08             	sub    $0x8,%esp
  80053c:	8d 45 a8             	lea    -0x58(%ebp),%eax
  80053f:	50                   	push   %eax
  800540:	ff 75 a8             	pushl  -0x58(%ebp)
  800543:	e8 1c 1f 00 00       	call   802464 <spawn>
  800548:	89 c3                	mov    %eax,%ebx
  80054a:	83 c4 10             	add    $0x10,%esp
  80054d:	85 c0                	test   %eax,%eax
  80054f:	0f 89 c3 00 00 00    	jns    800618 <runcmd+0x40f>
		cprintf("spawn %s: %e\n", argv[0], r);
  800555:	83 ec 04             	sub    $0x4,%esp
  800558:	50                   	push   %eax
  800559:	ff 75 a8             	pushl  -0x58(%ebp)
  80055c:	68 06 33 80 00       	push   $0x803306
  800561:	e8 81 05 00 00       	call   800ae7 <cprintf>

	// In the parent, close all file descriptors and wait for the
	// spawned command to exit.
	close_all();
  800566:	e8 d6 17 00 00       	call   801d41 <close_all>
  80056b:	83 c4 10             	add    $0x10,%esp
  80056e:	eb 4c                	jmp    8005bc <runcmd+0x3b3>
	if (r >= 0) {
		if (debug)
			cprintf("[%08x] WAIT %s %08x\n", thisenv->env_id, argv[0], r);
  800570:	a1 24 54 80 00       	mov    0x805424,%eax
  800575:	8b 40 48             	mov    0x48(%eax),%eax
  800578:	53                   	push   %ebx
  800579:	ff 75 a8             	pushl  -0x58(%ebp)
  80057c:	50                   	push   %eax
  80057d:	68 14 33 80 00       	push   $0x803314
  800582:	e8 60 05 00 00       	call   800ae7 <cprintf>
  800587:	83 c4 10             	add    $0x10,%esp
		wait(r);
  80058a:	83 ec 0c             	sub    $0xc,%esp
  80058d:	53                   	push   %ebx
  80058e:	e8 fb 27 00 00       	call   802d8e <wait>
		if (debug)
  800593:	83 c4 10             	add    $0x10,%esp
  800596:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  80059d:	0f 84 8c 00 00 00    	je     80062f <runcmd+0x426>
			cprintf("[%08x] wait finished\n", thisenv->env_id);
  8005a3:	a1 24 54 80 00       	mov    0x805424,%eax
  8005a8:	8b 40 48             	mov    0x48(%eax),%eax
  8005ab:	83 ec 08             	sub    $0x8,%esp
  8005ae:	50                   	push   %eax
  8005af:	68 29 33 80 00       	push   $0x803329
  8005b4:	e8 2e 05 00 00       	call   800ae7 <cprintf>
  8005b9:	83 c4 10             	add    $0x10,%esp
	}

	// If we were the left-hand part of a pipe,
	// wait for the right-hand part to finish.
	if (pipe_child) {
  8005bc:	85 ff                	test   %edi,%edi
  8005be:	74 51                	je     800611 <runcmd+0x408>
		if (debug)
  8005c0:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8005c7:	74 1a                	je     8005e3 <runcmd+0x3da>
			cprintf("[%08x] WAIT pipe_child %08x\n", thisenv->env_id, pipe_child);
  8005c9:	a1 24 54 80 00       	mov    0x805424,%eax
  8005ce:	8b 40 48             	mov    0x48(%eax),%eax
  8005d1:	83 ec 04             	sub    $0x4,%esp
  8005d4:	57                   	push   %edi
  8005d5:	50                   	push   %eax
  8005d6:	68 3f 33 80 00       	push   $0x80333f
  8005db:	e8 07 05 00 00       	call   800ae7 <cprintf>
  8005e0:	83 c4 10             	add    $0x10,%esp
		wait(pipe_child);
  8005e3:	83 ec 0c             	sub    $0xc,%esp
  8005e6:	57                   	push   %edi
  8005e7:	e8 a2 27 00 00       	call   802d8e <wait>
		if (debug)
  8005ec:	83 c4 10             	add    $0x10,%esp
  8005ef:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8005f6:	74 19                	je     800611 <runcmd+0x408>
			cprintf("[%08x] wait finished\n", thisenv->env_id);
  8005f8:	a1 24 54 80 00       	mov    0x805424,%eax
  8005fd:	8b 40 48             	mov    0x48(%eax),%eax
  800600:	83 ec 08             	sub    $0x8,%esp
  800603:	50                   	push   %eax
  800604:	68 29 33 80 00       	push   $0x803329
  800609:	e8 d9 04 00 00       	call   800ae7 <cprintf>
  80060e:	83 c4 10             	add    $0x10,%esp
	}

	// Done!
	exit();
  800611:	e8 e7 03 00 00       	call   8009fd <exit>
  800616:	eb 1d                	jmp    800635 <runcmd+0x42c>
	if ((r = spawn(argv[0], (const char**) argv)) < 0)
		cprintf("spawn %s: %e\n", argv[0], r);

	// In the parent, close all file descriptors and wait for the
	// spawned command to exit.
	close_all();
  800618:	e8 24 17 00 00       	call   801d41 <close_all>
	if (r >= 0) {
		if (debug)
  80061d:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  800624:	0f 84 60 ff ff ff    	je     80058a <runcmd+0x381>
  80062a:	e9 41 ff ff ff       	jmp    800570 <runcmd+0x367>
			cprintf("[%08x] wait finished\n", thisenv->env_id);
	}

	// If we were the left-hand part of a pipe,
	// wait for the right-hand part to finish.
	if (pipe_child) {
  80062f:	85 ff                	test   %edi,%edi
  800631:	75 b0                	jne    8005e3 <runcmd+0x3da>
  800633:	eb dc                	jmp    800611 <runcmd+0x408>
			cprintf("[%08x] wait finished\n", thisenv->env_id);
	}

	// Done!
	exit();
}
  800635:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800638:	5b                   	pop    %ebx
  800639:	5e                   	pop    %esi
  80063a:	5f                   	pop    %edi
  80063b:	5d                   	pop    %ebp
  80063c:	c3                   	ret    

0080063d <usage>:
}


void
usage(void)
{
  80063d:	55                   	push   %ebp
  80063e:	89 e5                	mov    %esp,%ebp
  800640:	83 ec 14             	sub    $0x14,%esp
	cprintf("usage: sh [-dix] [command-file]\n");
  800643:	68 08 34 80 00       	push   $0x803408
  800648:	e8 9a 04 00 00       	call   800ae7 <cprintf>
	exit();
  80064d:	e8 ab 03 00 00       	call   8009fd <exit>
}
  800652:	83 c4 10             	add    $0x10,%esp
  800655:	c9                   	leave  
  800656:	c3                   	ret    

00800657 <umain>:

void
umain(int argc, char **argv)
{
  800657:	55                   	push   %ebp
  800658:	89 e5                	mov    %esp,%ebp
  80065a:	57                   	push   %edi
  80065b:	56                   	push   %esi
  80065c:	53                   	push   %ebx
  80065d:	83 ec 30             	sub    $0x30,%esp
  800660:	8b 7d 0c             	mov    0xc(%ebp),%edi
	int r, interactive, echocmds;
	struct Argstate args;

	interactive = '?';
	echocmds = 0;
	argstart(&argc, argv, &args);
  800663:	8d 45 d8             	lea    -0x28(%ebp),%eax
  800666:	50                   	push   %eax
  800667:	57                   	push   %edi
  800668:	8d 45 08             	lea    0x8(%ebp),%eax
  80066b:	50                   	push   %eax
  80066c:	e8 aa 13 00 00       	call   801a1b <argstart>
	while ((r = argnext(&args)) >= 0)
  800671:	83 c4 10             	add    $0x10,%esp
{
	int r, interactive, echocmds;
	struct Argstate args;

	interactive = '?';
	echocmds = 0;
  800674:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
umain(int argc, char **argv)
{
	int r, interactive, echocmds;
	struct Argstate args;

	interactive = '?';
  80067b:	be 3f 00 00 00       	mov    $0x3f,%esi
	echocmds = 0;
	argstart(&argc, argv, &args);
	while ((r = argnext(&args)) >= 0)
  800680:	8d 5d d8             	lea    -0x28(%ebp),%ebx
  800683:	eb 2e                	jmp    8006b3 <umain+0x5c>
		switch (r) {
  800685:	83 f8 69             	cmp    $0x69,%eax
  800688:	74 24                	je     8006ae <umain+0x57>
  80068a:	83 f8 78             	cmp    $0x78,%eax
  80068d:	74 07                	je     800696 <umain+0x3f>
  80068f:	83 f8 64             	cmp    $0x64,%eax
  800692:	75 13                	jne    8006a7 <umain+0x50>
  800694:	eb 09                	jmp    80069f <umain+0x48>
			break;
		case 'i':
			interactive = 1;
			break;
		case 'x':
			echocmds = 1;
  800696:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  80069d:	eb 14                	jmp    8006b3 <umain+0x5c>
	echocmds = 0;
	argstart(&argc, argv, &args);
	while ((r = argnext(&args)) >= 0)
		switch (r) {
		case 'd':
			debug++;
  80069f:	ff 05 00 50 80 00    	incl   0x805000
			break;
  8006a5:	eb 0c                	jmp    8006b3 <umain+0x5c>
			break;
		case 'x':
			echocmds = 1;
			break;
		default:
			usage();
  8006a7:	e8 91 ff ff ff       	call   80063d <usage>
  8006ac:	eb 05                	jmp    8006b3 <umain+0x5c>
		switch (r) {
		case 'd':
			debug++;
			break;
		case 'i':
			interactive = 1;
  8006ae:	be 01 00 00 00       	mov    $0x1,%esi
	struct Argstate args;

	interactive = '?';
	echocmds = 0;
	argstart(&argc, argv, &args);
	while ((r = argnext(&args)) >= 0)
  8006b3:	83 ec 0c             	sub    $0xc,%esp
  8006b6:	53                   	push   %ebx
  8006b7:	e8 98 13 00 00       	call   801a54 <argnext>
  8006bc:	83 c4 10             	add    $0x10,%esp
  8006bf:	85 c0                	test   %eax,%eax
  8006c1:	79 c2                	jns    800685 <umain+0x2e>
			break;
		default:
			usage();
		}

	if (argc > 2)
  8006c3:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
  8006c7:	7e 05                	jle    8006ce <umain+0x77>
		usage();
  8006c9:	e8 6f ff ff ff       	call   80063d <usage>
	if (argc == 2) {
  8006ce:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
  8006d2:	75 56                	jne    80072a <umain+0xd3>
		close(0);
  8006d4:	83 ec 0c             	sub    $0xc,%esp
  8006d7:	6a 00                	push   $0x0
  8006d9:	e8 38 16 00 00       	call   801d16 <close>
		if ((r = open(argv[1], O_RDONLY)) < 0)
  8006de:	83 c4 08             	add    $0x8,%esp
  8006e1:	6a 00                	push   $0x0
  8006e3:	ff 77 04             	pushl  0x4(%edi)
  8006e6:	e8 ca 1b 00 00       	call   8022b5 <open>
  8006eb:	83 c4 10             	add    $0x10,%esp
  8006ee:	85 c0                	test   %eax,%eax
  8006f0:	79 1b                	jns    80070d <umain+0xb6>
			panic("open %s: %e", argv[1], r);
  8006f2:	83 ec 0c             	sub    $0xc,%esp
  8006f5:	50                   	push   %eax
  8006f6:	ff 77 04             	pushl  0x4(%edi)
  8006f9:	68 5c 33 80 00       	push   $0x80335c
  8006fe:	68 29 01 00 00       	push   $0x129
  800703:	68 df 32 80 00       	push   $0x8032df
  800708:	e8 02 03 00 00       	call   800a0f <_panic>
		assert(r == 0);
  80070d:	85 c0                	test   %eax,%eax
  80070f:	74 19                	je     80072a <umain+0xd3>
  800711:	68 68 33 80 00       	push   $0x803368
  800716:	68 6f 33 80 00       	push   $0x80336f
  80071b:	68 2a 01 00 00       	push   $0x12a
  800720:	68 df 32 80 00       	push   $0x8032df
  800725:	e8 e5 02 00 00       	call   800a0f <_panic>
	}
	if (interactive == '?')
  80072a:	83 fe 3f             	cmp    $0x3f,%esi
  80072d:	75 0f                	jne    80073e <umain+0xe7>
		interactive = iscons(0);
  80072f:	83 ec 0c             	sub    $0xc,%esp
  800732:	6a 00                	push   $0x0
  800734:	e8 f7 01 00 00       	call   800930 <iscons>
  800739:	89 c6                	mov    %eax,%esi
  80073b:	83 c4 10             	add    $0x10,%esp
  80073e:	85 f6                	test   %esi,%esi
  800740:	74 07                	je     800749 <umain+0xf2>
  800742:	bf 84 33 80 00       	mov    $0x803384,%edi
  800747:	eb 05                	jmp    80074e <umain+0xf7>
  800749:	bf 00 00 00 00       	mov    $0x0,%edi

	while (1) {
		char *buf;

		buf = readline(interactive ? "$ " : NULL);
  80074e:	83 ec 0c             	sub    $0xc,%esp
  800751:	57                   	push   %edi
  800752:	e8 bf 08 00 00       	call   801016 <readline>
  800757:	89 c3                	mov    %eax,%ebx
		if (buf == NULL) {
  800759:	83 c4 10             	add    $0x10,%esp
  80075c:	85 c0                	test   %eax,%eax
  80075e:	75 1e                	jne    80077e <umain+0x127>
			if (debug)
  800760:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  800767:	74 10                	je     800779 <umain+0x122>
				cprintf("EXITING\n");
  800769:	83 ec 0c             	sub    $0xc,%esp
  80076c:	68 87 33 80 00       	push   $0x803387
  800771:	e8 71 03 00 00       	call   800ae7 <cprintf>
  800776:	83 c4 10             	add    $0x10,%esp
			exit();	// end of file
  800779:	e8 7f 02 00 00       	call   8009fd <exit>
		}
		if (debug)
  80077e:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  800785:	74 11                	je     800798 <umain+0x141>
			cprintf("LINE: %s\n", buf);
  800787:	83 ec 08             	sub    $0x8,%esp
  80078a:	53                   	push   %ebx
  80078b:	68 90 33 80 00       	push   $0x803390
  800790:	e8 52 03 00 00       	call   800ae7 <cprintf>
  800795:	83 c4 10             	add    $0x10,%esp
		if (buf[0] == '#')
  800798:	80 3b 23             	cmpb   $0x23,(%ebx)
  80079b:	74 b1                	je     80074e <umain+0xf7>
			continue;
		if (echocmds)
  80079d:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
  8007a1:	74 11                	je     8007b4 <umain+0x15d>
			printf("# %s\n", buf);
  8007a3:	83 ec 08             	sub    $0x8,%esp
  8007a6:	53                   	push   %ebx
  8007a7:	68 9a 33 80 00       	push   $0x80339a
  8007ac:	e8 9d 1c 00 00       	call   80244e <printf>
  8007b1:	83 c4 10             	add    $0x10,%esp
		if (debug)
  8007b4:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8007bb:	74 10                	je     8007cd <umain+0x176>
			cprintf("BEFORE FORK\n");
  8007bd:	83 ec 0c             	sub    $0xc,%esp
  8007c0:	68 a0 33 80 00       	push   $0x8033a0
  8007c5:	e8 1d 03 00 00       	call   800ae7 <cprintf>
  8007ca:	83 c4 10             	add    $0x10,%esp
		if ((r = fork()) < 0)
  8007cd:	e8 25 10 00 00       	call   8017f7 <fork>
  8007d2:	89 c6                	mov    %eax,%esi
  8007d4:	85 c0                	test   %eax,%eax
  8007d6:	79 15                	jns    8007ed <umain+0x196>
			panic("fork: %e", r);
  8007d8:	50                   	push   %eax
  8007d9:	68 ba 32 80 00       	push   $0x8032ba
  8007de:	68 41 01 00 00       	push   $0x141
  8007e3:	68 df 32 80 00       	push   $0x8032df
  8007e8:	e8 22 02 00 00       	call   800a0f <_panic>
		if (debug)
  8007ed:	83 3d 00 50 80 00 00 	cmpl   $0x0,0x805000
  8007f4:	74 11                	je     800807 <umain+0x1b0>
			cprintf("FORK: %d\n", r);
  8007f6:	83 ec 08             	sub    $0x8,%esp
  8007f9:	50                   	push   %eax
  8007fa:	68 ad 33 80 00       	push   $0x8033ad
  8007ff:	e8 e3 02 00 00       	call   800ae7 <cprintf>
  800804:	83 c4 10             	add    $0x10,%esp
		if (r == 0) {
  800807:	85 f6                	test   %esi,%esi
  800809:	75 16                	jne    800821 <umain+0x1ca>
			runcmd(buf);
  80080b:	83 ec 0c             	sub    $0xc,%esp
  80080e:	53                   	push   %ebx
  80080f:	e8 f5 f9 ff ff       	call   800209 <runcmd>
			exit();
  800814:	e8 e4 01 00 00       	call   8009fd <exit>
  800819:	83 c4 10             	add    $0x10,%esp
  80081c:	e9 2d ff ff ff       	jmp    80074e <umain+0xf7>
		} else
			wait(r);
  800821:	83 ec 0c             	sub    $0xc,%esp
  800824:	56                   	push   %esi
  800825:	e8 64 25 00 00       	call   802d8e <wait>
  80082a:	83 c4 10             	add    $0x10,%esp
  80082d:	e9 1c ff ff ff       	jmp    80074e <umain+0xf7>

00800832 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  800832:	55                   	push   %ebp
  800833:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  800835:	b8 00 00 00 00       	mov    $0x0,%eax
  80083a:	5d                   	pop    %ebp
  80083b:	c3                   	ret    

0080083c <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  80083c:	55                   	push   %ebp
  80083d:	89 e5                	mov    %esp,%ebp
  80083f:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  800842:	68 29 34 80 00       	push   $0x803429
  800847:	ff 75 0c             	pushl  0xc(%ebp)
  80084a:	e8 02 09 00 00       	call   801151 <strcpy>
	return 0;
}
  80084f:	b8 00 00 00 00       	mov    $0x0,%eax
  800854:	c9                   	leave  
  800855:	c3                   	ret    

00800856 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  800856:	55                   	push   %ebp
  800857:	89 e5                	mov    %esp,%ebp
  800859:	57                   	push   %edi
  80085a:	56                   	push   %esi
  80085b:	53                   	push   %ebx
  80085c:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800862:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  800867:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  80086d:	eb 2c                	jmp    80089b <devcons_write+0x45>
		m = n - tot;
  80086f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800872:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  800874:	83 fb 7f             	cmp    $0x7f,%ebx
  800877:	76 05                	jbe    80087e <devcons_write+0x28>
			m = sizeof(buf) - 1;
  800879:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  80087e:	83 ec 04             	sub    $0x4,%esp
  800881:	53                   	push   %ebx
  800882:	03 45 0c             	add    0xc(%ebp),%eax
  800885:	50                   	push   %eax
  800886:	57                   	push   %edi
  800887:	e8 3a 0a 00 00       	call   8012c6 <memmove>
		sys_cputs(buf, m);
  80088c:	83 c4 08             	add    $0x8,%esp
  80088f:	53                   	push   %ebx
  800890:	57                   	push   %edi
  800891:	e8 e3 0b 00 00       	call   801479 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  800896:	01 de                	add    %ebx,%esi
  800898:	83 c4 10             	add    $0x10,%esp
  80089b:	89 f0                	mov    %esi,%eax
  80089d:	3b 75 10             	cmp    0x10(%ebp),%esi
  8008a0:	72 cd                	jb     80086f <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  8008a2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008a5:	5b                   	pop    %ebx
  8008a6:	5e                   	pop    %esi
  8008a7:	5f                   	pop    %edi
  8008a8:	5d                   	pop    %ebp
  8008a9:	c3                   	ret    

008008aa <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  8008aa:	55                   	push   %ebp
  8008ab:	89 e5                	mov    %esp,%ebp
  8008ad:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  8008b0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8008b4:	75 07                	jne    8008bd <devcons_read+0x13>
  8008b6:	eb 23                	jmp    8008db <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  8008b8:	e8 59 0c 00 00       	call   801516 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  8008bd:	e8 d5 0b 00 00       	call   801497 <sys_cgetc>
  8008c2:	85 c0                	test   %eax,%eax
  8008c4:	74 f2                	je     8008b8 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  8008c6:	85 c0                	test   %eax,%eax
  8008c8:	78 1d                	js     8008e7 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  8008ca:	83 f8 04             	cmp    $0x4,%eax
  8008cd:	74 13                	je     8008e2 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  8008cf:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008d2:	88 02                	mov    %al,(%edx)
	return 1;
  8008d4:	b8 01 00 00 00       	mov    $0x1,%eax
  8008d9:	eb 0c                	jmp    8008e7 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  8008db:	b8 00 00 00 00       	mov    $0x0,%eax
  8008e0:	eb 05                	jmp    8008e7 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  8008e2:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  8008e7:	c9                   	leave  
  8008e8:	c3                   	ret    

008008e9 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  8008e9:	55                   	push   %ebp
  8008ea:	89 e5                	mov    %esp,%ebp
  8008ec:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  8008ef:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f2:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  8008f5:	6a 01                	push   $0x1
  8008f7:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8008fa:	50                   	push   %eax
  8008fb:	e8 79 0b 00 00       	call   801479 <sys_cputs>
}
  800900:	83 c4 10             	add    $0x10,%esp
  800903:	c9                   	leave  
  800904:	c3                   	ret    

00800905 <getchar>:

int
getchar(void)
{
  800905:	55                   	push   %ebp
  800906:	89 e5                	mov    %esp,%ebp
  800908:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  80090b:	6a 01                	push   $0x1
  80090d:	8d 45 f7             	lea    -0x9(%ebp),%eax
  800910:	50                   	push   %eax
  800911:	6a 00                	push   $0x0
  800913:	e8 36 15 00 00       	call   801e4e <read>
	if (r < 0)
  800918:	83 c4 10             	add    $0x10,%esp
  80091b:	85 c0                	test   %eax,%eax
  80091d:	78 0f                	js     80092e <getchar+0x29>
		return r;
	if (r < 1)
  80091f:	85 c0                	test   %eax,%eax
  800921:	7e 06                	jle    800929 <getchar+0x24>
		return -E_EOF;
	return c;
  800923:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  800927:	eb 05                	jmp    80092e <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  800929:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  80092e:	c9                   	leave  
  80092f:	c3                   	ret    

00800930 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  800930:	55                   	push   %ebp
  800931:	89 e5                	mov    %esp,%ebp
  800933:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800936:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800939:	50                   	push   %eax
  80093a:	ff 75 08             	pushl  0x8(%ebp)
  80093d:	e8 a6 12 00 00       	call   801be8 <fd_lookup>
  800942:	83 c4 10             	add    $0x10,%esp
  800945:	85 c0                	test   %eax,%eax
  800947:	78 11                	js     80095a <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  800949:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80094c:	8b 15 00 40 80 00    	mov    0x804000,%edx
  800952:	39 10                	cmp    %edx,(%eax)
  800954:	0f 94 c0             	sete   %al
  800957:	0f b6 c0             	movzbl %al,%eax
}
  80095a:	c9                   	leave  
  80095b:	c3                   	ret    

0080095c <opencons>:

int
opencons(void)
{
  80095c:	55                   	push   %ebp
  80095d:	89 e5                	mov    %esp,%ebp
  80095f:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  800962:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800965:	50                   	push   %eax
  800966:	e8 2e 12 00 00       	call   801b99 <fd_alloc>
  80096b:	83 c4 10             	add    $0x10,%esp
  80096e:	85 c0                	test   %eax,%eax
  800970:	78 3a                	js     8009ac <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  800972:	83 ec 04             	sub    $0x4,%esp
  800975:	68 07 04 00 00       	push   $0x407
  80097a:	ff 75 f4             	pushl  -0xc(%ebp)
  80097d:	6a 00                	push   $0x0
  80097f:	e8 b1 0b 00 00       	call   801535 <sys_page_alloc>
  800984:	83 c4 10             	add    $0x10,%esp
  800987:	85 c0                	test   %eax,%eax
  800989:	78 21                	js     8009ac <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  80098b:	8b 15 00 40 80 00    	mov    0x804000,%edx
  800991:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800994:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  800996:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800999:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  8009a0:	83 ec 0c             	sub    $0xc,%esp
  8009a3:	50                   	push   %eax
  8009a4:	e8 c9 11 00 00       	call   801b72 <fd2num>
  8009a9:	83 c4 10             	add    $0x10,%esp
}
  8009ac:	c9                   	leave  
  8009ad:	c3                   	ret    

008009ae <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8009ae:	55                   	push   %ebp
  8009af:	89 e5                	mov    %esp,%ebp
  8009b1:	56                   	push   %esi
  8009b2:	53                   	push   %ebx
  8009b3:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8009b6:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8009b9:	e8 39 0b 00 00       	call   8014f7 <sys_getenvid>
  8009be:	25 ff 03 00 00       	and    $0x3ff,%eax
  8009c3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8009ca:	c1 e0 07             	shl    $0x7,%eax
  8009cd:	29 d0                	sub    %edx,%eax
  8009cf:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8009d4:	a3 24 54 80 00       	mov    %eax,0x805424
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8009d9:	85 db                	test   %ebx,%ebx
  8009db:	7e 07                	jle    8009e4 <libmain+0x36>
		binaryname = argv[0];
  8009dd:	8b 06                	mov    (%esi),%eax
  8009df:	a3 1c 40 80 00       	mov    %eax,0x80401c

	// call user main routine
	umain(argc, argv);
  8009e4:	83 ec 08             	sub    $0x8,%esp
  8009e7:	56                   	push   %esi
  8009e8:	53                   	push   %ebx
  8009e9:	e8 69 fc ff ff       	call   800657 <umain>

	// exit gracefully
	exit();
  8009ee:	e8 0a 00 00 00       	call   8009fd <exit>
}
  8009f3:	83 c4 10             	add    $0x10,%esp
  8009f6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8009f9:	5b                   	pop    %ebx
  8009fa:	5e                   	pop    %esi
  8009fb:	5d                   	pop    %ebp
  8009fc:	c3                   	ret    

008009fd <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8009fd:	55                   	push   %ebp
  8009fe:	89 e5                	mov    %esp,%ebp
  800a00:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800a03:	6a 00                	push   $0x0
  800a05:	e8 ac 0a 00 00       	call   8014b6 <sys_env_destroy>
}
  800a0a:	83 c4 10             	add    $0x10,%esp
  800a0d:	c9                   	leave  
  800a0e:	c3                   	ret    

00800a0f <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a0f:	55                   	push   %ebp
  800a10:	89 e5                	mov    %esp,%ebp
  800a12:	56                   	push   %esi
  800a13:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a14:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a17:	8b 35 1c 40 80 00    	mov    0x80401c,%esi
  800a1d:	e8 d5 0a 00 00       	call   8014f7 <sys_getenvid>
  800a22:	83 ec 0c             	sub    $0xc,%esp
  800a25:	ff 75 0c             	pushl  0xc(%ebp)
  800a28:	ff 75 08             	pushl  0x8(%ebp)
  800a2b:	56                   	push   %esi
  800a2c:	50                   	push   %eax
  800a2d:	68 40 34 80 00       	push   $0x803440
  800a32:	e8 b0 00 00 00       	call   800ae7 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800a37:	83 c4 18             	add    $0x18,%esp
  800a3a:	53                   	push   %ebx
  800a3b:	ff 75 10             	pushl  0x10(%ebp)
  800a3e:	e8 53 00 00 00       	call   800a96 <vcprintf>
	cprintf("\n");
  800a43:	c7 04 24 40 32 80 00 	movl   $0x803240,(%esp)
  800a4a:	e8 98 00 00 00       	call   800ae7 <cprintf>
  800a4f:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800a52:	cc                   	int3   
  800a53:	eb fd                	jmp    800a52 <_panic+0x43>

00800a55 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800a55:	55                   	push   %ebp
  800a56:	89 e5                	mov    %esp,%ebp
  800a58:	53                   	push   %ebx
  800a59:	83 ec 04             	sub    $0x4,%esp
  800a5c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800a5f:	8b 13                	mov    (%ebx),%edx
  800a61:	8d 42 01             	lea    0x1(%edx),%eax
  800a64:	89 03                	mov    %eax,(%ebx)
  800a66:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a69:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800a6d:	3d ff 00 00 00       	cmp    $0xff,%eax
  800a72:	75 1a                	jne    800a8e <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800a74:	83 ec 08             	sub    $0x8,%esp
  800a77:	68 ff 00 00 00       	push   $0xff
  800a7c:	8d 43 08             	lea    0x8(%ebx),%eax
  800a7f:	50                   	push   %eax
  800a80:	e8 f4 09 00 00       	call   801479 <sys_cputs>
		b->idx = 0;
  800a85:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800a8b:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800a8e:	ff 43 04             	incl   0x4(%ebx)
}
  800a91:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800a94:	c9                   	leave  
  800a95:	c3                   	ret    

00800a96 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800a96:	55                   	push   %ebp
  800a97:	89 e5                	mov    %esp,%ebp
  800a99:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800a9f:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800aa6:	00 00 00 
	b.cnt = 0;
  800aa9:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800ab0:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800ab3:	ff 75 0c             	pushl  0xc(%ebp)
  800ab6:	ff 75 08             	pushl  0x8(%ebp)
  800ab9:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800abf:	50                   	push   %eax
  800ac0:	68 55 0a 80 00       	push   $0x800a55
  800ac5:	e8 51 01 00 00       	call   800c1b <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800aca:	83 c4 08             	add    $0x8,%esp
  800acd:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800ad3:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800ad9:	50                   	push   %eax
  800ada:	e8 9a 09 00 00       	call   801479 <sys_cputs>

	return b.cnt;
}
  800adf:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800ae5:	c9                   	leave  
  800ae6:	c3                   	ret    

00800ae7 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800ae7:	55                   	push   %ebp
  800ae8:	89 e5                	mov    %esp,%ebp
  800aea:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800aed:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800af0:	50                   	push   %eax
  800af1:	ff 75 08             	pushl  0x8(%ebp)
  800af4:	e8 9d ff ff ff       	call   800a96 <vcprintf>
	va_end(ap);

	return cnt;
}
  800af9:	c9                   	leave  
  800afa:	c3                   	ret    

00800afb <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800afb:	55                   	push   %ebp
  800afc:	89 e5                	mov    %esp,%ebp
  800afe:	57                   	push   %edi
  800aff:	56                   	push   %esi
  800b00:	53                   	push   %ebx
  800b01:	83 ec 1c             	sub    $0x1c,%esp
  800b04:	89 c7                	mov    %eax,%edi
  800b06:	89 d6                	mov    %edx,%esi
  800b08:	8b 45 08             	mov    0x8(%ebp),%eax
  800b0b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b0e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800b11:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800b14:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800b17:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b1c:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800b1f:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800b22:	39 d3                	cmp    %edx,%ebx
  800b24:	72 05                	jb     800b2b <printnum+0x30>
  800b26:	39 45 10             	cmp    %eax,0x10(%ebp)
  800b29:	77 45                	ja     800b70 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800b2b:	83 ec 0c             	sub    $0xc,%esp
  800b2e:	ff 75 18             	pushl  0x18(%ebp)
  800b31:	8b 45 14             	mov    0x14(%ebp),%eax
  800b34:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800b37:	53                   	push   %ebx
  800b38:	ff 75 10             	pushl  0x10(%ebp)
  800b3b:	83 ec 08             	sub    $0x8,%esp
  800b3e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800b41:	ff 75 e0             	pushl  -0x20(%ebp)
  800b44:	ff 75 dc             	pushl  -0x24(%ebp)
  800b47:	ff 75 d8             	pushl  -0x28(%ebp)
  800b4a:	e8 5d 24 00 00       	call   802fac <__udivdi3>
  800b4f:	83 c4 18             	add    $0x18,%esp
  800b52:	52                   	push   %edx
  800b53:	50                   	push   %eax
  800b54:	89 f2                	mov    %esi,%edx
  800b56:	89 f8                	mov    %edi,%eax
  800b58:	e8 9e ff ff ff       	call   800afb <printnum>
  800b5d:	83 c4 20             	add    $0x20,%esp
  800b60:	eb 16                	jmp    800b78 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800b62:	83 ec 08             	sub    $0x8,%esp
  800b65:	56                   	push   %esi
  800b66:	ff 75 18             	pushl  0x18(%ebp)
  800b69:	ff d7                	call   *%edi
  800b6b:	83 c4 10             	add    $0x10,%esp
  800b6e:	eb 03                	jmp    800b73 <printnum+0x78>
  800b70:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800b73:	4b                   	dec    %ebx
  800b74:	85 db                	test   %ebx,%ebx
  800b76:	7f ea                	jg     800b62 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800b78:	83 ec 08             	sub    $0x8,%esp
  800b7b:	56                   	push   %esi
  800b7c:	83 ec 04             	sub    $0x4,%esp
  800b7f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800b82:	ff 75 e0             	pushl  -0x20(%ebp)
  800b85:	ff 75 dc             	pushl  -0x24(%ebp)
  800b88:	ff 75 d8             	pushl  -0x28(%ebp)
  800b8b:	e8 2c 25 00 00       	call   8030bc <__umoddi3>
  800b90:	83 c4 14             	add    $0x14,%esp
  800b93:	0f be 80 63 34 80 00 	movsbl 0x803463(%eax),%eax
  800b9a:	50                   	push   %eax
  800b9b:	ff d7                	call   *%edi
}
  800b9d:	83 c4 10             	add    $0x10,%esp
  800ba0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ba3:	5b                   	pop    %ebx
  800ba4:	5e                   	pop    %esi
  800ba5:	5f                   	pop    %edi
  800ba6:	5d                   	pop    %ebp
  800ba7:	c3                   	ret    

00800ba8 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800ba8:	55                   	push   %ebp
  800ba9:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800bab:	83 fa 01             	cmp    $0x1,%edx
  800bae:	7e 0e                	jle    800bbe <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800bb0:	8b 10                	mov    (%eax),%edx
  800bb2:	8d 4a 08             	lea    0x8(%edx),%ecx
  800bb5:	89 08                	mov    %ecx,(%eax)
  800bb7:	8b 02                	mov    (%edx),%eax
  800bb9:	8b 52 04             	mov    0x4(%edx),%edx
  800bbc:	eb 22                	jmp    800be0 <getuint+0x38>
	else if (lflag)
  800bbe:	85 d2                	test   %edx,%edx
  800bc0:	74 10                	je     800bd2 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800bc2:	8b 10                	mov    (%eax),%edx
  800bc4:	8d 4a 04             	lea    0x4(%edx),%ecx
  800bc7:	89 08                	mov    %ecx,(%eax)
  800bc9:	8b 02                	mov    (%edx),%eax
  800bcb:	ba 00 00 00 00       	mov    $0x0,%edx
  800bd0:	eb 0e                	jmp    800be0 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800bd2:	8b 10                	mov    (%eax),%edx
  800bd4:	8d 4a 04             	lea    0x4(%edx),%ecx
  800bd7:	89 08                	mov    %ecx,(%eax)
  800bd9:	8b 02                	mov    (%edx),%eax
  800bdb:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800be0:	5d                   	pop    %ebp
  800be1:	c3                   	ret    

00800be2 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800be2:	55                   	push   %ebp
  800be3:	89 e5                	mov    %esp,%ebp
  800be5:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800be8:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800beb:	8b 10                	mov    (%eax),%edx
  800bed:	3b 50 04             	cmp    0x4(%eax),%edx
  800bf0:	73 0a                	jae    800bfc <sprintputch+0x1a>
		*b->buf++ = ch;
  800bf2:	8d 4a 01             	lea    0x1(%edx),%ecx
  800bf5:	89 08                	mov    %ecx,(%eax)
  800bf7:	8b 45 08             	mov    0x8(%ebp),%eax
  800bfa:	88 02                	mov    %al,(%edx)
}
  800bfc:	5d                   	pop    %ebp
  800bfd:	c3                   	ret    

00800bfe <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800bfe:	55                   	push   %ebp
  800bff:	89 e5                	mov    %esp,%ebp
  800c01:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800c04:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800c07:	50                   	push   %eax
  800c08:	ff 75 10             	pushl  0x10(%ebp)
  800c0b:	ff 75 0c             	pushl  0xc(%ebp)
  800c0e:	ff 75 08             	pushl  0x8(%ebp)
  800c11:	e8 05 00 00 00       	call   800c1b <vprintfmt>
	va_end(ap);
}
  800c16:	83 c4 10             	add    $0x10,%esp
  800c19:	c9                   	leave  
  800c1a:	c3                   	ret    

00800c1b <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800c1b:	55                   	push   %ebp
  800c1c:	89 e5                	mov    %esp,%ebp
  800c1e:	57                   	push   %edi
  800c1f:	56                   	push   %esi
  800c20:	53                   	push   %ebx
  800c21:	83 ec 2c             	sub    $0x2c,%esp
  800c24:	8b 75 08             	mov    0x8(%ebp),%esi
  800c27:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800c2a:	8b 7d 10             	mov    0x10(%ebp),%edi
  800c2d:	eb 12                	jmp    800c41 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800c2f:	85 c0                	test   %eax,%eax
  800c31:	0f 84 68 03 00 00    	je     800f9f <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800c37:	83 ec 08             	sub    $0x8,%esp
  800c3a:	53                   	push   %ebx
  800c3b:	50                   	push   %eax
  800c3c:	ff d6                	call   *%esi
  800c3e:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800c41:	47                   	inc    %edi
  800c42:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800c46:	83 f8 25             	cmp    $0x25,%eax
  800c49:	75 e4                	jne    800c2f <vprintfmt+0x14>
  800c4b:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800c4f:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800c56:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800c5d:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800c64:	ba 00 00 00 00       	mov    $0x0,%edx
  800c69:	eb 07                	jmp    800c72 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800c6b:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800c6e:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800c72:	8d 47 01             	lea    0x1(%edi),%eax
  800c75:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800c78:	0f b6 0f             	movzbl (%edi),%ecx
  800c7b:	8a 07                	mov    (%edi),%al
  800c7d:	83 e8 23             	sub    $0x23,%eax
  800c80:	3c 55                	cmp    $0x55,%al
  800c82:	0f 87 fe 02 00 00    	ja     800f86 <vprintfmt+0x36b>
  800c88:	0f b6 c0             	movzbl %al,%eax
  800c8b:	ff 24 85 a0 35 80 00 	jmp    *0x8035a0(,%eax,4)
  800c92:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800c95:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800c99:	eb d7                	jmp    800c72 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800c9b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800c9e:	b8 00 00 00 00       	mov    $0x0,%eax
  800ca3:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800ca6:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800ca9:	01 c0                	add    %eax,%eax
  800cab:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800caf:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800cb2:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800cb5:	83 fa 09             	cmp    $0x9,%edx
  800cb8:	77 34                	ja     800cee <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800cba:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800cbb:	eb e9                	jmp    800ca6 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800cbd:	8b 45 14             	mov    0x14(%ebp),%eax
  800cc0:	8d 48 04             	lea    0x4(%eax),%ecx
  800cc3:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800cc6:	8b 00                	mov    (%eax),%eax
  800cc8:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800ccb:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800cce:	eb 24                	jmp    800cf4 <vprintfmt+0xd9>
  800cd0:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800cd4:	79 07                	jns    800cdd <vprintfmt+0xc2>
  800cd6:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800cdd:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800ce0:	eb 90                	jmp    800c72 <vprintfmt+0x57>
  800ce2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800ce5:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800cec:	eb 84                	jmp    800c72 <vprintfmt+0x57>
  800cee:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800cf1:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800cf4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800cf8:	0f 89 74 ff ff ff    	jns    800c72 <vprintfmt+0x57>
				width = precision, precision = -1;
  800cfe:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800d01:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800d04:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800d0b:	e9 62 ff ff ff       	jmp    800c72 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800d10:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800d11:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800d14:	e9 59 ff ff ff       	jmp    800c72 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800d19:	8b 45 14             	mov    0x14(%ebp),%eax
  800d1c:	8d 50 04             	lea    0x4(%eax),%edx
  800d1f:	89 55 14             	mov    %edx,0x14(%ebp)
  800d22:	83 ec 08             	sub    $0x8,%esp
  800d25:	53                   	push   %ebx
  800d26:	ff 30                	pushl  (%eax)
  800d28:	ff d6                	call   *%esi
			break;
  800d2a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800d2d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800d30:	e9 0c ff ff ff       	jmp    800c41 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800d35:	8b 45 14             	mov    0x14(%ebp),%eax
  800d38:	8d 50 04             	lea    0x4(%eax),%edx
  800d3b:	89 55 14             	mov    %edx,0x14(%ebp)
  800d3e:	8b 00                	mov    (%eax),%eax
  800d40:	85 c0                	test   %eax,%eax
  800d42:	79 02                	jns    800d46 <vprintfmt+0x12b>
  800d44:	f7 d8                	neg    %eax
  800d46:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800d48:	83 f8 0f             	cmp    $0xf,%eax
  800d4b:	7f 0b                	jg     800d58 <vprintfmt+0x13d>
  800d4d:	8b 04 85 00 37 80 00 	mov    0x803700(,%eax,4),%eax
  800d54:	85 c0                	test   %eax,%eax
  800d56:	75 18                	jne    800d70 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800d58:	52                   	push   %edx
  800d59:	68 7b 34 80 00       	push   $0x80347b
  800d5e:	53                   	push   %ebx
  800d5f:	56                   	push   %esi
  800d60:	e8 99 fe ff ff       	call   800bfe <printfmt>
  800d65:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800d68:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800d6b:	e9 d1 fe ff ff       	jmp    800c41 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800d70:	50                   	push   %eax
  800d71:	68 81 33 80 00       	push   $0x803381
  800d76:	53                   	push   %ebx
  800d77:	56                   	push   %esi
  800d78:	e8 81 fe ff ff       	call   800bfe <printfmt>
  800d7d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800d80:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800d83:	e9 b9 fe ff ff       	jmp    800c41 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800d88:	8b 45 14             	mov    0x14(%ebp),%eax
  800d8b:	8d 50 04             	lea    0x4(%eax),%edx
  800d8e:	89 55 14             	mov    %edx,0x14(%ebp)
  800d91:	8b 38                	mov    (%eax),%edi
  800d93:	85 ff                	test   %edi,%edi
  800d95:	75 05                	jne    800d9c <vprintfmt+0x181>
				p = "(null)";
  800d97:	bf 74 34 80 00       	mov    $0x803474,%edi
			if (width > 0 && padc != '-')
  800d9c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800da0:	0f 8e 90 00 00 00    	jle    800e36 <vprintfmt+0x21b>
  800da6:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800daa:	0f 84 8e 00 00 00    	je     800e3e <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800db0:	83 ec 08             	sub    $0x8,%esp
  800db3:	ff 75 d0             	pushl  -0x30(%ebp)
  800db6:	57                   	push   %edi
  800db7:	e8 76 03 00 00       	call   801132 <strnlen>
  800dbc:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800dbf:	29 c1                	sub    %eax,%ecx
  800dc1:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800dc4:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800dc7:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800dcb:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800dce:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800dd1:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800dd3:	eb 0d                	jmp    800de2 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800dd5:	83 ec 08             	sub    $0x8,%esp
  800dd8:	53                   	push   %ebx
  800dd9:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ddc:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800dde:	4f                   	dec    %edi
  800ddf:	83 c4 10             	add    $0x10,%esp
  800de2:	85 ff                	test   %edi,%edi
  800de4:	7f ef                	jg     800dd5 <vprintfmt+0x1ba>
  800de6:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800de9:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800dec:	89 c8                	mov    %ecx,%eax
  800dee:	85 c9                	test   %ecx,%ecx
  800df0:	79 05                	jns    800df7 <vprintfmt+0x1dc>
  800df2:	b8 00 00 00 00       	mov    $0x0,%eax
  800df7:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800dfa:	29 c1                	sub    %eax,%ecx
  800dfc:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800dff:	89 75 08             	mov    %esi,0x8(%ebp)
  800e02:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800e05:	eb 3d                	jmp    800e44 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800e07:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800e0b:	74 19                	je     800e26 <vprintfmt+0x20b>
  800e0d:	0f be c0             	movsbl %al,%eax
  800e10:	83 e8 20             	sub    $0x20,%eax
  800e13:	83 f8 5e             	cmp    $0x5e,%eax
  800e16:	76 0e                	jbe    800e26 <vprintfmt+0x20b>
					putch('?', putdat);
  800e18:	83 ec 08             	sub    $0x8,%esp
  800e1b:	53                   	push   %ebx
  800e1c:	6a 3f                	push   $0x3f
  800e1e:	ff 55 08             	call   *0x8(%ebp)
  800e21:	83 c4 10             	add    $0x10,%esp
  800e24:	eb 0b                	jmp    800e31 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800e26:	83 ec 08             	sub    $0x8,%esp
  800e29:	53                   	push   %ebx
  800e2a:	52                   	push   %edx
  800e2b:	ff 55 08             	call   *0x8(%ebp)
  800e2e:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800e31:	ff 4d e4             	decl   -0x1c(%ebp)
  800e34:	eb 0e                	jmp    800e44 <vprintfmt+0x229>
  800e36:	89 75 08             	mov    %esi,0x8(%ebp)
  800e39:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800e3c:	eb 06                	jmp    800e44 <vprintfmt+0x229>
  800e3e:	89 75 08             	mov    %esi,0x8(%ebp)
  800e41:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800e44:	47                   	inc    %edi
  800e45:	8a 47 ff             	mov    -0x1(%edi),%al
  800e48:	0f be d0             	movsbl %al,%edx
  800e4b:	85 d2                	test   %edx,%edx
  800e4d:	74 1d                	je     800e6c <vprintfmt+0x251>
  800e4f:	85 f6                	test   %esi,%esi
  800e51:	78 b4                	js     800e07 <vprintfmt+0x1ec>
  800e53:	4e                   	dec    %esi
  800e54:	79 b1                	jns    800e07 <vprintfmt+0x1ec>
  800e56:	8b 75 08             	mov    0x8(%ebp),%esi
  800e59:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800e5c:	eb 14                	jmp    800e72 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800e5e:	83 ec 08             	sub    $0x8,%esp
  800e61:	53                   	push   %ebx
  800e62:	6a 20                	push   $0x20
  800e64:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800e66:	4f                   	dec    %edi
  800e67:	83 c4 10             	add    $0x10,%esp
  800e6a:	eb 06                	jmp    800e72 <vprintfmt+0x257>
  800e6c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800e6f:	8b 75 08             	mov    0x8(%ebp),%esi
  800e72:	85 ff                	test   %edi,%edi
  800e74:	7f e8                	jg     800e5e <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800e76:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800e79:	e9 c3 fd ff ff       	jmp    800c41 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800e7e:	83 fa 01             	cmp    $0x1,%edx
  800e81:	7e 16                	jle    800e99 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800e83:	8b 45 14             	mov    0x14(%ebp),%eax
  800e86:	8d 50 08             	lea    0x8(%eax),%edx
  800e89:	89 55 14             	mov    %edx,0x14(%ebp)
  800e8c:	8b 50 04             	mov    0x4(%eax),%edx
  800e8f:	8b 00                	mov    (%eax),%eax
  800e91:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800e94:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800e97:	eb 32                	jmp    800ecb <vprintfmt+0x2b0>
	else if (lflag)
  800e99:	85 d2                	test   %edx,%edx
  800e9b:	74 18                	je     800eb5 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800e9d:	8b 45 14             	mov    0x14(%ebp),%eax
  800ea0:	8d 50 04             	lea    0x4(%eax),%edx
  800ea3:	89 55 14             	mov    %edx,0x14(%ebp)
  800ea6:	8b 00                	mov    (%eax),%eax
  800ea8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800eab:	89 c1                	mov    %eax,%ecx
  800ead:	c1 f9 1f             	sar    $0x1f,%ecx
  800eb0:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800eb3:	eb 16                	jmp    800ecb <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800eb5:	8b 45 14             	mov    0x14(%ebp),%eax
  800eb8:	8d 50 04             	lea    0x4(%eax),%edx
  800ebb:	89 55 14             	mov    %edx,0x14(%ebp)
  800ebe:	8b 00                	mov    (%eax),%eax
  800ec0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800ec3:	89 c1                	mov    %eax,%ecx
  800ec5:	c1 f9 1f             	sar    $0x1f,%ecx
  800ec8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800ecb:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800ece:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800ed1:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800ed5:	79 76                	jns    800f4d <vprintfmt+0x332>
				putch('-', putdat);
  800ed7:	83 ec 08             	sub    $0x8,%esp
  800eda:	53                   	push   %ebx
  800edb:	6a 2d                	push   $0x2d
  800edd:	ff d6                	call   *%esi
				num = -(long long) num;
  800edf:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800ee2:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800ee5:	f7 d8                	neg    %eax
  800ee7:	83 d2 00             	adc    $0x0,%edx
  800eea:	f7 da                	neg    %edx
  800eec:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800eef:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800ef4:	eb 5c                	jmp    800f52 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800ef6:	8d 45 14             	lea    0x14(%ebp),%eax
  800ef9:	e8 aa fc ff ff       	call   800ba8 <getuint>
			base = 10;
  800efe:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800f03:	eb 4d                	jmp    800f52 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800f05:	8d 45 14             	lea    0x14(%ebp),%eax
  800f08:	e8 9b fc ff ff       	call   800ba8 <getuint>
			base = 8;
  800f0d:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800f12:	eb 3e                	jmp    800f52 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800f14:	83 ec 08             	sub    $0x8,%esp
  800f17:	53                   	push   %ebx
  800f18:	6a 30                	push   $0x30
  800f1a:	ff d6                	call   *%esi
			putch('x', putdat);
  800f1c:	83 c4 08             	add    $0x8,%esp
  800f1f:	53                   	push   %ebx
  800f20:	6a 78                	push   $0x78
  800f22:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800f24:	8b 45 14             	mov    0x14(%ebp),%eax
  800f27:	8d 50 04             	lea    0x4(%eax),%edx
  800f2a:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800f2d:	8b 00                	mov    (%eax),%eax
  800f2f:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800f34:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800f37:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800f3c:	eb 14                	jmp    800f52 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800f3e:	8d 45 14             	lea    0x14(%ebp),%eax
  800f41:	e8 62 fc ff ff       	call   800ba8 <getuint>
			base = 16;
  800f46:	b9 10 00 00 00       	mov    $0x10,%ecx
  800f4b:	eb 05                	jmp    800f52 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800f4d:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800f52:	83 ec 0c             	sub    $0xc,%esp
  800f55:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800f59:	57                   	push   %edi
  800f5a:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f5d:	51                   	push   %ecx
  800f5e:	52                   	push   %edx
  800f5f:	50                   	push   %eax
  800f60:	89 da                	mov    %ebx,%edx
  800f62:	89 f0                	mov    %esi,%eax
  800f64:	e8 92 fb ff ff       	call   800afb <printnum>
			break;
  800f69:	83 c4 20             	add    $0x20,%esp
  800f6c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800f6f:	e9 cd fc ff ff       	jmp    800c41 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800f74:	83 ec 08             	sub    $0x8,%esp
  800f77:	53                   	push   %ebx
  800f78:	51                   	push   %ecx
  800f79:	ff d6                	call   *%esi
			break;
  800f7b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800f7e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800f81:	e9 bb fc ff ff       	jmp    800c41 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800f86:	83 ec 08             	sub    $0x8,%esp
  800f89:	53                   	push   %ebx
  800f8a:	6a 25                	push   $0x25
  800f8c:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800f8e:	83 c4 10             	add    $0x10,%esp
  800f91:	eb 01                	jmp    800f94 <vprintfmt+0x379>
  800f93:	4f                   	dec    %edi
  800f94:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800f98:	75 f9                	jne    800f93 <vprintfmt+0x378>
  800f9a:	e9 a2 fc ff ff       	jmp    800c41 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800f9f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fa2:	5b                   	pop    %ebx
  800fa3:	5e                   	pop    %esi
  800fa4:	5f                   	pop    %edi
  800fa5:	5d                   	pop    %ebp
  800fa6:	c3                   	ret    

00800fa7 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800fa7:	55                   	push   %ebp
  800fa8:	89 e5                	mov    %esp,%ebp
  800faa:	83 ec 18             	sub    $0x18,%esp
  800fad:	8b 45 08             	mov    0x8(%ebp),%eax
  800fb0:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800fb3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800fb6:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800fba:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800fbd:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800fc4:	85 c0                	test   %eax,%eax
  800fc6:	74 26                	je     800fee <vsnprintf+0x47>
  800fc8:	85 d2                	test   %edx,%edx
  800fca:	7e 29                	jle    800ff5 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800fcc:	ff 75 14             	pushl  0x14(%ebp)
  800fcf:	ff 75 10             	pushl  0x10(%ebp)
  800fd2:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800fd5:	50                   	push   %eax
  800fd6:	68 e2 0b 80 00       	push   $0x800be2
  800fdb:	e8 3b fc ff ff       	call   800c1b <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800fe0:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800fe3:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800fe6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800fe9:	83 c4 10             	add    $0x10,%esp
  800fec:	eb 0c                	jmp    800ffa <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800fee:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800ff3:	eb 05                	jmp    800ffa <vsnprintf+0x53>
  800ff5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800ffa:	c9                   	leave  
  800ffb:	c3                   	ret    

00800ffc <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800ffc:	55                   	push   %ebp
  800ffd:	89 e5                	mov    %esp,%ebp
  800fff:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  801002:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  801005:	50                   	push   %eax
  801006:	ff 75 10             	pushl  0x10(%ebp)
  801009:	ff 75 0c             	pushl  0xc(%ebp)
  80100c:	ff 75 08             	pushl  0x8(%ebp)
  80100f:	e8 93 ff ff ff       	call   800fa7 <vsnprintf>
	va_end(ap);

	return rc;
}
  801014:	c9                   	leave  
  801015:	c3                   	ret    

00801016 <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
  801016:	55                   	push   %ebp
  801017:	89 e5                	mov    %esp,%ebp
  801019:	57                   	push   %edi
  80101a:	56                   	push   %esi
  80101b:	53                   	push   %ebx
  80101c:	83 ec 0c             	sub    $0xc,%esp
  80101f:	8b 45 08             	mov    0x8(%ebp),%eax

#if JOS_KERNEL
	if (prompt != NULL)
		cprintf("%s", prompt);
#else
	if (prompt != NULL)
  801022:	85 c0                	test   %eax,%eax
  801024:	74 13                	je     801039 <readline+0x23>
		fprintf(1, "%s", prompt);
  801026:	83 ec 04             	sub    $0x4,%esp
  801029:	50                   	push   %eax
  80102a:	68 81 33 80 00       	push   $0x803381
  80102f:	6a 01                	push   $0x1
  801031:	e8 01 14 00 00       	call   802437 <fprintf>
  801036:	83 c4 10             	add    $0x10,%esp
#endif

	i = 0;
	echoing = iscons(0);
  801039:	83 ec 0c             	sub    $0xc,%esp
  80103c:	6a 00                	push   $0x0
  80103e:	e8 ed f8 ff ff       	call   800930 <iscons>
  801043:	89 c7                	mov    %eax,%edi
  801045:	83 c4 10             	add    $0x10,%esp
#else
	if (prompt != NULL)
		fprintf(1, "%s", prompt);
#endif

	i = 0;
  801048:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
  80104d:	e8 b3 f8 ff ff       	call   800905 <getchar>
  801052:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
  801054:	85 c0                	test   %eax,%eax
  801056:	79 24                	jns    80107c <readline+0x66>
			if (c != -E_EOF)
  801058:	83 f8 f8             	cmp    $0xfffffff8,%eax
  80105b:	0f 84 90 00 00 00    	je     8010f1 <readline+0xdb>
				cprintf("read error: %e\n", c);
  801061:	83 ec 08             	sub    $0x8,%esp
  801064:	50                   	push   %eax
  801065:	68 5f 37 80 00       	push   $0x80375f
  80106a:	e8 78 fa ff ff       	call   800ae7 <cprintf>
  80106f:	83 c4 10             	add    $0x10,%esp
			return NULL;
  801072:	b8 00 00 00 00       	mov    $0x0,%eax
  801077:	e9 98 00 00 00       	jmp    801114 <readline+0xfe>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
  80107c:	83 f8 08             	cmp    $0x8,%eax
  80107f:	74 7d                	je     8010fe <readline+0xe8>
  801081:	83 f8 7f             	cmp    $0x7f,%eax
  801084:	75 16                	jne    80109c <readline+0x86>
  801086:	eb 70                	jmp    8010f8 <readline+0xe2>
			if (echoing)
  801088:	85 ff                	test   %edi,%edi
  80108a:	74 0d                	je     801099 <readline+0x83>
				cputchar('\b');
  80108c:	83 ec 0c             	sub    $0xc,%esp
  80108f:	6a 08                	push   $0x8
  801091:	e8 53 f8 ff ff       	call   8008e9 <cputchar>
  801096:	83 c4 10             	add    $0x10,%esp
			i--;
  801099:	4e                   	dec    %esi
  80109a:	eb b1                	jmp    80104d <readline+0x37>
		} else if (c >= ' ' && i < BUFLEN-1) {
  80109c:	83 f8 1f             	cmp    $0x1f,%eax
  80109f:	7e 23                	jle    8010c4 <readline+0xae>
  8010a1:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
  8010a7:	7f 1b                	jg     8010c4 <readline+0xae>
			if (echoing)
  8010a9:	85 ff                	test   %edi,%edi
  8010ab:	74 0c                	je     8010b9 <readline+0xa3>
				cputchar(c);
  8010ad:	83 ec 0c             	sub    $0xc,%esp
  8010b0:	53                   	push   %ebx
  8010b1:	e8 33 f8 ff ff       	call   8008e9 <cputchar>
  8010b6:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
  8010b9:	88 9e 20 50 80 00    	mov    %bl,0x805020(%esi)
  8010bf:	8d 76 01             	lea    0x1(%esi),%esi
  8010c2:	eb 89                	jmp    80104d <readline+0x37>
		} else if (c == '\n' || c == '\r') {
  8010c4:	83 fb 0a             	cmp    $0xa,%ebx
  8010c7:	74 09                	je     8010d2 <readline+0xbc>
  8010c9:	83 fb 0d             	cmp    $0xd,%ebx
  8010cc:	0f 85 7b ff ff ff    	jne    80104d <readline+0x37>
			if (echoing)
  8010d2:	85 ff                	test   %edi,%edi
  8010d4:	74 0d                	je     8010e3 <readline+0xcd>
				cputchar('\n');
  8010d6:	83 ec 0c             	sub    $0xc,%esp
  8010d9:	6a 0a                	push   $0xa
  8010db:	e8 09 f8 ff ff       	call   8008e9 <cputchar>
  8010e0:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
  8010e3:	c6 86 20 50 80 00 00 	movb   $0x0,0x805020(%esi)
			return buf;
  8010ea:	b8 20 50 80 00       	mov    $0x805020,%eax
  8010ef:	eb 23                	jmp    801114 <readline+0xfe>
	while (1) {
		c = getchar();
		if (c < 0) {
			if (c != -E_EOF)
				cprintf("read error: %e\n", c);
			return NULL;
  8010f1:	b8 00 00 00 00       	mov    $0x0,%eax
  8010f6:	eb 1c                	jmp    801114 <readline+0xfe>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
  8010f8:	85 f6                	test   %esi,%esi
  8010fa:	7f 8c                	jg     801088 <readline+0x72>
  8010fc:	eb 09                	jmp    801107 <readline+0xf1>
  8010fe:	85 f6                	test   %esi,%esi
  801100:	7f 86                	jg     801088 <readline+0x72>
  801102:	e9 46 ff ff ff       	jmp    80104d <readline+0x37>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
  801107:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
  80110d:	7e 9a                	jle    8010a9 <readline+0x93>
  80110f:	e9 39 ff ff ff       	jmp    80104d <readline+0x37>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
  801114:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801117:	5b                   	pop    %ebx
  801118:	5e                   	pop    %esi
  801119:	5f                   	pop    %edi
  80111a:	5d                   	pop    %ebp
  80111b:	c3                   	ret    

0080111c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80111c:	55                   	push   %ebp
  80111d:	89 e5                	mov    %esp,%ebp
  80111f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  801122:	b8 00 00 00 00       	mov    $0x0,%eax
  801127:	eb 01                	jmp    80112a <strlen+0xe>
		n++;
  801129:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80112a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80112e:	75 f9                	jne    801129 <strlen+0xd>
		n++;
	return n;
}
  801130:	5d                   	pop    %ebp
  801131:	c3                   	ret    

00801132 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  801132:	55                   	push   %ebp
  801133:	89 e5                	mov    %esp,%ebp
  801135:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801138:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80113b:	ba 00 00 00 00       	mov    $0x0,%edx
  801140:	eb 01                	jmp    801143 <strnlen+0x11>
		n++;
  801142:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  801143:	39 c2                	cmp    %eax,%edx
  801145:	74 08                	je     80114f <strnlen+0x1d>
  801147:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80114b:	75 f5                	jne    801142 <strnlen+0x10>
  80114d:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80114f:	5d                   	pop    %ebp
  801150:	c3                   	ret    

00801151 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  801151:	55                   	push   %ebp
  801152:	89 e5                	mov    %esp,%ebp
  801154:	53                   	push   %ebx
  801155:	8b 45 08             	mov    0x8(%ebp),%eax
  801158:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80115b:	89 c2                	mov    %eax,%edx
  80115d:	42                   	inc    %edx
  80115e:	41                   	inc    %ecx
  80115f:	8a 59 ff             	mov    -0x1(%ecx),%bl
  801162:	88 5a ff             	mov    %bl,-0x1(%edx)
  801165:	84 db                	test   %bl,%bl
  801167:	75 f4                	jne    80115d <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  801169:	5b                   	pop    %ebx
  80116a:	5d                   	pop    %ebp
  80116b:	c3                   	ret    

0080116c <strcat>:

char *
strcat(char *dst, const char *src)
{
  80116c:	55                   	push   %ebp
  80116d:	89 e5                	mov    %esp,%ebp
  80116f:	53                   	push   %ebx
  801170:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  801173:	53                   	push   %ebx
  801174:	e8 a3 ff ff ff       	call   80111c <strlen>
  801179:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80117c:	ff 75 0c             	pushl  0xc(%ebp)
  80117f:	01 d8                	add    %ebx,%eax
  801181:	50                   	push   %eax
  801182:	e8 ca ff ff ff       	call   801151 <strcpy>
	return dst;
}
  801187:	89 d8                	mov    %ebx,%eax
  801189:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80118c:	c9                   	leave  
  80118d:	c3                   	ret    

0080118e <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80118e:	55                   	push   %ebp
  80118f:	89 e5                	mov    %esp,%ebp
  801191:	56                   	push   %esi
  801192:	53                   	push   %ebx
  801193:	8b 75 08             	mov    0x8(%ebp),%esi
  801196:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801199:	89 f3                	mov    %esi,%ebx
  80119b:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80119e:	89 f2                	mov    %esi,%edx
  8011a0:	eb 0c                	jmp    8011ae <strncpy+0x20>
		*dst++ = *src;
  8011a2:	42                   	inc    %edx
  8011a3:	8a 01                	mov    (%ecx),%al
  8011a5:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8011a8:	80 39 01             	cmpb   $0x1,(%ecx)
  8011ab:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8011ae:	39 da                	cmp    %ebx,%edx
  8011b0:	75 f0                	jne    8011a2 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8011b2:	89 f0                	mov    %esi,%eax
  8011b4:	5b                   	pop    %ebx
  8011b5:	5e                   	pop    %esi
  8011b6:	5d                   	pop    %ebp
  8011b7:	c3                   	ret    

008011b8 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8011b8:	55                   	push   %ebp
  8011b9:	89 e5                	mov    %esp,%ebp
  8011bb:	56                   	push   %esi
  8011bc:	53                   	push   %ebx
  8011bd:	8b 75 08             	mov    0x8(%ebp),%esi
  8011c0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8011c3:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8011c6:	85 c0                	test   %eax,%eax
  8011c8:	74 1e                	je     8011e8 <strlcpy+0x30>
  8011ca:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8011ce:	89 f2                	mov    %esi,%edx
  8011d0:	eb 05                	jmp    8011d7 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8011d2:	42                   	inc    %edx
  8011d3:	41                   	inc    %ecx
  8011d4:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8011d7:	39 c2                	cmp    %eax,%edx
  8011d9:	74 08                	je     8011e3 <strlcpy+0x2b>
  8011db:	8a 19                	mov    (%ecx),%bl
  8011dd:	84 db                	test   %bl,%bl
  8011df:	75 f1                	jne    8011d2 <strlcpy+0x1a>
  8011e1:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8011e3:	c6 00 00             	movb   $0x0,(%eax)
  8011e6:	eb 02                	jmp    8011ea <strlcpy+0x32>
  8011e8:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8011ea:	29 f0                	sub    %esi,%eax
}
  8011ec:	5b                   	pop    %ebx
  8011ed:	5e                   	pop    %esi
  8011ee:	5d                   	pop    %ebp
  8011ef:	c3                   	ret    

008011f0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8011f0:	55                   	push   %ebp
  8011f1:	89 e5                	mov    %esp,%ebp
  8011f3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8011f6:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8011f9:	eb 02                	jmp    8011fd <strcmp+0xd>
		p++, q++;
  8011fb:	41                   	inc    %ecx
  8011fc:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8011fd:	8a 01                	mov    (%ecx),%al
  8011ff:	84 c0                	test   %al,%al
  801201:	74 04                	je     801207 <strcmp+0x17>
  801203:	3a 02                	cmp    (%edx),%al
  801205:	74 f4                	je     8011fb <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  801207:	0f b6 c0             	movzbl %al,%eax
  80120a:	0f b6 12             	movzbl (%edx),%edx
  80120d:	29 d0                	sub    %edx,%eax
}
  80120f:	5d                   	pop    %ebp
  801210:	c3                   	ret    

00801211 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  801211:	55                   	push   %ebp
  801212:	89 e5                	mov    %esp,%ebp
  801214:	53                   	push   %ebx
  801215:	8b 45 08             	mov    0x8(%ebp),%eax
  801218:	8b 55 0c             	mov    0xc(%ebp),%edx
  80121b:	89 c3                	mov    %eax,%ebx
  80121d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  801220:	eb 02                	jmp    801224 <strncmp+0x13>
		n--, p++, q++;
  801222:	40                   	inc    %eax
  801223:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  801224:	39 d8                	cmp    %ebx,%eax
  801226:	74 14                	je     80123c <strncmp+0x2b>
  801228:	8a 08                	mov    (%eax),%cl
  80122a:	84 c9                	test   %cl,%cl
  80122c:	74 04                	je     801232 <strncmp+0x21>
  80122e:	3a 0a                	cmp    (%edx),%cl
  801230:	74 f0                	je     801222 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  801232:	0f b6 00             	movzbl (%eax),%eax
  801235:	0f b6 12             	movzbl (%edx),%edx
  801238:	29 d0                	sub    %edx,%eax
  80123a:	eb 05                	jmp    801241 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80123c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  801241:	5b                   	pop    %ebx
  801242:	5d                   	pop    %ebp
  801243:	c3                   	ret    

00801244 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  801244:	55                   	push   %ebp
  801245:	89 e5                	mov    %esp,%ebp
  801247:	8b 45 08             	mov    0x8(%ebp),%eax
  80124a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80124d:	eb 05                	jmp    801254 <strchr+0x10>
		if (*s == c)
  80124f:	38 ca                	cmp    %cl,%dl
  801251:	74 0c                	je     80125f <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  801253:	40                   	inc    %eax
  801254:	8a 10                	mov    (%eax),%dl
  801256:	84 d2                	test   %dl,%dl
  801258:	75 f5                	jne    80124f <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80125a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80125f:	5d                   	pop    %ebp
  801260:	c3                   	ret    

00801261 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  801261:	55                   	push   %ebp
  801262:	89 e5                	mov    %esp,%ebp
  801264:	8b 45 08             	mov    0x8(%ebp),%eax
  801267:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80126a:	eb 05                	jmp    801271 <strfind+0x10>
		if (*s == c)
  80126c:	38 ca                	cmp    %cl,%dl
  80126e:	74 07                	je     801277 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  801270:	40                   	inc    %eax
  801271:	8a 10                	mov    (%eax),%dl
  801273:	84 d2                	test   %dl,%dl
  801275:	75 f5                	jne    80126c <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  801277:	5d                   	pop    %ebp
  801278:	c3                   	ret    

00801279 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  801279:	55                   	push   %ebp
  80127a:	89 e5                	mov    %esp,%ebp
  80127c:	57                   	push   %edi
  80127d:	56                   	push   %esi
  80127e:	53                   	push   %ebx
  80127f:	8b 7d 08             	mov    0x8(%ebp),%edi
  801282:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  801285:	85 c9                	test   %ecx,%ecx
  801287:	74 36                	je     8012bf <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  801289:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80128f:	75 28                	jne    8012b9 <memset+0x40>
  801291:	f6 c1 03             	test   $0x3,%cl
  801294:	75 23                	jne    8012b9 <memset+0x40>
		c &= 0xFF;
  801296:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80129a:	89 d3                	mov    %edx,%ebx
  80129c:	c1 e3 08             	shl    $0x8,%ebx
  80129f:	89 d6                	mov    %edx,%esi
  8012a1:	c1 e6 18             	shl    $0x18,%esi
  8012a4:	89 d0                	mov    %edx,%eax
  8012a6:	c1 e0 10             	shl    $0x10,%eax
  8012a9:	09 f0                	or     %esi,%eax
  8012ab:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8012ad:	89 d8                	mov    %ebx,%eax
  8012af:	09 d0                	or     %edx,%eax
  8012b1:	c1 e9 02             	shr    $0x2,%ecx
  8012b4:	fc                   	cld    
  8012b5:	f3 ab                	rep stos %eax,%es:(%edi)
  8012b7:	eb 06                	jmp    8012bf <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8012b9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8012bc:	fc                   	cld    
  8012bd:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8012bf:	89 f8                	mov    %edi,%eax
  8012c1:	5b                   	pop    %ebx
  8012c2:	5e                   	pop    %esi
  8012c3:	5f                   	pop    %edi
  8012c4:	5d                   	pop    %ebp
  8012c5:	c3                   	ret    

008012c6 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8012c6:	55                   	push   %ebp
  8012c7:	89 e5                	mov    %esp,%ebp
  8012c9:	57                   	push   %edi
  8012ca:	56                   	push   %esi
  8012cb:	8b 45 08             	mov    0x8(%ebp),%eax
  8012ce:	8b 75 0c             	mov    0xc(%ebp),%esi
  8012d1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8012d4:	39 c6                	cmp    %eax,%esi
  8012d6:	73 33                	jae    80130b <memmove+0x45>
  8012d8:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8012db:	39 d0                	cmp    %edx,%eax
  8012dd:	73 2c                	jae    80130b <memmove+0x45>
		s += n;
		d += n;
  8012df:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8012e2:	89 d6                	mov    %edx,%esi
  8012e4:	09 fe                	or     %edi,%esi
  8012e6:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8012ec:	75 13                	jne    801301 <memmove+0x3b>
  8012ee:	f6 c1 03             	test   $0x3,%cl
  8012f1:	75 0e                	jne    801301 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8012f3:	83 ef 04             	sub    $0x4,%edi
  8012f6:	8d 72 fc             	lea    -0x4(%edx),%esi
  8012f9:	c1 e9 02             	shr    $0x2,%ecx
  8012fc:	fd                   	std    
  8012fd:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8012ff:	eb 07                	jmp    801308 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  801301:	4f                   	dec    %edi
  801302:	8d 72 ff             	lea    -0x1(%edx),%esi
  801305:	fd                   	std    
  801306:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  801308:	fc                   	cld    
  801309:	eb 1d                	jmp    801328 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80130b:	89 f2                	mov    %esi,%edx
  80130d:	09 c2                	or     %eax,%edx
  80130f:	f6 c2 03             	test   $0x3,%dl
  801312:	75 0f                	jne    801323 <memmove+0x5d>
  801314:	f6 c1 03             	test   $0x3,%cl
  801317:	75 0a                	jne    801323 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  801319:	c1 e9 02             	shr    $0x2,%ecx
  80131c:	89 c7                	mov    %eax,%edi
  80131e:	fc                   	cld    
  80131f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  801321:	eb 05                	jmp    801328 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  801323:	89 c7                	mov    %eax,%edi
  801325:	fc                   	cld    
  801326:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  801328:	5e                   	pop    %esi
  801329:	5f                   	pop    %edi
  80132a:	5d                   	pop    %ebp
  80132b:	c3                   	ret    

0080132c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80132c:	55                   	push   %ebp
  80132d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80132f:	ff 75 10             	pushl  0x10(%ebp)
  801332:	ff 75 0c             	pushl  0xc(%ebp)
  801335:	ff 75 08             	pushl  0x8(%ebp)
  801338:	e8 89 ff ff ff       	call   8012c6 <memmove>
}
  80133d:	c9                   	leave  
  80133e:	c3                   	ret    

0080133f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80133f:	55                   	push   %ebp
  801340:	89 e5                	mov    %esp,%ebp
  801342:	56                   	push   %esi
  801343:	53                   	push   %ebx
  801344:	8b 45 08             	mov    0x8(%ebp),%eax
  801347:	8b 55 0c             	mov    0xc(%ebp),%edx
  80134a:	89 c6                	mov    %eax,%esi
  80134c:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80134f:	eb 14                	jmp    801365 <memcmp+0x26>
		if (*s1 != *s2)
  801351:	8a 08                	mov    (%eax),%cl
  801353:	8a 1a                	mov    (%edx),%bl
  801355:	38 d9                	cmp    %bl,%cl
  801357:	74 0a                	je     801363 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  801359:	0f b6 c1             	movzbl %cl,%eax
  80135c:	0f b6 db             	movzbl %bl,%ebx
  80135f:	29 d8                	sub    %ebx,%eax
  801361:	eb 0b                	jmp    80136e <memcmp+0x2f>
		s1++, s2++;
  801363:	40                   	inc    %eax
  801364:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  801365:	39 f0                	cmp    %esi,%eax
  801367:	75 e8                	jne    801351 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  801369:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80136e:	5b                   	pop    %ebx
  80136f:	5e                   	pop    %esi
  801370:	5d                   	pop    %ebp
  801371:	c3                   	ret    

00801372 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  801372:	55                   	push   %ebp
  801373:	89 e5                	mov    %esp,%ebp
  801375:	53                   	push   %ebx
  801376:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  801379:	89 c1                	mov    %eax,%ecx
  80137b:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80137e:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  801382:	eb 08                	jmp    80138c <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  801384:	0f b6 10             	movzbl (%eax),%edx
  801387:	39 da                	cmp    %ebx,%edx
  801389:	74 05                	je     801390 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80138b:	40                   	inc    %eax
  80138c:	39 c8                	cmp    %ecx,%eax
  80138e:	72 f4                	jb     801384 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  801390:	5b                   	pop    %ebx
  801391:	5d                   	pop    %ebp
  801392:	c3                   	ret    

00801393 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  801393:	55                   	push   %ebp
  801394:	89 e5                	mov    %esp,%ebp
  801396:	57                   	push   %edi
  801397:	56                   	push   %esi
  801398:	53                   	push   %ebx
  801399:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80139c:	eb 01                	jmp    80139f <strtol+0xc>
		s++;
  80139e:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80139f:	8a 01                	mov    (%ecx),%al
  8013a1:	3c 20                	cmp    $0x20,%al
  8013a3:	74 f9                	je     80139e <strtol+0xb>
  8013a5:	3c 09                	cmp    $0x9,%al
  8013a7:	74 f5                	je     80139e <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8013a9:	3c 2b                	cmp    $0x2b,%al
  8013ab:	75 08                	jne    8013b5 <strtol+0x22>
		s++;
  8013ad:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8013ae:	bf 00 00 00 00       	mov    $0x0,%edi
  8013b3:	eb 11                	jmp    8013c6 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8013b5:	3c 2d                	cmp    $0x2d,%al
  8013b7:	75 08                	jne    8013c1 <strtol+0x2e>
		s++, neg = 1;
  8013b9:	41                   	inc    %ecx
  8013ba:	bf 01 00 00 00       	mov    $0x1,%edi
  8013bf:	eb 05                	jmp    8013c6 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8013c1:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8013c6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8013ca:	0f 84 87 00 00 00    	je     801457 <strtol+0xc4>
  8013d0:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8013d4:	75 27                	jne    8013fd <strtol+0x6a>
  8013d6:	80 39 30             	cmpb   $0x30,(%ecx)
  8013d9:	75 22                	jne    8013fd <strtol+0x6a>
  8013db:	e9 88 00 00 00       	jmp    801468 <strtol+0xd5>
		s += 2, base = 16;
  8013e0:	83 c1 02             	add    $0x2,%ecx
  8013e3:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8013ea:	eb 11                	jmp    8013fd <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8013ec:	41                   	inc    %ecx
  8013ed:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8013f4:	eb 07                	jmp    8013fd <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8013f6:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8013fd:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  801402:	8a 11                	mov    (%ecx),%dl
  801404:	8d 5a d0             	lea    -0x30(%edx),%ebx
  801407:	80 fb 09             	cmp    $0x9,%bl
  80140a:	77 08                	ja     801414 <strtol+0x81>
			dig = *s - '0';
  80140c:	0f be d2             	movsbl %dl,%edx
  80140f:	83 ea 30             	sub    $0x30,%edx
  801412:	eb 22                	jmp    801436 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  801414:	8d 72 9f             	lea    -0x61(%edx),%esi
  801417:	89 f3                	mov    %esi,%ebx
  801419:	80 fb 19             	cmp    $0x19,%bl
  80141c:	77 08                	ja     801426 <strtol+0x93>
			dig = *s - 'a' + 10;
  80141e:	0f be d2             	movsbl %dl,%edx
  801421:	83 ea 57             	sub    $0x57,%edx
  801424:	eb 10                	jmp    801436 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  801426:	8d 72 bf             	lea    -0x41(%edx),%esi
  801429:	89 f3                	mov    %esi,%ebx
  80142b:	80 fb 19             	cmp    $0x19,%bl
  80142e:	77 14                	ja     801444 <strtol+0xb1>
			dig = *s - 'A' + 10;
  801430:	0f be d2             	movsbl %dl,%edx
  801433:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  801436:	3b 55 10             	cmp    0x10(%ebp),%edx
  801439:	7d 09                	jge    801444 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80143b:	41                   	inc    %ecx
  80143c:	0f af 45 10          	imul   0x10(%ebp),%eax
  801440:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  801442:	eb be                	jmp    801402 <strtol+0x6f>

	if (endptr)
  801444:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  801448:	74 05                	je     80144f <strtol+0xbc>
		*endptr = (char *) s;
  80144a:	8b 75 0c             	mov    0xc(%ebp),%esi
  80144d:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  80144f:	85 ff                	test   %edi,%edi
  801451:	74 21                	je     801474 <strtol+0xe1>
  801453:	f7 d8                	neg    %eax
  801455:	eb 1d                	jmp    801474 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  801457:	80 39 30             	cmpb   $0x30,(%ecx)
  80145a:	75 9a                	jne    8013f6 <strtol+0x63>
  80145c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  801460:	0f 84 7a ff ff ff    	je     8013e0 <strtol+0x4d>
  801466:	eb 84                	jmp    8013ec <strtol+0x59>
  801468:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  80146c:	0f 84 6e ff ff ff    	je     8013e0 <strtol+0x4d>
  801472:	eb 89                	jmp    8013fd <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  801474:	5b                   	pop    %ebx
  801475:	5e                   	pop    %esi
  801476:	5f                   	pop    %edi
  801477:	5d                   	pop    %ebp
  801478:	c3                   	ret    

00801479 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  801479:	55                   	push   %ebp
  80147a:	89 e5                	mov    %esp,%ebp
  80147c:	57                   	push   %edi
  80147d:	56                   	push   %esi
  80147e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80147f:	b8 00 00 00 00       	mov    $0x0,%eax
  801484:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801487:	8b 55 08             	mov    0x8(%ebp),%edx
  80148a:	89 c3                	mov    %eax,%ebx
  80148c:	89 c7                	mov    %eax,%edi
  80148e:	89 c6                	mov    %eax,%esi
  801490:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  801492:	5b                   	pop    %ebx
  801493:	5e                   	pop    %esi
  801494:	5f                   	pop    %edi
  801495:	5d                   	pop    %ebp
  801496:	c3                   	ret    

00801497 <sys_cgetc>:

int
sys_cgetc(void)
{
  801497:	55                   	push   %ebp
  801498:	89 e5                	mov    %esp,%ebp
  80149a:	57                   	push   %edi
  80149b:	56                   	push   %esi
  80149c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80149d:	ba 00 00 00 00       	mov    $0x0,%edx
  8014a2:	b8 01 00 00 00       	mov    $0x1,%eax
  8014a7:	89 d1                	mov    %edx,%ecx
  8014a9:	89 d3                	mov    %edx,%ebx
  8014ab:	89 d7                	mov    %edx,%edi
  8014ad:	89 d6                	mov    %edx,%esi
  8014af:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8014b1:	5b                   	pop    %ebx
  8014b2:	5e                   	pop    %esi
  8014b3:	5f                   	pop    %edi
  8014b4:	5d                   	pop    %ebp
  8014b5:	c3                   	ret    

008014b6 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8014b6:	55                   	push   %ebp
  8014b7:	89 e5                	mov    %esp,%ebp
  8014b9:	57                   	push   %edi
  8014ba:	56                   	push   %esi
  8014bb:	53                   	push   %ebx
  8014bc:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8014bf:	b9 00 00 00 00       	mov    $0x0,%ecx
  8014c4:	b8 03 00 00 00       	mov    $0x3,%eax
  8014c9:	8b 55 08             	mov    0x8(%ebp),%edx
  8014cc:	89 cb                	mov    %ecx,%ebx
  8014ce:	89 cf                	mov    %ecx,%edi
  8014d0:	89 ce                	mov    %ecx,%esi
  8014d2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8014d4:	85 c0                	test   %eax,%eax
  8014d6:	7e 17                	jle    8014ef <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8014d8:	83 ec 0c             	sub    $0xc,%esp
  8014db:	50                   	push   %eax
  8014dc:	6a 03                	push   $0x3
  8014de:	68 6f 37 80 00       	push   $0x80376f
  8014e3:	6a 23                	push   $0x23
  8014e5:	68 8c 37 80 00       	push   $0x80378c
  8014ea:	e8 20 f5 ff ff       	call   800a0f <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  8014ef:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8014f2:	5b                   	pop    %ebx
  8014f3:	5e                   	pop    %esi
  8014f4:	5f                   	pop    %edi
  8014f5:	5d                   	pop    %ebp
  8014f6:	c3                   	ret    

008014f7 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  8014f7:	55                   	push   %ebp
  8014f8:	89 e5                	mov    %esp,%ebp
  8014fa:	57                   	push   %edi
  8014fb:	56                   	push   %esi
  8014fc:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8014fd:	ba 00 00 00 00       	mov    $0x0,%edx
  801502:	b8 02 00 00 00       	mov    $0x2,%eax
  801507:	89 d1                	mov    %edx,%ecx
  801509:	89 d3                	mov    %edx,%ebx
  80150b:	89 d7                	mov    %edx,%edi
  80150d:	89 d6                	mov    %edx,%esi
  80150f:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  801511:	5b                   	pop    %ebx
  801512:	5e                   	pop    %esi
  801513:	5f                   	pop    %edi
  801514:	5d                   	pop    %ebp
  801515:	c3                   	ret    

00801516 <sys_yield>:

void
sys_yield(void)
{
  801516:	55                   	push   %ebp
  801517:	89 e5                	mov    %esp,%ebp
  801519:	57                   	push   %edi
  80151a:	56                   	push   %esi
  80151b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80151c:	ba 00 00 00 00       	mov    $0x0,%edx
  801521:	b8 0b 00 00 00       	mov    $0xb,%eax
  801526:	89 d1                	mov    %edx,%ecx
  801528:	89 d3                	mov    %edx,%ebx
  80152a:	89 d7                	mov    %edx,%edi
  80152c:	89 d6                	mov    %edx,%esi
  80152e:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  801530:	5b                   	pop    %ebx
  801531:	5e                   	pop    %esi
  801532:	5f                   	pop    %edi
  801533:	5d                   	pop    %ebp
  801534:	c3                   	ret    

00801535 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  801535:	55                   	push   %ebp
  801536:	89 e5                	mov    %esp,%ebp
  801538:	57                   	push   %edi
  801539:	56                   	push   %esi
  80153a:	53                   	push   %ebx
  80153b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80153e:	be 00 00 00 00       	mov    $0x0,%esi
  801543:	b8 04 00 00 00       	mov    $0x4,%eax
  801548:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80154b:	8b 55 08             	mov    0x8(%ebp),%edx
  80154e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801551:	89 f7                	mov    %esi,%edi
  801553:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801555:	85 c0                	test   %eax,%eax
  801557:	7e 17                	jle    801570 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  801559:	83 ec 0c             	sub    $0xc,%esp
  80155c:	50                   	push   %eax
  80155d:	6a 04                	push   $0x4
  80155f:	68 6f 37 80 00       	push   $0x80376f
  801564:	6a 23                	push   $0x23
  801566:	68 8c 37 80 00       	push   $0x80378c
  80156b:	e8 9f f4 ff ff       	call   800a0f <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  801570:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801573:	5b                   	pop    %ebx
  801574:	5e                   	pop    %esi
  801575:	5f                   	pop    %edi
  801576:	5d                   	pop    %ebp
  801577:	c3                   	ret    

00801578 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  801578:	55                   	push   %ebp
  801579:	89 e5                	mov    %esp,%ebp
  80157b:	57                   	push   %edi
  80157c:	56                   	push   %esi
  80157d:	53                   	push   %ebx
  80157e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801581:	b8 05 00 00 00       	mov    $0x5,%eax
  801586:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801589:	8b 55 08             	mov    0x8(%ebp),%edx
  80158c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80158f:	8b 7d 14             	mov    0x14(%ebp),%edi
  801592:	8b 75 18             	mov    0x18(%ebp),%esi
  801595:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801597:	85 c0                	test   %eax,%eax
  801599:	7e 17                	jle    8015b2 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80159b:	83 ec 0c             	sub    $0xc,%esp
  80159e:	50                   	push   %eax
  80159f:	6a 05                	push   $0x5
  8015a1:	68 6f 37 80 00       	push   $0x80376f
  8015a6:	6a 23                	push   $0x23
  8015a8:	68 8c 37 80 00       	push   $0x80378c
  8015ad:	e8 5d f4 ff ff       	call   800a0f <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8015b2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8015b5:	5b                   	pop    %ebx
  8015b6:	5e                   	pop    %esi
  8015b7:	5f                   	pop    %edi
  8015b8:	5d                   	pop    %ebp
  8015b9:	c3                   	ret    

008015ba <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8015ba:	55                   	push   %ebp
  8015bb:	89 e5                	mov    %esp,%ebp
  8015bd:	57                   	push   %edi
  8015be:	56                   	push   %esi
  8015bf:	53                   	push   %ebx
  8015c0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8015c3:	bb 00 00 00 00       	mov    $0x0,%ebx
  8015c8:	b8 06 00 00 00       	mov    $0x6,%eax
  8015cd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8015d0:	8b 55 08             	mov    0x8(%ebp),%edx
  8015d3:	89 df                	mov    %ebx,%edi
  8015d5:	89 de                	mov    %ebx,%esi
  8015d7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8015d9:	85 c0                	test   %eax,%eax
  8015db:	7e 17                	jle    8015f4 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8015dd:	83 ec 0c             	sub    $0xc,%esp
  8015e0:	50                   	push   %eax
  8015e1:	6a 06                	push   $0x6
  8015e3:	68 6f 37 80 00       	push   $0x80376f
  8015e8:	6a 23                	push   $0x23
  8015ea:	68 8c 37 80 00       	push   $0x80378c
  8015ef:	e8 1b f4 ff ff       	call   800a0f <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  8015f4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8015f7:	5b                   	pop    %ebx
  8015f8:	5e                   	pop    %esi
  8015f9:	5f                   	pop    %edi
  8015fa:	5d                   	pop    %ebp
  8015fb:	c3                   	ret    

008015fc <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  8015fc:	55                   	push   %ebp
  8015fd:	89 e5                	mov    %esp,%ebp
  8015ff:	57                   	push   %edi
  801600:	56                   	push   %esi
  801601:	53                   	push   %ebx
  801602:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801605:	bb 00 00 00 00       	mov    $0x0,%ebx
  80160a:	b8 08 00 00 00       	mov    $0x8,%eax
  80160f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801612:	8b 55 08             	mov    0x8(%ebp),%edx
  801615:	89 df                	mov    %ebx,%edi
  801617:	89 de                	mov    %ebx,%esi
  801619:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80161b:	85 c0                	test   %eax,%eax
  80161d:	7e 17                	jle    801636 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80161f:	83 ec 0c             	sub    $0xc,%esp
  801622:	50                   	push   %eax
  801623:	6a 08                	push   $0x8
  801625:	68 6f 37 80 00       	push   $0x80376f
  80162a:	6a 23                	push   $0x23
  80162c:	68 8c 37 80 00       	push   $0x80378c
  801631:	e8 d9 f3 ff ff       	call   800a0f <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  801636:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801639:	5b                   	pop    %ebx
  80163a:	5e                   	pop    %esi
  80163b:	5f                   	pop    %edi
  80163c:	5d                   	pop    %ebp
  80163d:	c3                   	ret    

0080163e <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80163e:	55                   	push   %ebp
  80163f:	89 e5                	mov    %esp,%ebp
  801641:	57                   	push   %edi
  801642:	56                   	push   %esi
  801643:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801644:	ba 00 00 00 00       	mov    $0x0,%edx
  801649:	b8 0c 00 00 00       	mov    $0xc,%eax
  80164e:	89 d1                	mov    %edx,%ecx
  801650:	89 d3                	mov    %edx,%ebx
  801652:	89 d7                	mov    %edx,%edi
  801654:	89 d6                	mov    %edx,%esi
  801656:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  801658:	5b                   	pop    %ebx
  801659:	5e                   	pop    %esi
  80165a:	5f                   	pop    %edi
  80165b:	5d                   	pop    %ebp
  80165c:	c3                   	ret    

0080165d <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  80165d:	55                   	push   %ebp
  80165e:	89 e5                	mov    %esp,%ebp
  801660:	57                   	push   %edi
  801661:	56                   	push   %esi
  801662:	53                   	push   %ebx
  801663:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  801666:	bb 00 00 00 00       	mov    $0x0,%ebx
  80166b:	b8 09 00 00 00       	mov    $0x9,%eax
  801670:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801673:	8b 55 08             	mov    0x8(%ebp),%edx
  801676:	89 df                	mov    %ebx,%edi
  801678:	89 de                	mov    %ebx,%esi
  80167a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80167c:	85 c0                	test   %eax,%eax
  80167e:	7e 17                	jle    801697 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  801680:	83 ec 0c             	sub    $0xc,%esp
  801683:	50                   	push   %eax
  801684:	6a 09                	push   $0x9
  801686:	68 6f 37 80 00       	push   $0x80376f
  80168b:	6a 23                	push   $0x23
  80168d:	68 8c 37 80 00       	push   $0x80378c
  801692:	e8 78 f3 ff ff       	call   800a0f <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  801697:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80169a:	5b                   	pop    %ebx
  80169b:	5e                   	pop    %esi
  80169c:	5f                   	pop    %edi
  80169d:	5d                   	pop    %ebp
  80169e:	c3                   	ret    

0080169f <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  80169f:	55                   	push   %ebp
  8016a0:	89 e5                	mov    %esp,%ebp
  8016a2:	57                   	push   %edi
  8016a3:	56                   	push   %esi
  8016a4:	53                   	push   %ebx
  8016a5:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8016a8:	bb 00 00 00 00       	mov    $0x0,%ebx
  8016ad:	b8 0a 00 00 00       	mov    $0xa,%eax
  8016b2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8016b5:	8b 55 08             	mov    0x8(%ebp),%edx
  8016b8:	89 df                	mov    %ebx,%edi
  8016ba:	89 de                	mov    %ebx,%esi
  8016bc:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8016be:	85 c0                	test   %eax,%eax
  8016c0:	7e 17                	jle    8016d9 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8016c2:	83 ec 0c             	sub    $0xc,%esp
  8016c5:	50                   	push   %eax
  8016c6:	6a 0a                	push   $0xa
  8016c8:	68 6f 37 80 00       	push   $0x80376f
  8016cd:	6a 23                	push   $0x23
  8016cf:	68 8c 37 80 00       	push   $0x80378c
  8016d4:	e8 36 f3 ff ff       	call   800a0f <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8016d9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8016dc:	5b                   	pop    %ebx
  8016dd:	5e                   	pop    %esi
  8016de:	5f                   	pop    %edi
  8016df:	5d                   	pop    %ebp
  8016e0:	c3                   	ret    

008016e1 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  8016e1:	55                   	push   %ebp
  8016e2:	89 e5                	mov    %esp,%ebp
  8016e4:	57                   	push   %edi
  8016e5:	56                   	push   %esi
  8016e6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8016e7:	be 00 00 00 00       	mov    $0x0,%esi
  8016ec:	b8 0d 00 00 00       	mov    $0xd,%eax
  8016f1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8016f4:	8b 55 08             	mov    0x8(%ebp),%edx
  8016f7:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8016fa:	8b 7d 14             	mov    0x14(%ebp),%edi
  8016fd:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  8016ff:	5b                   	pop    %ebx
  801700:	5e                   	pop    %esi
  801701:	5f                   	pop    %edi
  801702:	5d                   	pop    %ebp
  801703:	c3                   	ret    

00801704 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  801704:	55                   	push   %ebp
  801705:	89 e5                	mov    %esp,%ebp
  801707:	57                   	push   %edi
  801708:	56                   	push   %esi
  801709:	53                   	push   %ebx
  80170a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80170d:	b9 00 00 00 00       	mov    $0x0,%ecx
  801712:	b8 0e 00 00 00       	mov    $0xe,%eax
  801717:	8b 55 08             	mov    0x8(%ebp),%edx
  80171a:	89 cb                	mov    %ecx,%ebx
  80171c:	89 cf                	mov    %ecx,%edi
  80171e:	89 ce                	mov    %ecx,%esi
  801720:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  801722:	85 c0                	test   %eax,%eax
  801724:	7e 17                	jle    80173d <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  801726:	83 ec 0c             	sub    $0xc,%esp
  801729:	50                   	push   %eax
  80172a:	6a 0e                	push   $0xe
  80172c:	68 6f 37 80 00       	push   $0x80376f
  801731:	6a 23                	push   $0x23
  801733:	68 8c 37 80 00       	push   $0x80378c
  801738:	e8 d2 f2 ff ff       	call   800a0f <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80173d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801740:	5b                   	pop    %ebx
  801741:	5e                   	pop    %esi
  801742:	5f                   	pop    %edi
  801743:	5d                   	pop    %ebp
  801744:	c3                   	ret    

00801745 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  801745:	55                   	push   %ebp
  801746:	89 e5                	mov    %esp,%ebp
  801748:	53                   	push   %ebx
  801749:	83 ec 04             	sub    $0x4,%esp
  80174c:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  80174f:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  801751:	89 da                	mov    %ebx,%edx
  801753:	c1 ea 0c             	shr    $0xc,%edx
  801756:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  80175d:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  801761:	74 05                	je     801768 <pgfault+0x23>
  801763:	f6 c6 08             	test   $0x8,%dh
  801766:	75 14                	jne    80177c <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  801768:	83 ec 04             	sub    $0x4,%esp
  80176b:	68 9c 37 80 00       	push   $0x80379c
  801770:	6a 26                	push   $0x26
  801772:	68 00 38 80 00       	push   $0x803800
  801777:	e8 93 f2 ff ff       	call   800a0f <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  80177c:	83 ec 04             	sub    $0x4,%esp
  80177f:	6a 07                	push   $0x7
  801781:	68 00 f0 7f 00       	push   $0x7ff000
  801786:	6a 00                	push   $0x0
  801788:	e8 a8 fd ff ff       	call   801535 <sys_page_alloc>
  80178d:	83 c4 10             	add    $0x10,%esp
  801790:	85 c0                	test   %eax,%eax
  801792:	74 14                	je     8017a8 <pgfault+0x63>
        panic("pgfault: no phys mem");
  801794:	83 ec 04             	sub    $0x4,%esp
  801797:	68 0b 38 80 00       	push   $0x80380b
  80179c:	6a 32                	push   $0x32
  80179e:	68 00 38 80 00       	push   $0x803800
  8017a3:	e8 67 f2 ff ff       	call   800a0f <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  8017a8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  8017ae:	83 ec 04             	sub    $0x4,%esp
  8017b1:	68 00 10 00 00       	push   $0x1000
  8017b6:	53                   	push   %ebx
  8017b7:	68 00 f0 7f 00       	push   $0x7ff000
  8017bc:	e8 05 fb ff ff       	call   8012c6 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  8017c1:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  8017c8:	53                   	push   %ebx
  8017c9:	6a 00                	push   $0x0
  8017cb:	68 00 f0 7f 00       	push   $0x7ff000
  8017d0:	6a 00                	push   $0x0
  8017d2:	e8 a1 fd ff ff       	call   801578 <sys_page_map>
  8017d7:	83 c4 20             	add    $0x20,%esp
  8017da:	85 c0                	test   %eax,%eax
  8017dc:	74 14                	je     8017f2 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  8017de:	83 ec 04             	sub    $0x4,%esp
  8017e1:	68 20 38 80 00       	push   $0x803820
  8017e6:	6a 3f                	push   $0x3f
  8017e8:	68 00 38 80 00       	push   $0x803800
  8017ed:	e8 1d f2 ff ff       	call   800a0f <_panic>


	//panic("pgfault not implemented");
}
  8017f2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017f5:	c9                   	leave  
  8017f6:	c3                   	ret    

008017f7 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  8017f7:	55                   	push   %ebp
  8017f8:	89 e5                	mov    %esp,%ebp
  8017fa:	57                   	push   %edi
  8017fb:	56                   	push   %esi
  8017fc:	53                   	push   %ebx
  8017fd:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  801800:	68 45 17 80 00       	push   $0x801745
  801805:	e8 dc 15 00 00       	call   802de6 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  80180a:	b8 07 00 00 00       	mov    $0x7,%eax
  80180f:	cd 30                	int    $0x30
  801811:	89 45 dc             	mov    %eax,-0x24(%ebp)
  801814:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  801817:	83 c4 10             	add    $0x10,%esp
  80181a:	85 c0                	test   %eax,%eax
  80181c:	79 17                	jns    801835 <fork+0x3e>
        panic("fork: cannot create child env");
  80181e:	83 ec 04             	sub    $0x4,%esp
  801821:	68 33 38 80 00       	push   $0x803833
  801826:	68 97 00 00 00       	push   $0x97
  80182b:	68 00 38 80 00       	push   $0x803800
  801830:	e8 da f1 ff ff       	call   800a0f <_panic>
    } else if (envid == 0) {
  801835:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  801839:	75 2a                	jne    801865 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  80183b:	e8 b7 fc ff ff       	call   8014f7 <sys_getenvid>
  801840:	25 ff 03 00 00       	and    $0x3ff,%eax
  801845:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80184c:	c1 e0 07             	shl    $0x7,%eax
  80184f:	29 d0                	sub    %edx,%eax
  801851:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801856:	a3 24 54 80 00       	mov    %eax,0x805424
        return 0;
  80185b:	b8 00 00 00 00       	mov    $0x0,%eax
  801860:	e9 94 01 00 00       	jmp    8019f9 <fork+0x202>
  801865:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  80186c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80186f:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  801876:	a8 01                	test   $0x1,%al
  801878:	0f 84 0a 01 00 00    	je     801988 <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  80187e:	c1 e7 0a             	shl    $0xa,%edi
  801881:	be 00 00 00 00       	mov    $0x0,%esi
  801886:	89 fb                	mov    %edi,%ebx
  801888:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  80188a:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  801890:	75 34                	jne    8018c6 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  801892:	83 ec 04             	sub    $0x4,%esp
  801895:	6a 07                	push   $0x7
  801897:	68 00 f0 bf ee       	push   $0xeebff000
  80189c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80189f:	e8 91 fc ff ff       	call   801535 <sys_page_alloc>
  8018a4:	83 c4 10             	add    $0x10,%esp
  8018a7:	85 c0                	test   %eax,%eax
  8018a9:	0f 84 cc 00 00 00    	je     80197b <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  8018af:	83 ec 04             	sub    $0x4,%esp
  8018b2:	68 51 38 80 00       	push   $0x803851
  8018b7:	68 ad 00 00 00       	push   $0xad
  8018bc:	68 00 38 80 00       	push   $0x803800
  8018c1:	e8 49 f1 ff ff       	call   800a0f <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  8018c6:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  8018cd:	a8 01                	test   $0x1,%al
  8018cf:	0f 84 a6 00 00 00    	je     80197b <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  8018d5:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  8018dc:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  8018df:	a9 02 08 00 00       	test   $0x802,%eax
  8018e4:	74 62                	je     801948 <fork+0x151>
  8018e6:	f6 c4 04             	test   $0x4,%ah
  8018e9:	75 78                	jne    801963 <fork+0x16c>
        if (sys_page_map(0,
  8018eb:	83 ec 0c             	sub    $0xc,%esp
  8018ee:	68 05 08 00 00       	push   $0x805
  8018f3:	53                   	push   %ebx
  8018f4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8018f7:	53                   	push   %ebx
  8018f8:	6a 00                	push   $0x0
  8018fa:	e8 79 fc ff ff       	call   801578 <sys_page_map>
  8018ff:	83 c4 20             	add    $0x20,%esp
  801902:	85 c0                	test   %eax,%eax
  801904:	74 14                	je     80191a <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  801906:	83 ec 04             	sub    $0x4,%esp
  801909:	68 6c 38 80 00       	push   $0x80386c
  80190e:	6a 64                	push   $0x64
  801910:	68 00 38 80 00       	push   $0x803800
  801915:	e8 f5 f0 ff ff       	call   800a0f <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  80191a:	83 ec 0c             	sub    $0xc,%esp
  80191d:	68 05 08 00 00       	push   $0x805
  801922:	53                   	push   %ebx
  801923:	6a 00                	push   $0x0
  801925:	53                   	push   %ebx
  801926:	6a 00                	push   $0x0
  801928:	e8 4b fc ff ff       	call   801578 <sys_page_map>
  80192d:	83 c4 20             	add    $0x20,%esp
  801930:	85 c0                	test   %eax,%eax
  801932:	74 47                	je     80197b <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  801934:	83 ec 04             	sub    $0x4,%esp
  801937:	68 83 38 80 00       	push   $0x803883
  80193c:	6a 6d                	push   $0x6d
  80193e:	68 00 38 80 00       	push   $0x803800
  801943:	e8 c7 f0 ff ff       	call   800a0f <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  801948:	f6 c4 04             	test   $0x4,%ah
  80194b:	75 16                	jne    801963 <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  80194d:	83 ec 0c             	sub    $0xc,%esp
  801950:	6a 05                	push   $0x5
  801952:	53                   	push   %ebx
  801953:	ff 75 e4             	pushl  -0x1c(%ebp)
  801956:	53                   	push   %ebx
  801957:	6a 00                	push   $0x0
  801959:	e8 1a fc ff ff       	call   801578 <sys_page_map>
  80195e:	83 c4 20             	add    $0x20,%esp
  801961:	eb 18                	jmp    80197b <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  801963:	83 ec 0c             	sub    $0xc,%esp
  801966:	25 07 0e 00 00       	and    $0xe07,%eax
  80196b:	50                   	push   %eax
  80196c:	53                   	push   %ebx
  80196d:	ff 75 e4             	pushl  -0x1c(%ebp)
  801970:	53                   	push   %ebx
  801971:	6a 00                	push   $0x0
  801973:	e8 00 fc ff ff       	call   801578 <sys_page_map>
  801978:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  80197b:	46                   	inc    %esi
  80197c:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  801982:	0f 85 fe fe ff ff    	jne    801886 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801988:	ff 45 e0             	incl   -0x20(%ebp)
  80198b:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80198e:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  801993:	0f 85 d3 fe ff ff    	jne    80186c <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  801999:	a1 24 54 80 00       	mov    0x805424,%eax
  80199e:	8b 40 64             	mov    0x64(%eax),%eax
  8019a1:	83 ec 08             	sub    $0x8,%esp
  8019a4:	50                   	push   %eax
  8019a5:	ff 75 dc             	pushl  -0x24(%ebp)
  8019a8:	e8 f2 fc ff ff       	call   80169f <sys_env_set_pgfault_upcall>
  8019ad:	83 c4 10             	add    $0x10,%esp
  8019b0:	85 c0                	test   %eax,%eax
  8019b2:	74 17                	je     8019cb <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  8019b4:	83 ec 04             	sub    $0x4,%esp
  8019b7:	68 e0 37 80 00       	push   $0x8037e0
  8019bc:	68 b9 00 00 00       	push   $0xb9
  8019c1:	68 00 38 80 00       	push   $0x803800
  8019c6:	e8 44 f0 ff ff       	call   800a0f <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  8019cb:	83 ec 08             	sub    $0x8,%esp
  8019ce:	6a 02                	push   $0x2
  8019d0:	ff 75 dc             	pushl  -0x24(%ebp)
  8019d3:	e8 24 fc ff ff       	call   8015fc <sys_env_set_status>
  8019d8:	83 c4 10             	add    $0x10,%esp
  8019db:	85 c0                	test   %eax,%eax
  8019dd:	74 17                	je     8019f6 <fork+0x1ff>
        panic("fork: cannot set env status");
  8019df:	83 ec 04             	sub    $0x4,%esp
  8019e2:	68 9e 38 80 00       	push   $0x80389e
  8019e7:	68 bd 00 00 00       	push   $0xbd
  8019ec:	68 00 38 80 00       	push   $0x803800
  8019f1:	e8 19 f0 ff ff       	call   800a0f <_panic>

    return envid;
  8019f6:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  8019f9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8019fc:	5b                   	pop    %ebx
  8019fd:	5e                   	pop    %esi
  8019fe:	5f                   	pop    %edi
  8019ff:	5d                   	pop    %ebp
  801a00:	c3                   	ret    

00801a01 <sfork>:

// Challenge!
int
sfork(void)
{
  801a01:	55                   	push   %ebp
  801a02:	89 e5                	mov    %esp,%ebp
  801a04:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  801a07:	68 ba 38 80 00       	push   $0x8038ba
  801a0c:	68 c8 00 00 00       	push   $0xc8
  801a11:	68 00 38 80 00       	push   $0x803800
  801a16:	e8 f4 ef ff ff       	call   800a0f <_panic>

00801a1b <argstart>:
#include <inc/args.h>
#include <inc/string.h>

void
argstart(int *argc, char **argv, struct Argstate *args)
{
  801a1b:	55                   	push   %ebp
  801a1c:	89 e5                	mov    %esp,%ebp
  801a1e:	8b 55 08             	mov    0x8(%ebp),%edx
  801a21:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801a24:	8b 45 10             	mov    0x10(%ebp),%eax
	args->argc = argc;
  801a27:	89 10                	mov    %edx,(%eax)
	args->argv = (const char **) argv;
  801a29:	89 48 04             	mov    %ecx,0x4(%eax)
	args->curarg = (*argc > 1 && argv ? "" : 0);
  801a2c:	83 3a 01             	cmpl   $0x1,(%edx)
  801a2f:	7e 0b                	jle    801a3c <argstart+0x21>
  801a31:	85 c9                	test   %ecx,%ecx
  801a33:	74 0e                	je     801a43 <argstart+0x28>
  801a35:	ba 41 32 80 00       	mov    $0x803241,%edx
  801a3a:	eb 0c                	jmp    801a48 <argstart+0x2d>
  801a3c:	ba 00 00 00 00       	mov    $0x0,%edx
  801a41:	eb 05                	jmp    801a48 <argstart+0x2d>
  801a43:	ba 00 00 00 00       	mov    $0x0,%edx
  801a48:	89 50 08             	mov    %edx,0x8(%eax)
	args->argvalue = 0;
  801a4b:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
}
  801a52:	5d                   	pop    %ebp
  801a53:	c3                   	ret    

00801a54 <argnext>:

int
argnext(struct Argstate *args)
{
  801a54:	55                   	push   %ebp
  801a55:	89 e5                	mov    %esp,%ebp
  801a57:	53                   	push   %ebx
  801a58:	83 ec 04             	sub    $0x4,%esp
  801a5b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int arg;

	args->argvalue = 0;
  801a5e:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)

	// Done processing arguments if args->curarg == 0
	if (args->curarg == 0)
  801a65:	8b 43 08             	mov    0x8(%ebx),%eax
  801a68:	85 c0                	test   %eax,%eax
  801a6a:	74 6a                	je     801ad6 <argnext+0x82>
		return -1;

	if (!*args->curarg) {
  801a6c:	80 38 00             	cmpb   $0x0,(%eax)
  801a6f:	75 4b                	jne    801abc <argnext+0x68>
		// Need to process the next argument
		// Check for end of argument list
		if (*args->argc == 1
  801a71:	8b 0b                	mov    (%ebx),%ecx
  801a73:	83 39 01             	cmpl   $0x1,(%ecx)
  801a76:	74 50                	je     801ac8 <argnext+0x74>
		    || args->argv[1][0] != '-'
  801a78:	8b 53 04             	mov    0x4(%ebx),%edx
  801a7b:	8b 42 04             	mov    0x4(%edx),%eax
  801a7e:	80 38 2d             	cmpb   $0x2d,(%eax)
  801a81:	75 45                	jne    801ac8 <argnext+0x74>
		    || args->argv[1][1] == '\0')
  801a83:	80 78 01 00          	cmpb   $0x0,0x1(%eax)
  801a87:	74 3f                	je     801ac8 <argnext+0x74>
			goto endofargs;
		// Shift arguments down one
		args->curarg = args->argv[1] + 1;
  801a89:	40                   	inc    %eax
  801a8a:	89 43 08             	mov    %eax,0x8(%ebx)
		memmove(args->argv + 1, args->argv + 2, sizeof(const char *) * (*args->argc - 1));
  801a8d:	83 ec 04             	sub    $0x4,%esp
  801a90:	8b 01                	mov    (%ecx),%eax
  801a92:	8d 04 85 fc ff ff ff 	lea    -0x4(,%eax,4),%eax
  801a99:	50                   	push   %eax
  801a9a:	8d 42 08             	lea    0x8(%edx),%eax
  801a9d:	50                   	push   %eax
  801a9e:	83 c2 04             	add    $0x4,%edx
  801aa1:	52                   	push   %edx
  801aa2:	e8 1f f8 ff ff       	call   8012c6 <memmove>
		(*args->argc)--;
  801aa7:	8b 03                	mov    (%ebx),%eax
  801aa9:	ff 08                	decl   (%eax)
		// Check for "--": end of argument list
		if (args->curarg[0] == '-' && args->curarg[1] == '\0')
  801aab:	8b 43 08             	mov    0x8(%ebx),%eax
  801aae:	83 c4 10             	add    $0x10,%esp
  801ab1:	80 38 2d             	cmpb   $0x2d,(%eax)
  801ab4:	75 06                	jne    801abc <argnext+0x68>
  801ab6:	80 78 01 00          	cmpb   $0x0,0x1(%eax)
  801aba:	74 0c                	je     801ac8 <argnext+0x74>
			goto endofargs;
	}

	arg = (unsigned char) *args->curarg;
  801abc:	8b 53 08             	mov    0x8(%ebx),%edx
  801abf:	0f b6 02             	movzbl (%edx),%eax
	args->curarg++;
  801ac2:	42                   	inc    %edx
  801ac3:	89 53 08             	mov    %edx,0x8(%ebx)
	return arg;
  801ac6:	eb 13                	jmp    801adb <argnext+0x87>

    endofargs:
	args->curarg = 0;
  801ac8:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
	return -1;
  801acf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  801ad4:	eb 05                	jmp    801adb <argnext+0x87>

	args->argvalue = 0;

	// Done processing arguments if args->curarg == 0
	if (args->curarg == 0)
		return -1;
  801ad6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return arg;

    endofargs:
	args->curarg = 0;
	return -1;
}
  801adb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801ade:	c9                   	leave  
  801adf:	c3                   	ret    

00801ae0 <argnextvalue>:
	return (char*) (args->argvalue ? args->argvalue : argnextvalue(args));
}

char *
argnextvalue(struct Argstate *args)
{
  801ae0:	55                   	push   %ebp
  801ae1:	89 e5                	mov    %esp,%ebp
  801ae3:	53                   	push   %ebx
  801ae4:	83 ec 04             	sub    $0x4,%esp
  801ae7:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (!args->curarg)
  801aea:	8b 43 08             	mov    0x8(%ebx),%eax
  801aed:	85 c0                	test   %eax,%eax
  801aef:	74 57                	je     801b48 <argnextvalue+0x68>
		return 0;
	if (*args->curarg) {
  801af1:	80 38 00             	cmpb   $0x0,(%eax)
  801af4:	74 0c                	je     801b02 <argnextvalue+0x22>
		args->argvalue = args->curarg;
  801af6:	89 43 0c             	mov    %eax,0xc(%ebx)
		args->curarg = "";
  801af9:	c7 43 08 41 32 80 00 	movl   $0x803241,0x8(%ebx)
  801b00:	eb 41                	jmp    801b43 <argnextvalue+0x63>
	} else if (*args->argc > 1) {
  801b02:	8b 13                	mov    (%ebx),%edx
  801b04:	83 3a 01             	cmpl   $0x1,(%edx)
  801b07:	7e 2c                	jle    801b35 <argnextvalue+0x55>
		args->argvalue = args->argv[1];
  801b09:	8b 43 04             	mov    0x4(%ebx),%eax
  801b0c:	8b 48 04             	mov    0x4(%eax),%ecx
  801b0f:	89 4b 0c             	mov    %ecx,0xc(%ebx)
		memmove(args->argv + 1, args->argv + 2, sizeof(const char *) * (*args->argc - 1));
  801b12:	83 ec 04             	sub    $0x4,%esp
  801b15:	8b 12                	mov    (%edx),%edx
  801b17:	8d 14 95 fc ff ff ff 	lea    -0x4(,%edx,4),%edx
  801b1e:	52                   	push   %edx
  801b1f:	8d 50 08             	lea    0x8(%eax),%edx
  801b22:	52                   	push   %edx
  801b23:	83 c0 04             	add    $0x4,%eax
  801b26:	50                   	push   %eax
  801b27:	e8 9a f7 ff ff       	call   8012c6 <memmove>
		(*args->argc)--;
  801b2c:	8b 03                	mov    (%ebx),%eax
  801b2e:	ff 08                	decl   (%eax)
  801b30:	83 c4 10             	add    $0x10,%esp
  801b33:	eb 0e                	jmp    801b43 <argnextvalue+0x63>
	} else {
		args->argvalue = 0;
  801b35:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
		args->curarg = 0;
  801b3c:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
	}
	return (char*) args->argvalue;
  801b43:	8b 43 0c             	mov    0xc(%ebx),%eax
  801b46:	eb 05                	jmp    801b4d <argnextvalue+0x6d>

char *
argnextvalue(struct Argstate *args)
{
	if (!args->curarg)
		return 0;
  801b48:	b8 00 00 00 00       	mov    $0x0,%eax
	} else {
		args->argvalue = 0;
		args->curarg = 0;
	}
	return (char*) args->argvalue;
}
  801b4d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801b50:	c9                   	leave  
  801b51:	c3                   	ret    

00801b52 <argvalue>:
	return -1;
}

char *
argvalue(struct Argstate *args)
{
  801b52:	55                   	push   %ebp
  801b53:	89 e5                	mov    %esp,%ebp
  801b55:	83 ec 08             	sub    $0x8,%esp
  801b58:	8b 4d 08             	mov    0x8(%ebp),%ecx
	return (char*) (args->argvalue ? args->argvalue : argnextvalue(args));
  801b5b:	8b 51 0c             	mov    0xc(%ecx),%edx
  801b5e:	89 d0                	mov    %edx,%eax
  801b60:	85 d2                	test   %edx,%edx
  801b62:	75 0c                	jne    801b70 <argvalue+0x1e>
  801b64:	83 ec 0c             	sub    $0xc,%esp
  801b67:	51                   	push   %ecx
  801b68:	e8 73 ff ff ff       	call   801ae0 <argnextvalue>
  801b6d:	83 c4 10             	add    $0x10,%esp
}
  801b70:	c9                   	leave  
  801b71:	c3                   	ret    

00801b72 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  801b72:	55                   	push   %ebp
  801b73:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  801b75:	8b 45 08             	mov    0x8(%ebp),%eax
  801b78:	05 00 00 00 30       	add    $0x30000000,%eax
  801b7d:	c1 e8 0c             	shr    $0xc,%eax
}
  801b80:	5d                   	pop    %ebp
  801b81:	c3                   	ret    

00801b82 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  801b82:	55                   	push   %ebp
  801b83:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  801b85:	8b 45 08             	mov    0x8(%ebp),%eax
  801b88:	05 00 00 00 30       	add    $0x30000000,%eax
  801b8d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  801b92:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  801b97:	5d                   	pop    %ebp
  801b98:	c3                   	ret    

00801b99 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  801b99:	55                   	push   %ebp
  801b9a:	89 e5                	mov    %esp,%ebp
  801b9c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801b9f:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  801ba4:	89 c2                	mov    %eax,%edx
  801ba6:	c1 ea 16             	shr    $0x16,%edx
  801ba9:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801bb0:	f6 c2 01             	test   $0x1,%dl
  801bb3:	74 11                	je     801bc6 <fd_alloc+0x2d>
  801bb5:	89 c2                	mov    %eax,%edx
  801bb7:	c1 ea 0c             	shr    $0xc,%edx
  801bba:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801bc1:	f6 c2 01             	test   $0x1,%dl
  801bc4:	75 09                	jne    801bcf <fd_alloc+0x36>
			*fd_store = fd;
  801bc6:	89 01                	mov    %eax,(%ecx)
			return 0;
  801bc8:	b8 00 00 00 00       	mov    $0x0,%eax
  801bcd:	eb 17                	jmp    801be6 <fd_alloc+0x4d>
  801bcf:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801bd4:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801bd9:	75 c9                	jne    801ba4 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801bdb:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801be1:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801be6:	5d                   	pop    %ebp
  801be7:	c3                   	ret    

00801be8 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  801be8:	55                   	push   %ebp
  801be9:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  801beb:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801bef:	77 39                	ja     801c2a <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801bf1:	8b 45 08             	mov    0x8(%ebp),%eax
  801bf4:	c1 e0 0c             	shl    $0xc,%eax
  801bf7:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  801bfc:	89 c2                	mov    %eax,%edx
  801bfe:	c1 ea 16             	shr    $0x16,%edx
  801c01:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801c08:	f6 c2 01             	test   $0x1,%dl
  801c0b:	74 24                	je     801c31 <fd_lookup+0x49>
  801c0d:	89 c2                	mov    %eax,%edx
  801c0f:	c1 ea 0c             	shr    $0xc,%edx
  801c12:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801c19:	f6 c2 01             	test   $0x1,%dl
  801c1c:	74 1a                	je     801c38 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  801c1e:	8b 55 0c             	mov    0xc(%ebp),%edx
  801c21:	89 02                	mov    %eax,(%edx)
	return 0;
  801c23:	b8 00 00 00 00       	mov    $0x0,%eax
  801c28:	eb 13                	jmp    801c3d <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801c2a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801c2f:	eb 0c                	jmp    801c3d <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801c31:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801c36:	eb 05                	jmp    801c3d <fd_lookup+0x55>
  801c38:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  801c3d:	5d                   	pop    %ebp
  801c3e:	c3                   	ret    

00801c3f <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  801c3f:	55                   	push   %ebp
  801c40:	89 e5                	mov    %esp,%ebp
  801c42:	83 ec 08             	sub    $0x8,%esp
  801c45:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801c48:	ba 4c 39 80 00       	mov    $0x80394c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801c4d:	eb 13                	jmp    801c62 <dev_lookup+0x23>
  801c4f:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  801c52:	39 08                	cmp    %ecx,(%eax)
  801c54:	75 0c                	jne    801c62 <dev_lookup+0x23>
			*dev = devtab[i];
  801c56:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801c59:	89 01                	mov    %eax,(%ecx)
			return 0;
  801c5b:	b8 00 00 00 00       	mov    $0x0,%eax
  801c60:	eb 2e                	jmp    801c90 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801c62:	8b 02                	mov    (%edx),%eax
  801c64:	85 c0                	test   %eax,%eax
  801c66:	75 e7                	jne    801c4f <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  801c68:	a1 24 54 80 00       	mov    0x805424,%eax
  801c6d:	8b 40 48             	mov    0x48(%eax),%eax
  801c70:	83 ec 04             	sub    $0x4,%esp
  801c73:	51                   	push   %ecx
  801c74:	50                   	push   %eax
  801c75:	68 d0 38 80 00       	push   $0x8038d0
  801c7a:	e8 68 ee ff ff       	call   800ae7 <cprintf>
	*dev = 0;
  801c7f:	8b 45 0c             	mov    0xc(%ebp),%eax
  801c82:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  801c88:	83 c4 10             	add    $0x10,%esp
  801c8b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801c90:	c9                   	leave  
  801c91:	c3                   	ret    

00801c92 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  801c92:	55                   	push   %ebp
  801c93:	89 e5                	mov    %esp,%ebp
  801c95:	56                   	push   %esi
  801c96:	53                   	push   %ebx
  801c97:	83 ec 10             	sub    $0x10,%esp
  801c9a:	8b 75 08             	mov    0x8(%ebp),%esi
  801c9d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  801ca0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ca3:	50                   	push   %eax
  801ca4:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  801caa:	c1 e8 0c             	shr    $0xc,%eax
  801cad:	50                   	push   %eax
  801cae:	e8 35 ff ff ff       	call   801be8 <fd_lookup>
  801cb3:	83 c4 08             	add    $0x8,%esp
  801cb6:	85 c0                	test   %eax,%eax
  801cb8:	78 05                	js     801cbf <fd_close+0x2d>
	    || fd != fd2)
  801cba:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  801cbd:	74 06                	je     801cc5 <fd_close+0x33>
		return (must_exist ? r : 0);
  801cbf:	84 db                	test   %bl,%bl
  801cc1:	74 47                	je     801d0a <fd_close+0x78>
  801cc3:	eb 4a                	jmp    801d0f <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801cc5:	83 ec 08             	sub    $0x8,%esp
  801cc8:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801ccb:	50                   	push   %eax
  801ccc:	ff 36                	pushl  (%esi)
  801cce:	e8 6c ff ff ff       	call   801c3f <dev_lookup>
  801cd3:	89 c3                	mov    %eax,%ebx
  801cd5:	83 c4 10             	add    $0x10,%esp
  801cd8:	85 c0                	test   %eax,%eax
  801cda:	78 1c                	js     801cf8 <fd_close+0x66>
		if (dev->dev_close)
  801cdc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801cdf:	8b 40 10             	mov    0x10(%eax),%eax
  801ce2:	85 c0                	test   %eax,%eax
  801ce4:	74 0d                	je     801cf3 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801ce6:	83 ec 0c             	sub    $0xc,%esp
  801ce9:	56                   	push   %esi
  801cea:	ff d0                	call   *%eax
  801cec:	89 c3                	mov    %eax,%ebx
  801cee:	83 c4 10             	add    $0x10,%esp
  801cf1:	eb 05                	jmp    801cf8 <fd_close+0x66>
		else
			r = 0;
  801cf3:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  801cf8:	83 ec 08             	sub    $0x8,%esp
  801cfb:	56                   	push   %esi
  801cfc:	6a 00                	push   $0x0
  801cfe:	e8 b7 f8 ff ff       	call   8015ba <sys_page_unmap>
	return r;
  801d03:	83 c4 10             	add    $0x10,%esp
  801d06:	89 d8                	mov    %ebx,%eax
  801d08:	eb 05                	jmp    801d0f <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  801d0a:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801d0f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801d12:	5b                   	pop    %ebx
  801d13:	5e                   	pop    %esi
  801d14:	5d                   	pop    %ebp
  801d15:	c3                   	ret    

00801d16 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  801d16:	55                   	push   %ebp
  801d17:	89 e5                	mov    %esp,%ebp
  801d19:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801d1c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801d1f:	50                   	push   %eax
  801d20:	ff 75 08             	pushl  0x8(%ebp)
  801d23:	e8 c0 fe ff ff       	call   801be8 <fd_lookup>
  801d28:	83 c4 08             	add    $0x8,%esp
  801d2b:	85 c0                	test   %eax,%eax
  801d2d:	78 10                	js     801d3f <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  801d2f:	83 ec 08             	sub    $0x8,%esp
  801d32:	6a 01                	push   $0x1
  801d34:	ff 75 f4             	pushl  -0xc(%ebp)
  801d37:	e8 56 ff ff ff       	call   801c92 <fd_close>
  801d3c:	83 c4 10             	add    $0x10,%esp
}
  801d3f:	c9                   	leave  
  801d40:	c3                   	ret    

00801d41 <close_all>:

void
close_all(void)
{
  801d41:	55                   	push   %ebp
  801d42:	89 e5                	mov    %esp,%ebp
  801d44:	53                   	push   %ebx
  801d45:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  801d48:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801d4d:	83 ec 0c             	sub    $0xc,%esp
  801d50:	53                   	push   %ebx
  801d51:	e8 c0 ff ff ff       	call   801d16 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801d56:	43                   	inc    %ebx
  801d57:	83 c4 10             	add    $0x10,%esp
  801d5a:	83 fb 20             	cmp    $0x20,%ebx
  801d5d:	75 ee                	jne    801d4d <close_all+0xc>
		close(i);
}
  801d5f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801d62:	c9                   	leave  
  801d63:	c3                   	ret    

00801d64 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801d64:	55                   	push   %ebp
  801d65:	89 e5                	mov    %esp,%ebp
  801d67:	57                   	push   %edi
  801d68:	56                   	push   %esi
  801d69:	53                   	push   %ebx
  801d6a:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801d6d:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801d70:	50                   	push   %eax
  801d71:	ff 75 08             	pushl  0x8(%ebp)
  801d74:	e8 6f fe ff ff       	call   801be8 <fd_lookup>
  801d79:	83 c4 08             	add    $0x8,%esp
  801d7c:	85 c0                	test   %eax,%eax
  801d7e:	0f 88 c2 00 00 00    	js     801e46 <dup+0xe2>
		return r;
	close(newfdnum);
  801d84:	83 ec 0c             	sub    $0xc,%esp
  801d87:	ff 75 0c             	pushl  0xc(%ebp)
  801d8a:	e8 87 ff ff ff       	call   801d16 <close>

	newfd = INDEX2FD(newfdnum);
  801d8f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801d92:	c1 e3 0c             	shl    $0xc,%ebx
  801d95:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801d9b:	83 c4 04             	add    $0x4,%esp
  801d9e:	ff 75 e4             	pushl  -0x1c(%ebp)
  801da1:	e8 dc fd ff ff       	call   801b82 <fd2data>
  801da6:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  801da8:	89 1c 24             	mov    %ebx,(%esp)
  801dab:	e8 d2 fd ff ff       	call   801b82 <fd2data>
  801db0:	83 c4 10             	add    $0x10,%esp
  801db3:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801db5:	89 f0                	mov    %esi,%eax
  801db7:	c1 e8 16             	shr    $0x16,%eax
  801dba:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801dc1:	a8 01                	test   $0x1,%al
  801dc3:	74 35                	je     801dfa <dup+0x96>
  801dc5:	89 f0                	mov    %esi,%eax
  801dc7:	c1 e8 0c             	shr    $0xc,%eax
  801dca:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801dd1:	f6 c2 01             	test   $0x1,%dl
  801dd4:	74 24                	je     801dfa <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801dd6:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801ddd:	83 ec 0c             	sub    $0xc,%esp
  801de0:	25 07 0e 00 00       	and    $0xe07,%eax
  801de5:	50                   	push   %eax
  801de6:	57                   	push   %edi
  801de7:	6a 00                	push   $0x0
  801de9:	56                   	push   %esi
  801dea:	6a 00                	push   $0x0
  801dec:	e8 87 f7 ff ff       	call   801578 <sys_page_map>
  801df1:	89 c6                	mov    %eax,%esi
  801df3:	83 c4 20             	add    $0x20,%esp
  801df6:	85 c0                	test   %eax,%eax
  801df8:	78 2c                	js     801e26 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  801dfa:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801dfd:	89 d0                	mov    %edx,%eax
  801dff:	c1 e8 0c             	shr    $0xc,%eax
  801e02:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801e09:	83 ec 0c             	sub    $0xc,%esp
  801e0c:	25 07 0e 00 00       	and    $0xe07,%eax
  801e11:	50                   	push   %eax
  801e12:	53                   	push   %ebx
  801e13:	6a 00                	push   $0x0
  801e15:	52                   	push   %edx
  801e16:	6a 00                	push   $0x0
  801e18:	e8 5b f7 ff ff       	call   801578 <sys_page_map>
  801e1d:	89 c6                	mov    %eax,%esi
  801e1f:	83 c4 20             	add    $0x20,%esp
  801e22:	85 c0                	test   %eax,%eax
  801e24:	79 1d                	jns    801e43 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801e26:	83 ec 08             	sub    $0x8,%esp
  801e29:	53                   	push   %ebx
  801e2a:	6a 00                	push   $0x0
  801e2c:	e8 89 f7 ff ff       	call   8015ba <sys_page_unmap>
	sys_page_unmap(0, nva);
  801e31:	83 c4 08             	add    $0x8,%esp
  801e34:	57                   	push   %edi
  801e35:	6a 00                	push   $0x0
  801e37:	e8 7e f7 ff ff       	call   8015ba <sys_page_unmap>
	return r;
  801e3c:	83 c4 10             	add    $0x10,%esp
  801e3f:	89 f0                	mov    %esi,%eax
  801e41:	eb 03                	jmp    801e46 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801e43:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801e46:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801e49:	5b                   	pop    %ebx
  801e4a:	5e                   	pop    %esi
  801e4b:	5f                   	pop    %edi
  801e4c:	5d                   	pop    %ebp
  801e4d:	c3                   	ret    

00801e4e <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801e4e:	55                   	push   %ebp
  801e4f:	89 e5                	mov    %esp,%ebp
  801e51:	53                   	push   %ebx
  801e52:	83 ec 14             	sub    $0x14,%esp
  801e55:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801e58:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801e5b:	50                   	push   %eax
  801e5c:	53                   	push   %ebx
  801e5d:	e8 86 fd ff ff       	call   801be8 <fd_lookup>
  801e62:	83 c4 08             	add    $0x8,%esp
  801e65:	85 c0                	test   %eax,%eax
  801e67:	78 67                	js     801ed0 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801e69:	83 ec 08             	sub    $0x8,%esp
  801e6c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801e6f:	50                   	push   %eax
  801e70:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801e73:	ff 30                	pushl  (%eax)
  801e75:	e8 c5 fd ff ff       	call   801c3f <dev_lookup>
  801e7a:	83 c4 10             	add    $0x10,%esp
  801e7d:	85 c0                	test   %eax,%eax
  801e7f:	78 4f                	js     801ed0 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801e81:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801e84:	8b 42 08             	mov    0x8(%edx),%eax
  801e87:	83 e0 03             	and    $0x3,%eax
  801e8a:	83 f8 01             	cmp    $0x1,%eax
  801e8d:	75 21                	jne    801eb0 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801e8f:	a1 24 54 80 00       	mov    0x805424,%eax
  801e94:	8b 40 48             	mov    0x48(%eax),%eax
  801e97:	83 ec 04             	sub    $0x4,%esp
  801e9a:	53                   	push   %ebx
  801e9b:	50                   	push   %eax
  801e9c:	68 11 39 80 00       	push   $0x803911
  801ea1:	e8 41 ec ff ff       	call   800ae7 <cprintf>
		return -E_INVAL;
  801ea6:	83 c4 10             	add    $0x10,%esp
  801ea9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801eae:	eb 20                	jmp    801ed0 <read+0x82>
	}
	if (!dev->dev_read)
  801eb0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801eb3:	8b 40 08             	mov    0x8(%eax),%eax
  801eb6:	85 c0                	test   %eax,%eax
  801eb8:	74 11                	je     801ecb <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801eba:	83 ec 04             	sub    $0x4,%esp
  801ebd:	ff 75 10             	pushl  0x10(%ebp)
  801ec0:	ff 75 0c             	pushl  0xc(%ebp)
  801ec3:	52                   	push   %edx
  801ec4:	ff d0                	call   *%eax
  801ec6:	83 c4 10             	add    $0x10,%esp
  801ec9:	eb 05                	jmp    801ed0 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801ecb:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801ed0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801ed3:	c9                   	leave  
  801ed4:	c3                   	ret    

00801ed5 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801ed5:	55                   	push   %ebp
  801ed6:	89 e5                	mov    %esp,%ebp
  801ed8:	57                   	push   %edi
  801ed9:	56                   	push   %esi
  801eda:	53                   	push   %ebx
  801edb:	83 ec 0c             	sub    $0xc,%esp
  801ede:	8b 7d 08             	mov    0x8(%ebp),%edi
  801ee1:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801ee4:	bb 00 00 00 00       	mov    $0x0,%ebx
  801ee9:	eb 21                	jmp    801f0c <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801eeb:	83 ec 04             	sub    $0x4,%esp
  801eee:	89 f0                	mov    %esi,%eax
  801ef0:	29 d8                	sub    %ebx,%eax
  801ef2:	50                   	push   %eax
  801ef3:	89 d8                	mov    %ebx,%eax
  801ef5:	03 45 0c             	add    0xc(%ebp),%eax
  801ef8:	50                   	push   %eax
  801ef9:	57                   	push   %edi
  801efa:	e8 4f ff ff ff       	call   801e4e <read>
		if (m < 0)
  801eff:	83 c4 10             	add    $0x10,%esp
  801f02:	85 c0                	test   %eax,%eax
  801f04:	78 10                	js     801f16 <readn+0x41>
			return m;
		if (m == 0)
  801f06:	85 c0                	test   %eax,%eax
  801f08:	74 0a                	je     801f14 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801f0a:	01 c3                	add    %eax,%ebx
  801f0c:	39 f3                	cmp    %esi,%ebx
  801f0e:	72 db                	jb     801eeb <readn+0x16>
  801f10:	89 d8                	mov    %ebx,%eax
  801f12:	eb 02                	jmp    801f16 <readn+0x41>
  801f14:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801f16:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f19:	5b                   	pop    %ebx
  801f1a:	5e                   	pop    %esi
  801f1b:	5f                   	pop    %edi
  801f1c:	5d                   	pop    %ebp
  801f1d:	c3                   	ret    

00801f1e <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801f1e:	55                   	push   %ebp
  801f1f:	89 e5                	mov    %esp,%ebp
  801f21:	53                   	push   %ebx
  801f22:	83 ec 14             	sub    $0x14,%esp
  801f25:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801f28:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801f2b:	50                   	push   %eax
  801f2c:	53                   	push   %ebx
  801f2d:	e8 b6 fc ff ff       	call   801be8 <fd_lookup>
  801f32:	83 c4 08             	add    $0x8,%esp
  801f35:	85 c0                	test   %eax,%eax
  801f37:	78 62                	js     801f9b <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801f39:	83 ec 08             	sub    $0x8,%esp
  801f3c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801f3f:	50                   	push   %eax
  801f40:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801f43:	ff 30                	pushl  (%eax)
  801f45:	e8 f5 fc ff ff       	call   801c3f <dev_lookup>
  801f4a:	83 c4 10             	add    $0x10,%esp
  801f4d:	85 c0                	test   %eax,%eax
  801f4f:	78 4a                	js     801f9b <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801f51:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801f54:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801f58:	75 21                	jne    801f7b <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801f5a:	a1 24 54 80 00       	mov    0x805424,%eax
  801f5f:	8b 40 48             	mov    0x48(%eax),%eax
  801f62:	83 ec 04             	sub    $0x4,%esp
  801f65:	53                   	push   %ebx
  801f66:	50                   	push   %eax
  801f67:	68 2d 39 80 00       	push   $0x80392d
  801f6c:	e8 76 eb ff ff       	call   800ae7 <cprintf>
		return -E_INVAL;
  801f71:	83 c4 10             	add    $0x10,%esp
  801f74:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801f79:	eb 20                	jmp    801f9b <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801f7b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801f7e:	8b 52 0c             	mov    0xc(%edx),%edx
  801f81:	85 d2                	test   %edx,%edx
  801f83:	74 11                	je     801f96 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801f85:	83 ec 04             	sub    $0x4,%esp
  801f88:	ff 75 10             	pushl  0x10(%ebp)
  801f8b:	ff 75 0c             	pushl  0xc(%ebp)
  801f8e:	50                   	push   %eax
  801f8f:	ff d2                	call   *%edx
  801f91:	83 c4 10             	add    $0x10,%esp
  801f94:	eb 05                	jmp    801f9b <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801f96:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801f9b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801f9e:	c9                   	leave  
  801f9f:	c3                   	ret    

00801fa0 <seek>:

int
seek(int fdnum, off_t offset)
{
  801fa0:	55                   	push   %ebp
  801fa1:	89 e5                	mov    %esp,%ebp
  801fa3:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801fa6:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801fa9:	50                   	push   %eax
  801faa:	ff 75 08             	pushl  0x8(%ebp)
  801fad:	e8 36 fc ff ff       	call   801be8 <fd_lookup>
  801fb2:	83 c4 08             	add    $0x8,%esp
  801fb5:	85 c0                	test   %eax,%eax
  801fb7:	78 0e                	js     801fc7 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801fb9:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801fbc:	8b 55 0c             	mov    0xc(%ebp),%edx
  801fbf:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801fc2:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801fc7:	c9                   	leave  
  801fc8:	c3                   	ret    

00801fc9 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801fc9:	55                   	push   %ebp
  801fca:	89 e5                	mov    %esp,%ebp
  801fcc:	53                   	push   %ebx
  801fcd:	83 ec 14             	sub    $0x14,%esp
  801fd0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801fd3:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801fd6:	50                   	push   %eax
  801fd7:	53                   	push   %ebx
  801fd8:	e8 0b fc ff ff       	call   801be8 <fd_lookup>
  801fdd:	83 c4 08             	add    $0x8,%esp
  801fe0:	85 c0                	test   %eax,%eax
  801fe2:	78 5f                	js     802043 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801fe4:	83 ec 08             	sub    $0x8,%esp
  801fe7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801fea:	50                   	push   %eax
  801feb:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801fee:	ff 30                	pushl  (%eax)
  801ff0:	e8 4a fc ff ff       	call   801c3f <dev_lookup>
  801ff5:	83 c4 10             	add    $0x10,%esp
  801ff8:	85 c0                	test   %eax,%eax
  801ffa:	78 47                	js     802043 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801ffc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801fff:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  802003:	75 21                	jne    802026 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  802005:	a1 24 54 80 00       	mov    0x805424,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80200a:	8b 40 48             	mov    0x48(%eax),%eax
  80200d:	83 ec 04             	sub    $0x4,%esp
  802010:	53                   	push   %ebx
  802011:	50                   	push   %eax
  802012:	68 f0 38 80 00       	push   $0x8038f0
  802017:	e8 cb ea ff ff       	call   800ae7 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  80201c:	83 c4 10             	add    $0x10,%esp
  80201f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  802024:	eb 1d                	jmp    802043 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  802026:	8b 55 f4             	mov    -0xc(%ebp),%edx
  802029:	8b 52 18             	mov    0x18(%edx),%edx
  80202c:	85 d2                	test   %edx,%edx
  80202e:	74 0e                	je     80203e <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  802030:	83 ec 08             	sub    $0x8,%esp
  802033:	ff 75 0c             	pushl  0xc(%ebp)
  802036:	50                   	push   %eax
  802037:	ff d2                	call   *%edx
  802039:	83 c4 10             	add    $0x10,%esp
  80203c:	eb 05                	jmp    802043 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  80203e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  802043:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802046:	c9                   	leave  
  802047:	c3                   	ret    

00802048 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  802048:	55                   	push   %ebp
  802049:	89 e5                	mov    %esp,%ebp
  80204b:	53                   	push   %ebx
  80204c:	83 ec 14             	sub    $0x14,%esp
  80204f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  802052:	8d 45 f0             	lea    -0x10(%ebp),%eax
  802055:	50                   	push   %eax
  802056:	ff 75 08             	pushl  0x8(%ebp)
  802059:	e8 8a fb ff ff       	call   801be8 <fd_lookup>
  80205e:	83 c4 08             	add    $0x8,%esp
  802061:	85 c0                	test   %eax,%eax
  802063:	78 52                	js     8020b7 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  802065:	83 ec 08             	sub    $0x8,%esp
  802068:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80206b:	50                   	push   %eax
  80206c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80206f:	ff 30                	pushl  (%eax)
  802071:	e8 c9 fb ff ff       	call   801c3f <dev_lookup>
  802076:	83 c4 10             	add    $0x10,%esp
  802079:	85 c0                	test   %eax,%eax
  80207b:	78 3a                	js     8020b7 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  80207d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802080:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  802084:	74 2c                	je     8020b2 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  802086:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  802089:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  802090:	00 00 00 
	stat->st_isdir = 0;
  802093:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  80209a:	00 00 00 
	stat->st_dev = dev;
  80209d:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8020a3:	83 ec 08             	sub    $0x8,%esp
  8020a6:	53                   	push   %ebx
  8020a7:	ff 75 f0             	pushl  -0x10(%ebp)
  8020aa:	ff 50 14             	call   *0x14(%eax)
  8020ad:	83 c4 10             	add    $0x10,%esp
  8020b0:	eb 05                	jmp    8020b7 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8020b2:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8020b7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8020ba:	c9                   	leave  
  8020bb:	c3                   	ret    

008020bc <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8020bc:	55                   	push   %ebp
  8020bd:	89 e5                	mov    %esp,%ebp
  8020bf:	56                   	push   %esi
  8020c0:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8020c1:	83 ec 08             	sub    $0x8,%esp
  8020c4:	6a 00                	push   $0x0
  8020c6:	ff 75 08             	pushl  0x8(%ebp)
  8020c9:	e8 e7 01 00 00       	call   8022b5 <open>
  8020ce:	89 c3                	mov    %eax,%ebx
  8020d0:	83 c4 10             	add    $0x10,%esp
  8020d3:	85 c0                	test   %eax,%eax
  8020d5:	78 1d                	js     8020f4 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  8020d7:	83 ec 08             	sub    $0x8,%esp
  8020da:	ff 75 0c             	pushl  0xc(%ebp)
  8020dd:	50                   	push   %eax
  8020de:	e8 65 ff ff ff       	call   802048 <fstat>
  8020e3:	89 c6                	mov    %eax,%esi
	close(fd);
  8020e5:	89 1c 24             	mov    %ebx,(%esp)
  8020e8:	e8 29 fc ff ff       	call   801d16 <close>
	return r;
  8020ed:	83 c4 10             	add    $0x10,%esp
  8020f0:	89 f0                	mov    %esi,%eax
  8020f2:	eb 00                	jmp    8020f4 <stat+0x38>
}
  8020f4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8020f7:	5b                   	pop    %ebx
  8020f8:	5e                   	pop    %esi
  8020f9:	5d                   	pop    %ebp
  8020fa:	c3                   	ret    

008020fb <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8020fb:	55                   	push   %ebp
  8020fc:	89 e5                	mov    %esp,%ebp
  8020fe:	56                   	push   %esi
  8020ff:	53                   	push   %ebx
  802100:	89 c6                	mov    %eax,%esi
  802102:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  802104:	83 3d 20 54 80 00 00 	cmpl   $0x0,0x805420
  80210b:	75 12                	jne    80211f <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  80210d:	83 ec 0c             	sub    $0xc,%esp
  802110:	6a 01                	push   $0x1
  802112:	e8 0b 0e 00 00       	call   802f22 <ipc_find_env>
  802117:	a3 20 54 80 00       	mov    %eax,0x805420
  80211c:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  80211f:	6a 07                	push   $0x7
  802121:	68 00 60 80 00       	push   $0x806000
  802126:	56                   	push   %esi
  802127:	ff 35 20 54 80 00    	pushl  0x805420
  80212d:	e8 9b 0d 00 00       	call   802ecd <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  802132:	83 c4 0c             	add    $0xc,%esp
  802135:	6a 00                	push   $0x0
  802137:	53                   	push   %ebx
  802138:	6a 00                	push   $0x0
  80213a:	e8 26 0d 00 00       	call   802e65 <ipc_recv>
}
  80213f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802142:	5b                   	pop    %ebx
  802143:	5e                   	pop    %esi
  802144:	5d                   	pop    %ebp
  802145:	c3                   	ret    

00802146 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  802146:	55                   	push   %ebp
  802147:	89 e5                	mov    %esp,%ebp
  802149:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  80214c:	8b 45 08             	mov    0x8(%ebp),%eax
  80214f:	8b 40 0c             	mov    0xc(%eax),%eax
  802152:	a3 00 60 80 00       	mov    %eax,0x806000
	fsipcbuf.set_size.req_size = newsize;
  802157:	8b 45 0c             	mov    0xc(%ebp),%eax
  80215a:	a3 04 60 80 00       	mov    %eax,0x806004
	return fsipc(FSREQ_SET_SIZE, NULL);
  80215f:	ba 00 00 00 00       	mov    $0x0,%edx
  802164:	b8 02 00 00 00       	mov    $0x2,%eax
  802169:	e8 8d ff ff ff       	call   8020fb <fsipc>
}
  80216e:	c9                   	leave  
  80216f:	c3                   	ret    

00802170 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  802170:	55                   	push   %ebp
  802171:	89 e5                	mov    %esp,%ebp
  802173:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  802176:	8b 45 08             	mov    0x8(%ebp),%eax
  802179:	8b 40 0c             	mov    0xc(%eax),%eax
  80217c:	a3 00 60 80 00       	mov    %eax,0x806000
	return fsipc(FSREQ_FLUSH, NULL);
  802181:	ba 00 00 00 00       	mov    $0x0,%edx
  802186:	b8 06 00 00 00       	mov    $0x6,%eax
  80218b:	e8 6b ff ff ff       	call   8020fb <fsipc>
}
  802190:	c9                   	leave  
  802191:	c3                   	ret    

00802192 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  802192:	55                   	push   %ebp
  802193:	89 e5                	mov    %esp,%ebp
  802195:	53                   	push   %ebx
  802196:	83 ec 04             	sub    $0x4,%esp
  802199:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  80219c:	8b 45 08             	mov    0x8(%ebp),%eax
  80219f:	8b 40 0c             	mov    0xc(%eax),%eax
  8021a2:	a3 00 60 80 00       	mov    %eax,0x806000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8021a7:	ba 00 00 00 00       	mov    $0x0,%edx
  8021ac:	b8 05 00 00 00       	mov    $0x5,%eax
  8021b1:	e8 45 ff ff ff       	call   8020fb <fsipc>
  8021b6:	85 c0                	test   %eax,%eax
  8021b8:	78 2c                	js     8021e6 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  8021ba:	83 ec 08             	sub    $0x8,%esp
  8021bd:	68 00 60 80 00       	push   $0x806000
  8021c2:	53                   	push   %ebx
  8021c3:	e8 89 ef ff ff       	call   801151 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  8021c8:	a1 80 60 80 00       	mov    0x806080,%eax
  8021cd:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  8021d3:	a1 84 60 80 00       	mov    0x806084,%eax
  8021d8:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  8021de:	83 c4 10             	add    $0x10,%esp
  8021e1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8021e6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8021e9:	c9                   	leave  
  8021ea:	c3                   	ret    

008021eb <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  8021eb:	55                   	push   %ebp
  8021ec:	89 e5                	mov    %esp,%ebp
  8021ee:	83 ec 08             	sub    $0x8,%esp
  8021f1:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  8021f4:	8b 55 08             	mov    0x8(%ebp),%edx
  8021f7:	8b 52 0c             	mov    0xc(%edx),%edx
  8021fa:	89 15 00 60 80 00    	mov    %edx,0x806000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  802200:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  802205:	76 05                	jbe    80220c <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  802207:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  80220c:	a3 04 60 80 00       	mov    %eax,0x806004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  802211:	83 ec 04             	sub    $0x4,%esp
  802214:	50                   	push   %eax
  802215:	ff 75 0c             	pushl  0xc(%ebp)
  802218:	68 08 60 80 00       	push   $0x806008
  80221d:	e8 a4 f0 ff ff       	call   8012c6 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  802222:	ba 00 00 00 00       	mov    $0x0,%edx
  802227:	b8 04 00 00 00       	mov    $0x4,%eax
  80222c:	e8 ca fe ff ff       	call   8020fb <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  802231:	c9                   	leave  
  802232:	c3                   	ret    

00802233 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  802233:	55                   	push   %ebp
  802234:	89 e5                	mov    %esp,%ebp
  802236:	56                   	push   %esi
  802237:	53                   	push   %ebx
  802238:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  80223b:	8b 45 08             	mov    0x8(%ebp),%eax
  80223e:	8b 40 0c             	mov    0xc(%eax),%eax
  802241:	a3 00 60 80 00       	mov    %eax,0x806000
	fsipcbuf.read.req_n = n;
  802246:	89 35 04 60 80 00    	mov    %esi,0x806004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  80224c:	ba 00 00 00 00       	mov    $0x0,%edx
  802251:	b8 03 00 00 00       	mov    $0x3,%eax
  802256:	e8 a0 fe ff ff       	call   8020fb <fsipc>
  80225b:	89 c3                	mov    %eax,%ebx
  80225d:	85 c0                	test   %eax,%eax
  80225f:	78 4b                	js     8022ac <devfile_read+0x79>
		return r;
	assert(r <= n);
  802261:	39 c6                	cmp    %eax,%esi
  802263:	73 16                	jae    80227b <devfile_read+0x48>
  802265:	68 5c 39 80 00       	push   $0x80395c
  80226a:	68 6f 33 80 00       	push   $0x80336f
  80226f:	6a 7c                	push   $0x7c
  802271:	68 63 39 80 00       	push   $0x803963
  802276:	e8 94 e7 ff ff       	call   800a0f <_panic>
	assert(r <= PGSIZE);
  80227b:	3d 00 10 00 00       	cmp    $0x1000,%eax
  802280:	7e 16                	jle    802298 <devfile_read+0x65>
  802282:	68 6e 39 80 00       	push   $0x80396e
  802287:	68 6f 33 80 00       	push   $0x80336f
  80228c:	6a 7d                	push   $0x7d
  80228e:	68 63 39 80 00       	push   $0x803963
  802293:	e8 77 e7 ff ff       	call   800a0f <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  802298:	83 ec 04             	sub    $0x4,%esp
  80229b:	50                   	push   %eax
  80229c:	68 00 60 80 00       	push   $0x806000
  8022a1:	ff 75 0c             	pushl  0xc(%ebp)
  8022a4:	e8 1d f0 ff ff       	call   8012c6 <memmove>
	return r;
  8022a9:	83 c4 10             	add    $0x10,%esp
}
  8022ac:	89 d8                	mov    %ebx,%eax
  8022ae:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8022b1:	5b                   	pop    %ebx
  8022b2:	5e                   	pop    %esi
  8022b3:	5d                   	pop    %ebp
  8022b4:	c3                   	ret    

008022b5 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  8022b5:	55                   	push   %ebp
  8022b6:	89 e5                	mov    %esp,%ebp
  8022b8:	53                   	push   %ebx
  8022b9:	83 ec 20             	sub    $0x20,%esp
  8022bc:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  8022bf:	53                   	push   %ebx
  8022c0:	e8 57 ee ff ff       	call   80111c <strlen>
  8022c5:	83 c4 10             	add    $0x10,%esp
  8022c8:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  8022cd:	7f 63                	jg     802332 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  8022cf:	83 ec 0c             	sub    $0xc,%esp
  8022d2:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8022d5:	50                   	push   %eax
  8022d6:	e8 be f8 ff ff       	call   801b99 <fd_alloc>
  8022db:	83 c4 10             	add    $0x10,%esp
  8022de:	85 c0                	test   %eax,%eax
  8022e0:	78 55                	js     802337 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  8022e2:	83 ec 08             	sub    $0x8,%esp
  8022e5:	53                   	push   %ebx
  8022e6:	68 00 60 80 00       	push   $0x806000
  8022eb:	e8 61 ee ff ff       	call   801151 <strcpy>
	fsipcbuf.open.req_omode = mode;
  8022f0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8022f3:	a3 00 64 80 00       	mov    %eax,0x806400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  8022f8:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8022fb:	b8 01 00 00 00       	mov    $0x1,%eax
  802300:	e8 f6 fd ff ff       	call   8020fb <fsipc>
  802305:	89 c3                	mov    %eax,%ebx
  802307:	83 c4 10             	add    $0x10,%esp
  80230a:	85 c0                	test   %eax,%eax
  80230c:	79 14                	jns    802322 <open+0x6d>
		fd_close(fd, 0);
  80230e:	83 ec 08             	sub    $0x8,%esp
  802311:	6a 00                	push   $0x0
  802313:	ff 75 f4             	pushl  -0xc(%ebp)
  802316:	e8 77 f9 ff ff       	call   801c92 <fd_close>
		return r;
  80231b:	83 c4 10             	add    $0x10,%esp
  80231e:	89 d8                	mov    %ebx,%eax
  802320:	eb 15                	jmp    802337 <open+0x82>
	}

	return fd2num(fd);
  802322:	83 ec 0c             	sub    $0xc,%esp
  802325:	ff 75 f4             	pushl  -0xc(%ebp)
  802328:	e8 45 f8 ff ff       	call   801b72 <fd2num>
  80232d:	83 c4 10             	add    $0x10,%esp
  802330:	eb 05                	jmp    802337 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  802332:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  802337:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80233a:	c9                   	leave  
  80233b:	c3                   	ret    

0080233c <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  80233c:	55                   	push   %ebp
  80233d:	89 e5                	mov    %esp,%ebp
  80233f:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  802342:	ba 00 00 00 00       	mov    $0x0,%edx
  802347:	b8 08 00 00 00       	mov    $0x8,%eax
  80234c:	e8 aa fd ff ff       	call   8020fb <fsipc>
}
  802351:	c9                   	leave  
  802352:	c3                   	ret    

00802353 <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  802353:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  802357:	7e 38                	jle    802391 <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  802359:	55                   	push   %ebp
  80235a:	89 e5                	mov    %esp,%ebp
  80235c:	53                   	push   %ebx
  80235d:	83 ec 08             	sub    $0x8,%esp
  802360:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  802362:	ff 70 04             	pushl  0x4(%eax)
  802365:	8d 40 10             	lea    0x10(%eax),%eax
  802368:	50                   	push   %eax
  802369:	ff 33                	pushl  (%ebx)
  80236b:	e8 ae fb ff ff       	call   801f1e <write>
		if (result > 0)
  802370:	83 c4 10             	add    $0x10,%esp
  802373:	85 c0                	test   %eax,%eax
  802375:	7e 03                	jle    80237a <writebuf+0x27>
			b->result += result;
  802377:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  80237a:	3b 43 04             	cmp    0x4(%ebx),%eax
  80237d:	74 0e                	je     80238d <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  80237f:	89 c2                	mov    %eax,%edx
  802381:	85 c0                	test   %eax,%eax
  802383:	7e 05                	jle    80238a <writebuf+0x37>
  802385:	ba 00 00 00 00       	mov    $0x0,%edx
  80238a:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  80238d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802390:	c9                   	leave  
  802391:	c3                   	ret    

00802392 <putch>:

static void
putch(int ch, void *thunk)
{
  802392:	55                   	push   %ebp
  802393:	89 e5                	mov    %esp,%ebp
  802395:	53                   	push   %ebx
  802396:	83 ec 04             	sub    $0x4,%esp
  802399:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  80239c:	8b 53 04             	mov    0x4(%ebx),%edx
  80239f:	8d 42 01             	lea    0x1(%edx),%eax
  8023a2:	89 43 04             	mov    %eax,0x4(%ebx)
  8023a5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8023a8:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  8023ac:	3d 00 01 00 00       	cmp    $0x100,%eax
  8023b1:	75 0e                	jne    8023c1 <putch+0x2f>
		writebuf(b);
  8023b3:	89 d8                	mov    %ebx,%eax
  8023b5:	e8 99 ff ff ff       	call   802353 <writebuf>
		b->idx = 0;
  8023ba:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  8023c1:	83 c4 04             	add    $0x4,%esp
  8023c4:	5b                   	pop    %ebx
  8023c5:	5d                   	pop    %ebp
  8023c6:	c3                   	ret    

008023c7 <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  8023c7:	55                   	push   %ebp
  8023c8:	89 e5                	mov    %esp,%ebp
  8023ca:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  8023d0:	8b 45 08             	mov    0x8(%ebp),%eax
  8023d3:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  8023d9:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  8023e0:	00 00 00 
	b.result = 0;
  8023e3:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8023ea:	00 00 00 
	b.error = 1;
  8023ed:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  8023f4:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  8023f7:	ff 75 10             	pushl  0x10(%ebp)
  8023fa:	ff 75 0c             	pushl  0xc(%ebp)
  8023fd:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  802403:	50                   	push   %eax
  802404:	68 92 23 80 00       	push   $0x802392
  802409:	e8 0d e8 ff ff       	call   800c1b <vprintfmt>
	if (b.idx > 0)
  80240e:	83 c4 10             	add    $0x10,%esp
  802411:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  802418:	7e 0b                	jle    802425 <vfprintf+0x5e>
		writebuf(&b);
  80241a:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  802420:	e8 2e ff ff ff       	call   802353 <writebuf>

	return (b.result ? b.result : b.error);
  802425:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  80242b:	85 c0                	test   %eax,%eax
  80242d:	75 06                	jne    802435 <vfprintf+0x6e>
  80242f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  802435:	c9                   	leave  
  802436:	c3                   	ret    

00802437 <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  802437:	55                   	push   %ebp
  802438:	89 e5                	mov    %esp,%ebp
  80243a:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80243d:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  802440:	50                   	push   %eax
  802441:	ff 75 0c             	pushl  0xc(%ebp)
  802444:	ff 75 08             	pushl  0x8(%ebp)
  802447:	e8 7b ff ff ff       	call   8023c7 <vfprintf>
	va_end(ap);

	return cnt;
}
  80244c:	c9                   	leave  
  80244d:	c3                   	ret    

0080244e <printf>:

int
printf(const char *fmt, ...)
{
  80244e:	55                   	push   %ebp
  80244f:	89 e5                	mov    %esp,%ebp
  802451:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  802454:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  802457:	50                   	push   %eax
  802458:	ff 75 08             	pushl  0x8(%ebp)
  80245b:	6a 01                	push   $0x1
  80245d:	e8 65 ff ff ff       	call   8023c7 <vfprintf>
	va_end(ap);

	return cnt;
}
  802462:	c9                   	leave  
  802463:	c3                   	ret    

00802464 <spawn>:
// argv: pointer to null-terminated array of pointers to strings,
// 	 which will be passed to the child as its command-line arguments.
// Returns child envid on success, < 0 on failure.
int
spawn(const char *prog, const char **argv)
{
  802464:	55                   	push   %ebp
  802465:	89 e5                	mov    %esp,%ebp
  802467:	57                   	push   %edi
  802468:	56                   	push   %esi
  802469:	53                   	push   %ebx
  80246a:	81 ec 94 02 00 00    	sub    $0x294,%esp
  802470:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//   - Call sys_env_set_trapframe(child, &child_tf) to set up the
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
  802473:	6a 00                	push   $0x0
  802475:	ff 75 08             	pushl  0x8(%ebp)
  802478:	e8 38 fe ff ff       	call   8022b5 <open>
  80247d:	89 c1                	mov    %eax,%ecx
  80247f:	89 85 8c fd ff ff    	mov    %eax,-0x274(%ebp)
  802485:	83 c4 10             	add    $0x10,%esp
  802488:	85 c0                	test   %eax,%eax
  80248a:	0f 88 e2 04 00 00    	js     802972 <spawn+0x50e>
		return r;
	fd = r;

	// Read elf header
	elf = (struct Elf*) elf_buf;
	if (readn(fd, elf_buf, sizeof(elf_buf)) != sizeof(elf_buf)
  802490:	83 ec 04             	sub    $0x4,%esp
  802493:	68 00 02 00 00       	push   $0x200
  802498:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  80249e:	50                   	push   %eax
  80249f:	51                   	push   %ecx
  8024a0:	e8 30 fa ff ff       	call   801ed5 <readn>
  8024a5:	83 c4 10             	add    $0x10,%esp
  8024a8:	3d 00 02 00 00       	cmp    $0x200,%eax
  8024ad:	75 0c                	jne    8024bb <spawn+0x57>
	    || elf->e_magic != ELF_MAGIC) {
  8024af:	81 bd e8 fd ff ff 7f 	cmpl   $0x464c457f,-0x218(%ebp)
  8024b6:	45 4c 46 
  8024b9:	74 33                	je     8024ee <spawn+0x8a>
		close(fd);
  8024bb:	83 ec 0c             	sub    $0xc,%esp
  8024be:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8024c4:	e8 4d f8 ff ff       	call   801d16 <close>
		cprintf("elf magic %08x want %08x\n", elf->e_magic, ELF_MAGIC);
  8024c9:	83 c4 0c             	add    $0xc,%esp
  8024cc:	68 7f 45 4c 46       	push   $0x464c457f
  8024d1:	ff b5 e8 fd ff ff    	pushl  -0x218(%ebp)
  8024d7:	68 7a 39 80 00       	push   $0x80397a
  8024dc:	e8 06 e6 ff ff       	call   800ae7 <cprintf>
		return -E_NOT_EXEC;
  8024e1:	83 c4 10             	add    $0x10,%esp
  8024e4:	bb f2 ff ff ff       	mov    $0xfffffff2,%ebx
  8024e9:	e9 e4 04 00 00       	jmp    8029d2 <spawn+0x56e>
  8024ee:	b8 07 00 00 00       	mov    $0x7,%eax
  8024f3:	cd 30                	int    $0x30
  8024f5:	89 85 74 fd ff ff    	mov    %eax,-0x28c(%ebp)
  8024fb:	89 85 84 fd ff ff    	mov    %eax,-0x27c(%ebp)
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
  802501:	85 c0                	test   %eax,%eax
  802503:	0f 88 71 04 00 00    	js     80297a <spawn+0x516>
		return r;
	child = r;

	// Set up trap frame, including initial stack.
	child_tf = envs[ENVX(child)].env_tf;
  802509:	25 ff 03 00 00       	and    $0x3ff,%eax
  80250e:	89 c6                	mov    %eax,%esi
  802510:	8d 04 85 00 00 00 00 	lea    0x0(,%eax,4),%eax
  802517:	c1 e6 07             	shl    $0x7,%esi
  80251a:	29 c6                	sub    %eax,%esi
  80251c:	81 c6 00 00 c0 ee    	add    $0xeec00000,%esi
  802522:	8d bd a4 fd ff ff    	lea    -0x25c(%ebp),%edi
  802528:	b9 11 00 00 00       	mov    $0x11,%ecx
  80252d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	child_tf.tf_eip = elf->e_entry;
  80252f:	8b 85 00 fe ff ff    	mov    -0x200(%ebp),%eax
  802535:	89 85 d4 fd ff ff    	mov    %eax,-0x22c(%ebp)
  80253b:	ba 00 00 00 00       	mov    $0x0,%edx
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  802540:	b8 00 00 00 00       	mov    $0x0,%eax
	char *string_store;
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
  802545:	bf 00 00 00 00       	mov    $0x0,%edi
  80254a:	89 5d 0c             	mov    %ebx,0xc(%ebp)
  80254d:	89 c3                	mov    %eax,%ebx
  80254f:	eb 13                	jmp    802564 <spawn+0x100>
	for (argc = 0; argv[argc] != 0; argc++)
		string_size += strlen(argv[argc]) + 1;
  802551:	83 ec 0c             	sub    $0xc,%esp
  802554:	50                   	push   %eax
  802555:	e8 c2 eb ff ff       	call   80111c <strlen>
  80255a:	8d 7c 38 01          	lea    0x1(%eax,%edi,1),%edi
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  80255e:	43                   	inc    %ebx
  80255f:	89 f2                	mov    %esi,%edx
  802561:	83 c4 10             	add    $0x10,%esp
  802564:	89 d9                	mov    %ebx,%ecx
  802566:	8d 72 04             	lea    0x4(%edx),%esi
  802569:	8b 45 0c             	mov    0xc(%ebp),%eax
  80256c:	8b 44 30 fc          	mov    -0x4(%eax,%esi,1),%eax
  802570:	85 c0                	test   %eax,%eax
  802572:	75 dd                	jne    802551 <spawn+0xed>
  802574:	89 9d 90 fd ff ff    	mov    %ebx,-0x270(%ebp)
  80257a:	89 9d 88 fd ff ff    	mov    %ebx,-0x278(%ebp)
  802580:	89 95 80 fd ff ff    	mov    %edx,-0x280(%ebp)
  802586:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Determine where to place the strings and the argv array.
	// Set up pointers into the temporary page 'UTEMP'; we'll map a page
	// there later, then remap that page into the child environment
	// at (USTACKTOP - PGSIZE).
	// strings is the topmost thing on the stack.
	string_store = (char*) UTEMP + PGSIZE - string_size;
  802589:	b8 00 10 40 00       	mov    $0x401000,%eax
  80258e:	29 f8                	sub    %edi,%eax
  802590:	89 c7                	mov    %eax,%edi
	// argv is below that.  There's one argument pointer per argument, plus
	// a null pointer.
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));
  802592:	89 c2                	mov    %eax,%edx
  802594:	83 e2 fc             	and    $0xfffffffc,%edx
  802597:	8d 04 8d 04 00 00 00 	lea    0x4(,%ecx,4),%eax
  80259e:	29 c2                	sub    %eax,%edx
  8025a0:	89 95 94 fd ff ff    	mov    %edx,-0x26c(%ebp)

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
  8025a6:	8d 42 f8             	lea    -0x8(%edx),%eax
  8025a9:	3d ff ff 3f 00       	cmp    $0x3fffff,%eax
  8025ae:	0f 86 d6 03 00 00    	jbe    80298a <spawn+0x526>
		return -E_NO_MEM;

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  8025b4:	83 ec 04             	sub    $0x4,%esp
  8025b7:	6a 07                	push   $0x7
  8025b9:	68 00 00 40 00       	push   $0x400000
  8025be:	6a 00                	push   $0x0
  8025c0:	e8 70 ef ff ff       	call   801535 <sys_page_alloc>
  8025c5:	83 c4 10             	add    $0x10,%esp
  8025c8:	85 c0                	test   %eax,%eax
  8025ca:	0f 88 c1 03 00 00    	js     802991 <spawn+0x52d>
  8025d0:	be 00 00 00 00       	mov    $0x0,%esi
  8025d5:	eb 2e                	jmp    802605 <spawn+0x1a1>
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
		argv_store[i] = UTEMP2USTACK(string_store);
  8025d7:	8d 87 00 d0 7f ee    	lea    -0x11803000(%edi),%eax
  8025dd:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  8025e3:	89 04 b1             	mov    %eax,(%ecx,%esi,4)
		strcpy(string_store, argv[i]);
  8025e6:	83 ec 08             	sub    $0x8,%esp
  8025e9:	ff 34 b3             	pushl  (%ebx,%esi,4)
  8025ec:	57                   	push   %edi
  8025ed:	e8 5f eb ff ff       	call   801151 <strcpy>
		string_store += strlen(argv[i]) + 1;
  8025f2:	83 c4 04             	add    $0x4,%esp
  8025f5:	ff 34 b3             	pushl  (%ebx,%esi,4)
  8025f8:	e8 1f eb ff ff       	call   80111c <strlen>
  8025fd:	8d 7c 07 01          	lea    0x1(%edi,%eax,1),%edi
	//	  (Again, argv should use an address valid in the child's
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
  802601:	46                   	inc    %esi
  802602:	83 c4 10             	add    $0x10,%esp
  802605:	39 b5 90 fd ff ff    	cmp    %esi,-0x270(%ebp)
  80260b:	7f ca                	jg     8025d7 <spawn+0x173>
		argv_store[i] = UTEMP2USTACK(string_store);
		strcpy(string_store, argv[i]);
		string_store += strlen(argv[i]) + 1;
	}
	argv_store[argc] = 0;
  80260d:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  802613:	8b 8d 80 fd ff ff    	mov    -0x280(%ebp),%ecx
  802619:	c7 04 08 00 00 00 00 	movl   $0x0,(%eax,%ecx,1)
	assert(string_store == (char*)UTEMP + PGSIZE);
  802620:	81 ff 00 10 40 00    	cmp    $0x401000,%edi
  802626:	74 19                	je     802641 <spawn+0x1dd>
  802628:	68 04 3a 80 00       	push   $0x803a04
  80262d:	68 6f 33 80 00       	push   $0x80336f
  802632:	68 f1 00 00 00       	push   $0xf1
  802637:	68 94 39 80 00       	push   $0x803994
  80263c:	e8 ce e3 ff ff       	call   800a0f <_panic>

	argv_store[-1] = UTEMP2USTACK(argv_store);
  802641:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  802647:	89 c8                	mov    %ecx,%eax
  802649:	2d 00 30 80 11       	sub    $0x11803000,%eax
  80264e:	89 41 fc             	mov    %eax,-0x4(%ecx)
	argv_store[-2] = argc;
  802651:	89 c8                	mov    %ecx,%eax
  802653:	8b 8d 88 fd ff ff    	mov    -0x278(%ebp),%ecx
  802659:	89 48 f8             	mov    %ecx,-0x8(%eax)

	*init_esp = UTEMP2USTACK(&argv_store[-2]);
  80265c:	2d 08 30 80 11       	sub    $0x11803008,%eax
  802661:	89 85 e0 fd ff ff    	mov    %eax,-0x220(%ebp)

	// After completing the stack, map it into the child's address space
	// and unmap it from ours!
	if ((r = sys_page_map(0, UTEMP, child, (void*) (USTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W)) < 0)
  802667:	83 ec 0c             	sub    $0xc,%esp
  80266a:	6a 07                	push   $0x7
  80266c:	68 00 d0 bf ee       	push   $0xeebfd000
  802671:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  802677:	68 00 00 40 00       	push   $0x400000
  80267c:	6a 00                	push   $0x0
  80267e:	e8 f5 ee ff ff       	call   801578 <sys_page_map>
  802683:	89 c3                	mov    %eax,%ebx
  802685:	83 c4 20             	add    $0x20,%esp
  802688:	85 c0                	test   %eax,%eax
  80268a:	0f 88 30 03 00 00    	js     8029c0 <spawn+0x55c>
		goto error;
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  802690:	83 ec 08             	sub    $0x8,%esp
  802693:	68 00 00 40 00       	push   $0x400000
  802698:	6a 00                	push   $0x0
  80269a:	e8 1b ef ff ff       	call   8015ba <sys_page_unmap>
  80269f:	89 c3                	mov    %eax,%ebx
  8026a1:	83 c4 10             	add    $0x10,%esp
  8026a4:	85 c0                	test   %eax,%eax
  8026a6:	0f 88 14 03 00 00    	js     8029c0 <spawn+0x55c>

	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
  8026ac:	8b 85 04 fe ff ff    	mov    -0x1fc(%ebp),%eax
  8026b2:	8d 84 05 e8 fd ff ff 	lea    -0x218(%ebp,%eax,1),%eax
  8026b9:	89 85 7c fd ff ff    	mov    %eax,-0x284(%ebp)
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  8026bf:	c7 85 78 fd ff ff 00 	movl   $0x0,-0x288(%ebp)
  8026c6:	00 00 00 
  8026c9:	e9 88 01 00 00       	jmp    802856 <spawn+0x3f2>
		if (ph->p_type != ELF_PROG_LOAD)
  8026ce:	8b 85 7c fd ff ff    	mov    -0x284(%ebp),%eax
  8026d4:	83 38 01             	cmpl   $0x1,(%eax)
  8026d7:	0f 85 6c 01 00 00    	jne    802849 <spawn+0x3e5>
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
  8026dd:	89 c1                	mov    %eax,%ecx
  8026df:	8b 40 18             	mov    0x18(%eax),%eax
  8026e2:	83 e0 02             	and    $0x2,%eax
			perm |= PTE_W;
  8026e5:	83 f8 01             	cmp    $0x1,%eax
  8026e8:	19 c0                	sbb    %eax,%eax
  8026ea:	83 e0 fe             	and    $0xfffffffe,%eax
  8026ed:	83 c0 07             	add    $0x7,%eax
  8026f0:	89 85 88 fd ff ff    	mov    %eax,-0x278(%ebp)
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
  8026f6:	89 c8                	mov    %ecx,%eax
  8026f8:	8b 49 04             	mov    0x4(%ecx),%ecx
  8026fb:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
  802701:	8b 78 10             	mov    0x10(%eax),%edi
  802704:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  80270a:	8b 70 14             	mov    0x14(%eax),%esi
  80270d:	89 f2                	mov    %esi,%edx
  80270f:	89 b5 90 fd ff ff    	mov    %esi,-0x270(%ebp)
  802715:	8b 70 08             	mov    0x8(%eax),%esi
	int i, r;
	void *blk;

	//cprintf("map_segment %x+%x\n", va, memsz);

	if ((i = PGOFF(va))) {
  802718:	89 f0                	mov    %esi,%eax
  80271a:	25 ff 0f 00 00       	and    $0xfff,%eax
  80271f:	74 1a                	je     80273b <spawn+0x2d7>
		va -= i;
  802721:	29 c6                	sub    %eax,%esi
		memsz += i;
  802723:	01 c2                	add    %eax,%edx
  802725:	89 95 90 fd ff ff    	mov    %edx,-0x270(%ebp)
		filesz += i;
  80272b:	01 c7                	add    %eax,%edi
  80272d:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
		fileoffset -= i;
  802733:	29 c1                	sub    %eax,%ecx
  802735:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  80273b:	bb 00 00 00 00       	mov    $0x0,%ebx
  802740:	e9 f6 00 00 00       	jmp    80283b <spawn+0x3d7>
		if (i >= filesz) {
  802745:	39 9d 94 fd ff ff    	cmp    %ebx,-0x26c(%ebp)
  80274b:	77 27                	ja     802774 <spawn+0x310>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
  80274d:	83 ec 04             	sub    $0x4,%esp
  802750:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  802756:	56                   	push   %esi
  802757:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  80275d:	e8 d3 ed ff ff       	call   801535 <sys_page_alloc>
  802762:	83 c4 10             	add    $0x10,%esp
  802765:	85 c0                	test   %eax,%eax
  802767:	0f 89 c2 00 00 00    	jns    80282f <spawn+0x3cb>
  80276d:	89 c3                	mov    %eax,%ebx
  80276f:	e9 2b 02 00 00       	jmp    80299f <spawn+0x53b>
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  802774:	83 ec 04             	sub    $0x4,%esp
  802777:	6a 07                	push   $0x7
  802779:	68 00 00 40 00       	push   $0x400000
  80277e:	6a 00                	push   $0x0
  802780:	e8 b0 ed ff ff       	call   801535 <sys_page_alloc>
  802785:	83 c4 10             	add    $0x10,%esp
  802788:	85 c0                	test   %eax,%eax
  80278a:	0f 88 05 02 00 00    	js     802995 <spawn+0x531>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  802790:	83 ec 08             	sub    $0x8,%esp
  802793:	8b 85 80 fd ff ff    	mov    -0x280(%ebp),%eax
  802799:	01 f8                	add    %edi,%eax
  80279b:	50                   	push   %eax
  80279c:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8027a2:	e8 f9 f7 ff ff       	call   801fa0 <seek>
  8027a7:	83 c4 10             	add    $0x10,%esp
  8027aa:	85 c0                	test   %eax,%eax
  8027ac:	0f 88 e7 01 00 00    	js     802999 <spawn+0x535>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  8027b2:	83 ec 04             	sub    $0x4,%esp
  8027b5:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  8027bb:	29 f8                	sub    %edi,%eax
  8027bd:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8027c2:	76 05                	jbe    8027c9 <spawn+0x365>
  8027c4:	b8 00 10 00 00       	mov    $0x1000,%eax
  8027c9:	50                   	push   %eax
  8027ca:	68 00 00 40 00       	push   $0x400000
  8027cf:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8027d5:	e8 fb f6 ff ff       	call   801ed5 <readn>
  8027da:	83 c4 10             	add    $0x10,%esp
  8027dd:	85 c0                	test   %eax,%eax
  8027df:	0f 88 b8 01 00 00    	js     80299d <spawn+0x539>
				return r;
			if ((r = sys_page_map(0, UTEMP, child, (void*) (va + i), perm)) < 0)
  8027e5:	83 ec 0c             	sub    $0xc,%esp
  8027e8:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  8027ee:	56                   	push   %esi
  8027ef:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  8027f5:	68 00 00 40 00       	push   $0x400000
  8027fa:	6a 00                	push   $0x0
  8027fc:	e8 77 ed ff ff       	call   801578 <sys_page_map>
  802801:	83 c4 20             	add    $0x20,%esp
  802804:	85 c0                	test   %eax,%eax
  802806:	79 15                	jns    80281d <spawn+0x3b9>
				panic("spawn: sys_page_map data: %e", r);
  802808:	50                   	push   %eax
  802809:	68 a0 39 80 00       	push   $0x8039a0
  80280e:	68 24 01 00 00       	push   $0x124
  802813:	68 94 39 80 00       	push   $0x803994
  802818:	e8 f2 e1 ff ff       	call   800a0f <_panic>
			sys_page_unmap(0, UTEMP);
  80281d:	83 ec 08             	sub    $0x8,%esp
  802820:	68 00 00 40 00       	push   $0x400000
  802825:	6a 00                	push   $0x0
  802827:	e8 8e ed ff ff       	call   8015ba <sys_page_unmap>
  80282c:	83 c4 10             	add    $0x10,%esp
		memsz += i;
		filesz += i;
		fileoffset -= i;
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  80282f:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  802835:	81 c6 00 10 00 00    	add    $0x1000,%esi
  80283b:	89 df                	mov    %ebx,%edi
  80283d:	39 9d 90 fd ff ff    	cmp    %ebx,-0x270(%ebp)
  802843:	0f 87 fc fe ff ff    	ja     802745 <spawn+0x2e1>
	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  802849:	ff 85 78 fd ff ff    	incl   -0x288(%ebp)
  80284f:	83 85 7c fd ff ff 20 	addl   $0x20,-0x284(%ebp)
  802856:	0f b7 85 14 fe ff ff 	movzwl -0x1ec(%ebp),%eax
  80285d:	39 85 78 fd ff ff    	cmp    %eax,-0x288(%ebp)
  802863:	0f 8c 65 fe ff ff    	jl     8026ce <spawn+0x26a>
			perm |= PTE_W;
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
				     fd, ph->p_filesz, ph->p_offset, perm)) < 0)
			goto error;
	}
	close(fd);
  802869:	83 ec 0c             	sub    $0xc,%esp
  80286c:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  802872:	e8 9f f4 ff ff       	call   801d16 <close>
  802877:	83 c4 10             	add    $0x10,%esp
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80287a:	bf 00 00 00 00       	mov    $0x0,%edi
  80287f:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  802885:	8b bd 84 fd ff ff    	mov    -0x27c(%ebp),%edi
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  80288b:	8b b5 94 fd ff ff    	mov    -0x26c(%ebp),%esi
  802891:	8b 04 b5 00 d0 7b ef 	mov    -0x10843000(,%esi,4),%eax
  802898:	a8 01                	test   $0x1,%al
  80289a:	74 4b                	je     8028e7 <spawn+0x483>
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
        	/* LAB 4: Your code here.
		    pte_t pte = uvpt[pn];
		    void *va = (void *)(pn << PGSHIFT);
		    int perm = pte & PTE_SYSCALL;*/
		    unsigned pn = (ipd << 10) | ipt;
  80289c:	c1 e6 0a             	shl    $0xa,%esi
  80289f:	bb 00 00 00 00       	mov    $0x0,%ebx
  8028a4:	89 f0                	mov    %esi,%eax
  8028a6:	09 d8                	or     %ebx,%eax
        	if(!(uvpt[pn] & PTE_P)){ //doesn't exist
  8028a8:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  8028af:	f6 c2 01             	test   $0x1,%dl
  8028b2:	74 2a                	je     8028de <spawn+0x47a>
        		continue;
        	}
	        int perm = uvpt[pn] & PTE_SYSCALL;
  8028b4:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
	        if(perm & PTE_SHARE){
  8028bb:	f6 c6 04             	test   $0x4,%dh
  8028be:	74 1e                	je     8028de <spawn+0x47a>
			    void *va = (void *)(pn << PGSHIFT);
  8028c0:	c1 e0 0c             	shl    $0xc,%eax
			    r = sys_page_map(0, va, child, va, perm);
  8028c3:	83 ec 0c             	sub    $0xc,%esp
  8028c6:	81 e2 07 0e 00 00    	and    $0xe07,%edx
  8028cc:	52                   	push   %edx
  8028cd:	50                   	push   %eax
  8028ce:	57                   	push   %edi
  8028cf:	50                   	push   %eax
  8028d0:	6a 00                	push   $0x0
  8028d2:	e8 a1 ec ff ff       	call   801578 <sys_page_map>
			    if (r){
  8028d7:	83 c4 20             	add    $0x20,%esp
  8028da:	85 c0                	test   %eax,%eax
  8028dc:	75 1e                	jne    8028fc <spawn+0x498>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  8028de:	43                   	inc    %ebx
  8028df:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  8028e5:	75 bd                	jne    8028a4 <spawn+0x440>
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  8028e7:	ff 85 94 fd ff ff    	incl   -0x26c(%ebp)
  8028ed:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  8028f3:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  8028f8:	75 91                	jne    80288b <spawn+0x427>
  8028fa:	eb 19                	jmp    802915 <spawn+0x4b1>
	}
	close(fd);
	fd = -1;

	// Copy shared library state.
	if ((r = copy_shared_pages(child)) < 0)
  8028fc:	85 c0                	test   %eax,%eax
  8028fe:	79 15                	jns    802915 <spawn+0x4b1>
		panic("copy_shared_pages: %e", r);
  802900:	50                   	push   %eax
  802901:	68 bd 39 80 00       	push   $0x8039bd
  802906:	68 82 00 00 00       	push   $0x82
  80290b:	68 94 39 80 00       	push   $0x803994
  802910:	e8 fa e0 ff ff       	call   800a0f <_panic>

	if ((r = sys_env_set_trapframe(child, &child_tf)) < 0)
  802915:	83 ec 08             	sub    $0x8,%esp
  802918:	8d 85 a4 fd ff ff    	lea    -0x25c(%ebp),%eax
  80291e:	50                   	push   %eax
  80291f:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  802925:	e8 33 ed ff ff       	call   80165d <sys_env_set_trapframe>
  80292a:	83 c4 10             	add    $0x10,%esp
  80292d:	85 c0                	test   %eax,%eax
  80292f:	79 15                	jns    802946 <spawn+0x4e2>
		panic("sys_env_set_trapframe: %e", r);
  802931:	50                   	push   %eax
  802932:	68 d3 39 80 00       	push   $0x8039d3
  802937:	68 85 00 00 00       	push   $0x85
  80293c:	68 94 39 80 00       	push   $0x803994
  802941:	e8 c9 e0 ff ff       	call   800a0f <_panic>

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
  802946:	83 ec 08             	sub    $0x8,%esp
  802949:	6a 02                	push   $0x2
  80294b:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  802951:	e8 a6 ec ff ff       	call   8015fc <sys_env_set_status>
  802956:	83 c4 10             	add    $0x10,%esp
  802959:	85 c0                	test   %eax,%eax
  80295b:	79 25                	jns    802982 <spawn+0x51e>
		panic("sys_env_set_status: %e", r);
  80295d:	50                   	push   %eax
  80295e:	68 ed 39 80 00       	push   $0x8039ed
  802963:	68 88 00 00 00       	push   $0x88
  802968:	68 94 39 80 00       	push   $0x803994
  80296d:	e8 9d e0 ff ff       	call   800a0f <_panic>
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
		return r;
  802972:	8b 9d 8c fd ff ff    	mov    -0x274(%ebp),%ebx
  802978:	eb 58                	jmp    8029d2 <spawn+0x56e>
		return -E_NOT_EXEC;
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
		return r;
  80297a:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  802980:	eb 50                	jmp    8029d2 <spawn+0x56e>
		panic("sys_env_set_trapframe: %e", r);

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
		panic("sys_env_set_status: %e", r);

	return child;
  802982:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  802988:	eb 48                	jmp    8029d2 <spawn+0x56e>
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
		return -E_NO_MEM;
  80298a:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
  80298f:	eb 41                	jmp    8029d2 <spawn+0x56e>

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
		return r;
  802991:	89 c3                	mov    %eax,%ebx
  802993:	eb 3d                	jmp    8029d2 <spawn+0x56e>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  802995:	89 c3                	mov    %eax,%ebx
  802997:	eb 06                	jmp    80299f <spawn+0x53b>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  802999:	89 c3                	mov    %eax,%ebx
  80299b:	eb 02                	jmp    80299f <spawn+0x53b>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  80299d:	89 c3                	mov    %eax,%ebx
		panic("sys_env_set_status: %e", r);

	return child;

error:
	sys_env_destroy(child);
  80299f:	83 ec 0c             	sub    $0xc,%esp
  8029a2:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8029a8:	e8 09 eb ff ff       	call   8014b6 <sys_env_destroy>
	close(fd);
  8029ad:	83 c4 04             	add    $0x4,%esp
  8029b0:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8029b6:	e8 5b f3 ff ff       	call   801d16 <close>
	return r;
  8029bb:	83 c4 10             	add    $0x10,%esp
  8029be:	eb 12                	jmp    8029d2 <spawn+0x56e>
		goto error;

	return 0;

error:
	sys_page_unmap(0, UTEMP);
  8029c0:	83 ec 08             	sub    $0x8,%esp
  8029c3:	68 00 00 40 00       	push   $0x400000
  8029c8:	6a 00                	push   $0x0
  8029ca:	e8 eb eb ff ff       	call   8015ba <sys_page_unmap>
  8029cf:	83 c4 10             	add    $0x10,%esp

error:
	sys_env_destroy(child);
	close(fd);
	return r;
}
  8029d2:	89 d8                	mov    %ebx,%eax
  8029d4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8029d7:	5b                   	pop    %ebx
  8029d8:	5e                   	pop    %esi
  8029d9:	5f                   	pop    %edi
  8029da:	5d                   	pop    %ebp
  8029db:	c3                   	ret    

008029dc <spawnl>:
// Spawn, taking command-line arguments array directly on the stack.
// NOTE: Must have a sentinal of NULL at the end of the args
// (none of the args may be NULL).
int
spawnl(const char *prog, const char *arg0, ...)
{
  8029dc:	55                   	push   %ebp
  8029dd:	89 e5                	mov    %esp,%ebp
  8029df:	56                   	push   %esi
  8029e0:	53                   	push   %ebx
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  8029e1:	8d 55 10             	lea    0x10(%ebp),%edx
{
	// We calculate argc by advancing the args until we hit NULL.
	// The contract of the function guarantees that the last
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
  8029e4:	b8 00 00 00 00       	mov    $0x0,%eax
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  8029e9:	eb 01                	jmp    8029ec <spawnl+0x10>
		argc++;
  8029eb:	40                   	inc    %eax
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  8029ec:	83 c2 04             	add    $0x4,%edx
  8029ef:	83 7a fc 00          	cmpl   $0x0,-0x4(%edx)
  8029f3:	75 f6                	jne    8029eb <spawnl+0xf>
		argc++;
	va_end(vl);

	// Now that we have the size of the args, do a second pass
	// and store the values in a VLA, which has the format of argv
	const char *argv[argc+2];
  8029f5:	8d 14 85 1a 00 00 00 	lea    0x1a(,%eax,4),%edx
  8029fc:	83 e2 f0             	and    $0xfffffff0,%edx
  8029ff:	29 d4                	sub    %edx,%esp
  802a01:	8d 54 24 03          	lea    0x3(%esp),%edx
  802a05:	c1 ea 02             	shr    $0x2,%edx
  802a08:	8d 34 95 00 00 00 00 	lea    0x0(,%edx,4),%esi
  802a0f:	89 f3                	mov    %esi,%ebx
	argv[0] = arg0;
  802a11:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  802a14:	89 0c 95 00 00 00 00 	mov    %ecx,0x0(,%edx,4)
	argv[argc+1] = NULL;
  802a1b:	c7 44 86 04 00 00 00 	movl   $0x0,0x4(%esi,%eax,4)
  802a22:	00 
  802a23:	89 c2                	mov    %eax,%edx

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  802a25:	b8 00 00 00 00       	mov    $0x0,%eax
  802a2a:	eb 08                	jmp    802a34 <spawnl+0x58>
		argv[i+1] = va_arg(vl, const char *);
  802a2c:	40                   	inc    %eax
  802a2d:	8b 4c 85 0c          	mov    0xc(%ebp,%eax,4),%ecx
  802a31:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
	argv[0] = arg0;
	argv[argc+1] = NULL;

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  802a34:	39 d0                	cmp    %edx,%eax
  802a36:	75 f4                	jne    802a2c <spawnl+0x50>
		argv[i+1] = va_arg(vl, const char *);
	va_end(vl);
	return spawn(prog, argv);
  802a38:	83 ec 08             	sub    $0x8,%esp
  802a3b:	56                   	push   %esi
  802a3c:	ff 75 08             	pushl  0x8(%ebp)
  802a3f:	e8 20 fa ff ff       	call   802464 <spawn>
}
  802a44:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802a47:	5b                   	pop    %ebx
  802a48:	5e                   	pop    %esi
  802a49:	5d                   	pop    %ebp
  802a4a:	c3                   	ret    

00802a4b <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  802a4b:	55                   	push   %ebp
  802a4c:	89 e5                	mov    %esp,%ebp
  802a4e:	56                   	push   %esi
  802a4f:	53                   	push   %ebx
  802a50:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  802a53:	83 ec 0c             	sub    $0xc,%esp
  802a56:	ff 75 08             	pushl  0x8(%ebp)
  802a59:	e8 24 f1 ff ff       	call   801b82 <fd2data>
  802a5e:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  802a60:	83 c4 08             	add    $0x8,%esp
  802a63:	68 2a 3a 80 00       	push   $0x803a2a
  802a68:	53                   	push   %ebx
  802a69:	e8 e3 e6 ff ff       	call   801151 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  802a6e:	8b 46 04             	mov    0x4(%esi),%eax
  802a71:	2b 06                	sub    (%esi),%eax
  802a73:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  802a79:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  802a80:	00 00 00 
	stat->st_dev = &devpipe;
  802a83:	c7 83 88 00 00 00 3c 	movl   $0x80403c,0x88(%ebx)
  802a8a:	40 80 00 
	return 0;
}
  802a8d:	b8 00 00 00 00       	mov    $0x0,%eax
  802a92:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802a95:	5b                   	pop    %ebx
  802a96:	5e                   	pop    %esi
  802a97:	5d                   	pop    %ebp
  802a98:	c3                   	ret    

00802a99 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  802a99:	55                   	push   %ebp
  802a9a:	89 e5                	mov    %esp,%ebp
  802a9c:	53                   	push   %ebx
  802a9d:	83 ec 0c             	sub    $0xc,%esp
  802aa0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  802aa3:	53                   	push   %ebx
  802aa4:	6a 00                	push   $0x0
  802aa6:	e8 0f eb ff ff       	call   8015ba <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  802aab:	89 1c 24             	mov    %ebx,(%esp)
  802aae:	e8 cf f0 ff ff       	call   801b82 <fd2data>
  802ab3:	83 c4 08             	add    $0x8,%esp
  802ab6:	50                   	push   %eax
  802ab7:	6a 00                	push   $0x0
  802ab9:	e8 fc ea ff ff       	call   8015ba <sys_page_unmap>
}
  802abe:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  802ac1:	c9                   	leave  
  802ac2:	c3                   	ret    

00802ac3 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  802ac3:	55                   	push   %ebp
  802ac4:	89 e5                	mov    %esp,%ebp
  802ac6:	57                   	push   %edi
  802ac7:	56                   	push   %esi
  802ac8:	53                   	push   %ebx
  802ac9:	83 ec 1c             	sub    $0x1c,%esp
  802acc:	89 45 e0             	mov    %eax,-0x20(%ebp)
  802acf:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  802ad1:	a1 24 54 80 00       	mov    0x805424,%eax
  802ad6:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  802ad9:	83 ec 0c             	sub    $0xc,%esp
  802adc:	ff 75 e0             	pushl  -0x20(%ebp)
  802adf:	e8 82 04 00 00       	call   802f66 <pageref>
  802ae4:	89 c3                	mov    %eax,%ebx
  802ae6:	89 3c 24             	mov    %edi,(%esp)
  802ae9:	e8 78 04 00 00       	call   802f66 <pageref>
  802aee:	83 c4 10             	add    $0x10,%esp
  802af1:	39 c3                	cmp    %eax,%ebx
  802af3:	0f 94 c1             	sete   %cl
  802af6:	0f b6 c9             	movzbl %cl,%ecx
  802af9:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  802afc:	8b 15 24 54 80 00    	mov    0x805424,%edx
  802b02:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  802b05:	39 ce                	cmp    %ecx,%esi
  802b07:	74 1b                	je     802b24 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  802b09:	39 c3                	cmp    %eax,%ebx
  802b0b:	75 c4                	jne    802ad1 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  802b0d:	8b 42 58             	mov    0x58(%edx),%eax
  802b10:	ff 75 e4             	pushl  -0x1c(%ebp)
  802b13:	50                   	push   %eax
  802b14:	56                   	push   %esi
  802b15:	68 31 3a 80 00       	push   $0x803a31
  802b1a:	e8 c8 df ff ff       	call   800ae7 <cprintf>
  802b1f:	83 c4 10             	add    $0x10,%esp
  802b22:	eb ad                	jmp    802ad1 <_pipeisclosed+0xe>
	}
}
  802b24:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  802b27:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802b2a:	5b                   	pop    %ebx
  802b2b:	5e                   	pop    %esi
  802b2c:	5f                   	pop    %edi
  802b2d:	5d                   	pop    %ebp
  802b2e:	c3                   	ret    

00802b2f <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  802b2f:	55                   	push   %ebp
  802b30:	89 e5                	mov    %esp,%ebp
  802b32:	57                   	push   %edi
  802b33:	56                   	push   %esi
  802b34:	53                   	push   %ebx
  802b35:	83 ec 18             	sub    $0x18,%esp
  802b38:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  802b3b:	56                   	push   %esi
  802b3c:	e8 41 f0 ff ff       	call   801b82 <fd2data>
  802b41:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802b43:	83 c4 10             	add    $0x10,%esp
  802b46:	bf 00 00 00 00       	mov    $0x0,%edi
  802b4b:	eb 3b                	jmp    802b88 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  802b4d:	89 da                	mov    %ebx,%edx
  802b4f:	89 f0                	mov    %esi,%eax
  802b51:	e8 6d ff ff ff       	call   802ac3 <_pipeisclosed>
  802b56:	85 c0                	test   %eax,%eax
  802b58:	75 38                	jne    802b92 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  802b5a:	e8 b7 e9 ff ff       	call   801516 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  802b5f:	8b 53 04             	mov    0x4(%ebx),%edx
  802b62:	8b 03                	mov    (%ebx),%eax
  802b64:	83 c0 20             	add    $0x20,%eax
  802b67:	39 c2                	cmp    %eax,%edx
  802b69:	73 e2                	jae    802b4d <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  802b6b:	8b 45 0c             	mov    0xc(%ebp),%eax
  802b6e:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  802b71:	89 d0                	mov    %edx,%eax
  802b73:	25 1f 00 00 80       	and    $0x8000001f,%eax
  802b78:	79 05                	jns    802b7f <devpipe_write+0x50>
  802b7a:	48                   	dec    %eax
  802b7b:	83 c8 e0             	or     $0xffffffe0,%eax
  802b7e:	40                   	inc    %eax
  802b7f:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  802b83:	42                   	inc    %edx
  802b84:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802b87:	47                   	inc    %edi
  802b88:	3b 7d 10             	cmp    0x10(%ebp),%edi
  802b8b:	75 d2                	jne    802b5f <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  802b8d:	8b 45 10             	mov    0x10(%ebp),%eax
  802b90:	eb 05                	jmp    802b97 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  802b92:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  802b97:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802b9a:	5b                   	pop    %ebx
  802b9b:	5e                   	pop    %esi
  802b9c:	5f                   	pop    %edi
  802b9d:	5d                   	pop    %ebp
  802b9e:	c3                   	ret    

00802b9f <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  802b9f:	55                   	push   %ebp
  802ba0:	89 e5                	mov    %esp,%ebp
  802ba2:	57                   	push   %edi
  802ba3:	56                   	push   %esi
  802ba4:	53                   	push   %ebx
  802ba5:	83 ec 18             	sub    $0x18,%esp
  802ba8:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  802bab:	57                   	push   %edi
  802bac:	e8 d1 ef ff ff       	call   801b82 <fd2data>
  802bb1:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802bb3:	83 c4 10             	add    $0x10,%esp
  802bb6:	bb 00 00 00 00       	mov    $0x0,%ebx
  802bbb:	eb 3a                	jmp    802bf7 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  802bbd:	85 db                	test   %ebx,%ebx
  802bbf:	74 04                	je     802bc5 <devpipe_read+0x26>
				return i;
  802bc1:	89 d8                	mov    %ebx,%eax
  802bc3:	eb 41                	jmp    802c06 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  802bc5:	89 f2                	mov    %esi,%edx
  802bc7:	89 f8                	mov    %edi,%eax
  802bc9:	e8 f5 fe ff ff       	call   802ac3 <_pipeisclosed>
  802bce:	85 c0                	test   %eax,%eax
  802bd0:	75 2f                	jne    802c01 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  802bd2:	e8 3f e9 ff ff       	call   801516 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  802bd7:	8b 06                	mov    (%esi),%eax
  802bd9:	3b 46 04             	cmp    0x4(%esi),%eax
  802bdc:	74 df                	je     802bbd <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  802bde:	25 1f 00 00 80       	and    $0x8000001f,%eax
  802be3:	79 05                	jns    802bea <devpipe_read+0x4b>
  802be5:	48                   	dec    %eax
  802be6:	83 c8 e0             	or     $0xffffffe0,%eax
  802be9:	40                   	inc    %eax
  802bea:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  802bee:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  802bf1:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  802bf4:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  802bf6:	43                   	inc    %ebx
  802bf7:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  802bfa:	75 db                	jne    802bd7 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  802bfc:	8b 45 10             	mov    0x10(%ebp),%eax
  802bff:	eb 05                	jmp    802c06 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  802c01:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  802c06:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802c09:	5b                   	pop    %ebx
  802c0a:	5e                   	pop    %esi
  802c0b:	5f                   	pop    %edi
  802c0c:	5d                   	pop    %ebp
  802c0d:	c3                   	ret    

00802c0e <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  802c0e:	55                   	push   %ebp
  802c0f:	89 e5                	mov    %esp,%ebp
  802c11:	56                   	push   %esi
  802c12:	53                   	push   %ebx
  802c13:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  802c16:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802c19:	50                   	push   %eax
  802c1a:	e8 7a ef ff ff       	call   801b99 <fd_alloc>
  802c1f:	83 c4 10             	add    $0x10,%esp
  802c22:	85 c0                	test   %eax,%eax
  802c24:	0f 88 2a 01 00 00    	js     802d54 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  802c2a:	83 ec 04             	sub    $0x4,%esp
  802c2d:	68 07 04 00 00       	push   $0x407
  802c32:	ff 75 f4             	pushl  -0xc(%ebp)
  802c35:	6a 00                	push   $0x0
  802c37:	e8 f9 e8 ff ff       	call   801535 <sys_page_alloc>
  802c3c:	83 c4 10             	add    $0x10,%esp
  802c3f:	85 c0                	test   %eax,%eax
  802c41:	0f 88 0d 01 00 00    	js     802d54 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  802c47:	83 ec 0c             	sub    $0xc,%esp
  802c4a:	8d 45 f0             	lea    -0x10(%ebp),%eax
  802c4d:	50                   	push   %eax
  802c4e:	e8 46 ef ff ff       	call   801b99 <fd_alloc>
  802c53:	89 c3                	mov    %eax,%ebx
  802c55:	83 c4 10             	add    $0x10,%esp
  802c58:	85 c0                	test   %eax,%eax
  802c5a:	0f 88 e2 00 00 00    	js     802d42 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  802c60:	83 ec 04             	sub    $0x4,%esp
  802c63:	68 07 04 00 00       	push   $0x407
  802c68:	ff 75 f0             	pushl  -0x10(%ebp)
  802c6b:	6a 00                	push   $0x0
  802c6d:	e8 c3 e8 ff ff       	call   801535 <sys_page_alloc>
  802c72:	89 c3                	mov    %eax,%ebx
  802c74:	83 c4 10             	add    $0x10,%esp
  802c77:	85 c0                	test   %eax,%eax
  802c79:	0f 88 c3 00 00 00    	js     802d42 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  802c7f:	83 ec 0c             	sub    $0xc,%esp
  802c82:	ff 75 f4             	pushl  -0xc(%ebp)
  802c85:	e8 f8 ee ff ff       	call   801b82 <fd2data>
  802c8a:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  802c8c:	83 c4 0c             	add    $0xc,%esp
  802c8f:	68 07 04 00 00       	push   $0x407
  802c94:	50                   	push   %eax
  802c95:	6a 00                	push   $0x0
  802c97:	e8 99 e8 ff ff       	call   801535 <sys_page_alloc>
  802c9c:	89 c3                	mov    %eax,%ebx
  802c9e:	83 c4 10             	add    $0x10,%esp
  802ca1:	85 c0                	test   %eax,%eax
  802ca3:	0f 88 89 00 00 00    	js     802d32 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  802ca9:	83 ec 0c             	sub    $0xc,%esp
  802cac:	ff 75 f0             	pushl  -0x10(%ebp)
  802caf:	e8 ce ee ff ff       	call   801b82 <fd2data>
  802cb4:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  802cbb:	50                   	push   %eax
  802cbc:	6a 00                	push   $0x0
  802cbe:	56                   	push   %esi
  802cbf:	6a 00                	push   $0x0
  802cc1:	e8 b2 e8 ff ff       	call   801578 <sys_page_map>
  802cc6:	89 c3                	mov    %eax,%ebx
  802cc8:	83 c4 20             	add    $0x20,%esp
  802ccb:	85 c0                	test   %eax,%eax
  802ccd:	78 55                	js     802d24 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  802ccf:	8b 15 3c 40 80 00    	mov    0x80403c,%edx
  802cd5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802cd8:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  802cda:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802cdd:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  802ce4:	8b 15 3c 40 80 00    	mov    0x80403c,%edx
  802cea:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802ced:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  802cef:	8b 45 f0             	mov    -0x10(%ebp),%eax
  802cf2:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  802cf9:	83 ec 0c             	sub    $0xc,%esp
  802cfc:	ff 75 f4             	pushl  -0xc(%ebp)
  802cff:	e8 6e ee ff ff       	call   801b72 <fd2num>
  802d04:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802d07:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  802d09:	83 c4 04             	add    $0x4,%esp
  802d0c:	ff 75 f0             	pushl  -0x10(%ebp)
  802d0f:	e8 5e ee ff ff       	call   801b72 <fd2num>
  802d14:	8b 4d 08             	mov    0x8(%ebp),%ecx
  802d17:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  802d1a:	83 c4 10             	add    $0x10,%esp
  802d1d:	b8 00 00 00 00       	mov    $0x0,%eax
  802d22:	eb 30                	jmp    802d54 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  802d24:	83 ec 08             	sub    $0x8,%esp
  802d27:	56                   	push   %esi
  802d28:	6a 00                	push   $0x0
  802d2a:	e8 8b e8 ff ff       	call   8015ba <sys_page_unmap>
  802d2f:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  802d32:	83 ec 08             	sub    $0x8,%esp
  802d35:	ff 75 f0             	pushl  -0x10(%ebp)
  802d38:	6a 00                	push   $0x0
  802d3a:	e8 7b e8 ff ff       	call   8015ba <sys_page_unmap>
  802d3f:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  802d42:	83 ec 08             	sub    $0x8,%esp
  802d45:	ff 75 f4             	pushl  -0xc(%ebp)
  802d48:	6a 00                	push   $0x0
  802d4a:	e8 6b e8 ff ff       	call   8015ba <sys_page_unmap>
  802d4f:	83 c4 10             	add    $0x10,%esp
  802d52:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  802d54:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802d57:	5b                   	pop    %ebx
  802d58:	5e                   	pop    %esi
  802d59:	5d                   	pop    %ebp
  802d5a:	c3                   	ret    

00802d5b <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  802d5b:	55                   	push   %ebp
  802d5c:	89 e5                	mov    %esp,%ebp
  802d5e:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  802d61:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802d64:	50                   	push   %eax
  802d65:	ff 75 08             	pushl  0x8(%ebp)
  802d68:	e8 7b ee ff ff       	call   801be8 <fd_lookup>
  802d6d:	83 c4 10             	add    $0x10,%esp
  802d70:	85 c0                	test   %eax,%eax
  802d72:	78 18                	js     802d8c <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  802d74:	83 ec 0c             	sub    $0xc,%esp
  802d77:	ff 75 f4             	pushl  -0xc(%ebp)
  802d7a:	e8 03 ee ff ff       	call   801b82 <fd2data>
	return _pipeisclosed(fd, p);
  802d7f:	89 c2                	mov    %eax,%edx
  802d81:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802d84:	e8 3a fd ff ff       	call   802ac3 <_pipeisclosed>
  802d89:	83 c4 10             	add    $0x10,%esp
}
  802d8c:	c9                   	leave  
  802d8d:	c3                   	ret    

00802d8e <wait>:
#include <inc/lib.h>

// Waits until 'envid' exits.
void
wait(envid_t envid)
{
  802d8e:	55                   	push   %ebp
  802d8f:	89 e5                	mov    %esp,%ebp
  802d91:	56                   	push   %esi
  802d92:	53                   	push   %ebx
  802d93:	8b 75 08             	mov    0x8(%ebp),%esi
	const volatile struct Env *e;

	assert(envid != 0);
  802d96:	85 f6                	test   %esi,%esi
  802d98:	75 16                	jne    802db0 <wait+0x22>
  802d9a:	68 49 3a 80 00       	push   $0x803a49
  802d9f:	68 6f 33 80 00       	push   $0x80336f
  802da4:	6a 09                	push   $0x9
  802da6:	68 54 3a 80 00       	push   $0x803a54
  802dab:	e8 5f dc ff ff       	call   800a0f <_panic>
	e = &envs[ENVX(envid)];
  802db0:	89 f3                	mov    %esi,%ebx
  802db2:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
	while (e->env_id == envid && e->env_status != ENV_FREE)
  802db8:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  802dbf:	c1 e3 07             	shl    $0x7,%ebx
  802dc2:	29 c3                	sub    %eax,%ebx
  802dc4:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  802dca:	eb 05                	jmp    802dd1 <wait+0x43>
		sys_yield();
  802dcc:	e8 45 e7 ff ff       	call   801516 <sys_yield>
{
	const volatile struct Env *e;

	assert(envid != 0);
	e = &envs[ENVX(envid)];
	while (e->env_id == envid && e->env_status != ENV_FREE)
  802dd1:	8b 43 48             	mov    0x48(%ebx),%eax
  802dd4:	39 c6                	cmp    %eax,%esi
  802dd6:	75 07                	jne    802ddf <wait+0x51>
  802dd8:	8b 43 54             	mov    0x54(%ebx),%eax
  802ddb:	85 c0                	test   %eax,%eax
  802ddd:	75 ed                	jne    802dcc <wait+0x3e>
		sys_yield();
}
  802ddf:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802de2:	5b                   	pop    %ebx
  802de3:	5e                   	pop    %esi
  802de4:	5d                   	pop    %ebp
  802de5:	c3                   	ret    

00802de6 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  802de6:	55                   	push   %ebp
  802de7:	89 e5                	mov    %esp,%ebp
  802de9:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  802dec:	83 3d 00 70 80 00 00 	cmpl   $0x0,0x807000
  802df3:	75 3e                	jne    802e33 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  802df5:	83 ec 04             	sub    $0x4,%esp
  802df8:	6a 07                	push   $0x7
  802dfa:	68 00 f0 bf ee       	push   $0xeebff000
  802dff:	6a 00                	push   $0x0
  802e01:	e8 2f e7 ff ff       	call   801535 <sys_page_alloc>
  802e06:	83 c4 10             	add    $0x10,%esp
  802e09:	85 c0                	test   %eax,%eax
  802e0b:	74 14                	je     802e21 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  802e0d:	83 ec 04             	sub    $0x4,%esp
  802e10:	68 60 3a 80 00       	push   $0x803a60
  802e15:	6a 23                	push   $0x23
  802e17:	68 84 3a 80 00       	push   $0x803a84
  802e1c:	e8 ee db ff ff       	call   800a0f <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  802e21:	83 ec 08             	sub    $0x8,%esp
  802e24:	68 3d 2e 80 00       	push   $0x802e3d
  802e29:	6a 00                	push   $0x0
  802e2b:	e8 6f e8 ff ff       	call   80169f <sys_env_set_pgfault_upcall>
  802e30:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  802e33:	8b 45 08             	mov    0x8(%ebp),%eax
  802e36:	a3 00 70 80 00       	mov    %eax,0x807000
}
  802e3b:	c9                   	leave  
  802e3c:	c3                   	ret    

00802e3d <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  802e3d:	54                   	push   %esp
	movl _pgfault_handler, %eax
  802e3e:	a1 00 70 80 00       	mov    0x807000,%eax
	call *%eax
  802e43:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  802e45:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  802e48:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  802e4a:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  802e4e:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  802e52:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  802e53:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  802e56:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  802e58:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  802e5b:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  802e5e:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  802e5f:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  802e62:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  802e63:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  802e64:	c3                   	ret    

00802e65 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  802e65:	55                   	push   %ebp
  802e66:	89 e5                	mov    %esp,%ebp
  802e68:	56                   	push   %esi
  802e69:	53                   	push   %ebx
  802e6a:	8b 75 08             	mov    0x8(%ebp),%esi
  802e6d:	8b 45 0c             	mov    0xc(%ebp),%eax
  802e70:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  802e73:	85 c0                	test   %eax,%eax
  802e75:	75 05                	jne    802e7c <ipc_recv+0x17>
        pg = (void *)UTOP;
  802e77:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  802e7c:	83 ec 0c             	sub    $0xc,%esp
  802e7f:	50                   	push   %eax
  802e80:	e8 7f e8 ff ff       	call   801704 <sys_ipc_recv>
  802e85:	83 c4 10             	add    $0x10,%esp
  802e88:	85 c0                	test   %eax,%eax
  802e8a:	74 16                	je     802ea2 <ipc_recv+0x3d>
        if (from_env_store)
  802e8c:	85 f6                	test   %esi,%esi
  802e8e:	74 06                	je     802e96 <ipc_recv+0x31>
            *from_env_store = 0;
  802e90:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  802e96:	85 db                	test   %ebx,%ebx
  802e98:	74 2c                	je     802ec6 <ipc_recv+0x61>
            *perm_store = 0;
  802e9a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  802ea0:	eb 24                	jmp    802ec6 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  802ea2:	85 f6                	test   %esi,%esi
  802ea4:	74 0a                	je     802eb0 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  802ea6:	a1 24 54 80 00       	mov    0x805424,%eax
  802eab:	8b 40 74             	mov    0x74(%eax),%eax
  802eae:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  802eb0:	85 db                	test   %ebx,%ebx
  802eb2:	74 0a                	je     802ebe <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  802eb4:	a1 24 54 80 00       	mov    0x805424,%eax
  802eb9:	8b 40 78             	mov    0x78(%eax),%eax
  802ebc:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  802ebe:	a1 24 54 80 00       	mov    0x805424,%eax
  802ec3:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  802ec6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  802ec9:	5b                   	pop    %ebx
  802eca:	5e                   	pop    %esi
  802ecb:	5d                   	pop    %ebp
  802ecc:	c3                   	ret    

00802ecd <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  802ecd:	55                   	push   %ebp
  802ece:	89 e5                	mov    %esp,%ebp
  802ed0:	57                   	push   %edi
  802ed1:	56                   	push   %esi
  802ed2:	53                   	push   %ebx
  802ed3:	83 ec 0c             	sub    $0xc,%esp
  802ed6:	8b 75 0c             	mov    0xc(%ebp),%esi
  802ed9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  802edc:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  802edf:	85 db                	test   %ebx,%ebx
  802ee1:	75 0c                	jne    802eef <ipc_send+0x22>
        pg = (void *)UTOP;
  802ee3:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  802ee8:	eb 05                	jmp    802eef <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  802eea:	e8 27 e6 ff ff       	call   801516 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  802eef:	57                   	push   %edi
  802ef0:	53                   	push   %ebx
  802ef1:	56                   	push   %esi
  802ef2:	ff 75 08             	pushl  0x8(%ebp)
  802ef5:	e8 e7 e7 ff ff       	call   8016e1 <sys_ipc_try_send>
  802efa:	83 c4 10             	add    $0x10,%esp
  802efd:	83 f8 f9             	cmp    $0xfffffff9,%eax
  802f00:	74 e8                	je     802eea <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  802f02:	85 c0                	test   %eax,%eax
  802f04:	74 14                	je     802f1a <ipc_send+0x4d>
        panic("ipc_send: error");
  802f06:	83 ec 04             	sub    $0x4,%esp
  802f09:	68 92 3a 80 00       	push   $0x803a92
  802f0e:	6a 6a                	push   $0x6a
  802f10:	68 a2 3a 80 00       	push   $0x803aa2
  802f15:	e8 f5 da ff ff       	call   800a0f <_panic>
    }
	//panic("ipc_send not implemented");
}
  802f1a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802f1d:	5b                   	pop    %ebx
  802f1e:	5e                   	pop    %esi
  802f1f:	5f                   	pop    %edi
  802f20:	5d                   	pop    %ebp
  802f21:	c3                   	ret    

00802f22 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  802f22:	55                   	push   %ebp
  802f23:	89 e5                	mov    %esp,%ebp
  802f25:	53                   	push   %ebx
  802f26:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  802f29:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  802f2e:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  802f35:	89 d0                	mov    %edx,%eax
  802f37:	c1 e0 07             	shl    $0x7,%eax
  802f3a:	29 d8                	sub    %ebx,%eax
  802f3c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  802f41:	8b 40 50             	mov    0x50(%eax),%eax
  802f44:	39 c8                	cmp    %ecx,%eax
  802f46:	75 0d                	jne    802f55 <ipc_find_env+0x33>
			return envs[i].env_id;
  802f48:	c1 e2 07             	shl    $0x7,%edx
  802f4b:	29 da                	sub    %ebx,%edx
  802f4d:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  802f53:	eb 0e                	jmp    802f63 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  802f55:	42                   	inc    %edx
  802f56:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  802f5c:	75 d0                	jne    802f2e <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  802f5e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802f63:	5b                   	pop    %ebx
  802f64:	5d                   	pop    %ebp
  802f65:	c3                   	ret    

00802f66 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802f66:	55                   	push   %ebp
  802f67:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  802f69:	8b 45 08             	mov    0x8(%ebp),%eax
  802f6c:	c1 e8 16             	shr    $0x16,%eax
  802f6f:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  802f76:	a8 01                	test   $0x1,%al
  802f78:	74 21                	je     802f9b <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  802f7a:	8b 45 08             	mov    0x8(%ebp),%eax
  802f7d:	c1 e8 0c             	shr    $0xc,%eax
  802f80:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  802f87:	a8 01                	test   $0x1,%al
  802f89:	74 17                	je     802fa2 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  802f8b:	c1 e8 0c             	shr    $0xc,%eax
  802f8e:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  802f95:	ef 
  802f96:	0f b7 c0             	movzwl %ax,%eax
  802f99:	eb 0c                	jmp    802fa7 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  802f9b:	b8 00 00 00 00       	mov    $0x0,%eax
  802fa0:	eb 05                	jmp    802fa7 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  802fa2:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  802fa7:	5d                   	pop    %ebp
  802fa8:	c3                   	ret    
  802fa9:	66 90                	xchg   %ax,%ax
  802fab:	90                   	nop

00802fac <__udivdi3>:
  802fac:	55                   	push   %ebp
  802fad:	57                   	push   %edi
  802fae:	56                   	push   %esi
  802faf:	53                   	push   %ebx
  802fb0:	83 ec 1c             	sub    $0x1c,%esp
  802fb3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  802fb7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  802fbb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  802fbf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  802fc3:	89 ca                	mov    %ecx,%edx
  802fc5:	89 f8                	mov    %edi,%eax
  802fc7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  802fcb:	85 f6                	test   %esi,%esi
  802fcd:	75 2d                	jne    802ffc <__udivdi3+0x50>
  802fcf:	39 cf                	cmp    %ecx,%edi
  802fd1:	77 65                	ja     803038 <__udivdi3+0x8c>
  802fd3:	89 fd                	mov    %edi,%ebp
  802fd5:	85 ff                	test   %edi,%edi
  802fd7:	75 0b                	jne    802fe4 <__udivdi3+0x38>
  802fd9:	b8 01 00 00 00       	mov    $0x1,%eax
  802fde:	31 d2                	xor    %edx,%edx
  802fe0:	f7 f7                	div    %edi
  802fe2:	89 c5                	mov    %eax,%ebp
  802fe4:	31 d2                	xor    %edx,%edx
  802fe6:	89 c8                	mov    %ecx,%eax
  802fe8:	f7 f5                	div    %ebp
  802fea:	89 c1                	mov    %eax,%ecx
  802fec:	89 d8                	mov    %ebx,%eax
  802fee:	f7 f5                	div    %ebp
  802ff0:	89 cf                	mov    %ecx,%edi
  802ff2:	89 fa                	mov    %edi,%edx
  802ff4:	83 c4 1c             	add    $0x1c,%esp
  802ff7:	5b                   	pop    %ebx
  802ff8:	5e                   	pop    %esi
  802ff9:	5f                   	pop    %edi
  802ffa:	5d                   	pop    %ebp
  802ffb:	c3                   	ret    
  802ffc:	39 ce                	cmp    %ecx,%esi
  802ffe:	77 28                	ja     803028 <__udivdi3+0x7c>
  803000:	0f bd fe             	bsr    %esi,%edi
  803003:	83 f7 1f             	xor    $0x1f,%edi
  803006:	75 40                	jne    803048 <__udivdi3+0x9c>
  803008:	39 ce                	cmp    %ecx,%esi
  80300a:	72 0a                	jb     803016 <__udivdi3+0x6a>
  80300c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  803010:	0f 87 9e 00 00 00    	ja     8030b4 <__udivdi3+0x108>
  803016:	b8 01 00 00 00       	mov    $0x1,%eax
  80301b:	89 fa                	mov    %edi,%edx
  80301d:	83 c4 1c             	add    $0x1c,%esp
  803020:	5b                   	pop    %ebx
  803021:	5e                   	pop    %esi
  803022:	5f                   	pop    %edi
  803023:	5d                   	pop    %ebp
  803024:	c3                   	ret    
  803025:	8d 76 00             	lea    0x0(%esi),%esi
  803028:	31 ff                	xor    %edi,%edi
  80302a:	31 c0                	xor    %eax,%eax
  80302c:	89 fa                	mov    %edi,%edx
  80302e:	83 c4 1c             	add    $0x1c,%esp
  803031:	5b                   	pop    %ebx
  803032:	5e                   	pop    %esi
  803033:	5f                   	pop    %edi
  803034:	5d                   	pop    %ebp
  803035:	c3                   	ret    
  803036:	66 90                	xchg   %ax,%ax
  803038:	89 d8                	mov    %ebx,%eax
  80303a:	f7 f7                	div    %edi
  80303c:	31 ff                	xor    %edi,%edi
  80303e:	89 fa                	mov    %edi,%edx
  803040:	83 c4 1c             	add    $0x1c,%esp
  803043:	5b                   	pop    %ebx
  803044:	5e                   	pop    %esi
  803045:	5f                   	pop    %edi
  803046:	5d                   	pop    %ebp
  803047:	c3                   	ret    
  803048:	bd 20 00 00 00       	mov    $0x20,%ebp
  80304d:	89 eb                	mov    %ebp,%ebx
  80304f:	29 fb                	sub    %edi,%ebx
  803051:	89 f9                	mov    %edi,%ecx
  803053:	d3 e6                	shl    %cl,%esi
  803055:	89 c5                	mov    %eax,%ebp
  803057:	88 d9                	mov    %bl,%cl
  803059:	d3 ed                	shr    %cl,%ebp
  80305b:	89 e9                	mov    %ebp,%ecx
  80305d:	09 f1                	or     %esi,%ecx
  80305f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  803063:	89 f9                	mov    %edi,%ecx
  803065:	d3 e0                	shl    %cl,%eax
  803067:	89 c5                	mov    %eax,%ebp
  803069:	89 d6                	mov    %edx,%esi
  80306b:	88 d9                	mov    %bl,%cl
  80306d:	d3 ee                	shr    %cl,%esi
  80306f:	89 f9                	mov    %edi,%ecx
  803071:	d3 e2                	shl    %cl,%edx
  803073:	8b 44 24 08          	mov    0x8(%esp),%eax
  803077:	88 d9                	mov    %bl,%cl
  803079:	d3 e8                	shr    %cl,%eax
  80307b:	09 c2                	or     %eax,%edx
  80307d:	89 d0                	mov    %edx,%eax
  80307f:	89 f2                	mov    %esi,%edx
  803081:	f7 74 24 0c          	divl   0xc(%esp)
  803085:	89 d6                	mov    %edx,%esi
  803087:	89 c3                	mov    %eax,%ebx
  803089:	f7 e5                	mul    %ebp
  80308b:	39 d6                	cmp    %edx,%esi
  80308d:	72 19                	jb     8030a8 <__udivdi3+0xfc>
  80308f:	74 0b                	je     80309c <__udivdi3+0xf0>
  803091:	89 d8                	mov    %ebx,%eax
  803093:	31 ff                	xor    %edi,%edi
  803095:	e9 58 ff ff ff       	jmp    802ff2 <__udivdi3+0x46>
  80309a:	66 90                	xchg   %ax,%ax
  80309c:	8b 54 24 08          	mov    0x8(%esp),%edx
  8030a0:	89 f9                	mov    %edi,%ecx
  8030a2:	d3 e2                	shl    %cl,%edx
  8030a4:	39 c2                	cmp    %eax,%edx
  8030a6:	73 e9                	jae    803091 <__udivdi3+0xe5>
  8030a8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8030ab:	31 ff                	xor    %edi,%edi
  8030ad:	e9 40 ff ff ff       	jmp    802ff2 <__udivdi3+0x46>
  8030b2:	66 90                	xchg   %ax,%ax
  8030b4:	31 c0                	xor    %eax,%eax
  8030b6:	e9 37 ff ff ff       	jmp    802ff2 <__udivdi3+0x46>
  8030bb:	90                   	nop

008030bc <__umoddi3>:
  8030bc:	55                   	push   %ebp
  8030bd:	57                   	push   %edi
  8030be:	56                   	push   %esi
  8030bf:	53                   	push   %ebx
  8030c0:	83 ec 1c             	sub    $0x1c,%esp
  8030c3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8030c7:	8b 74 24 34          	mov    0x34(%esp),%esi
  8030cb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8030cf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8030d3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8030d7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8030db:	89 f3                	mov    %esi,%ebx
  8030dd:	89 fa                	mov    %edi,%edx
  8030df:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8030e3:	89 34 24             	mov    %esi,(%esp)
  8030e6:	85 c0                	test   %eax,%eax
  8030e8:	75 1a                	jne    803104 <__umoddi3+0x48>
  8030ea:	39 f7                	cmp    %esi,%edi
  8030ec:	0f 86 a2 00 00 00    	jbe    803194 <__umoddi3+0xd8>
  8030f2:	89 c8                	mov    %ecx,%eax
  8030f4:	89 f2                	mov    %esi,%edx
  8030f6:	f7 f7                	div    %edi
  8030f8:	89 d0                	mov    %edx,%eax
  8030fa:	31 d2                	xor    %edx,%edx
  8030fc:	83 c4 1c             	add    $0x1c,%esp
  8030ff:	5b                   	pop    %ebx
  803100:	5e                   	pop    %esi
  803101:	5f                   	pop    %edi
  803102:	5d                   	pop    %ebp
  803103:	c3                   	ret    
  803104:	39 f0                	cmp    %esi,%eax
  803106:	0f 87 ac 00 00 00    	ja     8031b8 <__umoddi3+0xfc>
  80310c:	0f bd e8             	bsr    %eax,%ebp
  80310f:	83 f5 1f             	xor    $0x1f,%ebp
  803112:	0f 84 ac 00 00 00    	je     8031c4 <__umoddi3+0x108>
  803118:	bf 20 00 00 00       	mov    $0x20,%edi
  80311d:	29 ef                	sub    %ebp,%edi
  80311f:	89 fe                	mov    %edi,%esi
  803121:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  803125:	89 e9                	mov    %ebp,%ecx
  803127:	d3 e0                	shl    %cl,%eax
  803129:	89 d7                	mov    %edx,%edi
  80312b:	89 f1                	mov    %esi,%ecx
  80312d:	d3 ef                	shr    %cl,%edi
  80312f:	09 c7                	or     %eax,%edi
  803131:	89 e9                	mov    %ebp,%ecx
  803133:	d3 e2                	shl    %cl,%edx
  803135:	89 14 24             	mov    %edx,(%esp)
  803138:	89 d8                	mov    %ebx,%eax
  80313a:	d3 e0                	shl    %cl,%eax
  80313c:	89 c2                	mov    %eax,%edx
  80313e:	8b 44 24 08          	mov    0x8(%esp),%eax
  803142:	d3 e0                	shl    %cl,%eax
  803144:	89 44 24 04          	mov    %eax,0x4(%esp)
  803148:	8b 44 24 08          	mov    0x8(%esp),%eax
  80314c:	89 f1                	mov    %esi,%ecx
  80314e:	d3 e8                	shr    %cl,%eax
  803150:	09 d0                	or     %edx,%eax
  803152:	d3 eb                	shr    %cl,%ebx
  803154:	89 da                	mov    %ebx,%edx
  803156:	f7 f7                	div    %edi
  803158:	89 d3                	mov    %edx,%ebx
  80315a:	f7 24 24             	mull   (%esp)
  80315d:	89 c6                	mov    %eax,%esi
  80315f:	89 d1                	mov    %edx,%ecx
  803161:	39 d3                	cmp    %edx,%ebx
  803163:	0f 82 87 00 00 00    	jb     8031f0 <__umoddi3+0x134>
  803169:	0f 84 91 00 00 00    	je     803200 <__umoddi3+0x144>
  80316f:	8b 54 24 04          	mov    0x4(%esp),%edx
  803173:	29 f2                	sub    %esi,%edx
  803175:	19 cb                	sbb    %ecx,%ebx
  803177:	89 d8                	mov    %ebx,%eax
  803179:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  80317d:	d3 e0                	shl    %cl,%eax
  80317f:	89 e9                	mov    %ebp,%ecx
  803181:	d3 ea                	shr    %cl,%edx
  803183:	09 d0                	or     %edx,%eax
  803185:	89 e9                	mov    %ebp,%ecx
  803187:	d3 eb                	shr    %cl,%ebx
  803189:	89 da                	mov    %ebx,%edx
  80318b:	83 c4 1c             	add    $0x1c,%esp
  80318e:	5b                   	pop    %ebx
  80318f:	5e                   	pop    %esi
  803190:	5f                   	pop    %edi
  803191:	5d                   	pop    %ebp
  803192:	c3                   	ret    
  803193:	90                   	nop
  803194:	89 fd                	mov    %edi,%ebp
  803196:	85 ff                	test   %edi,%edi
  803198:	75 0b                	jne    8031a5 <__umoddi3+0xe9>
  80319a:	b8 01 00 00 00       	mov    $0x1,%eax
  80319f:	31 d2                	xor    %edx,%edx
  8031a1:	f7 f7                	div    %edi
  8031a3:	89 c5                	mov    %eax,%ebp
  8031a5:	89 f0                	mov    %esi,%eax
  8031a7:	31 d2                	xor    %edx,%edx
  8031a9:	f7 f5                	div    %ebp
  8031ab:	89 c8                	mov    %ecx,%eax
  8031ad:	f7 f5                	div    %ebp
  8031af:	89 d0                	mov    %edx,%eax
  8031b1:	e9 44 ff ff ff       	jmp    8030fa <__umoddi3+0x3e>
  8031b6:	66 90                	xchg   %ax,%ax
  8031b8:	89 c8                	mov    %ecx,%eax
  8031ba:	89 f2                	mov    %esi,%edx
  8031bc:	83 c4 1c             	add    $0x1c,%esp
  8031bf:	5b                   	pop    %ebx
  8031c0:	5e                   	pop    %esi
  8031c1:	5f                   	pop    %edi
  8031c2:	5d                   	pop    %ebp
  8031c3:	c3                   	ret    
  8031c4:	3b 04 24             	cmp    (%esp),%eax
  8031c7:	72 06                	jb     8031cf <__umoddi3+0x113>
  8031c9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8031cd:	77 0f                	ja     8031de <__umoddi3+0x122>
  8031cf:	89 f2                	mov    %esi,%edx
  8031d1:	29 f9                	sub    %edi,%ecx
  8031d3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8031d7:	89 14 24             	mov    %edx,(%esp)
  8031da:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8031de:	8b 44 24 04          	mov    0x4(%esp),%eax
  8031e2:	8b 14 24             	mov    (%esp),%edx
  8031e5:	83 c4 1c             	add    $0x1c,%esp
  8031e8:	5b                   	pop    %ebx
  8031e9:	5e                   	pop    %esi
  8031ea:	5f                   	pop    %edi
  8031eb:	5d                   	pop    %ebp
  8031ec:	c3                   	ret    
  8031ed:	8d 76 00             	lea    0x0(%esi),%esi
  8031f0:	2b 04 24             	sub    (%esp),%eax
  8031f3:	19 fa                	sbb    %edi,%edx
  8031f5:	89 d1                	mov    %edx,%ecx
  8031f7:	89 c6                	mov    %eax,%esi
  8031f9:	e9 71 ff ff ff       	jmp    80316f <__umoddi3+0xb3>
  8031fe:	66 90                	xchg   %ax,%ax
  803200:	39 44 24 04          	cmp    %eax,0x4(%esp)
  803204:	72 ea                	jb     8031f0 <__umoddi3+0x134>
  803206:	89 d9                	mov    %ebx,%ecx
  803208:	e9 62 ff ff ff       	jmp    80316f <__umoddi3+0xb3>
