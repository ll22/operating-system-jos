
obj/user/icode.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 03 01 00 00       	call   800134 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	81 ec 1c 02 00 00    	sub    $0x21c,%esp
	int fd, n, r;
	char buf[512+1];

	binaryname = "icode";
  80003e:	c7 05 00 30 80 00 00 	movl   $0x802400,0x803000
  800045:	24 80 00 

	cprintf("icode startup\n");
  800048:	68 06 24 80 00       	push   $0x802406
  80004d:	e8 1b 02 00 00       	call   80026d <cprintf>

	cprintf("icode: open /motd\n");
  800052:	c7 04 24 15 24 80 00 	movl   $0x802415,(%esp)
  800059:	e8 0f 02 00 00       	call   80026d <cprintf>
	if ((fd = open("/motd", O_RDONLY)) < 0)
  80005e:	83 c4 08             	add    $0x8,%esp
  800061:	6a 00                	push   $0x0
  800063:	68 28 24 80 00       	push   $0x802428
  800068:	e8 9b 14 00 00       	call   801508 <open>
  80006d:	89 c6                	mov    %eax,%esi
  80006f:	83 c4 10             	add    $0x10,%esp
  800072:	85 c0                	test   %eax,%eax
  800074:	79 12                	jns    800088 <umain+0x55>
		panic("icode: open /motd: %e", fd);
  800076:	50                   	push   %eax
  800077:	68 2e 24 80 00       	push   $0x80242e
  80007c:	6a 0f                	push   $0xf
  80007e:	68 44 24 80 00       	push   $0x802444
  800083:	e8 0d 01 00 00       	call   800195 <_panic>

	cprintf("icode: read /motd\n");
  800088:	83 ec 0c             	sub    $0xc,%esp
  80008b:	68 51 24 80 00       	push   $0x802451
  800090:	e8 d8 01 00 00       	call   80026d <cprintf>
	while ((n = read(fd, buf, sizeof buf-1)) > 0)
  800095:	83 c4 10             	add    $0x10,%esp
  800098:	8d 9d f7 fd ff ff    	lea    -0x209(%ebp),%ebx
  80009e:	eb 0d                	jmp    8000ad <umain+0x7a>
		sys_cputs(buf, n);
  8000a0:	83 ec 08             	sub    $0x8,%esp
  8000a3:	50                   	push   %eax
  8000a4:	53                   	push   %ebx
  8000a5:	e8 4f 0a 00 00       	call   800af9 <sys_cputs>
  8000aa:	83 c4 10             	add    $0x10,%esp
	cprintf("icode: open /motd\n");
	if ((fd = open("/motd", O_RDONLY)) < 0)
		panic("icode: open /motd: %e", fd);

	cprintf("icode: read /motd\n");
	while ((n = read(fd, buf, sizeof buf-1)) > 0)
  8000ad:	83 ec 04             	sub    $0x4,%esp
  8000b0:	68 00 02 00 00       	push   $0x200
  8000b5:	53                   	push   %ebx
  8000b6:	56                   	push   %esi
  8000b7:	e8 e5 0f 00 00       	call   8010a1 <read>
  8000bc:	83 c4 10             	add    $0x10,%esp
  8000bf:	85 c0                	test   %eax,%eax
  8000c1:	7f dd                	jg     8000a0 <umain+0x6d>
		sys_cputs(buf, n);

	cprintf("icode: close /motd\n");
  8000c3:	83 ec 0c             	sub    $0xc,%esp
  8000c6:	68 64 24 80 00       	push   $0x802464
  8000cb:	e8 9d 01 00 00       	call   80026d <cprintf>
	close(fd);
  8000d0:	89 34 24             	mov    %esi,(%esp)
  8000d3:	e8 91 0e 00 00       	call   800f69 <close>

	cprintf("icode: spawn /init\n");
  8000d8:	c7 04 24 78 24 80 00 	movl   $0x802478,(%esp)
  8000df:	e8 89 01 00 00       	call   80026d <cprintf>
	if ((r = spawnl("/init", "init", "initarg1", "initarg2", (char*)0)) < 0)
  8000e4:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  8000eb:	68 8c 24 80 00       	push   $0x80248c
  8000f0:	68 95 24 80 00       	push   $0x802495
  8000f5:	68 9f 24 80 00       	push   $0x80249f
  8000fa:	68 9e 24 80 00       	push   $0x80249e
  8000ff:	e8 1a 1a 00 00       	call   801b1e <spawnl>
  800104:	83 c4 20             	add    $0x20,%esp
  800107:	85 c0                	test   %eax,%eax
  800109:	79 12                	jns    80011d <umain+0xea>
		panic("icode: spawn /init: %e", r);
  80010b:	50                   	push   %eax
  80010c:	68 a4 24 80 00       	push   $0x8024a4
  800111:	6a 1a                	push   $0x1a
  800113:	68 44 24 80 00       	push   $0x802444
  800118:	e8 78 00 00 00       	call   800195 <_panic>

	cprintf("icode: exiting\n");
  80011d:	83 ec 0c             	sub    $0xc,%esp
  800120:	68 bb 24 80 00       	push   $0x8024bb
  800125:	e8 43 01 00 00       	call   80026d <cprintf>
}
  80012a:	83 c4 10             	add    $0x10,%esp
  80012d:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800130:	5b                   	pop    %ebx
  800131:	5e                   	pop    %esi
  800132:	5d                   	pop    %ebp
  800133:	c3                   	ret    

00800134 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800134:	55                   	push   %ebp
  800135:	89 e5                	mov    %esp,%ebp
  800137:	56                   	push   %esi
  800138:	53                   	push   %ebx
  800139:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80013c:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80013f:	e8 33 0a 00 00       	call   800b77 <sys_getenvid>
  800144:	25 ff 03 00 00       	and    $0x3ff,%eax
  800149:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800150:	c1 e0 07             	shl    $0x7,%eax
  800153:	29 d0                	sub    %edx,%eax
  800155:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80015a:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80015f:	85 db                	test   %ebx,%ebx
  800161:	7e 07                	jle    80016a <libmain+0x36>
		binaryname = argv[0];
  800163:	8b 06                	mov    (%esi),%eax
  800165:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  80016a:	83 ec 08             	sub    $0x8,%esp
  80016d:	56                   	push   %esi
  80016e:	53                   	push   %ebx
  80016f:	e8 bf fe ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800174:	e8 0a 00 00 00       	call   800183 <exit>
}
  800179:	83 c4 10             	add    $0x10,%esp
  80017c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80017f:	5b                   	pop    %ebx
  800180:	5e                   	pop    %esi
  800181:	5d                   	pop    %ebp
  800182:	c3                   	ret    

00800183 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800183:	55                   	push   %ebp
  800184:	89 e5                	mov    %esp,%ebp
  800186:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800189:	6a 00                	push   $0x0
  80018b:	e8 a6 09 00 00       	call   800b36 <sys_env_destroy>
}
  800190:	83 c4 10             	add    $0x10,%esp
  800193:	c9                   	leave  
  800194:	c3                   	ret    

00800195 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800195:	55                   	push   %ebp
  800196:	89 e5                	mov    %esp,%ebp
  800198:	56                   	push   %esi
  800199:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80019a:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80019d:	8b 35 00 30 80 00    	mov    0x803000,%esi
  8001a3:	e8 cf 09 00 00       	call   800b77 <sys_getenvid>
  8001a8:	83 ec 0c             	sub    $0xc,%esp
  8001ab:	ff 75 0c             	pushl  0xc(%ebp)
  8001ae:	ff 75 08             	pushl  0x8(%ebp)
  8001b1:	56                   	push   %esi
  8001b2:	50                   	push   %eax
  8001b3:	68 d8 24 80 00       	push   $0x8024d8
  8001b8:	e8 b0 00 00 00       	call   80026d <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8001bd:	83 c4 18             	add    $0x18,%esp
  8001c0:	53                   	push   %ebx
  8001c1:	ff 75 10             	pushl  0x10(%ebp)
  8001c4:	e8 53 00 00 00       	call   80021c <vcprintf>
	cprintf("\n");
  8001c9:	c7 04 24 b8 29 80 00 	movl   $0x8029b8,(%esp)
  8001d0:	e8 98 00 00 00       	call   80026d <cprintf>
  8001d5:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8001d8:	cc                   	int3   
  8001d9:	eb fd                	jmp    8001d8 <_panic+0x43>

008001db <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8001db:	55                   	push   %ebp
  8001dc:	89 e5                	mov    %esp,%ebp
  8001de:	53                   	push   %ebx
  8001df:	83 ec 04             	sub    $0x4,%esp
  8001e2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8001e5:	8b 13                	mov    (%ebx),%edx
  8001e7:	8d 42 01             	lea    0x1(%edx),%eax
  8001ea:	89 03                	mov    %eax,(%ebx)
  8001ec:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8001ef:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001f3:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001f8:	75 1a                	jne    800214 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001fa:	83 ec 08             	sub    $0x8,%esp
  8001fd:	68 ff 00 00 00       	push   $0xff
  800202:	8d 43 08             	lea    0x8(%ebx),%eax
  800205:	50                   	push   %eax
  800206:	e8 ee 08 00 00       	call   800af9 <sys_cputs>
		b->idx = 0;
  80020b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800211:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800214:	ff 43 04             	incl   0x4(%ebx)
}
  800217:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80021a:	c9                   	leave  
  80021b:	c3                   	ret    

0080021c <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80021c:	55                   	push   %ebp
  80021d:	89 e5                	mov    %esp,%ebp
  80021f:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800225:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80022c:	00 00 00 
	b.cnt = 0;
  80022f:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800236:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800239:	ff 75 0c             	pushl  0xc(%ebp)
  80023c:	ff 75 08             	pushl  0x8(%ebp)
  80023f:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800245:	50                   	push   %eax
  800246:	68 db 01 80 00       	push   $0x8001db
  80024b:	e8 51 01 00 00       	call   8003a1 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800250:	83 c4 08             	add    $0x8,%esp
  800253:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800259:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80025f:	50                   	push   %eax
  800260:	e8 94 08 00 00       	call   800af9 <sys_cputs>

	return b.cnt;
}
  800265:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80026b:	c9                   	leave  
  80026c:	c3                   	ret    

0080026d <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80026d:	55                   	push   %ebp
  80026e:	89 e5                	mov    %esp,%ebp
  800270:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800273:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800276:	50                   	push   %eax
  800277:	ff 75 08             	pushl  0x8(%ebp)
  80027a:	e8 9d ff ff ff       	call   80021c <vcprintf>
	va_end(ap);

	return cnt;
}
  80027f:	c9                   	leave  
  800280:	c3                   	ret    

00800281 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800281:	55                   	push   %ebp
  800282:	89 e5                	mov    %esp,%ebp
  800284:	57                   	push   %edi
  800285:	56                   	push   %esi
  800286:	53                   	push   %ebx
  800287:	83 ec 1c             	sub    $0x1c,%esp
  80028a:	89 c7                	mov    %eax,%edi
  80028c:	89 d6                	mov    %edx,%esi
  80028e:	8b 45 08             	mov    0x8(%ebp),%eax
  800291:	8b 55 0c             	mov    0xc(%ebp),%edx
  800294:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800297:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80029a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80029d:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002a2:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8002a5:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8002a8:	39 d3                	cmp    %edx,%ebx
  8002aa:	72 05                	jb     8002b1 <printnum+0x30>
  8002ac:	39 45 10             	cmp    %eax,0x10(%ebp)
  8002af:	77 45                	ja     8002f6 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8002b1:	83 ec 0c             	sub    $0xc,%esp
  8002b4:	ff 75 18             	pushl  0x18(%ebp)
  8002b7:	8b 45 14             	mov    0x14(%ebp),%eax
  8002ba:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8002bd:	53                   	push   %ebx
  8002be:	ff 75 10             	pushl  0x10(%ebp)
  8002c1:	83 ec 08             	sub    $0x8,%esp
  8002c4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002c7:	ff 75 e0             	pushl  -0x20(%ebp)
  8002ca:	ff 75 dc             	pushl  -0x24(%ebp)
  8002cd:	ff 75 d8             	pushl  -0x28(%ebp)
  8002d0:	e8 bb 1e 00 00       	call   802190 <__udivdi3>
  8002d5:	83 c4 18             	add    $0x18,%esp
  8002d8:	52                   	push   %edx
  8002d9:	50                   	push   %eax
  8002da:	89 f2                	mov    %esi,%edx
  8002dc:	89 f8                	mov    %edi,%eax
  8002de:	e8 9e ff ff ff       	call   800281 <printnum>
  8002e3:	83 c4 20             	add    $0x20,%esp
  8002e6:	eb 16                	jmp    8002fe <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8002e8:	83 ec 08             	sub    $0x8,%esp
  8002eb:	56                   	push   %esi
  8002ec:	ff 75 18             	pushl  0x18(%ebp)
  8002ef:	ff d7                	call   *%edi
  8002f1:	83 c4 10             	add    $0x10,%esp
  8002f4:	eb 03                	jmp    8002f9 <printnum+0x78>
  8002f6:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002f9:	4b                   	dec    %ebx
  8002fa:	85 db                	test   %ebx,%ebx
  8002fc:	7f ea                	jg     8002e8 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002fe:	83 ec 08             	sub    $0x8,%esp
  800301:	56                   	push   %esi
  800302:	83 ec 04             	sub    $0x4,%esp
  800305:	ff 75 e4             	pushl  -0x1c(%ebp)
  800308:	ff 75 e0             	pushl  -0x20(%ebp)
  80030b:	ff 75 dc             	pushl  -0x24(%ebp)
  80030e:	ff 75 d8             	pushl  -0x28(%ebp)
  800311:	e8 8a 1f 00 00       	call   8022a0 <__umoddi3>
  800316:	83 c4 14             	add    $0x14,%esp
  800319:	0f be 80 fb 24 80 00 	movsbl 0x8024fb(%eax),%eax
  800320:	50                   	push   %eax
  800321:	ff d7                	call   *%edi
}
  800323:	83 c4 10             	add    $0x10,%esp
  800326:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800329:	5b                   	pop    %ebx
  80032a:	5e                   	pop    %esi
  80032b:	5f                   	pop    %edi
  80032c:	5d                   	pop    %ebp
  80032d:	c3                   	ret    

0080032e <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80032e:	55                   	push   %ebp
  80032f:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800331:	83 fa 01             	cmp    $0x1,%edx
  800334:	7e 0e                	jle    800344 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800336:	8b 10                	mov    (%eax),%edx
  800338:	8d 4a 08             	lea    0x8(%edx),%ecx
  80033b:	89 08                	mov    %ecx,(%eax)
  80033d:	8b 02                	mov    (%edx),%eax
  80033f:	8b 52 04             	mov    0x4(%edx),%edx
  800342:	eb 22                	jmp    800366 <getuint+0x38>
	else if (lflag)
  800344:	85 d2                	test   %edx,%edx
  800346:	74 10                	je     800358 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800348:	8b 10                	mov    (%eax),%edx
  80034a:	8d 4a 04             	lea    0x4(%edx),%ecx
  80034d:	89 08                	mov    %ecx,(%eax)
  80034f:	8b 02                	mov    (%edx),%eax
  800351:	ba 00 00 00 00       	mov    $0x0,%edx
  800356:	eb 0e                	jmp    800366 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800358:	8b 10                	mov    (%eax),%edx
  80035a:	8d 4a 04             	lea    0x4(%edx),%ecx
  80035d:	89 08                	mov    %ecx,(%eax)
  80035f:	8b 02                	mov    (%edx),%eax
  800361:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800366:	5d                   	pop    %ebp
  800367:	c3                   	ret    

00800368 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800368:	55                   	push   %ebp
  800369:	89 e5                	mov    %esp,%ebp
  80036b:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80036e:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800371:	8b 10                	mov    (%eax),%edx
  800373:	3b 50 04             	cmp    0x4(%eax),%edx
  800376:	73 0a                	jae    800382 <sprintputch+0x1a>
		*b->buf++ = ch;
  800378:	8d 4a 01             	lea    0x1(%edx),%ecx
  80037b:	89 08                	mov    %ecx,(%eax)
  80037d:	8b 45 08             	mov    0x8(%ebp),%eax
  800380:	88 02                	mov    %al,(%edx)
}
  800382:	5d                   	pop    %ebp
  800383:	c3                   	ret    

00800384 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800384:	55                   	push   %ebp
  800385:	89 e5                	mov    %esp,%ebp
  800387:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80038a:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80038d:	50                   	push   %eax
  80038e:	ff 75 10             	pushl  0x10(%ebp)
  800391:	ff 75 0c             	pushl  0xc(%ebp)
  800394:	ff 75 08             	pushl  0x8(%ebp)
  800397:	e8 05 00 00 00       	call   8003a1 <vprintfmt>
	va_end(ap);
}
  80039c:	83 c4 10             	add    $0x10,%esp
  80039f:	c9                   	leave  
  8003a0:	c3                   	ret    

008003a1 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8003a1:	55                   	push   %ebp
  8003a2:	89 e5                	mov    %esp,%ebp
  8003a4:	57                   	push   %edi
  8003a5:	56                   	push   %esi
  8003a6:	53                   	push   %ebx
  8003a7:	83 ec 2c             	sub    $0x2c,%esp
  8003aa:	8b 75 08             	mov    0x8(%ebp),%esi
  8003ad:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8003b0:	8b 7d 10             	mov    0x10(%ebp),%edi
  8003b3:	eb 12                	jmp    8003c7 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8003b5:	85 c0                	test   %eax,%eax
  8003b7:	0f 84 68 03 00 00    	je     800725 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8003bd:	83 ec 08             	sub    $0x8,%esp
  8003c0:	53                   	push   %ebx
  8003c1:	50                   	push   %eax
  8003c2:	ff d6                	call   *%esi
  8003c4:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003c7:	47                   	inc    %edi
  8003c8:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8003cc:	83 f8 25             	cmp    $0x25,%eax
  8003cf:	75 e4                	jne    8003b5 <vprintfmt+0x14>
  8003d1:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8003d5:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8003dc:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003e3:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8003ea:	ba 00 00 00 00       	mov    $0x0,%edx
  8003ef:	eb 07                	jmp    8003f8 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f1:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003f4:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f8:	8d 47 01             	lea    0x1(%edi),%eax
  8003fb:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003fe:	0f b6 0f             	movzbl (%edi),%ecx
  800401:	8a 07                	mov    (%edi),%al
  800403:	83 e8 23             	sub    $0x23,%eax
  800406:	3c 55                	cmp    $0x55,%al
  800408:	0f 87 fe 02 00 00    	ja     80070c <vprintfmt+0x36b>
  80040e:	0f b6 c0             	movzbl %al,%eax
  800411:	ff 24 85 40 26 80 00 	jmp    *0x802640(,%eax,4)
  800418:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80041b:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80041f:	eb d7                	jmp    8003f8 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800421:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800424:	b8 00 00 00 00       	mov    $0x0,%eax
  800429:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80042c:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80042f:	01 c0                	add    %eax,%eax
  800431:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800435:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800438:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80043b:	83 fa 09             	cmp    $0x9,%edx
  80043e:	77 34                	ja     800474 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800440:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800441:	eb e9                	jmp    80042c <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800443:	8b 45 14             	mov    0x14(%ebp),%eax
  800446:	8d 48 04             	lea    0x4(%eax),%ecx
  800449:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80044c:	8b 00                	mov    (%eax),%eax
  80044e:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800451:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800454:	eb 24                	jmp    80047a <vprintfmt+0xd9>
  800456:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80045a:	79 07                	jns    800463 <vprintfmt+0xc2>
  80045c:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800463:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800466:	eb 90                	jmp    8003f8 <vprintfmt+0x57>
  800468:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80046b:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800472:	eb 84                	jmp    8003f8 <vprintfmt+0x57>
  800474:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800477:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80047a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80047e:	0f 89 74 ff ff ff    	jns    8003f8 <vprintfmt+0x57>
				width = precision, precision = -1;
  800484:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800487:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80048a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800491:	e9 62 ff ff ff       	jmp    8003f8 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800496:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800497:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80049a:	e9 59 ff ff ff       	jmp    8003f8 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80049f:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a2:	8d 50 04             	lea    0x4(%eax),%edx
  8004a5:	89 55 14             	mov    %edx,0x14(%ebp)
  8004a8:	83 ec 08             	sub    $0x8,%esp
  8004ab:	53                   	push   %ebx
  8004ac:	ff 30                	pushl  (%eax)
  8004ae:	ff d6                	call   *%esi
			break;
  8004b0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8004b6:	e9 0c ff ff ff       	jmp    8003c7 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004bb:	8b 45 14             	mov    0x14(%ebp),%eax
  8004be:	8d 50 04             	lea    0x4(%eax),%edx
  8004c1:	89 55 14             	mov    %edx,0x14(%ebp)
  8004c4:	8b 00                	mov    (%eax),%eax
  8004c6:	85 c0                	test   %eax,%eax
  8004c8:	79 02                	jns    8004cc <vprintfmt+0x12b>
  8004ca:	f7 d8                	neg    %eax
  8004cc:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8004ce:	83 f8 0f             	cmp    $0xf,%eax
  8004d1:	7f 0b                	jg     8004de <vprintfmt+0x13d>
  8004d3:	8b 04 85 a0 27 80 00 	mov    0x8027a0(,%eax,4),%eax
  8004da:	85 c0                	test   %eax,%eax
  8004dc:	75 18                	jne    8004f6 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8004de:	52                   	push   %edx
  8004df:	68 13 25 80 00       	push   $0x802513
  8004e4:	53                   	push   %ebx
  8004e5:	56                   	push   %esi
  8004e6:	e8 99 fe ff ff       	call   800384 <printfmt>
  8004eb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ee:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004f1:	e9 d1 fe ff ff       	jmp    8003c7 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004f6:	50                   	push   %eax
  8004f7:	68 d1 28 80 00       	push   $0x8028d1
  8004fc:	53                   	push   %ebx
  8004fd:	56                   	push   %esi
  8004fe:	e8 81 fe ff ff       	call   800384 <printfmt>
  800503:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800506:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800509:	e9 b9 fe ff ff       	jmp    8003c7 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80050e:	8b 45 14             	mov    0x14(%ebp),%eax
  800511:	8d 50 04             	lea    0x4(%eax),%edx
  800514:	89 55 14             	mov    %edx,0x14(%ebp)
  800517:	8b 38                	mov    (%eax),%edi
  800519:	85 ff                	test   %edi,%edi
  80051b:	75 05                	jne    800522 <vprintfmt+0x181>
				p = "(null)";
  80051d:	bf 0c 25 80 00       	mov    $0x80250c,%edi
			if (width > 0 && padc != '-')
  800522:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800526:	0f 8e 90 00 00 00    	jle    8005bc <vprintfmt+0x21b>
  80052c:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800530:	0f 84 8e 00 00 00    	je     8005c4 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800536:	83 ec 08             	sub    $0x8,%esp
  800539:	ff 75 d0             	pushl  -0x30(%ebp)
  80053c:	57                   	push   %edi
  80053d:	e8 70 02 00 00       	call   8007b2 <strnlen>
  800542:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800545:	29 c1                	sub    %eax,%ecx
  800547:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80054a:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80054d:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800551:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800554:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800557:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800559:	eb 0d                	jmp    800568 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80055b:	83 ec 08             	sub    $0x8,%esp
  80055e:	53                   	push   %ebx
  80055f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800562:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800564:	4f                   	dec    %edi
  800565:	83 c4 10             	add    $0x10,%esp
  800568:	85 ff                	test   %edi,%edi
  80056a:	7f ef                	jg     80055b <vprintfmt+0x1ba>
  80056c:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80056f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800572:	89 c8                	mov    %ecx,%eax
  800574:	85 c9                	test   %ecx,%ecx
  800576:	79 05                	jns    80057d <vprintfmt+0x1dc>
  800578:	b8 00 00 00 00       	mov    $0x0,%eax
  80057d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800580:	29 c1                	sub    %eax,%ecx
  800582:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800585:	89 75 08             	mov    %esi,0x8(%ebp)
  800588:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80058b:	eb 3d                	jmp    8005ca <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80058d:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800591:	74 19                	je     8005ac <vprintfmt+0x20b>
  800593:	0f be c0             	movsbl %al,%eax
  800596:	83 e8 20             	sub    $0x20,%eax
  800599:	83 f8 5e             	cmp    $0x5e,%eax
  80059c:	76 0e                	jbe    8005ac <vprintfmt+0x20b>
					putch('?', putdat);
  80059e:	83 ec 08             	sub    $0x8,%esp
  8005a1:	53                   	push   %ebx
  8005a2:	6a 3f                	push   $0x3f
  8005a4:	ff 55 08             	call   *0x8(%ebp)
  8005a7:	83 c4 10             	add    $0x10,%esp
  8005aa:	eb 0b                	jmp    8005b7 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8005ac:	83 ec 08             	sub    $0x8,%esp
  8005af:	53                   	push   %ebx
  8005b0:	52                   	push   %edx
  8005b1:	ff 55 08             	call   *0x8(%ebp)
  8005b4:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005b7:	ff 4d e4             	decl   -0x1c(%ebp)
  8005ba:	eb 0e                	jmp    8005ca <vprintfmt+0x229>
  8005bc:	89 75 08             	mov    %esi,0x8(%ebp)
  8005bf:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005c2:	eb 06                	jmp    8005ca <vprintfmt+0x229>
  8005c4:	89 75 08             	mov    %esi,0x8(%ebp)
  8005c7:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005ca:	47                   	inc    %edi
  8005cb:	8a 47 ff             	mov    -0x1(%edi),%al
  8005ce:	0f be d0             	movsbl %al,%edx
  8005d1:	85 d2                	test   %edx,%edx
  8005d3:	74 1d                	je     8005f2 <vprintfmt+0x251>
  8005d5:	85 f6                	test   %esi,%esi
  8005d7:	78 b4                	js     80058d <vprintfmt+0x1ec>
  8005d9:	4e                   	dec    %esi
  8005da:	79 b1                	jns    80058d <vprintfmt+0x1ec>
  8005dc:	8b 75 08             	mov    0x8(%ebp),%esi
  8005df:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005e2:	eb 14                	jmp    8005f8 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8005e4:	83 ec 08             	sub    $0x8,%esp
  8005e7:	53                   	push   %ebx
  8005e8:	6a 20                	push   $0x20
  8005ea:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005ec:	4f                   	dec    %edi
  8005ed:	83 c4 10             	add    $0x10,%esp
  8005f0:	eb 06                	jmp    8005f8 <vprintfmt+0x257>
  8005f2:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005f5:	8b 75 08             	mov    0x8(%ebp),%esi
  8005f8:	85 ff                	test   %edi,%edi
  8005fa:	7f e8                	jg     8005e4 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005fc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005ff:	e9 c3 fd ff ff       	jmp    8003c7 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800604:	83 fa 01             	cmp    $0x1,%edx
  800607:	7e 16                	jle    80061f <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800609:	8b 45 14             	mov    0x14(%ebp),%eax
  80060c:	8d 50 08             	lea    0x8(%eax),%edx
  80060f:	89 55 14             	mov    %edx,0x14(%ebp)
  800612:	8b 50 04             	mov    0x4(%eax),%edx
  800615:	8b 00                	mov    (%eax),%eax
  800617:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80061a:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80061d:	eb 32                	jmp    800651 <vprintfmt+0x2b0>
	else if (lflag)
  80061f:	85 d2                	test   %edx,%edx
  800621:	74 18                	je     80063b <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800623:	8b 45 14             	mov    0x14(%ebp),%eax
  800626:	8d 50 04             	lea    0x4(%eax),%edx
  800629:	89 55 14             	mov    %edx,0x14(%ebp)
  80062c:	8b 00                	mov    (%eax),%eax
  80062e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800631:	89 c1                	mov    %eax,%ecx
  800633:	c1 f9 1f             	sar    $0x1f,%ecx
  800636:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800639:	eb 16                	jmp    800651 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80063b:	8b 45 14             	mov    0x14(%ebp),%eax
  80063e:	8d 50 04             	lea    0x4(%eax),%edx
  800641:	89 55 14             	mov    %edx,0x14(%ebp)
  800644:	8b 00                	mov    (%eax),%eax
  800646:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800649:	89 c1                	mov    %eax,%ecx
  80064b:	c1 f9 1f             	sar    $0x1f,%ecx
  80064e:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800651:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800654:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800657:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80065b:	79 76                	jns    8006d3 <vprintfmt+0x332>
				putch('-', putdat);
  80065d:	83 ec 08             	sub    $0x8,%esp
  800660:	53                   	push   %ebx
  800661:	6a 2d                	push   $0x2d
  800663:	ff d6                	call   *%esi
				num = -(long long) num;
  800665:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800668:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80066b:	f7 d8                	neg    %eax
  80066d:	83 d2 00             	adc    $0x0,%edx
  800670:	f7 da                	neg    %edx
  800672:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800675:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80067a:	eb 5c                	jmp    8006d8 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80067c:	8d 45 14             	lea    0x14(%ebp),%eax
  80067f:	e8 aa fc ff ff       	call   80032e <getuint>
			base = 10;
  800684:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800689:	eb 4d                	jmp    8006d8 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80068b:	8d 45 14             	lea    0x14(%ebp),%eax
  80068e:	e8 9b fc ff ff       	call   80032e <getuint>
			base = 8;
  800693:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800698:	eb 3e                	jmp    8006d8 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80069a:	83 ec 08             	sub    $0x8,%esp
  80069d:	53                   	push   %ebx
  80069e:	6a 30                	push   $0x30
  8006a0:	ff d6                	call   *%esi
			putch('x', putdat);
  8006a2:	83 c4 08             	add    $0x8,%esp
  8006a5:	53                   	push   %ebx
  8006a6:	6a 78                	push   $0x78
  8006a8:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8006aa:	8b 45 14             	mov    0x14(%ebp),%eax
  8006ad:	8d 50 04             	lea    0x4(%eax),%edx
  8006b0:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006b3:	8b 00                	mov    (%eax),%eax
  8006b5:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8006ba:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8006bd:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8006c2:	eb 14                	jmp    8006d8 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006c4:	8d 45 14             	lea    0x14(%ebp),%eax
  8006c7:	e8 62 fc ff ff       	call   80032e <getuint>
			base = 16;
  8006cc:	b9 10 00 00 00       	mov    $0x10,%ecx
  8006d1:	eb 05                	jmp    8006d8 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8006d3:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006d8:	83 ec 0c             	sub    $0xc,%esp
  8006db:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8006df:	57                   	push   %edi
  8006e0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006e3:	51                   	push   %ecx
  8006e4:	52                   	push   %edx
  8006e5:	50                   	push   %eax
  8006e6:	89 da                	mov    %ebx,%edx
  8006e8:	89 f0                	mov    %esi,%eax
  8006ea:	e8 92 fb ff ff       	call   800281 <printnum>
			break;
  8006ef:	83 c4 20             	add    $0x20,%esp
  8006f2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006f5:	e9 cd fc ff ff       	jmp    8003c7 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006fa:	83 ec 08             	sub    $0x8,%esp
  8006fd:	53                   	push   %ebx
  8006fe:	51                   	push   %ecx
  8006ff:	ff d6                	call   *%esi
			break;
  800701:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800704:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800707:	e9 bb fc ff ff       	jmp    8003c7 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80070c:	83 ec 08             	sub    $0x8,%esp
  80070f:	53                   	push   %ebx
  800710:	6a 25                	push   $0x25
  800712:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800714:	83 c4 10             	add    $0x10,%esp
  800717:	eb 01                	jmp    80071a <vprintfmt+0x379>
  800719:	4f                   	dec    %edi
  80071a:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80071e:	75 f9                	jne    800719 <vprintfmt+0x378>
  800720:	e9 a2 fc ff ff       	jmp    8003c7 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800725:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800728:	5b                   	pop    %ebx
  800729:	5e                   	pop    %esi
  80072a:	5f                   	pop    %edi
  80072b:	5d                   	pop    %ebp
  80072c:	c3                   	ret    

0080072d <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80072d:	55                   	push   %ebp
  80072e:	89 e5                	mov    %esp,%ebp
  800730:	83 ec 18             	sub    $0x18,%esp
  800733:	8b 45 08             	mov    0x8(%ebp),%eax
  800736:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800739:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80073c:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800740:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800743:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80074a:	85 c0                	test   %eax,%eax
  80074c:	74 26                	je     800774 <vsnprintf+0x47>
  80074e:	85 d2                	test   %edx,%edx
  800750:	7e 29                	jle    80077b <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800752:	ff 75 14             	pushl  0x14(%ebp)
  800755:	ff 75 10             	pushl  0x10(%ebp)
  800758:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80075b:	50                   	push   %eax
  80075c:	68 68 03 80 00       	push   $0x800368
  800761:	e8 3b fc ff ff       	call   8003a1 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800766:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800769:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80076c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80076f:	83 c4 10             	add    $0x10,%esp
  800772:	eb 0c                	jmp    800780 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800774:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800779:	eb 05                	jmp    800780 <vsnprintf+0x53>
  80077b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800780:	c9                   	leave  
  800781:	c3                   	ret    

00800782 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800782:	55                   	push   %ebp
  800783:	89 e5                	mov    %esp,%ebp
  800785:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800788:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80078b:	50                   	push   %eax
  80078c:	ff 75 10             	pushl  0x10(%ebp)
  80078f:	ff 75 0c             	pushl  0xc(%ebp)
  800792:	ff 75 08             	pushl  0x8(%ebp)
  800795:	e8 93 ff ff ff       	call   80072d <vsnprintf>
	va_end(ap);

	return rc;
}
  80079a:	c9                   	leave  
  80079b:	c3                   	ret    

0080079c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80079c:	55                   	push   %ebp
  80079d:	89 e5                	mov    %esp,%ebp
  80079f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8007a2:	b8 00 00 00 00       	mov    $0x0,%eax
  8007a7:	eb 01                	jmp    8007aa <strlen+0xe>
		n++;
  8007a9:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8007aa:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8007ae:	75 f9                	jne    8007a9 <strlen+0xd>
		n++;
	return n;
}
  8007b0:	5d                   	pop    %ebp
  8007b1:	c3                   	ret    

008007b2 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8007b2:	55                   	push   %ebp
  8007b3:	89 e5                	mov    %esp,%ebp
  8007b5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007b8:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007bb:	ba 00 00 00 00       	mov    $0x0,%edx
  8007c0:	eb 01                	jmp    8007c3 <strnlen+0x11>
		n++;
  8007c2:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007c3:	39 c2                	cmp    %eax,%edx
  8007c5:	74 08                	je     8007cf <strnlen+0x1d>
  8007c7:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8007cb:	75 f5                	jne    8007c2 <strnlen+0x10>
  8007cd:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8007cf:	5d                   	pop    %ebp
  8007d0:	c3                   	ret    

008007d1 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8007d1:	55                   	push   %ebp
  8007d2:	89 e5                	mov    %esp,%ebp
  8007d4:	53                   	push   %ebx
  8007d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8007db:	89 c2                	mov    %eax,%edx
  8007dd:	42                   	inc    %edx
  8007de:	41                   	inc    %ecx
  8007df:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8007e2:	88 5a ff             	mov    %bl,-0x1(%edx)
  8007e5:	84 db                	test   %bl,%bl
  8007e7:	75 f4                	jne    8007dd <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8007e9:	5b                   	pop    %ebx
  8007ea:	5d                   	pop    %ebp
  8007eb:	c3                   	ret    

008007ec <strcat>:

char *
strcat(char *dst, const char *src)
{
  8007ec:	55                   	push   %ebp
  8007ed:	89 e5                	mov    %esp,%ebp
  8007ef:	53                   	push   %ebx
  8007f0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007f3:	53                   	push   %ebx
  8007f4:	e8 a3 ff ff ff       	call   80079c <strlen>
  8007f9:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007fc:	ff 75 0c             	pushl  0xc(%ebp)
  8007ff:	01 d8                	add    %ebx,%eax
  800801:	50                   	push   %eax
  800802:	e8 ca ff ff ff       	call   8007d1 <strcpy>
	return dst;
}
  800807:	89 d8                	mov    %ebx,%eax
  800809:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80080c:	c9                   	leave  
  80080d:	c3                   	ret    

0080080e <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80080e:	55                   	push   %ebp
  80080f:	89 e5                	mov    %esp,%ebp
  800811:	56                   	push   %esi
  800812:	53                   	push   %ebx
  800813:	8b 75 08             	mov    0x8(%ebp),%esi
  800816:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800819:	89 f3                	mov    %esi,%ebx
  80081b:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80081e:	89 f2                	mov    %esi,%edx
  800820:	eb 0c                	jmp    80082e <strncpy+0x20>
		*dst++ = *src;
  800822:	42                   	inc    %edx
  800823:	8a 01                	mov    (%ecx),%al
  800825:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800828:	80 39 01             	cmpb   $0x1,(%ecx)
  80082b:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80082e:	39 da                	cmp    %ebx,%edx
  800830:	75 f0                	jne    800822 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800832:	89 f0                	mov    %esi,%eax
  800834:	5b                   	pop    %ebx
  800835:	5e                   	pop    %esi
  800836:	5d                   	pop    %ebp
  800837:	c3                   	ret    

00800838 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800838:	55                   	push   %ebp
  800839:	89 e5                	mov    %esp,%ebp
  80083b:	56                   	push   %esi
  80083c:	53                   	push   %ebx
  80083d:	8b 75 08             	mov    0x8(%ebp),%esi
  800840:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800843:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800846:	85 c0                	test   %eax,%eax
  800848:	74 1e                	je     800868 <strlcpy+0x30>
  80084a:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80084e:	89 f2                	mov    %esi,%edx
  800850:	eb 05                	jmp    800857 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800852:	42                   	inc    %edx
  800853:	41                   	inc    %ecx
  800854:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800857:	39 c2                	cmp    %eax,%edx
  800859:	74 08                	je     800863 <strlcpy+0x2b>
  80085b:	8a 19                	mov    (%ecx),%bl
  80085d:	84 db                	test   %bl,%bl
  80085f:	75 f1                	jne    800852 <strlcpy+0x1a>
  800861:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800863:	c6 00 00             	movb   $0x0,(%eax)
  800866:	eb 02                	jmp    80086a <strlcpy+0x32>
  800868:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80086a:	29 f0                	sub    %esi,%eax
}
  80086c:	5b                   	pop    %ebx
  80086d:	5e                   	pop    %esi
  80086e:	5d                   	pop    %ebp
  80086f:	c3                   	ret    

00800870 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800870:	55                   	push   %ebp
  800871:	89 e5                	mov    %esp,%ebp
  800873:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800876:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800879:	eb 02                	jmp    80087d <strcmp+0xd>
		p++, q++;
  80087b:	41                   	inc    %ecx
  80087c:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80087d:	8a 01                	mov    (%ecx),%al
  80087f:	84 c0                	test   %al,%al
  800881:	74 04                	je     800887 <strcmp+0x17>
  800883:	3a 02                	cmp    (%edx),%al
  800885:	74 f4                	je     80087b <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800887:	0f b6 c0             	movzbl %al,%eax
  80088a:	0f b6 12             	movzbl (%edx),%edx
  80088d:	29 d0                	sub    %edx,%eax
}
  80088f:	5d                   	pop    %ebp
  800890:	c3                   	ret    

00800891 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800891:	55                   	push   %ebp
  800892:	89 e5                	mov    %esp,%ebp
  800894:	53                   	push   %ebx
  800895:	8b 45 08             	mov    0x8(%ebp),%eax
  800898:	8b 55 0c             	mov    0xc(%ebp),%edx
  80089b:	89 c3                	mov    %eax,%ebx
  80089d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8008a0:	eb 02                	jmp    8008a4 <strncmp+0x13>
		n--, p++, q++;
  8008a2:	40                   	inc    %eax
  8008a3:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8008a4:	39 d8                	cmp    %ebx,%eax
  8008a6:	74 14                	je     8008bc <strncmp+0x2b>
  8008a8:	8a 08                	mov    (%eax),%cl
  8008aa:	84 c9                	test   %cl,%cl
  8008ac:	74 04                	je     8008b2 <strncmp+0x21>
  8008ae:	3a 0a                	cmp    (%edx),%cl
  8008b0:	74 f0                	je     8008a2 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8008b2:	0f b6 00             	movzbl (%eax),%eax
  8008b5:	0f b6 12             	movzbl (%edx),%edx
  8008b8:	29 d0                	sub    %edx,%eax
  8008ba:	eb 05                	jmp    8008c1 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8008bc:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8008c1:	5b                   	pop    %ebx
  8008c2:	5d                   	pop    %ebp
  8008c3:	c3                   	ret    

008008c4 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8008c4:	55                   	push   %ebp
  8008c5:	89 e5                	mov    %esp,%ebp
  8008c7:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ca:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008cd:	eb 05                	jmp    8008d4 <strchr+0x10>
		if (*s == c)
  8008cf:	38 ca                	cmp    %cl,%dl
  8008d1:	74 0c                	je     8008df <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8008d3:	40                   	inc    %eax
  8008d4:	8a 10                	mov    (%eax),%dl
  8008d6:	84 d2                	test   %dl,%dl
  8008d8:	75 f5                	jne    8008cf <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8008da:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008df:	5d                   	pop    %ebp
  8008e0:	c3                   	ret    

008008e1 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8008e1:	55                   	push   %ebp
  8008e2:	89 e5                	mov    %esp,%ebp
  8008e4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e7:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008ea:	eb 05                	jmp    8008f1 <strfind+0x10>
		if (*s == c)
  8008ec:	38 ca                	cmp    %cl,%dl
  8008ee:	74 07                	je     8008f7 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8008f0:	40                   	inc    %eax
  8008f1:	8a 10                	mov    (%eax),%dl
  8008f3:	84 d2                	test   %dl,%dl
  8008f5:	75 f5                	jne    8008ec <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008f7:	5d                   	pop    %ebp
  8008f8:	c3                   	ret    

008008f9 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008f9:	55                   	push   %ebp
  8008fa:	89 e5                	mov    %esp,%ebp
  8008fc:	57                   	push   %edi
  8008fd:	56                   	push   %esi
  8008fe:	53                   	push   %ebx
  8008ff:	8b 7d 08             	mov    0x8(%ebp),%edi
  800902:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800905:	85 c9                	test   %ecx,%ecx
  800907:	74 36                	je     80093f <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800909:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80090f:	75 28                	jne    800939 <memset+0x40>
  800911:	f6 c1 03             	test   $0x3,%cl
  800914:	75 23                	jne    800939 <memset+0x40>
		c &= 0xFF;
  800916:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80091a:	89 d3                	mov    %edx,%ebx
  80091c:	c1 e3 08             	shl    $0x8,%ebx
  80091f:	89 d6                	mov    %edx,%esi
  800921:	c1 e6 18             	shl    $0x18,%esi
  800924:	89 d0                	mov    %edx,%eax
  800926:	c1 e0 10             	shl    $0x10,%eax
  800929:	09 f0                	or     %esi,%eax
  80092b:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80092d:	89 d8                	mov    %ebx,%eax
  80092f:	09 d0                	or     %edx,%eax
  800931:	c1 e9 02             	shr    $0x2,%ecx
  800934:	fc                   	cld    
  800935:	f3 ab                	rep stos %eax,%es:(%edi)
  800937:	eb 06                	jmp    80093f <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800939:	8b 45 0c             	mov    0xc(%ebp),%eax
  80093c:	fc                   	cld    
  80093d:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80093f:	89 f8                	mov    %edi,%eax
  800941:	5b                   	pop    %ebx
  800942:	5e                   	pop    %esi
  800943:	5f                   	pop    %edi
  800944:	5d                   	pop    %ebp
  800945:	c3                   	ret    

00800946 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800946:	55                   	push   %ebp
  800947:	89 e5                	mov    %esp,%ebp
  800949:	57                   	push   %edi
  80094a:	56                   	push   %esi
  80094b:	8b 45 08             	mov    0x8(%ebp),%eax
  80094e:	8b 75 0c             	mov    0xc(%ebp),%esi
  800951:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800954:	39 c6                	cmp    %eax,%esi
  800956:	73 33                	jae    80098b <memmove+0x45>
  800958:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80095b:	39 d0                	cmp    %edx,%eax
  80095d:	73 2c                	jae    80098b <memmove+0x45>
		s += n;
		d += n;
  80095f:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800962:	89 d6                	mov    %edx,%esi
  800964:	09 fe                	or     %edi,%esi
  800966:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80096c:	75 13                	jne    800981 <memmove+0x3b>
  80096e:	f6 c1 03             	test   $0x3,%cl
  800971:	75 0e                	jne    800981 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800973:	83 ef 04             	sub    $0x4,%edi
  800976:	8d 72 fc             	lea    -0x4(%edx),%esi
  800979:	c1 e9 02             	shr    $0x2,%ecx
  80097c:	fd                   	std    
  80097d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80097f:	eb 07                	jmp    800988 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800981:	4f                   	dec    %edi
  800982:	8d 72 ff             	lea    -0x1(%edx),%esi
  800985:	fd                   	std    
  800986:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800988:	fc                   	cld    
  800989:	eb 1d                	jmp    8009a8 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80098b:	89 f2                	mov    %esi,%edx
  80098d:	09 c2                	or     %eax,%edx
  80098f:	f6 c2 03             	test   $0x3,%dl
  800992:	75 0f                	jne    8009a3 <memmove+0x5d>
  800994:	f6 c1 03             	test   $0x3,%cl
  800997:	75 0a                	jne    8009a3 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800999:	c1 e9 02             	shr    $0x2,%ecx
  80099c:	89 c7                	mov    %eax,%edi
  80099e:	fc                   	cld    
  80099f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8009a1:	eb 05                	jmp    8009a8 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8009a3:	89 c7                	mov    %eax,%edi
  8009a5:	fc                   	cld    
  8009a6:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8009a8:	5e                   	pop    %esi
  8009a9:	5f                   	pop    %edi
  8009aa:	5d                   	pop    %ebp
  8009ab:	c3                   	ret    

008009ac <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8009ac:	55                   	push   %ebp
  8009ad:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8009af:	ff 75 10             	pushl  0x10(%ebp)
  8009b2:	ff 75 0c             	pushl  0xc(%ebp)
  8009b5:	ff 75 08             	pushl  0x8(%ebp)
  8009b8:	e8 89 ff ff ff       	call   800946 <memmove>
}
  8009bd:	c9                   	leave  
  8009be:	c3                   	ret    

008009bf <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8009bf:	55                   	push   %ebp
  8009c0:	89 e5                	mov    %esp,%ebp
  8009c2:	56                   	push   %esi
  8009c3:	53                   	push   %ebx
  8009c4:	8b 45 08             	mov    0x8(%ebp),%eax
  8009c7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009ca:	89 c6                	mov    %eax,%esi
  8009cc:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009cf:	eb 14                	jmp    8009e5 <memcmp+0x26>
		if (*s1 != *s2)
  8009d1:	8a 08                	mov    (%eax),%cl
  8009d3:	8a 1a                	mov    (%edx),%bl
  8009d5:	38 d9                	cmp    %bl,%cl
  8009d7:	74 0a                	je     8009e3 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8009d9:	0f b6 c1             	movzbl %cl,%eax
  8009dc:	0f b6 db             	movzbl %bl,%ebx
  8009df:	29 d8                	sub    %ebx,%eax
  8009e1:	eb 0b                	jmp    8009ee <memcmp+0x2f>
		s1++, s2++;
  8009e3:	40                   	inc    %eax
  8009e4:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009e5:	39 f0                	cmp    %esi,%eax
  8009e7:	75 e8                	jne    8009d1 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8009e9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009ee:	5b                   	pop    %ebx
  8009ef:	5e                   	pop    %esi
  8009f0:	5d                   	pop    %ebp
  8009f1:	c3                   	ret    

008009f2 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009f2:	55                   	push   %ebp
  8009f3:	89 e5                	mov    %esp,%ebp
  8009f5:	53                   	push   %ebx
  8009f6:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009f9:	89 c1                	mov    %eax,%ecx
  8009fb:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009fe:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a02:	eb 08                	jmp    800a0c <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a04:	0f b6 10             	movzbl (%eax),%edx
  800a07:	39 da                	cmp    %ebx,%edx
  800a09:	74 05                	je     800a10 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a0b:	40                   	inc    %eax
  800a0c:	39 c8                	cmp    %ecx,%eax
  800a0e:	72 f4                	jb     800a04 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800a10:	5b                   	pop    %ebx
  800a11:	5d                   	pop    %ebp
  800a12:	c3                   	ret    

00800a13 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800a13:	55                   	push   %ebp
  800a14:	89 e5                	mov    %esp,%ebp
  800a16:	57                   	push   %edi
  800a17:	56                   	push   %esi
  800a18:	53                   	push   %ebx
  800a19:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a1c:	eb 01                	jmp    800a1f <strtol+0xc>
		s++;
  800a1e:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a1f:	8a 01                	mov    (%ecx),%al
  800a21:	3c 20                	cmp    $0x20,%al
  800a23:	74 f9                	je     800a1e <strtol+0xb>
  800a25:	3c 09                	cmp    $0x9,%al
  800a27:	74 f5                	je     800a1e <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800a29:	3c 2b                	cmp    $0x2b,%al
  800a2b:	75 08                	jne    800a35 <strtol+0x22>
		s++;
  800a2d:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a2e:	bf 00 00 00 00       	mov    $0x0,%edi
  800a33:	eb 11                	jmp    800a46 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800a35:	3c 2d                	cmp    $0x2d,%al
  800a37:	75 08                	jne    800a41 <strtol+0x2e>
		s++, neg = 1;
  800a39:	41                   	inc    %ecx
  800a3a:	bf 01 00 00 00       	mov    $0x1,%edi
  800a3f:	eb 05                	jmp    800a46 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a41:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a46:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a4a:	0f 84 87 00 00 00    	je     800ad7 <strtol+0xc4>
  800a50:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a54:	75 27                	jne    800a7d <strtol+0x6a>
  800a56:	80 39 30             	cmpb   $0x30,(%ecx)
  800a59:	75 22                	jne    800a7d <strtol+0x6a>
  800a5b:	e9 88 00 00 00       	jmp    800ae8 <strtol+0xd5>
		s += 2, base = 16;
  800a60:	83 c1 02             	add    $0x2,%ecx
  800a63:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a6a:	eb 11                	jmp    800a7d <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a6c:	41                   	inc    %ecx
  800a6d:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a74:	eb 07                	jmp    800a7d <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a76:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a7d:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a82:	8a 11                	mov    (%ecx),%dl
  800a84:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a87:	80 fb 09             	cmp    $0x9,%bl
  800a8a:	77 08                	ja     800a94 <strtol+0x81>
			dig = *s - '0';
  800a8c:	0f be d2             	movsbl %dl,%edx
  800a8f:	83 ea 30             	sub    $0x30,%edx
  800a92:	eb 22                	jmp    800ab6 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a94:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a97:	89 f3                	mov    %esi,%ebx
  800a99:	80 fb 19             	cmp    $0x19,%bl
  800a9c:	77 08                	ja     800aa6 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a9e:	0f be d2             	movsbl %dl,%edx
  800aa1:	83 ea 57             	sub    $0x57,%edx
  800aa4:	eb 10                	jmp    800ab6 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800aa6:	8d 72 bf             	lea    -0x41(%edx),%esi
  800aa9:	89 f3                	mov    %esi,%ebx
  800aab:	80 fb 19             	cmp    $0x19,%bl
  800aae:	77 14                	ja     800ac4 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800ab0:	0f be d2             	movsbl %dl,%edx
  800ab3:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800ab6:	3b 55 10             	cmp    0x10(%ebp),%edx
  800ab9:	7d 09                	jge    800ac4 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800abb:	41                   	inc    %ecx
  800abc:	0f af 45 10          	imul   0x10(%ebp),%eax
  800ac0:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800ac2:	eb be                	jmp    800a82 <strtol+0x6f>

	if (endptr)
  800ac4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ac8:	74 05                	je     800acf <strtol+0xbc>
		*endptr = (char *) s;
  800aca:	8b 75 0c             	mov    0xc(%ebp),%esi
  800acd:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800acf:	85 ff                	test   %edi,%edi
  800ad1:	74 21                	je     800af4 <strtol+0xe1>
  800ad3:	f7 d8                	neg    %eax
  800ad5:	eb 1d                	jmp    800af4 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ad7:	80 39 30             	cmpb   $0x30,(%ecx)
  800ada:	75 9a                	jne    800a76 <strtol+0x63>
  800adc:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800ae0:	0f 84 7a ff ff ff    	je     800a60 <strtol+0x4d>
  800ae6:	eb 84                	jmp    800a6c <strtol+0x59>
  800ae8:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800aec:	0f 84 6e ff ff ff    	je     800a60 <strtol+0x4d>
  800af2:	eb 89                	jmp    800a7d <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800af4:	5b                   	pop    %ebx
  800af5:	5e                   	pop    %esi
  800af6:	5f                   	pop    %edi
  800af7:	5d                   	pop    %ebp
  800af8:	c3                   	ret    

00800af9 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800af9:	55                   	push   %ebp
  800afa:	89 e5                	mov    %esp,%ebp
  800afc:	57                   	push   %edi
  800afd:	56                   	push   %esi
  800afe:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aff:	b8 00 00 00 00       	mov    $0x0,%eax
  800b04:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b07:	8b 55 08             	mov    0x8(%ebp),%edx
  800b0a:	89 c3                	mov    %eax,%ebx
  800b0c:	89 c7                	mov    %eax,%edi
  800b0e:	89 c6                	mov    %eax,%esi
  800b10:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800b12:	5b                   	pop    %ebx
  800b13:	5e                   	pop    %esi
  800b14:	5f                   	pop    %edi
  800b15:	5d                   	pop    %ebp
  800b16:	c3                   	ret    

00800b17 <sys_cgetc>:

int
sys_cgetc(void)
{
  800b17:	55                   	push   %ebp
  800b18:	89 e5                	mov    %esp,%ebp
  800b1a:	57                   	push   %edi
  800b1b:	56                   	push   %esi
  800b1c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1d:	ba 00 00 00 00       	mov    $0x0,%edx
  800b22:	b8 01 00 00 00       	mov    $0x1,%eax
  800b27:	89 d1                	mov    %edx,%ecx
  800b29:	89 d3                	mov    %edx,%ebx
  800b2b:	89 d7                	mov    %edx,%edi
  800b2d:	89 d6                	mov    %edx,%esi
  800b2f:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800b31:	5b                   	pop    %ebx
  800b32:	5e                   	pop    %esi
  800b33:	5f                   	pop    %edi
  800b34:	5d                   	pop    %ebp
  800b35:	c3                   	ret    

00800b36 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800b36:	55                   	push   %ebp
  800b37:	89 e5                	mov    %esp,%ebp
  800b39:	57                   	push   %edi
  800b3a:	56                   	push   %esi
  800b3b:	53                   	push   %ebx
  800b3c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b3f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b44:	b8 03 00 00 00       	mov    $0x3,%eax
  800b49:	8b 55 08             	mov    0x8(%ebp),%edx
  800b4c:	89 cb                	mov    %ecx,%ebx
  800b4e:	89 cf                	mov    %ecx,%edi
  800b50:	89 ce                	mov    %ecx,%esi
  800b52:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b54:	85 c0                	test   %eax,%eax
  800b56:	7e 17                	jle    800b6f <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b58:	83 ec 0c             	sub    $0xc,%esp
  800b5b:	50                   	push   %eax
  800b5c:	6a 03                	push   $0x3
  800b5e:	68 ff 27 80 00       	push   $0x8027ff
  800b63:	6a 23                	push   $0x23
  800b65:	68 1c 28 80 00       	push   $0x80281c
  800b6a:	e8 26 f6 ff ff       	call   800195 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b6f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b72:	5b                   	pop    %ebx
  800b73:	5e                   	pop    %esi
  800b74:	5f                   	pop    %edi
  800b75:	5d                   	pop    %ebp
  800b76:	c3                   	ret    

00800b77 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b77:	55                   	push   %ebp
  800b78:	89 e5                	mov    %esp,%ebp
  800b7a:	57                   	push   %edi
  800b7b:	56                   	push   %esi
  800b7c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b7d:	ba 00 00 00 00       	mov    $0x0,%edx
  800b82:	b8 02 00 00 00       	mov    $0x2,%eax
  800b87:	89 d1                	mov    %edx,%ecx
  800b89:	89 d3                	mov    %edx,%ebx
  800b8b:	89 d7                	mov    %edx,%edi
  800b8d:	89 d6                	mov    %edx,%esi
  800b8f:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b91:	5b                   	pop    %ebx
  800b92:	5e                   	pop    %esi
  800b93:	5f                   	pop    %edi
  800b94:	5d                   	pop    %ebp
  800b95:	c3                   	ret    

00800b96 <sys_yield>:

void
sys_yield(void)
{
  800b96:	55                   	push   %ebp
  800b97:	89 e5                	mov    %esp,%ebp
  800b99:	57                   	push   %edi
  800b9a:	56                   	push   %esi
  800b9b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b9c:	ba 00 00 00 00       	mov    $0x0,%edx
  800ba1:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ba6:	89 d1                	mov    %edx,%ecx
  800ba8:	89 d3                	mov    %edx,%ebx
  800baa:	89 d7                	mov    %edx,%edi
  800bac:	89 d6                	mov    %edx,%esi
  800bae:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800bb0:	5b                   	pop    %ebx
  800bb1:	5e                   	pop    %esi
  800bb2:	5f                   	pop    %edi
  800bb3:	5d                   	pop    %ebp
  800bb4:	c3                   	ret    

00800bb5 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800bb5:	55                   	push   %ebp
  800bb6:	89 e5                	mov    %esp,%ebp
  800bb8:	57                   	push   %edi
  800bb9:	56                   	push   %esi
  800bba:	53                   	push   %ebx
  800bbb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bbe:	be 00 00 00 00       	mov    $0x0,%esi
  800bc3:	b8 04 00 00 00       	mov    $0x4,%eax
  800bc8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bcb:	8b 55 08             	mov    0x8(%ebp),%edx
  800bce:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800bd1:	89 f7                	mov    %esi,%edi
  800bd3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bd5:	85 c0                	test   %eax,%eax
  800bd7:	7e 17                	jle    800bf0 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bd9:	83 ec 0c             	sub    $0xc,%esp
  800bdc:	50                   	push   %eax
  800bdd:	6a 04                	push   $0x4
  800bdf:	68 ff 27 80 00       	push   $0x8027ff
  800be4:	6a 23                	push   $0x23
  800be6:	68 1c 28 80 00       	push   $0x80281c
  800beb:	e8 a5 f5 ff ff       	call   800195 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800bf0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf3:	5b                   	pop    %ebx
  800bf4:	5e                   	pop    %esi
  800bf5:	5f                   	pop    %edi
  800bf6:	5d                   	pop    %ebp
  800bf7:	c3                   	ret    

00800bf8 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800bf8:	55                   	push   %ebp
  800bf9:	89 e5                	mov    %esp,%ebp
  800bfb:	57                   	push   %edi
  800bfc:	56                   	push   %esi
  800bfd:	53                   	push   %ebx
  800bfe:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c01:	b8 05 00 00 00       	mov    $0x5,%eax
  800c06:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c09:	8b 55 08             	mov    0x8(%ebp),%edx
  800c0c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c0f:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c12:	8b 75 18             	mov    0x18(%ebp),%esi
  800c15:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c17:	85 c0                	test   %eax,%eax
  800c19:	7e 17                	jle    800c32 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c1b:	83 ec 0c             	sub    $0xc,%esp
  800c1e:	50                   	push   %eax
  800c1f:	6a 05                	push   $0x5
  800c21:	68 ff 27 80 00       	push   $0x8027ff
  800c26:	6a 23                	push   $0x23
  800c28:	68 1c 28 80 00       	push   $0x80281c
  800c2d:	e8 63 f5 ff ff       	call   800195 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800c32:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c35:	5b                   	pop    %ebx
  800c36:	5e                   	pop    %esi
  800c37:	5f                   	pop    %edi
  800c38:	5d                   	pop    %ebp
  800c39:	c3                   	ret    

00800c3a <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800c3a:	55                   	push   %ebp
  800c3b:	89 e5                	mov    %esp,%ebp
  800c3d:	57                   	push   %edi
  800c3e:	56                   	push   %esi
  800c3f:	53                   	push   %ebx
  800c40:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c43:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c48:	b8 06 00 00 00       	mov    $0x6,%eax
  800c4d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c50:	8b 55 08             	mov    0x8(%ebp),%edx
  800c53:	89 df                	mov    %ebx,%edi
  800c55:	89 de                	mov    %ebx,%esi
  800c57:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c59:	85 c0                	test   %eax,%eax
  800c5b:	7e 17                	jle    800c74 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c5d:	83 ec 0c             	sub    $0xc,%esp
  800c60:	50                   	push   %eax
  800c61:	6a 06                	push   $0x6
  800c63:	68 ff 27 80 00       	push   $0x8027ff
  800c68:	6a 23                	push   $0x23
  800c6a:	68 1c 28 80 00       	push   $0x80281c
  800c6f:	e8 21 f5 ff ff       	call   800195 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c74:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c77:	5b                   	pop    %ebx
  800c78:	5e                   	pop    %esi
  800c79:	5f                   	pop    %edi
  800c7a:	5d                   	pop    %ebp
  800c7b:	c3                   	ret    

00800c7c <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c7c:	55                   	push   %ebp
  800c7d:	89 e5                	mov    %esp,%ebp
  800c7f:	57                   	push   %edi
  800c80:	56                   	push   %esi
  800c81:	53                   	push   %ebx
  800c82:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c85:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c8a:	b8 08 00 00 00       	mov    $0x8,%eax
  800c8f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c92:	8b 55 08             	mov    0x8(%ebp),%edx
  800c95:	89 df                	mov    %ebx,%edi
  800c97:	89 de                	mov    %ebx,%esi
  800c99:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c9b:	85 c0                	test   %eax,%eax
  800c9d:	7e 17                	jle    800cb6 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c9f:	83 ec 0c             	sub    $0xc,%esp
  800ca2:	50                   	push   %eax
  800ca3:	6a 08                	push   $0x8
  800ca5:	68 ff 27 80 00       	push   $0x8027ff
  800caa:	6a 23                	push   $0x23
  800cac:	68 1c 28 80 00       	push   $0x80281c
  800cb1:	e8 df f4 ff ff       	call   800195 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800cb6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cb9:	5b                   	pop    %ebx
  800cba:	5e                   	pop    %esi
  800cbb:	5f                   	pop    %edi
  800cbc:	5d                   	pop    %ebp
  800cbd:	c3                   	ret    

00800cbe <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800cbe:	55                   	push   %ebp
  800cbf:	89 e5                	mov    %esp,%ebp
  800cc1:	57                   	push   %edi
  800cc2:	56                   	push   %esi
  800cc3:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cc4:	ba 00 00 00 00       	mov    $0x0,%edx
  800cc9:	b8 0c 00 00 00       	mov    $0xc,%eax
  800cce:	89 d1                	mov    %edx,%ecx
  800cd0:	89 d3                	mov    %edx,%ebx
  800cd2:	89 d7                	mov    %edx,%edi
  800cd4:	89 d6                	mov    %edx,%esi
  800cd6:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800cd8:	5b                   	pop    %ebx
  800cd9:	5e                   	pop    %esi
  800cda:	5f                   	pop    %edi
  800cdb:	5d                   	pop    %ebp
  800cdc:	c3                   	ret    

00800cdd <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800cdd:	55                   	push   %ebp
  800cde:	89 e5                	mov    %esp,%ebp
  800ce0:	57                   	push   %edi
  800ce1:	56                   	push   %esi
  800ce2:	53                   	push   %ebx
  800ce3:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ce6:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ceb:	b8 09 00 00 00       	mov    $0x9,%eax
  800cf0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cf3:	8b 55 08             	mov    0x8(%ebp),%edx
  800cf6:	89 df                	mov    %ebx,%edi
  800cf8:	89 de                	mov    %ebx,%esi
  800cfa:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cfc:	85 c0                	test   %eax,%eax
  800cfe:	7e 17                	jle    800d17 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d00:	83 ec 0c             	sub    $0xc,%esp
  800d03:	50                   	push   %eax
  800d04:	6a 09                	push   $0x9
  800d06:	68 ff 27 80 00       	push   $0x8027ff
  800d0b:	6a 23                	push   $0x23
  800d0d:	68 1c 28 80 00       	push   $0x80281c
  800d12:	e8 7e f4 ff ff       	call   800195 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800d17:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d1a:	5b                   	pop    %ebx
  800d1b:	5e                   	pop    %esi
  800d1c:	5f                   	pop    %edi
  800d1d:	5d                   	pop    %ebp
  800d1e:	c3                   	ret    

00800d1f <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800d1f:	55                   	push   %ebp
  800d20:	89 e5                	mov    %esp,%ebp
  800d22:	57                   	push   %edi
  800d23:	56                   	push   %esi
  800d24:	53                   	push   %ebx
  800d25:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d28:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d2d:	b8 0a 00 00 00       	mov    $0xa,%eax
  800d32:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d35:	8b 55 08             	mov    0x8(%ebp),%edx
  800d38:	89 df                	mov    %ebx,%edi
  800d3a:	89 de                	mov    %ebx,%esi
  800d3c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d3e:	85 c0                	test   %eax,%eax
  800d40:	7e 17                	jle    800d59 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d42:	83 ec 0c             	sub    $0xc,%esp
  800d45:	50                   	push   %eax
  800d46:	6a 0a                	push   $0xa
  800d48:	68 ff 27 80 00       	push   $0x8027ff
  800d4d:	6a 23                	push   $0x23
  800d4f:	68 1c 28 80 00       	push   $0x80281c
  800d54:	e8 3c f4 ff ff       	call   800195 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800d59:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d5c:	5b                   	pop    %ebx
  800d5d:	5e                   	pop    %esi
  800d5e:	5f                   	pop    %edi
  800d5f:	5d                   	pop    %ebp
  800d60:	c3                   	ret    

00800d61 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800d61:	55                   	push   %ebp
  800d62:	89 e5                	mov    %esp,%ebp
  800d64:	57                   	push   %edi
  800d65:	56                   	push   %esi
  800d66:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d67:	be 00 00 00 00       	mov    $0x0,%esi
  800d6c:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d71:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d74:	8b 55 08             	mov    0x8(%ebp),%edx
  800d77:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d7a:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d7d:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d7f:	5b                   	pop    %ebx
  800d80:	5e                   	pop    %esi
  800d81:	5f                   	pop    %edi
  800d82:	5d                   	pop    %ebp
  800d83:	c3                   	ret    

00800d84 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d84:	55                   	push   %ebp
  800d85:	89 e5                	mov    %esp,%ebp
  800d87:	57                   	push   %edi
  800d88:	56                   	push   %esi
  800d89:	53                   	push   %ebx
  800d8a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d8d:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d92:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d97:	8b 55 08             	mov    0x8(%ebp),%edx
  800d9a:	89 cb                	mov    %ecx,%ebx
  800d9c:	89 cf                	mov    %ecx,%edi
  800d9e:	89 ce                	mov    %ecx,%esi
  800da0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800da2:	85 c0                	test   %eax,%eax
  800da4:	7e 17                	jle    800dbd <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800da6:	83 ec 0c             	sub    $0xc,%esp
  800da9:	50                   	push   %eax
  800daa:	6a 0e                	push   $0xe
  800dac:	68 ff 27 80 00       	push   $0x8027ff
  800db1:	6a 23                	push   $0x23
  800db3:	68 1c 28 80 00       	push   $0x80281c
  800db8:	e8 d8 f3 ff ff       	call   800195 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800dbd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dc0:	5b                   	pop    %ebx
  800dc1:	5e                   	pop    %esi
  800dc2:	5f                   	pop    %edi
  800dc3:	5d                   	pop    %ebp
  800dc4:	c3                   	ret    

00800dc5 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800dc5:	55                   	push   %ebp
  800dc6:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  800dc8:	8b 45 08             	mov    0x8(%ebp),%eax
  800dcb:	05 00 00 00 30       	add    $0x30000000,%eax
  800dd0:	c1 e8 0c             	shr    $0xc,%eax
}
  800dd3:	5d                   	pop    %ebp
  800dd4:	c3                   	ret    

00800dd5 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  800dd5:	55                   	push   %ebp
  800dd6:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  800dd8:	8b 45 08             	mov    0x8(%ebp),%eax
  800ddb:	05 00 00 00 30       	add    $0x30000000,%eax
  800de0:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800de5:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  800dea:	5d                   	pop    %ebp
  800deb:	c3                   	ret    

00800dec <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  800dec:	55                   	push   %ebp
  800ded:	89 e5                	mov    %esp,%ebp
  800def:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800df2:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  800df7:	89 c2                	mov    %eax,%edx
  800df9:	c1 ea 16             	shr    $0x16,%edx
  800dfc:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800e03:	f6 c2 01             	test   $0x1,%dl
  800e06:	74 11                	je     800e19 <fd_alloc+0x2d>
  800e08:	89 c2                	mov    %eax,%edx
  800e0a:	c1 ea 0c             	shr    $0xc,%edx
  800e0d:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800e14:	f6 c2 01             	test   $0x1,%dl
  800e17:	75 09                	jne    800e22 <fd_alloc+0x36>
			*fd_store = fd;
  800e19:	89 01                	mov    %eax,(%ecx)
			return 0;
  800e1b:	b8 00 00 00 00       	mov    $0x0,%eax
  800e20:	eb 17                	jmp    800e39 <fd_alloc+0x4d>
  800e22:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  800e27:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  800e2c:	75 c9                	jne    800df7 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  800e2e:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  800e34:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  800e39:	5d                   	pop    %ebp
  800e3a:	c3                   	ret    

00800e3b <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  800e3b:	55                   	push   %ebp
  800e3c:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  800e3e:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  800e42:	77 39                	ja     800e7d <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  800e44:	8b 45 08             	mov    0x8(%ebp),%eax
  800e47:	c1 e0 0c             	shl    $0xc,%eax
  800e4a:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  800e4f:	89 c2                	mov    %eax,%edx
  800e51:	c1 ea 16             	shr    $0x16,%edx
  800e54:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800e5b:	f6 c2 01             	test   $0x1,%dl
  800e5e:	74 24                	je     800e84 <fd_lookup+0x49>
  800e60:	89 c2                	mov    %eax,%edx
  800e62:	c1 ea 0c             	shr    $0xc,%edx
  800e65:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800e6c:	f6 c2 01             	test   $0x1,%dl
  800e6f:	74 1a                	je     800e8b <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  800e71:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e74:	89 02                	mov    %eax,(%edx)
	return 0;
  800e76:	b8 00 00 00 00       	mov    $0x0,%eax
  800e7b:	eb 13                	jmp    800e90 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800e7d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800e82:	eb 0c                	jmp    800e90 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800e84:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800e89:	eb 05                	jmp    800e90 <fd_lookup+0x55>
  800e8b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  800e90:	5d                   	pop    %ebp
  800e91:	c3                   	ret    

00800e92 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  800e92:	55                   	push   %ebp
  800e93:	89 e5                	mov    %esp,%ebp
  800e95:	83 ec 08             	sub    $0x8,%esp
  800e98:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800e9b:	ba a8 28 80 00       	mov    $0x8028a8,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  800ea0:	eb 13                	jmp    800eb5 <dev_lookup+0x23>
  800ea2:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  800ea5:	39 08                	cmp    %ecx,(%eax)
  800ea7:	75 0c                	jne    800eb5 <dev_lookup+0x23>
			*dev = devtab[i];
  800ea9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800eac:	89 01                	mov    %eax,(%ecx)
			return 0;
  800eae:	b8 00 00 00 00       	mov    $0x0,%eax
  800eb3:	eb 2e                	jmp    800ee3 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  800eb5:	8b 02                	mov    (%edx),%eax
  800eb7:	85 c0                	test   %eax,%eax
  800eb9:	75 e7                	jne    800ea2 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  800ebb:	a1 04 40 80 00       	mov    0x804004,%eax
  800ec0:	8b 40 48             	mov    0x48(%eax),%eax
  800ec3:	83 ec 04             	sub    $0x4,%esp
  800ec6:	51                   	push   %ecx
  800ec7:	50                   	push   %eax
  800ec8:	68 2c 28 80 00       	push   $0x80282c
  800ecd:	e8 9b f3 ff ff       	call   80026d <cprintf>
	*dev = 0;
  800ed2:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ed5:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  800edb:	83 c4 10             	add    $0x10,%esp
  800ede:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  800ee3:	c9                   	leave  
  800ee4:	c3                   	ret    

00800ee5 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  800ee5:	55                   	push   %ebp
  800ee6:	89 e5                	mov    %esp,%ebp
  800ee8:	56                   	push   %esi
  800ee9:	53                   	push   %ebx
  800eea:	83 ec 10             	sub    $0x10,%esp
  800eed:	8b 75 08             	mov    0x8(%ebp),%esi
  800ef0:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  800ef3:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800ef6:	50                   	push   %eax
  800ef7:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  800efd:	c1 e8 0c             	shr    $0xc,%eax
  800f00:	50                   	push   %eax
  800f01:	e8 35 ff ff ff       	call   800e3b <fd_lookup>
  800f06:	83 c4 08             	add    $0x8,%esp
  800f09:	85 c0                	test   %eax,%eax
  800f0b:	78 05                	js     800f12 <fd_close+0x2d>
	    || fd != fd2)
  800f0d:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  800f10:	74 06                	je     800f18 <fd_close+0x33>
		return (must_exist ? r : 0);
  800f12:	84 db                	test   %bl,%bl
  800f14:	74 47                	je     800f5d <fd_close+0x78>
  800f16:	eb 4a                	jmp    800f62 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  800f18:	83 ec 08             	sub    $0x8,%esp
  800f1b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800f1e:	50                   	push   %eax
  800f1f:	ff 36                	pushl  (%esi)
  800f21:	e8 6c ff ff ff       	call   800e92 <dev_lookup>
  800f26:	89 c3                	mov    %eax,%ebx
  800f28:	83 c4 10             	add    $0x10,%esp
  800f2b:	85 c0                	test   %eax,%eax
  800f2d:	78 1c                	js     800f4b <fd_close+0x66>
		if (dev->dev_close)
  800f2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800f32:	8b 40 10             	mov    0x10(%eax),%eax
  800f35:	85 c0                	test   %eax,%eax
  800f37:	74 0d                	je     800f46 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  800f39:	83 ec 0c             	sub    $0xc,%esp
  800f3c:	56                   	push   %esi
  800f3d:	ff d0                	call   *%eax
  800f3f:	89 c3                	mov    %eax,%ebx
  800f41:	83 c4 10             	add    $0x10,%esp
  800f44:	eb 05                	jmp    800f4b <fd_close+0x66>
		else
			r = 0;
  800f46:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  800f4b:	83 ec 08             	sub    $0x8,%esp
  800f4e:	56                   	push   %esi
  800f4f:	6a 00                	push   $0x0
  800f51:	e8 e4 fc ff ff       	call   800c3a <sys_page_unmap>
	return r;
  800f56:	83 c4 10             	add    $0x10,%esp
  800f59:	89 d8                	mov    %ebx,%eax
  800f5b:	eb 05                	jmp    800f62 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  800f5d:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  800f62:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800f65:	5b                   	pop    %ebx
  800f66:	5e                   	pop    %esi
  800f67:	5d                   	pop    %ebp
  800f68:	c3                   	ret    

00800f69 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  800f69:	55                   	push   %ebp
  800f6a:	89 e5                	mov    %esp,%ebp
  800f6c:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800f6f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800f72:	50                   	push   %eax
  800f73:	ff 75 08             	pushl  0x8(%ebp)
  800f76:	e8 c0 fe ff ff       	call   800e3b <fd_lookup>
  800f7b:	83 c4 08             	add    $0x8,%esp
  800f7e:	85 c0                	test   %eax,%eax
  800f80:	78 10                	js     800f92 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  800f82:	83 ec 08             	sub    $0x8,%esp
  800f85:	6a 01                	push   $0x1
  800f87:	ff 75 f4             	pushl  -0xc(%ebp)
  800f8a:	e8 56 ff ff ff       	call   800ee5 <fd_close>
  800f8f:	83 c4 10             	add    $0x10,%esp
}
  800f92:	c9                   	leave  
  800f93:	c3                   	ret    

00800f94 <close_all>:

void
close_all(void)
{
  800f94:	55                   	push   %ebp
  800f95:	89 e5                	mov    %esp,%ebp
  800f97:	53                   	push   %ebx
  800f98:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  800f9b:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  800fa0:	83 ec 0c             	sub    $0xc,%esp
  800fa3:	53                   	push   %ebx
  800fa4:	e8 c0 ff ff ff       	call   800f69 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  800fa9:	43                   	inc    %ebx
  800faa:	83 c4 10             	add    $0x10,%esp
  800fad:	83 fb 20             	cmp    $0x20,%ebx
  800fb0:	75 ee                	jne    800fa0 <close_all+0xc>
		close(i);
}
  800fb2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800fb5:	c9                   	leave  
  800fb6:	c3                   	ret    

00800fb7 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  800fb7:	55                   	push   %ebp
  800fb8:	89 e5                	mov    %esp,%ebp
  800fba:	57                   	push   %edi
  800fbb:	56                   	push   %esi
  800fbc:	53                   	push   %ebx
  800fbd:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  800fc0:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  800fc3:	50                   	push   %eax
  800fc4:	ff 75 08             	pushl  0x8(%ebp)
  800fc7:	e8 6f fe ff ff       	call   800e3b <fd_lookup>
  800fcc:	83 c4 08             	add    $0x8,%esp
  800fcf:	85 c0                	test   %eax,%eax
  800fd1:	0f 88 c2 00 00 00    	js     801099 <dup+0xe2>
		return r;
	close(newfdnum);
  800fd7:	83 ec 0c             	sub    $0xc,%esp
  800fda:	ff 75 0c             	pushl  0xc(%ebp)
  800fdd:	e8 87 ff ff ff       	call   800f69 <close>

	newfd = INDEX2FD(newfdnum);
  800fe2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800fe5:	c1 e3 0c             	shl    $0xc,%ebx
  800fe8:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  800fee:	83 c4 04             	add    $0x4,%esp
  800ff1:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ff4:	e8 dc fd ff ff       	call   800dd5 <fd2data>
  800ff9:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  800ffb:	89 1c 24             	mov    %ebx,(%esp)
  800ffe:	e8 d2 fd ff ff       	call   800dd5 <fd2data>
  801003:	83 c4 10             	add    $0x10,%esp
  801006:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801008:	89 f0                	mov    %esi,%eax
  80100a:	c1 e8 16             	shr    $0x16,%eax
  80100d:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801014:	a8 01                	test   $0x1,%al
  801016:	74 35                	je     80104d <dup+0x96>
  801018:	89 f0                	mov    %esi,%eax
  80101a:	c1 e8 0c             	shr    $0xc,%eax
  80101d:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801024:	f6 c2 01             	test   $0x1,%dl
  801027:	74 24                	je     80104d <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801029:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801030:	83 ec 0c             	sub    $0xc,%esp
  801033:	25 07 0e 00 00       	and    $0xe07,%eax
  801038:	50                   	push   %eax
  801039:	57                   	push   %edi
  80103a:	6a 00                	push   $0x0
  80103c:	56                   	push   %esi
  80103d:	6a 00                	push   $0x0
  80103f:	e8 b4 fb ff ff       	call   800bf8 <sys_page_map>
  801044:	89 c6                	mov    %eax,%esi
  801046:	83 c4 20             	add    $0x20,%esp
  801049:	85 c0                	test   %eax,%eax
  80104b:	78 2c                	js     801079 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  80104d:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801050:	89 d0                	mov    %edx,%eax
  801052:	c1 e8 0c             	shr    $0xc,%eax
  801055:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80105c:	83 ec 0c             	sub    $0xc,%esp
  80105f:	25 07 0e 00 00       	and    $0xe07,%eax
  801064:	50                   	push   %eax
  801065:	53                   	push   %ebx
  801066:	6a 00                	push   $0x0
  801068:	52                   	push   %edx
  801069:	6a 00                	push   $0x0
  80106b:	e8 88 fb ff ff       	call   800bf8 <sys_page_map>
  801070:	89 c6                	mov    %eax,%esi
  801072:	83 c4 20             	add    $0x20,%esp
  801075:	85 c0                	test   %eax,%eax
  801077:	79 1d                	jns    801096 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801079:	83 ec 08             	sub    $0x8,%esp
  80107c:	53                   	push   %ebx
  80107d:	6a 00                	push   $0x0
  80107f:	e8 b6 fb ff ff       	call   800c3a <sys_page_unmap>
	sys_page_unmap(0, nva);
  801084:	83 c4 08             	add    $0x8,%esp
  801087:	57                   	push   %edi
  801088:	6a 00                	push   $0x0
  80108a:	e8 ab fb ff ff       	call   800c3a <sys_page_unmap>
	return r;
  80108f:	83 c4 10             	add    $0x10,%esp
  801092:	89 f0                	mov    %esi,%eax
  801094:	eb 03                	jmp    801099 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801096:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801099:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80109c:	5b                   	pop    %ebx
  80109d:	5e                   	pop    %esi
  80109e:	5f                   	pop    %edi
  80109f:	5d                   	pop    %ebp
  8010a0:	c3                   	ret    

008010a1 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8010a1:	55                   	push   %ebp
  8010a2:	89 e5                	mov    %esp,%ebp
  8010a4:	53                   	push   %ebx
  8010a5:	83 ec 14             	sub    $0x14,%esp
  8010a8:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8010ab:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8010ae:	50                   	push   %eax
  8010af:	53                   	push   %ebx
  8010b0:	e8 86 fd ff ff       	call   800e3b <fd_lookup>
  8010b5:	83 c4 08             	add    $0x8,%esp
  8010b8:	85 c0                	test   %eax,%eax
  8010ba:	78 67                	js     801123 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8010bc:	83 ec 08             	sub    $0x8,%esp
  8010bf:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8010c2:	50                   	push   %eax
  8010c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8010c6:	ff 30                	pushl  (%eax)
  8010c8:	e8 c5 fd ff ff       	call   800e92 <dev_lookup>
  8010cd:	83 c4 10             	add    $0x10,%esp
  8010d0:	85 c0                	test   %eax,%eax
  8010d2:	78 4f                	js     801123 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8010d4:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8010d7:	8b 42 08             	mov    0x8(%edx),%eax
  8010da:	83 e0 03             	and    $0x3,%eax
  8010dd:	83 f8 01             	cmp    $0x1,%eax
  8010e0:	75 21                	jne    801103 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  8010e2:	a1 04 40 80 00       	mov    0x804004,%eax
  8010e7:	8b 40 48             	mov    0x48(%eax),%eax
  8010ea:	83 ec 04             	sub    $0x4,%esp
  8010ed:	53                   	push   %ebx
  8010ee:	50                   	push   %eax
  8010ef:	68 6d 28 80 00       	push   $0x80286d
  8010f4:	e8 74 f1 ff ff       	call   80026d <cprintf>
		return -E_INVAL;
  8010f9:	83 c4 10             	add    $0x10,%esp
  8010fc:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801101:	eb 20                	jmp    801123 <read+0x82>
	}
	if (!dev->dev_read)
  801103:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801106:	8b 40 08             	mov    0x8(%eax),%eax
  801109:	85 c0                	test   %eax,%eax
  80110b:	74 11                	je     80111e <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  80110d:	83 ec 04             	sub    $0x4,%esp
  801110:	ff 75 10             	pushl  0x10(%ebp)
  801113:	ff 75 0c             	pushl  0xc(%ebp)
  801116:	52                   	push   %edx
  801117:	ff d0                	call   *%eax
  801119:	83 c4 10             	add    $0x10,%esp
  80111c:	eb 05                	jmp    801123 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  80111e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801123:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801126:	c9                   	leave  
  801127:	c3                   	ret    

00801128 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801128:	55                   	push   %ebp
  801129:	89 e5                	mov    %esp,%ebp
  80112b:	57                   	push   %edi
  80112c:	56                   	push   %esi
  80112d:	53                   	push   %ebx
  80112e:	83 ec 0c             	sub    $0xc,%esp
  801131:	8b 7d 08             	mov    0x8(%ebp),%edi
  801134:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801137:	bb 00 00 00 00       	mov    $0x0,%ebx
  80113c:	eb 21                	jmp    80115f <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  80113e:	83 ec 04             	sub    $0x4,%esp
  801141:	89 f0                	mov    %esi,%eax
  801143:	29 d8                	sub    %ebx,%eax
  801145:	50                   	push   %eax
  801146:	89 d8                	mov    %ebx,%eax
  801148:	03 45 0c             	add    0xc(%ebp),%eax
  80114b:	50                   	push   %eax
  80114c:	57                   	push   %edi
  80114d:	e8 4f ff ff ff       	call   8010a1 <read>
		if (m < 0)
  801152:	83 c4 10             	add    $0x10,%esp
  801155:	85 c0                	test   %eax,%eax
  801157:	78 10                	js     801169 <readn+0x41>
			return m;
		if (m == 0)
  801159:	85 c0                	test   %eax,%eax
  80115b:	74 0a                	je     801167 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80115d:	01 c3                	add    %eax,%ebx
  80115f:	39 f3                	cmp    %esi,%ebx
  801161:	72 db                	jb     80113e <readn+0x16>
  801163:	89 d8                	mov    %ebx,%eax
  801165:	eb 02                	jmp    801169 <readn+0x41>
  801167:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801169:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80116c:	5b                   	pop    %ebx
  80116d:	5e                   	pop    %esi
  80116e:	5f                   	pop    %edi
  80116f:	5d                   	pop    %ebp
  801170:	c3                   	ret    

00801171 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801171:	55                   	push   %ebp
  801172:	89 e5                	mov    %esp,%ebp
  801174:	53                   	push   %ebx
  801175:	83 ec 14             	sub    $0x14,%esp
  801178:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80117b:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80117e:	50                   	push   %eax
  80117f:	53                   	push   %ebx
  801180:	e8 b6 fc ff ff       	call   800e3b <fd_lookup>
  801185:	83 c4 08             	add    $0x8,%esp
  801188:	85 c0                	test   %eax,%eax
  80118a:	78 62                	js     8011ee <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80118c:	83 ec 08             	sub    $0x8,%esp
  80118f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801192:	50                   	push   %eax
  801193:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801196:	ff 30                	pushl  (%eax)
  801198:	e8 f5 fc ff ff       	call   800e92 <dev_lookup>
  80119d:	83 c4 10             	add    $0x10,%esp
  8011a0:	85 c0                	test   %eax,%eax
  8011a2:	78 4a                	js     8011ee <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8011a4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011a7:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8011ab:	75 21                	jne    8011ce <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  8011ad:	a1 04 40 80 00       	mov    0x804004,%eax
  8011b2:	8b 40 48             	mov    0x48(%eax),%eax
  8011b5:	83 ec 04             	sub    $0x4,%esp
  8011b8:	53                   	push   %ebx
  8011b9:	50                   	push   %eax
  8011ba:	68 89 28 80 00       	push   $0x802889
  8011bf:	e8 a9 f0 ff ff       	call   80026d <cprintf>
		return -E_INVAL;
  8011c4:	83 c4 10             	add    $0x10,%esp
  8011c7:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011cc:	eb 20                	jmp    8011ee <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8011ce:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8011d1:	8b 52 0c             	mov    0xc(%edx),%edx
  8011d4:	85 d2                	test   %edx,%edx
  8011d6:	74 11                	je     8011e9 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8011d8:	83 ec 04             	sub    $0x4,%esp
  8011db:	ff 75 10             	pushl  0x10(%ebp)
  8011de:	ff 75 0c             	pushl  0xc(%ebp)
  8011e1:	50                   	push   %eax
  8011e2:	ff d2                	call   *%edx
  8011e4:	83 c4 10             	add    $0x10,%esp
  8011e7:	eb 05                	jmp    8011ee <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8011e9:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8011ee:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8011f1:	c9                   	leave  
  8011f2:	c3                   	ret    

008011f3 <seek>:

int
seek(int fdnum, off_t offset)
{
  8011f3:	55                   	push   %ebp
  8011f4:	89 e5                	mov    %esp,%ebp
  8011f6:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8011f9:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8011fc:	50                   	push   %eax
  8011fd:	ff 75 08             	pushl  0x8(%ebp)
  801200:	e8 36 fc ff ff       	call   800e3b <fd_lookup>
  801205:	83 c4 08             	add    $0x8,%esp
  801208:	85 c0                	test   %eax,%eax
  80120a:	78 0e                	js     80121a <seek+0x27>
		return r;
	fd->fd_offset = offset;
  80120c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  80120f:	8b 55 0c             	mov    0xc(%ebp),%edx
  801212:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801215:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80121a:	c9                   	leave  
  80121b:	c3                   	ret    

0080121c <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  80121c:	55                   	push   %ebp
  80121d:	89 e5                	mov    %esp,%ebp
  80121f:	53                   	push   %ebx
  801220:	83 ec 14             	sub    $0x14,%esp
  801223:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801226:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801229:	50                   	push   %eax
  80122a:	53                   	push   %ebx
  80122b:	e8 0b fc ff ff       	call   800e3b <fd_lookup>
  801230:	83 c4 08             	add    $0x8,%esp
  801233:	85 c0                	test   %eax,%eax
  801235:	78 5f                	js     801296 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801237:	83 ec 08             	sub    $0x8,%esp
  80123a:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80123d:	50                   	push   %eax
  80123e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801241:	ff 30                	pushl  (%eax)
  801243:	e8 4a fc ff ff       	call   800e92 <dev_lookup>
  801248:	83 c4 10             	add    $0x10,%esp
  80124b:	85 c0                	test   %eax,%eax
  80124d:	78 47                	js     801296 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80124f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801252:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801256:	75 21                	jne    801279 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801258:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80125d:	8b 40 48             	mov    0x48(%eax),%eax
  801260:	83 ec 04             	sub    $0x4,%esp
  801263:	53                   	push   %ebx
  801264:	50                   	push   %eax
  801265:	68 4c 28 80 00       	push   $0x80284c
  80126a:	e8 fe ef ff ff       	call   80026d <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  80126f:	83 c4 10             	add    $0x10,%esp
  801272:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801277:	eb 1d                	jmp    801296 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  801279:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80127c:	8b 52 18             	mov    0x18(%edx),%edx
  80127f:	85 d2                	test   %edx,%edx
  801281:	74 0e                	je     801291 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  801283:	83 ec 08             	sub    $0x8,%esp
  801286:	ff 75 0c             	pushl  0xc(%ebp)
  801289:	50                   	push   %eax
  80128a:	ff d2                	call   *%edx
  80128c:	83 c4 10             	add    $0x10,%esp
  80128f:	eb 05                	jmp    801296 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  801291:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  801296:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801299:	c9                   	leave  
  80129a:	c3                   	ret    

0080129b <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  80129b:	55                   	push   %ebp
  80129c:	89 e5                	mov    %esp,%ebp
  80129e:	53                   	push   %ebx
  80129f:	83 ec 14             	sub    $0x14,%esp
  8012a2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8012a5:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8012a8:	50                   	push   %eax
  8012a9:	ff 75 08             	pushl  0x8(%ebp)
  8012ac:	e8 8a fb ff ff       	call   800e3b <fd_lookup>
  8012b1:	83 c4 08             	add    $0x8,%esp
  8012b4:	85 c0                	test   %eax,%eax
  8012b6:	78 52                	js     80130a <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8012b8:	83 ec 08             	sub    $0x8,%esp
  8012bb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012be:	50                   	push   %eax
  8012bf:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8012c2:	ff 30                	pushl  (%eax)
  8012c4:	e8 c9 fb ff ff       	call   800e92 <dev_lookup>
  8012c9:	83 c4 10             	add    $0x10,%esp
  8012cc:	85 c0                	test   %eax,%eax
  8012ce:	78 3a                	js     80130a <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8012d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8012d3:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8012d7:	74 2c                	je     801305 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8012d9:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8012dc:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8012e3:	00 00 00 
	stat->st_isdir = 0;
  8012e6:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8012ed:	00 00 00 
	stat->st_dev = dev;
  8012f0:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8012f6:	83 ec 08             	sub    $0x8,%esp
  8012f9:	53                   	push   %ebx
  8012fa:	ff 75 f0             	pushl  -0x10(%ebp)
  8012fd:	ff 50 14             	call   *0x14(%eax)
  801300:	83 c4 10             	add    $0x10,%esp
  801303:	eb 05                	jmp    80130a <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801305:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  80130a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80130d:	c9                   	leave  
  80130e:	c3                   	ret    

0080130f <stat>:

int
stat(const char *path, struct Stat *stat)
{
  80130f:	55                   	push   %ebp
  801310:	89 e5                	mov    %esp,%ebp
  801312:	56                   	push   %esi
  801313:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801314:	83 ec 08             	sub    $0x8,%esp
  801317:	6a 00                	push   $0x0
  801319:	ff 75 08             	pushl  0x8(%ebp)
  80131c:	e8 e7 01 00 00       	call   801508 <open>
  801321:	89 c3                	mov    %eax,%ebx
  801323:	83 c4 10             	add    $0x10,%esp
  801326:	85 c0                	test   %eax,%eax
  801328:	78 1d                	js     801347 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  80132a:	83 ec 08             	sub    $0x8,%esp
  80132d:	ff 75 0c             	pushl  0xc(%ebp)
  801330:	50                   	push   %eax
  801331:	e8 65 ff ff ff       	call   80129b <fstat>
  801336:	89 c6                	mov    %eax,%esi
	close(fd);
  801338:	89 1c 24             	mov    %ebx,(%esp)
  80133b:	e8 29 fc ff ff       	call   800f69 <close>
	return r;
  801340:	83 c4 10             	add    $0x10,%esp
  801343:	89 f0                	mov    %esi,%eax
  801345:	eb 00                	jmp    801347 <stat+0x38>
}
  801347:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80134a:	5b                   	pop    %ebx
  80134b:	5e                   	pop    %esi
  80134c:	5d                   	pop    %ebp
  80134d:	c3                   	ret    

0080134e <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  80134e:	55                   	push   %ebp
  80134f:	89 e5                	mov    %esp,%ebp
  801351:	56                   	push   %esi
  801352:	53                   	push   %ebx
  801353:	89 c6                	mov    %eax,%esi
  801355:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801357:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  80135e:	75 12                	jne    801372 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801360:	83 ec 0c             	sub    $0xc,%esp
  801363:	6a 01                	push   $0x1
  801365:	e8 9f 0d 00 00       	call   802109 <ipc_find_env>
  80136a:	a3 00 40 80 00       	mov    %eax,0x804000
  80136f:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  801372:	6a 07                	push   $0x7
  801374:	68 00 50 80 00       	push   $0x805000
  801379:	56                   	push   %esi
  80137a:	ff 35 00 40 80 00    	pushl  0x804000
  801380:	e8 2f 0d 00 00       	call   8020b4 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  801385:	83 c4 0c             	add    $0xc,%esp
  801388:	6a 00                	push   $0x0
  80138a:	53                   	push   %ebx
  80138b:	6a 00                	push   $0x0
  80138d:	e8 ba 0c 00 00       	call   80204c <ipc_recv>
}
  801392:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801395:	5b                   	pop    %ebx
  801396:	5e                   	pop    %esi
  801397:	5d                   	pop    %ebp
  801398:	c3                   	ret    

00801399 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801399:	55                   	push   %ebp
  80139a:	89 e5                	mov    %esp,%ebp
  80139c:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  80139f:	8b 45 08             	mov    0x8(%ebp),%eax
  8013a2:	8b 40 0c             	mov    0xc(%eax),%eax
  8013a5:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  8013aa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8013ad:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  8013b2:	ba 00 00 00 00       	mov    $0x0,%edx
  8013b7:	b8 02 00 00 00       	mov    $0x2,%eax
  8013bc:	e8 8d ff ff ff       	call   80134e <fsipc>
}
  8013c1:	c9                   	leave  
  8013c2:	c3                   	ret    

008013c3 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8013c3:	55                   	push   %ebp
  8013c4:	89 e5                	mov    %esp,%ebp
  8013c6:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8013c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8013cc:	8b 40 0c             	mov    0xc(%eax),%eax
  8013cf:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  8013d4:	ba 00 00 00 00       	mov    $0x0,%edx
  8013d9:	b8 06 00 00 00       	mov    $0x6,%eax
  8013de:	e8 6b ff ff ff       	call   80134e <fsipc>
}
  8013e3:	c9                   	leave  
  8013e4:	c3                   	ret    

008013e5 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  8013e5:	55                   	push   %ebp
  8013e6:	89 e5                	mov    %esp,%ebp
  8013e8:	53                   	push   %ebx
  8013e9:	83 ec 04             	sub    $0x4,%esp
  8013ec:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8013ef:	8b 45 08             	mov    0x8(%ebp),%eax
  8013f2:	8b 40 0c             	mov    0xc(%eax),%eax
  8013f5:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8013fa:	ba 00 00 00 00       	mov    $0x0,%edx
  8013ff:	b8 05 00 00 00       	mov    $0x5,%eax
  801404:	e8 45 ff ff ff       	call   80134e <fsipc>
  801409:	85 c0                	test   %eax,%eax
  80140b:	78 2c                	js     801439 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  80140d:	83 ec 08             	sub    $0x8,%esp
  801410:	68 00 50 80 00       	push   $0x805000
  801415:	53                   	push   %ebx
  801416:	e8 b6 f3 ff ff       	call   8007d1 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  80141b:	a1 80 50 80 00       	mov    0x805080,%eax
  801420:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801426:	a1 84 50 80 00       	mov    0x805084,%eax
  80142b:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801431:	83 c4 10             	add    $0x10,%esp
  801434:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801439:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80143c:	c9                   	leave  
  80143d:	c3                   	ret    

0080143e <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  80143e:	55                   	push   %ebp
  80143f:	89 e5                	mov    %esp,%ebp
  801441:	83 ec 08             	sub    $0x8,%esp
  801444:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801447:	8b 55 08             	mov    0x8(%ebp),%edx
  80144a:	8b 52 0c             	mov    0xc(%edx),%edx
  80144d:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801453:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801458:	76 05                	jbe    80145f <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  80145a:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  80145f:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801464:	83 ec 04             	sub    $0x4,%esp
  801467:	50                   	push   %eax
  801468:	ff 75 0c             	pushl  0xc(%ebp)
  80146b:	68 08 50 80 00       	push   $0x805008
  801470:	e8 d1 f4 ff ff       	call   800946 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801475:	ba 00 00 00 00       	mov    $0x0,%edx
  80147a:	b8 04 00 00 00       	mov    $0x4,%eax
  80147f:	e8 ca fe ff ff       	call   80134e <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801484:	c9                   	leave  
  801485:	c3                   	ret    

00801486 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801486:	55                   	push   %ebp
  801487:	89 e5                	mov    %esp,%ebp
  801489:	56                   	push   %esi
  80148a:	53                   	push   %ebx
  80148b:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  80148e:	8b 45 08             	mov    0x8(%ebp),%eax
  801491:	8b 40 0c             	mov    0xc(%eax),%eax
  801494:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801499:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  80149f:	ba 00 00 00 00       	mov    $0x0,%edx
  8014a4:	b8 03 00 00 00       	mov    $0x3,%eax
  8014a9:	e8 a0 fe ff ff       	call   80134e <fsipc>
  8014ae:	89 c3                	mov    %eax,%ebx
  8014b0:	85 c0                	test   %eax,%eax
  8014b2:	78 4b                	js     8014ff <devfile_read+0x79>
		return r;
	assert(r <= n);
  8014b4:	39 c6                	cmp    %eax,%esi
  8014b6:	73 16                	jae    8014ce <devfile_read+0x48>
  8014b8:	68 b8 28 80 00       	push   $0x8028b8
  8014bd:	68 bf 28 80 00       	push   $0x8028bf
  8014c2:	6a 7c                	push   $0x7c
  8014c4:	68 d4 28 80 00       	push   $0x8028d4
  8014c9:	e8 c7 ec ff ff       	call   800195 <_panic>
	assert(r <= PGSIZE);
  8014ce:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8014d3:	7e 16                	jle    8014eb <devfile_read+0x65>
  8014d5:	68 df 28 80 00       	push   $0x8028df
  8014da:	68 bf 28 80 00       	push   $0x8028bf
  8014df:	6a 7d                	push   $0x7d
  8014e1:	68 d4 28 80 00       	push   $0x8028d4
  8014e6:	e8 aa ec ff ff       	call   800195 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  8014eb:	83 ec 04             	sub    $0x4,%esp
  8014ee:	50                   	push   %eax
  8014ef:	68 00 50 80 00       	push   $0x805000
  8014f4:	ff 75 0c             	pushl  0xc(%ebp)
  8014f7:	e8 4a f4 ff ff       	call   800946 <memmove>
	return r;
  8014fc:	83 c4 10             	add    $0x10,%esp
}
  8014ff:	89 d8                	mov    %ebx,%eax
  801501:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801504:	5b                   	pop    %ebx
  801505:	5e                   	pop    %esi
  801506:	5d                   	pop    %ebp
  801507:	c3                   	ret    

00801508 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801508:	55                   	push   %ebp
  801509:	89 e5                	mov    %esp,%ebp
  80150b:	53                   	push   %ebx
  80150c:	83 ec 20             	sub    $0x20,%esp
  80150f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801512:	53                   	push   %ebx
  801513:	e8 84 f2 ff ff       	call   80079c <strlen>
  801518:	83 c4 10             	add    $0x10,%esp
  80151b:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801520:	7f 63                	jg     801585 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801522:	83 ec 0c             	sub    $0xc,%esp
  801525:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801528:	50                   	push   %eax
  801529:	e8 be f8 ff ff       	call   800dec <fd_alloc>
  80152e:	83 c4 10             	add    $0x10,%esp
  801531:	85 c0                	test   %eax,%eax
  801533:	78 55                	js     80158a <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801535:	83 ec 08             	sub    $0x8,%esp
  801538:	53                   	push   %ebx
  801539:	68 00 50 80 00       	push   $0x805000
  80153e:	e8 8e f2 ff ff       	call   8007d1 <strcpy>
	fsipcbuf.open.req_omode = mode;
  801543:	8b 45 0c             	mov    0xc(%ebp),%eax
  801546:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  80154b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80154e:	b8 01 00 00 00       	mov    $0x1,%eax
  801553:	e8 f6 fd ff ff       	call   80134e <fsipc>
  801558:	89 c3                	mov    %eax,%ebx
  80155a:	83 c4 10             	add    $0x10,%esp
  80155d:	85 c0                	test   %eax,%eax
  80155f:	79 14                	jns    801575 <open+0x6d>
		fd_close(fd, 0);
  801561:	83 ec 08             	sub    $0x8,%esp
  801564:	6a 00                	push   $0x0
  801566:	ff 75 f4             	pushl  -0xc(%ebp)
  801569:	e8 77 f9 ff ff       	call   800ee5 <fd_close>
		return r;
  80156e:	83 c4 10             	add    $0x10,%esp
  801571:	89 d8                	mov    %ebx,%eax
  801573:	eb 15                	jmp    80158a <open+0x82>
	}

	return fd2num(fd);
  801575:	83 ec 0c             	sub    $0xc,%esp
  801578:	ff 75 f4             	pushl  -0xc(%ebp)
  80157b:	e8 45 f8 ff ff       	call   800dc5 <fd2num>
  801580:	83 c4 10             	add    $0x10,%esp
  801583:	eb 05                	jmp    80158a <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801585:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  80158a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80158d:	c9                   	leave  
  80158e:	c3                   	ret    

0080158f <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  80158f:	55                   	push   %ebp
  801590:	89 e5                	mov    %esp,%ebp
  801592:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801595:	ba 00 00 00 00       	mov    $0x0,%edx
  80159a:	b8 08 00 00 00       	mov    $0x8,%eax
  80159f:	e8 aa fd ff ff       	call   80134e <fsipc>
}
  8015a4:	c9                   	leave  
  8015a5:	c3                   	ret    

008015a6 <spawn>:
// argv: pointer to null-terminated array of pointers to strings,
// 	 which will be passed to the child as its command-line arguments.
// Returns child envid on success, < 0 on failure.
int
spawn(const char *prog, const char **argv)
{
  8015a6:	55                   	push   %ebp
  8015a7:	89 e5                	mov    %esp,%ebp
  8015a9:	57                   	push   %edi
  8015aa:	56                   	push   %esi
  8015ab:	53                   	push   %ebx
  8015ac:	81 ec 94 02 00 00    	sub    $0x294,%esp
  8015b2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//   - Call sys_env_set_trapframe(child, &child_tf) to set up the
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
  8015b5:	6a 00                	push   $0x0
  8015b7:	ff 75 08             	pushl  0x8(%ebp)
  8015ba:	e8 49 ff ff ff       	call   801508 <open>
  8015bf:	89 c1                	mov    %eax,%ecx
  8015c1:	89 85 8c fd ff ff    	mov    %eax,-0x274(%ebp)
  8015c7:	83 c4 10             	add    $0x10,%esp
  8015ca:	85 c0                	test   %eax,%eax
  8015cc:	0f 88 e2 04 00 00    	js     801ab4 <spawn+0x50e>
		return r;
	fd = r;

	// Read elf header
	elf = (struct Elf*) elf_buf;
	if (readn(fd, elf_buf, sizeof(elf_buf)) != sizeof(elf_buf)
  8015d2:	83 ec 04             	sub    $0x4,%esp
  8015d5:	68 00 02 00 00       	push   $0x200
  8015da:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  8015e0:	50                   	push   %eax
  8015e1:	51                   	push   %ecx
  8015e2:	e8 41 fb ff ff       	call   801128 <readn>
  8015e7:	83 c4 10             	add    $0x10,%esp
  8015ea:	3d 00 02 00 00       	cmp    $0x200,%eax
  8015ef:	75 0c                	jne    8015fd <spawn+0x57>
	    || elf->e_magic != ELF_MAGIC) {
  8015f1:	81 bd e8 fd ff ff 7f 	cmpl   $0x464c457f,-0x218(%ebp)
  8015f8:	45 4c 46 
  8015fb:	74 33                	je     801630 <spawn+0x8a>
		close(fd);
  8015fd:	83 ec 0c             	sub    $0xc,%esp
  801600:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801606:	e8 5e f9 ff ff       	call   800f69 <close>
		cprintf("elf magic %08x want %08x\n", elf->e_magic, ELF_MAGIC);
  80160b:	83 c4 0c             	add    $0xc,%esp
  80160e:	68 7f 45 4c 46       	push   $0x464c457f
  801613:	ff b5 e8 fd ff ff    	pushl  -0x218(%ebp)
  801619:	68 eb 28 80 00       	push   $0x8028eb
  80161e:	e8 4a ec ff ff       	call   80026d <cprintf>
		return -E_NOT_EXEC;
  801623:	83 c4 10             	add    $0x10,%esp
  801626:	bb f2 ff ff ff       	mov    $0xfffffff2,%ebx
  80162b:	e9 e4 04 00 00       	jmp    801b14 <spawn+0x56e>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  801630:	b8 07 00 00 00       	mov    $0x7,%eax
  801635:	cd 30                	int    $0x30
  801637:	89 85 74 fd ff ff    	mov    %eax,-0x28c(%ebp)
  80163d:	89 85 84 fd ff ff    	mov    %eax,-0x27c(%ebp)
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
  801643:	85 c0                	test   %eax,%eax
  801645:	0f 88 71 04 00 00    	js     801abc <spawn+0x516>
		return r;
	child = r;

	// Set up trap frame, including initial stack.
	child_tf = envs[ENVX(child)].env_tf;
  80164b:	25 ff 03 00 00       	and    $0x3ff,%eax
  801650:	89 c6                	mov    %eax,%esi
  801652:	8d 04 85 00 00 00 00 	lea    0x0(,%eax,4),%eax
  801659:	c1 e6 07             	shl    $0x7,%esi
  80165c:	29 c6                	sub    %eax,%esi
  80165e:	81 c6 00 00 c0 ee    	add    $0xeec00000,%esi
  801664:	8d bd a4 fd ff ff    	lea    -0x25c(%ebp),%edi
  80166a:	b9 11 00 00 00       	mov    $0x11,%ecx
  80166f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	child_tf.tf_eip = elf->e_entry;
  801671:	8b 85 00 fe ff ff    	mov    -0x200(%ebp),%eax
  801677:	89 85 d4 fd ff ff    	mov    %eax,-0x22c(%ebp)
  80167d:	ba 00 00 00 00       	mov    $0x0,%edx
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  801682:	b8 00 00 00 00       	mov    $0x0,%eax
	char *string_store;
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
  801687:	bf 00 00 00 00       	mov    $0x0,%edi
  80168c:	89 5d 0c             	mov    %ebx,0xc(%ebp)
  80168f:	89 c3                	mov    %eax,%ebx
  801691:	eb 13                	jmp    8016a6 <spawn+0x100>
	for (argc = 0; argv[argc] != 0; argc++)
		string_size += strlen(argv[argc]) + 1;
  801693:	83 ec 0c             	sub    $0xc,%esp
  801696:	50                   	push   %eax
  801697:	e8 00 f1 ff ff       	call   80079c <strlen>
  80169c:	8d 7c 38 01          	lea    0x1(%eax,%edi,1),%edi
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  8016a0:	43                   	inc    %ebx
  8016a1:	89 f2                	mov    %esi,%edx
  8016a3:	83 c4 10             	add    $0x10,%esp
  8016a6:	89 d9                	mov    %ebx,%ecx
  8016a8:	8d 72 04             	lea    0x4(%edx),%esi
  8016ab:	8b 45 0c             	mov    0xc(%ebp),%eax
  8016ae:	8b 44 30 fc          	mov    -0x4(%eax,%esi,1),%eax
  8016b2:	85 c0                	test   %eax,%eax
  8016b4:	75 dd                	jne    801693 <spawn+0xed>
  8016b6:	89 9d 90 fd ff ff    	mov    %ebx,-0x270(%ebp)
  8016bc:	89 9d 88 fd ff ff    	mov    %ebx,-0x278(%ebp)
  8016c2:	89 95 80 fd ff ff    	mov    %edx,-0x280(%ebp)
  8016c8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Determine where to place the strings and the argv array.
	// Set up pointers into the temporary page 'UTEMP'; we'll map a page
	// there later, then remap that page into the child environment
	// at (USTACKTOP - PGSIZE).
	// strings is the topmost thing on the stack.
	string_store = (char*) UTEMP + PGSIZE - string_size;
  8016cb:	b8 00 10 40 00       	mov    $0x401000,%eax
  8016d0:	29 f8                	sub    %edi,%eax
  8016d2:	89 c7                	mov    %eax,%edi
	// argv is below that.  There's one argument pointer per argument, plus
	// a null pointer.
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));
  8016d4:	89 c2                	mov    %eax,%edx
  8016d6:	83 e2 fc             	and    $0xfffffffc,%edx
  8016d9:	8d 04 8d 04 00 00 00 	lea    0x4(,%ecx,4),%eax
  8016e0:	29 c2                	sub    %eax,%edx
  8016e2:	89 95 94 fd ff ff    	mov    %edx,-0x26c(%ebp)

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
  8016e8:	8d 42 f8             	lea    -0x8(%edx),%eax
  8016eb:	3d ff ff 3f 00       	cmp    $0x3fffff,%eax
  8016f0:	0f 86 d6 03 00 00    	jbe    801acc <spawn+0x526>
		return -E_NO_MEM;

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  8016f6:	83 ec 04             	sub    $0x4,%esp
  8016f9:	6a 07                	push   $0x7
  8016fb:	68 00 00 40 00       	push   $0x400000
  801700:	6a 00                	push   $0x0
  801702:	e8 ae f4 ff ff       	call   800bb5 <sys_page_alloc>
  801707:	83 c4 10             	add    $0x10,%esp
  80170a:	85 c0                	test   %eax,%eax
  80170c:	0f 88 c1 03 00 00    	js     801ad3 <spawn+0x52d>
  801712:	be 00 00 00 00       	mov    $0x0,%esi
  801717:	eb 2e                	jmp    801747 <spawn+0x1a1>
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
		argv_store[i] = UTEMP2USTACK(string_store);
  801719:	8d 87 00 d0 7f ee    	lea    -0x11803000(%edi),%eax
  80171f:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  801725:	89 04 b1             	mov    %eax,(%ecx,%esi,4)
		strcpy(string_store, argv[i]);
  801728:	83 ec 08             	sub    $0x8,%esp
  80172b:	ff 34 b3             	pushl  (%ebx,%esi,4)
  80172e:	57                   	push   %edi
  80172f:	e8 9d f0 ff ff       	call   8007d1 <strcpy>
		string_store += strlen(argv[i]) + 1;
  801734:	83 c4 04             	add    $0x4,%esp
  801737:	ff 34 b3             	pushl  (%ebx,%esi,4)
  80173a:	e8 5d f0 ff ff       	call   80079c <strlen>
  80173f:	8d 7c 07 01          	lea    0x1(%edi,%eax,1),%edi
	//	  (Again, argv should use an address valid in the child's
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
  801743:	46                   	inc    %esi
  801744:	83 c4 10             	add    $0x10,%esp
  801747:	39 b5 90 fd ff ff    	cmp    %esi,-0x270(%ebp)
  80174d:	7f ca                	jg     801719 <spawn+0x173>
		argv_store[i] = UTEMP2USTACK(string_store);
		strcpy(string_store, argv[i]);
		string_store += strlen(argv[i]) + 1;
	}
	argv_store[argc] = 0;
  80174f:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801755:	8b 8d 80 fd ff ff    	mov    -0x280(%ebp),%ecx
  80175b:	c7 04 08 00 00 00 00 	movl   $0x0,(%eax,%ecx,1)
	assert(string_store == (char*)UTEMP + PGSIZE);
  801762:	81 ff 00 10 40 00    	cmp    $0x401000,%edi
  801768:	74 19                	je     801783 <spawn+0x1dd>
  80176a:	68 78 29 80 00       	push   $0x802978
  80176f:	68 bf 28 80 00       	push   $0x8028bf
  801774:	68 f1 00 00 00       	push   $0xf1
  801779:	68 05 29 80 00       	push   $0x802905
  80177e:	e8 12 ea ff ff       	call   800195 <_panic>

	argv_store[-1] = UTEMP2USTACK(argv_store);
  801783:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  801789:	89 c8                	mov    %ecx,%eax
  80178b:	2d 00 30 80 11       	sub    $0x11803000,%eax
  801790:	89 41 fc             	mov    %eax,-0x4(%ecx)
	argv_store[-2] = argc;
  801793:	89 c8                	mov    %ecx,%eax
  801795:	8b 8d 88 fd ff ff    	mov    -0x278(%ebp),%ecx
  80179b:	89 48 f8             	mov    %ecx,-0x8(%eax)

	*init_esp = UTEMP2USTACK(&argv_store[-2]);
  80179e:	2d 08 30 80 11       	sub    $0x11803008,%eax
  8017a3:	89 85 e0 fd ff ff    	mov    %eax,-0x220(%ebp)

	// After completing the stack, map it into the child's address space
	// and unmap it from ours!
	if ((r = sys_page_map(0, UTEMP, child, (void*) (USTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W)) < 0)
  8017a9:	83 ec 0c             	sub    $0xc,%esp
  8017ac:	6a 07                	push   $0x7
  8017ae:	68 00 d0 bf ee       	push   $0xeebfd000
  8017b3:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8017b9:	68 00 00 40 00       	push   $0x400000
  8017be:	6a 00                	push   $0x0
  8017c0:	e8 33 f4 ff ff       	call   800bf8 <sys_page_map>
  8017c5:	89 c3                	mov    %eax,%ebx
  8017c7:	83 c4 20             	add    $0x20,%esp
  8017ca:	85 c0                	test   %eax,%eax
  8017cc:	0f 88 30 03 00 00    	js     801b02 <spawn+0x55c>
		goto error;
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  8017d2:	83 ec 08             	sub    $0x8,%esp
  8017d5:	68 00 00 40 00       	push   $0x400000
  8017da:	6a 00                	push   $0x0
  8017dc:	e8 59 f4 ff ff       	call   800c3a <sys_page_unmap>
  8017e1:	89 c3                	mov    %eax,%ebx
  8017e3:	83 c4 10             	add    $0x10,%esp
  8017e6:	85 c0                	test   %eax,%eax
  8017e8:	0f 88 14 03 00 00    	js     801b02 <spawn+0x55c>

	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
  8017ee:	8b 85 04 fe ff ff    	mov    -0x1fc(%ebp),%eax
  8017f4:	8d 84 05 e8 fd ff ff 	lea    -0x218(%ebp,%eax,1),%eax
  8017fb:	89 85 7c fd ff ff    	mov    %eax,-0x284(%ebp)
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  801801:	c7 85 78 fd ff ff 00 	movl   $0x0,-0x288(%ebp)
  801808:	00 00 00 
  80180b:	e9 88 01 00 00       	jmp    801998 <spawn+0x3f2>
		if (ph->p_type != ELF_PROG_LOAD)
  801810:	8b 85 7c fd ff ff    	mov    -0x284(%ebp),%eax
  801816:	83 38 01             	cmpl   $0x1,(%eax)
  801819:	0f 85 6c 01 00 00    	jne    80198b <spawn+0x3e5>
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
  80181f:	89 c1                	mov    %eax,%ecx
  801821:	8b 40 18             	mov    0x18(%eax),%eax
  801824:	83 e0 02             	and    $0x2,%eax
			perm |= PTE_W;
  801827:	83 f8 01             	cmp    $0x1,%eax
  80182a:	19 c0                	sbb    %eax,%eax
  80182c:	83 e0 fe             	and    $0xfffffffe,%eax
  80182f:	83 c0 07             	add    $0x7,%eax
  801832:	89 85 88 fd ff ff    	mov    %eax,-0x278(%ebp)
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
  801838:	89 c8                	mov    %ecx,%eax
  80183a:	8b 49 04             	mov    0x4(%ecx),%ecx
  80183d:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
  801843:	8b 78 10             	mov    0x10(%eax),%edi
  801846:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  80184c:	8b 70 14             	mov    0x14(%eax),%esi
  80184f:	89 f2                	mov    %esi,%edx
  801851:	89 b5 90 fd ff ff    	mov    %esi,-0x270(%ebp)
  801857:	8b 70 08             	mov    0x8(%eax),%esi
	int i, r;
	void *blk;

	//cprintf("map_segment %x+%x\n", va, memsz);

	if ((i = PGOFF(va))) {
  80185a:	89 f0                	mov    %esi,%eax
  80185c:	25 ff 0f 00 00       	and    $0xfff,%eax
  801861:	74 1a                	je     80187d <spawn+0x2d7>
		va -= i;
  801863:	29 c6                	sub    %eax,%esi
		memsz += i;
  801865:	01 c2                	add    %eax,%edx
  801867:	89 95 90 fd ff ff    	mov    %edx,-0x270(%ebp)
		filesz += i;
  80186d:	01 c7                	add    %eax,%edi
  80186f:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
		fileoffset -= i;
  801875:	29 c1                	sub    %eax,%ecx
  801877:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  80187d:	bb 00 00 00 00       	mov    $0x0,%ebx
  801882:	e9 f6 00 00 00       	jmp    80197d <spawn+0x3d7>
		if (i >= filesz) {
  801887:	39 9d 94 fd ff ff    	cmp    %ebx,-0x26c(%ebp)
  80188d:	77 27                	ja     8018b6 <spawn+0x310>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
  80188f:	83 ec 04             	sub    $0x4,%esp
  801892:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801898:	56                   	push   %esi
  801899:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  80189f:	e8 11 f3 ff ff       	call   800bb5 <sys_page_alloc>
  8018a4:	83 c4 10             	add    $0x10,%esp
  8018a7:	85 c0                	test   %eax,%eax
  8018a9:	0f 89 c2 00 00 00    	jns    801971 <spawn+0x3cb>
  8018af:	89 c3                	mov    %eax,%ebx
  8018b1:	e9 2b 02 00 00       	jmp    801ae1 <spawn+0x53b>
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  8018b6:	83 ec 04             	sub    $0x4,%esp
  8018b9:	6a 07                	push   $0x7
  8018bb:	68 00 00 40 00       	push   $0x400000
  8018c0:	6a 00                	push   $0x0
  8018c2:	e8 ee f2 ff ff       	call   800bb5 <sys_page_alloc>
  8018c7:	83 c4 10             	add    $0x10,%esp
  8018ca:	85 c0                	test   %eax,%eax
  8018cc:	0f 88 05 02 00 00    	js     801ad7 <spawn+0x531>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  8018d2:	83 ec 08             	sub    $0x8,%esp
  8018d5:	8b 85 80 fd ff ff    	mov    -0x280(%ebp),%eax
  8018db:	01 f8                	add    %edi,%eax
  8018dd:	50                   	push   %eax
  8018de:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8018e4:	e8 0a f9 ff ff       	call   8011f3 <seek>
  8018e9:	83 c4 10             	add    $0x10,%esp
  8018ec:	85 c0                	test   %eax,%eax
  8018ee:	0f 88 e7 01 00 00    	js     801adb <spawn+0x535>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  8018f4:	83 ec 04             	sub    $0x4,%esp
  8018f7:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  8018fd:	29 f8                	sub    %edi,%eax
  8018ff:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801904:	76 05                	jbe    80190b <spawn+0x365>
  801906:	b8 00 10 00 00       	mov    $0x1000,%eax
  80190b:	50                   	push   %eax
  80190c:	68 00 00 40 00       	push   $0x400000
  801911:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801917:	e8 0c f8 ff ff       	call   801128 <readn>
  80191c:	83 c4 10             	add    $0x10,%esp
  80191f:	85 c0                	test   %eax,%eax
  801921:	0f 88 b8 01 00 00    	js     801adf <spawn+0x539>
				return r;
			if ((r = sys_page_map(0, UTEMP, child, (void*) (va + i), perm)) < 0)
  801927:	83 ec 0c             	sub    $0xc,%esp
  80192a:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801930:	56                   	push   %esi
  801931:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801937:	68 00 00 40 00       	push   $0x400000
  80193c:	6a 00                	push   $0x0
  80193e:	e8 b5 f2 ff ff       	call   800bf8 <sys_page_map>
  801943:	83 c4 20             	add    $0x20,%esp
  801946:	85 c0                	test   %eax,%eax
  801948:	79 15                	jns    80195f <spawn+0x3b9>
				panic("spawn: sys_page_map data: %e", r);
  80194a:	50                   	push   %eax
  80194b:	68 11 29 80 00       	push   $0x802911
  801950:	68 24 01 00 00       	push   $0x124
  801955:	68 05 29 80 00       	push   $0x802905
  80195a:	e8 36 e8 ff ff       	call   800195 <_panic>
			sys_page_unmap(0, UTEMP);
  80195f:	83 ec 08             	sub    $0x8,%esp
  801962:	68 00 00 40 00       	push   $0x400000
  801967:	6a 00                	push   $0x0
  801969:	e8 cc f2 ff ff       	call   800c3a <sys_page_unmap>
  80196e:	83 c4 10             	add    $0x10,%esp
		memsz += i;
		filesz += i;
		fileoffset -= i;
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  801971:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  801977:	81 c6 00 10 00 00    	add    $0x1000,%esi
  80197d:	89 df                	mov    %ebx,%edi
  80197f:	39 9d 90 fd ff ff    	cmp    %ebx,-0x270(%ebp)
  801985:	0f 87 fc fe ff ff    	ja     801887 <spawn+0x2e1>
	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  80198b:	ff 85 78 fd ff ff    	incl   -0x288(%ebp)
  801991:	83 85 7c fd ff ff 20 	addl   $0x20,-0x284(%ebp)
  801998:	0f b7 85 14 fe ff ff 	movzwl -0x1ec(%ebp),%eax
  80199f:	39 85 78 fd ff ff    	cmp    %eax,-0x288(%ebp)
  8019a5:	0f 8c 65 fe ff ff    	jl     801810 <spawn+0x26a>
			perm |= PTE_W;
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
				     fd, ph->p_filesz, ph->p_offset, perm)) < 0)
			goto error;
	}
	close(fd);
  8019ab:	83 ec 0c             	sub    $0xc,%esp
  8019ae:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  8019b4:	e8 b0 f5 ff ff       	call   800f69 <close>
  8019b9:	83 c4 10             	add    $0x10,%esp
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  8019bc:	bf 00 00 00 00       	mov    $0x0,%edi
  8019c1:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  8019c7:	8b bd 84 fd ff ff    	mov    -0x27c(%ebp),%edi
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  8019cd:	8b b5 94 fd ff ff    	mov    -0x26c(%ebp),%esi
  8019d3:	8b 04 b5 00 d0 7b ef 	mov    -0x10843000(,%esi,4),%eax
  8019da:	a8 01                	test   $0x1,%al
  8019dc:	74 4b                	je     801a29 <spawn+0x483>
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
        	/* LAB 4: Your code here.
		    pte_t pte = uvpt[pn];
		    void *va = (void *)(pn << PGSHIFT);
		    int perm = pte & PTE_SYSCALL;*/
		    unsigned pn = (ipd << 10) | ipt;
  8019de:	c1 e6 0a             	shl    $0xa,%esi
  8019e1:	bb 00 00 00 00       	mov    $0x0,%ebx
  8019e6:	89 f0                	mov    %esi,%eax
  8019e8:	09 d8                	or     %ebx,%eax
        	if(!(uvpt[pn] & PTE_P)){ //doesn't exist
  8019ea:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  8019f1:	f6 c2 01             	test   $0x1,%dl
  8019f4:	74 2a                	je     801a20 <spawn+0x47a>
        		continue;
        	}
	        int perm = uvpt[pn] & PTE_SYSCALL;
  8019f6:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
	        if(perm & PTE_SHARE){
  8019fd:	f6 c6 04             	test   $0x4,%dh
  801a00:	74 1e                	je     801a20 <spawn+0x47a>
			    void *va = (void *)(pn << PGSHIFT);
  801a02:	c1 e0 0c             	shl    $0xc,%eax
			    r = sys_page_map(0, va, child, va, perm);
  801a05:	83 ec 0c             	sub    $0xc,%esp
  801a08:	81 e2 07 0e 00 00    	and    $0xe07,%edx
  801a0e:	52                   	push   %edx
  801a0f:	50                   	push   %eax
  801a10:	57                   	push   %edi
  801a11:	50                   	push   %eax
  801a12:	6a 00                	push   $0x0
  801a14:	e8 df f1 ff ff       	call   800bf8 <sys_page_map>
			    if (r){
  801a19:	83 c4 20             	add    $0x20,%esp
  801a1c:	85 c0                	test   %eax,%eax
  801a1e:	75 1e                	jne    801a3e <spawn+0x498>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801a20:	43                   	inc    %ebx
  801a21:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  801a27:	75 bd                	jne    8019e6 <spawn+0x440>
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801a29:	ff 85 94 fd ff ff    	incl   -0x26c(%ebp)
  801a2f:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801a35:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  801a3a:	75 91                	jne    8019cd <spawn+0x427>
  801a3c:	eb 19                	jmp    801a57 <spawn+0x4b1>
	}
	close(fd);
	fd = -1;

	// Copy shared library state.
	if ((r = copy_shared_pages(child)) < 0)
  801a3e:	85 c0                	test   %eax,%eax
  801a40:	79 15                	jns    801a57 <spawn+0x4b1>
		panic("copy_shared_pages: %e", r);
  801a42:	50                   	push   %eax
  801a43:	68 2e 29 80 00       	push   $0x80292e
  801a48:	68 82 00 00 00       	push   $0x82
  801a4d:	68 05 29 80 00       	push   $0x802905
  801a52:	e8 3e e7 ff ff       	call   800195 <_panic>

	if ((r = sys_env_set_trapframe(child, &child_tf)) < 0)
  801a57:	83 ec 08             	sub    $0x8,%esp
  801a5a:	8d 85 a4 fd ff ff    	lea    -0x25c(%ebp),%eax
  801a60:	50                   	push   %eax
  801a61:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801a67:	e8 71 f2 ff ff       	call   800cdd <sys_env_set_trapframe>
  801a6c:	83 c4 10             	add    $0x10,%esp
  801a6f:	85 c0                	test   %eax,%eax
  801a71:	79 15                	jns    801a88 <spawn+0x4e2>
		panic("sys_env_set_trapframe: %e", r);
  801a73:	50                   	push   %eax
  801a74:	68 44 29 80 00       	push   $0x802944
  801a79:	68 85 00 00 00       	push   $0x85
  801a7e:	68 05 29 80 00       	push   $0x802905
  801a83:	e8 0d e7 ff ff       	call   800195 <_panic>

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
  801a88:	83 ec 08             	sub    $0x8,%esp
  801a8b:	6a 02                	push   $0x2
  801a8d:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801a93:	e8 e4 f1 ff ff       	call   800c7c <sys_env_set_status>
  801a98:	83 c4 10             	add    $0x10,%esp
  801a9b:	85 c0                	test   %eax,%eax
  801a9d:	79 25                	jns    801ac4 <spawn+0x51e>
		panic("sys_env_set_status: %e", r);
  801a9f:	50                   	push   %eax
  801aa0:	68 5e 29 80 00       	push   $0x80295e
  801aa5:	68 88 00 00 00       	push   $0x88
  801aaa:	68 05 29 80 00       	push   $0x802905
  801aaf:	e8 e1 e6 ff ff       	call   800195 <_panic>
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
		return r;
  801ab4:	8b 9d 8c fd ff ff    	mov    -0x274(%ebp),%ebx
  801aba:	eb 58                	jmp    801b14 <spawn+0x56e>
		return -E_NOT_EXEC;
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
		return r;
  801abc:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801ac2:	eb 50                	jmp    801b14 <spawn+0x56e>
		panic("sys_env_set_trapframe: %e", r);

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
		panic("sys_env_set_status: %e", r);

	return child;
  801ac4:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801aca:	eb 48                	jmp    801b14 <spawn+0x56e>
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
		return -E_NO_MEM;
  801acc:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
  801ad1:	eb 41                	jmp    801b14 <spawn+0x56e>

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
		return r;
  801ad3:	89 c3                	mov    %eax,%ebx
  801ad5:	eb 3d                	jmp    801b14 <spawn+0x56e>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  801ad7:	89 c3                	mov    %eax,%ebx
  801ad9:	eb 06                	jmp    801ae1 <spawn+0x53b>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801adb:	89 c3                	mov    %eax,%ebx
  801add:	eb 02                	jmp    801ae1 <spawn+0x53b>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801adf:	89 c3                	mov    %eax,%ebx
		panic("sys_env_set_status: %e", r);

	return child;

error:
	sys_env_destroy(child);
  801ae1:	83 ec 0c             	sub    $0xc,%esp
  801ae4:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801aea:	e8 47 f0 ff ff       	call   800b36 <sys_env_destroy>
	close(fd);
  801aef:	83 c4 04             	add    $0x4,%esp
  801af2:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  801af8:	e8 6c f4 ff ff       	call   800f69 <close>
	return r;
  801afd:	83 c4 10             	add    $0x10,%esp
  801b00:	eb 12                	jmp    801b14 <spawn+0x56e>
		goto error;

	return 0;

error:
	sys_page_unmap(0, UTEMP);
  801b02:	83 ec 08             	sub    $0x8,%esp
  801b05:	68 00 00 40 00       	push   $0x400000
  801b0a:	6a 00                	push   $0x0
  801b0c:	e8 29 f1 ff ff       	call   800c3a <sys_page_unmap>
  801b11:	83 c4 10             	add    $0x10,%esp

error:
	sys_env_destroy(child);
	close(fd);
	return r;
}
  801b14:	89 d8                	mov    %ebx,%eax
  801b16:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b19:	5b                   	pop    %ebx
  801b1a:	5e                   	pop    %esi
  801b1b:	5f                   	pop    %edi
  801b1c:	5d                   	pop    %ebp
  801b1d:	c3                   	ret    

00801b1e <spawnl>:
// Spawn, taking command-line arguments array directly on the stack.
// NOTE: Must have a sentinal of NULL at the end of the args
// (none of the args may be NULL).
int
spawnl(const char *prog, const char *arg0, ...)
{
  801b1e:	55                   	push   %ebp
  801b1f:	89 e5                	mov    %esp,%ebp
  801b21:	56                   	push   %esi
  801b22:	53                   	push   %ebx
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801b23:	8d 55 10             	lea    0x10(%ebp),%edx
{
	// We calculate argc by advancing the args until we hit NULL.
	// The contract of the function guarantees that the last
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
  801b26:	b8 00 00 00 00       	mov    $0x0,%eax
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801b2b:	eb 01                	jmp    801b2e <spawnl+0x10>
		argc++;
  801b2d:	40                   	inc    %eax
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801b2e:	83 c2 04             	add    $0x4,%edx
  801b31:	83 7a fc 00          	cmpl   $0x0,-0x4(%edx)
  801b35:	75 f6                	jne    801b2d <spawnl+0xf>
		argc++;
	va_end(vl);

	// Now that we have the size of the args, do a second pass
	// and store the values in a VLA, which has the format of argv
	const char *argv[argc+2];
  801b37:	8d 14 85 1a 00 00 00 	lea    0x1a(,%eax,4),%edx
  801b3e:	83 e2 f0             	and    $0xfffffff0,%edx
  801b41:	29 d4                	sub    %edx,%esp
  801b43:	8d 54 24 03          	lea    0x3(%esp),%edx
  801b47:	c1 ea 02             	shr    $0x2,%edx
  801b4a:	8d 34 95 00 00 00 00 	lea    0x0(,%edx,4),%esi
  801b51:	89 f3                	mov    %esi,%ebx
	argv[0] = arg0;
  801b53:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801b56:	89 0c 95 00 00 00 00 	mov    %ecx,0x0(,%edx,4)
	argv[argc+1] = NULL;
  801b5d:	c7 44 86 04 00 00 00 	movl   $0x0,0x4(%esi,%eax,4)
  801b64:	00 
  801b65:	89 c2                	mov    %eax,%edx

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  801b67:	b8 00 00 00 00       	mov    $0x0,%eax
  801b6c:	eb 08                	jmp    801b76 <spawnl+0x58>
		argv[i+1] = va_arg(vl, const char *);
  801b6e:	40                   	inc    %eax
  801b6f:	8b 4c 85 0c          	mov    0xc(%ebp,%eax,4),%ecx
  801b73:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
	argv[0] = arg0;
	argv[argc+1] = NULL;

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  801b76:	39 d0                	cmp    %edx,%eax
  801b78:	75 f4                	jne    801b6e <spawnl+0x50>
		argv[i+1] = va_arg(vl, const char *);
	va_end(vl);
	return spawn(prog, argv);
  801b7a:	83 ec 08             	sub    $0x8,%esp
  801b7d:	56                   	push   %esi
  801b7e:	ff 75 08             	pushl  0x8(%ebp)
  801b81:	e8 20 fa ff ff       	call   8015a6 <spawn>
}
  801b86:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801b89:	5b                   	pop    %ebx
  801b8a:	5e                   	pop    %esi
  801b8b:	5d                   	pop    %ebp
  801b8c:	c3                   	ret    

00801b8d <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801b8d:	55                   	push   %ebp
  801b8e:	89 e5                	mov    %esp,%ebp
  801b90:	56                   	push   %esi
  801b91:	53                   	push   %ebx
  801b92:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801b95:	83 ec 0c             	sub    $0xc,%esp
  801b98:	ff 75 08             	pushl  0x8(%ebp)
  801b9b:	e8 35 f2 ff ff       	call   800dd5 <fd2data>
  801ba0:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801ba2:	83 c4 08             	add    $0x8,%esp
  801ba5:	68 a0 29 80 00       	push   $0x8029a0
  801baa:	53                   	push   %ebx
  801bab:	e8 21 ec ff ff       	call   8007d1 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801bb0:	8b 46 04             	mov    0x4(%esi),%eax
  801bb3:	2b 06                	sub    (%esi),%eax
  801bb5:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801bbb:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801bc2:	00 00 00 
	stat->st_dev = &devpipe;
  801bc5:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  801bcc:	30 80 00 
	return 0;
}
  801bcf:	b8 00 00 00 00       	mov    $0x0,%eax
  801bd4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801bd7:	5b                   	pop    %ebx
  801bd8:	5e                   	pop    %esi
  801bd9:	5d                   	pop    %ebp
  801bda:	c3                   	ret    

00801bdb <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801bdb:	55                   	push   %ebp
  801bdc:	89 e5                	mov    %esp,%ebp
  801bde:	53                   	push   %ebx
  801bdf:	83 ec 0c             	sub    $0xc,%esp
  801be2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801be5:	53                   	push   %ebx
  801be6:	6a 00                	push   $0x0
  801be8:	e8 4d f0 ff ff       	call   800c3a <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801bed:	89 1c 24             	mov    %ebx,(%esp)
  801bf0:	e8 e0 f1 ff ff       	call   800dd5 <fd2data>
  801bf5:	83 c4 08             	add    $0x8,%esp
  801bf8:	50                   	push   %eax
  801bf9:	6a 00                	push   $0x0
  801bfb:	e8 3a f0 ff ff       	call   800c3a <sys_page_unmap>
}
  801c00:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801c03:	c9                   	leave  
  801c04:	c3                   	ret    

00801c05 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801c05:	55                   	push   %ebp
  801c06:	89 e5                	mov    %esp,%ebp
  801c08:	57                   	push   %edi
  801c09:	56                   	push   %esi
  801c0a:	53                   	push   %ebx
  801c0b:	83 ec 1c             	sub    $0x1c,%esp
  801c0e:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801c11:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801c13:	a1 04 40 80 00       	mov    0x804004,%eax
  801c18:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801c1b:	83 ec 0c             	sub    $0xc,%esp
  801c1e:	ff 75 e0             	pushl  -0x20(%ebp)
  801c21:	e8 27 05 00 00       	call   80214d <pageref>
  801c26:	89 c3                	mov    %eax,%ebx
  801c28:	89 3c 24             	mov    %edi,(%esp)
  801c2b:	e8 1d 05 00 00       	call   80214d <pageref>
  801c30:	83 c4 10             	add    $0x10,%esp
  801c33:	39 c3                	cmp    %eax,%ebx
  801c35:	0f 94 c1             	sete   %cl
  801c38:	0f b6 c9             	movzbl %cl,%ecx
  801c3b:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801c3e:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801c44:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801c47:	39 ce                	cmp    %ecx,%esi
  801c49:	74 1b                	je     801c66 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801c4b:	39 c3                	cmp    %eax,%ebx
  801c4d:	75 c4                	jne    801c13 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801c4f:	8b 42 58             	mov    0x58(%edx),%eax
  801c52:	ff 75 e4             	pushl  -0x1c(%ebp)
  801c55:	50                   	push   %eax
  801c56:	56                   	push   %esi
  801c57:	68 a7 29 80 00       	push   $0x8029a7
  801c5c:	e8 0c e6 ff ff       	call   80026d <cprintf>
  801c61:	83 c4 10             	add    $0x10,%esp
  801c64:	eb ad                	jmp    801c13 <_pipeisclosed+0xe>
	}
}
  801c66:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801c69:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c6c:	5b                   	pop    %ebx
  801c6d:	5e                   	pop    %esi
  801c6e:	5f                   	pop    %edi
  801c6f:	5d                   	pop    %ebp
  801c70:	c3                   	ret    

00801c71 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801c71:	55                   	push   %ebp
  801c72:	89 e5                	mov    %esp,%ebp
  801c74:	57                   	push   %edi
  801c75:	56                   	push   %esi
  801c76:	53                   	push   %ebx
  801c77:	83 ec 18             	sub    $0x18,%esp
  801c7a:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801c7d:	56                   	push   %esi
  801c7e:	e8 52 f1 ff ff       	call   800dd5 <fd2data>
  801c83:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c85:	83 c4 10             	add    $0x10,%esp
  801c88:	bf 00 00 00 00       	mov    $0x0,%edi
  801c8d:	eb 3b                	jmp    801cca <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801c8f:	89 da                	mov    %ebx,%edx
  801c91:	89 f0                	mov    %esi,%eax
  801c93:	e8 6d ff ff ff       	call   801c05 <_pipeisclosed>
  801c98:	85 c0                	test   %eax,%eax
  801c9a:	75 38                	jne    801cd4 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801c9c:	e8 f5 ee ff ff       	call   800b96 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801ca1:	8b 53 04             	mov    0x4(%ebx),%edx
  801ca4:	8b 03                	mov    (%ebx),%eax
  801ca6:	83 c0 20             	add    $0x20,%eax
  801ca9:	39 c2                	cmp    %eax,%edx
  801cab:	73 e2                	jae    801c8f <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801cad:	8b 45 0c             	mov    0xc(%ebp),%eax
  801cb0:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801cb3:	89 d0                	mov    %edx,%eax
  801cb5:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801cba:	79 05                	jns    801cc1 <devpipe_write+0x50>
  801cbc:	48                   	dec    %eax
  801cbd:	83 c8 e0             	or     $0xffffffe0,%eax
  801cc0:	40                   	inc    %eax
  801cc1:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801cc5:	42                   	inc    %edx
  801cc6:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801cc9:	47                   	inc    %edi
  801cca:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801ccd:	75 d2                	jne    801ca1 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801ccf:	8b 45 10             	mov    0x10(%ebp),%eax
  801cd2:	eb 05                	jmp    801cd9 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801cd4:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801cd9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801cdc:	5b                   	pop    %ebx
  801cdd:	5e                   	pop    %esi
  801cde:	5f                   	pop    %edi
  801cdf:	5d                   	pop    %ebp
  801ce0:	c3                   	ret    

00801ce1 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801ce1:	55                   	push   %ebp
  801ce2:	89 e5                	mov    %esp,%ebp
  801ce4:	57                   	push   %edi
  801ce5:	56                   	push   %esi
  801ce6:	53                   	push   %ebx
  801ce7:	83 ec 18             	sub    $0x18,%esp
  801cea:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801ced:	57                   	push   %edi
  801cee:	e8 e2 f0 ff ff       	call   800dd5 <fd2data>
  801cf3:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801cf5:	83 c4 10             	add    $0x10,%esp
  801cf8:	bb 00 00 00 00       	mov    $0x0,%ebx
  801cfd:	eb 3a                	jmp    801d39 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801cff:	85 db                	test   %ebx,%ebx
  801d01:	74 04                	je     801d07 <devpipe_read+0x26>
				return i;
  801d03:	89 d8                	mov    %ebx,%eax
  801d05:	eb 41                	jmp    801d48 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801d07:	89 f2                	mov    %esi,%edx
  801d09:	89 f8                	mov    %edi,%eax
  801d0b:	e8 f5 fe ff ff       	call   801c05 <_pipeisclosed>
  801d10:	85 c0                	test   %eax,%eax
  801d12:	75 2f                	jne    801d43 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801d14:	e8 7d ee ff ff       	call   800b96 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801d19:	8b 06                	mov    (%esi),%eax
  801d1b:	3b 46 04             	cmp    0x4(%esi),%eax
  801d1e:	74 df                	je     801cff <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801d20:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801d25:	79 05                	jns    801d2c <devpipe_read+0x4b>
  801d27:	48                   	dec    %eax
  801d28:	83 c8 e0             	or     $0xffffffe0,%eax
  801d2b:	40                   	inc    %eax
  801d2c:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801d30:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801d33:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801d36:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801d38:	43                   	inc    %ebx
  801d39:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801d3c:	75 db                	jne    801d19 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801d3e:	8b 45 10             	mov    0x10(%ebp),%eax
  801d41:	eb 05                	jmp    801d48 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801d43:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801d48:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d4b:	5b                   	pop    %ebx
  801d4c:	5e                   	pop    %esi
  801d4d:	5f                   	pop    %edi
  801d4e:	5d                   	pop    %ebp
  801d4f:	c3                   	ret    

00801d50 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801d50:	55                   	push   %ebp
  801d51:	89 e5                	mov    %esp,%ebp
  801d53:	56                   	push   %esi
  801d54:	53                   	push   %ebx
  801d55:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801d58:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801d5b:	50                   	push   %eax
  801d5c:	e8 8b f0 ff ff       	call   800dec <fd_alloc>
  801d61:	83 c4 10             	add    $0x10,%esp
  801d64:	85 c0                	test   %eax,%eax
  801d66:	0f 88 2a 01 00 00    	js     801e96 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801d6c:	83 ec 04             	sub    $0x4,%esp
  801d6f:	68 07 04 00 00       	push   $0x407
  801d74:	ff 75 f4             	pushl  -0xc(%ebp)
  801d77:	6a 00                	push   $0x0
  801d79:	e8 37 ee ff ff       	call   800bb5 <sys_page_alloc>
  801d7e:	83 c4 10             	add    $0x10,%esp
  801d81:	85 c0                	test   %eax,%eax
  801d83:	0f 88 0d 01 00 00    	js     801e96 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801d89:	83 ec 0c             	sub    $0xc,%esp
  801d8c:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801d8f:	50                   	push   %eax
  801d90:	e8 57 f0 ff ff       	call   800dec <fd_alloc>
  801d95:	89 c3                	mov    %eax,%ebx
  801d97:	83 c4 10             	add    $0x10,%esp
  801d9a:	85 c0                	test   %eax,%eax
  801d9c:	0f 88 e2 00 00 00    	js     801e84 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801da2:	83 ec 04             	sub    $0x4,%esp
  801da5:	68 07 04 00 00       	push   $0x407
  801daa:	ff 75 f0             	pushl  -0x10(%ebp)
  801dad:	6a 00                	push   $0x0
  801daf:	e8 01 ee ff ff       	call   800bb5 <sys_page_alloc>
  801db4:	89 c3                	mov    %eax,%ebx
  801db6:	83 c4 10             	add    $0x10,%esp
  801db9:	85 c0                	test   %eax,%eax
  801dbb:	0f 88 c3 00 00 00    	js     801e84 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801dc1:	83 ec 0c             	sub    $0xc,%esp
  801dc4:	ff 75 f4             	pushl  -0xc(%ebp)
  801dc7:	e8 09 f0 ff ff       	call   800dd5 <fd2data>
  801dcc:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801dce:	83 c4 0c             	add    $0xc,%esp
  801dd1:	68 07 04 00 00       	push   $0x407
  801dd6:	50                   	push   %eax
  801dd7:	6a 00                	push   $0x0
  801dd9:	e8 d7 ed ff ff       	call   800bb5 <sys_page_alloc>
  801dde:	89 c3                	mov    %eax,%ebx
  801de0:	83 c4 10             	add    $0x10,%esp
  801de3:	85 c0                	test   %eax,%eax
  801de5:	0f 88 89 00 00 00    	js     801e74 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801deb:	83 ec 0c             	sub    $0xc,%esp
  801dee:	ff 75 f0             	pushl  -0x10(%ebp)
  801df1:	e8 df ef ff ff       	call   800dd5 <fd2data>
  801df6:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801dfd:	50                   	push   %eax
  801dfe:	6a 00                	push   $0x0
  801e00:	56                   	push   %esi
  801e01:	6a 00                	push   $0x0
  801e03:	e8 f0 ed ff ff       	call   800bf8 <sys_page_map>
  801e08:	89 c3                	mov    %eax,%ebx
  801e0a:	83 c4 20             	add    $0x20,%esp
  801e0d:	85 c0                	test   %eax,%eax
  801e0f:	78 55                	js     801e66 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801e11:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801e17:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e1a:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801e1c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e1f:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801e26:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801e2c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801e2f:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801e31:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801e34:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801e3b:	83 ec 0c             	sub    $0xc,%esp
  801e3e:	ff 75 f4             	pushl  -0xc(%ebp)
  801e41:	e8 7f ef ff ff       	call   800dc5 <fd2num>
  801e46:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801e49:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801e4b:	83 c4 04             	add    $0x4,%esp
  801e4e:	ff 75 f0             	pushl  -0x10(%ebp)
  801e51:	e8 6f ef ff ff       	call   800dc5 <fd2num>
  801e56:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801e59:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801e5c:	83 c4 10             	add    $0x10,%esp
  801e5f:	b8 00 00 00 00       	mov    $0x0,%eax
  801e64:	eb 30                	jmp    801e96 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801e66:	83 ec 08             	sub    $0x8,%esp
  801e69:	56                   	push   %esi
  801e6a:	6a 00                	push   $0x0
  801e6c:	e8 c9 ed ff ff       	call   800c3a <sys_page_unmap>
  801e71:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801e74:	83 ec 08             	sub    $0x8,%esp
  801e77:	ff 75 f0             	pushl  -0x10(%ebp)
  801e7a:	6a 00                	push   $0x0
  801e7c:	e8 b9 ed ff ff       	call   800c3a <sys_page_unmap>
  801e81:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801e84:	83 ec 08             	sub    $0x8,%esp
  801e87:	ff 75 f4             	pushl  -0xc(%ebp)
  801e8a:	6a 00                	push   $0x0
  801e8c:	e8 a9 ed ff ff       	call   800c3a <sys_page_unmap>
  801e91:	83 c4 10             	add    $0x10,%esp
  801e94:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801e96:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801e99:	5b                   	pop    %ebx
  801e9a:	5e                   	pop    %esi
  801e9b:	5d                   	pop    %ebp
  801e9c:	c3                   	ret    

00801e9d <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801e9d:	55                   	push   %ebp
  801e9e:	89 e5                	mov    %esp,%ebp
  801ea0:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801ea3:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ea6:	50                   	push   %eax
  801ea7:	ff 75 08             	pushl  0x8(%ebp)
  801eaa:	e8 8c ef ff ff       	call   800e3b <fd_lookup>
  801eaf:	83 c4 10             	add    $0x10,%esp
  801eb2:	85 c0                	test   %eax,%eax
  801eb4:	78 18                	js     801ece <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801eb6:	83 ec 0c             	sub    $0xc,%esp
  801eb9:	ff 75 f4             	pushl  -0xc(%ebp)
  801ebc:	e8 14 ef ff ff       	call   800dd5 <fd2data>
	return _pipeisclosed(fd, p);
  801ec1:	89 c2                	mov    %eax,%edx
  801ec3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ec6:	e8 3a fd ff ff       	call   801c05 <_pipeisclosed>
  801ecb:	83 c4 10             	add    $0x10,%esp
}
  801ece:	c9                   	leave  
  801ecf:	c3                   	ret    

00801ed0 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801ed0:	55                   	push   %ebp
  801ed1:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801ed3:	b8 00 00 00 00       	mov    $0x0,%eax
  801ed8:	5d                   	pop    %ebp
  801ed9:	c3                   	ret    

00801eda <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801eda:	55                   	push   %ebp
  801edb:	89 e5                	mov    %esp,%ebp
  801edd:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801ee0:	68 bf 29 80 00       	push   $0x8029bf
  801ee5:	ff 75 0c             	pushl  0xc(%ebp)
  801ee8:	e8 e4 e8 ff ff       	call   8007d1 <strcpy>
	return 0;
}
  801eed:	b8 00 00 00 00       	mov    $0x0,%eax
  801ef2:	c9                   	leave  
  801ef3:	c3                   	ret    

00801ef4 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801ef4:	55                   	push   %ebp
  801ef5:	89 e5                	mov    %esp,%ebp
  801ef7:	57                   	push   %edi
  801ef8:	56                   	push   %esi
  801ef9:	53                   	push   %ebx
  801efa:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801f00:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801f05:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801f0b:	eb 2c                	jmp    801f39 <devcons_write+0x45>
		m = n - tot;
  801f0d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801f10:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801f12:	83 fb 7f             	cmp    $0x7f,%ebx
  801f15:	76 05                	jbe    801f1c <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801f17:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801f1c:	83 ec 04             	sub    $0x4,%esp
  801f1f:	53                   	push   %ebx
  801f20:	03 45 0c             	add    0xc(%ebp),%eax
  801f23:	50                   	push   %eax
  801f24:	57                   	push   %edi
  801f25:	e8 1c ea ff ff       	call   800946 <memmove>
		sys_cputs(buf, m);
  801f2a:	83 c4 08             	add    $0x8,%esp
  801f2d:	53                   	push   %ebx
  801f2e:	57                   	push   %edi
  801f2f:	e8 c5 eb ff ff       	call   800af9 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801f34:	01 de                	add    %ebx,%esi
  801f36:	83 c4 10             	add    $0x10,%esp
  801f39:	89 f0                	mov    %esi,%eax
  801f3b:	3b 75 10             	cmp    0x10(%ebp),%esi
  801f3e:	72 cd                	jb     801f0d <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801f40:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f43:	5b                   	pop    %ebx
  801f44:	5e                   	pop    %esi
  801f45:	5f                   	pop    %edi
  801f46:	5d                   	pop    %ebp
  801f47:	c3                   	ret    

00801f48 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801f48:	55                   	push   %ebp
  801f49:	89 e5                	mov    %esp,%ebp
  801f4b:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801f4e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801f52:	75 07                	jne    801f5b <devcons_read+0x13>
  801f54:	eb 23                	jmp    801f79 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801f56:	e8 3b ec ff ff       	call   800b96 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801f5b:	e8 b7 eb ff ff       	call   800b17 <sys_cgetc>
  801f60:	85 c0                	test   %eax,%eax
  801f62:	74 f2                	je     801f56 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801f64:	85 c0                	test   %eax,%eax
  801f66:	78 1d                	js     801f85 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801f68:	83 f8 04             	cmp    $0x4,%eax
  801f6b:	74 13                	je     801f80 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801f6d:	8b 55 0c             	mov    0xc(%ebp),%edx
  801f70:	88 02                	mov    %al,(%edx)
	return 1;
  801f72:	b8 01 00 00 00       	mov    $0x1,%eax
  801f77:	eb 0c                	jmp    801f85 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801f79:	b8 00 00 00 00       	mov    $0x0,%eax
  801f7e:	eb 05                	jmp    801f85 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801f80:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801f85:	c9                   	leave  
  801f86:	c3                   	ret    

00801f87 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801f87:	55                   	push   %ebp
  801f88:	89 e5                	mov    %esp,%ebp
  801f8a:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801f8d:	8b 45 08             	mov    0x8(%ebp),%eax
  801f90:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801f93:	6a 01                	push   $0x1
  801f95:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801f98:	50                   	push   %eax
  801f99:	e8 5b eb ff ff       	call   800af9 <sys_cputs>
}
  801f9e:	83 c4 10             	add    $0x10,%esp
  801fa1:	c9                   	leave  
  801fa2:	c3                   	ret    

00801fa3 <getchar>:

int
getchar(void)
{
  801fa3:	55                   	push   %ebp
  801fa4:	89 e5                	mov    %esp,%ebp
  801fa6:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801fa9:	6a 01                	push   $0x1
  801fab:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801fae:	50                   	push   %eax
  801faf:	6a 00                	push   $0x0
  801fb1:	e8 eb f0 ff ff       	call   8010a1 <read>
	if (r < 0)
  801fb6:	83 c4 10             	add    $0x10,%esp
  801fb9:	85 c0                	test   %eax,%eax
  801fbb:	78 0f                	js     801fcc <getchar+0x29>
		return r;
	if (r < 1)
  801fbd:	85 c0                	test   %eax,%eax
  801fbf:	7e 06                	jle    801fc7 <getchar+0x24>
		return -E_EOF;
	return c;
  801fc1:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801fc5:	eb 05                	jmp    801fcc <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801fc7:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801fcc:	c9                   	leave  
  801fcd:	c3                   	ret    

00801fce <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801fce:	55                   	push   %ebp
  801fcf:	89 e5                	mov    %esp,%ebp
  801fd1:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801fd4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801fd7:	50                   	push   %eax
  801fd8:	ff 75 08             	pushl  0x8(%ebp)
  801fdb:	e8 5b ee ff ff       	call   800e3b <fd_lookup>
  801fe0:	83 c4 10             	add    $0x10,%esp
  801fe3:	85 c0                	test   %eax,%eax
  801fe5:	78 11                	js     801ff8 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801fe7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801fea:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801ff0:	39 10                	cmp    %edx,(%eax)
  801ff2:	0f 94 c0             	sete   %al
  801ff5:	0f b6 c0             	movzbl %al,%eax
}
  801ff8:	c9                   	leave  
  801ff9:	c3                   	ret    

00801ffa <opencons>:

int
opencons(void)
{
  801ffa:	55                   	push   %ebp
  801ffb:	89 e5                	mov    %esp,%ebp
  801ffd:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  802000:	8d 45 f4             	lea    -0xc(%ebp),%eax
  802003:	50                   	push   %eax
  802004:	e8 e3 ed ff ff       	call   800dec <fd_alloc>
  802009:	83 c4 10             	add    $0x10,%esp
  80200c:	85 c0                	test   %eax,%eax
  80200e:	78 3a                	js     80204a <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  802010:	83 ec 04             	sub    $0x4,%esp
  802013:	68 07 04 00 00       	push   $0x407
  802018:	ff 75 f4             	pushl  -0xc(%ebp)
  80201b:	6a 00                	push   $0x0
  80201d:	e8 93 eb ff ff       	call   800bb5 <sys_page_alloc>
  802022:	83 c4 10             	add    $0x10,%esp
  802025:	85 c0                	test   %eax,%eax
  802027:	78 21                	js     80204a <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  802029:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  80202f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802032:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  802034:	8b 45 f4             	mov    -0xc(%ebp),%eax
  802037:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  80203e:	83 ec 0c             	sub    $0xc,%esp
  802041:	50                   	push   %eax
  802042:	e8 7e ed ff ff       	call   800dc5 <fd2num>
  802047:	83 c4 10             	add    $0x10,%esp
}
  80204a:	c9                   	leave  
  80204b:	c3                   	ret    

0080204c <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  80204c:	55                   	push   %ebp
  80204d:	89 e5                	mov    %esp,%ebp
  80204f:	56                   	push   %esi
  802050:	53                   	push   %ebx
  802051:	8b 75 08             	mov    0x8(%ebp),%esi
  802054:	8b 45 0c             	mov    0xc(%ebp),%eax
  802057:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  80205a:	85 c0                	test   %eax,%eax
  80205c:	75 05                	jne    802063 <ipc_recv+0x17>
        pg = (void *)UTOP;
  80205e:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  802063:	83 ec 0c             	sub    $0xc,%esp
  802066:	50                   	push   %eax
  802067:	e8 18 ed ff ff       	call   800d84 <sys_ipc_recv>
  80206c:	83 c4 10             	add    $0x10,%esp
  80206f:	85 c0                	test   %eax,%eax
  802071:	74 16                	je     802089 <ipc_recv+0x3d>
        if (from_env_store)
  802073:	85 f6                	test   %esi,%esi
  802075:	74 06                	je     80207d <ipc_recv+0x31>
            *from_env_store = 0;
  802077:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  80207d:	85 db                	test   %ebx,%ebx
  80207f:	74 2c                	je     8020ad <ipc_recv+0x61>
            *perm_store = 0;
  802081:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  802087:	eb 24                	jmp    8020ad <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  802089:	85 f6                	test   %esi,%esi
  80208b:	74 0a                	je     802097 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  80208d:	a1 04 40 80 00       	mov    0x804004,%eax
  802092:	8b 40 74             	mov    0x74(%eax),%eax
  802095:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  802097:	85 db                	test   %ebx,%ebx
  802099:	74 0a                	je     8020a5 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  80209b:	a1 04 40 80 00       	mov    0x804004,%eax
  8020a0:	8b 40 78             	mov    0x78(%eax),%eax
  8020a3:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  8020a5:	a1 04 40 80 00       	mov    0x804004,%eax
  8020aa:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  8020ad:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8020b0:	5b                   	pop    %ebx
  8020b1:	5e                   	pop    %esi
  8020b2:	5d                   	pop    %ebp
  8020b3:	c3                   	ret    

008020b4 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  8020b4:	55                   	push   %ebp
  8020b5:	89 e5                	mov    %esp,%ebp
  8020b7:	57                   	push   %edi
  8020b8:	56                   	push   %esi
  8020b9:	53                   	push   %ebx
  8020ba:	83 ec 0c             	sub    $0xc,%esp
  8020bd:	8b 75 0c             	mov    0xc(%ebp),%esi
  8020c0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8020c3:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  8020c6:	85 db                	test   %ebx,%ebx
  8020c8:	75 0c                	jne    8020d6 <ipc_send+0x22>
        pg = (void *)UTOP;
  8020ca:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  8020cf:	eb 05                	jmp    8020d6 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  8020d1:	e8 c0 ea ff ff       	call   800b96 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  8020d6:	57                   	push   %edi
  8020d7:	53                   	push   %ebx
  8020d8:	56                   	push   %esi
  8020d9:	ff 75 08             	pushl  0x8(%ebp)
  8020dc:	e8 80 ec ff ff       	call   800d61 <sys_ipc_try_send>
  8020e1:	83 c4 10             	add    $0x10,%esp
  8020e4:	83 f8 f9             	cmp    $0xfffffff9,%eax
  8020e7:	74 e8                	je     8020d1 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  8020e9:	85 c0                	test   %eax,%eax
  8020eb:	74 14                	je     802101 <ipc_send+0x4d>
        panic("ipc_send: error");
  8020ed:	83 ec 04             	sub    $0x4,%esp
  8020f0:	68 cb 29 80 00       	push   $0x8029cb
  8020f5:	6a 6a                	push   $0x6a
  8020f7:	68 db 29 80 00       	push   $0x8029db
  8020fc:	e8 94 e0 ff ff       	call   800195 <_panic>
    }
	//panic("ipc_send not implemented");
}
  802101:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802104:	5b                   	pop    %ebx
  802105:	5e                   	pop    %esi
  802106:	5f                   	pop    %edi
  802107:	5d                   	pop    %ebp
  802108:	c3                   	ret    

00802109 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  802109:	55                   	push   %ebp
  80210a:	89 e5                	mov    %esp,%ebp
  80210c:	53                   	push   %ebx
  80210d:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  802110:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  802115:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  80211c:	89 d0                	mov    %edx,%eax
  80211e:	c1 e0 07             	shl    $0x7,%eax
  802121:	29 d8                	sub    %ebx,%eax
  802123:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  802128:	8b 40 50             	mov    0x50(%eax),%eax
  80212b:	39 c8                	cmp    %ecx,%eax
  80212d:	75 0d                	jne    80213c <ipc_find_env+0x33>
			return envs[i].env_id;
  80212f:	c1 e2 07             	shl    $0x7,%edx
  802132:	29 da                	sub    %ebx,%edx
  802134:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  80213a:	eb 0e                	jmp    80214a <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  80213c:	42                   	inc    %edx
  80213d:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  802143:	75 d0                	jne    802115 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  802145:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80214a:	5b                   	pop    %ebx
  80214b:	5d                   	pop    %ebp
  80214c:	c3                   	ret    

0080214d <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  80214d:	55                   	push   %ebp
  80214e:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  802150:	8b 45 08             	mov    0x8(%ebp),%eax
  802153:	c1 e8 16             	shr    $0x16,%eax
  802156:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  80215d:	a8 01                	test   $0x1,%al
  80215f:	74 21                	je     802182 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  802161:	8b 45 08             	mov    0x8(%ebp),%eax
  802164:	c1 e8 0c             	shr    $0xc,%eax
  802167:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  80216e:	a8 01                	test   $0x1,%al
  802170:	74 17                	je     802189 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  802172:	c1 e8 0c             	shr    $0xc,%eax
  802175:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  80217c:	ef 
  80217d:	0f b7 c0             	movzwl %ax,%eax
  802180:	eb 0c                	jmp    80218e <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  802182:	b8 00 00 00 00       	mov    $0x0,%eax
  802187:	eb 05                	jmp    80218e <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  802189:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  80218e:	5d                   	pop    %ebp
  80218f:	c3                   	ret    

00802190 <__udivdi3>:
  802190:	55                   	push   %ebp
  802191:	57                   	push   %edi
  802192:	56                   	push   %esi
  802193:	53                   	push   %ebx
  802194:	83 ec 1c             	sub    $0x1c,%esp
  802197:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  80219b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  80219f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8021a3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8021a7:	89 ca                	mov    %ecx,%edx
  8021a9:	89 f8                	mov    %edi,%eax
  8021ab:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8021af:	85 f6                	test   %esi,%esi
  8021b1:	75 2d                	jne    8021e0 <__udivdi3+0x50>
  8021b3:	39 cf                	cmp    %ecx,%edi
  8021b5:	77 65                	ja     80221c <__udivdi3+0x8c>
  8021b7:	89 fd                	mov    %edi,%ebp
  8021b9:	85 ff                	test   %edi,%edi
  8021bb:	75 0b                	jne    8021c8 <__udivdi3+0x38>
  8021bd:	b8 01 00 00 00       	mov    $0x1,%eax
  8021c2:	31 d2                	xor    %edx,%edx
  8021c4:	f7 f7                	div    %edi
  8021c6:	89 c5                	mov    %eax,%ebp
  8021c8:	31 d2                	xor    %edx,%edx
  8021ca:	89 c8                	mov    %ecx,%eax
  8021cc:	f7 f5                	div    %ebp
  8021ce:	89 c1                	mov    %eax,%ecx
  8021d0:	89 d8                	mov    %ebx,%eax
  8021d2:	f7 f5                	div    %ebp
  8021d4:	89 cf                	mov    %ecx,%edi
  8021d6:	89 fa                	mov    %edi,%edx
  8021d8:	83 c4 1c             	add    $0x1c,%esp
  8021db:	5b                   	pop    %ebx
  8021dc:	5e                   	pop    %esi
  8021dd:	5f                   	pop    %edi
  8021de:	5d                   	pop    %ebp
  8021df:	c3                   	ret    
  8021e0:	39 ce                	cmp    %ecx,%esi
  8021e2:	77 28                	ja     80220c <__udivdi3+0x7c>
  8021e4:	0f bd fe             	bsr    %esi,%edi
  8021e7:	83 f7 1f             	xor    $0x1f,%edi
  8021ea:	75 40                	jne    80222c <__udivdi3+0x9c>
  8021ec:	39 ce                	cmp    %ecx,%esi
  8021ee:	72 0a                	jb     8021fa <__udivdi3+0x6a>
  8021f0:	3b 44 24 08          	cmp    0x8(%esp),%eax
  8021f4:	0f 87 9e 00 00 00    	ja     802298 <__udivdi3+0x108>
  8021fa:	b8 01 00 00 00       	mov    $0x1,%eax
  8021ff:	89 fa                	mov    %edi,%edx
  802201:	83 c4 1c             	add    $0x1c,%esp
  802204:	5b                   	pop    %ebx
  802205:	5e                   	pop    %esi
  802206:	5f                   	pop    %edi
  802207:	5d                   	pop    %ebp
  802208:	c3                   	ret    
  802209:	8d 76 00             	lea    0x0(%esi),%esi
  80220c:	31 ff                	xor    %edi,%edi
  80220e:	31 c0                	xor    %eax,%eax
  802210:	89 fa                	mov    %edi,%edx
  802212:	83 c4 1c             	add    $0x1c,%esp
  802215:	5b                   	pop    %ebx
  802216:	5e                   	pop    %esi
  802217:	5f                   	pop    %edi
  802218:	5d                   	pop    %ebp
  802219:	c3                   	ret    
  80221a:	66 90                	xchg   %ax,%ax
  80221c:	89 d8                	mov    %ebx,%eax
  80221e:	f7 f7                	div    %edi
  802220:	31 ff                	xor    %edi,%edi
  802222:	89 fa                	mov    %edi,%edx
  802224:	83 c4 1c             	add    $0x1c,%esp
  802227:	5b                   	pop    %ebx
  802228:	5e                   	pop    %esi
  802229:	5f                   	pop    %edi
  80222a:	5d                   	pop    %ebp
  80222b:	c3                   	ret    
  80222c:	bd 20 00 00 00       	mov    $0x20,%ebp
  802231:	89 eb                	mov    %ebp,%ebx
  802233:	29 fb                	sub    %edi,%ebx
  802235:	89 f9                	mov    %edi,%ecx
  802237:	d3 e6                	shl    %cl,%esi
  802239:	89 c5                	mov    %eax,%ebp
  80223b:	88 d9                	mov    %bl,%cl
  80223d:	d3 ed                	shr    %cl,%ebp
  80223f:	89 e9                	mov    %ebp,%ecx
  802241:	09 f1                	or     %esi,%ecx
  802243:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  802247:	89 f9                	mov    %edi,%ecx
  802249:	d3 e0                	shl    %cl,%eax
  80224b:	89 c5                	mov    %eax,%ebp
  80224d:	89 d6                	mov    %edx,%esi
  80224f:	88 d9                	mov    %bl,%cl
  802251:	d3 ee                	shr    %cl,%esi
  802253:	89 f9                	mov    %edi,%ecx
  802255:	d3 e2                	shl    %cl,%edx
  802257:	8b 44 24 08          	mov    0x8(%esp),%eax
  80225b:	88 d9                	mov    %bl,%cl
  80225d:	d3 e8                	shr    %cl,%eax
  80225f:	09 c2                	or     %eax,%edx
  802261:	89 d0                	mov    %edx,%eax
  802263:	89 f2                	mov    %esi,%edx
  802265:	f7 74 24 0c          	divl   0xc(%esp)
  802269:	89 d6                	mov    %edx,%esi
  80226b:	89 c3                	mov    %eax,%ebx
  80226d:	f7 e5                	mul    %ebp
  80226f:	39 d6                	cmp    %edx,%esi
  802271:	72 19                	jb     80228c <__udivdi3+0xfc>
  802273:	74 0b                	je     802280 <__udivdi3+0xf0>
  802275:	89 d8                	mov    %ebx,%eax
  802277:	31 ff                	xor    %edi,%edi
  802279:	e9 58 ff ff ff       	jmp    8021d6 <__udivdi3+0x46>
  80227e:	66 90                	xchg   %ax,%ax
  802280:	8b 54 24 08          	mov    0x8(%esp),%edx
  802284:	89 f9                	mov    %edi,%ecx
  802286:	d3 e2                	shl    %cl,%edx
  802288:	39 c2                	cmp    %eax,%edx
  80228a:	73 e9                	jae    802275 <__udivdi3+0xe5>
  80228c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  80228f:	31 ff                	xor    %edi,%edi
  802291:	e9 40 ff ff ff       	jmp    8021d6 <__udivdi3+0x46>
  802296:	66 90                	xchg   %ax,%ax
  802298:	31 c0                	xor    %eax,%eax
  80229a:	e9 37 ff ff ff       	jmp    8021d6 <__udivdi3+0x46>
  80229f:	90                   	nop

008022a0 <__umoddi3>:
  8022a0:	55                   	push   %ebp
  8022a1:	57                   	push   %edi
  8022a2:	56                   	push   %esi
  8022a3:	53                   	push   %ebx
  8022a4:	83 ec 1c             	sub    $0x1c,%esp
  8022a7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8022ab:	8b 74 24 34          	mov    0x34(%esp),%esi
  8022af:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8022b3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8022b7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8022bb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8022bf:	89 f3                	mov    %esi,%ebx
  8022c1:	89 fa                	mov    %edi,%edx
  8022c3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8022c7:	89 34 24             	mov    %esi,(%esp)
  8022ca:	85 c0                	test   %eax,%eax
  8022cc:	75 1a                	jne    8022e8 <__umoddi3+0x48>
  8022ce:	39 f7                	cmp    %esi,%edi
  8022d0:	0f 86 a2 00 00 00    	jbe    802378 <__umoddi3+0xd8>
  8022d6:	89 c8                	mov    %ecx,%eax
  8022d8:	89 f2                	mov    %esi,%edx
  8022da:	f7 f7                	div    %edi
  8022dc:	89 d0                	mov    %edx,%eax
  8022de:	31 d2                	xor    %edx,%edx
  8022e0:	83 c4 1c             	add    $0x1c,%esp
  8022e3:	5b                   	pop    %ebx
  8022e4:	5e                   	pop    %esi
  8022e5:	5f                   	pop    %edi
  8022e6:	5d                   	pop    %ebp
  8022e7:	c3                   	ret    
  8022e8:	39 f0                	cmp    %esi,%eax
  8022ea:	0f 87 ac 00 00 00    	ja     80239c <__umoddi3+0xfc>
  8022f0:	0f bd e8             	bsr    %eax,%ebp
  8022f3:	83 f5 1f             	xor    $0x1f,%ebp
  8022f6:	0f 84 ac 00 00 00    	je     8023a8 <__umoddi3+0x108>
  8022fc:	bf 20 00 00 00       	mov    $0x20,%edi
  802301:	29 ef                	sub    %ebp,%edi
  802303:	89 fe                	mov    %edi,%esi
  802305:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802309:	89 e9                	mov    %ebp,%ecx
  80230b:	d3 e0                	shl    %cl,%eax
  80230d:	89 d7                	mov    %edx,%edi
  80230f:	89 f1                	mov    %esi,%ecx
  802311:	d3 ef                	shr    %cl,%edi
  802313:	09 c7                	or     %eax,%edi
  802315:	89 e9                	mov    %ebp,%ecx
  802317:	d3 e2                	shl    %cl,%edx
  802319:	89 14 24             	mov    %edx,(%esp)
  80231c:	89 d8                	mov    %ebx,%eax
  80231e:	d3 e0                	shl    %cl,%eax
  802320:	89 c2                	mov    %eax,%edx
  802322:	8b 44 24 08          	mov    0x8(%esp),%eax
  802326:	d3 e0                	shl    %cl,%eax
  802328:	89 44 24 04          	mov    %eax,0x4(%esp)
  80232c:	8b 44 24 08          	mov    0x8(%esp),%eax
  802330:	89 f1                	mov    %esi,%ecx
  802332:	d3 e8                	shr    %cl,%eax
  802334:	09 d0                	or     %edx,%eax
  802336:	d3 eb                	shr    %cl,%ebx
  802338:	89 da                	mov    %ebx,%edx
  80233a:	f7 f7                	div    %edi
  80233c:	89 d3                	mov    %edx,%ebx
  80233e:	f7 24 24             	mull   (%esp)
  802341:	89 c6                	mov    %eax,%esi
  802343:	89 d1                	mov    %edx,%ecx
  802345:	39 d3                	cmp    %edx,%ebx
  802347:	0f 82 87 00 00 00    	jb     8023d4 <__umoddi3+0x134>
  80234d:	0f 84 91 00 00 00    	je     8023e4 <__umoddi3+0x144>
  802353:	8b 54 24 04          	mov    0x4(%esp),%edx
  802357:	29 f2                	sub    %esi,%edx
  802359:	19 cb                	sbb    %ecx,%ebx
  80235b:	89 d8                	mov    %ebx,%eax
  80235d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  802361:	d3 e0                	shl    %cl,%eax
  802363:	89 e9                	mov    %ebp,%ecx
  802365:	d3 ea                	shr    %cl,%edx
  802367:	09 d0                	or     %edx,%eax
  802369:	89 e9                	mov    %ebp,%ecx
  80236b:	d3 eb                	shr    %cl,%ebx
  80236d:	89 da                	mov    %ebx,%edx
  80236f:	83 c4 1c             	add    $0x1c,%esp
  802372:	5b                   	pop    %ebx
  802373:	5e                   	pop    %esi
  802374:	5f                   	pop    %edi
  802375:	5d                   	pop    %ebp
  802376:	c3                   	ret    
  802377:	90                   	nop
  802378:	89 fd                	mov    %edi,%ebp
  80237a:	85 ff                	test   %edi,%edi
  80237c:	75 0b                	jne    802389 <__umoddi3+0xe9>
  80237e:	b8 01 00 00 00       	mov    $0x1,%eax
  802383:	31 d2                	xor    %edx,%edx
  802385:	f7 f7                	div    %edi
  802387:	89 c5                	mov    %eax,%ebp
  802389:	89 f0                	mov    %esi,%eax
  80238b:	31 d2                	xor    %edx,%edx
  80238d:	f7 f5                	div    %ebp
  80238f:	89 c8                	mov    %ecx,%eax
  802391:	f7 f5                	div    %ebp
  802393:	89 d0                	mov    %edx,%eax
  802395:	e9 44 ff ff ff       	jmp    8022de <__umoddi3+0x3e>
  80239a:	66 90                	xchg   %ax,%ax
  80239c:	89 c8                	mov    %ecx,%eax
  80239e:	89 f2                	mov    %esi,%edx
  8023a0:	83 c4 1c             	add    $0x1c,%esp
  8023a3:	5b                   	pop    %ebx
  8023a4:	5e                   	pop    %esi
  8023a5:	5f                   	pop    %edi
  8023a6:	5d                   	pop    %ebp
  8023a7:	c3                   	ret    
  8023a8:	3b 04 24             	cmp    (%esp),%eax
  8023ab:	72 06                	jb     8023b3 <__umoddi3+0x113>
  8023ad:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8023b1:	77 0f                	ja     8023c2 <__umoddi3+0x122>
  8023b3:	89 f2                	mov    %esi,%edx
  8023b5:	29 f9                	sub    %edi,%ecx
  8023b7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8023bb:	89 14 24             	mov    %edx,(%esp)
  8023be:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8023c2:	8b 44 24 04          	mov    0x4(%esp),%eax
  8023c6:	8b 14 24             	mov    (%esp),%edx
  8023c9:	83 c4 1c             	add    $0x1c,%esp
  8023cc:	5b                   	pop    %ebx
  8023cd:	5e                   	pop    %esi
  8023ce:	5f                   	pop    %edi
  8023cf:	5d                   	pop    %ebp
  8023d0:	c3                   	ret    
  8023d1:	8d 76 00             	lea    0x0(%esi),%esi
  8023d4:	2b 04 24             	sub    (%esp),%eax
  8023d7:	19 fa                	sbb    %edi,%edx
  8023d9:	89 d1                	mov    %edx,%ecx
  8023db:	89 c6                	mov    %eax,%esi
  8023dd:	e9 71 ff ff ff       	jmp    802353 <__umoddi3+0xb3>
  8023e2:	66 90                	xchg   %ax,%ax
  8023e4:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8023e8:	72 ea                	jb     8023d4 <__umoddi3+0x134>
  8023ea:	89 d9                	mov    %ebx,%ecx
  8023ec:	e9 62 ff ff ff       	jmp    802353 <__umoddi3+0xb3>
