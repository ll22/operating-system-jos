
obj/user/testpipe.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 81 02 00 00       	call   8002b2 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

char *msg = "Now is the time for all good men to come to the aid of their party.";

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	83 ec 7c             	sub    $0x7c,%esp
	char buf[100];
	int i, pid, p[2];

	binaryname = "pipereadeof";
  80003b:	c7 05 04 30 80 00 40 	movl   $0x802340,0x803004
  800042:	23 80 00 

	if ((i = pipe(p)) < 0)
  800045:	8d 45 8c             	lea    -0x74(%ebp),%eax
  800048:	50                   	push   %eax
  800049:	e8 6f 1b 00 00       	call   801bbd <pipe>
  80004e:	89 c6                	mov    %eax,%esi
  800050:	83 c4 10             	add    $0x10,%esp
  800053:	85 c0                	test   %eax,%eax
  800055:	79 12                	jns    800069 <umain+0x36>
		panic("pipe: %e", i);
  800057:	50                   	push   %eax
  800058:	68 4c 23 80 00       	push   $0x80234c
  80005d:	6a 0e                	push   $0xe
  80005f:	68 55 23 80 00       	push   $0x802355
  800064:	e8 aa 02 00 00       	call   800313 <_panic>

	if ((pid = fork()) < 0)
  800069:	e8 87 0f 00 00       	call   800ff5 <fork>
  80006e:	89 c3                	mov    %eax,%ebx
  800070:	85 c0                	test   %eax,%eax
  800072:	79 12                	jns    800086 <umain+0x53>
		panic("fork: %e", i);
  800074:	56                   	push   %esi
  800075:	68 65 23 80 00       	push   $0x802365
  80007a:	6a 11                	push   $0x11
  80007c:	68 55 23 80 00       	push   $0x802355
  800081:	e8 8d 02 00 00       	call   800313 <_panic>

	if (pid == 0) {
  800086:	85 c0                	test   %eax,%eax
  800088:	0f 85 b8 00 00 00    	jne    800146 <umain+0x113>
		cprintf("[%08x] pipereadeof close %d\n", thisenv->env_id, p[1]);
  80008e:	a1 04 40 80 00       	mov    0x804004,%eax
  800093:	8b 40 48             	mov    0x48(%eax),%eax
  800096:	83 ec 04             	sub    $0x4,%esp
  800099:	ff 75 90             	pushl  -0x70(%ebp)
  80009c:	50                   	push   %eax
  80009d:	68 6e 23 80 00       	push   $0x80236e
  8000a2:	e8 44 03 00 00       	call   8003eb <cprintf>
		close(p[1]);
  8000a7:	83 c4 04             	add    $0x4,%esp
  8000aa:	ff 75 90             	pushl  -0x70(%ebp)
  8000ad:	e8 0b 13 00 00       	call   8013bd <close>
		cprintf("[%08x] pipereadeof readn %d\n", thisenv->env_id, p[0]);
  8000b2:	a1 04 40 80 00       	mov    0x804004,%eax
  8000b7:	8b 40 48             	mov    0x48(%eax),%eax
  8000ba:	83 c4 0c             	add    $0xc,%esp
  8000bd:	ff 75 8c             	pushl  -0x74(%ebp)
  8000c0:	50                   	push   %eax
  8000c1:	68 8b 23 80 00       	push   $0x80238b
  8000c6:	e8 20 03 00 00       	call   8003eb <cprintf>
		i = readn(p[0], buf, sizeof buf-1);
  8000cb:	83 c4 0c             	add    $0xc,%esp
  8000ce:	6a 63                	push   $0x63
  8000d0:	8d 45 94             	lea    -0x6c(%ebp),%eax
  8000d3:	50                   	push   %eax
  8000d4:	ff 75 8c             	pushl  -0x74(%ebp)
  8000d7:	e8 a0 14 00 00       	call   80157c <readn>
  8000dc:	89 c6                	mov    %eax,%esi
		if (i < 0)
  8000de:	83 c4 10             	add    $0x10,%esp
  8000e1:	85 c0                	test   %eax,%eax
  8000e3:	79 12                	jns    8000f7 <umain+0xc4>
			panic("read: %e", i);
  8000e5:	50                   	push   %eax
  8000e6:	68 a8 23 80 00       	push   $0x8023a8
  8000eb:	6a 19                	push   $0x19
  8000ed:	68 55 23 80 00       	push   $0x802355
  8000f2:	e8 1c 02 00 00       	call   800313 <_panic>
		buf[i] = 0;
  8000f7:	c6 44 05 94 00       	movb   $0x0,-0x6c(%ebp,%eax,1)
		if (strcmp(buf, msg) == 0)
  8000fc:	83 ec 08             	sub    $0x8,%esp
  8000ff:	ff 35 00 30 80 00    	pushl  0x803000
  800105:	8d 45 94             	lea    -0x6c(%ebp),%eax
  800108:	50                   	push   %eax
  800109:	e8 e0 08 00 00       	call   8009ee <strcmp>
  80010e:	83 c4 10             	add    $0x10,%esp
  800111:	85 c0                	test   %eax,%eax
  800113:	75 12                	jne    800127 <umain+0xf4>
			cprintf("\npipe read closed properly\n");
  800115:	83 ec 0c             	sub    $0xc,%esp
  800118:	68 b1 23 80 00       	push   $0x8023b1
  80011d:	e8 c9 02 00 00       	call   8003eb <cprintf>
  800122:	83 c4 10             	add    $0x10,%esp
  800125:	eb 15                	jmp    80013c <umain+0x109>
		else
			cprintf("\ngot %d bytes: %s\n", i, buf);
  800127:	83 ec 04             	sub    $0x4,%esp
  80012a:	8d 45 94             	lea    -0x6c(%ebp),%eax
  80012d:	50                   	push   %eax
  80012e:	56                   	push   %esi
  80012f:	68 cd 23 80 00       	push   $0x8023cd
  800134:	e8 b2 02 00 00       	call   8003eb <cprintf>
  800139:	83 c4 10             	add    $0x10,%esp
		exit();
  80013c:	e8 c0 01 00 00       	call   800301 <exit>
  800141:	e9 94 00 00 00       	jmp    8001da <umain+0x1a7>
	} else {
		cprintf("[%08x] pipereadeof close %d\n", thisenv->env_id, p[0]);
  800146:	a1 04 40 80 00       	mov    0x804004,%eax
  80014b:	8b 40 48             	mov    0x48(%eax),%eax
  80014e:	83 ec 04             	sub    $0x4,%esp
  800151:	ff 75 8c             	pushl  -0x74(%ebp)
  800154:	50                   	push   %eax
  800155:	68 6e 23 80 00       	push   $0x80236e
  80015a:	e8 8c 02 00 00       	call   8003eb <cprintf>
		close(p[0]);
  80015f:	83 c4 04             	add    $0x4,%esp
  800162:	ff 75 8c             	pushl  -0x74(%ebp)
  800165:	e8 53 12 00 00       	call   8013bd <close>
		cprintf("[%08x] pipereadeof write %d\n", thisenv->env_id, p[1]);
  80016a:	a1 04 40 80 00       	mov    0x804004,%eax
  80016f:	8b 40 48             	mov    0x48(%eax),%eax
  800172:	83 c4 0c             	add    $0xc,%esp
  800175:	ff 75 90             	pushl  -0x70(%ebp)
  800178:	50                   	push   %eax
  800179:	68 e0 23 80 00       	push   $0x8023e0
  80017e:	e8 68 02 00 00       	call   8003eb <cprintf>
		if ((i = write(p[1], msg, strlen(msg))) != strlen(msg))
  800183:	83 c4 04             	add    $0x4,%esp
  800186:	ff 35 00 30 80 00    	pushl  0x803000
  80018c:	e8 89 07 00 00       	call   80091a <strlen>
  800191:	83 c4 0c             	add    $0xc,%esp
  800194:	50                   	push   %eax
  800195:	ff 35 00 30 80 00    	pushl  0x803000
  80019b:	ff 75 90             	pushl  -0x70(%ebp)
  80019e:	e8 22 14 00 00       	call   8015c5 <write>
  8001a3:	89 c6                	mov    %eax,%esi
  8001a5:	83 c4 04             	add    $0x4,%esp
  8001a8:	ff 35 00 30 80 00    	pushl  0x803000
  8001ae:	e8 67 07 00 00       	call   80091a <strlen>
  8001b3:	83 c4 10             	add    $0x10,%esp
  8001b6:	39 c6                	cmp    %eax,%esi
  8001b8:	74 12                	je     8001cc <umain+0x199>
			panic("write: %e", i);
  8001ba:	56                   	push   %esi
  8001bb:	68 fd 23 80 00       	push   $0x8023fd
  8001c0:	6a 25                	push   $0x25
  8001c2:	68 55 23 80 00       	push   $0x802355
  8001c7:	e8 47 01 00 00       	call   800313 <_panic>
		close(p[1]);
  8001cc:	83 ec 0c             	sub    $0xc,%esp
  8001cf:	ff 75 90             	pushl  -0x70(%ebp)
  8001d2:	e8 e6 11 00 00       	call   8013bd <close>
  8001d7:	83 c4 10             	add    $0x10,%esp
	}
	wait(pid);
  8001da:	83 ec 0c             	sub    $0xc,%esp
  8001dd:	53                   	push   %ebx
  8001de:	e8 5a 1b 00 00       	call   801d3d <wait>

	binaryname = "pipewriteeof";
  8001e3:	c7 05 04 30 80 00 07 	movl   $0x802407,0x803004
  8001ea:	24 80 00 
	if ((i = pipe(p)) < 0)
  8001ed:	8d 45 8c             	lea    -0x74(%ebp),%eax
  8001f0:	89 04 24             	mov    %eax,(%esp)
  8001f3:	e8 c5 19 00 00       	call   801bbd <pipe>
  8001f8:	89 c6                	mov    %eax,%esi
  8001fa:	83 c4 10             	add    $0x10,%esp
  8001fd:	85 c0                	test   %eax,%eax
  8001ff:	79 12                	jns    800213 <umain+0x1e0>
		panic("pipe: %e", i);
  800201:	50                   	push   %eax
  800202:	68 4c 23 80 00       	push   $0x80234c
  800207:	6a 2c                	push   $0x2c
  800209:	68 55 23 80 00       	push   $0x802355
  80020e:	e8 00 01 00 00       	call   800313 <_panic>

	if ((pid = fork()) < 0)
  800213:	e8 dd 0d 00 00       	call   800ff5 <fork>
  800218:	89 c3                	mov    %eax,%ebx
  80021a:	85 c0                	test   %eax,%eax
  80021c:	79 12                	jns    800230 <umain+0x1fd>
		panic("fork: %e", i);
  80021e:	56                   	push   %esi
  80021f:	68 65 23 80 00       	push   $0x802365
  800224:	6a 2f                	push   $0x2f
  800226:	68 55 23 80 00       	push   $0x802355
  80022b:	e8 e3 00 00 00       	call   800313 <_panic>

	if (pid == 0) {
  800230:	85 c0                	test   %eax,%eax
  800232:	75 4a                	jne    80027e <umain+0x24b>
		close(p[0]);
  800234:	83 ec 0c             	sub    $0xc,%esp
  800237:	ff 75 8c             	pushl  -0x74(%ebp)
  80023a:	e8 7e 11 00 00       	call   8013bd <close>
  80023f:	83 c4 10             	add    $0x10,%esp
		while (1) {
			cprintf(".");
  800242:	83 ec 0c             	sub    $0xc,%esp
  800245:	68 14 24 80 00       	push   $0x802414
  80024a:	e8 9c 01 00 00       	call   8003eb <cprintf>
			if (write(p[1], "x", 1) != 1)
  80024f:	83 c4 0c             	add    $0xc,%esp
  800252:	6a 01                	push   $0x1
  800254:	68 16 24 80 00       	push   $0x802416
  800259:	ff 75 90             	pushl  -0x70(%ebp)
  80025c:	e8 64 13 00 00       	call   8015c5 <write>
  800261:	83 c4 10             	add    $0x10,%esp
  800264:	83 f8 01             	cmp    $0x1,%eax
  800267:	74 d9                	je     800242 <umain+0x20f>
				break;
		}
		cprintf("\npipe write closed properly\n");
  800269:	83 ec 0c             	sub    $0xc,%esp
  80026c:	68 18 24 80 00       	push   $0x802418
  800271:	e8 75 01 00 00       	call   8003eb <cprintf>
		exit();
  800276:	e8 86 00 00 00       	call   800301 <exit>
  80027b:	83 c4 10             	add    $0x10,%esp
	}
	close(p[0]);
  80027e:	83 ec 0c             	sub    $0xc,%esp
  800281:	ff 75 8c             	pushl  -0x74(%ebp)
  800284:	e8 34 11 00 00       	call   8013bd <close>
	close(p[1]);
  800289:	83 c4 04             	add    $0x4,%esp
  80028c:	ff 75 90             	pushl  -0x70(%ebp)
  80028f:	e8 29 11 00 00       	call   8013bd <close>
	wait(pid);
  800294:	89 1c 24             	mov    %ebx,(%esp)
  800297:	e8 a1 1a 00 00       	call   801d3d <wait>

	cprintf("pipe tests passed\n");
  80029c:	c7 04 24 35 24 80 00 	movl   $0x802435,(%esp)
  8002a3:	e8 43 01 00 00       	call   8003eb <cprintf>
}
  8002a8:	83 c4 10             	add    $0x10,%esp
  8002ab:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8002ae:	5b                   	pop    %ebx
  8002af:	5e                   	pop    %esi
  8002b0:	5d                   	pop    %ebp
  8002b1:	c3                   	ret    

008002b2 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8002b2:	55                   	push   %ebp
  8002b3:	89 e5                	mov    %esp,%ebp
  8002b5:	56                   	push   %esi
  8002b6:	53                   	push   %ebx
  8002b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8002ba:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8002bd:	e8 33 0a 00 00       	call   800cf5 <sys_getenvid>
  8002c2:	25 ff 03 00 00       	and    $0x3ff,%eax
  8002c7:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8002ce:	c1 e0 07             	shl    $0x7,%eax
  8002d1:	29 d0                	sub    %edx,%eax
  8002d3:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8002d8:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8002dd:	85 db                	test   %ebx,%ebx
  8002df:	7e 07                	jle    8002e8 <libmain+0x36>
		binaryname = argv[0];
  8002e1:	8b 06                	mov    (%esi),%eax
  8002e3:	a3 04 30 80 00       	mov    %eax,0x803004

	// call user main routine
	umain(argc, argv);
  8002e8:	83 ec 08             	sub    $0x8,%esp
  8002eb:	56                   	push   %esi
  8002ec:	53                   	push   %ebx
  8002ed:	e8 41 fd ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8002f2:	e8 0a 00 00 00       	call   800301 <exit>
}
  8002f7:	83 c4 10             	add    $0x10,%esp
  8002fa:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8002fd:	5b                   	pop    %ebx
  8002fe:	5e                   	pop    %esi
  8002ff:	5d                   	pop    %ebp
  800300:	c3                   	ret    

00800301 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800301:	55                   	push   %ebp
  800302:	89 e5                	mov    %esp,%ebp
  800304:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800307:	6a 00                	push   $0x0
  800309:	e8 a6 09 00 00       	call   800cb4 <sys_env_destroy>
}
  80030e:	83 c4 10             	add    $0x10,%esp
  800311:	c9                   	leave  
  800312:	c3                   	ret    

00800313 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800313:	55                   	push   %ebp
  800314:	89 e5                	mov    %esp,%ebp
  800316:	56                   	push   %esi
  800317:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800318:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80031b:	8b 35 04 30 80 00    	mov    0x803004,%esi
  800321:	e8 cf 09 00 00       	call   800cf5 <sys_getenvid>
  800326:	83 ec 0c             	sub    $0xc,%esp
  800329:	ff 75 0c             	pushl  0xc(%ebp)
  80032c:	ff 75 08             	pushl  0x8(%ebp)
  80032f:	56                   	push   %esi
  800330:	50                   	push   %eax
  800331:	68 98 24 80 00       	push   $0x802498
  800336:	e8 b0 00 00 00       	call   8003eb <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80033b:	83 c4 18             	add    $0x18,%esp
  80033e:	53                   	push   %ebx
  80033f:	ff 75 10             	pushl  0x10(%ebp)
  800342:	e8 53 00 00 00       	call   80039a <vcprintf>
	cprintf("\n");
  800347:	c7 04 24 89 23 80 00 	movl   $0x802389,(%esp)
  80034e:	e8 98 00 00 00       	call   8003eb <cprintf>
  800353:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800356:	cc                   	int3   
  800357:	eb fd                	jmp    800356 <_panic+0x43>

00800359 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800359:	55                   	push   %ebp
  80035a:	89 e5                	mov    %esp,%ebp
  80035c:	53                   	push   %ebx
  80035d:	83 ec 04             	sub    $0x4,%esp
  800360:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800363:	8b 13                	mov    (%ebx),%edx
  800365:	8d 42 01             	lea    0x1(%edx),%eax
  800368:	89 03                	mov    %eax,(%ebx)
  80036a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80036d:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800371:	3d ff 00 00 00       	cmp    $0xff,%eax
  800376:	75 1a                	jne    800392 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800378:	83 ec 08             	sub    $0x8,%esp
  80037b:	68 ff 00 00 00       	push   $0xff
  800380:	8d 43 08             	lea    0x8(%ebx),%eax
  800383:	50                   	push   %eax
  800384:	e8 ee 08 00 00       	call   800c77 <sys_cputs>
		b->idx = 0;
  800389:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80038f:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800392:	ff 43 04             	incl   0x4(%ebx)
}
  800395:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800398:	c9                   	leave  
  800399:	c3                   	ret    

0080039a <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80039a:	55                   	push   %ebp
  80039b:	89 e5                	mov    %esp,%ebp
  80039d:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003a3:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003aa:	00 00 00 
	b.cnt = 0;
  8003ad:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003b4:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003b7:	ff 75 0c             	pushl  0xc(%ebp)
  8003ba:	ff 75 08             	pushl  0x8(%ebp)
  8003bd:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003c3:	50                   	push   %eax
  8003c4:	68 59 03 80 00       	push   $0x800359
  8003c9:	e8 51 01 00 00       	call   80051f <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003ce:	83 c4 08             	add    $0x8,%esp
  8003d1:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003d7:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003dd:	50                   	push   %eax
  8003de:	e8 94 08 00 00       	call   800c77 <sys_cputs>

	return b.cnt;
}
  8003e3:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8003e9:	c9                   	leave  
  8003ea:	c3                   	ret    

008003eb <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8003eb:	55                   	push   %ebp
  8003ec:	89 e5                	mov    %esp,%ebp
  8003ee:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8003f1:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8003f4:	50                   	push   %eax
  8003f5:	ff 75 08             	pushl  0x8(%ebp)
  8003f8:	e8 9d ff ff ff       	call   80039a <vcprintf>
	va_end(ap);

	return cnt;
}
  8003fd:	c9                   	leave  
  8003fe:	c3                   	ret    

008003ff <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8003ff:	55                   	push   %ebp
  800400:	89 e5                	mov    %esp,%ebp
  800402:	57                   	push   %edi
  800403:	56                   	push   %esi
  800404:	53                   	push   %ebx
  800405:	83 ec 1c             	sub    $0x1c,%esp
  800408:	89 c7                	mov    %eax,%edi
  80040a:	89 d6                	mov    %edx,%esi
  80040c:	8b 45 08             	mov    0x8(%ebp),%eax
  80040f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800412:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800415:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800418:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80041b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800420:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800423:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800426:	39 d3                	cmp    %edx,%ebx
  800428:	72 05                	jb     80042f <printnum+0x30>
  80042a:	39 45 10             	cmp    %eax,0x10(%ebp)
  80042d:	77 45                	ja     800474 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80042f:	83 ec 0c             	sub    $0xc,%esp
  800432:	ff 75 18             	pushl  0x18(%ebp)
  800435:	8b 45 14             	mov    0x14(%ebp),%eax
  800438:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80043b:	53                   	push   %ebx
  80043c:	ff 75 10             	pushl  0x10(%ebp)
  80043f:	83 ec 08             	sub    $0x8,%esp
  800442:	ff 75 e4             	pushl  -0x1c(%ebp)
  800445:	ff 75 e0             	pushl  -0x20(%ebp)
  800448:	ff 75 dc             	pushl  -0x24(%ebp)
  80044b:	ff 75 d8             	pushl  -0x28(%ebp)
  80044e:	e8 81 1c 00 00       	call   8020d4 <__udivdi3>
  800453:	83 c4 18             	add    $0x18,%esp
  800456:	52                   	push   %edx
  800457:	50                   	push   %eax
  800458:	89 f2                	mov    %esi,%edx
  80045a:	89 f8                	mov    %edi,%eax
  80045c:	e8 9e ff ff ff       	call   8003ff <printnum>
  800461:	83 c4 20             	add    $0x20,%esp
  800464:	eb 16                	jmp    80047c <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800466:	83 ec 08             	sub    $0x8,%esp
  800469:	56                   	push   %esi
  80046a:	ff 75 18             	pushl  0x18(%ebp)
  80046d:	ff d7                	call   *%edi
  80046f:	83 c4 10             	add    $0x10,%esp
  800472:	eb 03                	jmp    800477 <printnum+0x78>
  800474:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800477:	4b                   	dec    %ebx
  800478:	85 db                	test   %ebx,%ebx
  80047a:	7f ea                	jg     800466 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80047c:	83 ec 08             	sub    $0x8,%esp
  80047f:	56                   	push   %esi
  800480:	83 ec 04             	sub    $0x4,%esp
  800483:	ff 75 e4             	pushl  -0x1c(%ebp)
  800486:	ff 75 e0             	pushl  -0x20(%ebp)
  800489:	ff 75 dc             	pushl  -0x24(%ebp)
  80048c:	ff 75 d8             	pushl  -0x28(%ebp)
  80048f:	e8 50 1d 00 00       	call   8021e4 <__umoddi3>
  800494:	83 c4 14             	add    $0x14,%esp
  800497:	0f be 80 bb 24 80 00 	movsbl 0x8024bb(%eax),%eax
  80049e:	50                   	push   %eax
  80049f:	ff d7                	call   *%edi
}
  8004a1:	83 c4 10             	add    $0x10,%esp
  8004a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004a7:	5b                   	pop    %ebx
  8004a8:	5e                   	pop    %esi
  8004a9:	5f                   	pop    %edi
  8004aa:	5d                   	pop    %ebp
  8004ab:	c3                   	ret    

008004ac <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004ac:	55                   	push   %ebp
  8004ad:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004af:	83 fa 01             	cmp    $0x1,%edx
  8004b2:	7e 0e                	jle    8004c2 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004b4:	8b 10                	mov    (%eax),%edx
  8004b6:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004b9:	89 08                	mov    %ecx,(%eax)
  8004bb:	8b 02                	mov    (%edx),%eax
  8004bd:	8b 52 04             	mov    0x4(%edx),%edx
  8004c0:	eb 22                	jmp    8004e4 <getuint+0x38>
	else if (lflag)
  8004c2:	85 d2                	test   %edx,%edx
  8004c4:	74 10                	je     8004d6 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004c6:	8b 10                	mov    (%eax),%edx
  8004c8:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004cb:	89 08                	mov    %ecx,(%eax)
  8004cd:	8b 02                	mov    (%edx),%eax
  8004cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8004d4:	eb 0e                	jmp    8004e4 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004d6:	8b 10                	mov    (%eax),%edx
  8004d8:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004db:	89 08                	mov    %ecx,(%eax)
  8004dd:	8b 02                	mov    (%edx),%eax
  8004df:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8004e4:	5d                   	pop    %ebp
  8004e5:	c3                   	ret    

008004e6 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8004e6:	55                   	push   %ebp
  8004e7:	89 e5                	mov    %esp,%ebp
  8004e9:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8004ec:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8004ef:	8b 10                	mov    (%eax),%edx
  8004f1:	3b 50 04             	cmp    0x4(%eax),%edx
  8004f4:	73 0a                	jae    800500 <sprintputch+0x1a>
		*b->buf++ = ch;
  8004f6:	8d 4a 01             	lea    0x1(%edx),%ecx
  8004f9:	89 08                	mov    %ecx,(%eax)
  8004fb:	8b 45 08             	mov    0x8(%ebp),%eax
  8004fe:	88 02                	mov    %al,(%edx)
}
  800500:	5d                   	pop    %ebp
  800501:	c3                   	ret    

00800502 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800502:	55                   	push   %ebp
  800503:	89 e5                	mov    %esp,%ebp
  800505:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800508:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80050b:	50                   	push   %eax
  80050c:	ff 75 10             	pushl  0x10(%ebp)
  80050f:	ff 75 0c             	pushl  0xc(%ebp)
  800512:	ff 75 08             	pushl  0x8(%ebp)
  800515:	e8 05 00 00 00       	call   80051f <vprintfmt>
	va_end(ap);
}
  80051a:	83 c4 10             	add    $0x10,%esp
  80051d:	c9                   	leave  
  80051e:	c3                   	ret    

0080051f <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80051f:	55                   	push   %ebp
  800520:	89 e5                	mov    %esp,%ebp
  800522:	57                   	push   %edi
  800523:	56                   	push   %esi
  800524:	53                   	push   %ebx
  800525:	83 ec 2c             	sub    $0x2c,%esp
  800528:	8b 75 08             	mov    0x8(%ebp),%esi
  80052b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80052e:	8b 7d 10             	mov    0x10(%ebp),%edi
  800531:	eb 12                	jmp    800545 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800533:	85 c0                	test   %eax,%eax
  800535:	0f 84 68 03 00 00    	je     8008a3 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80053b:	83 ec 08             	sub    $0x8,%esp
  80053e:	53                   	push   %ebx
  80053f:	50                   	push   %eax
  800540:	ff d6                	call   *%esi
  800542:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800545:	47                   	inc    %edi
  800546:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80054a:	83 f8 25             	cmp    $0x25,%eax
  80054d:	75 e4                	jne    800533 <vprintfmt+0x14>
  80054f:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800553:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80055a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800561:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800568:	ba 00 00 00 00       	mov    $0x0,%edx
  80056d:	eb 07                	jmp    800576 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80056f:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800572:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800576:	8d 47 01             	lea    0x1(%edi),%eax
  800579:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80057c:	0f b6 0f             	movzbl (%edi),%ecx
  80057f:	8a 07                	mov    (%edi),%al
  800581:	83 e8 23             	sub    $0x23,%eax
  800584:	3c 55                	cmp    $0x55,%al
  800586:	0f 87 fe 02 00 00    	ja     80088a <vprintfmt+0x36b>
  80058c:	0f b6 c0             	movzbl %al,%eax
  80058f:	ff 24 85 00 26 80 00 	jmp    *0x802600(,%eax,4)
  800596:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800599:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80059d:	eb d7                	jmp    800576 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005a2:	b8 00 00 00 00       	mov    $0x0,%eax
  8005a7:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005aa:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005ad:	01 c0                	add    %eax,%eax
  8005af:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005b3:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005b6:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005b9:	83 fa 09             	cmp    $0x9,%edx
  8005bc:	77 34                	ja     8005f2 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005be:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005bf:	eb e9                	jmp    8005aa <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005c1:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c4:	8d 48 04             	lea    0x4(%eax),%ecx
  8005c7:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005ca:	8b 00                	mov    (%eax),%eax
  8005cc:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005cf:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005d2:	eb 24                	jmp    8005f8 <vprintfmt+0xd9>
  8005d4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005d8:	79 07                	jns    8005e1 <vprintfmt+0xc2>
  8005da:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005e4:	eb 90                	jmp    800576 <vprintfmt+0x57>
  8005e6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8005e9:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8005f0:	eb 84                	jmp    800576 <vprintfmt+0x57>
  8005f2:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8005f5:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8005f8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005fc:	0f 89 74 ff ff ff    	jns    800576 <vprintfmt+0x57>
				width = precision, precision = -1;
  800602:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800605:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800608:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80060f:	e9 62 ff ff ff       	jmp    800576 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800614:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800615:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800618:	e9 59 ff ff ff       	jmp    800576 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80061d:	8b 45 14             	mov    0x14(%ebp),%eax
  800620:	8d 50 04             	lea    0x4(%eax),%edx
  800623:	89 55 14             	mov    %edx,0x14(%ebp)
  800626:	83 ec 08             	sub    $0x8,%esp
  800629:	53                   	push   %ebx
  80062a:	ff 30                	pushl  (%eax)
  80062c:	ff d6                	call   *%esi
			break;
  80062e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800631:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800634:	e9 0c ff ff ff       	jmp    800545 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800639:	8b 45 14             	mov    0x14(%ebp),%eax
  80063c:	8d 50 04             	lea    0x4(%eax),%edx
  80063f:	89 55 14             	mov    %edx,0x14(%ebp)
  800642:	8b 00                	mov    (%eax),%eax
  800644:	85 c0                	test   %eax,%eax
  800646:	79 02                	jns    80064a <vprintfmt+0x12b>
  800648:	f7 d8                	neg    %eax
  80064a:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80064c:	83 f8 0f             	cmp    $0xf,%eax
  80064f:	7f 0b                	jg     80065c <vprintfmt+0x13d>
  800651:	8b 04 85 60 27 80 00 	mov    0x802760(,%eax,4),%eax
  800658:	85 c0                	test   %eax,%eax
  80065a:	75 18                	jne    800674 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80065c:	52                   	push   %edx
  80065d:	68 d3 24 80 00       	push   $0x8024d3
  800662:	53                   	push   %ebx
  800663:	56                   	push   %esi
  800664:	e8 99 fe ff ff       	call   800502 <printfmt>
  800669:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80066c:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80066f:	e9 d1 fe ff ff       	jmp    800545 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800674:	50                   	push   %eax
  800675:	68 c5 29 80 00       	push   $0x8029c5
  80067a:	53                   	push   %ebx
  80067b:	56                   	push   %esi
  80067c:	e8 81 fe ff ff       	call   800502 <printfmt>
  800681:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800684:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800687:	e9 b9 fe ff ff       	jmp    800545 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80068c:	8b 45 14             	mov    0x14(%ebp),%eax
  80068f:	8d 50 04             	lea    0x4(%eax),%edx
  800692:	89 55 14             	mov    %edx,0x14(%ebp)
  800695:	8b 38                	mov    (%eax),%edi
  800697:	85 ff                	test   %edi,%edi
  800699:	75 05                	jne    8006a0 <vprintfmt+0x181>
				p = "(null)";
  80069b:	bf cc 24 80 00       	mov    $0x8024cc,%edi
			if (width > 0 && padc != '-')
  8006a0:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006a4:	0f 8e 90 00 00 00    	jle    80073a <vprintfmt+0x21b>
  8006aa:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006ae:	0f 84 8e 00 00 00    	je     800742 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006b4:	83 ec 08             	sub    $0x8,%esp
  8006b7:	ff 75 d0             	pushl  -0x30(%ebp)
  8006ba:	57                   	push   %edi
  8006bb:	e8 70 02 00 00       	call   800930 <strnlen>
  8006c0:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006c3:	29 c1                	sub    %eax,%ecx
  8006c5:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006c8:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006cb:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006cf:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006d2:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006d5:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006d7:	eb 0d                	jmp    8006e6 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006d9:	83 ec 08             	sub    $0x8,%esp
  8006dc:	53                   	push   %ebx
  8006dd:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006e0:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006e2:	4f                   	dec    %edi
  8006e3:	83 c4 10             	add    $0x10,%esp
  8006e6:	85 ff                	test   %edi,%edi
  8006e8:	7f ef                	jg     8006d9 <vprintfmt+0x1ba>
  8006ea:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8006ed:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8006f0:	89 c8                	mov    %ecx,%eax
  8006f2:	85 c9                	test   %ecx,%ecx
  8006f4:	79 05                	jns    8006fb <vprintfmt+0x1dc>
  8006f6:	b8 00 00 00 00       	mov    $0x0,%eax
  8006fb:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8006fe:	29 c1                	sub    %eax,%ecx
  800700:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800703:	89 75 08             	mov    %esi,0x8(%ebp)
  800706:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800709:	eb 3d                	jmp    800748 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80070b:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80070f:	74 19                	je     80072a <vprintfmt+0x20b>
  800711:	0f be c0             	movsbl %al,%eax
  800714:	83 e8 20             	sub    $0x20,%eax
  800717:	83 f8 5e             	cmp    $0x5e,%eax
  80071a:	76 0e                	jbe    80072a <vprintfmt+0x20b>
					putch('?', putdat);
  80071c:	83 ec 08             	sub    $0x8,%esp
  80071f:	53                   	push   %ebx
  800720:	6a 3f                	push   $0x3f
  800722:	ff 55 08             	call   *0x8(%ebp)
  800725:	83 c4 10             	add    $0x10,%esp
  800728:	eb 0b                	jmp    800735 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80072a:	83 ec 08             	sub    $0x8,%esp
  80072d:	53                   	push   %ebx
  80072e:	52                   	push   %edx
  80072f:	ff 55 08             	call   *0x8(%ebp)
  800732:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800735:	ff 4d e4             	decl   -0x1c(%ebp)
  800738:	eb 0e                	jmp    800748 <vprintfmt+0x229>
  80073a:	89 75 08             	mov    %esi,0x8(%ebp)
  80073d:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800740:	eb 06                	jmp    800748 <vprintfmt+0x229>
  800742:	89 75 08             	mov    %esi,0x8(%ebp)
  800745:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800748:	47                   	inc    %edi
  800749:	8a 47 ff             	mov    -0x1(%edi),%al
  80074c:	0f be d0             	movsbl %al,%edx
  80074f:	85 d2                	test   %edx,%edx
  800751:	74 1d                	je     800770 <vprintfmt+0x251>
  800753:	85 f6                	test   %esi,%esi
  800755:	78 b4                	js     80070b <vprintfmt+0x1ec>
  800757:	4e                   	dec    %esi
  800758:	79 b1                	jns    80070b <vprintfmt+0x1ec>
  80075a:	8b 75 08             	mov    0x8(%ebp),%esi
  80075d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800760:	eb 14                	jmp    800776 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800762:	83 ec 08             	sub    $0x8,%esp
  800765:	53                   	push   %ebx
  800766:	6a 20                	push   $0x20
  800768:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80076a:	4f                   	dec    %edi
  80076b:	83 c4 10             	add    $0x10,%esp
  80076e:	eb 06                	jmp    800776 <vprintfmt+0x257>
  800770:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800773:	8b 75 08             	mov    0x8(%ebp),%esi
  800776:	85 ff                	test   %edi,%edi
  800778:	7f e8                	jg     800762 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80077a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80077d:	e9 c3 fd ff ff       	jmp    800545 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800782:	83 fa 01             	cmp    $0x1,%edx
  800785:	7e 16                	jle    80079d <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800787:	8b 45 14             	mov    0x14(%ebp),%eax
  80078a:	8d 50 08             	lea    0x8(%eax),%edx
  80078d:	89 55 14             	mov    %edx,0x14(%ebp)
  800790:	8b 50 04             	mov    0x4(%eax),%edx
  800793:	8b 00                	mov    (%eax),%eax
  800795:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800798:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80079b:	eb 32                	jmp    8007cf <vprintfmt+0x2b0>
	else if (lflag)
  80079d:	85 d2                	test   %edx,%edx
  80079f:	74 18                	je     8007b9 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007a1:	8b 45 14             	mov    0x14(%ebp),%eax
  8007a4:	8d 50 04             	lea    0x4(%eax),%edx
  8007a7:	89 55 14             	mov    %edx,0x14(%ebp)
  8007aa:	8b 00                	mov    (%eax),%eax
  8007ac:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007af:	89 c1                	mov    %eax,%ecx
  8007b1:	c1 f9 1f             	sar    $0x1f,%ecx
  8007b4:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007b7:	eb 16                	jmp    8007cf <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007b9:	8b 45 14             	mov    0x14(%ebp),%eax
  8007bc:	8d 50 04             	lea    0x4(%eax),%edx
  8007bf:	89 55 14             	mov    %edx,0x14(%ebp)
  8007c2:	8b 00                	mov    (%eax),%eax
  8007c4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007c7:	89 c1                	mov    %eax,%ecx
  8007c9:	c1 f9 1f             	sar    $0x1f,%ecx
  8007cc:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007cf:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007d2:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007d5:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007d9:	79 76                	jns    800851 <vprintfmt+0x332>
				putch('-', putdat);
  8007db:	83 ec 08             	sub    $0x8,%esp
  8007de:	53                   	push   %ebx
  8007df:	6a 2d                	push   $0x2d
  8007e1:	ff d6                	call   *%esi
				num = -(long long) num;
  8007e3:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007e6:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8007e9:	f7 d8                	neg    %eax
  8007eb:	83 d2 00             	adc    $0x0,%edx
  8007ee:	f7 da                	neg    %edx
  8007f0:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8007f3:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8007f8:	eb 5c                	jmp    800856 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8007fa:	8d 45 14             	lea    0x14(%ebp),%eax
  8007fd:	e8 aa fc ff ff       	call   8004ac <getuint>
			base = 10;
  800802:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800807:	eb 4d                	jmp    800856 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800809:	8d 45 14             	lea    0x14(%ebp),%eax
  80080c:	e8 9b fc ff ff       	call   8004ac <getuint>
			base = 8;
  800811:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800816:	eb 3e                	jmp    800856 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800818:	83 ec 08             	sub    $0x8,%esp
  80081b:	53                   	push   %ebx
  80081c:	6a 30                	push   $0x30
  80081e:	ff d6                	call   *%esi
			putch('x', putdat);
  800820:	83 c4 08             	add    $0x8,%esp
  800823:	53                   	push   %ebx
  800824:	6a 78                	push   $0x78
  800826:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800828:	8b 45 14             	mov    0x14(%ebp),%eax
  80082b:	8d 50 04             	lea    0x4(%eax),%edx
  80082e:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800831:	8b 00                	mov    (%eax),%eax
  800833:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800838:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80083b:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800840:	eb 14                	jmp    800856 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800842:	8d 45 14             	lea    0x14(%ebp),%eax
  800845:	e8 62 fc ff ff       	call   8004ac <getuint>
			base = 16;
  80084a:	b9 10 00 00 00       	mov    $0x10,%ecx
  80084f:	eb 05                	jmp    800856 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800851:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800856:	83 ec 0c             	sub    $0xc,%esp
  800859:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80085d:	57                   	push   %edi
  80085e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800861:	51                   	push   %ecx
  800862:	52                   	push   %edx
  800863:	50                   	push   %eax
  800864:	89 da                	mov    %ebx,%edx
  800866:	89 f0                	mov    %esi,%eax
  800868:	e8 92 fb ff ff       	call   8003ff <printnum>
			break;
  80086d:	83 c4 20             	add    $0x20,%esp
  800870:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800873:	e9 cd fc ff ff       	jmp    800545 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800878:	83 ec 08             	sub    $0x8,%esp
  80087b:	53                   	push   %ebx
  80087c:	51                   	push   %ecx
  80087d:	ff d6                	call   *%esi
			break;
  80087f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800882:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800885:	e9 bb fc ff ff       	jmp    800545 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80088a:	83 ec 08             	sub    $0x8,%esp
  80088d:	53                   	push   %ebx
  80088e:	6a 25                	push   $0x25
  800890:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800892:	83 c4 10             	add    $0x10,%esp
  800895:	eb 01                	jmp    800898 <vprintfmt+0x379>
  800897:	4f                   	dec    %edi
  800898:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80089c:	75 f9                	jne    800897 <vprintfmt+0x378>
  80089e:	e9 a2 fc ff ff       	jmp    800545 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008a3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008a6:	5b                   	pop    %ebx
  8008a7:	5e                   	pop    %esi
  8008a8:	5f                   	pop    %edi
  8008a9:	5d                   	pop    %ebp
  8008aa:	c3                   	ret    

008008ab <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008ab:	55                   	push   %ebp
  8008ac:	89 e5                	mov    %esp,%ebp
  8008ae:	83 ec 18             	sub    $0x18,%esp
  8008b1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b4:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008b7:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008ba:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008be:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008c1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008c8:	85 c0                	test   %eax,%eax
  8008ca:	74 26                	je     8008f2 <vsnprintf+0x47>
  8008cc:	85 d2                	test   %edx,%edx
  8008ce:	7e 29                	jle    8008f9 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008d0:	ff 75 14             	pushl  0x14(%ebp)
  8008d3:	ff 75 10             	pushl  0x10(%ebp)
  8008d6:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008d9:	50                   	push   %eax
  8008da:	68 e6 04 80 00       	push   $0x8004e6
  8008df:	e8 3b fc ff ff       	call   80051f <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8008e4:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8008e7:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8008ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8008ed:	83 c4 10             	add    $0x10,%esp
  8008f0:	eb 0c                	jmp    8008fe <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8008f2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8008f7:	eb 05                	jmp    8008fe <vsnprintf+0x53>
  8008f9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8008fe:	c9                   	leave  
  8008ff:	c3                   	ret    

00800900 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800900:	55                   	push   %ebp
  800901:	89 e5                	mov    %esp,%ebp
  800903:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800906:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800909:	50                   	push   %eax
  80090a:	ff 75 10             	pushl  0x10(%ebp)
  80090d:	ff 75 0c             	pushl  0xc(%ebp)
  800910:	ff 75 08             	pushl  0x8(%ebp)
  800913:	e8 93 ff ff ff       	call   8008ab <vsnprintf>
	va_end(ap);

	return rc;
}
  800918:	c9                   	leave  
  800919:	c3                   	ret    

0080091a <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80091a:	55                   	push   %ebp
  80091b:	89 e5                	mov    %esp,%ebp
  80091d:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800920:	b8 00 00 00 00       	mov    $0x0,%eax
  800925:	eb 01                	jmp    800928 <strlen+0xe>
		n++;
  800927:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800928:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80092c:	75 f9                	jne    800927 <strlen+0xd>
		n++;
	return n;
}
  80092e:	5d                   	pop    %ebp
  80092f:	c3                   	ret    

00800930 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800930:	55                   	push   %ebp
  800931:	89 e5                	mov    %esp,%ebp
  800933:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800936:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800939:	ba 00 00 00 00       	mov    $0x0,%edx
  80093e:	eb 01                	jmp    800941 <strnlen+0x11>
		n++;
  800940:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800941:	39 c2                	cmp    %eax,%edx
  800943:	74 08                	je     80094d <strnlen+0x1d>
  800945:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800949:	75 f5                	jne    800940 <strnlen+0x10>
  80094b:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80094d:	5d                   	pop    %ebp
  80094e:	c3                   	ret    

0080094f <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80094f:	55                   	push   %ebp
  800950:	89 e5                	mov    %esp,%ebp
  800952:	53                   	push   %ebx
  800953:	8b 45 08             	mov    0x8(%ebp),%eax
  800956:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800959:	89 c2                	mov    %eax,%edx
  80095b:	42                   	inc    %edx
  80095c:	41                   	inc    %ecx
  80095d:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800960:	88 5a ff             	mov    %bl,-0x1(%edx)
  800963:	84 db                	test   %bl,%bl
  800965:	75 f4                	jne    80095b <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800967:	5b                   	pop    %ebx
  800968:	5d                   	pop    %ebp
  800969:	c3                   	ret    

0080096a <strcat>:

char *
strcat(char *dst, const char *src)
{
  80096a:	55                   	push   %ebp
  80096b:	89 e5                	mov    %esp,%ebp
  80096d:	53                   	push   %ebx
  80096e:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800971:	53                   	push   %ebx
  800972:	e8 a3 ff ff ff       	call   80091a <strlen>
  800977:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80097a:	ff 75 0c             	pushl  0xc(%ebp)
  80097d:	01 d8                	add    %ebx,%eax
  80097f:	50                   	push   %eax
  800980:	e8 ca ff ff ff       	call   80094f <strcpy>
	return dst;
}
  800985:	89 d8                	mov    %ebx,%eax
  800987:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80098a:	c9                   	leave  
  80098b:	c3                   	ret    

0080098c <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80098c:	55                   	push   %ebp
  80098d:	89 e5                	mov    %esp,%ebp
  80098f:	56                   	push   %esi
  800990:	53                   	push   %ebx
  800991:	8b 75 08             	mov    0x8(%ebp),%esi
  800994:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800997:	89 f3                	mov    %esi,%ebx
  800999:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80099c:	89 f2                	mov    %esi,%edx
  80099e:	eb 0c                	jmp    8009ac <strncpy+0x20>
		*dst++ = *src;
  8009a0:	42                   	inc    %edx
  8009a1:	8a 01                	mov    (%ecx),%al
  8009a3:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009a6:	80 39 01             	cmpb   $0x1,(%ecx)
  8009a9:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ac:	39 da                	cmp    %ebx,%edx
  8009ae:	75 f0                	jne    8009a0 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009b0:	89 f0                	mov    %esi,%eax
  8009b2:	5b                   	pop    %ebx
  8009b3:	5e                   	pop    %esi
  8009b4:	5d                   	pop    %ebp
  8009b5:	c3                   	ret    

008009b6 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009b6:	55                   	push   %ebp
  8009b7:	89 e5                	mov    %esp,%ebp
  8009b9:	56                   	push   %esi
  8009ba:	53                   	push   %ebx
  8009bb:	8b 75 08             	mov    0x8(%ebp),%esi
  8009be:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009c1:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009c4:	85 c0                	test   %eax,%eax
  8009c6:	74 1e                	je     8009e6 <strlcpy+0x30>
  8009c8:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009cc:	89 f2                	mov    %esi,%edx
  8009ce:	eb 05                	jmp    8009d5 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009d0:	42                   	inc    %edx
  8009d1:	41                   	inc    %ecx
  8009d2:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009d5:	39 c2                	cmp    %eax,%edx
  8009d7:	74 08                	je     8009e1 <strlcpy+0x2b>
  8009d9:	8a 19                	mov    (%ecx),%bl
  8009db:	84 db                	test   %bl,%bl
  8009dd:	75 f1                	jne    8009d0 <strlcpy+0x1a>
  8009df:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8009e1:	c6 00 00             	movb   $0x0,(%eax)
  8009e4:	eb 02                	jmp    8009e8 <strlcpy+0x32>
  8009e6:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8009e8:	29 f0                	sub    %esi,%eax
}
  8009ea:	5b                   	pop    %ebx
  8009eb:	5e                   	pop    %esi
  8009ec:	5d                   	pop    %ebp
  8009ed:	c3                   	ret    

008009ee <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8009ee:	55                   	push   %ebp
  8009ef:	89 e5                	mov    %esp,%ebp
  8009f1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8009f4:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8009f7:	eb 02                	jmp    8009fb <strcmp+0xd>
		p++, q++;
  8009f9:	41                   	inc    %ecx
  8009fa:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8009fb:	8a 01                	mov    (%ecx),%al
  8009fd:	84 c0                	test   %al,%al
  8009ff:	74 04                	je     800a05 <strcmp+0x17>
  800a01:	3a 02                	cmp    (%edx),%al
  800a03:	74 f4                	je     8009f9 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a05:	0f b6 c0             	movzbl %al,%eax
  800a08:	0f b6 12             	movzbl (%edx),%edx
  800a0b:	29 d0                	sub    %edx,%eax
}
  800a0d:	5d                   	pop    %ebp
  800a0e:	c3                   	ret    

00800a0f <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a0f:	55                   	push   %ebp
  800a10:	89 e5                	mov    %esp,%ebp
  800a12:	53                   	push   %ebx
  800a13:	8b 45 08             	mov    0x8(%ebp),%eax
  800a16:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a19:	89 c3                	mov    %eax,%ebx
  800a1b:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a1e:	eb 02                	jmp    800a22 <strncmp+0x13>
		n--, p++, q++;
  800a20:	40                   	inc    %eax
  800a21:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a22:	39 d8                	cmp    %ebx,%eax
  800a24:	74 14                	je     800a3a <strncmp+0x2b>
  800a26:	8a 08                	mov    (%eax),%cl
  800a28:	84 c9                	test   %cl,%cl
  800a2a:	74 04                	je     800a30 <strncmp+0x21>
  800a2c:	3a 0a                	cmp    (%edx),%cl
  800a2e:	74 f0                	je     800a20 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a30:	0f b6 00             	movzbl (%eax),%eax
  800a33:	0f b6 12             	movzbl (%edx),%edx
  800a36:	29 d0                	sub    %edx,%eax
  800a38:	eb 05                	jmp    800a3f <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a3a:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a3f:	5b                   	pop    %ebx
  800a40:	5d                   	pop    %ebp
  800a41:	c3                   	ret    

00800a42 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a42:	55                   	push   %ebp
  800a43:	89 e5                	mov    %esp,%ebp
  800a45:	8b 45 08             	mov    0x8(%ebp),%eax
  800a48:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a4b:	eb 05                	jmp    800a52 <strchr+0x10>
		if (*s == c)
  800a4d:	38 ca                	cmp    %cl,%dl
  800a4f:	74 0c                	je     800a5d <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a51:	40                   	inc    %eax
  800a52:	8a 10                	mov    (%eax),%dl
  800a54:	84 d2                	test   %dl,%dl
  800a56:	75 f5                	jne    800a4d <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a58:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a5d:	5d                   	pop    %ebp
  800a5e:	c3                   	ret    

00800a5f <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a5f:	55                   	push   %ebp
  800a60:	89 e5                	mov    %esp,%ebp
  800a62:	8b 45 08             	mov    0x8(%ebp),%eax
  800a65:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a68:	eb 05                	jmp    800a6f <strfind+0x10>
		if (*s == c)
  800a6a:	38 ca                	cmp    %cl,%dl
  800a6c:	74 07                	je     800a75 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a6e:	40                   	inc    %eax
  800a6f:	8a 10                	mov    (%eax),%dl
  800a71:	84 d2                	test   %dl,%dl
  800a73:	75 f5                	jne    800a6a <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a75:	5d                   	pop    %ebp
  800a76:	c3                   	ret    

00800a77 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a77:	55                   	push   %ebp
  800a78:	89 e5                	mov    %esp,%ebp
  800a7a:	57                   	push   %edi
  800a7b:	56                   	push   %esi
  800a7c:	53                   	push   %ebx
  800a7d:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a80:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a83:	85 c9                	test   %ecx,%ecx
  800a85:	74 36                	je     800abd <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800a87:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800a8d:	75 28                	jne    800ab7 <memset+0x40>
  800a8f:	f6 c1 03             	test   $0x3,%cl
  800a92:	75 23                	jne    800ab7 <memset+0x40>
		c &= 0xFF;
  800a94:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800a98:	89 d3                	mov    %edx,%ebx
  800a9a:	c1 e3 08             	shl    $0x8,%ebx
  800a9d:	89 d6                	mov    %edx,%esi
  800a9f:	c1 e6 18             	shl    $0x18,%esi
  800aa2:	89 d0                	mov    %edx,%eax
  800aa4:	c1 e0 10             	shl    $0x10,%eax
  800aa7:	09 f0                	or     %esi,%eax
  800aa9:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800aab:	89 d8                	mov    %ebx,%eax
  800aad:	09 d0                	or     %edx,%eax
  800aaf:	c1 e9 02             	shr    $0x2,%ecx
  800ab2:	fc                   	cld    
  800ab3:	f3 ab                	rep stos %eax,%es:(%edi)
  800ab5:	eb 06                	jmp    800abd <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ab7:	8b 45 0c             	mov    0xc(%ebp),%eax
  800aba:	fc                   	cld    
  800abb:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800abd:	89 f8                	mov    %edi,%eax
  800abf:	5b                   	pop    %ebx
  800ac0:	5e                   	pop    %esi
  800ac1:	5f                   	pop    %edi
  800ac2:	5d                   	pop    %ebp
  800ac3:	c3                   	ret    

00800ac4 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ac4:	55                   	push   %ebp
  800ac5:	89 e5                	mov    %esp,%ebp
  800ac7:	57                   	push   %edi
  800ac8:	56                   	push   %esi
  800ac9:	8b 45 08             	mov    0x8(%ebp),%eax
  800acc:	8b 75 0c             	mov    0xc(%ebp),%esi
  800acf:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ad2:	39 c6                	cmp    %eax,%esi
  800ad4:	73 33                	jae    800b09 <memmove+0x45>
  800ad6:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800ad9:	39 d0                	cmp    %edx,%eax
  800adb:	73 2c                	jae    800b09 <memmove+0x45>
		s += n;
		d += n;
  800add:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800ae0:	89 d6                	mov    %edx,%esi
  800ae2:	09 fe                	or     %edi,%esi
  800ae4:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800aea:	75 13                	jne    800aff <memmove+0x3b>
  800aec:	f6 c1 03             	test   $0x3,%cl
  800aef:	75 0e                	jne    800aff <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800af1:	83 ef 04             	sub    $0x4,%edi
  800af4:	8d 72 fc             	lea    -0x4(%edx),%esi
  800af7:	c1 e9 02             	shr    $0x2,%ecx
  800afa:	fd                   	std    
  800afb:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800afd:	eb 07                	jmp    800b06 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800aff:	4f                   	dec    %edi
  800b00:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b03:	fd                   	std    
  800b04:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b06:	fc                   	cld    
  800b07:	eb 1d                	jmp    800b26 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b09:	89 f2                	mov    %esi,%edx
  800b0b:	09 c2                	or     %eax,%edx
  800b0d:	f6 c2 03             	test   $0x3,%dl
  800b10:	75 0f                	jne    800b21 <memmove+0x5d>
  800b12:	f6 c1 03             	test   $0x3,%cl
  800b15:	75 0a                	jne    800b21 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b17:	c1 e9 02             	shr    $0x2,%ecx
  800b1a:	89 c7                	mov    %eax,%edi
  800b1c:	fc                   	cld    
  800b1d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b1f:	eb 05                	jmp    800b26 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b21:	89 c7                	mov    %eax,%edi
  800b23:	fc                   	cld    
  800b24:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b26:	5e                   	pop    %esi
  800b27:	5f                   	pop    %edi
  800b28:	5d                   	pop    %ebp
  800b29:	c3                   	ret    

00800b2a <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b2a:	55                   	push   %ebp
  800b2b:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b2d:	ff 75 10             	pushl  0x10(%ebp)
  800b30:	ff 75 0c             	pushl  0xc(%ebp)
  800b33:	ff 75 08             	pushl  0x8(%ebp)
  800b36:	e8 89 ff ff ff       	call   800ac4 <memmove>
}
  800b3b:	c9                   	leave  
  800b3c:	c3                   	ret    

00800b3d <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b3d:	55                   	push   %ebp
  800b3e:	89 e5                	mov    %esp,%ebp
  800b40:	56                   	push   %esi
  800b41:	53                   	push   %ebx
  800b42:	8b 45 08             	mov    0x8(%ebp),%eax
  800b45:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b48:	89 c6                	mov    %eax,%esi
  800b4a:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b4d:	eb 14                	jmp    800b63 <memcmp+0x26>
		if (*s1 != *s2)
  800b4f:	8a 08                	mov    (%eax),%cl
  800b51:	8a 1a                	mov    (%edx),%bl
  800b53:	38 d9                	cmp    %bl,%cl
  800b55:	74 0a                	je     800b61 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b57:	0f b6 c1             	movzbl %cl,%eax
  800b5a:	0f b6 db             	movzbl %bl,%ebx
  800b5d:	29 d8                	sub    %ebx,%eax
  800b5f:	eb 0b                	jmp    800b6c <memcmp+0x2f>
		s1++, s2++;
  800b61:	40                   	inc    %eax
  800b62:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b63:	39 f0                	cmp    %esi,%eax
  800b65:	75 e8                	jne    800b4f <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b67:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b6c:	5b                   	pop    %ebx
  800b6d:	5e                   	pop    %esi
  800b6e:	5d                   	pop    %ebp
  800b6f:	c3                   	ret    

00800b70 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b70:	55                   	push   %ebp
  800b71:	89 e5                	mov    %esp,%ebp
  800b73:	53                   	push   %ebx
  800b74:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b77:	89 c1                	mov    %eax,%ecx
  800b79:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b7c:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b80:	eb 08                	jmp    800b8a <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b82:	0f b6 10             	movzbl (%eax),%edx
  800b85:	39 da                	cmp    %ebx,%edx
  800b87:	74 05                	je     800b8e <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b89:	40                   	inc    %eax
  800b8a:	39 c8                	cmp    %ecx,%eax
  800b8c:	72 f4                	jb     800b82 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800b8e:	5b                   	pop    %ebx
  800b8f:	5d                   	pop    %ebp
  800b90:	c3                   	ret    

00800b91 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800b91:	55                   	push   %ebp
  800b92:	89 e5                	mov    %esp,%ebp
  800b94:	57                   	push   %edi
  800b95:	56                   	push   %esi
  800b96:	53                   	push   %ebx
  800b97:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800b9a:	eb 01                	jmp    800b9d <strtol+0xc>
		s++;
  800b9c:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800b9d:	8a 01                	mov    (%ecx),%al
  800b9f:	3c 20                	cmp    $0x20,%al
  800ba1:	74 f9                	je     800b9c <strtol+0xb>
  800ba3:	3c 09                	cmp    $0x9,%al
  800ba5:	74 f5                	je     800b9c <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800ba7:	3c 2b                	cmp    $0x2b,%al
  800ba9:	75 08                	jne    800bb3 <strtol+0x22>
		s++;
  800bab:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bac:	bf 00 00 00 00       	mov    $0x0,%edi
  800bb1:	eb 11                	jmp    800bc4 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bb3:	3c 2d                	cmp    $0x2d,%al
  800bb5:	75 08                	jne    800bbf <strtol+0x2e>
		s++, neg = 1;
  800bb7:	41                   	inc    %ecx
  800bb8:	bf 01 00 00 00       	mov    $0x1,%edi
  800bbd:	eb 05                	jmp    800bc4 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bbf:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bc4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bc8:	0f 84 87 00 00 00    	je     800c55 <strtol+0xc4>
  800bce:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bd2:	75 27                	jne    800bfb <strtol+0x6a>
  800bd4:	80 39 30             	cmpb   $0x30,(%ecx)
  800bd7:	75 22                	jne    800bfb <strtol+0x6a>
  800bd9:	e9 88 00 00 00       	jmp    800c66 <strtol+0xd5>
		s += 2, base = 16;
  800bde:	83 c1 02             	add    $0x2,%ecx
  800be1:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800be8:	eb 11                	jmp    800bfb <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800bea:	41                   	inc    %ecx
  800beb:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800bf2:	eb 07                	jmp    800bfb <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800bf4:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800bfb:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c00:	8a 11                	mov    (%ecx),%dl
  800c02:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c05:	80 fb 09             	cmp    $0x9,%bl
  800c08:	77 08                	ja     800c12 <strtol+0x81>
			dig = *s - '0';
  800c0a:	0f be d2             	movsbl %dl,%edx
  800c0d:	83 ea 30             	sub    $0x30,%edx
  800c10:	eb 22                	jmp    800c34 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c12:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c15:	89 f3                	mov    %esi,%ebx
  800c17:	80 fb 19             	cmp    $0x19,%bl
  800c1a:	77 08                	ja     800c24 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c1c:	0f be d2             	movsbl %dl,%edx
  800c1f:	83 ea 57             	sub    $0x57,%edx
  800c22:	eb 10                	jmp    800c34 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c24:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c27:	89 f3                	mov    %esi,%ebx
  800c29:	80 fb 19             	cmp    $0x19,%bl
  800c2c:	77 14                	ja     800c42 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c2e:	0f be d2             	movsbl %dl,%edx
  800c31:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c34:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c37:	7d 09                	jge    800c42 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c39:	41                   	inc    %ecx
  800c3a:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c3e:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c40:	eb be                	jmp    800c00 <strtol+0x6f>

	if (endptr)
  800c42:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c46:	74 05                	je     800c4d <strtol+0xbc>
		*endptr = (char *) s;
  800c48:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c4b:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c4d:	85 ff                	test   %edi,%edi
  800c4f:	74 21                	je     800c72 <strtol+0xe1>
  800c51:	f7 d8                	neg    %eax
  800c53:	eb 1d                	jmp    800c72 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c55:	80 39 30             	cmpb   $0x30,(%ecx)
  800c58:	75 9a                	jne    800bf4 <strtol+0x63>
  800c5a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c5e:	0f 84 7a ff ff ff    	je     800bde <strtol+0x4d>
  800c64:	eb 84                	jmp    800bea <strtol+0x59>
  800c66:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c6a:	0f 84 6e ff ff ff    	je     800bde <strtol+0x4d>
  800c70:	eb 89                	jmp    800bfb <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c72:	5b                   	pop    %ebx
  800c73:	5e                   	pop    %esi
  800c74:	5f                   	pop    %edi
  800c75:	5d                   	pop    %ebp
  800c76:	c3                   	ret    

00800c77 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800c77:	55                   	push   %ebp
  800c78:	89 e5                	mov    %esp,%ebp
  800c7a:	57                   	push   %edi
  800c7b:	56                   	push   %esi
  800c7c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c7d:	b8 00 00 00 00       	mov    $0x0,%eax
  800c82:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c85:	8b 55 08             	mov    0x8(%ebp),%edx
  800c88:	89 c3                	mov    %eax,%ebx
  800c8a:	89 c7                	mov    %eax,%edi
  800c8c:	89 c6                	mov    %eax,%esi
  800c8e:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800c90:	5b                   	pop    %ebx
  800c91:	5e                   	pop    %esi
  800c92:	5f                   	pop    %edi
  800c93:	5d                   	pop    %ebp
  800c94:	c3                   	ret    

00800c95 <sys_cgetc>:

int
sys_cgetc(void)
{
  800c95:	55                   	push   %ebp
  800c96:	89 e5                	mov    %esp,%ebp
  800c98:	57                   	push   %edi
  800c99:	56                   	push   %esi
  800c9a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c9b:	ba 00 00 00 00       	mov    $0x0,%edx
  800ca0:	b8 01 00 00 00       	mov    $0x1,%eax
  800ca5:	89 d1                	mov    %edx,%ecx
  800ca7:	89 d3                	mov    %edx,%ebx
  800ca9:	89 d7                	mov    %edx,%edi
  800cab:	89 d6                	mov    %edx,%esi
  800cad:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800caf:	5b                   	pop    %ebx
  800cb0:	5e                   	pop    %esi
  800cb1:	5f                   	pop    %edi
  800cb2:	5d                   	pop    %ebp
  800cb3:	c3                   	ret    

00800cb4 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800cb4:	55                   	push   %ebp
  800cb5:	89 e5                	mov    %esp,%ebp
  800cb7:	57                   	push   %edi
  800cb8:	56                   	push   %esi
  800cb9:	53                   	push   %ebx
  800cba:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cbd:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cc2:	b8 03 00 00 00       	mov    $0x3,%eax
  800cc7:	8b 55 08             	mov    0x8(%ebp),%edx
  800cca:	89 cb                	mov    %ecx,%ebx
  800ccc:	89 cf                	mov    %ecx,%edi
  800cce:	89 ce                	mov    %ecx,%esi
  800cd0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cd2:	85 c0                	test   %eax,%eax
  800cd4:	7e 17                	jle    800ced <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cd6:	83 ec 0c             	sub    $0xc,%esp
  800cd9:	50                   	push   %eax
  800cda:	6a 03                	push   $0x3
  800cdc:	68 bf 27 80 00       	push   $0x8027bf
  800ce1:	6a 23                	push   $0x23
  800ce3:	68 dc 27 80 00       	push   $0x8027dc
  800ce8:	e8 26 f6 ff ff       	call   800313 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800ced:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cf0:	5b                   	pop    %ebx
  800cf1:	5e                   	pop    %esi
  800cf2:	5f                   	pop    %edi
  800cf3:	5d                   	pop    %ebp
  800cf4:	c3                   	ret    

00800cf5 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800cf5:	55                   	push   %ebp
  800cf6:	89 e5                	mov    %esp,%ebp
  800cf8:	57                   	push   %edi
  800cf9:	56                   	push   %esi
  800cfa:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cfb:	ba 00 00 00 00       	mov    $0x0,%edx
  800d00:	b8 02 00 00 00       	mov    $0x2,%eax
  800d05:	89 d1                	mov    %edx,%ecx
  800d07:	89 d3                	mov    %edx,%ebx
  800d09:	89 d7                	mov    %edx,%edi
  800d0b:	89 d6                	mov    %edx,%esi
  800d0d:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800d0f:	5b                   	pop    %ebx
  800d10:	5e                   	pop    %esi
  800d11:	5f                   	pop    %edi
  800d12:	5d                   	pop    %ebp
  800d13:	c3                   	ret    

00800d14 <sys_yield>:

void
sys_yield(void)
{
  800d14:	55                   	push   %ebp
  800d15:	89 e5                	mov    %esp,%ebp
  800d17:	57                   	push   %edi
  800d18:	56                   	push   %esi
  800d19:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d1a:	ba 00 00 00 00       	mov    $0x0,%edx
  800d1f:	b8 0b 00 00 00       	mov    $0xb,%eax
  800d24:	89 d1                	mov    %edx,%ecx
  800d26:	89 d3                	mov    %edx,%ebx
  800d28:	89 d7                	mov    %edx,%edi
  800d2a:	89 d6                	mov    %edx,%esi
  800d2c:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800d2e:	5b                   	pop    %ebx
  800d2f:	5e                   	pop    %esi
  800d30:	5f                   	pop    %edi
  800d31:	5d                   	pop    %ebp
  800d32:	c3                   	ret    

00800d33 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800d33:	55                   	push   %ebp
  800d34:	89 e5                	mov    %esp,%ebp
  800d36:	57                   	push   %edi
  800d37:	56                   	push   %esi
  800d38:	53                   	push   %ebx
  800d39:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d3c:	be 00 00 00 00       	mov    $0x0,%esi
  800d41:	b8 04 00 00 00       	mov    $0x4,%eax
  800d46:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d49:	8b 55 08             	mov    0x8(%ebp),%edx
  800d4c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d4f:	89 f7                	mov    %esi,%edi
  800d51:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d53:	85 c0                	test   %eax,%eax
  800d55:	7e 17                	jle    800d6e <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d57:	83 ec 0c             	sub    $0xc,%esp
  800d5a:	50                   	push   %eax
  800d5b:	6a 04                	push   $0x4
  800d5d:	68 bf 27 80 00       	push   $0x8027bf
  800d62:	6a 23                	push   $0x23
  800d64:	68 dc 27 80 00       	push   $0x8027dc
  800d69:	e8 a5 f5 ff ff       	call   800313 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800d6e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d71:	5b                   	pop    %ebx
  800d72:	5e                   	pop    %esi
  800d73:	5f                   	pop    %edi
  800d74:	5d                   	pop    %ebp
  800d75:	c3                   	ret    

00800d76 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800d76:	55                   	push   %ebp
  800d77:	89 e5                	mov    %esp,%ebp
  800d79:	57                   	push   %edi
  800d7a:	56                   	push   %esi
  800d7b:	53                   	push   %ebx
  800d7c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d7f:	b8 05 00 00 00       	mov    $0x5,%eax
  800d84:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d87:	8b 55 08             	mov    0x8(%ebp),%edx
  800d8a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d8d:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d90:	8b 75 18             	mov    0x18(%ebp),%esi
  800d93:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d95:	85 c0                	test   %eax,%eax
  800d97:	7e 17                	jle    800db0 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d99:	83 ec 0c             	sub    $0xc,%esp
  800d9c:	50                   	push   %eax
  800d9d:	6a 05                	push   $0x5
  800d9f:	68 bf 27 80 00       	push   $0x8027bf
  800da4:	6a 23                	push   $0x23
  800da6:	68 dc 27 80 00       	push   $0x8027dc
  800dab:	e8 63 f5 ff ff       	call   800313 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800db0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800db3:	5b                   	pop    %ebx
  800db4:	5e                   	pop    %esi
  800db5:	5f                   	pop    %edi
  800db6:	5d                   	pop    %ebp
  800db7:	c3                   	ret    

00800db8 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800db8:	55                   	push   %ebp
  800db9:	89 e5                	mov    %esp,%ebp
  800dbb:	57                   	push   %edi
  800dbc:	56                   	push   %esi
  800dbd:	53                   	push   %ebx
  800dbe:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dc1:	bb 00 00 00 00       	mov    $0x0,%ebx
  800dc6:	b8 06 00 00 00       	mov    $0x6,%eax
  800dcb:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800dce:	8b 55 08             	mov    0x8(%ebp),%edx
  800dd1:	89 df                	mov    %ebx,%edi
  800dd3:	89 de                	mov    %ebx,%esi
  800dd5:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800dd7:	85 c0                	test   %eax,%eax
  800dd9:	7e 17                	jle    800df2 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ddb:	83 ec 0c             	sub    $0xc,%esp
  800dde:	50                   	push   %eax
  800ddf:	6a 06                	push   $0x6
  800de1:	68 bf 27 80 00       	push   $0x8027bf
  800de6:	6a 23                	push   $0x23
  800de8:	68 dc 27 80 00       	push   $0x8027dc
  800ded:	e8 21 f5 ff ff       	call   800313 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800df2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800df5:	5b                   	pop    %ebx
  800df6:	5e                   	pop    %esi
  800df7:	5f                   	pop    %edi
  800df8:	5d                   	pop    %ebp
  800df9:	c3                   	ret    

00800dfa <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800dfa:	55                   	push   %ebp
  800dfb:	89 e5                	mov    %esp,%ebp
  800dfd:	57                   	push   %edi
  800dfe:	56                   	push   %esi
  800dff:	53                   	push   %ebx
  800e00:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e03:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e08:	b8 08 00 00 00       	mov    $0x8,%eax
  800e0d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e10:	8b 55 08             	mov    0x8(%ebp),%edx
  800e13:	89 df                	mov    %ebx,%edi
  800e15:	89 de                	mov    %ebx,%esi
  800e17:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e19:	85 c0                	test   %eax,%eax
  800e1b:	7e 17                	jle    800e34 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e1d:	83 ec 0c             	sub    $0xc,%esp
  800e20:	50                   	push   %eax
  800e21:	6a 08                	push   $0x8
  800e23:	68 bf 27 80 00       	push   $0x8027bf
  800e28:	6a 23                	push   $0x23
  800e2a:	68 dc 27 80 00       	push   $0x8027dc
  800e2f:	e8 df f4 ff ff       	call   800313 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800e34:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e37:	5b                   	pop    %ebx
  800e38:	5e                   	pop    %esi
  800e39:	5f                   	pop    %edi
  800e3a:	5d                   	pop    %ebp
  800e3b:	c3                   	ret    

00800e3c <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800e3c:	55                   	push   %ebp
  800e3d:	89 e5                	mov    %esp,%ebp
  800e3f:	57                   	push   %edi
  800e40:	56                   	push   %esi
  800e41:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e42:	ba 00 00 00 00       	mov    $0x0,%edx
  800e47:	b8 0c 00 00 00       	mov    $0xc,%eax
  800e4c:	89 d1                	mov    %edx,%ecx
  800e4e:	89 d3                	mov    %edx,%ebx
  800e50:	89 d7                	mov    %edx,%edi
  800e52:	89 d6                	mov    %edx,%esi
  800e54:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800e56:	5b                   	pop    %ebx
  800e57:	5e                   	pop    %esi
  800e58:	5f                   	pop    %edi
  800e59:	5d                   	pop    %ebp
  800e5a:	c3                   	ret    

00800e5b <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800e5b:	55                   	push   %ebp
  800e5c:	89 e5                	mov    %esp,%ebp
  800e5e:	57                   	push   %edi
  800e5f:	56                   	push   %esi
  800e60:	53                   	push   %ebx
  800e61:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e64:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e69:	b8 09 00 00 00       	mov    $0x9,%eax
  800e6e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e71:	8b 55 08             	mov    0x8(%ebp),%edx
  800e74:	89 df                	mov    %ebx,%edi
  800e76:	89 de                	mov    %ebx,%esi
  800e78:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e7a:	85 c0                	test   %eax,%eax
  800e7c:	7e 17                	jle    800e95 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e7e:	83 ec 0c             	sub    $0xc,%esp
  800e81:	50                   	push   %eax
  800e82:	6a 09                	push   $0x9
  800e84:	68 bf 27 80 00       	push   $0x8027bf
  800e89:	6a 23                	push   $0x23
  800e8b:	68 dc 27 80 00       	push   $0x8027dc
  800e90:	e8 7e f4 ff ff       	call   800313 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800e95:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e98:	5b                   	pop    %ebx
  800e99:	5e                   	pop    %esi
  800e9a:	5f                   	pop    %edi
  800e9b:	5d                   	pop    %ebp
  800e9c:	c3                   	ret    

00800e9d <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800e9d:	55                   	push   %ebp
  800e9e:	89 e5                	mov    %esp,%ebp
  800ea0:	57                   	push   %edi
  800ea1:	56                   	push   %esi
  800ea2:	53                   	push   %ebx
  800ea3:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ea6:	bb 00 00 00 00       	mov    $0x0,%ebx
  800eab:	b8 0a 00 00 00       	mov    $0xa,%eax
  800eb0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800eb3:	8b 55 08             	mov    0x8(%ebp),%edx
  800eb6:	89 df                	mov    %ebx,%edi
  800eb8:	89 de                	mov    %ebx,%esi
  800eba:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ebc:	85 c0                	test   %eax,%eax
  800ebe:	7e 17                	jle    800ed7 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ec0:	83 ec 0c             	sub    $0xc,%esp
  800ec3:	50                   	push   %eax
  800ec4:	6a 0a                	push   $0xa
  800ec6:	68 bf 27 80 00       	push   $0x8027bf
  800ecb:	6a 23                	push   $0x23
  800ecd:	68 dc 27 80 00       	push   $0x8027dc
  800ed2:	e8 3c f4 ff ff       	call   800313 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800ed7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800eda:	5b                   	pop    %ebx
  800edb:	5e                   	pop    %esi
  800edc:	5f                   	pop    %edi
  800edd:	5d                   	pop    %ebp
  800ede:	c3                   	ret    

00800edf <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800edf:	55                   	push   %ebp
  800ee0:	89 e5                	mov    %esp,%ebp
  800ee2:	57                   	push   %edi
  800ee3:	56                   	push   %esi
  800ee4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ee5:	be 00 00 00 00       	mov    $0x0,%esi
  800eea:	b8 0d 00 00 00       	mov    $0xd,%eax
  800eef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ef2:	8b 55 08             	mov    0x8(%ebp),%edx
  800ef5:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ef8:	8b 7d 14             	mov    0x14(%ebp),%edi
  800efb:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800efd:	5b                   	pop    %ebx
  800efe:	5e                   	pop    %esi
  800eff:	5f                   	pop    %edi
  800f00:	5d                   	pop    %ebp
  800f01:	c3                   	ret    

00800f02 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800f02:	55                   	push   %ebp
  800f03:	89 e5                	mov    %esp,%ebp
  800f05:	57                   	push   %edi
  800f06:	56                   	push   %esi
  800f07:	53                   	push   %ebx
  800f08:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f0b:	b9 00 00 00 00       	mov    $0x0,%ecx
  800f10:	b8 0e 00 00 00       	mov    $0xe,%eax
  800f15:	8b 55 08             	mov    0x8(%ebp),%edx
  800f18:	89 cb                	mov    %ecx,%ebx
  800f1a:	89 cf                	mov    %ecx,%edi
  800f1c:	89 ce                	mov    %ecx,%esi
  800f1e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f20:	85 c0                	test   %eax,%eax
  800f22:	7e 17                	jle    800f3b <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f24:	83 ec 0c             	sub    $0xc,%esp
  800f27:	50                   	push   %eax
  800f28:	6a 0e                	push   $0xe
  800f2a:	68 bf 27 80 00       	push   $0x8027bf
  800f2f:	6a 23                	push   $0x23
  800f31:	68 dc 27 80 00       	push   $0x8027dc
  800f36:	e8 d8 f3 ff ff       	call   800313 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800f3b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f3e:	5b                   	pop    %ebx
  800f3f:	5e                   	pop    %esi
  800f40:	5f                   	pop    %edi
  800f41:	5d                   	pop    %ebp
  800f42:	c3                   	ret    

00800f43 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800f43:	55                   	push   %ebp
  800f44:	89 e5                	mov    %esp,%ebp
  800f46:	53                   	push   %ebx
  800f47:	83 ec 04             	sub    $0x4,%esp
  800f4a:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800f4d:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800f4f:	89 da                	mov    %ebx,%edx
  800f51:	c1 ea 0c             	shr    $0xc,%edx
  800f54:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800f5b:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800f5f:	74 05                	je     800f66 <pgfault+0x23>
  800f61:	f6 c6 08             	test   $0x8,%dh
  800f64:	75 14                	jne    800f7a <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800f66:	83 ec 04             	sub    $0x4,%esp
  800f69:	68 ec 27 80 00       	push   $0x8027ec
  800f6e:	6a 26                	push   $0x26
  800f70:	68 50 28 80 00       	push   $0x802850
  800f75:	e8 99 f3 ff ff       	call   800313 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800f7a:	83 ec 04             	sub    $0x4,%esp
  800f7d:	6a 07                	push   $0x7
  800f7f:	68 00 f0 7f 00       	push   $0x7ff000
  800f84:	6a 00                	push   $0x0
  800f86:	e8 a8 fd ff ff       	call   800d33 <sys_page_alloc>
  800f8b:	83 c4 10             	add    $0x10,%esp
  800f8e:	85 c0                	test   %eax,%eax
  800f90:	74 14                	je     800fa6 <pgfault+0x63>
        panic("pgfault: no phys mem");
  800f92:	83 ec 04             	sub    $0x4,%esp
  800f95:	68 5b 28 80 00       	push   $0x80285b
  800f9a:	6a 32                	push   $0x32
  800f9c:	68 50 28 80 00       	push   $0x802850
  800fa1:	e8 6d f3 ff ff       	call   800313 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800fa6:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800fac:	83 ec 04             	sub    $0x4,%esp
  800faf:	68 00 10 00 00       	push   $0x1000
  800fb4:	53                   	push   %ebx
  800fb5:	68 00 f0 7f 00       	push   $0x7ff000
  800fba:	e8 05 fb ff ff       	call   800ac4 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800fbf:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800fc6:	53                   	push   %ebx
  800fc7:	6a 00                	push   $0x0
  800fc9:	68 00 f0 7f 00       	push   $0x7ff000
  800fce:	6a 00                	push   $0x0
  800fd0:	e8 a1 fd ff ff       	call   800d76 <sys_page_map>
  800fd5:	83 c4 20             	add    $0x20,%esp
  800fd8:	85 c0                	test   %eax,%eax
  800fda:	74 14                	je     800ff0 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800fdc:	83 ec 04             	sub    $0x4,%esp
  800fdf:	68 70 28 80 00       	push   $0x802870
  800fe4:	6a 3f                	push   $0x3f
  800fe6:	68 50 28 80 00       	push   $0x802850
  800feb:	e8 23 f3 ff ff       	call   800313 <_panic>


	//panic("pgfault not implemented");
}
  800ff0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ff3:	c9                   	leave  
  800ff4:	c3                   	ret    

00800ff5 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800ff5:	55                   	push   %ebp
  800ff6:	89 e5                	mov    %esp,%ebp
  800ff8:	57                   	push   %edi
  800ff9:	56                   	push   %esi
  800ffa:	53                   	push   %ebx
  800ffb:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800ffe:	68 43 0f 80 00       	push   $0x800f43
  801003:	e8 09 0f 00 00       	call   801f11 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  801008:	b8 07 00 00 00       	mov    $0x7,%eax
  80100d:	cd 30                	int    $0x30
  80100f:	89 45 dc             	mov    %eax,-0x24(%ebp)
  801012:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  801015:	83 c4 10             	add    $0x10,%esp
  801018:	85 c0                	test   %eax,%eax
  80101a:	79 17                	jns    801033 <fork+0x3e>
        panic("fork: cannot create child env");
  80101c:	83 ec 04             	sub    $0x4,%esp
  80101f:	68 83 28 80 00       	push   $0x802883
  801024:	68 97 00 00 00       	push   $0x97
  801029:	68 50 28 80 00       	push   $0x802850
  80102e:	e8 e0 f2 ff ff       	call   800313 <_panic>
    } else if (envid == 0) {
  801033:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  801037:	75 2a                	jne    801063 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  801039:	e8 b7 fc ff ff       	call   800cf5 <sys_getenvid>
  80103e:	25 ff 03 00 00       	and    $0x3ff,%eax
  801043:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80104a:	c1 e0 07             	shl    $0x7,%eax
  80104d:	29 d0                	sub    %edx,%eax
  80104f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801054:	a3 04 40 80 00       	mov    %eax,0x804004
        return 0;
  801059:	b8 00 00 00 00       	mov    $0x0,%eax
  80105e:	e9 94 01 00 00       	jmp    8011f7 <fork+0x202>
  801063:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  80106a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80106d:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  801074:	a8 01                	test   $0x1,%al
  801076:	0f 84 0a 01 00 00    	je     801186 <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  80107c:	c1 e7 0a             	shl    $0xa,%edi
  80107f:	be 00 00 00 00       	mov    $0x0,%esi
  801084:	89 fb                	mov    %edi,%ebx
  801086:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  801088:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  80108e:	75 34                	jne    8010c4 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  801090:	83 ec 04             	sub    $0x4,%esp
  801093:	6a 07                	push   $0x7
  801095:	68 00 f0 bf ee       	push   $0xeebff000
  80109a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80109d:	e8 91 fc ff ff       	call   800d33 <sys_page_alloc>
  8010a2:	83 c4 10             	add    $0x10,%esp
  8010a5:	85 c0                	test   %eax,%eax
  8010a7:	0f 84 cc 00 00 00    	je     801179 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  8010ad:	83 ec 04             	sub    $0x4,%esp
  8010b0:	68 a1 28 80 00       	push   $0x8028a1
  8010b5:	68 ad 00 00 00       	push   $0xad
  8010ba:	68 50 28 80 00       	push   $0x802850
  8010bf:	e8 4f f2 ff ff       	call   800313 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  8010c4:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  8010cb:	a8 01                	test   $0x1,%al
  8010cd:	0f 84 a6 00 00 00    	je     801179 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  8010d3:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  8010da:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  8010dd:	a9 02 08 00 00       	test   $0x802,%eax
  8010e2:	74 62                	je     801146 <fork+0x151>
  8010e4:	f6 c4 04             	test   $0x4,%ah
  8010e7:	75 78                	jne    801161 <fork+0x16c>
        if (sys_page_map(0,
  8010e9:	83 ec 0c             	sub    $0xc,%esp
  8010ec:	68 05 08 00 00       	push   $0x805
  8010f1:	53                   	push   %ebx
  8010f2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8010f5:	53                   	push   %ebx
  8010f6:	6a 00                	push   $0x0
  8010f8:	e8 79 fc ff ff       	call   800d76 <sys_page_map>
  8010fd:	83 c4 20             	add    $0x20,%esp
  801100:	85 c0                	test   %eax,%eax
  801102:	74 14                	je     801118 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  801104:	83 ec 04             	sub    $0x4,%esp
  801107:	68 bc 28 80 00       	push   $0x8028bc
  80110c:	6a 64                	push   $0x64
  80110e:	68 50 28 80 00       	push   $0x802850
  801113:	e8 fb f1 ff ff       	call   800313 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  801118:	83 ec 0c             	sub    $0xc,%esp
  80111b:	68 05 08 00 00       	push   $0x805
  801120:	53                   	push   %ebx
  801121:	6a 00                	push   $0x0
  801123:	53                   	push   %ebx
  801124:	6a 00                	push   $0x0
  801126:	e8 4b fc ff ff       	call   800d76 <sys_page_map>
  80112b:	83 c4 20             	add    $0x20,%esp
  80112e:	85 c0                	test   %eax,%eax
  801130:	74 47                	je     801179 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  801132:	83 ec 04             	sub    $0x4,%esp
  801135:	68 d3 28 80 00       	push   $0x8028d3
  80113a:	6a 6d                	push   $0x6d
  80113c:	68 50 28 80 00       	push   $0x802850
  801141:	e8 cd f1 ff ff       	call   800313 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  801146:	f6 c4 04             	test   $0x4,%ah
  801149:	75 16                	jne    801161 <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  80114b:	83 ec 0c             	sub    $0xc,%esp
  80114e:	6a 05                	push   $0x5
  801150:	53                   	push   %ebx
  801151:	ff 75 e4             	pushl  -0x1c(%ebp)
  801154:	53                   	push   %ebx
  801155:	6a 00                	push   $0x0
  801157:	e8 1a fc ff ff       	call   800d76 <sys_page_map>
  80115c:	83 c4 20             	add    $0x20,%esp
  80115f:	eb 18                	jmp    801179 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  801161:	83 ec 0c             	sub    $0xc,%esp
  801164:	25 07 0e 00 00       	and    $0xe07,%eax
  801169:	50                   	push   %eax
  80116a:	53                   	push   %ebx
  80116b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80116e:	53                   	push   %ebx
  80116f:	6a 00                	push   $0x0
  801171:	e8 00 fc ff ff       	call   800d76 <sys_page_map>
  801176:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801179:	46                   	inc    %esi
  80117a:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  801180:	0f 85 fe fe ff ff    	jne    801084 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801186:	ff 45 e0             	incl   -0x20(%ebp)
  801189:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80118c:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  801191:	0f 85 d3 fe ff ff    	jne    80106a <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  801197:	a1 04 40 80 00       	mov    0x804004,%eax
  80119c:	8b 40 64             	mov    0x64(%eax),%eax
  80119f:	83 ec 08             	sub    $0x8,%esp
  8011a2:	50                   	push   %eax
  8011a3:	ff 75 dc             	pushl  -0x24(%ebp)
  8011a6:	e8 f2 fc ff ff       	call   800e9d <sys_env_set_pgfault_upcall>
  8011ab:	83 c4 10             	add    $0x10,%esp
  8011ae:	85 c0                	test   %eax,%eax
  8011b0:	74 17                	je     8011c9 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  8011b2:	83 ec 04             	sub    $0x4,%esp
  8011b5:	68 30 28 80 00       	push   $0x802830
  8011ba:	68 b9 00 00 00       	push   $0xb9
  8011bf:	68 50 28 80 00       	push   $0x802850
  8011c4:	e8 4a f1 ff ff       	call   800313 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  8011c9:	83 ec 08             	sub    $0x8,%esp
  8011cc:	6a 02                	push   $0x2
  8011ce:	ff 75 dc             	pushl  -0x24(%ebp)
  8011d1:	e8 24 fc ff ff       	call   800dfa <sys_env_set_status>
  8011d6:	83 c4 10             	add    $0x10,%esp
  8011d9:	85 c0                	test   %eax,%eax
  8011db:	74 17                	je     8011f4 <fork+0x1ff>
        panic("fork: cannot set env status");
  8011dd:	83 ec 04             	sub    $0x4,%esp
  8011e0:	68 ee 28 80 00       	push   $0x8028ee
  8011e5:	68 bd 00 00 00       	push   $0xbd
  8011ea:	68 50 28 80 00       	push   $0x802850
  8011ef:	e8 1f f1 ff ff       	call   800313 <_panic>

    return envid;
  8011f4:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  8011f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8011fa:	5b                   	pop    %ebx
  8011fb:	5e                   	pop    %esi
  8011fc:	5f                   	pop    %edi
  8011fd:	5d                   	pop    %ebp
  8011fe:	c3                   	ret    

008011ff <sfork>:

// Challenge!
int
sfork(void)
{
  8011ff:	55                   	push   %ebp
  801200:	89 e5                	mov    %esp,%ebp
  801202:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  801205:	68 0a 29 80 00       	push   $0x80290a
  80120a:	68 c8 00 00 00       	push   $0xc8
  80120f:	68 50 28 80 00       	push   $0x802850
  801214:	e8 fa f0 ff ff       	call   800313 <_panic>

00801219 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  801219:	55                   	push   %ebp
  80121a:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  80121c:	8b 45 08             	mov    0x8(%ebp),%eax
  80121f:	05 00 00 00 30       	add    $0x30000000,%eax
  801224:	c1 e8 0c             	shr    $0xc,%eax
}
  801227:	5d                   	pop    %ebp
  801228:	c3                   	ret    

00801229 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  801229:	55                   	push   %ebp
  80122a:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  80122c:	8b 45 08             	mov    0x8(%ebp),%eax
  80122f:	05 00 00 00 30       	add    $0x30000000,%eax
  801234:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  801239:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  80123e:	5d                   	pop    %ebp
  80123f:	c3                   	ret    

00801240 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  801240:	55                   	push   %ebp
  801241:	89 e5                	mov    %esp,%ebp
  801243:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801246:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  80124b:	89 c2                	mov    %eax,%edx
  80124d:	c1 ea 16             	shr    $0x16,%edx
  801250:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801257:	f6 c2 01             	test   $0x1,%dl
  80125a:	74 11                	je     80126d <fd_alloc+0x2d>
  80125c:	89 c2                	mov    %eax,%edx
  80125e:	c1 ea 0c             	shr    $0xc,%edx
  801261:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801268:	f6 c2 01             	test   $0x1,%dl
  80126b:	75 09                	jne    801276 <fd_alloc+0x36>
			*fd_store = fd;
  80126d:	89 01                	mov    %eax,(%ecx)
			return 0;
  80126f:	b8 00 00 00 00       	mov    $0x0,%eax
  801274:	eb 17                	jmp    80128d <fd_alloc+0x4d>
  801276:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  80127b:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801280:	75 c9                	jne    80124b <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801282:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801288:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  80128d:	5d                   	pop    %ebp
  80128e:	c3                   	ret    

0080128f <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  80128f:	55                   	push   %ebp
  801290:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  801292:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801296:	77 39                	ja     8012d1 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801298:	8b 45 08             	mov    0x8(%ebp),%eax
  80129b:	c1 e0 0c             	shl    $0xc,%eax
  80129e:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  8012a3:	89 c2                	mov    %eax,%edx
  8012a5:	c1 ea 16             	shr    $0x16,%edx
  8012a8:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8012af:	f6 c2 01             	test   $0x1,%dl
  8012b2:	74 24                	je     8012d8 <fd_lookup+0x49>
  8012b4:	89 c2                	mov    %eax,%edx
  8012b6:	c1 ea 0c             	shr    $0xc,%edx
  8012b9:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8012c0:	f6 c2 01             	test   $0x1,%dl
  8012c3:	74 1a                	je     8012df <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  8012c5:	8b 55 0c             	mov    0xc(%ebp),%edx
  8012c8:	89 02                	mov    %eax,(%edx)
	return 0;
  8012ca:	b8 00 00 00 00       	mov    $0x0,%eax
  8012cf:	eb 13                	jmp    8012e4 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8012d1:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8012d6:	eb 0c                	jmp    8012e4 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8012d8:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8012dd:	eb 05                	jmp    8012e4 <fd_lookup+0x55>
  8012df:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8012e4:	5d                   	pop    %ebp
  8012e5:	c3                   	ret    

008012e6 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8012e6:	55                   	push   %ebp
  8012e7:	89 e5                	mov    %esp,%ebp
  8012e9:	83 ec 08             	sub    $0x8,%esp
  8012ec:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8012ef:	ba 9c 29 80 00       	mov    $0x80299c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  8012f4:	eb 13                	jmp    801309 <dev_lookup+0x23>
  8012f6:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  8012f9:	39 08                	cmp    %ecx,(%eax)
  8012fb:	75 0c                	jne    801309 <dev_lookup+0x23>
			*dev = devtab[i];
  8012fd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801300:	89 01                	mov    %eax,(%ecx)
			return 0;
  801302:	b8 00 00 00 00       	mov    $0x0,%eax
  801307:	eb 2e                	jmp    801337 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801309:	8b 02                	mov    (%edx),%eax
  80130b:	85 c0                	test   %eax,%eax
  80130d:	75 e7                	jne    8012f6 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  80130f:	a1 04 40 80 00       	mov    0x804004,%eax
  801314:	8b 40 48             	mov    0x48(%eax),%eax
  801317:	83 ec 04             	sub    $0x4,%esp
  80131a:	51                   	push   %ecx
  80131b:	50                   	push   %eax
  80131c:	68 20 29 80 00       	push   $0x802920
  801321:	e8 c5 f0 ff ff       	call   8003eb <cprintf>
	*dev = 0;
  801326:	8b 45 0c             	mov    0xc(%ebp),%eax
  801329:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  80132f:	83 c4 10             	add    $0x10,%esp
  801332:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801337:	c9                   	leave  
  801338:	c3                   	ret    

00801339 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  801339:	55                   	push   %ebp
  80133a:	89 e5                	mov    %esp,%ebp
  80133c:	56                   	push   %esi
  80133d:	53                   	push   %ebx
  80133e:	83 ec 10             	sub    $0x10,%esp
  801341:	8b 75 08             	mov    0x8(%ebp),%esi
  801344:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  801347:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80134a:	50                   	push   %eax
  80134b:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  801351:	c1 e8 0c             	shr    $0xc,%eax
  801354:	50                   	push   %eax
  801355:	e8 35 ff ff ff       	call   80128f <fd_lookup>
  80135a:	83 c4 08             	add    $0x8,%esp
  80135d:	85 c0                	test   %eax,%eax
  80135f:	78 05                	js     801366 <fd_close+0x2d>
	    || fd != fd2)
  801361:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  801364:	74 06                	je     80136c <fd_close+0x33>
		return (must_exist ? r : 0);
  801366:	84 db                	test   %bl,%bl
  801368:	74 47                	je     8013b1 <fd_close+0x78>
  80136a:	eb 4a                	jmp    8013b6 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  80136c:	83 ec 08             	sub    $0x8,%esp
  80136f:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801372:	50                   	push   %eax
  801373:	ff 36                	pushl  (%esi)
  801375:	e8 6c ff ff ff       	call   8012e6 <dev_lookup>
  80137a:	89 c3                	mov    %eax,%ebx
  80137c:	83 c4 10             	add    $0x10,%esp
  80137f:	85 c0                	test   %eax,%eax
  801381:	78 1c                	js     80139f <fd_close+0x66>
		if (dev->dev_close)
  801383:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801386:	8b 40 10             	mov    0x10(%eax),%eax
  801389:	85 c0                	test   %eax,%eax
  80138b:	74 0d                	je     80139a <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  80138d:	83 ec 0c             	sub    $0xc,%esp
  801390:	56                   	push   %esi
  801391:	ff d0                	call   *%eax
  801393:	89 c3                	mov    %eax,%ebx
  801395:	83 c4 10             	add    $0x10,%esp
  801398:	eb 05                	jmp    80139f <fd_close+0x66>
		else
			r = 0;
  80139a:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  80139f:	83 ec 08             	sub    $0x8,%esp
  8013a2:	56                   	push   %esi
  8013a3:	6a 00                	push   $0x0
  8013a5:	e8 0e fa ff ff       	call   800db8 <sys_page_unmap>
	return r;
  8013aa:	83 c4 10             	add    $0x10,%esp
  8013ad:	89 d8                	mov    %ebx,%eax
  8013af:	eb 05                	jmp    8013b6 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  8013b1:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  8013b6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8013b9:	5b                   	pop    %ebx
  8013ba:	5e                   	pop    %esi
  8013bb:	5d                   	pop    %ebp
  8013bc:	c3                   	ret    

008013bd <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  8013bd:	55                   	push   %ebp
  8013be:	89 e5                	mov    %esp,%ebp
  8013c0:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8013c3:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013c6:	50                   	push   %eax
  8013c7:	ff 75 08             	pushl  0x8(%ebp)
  8013ca:	e8 c0 fe ff ff       	call   80128f <fd_lookup>
  8013cf:	83 c4 08             	add    $0x8,%esp
  8013d2:	85 c0                	test   %eax,%eax
  8013d4:	78 10                	js     8013e6 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8013d6:	83 ec 08             	sub    $0x8,%esp
  8013d9:	6a 01                	push   $0x1
  8013db:	ff 75 f4             	pushl  -0xc(%ebp)
  8013de:	e8 56 ff ff ff       	call   801339 <fd_close>
  8013e3:	83 c4 10             	add    $0x10,%esp
}
  8013e6:	c9                   	leave  
  8013e7:	c3                   	ret    

008013e8 <close_all>:

void
close_all(void)
{
  8013e8:	55                   	push   %ebp
  8013e9:	89 e5                	mov    %esp,%ebp
  8013eb:	53                   	push   %ebx
  8013ec:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  8013ef:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  8013f4:	83 ec 0c             	sub    $0xc,%esp
  8013f7:	53                   	push   %ebx
  8013f8:	e8 c0 ff ff ff       	call   8013bd <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  8013fd:	43                   	inc    %ebx
  8013fe:	83 c4 10             	add    $0x10,%esp
  801401:	83 fb 20             	cmp    $0x20,%ebx
  801404:	75 ee                	jne    8013f4 <close_all+0xc>
		close(i);
}
  801406:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801409:	c9                   	leave  
  80140a:	c3                   	ret    

0080140b <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  80140b:	55                   	push   %ebp
  80140c:	89 e5                	mov    %esp,%ebp
  80140e:	57                   	push   %edi
  80140f:	56                   	push   %esi
  801410:	53                   	push   %ebx
  801411:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801414:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801417:	50                   	push   %eax
  801418:	ff 75 08             	pushl  0x8(%ebp)
  80141b:	e8 6f fe ff ff       	call   80128f <fd_lookup>
  801420:	83 c4 08             	add    $0x8,%esp
  801423:	85 c0                	test   %eax,%eax
  801425:	0f 88 c2 00 00 00    	js     8014ed <dup+0xe2>
		return r;
	close(newfdnum);
  80142b:	83 ec 0c             	sub    $0xc,%esp
  80142e:	ff 75 0c             	pushl  0xc(%ebp)
  801431:	e8 87 ff ff ff       	call   8013bd <close>

	newfd = INDEX2FD(newfdnum);
  801436:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801439:	c1 e3 0c             	shl    $0xc,%ebx
  80143c:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801442:	83 c4 04             	add    $0x4,%esp
  801445:	ff 75 e4             	pushl  -0x1c(%ebp)
  801448:	e8 dc fd ff ff       	call   801229 <fd2data>
  80144d:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  80144f:	89 1c 24             	mov    %ebx,(%esp)
  801452:	e8 d2 fd ff ff       	call   801229 <fd2data>
  801457:	83 c4 10             	add    $0x10,%esp
  80145a:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  80145c:	89 f0                	mov    %esi,%eax
  80145e:	c1 e8 16             	shr    $0x16,%eax
  801461:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801468:	a8 01                	test   $0x1,%al
  80146a:	74 35                	je     8014a1 <dup+0x96>
  80146c:	89 f0                	mov    %esi,%eax
  80146e:	c1 e8 0c             	shr    $0xc,%eax
  801471:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801478:	f6 c2 01             	test   $0x1,%dl
  80147b:	74 24                	je     8014a1 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  80147d:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801484:	83 ec 0c             	sub    $0xc,%esp
  801487:	25 07 0e 00 00       	and    $0xe07,%eax
  80148c:	50                   	push   %eax
  80148d:	57                   	push   %edi
  80148e:	6a 00                	push   $0x0
  801490:	56                   	push   %esi
  801491:	6a 00                	push   $0x0
  801493:	e8 de f8 ff ff       	call   800d76 <sys_page_map>
  801498:	89 c6                	mov    %eax,%esi
  80149a:	83 c4 20             	add    $0x20,%esp
  80149d:	85 c0                	test   %eax,%eax
  80149f:	78 2c                	js     8014cd <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8014a1:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8014a4:	89 d0                	mov    %edx,%eax
  8014a6:	c1 e8 0c             	shr    $0xc,%eax
  8014a9:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8014b0:	83 ec 0c             	sub    $0xc,%esp
  8014b3:	25 07 0e 00 00       	and    $0xe07,%eax
  8014b8:	50                   	push   %eax
  8014b9:	53                   	push   %ebx
  8014ba:	6a 00                	push   $0x0
  8014bc:	52                   	push   %edx
  8014bd:	6a 00                	push   $0x0
  8014bf:	e8 b2 f8 ff ff       	call   800d76 <sys_page_map>
  8014c4:	89 c6                	mov    %eax,%esi
  8014c6:	83 c4 20             	add    $0x20,%esp
  8014c9:	85 c0                	test   %eax,%eax
  8014cb:	79 1d                	jns    8014ea <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8014cd:	83 ec 08             	sub    $0x8,%esp
  8014d0:	53                   	push   %ebx
  8014d1:	6a 00                	push   $0x0
  8014d3:	e8 e0 f8 ff ff       	call   800db8 <sys_page_unmap>
	sys_page_unmap(0, nva);
  8014d8:	83 c4 08             	add    $0x8,%esp
  8014db:	57                   	push   %edi
  8014dc:	6a 00                	push   $0x0
  8014de:	e8 d5 f8 ff ff       	call   800db8 <sys_page_unmap>
	return r;
  8014e3:	83 c4 10             	add    $0x10,%esp
  8014e6:	89 f0                	mov    %esi,%eax
  8014e8:	eb 03                	jmp    8014ed <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8014ea:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8014ed:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8014f0:	5b                   	pop    %ebx
  8014f1:	5e                   	pop    %esi
  8014f2:	5f                   	pop    %edi
  8014f3:	5d                   	pop    %ebp
  8014f4:	c3                   	ret    

008014f5 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8014f5:	55                   	push   %ebp
  8014f6:	89 e5                	mov    %esp,%ebp
  8014f8:	53                   	push   %ebx
  8014f9:	83 ec 14             	sub    $0x14,%esp
  8014fc:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8014ff:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801502:	50                   	push   %eax
  801503:	53                   	push   %ebx
  801504:	e8 86 fd ff ff       	call   80128f <fd_lookup>
  801509:	83 c4 08             	add    $0x8,%esp
  80150c:	85 c0                	test   %eax,%eax
  80150e:	78 67                	js     801577 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801510:	83 ec 08             	sub    $0x8,%esp
  801513:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801516:	50                   	push   %eax
  801517:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80151a:	ff 30                	pushl  (%eax)
  80151c:	e8 c5 fd ff ff       	call   8012e6 <dev_lookup>
  801521:	83 c4 10             	add    $0x10,%esp
  801524:	85 c0                	test   %eax,%eax
  801526:	78 4f                	js     801577 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801528:	8b 55 f0             	mov    -0x10(%ebp),%edx
  80152b:	8b 42 08             	mov    0x8(%edx),%eax
  80152e:	83 e0 03             	and    $0x3,%eax
  801531:	83 f8 01             	cmp    $0x1,%eax
  801534:	75 21                	jne    801557 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801536:	a1 04 40 80 00       	mov    0x804004,%eax
  80153b:	8b 40 48             	mov    0x48(%eax),%eax
  80153e:	83 ec 04             	sub    $0x4,%esp
  801541:	53                   	push   %ebx
  801542:	50                   	push   %eax
  801543:	68 61 29 80 00       	push   $0x802961
  801548:	e8 9e ee ff ff       	call   8003eb <cprintf>
		return -E_INVAL;
  80154d:	83 c4 10             	add    $0x10,%esp
  801550:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801555:	eb 20                	jmp    801577 <read+0x82>
	}
	if (!dev->dev_read)
  801557:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80155a:	8b 40 08             	mov    0x8(%eax),%eax
  80155d:	85 c0                	test   %eax,%eax
  80155f:	74 11                	je     801572 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801561:	83 ec 04             	sub    $0x4,%esp
  801564:	ff 75 10             	pushl  0x10(%ebp)
  801567:	ff 75 0c             	pushl  0xc(%ebp)
  80156a:	52                   	push   %edx
  80156b:	ff d0                	call   *%eax
  80156d:	83 c4 10             	add    $0x10,%esp
  801570:	eb 05                	jmp    801577 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801572:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801577:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80157a:	c9                   	leave  
  80157b:	c3                   	ret    

0080157c <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  80157c:	55                   	push   %ebp
  80157d:	89 e5                	mov    %esp,%ebp
  80157f:	57                   	push   %edi
  801580:	56                   	push   %esi
  801581:	53                   	push   %ebx
  801582:	83 ec 0c             	sub    $0xc,%esp
  801585:	8b 7d 08             	mov    0x8(%ebp),%edi
  801588:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80158b:	bb 00 00 00 00       	mov    $0x0,%ebx
  801590:	eb 21                	jmp    8015b3 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801592:	83 ec 04             	sub    $0x4,%esp
  801595:	89 f0                	mov    %esi,%eax
  801597:	29 d8                	sub    %ebx,%eax
  801599:	50                   	push   %eax
  80159a:	89 d8                	mov    %ebx,%eax
  80159c:	03 45 0c             	add    0xc(%ebp),%eax
  80159f:	50                   	push   %eax
  8015a0:	57                   	push   %edi
  8015a1:	e8 4f ff ff ff       	call   8014f5 <read>
		if (m < 0)
  8015a6:	83 c4 10             	add    $0x10,%esp
  8015a9:	85 c0                	test   %eax,%eax
  8015ab:	78 10                	js     8015bd <readn+0x41>
			return m;
		if (m == 0)
  8015ad:	85 c0                	test   %eax,%eax
  8015af:	74 0a                	je     8015bb <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8015b1:	01 c3                	add    %eax,%ebx
  8015b3:	39 f3                	cmp    %esi,%ebx
  8015b5:	72 db                	jb     801592 <readn+0x16>
  8015b7:	89 d8                	mov    %ebx,%eax
  8015b9:	eb 02                	jmp    8015bd <readn+0x41>
  8015bb:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  8015bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8015c0:	5b                   	pop    %ebx
  8015c1:	5e                   	pop    %esi
  8015c2:	5f                   	pop    %edi
  8015c3:	5d                   	pop    %ebp
  8015c4:	c3                   	ret    

008015c5 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8015c5:	55                   	push   %ebp
  8015c6:	89 e5                	mov    %esp,%ebp
  8015c8:	53                   	push   %ebx
  8015c9:	83 ec 14             	sub    $0x14,%esp
  8015cc:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8015cf:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8015d2:	50                   	push   %eax
  8015d3:	53                   	push   %ebx
  8015d4:	e8 b6 fc ff ff       	call   80128f <fd_lookup>
  8015d9:	83 c4 08             	add    $0x8,%esp
  8015dc:	85 c0                	test   %eax,%eax
  8015de:	78 62                	js     801642 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8015e0:	83 ec 08             	sub    $0x8,%esp
  8015e3:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8015e6:	50                   	push   %eax
  8015e7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015ea:	ff 30                	pushl  (%eax)
  8015ec:	e8 f5 fc ff ff       	call   8012e6 <dev_lookup>
  8015f1:	83 c4 10             	add    $0x10,%esp
  8015f4:	85 c0                	test   %eax,%eax
  8015f6:	78 4a                	js     801642 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8015f8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015fb:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8015ff:	75 21                	jne    801622 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801601:	a1 04 40 80 00       	mov    0x804004,%eax
  801606:	8b 40 48             	mov    0x48(%eax),%eax
  801609:	83 ec 04             	sub    $0x4,%esp
  80160c:	53                   	push   %ebx
  80160d:	50                   	push   %eax
  80160e:	68 7d 29 80 00       	push   $0x80297d
  801613:	e8 d3 ed ff ff       	call   8003eb <cprintf>
		return -E_INVAL;
  801618:	83 c4 10             	add    $0x10,%esp
  80161b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801620:	eb 20                	jmp    801642 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801622:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801625:	8b 52 0c             	mov    0xc(%edx),%edx
  801628:	85 d2                	test   %edx,%edx
  80162a:	74 11                	je     80163d <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  80162c:	83 ec 04             	sub    $0x4,%esp
  80162f:	ff 75 10             	pushl  0x10(%ebp)
  801632:	ff 75 0c             	pushl  0xc(%ebp)
  801635:	50                   	push   %eax
  801636:	ff d2                	call   *%edx
  801638:	83 c4 10             	add    $0x10,%esp
  80163b:	eb 05                	jmp    801642 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  80163d:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801642:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801645:	c9                   	leave  
  801646:	c3                   	ret    

00801647 <seek>:

int
seek(int fdnum, off_t offset)
{
  801647:	55                   	push   %ebp
  801648:	89 e5                	mov    %esp,%ebp
  80164a:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80164d:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801650:	50                   	push   %eax
  801651:	ff 75 08             	pushl  0x8(%ebp)
  801654:	e8 36 fc ff ff       	call   80128f <fd_lookup>
  801659:	83 c4 08             	add    $0x8,%esp
  80165c:	85 c0                	test   %eax,%eax
  80165e:	78 0e                	js     80166e <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801660:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801663:	8b 55 0c             	mov    0xc(%ebp),%edx
  801666:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801669:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80166e:	c9                   	leave  
  80166f:	c3                   	ret    

00801670 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801670:	55                   	push   %ebp
  801671:	89 e5                	mov    %esp,%ebp
  801673:	53                   	push   %ebx
  801674:	83 ec 14             	sub    $0x14,%esp
  801677:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  80167a:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80167d:	50                   	push   %eax
  80167e:	53                   	push   %ebx
  80167f:	e8 0b fc ff ff       	call   80128f <fd_lookup>
  801684:	83 c4 08             	add    $0x8,%esp
  801687:	85 c0                	test   %eax,%eax
  801689:	78 5f                	js     8016ea <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80168b:	83 ec 08             	sub    $0x8,%esp
  80168e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801691:	50                   	push   %eax
  801692:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801695:	ff 30                	pushl  (%eax)
  801697:	e8 4a fc ff ff       	call   8012e6 <dev_lookup>
  80169c:	83 c4 10             	add    $0x10,%esp
  80169f:	85 c0                	test   %eax,%eax
  8016a1:	78 47                	js     8016ea <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8016a3:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8016a6:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8016aa:	75 21                	jne    8016cd <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  8016ac:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  8016b1:	8b 40 48             	mov    0x48(%eax),%eax
  8016b4:	83 ec 04             	sub    $0x4,%esp
  8016b7:	53                   	push   %ebx
  8016b8:	50                   	push   %eax
  8016b9:	68 40 29 80 00       	push   $0x802940
  8016be:	e8 28 ed ff ff       	call   8003eb <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8016c3:	83 c4 10             	add    $0x10,%esp
  8016c6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8016cb:	eb 1d                	jmp    8016ea <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8016cd:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8016d0:	8b 52 18             	mov    0x18(%edx),%edx
  8016d3:	85 d2                	test   %edx,%edx
  8016d5:	74 0e                	je     8016e5 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8016d7:	83 ec 08             	sub    $0x8,%esp
  8016da:	ff 75 0c             	pushl  0xc(%ebp)
  8016dd:	50                   	push   %eax
  8016de:	ff d2                	call   *%edx
  8016e0:	83 c4 10             	add    $0x10,%esp
  8016e3:	eb 05                	jmp    8016ea <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8016e5:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8016ea:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8016ed:	c9                   	leave  
  8016ee:	c3                   	ret    

008016ef <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8016ef:	55                   	push   %ebp
  8016f0:	89 e5                	mov    %esp,%ebp
  8016f2:	53                   	push   %ebx
  8016f3:	83 ec 14             	sub    $0x14,%esp
  8016f6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8016f9:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8016fc:	50                   	push   %eax
  8016fd:	ff 75 08             	pushl  0x8(%ebp)
  801700:	e8 8a fb ff ff       	call   80128f <fd_lookup>
  801705:	83 c4 08             	add    $0x8,%esp
  801708:	85 c0                	test   %eax,%eax
  80170a:	78 52                	js     80175e <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80170c:	83 ec 08             	sub    $0x8,%esp
  80170f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801712:	50                   	push   %eax
  801713:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801716:	ff 30                	pushl  (%eax)
  801718:	e8 c9 fb ff ff       	call   8012e6 <dev_lookup>
  80171d:	83 c4 10             	add    $0x10,%esp
  801720:	85 c0                	test   %eax,%eax
  801722:	78 3a                	js     80175e <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801724:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801727:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  80172b:	74 2c                	je     801759 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  80172d:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  801730:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801737:	00 00 00 
	stat->st_isdir = 0;
  80173a:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801741:	00 00 00 
	stat->st_dev = dev;
  801744:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  80174a:	83 ec 08             	sub    $0x8,%esp
  80174d:	53                   	push   %ebx
  80174e:	ff 75 f0             	pushl  -0x10(%ebp)
  801751:	ff 50 14             	call   *0x14(%eax)
  801754:	83 c4 10             	add    $0x10,%esp
  801757:	eb 05                	jmp    80175e <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801759:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  80175e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801761:	c9                   	leave  
  801762:	c3                   	ret    

00801763 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801763:	55                   	push   %ebp
  801764:	89 e5                	mov    %esp,%ebp
  801766:	56                   	push   %esi
  801767:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801768:	83 ec 08             	sub    $0x8,%esp
  80176b:	6a 00                	push   $0x0
  80176d:	ff 75 08             	pushl  0x8(%ebp)
  801770:	e8 e7 01 00 00       	call   80195c <open>
  801775:	89 c3                	mov    %eax,%ebx
  801777:	83 c4 10             	add    $0x10,%esp
  80177a:	85 c0                	test   %eax,%eax
  80177c:	78 1d                	js     80179b <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  80177e:	83 ec 08             	sub    $0x8,%esp
  801781:	ff 75 0c             	pushl  0xc(%ebp)
  801784:	50                   	push   %eax
  801785:	e8 65 ff ff ff       	call   8016ef <fstat>
  80178a:	89 c6                	mov    %eax,%esi
	close(fd);
  80178c:	89 1c 24             	mov    %ebx,(%esp)
  80178f:	e8 29 fc ff ff       	call   8013bd <close>
	return r;
  801794:	83 c4 10             	add    $0x10,%esp
  801797:	89 f0                	mov    %esi,%eax
  801799:	eb 00                	jmp    80179b <stat+0x38>
}
  80179b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80179e:	5b                   	pop    %ebx
  80179f:	5e                   	pop    %esi
  8017a0:	5d                   	pop    %ebp
  8017a1:	c3                   	ret    

008017a2 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8017a2:	55                   	push   %ebp
  8017a3:	89 e5                	mov    %esp,%ebp
  8017a5:	56                   	push   %esi
  8017a6:	53                   	push   %ebx
  8017a7:	89 c6                	mov    %eax,%esi
  8017a9:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8017ab:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  8017b2:	75 12                	jne    8017c6 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  8017b4:	83 ec 0c             	sub    $0xc,%esp
  8017b7:	6a 01                	push   $0x1
  8017b9:	e8 8f 08 00 00       	call   80204d <ipc_find_env>
  8017be:	a3 00 40 80 00       	mov    %eax,0x804000
  8017c3:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8017c6:	6a 07                	push   $0x7
  8017c8:	68 00 50 80 00       	push   $0x805000
  8017cd:	56                   	push   %esi
  8017ce:	ff 35 00 40 80 00    	pushl  0x804000
  8017d4:	e8 1f 08 00 00       	call   801ff8 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8017d9:	83 c4 0c             	add    $0xc,%esp
  8017dc:	6a 00                	push   $0x0
  8017de:	53                   	push   %ebx
  8017df:	6a 00                	push   $0x0
  8017e1:	e8 aa 07 00 00       	call   801f90 <ipc_recv>
}
  8017e6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8017e9:	5b                   	pop    %ebx
  8017ea:	5e                   	pop    %esi
  8017eb:	5d                   	pop    %ebp
  8017ec:	c3                   	ret    

008017ed <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8017ed:	55                   	push   %ebp
  8017ee:	89 e5                	mov    %esp,%ebp
  8017f0:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8017f3:	8b 45 08             	mov    0x8(%ebp),%eax
  8017f6:	8b 40 0c             	mov    0xc(%eax),%eax
  8017f9:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  8017fe:	8b 45 0c             	mov    0xc(%ebp),%eax
  801801:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801806:	ba 00 00 00 00       	mov    $0x0,%edx
  80180b:	b8 02 00 00 00       	mov    $0x2,%eax
  801810:	e8 8d ff ff ff       	call   8017a2 <fsipc>
}
  801815:	c9                   	leave  
  801816:	c3                   	ret    

00801817 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  801817:	55                   	push   %ebp
  801818:	89 e5                	mov    %esp,%ebp
  80181a:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  80181d:	8b 45 08             	mov    0x8(%ebp),%eax
  801820:	8b 40 0c             	mov    0xc(%eax),%eax
  801823:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  801828:	ba 00 00 00 00       	mov    $0x0,%edx
  80182d:	b8 06 00 00 00       	mov    $0x6,%eax
  801832:	e8 6b ff ff ff       	call   8017a2 <fsipc>
}
  801837:	c9                   	leave  
  801838:	c3                   	ret    

00801839 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801839:	55                   	push   %ebp
  80183a:	89 e5                	mov    %esp,%ebp
  80183c:	53                   	push   %ebx
  80183d:	83 ec 04             	sub    $0x4,%esp
  801840:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801843:	8b 45 08             	mov    0x8(%ebp),%eax
  801846:	8b 40 0c             	mov    0xc(%eax),%eax
  801849:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  80184e:	ba 00 00 00 00       	mov    $0x0,%edx
  801853:	b8 05 00 00 00       	mov    $0x5,%eax
  801858:	e8 45 ff ff ff       	call   8017a2 <fsipc>
  80185d:	85 c0                	test   %eax,%eax
  80185f:	78 2c                	js     80188d <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801861:	83 ec 08             	sub    $0x8,%esp
  801864:	68 00 50 80 00       	push   $0x805000
  801869:	53                   	push   %ebx
  80186a:	e8 e0 f0 ff ff       	call   80094f <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  80186f:	a1 80 50 80 00       	mov    0x805080,%eax
  801874:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  80187a:	a1 84 50 80 00       	mov    0x805084,%eax
  80187f:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801885:	83 c4 10             	add    $0x10,%esp
  801888:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80188d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801890:	c9                   	leave  
  801891:	c3                   	ret    

00801892 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801892:	55                   	push   %ebp
  801893:	89 e5                	mov    %esp,%ebp
  801895:	83 ec 08             	sub    $0x8,%esp
  801898:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  80189b:	8b 55 08             	mov    0x8(%ebp),%edx
  80189e:	8b 52 0c             	mov    0xc(%edx),%edx
  8018a1:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  8018a7:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  8018ac:	76 05                	jbe    8018b3 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  8018ae:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  8018b3:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  8018b8:	83 ec 04             	sub    $0x4,%esp
  8018bb:	50                   	push   %eax
  8018bc:	ff 75 0c             	pushl  0xc(%ebp)
  8018bf:	68 08 50 80 00       	push   $0x805008
  8018c4:	e8 fb f1 ff ff       	call   800ac4 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8018c9:	ba 00 00 00 00       	mov    $0x0,%edx
  8018ce:	b8 04 00 00 00       	mov    $0x4,%eax
  8018d3:	e8 ca fe ff ff       	call   8017a2 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8018d8:	c9                   	leave  
  8018d9:	c3                   	ret    

008018da <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8018da:	55                   	push   %ebp
  8018db:	89 e5                	mov    %esp,%ebp
  8018dd:	56                   	push   %esi
  8018de:	53                   	push   %ebx
  8018df:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8018e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8018e5:	8b 40 0c             	mov    0xc(%eax),%eax
  8018e8:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  8018ed:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  8018f3:	ba 00 00 00 00       	mov    $0x0,%edx
  8018f8:	b8 03 00 00 00       	mov    $0x3,%eax
  8018fd:	e8 a0 fe ff ff       	call   8017a2 <fsipc>
  801902:	89 c3                	mov    %eax,%ebx
  801904:	85 c0                	test   %eax,%eax
  801906:	78 4b                	js     801953 <devfile_read+0x79>
		return r;
	assert(r <= n);
  801908:	39 c6                	cmp    %eax,%esi
  80190a:	73 16                	jae    801922 <devfile_read+0x48>
  80190c:	68 ac 29 80 00       	push   $0x8029ac
  801911:	68 b3 29 80 00       	push   $0x8029b3
  801916:	6a 7c                	push   $0x7c
  801918:	68 c8 29 80 00       	push   $0x8029c8
  80191d:	e8 f1 e9 ff ff       	call   800313 <_panic>
	assert(r <= PGSIZE);
  801922:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801927:	7e 16                	jle    80193f <devfile_read+0x65>
  801929:	68 d3 29 80 00       	push   $0x8029d3
  80192e:	68 b3 29 80 00       	push   $0x8029b3
  801933:	6a 7d                	push   $0x7d
  801935:	68 c8 29 80 00       	push   $0x8029c8
  80193a:	e8 d4 e9 ff ff       	call   800313 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  80193f:	83 ec 04             	sub    $0x4,%esp
  801942:	50                   	push   %eax
  801943:	68 00 50 80 00       	push   $0x805000
  801948:	ff 75 0c             	pushl  0xc(%ebp)
  80194b:	e8 74 f1 ff ff       	call   800ac4 <memmove>
	return r;
  801950:	83 c4 10             	add    $0x10,%esp
}
  801953:	89 d8                	mov    %ebx,%eax
  801955:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801958:	5b                   	pop    %ebx
  801959:	5e                   	pop    %esi
  80195a:	5d                   	pop    %ebp
  80195b:	c3                   	ret    

0080195c <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  80195c:	55                   	push   %ebp
  80195d:	89 e5                	mov    %esp,%ebp
  80195f:	53                   	push   %ebx
  801960:	83 ec 20             	sub    $0x20,%esp
  801963:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801966:	53                   	push   %ebx
  801967:	e8 ae ef ff ff       	call   80091a <strlen>
  80196c:	83 c4 10             	add    $0x10,%esp
  80196f:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801974:	7f 63                	jg     8019d9 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801976:	83 ec 0c             	sub    $0xc,%esp
  801979:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80197c:	50                   	push   %eax
  80197d:	e8 be f8 ff ff       	call   801240 <fd_alloc>
  801982:	83 c4 10             	add    $0x10,%esp
  801985:	85 c0                	test   %eax,%eax
  801987:	78 55                	js     8019de <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801989:	83 ec 08             	sub    $0x8,%esp
  80198c:	53                   	push   %ebx
  80198d:	68 00 50 80 00       	push   $0x805000
  801992:	e8 b8 ef ff ff       	call   80094f <strcpy>
	fsipcbuf.open.req_omode = mode;
  801997:	8b 45 0c             	mov    0xc(%ebp),%eax
  80199a:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  80199f:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8019a2:	b8 01 00 00 00       	mov    $0x1,%eax
  8019a7:	e8 f6 fd ff ff       	call   8017a2 <fsipc>
  8019ac:	89 c3                	mov    %eax,%ebx
  8019ae:	83 c4 10             	add    $0x10,%esp
  8019b1:	85 c0                	test   %eax,%eax
  8019b3:	79 14                	jns    8019c9 <open+0x6d>
		fd_close(fd, 0);
  8019b5:	83 ec 08             	sub    $0x8,%esp
  8019b8:	6a 00                	push   $0x0
  8019ba:	ff 75 f4             	pushl  -0xc(%ebp)
  8019bd:	e8 77 f9 ff ff       	call   801339 <fd_close>
		return r;
  8019c2:	83 c4 10             	add    $0x10,%esp
  8019c5:	89 d8                	mov    %ebx,%eax
  8019c7:	eb 15                	jmp    8019de <open+0x82>
	}

	return fd2num(fd);
  8019c9:	83 ec 0c             	sub    $0xc,%esp
  8019cc:	ff 75 f4             	pushl  -0xc(%ebp)
  8019cf:	e8 45 f8 ff ff       	call   801219 <fd2num>
  8019d4:	83 c4 10             	add    $0x10,%esp
  8019d7:	eb 05                	jmp    8019de <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8019d9:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8019de:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8019e1:	c9                   	leave  
  8019e2:	c3                   	ret    

008019e3 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8019e3:	55                   	push   %ebp
  8019e4:	89 e5                	mov    %esp,%ebp
  8019e6:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8019e9:	ba 00 00 00 00       	mov    $0x0,%edx
  8019ee:	b8 08 00 00 00       	mov    $0x8,%eax
  8019f3:	e8 aa fd ff ff       	call   8017a2 <fsipc>
}
  8019f8:	c9                   	leave  
  8019f9:	c3                   	ret    

008019fa <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  8019fa:	55                   	push   %ebp
  8019fb:	89 e5                	mov    %esp,%ebp
  8019fd:	56                   	push   %esi
  8019fe:	53                   	push   %ebx
  8019ff:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801a02:	83 ec 0c             	sub    $0xc,%esp
  801a05:	ff 75 08             	pushl  0x8(%ebp)
  801a08:	e8 1c f8 ff ff       	call   801229 <fd2data>
  801a0d:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801a0f:	83 c4 08             	add    $0x8,%esp
  801a12:	68 df 29 80 00       	push   $0x8029df
  801a17:	53                   	push   %ebx
  801a18:	e8 32 ef ff ff       	call   80094f <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801a1d:	8b 46 04             	mov    0x4(%esi),%eax
  801a20:	2b 06                	sub    (%esi),%eax
  801a22:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801a28:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801a2f:	00 00 00 
	stat->st_dev = &devpipe;
  801a32:	c7 83 88 00 00 00 24 	movl   $0x803024,0x88(%ebx)
  801a39:	30 80 00 
	return 0;
}
  801a3c:	b8 00 00 00 00       	mov    $0x0,%eax
  801a41:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801a44:	5b                   	pop    %ebx
  801a45:	5e                   	pop    %esi
  801a46:	5d                   	pop    %ebp
  801a47:	c3                   	ret    

00801a48 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801a48:	55                   	push   %ebp
  801a49:	89 e5                	mov    %esp,%ebp
  801a4b:	53                   	push   %ebx
  801a4c:	83 ec 0c             	sub    $0xc,%esp
  801a4f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801a52:	53                   	push   %ebx
  801a53:	6a 00                	push   $0x0
  801a55:	e8 5e f3 ff ff       	call   800db8 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801a5a:	89 1c 24             	mov    %ebx,(%esp)
  801a5d:	e8 c7 f7 ff ff       	call   801229 <fd2data>
  801a62:	83 c4 08             	add    $0x8,%esp
  801a65:	50                   	push   %eax
  801a66:	6a 00                	push   $0x0
  801a68:	e8 4b f3 ff ff       	call   800db8 <sys_page_unmap>
}
  801a6d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801a70:	c9                   	leave  
  801a71:	c3                   	ret    

00801a72 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801a72:	55                   	push   %ebp
  801a73:	89 e5                	mov    %esp,%ebp
  801a75:	57                   	push   %edi
  801a76:	56                   	push   %esi
  801a77:	53                   	push   %ebx
  801a78:	83 ec 1c             	sub    $0x1c,%esp
  801a7b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801a7e:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801a80:	a1 04 40 80 00       	mov    0x804004,%eax
  801a85:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801a88:	83 ec 0c             	sub    $0xc,%esp
  801a8b:	ff 75 e0             	pushl  -0x20(%ebp)
  801a8e:	e8 fe 05 00 00       	call   802091 <pageref>
  801a93:	89 c3                	mov    %eax,%ebx
  801a95:	89 3c 24             	mov    %edi,(%esp)
  801a98:	e8 f4 05 00 00       	call   802091 <pageref>
  801a9d:	83 c4 10             	add    $0x10,%esp
  801aa0:	39 c3                	cmp    %eax,%ebx
  801aa2:	0f 94 c1             	sete   %cl
  801aa5:	0f b6 c9             	movzbl %cl,%ecx
  801aa8:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801aab:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801ab1:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801ab4:	39 ce                	cmp    %ecx,%esi
  801ab6:	74 1b                	je     801ad3 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801ab8:	39 c3                	cmp    %eax,%ebx
  801aba:	75 c4                	jne    801a80 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801abc:	8b 42 58             	mov    0x58(%edx),%eax
  801abf:	ff 75 e4             	pushl  -0x1c(%ebp)
  801ac2:	50                   	push   %eax
  801ac3:	56                   	push   %esi
  801ac4:	68 e6 29 80 00       	push   $0x8029e6
  801ac9:	e8 1d e9 ff ff       	call   8003eb <cprintf>
  801ace:	83 c4 10             	add    $0x10,%esp
  801ad1:	eb ad                	jmp    801a80 <_pipeisclosed+0xe>
	}
}
  801ad3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801ad6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801ad9:	5b                   	pop    %ebx
  801ada:	5e                   	pop    %esi
  801adb:	5f                   	pop    %edi
  801adc:	5d                   	pop    %ebp
  801add:	c3                   	ret    

00801ade <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801ade:	55                   	push   %ebp
  801adf:	89 e5                	mov    %esp,%ebp
  801ae1:	57                   	push   %edi
  801ae2:	56                   	push   %esi
  801ae3:	53                   	push   %ebx
  801ae4:	83 ec 18             	sub    $0x18,%esp
  801ae7:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801aea:	56                   	push   %esi
  801aeb:	e8 39 f7 ff ff       	call   801229 <fd2data>
  801af0:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801af2:	83 c4 10             	add    $0x10,%esp
  801af5:	bf 00 00 00 00       	mov    $0x0,%edi
  801afa:	eb 3b                	jmp    801b37 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801afc:	89 da                	mov    %ebx,%edx
  801afe:	89 f0                	mov    %esi,%eax
  801b00:	e8 6d ff ff ff       	call   801a72 <_pipeisclosed>
  801b05:	85 c0                	test   %eax,%eax
  801b07:	75 38                	jne    801b41 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801b09:	e8 06 f2 ff ff       	call   800d14 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801b0e:	8b 53 04             	mov    0x4(%ebx),%edx
  801b11:	8b 03                	mov    (%ebx),%eax
  801b13:	83 c0 20             	add    $0x20,%eax
  801b16:	39 c2                	cmp    %eax,%edx
  801b18:	73 e2                	jae    801afc <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801b1a:	8b 45 0c             	mov    0xc(%ebp),%eax
  801b1d:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801b20:	89 d0                	mov    %edx,%eax
  801b22:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801b27:	79 05                	jns    801b2e <devpipe_write+0x50>
  801b29:	48                   	dec    %eax
  801b2a:	83 c8 e0             	or     $0xffffffe0,%eax
  801b2d:	40                   	inc    %eax
  801b2e:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801b32:	42                   	inc    %edx
  801b33:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801b36:	47                   	inc    %edi
  801b37:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801b3a:	75 d2                	jne    801b0e <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801b3c:	8b 45 10             	mov    0x10(%ebp),%eax
  801b3f:	eb 05                	jmp    801b46 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801b41:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801b46:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b49:	5b                   	pop    %ebx
  801b4a:	5e                   	pop    %esi
  801b4b:	5f                   	pop    %edi
  801b4c:	5d                   	pop    %ebp
  801b4d:	c3                   	ret    

00801b4e <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801b4e:	55                   	push   %ebp
  801b4f:	89 e5                	mov    %esp,%ebp
  801b51:	57                   	push   %edi
  801b52:	56                   	push   %esi
  801b53:	53                   	push   %ebx
  801b54:	83 ec 18             	sub    $0x18,%esp
  801b57:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801b5a:	57                   	push   %edi
  801b5b:	e8 c9 f6 ff ff       	call   801229 <fd2data>
  801b60:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801b62:	83 c4 10             	add    $0x10,%esp
  801b65:	bb 00 00 00 00       	mov    $0x0,%ebx
  801b6a:	eb 3a                	jmp    801ba6 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801b6c:	85 db                	test   %ebx,%ebx
  801b6e:	74 04                	je     801b74 <devpipe_read+0x26>
				return i;
  801b70:	89 d8                	mov    %ebx,%eax
  801b72:	eb 41                	jmp    801bb5 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801b74:	89 f2                	mov    %esi,%edx
  801b76:	89 f8                	mov    %edi,%eax
  801b78:	e8 f5 fe ff ff       	call   801a72 <_pipeisclosed>
  801b7d:	85 c0                	test   %eax,%eax
  801b7f:	75 2f                	jne    801bb0 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801b81:	e8 8e f1 ff ff       	call   800d14 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801b86:	8b 06                	mov    (%esi),%eax
  801b88:	3b 46 04             	cmp    0x4(%esi),%eax
  801b8b:	74 df                	je     801b6c <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801b8d:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801b92:	79 05                	jns    801b99 <devpipe_read+0x4b>
  801b94:	48                   	dec    %eax
  801b95:	83 c8 e0             	or     $0xffffffe0,%eax
  801b98:	40                   	inc    %eax
  801b99:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801b9d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801ba0:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801ba3:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801ba5:	43                   	inc    %ebx
  801ba6:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801ba9:	75 db                	jne    801b86 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801bab:	8b 45 10             	mov    0x10(%ebp),%eax
  801bae:	eb 05                	jmp    801bb5 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801bb0:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801bb5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801bb8:	5b                   	pop    %ebx
  801bb9:	5e                   	pop    %esi
  801bba:	5f                   	pop    %edi
  801bbb:	5d                   	pop    %ebp
  801bbc:	c3                   	ret    

00801bbd <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801bbd:	55                   	push   %ebp
  801bbe:	89 e5                	mov    %esp,%ebp
  801bc0:	56                   	push   %esi
  801bc1:	53                   	push   %ebx
  801bc2:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801bc5:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801bc8:	50                   	push   %eax
  801bc9:	e8 72 f6 ff ff       	call   801240 <fd_alloc>
  801bce:	83 c4 10             	add    $0x10,%esp
  801bd1:	85 c0                	test   %eax,%eax
  801bd3:	0f 88 2a 01 00 00    	js     801d03 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801bd9:	83 ec 04             	sub    $0x4,%esp
  801bdc:	68 07 04 00 00       	push   $0x407
  801be1:	ff 75 f4             	pushl  -0xc(%ebp)
  801be4:	6a 00                	push   $0x0
  801be6:	e8 48 f1 ff ff       	call   800d33 <sys_page_alloc>
  801beb:	83 c4 10             	add    $0x10,%esp
  801bee:	85 c0                	test   %eax,%eax
  801bf0:	0f 88 0d 01 00 00    	js     801d03 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801bf6:	83 ec 0c             	sub    $0xc,%esp
  801bf9:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801bfc:	50                   	push   %eax
  801bfd:	e8 3e f6 ff ff       	call   801240 <fd_alloc>
  801c02:	89 c3                	mov    %eax,%ebx
  801c04:	83 c4 10             	add    $0x10,%esp
  801c07:	85 c0                	test   %eax,%eax
  801c09:	0f 88 e2 00 00 00    	js     801cf1 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801c0f:	83 ec 04             	sub    $0x4,%esp
  801c12:	68 07 04 00 00       	push   $0x407
  801c17:	ff 75 f0             	pushl  -0x10(%ebp)
  801c1a:	6a 00                	push   $0x0
  801c1c:	e8 12 f1 ff ff       	call   800d33 <sys_page_alloc>
  801c21:	89 c3                	mov    %eax,%ebx
  801c23:	83 c4 10             	add    $0x10,%esp
  801c26:	85 c0                	test   %eax,%eax
  801c28:	0f 88 c3 00 00 00    	js     801cf1 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801c2e:	83 ec 0c             	sub    $0xc,%esp
  801c31:	ff 75 f4             	pushl  -0xc(%ebp)
  801c34:	e8 f0 f5 ff ff       	call   801229 <fd2data>
  801c39:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801c3b:	83 c4 0c             	add    $0xc,%esp
  801c3e:	68 07 04 00 00       	push   $0x407
  801c43:	50                   	push   %eax
  801c44:	6a 00                	push   $0x0
  801c46:	e8 e8 f0 ff ff       	call   800d33 <sys_page_alloc>
  801c4b:	89 c3                	mov    %eax,%ebx
  801c4d:	83 c4 10             	add    $0x10,%esp
  801c50:	85 c0                	test   %eax,%eax
  801c52:	0f 88 89 00 00 00    	js     801ce1 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801c58:	83 ec 0c             	sub    $0xc,%esp
  801c5b:	ff 75 f0             	pushl  -0x10(%ebp)
  801c5e:	e8 c6 f5 ff ff       	call   801229 <fd2data>
  801c63:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801c6a:	50                   	push   %eax
  801c6b:	6a 00                	push   $0x0
  801c6d:	56                   	push   %esi
  801c6e:	6a 00                	push   $0x0
  801c70:	e8 01 f1 ff ff       	call   800d76 <sys_page_map>
  801c75:	89 c3                	mov    %eax,%ebx
  801c77:	83 c4 20             	add    $0x20,%esp
  801c7a:	85 c0                	test   %eax,%eax
  801c7c:	78 55                	js     801cd3 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801c7e:	8b 15 24 30 80 00    	mov    0x803024,%edx
  801c84:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c87:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801c89:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c8c:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801c93:	8b 15 24 30 80 00    	mov    0x803024,%edx
  801c99:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c9c:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801c9e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801ca1:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801ca8:	83 ec 0c             	sub    $0xc,%esp
  801cab:	ff 75 f4             	pushl  -0xc(%ebp)
  801cae:	e8 66 f5 ff ff       	call   801219 <fd2num>
  801cb3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801cb6:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801cb8:	83 c4 04             	add    $0x4,%esp
  801cbb:	ff 75 f0             	pushl  -0x10(%ebp)
  801cbe:	e8 56 f5 ff ff       	call   801219 <fd2num>
  801cc3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801cc6:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801cc9:	83 c4 10             	add    $0x10,%esp
  801ccc:	b8 00 00 00 00       	mov    $0x0,%eax
  801cd1:	eb 30                	jmp    801d03 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801cd3:	83 ec 08             	sub    $0x8,%esp
  801cd6:	56                   	push   %esi
  801cd7:	6a 00                	push   $0x0
  801cd9:	e8 da f0 ff ff       	call   800db8 <sys_page_unmap>
  801cde:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801ce1:	83 ec 08             	sub    $0x8,%esp
  801ce4:	ff 75 f0             	pushl  -0x10(%ebp)
  801ce7:	6a 00                	push   $0x0
  801ce9:	e8 ca f0 ff ff       	call   800db8 <sys_page_unmap>
  801cee:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801cf1:	83 ec 08             	sub    $0x8,%esp
  801cf4:	ff 75 f4             	pushl  -0xc(%ebp)
  801cf7:	6a 00                	push   $0x0
  801cf9:	e8 ba f0 ff ff       	call   800db8 <sys_page_unmap>
  801cfe:	83 c4 10             	add    $0x10,%esp
  801d01:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801d03:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801d06:	5b                   	pop    %ebx
  801d07:	5e                   	pop    %esi
  801d08:	5d                   	pop    %ebp
  801d09:	c3                   	ret    

00801d0a <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801d0a:	55                   	push   %ebp
  801d0b:	89 e5                	mov    %esp,%ebp
  801d0d:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801d10:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801d13:	50                   	push   %eax
  801d14:	ff 75 08             	pushl  0x8(%ebp)
  801d17:	e8 73 f5 ff ff       	call   80128f <fd_lookup>
  801d1c:	83 c4 10             	add    $0x10,%esp
  801d1f:	85 c0                	test   %eax,%eax
  801d21:	78 18                	js     801d3b <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801d23:	83 ec 0c             	sub    $0xc,%esp
  801d26:	ff 75 f4             	pushl  -0xc(%ebp)
  801d29:	e8 fb f4 ff ff       	call   801229 <fd2data>
	return _pipeisclosed(fd, p);
  801d2e:	89 c2                	mov    %eax,%edx
  801d30:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801d33:	e8 3a fd ff ff       	call   801a72 <_pipeisclosed>
  801d38:	83 c4 10             	add    $0x10,%esp
}
  801d3b:	c9                   	leave  
  801d3c:	c3                   	ret    

00801d3d <wait>:
#include <inc/lib.h>

// Waits until 'envid' exits.
void
wait(envid_t envid)
{
  801d3d:	55                   	push   %ebp
  801d3e:	89 e5                	mov    %esp,%ebp
  801d40:	56                   	push   %esi
  801d41:	53                   	push   %ebx
  801d42:	8b 75 08             	mov    0x8(%ebp),%esi
	const volatile struct Env *e;

	assert(envid != 0);
  801d45:	85 f6                	test   %esi,%esi
  801d47:	75 16                	jne    801d5f <wait+0x22>
  801d49:	68 fe 29 80 00       	push   $0x8029fe
  801d4e:	68 b3 29 80 00       	push   $0x8029b3
  801d53:	6a 09                	push   $0x9
  801d55:	68 09 2a 80 00       	push   $0x802a09
  801d5a:	e8 b4 e5 ff ff       	call   800313 <_panic>
	e = &envs[ENVX(envid)];
  801d5f:	89 f3                	mov    %esi,%ebx
  801d61:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
	while (e->env_id == envid && e->env_status != ENV_FREE)
  801d67:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  801d6e:	c1 e3 07             	shl    $0x7,%ebx
  801d71:	29 c3                	sub    %eax,%ebx
  801d73:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  801d79:	eb 05                	jmp    801d80 <wait+0x43>
		sys_yield();
  801d7b:	e8 94 ef ff ff       	call   800d14 <sys_yield>
{
	const volatile struct Env *e;

	assert(envid != 0);
	e = &envs[ENVX(envid)];
	while (e->env_id == envid && e->env_status != ENV_FREE)
  801d80:	8b 43 48             	mov    0x48(%ebx),%eax
  801d83:	39 c6                	cmp    %eax,%esi
  801d85:	75 07                	jne    801d8e <wait+0x51>
  801d87:	8b 43 54             	mov    0x54(%ebx),%eax
  801d8a:	85 c0                	test   %eax,%eax
  801d8c:	75 ed                	jne    801d7b <wait+0x3e>
		sys_yield();
}
  801d8e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801d91:	5b                   	pop    %ebx
  801d92:	5e                   	pop    %esi
  801d93:	5d                   	pop    %ebp
  801d94:	c3                   	ret    

00801d95 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801d95:	55                   	push   %ebp
  801d96:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801d98:	b8 00 00 00 00       	mov    $0x0,%eax
  801d9d:	5d                   	pop    %ebp
  801d9e:	c3                   	ret    

00801d9f <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801d9f:	55                   	push   %ebp
  801da0:	89 e5                	mov    %esp,%ebp
  801da2:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801da5:	68 14 2a 80 00       	push   $0x802a14
  801daa:	ff 75 0c             	pushl  0xc(%ebp)
  801dad:	e8 9d eb ff ff       	call   80094f <strcpy>
	return 0;
}
  801db2:	b8 00 00 00 00       	mov    $0x0,%eax
  801db7:	c9                   	leave  
  801db8:	c3                   	ret    

00801db9 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801db9:	55                   	push   %ebp
  801dba:	89 e5                	mov    %esp,%ebp
  801dbc:	57                   	push   %edi
  801dbd:	56                   	push   %esi
  801dbe:	53                   	push   %ebx
  801dbf:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801dc5:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801dca:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801dd0:	eb 2c                	jmp    801dfe <devcons_write+0x45>
		m = n - tot;
  801dd2:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801dd5:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801dd7:	83 fb 7f             	cmp    $0x7f,%ebx
  801dda:	76 05                	jbe    801de1 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801ddc:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801de1:	83 ec 04             	sub    $0x4,%esp
  801de4:	53                   	push   %ebx
  801de5:	03 45 0c             	add    0xc(%ebp),%eax
  801de8:	50                   	push   %eax
  801de9:	57                   	push   %edi
  801dea:	e8 d5 ec ff ff       	call   800ac4 <memmove>
		sys_cputs(buf, m);
  801def:	83 c4 08             	add    $0x8,%esp
  801df2:	53                   	push   %ebx
  801df3:	57                   	push   %edi
  801df4:	e8 7e ee ff ff       	call   800c77 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801df9:	01 de                	add    %ebx,%esi
  801dfb:	83 c4 10             	add    $0x10,%esp
  801dfe:	89 f0                	mov    %esi,%eax
  801e00:	3b 75 10             	cmp    0x10(%ebp),%esi
  801e03:	72 cd                	jb     801dd2 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801e05:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801e08:	5b                   	pop    %ebx
  801e09:	5e                   	pop    %esi
  801e0a:	5f                   	pop    %edi
  801e0b:	5d                   	pop    %ebp
  801e0c:	c3                   	ret    

00801e0d <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801e0d:	55                   	push   %ebp
  801e0e:	89 e5                	mov    %esp,%ebp
  801e10:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801e13:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801e17:	75 07                	jne    801e20 <devcons_read+0x13>
  801e19:	eb 23                	jmp    801e3e <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801e1b:	e8 f4 ee ff ff       	call   800d14 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801e20:	e8 70 ee ff ff       	call   800c95 <sys_cgetc>
  801e25:	85 c0                	test   %eax,%eax
  801e27:	74 f2                	je     801e1b <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801e29:	85 c0                	test   %eax,%eax
  801e2b:	78 1d                	js     801e4a <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801e2d:	83 f8 04             	cmp    $0x4,%eax
  801e30:	74 13                	je     801e45 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801e32:	8b 55 0c             	mov    0xc(%ebp),%edx
  801e35:	88 02                	mov    %al,(%edx)
	return 1;
  801e37:	b8 01 00 00 00       	mov    $0x1,%eax
  801e3c:	eb 0c                	jmp    801e4a <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801e3e:	b8 00 00 00 00       	mov    $0x0,%eax
  801e43:	eb 05                	jmp    801e4a <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801e45:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801e4a:	c9                   	leave  
  801e4b:	c3                   	ret    

00801e4c <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801e4c:	55                   	push   %ebp
  801e4d:	89 e5                	mov    %esp,%ebp
  801e4f:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801e52:	8b 45 08             	mov    0x8(%ebp),%eax
  801e55:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801e58:	6a 01                	push   $0x1
  801e5a:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801e5d:	50                   	push   %eax
  801e5e:	e8 14 ee ff ff       	call   800c77 <sys_cputs>
}
  801e63:	83 c4 10             	add    $0x10,%esp
  801e66:	c9                   	leave  
  801e67:	c3                   	ret    

00801e68 <getchar>:

int
getchar(void)
{
  801e68:	55                   	push   %ebp
  801e69:	89 e5                	mov    %esp,%ebp
  801e6b:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801e6e:	6a 01                	push   $0x1
  801e70:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801e73:	50                   	push   %eax
  801e74:	6a 00                	push   $0x0
  801e76:	e8 7a f6 ff ff       	call   8014f5 <read>
	if (r < 0)
  801e7b:	83 c4 10             	add    $0x10,%esp
  801e7e:	85 c0                	test   %eax,%eax
  801e80:	78 0f                	js     801e91 <getchar+0x29>
		return r;
	if (r < 1)
  801e82:	85 c0                	test   %eax,%eax
  801e84:	7e 06                	jle    801e8c <getchar+0x24>
		return -E_EOF;
	return c;
  801e86:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801e8a:	eb 05                	jmp    801e91 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801e8c:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801e91:	c9                   	leave  
  801e92:	c3                   	ret    

00801e93 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801e93:	55                   	push   %ebp
  801e94:	89 e5                	mov    %esp,%ebp
  801e96:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801e99:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801e9c:	50                   	push   %eax
  801e9d:	ff 75 08             	pushl  0x8(%ebp)
  801ea0:	e8 ea f3 ff ff       	call   80128f <fd_lookup>
  801ea5:	83 c4 10             	add    $0x10,%esp
  801ea8:	85 c0                	test   %eax,%eax
  801eaa:	78 11                	js     801ebd <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801eac:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801eaf:	8b 15 40 30 80 00    	mov    0x803040,%edx
  801eb5:	39 10                	cmp    %edx,(%eax)
  801eb7:	0f 94 c0             	sete   %al
  801eba:	0f b6 c0             	movzbl %al,%eax
}
  801ebd:	c9                   	leave  
  801ebe:	c3                   	ret    

00801ebf <opencons>:

int
opencons(void)
{
  801ebf:	55                   	push   %ebp
  801ec0:	89 e5                	mov    %esp,%ebp
  801ec2:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801ec5:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ec8:	50                   	push   %eax
  801ec9:	e8 72 f3 ff ff       	call   801240 <fd_alloc>
  801ece:	83 c4 10             	add    $0x10,%esp
  801ed1:	85 c0                	test   %eax,%eax
  801ed3:	78 3a                	js     801f0f <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801ed5:	83 ec 04             	sub    $0x4,%esp
  801ed8:	68 07 04 00 00       	push   $0x407
  801edd:	ff 75 f4             	pushl  -0xc(%ebp)
  801ee0:	6a 00                	push   $0x0
  801ee2:	e8 4c ee ff ff       	call   800d33 <sys_page_alloc>
  801ee7:	83 c4 10             	add    $0x10,%esp
  801eea:	85 c0                	test   %eax,%eax
  801eec:	78 21                	js     801f0f <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801eee:	8b 15 40 30 80 00    	mov    0x803040,%edx
  801ef4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ef7:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801ef9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801efc:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801f03:	83 ec 0c             	sub    $0xc,%esp
  801f06:	50                   	push   %eax
  801f07:	e8 0d f3 ff ff       	call   801219 <fd2num>
  801f0c:	83 c4 10             	add    $0x10,%esp
}
  801f0f:	c9                   	leave  
  801f10:	c3                   	ret    

00801f11 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801f11:	55                   	push   %ebp
  801f12:	89 e5                	mov    %esp,%ebp
  801f14:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801f17:	83 3d 00 60 80 00 00 	cmpl   $0x0,0x806000
  801f1e:	75 3e                	jne    801f5e <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801f20:	83 ec 04             	sub    $0x4,%esp
  801f23:	6a 07                	push   $0x7
  801f25:	68 00 f0 bf ee       	push   $0xeebff000
  801f2a:	6a 00                	push   $0x0
  801f2c:	e8 02 ee ff ff       	call   800d33 <sys_page_alloc>
  801f31:	83 c4 10             	add    $0x10,%esp
  801f34:	85 c0                	test   %eax,%eax
  801f36:	74 14                	je     801f4c <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801f38:	83 ec 04             	sub    $0x4,%esp
  801f3b:	68 20 2a 80 00       	push   $0x802a20
  801f40:	6a 23                	push   $0x23
  801f42:	68 44 2a 80 00       	push   $0x802a44
  801f47:	e8 c7 e3 ff ff       	call   800313 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801f4c:	83 ec 08             	sub    $0x8,%esp
  801f4f:	68 68 1f 80 00       	push   $0x801f68
  801f54:	6a 00                	push   $0x0
  801f56:	e8 42 ef ff ff       	call   800e9d <sys_env_set_pgfault_upcall>
  801f5b:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801f5e:	8b 45 08             	mov    0x8(%ebp),%eax
  801f61:	a3 00 60 80 00       	mov    %eax,0x806000
}
  801f66:	c9                   	leave  
  801f67:	c3                   	ret    

00801f68 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801f68:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801f69:	a1 00 60 80 00       	mov    0x806000,%eax
	call *%eax
  801f6e:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801f70:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801f73:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801f75:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801f79:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801f7d:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801f7e:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801f81:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801f83:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801f86:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801f89:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801f8a:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  801f8d:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801f8e:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801f8f:	c3                   	ret    

00801f90 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801f90:	55                   	push   %ebp
  801f91:	89 e5                	mov    %esp,%ebp
  801f93:	56                   	push   %esi
  801f94:	53                   	push   %ebx
  801f95:	8b 75 08             	mov    0x8(%ebp),%esi
  801f98:	8b 45 0c             	mov    0xc(%ebp),%eax
  801f9b:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801f9e:	85 c0                	test   %eax,%eax
  801fa0:	75 05                	jne    801fa7 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801fa2:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801fa7:	83 ec 0c             	sub    $0xc,%esp
  801faa:	50                   	push   %eax
  801fab:	e8 52 ef ff ff       	call   800f02 <sys_ipc_recv>
  801fb0:	83 c4 10             	add    $0x10,%esp
  801fb3:	85 c0                	test   %eax,%eax
  801fb5:	74 16                	je     801fcd <ipc_recv+0x3d>
        if (from_env_store)
  801fb7:	85 f6                	test   %esi,%esi
  801fb9:	74 06                	je     801fc1 <ipc_recv+0x31>
            *from_env_store = 0;
  801fbb:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801fc1:	85 db                	test   %ebx,%ebx
  801fc3:	74 2c                	je     801ff1 <ipc_recv+0x61>
            *perm_store = 0;
  801fc5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801fcb:	eb 24                	jmp    801ff1 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801fcd:	85 f6                	test   %esi,%esi
  801fcf:	74 0a                	je     801fdb <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801fd1:	a1 04 40 80 00       	mov    0x804004,%eax
  801fd6:	8b 40 74             	mov    0x74(%eax),%eax
  801fd9:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801fdb:	85 db                	test   %ebx,%ebx
  801fdd:	74 0a                	je     801fe9 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801fdf:	a1 04 40 80 00       	mov    0x804004,%eax
  801fe4:	8b 40 78             	mov    0x78(%eax),%eax
  801fe7:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801fe9:	a1 04 40 80 00       	mov    0x804004,%eax
  801fee:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801ff1:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801ff4:	5b                   	pop    %ebx
  801ff5:	5e                   	pop    %esi
  801ff6:	5d                   	pop    %ebp
  801ff7:	c3                   	ret    

00801ff8 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801ff8:	55                   	push   %ebp
  801ff9:	89 e5                	mov    %esp,%ebp
  801ffb:	57                   	push   %edi
  801ffc:	56                   	push   %esi
  801ffd:	53                   	push   %ebx
  801ffe:	83 ec 0c             	sub    $0xc,%esp
  802001:	8b 75 0c             	mov    0xc(%ebp),%esi
  802004:	8b 5d 10             	mov    0x10(%ebp),%ebx
  802007:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  80200a:	85 db                	test   %ebx,%ebx
  80200c:	75 0c                	jne    80201a <ipc_send+0x22>
        pg = (void *)UTOP;
  80200e:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  802013:	eb 05                	jmp    80201a <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  802015:	e8 fa ec ff ff       	call   800d14 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  80201a:	57                   	push   %edi
  80201b:	53                   	push   %ebx
  80201c:	56                   	push   %esi
  80201d:	ff 75 08             	pushl  0x8(%ebp)
  802020:	e8 ba ee ff ff       	call   800edf <sys_ipc_try_send>
  802025:	83 c4 10             	add    $0x10,%esp
  802028:	83 f8 f9             	cmp    $0xfffffff9,%eax
  80202b:	74 e8                	je     802015 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  80202d:	85 c0                	test   %eax,%eax
  80202f:	74 14                	je     802045 <ipc_send+0x4d>
        panic("ipc_send: error");
  802031:	83 ec 04             	sub    $0x4,%esp
  802034:	68 52 2a 80 00       	push   $0x802a52
  802039:	6a 6a                	push   $0x6a
  80203b:	68 62 2a 80 00       	push   $0x802a62
  802040:	e8 ce e2 ff ff       	call   800313 <_panic>
    }
	//panic("ipc_send not implemented");
}
  802045:	8d 65 f4             	lea    -0xc(%ebp),%esp
  802048:	5b                   	pop    %ebx
  802049:	5e                   	pop    %esi
  80204a:	5f                   	pop    %edi
  80204b:	5d                   	pop    %ebp
  80204c:	c3                   	ret    

0080204d <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  80204d:	55                   	push   %ebp
  80204e:	89 e5                	mov    %esp,%ebp
  802050:	53                   	push   %ebx
  802051:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  802054:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  802059:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  802060:	89 d0                	mov    %edx,%eax
  802062:	c1 e0 07             	shl    $0x7,%eax
  802065:	29 d8                	sub    %ebx,%eax
  802067:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80206c:	8b 40 50             	mov    0x50(%eax),%eax
  80206f:	39 c8                	cmp    %ecx,%eax
  802071:	75 0d                	jne    802080 <ipc_find_env+0x33>
			return envs[i].env_id;
  802073:	c1 e2 07             	shl    $0x7,%edx
  802076:	29 da                	sub    %ebx,%edx
  802078:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  80207e:	eb 0e                	jmp    80208e <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  802080:	42                   	inc    %edx
  802081:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  802087:	75 d0                	jne    802059 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  802089:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80208e:	5b                   	pop    %ebx
  80208f:	5d                   	pop    %ebp
  802090:	c3                   	ret    

00802091 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802091:	55                   	push   %ebp
  802092:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  802094:	8b 45 08             	mov    0x8(%ebp),%eax
  802097:	c1 e8 16             	shr    $0x16,%eax
  80209a:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8020a1:	a8 01                	test   $0x1,%al
  8020a3:	74 21                	je     8020c6 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  8020a5:	8b 45 08             	mov    0x8(%ebp),%eax
  8020a8:	c1 e8 0c             	shr    $0xc,%eax
  8020ab:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  8020b2:	a8 01                	test   $0x1,%al
  8020b4:	74 17                	je     8020cd <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  8020b6:	c1 e8 0c             	shr    $0xc,%eax
  8020b9:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  8020c0:	ef 
  8020c1:	0f b7 c0             	movzwl %ax,%eax
  8020c4:	eb 0c                	jmp    8020d2 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  8020c6:	b8 00 00 00 00       	mov    $0x0,%eax
  8020cb:	eb 05                	jmp    8020d2 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  8020cd:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  8020d2:	5d                   	pop    %ebp
  8020d3:	c3                   	ret    

008020d4 <__udivdi3>:
  8020d4:	55                   	push   %ebp
  8020d5:	57                   	push   %edi
  8020d6:	56                   	push   %esi
  8020d7:	53                   	push   %ebx
  8020d8:	83 ec 1c             	sub    $0x1c,%esp
  8020db:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8020df:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8020e3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8020e7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8020eb:	89 ca                	mov    %ecx,%edx
  8020ed:	89 f8                	mov    %edi,%eax
  8020ef:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8020f3:	85 f6                	test   %esi,%esi
  8020f5:	75 2d                	jne    802124 <__udivdi3+0x50>
  8020f7:	39 cf                	cmp    %ecx,%edi
  8020f9:	77 65                	ja     802160 <__udivdi3+0x8c>
  8020fb:	89 fd                	mov    %edi,%ebp
  8020fd:	85 ff                	test   %edi,%edi
  8020ff:	75 0b                	jne    80210c <__udivdi3+0x38>
  802101:	b8 01 00 00 00       	mov    $0x1,%eax
  802106:	31 d2                	xor    %edx,%edx
  802108:	f7 f7                	div    %edi
  80210a:	89 c5                	mov    %eax,%ebp
  80210c:	31 d2                	xor    %edx,%edx
  80210e:	89 c8                	mov    %ecx,%eax
  802110:	f7 f5                	div    %ebp
  802112:	89 c1                	mov    %eax,%ecx
  802114:	89 d8                	mov    %ebx,%eax
  802116:	f7 f5                	div    %ebp
  802118:	89 cf                	mov    %ecx,%edi
  80211a:	89 fa                	mov    %edi,%edx
  80211c:	83 c4 1c             	add    $0x1c,%esp
  80211f:	5b                   	pop    %ebx
  802120:	5e                   	pop    %esi
  802121:	5f                   	pop    %edi
  802122:	5d                   	pop    %ebp
  802123:	c3                   	ret    
  802124:	39 ce                	cmp    %ecx,%esi
  802126:	77 28                	ja     802150 <__udivdi3+0x7c>
  802128:	0f bd fe             	bsr    %esi,%edi
  80212b:	83 f7 1f             	xor    $0x1f,%edi
  80212e:	75 40                	jne    802170 <__udivdi3+0x9c>
  802130:	39 ce                	cmp    %ecx,%esi
  802132:	72 0a                	jb     80213e <__udivdi3+0x6a>
  802134:	3b 44 24 08          	cmp    0x8(%esp),%eax
  802138:	0f 87 9e 00 00 00    	ja     8021dc <__udivdi3+0x108>
  80213e:	b8 01 00 00 00       	mov    $0x1,%eax
  802143:	89 fa                	mov    %edi,%edx
  802145:	83 c4 1c             	add    $0x1c,%esp
  802148:	5b                   	pop    %ebx
  802149:	5e                   	pop    %esi
  80214a:	5f                   	pop    %edi
  80214b:	5d                   	pop    %ebp
  80214c:	c3                   	ret    
  80214d:	8d 76 00             	lea    0x0(%esi),%esi
  802150:	31 ff                	xor    %edi,%edi
  802152:	31 c0                	xor    %eax,%eax
  802154:	89 fa                	mov    %edi,%edx
  802156:	83 c4 1c             	add    $0x1c,%esp
  802159:	5b                   	pop    %ebx
  80215a:	5e                   	pop    %esi
  80215b:	5f                   	pop    %edi
  80215c:	5d                   	pop    %ebp
  80215d:	c3                   	ret    
  80215e:	66 90                	xchg   %ax,%ax
  802160:	89 d8                	mov    %ebx,%eax
  802162:	f7 f7                	div    %edi
  802164:	31 ff                	xor    %edi,%edi
  802166:	89 fa                	mov    %edi,%edx
  802168:	83 c4 1c             	add    $0x1c,%esp
  80216b:	5b                   	pop    %ebx
  80216c:	5e                   	pop    %esi
  80216d:	5f                   	pop    %edi
  80216e:	5d                   	pop    %ebp
  80216f:	c3                   	ret    
  802170:	bd 20 00 00 00       	mov    $0x20,%ebp
  802175:	89 eb                	mov    %ebp,%ebx
  802177:	29 fb                	sub    %edi,%ebx
  802179:	89 f9                	mov    %edi,%ecx
  80217b:	d3 e6                	shl    %cl,%esi
  80217d:	89 c5                	mov    %eax,%ebp
  80217f:	88 d9                	mov    %bl,%cl
  802181:	d3 ed                	shr    %cl,%ebp
  802183:	89 e9                	mov    %ebp,%ecx
  802185:	09 f1                	or     %esi,%ecx
  802187:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80218b:	89 f9                	mov    %edi,%ecx
  80218d:	d3 e0                	shl    %cl,%eax
  80218f:	89 c5                	mov    %eax,%ebp
  802191:	89 d6                	mov    %edx,%esi
  802193:	88 d9                	mov    %bl,%cl
  802195:	d3 ee                	shr    %cl,%esi
  802197:	89 f9                	mov    %edi,%ecx
  802199:	d3 e2                	shl    %cl,%edx
  80219b:	8b 44 24 08          	mov    0x8(%esp),%eax
  80219f:	88 d9                	mov    %bl,%cl
  8021a1:	d3 e8                	shr    %cl,%eax
  8021a3:	09 c2                	or     %eax,%edx
  8021a5:	89 d0                	mov    %edx,%eax
  8021a7:	89 f2                	mov    %esi,%edx
  8021a9:	f7 74 24 0c          	divl   0xc(%esp)
  8021ad:	89 d6                	mov    %edx,%esi
  8021af:	89 c3                	mov    %eax,%ebx
  8021b1:	f7 e5                	mul    %ebp
  8021b3:	39 d6                	cmp    %edx,%esi
  8021b5:	72 19                	jb     8021d0 <__udivdi3+0xfc>
  8021b7:	74 0b                	je     8021c4 <__udivdi3+0xf0>
  8021b9:	89 d8                	mov    %ebx,%eax
  8021bb:	31 ff                	xor    %edi,%edi
  8021bd:	e9 58 ff ff ff       	jmp    80211a <__udivdi3+0x46>
  8021c2:	66 90                	xchg   %ax,%ax
  8021c4:	8b 54 24 08          	mov    0x8(%esp),%edx
  8021c8:	89 f9                	mov    %edi,%ecx
  8021ca:	d3 e2                	shl    %cl,%edx
  8021cc:	39 c2                	cmp    %eax,%edx
  8021ce:	73 e9                	jae    8021b9 <__udivdi3+0xe5>
  8021d0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8021d3:	31 ff                	xor    %edi,%edi
  8021d5:	e9 40 ff ff ff       	jmp    80211a <__udivdi3+0x46>
  8021da:	66 90                	xchg   %ax,%ax
  8021dc:	31 c0                	xor    %eax,%eax
  8021de:	e9 37 ff ff ff       	jmp    80211a <__udivdi3+0x46>
  8021e3:	90                   	nop

008021e4 <__umoddi3>:
  8021e4:	55                   	push   %ebp
  8021e5:	57                   	push   %edi
  8021e6:	56                   	push   %esi
  8021e7:	53                   	push   %ebx
  8021e8:	83 ec 1c             	sub    $0x1c,%esp
  8021eb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8021ef:	8b 74 24 34          	mov    0x34(%esp),%esi
  8021f3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8021f7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8021fb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8021ff:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  802203:	89 f3                	mov    %esi,%ebx
  802205:	89 fa                	mov    %edi,%edx
  802207:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80220b:	89 34 24             	mov    %esi,(%esp)
  80220e:	85 c0                	test   %eax,%eax
  802210:	75 1a                	jne    80222c <__umoddi3+0x48>
  802212:	39 f7                	cmp    %esi,%edi
  802214:	0f 86 a2 00 00 00    	jbe    8022bc <__umoddi3+0xd8>
  80221a:	89 c8                	mov    %ecx,%eax
  80221c:	89 f2                	mov    %esi,%edx
  80221e:	f7 f7                	div    %edi
  802220:	89 d0                	mov    %edx,%eax
  802222:	31 d2                	xor    %edx,%edx
  802224:	83 c4 1c             	add    $0x1c,%esp
  802227:	5b                   	pop    %ebx
  802228:	5e                   	pop    %esi
  802229:	5f                   	pop    %edi
  80222a:	5d                   	pop    %ebp
  80222b:	c3                   	ret    
  80222c:	39 f0                	cmp    %esi,%eax
  80222e:	0f 87 ac 00 00 00    	ja     8022e0 <__umoddi3+0xfc>
  802234:	0f bd e8             	bsr    %eax,%ebp
  802237:	83 f5 1f             	xor    $0x1f,%ebp
  80223a:	0f 84 ac 00 00 00    	je     8022ec <__umoddi3+0x108>
  802240:	bf 20 00 00 00       	mov    $0x20,%edi
  802245:	29 ef                	sub    %ebp,%edi
  802247:	89 fe                	mov    %edi,%esi
  802249:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80224d:	89 e9                	mov    %ebp,%ecx
  80224f:	d3 e0                	shl    %cl,%eax
  802251:	89 d7                	mov    %edx,%edi
  802253:	89 f1                	mov    %esi,%ecx
  802255:	d3 ef                	shr    %cl,%edi
  802257:	09 c7                	or     %eax,%edi
  802259:	89 e9                	mov    %ebp,%ecx
  80225b:	d3 e2                	shl    %cl,%edx
  80225d:	89 14 24             	mov    %edx,(%esp)
  802260:	89 d8                	mov    %ebx,%eax
  802262:	d3 e0                	shl    %cl,%eax
  802264:	89 c2                	mov    %eax,%edx
  802266:	8b 44 24 08          	mov    0x8(%esp),%eax
  80226a:	d3 e0                	shl    %cl,%eax
  80226c:	89 44 24 04          	mov    %eax,0x4(%esp)
  802270:	8b 44 24 08          	mov    0x8(%esp),%eax
  802274:	89 f1                	mov    %esi,%ecx
  802276:	d3 e8                	shr    %cl,%eax
  802278:	09 d0                	or     %edx,%eax
  80227a:	d3 eb                	shr    %cl,%ebx
  80227c:	89 da                	mov    %ebx,%edx
  80227e:	f7 f7                	div    %edi
  802280:	89 d3                	mov    %edx,%ebx
  802282:	f7 24 24             	mull   (%esp)
  802285:	89 c6                	mov    %eax,%esi
  802287:	89 d1                	mov    %edx,%ecx
  802289:	39 d3                	cmp    %edx,%ebx
  80228b:	0f 82 87 00 00 00    	jb     802318 <__umoddi3+0x134>
  802291:	0f 84 91 00 00 00    	je     802328 <__umoddi3+0x144>
  802297:	8b 54 24 04          	mov    0x4(%esp),%edx
  80229b:	29 f2                	sub    %esi,%edx
  80229d:	19 cb                	sbb    %ecx,%ebx
  80229f:	89 d8                	mov    %ebx,%eax
  8022a1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8022a5:	d3 e0                	shl    %cl,%eax
  8022a7:	89 e9                	mov    %ebp,%ecx
  8022a9:	d3 ea                	shr    %cl,%edx
  8022ab:	09 d0                	or     %edx,%eax
  8022ad:	89 e9                	mov    %ebp,%ecx
  8022af:	d3 eb                	shr    %cl,%ebx
  8022b1:	89 da                	mov    %ebx,%edx
  8022b3:	83 c4 1c             	add    $0x1c,%esp
  8022b6:	5b                   	pop    %ebx
  8022b7:	5e                   	pop    %esi
  8022b8:	5f                   	pop    %edi
  8022b9:	5d                   	pop    %ebp
  8022ba:	c3                   	ret    
  8022bb:	90                   	nop
  8022bc:	89 fd                	mov    %edi,%ebp
  8022be:	85 ff                	test   %edi,%edi
  8022c0:	75 0b                	jne    8022cd <__umoddi3+0xe9>
  8022c2:	b8 01 00 00 00       	mov    $0x1,%eax
  8022c7:	31 d2                	xor    %edx,%edx
  8022c9:	f7 f7                	div    %edi
  8022cb:	89 c5                	mov    %eax,%ebp
  8022cd:	89 f0                	mov    %esi,%eax
  8022cf:	31 d2                	xor    %edx,%edx
  8022d1:	f7 f5                	div    %ebp
  8022d3:	89 c8                	mov    %ecx,%eax
  8022d5:	f7 f5                	div    %ebp
  8022d7:	89 d0                	mov    %edx,%eax
  8022d9:	e9 44 ff ff ff       	jmp    802222 <__umoddi3+0x3e>
  8022de:	66 90                	xchg   %ax,%ax
  8022e0:	89 c8                	mov    %ecx,%eax
  8022e2:	89 f2                	mov    %esi,%edx
  8022e4:	83 c4 1c             	add    $0x1c,%esp
  8022e7:	5b                   	pop    %ebx
  8022e8:	5e                   	pop    %esi
  8022e9:	5f                   	pop    %edi
  8022ea:	5d                   	pop    %ebp
  8022eb:	c3                   	ret    
  8022ec:	3b 04 24             	cmp    (%esp),%eax
  8022ef:	72 06                	jb     8022f7 <__umoddi3+0x113>
  8022f1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8022f5:	77 0f                	ja     802306 <__umoddi3+0x122>
  8022f7:	89 f2                	mov    %esi,%edx
  8022f9:	29 f9                	sub    %edi,%ecx
  8022fb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8022ff:	89 14 24             	mov    %edx,(%esp)
  802302:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  802306:	8b 44 24 04          	mov    0x4(%esp),%eax
  80230a:	8b 14 24             	mov    (%esp),%edx
  80230d:	83 c4 1c             	add    $0x1c,%esp
  802310:	5b                   	pop    %ebx
  802311:	5e                   	pop    %esi
  802312:	5f                   	pop    %edi
  802313:	5d                   	pop    %ebp
  802314:	c3                   	ret    
  802315:	8d 76 00             	lea    0x0(%esi),%esi
  802318:	2b 04 24             	sub    (%esp),%eax
  80231b:	19 fa                	sbb    %edi,%edx
  80231d:	89 d1                	mov    %edx,%ecx
  80231f:	89 c6                	mov    %eax,%esi
  802321:	e9 71 ff ff ff       	jmp    802297 <__umoddi3+0xb3>
  802326:	66 90                	xchg   %ax,%ax
  802328:	39 44 24 04          	cmp    %eax,0x4(%esp)
  80232c:	72 ea                	jb     802318 <__umoddi3+0x134>
  80232e:	89 d9                	mov    %ebx,%ecx
  802330:	e9 62 ff ff ff       	jmp    802297 <__umoddi3+0xb3>
