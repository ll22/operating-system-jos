
obj/user/buggyhello.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 16 00 00 00       	call   800047 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs((char*)1, 1);
  800039:	6a 01                	push   $0x1
  80003b:	6a 01                	push   $0x1
  80003d:	e8 66 00 00 00       	call   8000a8 <sys_cputs>
}
  800042:	83 c4 10             	add    $0x10,%esp
  800045:	c9                   	leave  
  800046:	c3                   	ret    

00800047 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800047:	55                   	push   %ebp
  800048:	89 e5                	mov    %esp,%ebp
  80004a:	56                   	push   %esi
  80004b:	53                   	push   %ebx
  80004c:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004f:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800052:	e8 cf 00 00 00       	call   800126 <sys_getenvid>
  800057:	25 ff 03 00 00       	and    $0x3ff,%eax
  80005c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800063:	c1 e0 07             	shl    $0x7,%eax
  800066:	29 d0                	sub    %edx,%eax
  800068:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80006d:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800072:	85 db                	test   %ebx,%ebx
  800074:	7e 07                	jle    80007d <libmain+0x36>
		binaryname = argv[0];
  800076:	8b 06                	mov    (%esi),%eax
  800078:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80007d:	83 ec 08             	sub    $0x8,%esp
  800080:	56                   	push   %esi
  800081:	53                   	push   %ebx
  800082:	e8 ac ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800087:	e8 0a 00 00 00       	call   800096 <exit>
}
  80008c:	83 c4 10             	add    $0x10,%esp
  80008f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800092:	5b                   	pop    %ebx
  800093:	5e                   	pop    %esi
  800094:	5d                   	pop    %ebp
  800095:	c3                   	ret    

00800096 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800096:	55                   	push   %ebp
  800097:	89 e5                	mov    %esp,%ebp
  800099:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80009c:	6a 00                	push   $0x0
  80009e:	e8 42 00 00 00       	call   8000e5 <sys_env_destroy>
}
  8000a3:	83 c4 10             	add    $0x10,%esp
  8000a6:	c9                   	leave  
  8000a7:	c3                   	ret    

008000a8 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000a8:	55                   	push   %ebp
  8000a9:	89 e5                	mov    %esp,%ebp
  8000ab:	57                   	push   %edi
  8000ac:	56                   	push   %esi
  8000ad:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ae:	b8 00 00 00 00       	mov    $0x0,%eax
  8000b3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b6:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b9:	89 c3                	mov    %eax,%ebx
  8000bb:	89 c7                	mov    %eax,%edi
  8000bd:	89 c6                	mov    %eax,%esi
  8000bf:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c1:	5b                   	pop    %ebx
  8000c2:	5e                   	pop    %esi
  8000c3:	5f                   	pop    %edi
  8000c4:	5d                   	pop    %ebp
  8000c5:	c3                   	ret    

008000c6 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c6:	55                   	push   %ebp
  8000c7:	89 e5                	mov    %esp,%ebp
  8000c9:	57                   	push   %edi
  8000ca:	56                   	push   %esi
  8000cb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cc:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d1:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d6:	89 d1                	mov    %edx,%ecx
  8000d8:	89 d3                	mov    %edx,%ebx
  8000da:	89 d7                	mov    %edx,%edi
  8000dc:	89 d6                	mov    %edx,%esi
  8000de:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e0:	5b                   	pop    %ebx
  8000e1:	5e                   	pop    %esi
  8000e2:	5f                   	pop    %edi
  8000e3:	5d                   	pop    %ebp
  8000e4:	c3                   	ret    

008000e5 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e5:	55                   	push   %ebp
  8000e6:	89 e5                	mov    %esp,%ebp
  8000e8:	57                   	push   %edi
  8000e9:	56                   	push   %esi
  8000ea:	53                   	push   %ebx
  8000eb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ee:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000f3:	b8 03 00 00 00       	mov    $0x3,%eax
  8000f8:	8b 55 08             	mov    0x8(%ebp),%edx
  8000fb:	89 cb                	mov    %ecx,%ebx
  8000fd:	89 cf                	mov    %ecx,%edi
  8000ff:	89 ce                	mov    %ecx,%esi
  800101:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800103:	85 c0                	test   %eax,%eax
  800105:	7e 17                	jle    80011e <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800107:	83 ec 0c             	sub    $0xc,%esp
  80010a:	50                   	push   %eax
  80010b:	6a 03                	push   $0x3
  80010d:	68 4a 0f 80 00       	push   $0x800f4a
  800112:	6a 23                	push   $0x23
  800114:	68 67 0f 80 00       	push   $0x800f67
  800119:	e8 56 02 00 00       	call   800374 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80011e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800121:	5b                   	pop    %ebx
  800122:	5e                   	pop    %esi
  800123:	5f                   	pop    %edi
  800124:	5d                   	pop    %ebp
  800125:	c3                   	ret    

00800126 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800126:	55                   	push   %ebp
  800127:	89 e5                	mov    %esp,%ebp
  800129:	57                   	push   %edi
  80012a:	56                   	push   %esi
  80012b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012c:	ba 00 00 00 00       	mov    $0x0,%edx
  800131:	b8 02 00 00 00       	mov    $0x2,%eax
  800136:	89 d1                	mov    %edx,%ecx
  800138:	89 d3                	mov    %edx,%ebx
  80013a:	89 d7                	mov    %edx,%edi
  80013c:	89 d6                	mov    %edx,%esi
  80013e:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800140:	5b                   	pop    %ebx
  800141:	5e                   	pop    %esi
  800142:	5f                   	pop    %edi
  800143:	5d                   	pop    %ebp
  800144:	c3                   	ret    

00800145 <sys_yield>:

void
sys_yield(void)
{
  800145:	55                   	push   %ebp
  800146:	89 e5                	mov    %esp,%ebp
  800148:	57                   	push   %edi
  800149:	56                   	push   %esi
  80014a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80014b:	ba 00 00 00 00       	mov    $0x0,%edx
  800150:	b8 0b 00 00 00       	mov    $0xb,%eax
  800155:	89 d1                	mov    %edx,%ecx
  800157:	89 d3                	mov    %edx,%ebx
  800159:	89 d7                	mov    %edx,%edi
  80015b:	89 d6                	mov    %edx,%esi
  80015d:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  80015f:	5b                   	pop    %ebx
  800160:	5e                   	pop    %esi
  800161:	5f                   	pop    %edi
  800162:	5d                   	pop    %ebp
  800163:	c3                   	ret    

00800164 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800164:	55                   	push   %ebp
  800165:	89 e5                	mov    %esp,%ebp
  800167:	57                   	push   %edi
  800168:	56                   	push   %esi
  800169:	53                   	push   %ebx
  80016a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80016d:	be 00 00 00 00       	mov    $0x0,%esi
  800172:	b8 04 00 00 00       	mov    $0x4,%eax
  800177:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80017a:	8b 55 08             	mov    0x8(%ebp),%edx
  80017d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800180:	89 f7                	mov    %esi,%edi
  800182:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800184:	85 c0                	test   %eax,%eax
  800186:	7e 17                	jle    80019f <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800188:	83 ec 0c             	sub    $0xc,%esp
  80018b:	50                   	push   %eax
  80018c:	6a 04                	push   $0x4
  80018e:	68 4a 0f 80 00       	push   $0x800f4a
  800193:	6a 23                	push   $0x23
  800195:	68 67 0f 80 00       	push   $0x800f67
  80019a:	e8 d5 01 00 00       	call   800374 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  80019f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a2:	5b                   	pop    %ebx
  8001a3:	5e                   	pop    %esi
  8001a4:	5f                   	pop    %edi
  8001a5:	5d                   	pop    %ebp
  8001a6:	c3                   	ret    

008001a7 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  8001a7:	55                   	push   %ebp
  8001a8:	89 e5                	mov    %esp,%ebp
  8001aa:	57                   	push   %edi
  8001ab:	56                   	push   %esi
  8001ac:	53                   	push   %ebx
  8001ad:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001b0:	b8 05 00 00 00       	mov    $0x5,%eax
  8001b5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001b8:	8b 55 08             	mov    0x8(%ebp),%edx
  8001bb:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001be:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001c1:	8b 75 18             	mov    0x18(%ebp),%esi
  8001c4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001c6:	85 c0                	test   %eax,%eax
  8001c8:	7e 17                	jle    8001e1 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001ca:	83 ec 0c             	sub    $0xc,%esp
  8001cd:	50                   	push   %eax
  8001ce:	6a 05                	push   $0x5
  8001d0:	68 4a 0f 80 00       	push   $0x800f4a
  8001d5:	6a 23                	push   $0x23
  8001d7:	68 67 0f 80 00       	push   $0x800f67
  8001dc:	e8 93 01 00 00       	call   800374 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001e1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001e4:	5b                   	pop    %ebx
  8001e5:	5e                   	pop    %esi
  8001e6:	5f                   	pop    %edi
  8001e7:	5d                   	pop    %ebp
  8001e8:	c3                   	ret    

008001e9 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001e9:	55                   	push   %ebp
  8001ea:	89 e5                	mov    %esp,%ebp
  8001ec:	57                   	push   %edi
  8001ed:	56                   	push   %esi
  8001ee:	53                   	push   %ebx
  8001ef:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001f2:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001f7:	b8 06 00 00 00       	mov    $0x6,%eax
  8001fc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001ff:	8b 55 08             	mov    0x8(%ebp),%edx
  800202:	89 df                	mov    %ebx,%edi
  800204:	89 de                	mov    %ebx,%esi
  800206:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800208:	85 c0                	test   %eax,%eax
  80020a:	7e 17                	jle    800223 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80020c:	83 ec 0c             	sub    $0xc,%esp
  80020f:	50                   	push   %eax
  800210:	6a 06                	push   $0x6
  800212:	68 4a 0f 80 00       	push   $0x800f4a
  800217:	6a 23                	push   $0x23
  800219:	68 67 0f 80 00       	push   $0x800f67
  80021e:	e8 51 01 00 00       	call   800374 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800223:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800226:	5b                   	pop    %ebx
  800227:	5e                   	pop    %esi
  800228:	5f                   	pop    %edi
  800229:	5d                   	pop    %ebp
  80022a:	c3                   	ret    

0080022b <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80022b:	55                   	push   %ebp
  80022c:	89 e5                	mov    %esp,%ebp
  80022e:	57                   	push   %edi
  80022f:	56                   	push   %esi
  800230:	53                   	push   %ebx
  800231:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800234:	bb 00 00 00 00       	mov    $0x0,%ebx
  800239:	b8 08 00 00 00       	mov    $0x8,%eax
  80023e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800241:	8b 55 08             	mov    0x8(%ebp),%edx
  800244:	89 df                	mov    %ebx,%edi
  800246:	89 de                	mov    %ebx,%esi
  800248:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80024a:	85 c0                	test   %eax,%eax
  80024c:	7e 17                	jle    800265 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  80024e:	83 ec 0c             	sub    $0xc,%esp
  800251:	50                   	push   %eax
  800252:	6a 08                	push   $0x8
  800254:	68 4a 0f 80 00       	push   $0x800f4a
  800259:	6a 23                	push   $0x23
  80025b:	68 67 0f 80 00       	push   $0x800f67
  800260:	e8 0f 01 00 00       	call   800374 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800265:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800268:	5b                   	pop    %ebx
  800269:	5e                   	pop    %esi
  80026a:	5f                   	pop    %edi
  80026b:	5d                   	pop    %ebp
  80026c:	c3                   	ret    

0080026d <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80026d:	55                   	push   %ebp
  80026e:	89 e5                	mov    %esp,%ebp
  800270:	57                   	push   %edi
  800271:	56                   	push   %esi
  800272:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800273:	ba 00 00 00 00       	mov    $0x0,%edx
  800278:	b8 0c 00 00 00       	mov    $0xc,%eax
  80027d:	89 d1                	mov    %edx,%ecx
  80027f:	89 d3                	mov    %edx,%ebx
  800281:	89 d7                	mov    %edx,%edi
  800283:	89 d6                	mov    %edx,%esi
  800285:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800287:	5b                   	pop    %ebx
  800288:	5e                   	pop    %esi
  800289:	5f                   	pop    %edi
  80028a:	5d                   	pop    %ebp
  80028b:	c3                   	ret    

0080028c <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  80028c:	55                   	push   %ebp
  80028d:	89 e5                	mov    %esp,%ebp
  80028f:	57                   	push   %edi
  800290:	56                   	push   %esi
  800291:	53                   	push   %ebx
  800292:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800295:	bb 00 00 00 00       	mov    $0x0,%ebx
  80029a:	b8 09 00 00 00       	mov    $0x9,%eax
  80029f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002a2:	8b 55 08             	mov    0x8(%ebp),%edx
  8002a5:	89 df                	mov    %ebx,%edi
  8002a7:	89 de                	mov    %ebx,%esi
  8002a9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002ab:	85 c0                	test   %eax,%eax
  8002ad:	7e 17                	jle    8002c6 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002af:	83 ec 0c             	sub    $0xc,%esp
  8002b2:	50                   	push   %eax
  8002b3:	6a 09                	push   $0x9
  8002b5:	68 4a 0f 80 00       	push   $0x800f4a
  8002ba:	6a 23                	push   $0x23
  8002bc:	68 67 0f 80 00       	push   $0x800f67
  8002c1:	e8 ae 00 00 00       	call   800374 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002c6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c9:	5b                   	pop    %ebx
  8002ca:	5e                   	pop    %esi
  8002cb:	5f                   	pop    %edi
  8002cc:	5d                   	pop    %ebp
  8002cd:	c3                   	ret    

008002ce <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002ce:	55                   	push   %ebp
  8002cf:	89 e5                	mov    %esp,%ebp
  8002d1:	57                   	push   %edi
  8002d2:	56                   	push   %esi
  8002d3:	53                   	push   %ebx
  8002d4:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002d7:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002dc:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002e1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002e4:	8b 55 08             	mov    0x8(%ebp),%edx
  8002e7:	89 df                	mov    %ebx,%edi
  8002e9:	89 de                	mov    %ebx,%esi
  8002eb:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002ed:	85 c0                	test   %eax,%eax
  8002ef:	7e 17                	jle    800308 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002f1:	83 ec 0c             	sub    $0xc,%esp
  8002f4:	50                   	push   %eax
  8002f5:	6a 0a                	push   $0xa
  8002f7:	68 4a 0f 80 00       	push   $0x800f4a
  8002fc:	6a 23                	push   $0x23
  8002fe:	68 67 0f 80 00       	push   $0x800f67
  800303:	e8 6c 00 00 00       	call   800374 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800308:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80030b:	5b                   	pop    %ebx
  80030c:	5e                   	pop    %esi
  80030d:	5f                   	pop    %edi
  80030e:	5d                   	pop    %ebp
  80030f:	c3                   	ret    

00800310 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800310:	55                   	push   %ebp
  800311:	89 e5                	mov    %esp,%ebp
  800313:	57                   	push   %edi
  800314:	56                   	push   %esi
  800315:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800316:	be 00 00 00 00       	mov    $0x0,%esi
  80031b:	b8 0d 00 00 00       	mov    $0xd,%eax
  800320:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800323:	8b 55 08             	mov    0x8(%ebp),%edx
  800326:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800329:	8b 7d 14             	mov    0x14(%ebp),%edi
  80032c:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  80032e:	5b                   	pop    %ebx
  80032f:	5e                   	pop    %esi
  800330:	5f                   	pop    %edi
  800331:	5d                   	pop    %ebp
  800332:	c3                   	ret    

00800333 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800333:	55                   	push   %ebp
  800334:	89 e5                	mov    %esp,%ebp
  800336:	57                   	push   %edi
  800337:	56                   	push   %esi
  800338:	53                   	push   %ebx
  800339:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80033c:	b9 00 00 00 00       	mov    $0x0,%ecx
  800341:	b8 0e 00 00 00       	mov    $0xe,%eax
  800346:	8b 55 08             	mov    0x8(%ebp),%edx
  800349:	89 cb                	mov    %ecx,%ebx
  80034b:	89 cf                	mov    %ecx,%edi
  80034d:	89 ce                	mov    %ecx,%esi
  80034f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800351:	85 c0                	test   %eax,%eax
  800353:	7e 17                	jle    80036c <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800355:	83 ec 0c             	sub    $0xc,%esp
  800358:	50                   	push   %eax
  800359:	6a 0e                	push   $0xe
  80035b:	68 4a 0f 80 00       	push   $0x800f4a
  800360:	6a 23                	push   $0x23
  800362:	68 67 0f 80 00       	push   $0x800f67
  800367:	e8 08 00 00 00       	call   800374 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80036c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80036f:	5b                   	pop    %ebx
  800370:	5e                   	pop    %esi
  800371:	5f                   	pop    %edi
  800372:	5d                   	pop    %ebp
  800373:	c3                   	ret    

00800374 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800374:	55                   	push   %ebp
  800375:	89 e5                	mov    %esp,%ebp
  800377:	56                   	push   %esi
  800378:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800379:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80037c:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800382:	e8 9f fd ff ff       	call   800126 <sys_getenvid>
  800387:	83 ec 0c             	sub    $0xc,%esp
  80038a:	ff 75 0c             	pushl  0xc(%ebp)
  80038d:	ff 75 08             	pushl  0x8(%ebp)
  800390:	56                   	push   %esi
  800391:	50                   	push   %eax
  800392:	68 78 0f 80 00       	push   $0x800f78
  800397:	e8 b0 00 00 00       	call   80044c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80039c:	83 c4 18             	add    $0x18,%esp
  80039f:	53                   	push   %ebx
  8003a0:	ff 75 10             	pushl  0x10(%ebp)
  8003a3:	e8 53 00 00 00       	call   8003fb <vcprintf>
	cprintf("\n");
  8003a8:	c7 04 24 9b 0f 80 00 	movl   $0x800f9b,(%esp)
  8003af:	e8 98 00 00 00       	call   80044c <cprintf>
  8003b4:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003b7:	cc                   	int3   
  8003b8:	eb fd                	jmp    8003b7 <_panic+0x43>

008003ba <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003ba:	55                   	push   %ebp
  8003bb:	89 e5                	mov    %esp,%ebp
  8003bd:	53                   	push   %ebx
  8003be:	83 ec 04             	sub    $0x4,%esp
  8003c1:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003c4:	8b 13                	mov    (%ebx),%edx
  8003c6:	8d 42 01             	lea    0x1(%edx),%eax
  8003c9:	89 03                	mov    %eax,(%ebx)
  8003cb:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003ce:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003d2:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003d7:	75 1a                	jne    8003f3 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003d9:	83 ec 08             	sub    $0x8,%esp
  8003dc:	68 ff 00 00 00       	push   $0xff
  8003e1:	8d 43 08             	lea    0x8(%ebx),%eax
  8003e4:	50                   	push   %eax
  8003e5:	e8 be fc ff ff       	call   8000a8 <sys_cputs>
		b->idx = 0;
  8003ea:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003f0:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003f3:	ff 43 04             	incl   0x4(%ebx)
}
  8003f6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003f9:	c9                   	leave  
  8003fa:	c3                   	ret    

008003fb <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003fb:	55                   	push   %ebp
  8003fc:	89 e5                	mov    %esp,%ebp
  8003fe:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800404:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80040b:	00 00 00 
	b.cnt = 0;
  80040e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800415:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800418:	ff 75 0c             	pushl  0xc(%ebp)
  80041b:	ff 75 08             	pushl  0x8(%ebp)
  80041e:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800424:	50                   	push   %eax
  800425:	68 ba 03 80 00       	push   $0x8003ba
  80042a:	e8 51 01 00 00       	call   800580 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80042f:	83 c4 08             	add    $0x8,%esp
  800432:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800438:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80043e:	50                   	push   %eax
  80043f:	e8 64 fc ff ff       	call   8000a8 <sys_cputs>

	return b.cnt;
}
  800444:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80044a:	c9                   	leave  
  80044b:	c3                   	ret    

0080044c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80044c:	55                   	push   %ebp
  80044d:	89 e5                	mov    %esp,%ebp
  80044f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800452:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800455:	50                   	push   %eax
  800456:	ff 75 08             	pushl  0x8(%ebp)
  800459:	e8 9d ff ff ff       	call   8003fb <vcprintf>
	va_end(ap);

	return cnt;
}
  80045e:	c9                   	leave  
  80045f:	c3                   	ret    

00800460 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800460:	55                   	push   %ebp
  800461:	89 e5                	mov    %esp,%ebp
  800463:	57                   	push   %edi
  800464:	56                   	push   %esi
  800465:	53                   	push   %ebx
  800466:	83 ec 1c             	sub    $0x1c,%esp
  800469:	89 c7                	mov    %eax,%edi
  80046b:	89 d6                	mov    %edx,%esi
  80046d:	8b 45 08             	mov    0x8(%ebp),%eax
  800470:	8b 55 0c             	mov    0xc(%ebp),%edx
  800473:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800476:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800479:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80047c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800481:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800484:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800487:	39 d3                	cmp    %edx,%ebx
  800489:	72 05                	jb     800490 <printnum+0x30>
  80048b:	39 45 10             	cmp    %eax,0x10(%ebp)
  80048e:	77 45                	ja     8004d5 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800490:	83 ec 0c             	sub    $0xc,%esp
  800493:	ff 75 18             	pushl  0x18(%ebp)
  800496:	8b 45 14             	mov    0x14(%ebp),%eax
  800499:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80049c:	53                   	push   %ebx
  80049d:	ff 75 10             	pushl  0x10(%ebp)
  8004a0:	83 ec 08             	sub    $0x8,%esp
  8004a3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a6:	ff 75 e0             	pushl  -0x20(%ebp)
  8004a9:	ff 75 dc             	pushl  -0x24(%ebp)
  8004ac:	ff 75 d8             	pushl  -0x28(%ebp)
  8004af:	e8 24 08 00 00       	call   800cd8 <__udivdi3>
  8004b4:	83 c4 18             	add    $0x18,%esp
  8004b7:	52                   	push   %edx
  8004b8:	50                   	push   %eax
  8004b9:	89 f2                	mov    %esi,%edx
  8004bb:	89 f8                	mov    %edi,%eax
  8004bd:	e8 9e ff ff ff       	call   800460 <printnum>
  8004c2:	83 c4 20             	add    $0x20,%esp
  8004c5:	eb 16                	jmp    8004dd <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004c7:	83 ec 08             	sub    $0x8,%esp
  8004ca:	56                   	push   %esi
  8004cb:	ff 75 18             	pushl  0x18(%ebp)
  8004ce:	ff d7                	call   *%edi
  8004d0:	83 c4 10             	add    $0x10,%esp
  8004d3:	eb 03                	jmp    8004d8 <printnum+0x78>
  8004d5:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004d8:	4b                   	dec    %ebx
  8004d9:	85 db                	test   %ebx,%ebx
  8004db:	7f ea                	jg     8004c7 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004dd:	83 ec 08             	sub    $0x8,%esp
  8004e0:	56                   	push   %esi
  8004e1:	83 ec 04             	sub    $0x4,%esp
  8004e4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004e7:	ff 75 e0             	pushl  -0x20(%ebp)
  8004ea:	ff 75 dc             	pushl  -0x24(%ebp)
  8004ed:	ff 75 d8             	pushl  -0x28(%ebp)
  8004f0:	e8 f3 08 00 00       	call   800de8 <__umoddi3>
  8004f5:	83 c4 14             	add    $0x14,%esp
  8004f8:	0f be 80 9d 0f 80 00 	movsbl 0x800f9d(%eax),%eax
  8004ff:	50                   	push   %eax
  800500:	ff d7                	call   *%edi
}
  800502:	83 c4 10             	add    $0x10,%esp
  800505:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800508:	5b                   	pop    %ebx
  800509:	5e                   	pop    %esi
  80050a:	5f                   	pop    %edi
  80050b:	5d                   	pop    %ebp
  80050c:	c3                   	ret    

0080050d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80050d:	55                   	push   %ebp
  80050e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800510:	83 fa 01             	cmp    $0x1,%edx
  800513:	7e 0e                	jle    800523 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800515:	8b 10                	mov    (%eax),%edx
  800517:	8d 4a 08             	lea    0x8(%edx),%ecx
  80051a:	89 08                	mov    %ecx,(%eax)
  80051c:	8b 02                	mov    (%edx),%eax
  80051e:	8b 52 04             	mov    0x4(%edx),%edx
  800521:	eb 22                	jmp    800545 <getuint+0x38>
	else if (lflag)
  800523:	85 d2                	test   %edx,%edx
  800525:	74 10                	je     800537 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800527:	8b 10                	mov    (%eax),%edx
  800529:	8d 4a 04             	lea    0x4(%edx),%ecx
  80052c:	89 08                	mov    %ecx,(%eax)
  80052e:	8b 02                	mov    (%edx),%eax
  800530:	ba 00 00 00 00       	mov    $0x0,%edx
  800535:	eb 0e                	jmp    800545 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800537:	8b 10                	mov    (%eax),%edx
  800539:	8d 4a 04             	lea    0x4(%edx),%ecx
  80053c:	89 08                	mov    %ecx,(%eax)
  80053e:	8b 02                	mov    (%edx),%eax
  800540:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800545:	5d                   	pop    %ebp
  800546:	c3                   	ret    

00800547 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800547:	55                   	push   %ebp
  800548:	89 e5                	mov    %esp,%ebp
  80054a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80054d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800550:	8b 10                	mov    (%eax),%edx
  800552:	3b 50 04             	cmp    0x4(%eax),%edx
  800555:	73 0a                	jae    800561 <sprintputch+0x1a>
		*b->buf++ = ch;
  800557:	8d 4a 01             	lea    0x1(%edx),%ecx
  80055a:	89 08                	mov    %ecx,(%eax)
  80055c:	8b 45 08             	mov    0x8(%ebp),%eax
  80055f:	88 02                	mov    %al,(%edx)
}
  800561:	5d                   	pop    %ebp
  800562:	c3                   	ret    

00800563 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800563:	55                   	push   %ebp
  800564:	89 e5                	mov    %esp,%ebp
  800566:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800569:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80056c:	50                   	push   %eax
  80056d:	ff 75 10             	pushl  0x10(%ebp)
  800570:	ff 75 0c             	pushl  0xc(%ebp)
  800573:	ff 75 08             	pushl  0x8(%ebp)
  800576:	e8 05 00 00 00       	call   800580 <vprintfmt>
	va_end(ap);
}
  80057b:	83 c4 10             	add    $0x10,%esp
  80057e:	c9                   	leave  
  80057f:	c3                   	ret    

00800580 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800580:	55                   	push   %ebp
  800581:	89 e5                	mov    %esp,%ebp
  800583:	57                   	push   %edi
  800584:	56                   	push   %esi
  800585:	53                   	push   %ebx
  800586:	83 ec 2c             	sub    $0x2c,%esp
  800589:	8b 75 08             	mov    0x8(%ebp),%esi
  80058c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80058f:	8b 7d 10             	mov    0x10(%ebp),%edi
  800592:	eb 12                	jmp    8005a6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800594:	85 c0                	test   %eax,%eax
  800596:	0f 84 68 03 00 00    	je     800904 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80059c:	83 ec 08             	sub    $0x8,%esp
  80059f:	53                   	push   %ebx
  8005a0:	50                   	push   %eax
  8005a1:	ff d6                	call   *%esi
  8005a3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8005a6:	47                   	inc    %edi
  8005a7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8005ab:	83 f8 25             	cmp    $0x25,%eax
  8005ae:	75 e4                	jne    800594 <vprintfmt+0x14>
  8005b0:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005b4:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005bb:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005c2:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005c9:	ba 00 00 00 00       	mov    $0x0,%edx
  8005ce:	eb 07                	jmp    8005d7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d0:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005d3:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d7:	8d 47 01             	lea    0x1(%edi),%eax
  8005da:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005dd:	0f b6 0f             	movzbl (%edi),%ecx
  8005e0:	8a 07                	mov    (%edi),%al
  8005e2:	83 e8 23             	sub    $0x23,%eax
  8005e5:	3c 55                	cmp    $0x55,%al
  8005e7:	0f 87 fe 02 00 00    	ja     8008eb <vprintfmt+0x36b>
  8005ed:	0f b6 c0             	movzbl %al,%eax
  8005f0:	ff 24 85 e0 10 80 00 	jmp    *0x8010e0(,%eax,4)
  8005f7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005fa:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005fe:	eb d7                	jmp    8005d7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800600:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800603:	b8 00 00 00 00       	mov    $0x0,%eax
  800608:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80060b:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80060e:	01 c0                	add    %eax,%eax
  800610:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800614:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800617:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80061a:	83 fa 09             	cmp    $0x9,%edx
  80061d:	77 34                	ja     800653 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80061f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800620:	eb e9                	jmp    80060b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800622:	8b 45 14             	mov    0x14(%ebp),%eax
  800625:	8d 48 04             	lea    0x4(%eax),%ecx
  800628:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80062b:	8b 00                	mov    (%eax),%eax
  80062d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800630:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800633:	eb 24                	jmp    800659 <vprintfmt+0xd9>
  800635:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800639:	79 07                	jns    800642 <vprintfmt+0xc2>
  80063b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800642:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800645:	eb 90                	jmp    8005d7 <vprintfmt+0x57>
  800647:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80064a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800651:	eb 84                	jmp    8005d7 <vprintfmt+0x57>
  800653:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800656:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800659:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80065d:	0f 89 74 ff ff ff    	jns    8005d7 <vprintfmt+0x57>
				width = precision, precision = -1;
  800663:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800666:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800669:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800670:	e9 62 ff ff ff       	jmp    8005d7 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800675:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800676:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800679:	e9 59 ff ff ff       	jmp    8005d7 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80067e:	8b 45 14             	mov    0x14(%ebp),%eax
  800681:	8d 50 04             	lea    0x4(%eax),%edx
  800684:	89 55 14             	mov    %edx,0x14(%ebp)
  800687:	83 ec 08             	sub    $0x8,%esp
  80068a:	53                   	push   %ebx
  80068b:	ff 30                	pushl  (%eax)
  80068d:	ff d6                	call   *%esi
			break;
  80068f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800692:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800695:	e9 0c ff ff ff       	jmp    8005a6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80069a:	8b 45 14             	mov    0x14(%ebp),%eax
  80069d:	8d 50 04             	lea    0x4(%eax),%edx
  8006a0:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a3:	8b 00                	mov    (%eax),%eax
  8006a5:	85 c0                	test   %eax,%eax
  8006a7:	79 02                	jns    8006ab <vprintfmt+0x12b>
  8006a9:	f7 d8                	neg    %eax
  8006ab:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8006ad:	83 f8 0f             	cmp    $0xf,%eax
  8006b0:	7f 0b                	jg     8006bd <vprintfmt+0x13d>
  8006b2:	8b 04 85 40 12 80 00 	mov    0x801240(,%eax,4),%eax
  8006b9:	85 c0                	test   %eax,%eax
  8006bb:	75 18                	jne    8006d5 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006bd:	52                   	push   %edx
  8006be:	68 b5 0f 80 00       	push   $0x800fb5
  8006c3:	53                   	push   %ebx
  8006c4:	56                   	push   %esi
  8006c5:	e8 99 fe ff ff       	call   800563 <printfmt>
  8006ca:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006cd:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006d0:	e9 d1 fe ff ff       	jmp    8005a6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006d5:	50                   	push   %eax
  8006d6:	68 be 0f 80 00       	push   $0x800fbe
  8006db:	53                   	push   %ebx
  8006dc:	56                   	push   %esi
  8006dd:	e8 81 fe ff ff       	call   800563 <printfmt>
  8006e2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006e5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006e8:	e9 b9 fe ff ff       	jmp    8005a6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006ed:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f0:	8d 50 04             	lea    0x4(%eax),%edx
  8006f3:	89 55 14             	mov    %edx,0x14(%ebp)
  8006f6:	8b 38                	mov    (%eax),%edi
  8006f8:	85 ff                	test   %edi,%edi
  8006fa:	75 05                	jne    800701 <vprintfmt+0x181>
				p = "(null)";
  8006fc:	bf ae 0f 80 00       	mov    $0x800fae,%edi
			if (width > 0 && padc != '-')
  800701:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800705:	0f 8e 90 00 00 00    	jle    80079b <vprintfmt+0x21b>
  80070b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80070f:	0f 84 8e 00 00 00    	je     8007a3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800715:	83 ec 08             	sub    $0x8,%esp
  800718:	ff 75 d0             	pushl  -0x30(%ebp)
  80071b:	57                   	push   %edi
  80071c:	e8 70 02 00 00       	call   800991 <strnlen>
  800721:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800724:	29 c1                	sub    %eax,%ecx
  800726:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800729:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80072c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800730:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800733:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800736:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800738:	eb 0d                	jmp    800747 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80073a:	83 ec 08             	sub    $0x8,%esp
  80073d:	53                   	push   %ebx
  80073e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800741:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800743:	4f                   	dec    %edi
  800744:	83 c4 10             	add    $0x10,%esp
  800747:	85 ff                	test   %edi,%edi
  800749:	7f ef                	jg     80073a <vprintfmt+0x1ba>
  80074b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80074e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800751:	89 c8                	mov    %ecx,%eax
  800753:	85 c9                	test   %ecx,%ecx
  800755:	79 05                	jns    80075c <vprintfmt+0x1dc>
  800757:	b8 00 00 00 00       	mov    $0x0,%eax
  80075c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80075f:	29 c1                	sub    %eax,%ecx
  800761:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800764:	89 75 08             	mov    %esi,0x8(%ebp)
  800767:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80076a:	eb 3d                	jmp    8007a9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80076c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800770:	74 19                	je     80078b <vprintfmt+0x20b>
  800772:	0f be c0             	movsbl %al,%eax
  800775:	83 e8 20             	sub    $0x20,%eax
  800778:	83 f8 5e             	cmp    $0x5e,%eax
  80077b:	76 0e                	jbe    80078b <vprintfmt+0x20b>
					putch('?', putdat);
  80077d:	83 ec 08             	sub    $0x8,%esp
  800780:	53                   	push   %ebx
  800781:	6a 3f                	push   $0x3f
  800783:	ff 55 08             	call   *0x8(%ebp)
  800786:	83 c4 10             	add    $0x10,%esp
  800789:	eb 0b                	jmp    800796 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80078b:	83 ec 08             	sub    $0x8,%esp
  80078e:	53                   	push   %ebx
  80078f:	52                   	push   %edx
  800790:	ff 55 08             	call   *0x8(%ebp)
  800793:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800796:	ff 4d e4             	decl   -0x1c(%ebp)
  800799:	eb 0e                	jmp    8007a9 <vprintfmt+0x229>
  80079b:	89 75 08             	mov    %esi,0x8(%ebp)
  80079e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007a1:	eb 06                	jmp    8007a9 <vprintfmt+0x229>
  8007a3:	89 75 08             	mov    %esi,0x8(%ebp)
  8007a6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8007a9:	47                   	inc    %edi
  8007aa:	8a 47 ff             	mov    -0x1(%edi),%al
  8007ad:	0f be d0             	movsbl %al,%edx
  8007b0:	85 d2                	test   %edx,%edx
  8007b2:	74 1d                	je     8007d1 <vprintfmt+0x251>
  8007b4:	85 f6                	test   %esi,%esi
  8007b6:	78 b4                	js     80076c <vprintfmt+0x1ec>
  8007b8:	4e                   	dec    %esi
  8007b9:	79 b1                	jns    80076c <vprintfmt+0x1ec>
  8007bb:	8b 75 08             	mov    0x8(%ebp),%esi
  8007be:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007c1:	eb 14                	jmp    8007d7 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007c3:	83 ec 08             	sub    $0x8,%esp
  8007c6:	53                   	push   %ebx
  8007c7:	6a 20                	push   $0x20
  8007c9:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007cb:	4f                   	dec    %edi
  8007cc:	83 c4 10             	add    $0x10,%esp
  8007cf:	eb 06                	jmp    8007d7 <vprintfmt+0x257>
  8007d1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007d4:	8b 75 08             	mov    0x8(%ebp),%esi
  8007d7:	85 ff                	test   %edi,%edi
  8007d9:	7f e8                	jg     8007c3 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007db:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007de:	e9 c3 fd ff ff       	jmp    8005a6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007e3:	83 fa 01             	cmp    $0x1,%edx
  8007e6:	7e 16                	jle    8007fe <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007e8:	8b 45 14             	mov    0x14(%ebp),%eax
  8007eb:	8d 50 08             	lea    0x8(%eax),%edx
  8007ee:	89 55 14             	mov    %edx,0x14(%ebp)
  8007f1:	8b 50 04             	mov    0x4(%eax),%edx
  8007f4:	8b 00                	mov    (%eax),%eax
  8007f6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007f9:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007fc:	eb 32                	jmp    800830 <vprintfmt+0x2b0>
	else if (lflag)
  8007fe:	85 d2                	test   %edx,%edx
  800800:	74 18                	je     80081a <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800802:	8b 45 14             	mov    0x14(%ebp),%eax
  800805:	8d 50 04             	lea    0x4(%eax),%edx
  800808:	89 55 14             	mov    %edx,0x14(%ebp)
  80080b:	8b 00                	mov    (%eax),%eax
  80080d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800810:	89 c1                	mov    %eax,%ecx
  800812:	c1 f9 1f             	sar    $0x1f,%ecx
  800815:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800818:	eb 16                	jmp    800830 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80081a:	8b 45 14             	mov    0x14(%ebp),%eax
  80081d:	8d 50 04             	lea    0x4(%eax),%edx
  800820:	89 55 14             	mov    %edx,0x14(%ebp)
  800823:	8b 00                	mov    (%eax),%eax
  800825:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800828:	89 c1                	mov    %eax,%ecx
  80082a:	c1 f9 1f             	sar    $0x1f,%ecx
  80082d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800830:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800833:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800836:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80083a:	79 76                	jns    8008b2 <vprintfmt+0x332>
				putch('-', putdat);
  80083c:	83 ec 08             	sub    $0x8,%esp
  80083f:	53                   	push   %ebx
  800840:	6a 2d                	push   $0x2d
  800842:	ff d6                	call   *%esi
				num = -(long long) num;
  800844:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800847:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80084a:	f7 d8                	neg    %eax
  80084c:	83 d2 00             	adc    $0x0,%edx
  80084f:	f7 da                	neg    %edx
  800851:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800854:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800859:	eb 5c                	jmp    8008b7 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80085b:	8d 45 14             	lea    0x14(%ebp),%eax
  80085e:	e8 aa fc ff ff       	call   80050d <getuint>
			base = 10;
  800863:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800868:	eb 4d                	jmp    8008b7 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80086a:	8d 45 14             	lea    0x14(%ebp),%eax
  80086d:	e8 9b fc ff ff       	call   80050d <getuint>
			base = 8;
  800872:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800877:	eb 3e                	jmp    8008b7 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800879:	83 ec 08             	sub    $0x8,%esp
  80087c:	53                   	push   %ebx
  80087d:	6a 30                	push   $0x30
  80087f:	ff d6                	call   *%esi
			putch('x', putdat);
  800881:	83 c4 08             	add    $0x8,%esp
  800884:	53                   	push   %ebx
  800885:	6a 78                	push   $0x78
  800887:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800889:	8b 45 14             	mov    0x14(%ebp),%eax
  80088c:	8d 50 04             	lea    0x4(%eax),%edx
  80088f:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800892:	8b 00                	mov    (%eax),%eax
  800894:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800899:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80089c:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8008a1:	eb 14                	jmp    8008b7 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8008a3:	8d 45 14             	lea    0x14(%ebp),%eax
  8008a6:	e8 62 fc ff ff       	call   80050d <getuint>
			base = 16;
  8008ab:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008b0:	eb 05                	jmp    8008b7 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008b2:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008b7:	83 ec 0c             	sub    $0xc,%esp
  8008ba:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008be:	57                   	push   %edi
  8008bf:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008c2:	51                   	push   %ecx
  8008c3:	52                   	push   %edx
  8008c4:	50                   	push   %eax
  8008c5:	89 da                	mov    %ebx,%edx
  8008c7:	89 f0                	mov    %esi,%eax
  8008c9:	e8 92 fb ff ff       	call   800460 <printnum>
			break;
  8008ce:	83 c4 20             	add    $0x20,%esp
  8008d1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008d4:	e9 cd fc ff ff       	jmp    8005a6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008d9:	83 ec 08             	sub    $0x8,%esp
  8008dc:	53                   	push   %ebx
  8008dd:	51                   	push   %ecx
  8008de:	ff d6                	call   *%esi
			break;
  8008e0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008e3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008e6:	e9 bb fc ff ff       	jmp    8005a6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008eb:	83 ec 08             	sub    $0x8,%esp
  8008ee:	53                   	push   %ebx
  8008ef:	6a 25                	push   $0x25
  8008f1:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008f3:	83 c4 10             	add    $0x10,%esp
  8008f6:	eb 01                	jmp    8008f9 <vprintfmt+0x379>
  8008f8:	4f                   	dec    %edi
  8008f9:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008fd:	75 f9                	jne    8008f8 <vprintfmt+0x378>
  8008ff:	e9 a2 fc ff ff       	jmp    8005a6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800904:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800907:	5b                   	pop    %ebx
  800908:	5e                   	pop    %esi
  800909:	5f                   	pop    %edi
  80090a:	5d                   	pop    %ebp
  80090b:	c3                   	ret    

0080090c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80090c:	55                   	push   %ebp
  80090d:	89 e5                	mov    %esp,%ebp
  80090f:	83 ec 18             	sub    $0x18,%esp
  800912:	8b 45 08             	mov    0x8(%ebp),%eax
  800915:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800918:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80091b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80091f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800922:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800929:	85 c0                	test   %eax,%eax
  80092b:	74 26                	je     800953 <vsnprintf+0x47>
  80092d:	85 d2                	test   %edx,%edx
  80092f:	7e 29                	jle    80095a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800931:	ff 75 14             	pushl  0x14(%ebp)
  800934:	ff 75 10             	pushl  0x10(%ebp)
  800937:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80093a:	50                   	push   %eax
  80093b:	68 47 05 80 00       	push   $0x800547
  800940:	e8 3b fc ff ff       	call   800580 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800945:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800948:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80094b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80094e:	83 c4 10             	add    $0x10,%esp
  800951:	eb 0c                	jmp    80095f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800953:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800958:	eb 05                	jmp    80095f <vsnprintf+0x53>
  80095a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80095f:	c9                   	leave  
  800960:	c3                   	ret    

00800961 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800961:	55                   	push   %ebp
  800962:	89 e5                	mov    %esp,%ebp
  800964:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800967:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80096a:	50                   	push   %eax
  80096b:	ff 75 10             	pushl  0x10(%ebp)
  80096e:	ff 75 0c             	pushl  0xc(%ebp)
  800971:	ff 75 08             	pushl  0x8(%ebp)
  800974:	e8 93 ff ff ff       	call   80090c <vsnprintf>
	va_end(ap);

	return rc;
}
  800979:	c9                   	leave  
  80097a:	c3                   	ret    

0080097b <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80097b:	55                   	push   %ebp
  80097c:	89 e5                	mov    %esp,%ebp
  80097e:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800981:	b8 00 00 00 00       	mov    $0x0,%eax
  800986:	eb 01                	jmp    800989 <strlen+0xe>
		n++;
  800988:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800989:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80098d:	75 f9                	jne    800988 <strlen+0xd>
		n++;
	return n;
}
  80098f:	5d                   	pop    %ebp
  800990:	c3                   	ret    

00800991 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800991:	55                   	push   %ebp
  800992:	89 e5                	mov    %esp,%ebp
  800994:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800997:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80099a:	ba 00 00 00 00       	mov    $0x0,%edx
  80099f:	eb 01                	jmp    8009a2 <strnlen+0x11>
		n++;
  8009a1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009a2:	39 c2                	cmp    %eax,%edx
  8009a4:	74 08                	je     8009ae <strnlen+0x1d>
  8009a6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8009aa:	75 f5                	jne    8009a1 <strnlen+0x10>
  8009ac:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009ae:	5d                   	pop    %ebp
  8009af:	c3                   	ret    

008009b0 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009b0:	55                   	push   %ebp
  8009b1:	89 e5                	mov    %esp,%ebp
  8009b3:	53                   	push   %ebx
  8009b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8009b7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009ba:	89 c2                	mov    %eax,%edx
  8009bc:	42                   	inc    %edx
  8009bd:	41                   	inc    %ecx
  8009be:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009c1:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009c4:	84 db                	test   %bl,%bl
  8009c6:	75 f4                	jne    8009bc <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009c8:	5b                   	pop    %ebx
  8009c9:	5d                   	pop    %ebp
  8009ca:	c3                   	ret    

008009cb <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009cb:	55                   	push   %ebp
  8009cc:	89 e5                	mov    %esp,%ebp
  8009ce:	53                   	push   %ebx
  8009cf:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009d2:	53                   	push   %ebx
  8009d3:	e8 a3 ff ff ff       	call   80097b <strlen>
  8009d8:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009db:	ff 75 0c             	pushl  0xc(%ebp)
  8009de:	01 d8                	add    %ebx,%eax
  8009e0:	50                   	push   %eax
  8009e1:	e8 ca ff ff ff       	call   8009b0 <strcpy>
	return dst;
}
  8009e6:	89 d8                	mov    %ebx,%eax
  8009e8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009eb:	c9                   	leave  
  8009ec:	c3                   	ret    

008009ed <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009ed:	55                   	push   %ebp
  8009ee:	89 e5                	mov    %esp,%ebp
  8009f0:	56                   	push   %esi
  8009f1:	53                   	push   %ebx
  8009f2:	8b 75 08             	mov    0x8(%ebp),%esi
  8009f5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009f8:	89 f3                	mov    %esi,%ebx
  8009fa:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009fd:	89 f2                	mov    %esi,%edx
  8009ff:	eb 0c                	jmp    800a0d <strncpy+0x20>
		*dst++ = *src;
  800a01:	42                   	inc    %edx
  800a02:	8a 01                	mov    (%ecx),%al
  800a04:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800a07:	80 39 01             	cmpb   $0x1,(%ecx)
  800a0a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a0d:	39 da                	cmp    %ebx,%edx
  800a0f:	75 f0                	jne    800a01 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a11:	89 f0                	mov    %esi,%eax
  800a13:	5b                   	pop    %ebx
  800a14:	5e                   	pop    %esi
  800a15:	5d                   	pop    %ebp
  800a16:	c3                   	ret    

00800a17 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a17:	55                   	push   %ebp
  800a18:	89 e5                	mov    %esp,%ebp
  800a1a:	56                   	push   %esi
  800a1b:	53                   	push   %ebx
  800a1c:	8b 75 08             	mov    0x8(%ebp),%esi
  800a1f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a22:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a25:	85 c0                	test   %eax,%eax
  800a27:	74 1e                	je     800a47 <strlcpy+0x30>
  800a29:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a2d:	89 f2                	mov    %esi,%edx
  800a2f:	eb 05                	jmp    800a36 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a31:	42                   	inc    %edx
  800a32:	41                   	inc    %ecx
  800a33:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a36:	39 c2                	cmp    %eax,%edx
  800a38:	74 08                	je     800a42 <strlcpy+0x2b>
  800a3a:	8a 19                	mov    (%ecx),%bl
  800a3c:	84 db                	test   %bl,%bl
  800a3e:	75 f1                	jne    800a31 <strlcpy+0x1a>
  800a40:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a42:	c6 00 00             	movb   $0x0,(%eax)
  800a45:	eb 02                	jmp    800a49 <strlcpy+0x32>
  800a47:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a49:	29 f0                	sub    %esi,%eax
}
  800a4b:	5b                   	pop    %ebx
  800a4c:	5e                   	pop    %esi
  800a4d:	5d                   	pop    %ebp
  800a4e:	c3                   	ret    

00800a4f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a4f:	55                   	push   %ebp
  800a50:	89 e5                	mov    %esp,%ebp
  800a52:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a55:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a58:	eb 02                	jmp    800a5c <strcmp+0xd>
		p++, q++;
  800a5a:	41                   	inc    %ecx
  800a5b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a5c:	8a 01                	mov    (%ecx),%al
  800a5e:	84 c0                	test   %al,%al
  800a60:	74 04                	je     800a66 <strcmp+0x17>
  800a62:	3a 02                	cmp    (%edx),%al
  800a64:	74 f4                	je     800a5a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a66:	0f b6 c0             	movzbl %al,%eax
  800a69:	0f b6 12             	movzbl (%edx),%edx
  800a6c:	29 d0                	sub    %edx,%eax
}
  800a6e:	5d                   	pop    %ebp
  800a6f:	c3                   	ret    

00800a70 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a70:	55                   	push   %ebp
  800a71:	89 e5                	mov    %esp,%ebp
  800a73:	53                   	push   %ebx
  800a74:	8b 45 08             	mov    0x8(%ebp),%eax
  800a77:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a7a:	89 c3                	mov    %eax,%ebx
  800a7c:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a7f:	eb 02                	jmp    800a83 <strncmp+0x13>
		n--, p++, q++;
  800a81:	40                   	inc    %eax
  800a82:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a83:	39 d8                	cmp    %ebx,%eax
  800a85:	74 14                	je     800a9b <strncmp+0x2b>
  800a87:	8a 08                	mov    (%eax),%cl
  800a89:	84 c9                	test   %cl,%cl
  800a8b:	74 04                	je     800a91 <strncmp+0x21>
  800a8d:	3a 0a                	cmp    (%edx),%cl
  800a8f:	74 f0                	je     800a81 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a91:	0f b6 00             	movzbl (%eax),%eax
  800a94:	0f b6 12             	movzbl (%edx),%edx
  800a97:	29 d0                	sub    %edx,%eax
  800a99:	eb 05                	jmp    800aa0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a9b:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800aa0:	5b                   	pop    %ebx
  800aa1:	5d                   	pop    %ebp
  800aa2:	c3                   	ret    

00800aa3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800aa3:	55                   	push   %ebp
  800aa4:	89 e5                	mov    %esp,%ebp
  800aa6:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800aac:	eb 05                	jmp    800ab3 <strchr+0x10>
		if (*s == c)
  800aae:	38 ca                	cmp    %cl,%dl
  800ab0:	74 0c                	je     800abe <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800ab2:	40                   	inc    %eax
  800ab3:	8a 10                	mov    (%eax),%dl
  800ab5:	84 d2                	test   %dl,%dl
  800ab7:	75 f5                	jne    800aae <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800ab9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800abe:	5d                   	pop    %ebp
  800abf:	c3                   	ret    

00800ac0 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ac0:	55                   	push   %ebp
  800ac1:	89 e5                	mov    %esp,%ebp
  800ac3:	8b 45 08             	mov    0x8(%ebp),%eax
  800ac6:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800ac9:	eb 05                	jmp    800ad0 <strfind+0x10>
		if (*s == c)
  800acb:	38 ca                	cmp    %cl,%dl
  800acd:	74 07                	je     800ad6 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800acf:	40                   	inc    %eax
  800ad0:	8a 10                	mov    (%eax),%dl
  800ad2:	84 d2                	test   %dl,%dl
  800ad4:	75 f5                	jne    800acb <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800ad6:	5d                   	pop    %ebp
  800ad7:	c3                   	ret    

00800ad8 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800ad8:	55                   	push   %ebp
  800ad9:	89 e5                	mov    %esp,%ebp
  800adb:	57                   	push   %edi
  800adc:	56                   	push   %esi
  800add:	53                   	push   %ebx
  800ade:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ae1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800ae4:	85 c9                	test   %ecx,%ecx
  800ae6:	74 36                	je     800b1e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800ae8:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800aee:	75 28                	jne    800b18 <memset+0x40>
  800af0:	f6 c1 03             	test   $0x3,%cl
  800af3:	75 23                	jne    800b18 <memset+0x40>
		c &= 0xFF;
  800af5:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800af9:	89 d3                	mov    %edx,%ebx
  800afb:	c1 e3 08             	shl    $0x8,%ebx
  800afe:	89 d6                	mov    %edx,%esi
  800b00:	c1 e6 18             	shl    $0x18,%esi
  800b03:	89 d0                	mov    %edx,%eax
  800b05:	c1 e0 10             	shl    $0x10,%eax
  800b08:	09 f0                	or     %esi,%eax
  800b0a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800b0c:	89 d8                	mov    %ebx,%eax
  800b0e:	09 d0                	or     %edx,%eax
  800b10:	c1 e9 02             	shr    $0x2,%ecx
  800b13:	fc                   	cld    
  800b14:	f3 ab                	rep stos %eax,%es:(%edi)
  800b16:	eb 06                	jmp    800b1e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b18:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b1b:	fc                   	cld    
  800b1c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b1e:	89 f8                	mov    %edi,%eax
  800b20:	5b                   	pop    %ebx
  800b21:	5e                   	pop    %esi
  800b22:	5f                   	pop    %edi
  800b23:	5d                   	pop    %ebp
  800b24:	c3                   	ret    

00800b25 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b25:	55                   	push   %ebp
  800b26:	89 e5                	mov    %esp,%ebp
  800b28:	57                   	push   %edi
  800b29:	56                   	push   %esi
  800b2a:	8b 45 08             	mov    0x8(%ebp),%eax
  800b2d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b30:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b33:	39 c6                	cmp    %eax,%esi
  800b35:	73 33                	jae    800b6a <memmove+0x45>
  800b37:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b3a:	39 d0                	cmp    %edx,%eax
  800b3c:	73 2c                	jae    800b6a <memmove+0x45>
		s += n;
		d += n;
  800b3e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b41:	89 d6                	mov    %edx,%esi
  800b43:	09 fe                	or     %edi,%esi
  800b45:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b4b:	75 13                	jne    800b60 <memmove+0x3b>
  800b4d:	f6 c1 03             	test   $0x3,%cl
  800b50:	75 0e                	jne    800b60 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b52:	83 ef 04             	sub    $0x4,%edi
  800b55:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b58:	c1 e9 02             	shr    $0x2,%ecx
  800b5b:	fd                   	std    
  800b5c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b5e:	eb 07                	jmp    800b67 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b60:	4f                   	dec    %edi
  800b61:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b64:	fd                   	std    
  800b65:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b67:	fc                   	cld    
  800b68:	eb 1d                	jmp    800b87 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b6a:	89 f2                	mov    %esi,%edx
  800b6c:	09 c2                	or     %eax,%edx
  800b6e:	f6 c2 03             	test   $0x3,%dl
  800b71:	75 0f                	jne    800b82 <memmove+0x5d>
  800b73:	f6 c1 03             	test   $0x3,%cl
  800b76:	75 0a                	jne    800b82 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b78:	c1 e9 02             	shr    $0x2,%ecx
  800b7b:	89 c7                	mov    %eax,%edi
  800b7d:	fc                   	cld    
  800b7e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b80:	eb 05                	jmp    800b87 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b82:	89 c7                	mov    %eax,%edi
  800b84:	fc                   	cld    
  800b85:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b87:	5e                   	pop    %esi
  800b88:	5f                   	pop    %edi
  800b89:	5d                   	pop    %ebp
  800b8a:	c3                   	ret    

00800b8b <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b8b:	55                   	push   %ebp
  800b8c:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b8e:	ff 75 10             	pushl  0x10(%ebp)
  800b91:	ff 75 0c             	pushl  0xc(%ebp)
  800b94:	ff 75 08             	pushl  0x8(%ebp)
  800b97:	e8 89 ff ff ff       	call   800b25 <memmove>
}
  800b9c:	c9                   	leave  
  800b9d:	c3                   	ret    

00800b9e <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b9e:	55                   	push   %ebp
  800b9f:	89 e5                	mov    %esp,%ebp
  800ba1:	56                   	push   %esi
  800ba2:	53                   	push   %ebx
  800ba3:	8b 45 08             	mov    0x8(%ebp),%eax
  800ba6:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ba9:	89 c6                	mov    %eax,%esi
  800bab:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bae:	eb 14                	jmp    800bc4 <memcmp+0x26>
		if (*s1 != *s2)
  800bb0:	8a 08                	mov    (%eax),%cl
  800bb2:	8a 1a                	mov    (%edx),%bl
  800bb4:	38 d9                	cmp    %bl,%cl
  800bb6:	74 0a                	je     800bc2 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800bb8:	0f b6 c1             	movzbl %cl,%eax
  800bbb:	0f b6 db             	movzbl %bl,%ebx
  800bbe:	29 d8                	sub    %ebx,%eax
  800bc0:	eb 0b                	jmp    800bcd <memcmp+0x2f>
		s1++, s2++;
  800bc2:	40                   	inc    %eax
  800bc3:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bc4:	39 f0                	cmp    %esi,%eax
  800bc6:	75 e8                	jne    800bb0 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bc8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bcd:	5b                   	pop    %ebx
  800bce:	5e                   	pop    %esi
  800bcf:	5d                   	pop    %ebp
  800bd0:	c3                   	ret    

00800bd1 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bd1:	55                   	push   %ebp
  800bd2:	89 e5                	mov    %esp,%ebp
  800bd4:	53                   	push   %ebx
  800bd5:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bd8:	89 c1                	mov    %eax,%ecx
  800bda:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bdd:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800be1:	eb 08                	jmp    800beb <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800be3:	0f b6 10             	movzbl (%eax),%edx
  800be6:	39 da                	cmp    %ebx,%edx
  800be8:	74 05                	je     800bef <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bea:	40                   	inc    %eax
  800beb:	39 c8                	cmp    %ecx,%eax
  800bed:	72 f4                	jb     800be3 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800bef:	5b                   	pop    %ebx
  800bf0:	5d                   	pop    %ebp
  800bf1:	c3                   	ret    

00800bf2 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bf2:	55                   	push   %ebp
  800bf3:	89 e5                	mov    %esp,%ebp
  800bf5:	57                   	push   %edi
  800bf6:	56                   	push   %esi
  800bf7:	53                   	push   %ebx
  800bf8:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bfb:	eb 01                	jmp    800bfe <strtol+0xc>
		s++;
  800bfd:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bfe:	8a 01                	mov    (%ecx),%al
  800c00:	3c 20                	cmp    $0x20,%al
  800c02:	74 f9                	je     800bfd <strtol+0xb>
  800c04:	3c 09                	cmp    $0x9,%al
  800c06:	74 f5                	je     800bfd <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c08:	3c 2b                	cmp    $0x2b,%al
  800c0a:	75 08                	jne    800c14 <strtol+0x22>
		s++;
  800c0c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c0d:	bf 00 00 00 00       	mov    $0x0,%edi
  800c12:	eb 11                	jmp    800c25 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c14:	3c 2d                	cmp    $0x2d,%al
  800c16:	75 08                	jne    800c20 <strtol+0x2e>
		s++, neg = 1;
  800c18:	41                   	inc    %ecx
  800c19:	bf 01 00 00 00       	mov    $0x1,%edi
  800c1e:	eb 05                	jmp    800c25 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c20:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c25:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c29:	0f 84 87 00 00 00    	je     800cb6 <strtol+0xc4>
  800c2f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c33:	75 27                	jne    800c5c <strtol+0x6a>
  800c35:	80 39 30             	cmpb   $0x30,(%ecx)
  800c38:	75 22                	jne    800c5c <strtol+0x6a>
  800c3a:	e9 88 00 00 00       	jmp    800cc7 <strtol+0xd5>
		s += 2, base = 16;
  800c3f:	83 c1 02             	add    $0x2,%ecx
  800c42:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c49:	eb 11                	jmp    800c5c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c4b:	41                   	inc    %ecx
  800c4c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c53:	eb 07                	jmp    800c5c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c55:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c5c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c61:	8a 11                	mov    (%ecx),%dl
  800c63:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c66:	80 fb 09             	cmp    $0x9,%bl
  800c69:	77 08                	ja     800c73 <strtol+0x81>
			dig = *s - '0';
  800c6b:	0f be d2             	movsbl %dl,%edx
  800c6e:	83 ea 30             	sub    $0x30,%edx
  800c71:	eb 22                	jmp    800c95 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c73:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c76:	89 f3                	mov    %esi,%ebx
  800c78:	80 fb 19             	cmp    $0x19,%bl
  800c7b:	77 08                	ja     800c85 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c7d:	0f be d2             	movsbl %dl,%edx
  800c80:	83 ea 57             	sub    $0x57,%edx
  800c83:	eb 10                	jmp    800c95 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c85:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c88:	89 f3                	mov    %esi,%ebx
  800c8a:	80 fb 19             	cmp    $0x19,%bl
  800c8d:	77 14                	ja     800ca3 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c8f:	0f be d2             	movsbl %dl,%edx
  800c92:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c95:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c98:	7d 09                	jge    800ca3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c9a:	41                   	inc    %ecx
  800c9b:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c9f:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800ca1:	eb be                	jmp    800c61 <strtol+0x6f>

	if (endptr)
  800ca3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ca7:	74 05                	je     800cae <strtol+0xbc>
		*endptr = (char *) s;
  800ca9:	8b 75 0c             	mov    0xc(%ebp),%esi
  800cac:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800cae:	85 ff                	test   %edi,%edi
  800cb0:	74 21                	je     800cd3 <strtol+0xe1>
  800cb2:	f7 d8                	neg    %eax
  800cb4:	eb 1d                	jmp    800cd3 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800cb6:	80 39 30             	cmpb   $0x30,(%ecx)
  800cb9:	75 9a                	jne    800c55 <strtol+0x63>
  800cbb:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cbf:	0f 84 7a ff ff ff    	je     800c3f <strtol+0x4d>
  800cc5:	eb 84                	jmp    800c4b <strtol+0x59>
  800cc7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800ccb:	0f 84 6e ff ff ff    	je     800c3f <strtol+0x4d>
  800cd1:	eb 89                	jmp    800c5c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cd3:	5b                   	pop    %ebx
  800cd4:	5e                   	pop    %esi
  800cd5:	5f                   	pop    %edi
  800cd6:	5d                   	pop    %ebp
  800cd7:	c3                   	ret    

00800cd8 <__udivdi3>:
  800cd8:	55                   	push   %ebp
  800cd9:	57                   	push   %edi
  800cda:	56                   	push   %esi
  800cdb:	53                   	push   %ebx
  800cdc:	83 ec 1c             	sub    $0x1c,%esp
  800cdf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ce3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ce7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ceb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800cef:	89 ca                	mov    %ecx,%edx
  800cf1:	89 f8                	mov    %edi,%eax
  800cf3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800cf7:	85 f6                	test   %esi,%esi
  800cf9:	75 2d                	jne    800d28 <__udivdi3+0x50>
  800cfb:	39 cf                	cmp    %ecx,%edi
  800cfd:	77 65                	ja     800d64 <__udivdi3+0x8c>
  800cff:	89 fd                	mov    %edi,%ebp
  800d01:	85 ff                	test   %edi,%edi
  800d03:	75 0b                	jne    800d10 <__udivdi3+0x38>
  800d05:	b8 01 00 00 00       	mov    $0x1,%eax
  800d0a:	31 d2                	xor    %edx,%edx
  800d0c:	f7 f7                	div    %edi
  800d0e:	89 c5                	mov    %eax,%ebp
  800d10:	31 d2                	xor    %edx,%edx
  800d12:	89 c8                	mov    %ecx,%eax
  800d14:	f7 f5                	div    %ebp
  800d16:	89 c1                	mov    %eax,%ecx
  800d18:	89 d8                	mov    %ebx,%eax
  800d1a:	f7 f5                	div    %ebp
  800d1c:	89 cf                	mov    %ecx,%edi
  800d1e:	89 fa                	mov    %edi,%edx
  800d20:	83 c4 1c             	add    $0x1c,%esp
  800d23:	5b                   	pop    %ebx
  800d24:	5e                   	pop    %esi
  800d25:	5f                   	pop    %edi
  800d26:	5d                   	pop    %ebp
  800d27:	c3                   	ret    
  800d28:	39 ce                	cmp    %ecx,%esi
  800d2a:	77 28                	ja     800d54 <__udivdi3+0x7c>
  800d2c:	0f bd fe             	bsr    %esi,%edi
  800d2f:	83 f7 1f             	xor    $0x1f,%edi
  800d32:	75 40                	jne    800d74 <__udivdi3+0x9c>
  800d34:	39 ce                	cmp    %ecx,%esi
  800d36:	72 0a                	jb     800d42 <__udivdi3+0x6a>
  800d38:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d3c:	0f 87 9e 00 00 00    	ja     800de0 <__udivdi3+0x108>
  800d42:	b8 01 00 00 00       	mov    $0x1,%eax
  800d47:	89 fa                	mov    %edi,%edx
  800d49:	83 c4 1c             	add    $0x1c,%esp
  800d4c:	5b                   	pop    %ebx
  800d4d:	5e                   	pop    %esi
  800d4e:	5f                   	pop    %edi
  800d4f:	5d                   	pop    %ebp
  800d50:	c3                   	ret    
  800d51:	8d 76 00             	lea    0x0(%esi),%esi
  800d54:	31 ff                	xor    %edi,%edi
  800d56:	31 c0                	xor    %eax,%eax
  800d58:	89 fa                	mov    %edi,%edx
  800d5a:	83 c4 1c             	add    $0x1c,%esp
  800d5d:	5b                   	pop    %ebx
  800d5e:	5e                   	pop    %esi
  800d5f:	5f                   	pop    %edi
  800d60:	5d                   	pop    %ebp
  800d61:	c3                   	ret    
  800d62:	66 90                	xchg   %ax,%ax
  800d64:	89 d8                	mov    %ebx,%eax
  800d66:	f7 f7                	div    %edi
  800d68:	31 ff                	xor    %edi,%edi
  800d6a:	89 fa                	mov    %edi,%edx
  800d6c:	83 c4 1c             	add    $0x1c,%esp
  800d6f:	5b                   	pop    %ebx
  800d70:	5e                   	pop    %esi
  800d71:	5f                   	pop    %edi
  800d72:	5d                   	pop    %ebp
  800d73:	c3                   	ret    
  800d74:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d79:	89 eb                	mov    %ebp,%ebx
  800d7b:	29 fb                	sub    %edi,%ebx
  800d7d:	89 f9                	mov    %edi,%ecx
  800d7f:	d3 e6                	shl    %cl,%esi
  800d81:	89 c5                	mov    %eax,%ebp
  800d83:	88 d9                	mov    %bl,%cl
  800d85:	d3 ed                	shr    %cl,%ebp
  800d87:	89 e9                	mov    %ebp,%ecx
  800d89:	09 f1                	or     %esi,%ecx
  800d8b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d8f:	89 f9                	mov    %edi,%ecx
  800d91:	d3 e0                	shl    %cl,%eax
  800d93:	89 c5                	mov    %eax,%ebp
  800d95:	89 d6                	mov    %edx,%esi
  800d97:	88 d9                	mov    %bl,%cl
  800d99:	d3 ee                	shr    %cl,%esi
  800d9b:	89 f9                	mov    %edi,%ecx
  800d9d:	d3 e2                	shl    %cl,%edx
  800d9f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800da3:	88 d9                	mov    %bl,%cl
  800da5:	d3 e8                	shr    %cl,%eax
  800da7:	09 c2                	or     %eax,%edx
  800da9:	89 d0                	mov    %edx,%eax
  800dab:	89 f2                	mov    %esi,%edx
  800dad:	f7 74 24 0c          	divl   0xc(%esp)
  800db1:	89 d6                	mov    %edx,%esi
  800db3:	89 c3                	mov    %eax,%ebx
  800db5:	f7 e5                	mul    %ebp
  800db7:	39 d6                	cmp    %edx,%esi
  800db9:	72 19                	jb     800dd4 <__udivdi3+0xfc>
  800dbb:	74 0b                	je     800dc8 <__udivdi3+0xf0>
  800dbd:	89 d8                	mov    %ebx,%eax
  800dbf:	31 ff                	xor    %edi,%edi
  800dc1:	e9 58 ff ff ff       	jmp    800d1e <__udivdi3+0x46>
  800dc6:	66 90                	xchg   %ax,%ax
  800dc8:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dcc:	89 f9                	mov    %edi,%ecx
  800dce:	d3 e2                	shl    %cl,%edx
  800dd0:	39 c2                	cmp    %eax,%edx
  800dd2:	73 e9                	jae    800dbd <__udivdi3+0xe5>
  800dd4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800dd7:	31 ff                	xor    %edi,%edi
  800dd9:	e9 40 ff ff ff       	jmp    800d1e <__udivdi3+0x46>
  800dde:	66 90                	xchg   %ax,%ax
  800de0:	31 c0                	xor    %eax,%eax
  800de2:	e9 37 ff ff ff       	jmp    800d1e <__udivdi3+0x46>
  800de7:	90                   	nop

00800de8 <__umoddi3>:
  800de8:	55                   	push   %ebp
  800de9:	57                   	push   %edi
  800dea:	56                   	push   %esi
  800deb:	53                   	push   %ebx
  800dec:	83 ec 1c             	sub    $0x1c,%esp
  800def:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800df3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800df7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800dfb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800dff:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e03:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e07:	89 f3                	mov    %esi,%ebx
  800e09:	89 fa                	mov    %edi,%edx
  800e0b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e0f:	89 34 24             	mov    %esi,(%esp)
  800e12:	85 c0                	test   %eax,%eax
  800e14:	75 1a                	jne    800e30 <__umoddi3+0x48>
  800e16:	39 f7                	cmp    %esi,%edi
  800e18:	0f 86 a2 00 00 00    	jbe    800ec0 <__umoddi3+0xd8>
  800e1e:	89 c8                	mov    %ecx,%eax
  800e20:	89 f2                	mov    %esi,%edx
  800e22:	f7 f7                	div    %edi
  800e24:	89 d0                	mov    %edx,%eax
  800e26:	31 d2                	xor    %edx,%edx
  800e28:	83 c4 1c             	add    $0x1c,%esp
  800e2b:	5b                   	pop    %ebx
  800e2c:	5e                   	pop    %esi
  800e2d:	5f                   	pop    %edi
  800e2e:	5d                   	pop    %ebp
  800e2f:	c3                   	ret    
  800e30:	39 f0                	cmp    %esi,%eax
  800e32:	0f 87 ac 00 00 00    	ja     800ee4 <__umoddi3+0xfc>
  800e38:	0f bd e8             	bsr    %eax,%ebp
  800e3b:	83 f5 1f             	xor    $0x1f,%ebp
  800e3e:	0f 84 ac 00 00 00    	je     800ef0 <__umoddi3+0x108>
  800e44:	bf 20 00 00 00       	mov    $0x20,%edi
  800e49:	29 ef                	sub    %ebp,%edi
  800e4b:	89 fe                	mov    %edi,%esi
  800e4d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e51:	89 e9                	mov    %ebp,%ecx
  800e53:	d3 e0                	shl    %cl,%eax
  800e55:	89 d7                	mov    %edx,%edi
  800e57:	89 f1                	mov    %esi,%ecx
  800e59:	d3 ef                	shr    %cl,%edi
  800e5b:	09 c7                	or     %eax,%edi
  800e5d:	89 e9                	mov    %ebp,%ecx
  800e5f:	d3 e2                	shl    %cl,%edx
  800e61:	89 14 24             	mov    %edx,(%esp)
  800e64:	89 d8                	mov    %ebx,%eax
  800e66:	d3 e0                	shl    %cl,%eax
  800e68:	89 c2                	mov    %eax,%edx
  800e6a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e6e:	d3 e0                	shl    %cl,%eax
  800e70:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e74:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e78:	89 f1                	mov    %esi,%ecx
  800e7a:	d3 e8                	shr    %cl,%eax
  800e7c:	09 d0                	or     %edx,%eax
  800e7e:	d3 eb                	shr    %cl,%ebx
  800e80:	89 da                	mov    %ebx,%edx
  800e82:	f7 f7                	div    %edi
  800e84:	89 d3                	mov    %edx,%ebx
  800e86:	f7 24 24             	mull   (%esp)
  800e89:	89 c6                	mov    %eax,%esi
  800e8b:	89 d1                	mov    %edx,%ecx
  800e8d:	39 d3                	cmp    %edx,%ebx
  800e8f:	0f 82 87 00 00 00    	jb     800f1c <__umoddi3+0x134>
  800e95:	0f 84 91 00 00 00    	je     800f2c <__umoddi3+0x144>
  800e9b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e9f:	29 f2                	sub    %esi,%edx
  800ea1:	19 cb                	sbb    %ecx,%ebx
  800ea3:	89 d8                	mov    %ebx,%eax
  800ea5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ea9:	d3 e0                	shl    %cl,%eax
  800eab:	89 e9                	mov    %ebp,%ecx
  800ead:	d3 ea                	shr    %cl,%edx
  800eaf:	09 d0                	or     %edx,%eax
  800eb1:	89 e9                	mov    %ebp,%ecx
  800eb3:	d3 eb                	shr    %cl,%ebx
  800eb5:	89 da                	mov    %ebx,%edx
  800eb7:	83 c4 1c             	add    $0x1c,%esp
  800eba:	5b                   	pop    %ebx
  800ebb:	5e                   	pop    %esi
  800ebc:	5f                   	pop    %edi
  800ebd:	5d                   	pop    %ebp
  800ebe:	c3                   	ret    
  800ebf:	90                   	nop
  800ec0:	89 fd                	mov    %edi,%ebp
  800ec2:	85 ff                	test   %edi,%edi
  800ec4:	75 0b                	jne    800ed1 <__umoddi3+0xe9>
  800ec6:	b8 01 00 00 00       	mov    $0x1,%eax
  800ecb:	31 d2                	xor    %edx,%edx
  800ecd:	f7 f7                	div    %edi
  800ecf:	89 c5                	mov    %eax,%ebp
  800ed1:	89 f0                	mov    %esi,%eax
  800ed3:	31 d2                	xor    %edx,%edx
  800ed5:	f7 f5                	div    %ebp
  800ed7:	89 c8                	mov    %ecx,%eax
  800ed9:	f7 f5                	div    %ebp
  800edb:	89 d0                	mov    %edx,%eax
  800edd:	e9 44 ff ff ff       	jmp    800e26 <__umoddi3+0x3e>
  800ee2:	66 90                	xchg   %ax,%ax
  800ee4:	89 c8                	mov    %ecx,%eax
  800ee6:	89 f2                	mov    %esi,%edx
  800ee8:	83 c4 1c             	add    $0x1c,%esp
  800eeb:	5b                   	pop    %ebx
  800eec:	5e                   	pop    %esi
  800eed:	5f                   	pop    %edi
  800eee:	5d                   	pop    %ebp
  800eef:	c3                   	ret    
  800ef0:	3b 04 24             	cmp    (%esp),%eax
  800ef3:	72 06                	jb     800efb <__umoddi3+0x113>
  800ef5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ef9:	77 0f                	ja     800f0a <__umoddi3+0x122>
  800efb:	89 f2                	mov    %esi,%edx
  800efd:	29 f9                	sub    %edi,%ecx
  800eff:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f03:	89 14 24             	mov    %edx,(%esp)
  800f06:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f0a:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f0e:	8b 14 24             	mov    (%esp),%edx
  800f11:	83 c4 1c             	add    $0x1c,%esp
  800f14:	5b                   	pop    %ebx
  800f15:	5e                   	pop    %esi
  800f16:	5f                   	pop    %edi
  800f17:	5d                   	pop    %ebp
  800f18:	c3                   	ret    
  800f19:	8d 76 00             	lea    0x0(%esi),%esi
  800f1c:	2b 04 24             	sub    (%esp),%eax
  800f1f:	19 fa                	sbb    %edi,%edx
  800f21:	89 d1                	mov    %edx,%ecx
  800f23:	89 c6                	mov    %eax,%esi
  800f25:	e9 71 ff ff ff       	jmp    800e9b <__umoddi3+0xb3>
  800f2a:	66 90                	xchg   %ax,%ax
  800f2c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f30:	72 ea                	jb     800f1c <__umoddi3+0x134>
  800f32:	89 d9                	mov    %ebx,%ecx
  800f34:	e9 62 ff ff ff       	jmp    800e9b <__umoddi3+0xb3>
