
obj/user/lsfd.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 da 00 00 00       	call   80010b <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <usage>:
#include <inc/lib.h>

void
usage(void)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 14             	sub    $0x14,%esp
	cprintf("usage: lsfd [-1]\n");
  800039:	68 60 20 80 00       	push   $0x802060
  80003e:	e8 bb 01 00 00       	call   8001fe <cprintf>
	exit();
  800043:	e8 12 01 00 00       	call   80015a <exit>
}
  800048:	83 c4 10             	add    $0x10,%esp
  80004b:	c9                   	leave  
  80004c:	c3                   	ret    

0080004d <umain>:

void
umain(int argc, char **argv)
{
  80004d:	55                   	push   %ebp
  80004e:	89 e5                	mov    %esp,%ebp
  800050:	57                   	push   %edi
  800051:	56                   	push   %esi
  800052:	53                   	push   %ebx
  800053:	81 ec b0 00 00 00    	sub    $0xb0,%esp
	int i, usefprint = 0;
	struct Stat st;
	struct Argstate args;

	argstart(&argc, argv, &args);
  800059:	8d 85 4c ff ff ff    	lea    -0xb4(%ebp),%eax
  80005f:	50                   	push   %eax
  800060:	ff 75 0c             	pushl  0xc(%ebp)
  800063:	8d 45 08             	lea    0x8(%ebp),%eax
  800066:	50                   	push   %eax
  800067:	e8 ea 0c 00 00       	call   800d56 <argstart>
	while ((i = argnext(&args)) >= 0)
  80006c:	83 c4 10             	add    $0x10,%esp
}

void
umain(int argc, char **argv)
{
	int i, usefprint = 0;
  80006f:	be 00 00 00 00       	mov    $0x0,%esi
	struct Stat st;
	struct Argstate args;

	argstart(&argc, argv, &args);
	while ((i = argnext(&args)) >= 0)
  800074:	8d 9d 4c ff ff ff    	lea    -0xb4(%ebp),%ebx
  80007a:	eb 11                	jmp    80008d <umain+0x40>
		if (i == '1')
  80007c:	83 f8 31             	cmp    $0x31,%eax
  80007f:	74 07                	je     800088 <umain+0x3b>
			usefprint = 1;
		else
			usage();
  800081:	e8 ad ff ff ff       	call   800033 <usage>
  800086:	eb 05                	jmp    80008d <umain+0x40>
	struct Argstate args;

	argstart(&argc, argv, &args);
	while ((i = argnext(&args)) >= 0)
		if (i == '1')
			usefprint = 1;
  800088:	be 01 00 00 00       	mov    $0x1,%esi
	int i, usefprint = 0;
	struct Stat st;
	struct Argstate args;

	argstart(&argc, argv, &args);
	while ((i = argnext(&args)) >= 0)
  80008d:	83 ec 0c             	sub    $0xc,%esp
  800090:	53                   	push   %ebx
  800091:	e8 f9 0c 00 00       	call   800d8f <argnext>
  800096:	83 c4 10             	add    $0x10,%esp
  800099:	85 c0                	test   %eax,%eax
  80009b:	79 df                	jns    80007c <umain+0x2f>
  80009d:	bb 00 00 00 00       	mov    $0x0,%ebx
			usefprint = 1;
		else
			usage();

	for (i = 0; i < 32; i++)
		if (fstat(i, &st) >= 0) {
  8000a2:	8d bd 5c ff ff ff    	lea    -0xa4(%ebp),%edi
  8000a8:	83 ec 08             	sub    $0x8,%esp
  8000ab:	57                   	push   %edi
  8000ac:	53                   	push   %ebx
  8000ad:	e8 d1 12 00 00       	call   801383 <fstat>
  8000b2:	83 c4 10             	add    $0x10,%esp
  8000b5:	85 c0                	test   %eax,%eax
  8000b7:	78 44                	js     8000fd <umain+0xb0>
			if (usefprint)
  8000b9:	85 f6                	test   %esi,%esi
  8000bb:	74 22                	je     8000df <umain+0x92>
				fprintf(1, "fd %d: name %s isdir %d size %d dev %s\n",
  8000bd:	83 ec 04             	sub    $0x4,%esp
  8000c0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8000c3:	ff 70 04             	pushl  0x4(%eax)
  8000c6:	ff 75 dc             	pushl  -0x24(%ebp)
  8000c9:	ff 75 e0             	pushl  -0x20(%ebp)
  8000cc:	57                   	push   %edi
  8000cd:	53                   	push   %ebx
  8000ce:	68 74 20 80 00       	push   $0x802074
  8000d3:	6a 01                	push   $0x1
  8000d5:	e8 98 16 00 00       	call   801772 <fprintf>
  8000da:	83 c4 20             	add    $0x20,%esp
  8000dd:	eb 1e                	jmp    8000fd <umain+0xb0>
					i, st.st_name, st.st_isdir,
					st.st_size, st.st_dev->dev_name);
			else
				cprintf("fd %d: name %s isdir %d size %d dev %s\n",
  8000df:	83 ec 08             	sub    $0x8,%esp
  8000e2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8000e5:	ff 70 04             	pushl  0x4(%eax)
  8000e8:	ff 75 dc             	pushl  -0x24(%ebp)
  8000eb:	ff 75 e0             	pushl  -0x20(%ebp)
  8000ee:	57                   	push   %edi
  8000ef:	53                   	push   %ebx
  8000f0:	68 74 20 80 00       	push   $0x802074
  8000f5:	e8 04 01 00 00       	call   8001fe <cprintf>
  8000fa:	83 c4 20             	add    $0x20,%esp
		if (i == '1')
			usefprint = 1;
		else
			usage();

	for (i = 0; i < 32; i++)
  8000fd:	43                   	inc    %ebx
  8000fe:	83 fb 20             	cmp    $0x20,%ebx
  800101:	75 a5                	jne    8000a8 <umain+0x5b>
			else
				cprintf("fd %d: name %s isdir %d size %d dev %s\n",
					i, st.st_name, st.st_isdir,
					st.st_size, st.st_dev->dev_name);
		}
}
  800103:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800106:	5b                   	pop    %ebx
  800107:	5e                   	pop    %esi
  800108:	5f                   	pop    %edi
  800109:	5d                   	pop    %ebp
  80010a:	c3                   	ret    

0080010b <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80010b:	55                   	push   %ebp
  80010c:	89 e5                	mov    %esp,%ebp
  80010e:	56                   	push   %esi
  80010f:	53                   	push   %ebx
  800110:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800113:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800116:	e8 ed 09 00 00       	call   800b08 <sys_getenvid>
  80011b:	25 ff 03 00 00       	and    $0x3ff,%eax
  800120:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800127:	c1 e0 07             	shl    $0x7,%eax
  80012a:	29 d0                	sub    %edx,%eax
  80012c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800131:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800136:	85 db                	test   %ebx,%ebx
  800138:	7e 07                	jle    800141 <libmain+0x36>
		binaryname = argv[0];
  80013a:	8b 06                	mov    (%esi),%eax
  80013c:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  800141:	83 ec 08             	sub    $0x8,%esp
  800144:	56                   	push   %esi
  800145:	53                   	push   %ebx
  800146:	e8 02 ff ff ff       	call   80004d <umain>

	// exit gracefully
	exit();
  80014b:	e8 0a 00 00 00       	call   80015a <exit>
}
  800150:	83 c4 10             	add    $0x10,%esp
  800153:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800156:	5b                   	pop    %ebx
  800157:	5e                   	pop    %esi
  800158:	5d                   	pop    %ebp
  800159:	c3                   	ret    

0080015a <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80015a:	55                   	push   %ebp
  80015b:	89 e5                	mov    %esp,%ebp
  80015d:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800160:	6a 00                	push   $0x0
  800162:	e8 60 09 00 00       	call   800ac7 <sys_env_destroy>
}
  800167:	83 c4 10             	add    $0x10,%esp
  80016a:	c9                   	leave  
  80016b:	c3                   	ret    

0080016c <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80016c:	55                   	push   %ebp
  80016d:	89 e5                	mov    %esp,%ebp
  80016f:	53                   	push   %ebx
  800170:	83 ec 04             	sub    $0x4,%esp
  800173:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800176:	8b 13                	mov    (%ebx),%edx
  800178:	8d 42 01             	lea    0x1(%edx),%eax
  80017b:	89 03                	mov    %eax,(%ebx)
  80017d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800180:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800184:	3d ff 00 00 00       	cmp    $0xff,%eax
  800189:	75 1a                	jne    8001a5 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80018b:	83 ec 08             	sub    $0x8,%esp
  80018e:	68 ff 00 00 00       	push   $0xff
  800193:	8d 43 08             	lea    0x8(%ebx),%eax
  800196:	50                   	push   %eax
  800197:	e8 ee 08 00 00       	call   800a8a <sys_cputs>
		b->idx = 0;
  80019c:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001a2:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001a5:	ff 43 04             	incl   0x4(%ebx)
}
  8001a8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001ab:	c9                   	leave  
  8001ac:	c3                   	ret    

008001ad <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001ad:	55                   	push   %ebp
  8001ae:	89 e5                	mov    %esp,%ebp
  8001b0:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001b6:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001bd:	00 00 00 
	b.cnt = 0;
  8001c0:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001c7:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001ca:	ff 75 0c             	pushl  0xc(%ebp)
  8001cd:	ff 75 08             	pushl  0x8(%ebp)
  8001d0:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001d6:	50                   	push   %eax
  8001d7:	68 6c 01 80 00       	push   $0x80016c
  8001dc:	e8 51 01 00 00       	call   800332 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001e1:	83 c4 08             	add    $0x8,%esp
  8001e4:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001ea:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001f0:	50                   	push   %eax
  8001f1:	e8 94 08 00 00       	call   800a8a <sys_cputs>

	return b.cnt;
}
  8001f6:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001fc:	c9                   	leave  
  8001fd:	c3                   	ret    

008001fe <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001fe:	55                   	push   %ebp
  8001ff:	89 e5                	mov    %esp,%ebp
  800201:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800204:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800207:	50                   	push   %eax
  800208:	ff 75 08             	pushl  0x8(%ebp)
  80020b:	e8 9d ff ff ff       	call   8001ad <vcprintf>
	va_end(ap);

	return cnt;
}
  800210:	c9                   	leave  
  800211:	c3                   	ret    

00800212 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800212:	55                   	push   %ebp
  800213:	89 e5                	mov    %esp,%ebp
  800215:	57                   	push   %edi
  800216:	56                   	push   %esi
  800217:	53                   	push   %ebx
  800218:	83 ec 1c             	sub    $0x1c,%esp
  80021b:	89 c7                	mov    %eax,%edi
  80021d:	89 d6                	mov    %edx,%esi
  80021f:	8b 45 08             	mov    0x8(%ebp),%eax
  800222:	8b 55 0c             	mov    0xc(%ebp),%edx
  800225:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800228:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80022b:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80022e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800233:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800236:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800239:	39 d3                	cmp    %edx,%ebx
  80023b:	72 05                	jb     800242 <printnum+0x30>
  80023d:	39 45 10             	cmp    %eax,0x10(%ebp)
  800240:	77 45                	ja     800287 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800242:	83 ec 0c             	sub    $0xc,%esp
  800245:	ff 75 18             	pushl  0x18(%ebp)
  800248:	8b 45 14             	mov    0x14(%ebp),%eax
  80024b:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80024e:	53                   	push   %ebx
  80024f:	ff 75 10             	pushl  0x10(%ebp)
  800252:	83 ec 08             	sub    $0x8,%esp
  800255:	ff 75 e4             	pushl  -0x1c(%ebp)
  800258:	ff 75 e0             	pushl  -0x20(%ebp)
  80025b:	ff 75 dc             	pushl  -0x24(%ebp)
  80025e:	ff 75 d8             	pushl  -0x28(%ebp)
  800261:	e8 82 1b 00 00       	call   801de8 <__udivdi3>
  800266:	83 c4 18             	add    $0x18,%esp
  800269:	52                   	push   %edx
  80026a:	50                   	push   %eax
  80026b:	89 f2                	mov    %esi,%edx
  80026d:	89 f8                	mov    %edi,%eax
  80026f:	e8 9e ff ff ff       	call   800212 <printnum>
  800274:	83 c4 20             	add    $0x20,%esp
  800277:	eb 16                	jmp    80028f <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800279:	83 ec 08             	sub    $0x8,%esp
  80027c:	56                   	push   %esi
  80027d:	ff 75 18             	pushl  0x18(%ebp)
  800280:	ff d7                	call   *%edi
  800282:	83 c4 10             	add    $0x10,%esp
  800285:	eb 03                	jmp    80028a <printnum+0x78>
  800287:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80028a:	4b                   	dec    %ebx
  80028b:	85 db                	test   %ebx,%ebx
  80028d:	7f ea                	jg     800279 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80028f:	83 ec 08             	sub    $0x8,%esp
  800292:	56                   	push   %esi
  800293:	83 ec 04             	sub    $0x4,%esp
  800296:	ff 75 e4             	pushl  -0x1c(%ebp)
  800299:	ff 75 e0             	pushl  -0x20(%ebp)
  80029c:	ff 75 dc             	pushl  -0x24(%ebp)
  80029f:	ff 75 d8             	pushl  -0x28(%ebp)
  8002a2:	e8 51 1c 00 00       	call   801ef8 <__umoddi3>
  8002a7:	83 c4 14             	add    $0x14,%esp
  8002aa:	0f be 80 a6 20 80 00 	movsbl 0x8020a6(%eax),%eax
  8002b1:	50                   	push   %eax
  8002b2:	ff d7                	call   *%edi
}
  8002b4:	83 c4 10             	add    $0x10,%esp
  8002b7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002ba:	5b                   	pop    %ebx
  8002bb:	5e                   	pop    %esi
  8002bc:	5f                   	pop    %edi
  8002bd:	5d                   	pop    %ebp
  8002be:	c3                   	ret    

008002bf <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002bf:	55                   	push   %ebp
  8002c0:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002c2:	83 fa 01             	cmp    $0x1,%edx
  8002c5:	7e 0e                	jle    8002d5 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002c7:	8b 10                	mov    (%eax),%edx
  8002c9:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002cc:	89 08                	mov    %ecx,(%eax)
  8002ce:	8b 02                	mov    (%edx),%eax
  8002d0:	8b 52 04             	mov    0x4(%edx),%edx
  8002d3:	eb 22                	jmp    8002f7 <getuint+0x38>
	else if (lflag)
  8002d5:	85 d2                	test   %edx,%edx
  8002d7:	74 10                	je     8002e9 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002d9:	8b 10                	mov    (%eax),%edx
  8002db:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002de:	89 08                	mov    %ecx,(%eax)
  8002e0:	8b 02                	mov    (%edx),%eax
  8002e2:	ba 00 00 00 00       	mov    $0x0,%edx
  8002e7:	eb 0e                	jmp    8002f7 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002e9:	8b 10                	mov    (%eax),%edx
  8002eb:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002ee:	89 08                	mov    %ecx,(%eax)
  8002f0:	8b 02                	mov    (%edx),%eax
  8002f2:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002f7:	5d                   	pop    %ebp
  8002f8:	c3                   	ret    

008002f9 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002f9:	55                   	push   %ebp
  8002fa:	89 e5                	mov    %esp,%ebp
  8002fc:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002ff:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800302:	8b 10                	mov    (%eax),%edx
  800304:	3b 50 04             	cmp    0x4(%eax),%edx
  800307:	73 0a                	jae    800313 <sprintputch+0x1a>
		*b->buf++ = ch;
  800309:	8d 4a 01             	lea    0x1(%edx),%ecx
  80030c:	89 08                	mov    %ecx,(%eax)
  80030e:	8b 45 08             	mov    0x8(%ebp),%eax
  800311:	88 02                	mov    %al,(%edx)
}
  800313:	5d                   	pop    %ebp
  800314:	c3                   	ret    

00800315 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800315:	55                   	push   %ebp
  800316:	89 e5                	mov    %esp,%ebp
  800318:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80031b:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80031e:	50                   	push   %eax
  80031f:	ff 75 10             	pushl  0x10(%ebp)
  800322:	ff 75 0c             	pushl  0xc(%ebp)
  800325:	ff 75 08             	pushl  0x8(%ebp)
  800328:	e8 05 00 00 00       	call   800332 <vprintfmt>
	va_end(ap);
}
  80032d:	83 c4 10             	add    $0x10,%esp
  800330:	c9                   	leave  
  800331:	c3                   	ret    

00800332 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800332:	55                   	push   %ebp
  800333:	89 e5                	mov    %esp,%ebp
  800335:	57                   	push   %edi
  800336:	56                   	push   %esi
  800337:	53                   	push   %ebx
  800338:	83 ec 2c             	sub    $0x2c,%esp
  80033b:	8b 75 08             	mov    0x8(%ebp),%esi
  80033e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800341:	8b 7d 10             	mov    0x10(%ebp),%edi
  800344:	eb 12                	jmp    800358 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800346:	85 c0                	test   %eax,%eax
  800348:	0f 84 68 03 00 00    	je     8006b6 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80034e:	83 ec 08             	sub    $0x8,%esp
  800351:	53                   	push   %ebx
  800352:	50                   	push   %eax
  800353:	ff d6                	call   *%esi
  800355:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800358:	47                   	inc    %edi
  800359:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80035d:	83 f8 25             	cmp    $0x25,%eax
  800360:	75 e4                	jne    800346 <vprintfmt+0x14>
  800362:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800366:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80036d:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800374:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80037b:	ba 00 00 00 00       	mov    $0x0,%edx
  800380:	eb 07                	jmp    800389 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800382:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800385:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800389:	8d 47 01             	lea    0x1(%edi),%eax
  80038c:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80038f:	0f b6 0f             	movzbl (%edi),%ecx
  800392:	8a 07                	mov    (%edi),%al
  800394:	83 e8 23             	sub    $0x23,%eax
  800397:	3c 55                	cmp    $0x55,%al
  800399:	0f 87 fe 02 00 00    	ja     80069d <vprintfmt+0x36b>
  80039f:	0f b6 c0             	movzbl %al,%eax
  8003a2:	ff 24 85 e0 21 80 00 	jmp    *0x8021e0(,%eax,4)
  8003a9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003ac:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003b0:	eb d7                	jmp    800389 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003b5:	b8 00 00 00 00       	mov    $0x0,%eax
  8003ba:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003bd:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003c0:	01 c0                	add    %eax,%eax
  8003c2:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003c6:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003c9:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003cc:	83 fa 09             	cmp    $0x9,%edx
  8003cf:	77 34                	ja     800405 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003d1:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003d2:	eb e9                	jmp    8003bd <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003d4:	8b 45 14             	mov    0x14(%ebp),%eax
  8003d7:	8d 48 04             	lea    0x4(%eax),%ecx
  8003da:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003dd:	8b 00                	mov    (%eax),%eax
  8003df:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003e5:	eb 24                	jmp    80040b <vprintfmt+0xd9>
  8003e7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003eb:	79 07                	jns    8003f4 <vprintfmt+0xc2>
  8003ed:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003f7:	eb 90                	jmp    800389 <vprintfmt+0x57>
  8003f9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003fc:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800403:	eb 84                	jmp    800389 <vprintfmt+0x57>
  800405:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800408:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80040b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80040f:	0f 89 74 ff ff ff    	jns    800389 <vprintfmt+0x57>
				width = precision, precision = -1;
  800415:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800418:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80041b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800422:	e9 62 ff ff ff       	jmp    800389 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800427:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800428:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80042b:	e9 59 ff ff ff       	jmp    800389 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800430:	8b 45 14             	mov    0x14(%ebp),%eax
  800433:	8d 50 04             	lea    0x4(%eax),%edx
  800436:	89 55 14             	mov    %edx,0x14(%ebp)
  800439:	83 ec 08             	sub    $0x8,%esp
  80043c:	53                   	push   %ebx
  80043d:	ff 30                	pushl  (%eax)
  80043f:	ff d6                	call   *%esi
			break;
  800441:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800444:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800447:	e9 0c ff ff ff       	jmp    800358 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80044c:	8b 45 14             	mov    0x14(%ebp),%eax
  80044f:	8d 50 04             	lea    0x4(%eax),%edx
  800452:	89 55 14             	mov    %edx,0x14(%ebp)
  800455:	8b 00                	mov    (%eax),%eax
  800457:	85 c0                	test   %eax,%eax
  800459:	79 02                	jns    80045d <vprintfmt+0x12b>
  80045b:	f7 d8                	neg    %eax
  80045d:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80045f:	83 f8 0f             	cmp    $0xf,%eax
  800462:	7f 0b                	jg     80046f <vprintfmt+0x13d>
  800464:	8b 04 85 40 23 80 00 	mov    0x802340(,%eax,4),%eax
  80046b:	85 c0                	test   %eax,%eax
  80046d:	75 18                	jne    800487 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80046f:	52                   	push   %edx
  800470:	68 be 20 80 00       	push   $0x8020be
  800475:	53                   	push   %ebx
  800476:	56                   	push   %esi
  800477:	e8 99 fe ff ff       	call   800315 <printfmt>
  80047c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80047f:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800482:	e9 d1 fe ff ff       	jmp    800358 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800487:	50                   	push   %eax
  800488:	68 71 24 80 00       	push   $0x802471
  80048d:	53                   	push   %ebx
  80048e:	56                   	push   %esi
  80048f:	e8 81 fe ff ff       	call   800315 <printfmt>
  800494:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800497:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80049a:	e9 b9 fe ff ff       	jmp    800358 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80049f:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a2:	8d 50 04             	lea    0x4(%eax),%edx
  8004a5:	89 55 14             	mov    %edx,0x14(%ebp)
  8004a8:	8b 38                	mov    (%eax),%edi
  8004aa:	85 ff                	test   %edi,%edi
  8004ac:	75 05                	jne    8004b3 <vprintfmt+0x181>
				p = "(null)";
  8004ae:	bf b7 20 80 00       	mov    $0x8020b7,%edi
			if (width > 0 && padc != '-')
  8004b3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004b7:	0f 8e 90 00 00 00    	jle    80054d <vprintfmt+0x21b>
  8004bd:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004c1:	0f 84 8e 00 00 00    	je     800555 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004c7:	83 ec 08             	sub    $0x8,%esp
  8004ca:	ff 75 d0             	pushl  -0x30(%ebp)
  8004cd:	57                   	push   %edi
  8004ce:	e8 70 02 00 00       	call   800743 <strnlen>
  8004d3:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004d6:	29 c1                	sub    %eax,%ecx
  8004d8:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004db:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004de:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004e2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004e5:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004e8:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004ea:	eb 0d                	jmp    8004f9 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004ec:	83 ec 08             	sub    $0x8,%esp
  8004ef:	53                   	push   %ebx
  8004f0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004f3:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004f5:	4f                   	dec    %edi
  8004f6:	83 c4 10             	add    $0x10,%esp
  8004f9:	85 ff                	test   %edi,%edi
  8004fb:	7f ef                	jg     8004ec <vprintfmt+0x1ba>
  8004fd:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800500:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800503:	89 c8                	mov    %ecx,%eax
  800505:	85 c9                	test   %ecx,%ecx
  800507:	79 05                	jns    80050e <vprintfmt+0x1dc>
  800509:	b8 00 00 00 00       	mov    $0x0,%eax
  80050e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800511:	29 c1                	sub    %eax,%ecx
  800513:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800516:	89 75 08             	mov    %esi,0x8(%ebp)
  800519:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80051c:	eb 3d                	jmp    80055b <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80051e:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800522:	74 19                	je     80053d <vprintfmt+0x20b>
  800524:	0f be c0             	movsbl %al,%eax
  800527:	83 e8 20             	sub    $0x20,%eax
  80052a:	83 f8 5e             	cmp    $0x5e,%eax
  80052d:	76 0e                	jbe    80053d <vprintfmt+0x20b>
					putch('?', putdat);
  80052f:	83 ec 08             	sub    $0x8,%esp
  800532:	53                   	push   %ebx
  800533:	6a 3f                	push   $0x3f
  800535:	ff 55 08             	call   *0x8(%ebp)
  800538:	83 c4 10             	add    $0x10,%esp
  80053b:	eb 0b                	jmp    800548 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80053d:	83 ec 08             	sub    $0x8,%esp
  800540:	53                   	push   %ebx
  800541:	52                   	push   %edx
  800542:	ff 55 08             	call   *0x8(%ebp)
  800545:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800548:	ff 4d e4             	decl   -0x1c(%ebp)
  80054b:	eb 0e                	jmp    80055b <vprintfmt+0x229>
  80054d:	89 75 08             	mov    %esi,0x8(%ebp)
  800550:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800553:	eb 06                	jmp    80055b <vprintfmt+0x229>
  800555:	89 75 08             	mov    %esi,0x8(%ebp)
  800558:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80055b:	47                   	inc    %edi
  80055c:	8a 47 ff             	mov    -0x1(%edi),%al
  80055f:	0f be d0             	movsbl %al,%edx
  800562:	85 d2                	test   %edx,%edx
  800564:	74 1d                	je     800583 <vprintfmt+0x251>
  800566:	85 f6                	test   %esi,%esi
  800568:	78 b4                	js     80051e <vprintfmt+0x1ec>
  80056a:	4e                   	dec    %esi
  80056b:	79 b1                	jns    80051e <vprintfmt+0x1ec>
  80056d:	8b 75 08             	mov    0x8(%ebp),%esi
  800570:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800573:	eb 14                	jmp    800589 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800575:	83 ec 08             	sub    $0x8,%esp
  800578:	53                   	push   %ebx
  800579:	6a 20                	push   $0x20
  80057b:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80057d:	4f                   	dec    %edi
  80057e:	83 c4 10             	add    $0x10,%esp
  800581:	eb 06                	jmp    800589 <vprintfmt+0x257>
  800583:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800586:	8b 75 08             	mov    0x8(%ebp),%esi
  800589:	85 ff                	test   %edi,%edi
  80058b:	7f e8                	jg     800575 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80058d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800590:	e9 c3 fd ff ff       	jmp    800358 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800595:	83 fa 01             	cmp    $0x1,%edx
  800598:	7e 16                	jle    8005b0 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80059a:	8b 45 14             	mov    0x14(%ebp),%eax
  80059d:	8d 50 08             	lea    0x8(%eax),%edx
  8005a0:	89 55 14             	mov    %edx,0x14(%ebp)
  8005a3:	8b 50 04             	mov    0x4(%eax),%edx
  8005a6:	8b 00                	mov    (%eax),%eax
  8005a8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005ab:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005ae:	eb 32                	jmp    8005e2 <vprintfmt+0x2b0>
	else if (lflag)
  8005b0:	85 d2                	test   %edx,%edx
  8005b2:	74 18                	je     8005cc <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005b4:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b7:	8d 50 04             	lea    0x4(%eax),%edx
  8005ba:	89 55 14             	mov    %edx,0x14(%ebp)
  8005bd:	8b 00                	mov    (%eax),%eax
  8005bf:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005c2:	89 c1                	mov    %eax,%ecx
  8005c4:	c1 f9 1f             	sar    $0x1f,%ecx
  8005c7:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005ca:	eb 16                	jmp    8005e2 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005cc:	8b 45 14             	mov    0x14(%ebp),%eax
  8005cf:	8d 50 04             	lea    0x4(%eax),%edx
  8005d2:	89 55 14             	mov    %edx,0x14(%ebp)
  8005d5:	8b 00                	mov    (%eax),%eax
  8005d7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005da:	89 c1                	mov    %eax,%ecx
  8005dc:	c1 f9 1f             	sar    $0x1f,%ecx
  8005df:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005e2:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005e5:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005e8:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005ec:	79 76                	jns    800664 <vprintfmt+0x332>
				putch('-', putdat);
  8005ee:	83 ec 08             	sub    $0x8,%esp
  8005f1:	53                   	push   %ebx
  8005f2:	6a 2d                	push   $0x2d
  8005f4:	ff d6                	call   *%esi
				num = -(long long) num;
  8005f6:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005f9:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005fc:	f7 d8                	neg    %eax
  8005fe:	83 d2 00             	adc    $0x0,%edx
  800601:	f7 da                	neg    %edx
  800603:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800606:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80060b:	eb 5c                	jmp    800669 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80060d:	8d 45 14             	lea    0x14(%ebp),%eax
  800610:	e8 aa fc ff ff       	call   8002bf <getuint>
			base = 10;
  800615:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80061a:	eb 4d                	jmp    800669 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80061c:	8d 45 14             	lea    0x14(%ebp),%eax
  80061f:	e8 9b fc ff ff       	call   8002bf <getuint>
			base = 8;
  800624:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800629:	eb 3e                	jmp    800669 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80062b:	83 ec 08             	sub    $0x8,%esp
  80062e:	53                   	push   %ebx
  80062f:	6a 30                	push   $0x30
  800631:	ff d6                	call   *%esi
			putch('x', putdat);
  800633:	83 c4 08             	add    $0x8,%esp
  800636:	53                   	push   %ebx
  800637:	6a 78                	push   $0x78
  800639:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80063b:	8b 45 14             	mov    0x14(%ebp),%eax
  80063e:	8d 50 04             	lea    0x4(%eax),%edx
  800641:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800644:	8b 00                	mov    (%eax),%eax
  800646:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80064b:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80064e:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800653:	eb 14                	jmp    800669 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800655:	8d 45 14             	lea    0x14(%ebp),%eax
  800658:	e8 62 fc ff ff       	call   8002bf <getuint>
			base = 16;
  80065d:	b9 10 00 00 00       	mov    $0x10,%ecx
  800662:	eb 05                	jmp    800669 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800664:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800669:	83 ec 0c             	sub    $0xc,%esp
  80066c:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800670:	57                   	push   %edi
  800671:	ff 75 e4             	pushl  -0x1c(%ebp)
  800674:	51                   	push   %ecx
  800675:	52                   	push   %edx
  800676:	50                   	push   %eax
  800677:	89 da                	mov    %ebx,%edx
  800679:	89 f0                	mov    %esi,%eax
  80067b:	e8 92 fb ff ff       	call   800212 <printnum>
			break;
  800680:	83 c4 20             	add    $0x20,%esp
  800683:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800686:	e9 cd fc ff ff       	jmp    800358 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80068b:	83 ec 08             	sub    $0x8,%esp
  80068e:	53                   	push   %ebx
  80068f:	51                   	push   %ecx
  800690:	ff d6                	call   *%esi
			break;
  800692:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800695:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800698:	e9 bb fc ff ff       	jmp    800358 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80069d:	83 ec 08             	sub    $0x8,%esp
  8006a0:	53                   	push   %ebx
  8006a1:	6a 25                	push   $0x25
  8006a3:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006a5:	83 c4 10             	add    $0x10,%esp
  8006a8:	eb 01                	jmp    8006ab <vprintfmt+0x379>
  8006aa:	4f                   	dec    %edi
  8006ab:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006af:	75 f9                	jne    8006aa <vprintfmt+0x378>
  8006b1:	e9 a2 fc ff ff       	jmp    800358 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006b6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006b9:	5b                   	pop    %ebx
  8006ba:	5e                   	pop    %esi
  8006bb:	5f                   	pop    %edi
  8006bc:	5d                   	pop    %ebp
  8006bd:	c3                   	ret    

008006be <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006be:	55                   	push   %ebp
  8006bf:	89 e5                	mov    %esp,%ebp
  8006c1:	83 ec 18             	sub    $0x18,%esp
  8006c4:	8b 45 08             	mov    0x8(%ebp),%eax
  8006c7:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006ca:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006cd:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006d1:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006d4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006db:	85 c0                	test   %eax,%eax
  8006dd:	74 26                	je     800705 <vsnprintf+0x47>
  8006df:	85 d2                	test   %edx,%edx
  8006e1:	7e 29                	jle    80070c <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006e3:	ff 75 14             	pushl  0x14(%ebp)
  8006e6:	ff 75 10             	pushl  0x10(%ebp)
  8006e9:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006ec:	50                   	push   %eax
  8006ed:	68 f9 02 80 00       	push   $0x8002f9
  8006f2:	e8 3b fc ff ff       	call   800332 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006f7:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006fa:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800700:	83 c4 10             	add    $0x10,%esp
  800703:	eb 0c                	jmp    800711 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800705:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80070a:	eb 05                	jmp    800711 <vsnprintf+0x53>
  80070c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800711:	c9                   	leave  
  800712:	c3                   	ret    

00800713 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800713:	55                   	push   %ebp
  800714:	89 e5                	mov    %esp,%ebp
  800716:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800719:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80071c:	50                   	push   %eax
  80071d:	ff 75 10             	pushl  0x10(%ebp)
  800720:	ff 75 0c             	pushl  0xc(%ebp)
  800723:	ff 75 08             	pushl  0x8(%ebp)
  800726:	e8 93 ff ff ff       	call   8006be <vsnprintf>
	va_end(ap);

	return rc;
}
  80072b:	c9                   	leave  
  80072c:	c3                   	ret    

0080072d <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80072d:	55                   	push   %ebp
  80072e:	89 e5                	mov    %esp,%ebp
  800730:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800733:	b8 00 00 00 00       	mov    $0x0,%eax
  800738:	eb 01                	jmp    80073b <strlen+0xe>
		n++;
  80073a:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80073b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80073f:	75 f9                	jne    80073a <strlen+0xd>
		n++;
	return n;
}
  800741:	5d                   	pop    %ebp
  800742:	c3                   	ret    

00800743 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800743:	55                   	push   %ebp
  800744:	89 e5                	mov    %esp,%ebp
  800746:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800749:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80074c:	ba 00 00 00 00       	mov    $0x0,%edx
  800751:	eb 01                	jmp    800754 <strnlen+0x11>
		n++;
  800753:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800754:	39 c2                	cmp    %eax,%edx
  800756:	74 08                	je     800760 <strnlen+0x1d>
  800758:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80075c:	75 f5                	jne    800753 <strnlen+0x10>
  80075e:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800760:	5d                   	pop    %ebp
  800761:	c3                   	ret    

00800762 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800762:	55                   	push   %ebp
  800763:	89 e5                	mov    %esp,%ebp
  800765:	53                   	push   %ebx
  800766:	8b 45 08             	mov    0x8(%ebp),%eax
  800769:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80076c:	89 c2                	mov    %eax,%edx
  80076e:	42                   	inc    %edx
  80076f:	41                   	inc    %ecx
  800770:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800773:	88 5a ff             	mov    %bl,-0x1(%edx)
  800776:	84 db                	test   %bl,%bl
  800778:	75 f4                	jne    80076e <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80077a:	5b                   	pop    %ebx
  80077b:	5d                   	pop    %ebp
  80077c:	c3                   	ret    

0080077d <strcat>:

char *
strcat(char *dst, const char *src)
{
  80077d:	55                   	push   %ebp
  80077e:	89 e5                	mov    %esp,%ebp
  800780:	53                   	push   %ebx
  800781:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800784:	53                   	push   %ebx
  800785:	e8 a3 ff ff ff       	call   80072d <strlen>
  80078a:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80078d:	ff 75 0c             	pushl  0xc(%ebp)
  800790:	01 d8                	add    %ebx,%eax
  800792:	50                   	push   %eax
  800793:	e8 ca ff ff ff       	call   800762 <strcpy>
	return dst;
}
  800798:	89 d8                	mov    %ebx,%eax
  80079a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80079d:	c9                   	leave  
  80079e:	c3                   	ret    

0080079f <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80079f:	55                   	push   %ebp
  8007a0:	89 e5                	mov    %esp,%ebp
  8007a2:	56                   	push   %esi
  8007a3:	53                   	push   %ebx
  8007a4:	8b 75 08             	mov    0x8(%ebp),%esi
  8007a7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007aa:	89 f3                	mov    %esi,%ebx
  8007ac:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007af:	89 f2                	mov    %esi,%edx
  8007b1:	eb 0c                	jmp    8007bf <strncpy+0x20>
		*dst++ = *src;
  8007b3:	42                   	inc    %edx
  8007b4:	8a 01                	mov    (%ecx),%al
  8007b6:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007b9:	80 39 01             	cmpb   $0x1,(%ecx)
  8007bc:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007bf:	39 da                	cmp    %ebx,%edx
  8007c1:	75 f0                	jne    8007b3 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007c3:	89 f0                	mov    %esi,%eax
  8007c5:	5b                   	pop    %ebx
  8007c6:	5e                   	pop    %esi
  8007c7:	5d                   	pop    %ebp
  8007c8:	c3                   	ret    

008007c9 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007c9:	55                   	push   %ebp
  8007ca:	89 e5                	mov    %esp,%ebp
  8007cc:	56                   	push   %esi
  8007cd:	53                   	push   %ebx
  8007ce:	8b 75 08             	mov    0x8(%ebp),%esi
  8007d1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007d4:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007d7:	85 c0                	test   %eax,%eax
  8007d9:	74 1e                	je     8007f9 <strlcpy+0x30>
  8007db:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007df:	89 f2                	mov    %esi,%edx
  8007e1:	eb 05                	jmp    8007e8 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007e3:	42                   	inc    %edx
  8007e4:	41                   	inc    %ecx
  8007e5:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007e8:	39 c2                	cmp    %eax,%edx
  8007ea:	74 08                	je     8007f4 <strlcpy+0x2b>
  8007ec:	8a 19                	mov    (%ecx),%bl
  8007ee:	84 db                	test   %bl,%bl
  8007f0:	75 f1                	jne    8007e3 <strlcpy+0x1a>
  8007f2:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007f4:	c6 00 00             	movb   $0x0,(%eax)
  8007f7:	eb 02                	jmp    8007fb <strlcpy+0x32>
  8007f9:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007fb:	29 f0                	sub    %esi,%eax
}
  8007fd:	5b                   	pop    %ebx
  8007fe:	5e                   	pop    %esi
  8007ff:	5d                   	pop    %ebp
  800800:	c3                   	ret    

00800801 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800801:	55                   	push   %ebp
  800802:	89 e5                	mov    %esp,%ebp
  800804:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800807:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80080a:	eb 02                	jmp    80080e <strcmp+0xd>
		p++, q++;
  80080c:	41                   	inc    %ecx
  80080d:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80080e:	8a 01                	mov    (%ecx),%al
  800810:	84 c0                	test   %al,%al
  800812:	74 04                	je     800818 <strcmp+0x17>
  800814:	3a 02                	cmp    (%edx),%al
  800816:	74 f4                	je     80080c <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800818:	0f b6 c0             	movzbl %al,%eax
  80081b:	0f b6 12             	movzbl (%edx),%edx
  80081e:	29 d0                	sub    %edx,%eax
}
  800820:	5d                   	pop    %ebp
  800821:	c3                   	ret    

00800822 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800822:	55                   	push   %ebp
  800823:	89 e5                	mov    %esp,%ebp
  800825:	53                   	push   %ebx
  800826:	8b 45 08             	mov    0x8(%ebp),%eax
  800829:	8b 55 0c             	mov    0xc(%ebp),%edx
  80082c:	89 c3                	mov    %eax,%ebx
  80082e:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800831:	eb 02                	jmp    800835 <strncmp+0x13>
		n--, p++, q++;
  800833:	40                   	inc    %eax
  800834:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800835:	39 d8                	cmp    %ebx,%eax
  800837:	74 14                	je     80084d <strncmp+0x2b>
  800839:	8a 08                	mov    (%eax),%cl
  80083b:	84 c9                	test   %cl,%cl
  80083d:	74 04                	je     800843 <strncmp+0x21>
  80083f:	3a 0a                	cmp    (%edx),%cl
  800841:	74 f0                	je     800833 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800843:	0f b6 00             	movzbl (%eax),%eax
  800846:	0f b6 12             	movzbl (%edx),%edx
  800849:	29 d0                	sub    %edx,%eax
  80084b:	eb 05                	jmp    800852 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80084d:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800852:	5b                   	pop    %ebx
  800853:	5d                   	pop    %ebp
  800854:	c3                   	ret    

00800855 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800855:	55                   	push   %ebp
  800856:	89 e5                	mov    %esp,%ebp
  800858:	8b 45 08             	mov    0x8(%ebp),%eax
  80085b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80085e:	eb 05                	jmp    800865 <strchr+0x10>
		if (*s == c)
  800860:	38 ca                	cmp    %cl,%dl
  800862:	74 0c                	je     800870 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800864:	40                   	inc    %eax
  800865:	8a 10                	mov    (%eax),%dl
  800867:	84 d2                	test   %dl,%dl
  800869:	75 f5                	jne    800860 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80086b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800870:	5d                   	pop    %ebp
  800871:	c3                   	ret    

00800872 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800872:	55                   	push   %ebp
  800873:	89 e5                	mov    %esp,%ebp
  800875:	8b 45 08             	mov    0x8(%ebp),%eax
  800878:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80087b:	eb 05                	jmp    800882 <strfind+0x10>
		if (*s == c)
  80087d:	38 ca                	cmp    %cl,%dl
  80087f:	74 07                	je     800888 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800881:	40                   	inc    %eax
  800882:	8a 10                	mov    (%eax),%dl
  800884:	84 d2                	test   %dl,%dl
  800886:	75 f5                	jne    80087d <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800888:	5d                   	pop    %ebp
  800889:	c3                   	ret    

0080088a <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80088a:	55                   	push   %ebp
  80088b:	89 e5                	mov    %esp,%ebp
  80088d:	57                   	push   %edi
  80088e:	56                   	push   %esi
  80088f:	53                   	push   %ebx
  800890:	8b 7d 08             	mov    0x8(%ebp),%edi
  800893:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800896:	85 c9                	test   %ecx,%ecx
  800898:	74 36                	je     8008d0 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80089a:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008a0:	75 28                	jne    8008ca <memset+0x40>
  8008a2:	f6 c1 03             	test   $0x3,%cl
  8008a5:	75 23                	jne    8008ca <memset+0x40>
		c &= 0xFF;
  8008a7:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008ab:	89 d3                	mov    %edx,%ebx
  8008ad:	c1 e3 08             	shl    $0x8,%ebx
  8008b0:	89 d6                	mov    %edx,%esi
  8008b2:	c1 e6 18             	shl    $0x18,%esi
  8008b5:	89 d0                	mov    %edx,%eax
  8008b7:	c1 e0 10             	shl    $0x10,%eax
  8008ba:	09 f0                	or     %esi,%eax
  8008bc:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008be:	89 d8                	mov    %ebx,%eax
  8008c0:	09 d0                	or     %edx,%eax
  8008c2:	c1 e9 02             	shr    $0x2,%ecx
  8008c5:	fc                   	cld    
  8008c6:	f3 ab                	rep stos %eax,%es:(%edi)
  8008c8:	eb 06                	jmp    8008d0 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008ca:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008cd:	fc                   	cld    
  8008ce:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008d0:	89 f8                	mov    %edi,%eax
  8008d2:	5b                   	pop    %ebx
  8008d3:	5e                   	pop    %esi
  8008d4:	5f                   	pop    %edi
  8008d5:	5d                   	pop    %ebp
  8008d6:	c3                   	ret    

008008d7 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008d7:	55                   	push   %ebp
  8008d8:	89 e5                	mov    %esp,%ebp
  8008da:	57                   	push   %edi
  8008db:	56                   	push   %esi
  8008dc:	8b 45 08             	mov    0x8(%ebp),%eax
  8008df:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008e2:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008e5:	39 c6                	cmp    %eax,%esi
  8008e7:	73 33                	jae    80091c <memmove+0x45>
  8008e9:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008ec:	39 d0                	cmp    %edx,%eax
  8008ee:	73 2c                	jae    80091c <memmove+0x45>
		s += n;
		d += n;
  8008f0:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008f3:	89 d6                	mov    %edx,%esi
  8008f5:	09 fe                	or     %edi,%esi
  8008f7:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008fd:	75 13                	jne    800912 <memmove+0x3b>
  8008ff:	f6 c1 03             	test   $0x3,%cl
  800902:	75 0e                	jne    800912 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800904:	83 ef 04             	sub    $0x4,%edi
  800907:	8d 72 fc             	lea    -0x4(%edx),%esi
  80090a:	c1 e9 02             	shr    $0x2,%ecx
  80090d:	fd                   	std    
  80090e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800910:	eb 07                	jmp    800919 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800912:	4f                   	dec    %edi
  800913:	8d 72 ff             	lea    -0x1(%edx),%esi
  800916:	fd                   	std    
  800917:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800919:	fc                   	cld    
  80091a:	eb 1d                	jmp    800939 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80091c:	89 f2                	mov    %esi,%edx
  80091e:	09 c2                	or     %eax,%edx
  800920:	f6 c2 03             	test   $0x3,%dl
  800923:	75 0f                	jne    800934 <memmove+0x5d>
  800925:	f6 c1 03             	test   $0x3,%cl
  800928:	75 0a                	jne    800934 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80092a:	c1 e9 02             	shr    $0x2,%ecx
  80092d:	89 c7                	mov    %eax,%edi
  80092f:	fc                   	cld    
  800930:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800932:	eb 05                	jmp    800939 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800934:	89 c7                	mov    %eax,%edi
  800936:	fc                   	cld    
  800937:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800939:	5e                   	pop    %esi
  80093a:	5f                   	pop    %edi
  80093b:	5d                   	pop    %ebp
  80093c:	c3                   	ret    

0080093d <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80093d:	55                   	push   %ebp
  80093e:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800940:	ff 75 10             	pushl  0x10(%ebp)
  800943:	ff 75 0c             	pushl  0xc(%ebp)
  800946:	ff 75 08             	pushl  0x8(%ebp)
  800949:	e8 89 ff ff ff       	call   8008d7 <memmove>
}
  80094e:	c9                   	leave  
  80094f:	c3                   	ret    

00800950 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800950:	55                   	push   %ebp
  800951:	89 e5                	mov    %esp,%ebp
  800953:	56                   	push   %esi
  800954:	53                   	push   %ebx
  800955:	8b 45 08             	mov    0x8(%ebp),%eax
  800958:	8b 55 0c             	mov    0xc(%ebp),%edx
  80095b:	89 c6                	mov    %eax,%esi
  80095d:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800960:	eb 14                	jmp    800976 <memcmp+0x26>
		if (*s1 != *s2)
  800962:	8a 08                	mov    (%eax),%cl
  800964:	8a 1a                	mov    (%edx),%bl
  800966:	38 d9                	cmp    %bl,%cl
  800968:	74 0a                	je     800974 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80096a:	0f b6 c1             	movzbl %cl,%eax
  80096d:	0f b6 db             	movzbl %bl,%ebx
  800970:	29 d8                	sub    %ebx,%eax
  800972:	eb 0b                	jmp    80097f <memcmp+0x2f>
		s1++, s2++;
  800974:	40                   	inc    %eax
  800975:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800976:	39 f0                	cmp    %esi,%eax
  800978:	75 e8                	jne    800962 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80097a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80097f:	5b                   	pop    %ebx
  800980:	5e                   	pop    %esi
  800981:	5d                   	pop    %ebp
  800982:	c3                   	ret    

00800983 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800983:	55                   	push   %ebp
  800984:	89 e5                	mov    %esp,%ebp
  800986:	53                   	push   %ebx
  800987:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80098a:	89 c1                	mov    %eax,%ecx
  80098c:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80098f:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800993:	eb 08                	jmp    80099d <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800995:	0f b6 10             	movzbl (%eax),%edx
  800998:	39 da                	cmp    %ebx,%edx
  80099a:	74 05                	je     8009a1 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80099c:	40                   	inc    %eax
  80099d:	39 c8                	cmp    %ecx,%eax
  80099f:	72 f4                	jb     800995 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009a1:	5b                   	pop    %ebx
  8009a2:	5d                   	pop    %ebp
  8009a3:	c3                   	ret    

008009a4 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009a4:	55                   	push   %ebp
  8009a5:	89 e5                	mov    %esp,%ebp
  8009a7:	57                   	push   %edi
  8009a8:	56                   	push   %esi
  8009a9:	53                   	push   %ebx
  8009aa:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009ad:	eb 01                	jmp    8009b0 <strtol+0xc>
		s++;
  8009af:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009b0:	8a 01                	mov    (%ecx),%al
  8009b2:	3c 20                	cmp    $0x20,%al
  8009b4:	74 f9                	je     8009af <strtol+0xb>
  8009b6:	3c 09                	cmp    $0x9,%al
  8009b8:	74 f5                	je     8009af <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009ba:	3c 2b                	cmp    $0x2b,%al
  8009bc:	75 08                	jne    8009c6 <strtol+0x22>
		s++;
  8009be:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009bf:	bf 00 00 00 00       	mov    $0x0,%edi
  8009c4:	eb 11                	jmp    8009d7 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009c6:	3c 2d                	cmp    $0x2d,%al
  8009c8:	75 08                	jne    8009d2 <strtol+0x2e>
		s++, neg = 1;
  8009ca:	41                   	inc    %ecx
  8009cb:	bf 01 00 00 00       	mov    $0x1,%edi
  8009d0:	eb 05                	jmp    8009d7 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009d2:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009d7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009db:	0f 84 87 00 00 00    	je     800a68 <strtol+0xc4>
  8009e1:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009e5:	75 27                	jne    800a0e <strtol+0x6a>
  8009e7:	80 39 30             	cmpb   $0x30,(%ecx)
  8009ea:	75 22                	jne    800a0e <strtol+0x6a>
  8009ec:	e9 88 00 00 00       	jmp    800a79 <strtol+0xd5>
		s += 2, base = 16;
  8009f1:	83 c1 02             	add    $0x2,%ecx
  8009f4:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009fb:	eb 11                	jmp    800a0e <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009fd:	41                   	inc    %ecx
  8009fe:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a05:	eb 07                	jmp    800a0e <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a07:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a0e:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a13:	8a 11                	mov    (%ecx),%dl
  800a15:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a18:	80 fb 09             	cmp    $0x9,%bl
  800a1b:	77 08                	ja     800a25 <strtol+0x81>
			dig = *s - '0';
  800a1d:	0f be d2             	movsbl %dl,%edx
  800a20:	83 ea 30             	sub    $0x30,%edx
  800a23:	eb 22                	jmp    800a47 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a25:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a28:	89 f3                	mov    %esi,%ebx
  800a2a:	80 fb 19             	cmp    $0x19,%bl
  800a2d:	77 08                	ja     800a37 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a2f:	0f be d2             	movsbl %dl,%edx
  800a32:	83 ea 57             	sub    $0x57,%edx
  800a35:	eb 10                	jmp    800a47 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a37:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a3a:	89 f3                	mov    %esi,%ebx
  800a3c:	80 fb 19             	cmp    $0x19,%bl
  800a3f:	77 14                	ja     800a55 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a41:	0f be d2             	movsbl %dl,%edx
  800a44:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a47:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a4a:	7d 09                	jge    800a55 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a4c:	41                   	inc    %ecx
  800a4d:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a51:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a53:	eb be                	jmp    800a13 <strtol+0x6f>

	if (endptr)
  800a55:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a59:	74 05                	je     800a60 <strtol+0xbc>
		*endptr = (char *) s;
  800a5b:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a5e:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a60:	85 ff                	test   %edi,%edi
  800a62:	74 21                	je     800a85 <strtol+0xe1>
  800a64:	f7 d8                	neg    %eax
  800a66:	eb 1d                	jmp    800a85 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a68:	80 39 30             	cmpb   $0x30,(%ecx)
  800a6b:	75 9a                	jne    800a07 <strtol+0x63>
  800a6d:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a71:	0f 84 7a ff ff ff    	je     8009f1 <strtol+0x4d>
  800a77:	eb 84                	jmp    8009fd <strtol+0x59>
  800a79:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a7d:	0f 84 6e ff ff ff    	je     8009f1 <strtol+0x4d>
  800a83:	eb 89                	jmp    800a0e <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a85:	5b                   	pop    %ebx
  800a86:	5e                   	pop    %esi
  800a87:	5f                   	pop    %edi
  800a88:	5d                   	pop    %ebp
  800a89:	c3                   	ret    

00800a8a <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a8a:	55                   	push   %ebp
  800a8b:	89 e5                	mov    %esp,%ebp
  800a8d:	57                   	push   %edi
  800a8e:	56                   	push   %esi
  800a8f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a90:	b8 00 00 00 00       	mov    $0x0,%eax
  800a95:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a98:	8b 55 08             	mov    0x8(%ebp),%edx
  800a9b:	89 c3                	mov    %eax,%ebx
  800a9d:	89 c7                	mov    %eax,%edi
  800a9f:	89 c6                	mov    %eax,%esi
  800aa1:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800aa3:	5b                   	pop    %ebx
  800aa4:	5e                   	pop    %esi
  800aa5:	5f                   	pop    %edi
  800aa6:	5d                   	pop    %ebp
  800aa7:	c3                   	ret    

00800aa8 <sys_cgetc>:

int
sys_cgetc(void)
{
  800aa8:	55                   	push   %ebp
  800aa9:	89 e5                	mov    %esp,%ebp
  800aab:	57                   	push   %edi
  800aac:	56                   	push   %esi
  800aad:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aae:	ba 00 00 00 00       	mov    $0x0,%edx
  800ab3:	b8 01 00 00 00       	mov    $0x1,%eax
  800ab8:	89 d1                	mov    %edx,%ecx
  800aba:	89 d3                	mov    %edx,%ebx
  800abc:	89 d7                	mov    %edx,%edi
  800abe:	89 d6                	mov    %edx,%esi
  800ac0:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800ac2:	5b                   	pop    %ebx
  800ac3:	5e                   	pop    %esi
  800ac4:	5f                   	pop    %edi
  800ac5:	5d                   	pop    %ebp
  800ac6:	c3                   	ret    

00800ac7 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800ac7:	55                   	push   %ebp
  800ac8:	89 e5                	mov    %esp,%ebp
  800aca:	57                   	push   %edi
  800acb:	56                   	push   %esi
  800acc:	53                   	push   %ebx
  800acd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ad0:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ad5:	b8 03 00 00 00       	mov    $0x3,%eax
  800ada:	8b 55 08             	mov    0x8(%ebp),%edx
  800add:	89 cb                	mov    %ecx,%ebx
  800adf:	89 cf                	mov    %ecx,%edi
  800ae1:	89 ce                	mov    %ecx,%esi
  800ae3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ae5:	85 c0                	test   %eax,%eax
  800ae7:	7e 17                	jle    800b00 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ae9:	83 ec 0c             	sub    $0xc,%esp
  800aec:	50                   	push   %eax
  800aed:	6a 03                	push   $0x3
  800aef:	68 9f 23 80 00       	push   $0x80239f
  800af4:	6a 23                	push   $0x23
  800af6:	68 bc 23 80 00       	push   $0x8023bc
  800afb:	e8 5e 11 00 00       	call   801c5e <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b00:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b03:	5b                   	pop    %ebx
  800b04:	5e                   	pop    %esi
  800b05:	5f                   	pop    %edi
  800b06:	5d                   	pop    %ebp
  800b07:	c3                   	ret    

00800b08 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b08:	55                   	push   %ebp
  800b09:	89 e5                	mov    %esp,%ebp
  800b0b:	57                   	push   %edi
  800b0c:	56                   	push   %esi
  800b0d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b0e:	ba 00 00 00 00       	mov    $0x0,%edx
  800b13:	b8 02 00 00 00       	mov    $0x2,%eax
  800b18:	89 d1                	mov    %edx,%ecx
  800b1a:	89 d3                	mov    %edx,%ebx
  800b1c:	89 d7                	mov    %edx,%edi
  800b1e:	89 d6                	mov    %edx,%esi
  800b20:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b22:	5b                   	pop    %ebx
  800b23:	5e                   	pop    %esi
  800b24:	5f                   	pop    %edi
  800b25:	5d                   	pop    %ebp
  800b26:	c3                   	ret    

00800b27 <sys_yield>:

void
sys_yield(void)
{
  800b27:	55                   	push   %ebp
  800b28:	89 e5                	mov    %esp,%ebp
  800b2a:	57                   	push   %edi
  800b2b:	56                   	push   %esi
  800b2c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b2d:	ba 00 00 00 00       	mov    $0x0,%edx
  800b32:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b37:	89 d1                	mov    %edx,%ecx
  800b39:	89 d3                	mov    %edx,%ebx
  800b3b:	89 d7                	mov    %edx,%edi
  800b3d:	89 d6                	mov    %edx,%esi
  800b3f:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b41:	5b                   	pop    %ebx
  800b42:	5e                   	pop    %esi
  800b43:	5f                   	pop    %edi
  800b44:	5d                   	pop    %ebp
  800b45:	c3                   	ret    

00800b46 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b46:	55                   	push   %ebp
  800b47:	89 e5                	mov    %esp,%ebp
  800b49:	57                   	push   %edi
  800b4a:	56                   	push   %esi
  800b4b:	53                   	push   %ebx
  800b4c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b4f:	be 00 00 00 00       	mov    $0x0,%esi
  800b54:	b8 04 00 00 00       	mov    $0x4,%eax
  800b59:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b5c:	8b 55 08             	mov    0x8(%ebp),%edx
  800b5f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b62:	89 f7                	mov    %esi,%edi
  800b64:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b66:	85 c0                	test   %eax,%eax
  800b68:	7e 17                	jle    800b81 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b6a:	83 ec 0c             	sub    $0xc,%esp
  800b6d:	50                   	push   %eax
  800b6e:	6a 04                	push   $0x4
  800b70:	68 9f 23 80 00       	push   $0x80239f
  800b75:	6a 23                	push   $0x23
  800b77:	68 bc 23 80 00       	push   $0x8023bc
  800b7c:	e8 dd 10 00 00       	call   801c5e <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b81:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b84:	5b                   	pop    %ebx
  800b85:	5e                   	pop    %esi
  800b86:	5f                   	pop    %edi
  800b87:	5d                   	pop    %ebp
  800b88:	c3                   	ret    

00800b89 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b89:	55                   	push   %ebp
  800b8a:	89 e5                	mov    %esp,%ebp
  800b8c:	57                   	push   %edi
  800b8d:	56                   	push   %esi
  800b8e:	53                   	push   %ebx
  800b8f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b92:	b8 05 00 00 00       	mov    $0x5,%eax
  800b97:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b9a:	8b 55 08             	mov    0x8(%ebp),%edx
  800b9d:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ba0:	8b 7d 14             	mov    0x14(%ebp),%edi
  800ba3:	8b 75 18             	mov    0x18(%ebp),%esi
  800ba6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ba8:	85 c0                	test   %eax,%eax
  800baa:	7e 17                	jle    800bc3 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bac:	83 ec 0c             	sub    $0xc,%esp
  800baf:	50                   	push   %eax
  800bb0:	6a 05                	push   $0x5
  800bb2:	68 9f 23 80 00       	push   $0x80239f
  800bb7:	6a 23                	push   $0x23
  800bb9:	68 bc 23 80 00       	push   $0x8023bc
  800bbe:	e8 9b 10 00 00       	call   801c5e <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bc3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bc6:	5b                   	pop    %ebx
  800bc7:	5e                   	pop    %esi
  800bc8:	5f                   	pop    %edi
  800bc9:	5d                   	pop    %ebp
  800bca:	c3                   	ret    

00800bcb <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bcb:	55                   	push   %ebp
  800bcc:	89 e5                	mov    %esp,%ebp
  800bce:	57                   	push   %edi
  800bcf:	56                   	push   %esi
  800bd0:	53                   	push   %ebx
  800bd1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bd4:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bd9:	b8 06 00 00 00       	mov    $0x6,%eax
  800bde:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800be1:	8b 55 08             	mov    0x8(%ebp),%edx
  800be4:	89 df                	mov    %ebx,%edi
  800be6:	89 de                	mov    %ebx,%esi
  800be8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bea:	85 c0                	test   %eax,%eax
  800bec:	7e 17                	jle    800c05 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bee:	83 ec 0c             	sub    $0xc,%esp
  800bf1:	50                   	push   %eax
  800bf2:	6a 06                	push   $0x6
  800bf4:	68 9f 23 80 00       	push   $0x80239f
  800bf9:	6a 23                	push   $0x23
  800bfb:	68 bc 23 80 00       	push   $0x8023bc
  800c00:	e8 59 10 00 00       	call   801c5e <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c05:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c08:	5b                   	pop    %ebx
  800c09:	5e                   	pop    %esi
  800c0a:	5f                   	pop    %edi
  800c0b:	5d                   	pop    %ebp
  800c0c:	c3                   	ret    

00800c0d <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c0d:	55                   	push   %ebp
  800c0e:	89 e5                	mov    %esp,%ebp
  800c10:	57                   	push   %edi
  800c11:	56                   	push   %esi
  800c12:	53                   	push   %ebx
  800c13:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c16:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c1b:	b8 08 00 00 00       	mov    $0x8,%eax
  800c20:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c23:	8b 55 08             	mov    0x8(%ebp),%edx
  800c26:	89 df                	mov    %ebx,%edi
  800c28:	89 de                	mov    %ebx,%esi
  800c2a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c2c:	85 c0                	test   %eax,%eax
  800c2e:	7e 17                	jle    800c47 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c30:	83 ec 0c             	sub    $0xc,%esp
  800c33:	50                   	push   %eax
  800c34:	6a 08                	push   $0x8
  800c36:	68 9f 23 80 00       	push   $0x80239f
  800c3b:	6a 23                	push   $0x23
  800c3d:	68 bc 23 80 00       	push   $0x8023bc
  800c42:	e8 17 10 00 00       	call   801c5e <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c47:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c4a:	5b                   	pop    %ebx
  800c4b:	5e                   	pop    %esi
  800c4c:	5f                   	pop    %edi
  800c4d:	5d                   	pop    %ebp
  800c4e:	c3                   	ret    

00800c4f <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c4f:	55                   	push   %ebp
  800c50:	89 e5                	mov    %esp,%ebp
  800c52:	57                   	push   %edi
  800c53:	56                   	push   %esi
  800c54:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c55:	ba 00 00 00 00       	mov    $0x0,%edx
  800c5a:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c5f:	89 d1                	mov    %edx,%ecx
  800c61:	89 d3                	mov    %edx,%ebx
  800c63:	89 d7                	mov    %edx,%edi
  800c65:	89 d6                	mov    %edx,%esi
  800c67:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c69:	5b                   	pop    %ebx
  800c6a:	5e                   	pop    %esi
  800c6b:	5f                   	pop    %edi
  800c6c:	5d                   	pop    %ebp
  800c6d:	c3                   	ret    

00800c6e <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c6e:	55                   	push   %ebp
  800c6f:	89 e5                	mov    %esp,%ebp
  800c71:	57                   	push   %edi
  800c72:	56                   	push   %esi
  800c73:	53                   	push   %ebx
  800c74:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c77:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c7c:	b8 09 00 00 00       	mov    $0x9,%eax
  800c81:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c84:	8b 55 08             	mov    0x8(%ebp),%edx
  800c87:	89 df                	mov    %ebx,%edi
  800c89:	89 de                	mov    %ebx,%esi
  800c8b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c8d:	85 c0                	test   %eax,%eax
  800c8f:	7e 17                	jle    800ca8 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c91:	83 ec 0c             	sub    $0xc,%esp
  800c94:	50                   	push   %eax
  800c95:	6a 09                	push   $0x9
  800c97:	68 9f 23 80 00       	push   $0x80239f
  800c9c:	6a 23                	push   $0x23
  800c9e:	68 bc 23 80 00       	push   $0x8023bc
  800ca3:	e8 b6 0f 00 00       	call   801c5e <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800ca8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cab:	5b                   	pop    %ebx
  800cac:	5e                   	pop    %esi
  800cad:	5f                   	pop    %edi
  800cae:	5d                   	pop    %ebp
  800caf:	c3                   	ret    

00800cb0 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800cb0:	55                   	push   %ebp
  800cb1:	89 e5                	mov    %esp,%ebp
  800cb3:	57                   	push   %edi
  800cb4:	56                   	push   %esi
  800cb5:	53                   	push   %ebx
  800cb6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cb9:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cbe:	b8 0a 00 00 00       	mov    $0xa,%eax
  800cc3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cc6:	8b 55 08             	mov    0x8(%ebp),%edx
  800cc9:	89 df                	mov    %ebx,%edi
  800ccb:	89 de                	mov    %ebx,%esi
  800ccd:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ccf:	85 c0                	test   %eax,%eax
  800cd1:	7e 17                	jle    800cea <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cd3:	83 ec 0c             	sub    $0xc,%esp
  800cd6:	50                   	push   %eax
  800cd7:	6a 0a                	push   $0xa
  800cd9:	68 9f 23 80 00       	push   $0x80239f
  800cde:	6a 23                	push   $0x23
  800ce0:	68 bc 23 80 00       	push   $0x8023bc
  800ce5:	e8 74 0f 00 00       	call   801c5e <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cea:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ced:	5b                   	pop    %ebx
  800cee:	5e                   	pop    %esi
  800cef:	5f                   	pop    %edi
  800cf0:	5d                   	pop    %ebp
  800cf1:	c3                   	ret    

00800cf2 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800cf2:	55                   	push   %ebp
  800cf3:	89 e5                	mov    %esp,%ebp
  800cf5:	57                   	push   %edi
  800cf6:	56                   	push   %esi
  800cf7:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cf8:	be 00 00 00 00       	mov    $0x0,%esi
  800cfd:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d02:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d05:	8b 55 08             	mov    0x8(%ebp),%edx
  800d08:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d0b:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d0e:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d10:	5b                   	pop    %ebx
  800d11:	5e                   	pop    %esi
  800d12:	5f                   	pop    %edi
  800d13:	5d                   	pop    %ebp
  800d14:	c3                   	ret    

00800d15 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d15:	55                   	push   %ebp
  800d16:	89 e5                	mov    %esp,%ebp
  800d18:	57                   	push   %edi
  800d19:	56                   	push   %esi
  800d1a:	53                   	push   %ebx
  800d1b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d1e:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d23:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d28:	8b 55 08             	mov    0x8(%ebp),%edx
  800d2b:	89 cb                	mov    %ecx,%ebx
  800d2d:	89 cf                	mov    %ecx,%edi
  800d2f:	89 ce                	mov    %ecx,%esi
  800d31:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d33:	85 c0                	test   %eax,%eax
  800d35:	7e 17                	jle    800d4e <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d37:	83 ec 0c             	sub    $0xc,%esp
  800d3a:	50                   	push   %eax
  800d3b:	6a 0e                	push   $0xe
  800d3d:	68 9f 23 80 00       	push   $0x80239f
  800d42:	6a 23                	push   $0x23
  800d44:	68 bc 23 80 00       	push   $0x8023bc
  800d49:	e8 10 0f 00 00       	call   801c5e <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d4e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d51:	5b                   	pop    %ebx
  800d52:	5e                   	pop    %esi
  800d53:	5f                   	pop    %edi
  800d54:	5d                   	pop    %ebp
  800d55:	c3                   	ret    

00800d56 <argstart>:
#include <inc/args.h>
#include <inc/string.h>

void
argstart(int *argc, char **argv, struct Argstate *args)
{
  800d56:	55                   	push   %ebp
  800d57:	89 e5                	mov    %esp,%ebp
  800d59:	8b 55 08             	mov    0x8(%ebp),%edx
  800d5c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d5f:	8b 45 10             	mov    0x10(%ebp),%eax
	args->argc = argc;
  800d62:	89 10                	mov    %edx,(%eax)
	args->argv = (const char **) argv;
  800d64:	89 48 04             	mov    %ecx,0x4(%eax)
	args->curarg = (*argc > 1 && argv ? "" : 0);
  800d67:	83 3a 01             	cmpl   $0x1,(%edx)
  800d6a:	7e 0b                	jle    800d77 <argstart+0x21>
  800d6c:	85 c9                	test   %ecx,%ecx
  800d6e:	74 0e                	je     800d7e <argstart+0x28>
  800d70:	ba 71 20 80 00       	mov    $0x802071,%edx
  800d75:	eb 0c                	jmp    800d83 <argstart+0x2d>
  800d77:	ba 00 00 00 00       	mov    $0x0,%edx
  800d7c:	eb 05                	jmp    800d83 <argstart+0x2d>
  800d7e:	ba 00 00 00 00       	mov    $0x0,%edx
  800d83:	89 50 08             	mov    %edx,0x8(%eax)
	args->argvalue = 0;
  800d86:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
}
  800d8d:	5d                   	pop    %ebp
  800d8e:	c3                   	ret    

00800d8f <argnext>:

int
argnext(struct Argstate *args)
{
  800d8f:	55                   	push   %ebp
  800d90:	89 e5                	mov    %esp,%ebp
  800d92:	53                   	push   %ebx
  800d93:	83 ec 04             	sub    $0x4,%esp
  800d96:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int arg;

	args->argvalue = 0;
  800d99:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)

	// Done processing arguments if args->curarg == 0
	if (args->curarg == 0)
  800da0:	8b 43 08             	mov    0x8(%ebx),%eax
  800da3:	85 c0                	test   %eax,%eax
  800da5:	74 6a                	je     800e11 <argnext+0x82>
		return -1;

	if (!*args->curarg) {
  800da7:	80 38 00             	cmpb   $0x0,(%eax)
  800daa:	75 4b                	jne    800df7 <argnext+0x68>
		// Need to process the next argument
		// Check for end of argument list
		if (*args->argc == 1
  800dac:	8b 0b                	mov    (%ebx),%ecx
  800dae:	83 39 01             	cmpl   $0x1,(%ecx)
  800db1:	74 50                	je     800e03 <argnext+0x74>
		    || args->argv[1][0] != '-'
  800db3:	8b 53 04             	mov    0x4(%ebx),%edx
  800db6:	8b 42 04             	mov    0x4(%edx),%eax
  800db9:	80 38 2d             	cmpb   $0x2d,(%eax)
  800dbc:	75 45                	jne    800e03 <argnext+0x74>
		    || args->argv[1][1] == '\0')
  800dbe:	80 78 01 00          	cmpb   $0x0,0x1(%eax)
  800dc2:	74 3f                	je     800e03 <argnext+0x74>
			goto endofargs;
		// Shift arguments down one
		args->curarg = args->argv[1] + 1;
  800dc4:	40                   	inc    %eax
  800dc5:	89 43 08             	mov    %eax,0x8(%ebx)
		memmove(args->argv + 1, args->argv + 2, sizeof(const char *) * (*args->argc - 1));
  800dc8:	83 ec 04             	sub    $0x4,%esp
  800dcb:	8b 01                	mov    (%ecx),%eax
  800dcd:	8d 04 85 fc ff ff ff 	lea    -0x4(,%eax,4),%eax
  800dd4:	50                   	push   %eax
  800dd5:	8d 42 08             	lea    0x8(%edx),%eax
  800dd8:	50                   	push   %eax
  800dd9:	83 c2 04             	add    $0x4,%edx
  800ddc:	52                   	push   %edx
  800ddd:	e8 f5 fa ff ff       	call   8008d7 <memmove>
		(*args->argc)--;
  800de2:	8b 03                	mov    (%ebx),%eax
  800de4:	ff 08                	decl   (%eax)
		// Check for "--": end of argument list
		if (args->curarg[0] == '-' && args->curarg[1] == '\0')
  800de6:	8b 43 08             	mov    0x8(%ebx),%eax
  800de9:	83 c4 10             	add    $0x10,%esp
  800dec:	80 38 2d             	cmpb   $0x2d,(%eax)
  800def:	75 06                	jne    800df7 <argnext+0x68>
  800df1:	80 78 01 00          	cmpb   $0x0,0x1(%eax)
  800df5:	74 0c                	je     800e03 <argnext+0x74>
			goto endofargs;
	}

	arg = (unsigned char) *args->curarg;
  800df7:	8b 53 08             	mov    0x8(%ebx),%edx
  800dfa:	0f b6 02             	movzbl (%edx),%eax
	args->curarg++;
  800dfd:	42                   	inc    %edx
  800dfe:	89 53 08             	mov    %edx,0x8(%ebx)
	return arg;
  800e01:	eb 13                	jmp    800e16 <argnext+0x87>

    endofargs:
	args->curarg = 0;
  800e03:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
	return -1;
  800e0a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  800e0f:	eb 05                	jmp    800e16 <argnext+0x87>

	args->argvalue = 0;

	// Done processing arguments if args->curarg == 0
	if (args->curarg == 0)
		return -1;
  800e11:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return arg;

    endofargs:
	args->curarg = 0;
	return -1;
}
  800e16:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800e19:	c9                   	leave  
  800e1a:	c3                   	ret    

00800e1b <argnextvalue>:
	return (char*) (args->argvalue ? args->argvalue : argnextvalue(args));
}

char *
argnextvalue(struct Argstate *args)
{
  800e1b:	55                   	push   %ebp
  800e1c:	89 e5                	mov    %esp,%ebp
  800e1e:	53                   	push   %ebx
  800e1f:	83 ec 04             	sub    $0x4,%esp
  800e22:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (!args->curarg)
  800e25:	8b 43 08             	mov    0x8(%ebx),%eax
  800e28:	85 c0                	test   %eax,%eax
  800e2a:	74 57                	je     800e83 <argnextvalue+0x68>
		return 0;
	if (*args->curarg) {
  800e2c:	80 38 00             	cmpb   $0x0,(%eax)
  800e2f:	74 0c                	je     800e3d <argnextvalue+0x22>
		args->argvalue = args->curarg;
  800e31:	89 43 0c             	mov    %eax,0xc(%ebx)
		args->curarg = "";
  800e34:	c7 43 08 71 20 80 00 	movl   $0x802071,0x8(%ebx)
  800e3b:	eb 41                	jmp    800e7e <argnextvalue+0x63>
	} else if (*args->argc > 1) {
  800e3d:	8b 13                	mov    (%ebx),%edx
  800e3f:	83 3a 01             	cmpl   $0x1,(%edx)
  800e42:	7e 2c                	jle    800e70 <argnextvalue+0x55>
		args->argvalue = args->argv[1];
  800e44:	8b 43 04             	mov    0x4(%ebx),%eax
  800e47:	8b 48 04             	mov    0x4(%eax),%ecx
  800e4a:	89 4b 0c             	mov    %ecx,0xc(%ebx)
		memmove(args->argv + 1, args->argv + 2, sizeof(const char *) * (*args->argc - 1));
  800e4d:	83 ec 04             	sub    $0x4,%esp
  800e50:	8b 12                	mov    (%edx),%edx
  800e52:	8d 14 95 fc ff ff ff 	lea    -0x4(,%edx,4),%edx
  800e59:	52                   	push   %edx
  800e5a:	8d 50 08             	lea    0x8(%eax),%edx
  800e5d:	52                   	push   %edx
  800e5e:	83 c0 04             	add    $0x4,%eax
  800e61:	50                   	push   %eax
  800e62:	e8 70 fa ff ff       	call   8008d7 <memmove>
		(*args->argc)--;
  800e67:	8b 03                	mov    (%ebx),%eax
  800e69:	ff 08                	decl   (%eax)
  800e6b:	83 c4 10             	add    $0x10,%esp
  800e6e:	eb 0e                	jmp    800e7e <argnextvalue+0x63>
	} else {
		args->argvalue = 0;
  800e70:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
		args->curarg = 0;
  800e77:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
	}
	return (char*) args->argvalue;
  800e7e:	8b 43 0c             	mov    0xc(%ebx),%eax
  800e81:	eb 05                	jmp    800e88 <argnextvalue+0x6d>

char *
argnextvalue(struct Argstate *args)
{
	if (!args->curarg)
		return 0;
  800e83:	b8 00 00 00 00       	mov    $0x0,%eax
	} else {
		args->argvalue = 0;
		args->curarg = 0;
	}
	return (char*) args->argvalue;
}
  800e88:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800e8b:	c9                   	leave  
  800e8c:	c3                   	ret    

00800e8d <argvalue>:
	return -1;
}

char *
argvalue(struct Argstate *args)
{
  800e8d:	55                   	push   %ebp
  800e8e:	89 e5                	mov    %esp,%ebp
  800e90:	83 ec 08             	sub    $0x8,%esp
  800e93:	8b 4d 08             	mov    0x8(%ebp),%ecx
	return (char*) (args->argvalue ? args->argvalue : argnextvalue(args));
  800e96:	8b 51 0c             	mov    0xc(%ecx),%edx
  800e99:	89 d0                	mov    %edx,%eax
  800e9b:	85 d2                	test   %edx,%edx
  800e9d:	75 0c                	jne    800eab <argvalue+0x1e>
  800e9f:	83 ec 0c             	sub    $0xc,%esp
  800ea2:	51                   	push   %ecx
  800ea3:	e8 73 ff ff ff       	call   800e1b <argnextvalue>
  800ea8:	83 c4 10             	add    $0x10,%esp
}
  800eab:	c9                   	leave  
  800eac:	c3                   	ret    

00800ead <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800ead:	55                   	push   %ebp
  800eae:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  800eb0:	8b 45 08             	mov    0x8(%ebp),%eax
  800eb3:	05 00 00 00 30       	add    $0x30000000,%eax
  800eb8:	c1 e8 0c             	shr    $0xc,%eax
}
  800ebb:	5d                   	pop    %ebp
  800ebc:	c3                   	ret    

00800ebd <fd2data>:

char*
fd2data(struct Fd *fd)
{
  800ebd:	55                   	push   %ebp
  800ebe:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  800ec0:	8b 45 08             	mov    0x8(%ebp),%eax
  800ec3:	05 00 00 00 30       	add    $0x30000000,%eax
  800ec8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800ecd:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  800ed2:	5d                   	pop    %ebp
  800ed3:	c3                   	ret    

00800ed4 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  800ed4:	55                   	push   %ebp
  800ed5:	89 e5                	mov    %esp,%ebp
  800ed7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800eda:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  800edf:	89 c2                	mov    %eax,%edx
  800ee1:	c1 ea 16             	shr    $0x16,%edx
  800ee4:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800eeb:	f6 c2 01             	test   $0x1,%dl
  800eee:	74 11                	je     800f01 <fd_alloc+0x2d>
  800ef0:	89 c2                	mov    %eax,%edx
  800ef2:	c1 ea 0c             	shr    $0xc,%edx
  800ef5:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800efc:	f6 c2 01             	test   $0x1,%dl
  800eff:	75 09                	jne    800f0a <fd_alloc+0x36>
			*fd_store = fd;
  800f01:	89 01                	mov    %eax,(%ecx)
			return 0;
  800f03:	b8 00 00 00 00       	mov    $0x0,%eax
  800f08:	eb 17                	jmp    800f21 <fd_alloc+0x4d>
  800f0a:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  800f0f:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  800f14:	75 c9                	jne    800edf <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  800f16:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  800f1c:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  800f21:	5d                   	pop    %ebp
  800f22:	c3                   	ret    

00800f23 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  800f23:	55                   	push   %ebp
  800f24:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  800f26:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  800f2a:	77 39                	ja     800f65 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  800f2c:	8b 45 08             	mov    0x8(%ebp),%eax
  800f2f:	c1 e0 0c             	shl    $0xc,%eax
  800f32:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  800f37:	89 c2                	mov    %eax,%edx
  800f39:	c1 ea 16             	shr    $0x16,%edx
  800f3c:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800f43:	f6 c2 01             	test   $0x1,%dl
  800f46:	74 24                	je     800f6c <fd_lookup+0x49>
  800f48:	89 c2                	mov    %eax,%edx
  800f4a:	c1 ea 0c             	shr    $0xc,%edx
  800f4d:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800f54:	f6 c2 01             	test   $0x1,%dl
  800f57:	74 1a                	je     800f73 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  800f59:	8b 55 0c             	mov    0xc(%ebp),%edx
  800f5c:	89 02                	mov    %eax,(%edx)
	return 0;
  800f5e:	b8 00 00 00 00       	mov    $0x0,%eax
  800f63:	eb 13                	jmp    800f78 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800f65:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800f6a:	eb 0c                	jmp    800f78 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800f6c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800f71:	eb 05                	jmp    800f78 <fd_lookup+0x55>
  800f73:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  800f78:	5d                   	pop    %ebp
  800f79:	c3                   	ret    

00800f7a <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  800f7a:	55                   	push   %ebp
  800f7b:	89 e5                	mov    %esp,%ebp
  800f7d:	83 ec 08             	sub    $0x8,%esp
  800f80:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800f83:	ba 48 24 80 00       	mov    $0x802448,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  800f88:	eb 13                	jmp    800f9d <dev_lookup+0x23>
  800f8a:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  800f8d:	39 08                	cmp    %ecx,(%eax)
  800f8f:	75 0c                	jne    800f9d <dev_lookup+0x23>
			*dev = devtab[i];
  800f91:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f94:	89 01                	mov    %eax,(%ecx)
			return 0;
  800f96:	b8 00 00 00 00       	mov    $0x0,%eax
  800f9b:	eb 2e                	jmp    800fcb <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  800f9d:	8b 02                	mov    (%edx),%eax
  800f9f:	85 c0                	test   %eax,%eax
  800fa1:	75 e7                	jne    800f8a <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  800fa3:	a1 04 40 80 00       	mov    0x804004,%eax
  800fa8:	8b 40 48             	mov    0x48(%eax),%eax
  800fab:	83 ec 04             	sub    $0x4,%esp
  800fae:	51                   	push   %ecx
  800faf:	50                   	push   %eax
  800fb0:	68 cc 23 80 00       	push   $0x8023cc
  800fb5:	e8 44 f2 ff ff       	call   8001fe <cprintf>
	*dev = 0;
  800fba:	8b 45 0c             	mov    0xc(%ebp),%eax
  800fbd:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  800fc3:	83 c4 10             	add    $0x10,%esp
  800fc6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  800fcb:	c9                   	leave  
  800fcc:	c3                   	ret    

00800fcd <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  800fcd:	55                   	push   %ebp
  800fce:	89 e5                	mov    %esp,%ebp
  800fd0:	56                   	push   %esi
  800fd1:	53                   	push   %ebx
  800fd2:	83 ec 10             	sub    $0x10,%esp
  800fd5:	8b 75 08             	mov    0x8(%ebp),%esi
  800fd8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  800fdb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800fde:	50                   	push   %eax
  800fdf:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  800fe5:	c1 e8 0c             	shr    $0xc,%eax
  800fe8:	50                   	push   %eax
  800fe9:	e8 35 ff ff ff       	call   800f23 <fd_lookup>
  800fee:	83 c4 08             	add    $0x8,%esp
  800ff1:	85 c0                	test   %eax,%eax
  800ff3:	78 05                	js     800ffa <fd_close+0x2d>
	    || fd != fd2)
  800ff5:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  800ff8:	74 06                	je     801000 <fd_close+0x33>
		return (must_exist ? r : 0);
  800ffa:	84 db                	test   %bl,%bl
  800ffc:	74 47                	je     801045 <fd_close+0x78>
  800ffe:	eb 4a                	jmp    80104a <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801000:	83 ec 08             	sub    $0x8,%esp
  801003:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801006:	50                   	push   %eax
  801007:	ff 36                	pushl  (%esi)
  801009:	e8 6c ff ff ff       	call   800f7a <dev_lookup>
  80100e:	89 c3                	mov    %eax,%ebx
  801010:	83 c4 10             	add    $0x10,%esp
  801013:	85 c0                	test   %eax,%eax
  801015:	78 1c                	js     801033 <fd_close+0x66>
		if (dev->dev_close)
  801017:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80101a:	8b 40 10             	mov    0x10(%eax),%eax
  80101d:	85 c0                	test   %eax,%eax
  80101f:	74 0d                	je     80102e <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801021:	83 ec 0c             	sub    $0xc,%esp
  801024:	56                   	push   %esi
  801025:	ff d0                	call   *%eax
  801027:	89 c3                	mov    %eax,%ebx
  801029:	83 c4 10             	add    $0x10,%esp
  80102c:	eb 05                	jmp    801033 <fd_close+0x66>
		else
			r = 0;
  80102e:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  801033:	83 ec 08             	sub    $0x8,%esp
  801036:	56                   	push   %esi
  801037:	6a 00                	push   $0x0
  801039:	e8 8d fb ff ff       	call   800bcb <sys_page_unmap>
	return r;
  80103e:	83 c4 10             	add    $0x10,%esp
  801041:	89 d8                	mov    %ebx,%eax
  801043:	eb 05                	jmp    80104a <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  801045:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  80104a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80104d:	5b                   	pop    %ebx
  80104e:	5e                   	pop    %esi
  80104f:	5d                   	pop    %ebp
  801050:	c3                   	ret    

00801051 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  801051:	55                   	push   %ebp
  801052:	89 e5                	mov    %esp,%ebp
  801054:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801057:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80105a:	50                   	push   %eax
  80105b:	ff 75 08             	pushl  0x8(%ebp)
  80105e:	e8 c0 fe ff ff       	call   800f23 <fd_lookup>
  801063:	83 c4 08             	add    $0x8,%esp
  801066:	85 c0                	test   %eax,%eax
  801068:	78 10                	js     80107a <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  80106a:	83 ec 08             	sub    $0x8,%esp
  80106d:	6a 01                	push   $0x1
  80106f:	ff 75 f4             	pushl  -0xc(%ebp)
  801072:	e8 56 ff ff ff       	call   800fcd <fd_close>
  801077:	83 c4 10             	add    $0x10,%esp
}
  80107a:	c9                   	leave  
  80107b:	c3                   	ret    

0080107c <close_all>:

void
close_all(void)
{
  80107c:	55                   	push   %ebp
  80107d:	89 e5                	mov    %esp,%ebp
  80107f:	53                   	push   %ebx
  801080:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  801083:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801088:	83 ec 0c             	sub    $0xc,%esp
  80108b:	53                   	push   %ebx
  80108c:	e8 c0 ff ff ff       	call   801051 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801091:	43                   	inc    %ebx
  801092:	83 c4 10             	add    $0x10,%esp
  801095:	83 fb 20             	cmp    $0x20,%ebx
  801098:	75 ee                	jne    801088 <close_all+0xc>
		close(i);
}
  80109a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80109d:	c9                   	leave  
  80109e:	c3                   	ret    

0080109f <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  80109f:	55                   	push   %ebp
  8010a0:	89 e5                	mov    %esp,%ebp
  8010a2:	57                   	push   %edi
  8010a3:	56                   	push   %esi
  8010a4:	53                   	push   %ebx
  8010a5:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  8010a8:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8010ab:	50                   	push   %eax
  8010ac:	ff 75 08             	pushl  0x8(%ebp)
  8010af:	e8 6f fe ff ff       	call   800f23 <fd_lookup>
  8010b4:	83 c4 08             	add    $0x8,%esp
  8010b7:	85 c0                	test   %eax,%eax
  8010b9:	0f 88 c2 00 00 00    	js     801181 <dup+0xe2>
		return r;
	close(newfdnum);
  8010bf:	83 ec 0c             	sub    $0xc,%esp
  8010c2:	ff 75 0c             	pushl  0xc(%ebp)
  8010c5:	e8 87 ff ff ff       	call   801051 <close>

	newfd = INDEX2FD(newfdnum);
  8010ca:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8010cd:	c1 e3 0c             	shl    $0xc,%ebx
  8010d0:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  8010d6:	83 c4 04             	add    $0x4,%esp
  8010d9:	ff 75 e4             	pushl  -0x1c(%ebp)
  8010dc:	e8 dc fd ff ff       	call   800ebd <fd2data>
  8010e1:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  8010e3:	89 1c 24             	mov    %ebx,(%esp)
  8010e6:	e8 d2 fd ff ff       	call   800ebd <fd2data>
  8010eb:	83 c4 10             	add    $0x10,%esp
  8010ee:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8010f0:	89 f0                	mov    %esi,%eax
  8010f2:	c1 e8 16             	shr    $0x16,%eax
  8010f5:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8010fc:	a8 01                	test   $0x1,%al
  8010fe:	74 35                	je     801135 <dup+0x96>
  801100:	89 f0                	mov    %esi,%eax
  801102:	c1 e8 0c             	shr    $0xc,%eax
  801105:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  80110c:	f6 c2 01             	test   $0x1,%dl
  80110f:	74 24                	je     801135 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801111:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801118:	83 ec 0c             	sub    $0xc,%esp
  80111b:	25 07 0e 00 00       	and    $0xe07,%eax
  801120:	50                   	push   %eax
  801121:	57                   	push   %edi
  801122:	6a 00                	push   $0x0
  801124:	56                   	push   %esi
  801125:	6a 00                	push   $0x0
  801127:	e8 5d fa ff ff       	call   800b89 <sys_page_map>
  80112c:	89 c6                	mov    %eax,%esi
  80112e:	83 c4 20             	add    $0x20,%esp
  801131:	85 c0                	test   %eax,%eax
  801133:	78 2c                	js     801161 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  801135:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801138:	89 d0                	mov    %edx,%eax
  80113a:	c1 e8 0c             	shr    $0xc,%eax
  80113d:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801144:	83 ec 0c             	sub    $0xc,%esp
  801147:	25 07 0e 00 00       	and    $0xe07,%eax
  80114c:	50                   	push   %eax
  80114d:	53                   	push   %ebx
  80114e:	6a 00                	push   $0x0
  801150:	52                   	push   %edx
  801151:	6a 00                	push   $0x0
  801153:	e8 31 fa ff ff       	call   800b89 <sys_page_map>
  801158:	89 c6                	mov    %eax,%esi
  80115a:	83 c4 20             	add    $0x20,%esp
  80115d:	85 c0                	test   %eax,%eax
  80115f:	79 1d                	jns    80117e <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801161:	83 ec 08             	sub    $0x8,%esp
  801164:	53                   	push   %ebx
  801165:	6a 00                	push   $0x0
  801167:	e8 5f fa ff ff       	call   800bcb <sys_page_unmap>
	sys_page_unmap(0, nva);
  80116c:	83 c4 08             	add    $0x8,%esp
  80116f:	57                   	push   %edi
  801170:	6a 00                	push   $0x0
  801172:	e8 54 fa ff ff       	call   800bcb <sys_page_unmap>
	return r;
  801177:	83 c4 10             	add    $0x10,%esp
  80117a:	89 f0                	mov    %esi,%eax
  80117c:	eb 03                	jmp    801181 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  80117e:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801181:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801184:	5b                   	pop    %ebx
  801185:	5e                   	pop    %esi
  801186:	5f                   	pop    %edi
  801187:	5d                   	pop    %ebp
  801188:	c3                   	ret    

00801189 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801189:	55                   	push   %ebp
  80118a:	89 e5                	mov    %esp,%ebp
  80118c:	53                   	push   %ebx
  80118d:	83 ec 14             	sub    $0x14,%esp
  801190:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801193:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801196:	50                   	push   %eax
  801197:	53                   	push   %ebx
  801198:	e8 86 fd ff ff       	call   800f23 <fd_lookup>
  80119d:	83 c4 08             	add    $0x8,%esp
  8011a0:	85 c0                	test   %eax,%eax
  8011a2:	78 67                	js     80120b <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8011a4:	83 ec 08             	sub    $0x8,%esp
  8011a7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8011aa:	50                   	push   %eax
  8011ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011ae:	ff 30                	pushl  (%eax)
  8011b0:	e8 c5 fd ff ff       	call   800f7a <dev_lookup>
  8011b5:	83 c4 10             	add    $0x10,%esp
  8011b8:	85 c0                	test   %eax,%eax
  8011ba:	78 4f                	js     80120b <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8011bc:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8011bf:	8b 42 08             	mov    0x8(%edx),%eax
  8011c2:	83 e0 03             	and    $0x3,%eax
  8011c5:	83 f8 01             	cmp    $0x1,%eax
  8011c8:	75 21                	jne    8011eb <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  8011ca:	a1 04 40 80 00       	mov    0x804004,%eax
  8011cf:	8b 40 48             	mov    0x48(%eax),%eax
  8011d2:	83 ec 04             	sub    $0x4,%esp
  8011d5:	53                   	push   %ebx
  8011d6:	50                   	push   %eax
  8011d7:	68 0d 24 80 00       	push   $0x80240d
  8011dc:	e8 1d f0 ff ff       	call   8001fe <cprintf>
		return -E_INVAL;
  8011e1:	83 c4 10             	add    $0x10,%esp
  8011e4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011e9:	eb 20                	jmp    80120b <read+0x82>
	}
	if (!dev->dev_read)
  8011eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8011ee:	8b 40 08             	mov    0x8(%eax),%eax
  8011f1:	85 c0                	test   %eax,%eax
  8011f3:	74 11                	je     801206 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  8011f5:	83 ec 04             	sub    $0x4,%esp
  8011f8:	ff 75 10             	pushl  0x10(%ebp)
  8011fb:	ff 75 0c             	pushl  0xc(%ebp)
  8011fe:	52                   	push   %edx
  8011ff:	ff d0                	call   *%eax
  801201:	83 c4 10             	add    $0x10,%esp
  801204:	eb 05                	jmp    80120b <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801206:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  80120b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80120e:	c9                   	leave  
  80120f:	c3                   	ret    

00801210 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801210:	55                   	push   %ebp
  801211:	89 e5                	mov    %esp,%ebp
  801213:	57                   	push   %edi
  801214:	56                   	push   %esi
  801215:	53                   	push   %ebx
  801216:	83 ec 0c             	sub    $0xc,%esp
  801219:	8b 7d 08             	mov    0x8(%ebp),%edi
  80121c:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80121f:	bb 00 00 00 00       	mov    $0x0,%ebx
  801224:	eb 21                	jmp    801247 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801226:	83 ec 04             	sub    $0x4,%esp
  801229:	89 f0                	mov    %esi,%eax
  80122b:	29 d8                	sub    %ebx,%eax
  80122d:	50                   	push   %eax
  80122e:	89 d8                	mov    %ebx,%eax
  801230:	03 45 0c             	add    0xc(%ebp),%eax
  801233:	50                   	push   %eax
  801234:	57                   	push   %edi
  801235:	e8 4f ff ff ff       	call   801189 <read>
		if (m < 0)
  80123a:	83 c4 10             	add    $0x10,%esp
  80123d:	85 c0                	test   %eax,%eax
  80123f:	78 10                	js     801251 <readn+0x41>
			return m;
		if (m == 0)
  801241:	85 c0                	test   %eax,%eax
  801243:	74 0a                	je     80124f <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801245:	01 c3                	add    %eax,%ebx
  801247:	39 f3                	cmp    %esi,%ebx
  801249:	72 db                	jb     801226 <readn+0x16>
  80124b:	89 d8                	mov    %ebx,%eax
  80124d:	eb 02                	jmp    801251 <readn+0x41>
  80124f:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801251:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801254:	5b                   	pop    %ebx
  801255:	5e                   	pop    %esi
  801256:	5f                   	pop    %edi
  801257:	5d                   	pop    %ebp
  801258:	c3                   	ret    

00801259 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801259:	55                   	push   %ebp
  80125a:	89 e5                	mov    %esp,%ebp
  80125c:	53                   	push   %ebx
  80125d:	83 ec 14             	sub    $0x14,%esp
  801260:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801263:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801266:	50                   	push   %eax
  801267:	53                   	push   %ebx
  801268:	e8 b6 fc ff ff       	call   800f23 <fd_lookup>
  80126d:	83 c4 08             	add    $0x8,%esp
  801270:	85 c0                	test   %eax,%eax
  801272:	78 62                	js     8012d6 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801274:	83 ec 08             	sub    $0x8,%esp
  801277:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80127a:	50                   	push   %eax
  80127b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80127e:	ff 30                	pushl  (%eax)
  801280:	e8 f5 fc ff ff       	call   800f7a <dev_lookup>
  801285:	83 c4 10             	add    $0x10,%esp
  801288:	85 c0                	test   %eax,%eax
  80128a:	78 4a                	js     8012d6 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80128c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80128f:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801293:	75 21                	jne    8012b6 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801295:	a1 04 40 80 00       	mov    0x804004,%eax
  80129a:	8b 40 48             	mov    0x48(%eax),%eax
  80129d:	83 ec 04             	sub    $0x4,%esp
  8012a0:	53                   	push   %ebx
  8012a1:	50                   	push   %eax
  8012a2:	68 29 24 80 00       	push   $0x802429
  8012a7:	e8 52 ef ff ff       	call   8001fe <cprintf>
		return -E_INVAL;
  8012ac:	83 c4 10             	add    $0x10,%esp
  8012af:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8012b4:	eb 20                	jmp    8012d6 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8012b6:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8012b9:	8b 52 0c             	mov    0xc(%edx),%edx
  8012bc:	85 d2                	test   %edx,%edx
  8012be:	74 11                	je     8012d1 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8012c0:	83 ec 04             	sub    $0x4,%esp
  8012c3:	ff 75 10             	pushl  0x10(%ebp)
  8012c6:	ff 75 0c             	pushl  0xc(%ebp)
  8012c9:	50                   	push   %eax
  8012ca:	ff d2                	call   *%edx
  8012cc:	83 c4 10             	add    $0x10,%esp
  8012cf:	eb 05                	jmp    8012d6 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8012d1:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8012d6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8012d9:	c9                   	leave  
  8012da:	c3                   	ret    

008012db <seek>:

int
seek(int fdnum, off_t offset)
{
  8012db:	55                   	push   %ebp
  8012dc:	89 e5                	mov    %esp,%ebp
  8012de:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8012e1:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8012e4:	50                   	push   %eax
  8012e5:	ff 75 08             	pushl  0x8(%ebp)
  8012e8:	e8 36 fc ff ff       	call   800f23 <fd_lookup>
  8012ed:	83 c4 08             	add    $0x8,%esp
  8012f0:	85 c0                	test   %eax,%eax
  8012f2:	78 0e                	js     801302 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  8012f4:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8012f7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8012fa:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  8012fd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801302:	c9                   	leave  
  801303:	c3                   	ret    

00801304 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801304:	55                   	push   %ebp
  801305:	89 e5                	mov    %esp,%ebp
  801307:	53                   	push   %ebx
  801308:	83 ec 14             	sub    $0x14,%esp
  80130b:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  80130e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801311:	50                   	push   %eax
  801312:	53                   	push   %ebx
  801313:	e8 0b fc ff ff       	call   800f23 <fd_lookup>
  801318:	83 c4 08             	add    $0x8,%esp
  80131b:	85 c0                	test   %eax,%eax
  80131d:	78 5f                	js     80137e <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80131f:	83 ec 08             	sub    $0x8,%esp
  801322:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801325:	50                   	push   %eax
  801326:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801329:	ff 30                	pushl  (%eax)
  80132b:	e8 4a fc ff ff       	call   800f7a <dev_lookup>
  801330:	83 c4 10             	add    $0x10,%esp
  801333:	85 c0                	test   %eax,%eax
  801335:	78 47                	js     80137e <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801337:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80133a:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80133e:	75 21                	jne    801361 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801340:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  801345:	8b 40 48             	mov    0x48(%eax),%eax
  801348:	83 ec 04             	sub    $0x4,%esp
  80134b:	53                   	push   %ebx
  80134c:	50                   	push   %eax
  80134d:	68 ec 23 80 00       	push   $0x8023ec
  801352:	e8 a7 ee ff ff       	call   8001fe <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  801357:	83 c4 10             	add    $0x10,%esp
  80135a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80135f:	eb 1d                	jmp    80137e <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  801361:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801364:	8b 52 18             	mov    0x18(%edx),%edx
  801367:	85 d2                	test   %edx,%edx
  801369:	74 0e                	je     801379 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  80136b:	83 ec 08             	sub    $0x8,%esp
  80136e:	ff 75 0c             	pushl  0xc(%ebp)
  801371:	50                   	push   %eax
  801372:	ff d2                	call   *%edx
  801374:	83 c4 10             	add    $0x10,%esp
  801377:	eb 05                	jmp    80137e <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  801379:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  80137e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801381:	c9                   	leave  
  801382:	c3                   	ret    

00801383 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  801383:	55                   	push   %ebp
  801384:	89 e5                	mov    %esp,%ebp
  801386:	53                   	push   %ebx
  801387:	83 ec 14             	sub    $0x14,%esp
  80138a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80138d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801390:	50                   	push   %eax
  801391:	ff 75 08             	pushl  0x8(%ebp)
  801394:	e8 8a fb ff ff       	call   800f23 <fd_lookup>
  801399:	83 c4 08             	add    $0x8,%esp
  80139c:	85 c0                	test   %eax,%eax
  80139e:	78 52                	js     8013f2 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8013a0:	83 ec 08             	sub    $0x8,%esp
  8013a3:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013a6:	50                   	push   %eax
  8013a7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8013aa:	ff 30                	pushl  (%eax)
  8013ac:	e8 c9 fb ff ff       	call   800f7a <dev_lookup>
  8013b1:	83 c4 10             	add    $0x10,%esp
  8013b4:	85 c0                	test   %eax,%eax
  8013b6:	78 3a                	js     8013f2 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8013b8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8013bb:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8013bf:	74 2c                	je     8013ed <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8013c1:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8013c4:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8013cb:	00 00 00 
	stat->st_isdir = 0;
  8013ce:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8013d5:	00 00 00 
	stat->st_dev = dev;
  8013d8:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8013de:	83 ec 08             	sub    $0x8,%esp
  8013e1:	53                   	push   %ebx
  8013e2:	ff 75 f0             	pushl  -0x10(%ebp)
  8013e5:	ff 50 14             	call   *0x14(%eax)
  8013e8:	83 c4 10             	add    $0x10,%esp
  8013eb:	eb 05                	jmp    8013f2 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8013ed:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8013f2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8013f5:	c9                   	leave  
  8013f6:	c3                   	ret    

008013f7 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8013f7:	55                   	push   %ebp
  8013f8:	89 e5                	mov    %esp,%ebp
  8013fa:	56                   	push   %esi
  8013fb:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8013fc:	83 ec 08             	sub    $0x8,%esp
  8013ff:	6a 00                	push   $0x0
  801401:	ff 75 08             	pushl  0x8(%ebp)
  801404:	e8 e7 01 00 00       	call   8015f0 <open>
  801409:	89 c3                	mov    %eax,%ebx
  80140b:	83 c4 10             	add    $0x10,%esp
  80140e:	85 c0                	test   %eax,%eax
  801410:	78 1d                	js     80142f <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801412:	83 ec 08             	sub    $0x8,%esp
  801415:	ff 75 0c             	pushl  0xc(%ebp)
  801418:	50                   	push   %eax
  801419:	e8 65 ff ff ff       	call   801383 <fstat>
  80141e:	89 c6                	mov    %eax,%esi
	close(fd);
  801420:	89 1c 24             	mov    %ebx,(%esp)
  801423:	e8 29 fc ff ff       	call   801051 <close>
	return r;
  801428:	83 c4 10             	add    $0x10,%esp
  80142b:	89 f0                	mov    %esi,%eax
  80142d:	eb 00                	jmp    80142f <stat+0x38>
}
  80142f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801432:	5b                   	pop    %ebx
  801433:	5e                   	pop    %esi
  801434:	5d                   	pop    %ebp
  801435:	c3                   	ret    

00801436 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  801436:	55                   	push   %ebp
  801437:	89 e5                	mov    %esp,%ebp
  801439:	56                   	push   %esi
  80143a:	53                   	push   %ebx
  80143b:	89 c6                	mov    %eax,%esi
  80143d:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  80143f:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  801446:	75 12                	jne    80145a <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801448:	83 ec 0c             	sub    $0xc,%esp
  80144b:	6a 01                	push   $0x1
  80144d:	e8 0f 09 00 00       	call   801d61 <ipc_find_env>
  801452:	a3 00 40 80 00       	mov    %eax,0x804000
  801457:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  80145a:	6a 07                	push   $0x7
  80145c:	68 00 50 80 00       	push   $0x805000
  801461:	56                   	push   %esi
  801462:	ff 35 00 40 80 00    	pushl  0x804000
  801468:	e8 9f 08 00 00       	call   801d0c <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  80146d:	83 c4 0c             	add    $0xc,%esp
  801470:	6a 00                	push   $0x0
  801472:	53                   	push   %ebx
  801473:	6a 00                	push   $0x0
  801475:	e8 2a 08 00 00       	call   801ca4 <ipc_recv>
}
  80147a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80147d:	5b                   	pop    %ebx
  80147e:	5e                   	pop    %esi
  80147f:	5d                   	pop    %ebp
  801480:	c3                   	ret    

00801481 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801481:	55                   	push   %ebp
  801482:	89 e5                	mov    %esp,%ebp
  801484:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801487:	8b 45 08             	mov    0x8(%ebp),%eax
  80148a:	8b 40 0c             	mov    0xc(%eax),%eax
  80148d:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  801492:	8b 45 0c             	mov    0xc(%ebp),%eax
  801495:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  80149a:	ba 00 00 00 00       	mov    $0x0,%edx
  80149f:	b8 02 00 00 00       	mov    $0x2,%eax
  8014a4:	e8 8d ff ff ff       	call   801436 <fsipc>
}
  8014a9:	c9                   	leave  
  8014aa:	c3                   	ret    

008014ab <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8014ab:	55                   	push   %ebp
  8014ac:	89 e5                	mov    %esp,%ebp
  8014ae:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8014b1:	8b 45 08             	mov    0x8(%ebp),%eax
  8014b4:	8b 40 0c             	mov    0xc(%eax),%eax
  8014b7:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  8014bc:	ba 00 00 00 00       	mov    $0x0,%edx
  8014c1:	b8 06 00 00 00       	mov    $0x6,%eax
  8014c6:	e8 6b ff ff ff       	call   801436 <fsipc>
}
  8014cb:	c9                   	leave  
  8014cc:	c3                   	ret    

008014cd <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  8014cd:	55                   	push   %ebp
  8014ce:	89 e5                	mov    %esp,%ebp
  8014d0:	53                   	push   %ebx
  8014d1:	83 ec 04             	sub    $0x4,%esp
  8014d4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8014d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8014da:	8b 40 0c             	mov    0xc(%eax),%eax
  8014dd:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8014e2:	ba 00 00 00 00       	mov    $0x0,%edx
  8014e7:	b8 05 00 00 00       	mov    $0x5,%eax
  8014ec:	e8 45 ff ff ff       	call   801436 <fsipc>
  8014f1:	85 c0                	test   %eax,%eax
  8014f3:	78 2c                	js     801521 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  8014f5:	83 ec 08             	sub    $0x8,%esp
  8014f8:	68 00 50 80 00       	push   $0x805000
  8014fd:	53                   	push   %ebx
  8014fe:	e8 5f f2 ff ff       	call   800762 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801503:	a1 80 50 80 00       	mov    0x805080,%eax
  801508:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  80150e:	a1 84 50 80 00       	mov    0x805084,%eax
  801513:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801519:	83 c4 10             	add    $0x10,%esp
  80151c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801521:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801524:	c9                   	leave  
  801525:	c3                   	ret    

00801526 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801526:	55                   	push   %ebp
  801527:	89 e5                	mov    %esp,%ebp
  801529:	83 ec 08             	sub    $0x8,%esp
  80152c:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  80152f:	8b 55 08             	mov    0x8(%ebp),%edx
  801532:	8b 52 0c             	mov    0xc(%edx),%edx
  801535:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  80153b:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801540:	76 05                	jbe    801547 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801542:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801547:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  80154c:	83 ec 04             	sub    $0x4,%esp
  80154f:	50                   	push   %eax
  801550:	ff 75 0c             	pushl  0xc(%ebp)
  801553:	68 08 50 80 00       	push   $0x805008
  801558:	e8 7a f3 ff ff       	call   8008d7 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  80155d:	ba 00 00 00 00       	mov    $0x0,%edx
  801562:	b8 04 00 00 00       	mov    $0x4,%eax
  801567:	e8 ca fe ff ff       	call   801436 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  80156c:	c9                   	leave  
  80156d:	c3                   	ret    

0080156e <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  80156e:	55                   	push   %ebp
  80156f:	89 e5                	mov    %esp,%ebp
  801571:	56                   	push   %esi
  801572:	53                   	push   %ebx
  801573:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  801576:	8b 45 08             	mov    0x8(%ebp),%eax
  801579:	8b 40 0c             	mov    0xc(%eax),%eax
  80157c:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801581:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801587:	ba 00 00 00 00       	mov    $0x0,%edx
  80158c:	b8 03 00 00 00       	mov    $0x3,%eax
  801591:	e8 a0 fe ff ff       	call   801436 <fsipc>
  801596:	89 c3                	mov    %eax,%ebx
  801598:	85 c0                	test   %eax,%eax
  80159a:	78 4b                	js     8015e7 <devfile_read+0x79>
		return r;
	assert(r <= n);
  80159c:	39 c6                	cmp    %eax,%esi
  80159e:	73 16                	jae    8015b6 <devfile_read+0x48>
  8015a0:	68 58 24 80 00       	push   $0x802458
  8015a5:	68 5f 24 80 00       	push   $0x80245f
  8015aa:	6a 7c                	push   $0x7c
  8015ac:	68 74 24 80 00       	push   $0x802474
  8015b1:	e8 a8 06 00 00       	call   801c5e <_panic>
	assert(r <= PGSIZE);
  8015b6:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8015bb:	7e 16                	jle    8015d3 <devfile_read+0x65>
  8015bd:	68 7f 24 80 00       	push   $0x80247f
  8015c2:	68 5f 24 80 00       	push   $0x80245f
  8015c7:	6a 7d                	push   $0x7d
  8015c9:	68 74 24 80 00       	push   $0x802474
  8015ce:	e8 8b 06 00 00       	call   801c5e <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  8015d3:	83 ec 04             	sub    $0x4,%esp
  8015d6:	50                   	push   %eax
  8015d7:	68 00 50 80 00       	push   $0x805000
  8015dc:	ff 75 0c             	pushl  0xc(%ebp)
  8015df:	e8 f3 f2 ff ff       	call   8008d7 <memmove>
	return r;
  8015e4:	83 c4 10             	add    $0x10,%esp
}
  8015e7:	89 d8                	mov    %ebx,%eax
  8015e9:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8015ec:	5b                   	pop    %ebx
  8015ed:	5e                   	pop    %esi
  8015ee:	5d                   	pop    %ebp
  8015ef:	c3                   	ret    

008015f0 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  8015f0:	55                   	push   %ebp
  8015f1:	89 e5                	mov    %esp,%ebp
  8015f3:	53                   	push   %ebx
  8015f4:	83 ec 20             	sub    $0x20,%esp
  8015f7:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  8015fa:	53                   	push   %ebx
  8015fb:	e8 2d f1 ff ff       	call   80072d <strlen>
  801600:	83 c4 10             	add    $0x10,%esp
  801603:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801608:	7f 63                	jg     80166d <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  80160a:	83 ec 0c             	sub    $0xc,%esp
  80160d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801610:	50                   	push   %eax
  801611:	e8 be f8 ff ff       	call   800ed4 <fd_alloc>
  801616:	83 c4 10             	add    $0x10,%esp
  801619:	85 c0                	test   %eax,%eax
  80161b:	78 55                	js     801672 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  80161d:	83 ec 08             	sub    $0x8,%esp
  801620:	53                   	push   %ebx
  801621:	68 00 50 80 00       	push   $0x805000
  801626:	e8 37 f1 ff ff       	call   800762 <strcpy>
	fsipcbuf.open.req_omode = mode;
  80162b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80162e:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801633:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801636:	b8 01 00 00 00       	mov    $0x1,%eax
  80163b:	e8 f6 fd ff ff       	call   801436 <fsipc>
  801640:	89 c3                	mov    %eax,%ebx
  801642:	83 c4 10             	add    $0x10,%esp
  801645:	85 c0                	test   %eax,%eax
  801647:	79 14                	jns    80165d <open+0x6d>
		fd_close(fd, 0);
  801649:	83 ec 08             	sub    $0x8,%esp
  80164c:	6a 00                	push   $0x0
  80164e:	ff 75 f4             	pushl  -0xc(%ebp)
  801651:	e8 77 f9 ff ff       	call   800fcd <fd_close>
		return r;
  801656:	83 c4 10             	add    $0x10,%esp
  801659:	89 d8                	mov    %ebx,%eax
  80165b:	eb 15                	jmp    801672 <open+0x82>
	}

	return fd2num(fd);
  80165d:	83 ec 0c             	sub    $0xc,%esp
  801660:	ff 75 f4             	pushl  -0xc(%ebp)
  801663:	e8 45 f8 ff ff       	call   800ead <fd2num>
  801668:	83 c4 10             	add    $0x10,%esp
  80166b:	eb 05                	jmp    801672 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  80166d:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801672:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801675:	c9                   	leave  
  801676:	c3                   	ret    

00801677 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801677:	55                   	push   %ebp
  801678:	89 e5                	mov    %esp,%ebp
  80167a:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  80167d:	ba 00 00 00 00       	mov    $0x0,%edx
  801682:	b8 08 00 00 00       	mov    $0x8,%eax
  801687:	e8 aa fd ff ff       	call   801436 <fsipc>
}
  80168c:	c9                   	leave  
  80168d:	c3                   	ret    

0080168e <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  80168e:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  801692:	7e 38                	jle    8016cc <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  801694:	55                   	push   %ebp
  801695:	89 e5                	mov    %esp,%ebp
  801697:	53                   	push   %ebx
  801698:	83 ec 08             	sub    $0x8,%esp
  80169b:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  80169d:	ff 70 04             	pushl  0x4(%eax)
  8016a0:	8d 40 10             	lea    0x10(%eax),%eax
  8016a3:	50                   	push   %eax
  8016a4:	ff 33                	pushl  (%ebx)
  8016a6:	e8 ae fb ff ff       	call   801259 <write>
		if (result > 0)
  8016ab:	83 c4 10             	add    $0x10,%esp
  8016ae:	85 c0                	test   %eax,%eax
  8016b0:	7e 03                	jle    8016b5 <writebuf+0x27>
			b->result += result;
  8016b2:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  8016b5:	3b 43 04             	cmp    0x4(%ebx),%eax
  8016b8:	74 0e                	je     8016c8 <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  8016ba:	89 c2                	mov    %eax,%edx
  8016bc:	85 c0                	test   %eax,%eax
  8016be:	7e 05                	jle    8016c5 <writebuf+0x37>
  8016c0:	ba 00 00 00 00       	mov    $0x0,%edx
  8016c5:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  8016c8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8016cb:	c9                   	leave  
  8016cc:	c3                   	ret    

008016cd <putch>:

static void
putch(int ch, void *thunk)
{
  8016cd:	55                   	push   %ebp
  8016ce:	89 e5                	mov    %esp,%ebp
  8016d0:	53                   	push   %ebx
  8016d1:	83 ec 04             	sub    $0x4,%esp
  8016d4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  8016d7:	8b 53 04             	mov    0x4(%ebx),%edx
  8016da:	8d 42 01             	lea    0x1(%edx),%eax
  8016dd:	89 43 04             	mov    %eax,0x4(%ebx)
  8016e0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8016e3:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  8016e7:	3d 00 01 00 00       	cmp    $0x100,%eax
  8016ec:	75 0e                	jne    8016fc <putch+0x2f>
		writebuf(b);
  8016ee:	89 d8                	mov    %ebx,%eax
  8016f0:	e8 99 ff ff ff       	call   80168e <writebuf>
		b->idx = 0;
  8016f5:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  8016fc:	83 c4 04             	add    $0x4,%esp
  8016ff:	5b                   	pop    %ebx
  801700:	5d                   	pop    %ebp
  801701:	c3                   	ret    

00801702 <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  801702:	55                   	push   %ebp
  801703:	89 e5                	mov    %esp,%ebp
  801705:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  80170b:	8b 45 08             	mov    0x8(%ebp),%eax
  80170e:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  801714:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  80171b:	00 00 00 
	b.result = 0;
  80171e:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  801725:	00 00 00 
	b.error = 1;
  801728:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  80172f:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  801732:	ff 75 10             	pushl  0x10(%ebp)
  801735:	ff 75 0c             	pushl  0xc(%ebp)
  801738:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  80173e:	50                   	push   %eax
  80173f:	68 cd 16 80 00       	push   $0x8016cd
  801744:	e8 e9 eb ff ff       	call   800332 <vprintfmt>
	if (b.idx > 0)
  801749:	83 c4 10             	add    $0x10,%esp
  80174c:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  801753:	7e 0b                	jle    801760 <vfprintf+0x5e>
		writebuf(&b);
  801755:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  80175b:	e8 2e ff ff ff       	call   80168e <writebuf>

	return (b.result ? b.result : b.error);
  801760:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  801766:	85 c0                	test   %eax,%eax
  801768:	75 06                	jne    801770 <vfprintf+0x6e>
  80176a:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  801770:	c9                   	leave  
  801771:	c3                   	ret    

00801772 <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  801772:	55                   	push   %ebp
  801773:	89 e5                	mov    %esp,%ebp
  801775:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  801778:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  80177b:	50                   	push   %eax
  80177c:	ff 75 0c             	pushl  0xc(%ebp)
  80177f:	ff 75 08             	pushl  0x8(%ebp)
  801782:	e8 7b ff ff ff       	call   801702 <vfprintf>
	va_end(ap);

	return cnt;
}
  801787:	c9                   	leave  
  801788:	c3                   	ret    

00801789 <printf>:

int
printf(const char *fmt, ...)
{
  801789:	55                   	push   %ebp
  80178a:	89 e5                	mov    %esp,%ebp
  80178c:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80178f:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  801792:	50                   	push   %eax
  801793:	ff 75 08             	pushl  0x8(%ebp)
  801796:	6a 01                	push   $0x1
  801798:	e8 65 ff ff ff       	call   801702 <vfprintf>
	va_end(ap);

	return cnt;
}
  80179d:	c9                   	leave  
  80179e:	c3                   	ret    

0080179f <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  80179f:	55                   	push   %ebp
  8017a0:	89 e5                	mov    %esp,%ebp
  8017a2:	56                   	push   %esi
  8017a3:	53                   	push   %ebx
  8017a4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  8017a7:	83 ec 0c             	sub    $0xc,%esp
  8017aa:	ff 75 08             	pushl  0x8(%ebp)
  8017ad:	e8 0b f7 ff ff       	call   800ebd <fd2data>
  8017b2:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  8017b4:	83 c4 08             	add    $0x8,%esp
  8017b7:	68 8b 24 80 00       	push   $0x80248b
  8017bc:	53                   	push   %ebx
  8017bd:	e8 a0 ef ff ff       	call   800762 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  8017c2:	8b 46 04             	mov    0x4(%esi),%eax
  8017c5:	2b 06                	sub    (%esi),%eax
  8017c7:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  8017cd:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8017d4:	00 00 00 
	stat->st_dev = &devpipe;
  8017d7:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  8017de:	30 80 00 
	return 0;
}
  8017e1:	b8 00 00 00 00       	mov    $0x0,%eax
  8017e6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8017e9:	5b                   	pop    %ebx
  8017ea:	5e                   	pop    %esi
  8017eb:	5d                   	pop    %ebp
  8017ec:	c3                   	ret    

008017ed <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  8017ed:	55                   	push   %ebp
  8017ee:	89 e5                	mov    %esp,%ebp
  8017f0:	53                   	push   %ebx
  8017f1:	83 ec 0c             	sub    $0xc,%esp
  8017f4:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  8017f7:	53                   	push   %ebx
  8017f8:	6a 00                	push   $0x0
  8017fa:	e8 cc f3 ff ff       	call   800bcb <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  8017ff:	89 1c 24             	mov    %ebx,(%esp)
  801802:	e8 b6 f6 ff ff       	call   800ebd <fd2data>
  801807:	83 c4 08             	add    $0x8,%esp
  80180a:	50                   	push   %eax
  80180b:	6a 00                	push   $0x0
  80180d:	e8 b9 f3 ff ff       	call   800bcb <sys_page_unmap>
}
  801812:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801815:	c9                   	leave  
  801816:	c3                   	ret    

00801817 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801817:	55                   	push   %ebp
  801818:	89 e5                	mov    %esp,%ebp
  80181a:	57                   	push   %edi
  80181b:	56                   	push   %esi
  80181c:	53                   	push   %ebx
  80181d:	83 ec 1c             	sub    $0x1c,%esp
  801820:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801823:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801825:	a1 04 40 80 00       	mov    0x804004,%eax
  80182a:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  80182d:	83 ec 0c             	sub    $0xc,%esp
  801830:	ff 75 e0             	pushl  -0x20(%ebp)
  801833:	e8 6d 05 00 00       	call   801da5 <pageref>
  801838:	89 c3                	mov    %eax,%ebx
  80183a:	89 3c 24             	mov    %edi,(%esp)
  80183d:	e8 63 05 00 00       	call   801da5 <pageref>
  801842:	83 c4 10             	add    $0x10,%esp
  801845:	39 c3                	cmp    %eax,%ebx
  801847:	0f 94 c1             	sete   %cl
  80184a:	0f b6 c9             	movzbl %cl,%ecx
  80184d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801850:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801856:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801859:	39 ce                	cmp    %ecx,%esi
  80185b:	74 1b                	je     801878 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  80185d:	39 c3                	cmp    %eax,%ebx
  80185f:	75 c4                	jne    801825 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801861:	8b 42 58             	mov    0x58(%edx),%eax
  801864:	ff 75 e4             	pushl  -0x1c(%ebp)
  801867:	50                   	push   %eax
  801868:	56                   	push   %esi
  801869:	68 92 24 80 00       	push   $0x802492
  80186e:	e8 8b e9 ff ff       	call   8001fe <cprintf>
  801873:	83 c4 10             	add    $0x10,%esp
  801876:	eb ad                	jmp    801825 <_pipeisclosed+0xe>
	}
}
  801878:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  80187b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80187e:	5b                   	pop    %ebx
  80187f:	5e                   	pop    %esi
  801880:	5f                   	pop    %edi
  801881:	5d                   	pop    %ebp
  801882:	c3                   	ret    

00801883 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801883:	55                   	push   %ebp
  801884:	89 e5                	mov    %esp,%ebp
  801886:	57                   	push   %edi
  801887:	56                   	push   %esi
  801888:	53                   	push   %ebx
  801889:	83 ec 18             	sub    $0x18,%esp
  80188c:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  80188f:	56                   	push   %esi
  801890:	e8 28 f6 ff ff       	call   800ebd <fd2data>
  801895:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801897:	83 c4 10             	add    $0x10,%esp
  80189a:	bf 00 00 00 00       	mov    $0x0,%edi
  80189f:	eb 3b                	jmp    8018dc <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  8018a1:	89 da                	mov    %ebx,%edx
  8018a3:	89 f0                	mov    %esi,%eax
  8018a5:	e8 6d ff ff ff       	call   801817 <_pipeisclosed>
  8018aa:	85 c0                	test   %eax,%eax
  8018ac:	75 38                	jne    8018e6 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  8018ae:	e8 74 f2 ff ff       	call   800b27 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  8018b3:	8b 53 04             	mov    0x4(%ebx),%edx
  8018b6:	8b 03                	mov    (%ebx),%eax
  8018b8:	83 c0 20             	add    $0x20,%eax
  8018bb:	39 c2                	cmp    %eax,%edx
  8018bd:	73 e2                	jae    8018a1 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  8018bf:	8b 45 0c             	mov    0xc(%ebp),%eax
  8018c2:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  8018c5:	89 d0                	mov    %edx,%eax
  8018c7:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8018cc:	79 05                	jns    8018d3 <devpipe_write+0x50>
  8018ce:	48                   	dec    %eax
  8018cf:	83 c8 e0             	or     $0xffffffe0,%eax
  8018d2:	40                   	inc    %eax
  8018d3:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  8018d7:	42                   	inc    %edx
  8018d8:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8018db:	47                   	inc    %edi
  8018dc:	3b 7d 10             	cmp    0x10(%ebp),%edi
  8018df:	75 d2                	jne    8018b3 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  8018e1:	8b 45 10             	mov    0x10(%ebp),%eax
  8018e4:	eb 05                	jmp    8018eb <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8018e6:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  8018eb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8018ee:	5b                   	pop    %ebx
  8018ef:	5e                   	pop    %esi
  8018f0:	5f                   	pop    %edi
  8018f1:	5d                   	pop    %ebp
  8018f2:	c3                   	ret    

008018f3 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  8018f3:	55                   	push   %ebp
  8018f4:	89 e5                	mov    %esp,%ebp
  8018f6:	57                   	push   %edi
  8018f7:	56                   	push   %esi
  8018f8:	53                   	push   %ebx
  8018f9:	83 ec 18             	sub    $0x18,%esp
  8018fc:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  8018ff:	57                   	push   %edi
  801900:	e8 b8 f5 ff ff       	call   800ebd <fd2data>
  801905:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801907:	83 c4 10             	add    $0x10,%esp
  80190a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80190f:	eb 3a                	jmp    80194b <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801911:	85 db                	test   %ebx,%ebx
  801913:	74 04                	je     801919 <devpipe_read+0x26>
				return i;
  801915:	89 d8                	mov    %ebx,%eax
  801917:	eb 41                	jmp    80195a <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801919:	89 f2                	mov    %esi,%edx
  80191b:	89 f8                	mov    %edi,%eax
  80191d:	e8 f5 fe ff ff       	call   801817 <_pipeisclosed>
  801922:	85 c0                	test   %eax,%eax
  801924:	75 2f                	jne    801955 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801926:	e8 fc f1 ff ff       	call   800b27 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  80192b:	8b 06                	mov    (%esi),%eax
  80192d:	3b 46 04             	cmp    0x4(%esi),%eax
  801930:	74 df                	je     801911 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801932:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801937:	79 05                	jns    80193e <devpipe_read+0x4b>
  801939:	48                   	dec    %eax
  80193a:	83 c8 e0             	or     $0xffffffe0,%eax
  80193d:	40                   	inc    %eax
  80193e:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801942:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801945:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801948:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80194a:	43                   	inc    %ebx
  80194b:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  80194e:	75 db                	jne    80192b <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801950:	8b 45 10             	mov    0x10(%ebp),%eax
  801953:	eb 05                	jmp    80195a <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801955:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  80195a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80195d:	5b                   	pop    %ebx
  80195e:	5e                   	pop    %esi
  80195f:	5f                   	pop    %edi
  801960:	5d                   	pop    %ebp
  801961:	c3                   	ret    

00801962 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801962:	55                   	push   %ebp
  801963:	89 e5                	mov    %esp,%ebp
  801965:	56                   	push   %esi
  801966:	53                   	push   %ebx
  801967:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  80196a:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80196d:	50                   	push   %eax
  80196e:	e8 61 f5 ff ff       	call   800ed4 <fd_alloc>
  801973:	83 c4 10             	add    $0x10,%esp
  801976:	85 c0                	test   %eax,%eax
  801978:	0f 88 2a 01 00 00    	js     801aa8 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80197e:	83 ec 04             	sub    $0x4,%esp
  801981:	68 07 04 00 00       	push   $0x407
  801986:	ff 75 f4             	pushl  -0xc(%ebp)
  801989:	6a 00                	push   $0x0
  80198b:	e8 b6 f1 ff ff       	call   800b46 <sys_page_alloc>
  801990:	83 c4 10             	add    $0x10,%esp
  801993:	85 c0                	test   %eax,%eax
  801995:	0f 88 0d 01 00 00    	js     801aa8 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  80199b:	83 ec 0c             	sub    $0xc,%esp
  80199e:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8019a1:	50                   	push   %eax
  8019a2:	e8 2d f5 ff ff       	call   800ed4 <fd_alloc>
  8019a7:	89 c3                	mov    %eax,%ebx
  8019a9:	83 c4 10             	add    $0x10,%esp
  8019ac:	85 c0                	test   %eax,%eax
  8019ae:	0f 88 e2 00 00 00    	js     801a96 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8019b4:	83 ec 04             	sub    $0x4,%esp
  8019b7:	68 07 04 00 00       	push   $0x407
  8019bc:	ff 75 f0             	pushl  -0x10(%ebp)
  8019bf:	6a 00                	push   $0x0
  8019c1:	e8 80 f1 ff ff       	call   800b46 <sys_page_alloc>
  8019c6:	89 c3                	mov    %eax,%ebx
  8019c8:	83 c4 10             	add    $0x10,%esp
  8019cb:	85 c0                	test   %eax,%eax
  8019cd:	0f 88 c3 00 00 00    	js     801a96 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  8019d3:	83 ec 0c             	sub    $0xc,%esp
  8019d6:	ff 75 f4             	pushl  -0xc(%ebp)
  8019d9:	e8 df f4 ff ff       	call   800ebd <fd2data>
  8019de:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8019e0:	83 c4 0c             	add    $0xc,%esp
  8019e3:	68 07 04 00 00       	push   $0x407
  8019e8:	50                   	push   %eax
  8019e9:	6a 00                	push   $0x0
  8019eb:	e8 56 f1 ff ff       	call   800b46 <sys_page_alloc>
  8019f0:	89 c3                	mov    %eax,%ebx
  8019f2:	83 c4 10             	add    $0x10,%esp
  8019f5:	85 c0                	test   %eax,%eax
  8019f7:	0f 88 89 00 00 00    	js     801a86 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8019fd:	83 ec 0c             	sub    $0xc,%esp
  801a00:	ff 75 f0             	pushl  -0x10(%ebp)
  801a03:	e8 b5 f4 ff ff       	call   800ebd <fd2data>
  801a08:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801a0f:	50                   	push   %eax
  801a10:	6a 00                	push   $0x0
  801a12:	56                   	push   %esi
  801a13:	6a 00                	push   $0x0
  801a15:	e8 6f f1 ff ff       	call   800b89 <sys_page_map>
  801a1a:	89 c3                	mov    %eax,%ebx
  801a1c:	83 c4 20             	add    $0x20,%esp
  801a1f:	85 c0                	test   %eax,%eax
  801a21:	78 55                	js     801a78 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801a23:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801a29:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801a2c:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801a2e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801a31:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801a38:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801a3e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801a41:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801a43:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801a46:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801a4d:	83 ec 0c             	sub    $0xc,%esp
  801a50:	ff 75 f4             	pushl  -0xc(%ebp)
  801a53:	e8 55 f4 ff ff       	call   800ead <fd2num>
  801a58:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801a5b:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801a5d:	83 c4 04             	add    $0x4,%esp
  801a60:	ff 75 f0             	pushl  -0x10(%ebp)
  801a63:	e8 45 f4 ff ff       	call   800ead <fd2num>
  801a68:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801a6b:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801a6e:	83 c4 10             	add    $0x10,%esp
  801a71:	b8 00 00 00 00       	mov    $0x0,%eax
  801a76:	eb 30                	jmp    801aa8 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801a78:	83 ec 08             	sub    $0x8,%esp
  801a7b:	56                   	push   %esi
  801a7c:	6a 00                	push   $0x0
  801a7e:	e8 48 f1 ff ff       	call   800bcb <sys_page_unmap>
  801a83:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801a86:	83 ec 08             	sub    $0x8,%esp
  801a89:	ff 75 f0             	pushl  -0x10(%ebp)
  801a8c:	6a 00                	push   $0x0
  801a8e:	e8 38 f1 ff ff       	call   800bcb <sys_page_unmap>
  801a93:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801a96:	83 ec 08             	sub    $0x8,%esp
  801a99:	ff 75 f4             	pushl  -0xc(%ebp)
  801a9c:	6a 00                	push   $0x0
  801a9e:	e8 28 f1 ff ff       	call   800bcb <sys_page_unmap>
  801aa3:	83 c4 10             	add    $0x10,%esp
  801aa6:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801aa8:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801aab:	5b                   	pop    %ebx
  801aac:	5e                   	pop    %esi
  801aad:	5d                   	pop    %ebp
  801aae:	c3                   	ret    

00801aaf <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801aaf:	55                   	push   %ebp
  801ab0:	89 e5                	mov    %esp,%ebp
  801ab2:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801ab5:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ab8:	50                   	push   %eax
  801ab9:	ff 75 08             	pushl  0x8(%ebp)
  801abc:	e8 62 f4 ff ff       	call   800f23 <fd_lookup>
  801ac1:	83 c4 10             	add    $0x10,%esp
  801ac4:	85 c0                	test   %eax,%eax
  801ac6:	78 18                	js     801ae0 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801ac8:	83 ec 0c             	sub    $0xc,%esp
  801acb:	ff 75 f4             	pushl  -0xc(%ebp)
  801ace:	e8 ea f3 ff ff       	call   800ebd <fd2data>
	return _pipeisclosed(fd, p);
  801ad3:	89 c2                	mov    %eax,%edx
  801ad5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ad8:	e8 3a fd ff ff       	call   801817 <_pipeisclosed>
  801add:	83 c4 10             	add    $0x10,%esp
}
  801ae0:	c9                   	leave  
  801ae1:	c3                   	ret    

00801ae2 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801ae2:	55                   	push   %ebp
  801ae3:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801ae5:	b8 00 00 00 00       	mov    $0x0,%eax
  801aea:	5d                   	pop    %ebp
  801aeb:	c3                   	ret    

00801aec <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801aec:	55                   	push   %ebp
  801aed:	89 e5                	mov    %esp,%ebp
  801aef:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801af2:	68 aa 24 80 00       	push   $0x8024aa
  801af7:	ff 75 0c             	pushl  0xc(%ebp)
  801afa:	e8 63 ec ff ff       	call   800762 <strcpy>
	return 0;
}
  801aff:	b8 00 00 00 00       	mov    $0x0,%eax
  801b04:	c9                   	leave  
  801b05:	c3                   	ret    

00801b06 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801b06:	55                   	push   %ebp
  801b07:	89 e5                	mov    %esp,%ebp
  801b09:	57                   	push   %edi
  801b0a:	56                   	push   %esi
  801b0b:	53                   	push   %ebx
  801b0c:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801b12:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801b17:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801b1d:	eb 2c                	jmp    801b4b <devcons_write+0x45>
		m = n - tot;
  801b1f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801b22:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801b24:	83 fb 7f             	cmp    $0x7f,%ebx
  801b27:	76 05                	jbe    801b2e <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801b29:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801b2e:	83 ec 04             	sub    $0x4,%esp
  801b31:	53                   	push   %ebx
  801b32:	03 45 0c             	add    0xc(%ebp),%eax
  801b35:	50                   	push   %eax
  801b36:	57                   	push   %edi
  801b37:	e8 9b ed ff ff       	call   8008d7 <memmove>
		sys_cputs(buf, m);
  801b3c:	83 c4 08             	add    $0x8,%esp
  801b3f:	53                   	push   %ebx
  801b40:	57                   	push   %edi
  801b41:	e8 44 ef ff ff       	call   800a8a <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801b46:	01 de                	add    %ebx,%esi
  801b48:	83 c4 10             	add    $0x10,%esp
  801b4b:	89 f0                	mov    %esi,%eax
  801b4d:	3b 75 10             	cmp    0x10(%ebp),%esi
  801b50:	72 cd                	jb     801b1f <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801b52:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b55:	5b                   	pop    %ebx
  801b56:	5e                   	pop    %esi
  801b57:	5f                   	pop    %edi
  801b58:	5d                   	pop    %ebp
  801b59:	c3                   	ret    

00801b5a <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801b5a:	55                   	push   %ebp
  801b5b:	89 e5                	mov    %esp,%ebp
  801b5d:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801b60:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801b64:	75 07                	jne    801b6d <devcons_read+0x13>
  801b66:	eb 23                	jmp    801b8b <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801b68:	e8 ba ef ff ff       	call   800b27 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801b6d:	e8 36 ef ff ff       	call   800aa8 <sys_cgetc>
  801b72:	85 c0                	test   %eax,%eax
  801b74:	74 f2                	je     801b68 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801b76:	85 c0                	test   %eax,%eax
  801b78:	78 1d                	js     801b97 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801b7a:	83 f8 04             	cmp    $0x4,%eax
  801b7d:	74 13                	je     801b92 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801b7f:	8b 55 0c             	mov    0xc(%ebp),%edx
  801b82:	88 02                	mov    %al,(%edx)
	return 1;
  801b84:	b8 01 00 00 00       	mov    $0x1,%eax
  801b89:	eb 0c                	jmp    801b97 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801b8b:	b8 00 00 00 00       	mov    $0x0,%eax
  801b90:	eb 05                	jmp    801b97 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801b92:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801b97:	c9                   	leave  
  801b98:	c3                   	ret    

00801b99 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801b99:	55                   	push   %ebp
  801b9a:	89 e5                	mov    %esp,%ebp
  801b9c:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801b9f:	8b 45 08             	mov    0x8(%ebp),%eax
  801ba2:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801ba5:	6a 01                	push   $0x1
  801ba7:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801baa:	50                   	push   %eax
  801bab:	e8 da ee ff ff       	call   800a8a <sys_cputs>
}
  801bb0:	83 c4 10             	add    $0x10,%esp
  801bb3:	c9                   	leave  
  801bb4:	c3                   	ret    

00801bb5 <getchar>:

int
getchar(void)
{
  801bb5:	55                   	push   %ebp
  801bb6:	89 e5                	mov    %esp,%ebp
  801bb8:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801bbb:	6a 01                	push   $0x1
  801bbd:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801bc0:	50                   	push   %eax
  801bc1:	6a 00                	push   $0x0
  801bc3:	e8 c1 f5 ff ff       	call   801189 <read>
	if (r < 0)
  801bc8:	83 c4 10             	add    $0x10,%esp
  801bcb:	85 c0                	test   %eax,%eax
  801bcd:	78 0f                	js     801bde <getchar+0x29>
		return r;
	if (r < 1)
  801bcf:	85 c0                	test   %eax,%eax
  801bd1:	7e 06                	jle    801bd9 <getchar+0x24>
		return -E_EOF;
	return c;
  801bd3:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801bd7:	eb 05                	jmp    801bde <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801bd9:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801bde:	c9                   	leave  
  801bdf:	c3                   	ret    

00801be0 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801be0:	55                   	push   %ebp
  801be1:	89 e5                	mov    %esp,%ebp
  801be3:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801be6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801be9:	50                   	push   %eax
  801bea:	ff 75 08             	pushl  0x8(%ebp)
  801bed:	e8 31 f3 ff ff       	call   800f23 <fd_lookup>
  801bf2:	83 c4 10             	add    $0x10,%esp
  801bf5:	85 c0                	test   %eax,%eax
  801bf7:	78 11                	js     801c0a <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801bf9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801bfc:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801c02:	39 10                	cmp    %edx,(%eax)
  801c04:	0f 94 c0             	sete   %al
  801c07:	0f b6 c0             	movzbl %al,%eax
}
  801c0a:	c9                   	leave  
  801c0b:	c3                   	ret    

00801c0c <opencons>:

int
opencons(void)
{
  801c0c:	55                   	push   %ebp
  801c0d:	89 e5                	mov    %esp,%ebp
  801c0f:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801c12:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c15:	50                   	push   %eax
  801c16:	e8 b9 f2 ff ff       	call   800ed4 <fd_alloc>
  801c1b:	83 c4 10             	add    $0x10,%esp
  801c1e:	85 c0                	test   %eax,%eax
  801c20:	78 3a                	js     801c5c <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801c22:	83 ec 04             	sub    $0x4,%esp
  801c25:	68 07 04 00 00       	push   $0x407
  801c2a:	ff 75 f4             	pushl  -0xc(%ebp)
  801c2d:	6a 00                	push   $0x0
  801c2f:	e8 12 ef ff ff       	call   800b46 <sys_page_alloc>
  801c34:	83 c4 10             	add    $0x10,%esp
  801c37:	85 c0                	test   %eax,%eax
  801c39:	78 21                	js     801c5c <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801c3b:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801c41:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c44:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801c46:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c49:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801c50:	83 ec 0c             	sub    $0xc,%esp
  801c53:	50                   	push   %eax
  801c54:	e8 54 f2 ff ff       	call   800ead <fd2num>
  801c59:	83 c4 10             	add    $0x10,%esp
}
  801c5c:	c9                   	leave  
  801c5d:	c3                   	ret    

00801c5e <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  801c5e:	55                   	push   %ebp
  801c5f:	89 e5                	mov    %esp,%ebp
  801c61:	56                   	push   %esi
  801c62:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  801c63:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  801c66:	8b 35 00 30 80 00    	mov    0x803000,%esi
  801c6c:	e8 97 ee ff ff       	call   800b08 <sys_getenvid>
  801c71:	83 ec 0c             	sub    $0xc,%esp
  801c74:	ff 75 0c             	pushl  0xc(%ebp)
  801c77:	ff 75 08             	pushl  0x8(%ebp)
  801c7a:	56                   	push   %esi
  801c7b:	50                   	push   %eax
  801c7c:	68 b8 24 80 00       	push   $0x8024b8
  801c81:	e8 78 e5 ff ff       	call   8001fe <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  801c86:	83 c4 18             	add    $0x18,%esp
  801c89:	53                   	push   %ebx
  801c8a:	ff 75 10             	pushl  0x10(%ebp)
  801c8d:	e8 1b e5 ff ff       	call   8001ad <vcprintf>
	cprintf("\n");
  801c92:	c7 04 24 70 20 80 00 	movl   $0x802070,(%esp)
  801c99:	e8 60 e5 ff ff       	call   8001fe <cprintf>
  801c9e:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801ca1:	cc                   	int3   
  801ca2:	eb fd                	jmp    801ca1 <_panic+0x43>

00801ca4 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801ca4:	55                   	push   %ebp
  801ca5:	89 e5                	mov    %esp,%ebp
  801ca7:	56                   	push   %esi
  801ca8:	53                   	push   %ebx
  801ca9:	8b 75 08             	mov    0x8(%ebp),%esi
  801cac:	8b 45 0c             	mov    0xc(%ebp),%eax
  801caf:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801cb2:	85 c0                	test   %eax,%eax
  801cb4:	75 05                	jne    801cbb <ipc_recv+0x17>
        pg = (void *)UTOP;
  801cb6:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801cbb:	83 ec 0c             	sub    $0xc,%esp
  801cbe:	50                   	push   %eax
  801cbf:	e8 51 f0 ff ff       	call   800d15 <sys_ipc_recv>
  801cc4:	83 c4 10             	add    $0x10,%esp
  801cc7:	85 c0                	test   %eax,%eax
  801cc9:	74 16                	je     801ce1 <ipc_recv+0x3d>
        if (from_env_store)
  801ccb:	85 f6                	test   %esi,%esi
  801ccd:	74 06                	je     801cd5 <ipc_recv+0x31>
            *from_env_store = 0;
  801ccf:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801cd5:	85 db                	test   %ebx,%ebx
  801cd7:	74 2c                	je     801d05 <ipc_recv+0x61>
            *perm_store = 0;
  801cd9:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801cdf:	eb 24                	jmp    801d05 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801ce1:	85 f6                	test   %esi,%esi
  801ce3:	74 0a                	je     801cef <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801ce5:	a1 04 40 80 00       	mov    0x804004,%eax
  801cea:	8b 40 74             	mov    0x74(%eax),%eax
  801ced:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801cef:	85 db                	test   %ebx,%ebx
  801cf1:	74 0a                	je     801cfd <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801cf3:	a1 04 40 80 00       	mov    0x804004,%eax
  801cf8:	8b 40 78             	mov    0x78(%eax),%eax
  801cfb:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801cfd:	a1 04 40 80 00       	mov    0x804004,%eax
  801d02:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801d05:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801d08:	5b                   	pop    %ebx
  801d09:	5e                   	pop    %esi
  801d0a:	5d                   	pop    %ebp
  801d0b:	c3                   	ret    

00801d0c <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801d0c:	55                   	push   %ebp
  801d0d:	89 e5                	mov    %esp,%ebp
  801d0f:	57                   	push   %edi
  801d10:	56                   	push   %esi
  801d11:	53                   	push   %ebx
  801d12:	83 ec 0c             	sub    $0xc,%esp
  801d15:	8b 75 0c             	mov    0xc(%ebp),%esi
  801d18:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801d1b:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801d1e:	85 db                	test   %ebx,%ebx
  801d20:	75 0c                	jne    801d2e <ipc_send+0x22>
        pg = (void *)UTOP;
  801d22:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801d27:	eb 05                	jmp    801d2e <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801d29:	e8 f9 ed ff ff       	call   800b27 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801d2e:	57                   	push   %edi
  801d2f:	53                   	push   %ebx
  801d30:	56                   	push   %esi
  801d31:	ff 75 08             	pushl  0x8(%ebp)
  801d34:	e8 b9 ef ff ff       	call   800cf2 <sys_ipc_try_send>
  801d39:	83 c4 10             	add    $0x10,%esp
  801d3c:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801d3f:	74 e8                	je     801d29 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801d41:	85 c0                	test   %eax,%eax
  801d43:	74 14                	je     801d59 <ipc_send+0x4d>
        panic("ipc_send: error");
  801d45:	83 ec 04             	sub    $0x4,%esp
  801d48:	68 dc 24 80 00       	push   $0x8024dc
  801d4d:	6a 6a                	push   $0x6a
  801d4f:	68 ec 24 80 00       	push   $0x8024ec
  801d54:	e8 05 ff ff ff       	call   801c5e <_panic>
    }
	//panic("ipc_send not implemented");
}
  801d59:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d5c:	5b                   	pop    %ebx
  801d5d:	5e                   	pop    %esi
  801d5e:	5f                   	pop    %edi
  801d5f:	5d                   	pop    %ebp
  801d60:	c3                   	ret    

00801d61 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801d61:	55                   	push   %ebp
  801d62:	89 e5                	mov    %esp,%ebp
  801d64:	53                   	push   %ebx
  801d65:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801d68:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801d6d:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801d74:	89 d0                	mov    %edx,%eax
  801d76:	c1 e0 07             	shl    $0x7,%eax
  801d79:	29 d8                	sub    %ebx,%eax
  801d7b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801d80:	8b 40 50             	mov    0x50(%eax),%eax
  801d83:	39 c8                	cmp    %ecx,%eax
  801d85:	75 0d                	jne    801d94 <ipc_find_env+0x33>
			return envs[i].env_id;
  801d87:	c1 e2 07             	shl    $0x7,%edx
  801d8a:	29 da                	sub    %ebx,%edx
  801d8c:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801d92:	eb 0e                	jmp    801da2 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801d94:	42                   	inc    %edx
  801d95:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801d9b:	75 d0                	jne    801d6d <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801d9d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801da2:	5b                   	pop    %ebx
  801da3:	5d                   	pop    %ebp
  801da4:	c3                   	ret    

00801da5 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801da5:	55                   	push   %ebp
  801da6:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801da8:	8b 45 08             	mov    0x8(%ebp),%eax
  801dab:	c1 e8 16             	shr    $0x16,%eax
  801dae:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801db5:	a8 01                	test   $0x1,%al
  801db7:	74 21                	je     801dda <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801db9:	8b 45 08             	mov    0x8(%ebp),%eax
  801dbc:	c1 e8 0c             	shr    $0xc,%eax
  801dbf:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801dc6:	a8 01                	test   $0x1,%al
  801dc8:	74 17                	je     801de1 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801dca:	c1 e8 0c             	shr    $0xc,%eax
  801dcd:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801dd4:	ef 
  801dd5:	0f b7 c0             	movzwl %ax,%eax
  801dd8:	eb 0c                	jmp    801de6 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801dda:	b8 00 00 00 00       	mov    $0x0,%eax
  801ddf:	eb 05                	jmp    801de6 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801de1:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801de6:	5d                   	pop    %ebp
  801de7:	c3                   	ret    

00801de8 <__udivdi3>:
  801de8:	55                   	push   %ebp
  801de9:	57                   	push   %edi
  801dea:	56                   	push   %esi
  801deb:	53                   	push   %ebx
  801dec:	83 ec 1c             	sub    $0x1c,%esp
  801def:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801df3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801df7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801dfb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801dff:	89 ca                	mov    %ecx,%edx
  801e01:	89 f8                	mov    %edi,%eax
  801e03:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801e07:	85 f6                	test   %esi,%esi
  801e09:	75 2d                	jne    801e38 <__udivdi3+0x50>
  801e0b:	39 cf                	cmp    %ecx,%edi
  801e0d:	77 65                	ja     801e74 <__udivdi3+0x8c>
  801e0f:	89 fd                	mov    %edi,%ebp
  801e11:	85 ff                	test   %edi,%edi
  801e13:	75 0b                	jne    801e20 <__udivdi3+0x38>
  801e15:	b8 01 00 00 00       	mov    $0x1,%eax
  801e1a:	31 d2                	xor    %edx,%edx
  801e1c:	f7 f7                	div    %edi
  801e1e:	89 c5                	mov    %eax,%ebp
  801e20:	31 d2                	xor    %edx,%edx
  801e22:	89 c8                	mov    %ecx,%eax
  801e24:	f7 f5                	div    %ebp
  801e26:	89 c1                	mov    %eax,%ecx
  801e28:	89 d8                	mov    %ebx,%eax
  801e2a:	f7 f5                	div    %ebp
  801e2c:	89 cf                	mov    %ecx,%edi
  801e2e:	89 fa                	mov    %edi,%edx
  801e30:	83 c4 1c             	add    $0x1c,%esp
  801e33:	5b                   	pop    %ebx
  801e34:	5e                   	pop    %esi
  801e35:	5f                   	pop    %edi
  801e36:	5d                   	pop    %ebp
  801e37:	c3                   	ret    
  801e38:	39 ce                	cmp    %ecx,%esi
  801e3a:	77 28                	ja     801e64 <__udivdi3+0x7c>
  801e3c:	0f bd fe             	bsr    %esi,%edi
  801e3f:	83 f7 1f             	xor    $0x1f,%edi
  801e42:	75 40                	jne    801e84 <__udivdi3+0x9c>
  801e44:	39 ce                	cmp    %ecx,%esi
  801e46:	72 0a                	jb     801e52 <__udivdi3+0x6a>
  801e48:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801e4c:	0f 87 9e 00 00 00    	ja     801ef0 <__udivdi3+0x108>
  801e52:	b8 01 00 00 00       	mov    $0x1,%eax
  801e57:	89 fa                	mov    %edi,%edx
  801e59:	83 c4 1c             	add    $0x1c,%esp
  801e5c:	5b                   	pop    %ebx
  801e5d:	5e                   	pop    %esi
  801e5e:	5f                   	pop    %edi
  801e5f:	5d                   	pop    %ebp
  801e60:	c3                   	ret    
  801e61:	8d 76 00             	lea    0x0(%esi),%esi
  801e64:	31 ff                	xor    %edi,%edi
  801e66:	31 c0                	xor    %eax,%eax
  801e68:	89 fa                	mov    %edi,%edx
  801e6a:	83 c4 1c             	add    $0x1c,%esp
  801e6d:	5b                   	pop    %ebx
  801e6e:	5e                   	pop    %esi
  801e6f:	5f                   	pop    %edi
  801e70:	5d                   	pop    %ebp
  801e71:	c3                   	ret    
  801e72:	66 90                	xchg   %ax,%ax
  801e74:	89 d8                	mov    %ebx,%eax
  801e76:	f7 f7                	div    %edi
  801e78:	31 ff                	xor    %edi,%edi
  801e7a:	89 fa                	mov    %edi,%edx
  801e7c:	83 c4 1c             	add    $0x1c,%esp
  801e7f:	5b                   	pop    %ebx
  801e80:	5e                   	pop    %esi
  801e81:	5f                   	pop    %edi
  801e82:	5d                   	pop    %ebp
  801e83:	c3                   	ret    
  801e84:	bd 20 00 00 00       	mov    $0x20,%ebp
  801e89:	89 eb                	mov    %ebp,%ebx
  801e8b:	29 fb                	sub    %edi,%ebx
  801e8d:	89 f9                	mov    %edi,%ecx
  801e8f:	d3 e6                	shl    %cl,%esi
  801e91:	89 c5                	mov    %eax,%ebp
  801e93:	88 d9                	mov    %bl,%cl
  801e95:	d3 ed                	shr    %cl,%ebp
  801e97:	89 e9                	mov    %ebp,%ecx
  801e99:	09 f1                	or     %esi,%ecx
  801e9b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801e9f:	89 f9                	mov    %edi,%ecx
  801ea1:	d3 e0                	shl    %cl,%eax
  801ea3:	89 c5                	mov    %eax,%ebp
  801ea5:	89 d6                	mov    %edx,%esi
  801ea7:	88 d9                	mov    %bl,%cl
  801ea9:	d3 ee                	shr    %cl,%esi
  801eab:	89 f9                	mov    %edi,%ecx
  801ead:	d3 e2                	shl    %cl,%edx
  801eaf:	8b 44 24 08          	mov    0x8(%esp),%eax
  801eb3:	88 d9                	mov    %bl,%cl
  801eb5:	d3 e8                	shr    %cl,%eax
  801eb7:	09 c2                	or     %eax,%edx
  801eb9:	89 d0                	mov    %edx,%eax
  801ebb:	89 f2                	mov    %esi,%edx
  801ebd:	f7 74 24 0c          	divl   0xc(%esp)
  801ec1:	89 d6                	mov    %edx,%esi
  801ec3:	89 c3                	mov    %eax,%ebx
  801ec5:	f7 e5                	mul    %ebp
  801ec7:	39 d6                	cmp    %edx,%esi
  801ec9:	72 19                	jb     801ee4 <__udivdi3+0xfc>
  801ecb:	74 0b                	je     801ed8 <__udivdi3+0xf0>
  801ecd:	89 d8                	mov    %ebx,%eax
  801ecf:	31 ff                	xor    %edi,%edi
  801ed1:	e9 58 ff ff ff       	jmp    801e2e <__udivdi3+0x46>
  801ed6:	66 90                	xchg   %ax,%ax
  801ed8:	8b 54 24 08          	mov    0x8(%esp),%edx
  801edc:	89 f9                	mov    %edi,%ecx
  801ede:	d3 e2                	shl    %cl,%edx
  801ee0:	39 c2                	cmp    %eax,%edx
  801ee2:	73 e9                	jae    801ecd <__udivdi3+0xe5>
  801ee4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801ee7:	31 ff                	xor    %edi,%edi
  801ee9:	e9 40 ff ff ff       	jmp    801e2e <__udivdi3+0x46>
  801eee:	66 90                	xchg   %ax,%ax
  801ef0:	31 c0                	xor    %eax,%eax
  801ef2:	e9 37 ff ff ff       	jmp    801e2e <__udivdi3+0x46>
  801ef7:	90                   	nop

00801ef8 <__umoddi3>:
  801ef8:	55                   	push   %ebp
  801ef9:	57                   	push   %edi
  801efa:	56                   	push   %esi
  801efb:	53                   	push   %ebx
  801efc:	83 ec 1c             	sub    $0x1c,%esp
  801eff:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  801f03:	8b 74 24 34          	mov    0x34(%esp),%esi
  801f07:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801f0b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801f0f:	89 44 24 0c          	mov    %eax,0xc(%esp)
  801f13:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801f17:	89 f3                	mov    %esi,%ebx
  801f19:	89 fa                	mov    %edi,%edx
  801f1b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801f1f:	89 34 24             	mov    %esi,(%esp)
  801f22:	85 c0                	test   %eax,%eax
  801f24:	75 1a                	jne    801f40 <__umoddi3+0x48>
  801f26:	39 f7                	cmp    %esi,%edi
  801f28:	0f 86 a2 00 00 00    	jbe    801fd0 <__umoddi3+0xd8>
  801f2e:	89 c8                	mov    %ecx,%eax
  801f30:	89 f2                	mov    %esi,%edx
  801f32:	f7 f7                	div    %edi
  801f34:	89 d0                	mov    %edx,%eax
  801f36:	31 d2                	xor    %edx,%edx
  801f38:	83 c4 1c             	add    $0x1c,%esp
  801f3b:	5b                   	pop    %ebx
  801f3c:	5e                   	pop    %esi
  801f3d:	5f                   	pop    %edi
  801f3e:	5d                   	pop    %ebp
  801f3f:	c3                   	ret    
  801f40:	39 f0                	cmp    %esi,%eax
  801f42:	0f 87 ac 00 00 00    	ja     801ff4 <__umoddi3+0xfc>
  801f48:	0f bd e8             	bsr    %eax,%ebp
  801f4b:	83 f5 1f             	xor    $0x1f,%ebp
  801f4e:	0f 84 ac 00 00 00    	je     802000 <__umoddi3+0x108>
  801f54:	bf 20 00 00 00       	mov    $0x20,%edi
  801f59:	29 ef                	sub    %ebp,%edi
  801f5b:	89 fe                	mov    %edi,%esi
  801f5d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801f61:	89 e9                	mov    %ebp,%ecx
  801f63:	d3 e0                	shl    %cl,%eax
  801f65:	89 d7                	mov    %edx,%edi
  801f67:	89 f1                	mov    %esi,%ecx
  801f69:	d3 ef                	shr    %cl,%edi
  801f6b:	09 c7                	or     %eax,%edi
  801f6d:	89 e9                	mov    %ebp,%ecx
  801f6f:	d3 e2                	shl    %cl,%edx
  801f71:	89 14 24             	mov    %edx,(%esp)
  801f74:	89 d8                	mov    %ebx,%eax
  801f76:	d3 e0                	shl    %cl,%eax
  801f78:	89 c2                	mov    %eax,%edx
  801f7a:	8b 44 24 08          	mov    0x8(%esp),%eax
  801f7e:	d3 e0                	shl    %cl,%eax
  801f80:	89 44 24 04          	mov    %eax,0x4(%esp)
  801f84:	8b 44 24 08          	mov    0x8(%esp),%eax
  801f88:	89 f1                	mov    %esi,%ecx
  801f8a:	d3 e8                	shr    %cl,%eax
  801f8c:	09 d0                	or     %edx,%eax
  801f8e:	d3 eb                	shr    %cl,%ebx
  801f90:	89 da                	mov    %ebx,%edx
  801f92:	f7 f7                	div    %edi
  801f94:	89 d3                	mov    %edx,%ebx
  801f96:	f7 24 24             	mull   (%esp)
  801f99:	89 c6                	mov    %eax,%esi
  801f9b:	89 d1                	mov    %edx,%ecx
  801f9d:	39 d3                	cmp    %edx,%ebx
  801f9f:	0f 82 87 00 00 00    	jb     80202c <__umoddi3+0x134>
  801fa5:	0f 84 91 00 00 00    	je     80203c <__umoddi3+0x144>
  801fab:	8b 54 24 04          	mov    0x4(%esp),%edx
  801faf:	29 f2                	sub    %esi,%edx
  801fb1:	19 cb                	sbb    %ecx,%ebx
  801fb3:	89 d8                	mov    %ebx,%eax
  801fb5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801fb9:	d3 e0                	shl    %cl,%eax
  801fbb:	89 e9                	mov    %ebp,%ecx
  801fbd:	d3 ea                	shr    %cl,%edx
  801fbf:	09 d0                	or     %edx,%eax
  801fc1:	89 e9                	mov    %ebp,%ecx
  801fc3:	d3 eb                	shr    %cl,%ebx
  801fc5:	89 da                	mov    %ebx,%edx
  801fc7:	83 c4 1c             	add    $0x1c,%esp
  801fca:	5b                   	pop    %ebx
  801fcb:	5e                   	pop    %esi
  801fcc:	5f                   	pop    %edi
  801fcd:	5d                   	pop    %ebp
  801fce:	c3                   	ret    
  801fcf:	90                   	nop
  801fd0:	89 fd                	mov    %edi,%ebp
  801fd2:	85 ff                	test   %edi,%edi
  801fd4:	75 0b                	jne    801fe1 <__umoddi3+0xe9>
  801fd6:	b8 01 00 00 00       	mov    $0x1,%eax
  801fdb:	31 d2                	xor    %edx,%edx
  801fdd:	f7 f7                	div    %edi
  801fdf:	89 c5                	mov    %eax,%ebp
  801fe1:	89 f0                	mov    %esi,%eax
  801fe3:	31 d2                	xor    %edx,%edx
  801fe5:	f7 f5                	div    %ebp
  801fe7:	89 c8                	mov    %ecx,%eax
  801fe9:	f7 f5                	div    %ebp
  801feb:	89 d0                	mov    %edx,%eax
  801fed:	e9 44 ff ff ff       	jmp    801f36 <__umoddi3+0x3e>
  801ff2:	66 90                	xchg   %ax,%ax
  801ff4:	89 c8                	mov    %ecx,%eax
  801ff6:	89 f2                	mov    %esi,%edx
  801ff8:	83 c4 1c             	add    $0x1c,%esp
  801ffb:	5b                   	pop    %ebx
  801ffc:	5e                   	pop    %esi
  801ffd:	5f                   	pop    %edi
  801ffe:	5d                   	pop    %ebp
  801fff:	c3                   	ret    
  802000:	3b 04 24             	cmp    (%esp),%eax
  802003:	72 06                	jb     80200b <__umoddi3+0x113>
  802005:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  802009:	77 0f                	ja     80201a <__umoddi3+0x122>
  80200b:	89 f2                	mov    %esi,%edx
  80200d:	29 f9                	sub    %edi,%ecx
  80200f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  802013:	89 14 24             	mov    %edx,(%esp)
  802016:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80201a:	8b 44 24 04          	mov    0x4(%esp),%eax
  80201e:	8b 14 24             	mov    (%esp),%edx
  802021:	83 c4 1c             	add    $0x1c,%esp
  802024:	5b                   	pop    %ebx
  802025:	5e                   	pop    %esi
  802026:	5f                   	pop    %edi
  802027:	5d                   	pop    %ebp
  802028:	c3                   	ret    
  802029:	8d 76 00             	lea    0x0(%esi),%esi
  80202c:	2b 04 24             	sub    (%esp),%eax
  80202f:	19 fa                	sbb    %edi,%edx
  802031:	89 d1                	mov    %edx,%ecx
  802033:	89 c6                	mov    %eax,%esi
  802035:	e9 71 ff ff ff       	jmp    801fab <__umoddi3+0xb3>
  80203a:	66 90                	xchg   %ax,%ax
  80203c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802040:	72 ea                	jb     80202c <__umoddi3+0x134>
  802042:	89 d9                	mov    %ebx,%ecx
  802044:	e9 62 ff ff ff       	jmp    801fab <__umoddi3+0xb3>
