
obj/user/forktree.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 b0 00 00 00       	call   8000e1 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <forktree>:
	}
}

void
forktree(const char *cur)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 04             	sub    $0x4,%esp
  80003a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("%04x: I am '%s'\n", sys_getenvid(), cur);
  80003d:	e8 9c 0a 00 00       	call   800ade <sys_getenvid>
  800042:	83 ec 04             	sub    $0x4,%esp
  800045:	53                   	push   %ebx
  800046:	50                   	push   %eax
  800047:	68 40 13 80 00       	push   $0x801340
  80004c:	e8 83 01 00 00       	call   8001d4 <cprintf>

	forkchild(cur, '0');
  800051:	83 c4 08             	add    $0x8,%esp
  800054:	6a 30                	push   $0x30
  800056:	53                   	push   %ebx
  800057:	e8 13 00 00 00       	call   80006f <forkchild>
	forkchild(cur, '1');
  80005c:	83 c4 08             	add    $0x8,%esp
  80005f:	6a 31                	push   $0x31
  800061:	53                   	push   %ebx
  800062:	e8 08 00 00 00       	call   80006f <forkchild>
}
  800067:	83 c4 10             	add    $0x10,%esp
  80006a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80006d:	c9                   	leave  
  80006e:	c3                   	ret    

0080006f <forkchild>:

void forktree(const char *cur);

void
forkchild(const char *cur, char branch)
{
  80006f:	55                   	push   %ebp
  800070:	89 e5                	mov    %esp,%ebp
  800072:	56                   	push   %esi
  800073:	53                   	push   %ebx
  800074:	83 ec 1c             	sub    $0x1c,%esp
  800077:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80007a:	8b 75 0c             	mov    0xc(%ebp),%esi
	char nxt[DEPTH+1];

	if (strlen(cur) >= DEPTH)
  80007d:	53                   	push   %ebx
  80007e:	e8 80 06 00 00       	call   800703 <strlen>
  800083:	83 c4 10             	add    $0x10,%esp
  800086:	83 f8 02             	cmp    $0x2,%eax
  800089:	7f 3a                	jg     8000c5 <forkchild+0x56>
		return;

	snprintf(nxt, DEPTH+1, "%s%c", cur, branch);
  80008b:	83 ec 0c             	sub    $0xc,%esp
  80008e:	89 f0                	mov    %esi,%eax
  800090:	0f be f0             	movsbl %al,%esi
  800093:	56                   	push   %esi
  800094:	53                   	push   %ebx
  800095:	68 51 13 80 00       	push   $0x801351
  80009a:	6a 04                	push   $0x4
  80009c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80009f:	50                   	push   %eax
  8000a0:	e8 44 06 00 00       	call   8006e9 <snprintf>
	if (fork() == 0) {
  8000a5:	83 c4 20             	add    $0x20,%esp
  8000a8:	e8 31 0d 00 00       	call   800dde <fork>
  8000ad:	85 c0                	test   %eax,%eax
  8000af:	75 14                	jne    8000c5 <forkchild+0x56>
		forktree(nxt);
  8000b1:	83 ec 0c             	sub    $0xc,%esp
  8000b4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8000b7:	50                   	push   %eax
  8000b8:	e8 76 ff ff ff       	call   800033 <forktree>
		exit();
  8000bd:	e8 6e 00 00 00       	call   800130 <exit>
  8000c2:	83 c4 10             	add    $0x10,%esp
	}
}
  8000c5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000c8:	5b                   	pop    %ebx
  8000c9:	5e                   	pop    %esi
  8000ca:	5d                   	pop    %ebp
  8000cb:	c3                   	ret    

008000cc <umain>:
	forkchild(cur, '1');
}

void
umain(int argc, char **argv)
{
  8000cc:	55                   	push   %ebp
  8000cd:	89 e5                	mov    %esp,%ebp
  8000cf:	83 ec 14             	sub    $0x14,%esp
	forktree("");
  8000d2:	68 50 13 80 00       	push   $0x801350
  8000d7:	e8 57 ff ff ff       	call   800033 <forktree>
}
  8000dc:	83 c4 10             	add    $0x10,%esp
  8000df:	c9                   	leave  
  8000e0:	c3                   	ret    

008000e1 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000e1:	55                   	push   %ebp
  8000e2:	89 e5                	mov    %esp,%ebp
  8000e4:	56                   	push   %esi
  8000e5:	53                   	push   %ebx
  8000e6:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000e9:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000ec:	e8 ed 09 00 00       	call   800ade <sys_getenvid>
  8000f1:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000f6:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000fd:	c1 e0 07             	shl    $0x7,%eax
  800100:	29 d0                	sub    %edx,%eax
  800102:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800107:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80010c:	85 db                	test   %ebx,%ebx
  80010e:	7e 07                	jle    800117 <libmain+0x36>
		binaryname = argv[0];
  800110:	8b 06                	mov    (%esi),%eax
  800112:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800117:	83 ec 08             	sub    $0x8,%esp
  80011a:	56                   	push   %esi
  80011b:	53                   	push   %ebx
  80011c:	e8 ab ff ff ff       	call   8000cc <umain>

	// exit gracefully
	exit();
  800121:	e8 0a 00 00 00       	call   800130 <exit>
}
  800126:	83 c4 10             	add    $0x10,%esp
  800129:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80012c:	5b                   	pop    %ebx
  80012d:	5e                   	pop    %esi
  80012e:	5d                   	pop    %ebp
  80012f:	c3                   	ret    

00800130 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800130:	55                   	push   %ebp
  800131:	89 e5                	mov    %esp,%ebp
  800133:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800136:	6a 00                	push   $0x0
  800138:	e8 60 09 00 00       	call   800a9d <sys_env_destroy>
}
  80013d:	83 c4 10             	add    $0x10,%esp
  800140:	c9                   	leave  
  800141:	c3                   	ret    

00800142 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800142:	55                   	push   %ebp
  800143:	89 e5                	mov    %esp,%ebp
  800145:	53                   	push   %ebx
  800146:	83 ec 04             	sub    $0x4,%esp
  800149:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80014c:	8b 13                	mov    (%ebx),%edx
  80014e:	8d 42 01             	lea    0x1(%edx),%eax
  800151:	89 03                	mov    %eax,(%ebx)
  800153:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800156:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80015a:	3d ff 00 00 00       	cmp    $0xff,%eax
  80015f:	75 1a                	jne    80017b <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800161:	83 ec 08             	sub    $0x8,%esp
  800164:	68 ff 00 00 00       	push   $0xff
  800169:	8d 43 08             	lea    0x8(%ebx),%eax
  80016c:	50                   	push   %eax
  80016d:	e8 ee 08 00 00       	call   800a60 <sys_cputs>
		b->idx = 0;
  800172:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800178:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80017b:	ff 43 04             	incl   0x4(%ebx)
}
  80017e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800181:	c9                   	leave  
  800182:	c3                   	ret    

00800183 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800183:	55                   	push   %ebp
  800184:	89 e5                	mov    %esp,%ebp
  800186:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80018c:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800193:	00 00 00 
	b.cnt = 0;
  800196:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80019d:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001a0:	ff 75 0c             	pushl  0xc(%ebp)
  8001a3:	ff 75 08             	pushl  0x8(%ebp)
  8001a6:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001ac:	50                   	push   %eax
  8001ad:	68 42 01 80 00       	push   $0x800142
  8001b2:	e8 51 01 00 00       	call   800308 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001b7:	83 c4 08             	add    $0x8,%esp
  8001ba:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001c0:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001c6:	50                   	push   %eax
  8001c7:	e8 94 08 00 00       	call   800a60 <sys_cputs>

	return b.cnt;
}
  8001cc:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001d2:	c9                   	leave  
  8001d3:	c3                   	ret    

008001d4 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001d4:	55                   	push   %ebp
  8001d5:	89 e5                	mov    %esp,%ebp
  8001d7:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001da:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8001dd:	50                   	push   %eax
  8001de:	ff 75 08             	pushl  0x8(%ebp)
  8001e1:	e8 9d ff ff ff       	call   800183 <vcprintf>
	va_end(ap);

	return cnt;
}
  8001e6:	c9                   	leave  
  8001e7:	c3                   	ret    

008001e8 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001e8:	55                   	push   %ebp
  8001e9:	89 e5                	mov    %esp,%ebp
  8001eb:	57                   	push   %edi
  8001ec:	56                   	push   %esi
  8001ed:	53                   	push   %ebx
  8001ee:	83 ec 1c             	sub    $0x1c,%esp
  8001f1:	89 c7                	mov    %eax,%edi
  8001f3:	89 d6                	mov    %edx,%esi
  8001f5:	8b 45 08             	mov    0x8(%ebp),%eax
  8001f8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001fb:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001fe:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800201:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800204:	bb 00 00 00 00       	mov    $0x0,%ebx
  800209:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80020c:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80020f:	39 d3                	cmp    %edx,%ebx
  800211:	72 05                	jb     800218 <printnum+0x30>
  800213:	39 45 10             	cmp    %eax,0x10(%ebp)
  800216:	77 45                	ja     80025d <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800218:	83 ec 0c             	sub    $0xc,%esp
  80021b:	ff 75 18             	pushl  0x18(%ebp)
  80021e:	8b 45 14             	mov    0x14(%ebp),%eax
  800221:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800224:	53                   	push   %ebx
  800225:	ff 75 10             	pushl  0x10(%ebp)
  800228:	83 ec 08             	sub    $0x8,%esp
  80022b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80022e:	ff 75 e0             	pushl  -0x20(%ebp)
  800231:	ff 75 dc             	pushl  -0x24(%ebp)
  800234:	ff 75 d8             	pushl  -0x28(%ebp)
  800237:	e8 8c 0e 00 00       	call   8010c8 <__udivdi3>
  80023c:	83 c4 18             	add    $0x18,%esp
  80023f:	52                   	push   %edx
  800240:	50                   	push   %eax
  800241:	89 f2                	mov    %esi,%edx
  800243:	89 f8                	mov    %edi,%eax
  800245:	e8 9e ff ff ff       	call   8001e8 <printnum>
  80024a:	83 c4 20             	add    $0x20,%esp
  80024d:	eb 16                	jmp    800265 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80024f:	83 ec 08             	sub    $0x8,%esp
  800252:	56                   	push   %esi
  800253:	ff 75 18             	pushl  0x18(%ebp)
  800256:	ff d7                	call   *%edi
  800258:	83 c4 10             	add    $0x10,%esp
  80025b:	eb 03                	jmp    800260 <printnum+0x78>
  80025d:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800260:	4b                   	dec    %ebx
  800261:	85 db                	test   %ebx,%ebx
  800263:	7f ea                	jg     80024f <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800265:	83 ec 08             	sub    $0x8,%esp
  800268:	56                   	push   %esi
  800269:	83 ec 04             	sub    $0x4,%esp
  80026c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80026f:	ff 75 e0             	pushl  -0x20(%ebp)
  800272:	ff 75 dc             	pushl  -0x24(%ebp)
  800275:	ff 75 d8             	pushl  -0x28(%ebp)
  800278:	e8 5b 0f 00 00       	call   8011d8 <__umoddi3>
  80027d:	83 c4 14             	add    $0x14,%esp
  800280:	0f be 80 60 13 80 00 	movsbl 0x801360(%eax),%eax
  800287:	50                   	push   %eax
  800288:	ff d7                	call   *%edi
}
  80028a:	83 c4 10             	add    $0x10,%esp
  80028d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800290:	5b                   	pop    %ebx
  800291:	5e                   	pop    %esi
  800292:	5f                   	pop    %edi
  800293:	5d                   	pop    %ebp
  800294:	c3                   	ret    

00800295 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800295:	55                   	push   %ebp
  800296:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800298:	83 fa 01             	cmp    $0x1,%edx
  80029b:	7e 0e                	jle    8002ab <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80029d:	8b 10                	mov    (%eax),%edx
  80029f:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002a2:	89 08                	mov    %ecx,(%eax)
  8002a4:	8b 02                	mov    (%edx),%eax
  8002a6:	8b 52 04             	mov    0x4(%edx),%edx
  8002a9:	eb 22                	jmp    8002cd <getuint+0x38>
	else if (lflag)
  8002ab:	85 d2                	test   %edx,%edx
  8002ad:	74 10                	je     8002bf <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002af:	8b 10                	mov    (%eax),%edx
  8002b1:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002b4:	89 08                	mov    %ecx,(%eax)
  8002b6:	8b 02                	mov    (%edx),%eax
  8002b8:	ba 00 00 00 00       	mov    $0x0,%edx
  8002bd:	eb 0e                	jmp    8002cd <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002bf:	8b 10                	mov    (%eax),%edx
  8002c1:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002c4:	89 08                	mov    %ecx,(%eax)
  8002c6:	8b 02                	mov    (%edx),%eax
  8002c8:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002cd:	5d                   	pop    %ebp
  8002ce:	c3                   	ret    

008002cf <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002cf:	55                   	push   %ebp
  8002d0:	89 e5                	mov    %esp,%ebp
  8002d2:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002d5:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8002d8:	8b 10                	mov    (%eax),%edx
  8002da:	3b 50 04             	cmp    0x4(%eax),%edx
  8002dd:	73 0a                	jae    8002e9 <sprintputch+0x1a>
		*b->buf++ = ch;
  8002df:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002e2:	89 08                	mov    %ecx,(%eax)
  8002e4:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e7:	88 02                	mov    %al,(%edx)
}
  8002e9:	5d                   	pop    %ebp
  8002ea:	c3                   	ret    

008002eb <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002eb:	55                   	push   %ebp
  8002ec:	89 e5                	mov    %esp,%ebp
  8002ee:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002f1:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002f4:	50                   	push   %eax
  8002f5:	ff 75 10             	pushl  0x10(%ebp)
  8002f8:	ff 75 0c             	pushl  0xc(%ebp)
  8002fb:	ff 75 08             	pushl  0x8(%ebp)
  8002fe:	e8 05 00 00 00       	call   800308 <vprintfmt>
	va_end(ap);
}
  800303:	83 c4 10             	add    $0x10,%esp
  800306:	c9                   	leave  
  800307:	c3                   	ret    

00800308 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800308:	55                   	push   %ebp
  800309:	89 e5                	mov    %esp,%ebp
  80030b:	57                   	push   %edi
  80030c:	56                   	push   %esi
  80030d:	53                   	push   %ebx
  80030e:	83 ec 2c             	sub    $0x2c,%esp
  800311:	8b 75 08             	mov    0x8(%ebp),%esi
  800314:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800317:	8b 7d 10             	mov    0x10(%ebp),%edi
  80031a:	eb 12                	jmp    80032e <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80031c:	85 c0                	test   %eax,%eax
  80031e:	0f 84 68 03 00 00    	je     80068c <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800324:	83 ec 08             	sub    $0x8,%esp
  800327:	53                   	push   %ebx
  800328:	50                   	push   %eax
  800329:	ff d6                	call   *%esi
  80032b:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80032e:	47                   	inc    %edi
  80032f:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800333:	83 f8 25             	cmp    $0x25,%eax
  800336:	75 e4                	jne    80031c <vprintfmt+0x14>
  800338:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80033c:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800343:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80034a:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800351:	ba 00 00 00 00       	mov    $0x0,%edx
  800356:	eb 07                	jmp    80035f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800358:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80035b:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80035f:	8d 47 01             	lea    0x1(%edi),%eax
  800362:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800365:	0f b6 0f             	movzbl (%edi),%ecx
  800368:	8a 07                	mov    (%edi),%al
  80036a:	83 e8 23             	sub    $0x23,%eax
  80036d:	3c 55                	cmp    $0x55,%al
  80036f:	0f 87 fe 02 00 00    	ja     800673 <vprintfmt+0x36b>
  800375:	0f b6 c0             	movzbl %al,%eax
  800378:	ff 24 85 a0 14 80 00 	jmp    *0x8014a0(,%eax,4)
  80037f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800382:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800386:	eb d7                	jmp    80035f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800388:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80038b:	b8 00 00 00 00       	mov    $0x0,%eax
  800390:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800393:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800396:	01 c0                	add    %eax,%eax
  800398:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80039c:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80039f:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003a2:	83 fa 09             	cmp    $0x9,%edx
  8003a5:	77 34                	ja     8003db <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003a7:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003a8:	eb e9                	jmp    800393 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003aa:	8b 45 14             	mov    0x14(%ebp),%eax
  8003ad:	8d 48 04             	lea    0x4(%eax),%ecx
  8003b0:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003b3:	8b 00                	mov    (%eax),%eax
  8003b5:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003bb:	eb 24                	jmp    8003e1 <vprintfmt+0xd9>
  8003bd:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003c1:	79 07                	jns    8003ca <vprintfmt+0xc2>
  8003c3:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ca:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003cd:	eb 90                	jmp    80035f <vprintfmt+0x57>
  8003cf:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003d2:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8003d9:	eb 84                	jmp    80035f <vprintfmt+0x57>
  8003db:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003de:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003e1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003e5:	0f 89 74 ff ff ff    	jns    80035f <vprintfmt+0x57>
				width = precision, precision = -1;
  8003eb:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003ee:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003f1:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003f8:	e9 62 ff ff ff       	jmp    80035f <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003fd:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800401:	e9 59 ff ff ff       	jmp    80035f <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800406:	8b 45 14             	mov    0x14(%ebp),%eax
  800409:	8d 50 04             	lea    0x4(%eax),%edx
  80040c:	89 55 14             	mov    %edx,0x14(%ebp)
  80040f:	83 ec 08             	sub    $0x8,%esp
  800412:	53                   	push   %ebx
  800413:	ff 30                	pushl  (%eax)
  800415:	ff d6                	call   *%esi
			break;
  800417:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80041a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80041d:	e9 0c ff ff ff       	jmp    80032e <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800422:	8b 45 14             	mov    0x14(%ebp),%eax
  800425:	8d 50 04             	lea    0x4(%eax),%edx
  800428:	89 55 14             	mov    %edx,0x14(%ebp)
  80042b:	8b 00                	mov    (%eax),%eax
  80042d:	85 c0                	test   %eax,%eax
  80042f:	79 02                	jns    800433 <vprintfmt+0x12b>
  800431:	f7 d8                	neg    %eax
  800433:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800435:	83 f8 0f             	cmp    $0xf,%eax
  800438:	7f 0b                	jg     800445 <vprintfmt+0x13d>
  80043a:	8b 04 85 00 16 80 00 	mov    0x801600(,%eax,4),%eax
  800441:	85 c0                	test   %eax,%eax
  800443:	75 18                	jne    80045d <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800445:	52                   	push   %edx
  800446:	68 78 13 80 00       	push   $0x801378
  80044b:	53                   	push   %ebx
  80044c:	56                   	push   %esi
  80044d:	e8 99 fe ff ff       	call   8002eb <printfmt>
  800452:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800455:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800458:	e9 d1 fe ff ff       	jmp    80032e <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80045d:	50                   	push   %eax
  80045e:	68 81 13 80 00       	push   $0x801381
  800463:	53                   	push   %ebx
  800464:	56                   	push   %esi
  800465:	e8 81 fe ff ff       	call   8002eb <printfmt>
  80046a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80046d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800470:	e9 b9 fe ff ff       	jmp    80032e <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800475:	8b 45 14             	mov    0x14(%ebp),%eax
  800478:	8d 50 04             	lea    0x4(%eax),%edx
  80047b:	89 55 14             	mov    %edx,0x14(%ebp)
  80047e:	8b 38                	mov    (%eax),%edi
  800480:	85 ff                	test   %edi,%edi
  800482:	75 05                	jne    800489 <vprintfmt+0x181>
				p = "(null)";
  800484:	bf 71 13 80 00       	mov    $0x801371,%edi
			if (width > 0 && padc != '-')
  800489:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80048d:	0f 8e 90 00 00 00    	jle    800523 <vprintfmt+0x21b>
  800493:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800497:	0f 84 8e 00 00 00    	je     80052b <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80049d:	83 ec 08             	sub    $0x8,%esp
  8004a0:	ff 75 d0             	pushl  -0x30(%ebp)
  8004a3:	57                   	push   %edi
  8004a4:	e8 70 02 00 00       	call   800719 <strnlen>
  8004a9:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004ac:	29 c1                	sub    %eax,%ecx
  8004ae:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004b1:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004b4:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004b8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004bb:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004be:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004c0:	eb 0d                	jmp    8004cf <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004c2:	83 ec 08             	sub    $0x8,%esp
  8004c5:	53                   	push   %ebx
  8004c6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004c9:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004cb:	4f                   	dec    %edi
  8004cc:	83 c4 10             	add    $0x10,%esp
  8004cf:	85 ff                	test   %edi,%edi
  8004d1:	7f ef                	jg     8004c2 <vprintfmt+0x1ba>
  8004d3:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8004d6:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004d9:	89 c8                	mov    %ecx,%eax
  8004db:	85 c9                	test   %ecx,%ecx
  8004dd:	79 05                	jns    8004e4 <vprintfmt+0x1dc>
  8004df:	b8 00 00 00 00       	mov    $0x0,%eax
  8004e4:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004e7:	29 c1                	sub    %eax,%ecx
  8004e9:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004ec:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ef:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004f2:	eb 3d                	jmp    800531 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004f4:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004f8:	74 19                	je     800513 <vprintfmt+0x20b>
  8004fa:	0f be c0             	movsbl %al,%eax
  8004fd:	83 e8 20             	sub    $0x20,%eax
  800500:	83 f8 5e             	cmp    $0x5e,%eax
  800503:	76 0e                	jbe    800513 <vprintfmt+0x20b>
					putch('?', putdat);
  800505:	83 ec 08             	sub    $0x8,%esp
  800508:	53                   	push   %ebx
  800509:	6a 3f                	push   $0x3f
  80050b:	ff 55 08             	call   *0x8(%ebp)
  80050e:	83 c4 10             	add    $0x10,%esp
  800511:	eb 0b                	jmp    80051e <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800513:	83 ec 08             	sub    $0x8,%esp
  800516:	53                   	push   %ebx
  800517:	52                   	push   %edx
  800518:	ff 55 08             	call   *0x8(%ebp)
  80051b:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80051e:	ff 4d e4             	decl   -0x1c(%ebp)
  800521:	eb 0e                	jmp    800531 <vprintfmt+0x229>
  800523:	89 75 08             	mov    %esi,0x8(%ebp)
  800526:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800529:	eb 06                	jmp    800531 <vprintfmt+0x229>
  80052b:	89 75 08             	mov    %esi,0x8(%ebp)
  80052e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800531:	47                   	inc    %edi
  800532:	8a 47 ff             	mov    -0x1(%edi),%al
  800535:	0f be d0             	movsbl %al,%edx
  800538:	85 d2                	test   %edx,%edx
  80053a:	74 1d                	je     800559 <vprintfmt+0x251>
  80053c:	85 f6                	test   %esi,%esi
  80053e:	78 b4                	js     8004f4 <vprintfmt+0x1ec>
  800540:	4e                   	dec    %esi
  800541:	79 b1                	jns    8004f4 <vprintfmt+0x1ec>
  800543:	8b 75 08             	mov    0x8(%ebp),%esi
  800546:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800549:	eb 14                	jmp    80055f <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80054b:	83 ec 08             	sub    $0x8,%esp
  80054e:	53                   	push   %ebx
  80054f:	6a 20                	push   $0x20
  800551:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800553:	4f                   	dec    %edi
  800554:	83 c4 10             	add    $0x10,%esp
  800557:	eb 06                	jmp    80055f <vprintfmt+0x257>
  800559:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80055c:	8b 75 08             	mov    0x8(%ebp),%esi
  80055f:	85 ff                	test   %edi,%edi
  800561:	7f e8                	jg     80054b <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800563:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800566:	e9 c3 fd ff ff       	jmp    80032e <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80056b:	83 fa 01             	cmp    $0x1,%edx
  80056e:	7e 16                	jle    800586 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800570:	8b 45 14             	mov    0x14(%ebp),%eax
  800573:	8d 50 08             	lea    0x8(%eax),%edx
  800576:	89 55 14             	mov    %edx,0x14(%ebp)
  800579:	8b 50 04             	mov    0x4(%eax),%edx
  80057c:	8b 00                	mov    (%eax),%eax
  80057e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800581:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800584:	eb 32                	jmp    8005b8 <vprintfmt+0x2b0>
	else if (lflag)
  800586:	85 d2                	test   %edx,%edx
  800588:	74 18                	je     8005a2 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80058a:	8b 45 14             	mov    0x14(%ebp),%eax
  80058d:	8d 50 04             	lea    0x4(%eax),%edx
  800590:	89 55 14             	mov    %edx,0x14(%ebp)
  800593:	8b 00                	mov    (%eax),%eax
  800595:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800598:	89 c1                	mov    %eax,%ecx
  80059a:	c1 f9 1f             	sar    $0x1f,%ecx
  80059d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005a0:	eb 16                	jmp    8005b8 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005a2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a5:	8d 50 04             	lea    0x4(%eax),%edx
  8005a8:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ab:	8b 00                	mov    (%eax),%eax
  8005ad:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005b0:	89 c1                	mov    %eax,%ecx
  8005b2:	c1 f9 1f             	sar    $0x1f,%ecx
  8005b5:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005b8:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005bb:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005be:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005c2:	79 76                	jns    80063a <vprintfmt+0x332>
				putch('-', putdat);
  8005c4:	83 ec 08             	sub    $0x8,%esp
  8005c7:	53                   	push   %ebx
  8005c8:	6a 2d                	push   $0x2d
  8005ca:	ff d6                	call   *%esi
				num = -(long long) num;
  8005cc:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005cf:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005d2:	f7 d8                	neg    %eax
  8005d4:	83 d2 00             	adc    $0x0,%edx
  8005d7:	f7 da                	neg    %edx
  8005d9:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8005dc:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005e1:	eb 5c                	jmp    80063f <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005e3:	8d 45 14             	lea    0x14(%ebp),%eax
  8005e6:	e8 aa fc ff ff       	call   800295 <getuint>
			base = 10;
  8005eb:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005f0:	eb 4d                	jmp    80063f <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005f2:	8d 45 14             	lea    0x14(%ebp),%eax
  8005f5:	e8 9b fc ff ff       	call   800295 <getuint>
			base = 8;
  8005fa:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005ff:	eb 3e                	jmp    80063f <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800601:	83 ec 08             	sub    $0x8,%esp
  800604:	53                   	push   %ebx
  800605:	6a 30                	push   $0x30
  800607:	ff d6                	call   *%esi
			putch('x', putdat);
  800609:	83 c4 08             	add    $0x8,%esp
  80060c:	53                   	push   %ebx
  80060d:	6a 78                	push   $0x78
  80060f:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800611:	8b 45 14             	mov    0x14(%ebp),%eax
  800614:	8d 50 04             	lea    0x4(%eax),%edx
  800617:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80061a:	8b 00                	mov    (%eax),%eax
  80061c:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800621:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800624:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800629:	eb 14                	jmp    80063f <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80062b:	8d 45 14             	lea    0x14(%ebp),%eax
  80062e:	e8 62 fc ff ff       	call   800295 <getuint>
			base = 16;
  800633:	b9 10 00 00 00       	mov    $0x10,%ecx
  800638:	eb 05                	jmp    80063f <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80063a:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80063f:	83 ec 0c             	sub    $0xc,%esp
  800642:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800646:	57                   	push   %edi
  800647:	ff 75 e4             	pushl  -0x1c(%ebp)
  80064a:	51                   	push   %ecx
  80064b:	52                   	push   %edx
  80064c:	50                   	push   %eax
  80064d:	89 da                	mov    %ebx,%edx
  80064f:	89 f0                	mov    %esi,%eax
  800651:	e8 92 fb ff ff       	call   8001e8 <printnum>
			break;
  800656:	83 c4 20             	add    $0x20,%esp
  800659:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80065c:	e9 cd fc ff ff       	jmp    80032e <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800661:	83 ec 08             	sub    $0x8,%esp
  800664:	53                   	push   %ebx
  800665:	51                   	push   %ecx
  800666:	ff d6                	call   *%esi
			break;
  800668:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80066b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80066e:	e9 bb fc ff ff       	jmp    80032e <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800673:	83 ec 08             	sub    $0x8,%esp
  800676:	53                   	push   %ebx
  800677:	6a 25                	push   $0x25
  800679:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80067b:	83 c4 10             	add    $0x10,%esp
  80067e:	eb 01                	jmp    800681 <vprintfmt+0x379>
  800680:	4f                   	dec    %edi
  800681:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800685:	75 f9                	jne    800680 <vprintfmt+0x378>
  800687:	e9 a2 fc ff ff       	jmp    80032e <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80068c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80068f:	5b                   	pop    %ebx
  800690:	5e                   	pop    %esi
  800691:	5f                   	pop    %edi
  800692:	5d                   	pop    %ebp
  800693:	c3                   	ret    

00800694 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800694:	55                   	push   %ebp
  800695:	89 e5                	mov    %esp,%ebp
  800697:	83 ec 18             	sub    $0x18,%esp
  80069a:	8b 45 08             	mov    0x8(%ebp),%eax
  80069d:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006a0:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006a3:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006a7:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006aa:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006b1:	85 c0                	test   %eax,%eax
  8006b3:	74 26                	je     8006db <vsnprintf+0x47>
  8006b5:	85 d2                	test   %edx,%edx
  8006b7:	7e 29                	jle    8006e2 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006b9:	ff 75 14             	pushl  0x14(%ebp)
  8006bc:	ff 75 10             	pushl  0x10(%ebp)
  8006bf:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006c2:	50                   	push   %eax
  8006c3:	68 cf 02 80 00       	push   $0x8002cf
  8006c8:	e8 3b fc ff ff       	call   800308 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006cd:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006d0:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8006d6:	83 c4 10             	add    $0x10,%esp
  8006d9:	eb 0c                	jmp    8006e7 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8006db:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006e0:	eb 05                	jmp    8006e7 <vsnprintf+0x53>
  8006e2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8006e7:	c9                   	leave  
  8006e8:	c3                   	ret    

008006e9 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006e9:	55                   	push   %ebp
  8006ea:	89 e5                	mov    %esp,%ebp
  8006ec:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006ef:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006f2:	50                   	push   %eax
  8006f3:	ff 75 10             	pushl  0x10(%ebp)
  8006f6:	ff 75 0c             	pushl  0xc(%ebp)
  8006f9:	ff 75 08             	pushl  0x8(%ebp)
  8006fc:	e8 93 ff ff ff       	call   800694 <vsnprintf>
	va_end(ap);

	return rc;
}
  800701:	c9                   	leave  
  800702:	c3                   	ret    

00800703 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800703:	55                   	push   %ebp
  800704:	89 e5                	mov    %esp,%ebp
  800706:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800709:	b8 00 00 00 00       	mov    $0x0,%eax
  80070e:	eb 01                	jmp    800711 <strlen+0xe>
		n++;
  800710:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800711:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800715:	75 f9                	jne    800710 <strlen+0xd>
		n++;
	return n;
}
  800717:	5d                   	pop    %ebp
  800718:	c3                   	ret    

00800719 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800719:	55                   	push   %ebp
  80071a:	89 e5                	mov    %esp,%ebp
  80071c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80071f:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800722:	ba 00 00 00 00       	mov    $0x0,%edx
  800727:	eb 01                	jmp    80072a <strnlen+0x11>
		n++;
  800729:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80072a:	39 c2                	cmp    %eax,%edx
  80072c:	74 08                	je     800736 <strnlen+0x1d>
  80072e:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800732:	75 f5                	jne    800729 <strnlen+0x10>
  800734:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800736:	5d                   	pop    %ebp
  800737:	c3                   	ret    

00800738 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800738:	55                   	push   %ebp
  800739:	89 e5                	mov    %esp,%ebp
  80073b:	53                   	push   %ebx
  80073c:	8b 45 08             	mov    0x8(%ebp),%eax
  80073f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800742:	89 c2                	mov    %eax,%edx
  800744:	42                   	inc    %edx
  800745:	41                   	inc    %ecx
  800746:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800749:	88 5a ff             	mov    %bl,-0x1(%edx)
  80074c:	84 db                	test   %bl,%bl
  80074e:	75 f4                	jne    800744 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800750:	5b                   	pop    %ebx
  800751:	5d                   	pop    %ebp
  800752:	c3                   	ret    

00800753 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800753:	55                   	push   %ebp
  800754:	89 e5                	mov    %esp,%ebp
  800756:	53                   	push   %ebx
  800757:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80075a:	53                   	push   %ebx
  80075b:	e8 a3 ff ff ff       	call   800703 <strlen>
  800760:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800763:	ff 75 0c             	pushl  0xc(%ebp)
  800766:	01 d8                	add    %ebx,%eax
  800768:	50                   	push   %eax
  800769:	e8 ca ff ff ff       	call   800738 <strcpy>
	return dst;
}
  80076e:	89 d8                	mov    %ebx,%eax
  800770:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800773:	c9                   	leave  
  800774:	c3                   	ret    

00800775 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800775:	55                   	push   %ebp
  800776:	89 e5                	mov    %esp,%ebp
  800778:	56                   	push   %esi
  800779:	53                   	push   %ebx
  80077a:	8b 75 08             	mov    0x8(%ebp),%esi
  80077d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800780:	89 f3                	mov    %esi,%ebx
  800782:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800785:	89 f2                	mov    %esi,%edx
  800787:	eb 0c                	jmp    800795 <strncpy+0x20>
		*dst++ = *src;
  800789:	42                   	inc    %edx
  80078a:	8a 01                	mov    (%ecx),%al
  80078c:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80078f:	80 39 01             	cmpb   $0x1,(%ecx)
  800792:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800795:	39 da                	cmp    %ebx,%edx
  800797:	75 f0                	jne    800789 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800799:	89 f0                	mov    %esi,%eax
  80079b:	5b                   	pop    %ebx
  80079c:	5e                   	pop    %esi
  80079d:	5d                   	pop    %ebp
  80079e:	c3                   	ret    

0080079f <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80079f:	55                   	push   %ebp
  8007a0:	89 e5                	mov    %esp,%ebp
  8007a2:	56                   	push   %esi
  8007a3:	53                   	push   %ebx
  8007a4:	8b 75 08             	mov    0x8(%ebp),%esi
  8007a7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007aa:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007ad:	85 c0                	test   %eax,%eax
  8007af:	74 1e                	je     8007cf <strlcpy+0x30>
  8007b1:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007b5:	89 f2                	mov    %esi,%edx
  8007b7:	eb 05                	jmp    8007be <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007b9:	42                   	inc    %edx
  8007ba:	41                   	inc    %ecx
  8007bb:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007be:	39 c2                	cmp    %eax,%edx
  8007c0:	74 08                	je     8007ca <strlcpy+0x2b>
  8007c2:	8a 19                	mov    (%ecx),%bl
  8007c4:	84 db                	test   %bl,%bl
  8007c6:	75 f1                	jne    8007b9 <strlcpy+0x1a>
  8007c8:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007ca:	c6 00 00             	movb   $0x0,(%eax)
  8007cd:	eb 02                	jmp    8007d1 <strlcpy+0x32>
  8007cf:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007d1:	29 f0                	sub    %esi,%eax
}
  8007d3:	5b                   	pop    %ebx
  8007d4:	5e                   	pop    %esi
  8007d5:	5d                   	pop    %ebp
  8007d6:	c3                   	ret    

008007d7 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8007d7:	55                   	push   %ebp
  8007d8:	89 e5                	mov    %esp,%ebp
  8007da:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007dd:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007e0:	eb 02                	jmp    8007e4 <strcmp+0xd>
		p++, q++;
  8007e2:	41                   	inc    %ecx
  8007e3:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007e4:	8a 01                	mov    (%ecx),%al
  8007e6:	84 c0                	test   %al,%al
  8007e8:	74 04                	je     8007ee <strcmp+0x17>
  8007ea:	3a 02                	cmp    (%edx),%al
  8007ec:	74 f4                	je     8007e2 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007ee:	0f b6 c0             	movzbl %al,%eax
  8007f1:	0f b6 12             	movzbl (%edx),%edx
  8007f4:	29 d0                	sub    %edx,%eax
}
  8007f6:	5d                   	pop    %ebp
  8007f7:	c3                   	ret    

008007f8 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007f8:	55                   	push   %ebp
  8007f9:	89 e5                	mov    %esp,%ebp
  8007fb:	53                   	push   %ebx
  8007fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ff:	8b 55 0c             	mov    0xc(%ebp),%edx
  800802:	89 c3                	mov    %eax,%ebx
  800804:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800807:	eb 02                	jmp    80080b <strncmp+0x13>
		n--, p++, q++;
  800809:	40                   	inc    %eax
  80080a:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80080b:	39 d8                	cmp    %ebx,%eax
  80080d:	74 14                	je     800823 <strncmp+0x2b>
  80080f:	8a 08                	mov    (%eax),%cl
  800811:	84 c9                	test   %cl,%cl
  800813:	74 04                	je     800819 <strncmp+0x21>
  800815:	3a 0a                	cmp    (%edx),%cl
  800817:	74 f0                	je     800809 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800819:	0f b6 00             	movzbl (%eax),%eax
  80081c:	0f b6 12             	movzbl (%edx),%edx
  80081f:	29 d0                	sub    %edx,%eax
  800821:	eb 05                	jmp    800828 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800823:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800828:	5b                   	pop    %ebx
  800829:	5d                   	pop    %ebp
  80082a:	c3                   	ret    

0080082b <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80082b:	55                   	push   %ebp
  80082c:	89 e5                	mov    %esp,%ebp
  80082e:	8b 45 08             	mov    0x8(%ebp),%eax
  800831:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800834:	eb 05                	jmp    80083b <strchr+0x10>
		if (*s == c)
  800836:	38 ca                	cmp    %cl,%dl
  800838:	74 0c                	je     800846 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80083a:	40                   	inc    %eax
  80083b:	8a 10                	mov    (%eax),%dl
  80083d:	84 d2                	test   %dl,%dl
  80083f:	75 f5                	jne    800836 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800841:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800846:	5d                   	pop    %ebp
  800847:	c3                   	ret    

00800848 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800848:	55                   	push   %ebp
  800849:	89 e5                	mov    %esp,%ebp
  80084b:	8b 45 08             	mov    0x8(%ebp),%eax
  80084e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800851:	eb 05                	jmp    800858 <strfind+0x10>
		if (*s == c)
  800853:	38 ca                	cmp    %cl,%dl
  800855:	74 07                	je     80085e <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800857:	40                   	inc    %eax
  800858:	8a 10                	mov    (%eax),%dl
  80085a:	84 d2                	test   %dl,%dl
  80085c:	75 f5                	jne    800853 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80085e:	5d                   	pop    %ebp
  80085f:	c3                   	ret    

00800860 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800860:	55                   	push   %ebp
  800861:	89 e5                	mov    %esp,%ebp
  800863:	57                   	push   %edi
  800864:	56                   	push   %esi
  800865:	53                   	push   %ebx
  800866:	8b 7d 08             	mov    0x8(%ebp),%edi
  800869:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80086c:	85 c9                	test   %ecx,%ecx
  80086e:	74 36                	je     8008a6 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800870:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800876:	75 28                	jne    8008a0 <memset+0x40>
  800878:	f6 c1 03             	test   $0x3,%cl
  80087b:	75 23                	jne    8008a0 <memset+0x40>
		c &= 0xFF;
  80087d:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800881:	89 d3                	mov    %edx,%ebx
  800883:	c1 e3 08             	shl    $0x8,%ebx
  800886:	89 d6                	mov    %edx,%esi
  800888:	c1 e6 18             	shl    $0x18,%esi
  80088b:	89 d0                	mov    %edx,%eax
  80088d:	c1 e0 10             	shl    $0x10,%eax
  800890:	09 f0                	or     %esi,%eax
  800892:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800894:	89 d8                	mov    %ebx,%eax
  800896:	09 d0                	or     %edx,%eax
  800898:	c1 e9 02             	shr    $0x2,%ecx
  80089b:	fc                   	cld    
  80089c:	f3 ab                	rep stos %eax,%es:(%edi)
  80089e:	eb 06                	jmp    8008a6 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008a0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008a3:	fc                   	cld    
  8008a4:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008a6:	89 f8                	mov    %edi,%eax
  8008a8:	5b                   	pop    %ebx
  8008a9:	5e                   	pop    %esi
  8008aa:	5f                   	pop    %edi
  8008ab:	5d                   	pop    %ebp
  8008ac:	c3                   	ret    

008008ad <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008ad:	55                   	push   %ebp
  8008ae:	89 e5                	mov    %esp,%ebp
  8008b0:	57                   	push   %edi
  8008b1:	56                   	push   %esi
  8008b2:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b5:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008b8:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008bb:	39 c6                	cmp    %eax,%esi
  8008bd:	73 33                	jae    8008f2 <memmove+0x45>
  8008bf:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008c2:	39 d0                	cmp    %edx,%eax
  8008c4:	73 2c                	jae    8008f2 <memmove+0x45>
		s += n;
		d += n;
  8008c6:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008c9:	89 d6                	mov    %edx,%esi
  8008cb:	09 fe                	or     %edi,%esi
  8008cd:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008d3:	75 13                	jne    8008e8 <memmove+0x3b>
  8008d5:	f6 c1 03             	test   $0x3,%cl
  8008d8:	75 0e                	jne    8008e8 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8008da:	83 ef 04             	sub    $0x4,%edi
  8008dd:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008e0:	c1 e9 02             	shr    $0x2,%ecx
  8008e3:	fd                   	std    
  8008e4:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008e6:	eb 07                	jmp    8008ef <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008e8:	4f                   	dec    %edi
  8008e9:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008ec:	fd                   	std    
  8008ed:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008ef:	fc                   	cld    
  8008f0:	eb 1d                	jmp    80090f <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008f2:	89 f2                	mov    %esi,%edx
  8008f4:	09 c2                	or     %eax,%edx
  8008f6:	f6 c2 03             	test   $0x3,%dl
  8008f9:	75 0f                	jne    80090a <memmove+0x5d>
  8008fb:	f6 c1 03             	test   $0x3,%cl
  8008fe:	75 0a                	jne    80090a <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800900:	c1 e9 02             	shr    $0x2,%ecx
  800903:	89 c7                	mov    %eax,%edi
  800905:	fc                   	cld    
  800906:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800908:	eb 05                	jmp    80090f <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  80090a:	89 c7                	mov    %eax,%edi
  80090c:	fc                   	cld    
  80090d:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80090f:	5e                   	pop    %esi
  800910:	5f                   	pop    %edi
  800911:	5d                   	pop    %ebp
  800912:	c3                   	ret    

00800913 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800913:	55                   	push   %ebp
  800914:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800916:	ff 75 10             	pushl  0x10(%ebp)
  800919:	ff 75 0c             	pushl  0xc(%ebp)
  80091c:	ff 75 08             	pushl  0x8(%ebp)
  80091f:	e8 89 ff ff ff       	call   8008ad <memmove>
}
  800924:	c9                   	leave  
  800925:	c3                   	ret    

00800926 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800926:	55                   	push   %ebp
  800927:	89 e5                	mov    %esp,%ebp
  800929:	56                   	push   %esi
  80092a:	53                   	push   %ebx
  80092b:	8b 45 08             	mov    0x8(%ebp),%eax
  80092e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800931:	89 c6                	mov    %eax,%esi
  800933:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800936:	eb 14                	jmp    80094c <memcmp+0x26>
		if (*s1 != *s2)
  800938:	8a 08                	mov    (%eax),%cl
  80093a:	8a 1a                	mov    (%edx),%bl
  80093c:	38 d9                	cmp    %bl,%cl
  80093e:	74 0a                	je     80094a <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800940:	0f b6 c1             	movzbl %cl,%eax
  800943:	0f b6 db             	movzbl %bl,%ebx
  800946:	29 d8                	sub    %ebx,%eax
  800948:	eb 0b                	jmp    800955 <memcmp+0x2f>
		s1++, s2++;
  80094a:	40                   	inc    %eax
  80094b:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80094c:	39 f0                	cmp    %esi,%eax
  80094e:	75 e8                	jne    800938 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800950:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800955:	5b                   	pop    %ebx
  800956:	5e                   	pop    %esi
  800957:	5d                   	pop    %ebp
  800958:	c3                   	ret    

00800959 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800959:	55                   	push   %ebp
  80095a:	89 e5                	mov    %esp,%ebp
  80095c:	53                   	push   %ebx
  80095d:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800960:	89 c1                	mov    %eax,%ecx
  800962:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800965:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800969:	eb 08                	jmp    800973 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80096b:	0f b6 10             	movzbl (%eax),%edx
  80096e:	39 da                	cmp    %ebx,%edx
  800970:	74 05                	je     800977 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800972:	40                   	inc    %eax
  800973:	39 c8                	cmp    %ecx,%eax
  800975:	72 f4                	jb     80096b <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800977:	5b                   	pop    %ebx
  800978:	5d                   	pop    %ebp
  800979:	c3                   	ret    

0080097a <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80097a:	55                   	push   %ebp
  80097b:	89 e5                	mov    %esp,%ebp
  80097d:	57                   	push   %edi
  80097e:	56                   	push   %esi
  80097f:	53                   	push   %ebx
  800980:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800983:	eb 01                	jmp    800986 <strtol+0xc>
		s++;
  800985:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800986:	8a 01                	mov    (%ecx),%al
  800988:	3c 20                	cmp    $0x20,%al
  80098a:	74 f9                	je     800985 <strtol+0xb>
  80098c:	3c 09                	cmp    $0x9,%al
  80098e:	74 f5                	je     800985 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800990:	3c 2b                	cmp    $0x2b,%al
  800992:	75 08                	jne    80099c <strtol+0x22>
		s++;
  800994:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800995:	bf 00 00 00 00       	mov    $0x0,%edi
  80099a:	eb 11                	jmp    8009ad <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80099c:	3c 2d                	cmp    $0x2d,%al
  80099e:	75 08                	jne    8009a8 <strtol+0x2e>
		s++, neg = 1;
  8009a0:	41                   	inc    %ecx
  8009a1:	bf 01 00 00 00       	mov    $0x1,%edi
  8009a6:	eb 05                	jmp    8009ad <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009a8:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009ad:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009b1:	0f 84 87 00 00 00    	je     800a3e <strtol+0xc4>
  8009b7:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009bb:	75 27                	jne    8009e4 <strtol+0x6a>
  8009bd:	80 39 30             	cmpb   $0x30,(%ecx)
  8009c0:	75 22                	jne    8009e4 <strtol+0x6a>
  8009c2:	e9 88 00 00 00       	jmp    800a4f <strtol+0xd5>
		s += 2, base = 16;
  8009c7:	83 c1 02             	add    $0x2,%ecx
  8009ca:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009d1:	eb 11                	jmp    8009e4 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009d3:	41                   	inc    %ecx
  8009d4:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8009db:	eb 07                	jmp    8009e4 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8009dd:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009e4:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009e9:	8a 11                	mov    (%ecx),%dl
  8009eb:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009ee:	80 fb 09             	cmp    $0x9,%bl
  8009f1:	77 08                	ja     8009fb <strtol+0x81>
			dig = *s - '0';
  8009f3:	0f be d2             	movsbl %dl,%edx
  8009f6:	83 ea 30             	sub    $0x30,%edx
  8009f9:	eb 22                	jmp    800a1d <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009fb:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009fe:	89 f3                	mov    %esi,%ebx
  800a00:	80 fb 19             	cmp    $0x19,%bl
  800a03:	77 08                	ja     800a0d <strtol+0x93>
			dig = *s - 'a' + 10;
  800a05:	0f be d2             	movsbl %dl,%edx
  800a08:	83 ea 57             	sub    $0x57,%edx
  800a0b:	eb 10                	jmp    800a1d <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a0d:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a10:	89 f3                	mov    %esi,%ebx
  800a12:	80 fb 19             	cmp    $0x19,%bl
  800a15:	77 14                	ja     800a2b <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a17:	0f be d2             	movsbl %dl,%edx
  800a1a:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a1d:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a20:	7d 09                	jge    800a2b <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a22:	41                   	inc    %ecx
  800a23:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a27:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a29:	eb be                	jmp    8009e9 <strtol+0x6f>

	if (endptr)
  800a2b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a2f:	74 05                	je     800a36 <strtol+0xbc>
		*endptr = (char *) s;
  800a31:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a34:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a36:	85 ff                	test   %edi,%edi
  800a38:	74 21                	je     800a5b <strtol+0xe1>
  800a3a:	f7 d8                	neg    %eax
  800a3c:	eb 1d                	jmp    800a5b <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a3e:	80 39 30             	cmpb   $0x30,(%ecx)
  800a41:	75 9a                	jne    8009dd <strtol+0x63>
  800a43:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a47:	0f 84 7a ff ff ff    	je     8009c7 <strtol+0x4d>
  800a4d:	eb 84                	jmp    8009d3 <strtol+0x59>
  800a4f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a53:	0f 84 6e ff ff ff    	je     8009c7 <strtol+0x4d>
  800a59:	eb 89                	jmp    8009e4 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a5b:	5b                   	pop    %ebx
  800a5c:	5e                   	pop    %esi
  800a5d:	5f                   	pop    %edi
  800a5e:	5d                   	pop    %ebp
  800a5f:	c3                   	ret    

00800a60 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a60:	55                   	push   %ebp
  800a61:	89 e5                	mov    %esp,%ebp
  800a63:	57                   	push   %edi
  800a64:	56                   	push   %esi
  800a65:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a66:	b8 00 00 00 00       	mov    $0x0,%eax
  800a6b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a6e:	8b 55 08             	mov    0x8(%ebp),%edx
  800a71:	89 c3                	mov    %eax,%ebx
  800a73:	89 c7                	mov    %eax,%edi
  800a75:	89 c6                	mov    %eax,%esi
  800a77:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a79:	5b                   	pop    %ebx
  800a7a:	5e                   	pop    %esi
  800a7b:	5f                   	pop    %edi
  800a7c:	5d                   	pop    %ebp
  800a7d:	c3                   	ret    

00800a7e <sys_cgetc>:

int
sys_cgetc(void)
{
  800a7e:	55                   	push   %ebp
  800a7f:	89 e5                	mov    %esp,%ebp
  800a81:	57                   	push   %edi
  800a82:	56                   	push   %esi
  800a83:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a84:	ba 00 00 00 00       	mov    $0x0,%edx
  800a89:	b8 01 00 00 00       	mov    $0x1,%eax
  800a8e:	89 d1                	mov    %edx,%ecx
  800a90:	89 d3                	mov    %edx,%ebx
  800a92:	89 d7                	mov    %edx,%edi
  800a94:	89 d6                	mov    %edx,%esi
  800a96:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a98:	5b                   	pop    %ebx
  800a99:	5e                   	pop    %esi
  800a9a:	5f                   	pop    %edi
  800a9b:	5d                   	pop    %ebp
  800a9c:	c3                   	ret    

00800a9d <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a9d:	55                   	push   %ebp
  800a9e:	89 e5                	mov    %esp,%ebp
  800aa0:	57                   	push   %edi
  800aa1:	56                   	push   %esi
  800aa2:	53                   	push   %ebx
  800aa3:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aa6:	b9 00 00 00 00       	mov    $0x0,%ecx
  800aab:	b8 03 00 00 00       	mov    $0x3,%eax
  800ab0:	8b 55 08             	mov    0x8(%ebp),%edx
  800ab3:	89 cb                	mov    %ecx,%ebx
  800ab5:	89 cf                	mov    %ecx,%edi
  800ab7:	89 ce                	mov    %ecx,%esi
  800ab9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800abb:	85 c0                	test   %eax,%eax
  800abd:	7e 17                	jle    800ad6 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800abf:	83 ec 0c             	sub    $0xc,%esp
  800ac2:	50                   	push   %eax
  800ac3:	6a 03                	push   $0x3
  800ac5:	68 5f 16 80 00       	push   $0x80165f
  800aca:	6a 23                	push   $0x23
  800acc:	68 7c 16 80 00       	push   $0x80167c
  800ad1:	e8 2c 05 00 00       	call   801002 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800ad6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ad9:	5b                   	pop    %ebx
  800ada:	5e                   	pop    %esi
  800adb:	5f                   	pop    %edi
  800adc:	5d                   	pop    %ebp
  800add:	c3                   	ret    

00800ade <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800ade:	55                   	push   %ebp
  800adf:	89 e5                	mov    %esp,%ebp
  800ae1:	57                   	push   %edi
  800ae2:	56                   	push   %esi
  800ae3:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae4:	ba 00 00 00 00       	mov    $0x0,%edx
  800ae9:	b8 02 00 00 00       	mov    $0x2,%eax
  800aee:	89 d1                	mov    %edx,%ecx
  800af0:	89 d3                	mov    %edx,%ebx
  800af2:	89 d7                	mov    %edx,%edi
  800af4:	89 d6                	mov    %edx,%esi
  800af6:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800af8:	5b                   	pop    %ebx
  800af9:	5e                   	pop    %esi
  800afa:	5f                   	pop    %edi
  800afb:	5d                   	pop    %ebp
  800afc:	c3                   	ret    

00800afd <sys_yield>:

void
sys_yield(void)
{
  800afd:	55                   	push   %ebp
  800afe:	89 e5                	mov    %esp,%ebp
  800b00:	57                   	push   %edi
  800b01:	56                   	push   %esi
  800b02:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b03:	ba 00 00 00 00       	mov    $0x0,%edx
  800b08:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b0d:	89 d1                	mov    %edx,%ecx
  800b0f:	89 d3                	mov    %edx,%ebx
  800b11:	89 d7                	mov    %edx,%edi
  800b13:	89 d6                	mov    %edx,%esi
  800b15:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b17:	5b                   	pop    %ebx
  800b18:	5e                   	pop    %esi
  800b19:	5f                   	pop    %edi
  800b1a:	5d                   	pop    %ebp
  800b1b:	c3                   	ret    

00800b1c <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b1c:	55                   	push   %ebp
  800b1d:	89 e5                	mov    %esp,%ebp
  800b1f:	57                   	push   %edi
  800b20:	56                   	push   %esi
  800b21:	53                   	push   %ebx
  800b22:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b25:	be 00 00 00 00       	mov    $0x0,%esi
  800b2a:	b8 04 00 00 00       	mov    $0x4,%eax
  800b2f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b32:	8b 55 08             	mov    0x8(%ebp),%edx
  800b35:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b38:	89 f7                	mov    %esi,%edi
  800b3a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b3c:	85 c0                	test   %eax,%eax
  800b3e:	7e 17                	jle    800b57 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b40:	83 ec 0c             	sub    $0xc,%esp
  800b43:	50                   	push   %eax
  800b44:	6a 04                	push   $0x4
  800b46:	68 5f 16 80 00       	push   $0x80165f
  800b4b:	6a 23                	push   $0x23
  800b4d:	68 7c 16 80 00       	push   $0x80167c
  800b52:	e8 ab 04 00 00       	call   801002 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b57:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b5a:	5b                   	pop    %ebx
  800b5b:	5e                   	pop    %esi
  800b5c:	5f                   	pop    %edi
  800b5d:	5d                   	pop    %ebp
  800b5e:	c3                   	ret    

00800b5f <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b5f:	55                   	push   %ebp
  800b60:	89 e5                	mov    %esp,%ebp
  800b62:	57                   	push   %edi
  800b63:	56                   	push   %esi
  800b64:	53                   	push   %ebx
  800b65:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b68:	b8 05 00 00 00       	mov    $0x5,%eax
  800b6d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b70:	8b 55 08             	mov    0x8(%ebp),%edx
  800b73:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b76:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b79:	8b 75 18             	mov    0x18(%ebp),%esi
  800b7c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b7e:	85 c0                	test   %eax,%eax
  800b80:	7e 17                	jle    800b99 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b82:	83 ec 0c             	sub    $0xc,%esp
  800b85:	50                   	push   %eax
  800b86:	6a 05                	push   $0x5
  800b88:	68 5f 16 80 00       	push   $0x80165f
  800b8d:	6a 23                	push   $0x23
  800b8f:	68 7c 16 80 00       	push   $0x80167c
  800b94:	e8 69 04 00 00       	call   801002 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b99:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b9c:	5b                   	pop    %ebx
  800b9d:	5e                   	pop    %esi
  800b9e:	5f                   	pop    %edi
  800b9f:	5d                   	pop    %ebp
  800ba0:	c3                   	ret    

00800ba1 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800ba1:	55                   	push   %ebp
  800ba2:	89 e5                	mov    %esp,%ebp
  800ba4:	57                   	push   %edi
  800ba5:	56                   	push   %esi
  800ba6:	53                   	push   %ebx
  800ba7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800baa:	bb 00 00 00 00       	mov    $0x0,%ebx
  800baf:	b8 06 00 00 00       	mov    $0x6,%eax
  800bb4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bb7:	8b 55 08             	mov    0x8(%ebp),%edx
  800bba:	89 df                	mov    %ebx,%edi
  800bbc:	89 de                	mov    %ebx,%esi
  800bbe:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bc0:	85 c0                	test   %eax,%eax
  800bc2:	7e 17                	jle    800bdb <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bc4:	83 ec 0c             	sub    $0xc,%esp
  800bc7:	50                   	push   %eax
  800bc8:	6a 06                	push   $0x6
  800bca:	68 5f 16 80 00       	push   $0x80165f
  800bcf:	6a 23                	push   $0x23
  800bd1:	68 7c 16 80 00       	push   $0x80167c
  800bd6:	e8 27 04 00 00       	call   801002 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800bdb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bde:	5b                   	pop    %ebx
  800bdf:	5e                   	pop    %esi
  800be0:	5f                   	pop    %edi
  800be1:	5d                   	pop    %ebp
  800be2:	c3                   	ret    

00800be3 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800be3:	55                   	push   %ebp
  800be4:	89 e5                	mov    %esp,%ebp
  800be6:	57                   	push   %edi
  800be7:	56                   	push   %esi
  800be8:	53                   	push   %ebx
  800be9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bec:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bf1:	b8 08 00 00 00       	mov    $0x8,%eax
  800bf6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bf9:	8b 55 08             	mov    0x8(%ebp),%edx
  800bfc:	89 df                	mov    %ebx,%edi
  800bfe:	89 de                	mov    %ebx,%esi
  800c00:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c02:	85 c0                	test   %eax,%eax
  800c04:	7e 17                	jle    800c1d <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c06:	83 ec 0c             	sub    $0xc,%esp
  800c09:	50                   	push   %eax
  800c0a:	6a 08                	push   $0x8
  800c0c:	68 5f 16 80 00       	push   $0x80165f
  800c11:	6a 23                	push   $0x23
  800c13:	68 7c 16 80 00       	push   $0x80167c
  800c18:	e8 e5 03 00 00       	call   801002 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c1d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c20:	5b                   	pop    %ebx
  800c21:	5e                   	pop    %esi
  800c22:	5f                   	pop    %edi
  800c23:	5d                   	pop    %ebp
  800c24:	c3                   	ret    

00800c25 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c25:	55                   	push   %ebp
  800c26:	89 e5                	mov    %esp,%ebp
  800c28:	57                   	push   %edi
  800c29:	56                   	push   %esi
  800c2a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2b:	ba 00 00 00 00       	mov    $0x0,%edx
  800c30:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c35:	89 d1                	mov    %edx,%ecx
  800c37:	89 d3                	mov    %edx,%ebx
  800c39:	89 d7                	mov    %edx,%edi
  800c3b:	89 d6                	mov    %edx,%esi
  800c3d:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c3f:	5b                   	pop    %ebx
  800c40:	5e                   	pop    %esi
  800c41:	5f                   	pop    %edi
  800c42:	5d                   	pop    %ebp
  800c43:	c3                   	ret    

00800c44 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c44:	55                   	push   %ebp
  800c45:	89 e5                	mov    %esp,%ebp
  800c47:	57                   	push   %edi
  800c48:	56                   	push   %esi
  800c49:	53                   	push   %ebx
  800c4a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c4d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c52:	b8 09 00 00 00       	mov    $0x9,%eax
  800c57:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c5a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c5d:	89 df                	mov    %ebx,%edi
  800c5f:	89 de                	mov    %ebx,%esi
  800c61:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c63:	85 c0                	test   %eax,%eax
  800c65:	7e 17                	jle    800c7e <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c67:	83 ec 0c             	sub    $0xc,%esp
  800c6a:	50                   	push   %eax
  800c6b:	6a 09                	push   $0x9
  800c6d:	68 5f 16 80 00       	push   $0x80165f
  800c72:	6a 23                	push   $0x23
  800c74:	68 7c 16 80 00       	push   $0x80167c
  800c79:	e8 84 03 00 00       	call   801002 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c7e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c81:	5b                   	pop    %ebx
  800c82:	5e                   	pop    %esi
  800c83:	5f                   	pop    %edi
  800c84:	5d                   	pop    %ebp
  800c85:	c3                   	ret    

00800c86 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c86:	55                   	push   %ebp
  800c87:	89 e5                	mov    %esp,%ebp
  800c89:	57                   	push   %edi
  800c8a:	56                   	push   %esi
  800c8b:	53                   	push   %ebx
  800c8c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c8f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c94:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c99:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c9c:	8b 55 08             	mov    0x8(%ebp),%edx
  800c9f:	89 df                	mov    %ebx,%edi
  800ca1:	89 de                	mov    %ebx,%esi
  800ca3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ca5:	85 c0                	test   %eax,%eax
  800ca7:	7e 17                	jle    800cc0 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ca9:	83 ec 0c             	sub    $0xc,%esp
  800cac:	50                   	push   %eax
  800cad:	6a 0a                	push   $0xa
  800caf:	68 5f 16 80 00       	push   $0x80165f
  800cb4:	6a 23                	push   $0x23
  800cb6:	68 7c 16 80 00       	push   $0x80167c
  800cbb:	e8 42 03 00 00       	call   801002 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cc0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cc3:	5b                   	pop    %ebx
  800cc4:	5e                   	pop    %esi
  800cc5:	5f                   	pop    %edi
  800cc6:	5d                   	pop    %ebp
  800cc7:	c3                   	ret    

00800cc8 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800cc8:	55                   	push   %ebp
  800cc9:	89 e5                	mov    %esp,%ebp
  800ccb:	57                   	push   %edi
  800ccc:	56                   	push   %esi
  800ccd:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cce:	be 00 00 00 00       	mov    $0x0,%esi
  800cd3:	b8 0d 00 00 00       	mov    $0xd,%eax
  800cd8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cdb:	8b 55 08             	mov    0x8(%ebp),%edx
  800cde:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ce1:	8b 7d 14             	mov    0x14(%ebp),%edi
  800ce4:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800ce6:	5b                   	pop    %ebx
  800ce7:	5e                   	pop    %esi
  800ce8:	5f                   	pop    %edi
  800ce9:	5d                   	pop    %ebp
  800cea:	c3                   	ret    

00800ceb <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800ceb:	55                   	push   %ebp
  800cec:	89 e5                	mov    %esp,%ebp
  800cee:	57                   	push   %edi
  800cef:	56                   	push   %esi
  800cf0:	53                   	push   %ebx
  800cf1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cf4:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cf9:	b8 0e 00 00 00       	mov    $0xe,%eax
  800cfe:	8b 55 08             	mov    0x8(%ebp),%edx
  800d01:	89 cb                	mov    %ecx,%ebx
  800d03:	89 cf                	mov    %ecx,%edi
  800d05:	89 ce                	mov    %ecx,%esi
  800d07:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d09:	85 c0                	test   %eax,%eax
  800d0b:	7e 17                	jle    800d24 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d0d:	83 ec 0c             	sub    $0xc,%esp
  800d10:	50                   	push   %eax
  800d11:	6a 0e                	push   $0xe
  800d13:	68 5f 16 80 00       	push   $0x80165f
  800d18:	6a 23                	push   $0x23
  800d1a:	68 7c 16 80 00       	push   $0x80167c
  800d1f:	e8 de 02 00 00       	call   801002 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d24:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d27:	5b                   	pop    %ebx
  800d28:	5e                   	pop    %esi
  800d29:	5f                   	pop    %edi
  800d2a:	5d                   	pop    %ebp
  800d2b:	c3                   	ret    

00800d2c <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800d2c:	55                   	push   %ebp
  800d2d:	89 e5                	mov    %esp,%ebp
  800d2f:	53                   	push   %ebx
  800d30:	83 ec 04             	sub    $0x4,%esp
  800d33:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800d36:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800d38:	89 da                	mov    %ebx,%edx
  800d3a:	c1 ea 0c             	shr    $0xc,%edx
  800d3d:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800d44:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800d48:	74 05                	je     800d4f <pgfault+0x23>
  800d4a:	f6 c6 08             	test   $0x8,%dh
  800d4d:	75 14                	jne    800d63 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800d4f:	83 ec 04             	sub    $0x4,%esp
  800d52:	68 8c 16 80 00       	push   $0x80168c
  800d57:	6a 26                	push   $0x26
  800d59:	68 f0 16 80 00       	push   $0x8016f0
  800d5e:	e8 9f 02 00 00       	call   801002 <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800d63:	83 ec 04             	sub    $0x4,%esp
  800d66:	6a 07                	push   $0x7
  800d68:	68 00 f0 7f 00       	push   $0x7ff000
  800d6d:	6a 00                	push   $0x0
  800d6f:	e8 a8 fd ff ff       	call   800b1c <sys_page_alloc>
  800d74:	83 c4 10             	add    $0x10,%esp
  800d77:	85 c0                	test   %eax,%eax
  800d79:	74 14                	je     800d8f <pgfault+0x63>
        panic("pgfault: no phys mem");
  800d7b:	83 ec 04             	sub    $0x4,%esp
  800d7e:	68 fb 16 80 00       	push   $0x8016fb
  800d83:	6a 32                	push   $0x32
  800d85:	68 f0 16 80 00       	push   $0x8016f0
  800d8a:	e8 73 02 00 00       	call   801002 <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800d8f:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800d95:	83 ec 04             	sub    $0x4,%esp
  800d98:	68 00 10 00 00       	push   $0x1000
  800d9d:	53                   	push   %ebx
  800d9e:	68 00 f0 7f 00       	push   $0x7ff000
  800da3:	e8 05 fb ff ff       	call   8008ad <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800da8:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800daf:	53                   	push   %ebx
  800db0:	6a 00                	push   $0x0
  800db2:	68 00 f0 7f 00       	push   $0x7ff000
  800db7:	6a 00                	push   $0x0
  800db9:	e8 a1 fd ff ff       	call   800b5f <sys_page_map>
  800dbe:	83 c4 20             	add    $0x20,%esp
  800dc1:	85 c0                	test   %eax,%eax
  800dc3:	74 14                	je     800dd9 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800dc5:	83 ec 04             	sub    $0x4,%esp
  800dc8:	68 10 17 80 00       	push   $0x801710
  800dcd:	6a 3f                	push   $0x3f
  800dcf:	68 f0 16 80 00       	push   $0x8016f0
  800dd4:	e8 29 02 00 00       	call   801002 <_panic>


	//panic("pgfault not implemented");
}
  800dd9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ddc:	c9                   	leave  
  800ddd:	c3                   	ret    

00800dde <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800dde:	55                   	push   %ebp
  800ddf:	89 e5                	mov    %esp,%ebp
  800de1:	57                   	push   %edi
  800de2:	56                   	push   %esi
  800de3:	53                   	push   %ebx
  800de4:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800de7:	68 2c 0d 80 00       	push   $0x800d2c
  800dec:	e8 57 02 00 00       	call   801048 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800df1:	b8 07 00 00 00       	mov    $0x7,%eax
  800df6:	cd 30                	int    $0x30
  800df8:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800dfb:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800dfe:	83 c4 10             	add    $0x10,%esp
  800e01:	85 c0                	test   %eax,%eax
  800e03:	79 17                	jns    800e1c <fork+0x3e>
        panic("fork: cannot create child env");
  800e05:	83 ec 04             	sub    $0x4,%esp
  800e08:	68 23 17 80 00       	push   $0x801723
  800e0d:	68 97 00 00 00       	push   $0x97
  800e12:	68 f0 16 80 00       	push   $0x8016f0
  800e17:	e8 e6 01 00 00       	call   801002 <_panic>
    } else if (envid == 0) {
  800e1c:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800e20:	75 2a                	jne    800e4c <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800e22:	e8 b7 fc ff ff       	call   800ade <sys_getenvid>
  800e27:	25 ff 03 00 00       	and    $0x3ff,%eax
  800e2c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e33:	c1 e0 07             	shl    $0x7,%eax
  800e36:	29 d0                	sub    %edx,%eax
  800e38:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800e3d:	a3 04 20 80 00       	mov    %eax,0x802004
        return 0;
  800e42:	b8 00 00 00 00       	mov    $0x0,%eax
  800e47:	e9 94 01 00 00       	jmp    800fe0 <fork+0x202>
  800e4c:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800e53:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800e56:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800e5d:	a8 01                	test   $0x1,%al
  800e5f:	0f 84 0a 01 00 00    	je     800f6f <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800e65:	c1 e7 0a             	shl    $0xa,%edi
  800e68:	be 00 00 00 00       	mov    $0x0,%esi
  800e6d:	89 fb                	mov    %edi,%ebx
  800e6f:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800e71:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800e77:	75 34                	jne    800ead <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800e79:	83 ec 04             	sub    $0x4,%esp
  800e7c:	6a 07                	push   $0x7
  800e7e:	68 00 f0 bf ee       	push   $0xeebff000
  800e83:	ff 75 e4             	pushl  -0x1c(%ebp)
  800e86:	e8 91 fc ff ff       	call   800b1c <sys_page_alloc>
  800e8b:	83 c4 10             	add    $0x10,%esp
  800e8e:	85 c0                	test   %eax,%eax
  800e90:	0f 84 cc 00 00 00    	je     800f62 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800e96:	83 ec 04             	sub    $0x4,%esp
  800e99:	68 41 17 80 00       	push   $0x801741
  800e9e:	68 ad 00 00 00       	push   $0xad
  800ea3:	68 f0 16 80 00       	push   $0x8016f0
  800ea8:	e8 55 01 00 00       	call   801002 <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800ead:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800eb4:	a8 01                	test   $0x1,%al
  800eb6:	0f 84 a6 00 00 00    	je     800f62 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800ebc:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800ec3:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800ec6:	a9 02 08 00 00       	test   $0x802,%eax
  800ecb:	74 62                	je     800f2f <fork+0x151>
  800ecd:	f6 c4 04             	test   $0x4,%ah
  800ed0:	75 78                	jne    800f4a <fork+0x16c>
        if (sys_page_map(0,
  800ed2:	83 ec 0c             	sub    $0xc,%esp
  800ed5:	68 05 08 00 00       	push   $0x805
  800eda:	53                   	push   %ebx
  800edb:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ede:	53                   	push   %ebx
  800edf:	6a 00                	push   $0x0
  800ee1:	e8 79 fc ff ff       	call   800b5f <sys_page_map>
  800ee6:	83 c4 20             	add    $0x20,%esp
  800ee9:	85 c0                	test   %eax,%eax
  800eeb:	74 14                	je     800f01 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800eed:	83 ec 04             	sub    $0x4,%esp
  800ef0:	68 5c 17 80 00       	push   $0x80175c
  800ef5:	6a 64                	push   $0x64
  800ef7:	68 f0 16 80 00       	push   $0x8016f0
  800efc:	e8 01 01 00 00       	call   801002 <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800f01:	83 ec 0c             	sub    $0xc,%esp
  800f04:	68 05 08 00 00       	push   $0x805
  800f09:	53                   	push   %ebx
  800f0a:	6a 00                	push   $0x0
  800f0c:	53                   	push   %ebx
  800f0d:	6a 00                	push   $0x0
  800f0f:	e8 4b fc ff ff       	call   800b5f <sys_page_map>
  800f14:	83 c4 20             	add    $0x20,%esp
  800f17:	85 c0                	test   %eax,%eax
  800f19:	74 47                	je     800f62 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  800f1b:	83 ec 04             	sub    $0x4,%esp
  800f1e:	68 73 17 80 00       	push   $0x801773
  800f23:	6a 6d                	push   $0x6d
  800f25:	68 f0 16 80 00       	push   $0x8016f0
  800f2a:	e8 d3 00 00 00       	call   801002 <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  800f2f:	f6 c4 04             	test   $0x4,%ah
  800f32:	75 16                	jne    800f4a <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  800f34:	83 ec 0c             	sub    $0xc,%esp
  800f37:	6a 05                	push   $0x5
  800f39:	53                   	push   %ebx
  800f3a:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f3d:	53                   	push   %ebx
  800f3e:	6a 00                	push   $0x0
  800f40:	e8 1a fc ff ff       	call   800b5f <sys_page_map>
  800f45:	83 c4 20             	add    $0x20,%esp
  800f48:	eb 18                	jmp    800f62 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  800f4a:	83 ec 0c             	sub    $0xc,%esp
  800f4d:	25 07 0e 00 00       	and    $0xe07,%eax
  800f52:	50                   	push   %eax
  800f53:	53                   	push   %ebx
  800f54:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f57:	53                   	push   %ebx
  800f58:	6a 00                	push   $0x0
  800f5a:	e8 00 fc ff ff       	call   800b5f <sys_page_map>
  800f5f:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  800f62:	46                   	inc    %esi
  800f63:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  800f69:	0f 85 fe fe ff ff    	jne    800e6d <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  800f6f:	ff 45 e0             	incl   -0x20(%ebp)
  800f72:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800f75:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  800f7a:	0f 85 d3 fe ff ff    	jne    800e53 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  800f80:	a1 04 20 80 00       	mov    0x802004,%eax
  800f85:	8b 40 64             	mov    0x64(%eax),%eax
  800f88:	83 ec 08             	sub    $0x8,%esp
  800f8b:	50                   	push   %eax
  800f8c:	ff 75 dc             	pushl  -0x24(%ebp)
  800f8f:	e8 f2 fc ff ff       	call   800c86 <sys_env_set_pgfault_upcall>
  800f94:	83 c4 10             	add    $0x10,%esp
  800f97:	85 c0                	test   %eax,%eax
  800f99:	74 17                	je     800fb2 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  800f9b:	83 ec 04             	sub    $0x4,%esp
  800f9e:	68 d0 16 80 00       	push   $0x8016d0
  800fa3:	68 b9 00 00 00       	push   $0xb9
  800fa8:	68 f0 16 80 00       	push   $0x8016f0
  800fad:	e8 50 00 00 00       	call   801002 <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  800fb2:	83 ec 08             	sub    $0x8,%esp
  800fb5:	6a 02                	push   $0x2
  800fb7:	ff 75 dc             	pushl  -0x24(%ebp)
  800fba:	e8 24 fc ff ff       	call   800be3 <sys_env_set_status>
  800fbf:	83 c4 10             	add    $0x10,%esp
  800fc2:	85 c0                	test   %eax,%eax
  800fc4:	74 17                	je     800fdd <fork+0x1ff>
        panic("fork: cannot set env status");
  800fc6:	83 ec 04             	sub    $0x4,%esp
  800fc9:	68 8e 17 80 00       	push   $0x80178e
  800fce:	68 bd 00 00 00       	push   $0xbd
  800fd3:	68 f0 16 80 00       	push   $0x8016f0
  800fd8:	e8 25 00 00 00       	call   801002 <_panic>

    return envid;
  800fdd:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  800fe0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800fe3:	5b                   	pop    %ebx
  800fe4:	5e                   	pop    %esi
  800fe5:	5f                   	pop    %edi
  800fe6:	5d                   	pop    %ebp
  800fe7:	c3                   	ret    

00800fe8 <sfork>:

// Challenge!
int
sfork(void)
{
  800fe8:	55                   	push   %ebp
  800fe9:	89 e5                	mov    %esp,%ebp
  800feb:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  800fee:	68 aa 17 80 00       	push   $0x8017aa
  800ff3:	68 c8 00 00 00       	push   $0xc8
  800ff8:	68 f0 16 80 00       	push   $0x8016f0
  800ffd:	e8 00 00 00 00       	call   801002 <_panic>

00801002 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  801002:	55                   	push   %ebp
  801003:	89 e5                	mov    %esp,%ebp
  801005:	56                   	push   %esi
  801006:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  801007:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80100a:	8b 35 00 20 80 00    	mov    0x802000,%esi
  801010:	e8 c9 fa ff ff       	call   800ade <sys_getenvid>
  801015:	83 ec 0c             	sub    $0xc,%esp
  801018:	ff 75 0c             	pushl  0xc(%ebp)
  80101b:	ff 75 08             	pushl  0x8(%ebp)
  80101e:	56                   	push   %esi
  80101f:	50                   	push   %eax
  801020:	68 c0 17 80 00       	push   $0x8017c0
  801025:	e8 aa f1 ff ff       	call   8001d4 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80102a:	83 c4 18             	add    $0x18,%esp
  80102d:	53                   	push   %ebx
  80102e:	ff 75 10             	pushl  0x10(%ebp)
  801031:	e8 4d f1 ff ff       	call   800183 <vcprintf>
	cprintf("\n");
  801036:	c7 04 24 4f 13 80 00 	movl   $0x80134f,(%esp)
  80103d:	e8 92 f1 ff ff       	call   8001d4 <cprintf>
  801042:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801045:	cc                   	int3   
  801046:	eb fd                	jmp    801045 <_panic+0x43>

00801048 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801048:	55                   	push   %ebp
  801049:	89 e5                	mov    %esp,%ebp
  80104b:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  80104e:	83 3d 08 20 80 00 00 	cmpl   $0x0,0x802008
  801055:	75 3e                	jne    801095 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801057:	83 ec 04             	sub    $0x4,%esp
  80105a:	6a 07                	push   $0x7
  80105c:	68 00 f0 bf ee       	push   $0xeebff000
  801061:	6a 00                	push   $0x0
  801063:	e8 b4 fa ff ff       	call   800b1c <sys_page_alloc>
  801068:	83 c4 10             	add    $0x10,%esp
  80106b:	85 c0                	test   %eax,%eax
  80106d:	74 14                	je     801083 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  80106f:	83 ec 04             	sub    $0x4,%esp
  801072:	68 e4 17 80 00       	push   $0x8017e4
  801077:	6a 23                	push   $0x23
  801079:	68 08 18 80 00       	push   $0x801808
  80107e:	e8 7f ff ff ff       	call   801002 <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801083:	83 ec 08             	sub    $0x8,%esp
  801086:	68 9f 10 80 00       	push   $0x80109f
  80108b:	6a 00                	push   $0x0
  80108d:	e8 f4 fb ff ff       	call   800c86 <sys_env_set_pgfault_upcall>
  801092:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801095:	8b 45 08             	mov    0x8(%ebp),%eax
  801098:	a3 08 20 80 00       	mov    %eax,0x802008
}
  80109d:	c9                   	leave  
  80109e:	c3                   	ret    

0080109f <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  80109f:	54                   	push   %esp
	movl _pgfault_handler, %eax
  8010a0:	a1 08 20 80 00       	mov    0x802008,%eax
	call *%eax
  8010a5:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  8010a7:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  8010aa:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  8010ac:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  8010b0:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  8010b4:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  8010b5:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  8010b8:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  8010ba:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  8010bd:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  8010c0:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  8010c1:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8010c4:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8010c5:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8010c6:	c3                   	ret    
  8010c7:	90                   	nop

008010c8 <__udivdi3>:
  8010c8:	55                   	push   %ebp
  8010c9:	57                   	push   %edi
  8010ca:	56                   	push   %esi
  8010cb:	53                   	push   %ebx
  8010cc:	83 ec 1c             	sub    $0x1c,%esp
  8010cf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8010d3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8010d7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8010db:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8010df:	89 ca                	mov    %ecx,%edx
  8010e1:	89 f8                	mov    %edi,%eax
  8010e3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8010e7:	85 f6                	test   %esi,%esi
  8010e9:	75 2d                	jne    801118 <__udivdi3+0x50>
  8010eb:	39 cf                	cmp    %ecx,%edi
  8010ed:	77 65                	ja     801154 <__udivdi3+0x8c>
  8010ef:	89 fd                	mov    %edi,%ebp
  8010f1:	85 ff                	test   %edi,%edi
  8010f3:	75 0b                	jne    801100 <__udivdi3+0x38>
  8010f5:	b8 01 00 00 00       	mov    $0x1,%eax
  8010fa:	31 d2                	xor    %edx,%edx
  8010fc:	f7 f7                	div    %edi
  8010fe:	89 c5                	mov    %eax,%ebp
  801100:	31 d2                	xor    %edx,%edx
  801102:	89 c8                	mov    %ecx,%eax
  801104:	f7 f5                	div    %ebp
  801106:	89 c1                	mov    %eax,%ecx
  801108:	89 d8                	mov    %ebx,%eax
  80110a:	f7 f5                	div    %ebp
  80110c:	89 cf                	mov    %ecx,%edi
  80110e:	89 fa                	mov    %edi,%edx
  801110:	83 c4 1c             	add    $0x1c,%esp
  801113:	5b                   	pop    %ebx
  801114:	5e                   	pop    %esi
  801115:	5f                   	pop    %edi
  801116:	5d                   	pop    %ebp
  801117:	c3                   	ret    
  801118:	39 ce                	cmp    %ecx,%esi
  80111a:	77 28                	ja     801144 <__udivdi3+0x7c>
  80111c:	0f bd fe             	bsr    %esi,%edi
  80111f:	83 f7 1f             	xor    $0x1f,%edi
  801122:	75 40                	jne    801164 <__udivdi3+0x9c>
  801124:	39 ce                	cmp    %ecx,%esi
  801126:	72 0a                	jb     801132 <__udivdi3+0x6a>
  801128:	3b 44 24 08          	cmp    0x8(%esp),%eax
  80112c:	0f 87 9e 00 00 00    	ja     8011d0 <__udivdi3+0x108>
  801132:	b8 01 00 00 00       	mov    $0x1,%eax
  801137:	89 fa                	mov    %edi,%edx
  801139:	83 c4 1c             	add    $0x1c,%esp
  80113c:	5b                   	pop    %ebx
  80113d:	5e                   	pop    %esi
  80113e:	5f                   	pop    %edi
  80113f:	5d                   	pop    %ebp
  801140:	c3                   	ret    
  801141:	8d 76 00             	lea    0x0(%esi),%esi
  801144:	31 ff                	xor    %edi,%edi
  801146:	31 c0                	xor    %eax,%eax
  801148:	89 fa                	mov    %edi,%edx
  80114a:	83 c4 1c             	add    $0x1c,%esp
  80114d:	5b                   	pop    %ebx
  80114e:	5e                   	pop    %esi
  80114f:	5f                   	pop    %edi
  801150:	5d                   	pop    %ebp
  801151:	c3                   	ret    
  801152:	66 90                	xchg   %ax,%ax
  801154:	89 d8                	mov    %ebx,%eax
  801156:	f7 f7                	div    %edi
  801158:	31 ff                	xor    %edi,%edi
  80115a:	89 fa                	mov    %edi,%edx
  80115c:	83 c4 1c             	add    $0x1c,%esp
  80115f:	5b                   	pop    %ebx
  801160:	5e                   	pop    %esi
  801161:	5f                   	pop    %edi
  801162:	5d                   	pop    %ebp
  801163:	c3                   	ret    
  801164:	bd 20 00 00 00       	mov    $0x20,%ebp
  801169:	89 eb                	mov    %ebp,%ebx
  80116b:	29 fb                	sub    %edi,%ebx
  80116d:	89 f9                	mov    %edi,%ecx
  80116f:	d3 e6                	shl    %cl,%esi
  801171:	89 c5                	mov    %eax,%ebp
  801173:	88 d9                	mov    %bl,%cl
  801175:	d3 ed                	shr    %cl,%ebp
  801177:	89 e9                	mov    %ebp,%ecx
  801179:	09 f1                	or     %esi,%ecx
  80117b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80117f:	89 f9                	mov    %edi,%ecx
  801181:	d3 e0                	shl    %cl,%eax
  801183:	89 c5                	mov    %eax,%ebp
  801185:	89 d6                	mov    %edx,%esi
  801187:	88 d9                	mov    %bl,%cl
  801189:	d3 ee                	shr    %cl,%esi
  80118b:	89 f9                	mov    %edi,%ecx
  80118d:	d3 e2                	shl    %cl,%edx
  80118f:	8b 44 24 08          	mov    0x8(%esp),%eax
  801193:	88 d9                	mov    %bl,%cl
  801195:	d3 e8                	shr    %cl,%eax
  801197:	09 c2                	or     %eax,%edx
  801199:	89 d0                	mov    %edx,%eax
  80119b:	89 f2                	mov    %esi,%edx
  80119d:	f7 74 24 0c          	divl   0xc(%esp)
  8011a1:	89 d6                	mov    %edx,%esi
  8011a3:	89 c3                	mov    %eax,%ebx
  8011a5:	f7 e5                	mul    %ebp
  8011a7:	39 d6                	cmp    %edx,%esi
  8011a9:	72 19                	jb     8011c4 <__udivdi3+0xfc>
  8011ab:	74 0b                	je     8011b8 <__udivdi3+0xf0>
  8011ad:	89 d8                	mov    %ebx,%eax
  8011af:	31 ff                	xor    %edi,%edi
  8011b1:	e9 58 ff ff ff       	jmp    80110e <__udivdi3+0x46>
  8011b6:	66 90                	xchg   %ax,%ax
  8011b8:	8b 54 24 08          	mov    0x8(%esp),%edx
  8011bc:	89 f9                	mov    %edi,%ecx
  8011be:	d3 e2                	shl    %cl,%edx
  8011c0:	39 c2                	cmp    %eax,%edx
  8011c2:	73 e9                	jae    8011ad <__udivdi3+0xe5>
  8011c4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8011c7:	31 ff                	xor    %edi,%edi
  8011c9:	e9 40 ff ff ff       	jmp    80110e <__udivdi3+0x46>
  8011ce:	66 90                	xchg   %ax,%ax
  8011d0:	31 c0                	xor    %eax,%eax
  8011d2:	e9 37 ff ff ff       	jmp    80110e <__udivdi3+0x46>
  8011d7:	90                   	nop

008011d8 <__umoddi3>:
  8011d8:	55                   	push   %ebp
  8011d9:	57                   	push   %edi
  8011da:	56                   	push   %esi
  8011db:	53                   	push   %ebx
  8011dc:	83 ec 1c             	sub    $0x1c,%esp
  8011df:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8011e3:	8b 74 24 34          	mov    0x34(%esp),%esi
  8011e7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8011eb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8011ef:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8011f3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8011f7:	89 f3                	mov    %esi,%ebx
  8011f9:	89 fa                	mov    %edi,%edx
  8011fb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8011ff:	89 34 24             	mov    %esi,(%esp)
  801202:	85 c0                	test   %eax,%eax
  801204:	75 1a                	jne    801220 <__umoddi3+0x48>
  801206:	39 f7                	cmp    %esi,%edi
  801208:	0f 86 a2 00 00 00    	jbe    8012b0 <__umoddi3+0xd8>
  80120e:	89 c8                	mov    %ecx,%eax
  801210:	89 f2                	mov    %esi,%edx
  801212:	f7 f7                	div    %edi
  801214:	89 d0                	mov    %edx,%eax
  801216:	31 d2                	xor    %edx,%edx
  801218:	83 c4 1c             	add    $0x1c,%esp
  80121b:	5b                   	pop    %ebx
  80121c:	5e                   	pop    %esi
  80121d:	5f                   	pop    %edi
  80121e:	5d                   	pop    %ebp
  80121f:	c3                   	ret    
  801220:	39 f0                	cmp    %esi,%eax
  801222:	0f 87 ac 00 00 00    	ja     8012d4 <__umoddi3+0xfc>
  801228:	0f bd e8             	bsr    %eax,%ebp
  80122b:	83 f5 1f             	xor    $0x1f,%ebp
  80122e:	0f 84 ac 00 00 00    	je     8012e0 <__umoddi3+0x108>
  801234:	bf 20 00 00 00       	mov    $0x20,%edi
  801239:	29 ef                	sub    %ebp,%edi
  80123b:	89 fe                	mov    %edi,%esi
  80123d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801241:	89 e9                	mov    %ebp,%ecx
  801243:	d3 e0                	shl    %cl,%eax
  801245:	89 d7                	mov    %edx,%edi
  801247:	89 f1                	mov    %esi,%ecx
  801249:	d3 ef                	shr    %cl,%edi
  80124b:	09 c7                	or     %eax,%edi
  80124d:	89 e9                	mov    %ebp,%ecx
  80124f:	d3 e2                	shl    %cl,%edx
  801251:	89 14 24             	mov    %edx,(%esp)
  801254:	89 d8                	mov    %ebx,%eax
  801256:	d3 e0                	shl    %cl,%eax
  801258:	89 c2                	mov    %eax,%edx
  80125a:	8b 44 24 08          	mov    0x8(%esp),%eax
  80125e:	d3 e0                	shl    %cl,%eax
  801260:	89 44 24 04          	mov    %eax,0x4(%esp)
  801264:	8b 44 24 08          	mov    0x8(%esp),%eax
  801268:	89 f1                	mov    %esi,%ecx
  80126a:	d3 e8                	shr    %cl,%eax
  80126c:	09 d0                	or     %edx,%eax
  80126e:	d3 eb                	shr    %cl,%ebx
  801270:	89 da                	mov    %ebx,%edx
  801272:	f7 f7                	div    %edi
  801274:	89 d3                	mov    %edx,%ebx
  801276:	f7 24 24             	mull   (%esp)
  801279:	89 c6                	mov    %eax,%esi
  80127b:	89 d1                	mov    %edx,%ecx
  80127d:	39 d3                	cmp    %edx,%ebx
  80127f:	0f 82 87 00 00 00    	jb     80130c <__umoddi3+0x134>
  801285:	0f 84 91 00 00 00    	je     80131c <__umoddi3+0x144>
  80128b:	8b 54 24 04          	mov    0x4(%esp),%edx
  80128f:	29 f2                	sub    %esi,%edx
  801291:	19 cb                	sbb    %ecx,%ebx
  801293:	89 d8                	mov    %ebx,%eax
  801295:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801299:	d3 e0                	shl    %cl,%eax
  80129b:	89 e9                	mov    %ebp,%ecx
  80129d:	d3 ea                	shr    %cl,%edx
  80129f:	09 d0                	or     %edx,%eax
  8012a1:	89 e9                	mov    %ebp,%ecx
  8012a3:	d3 eb                	shr    %cl,%ebx
  8012a5:	89 da                	mov    %ebx,%edx
  8012a7:	83 c4 1c             	add    $0x1c,%esp
  8012aa:	5b                   	pop    %ebx
  8012ab:	5e                   	pop    %esi
  8012ac:	5f                   	pop    %edi
  8012ad:	5d                   	pop    %ebp
  8012ae:	c3                   	ret    
  8012af:	90                   	nop
  8012b0:	89 fd                	mov    %edi,%ebp
  8012b2:	85 ff                	test   %edi,%edi
  8012b4:	75 0b                	jne    8012c1 <__umoddi3+0xe9>
  8012b6:	b8 01 00 00 00       	mov    $0x1,%eax
  8012bb:	31 d2                	xor    %edx,%edx
  8012bd:	f7 f7                	div    %edi
  8012bf:	89 c5                	mov    %eax,%ebp
  8012c1:	89 f0                	mov    %esi,%eax
  8012c3:	31 d2                	xor    %edx,%edx
  8012c5:	f7 f5                	div    %ebp
  8012c7:	89 c8                	mov    %ecx,%eax
  8012c9:	f7 f5                	div    %ebp
  8012cb:	89 d0                	mov    %edx,%eax
  8012cd:	e9 44 ff ff ff       	jmp    801216 <__umoddi3+0x3e>
  8012d2:	66 90                	xchg   %ax,%ax
  8012d4:	89 c8                	mov    %ecx,%eax
  8012d6:	89 f2                	mov    %esi,%edx
  8012d8:	83 c4 1c             	add    $0x1c,%esp
  8012db:	5b                   	pop    %ebx
  8012dc:	5e                   	pop    %esi
  8012dd:	5f                   	pop    %edi
  8012de:	5d                   	pop    %ebp
  8012df:	c3                   	ret    
  8012e0:	3b 04 24             	cmp    (%esp),%eax
  8012e3:	72 06                	jb     8012eb <__umoddi3+0x113>
  8012e5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8012e9:	77 0f                	ja     8012fa <__umoddi3+0x122>
  8012eb:	89 f2                	mov    %esi,%edx
  8012ed:	29 f9                	sub    %edi,%ecx
  8012ef:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8012f3:	89 14 24             	mov    %edx,(%esp)
  8012f6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8012fa:	8b 44 24 04          	mov    0x4(%esp),%eax
  8012fe:	8b 14 24             	mov    (%esp),%edx
  801301:	83 c4 1c             	add    $0x1c,%esp
  801304:	5b                   	pop    %ebx
  801305:	5e                   	pop    %esi
  801306:	5f                   	pop    %edi
  801307:	5d                   	pop    %ebp
  801308:	c3                   	ret    
  801309:	8d 76 00             	lea    0x0(%esi),%esi
  80130c:	2b 04 24             	sub    (%esp),%eax
  80130f:	19 fa                	sbb    %edi,%edx
  801311:	89 d1                	mov    %edx,%ecx
  801313:	89 c6                	mov    %eax,%esi
  801315:	e9 71 ff ff ff       	jmp    80128b <__umoddi3+0xb3>
  80131a:	66 90                	xchg   %ax,%ax
  80131c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801320:	72 ea                	jb     80130c <__umoddi3+0x134>
  801322:	89 d9                	mov    %ebx,%ecx
  801324:	e9 62 ff ff ff       	jmp    80128b <__umoddi3+0xb3>
