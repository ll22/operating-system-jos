
obj/user/num.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 4c 01 00 00       	call   80017d <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <num>:
int bol = 1;
int line = 0;

void
num(int f, const char *s)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	83 ec 10             	sub    $0x10,%esp
  80003b:	8b 75 08             	mov    0x8(%ebp),%esi
	long n;
	int r;
	char c;

	while ((n = read(f, &c, 1)) > 0) {
  80003e:	8d 5d f7             	lea    -0x9(%ebp),%ebx
  800041:	eb 6c                	jmp    8000af <num+0x7c>
		if (bol) {
  800043:	83 3d 00 30 80 00 00 	cmpl   $0x0,0x803000
  80004a:	74 26                	je     800072 <num+0x3f>
			printf("%5d ", ++line);
  80004c:	a1 00 40 80 00       	mov    0x804000,%eax
  800051:	40                   	inc    %eax
  800052:	a3 00 40 80 00       	mov    %eax,0x804000
  800057:	83 ec 08             	sub    $0x8,%esp
  80005a:	50                   	push   %eax
  80005b:	68 80 1f 80 00       	push   $0x801f80
  800060:	e8 85 16 00 00       	call   8016ea <printf>
			bol = 0;
  800065:	c7 05 00 30 80 00 00 	movl   $0x0,0x803000
  80006c:	00 00 00 
  80006f:	83 c4 10             	add    $0x10,%esp
		}
		if ((r = write(1, &c, 1)) != 1)
  800072:	83 ec 04             	sub    $0x4,%esp
  800075:	6a 01                	push   $0x1
  800077:	53                   	push   %ebx
  800078:	6a 01                	push   $0x1
  80007a:	e8 3b 11 00 00       	call   8011ba <write>
  80007f:	83 c4 10             	add    $0x10,%esp
  800082:	83 f8 01             	cmp    $0x1,%eax
  800085:	74 18                	je     80009f <num+0x6c>
			panic("write error copying %s: %e", s, r);
  800087:	83 ec 0c             	sub    $0xc,%esp
  80008a:	50                   	push   %eax
  80008b:	ff 75 0c             	pushl  0xc(%ebp)
  80008e:	68 85 1f 80 00       	push   $0x801f85
  800093:	6a 13                	push   $0x13
  800095:	68 a0 1f 80 00       	push   $0x801fa0
  80009a:	e8 3f 01 00 00       	call   8001de <_panic>
		if (c == '\n')
  80009f:	80 7d f7 0a          	cmpb   $0xa,-0x9(%ebp)
  8000a3:	75 0a                	jne    8000af <num+0x7c>
			bol = 1;
  8000a5:	c7 05 00 30 80 00 01 	movl   $0x1,0x803000
  8000ac:	00 00 00 
{
	long n;
	int r;
	char c;

	while ((n = read(f, &c, 1)) > 0) {
  8000af:	83 ec 04             	sub    $0x4,%esp
  8000b2:	6a 01                	push   $0x1
  8000b4:	53                   	push   %ebx
  8000b5:	56                   	push   %esi
  8000b6:	e8 2f 10 00 00       	call   8010ea <read>
  8000bb:	83 c4 10             	add    $0x10,%esp
  8000be:	85 c0                	test   %eax,%eax
  8000c0:	7f 81                	jg     800043 <num+0x10>
		if ((r = write(1, &c, 1)) != 1)
			panic("write error copying %s: %e", s, r);
		if (c == '\n')
			bol = 1;
	}
	if (n < 0)
  8000c2:	85 c0                	test   %eax,%eax
  8000c4:	79 18                	jns    8000de <num+0xab>
		panic("error reading %s: %e", s, n);
  8000c6:	83 ec 0c             	sub    $0xc,%esp
  8000c9:	50                   	push   %eax
  8000ca:	ff 75 0c             	pushl  0xc(%ebp)
  8000cd:	68 ab 1f 80 00       	push   $0x801fab
  8000d2:	6a 18                	push   $0x18
  8000d4:	68 a0 1f 80 00       	push   $0x801fa0
  8000d9:	e8 00 01 00 00       	call   8001de <_panic>
}
  8000de:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000e1:	5b                   	pop    %ebx
  8000e2:	5e                   	pop    %esi
  8000e3:	5d                   	pop    %ebp
  8000e4:	c3                   	ret    

008000e5 <umain>:

void
umain(int argc, char **argv)
{
  8000e5:	55                   	push   %ebp
  8000e6:	89 e5                	mov    %esp,%ebp
  8000e8:	57                   	push   %edi
  8000e9:	56                   	push   %esi
  8000ea:	53                   	push   %ebx
  8000eb:	83 ec 1c             	sub    $0x1c,%esp
	int f, i;

	binaryname = "num";
  8000ee:	c7 05 04 30 80 00 c0 	movl   $0x801fc0,0x803004
  8000f5:	1f 80 00 
	if (argc == 1)
  8000f8:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  8000fc:	74 0d                	je     80010b <umain+0x26>
  8000fe:	8b 45 0c             	mov    0xc(%ebp),%eax
  800101:	8d 58 04             	lea    0x4(%eax),%ebx
  800104:	bf 01 00 00 00       	mov    $0x1,%edi
  800109:	eb 60                	jmp    80016b <umain+0x86>
		num(0, "<stdin>");
  80010b:	83 ec 08             	sub    $0x8,%esp
  80010e:	68 c4 1f 80 00       	push   $0x801fc4
  800113:	6a 00                	push   $0x0
  800115:	e8 19 ff ff ff       	call   800033 <num>
  80011a:	83 c4 10             	add    $0x10,%esp
  80011d:	eb 51                	jmp    800170 <umain+0x8b>
	else
		for (i = 1; i < argc; i++) {
			f = open(argv[i], O_RDONLY);
  80011f:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800122:	83 ec 08             	sub    $0x8,%esp
  800125:	6a 00                	push   $0x0
  800127:	ff 33                	pushl  (%ebx)
  800129:	e8 23 14 00 00       	call   801551 <open>
  80012e:	89 c6                	mov    %eax,%esi
			if (f < 0)
  800130:	83 c4 10             	add    $0x10,%esp
  800133:	85 c0                	test   %eax,%eax
  800135:	79 1a                	jns    800151 <umain+0x6c>
				panic("can't open %s: %e", argv[i], f);
  800137:	83 ec 0c             	sub    $0xc,%esp
  80013a:	50                   	push   %eax
  80013b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  80013e:	ff 30                	pushl  (%eax)
  800140:	68 cc 1f 80 00       	push   $0x801fcc
  800145:	6a 27                	push   $0x27
  800147:	68 a0 1f 80 00       	push   $0x801fa0
  80014c:	e8 8d 00 00 00       	call   8001de <_panic>
			else {
				num(f, argv[i]);
  800151:	83 ec 08             	sub    $0x8,%esp
  800154:	ff 33                	pushl  (%ebx)
  800156:	50                   	push   %eax
  800157:	e8 d7 fe ff ff       	call   800033 <num>
				close(f);
  80015c:	89 34 24             	mov    %esi,(%esp)
  80015f:	e8 4e 0e 00 00       	call   800fb2 <close>

	binaryname = "num";
	if (argc == 1)
		num(0, "<stdin>");
	else
		for (i = 1; i < argc; i++) {
  800164:	47                   	inc    %edi
  800165:	83 c3 04             	add    $0x4,%ebx
  800168:	83 c4 10             	add    $0x10,%esp
  80016b:	3b 7d 08             	cmp    0x8(%ebp),%edi
  80016e:	7c af                	jl     80011f <umain+0x3a>
			else {
				num(f, argv[i]);
				close(f);
			}
		}
	exit();
  800170:	e8 57 00 00 00       	call   8001cc <exit>
}
  800175:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800178:	5b                   	pop    %ebx
  800179:	5e                   	pop    %esi
  80017a:	5f                   	pop    %edi
  80017b:	5d                   	pop    %ebp
  80017c:	c3                   	ret    

0080017d <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80017d:	55                   	push   %ebp
  80017e:	89 e5                	mov    %esp,%ebp
  800180:	56                   	push   %esi
  800181:	53                   	push   %ebx
  800182:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800185:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800188:	e8 33 0a 00 00       	call   800bc0 <sys_getenvid>
  80018d:	25 ff 03 00 00       	and    $0x3ff,%eax
  800192:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800199:	c1 e0 07             	shl    $0x7,%eax
  80019c:	29 d0                	sub    %edx,%eax
  80019e:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8001a3:	a3 08 40 80 00       	mov    %eax,0x804008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8001a8:	85 db                	test   %ebx,%ebx
  8001aa:	7e 07                	jle    8001b3 <libmain+0x36>
		binaryname = argv[0];
  8001ac:	8b 06                	mov    (%esi),%eax
  8001ae:	a3 04 30 80 00       	mov    %eax,0x803004

	// call user main routine
	umain(argc, argv);
  8001b3:	83 ec 08             	sub    $0x8,%esp
  8001b6:	56                   	push   %esi
  8001b7:	53                   	push   %ebx
  8001b8:	e8 28 ff ff ff       	call   8000e5 <umain>

	// exit gracefully
	exit();
  8001bd:	e8 0a 00 00 00       	call   8001cc <exit>
}
  8001c2:	83 c4 10             	add    $0x10,%esp
  8001c5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8001c8:	5b                   	pop    %ebx
  8001c9:	5e                   	pop    %esi
  8001ca:	5d                   	pop    %ebp
  8001cb:	c3                   	ret    

008001cc <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8001cc:	55                   	push   %ebp
  8001cd:	89 e5                	mov    %esp,%ebp
  8001cf:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8001d2:	6a 00                	push   $0x0
  8001d4:	e8 a6 09 00 00       	call   800b7f <sys_env_destroy>
}
  8001d9:	83 c4 10             	add    $0x10,%esp
  8001dc:	c9                   	leave  
  8001dd:	c3                   	ret    

008001de <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8001de:	55                   	push   %ebp
  8001df:	89 e5                	mov    %esp,%ebp
  8001e1:	56                   	push   %esi
  8001e2:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8001e3:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8001e6:	8b 35 04 30 80 00    	mov    0x803004,%esi
  8001ec:	e8 cf 09 00 00       	call   800bc0 <sys_getenvid>
  8001f1:	83 ec 0c             	sub    $0xc,%esp
  8001f4:	ff 75 0c             	pushl  0xc(%ebp)
  8001f7:	ff 75 08             	pushl  0x8(%ebp)
  8001fa:	56                   	push   %esi
  8001fb:	50                   	push   %eax
  8001fc:	68 e8 1f 80 00       	push   $0x801fe8
  800201:	e8 b0 00 00 00       	call   8002b6 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800206:	83 c4 18             	add    $0x18,%esp
  800209:	53                   	push   %ebx
  80020a:	ff 75 10             	pushl  0x10(%ebp)
  80020d:	e8 53 00 00 00       	call   800265 <vcprintf>
	cprintf("\n");
  800212:	c7 04 24 07 24 80 00 	movl   $0x802407,(%esp)
  800219:	e8 98 00 00 00       	call   8002b6 <cprintf>
  80021e:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800221:	cc                   	int3   
  800222:	eb fd                	jmp    800221 <_panic+0x43>

00800224 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800224:	55                   	push   %ebp
  800225:	89 e5                	mov    %esp,%ebp
  800227:	53                   	push   %ebx
  800228:	83 ec 04             	sub    $0x4,%esp
  80022b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80022e:	8b 13                	mov    (%ebx),%edx
  800230:	8d 42 01             	lea    0x1(%edx),%eax
  800233:	89 03                	mov    %eax,(%ebx)
  800235:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800238:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80023c:	3d ff 00 00 00       	cmp    $0xff,%eax
  800241:	75 1a                	jne    80025d <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800243:	83 ec 08             	sub    $0x8,%esp
  800246:	68 ff 00 00 00       	push   $0xff
  80024b:	8d 43 08             	lea    0x8(%ebx),%eax
  80024e:	50                   	push   %eax
  80024f:	e8 ee 08 00 00       	call   800b42 <sys_cputs>
		b->idx = 0;
  800254:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80025a:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80025d:	ff 43 04             	incl   0x4(%ebx)
}
  800260:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800263:	c9                   	leave  
  800264:	c3                   	ret    

00800265 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800265:	55                   	push   %ebp
  800266:	89 e5                	mov    %esp,%ebp
  800268:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80026e:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800275:	00 00 00 
	b.cnt = 0;
  800278:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80027f:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800282:	ff 75 0c             	pushl  0xc(%ebp)
  800285:	ff 75 08             	pushl  0x8(%ebp)
  800288:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80028e:	50                   	push   %eax
  80028f:	68 24 02 80 00       	push   $0x800224
  800294:	e8 51 01 00 00       	call   8003ea <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800299:	83 c4 08             	add    $0x8,%esp
  80029c:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8002a2:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8002a8:	50                   	push   %eax
  8002a9:	e8 94 08 00 00       	call   800b42 <sys_cputs>

	return b.cnt;
}
  8002ae:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8002b4:	c9                   	leave  
  8002b5:	c3                   	ret    

008002b6 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8002b6:	55                   	push   %ebp
  8002b7:	89 e5                	mov    %esp,%ebp
  8002b9:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8002bc:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8002bf:	50                   	push   %eax
  8002c0:	ff 75 08             	pushl  0x8(%ebp)
  8002c3:	e8 9d ff ff ff       	call   800265 <vcprintf>
	va_end(ap);

	return cnt;
}
  8002c8:	c9                   	leave  
  8002c9:	c3                   	ret    

008002ca <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8002ca:	55                   	push   %ebp
  8002cb:	89 e5                	mov    %esp,%ebp
  8002cd:	57                   	push   %edi
  8002ce:	56                   	push   %esi
  8002cf:	53                   	push   %ebx
  8002d0:	83 ec 1c             	sub    $0x1c,%esp
  8002d3:	89 c7                	mov    %eax,%edi
  8002d5:	89 d6                	mov    %edx,%esi
  8002d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8002da:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002dd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8002e0:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8002e3:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8002e6:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002eb:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8002ee:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8002f1:	39 d3                	cmp    %edx,%ebx
  8002f3:	72 05                	jb     8002fa <printnum+0x30>
  8002f5:	39 45 10             	cmp    %eax,0x10(%ebp)
  8002f8:	77 45                	ja     80033f <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8002fa:	83 ec 0c             	sub    $0xc,%esp
  8002fd:	ff 75 18             	pushl  0x18(%ebp)
  800300:	8b 45 14             	mov    0x14(%ebp),%eax
  800303:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800306:	53                   	push   %ebx
  800307:	ff 75 10             	pushl  0x10(%ebp)
  80030a:	83 ec 08             	sub    $0x8,%esp
  80030d:	ff 75 e4             	pushl  -0x1c(%ebp)
  800310:	ff 75 e0             	pushl  -0x20(%ebp)
  800313:	ff 75 dc             	pushl  -0x24(%ebp)
  800316:	ff 75 d8             	pushl  -0x28(%ebp)
  800319:	e8 e6 19 00 00       	call   801d04 <__udivdi3>
  80031e:	83 c4 18             	add    $0x18,%esp
  800321:	52                   	push   %edx
  800322:	50                   	push   %eax
  800323:	89 f2                	mov    %esi,%edx
  800325:	89 f8                	mov    %edi,%eax
  800327:	e8 9e ff ff ff       	call   8002ca <printnum>
  80032c:	83 c4 20             	add    $0x20,%esp
  80032f:	eb 16                	jmp    800347 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800331:	83 ec 08             	sub    $0x8,%esp
  800334:	56                   	push   %esi
  800335:	ff 75 18             	pushl  0x18(%ebp)
  800338:	ff d7                	call   *%edi
  80033a:	83 c4 10             	add    $0x10,%esp
  80033d:	eb 03                	jmp    800342 <printnum+0x78>
  80033f:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800342:	4b                   	dec    %ebx
  800343:	85 db                	test   %ebx,%ebx
  800345:	7f ea                	jg     800331 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800347:	83 ec 08             	sub    $0x8,%esp
  80034a:	56                   	push   %esi
  80034b:	83 ec 04             	sub    $0x4,%esp
  80034e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800351:	ff 75 e0             	pushl  -0x20(%ebp)
  800354:	ff 75 dc             	pushl  -0x24(%ebp)
  800357:	ff 75 d8             	pushl  -0x28(%ebp)
  80035a:	e8 b5 1a 00 00       	call   801e14 <__umoddi3>
  80035f:	83 c4 14             	add    $0x14,%esp
  800362:	0f be 80 0b 20 80 00 	movsbl 0x80200b(%eax),%eax
  800369:	50                   	push   %eax
  80036a:	ff d7                	call   *%edi
}
  80036c:	83 c4 10             	add    $0x10,%esp
  80036f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800372:	5b                   	pop    %ebx
  800373:	5e                   	pop    %esi
  800374:	5f                   	pop    %edi
  800375:	5d                   	pop    %ebp
  800376:	c3                   	ret    

00800377 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800377:	55                   	push   %ebp
  800378:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80037a:	83 fa 01             	cmp    $0x1,%edx
  80037d:	7e 0e                	jle    80038d <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80037f:	8b 10                	mov    (%eax),%edx
  800381:	8d 4a 08             	lea    0x8(%edx),%ecx
  800384:	89 08                	mov    %ecx,(%eax)
  800386:	8b 02                	mov    (%edx),%eax
  800388:	8b 52 04             	mov    0x4(%edx),%edx
  80038b:	eb 22                	jmp    8003af <getuint+0x38>
	else if (lflag)
  80038d:	85 d2                	test   %edx,%edx
  80038f:	74 10                	je     8003a1 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800391:	8b 10                	mov    (%eax),%edx
  800393:	8d 4a 04             	lea    0x4(%edx),%ecx
  800396:	89 08                	mov    %ecx,(%eax)
  800398:	8b 02                	mov    (%edx),%eax
  80039a:	ba 00 00 00 00       	mov    $0x0,%edx
  80039f:	eb 0e                	jmp    8003af <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8003a1:	8b 10                	mov    (%eax),%edx
  8003a3:	8d 4a 04             	lea    0x4(%edx),%ecx
  8003a6:	89 08                	mov    %ecx,(%eax)
  8003a8:	8b 02                	mov    (%edx),%eax
  8003aa:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8003af:	5d                   	pop    %ebp
  8003b0:	c3                   	ret    

008003b1 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8003b1:	55                   	push   %ebp
  8003b2:	89 e5                	mov    %esp,%ebp
  8003b4:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8003b7:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8003ba:	8b 10                	mov    (%eax),%edx
  8003bc:	3b 50 04             	cmp    0x4(%eax),%edx
  8003bf:	73 0a                	jae    8003cb <sprintputch+0x1a>
		*b->buf++ = ch;
  8003c1:	8d 4a 01             	lea    0x1(%edx),%ecx
  8003c4:	89 08                	mov    %ecx,(%eax)
  8003c6:	8b 45 08             	mov    0x8(%ebp),%eax
  8003c9:	88 02                	mov    %al,(%edx)
}
  8003cb:	5d                   	pop    %ebp
  8003cc:	c3                   	ret    

008003cd <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8003cd:	55                   	push   %ebp
  8003ce:	89 e5                	mov    %esp,%ebp
  8003d0:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8003d3:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8003d6:	50                   	push   %eax
  8003d7:	ff 75 10             	pushl  0x10(%ebp)
  8003da:	ff 75 0c             	pushl  0xc(%ebp)
  8003dd:	ff 75 08             	pushl  0x8(%ebp)
  8003e0:	e8 05 00 00 00       	call   8003ea <vprintfmt>
	va_end(ap);
}
  8003e5:	83 c4 10             	add    $0x10,%esp
  8003e8:	c9                   	leave  
  8003e9:	c3                   	ret    

008003ea <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8003ea:	55                   	push   %ebp
  8003eb:	89 e5                	mov    %esp,%ebp
  8003ed:	57                   	push   %edi
  8003ee:	56                   	push   %esi
  8003ef:	53                   	push   %ebx
  8003f0:	83 ec 2c             	sub    $0x2c,%esp
  8003f3:	8b 75 08             	mov    0x8(%ebp),%esi
  8003f6:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8003f9:	8b 7d 10             	mov    0x10(%ebp),%edi
  8003fc:	eb 12                	jmp    800410 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8003fe:	85 c0                	test   %eax,%eax
  800400:	0f 84 68 03 00 00    	je     80076e <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800406:	83 ec 08             	sub    $0x8,%esp
  800409:	53                   	push   %ebx
  80040a:	50                   	push   %eax
  80040b:	ff d6                	call   *%esi
  80040d:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800410:	47                   	inc    %edi
  800411:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800415:	83 f8 25             	cmp    $0x25,%eax
  800418:	75 e4                	jne    8003fe <vprintfmt+0x14>
  80041a:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80041e:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800425:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80042c:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800433:	ba 00 00 00 00       	mov    $0x0,%edx
  800438:	eb 07                	jmp    800441 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80043a:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80043d:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800441:	8d 47 01             	lea    0x1(%edi),%eax
  800444:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800447:	0f b6 0f             	movzbl (%edi),%ecx
  80044a:	8a 07                	mov    (%edi),%al
  80044c:	83 e8 23             	sub    $0x23,%eax
  80044f:	3c 55                	cmp    $0x55,%al
  800451:	0f 87 fe 02 00 00    	ja     800755 <vprintfmt+0x36b>
  800457:	0f b6 c0             	movzbl %al,%eax
  80045a:	ff 24 85 40 21 80 00 	jmp    *0x802140(,%eax,4)
  800461:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800464:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800468:	eb d7                	jmp    800441 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80046a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80046d:	b8 00 00 00 00       	mov    $0x0,%eax
  800472:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800475:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800478:	01 c0                	add    %eax,%eax
  80047a:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80047e:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800481:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800484:	83 fa 09             	cmp    $0x9,%edx
  800487:	77 34                	ja     8004bd <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800489:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80048a:	eb e9                	jmp    800475 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80048c:	8b 45 14             	mov    0x14(%ebp),%eax
  80048f:	8d 48 04             	lea    0x4(%eax),%ecx
  800492:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800495:	8b 00                	mov    (%eax),%eax
  800497:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80049a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80049d:	eb 24                	jmp    8004c3 <vprintfmt+0xd9>
  80049f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004a3:	79 07                	jns    8004ac <vprintfmt+0xc2>
  8004a5:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ac:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004af:	eb 90                	jmp    800441 <vprintfmt+0x57>
  8004b1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8004b4:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8004bb:	eb 84                	jmp    800441 <vprintfmt+0x57>
  8004bd:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8004c0:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8004c3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004c7:	0f 89 74 ff ff ff    	jns    800441 <vprintfmt+0x57>
				width = precision, precision = -1;
  8004cd:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8004d0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004d3:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8004da:	e9 62 ff ff ff       	jmp    800441 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8004df:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8004e3:	e9 59 ff ff ff       	jmp    800441 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8004e8:	8b 45 14             	mov    0x14(%ebp),%eax
  8004eb:	8d 50 04             	lea    0x4(%eax),%edx
  8004ee:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f1:	83 ec 08             	sub    $0x8,%esp
  8004f4:	53                   	push   %ebx
  8004f5:	ff 30                	pushl  (%eax)
  8004f7:	ff d6                	call   *%esi
			break;
  8004f9:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004fc:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8004ff:	e9 0c ff ff ff       	jmp    800410 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800504:	8b 45 14             	mov    0x14(%ebp),%eax
  800507:	8d 50 04             	lea    0x4(%eax),%edx
  80050a:	89 55 14             	mov    %edx,0x14(%ebp)
  80050d:	8b 00                	mov    (%eax),%eax
  80050f:	85 c0                	test   %eax,%eax
  800511:	79 02                	jns    800515 <vprintfmt+0x12b>
  800513:	f7 d8                	neg    %eax
  800515:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800517:	83 f8 0f             	cmp    $0xf,%eax
  80051a:	7f 0b                	jg     800527 <vprintfmt+0x13d>
  80051c:	8b 04 85 a0 22 80 00 	mov    0x8022a0(,%eax,4),%eax
  800523:	85 c0                	test   %eax,%eax
  800525:	75 18                	jne    80053f <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800527:	52                   	push   %edx
  800528:	68 23 20 80 00       	push   $0x802023
  80052d:	53                   	push   %ebx
  80052e:	56                   	push   %esi
  80052f:	e8 99 fe ff ff       	call   8003cd <printfmt>
  800534:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800537:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80053a:	e9 d1 fe ff ff       	jmp    800410 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80053f:	50                   	push   %eax
  800540:	68 d5 23 80 00       	push   $0x8023d5
  800545:	53                   	push   %ebx
  800546:	56                   	push   %esi
  800547:	e8 81 fe ff ff       	call   8003cd <printfmt>
  80054c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80054f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800552:	e9 b9 fe ff ff       	jmp    800410 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800557:	8b 45 14             	mov    0x14(%ebp),%eax
  80055a:	8d 50 04             	lea    0x4(%eax),%edx
  80055d:	89 55 14             	mov    %edx,0x14(%ebp)
  800560:	8b 38                	mov    (%eax),%edi
  800562:	85 ff                	test   %edi,%edi
  800564:	75 05                	jne    80056b <vprintfmt+0x181>
				p = "(null)";
  800566:	bf 1c 20 80 00       	mov    $0x80201c,%edi
			if (width > 0 && padc != '-')
  80056b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80056f:	0f 8e 90 00 00 00    	jle    800605 <vprintfmt+0x21b>
  800575:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800579:	0f 84 8e 00 00 00    	je     80060d <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80057f:	83 ec 08             	sub    $0x8,%esp
  800582:	ff 75 d0             	pushl  -0x30(%ebp)
  800585:	57                   	push   %edi
  800586:	e8 70 02 00 00       	call   8007fb <strnlen>
  80058b:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80058e:	29 c1                	sub    %eax,%ecx
  800590:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800593:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800596:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80059a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80059d:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8005a0:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005a2:	eb 0d                	jmp    8005b1 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8005a4:	83 ec 08             	sub    $0x8,%esp
  8005a7:	53                   	push   %ebx
  8005a8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005ab:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005ad:	4f                   	dec    %edi
  8005ae:	83 c4 10             	add    $0x10,%esp
  8005b1:	85 ff                	test   %edi,%edi
  8005b3:	7f ef                	jg     8005a4 <vprintfmt+0x1ba>
  8005b5:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8005b8:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8005bb:	89 c8                	mov    %ecx,%eax
  8005bd:	85 c9                	test   %ecx,%ecx
  8005bf:	79 05                	jns    8005c6 <vprintfmt+0x1dc>
  8005c1:	b8 00 00 00 00       	mov    $0x0,%eax
  8005c6:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8005c9:	29 c1                	sub    %eax,%ecx
  8005cb:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8005ce:	89 75 08             	mov    %esi,0x8(%ebp)
  8005d1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005d4:	eb 3d                	jmp    800613 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8005d6:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8005da:	74 19                	je     8005f5 <vprintfmt+0x20b>
  8005dc:	0f be c0             	movsbl %al,%eax
  8005df:	83 e8 20             	sub    $0x20,%eax
  8005e2:	83 f8 5e             	cmp    $0x5e,%eax
  8005e5:	76 0e                	jbe    8005f5 <vprintfmt+0x20b>
					putch('?', putdat);
  8005e7:	83 ec 08             	sub    $0x8,%esp
  8005ea:	53                   	push   %ebx
  8005eb:	6a 3f                	push   $0x3f
  8005ed:	ff 55 08             	call   *0x8(%ebp)
  8005f0:	83 c4 10             	add    $0x10,%esp
  8005f3:	eb 0b                	jmp    800600 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8005f5:	83 ec 08             	sub    $0x8,%esp
  8005f8:	53                   	push   %ebx
  8005f9:	52                   	push   %edx
  8005fa:	ff 55 08             	call   *0x8(%ebp)
  8005fd:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800600:	ff 4d e4             	decl   -0x1c(%ebp)
  800603:	eb 0e                	jmp    800613 <vprintfmt+0x229>
  800605:	89 75 08             	mov    %esi,0x8(%ebp)
  800608:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80060b:	eb 06                	jmp    800613 <vprintfmt+0x229>
  80060d:	89 75 08             	mov    %esi,0x8(%ebp)
  800610:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800613:	47                   	inc    %edi
  800614:	8a 47 ff             	mov    -0x1(%edi),%al
  800617:	0f be d0             	movsbl %al,%edx
  80061a:	85 d2                	test   %edx,%edx
  80061c:	74 1d                	je     80063b <vprintfmt+0x251>
  80061e:	85 f6                	test   %esi,%esi
  800620:	78 b4                	js     8005d6 <vprintfmt+0x1ec>
  800622:	4e                   	dec    %esi
  800623:	79 b1                	jns    8005d6 <vprintfmt+0x1ec>
  800625:	8b 75 08             	mov    0x8(%ebp),%esi
  800628:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80062b:	eb 14                	jmp    800641 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80062d:	83 ec 08             	sub    $0x8,%esp
  800630:	53                   	push   %ebx
  800631:	6a 20                	push   $0x20
  800633:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800635:	4f                   	dec    %edi
  800636:	83 c4 10             	add    $0x10,%esp
  800639:	eb 06                	jmp    800641 <vprintfmt+0x257>
  80063b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80063e:	8b 75 08             	mov    0x8(%ebp),%esi
  800641:	85 ff                	test   %edi,%edi
  800643:	7f e8                	jg     80062d <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800645:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800648:	e9 c3 fd ff ff       	jmp    800410 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80064d:	83 fa 01             	cmp    $0x1,%edx
  800650:	7e 16                	jle    800668 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800652:	8b 45 14             	mov    0x14(%ebp),%eax
  800655:	8d 50 08             	lea    0x8(%eax),%edx
  800658:	89 55 14             	mov    %edx,0x14(%ebp)
  80065b:	8b 50 04             	mov    0x4(%eax),%edx
  80065e:	8b 00                	mov    (%eax),%eax
  800660:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800663:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800666:	eb 32                	jmp    80069a <vprintfmt+0x2b0>
	else if (lflag)
  800668:	85 d2                	test   %edx,%edx
  80066a:	74 18                	je     800684 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80066c:	8b 45 14             	mov    0x14(%ebp),%eax
  80066f:	8d 50 04             	lea    0x4(%eax),%edx
  800672:	89 55 14             	mov    %edx,0x14(%ebp)
  800675:	8b 00                	mov    (%eax),%eax
  800677:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80067a:	89 c1                	mov    %eax,%ecx
  80067c:	c1 f9 1f             	sar    $0x1f,%ecx
  80067f:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800682:	eb 16                	jmp    80069a <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800684:	8b 45 14             	mov    0x14(%ebp),%eax
  800687:	8d 50 04             	lea    0x4(%eax),%edx
  80068a:	89 55 14             	mov    %edx,0x14(%ebp)
  80068d:	8b 00                	mov    (%eax),%eax
  80068f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800692:	89 c1                	mov    %eax,%ecx
  800694:	c1 f9 1f             	sar    $0x1f,%ecx
  800697:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80069a:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80069d:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8006a0:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8006a4:	79 76                	jns    80071c <vprintfmt+0x332>
				putch('-', putdat);
  8006a6:	83 ec 08             	sub    $0x8,%esp
  8006a9:	53                   	push   %ebx
  8006aa:	6a 2d                	push   $0x2d
  8006ac:	ff d6                	call   *%esi
				num = -(long long) num;
  8006ae:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8006b1:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8006b4:	f7 d8                	neg    %eax
  8006b6:	83 d2 00             	adc    $0x0,%edx
  8006b9:	f7 da                	neg    %edx
  8006bb:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8006be:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8006c3:	eb 5c                	jmp    800721 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8006c5:	8d 45 14             	lea    0x14(%ebp),%eax
  8006c8:	e8 aa fc ff ff       	call   800377 <getuint>
			base = 10;
  8006cd:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8006d2:	eb 4d                	jmp    800721 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8006d4:	8d 45 14             	lea    0x14(%ebp),%eax
  8006d7:	e8 9b fc ff ff       	call   800377 <getuint>
			base = 8;
  8006dc:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8006e1:	eb 3e                	jmp    800721 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8006e3:	83 ec 08             	sub    $0x8,%esp
  8006e6:	53                   	push   %ebx
  8006e7:	6a 30                	push   $0x30
  8006e9:	ff d6                	call   *%esi
			putch('x', putdat);
  8006eb:	83 c4 08             	add    $0x8,%esp
  8006ee:	53                   	push   %ebx
  8006ef:	6a 78                	push   $0x78
  8006f1:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8006f3:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f6:	8d 50 04             	lea    0x4(%eax),%edx
  8006f9:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006fc:	8b 00                	mov    (%eax),%eax
  8006fe:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800703:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800706:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80070b:	eb 14                	jmp    800721 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80070d:	8d 45 14             	lea    0x14(%ebp),%eax
  800710:	e8 62 fc ff ff       	call   800377 <getuint>
			base = 16;
  800715:	b9 10 00 00 00       	mov    $0x10,%ecx
  80071a:	eb 05                	jmp    800721 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80071c:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800721:	83 ec 0c             	sub    $0xc,%esp
  800724:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800728:	57                   	push   %edi
  800729:	ff 75 e4             	pushl  -0x1c(%ebp)
  80072c:	51                   	push   %ecx
  80072d:	52                   	push   %edx
  80072e:	50                   	push   %eax
  80072f:	89 da                	mov    %ebx,%edx
  800731:	89 f0                	mov    %esi,%eax
  800733:	e8 92 fb ff ff       	call   8002ca <printnum>
			break;
  800738:	83 c4 20             	add    $0x20,%esp
  80073b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80073e:	e9 cd fc ff ff       	jmp    800410 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800743:	83 ec 08             	sub    $0x8,%esp
  800746:	53                   	push   %ebx
  800747:	51                   	push   %ecx
  800748:	ff d6                	call   *%esi
			break;
  80074a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80074d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800750:	e9 bb fc ff ff       	jmp    800410 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800755:	83 ec 08             	sub    $0x8,%esp
  800758:	53                   	push   %ebx
  800759:	6a 25                	push   $0x25
  80075b:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80075d:	83 c4 10             	add    $0x10,%esp
  800760:	eb 01                	jmp    800763 <vprintfmt+0x379>
  800762:	4f                   	dec    %edi
  800763:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800767:	75 f9                	jne    800762 <vprintfmt+0x378>
  800769:	e9 a2 fc ff ff       	jmp    800410 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80076e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800771:	5b                   	pop    %ebx
  800772:	5e                   	pop    %esi
  800773:	5f                   	pop    %edi
  800774:	5d                   	pop    %ebp
  800775:	c3                   	ret    

00800776 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800776:	55                   	push   %ebp
  800777:	89 e5                	mov    %esp,%ebp
  800779:	83 ec 18             	sub    $0x18,%esp
  80077c:	8b 45 08             	mov    0x8(%ebp),%eax
  80077f:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800782:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800785:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800789:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80078c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800793:	85 c0                	test   %eax,%eax
  800795:	74 26                	je     8007bd <vsnprintf+0x47>
  800797:	85 d2                	test   %edx,%edx
  800799:	7e 29                	jle    8007c4 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80079b:	ff 75 14             	pushl  0x14(%ebp)
  80079e:	ff 75 10             	pushl  0x10(%ebp)
  8007a1:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007a4:	50                   	push   %eax
  8007a5:	68 b1 03 80 00       	push   $0x8003b1
  8007aa:	e8 3b fc ff ff       	call   8003ea <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8007af:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8007b2:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8007b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8007b8:	83 c4 10             	add    $0x10,%esp
  8007bb:	eb 0c                	jmp    8007c9 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8007bd:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8007c2:	eb 05                	jmp    8007c9 <vsnprintf+0x53>
  8007c4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8007c9:	c9                   	leave  
  8007ca:	c3                   	ret    

008007cb <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8007cb:	55                   	push   %ebp
  8007cc:	89 e5                	mov    %esp,%ebp
  8007ce:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8007d1:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8007d4:	50                   	push   %eax
  8007d5:	ff 75 10             	pushl  0x10(%ebp)
  8007d8:	ff 75 0c             	pushl  0xc(%ebp)
  8007db:	ff 75 08             	pushl  0x8(%ebp)
  8007de:	e8 93 ff ff ff       	call   800776 <vsnprintf>
	va_end(ap);

	return rc;
}
  8007e3:	c9                   	leave  
  8007e4:	c3                   	ret    

008007e5 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8007e5:	55                   	push   %ebp
  8007e6:	89 e5                	mov    %esp,%ebp
  8007e8:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8007eb:	b8 00 00 00 00       	mov    $0x0,%eax
  8007f0:	eb 01                	jmp    8007f3 <strlen+0xe>
		n++;
  8007f2:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8007f3:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8007f7:	75 f9                	jne    8007f2 <strlen+0xd>
		n++;
	return n;
}
  8007f9:	5d                   	pop    %ebp
  8007fa:	c3                   	ret    

008007fb <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8007fb:	55                   	push   %ebp
  8007fc:	89 e5                	mov    %esp,%ebp
  8007fe:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800801:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800804:	ba 00 00 00 00       	mov    $0x0,%edx
  800809:	eb 01                	jmp    80080c <strnlen+0x11>
		n++;
  80080b:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80080c:	39 c2                	cmp    %eax,%edx
  80080e:	74 08                	je     800818 <strnlen+0x1d>
  800810:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800814:	75 f5                	jne    80080b <strnlen+0x10>
  800816:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800818:	5d                   	pop    %ebp
  800819:	c3                   	ret    

0080081a <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80081a:	55                   	push   %ebp
  80081b:	89 e5                	mov    %esp,%ebp
  80081d:	53                   	push   %ebx
  80081e:	8b 45 08             	mov    0x8(%ebp),%eax
  800821:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800824:	89 c2                	mov    %eax,%edx
  800826:	42                   	inc    %edx
  800827:	41                   	inc    %ecx
  800828:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80082b:	88 5a ff             	mov    %bl,-0x1(%edx)
  80082e:	84 db                	test   %bl,%bl
  800830:	75 f4                	jne    800826 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800832:	5b                   	pop    %ebx
  800833:	5d                   	pop    %ebp
  800834:	c3                   	ret    

00800835 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800835:	55                   	push   %ebp
  800836:	89 e5                	mov    %esp,%ebp
  800838:	53                   	push   %ebx
  800839:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80083c:	53                   	push   %ebx
  80083d:	e8 a3 ff ff ff       	call   8007e5 <strlen>
  800842:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800845:	ff 75 0c             	pushl  0xc(%ebp)
  800848:	01 d8                	add    %ebx,%eax
  80084a:	50                   	push   %eax
  80084b:	e8 ca ff ff ff       	call   80081a <strcpy>
	return dst;
}
  800850:	89 d8                	mov    %ebx,%eax
  800852:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800855:	c9                   	leave  
  800856:	c3                   	ret    

00800857 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800857:	55                   	push   %ebp
  800858:	89 e5                	mov    %esp,%ebp
  80085a:	56                   	push   %esi
  80085b:	53                   	push   %ebx
  80085c:	8b 75 08             	mov    0x8(%ebp),%esi
  80085f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800862:	89 f3                	mov    %esi,%ebx
  800864:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800867:	89 f2                	mov    %esi,%edx
  800869:	eb 0c                	jmp    800877 <strncpy+0x20>
		*dst++ = *src;
  80086b:	42                   	inc    %edx
  80086c:	8a 01                	mov    (%ecx),%al
  80086e:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800871:	80 39 01             	cmpb   $0x1,(%ecx)
  800874:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800877:	39 da                	cmp    %ebx,%edx
  800879:	75 f0                	jne    80086b <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80087b:	89 f0                	mov    %esi,%eax
  80087d:	5b                   	pop    %ebx
  80087e:	5e                   	pop    %esi
  80087f:	5d                   	pop    %ebp
  800880:	c3                   	ret    

00800881 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800881:	55                   	push   %ebp
  800882:	89 e5                	mov    %esp,%ebp
  800884:	56                   	push   %esi
  800885:	53                   	push   %ebx
  800886:	8b 75 08             	mov    0x8(%ebp),%esi
  800889:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80088c:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80088f:	85 c0                	test   %eax,%eax
  800891:	74 1e                	je     8008b1 <strlcpy+0x30>
  800893:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800897:	89 f2                	mov    %esi,%edx
  800899:	eb 05                	jmp    8008a0 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80089b:	42                   	inc    %edx
  80089c:	41                   	inc    %ecx
  80089d:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8008a0:	39 c2                	cmp    %eax,%edx
  8008a2:	74 08                	je     8008ac <strlcpy+0x2b>
  8008a4:	8a 19                	mov    (%ecx),%bl
  8008a6:	84 db                	test   %bl,%bl
  8008a8:	75 f1                	jne    80089b <strlcpy+0x1a>
  8008aa:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8008ac:	c6 00 00             	movb   $0x0,(%eax)
  8008af:	eb 02                	jmp    8008b3 <strlcpy+0x32>
  8008b1:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8008b3:	29 f0                	sub    %esi,%eax
}
  8008b5:	5b                   	pop    %ebx
  8008b6:	5e                   	pop    %esi
  8008b7:	5d                   	pop    %ebp
  8008b8:	c3                   	ret    

008008b9 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8008b9:	55                   	push   %ebp
  8008ba:	89 e5                	mov    %esp,%ebp
  8008bc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8008bf:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8008c2:	eb 02                	jmp    8008c6 <strcmp+0xd>
		p++, q++;
  8008c4:	41                   	inc    %ecx
  8008c5:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8008c6:	8a 01                	mov    (%ecx),%al
  8008c8:	84 c0                	test   %al,%al
  8008ca:	74 04                	je     8008d0 <strcmp+0x17>
  8008cc:	3a 02                	cmp    (%edx),%al
  8008ce:	74 f4                	je     8008c4 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8008d0:	0f b6 c0             	movzbl %al,%eax
  8008d3:	0f b6 12             	movzbl (%edx),%edx
  8008d6:	29 d0                	sub    %edx,%eax
}
  8008d8:	5d                   	pop    %ebp
  8008d9:	c3                   	ret    

008008da <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8008da:	55                   	push   %ebp
  8008db:	89 e5                	mov    %esp,%ebp
  8008dd:	53                   	push   %ebx
  8008de:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008e4:	89 c3                	mov    %eax,%ebx
  8008e6:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8008e9:	eb 02                	jmp    8008ed <strncmp+0x13>
		n--, p++, q++;
  8008eb:	40                   	inc    %eax
  8008ec:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8008ed:	39 d8                	cmp    %ebx,%eax
  8008ef:	74 14                	je     800905 <strncmp+0x2b>
  8008f1:	8a 08                	mov    (%eax),%cl
  8008f3:	84 c9                	test   %cl,%cl
  8008f5:	74 04                	je     8008fb <strncmp+0x21>
  8008f7:	3a 0a                	cmp    (%edx),%cl
  8008f9:	74 f0                	je     8008eb <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8008fb:	0f b6 00             	movzbl (%eax),%eax
  8008fe:	0f b6 12             	movzbl (%edx),%edx
  800901:	29 d0                	sub    %edx,%eax
  800903:	eb 05                	jmp    80090a <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800905:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80090a:	5b                   	pop    %ebx
  80090b:	5d                   	pop    %ebp
  80090c:	c3                   	ret    

0080090d <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80090d:	55                   	push   %ebp
  80090e:	89 e5                	mov    %esp,%ebp
  800910:	8b 45 08             	mov    0x8(%ebp),%eax
  800913:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800916:	eb 05                	jmp    80091d <strchr+0x10>
		if (*s == c)
  800918:	38 ca                	cmp    %cl,%dl
  80091a:	74 0c                	je     800928 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80091c:	40                   	inc    %eax
  80091d:	8a 10                	mov    (%eax),%dl
  80091f:	84 d2                	test   %dl,%dl
  800921:	75 f5                	jne    800918 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800923:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800928:	5d                   	pop    %ebp
  800929:	c3                   	ret    

0080092a <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80092a:	55                   	push   %ebp
  80092b:	89 e5                	mov    %esp,%ebp
  80092d:	8b 45 08             	mov    0x8(%ebp),%eax
  800930:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800933:	eb 05                	jmp    80093a <strfind+0x10>
		if (*s == c)
  800935:	38 ca                	cmp    %cl,%dl
  800937:	74 07                	je     800940 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800939:	40                   	inc    %eax
  80093a:	8a 10                	mov    (%eax),%dl
  80093c:	84 d2                	test   %dl,%dl
  80093e:	75 f5                	jne    800935 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800940:	5d                   	pop    %ebp
  800941:	c3                   	ret    

00800942 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800942:	55                   	push   %ebp
  800943:	89 e5                	mov    %esp,%ebp
  800945:	57                   	push   %edi
  800946:	56                   	push   %esi
  800947:	53                   	push   %ebx
  800948:	8b 7d 08             	mov    0x8(%ebp),%edi
  80094b:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80094e:	85 c9                	test   %ecx,%ecx
  800950:	74 36                	je     800988 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800952:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800958:	75 28                	jne    800982 <memset+0x40>
  80095a:	f6 c1 03             	test   $0x3,%cl
  80095d:	75 23                	jne    800982 <memset+0x40>
		c &= 0xFF;
  80095f:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800963:	89 d3                	mov    %edx,%ebx
  800965:	c1 e3 08             	shl    $0x8,%ebx
  800968:	89 d6                	mov    %edx,%esi
  80096a:	c1 e6 18             	shl    $0x18,%esi
  80096d:	89 d0                	mov    %edx,%eax
  80096f:	c1 e0 10             	shl    $0x10,%eax
  800972:	09 f0                	or     %esi,%eax
  800974:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800976:	89 d8                	mov    %ebx,%eax
  800978:	09 d0                	or     %edx,%eax
  80097a:	c1 e9 02             	shr    $0x2,%ecx
  80097d:	fc                   	cld    
  80097e:	f3 ab                	rep stos %eax,%es:(%edi)
  800980:	eb 06                	jmp    800988 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800982:	8b 45 0c             	mov    0xc(%ebp),%eax
  800985:	fc                   	cld    
  800986:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800988:	89 f8                	mov    %edi,%eax
  80098a:	5b                   	pop    %ebx
  80098b:	5e                   	pop    %esi
  80098c:	5f                   	pop    %edi
  80098d:	5d                   	pop    %ebp
  80098e:	c3                   	ret    

0080098f <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80098f:	55                   	push   %ebp
  800990:	89 e5                	mov    %esp,%ebp
  800992:	57                   	push   %edi
  800993:	56                   	push   %esi
  800994:	8b 45 08             	mov    0x8(%ebp),%eax
  800997:	8b 75 0c             	mov    0xc(%ebp),%esi
  80099a:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80099d:	39 c6                	cmp    %eax,%esi
  80099f:	73 33                	jae    8009d4 <memmove+0x45>
  8009a1:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8009a4:	39 d0                	cmp    %edx,%eax
  8009a6:	73 2c                	jae    8009d4 <memmove+0x45>
		s += n;
		d += n;
  8009a8:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009ab:	89 d6                	mov    %edx,%esi
  8009ad:	09 fe                	or     %edi,%esi
  8009af:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8009b5:	75 13                	jne    8009ca <memmove+0x3b>
  8009b7:	f6 c1 03             	test   $0x3,%cl
  8009ba:	75 0e                	jne    8009ca <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8009bc:	83 ef 04             	sub    $0x4,%edi
  8009bf:	8d 72 fc             	lea    -0x4(%edx),%esi
  8009c2:	c1 e9 02             	shr    $0x2,%ecx
  8009c5:	fd                   	std    
  8009c6:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8009c8:	eb 07                	jmp    8009d1 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8009ca:	4f                   	dec    %edi
  8009cb:	8d 72 ff             	lea    -0x1(%edx),%esi
  8009ce:	fd                   	std    
  8009cf:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8009d1:	fc                   	cld    
  8009d2:	eb 1d                	jmp    8009f1 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8009d4:	89 f2                	mov    %esi,%edx
  8009d6:	09 c2                	or     %eax,%edx
  8009d8:	f6 c2 03             	test   $0x3,%dl
  8009db:	75 0f                	jne    8009ec <memmove+0x5d>
  8009dd:	f6 c1 03             	test   $0x3,%cl
  8009e0:	75 0a                	jne    8009ec <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8009e2:	c1 e9 02             	shr    $0x2,%ecx
  8009e5:	89 c7                	mov    %eax,%edi
  8009e7:	fc                   	cld    
  8009e8:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8009ea:	eb 05                	jmp    8009f1 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8009ec:	89 c7                	mov    %eax,%edi
  8009ee:	fc                   	cld    
  8009ef:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8009f1:	5e                   	pop    %esi
  8009f2:	5f                   	pop    %edi
  8009f3:	5d                   	pop    %ebp
  8009f4:	c3                   	ret    

008009f5 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8009f5:	55                   	push   %ebp
  8009f6:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8009f8:	ff 75 10             	pushl  0x10(%ebp)
  8009fb:	ff 75 0c             	pushl  0xc(%ebp)
  8009fe:	ff 75 08             	pushl  0x8(%ebp)
  800a01:	e8 89 ff ff ff       	call   80098f <memmove>
}
  800a06:	c9                   	leave  
  800a07:	c3                   	ret    

00800a08 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800a08:	55                   	push   %ebp
  800a09:	89 e5                	mov    %esp,%ebp
  800a0b:	56                   	push   %esi
  800a0c:	53                   	push   %ebx
  800a0d:	8b 45 08             	mov    0x8(%ebp),%eax
  800a10:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a13:	89 c6                	mov    %eax,%esi
  800a15:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a18:	eb 14                	jmp    800a2e <memcmp+0x26>
		if (*s1 != *s2)
  800a1a:	8a 08                	mov    (%eax),%cl
  800a1c:	8a 1a                	mov    (%edx),%bl
  800a1e:	38 d9                	cmp    %bl,%cl
  800a20:	74 0a                	je     800a2c <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800a22:	0f b6 c1             	movzbl %cl,%eax
  800a25:	0f b6 db             	movzbl %bl,%ebx
  800a28:	29 d8                	sub    %ebx,%eax
  800a2a:	eb 0b                	jmp    800a37 <memcmp+0x2f>
		s1++, s2++;
  800a2c:	40                   	inc    %eax
  800a2d:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800a2e:	39 f0                	cmp    %esi,%eax
  800a30:	75 e8                	jne    800a1a <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800a32:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a37:	5b                   	pop    %ebx
  800a38:	5e                   	pop    %esi
  800a39:	5d                   	pop    %ebp
  800a3a:	c3                   	ret    

00800a3b <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800a3b:	55                   	push   %ebp
  800a3c:	89 e5                	mov    %esp,%ebp
  800a3e:	53                   	push   %ebx
  800a3f:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800a42:	89 c1                	mov    %eax,%ecx
  800a44:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800a47:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a4b:	eb 08                	jmp    800a55 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a4d:	0f b6 10             	movzbl (%eax),%edx
  800a50:	39 da                	cmp    %ebx,%edx
  800a52:	74 05                	je     800a59 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a54:	40                   	inc    %eax
  800a55:	39 c8                	cmp    %ecx,%eax
  800a57:	72 f4                	jb     800a4d <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800a59:	5b                   	pop    %ebx
  800a5a:	5d                   	pop    %ebp
  800a5b:	c3                   	ret    

00800a5c <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800a5c:	55                   	push   %ebp
  800a5d:	89 e5                	mov    %esp,%ebp
  800a5f:	57                   	push   %edi
  800a60:	56                   	push   %esi
  800a61:	53                   	push   %ebx
  800a62:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a65:	eb 01                	jmp    800a68 <strtol+0xc>
		s++;
  800a67:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a68:	8a 01                	mov    (%ecx),%al
  800a6a:	3c 20                	cmp    $0x20,%al
  800a6c:	74 f9                	je     800a67 <strtol+0xb>
  800a6e:	3c 09                	cmp    $0x9,%al
  800a70:	74 f5                	je     800a67 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800a72:	3c 2b                	cmp    $0x2b,%al
  800a74:	75 08                	jne    800a7e <strtol+0x22>
		s++;
  800a76:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a77:	bf 00 00 00 00       	mov    $0x0,%edi
  800a7c:	eb 11                	jmp    800a8f <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800a7e:	3c 2d                	cmp    $0x2d,%al
  800a80:	75 08                	jne    800a8a <strtol+0x2e>
		s++, neg = 1;
  800a82:	41                   	inc    %ecx
  800a83:	bf 01 00 00 00       	mov    $0x1,%edi
  800a88:	eb 05                	jmp    800a8f <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a8a:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a8f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a93:	0f 84 87 00 00 00    	je     800b20 <strtol+0xc4>
  800a99:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a9d:	75 27                	jne    800ac6 <strtol+0x6a>
  800a9f:	80 39 30             	cmpb   $0x30,(%ecx)
  800aa2:	75 22                	jne    800ac6 <strtol+0x6a>
  800aa4:	e9 88 00 00 00       	jmp    800b31 <strtol+0xd5>
		s += 2, base = 16;
  800aa9:	83 c1 02             	add    $0x2,%ecx
  800aac:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800ab3:	eb 11                	jmp    800ac6 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800ab5:	41                   	inc    %ecx
  800ab6:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800abd:	eb 07                	jmp    800ac6 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800abf:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800ac6:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800acb:	8a 11                	mov    (%ecx),%dl
  800acd:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800ad0:	80 fb 09             	cmp    $0x9,%bl
  800ad3:	77 08                	ja     800add <strtol+0x81>
			dig = *s - '0';
  800ad5:	0f be d2             	movsbl %dl,%edx
  800ad8:	83 ea 30             	sub    $0x30,%edx
  800adb:	eb 22                	jmp    800aff <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800add:	8d 72 9f             	lea    -0x61(%edx),%esi
  800ae0:	89 f3                	mov    %esi,%ebx
  800ae2:	80 fb 19             	cmp    $0x19,%bl
  800ae5:	77 08                	ja     800aef <strtol+0x93>
			dig = *s - 'a' + 10;
  800ae7:	0f be d2             	movsbl %dl,%edx
  800aea:	83 ea 57             	sub    $0x57,%edx
  800aed:	eb 10                	jmp    800aff <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800aef:	8d 72 bf             	lea    -0x41(%edx),%esi
  800af2:	89 f3                	mov    %esi,%ebx
  800af4:	80 fb 19             	cmp    $0x19,%bl
  800af7:	77 14                	ja     800b0d <strtol+0xb1>
			dig = *s - 'A' + 10;
  800af9:	0f be d2             	movsbl %dl,%edx
  800afc:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800aff:	3b 55 10             	cmp    0x10(%ebp),%edx
  800b02:	7d 09                	jge    800b0d <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800b04:	41                   	inc    %ecx
  800b05:	0f af 45 10          	imul   0x10(%ebp),%eax
  800b09:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800b0b:	eb be                	jmp    800acb <strtol+0x6f>

	if (endptr)
  800b0d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800b11:	74 05                	je     800b18 <strtol+0xbc>
		*endptr = (char *) s;
  800b13:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b16:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800b18:	85 ff                	test   %edi,%edi
  800b1a:	74 21                	je     800b3d <strtol+0xe1>
  800b1c:	f7 d8                	neg    %eax
  800b1e:	eb 1d                	jmp    800b3d <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b20:	80 39 30             	cmpb   $0x30,(%ecx)
  800b23:	75 9a                	jne    800abf <strtol+0x63>
  800b25:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b29:	0f 84 7a ff ff ff    	je     800aa9 <strtol+0x4d>
  800b2f:	eb 84                	jmp    800ab5 <strtol+0x59>
  800b31:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800b35:	0f 84 6e ff ff ff    	je     800aa9 <strtol+0x4d>
  800b3b:	eb 89                	jmp    800ac6 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800b3d:	5b                   	pop    %ebx
  800b3e:	5e                   	pop    %esi
  800b3f:	5f                   	pop    %edi
  800b40:	5d                   	pop    %ebp
  800b41:	c3                   	ret    

00800b42 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800b42:	55                   	push   %ebp
  800b43:	89 e5                	mov    %esp,%ebp
  800b45:	57                   	push   %edi
  800b46:	56                   	push   %esi
  800b47:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b48:	b8 00 00 00 00       	mov    $0x0,%eax
  800b4d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b50:	8b 55 08             	mov    0x8(%ebp),%edx
  800b53:	89 c3                	mov    %eax,%ebx
  800b55:	89 c7                	mov    %eax,%edi
  800b57:	89 c6                	mov    %eax,%esi
  800b59:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800b5b:	5b                   	pop    %ebx
  800b5c:	5e                   	pop    %esi
  800b5d:	5f                   	pop    %edi
  800b5e:	5d                   	pop    %ebp
  800b5f:	c3                   	ret    

00800b60 <sys_cgetc>:

int
sys_cgetc(void)
{
  800b60:	55                   	push   %ebp
  800b61:	89 e5                	mov    %esp,%ebp
  800b63:	57                   	push   %edi
  800b64:	56                   	push   %esi
  800b65:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b66:	ba 00 00 00 00       	mov    $0x0,%edx
  800b6b:	b8 01 00 00 00       	mov    $0x1,%eax
  800b70:	89 d1                	mov    %edx,%ecx
  800b72:	89 d3                	mov    %edx,%ebx
  800b74:	89 d7                	mov    %edx,%edi
  800b76:	89 d6                	mov    %edx,%esi
  800b78:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800b7a:	5b                   	pop    %ebx
  800b7b:	5e                   	pop    %esi
  800b7c:	5f                   	pop    %edi
  800b7d:	5d                   	pop    %ebp
  800b7e:	c3                   	ret    

00800b7f <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800b7f:	55                   	push   %ebp
  800b80:	89 e5                	mov    %esp,%ebp
  800b82:	57                   	push   %edi
  800b83:	56                   	push   %esi
  800b84:	53                   	push   %ebx
  800b85:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b88:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b8d:	b8 03 00 00 00       	mov    $0x3,%eax
  800b92:	8b 55 08             	mov    0x8(%ebp),%edx
  800b95:	89 cb                	mov    %ecx,%ebx
  800b97:	89 cf                	mov    %ecx,%edi
  800b99:	89 ce                	mov    %ecx,%esi
  800b9b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b9d:	85 c0                	test   %eax,%eax
  800b9f:	7e 17                	jle    800bb8 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ba1:	83 ec 0c             	sub    $0xc,%esp
  800ba4:	50                   	push   %eax
  800ba5:	6a 03                	push   $0x3
  800ba7:	68 ff 22 80 00       	push   $0x8022ff
  800bac:	6a 23                	push   $0x23
  800bae:	68 1c 23 80 00       	push   $0x80231c
  800bb3:	e8 26 f6 ff ff       	call   8001de <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800bb8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bbb:	5b                   	pop    %ebx
  800bbc:	5e                   	pop    %esi
  800bbd:	5f                   	pop    %edi
  800bbe:	5d                   	pop    %ebp
  800bbf:	c3                   	ret    

00800bc0 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800bc0:	55                   	push   %ebp
  800bc1:	89 e5                	mov    %esp,%ebp
  800bc3:	57                   	push   %edi
  800bc4:	56                   	push   %esi
  800bc5:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc6:	ba 00 00 00 00       	mov    $0x0,%edx
  800bcb:	b8 02 00 00 00       	mov    $0x2,%eax
  800bd0:	89 d1                	mov    %edx,%ecx
  800bd2:	89 d3                	mov    %edx,%ebx
  800bd4:	89 d7                	mov    %edx,%edi
  800bd6:	89 d6                	mov    %edx,%esi
  800bd8:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800bda:	5b                   	pop    %ebx
  800bdb:	5e                   	pop    %esi
  800bdc:	5f                   	pop    %edi
  800bdd:	5d                   	pop    %ebp
  800bde:	c3                   	ret    

00800bdf <sys_yield>:

void
sys_yield(void)
{
  800bdf:	55                   	push   %ebp
  800be0:	89 e5                	mov    %esp,%ebp
  800be2:	57                   	push   %edi
  800be3:	56                   	push   %esi
  800be4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800be5:	ba 00 00 00 00       	mov    $0x0,%edx
  800bea:	b8 0b 00 00 00       	mov    $0xb,%eax
  800bef:	89 d1                	mov    %edx,%ecx
  800bf1:	89 d3                	mov    %edx,%ebx
  800bf3:	89 d7                	mov    %edx,%edi
  800bf5:	89 d6                	mov    %edx,%esi
  800bf7:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800bf9:	5b                   	pop    %ebx
  800bfa:	5e                   	pop    %esi
  800bfb:	5f                   	pop    %edi
  800bfc:	5d                   	pop    %ebp
  800bfd:	c3                   	ret    

00800bfe <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800bfe:	55                   	push   %ebp
  800bff:	89 e5                	mov    %esp,%ebp
  800c01:	57                   	push   %edi
  800c02:	56                   	push   %esi
  800c03:	53                   	push   %ebx
  800c04:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c07:	be 00 00 00 00       	mov    $0x0,%esi
  800c0c:	b8 04 00 00 00       	mov    $0x4,%eax
  800c11:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c14:	8b 55 08             	mov    0x8(%ebp),%edx
  800c17:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c1a:	89 f7                	mov    %esi,%edi
  800c1c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c1e:	85 c0                	test   %eax,%eax
  800c20:	7e 17                	jle    800c39 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c22:	83 ec 0c             	sub    $0xc,%esp
  800c25:	50                   	push   %eax
  800c26:	6a 04                	push   $0x4
  800c28:	68 ff 22 80 00       	push   $0x8022ff
  800c2d:	6a 23                	push   $0x23
  800c2f:	68 1c 23 80 00       	push   $0x80231c
  800c34:	e8 a5 f5 ff ff       	call   8001de <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800c39:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c3c:	5b                   	pop    %ebx
  800c3d:	5e                   	pop    %esi
  800c3e:	5f                   	pop    %edi
  800c3f:	5d                   	pop    %ebp
  800c40:	c3                   	ret    

00800c41 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800c41:	55                   	push   %ebp
  800c42:	89 e5                	mov    %esp,%ebp
  800c44:	57                   	push   %edi
  800c45:	56                   	push   %esi
  800c46:	53                   	push   %ebx
  800c47:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c4a:	b8 05 00 00 00       	mov    $0x5,%eax
  800c4f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c52:	8b 55 08             	mov    0x8(%ebp),%edx
  800c55:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c58:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c5b:	8b 75 18             	mov    0x18(%ebp),%esi
  800c5e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c60:	85 c0                	test   %eax,%eax
  800c62:	7e 17                	jle    800c7b <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c64:	83 ec 0c             	sub    $0xc,%esp
  800c67:	50                   	push   %eax
  800c68:	6a 05                	push   $0x5
  800c6a:	68 ff 22 80 00       	push   $0x8022ff
  800c6f:	6a 23                	push   $0x23
  800c71:	68 1c 23 80 00       	push   $0x80231c
  800c76:	e8 63 f5 ff ff       	call   8001de <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800c7b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c7e:	5b                   	pop    %ebx
  800c7f:	5e                   	pop    %esi
  800c80:	5f                   	pop    %edi
  800c81:	5d                   	pop    %ebp
  800c82:	c3                   	ret    

00800c83 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800c83:	55                   	push   %ebp
  800c84:	89 e5                	mov    %esp,%ebp
  800c86:	57                   	push   %edi
  800c87:	56                   	push   %esi
  800c88:	53                   	push   %ebx
  800c89:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c8c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c91:	b8 06 00 00 00       	mov    $0x6,%eax
  800c96:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c99:	8b 55 08             	mov    0x8(%ebp),%edx
  800c9c:	89 df                	mov    %ebx,%edi
  800c9e:	89 de                	mov    %ebx,%esi
  800ca0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ca2:	85 c0                	test   %eax,%eax
  800ca4:	7e 17                	jle    800cbd <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ca6:	83 ec 0c             	sub    $0xc,%esp
  800ca9:	50                   	push   %eax
  800caa:	6a 06                	push   $0x6
  800cac:	68 ff 22 80 00       	push   $0x8022ff
  800cb1:	6a 23                	push   $0x23
  800cb3:	68 1c 23 80 00       	push   $0x80231c
  800cb8:	e8 21 f5 ff ff       	call   8001de <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800cbd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cc0:	5b                   	pop    %ebx
  800cc1:	5e                   	pop    %esi
  800cc2:	5f                   	pop    %edi
  800cc3:	5d                   	pop    %ebp
  800cc4:	c3                   	ret    

00800cc5 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800cc5:	55                   	push   %ebp
  800cc6:	89 e5                	mov    %esp,%ebp
  800cc8:	57                   	push   %edi
  800cc9:	56                   	push   %esi
  800cca:	53                   	push   %ebx
  800ccb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cce:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cd3:	b8 08 00 00 00       	mov    $0x8,%eax
  800cd8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cdb:	8b 55 08             	mov    0x8(%ebp),%edx
  800cde:	89 df                	mov    %ebx,%edi
  800ce0:	89 de                	mov    %ebx,%esi
  800ce2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ce4:	85 c0                	test   %eax,%eax
  800ce6:	7e 17                	jle    800cff <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ce8:	83 ec 0c             	sub    $0xc,%esp
  800ceb:	50                   	push   %eax
  800cec:	6a 08                	push   $0x8
  800cee:	68 ff 22 80 00       	push   $0x8022ff
  800cf3:	6a 23                	push   $0x23
  800cf5:	68 1c 23 80 00       	push   $0x80231c
  800cfa:	e8 df f4 ff ff       	call   8001de <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800cff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d02:	5b                   	pop    %ebx
  800d03:	5e                   	pop    %esi
  800d04:	5f                   	pop    %edi
  800d05:	5d                   	pop    %ebp
  800d06:	c3                   	ret    

00800d07 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800d07:	55                   	push   %ebp
  800d08:	89 e5                	mov    %esp,%ebp
  800d0a:	57                   	push   %edi
  800d0b:	56                   	push   %esi
  800d0c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d0d:	ba 00 00 00 00       	mov    $0x0,%edx
  800d12:	b8 0c 00 00 00       	mov    $0xc,%eax
  800d17:	89 d1                	mov    %edx,%ecx
  800d19:	89 d3                	mov    %edx,%ebx
  800d1b:	89 d7                	mov    %edx,%edi
  800d1d:	89 d6                	mov    %edx,%esi
  800d1f:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800d21:	5b                   	pop    %ebx
  800d22:	5e                   	pop    %esi
  800d23:	5f                   	pop    %edi
  800d24:	5d                   	pop    %ebp
  800d25:	c3                   	ret    

00800d26 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800d26:	55                   	push   %ebp
  800d27:	89 e5                	mov    %esp,%ebp
  800d29:	57                   	push   %edi
  800d2a:	56                   	push   %esi
  800d2b:	53                   	push   %ebx
  800d2c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d2f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d34:	b8 09 00 00 00       	mov    $0x9,%eax
  800d39:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d3c:	8b 55 08             	mov    0x8(%ebp),%edx
  800d3f:	89 df                	mov    %ebx,%edi
  800d41:	89 de                	mov    %ebx,%esi
  800d43:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d45:	85 c0                	test   %eax,%eax
  800d47:	7e 17                	jle    800d60 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d49:	83 ec 0c             	sub    $0xc,%esp
  800d4c:	50                   	push   %eax
  800d4d:	6a 09                	push   $0x9
  800d4f:	68 ff 22 80 00       	push   $0x8022ff
  800d54:	6a 23                	push   $0x23
  800d56:	68 1c 23 80 00       	push   $0x80231c
  800d5b:	e8 7e f4 ff ff       	call   8001de <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800d60:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d63:	5b                   	pop    %ebx
  800d64:	5e                   	pop    %esi
  800d65:	5f                   	pop    %edi
  800d66:	5d                   	pop    %ebp
  800d67:	c3                   	ret    

00800d68 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800d68:	55                   	push   %ebp
  800d69:	89 e5                	mov    %esp,%ebp
  800d6b:	57                   	push   %edi
  800d6c:	56                   	push   %esi
  800d6d:	53                   	push   %ebx
  800d6e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d71:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d76:	b8 0a 00 00 00       	mov    $0xa,%eax
  800d7b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d7e:	8b 55 08             	mov    0x8(%ebp),%edx
  800d81:	89 df                	mov    %ebx,%edi
  800d83:	89 de                	mov    %ebx,%esi
  800d85:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d87:	85 c0                	test   %eax,%eax
  800d89:	7e 17                	jle    800da2 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d8b:	83 ec 0c             	sub    $0xc,%esp
  800d8e:	50                   	push   %eax
  800d8f:	6a 0a                	push   $0xa
  800d91:	68 ff 22 80 00       	push   $0x8022ff
  800d96:	6a 23                	push   $0x23
  800d98:	68 1c 23 80 00       	push   $0x80231c
  800d9d:	e8 3c f4 ff ff       	call   8001de <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800da2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800da5:	5b                   	pop    %ebx
  800da6:	5e                   	pop    %esi
  800da7:	5f                   	pop    %edi
  800da8:	5d                   	pop    %ebp
  800da9:	c3                   	ret    

00800daa <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800daa:	55                   	push   %ebp
  800dab:	89 e5                	mov    %esp,%ebp
  800dad:	57                   	push   %edi
  800dae:	56                   	push   %esi
  800daf:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800db0:	be 00 00 00 00       	mov    $0x0,%esi
  800db5:	b8 0d 00 00 00       	mov    $0xd,%eax
  800dba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800dbd:	8b 55 08             	mov    0x8(%ebp),%edx
  800dc0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800dc3:	8b 7d 14             	mov    0x14(%ebp),%edi
  800dc6:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800dc8:	5b                   	pop    %ebx
  800dc9:	5e                   	pop    %esi
  800dca:	5f                   	pop    %edi
  800dcb:	5d                   	pop    %ebp
  800dcc:	c3                   	ret    

00800dcd <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800dcd:	55                   	push   %ebp
  800dce:	89 e5                	mov    %esp,%ebp
  800dd0:	57                   	push   %edi
  800dd1:	56                   	push   %esi
  800dd2:	53                   	push   %ebx
  800dd3:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dd6:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ddb:	b8 0e 00 00 00       	mov    $0xe,%eax
  800de0:	8b 55 08             	mov    0x8(%ebp),%edx
  800de3:	89 cb                	mov    %ecx,%ebx
  800de5:	89 cf                	mov    %ecx,%edi
  800de7:	89 ce                	mov    %ecx,%esi
  800de9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800deb:	85 c0                	test   %eax,%eax
  800ded:	7e 17                	jle    800e06 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800def:	83 ec 0c             	sub    $0xc,%esp
  800df2:	50                   	push   %eax
  800df3:	6a 0e                	push   $0xe
  800df5:	68 ff 22 80 00       	push   $0x8022ff
  800dfa:	6a 23                	push   $0x23
  800dfc:	68 1c 23 80 00       	push   $0x80231c
  800e01:	e8 d8 f3 ff ff       	call   8001de <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800e06:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e09:	5b                   	pop    %ebx
  800e0a:	5e                   	pop    %esi
  800e0b:	5f                   	pop    %edi
  800e0c:	5d                   	pop    %ebp
  800e0d:	c3                   	ret    

00800e0e <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800e0e:	55                   	push   %ebp
  800e0f:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  800e11:	8b 45 08             	mov    0x8(%ebp),%eax
  800e14:	05 00 00 00 30       	add    $0x30000000,%eax
  800e19:	c1 e8 0c             	shr    $0xc,%eax
}
  800e1c:	5d                   	pop    %ebp
  800e1d:	c3                   	ret    

00800e1e <fd2data>:

char*
fd2data(struct Fd *fd)
{
  800e1e:	55                   	push   %ebp
  800e1f:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  800e21:	8b 45 08             	mov    0x8(%ebp),%eax
  800e24:	05 00 00 00 30       	add    $0x30000000,%eax
  800e29:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800e2e:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  800e33:	5d                   	pop    %ebp
  800e34:	c3                   	ret    

00800e35 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  800e35:	55                   	push   %ebp
  800e36:	89 e5                	mov    %esp,%ebp
  800e38:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800e3b:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  800e40:	89 c2                	mov    %eax,%edx
  800e42:	c1 ea 16             	shr    $0x16,%edx
  800e45:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800e4c:	f6 c2 01             	test   $0x1,%dl
  800e4f:	74 11                	je     800e62 <fd_alloc+0x2d>
  800e51:	89 c2                	mov    %eax,%edx
  800e53:	c1 ea 0c             	shr    $0xc,%edx
  800e56:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800e5d:	f6 c2 01             	test   $0x1,%dl
  800e60:	75 09                	jne    800e6b <fd_alloc+0x36>
			*fd_store = fd;
  800e62:	89 01                	mov    %eax,(%ecx)
			return 0;
  800e64:	b8 00 00 00 00       	mov    $0x0,%eax
  800e69:	eb 17                	jmp    800e82 <fd_alloc+0x4d>
  800e6b:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  800e70:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  800e75:	75 c9                	jne    800e40 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  800e77:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  800e7d:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  800e82:	5d                   	pop    %ebp
  800e83:	c3                   	ret    

00800e84 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  800e84:	55                   	push   %ebp
  800e85:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  800e87:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  800e8b:	77 39                	ja     800ec6 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  800e8d:	8b 45 08             	mov    0x8(%ebp),%eax
  800e90:	c1 e0 0c             	shl    $0xc,%eax
  800e93:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  800e98:	89 c2                	mov    %eax,%edx
  800e9a:	c1 ea 16             	shr    $0x16,%edx
  800e9d:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800ea4:	f6 c2 01             	test   $0x1,%dl
  800ea7:	74 24                	je     800ecd <fd_lookup+0x49>
  800ea9:	89 c2                	mov    %eax,%edx
  800eab:	c1 ea 0c             	shr    $0xc,%edx
  800eae:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800eb5:	f6 c2 01             	test   $0x1,%dl
  800eb8:	74 1a                	je     800ed4 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  800eba:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ebd:	89 02                	mov    %eax,(%edx)
	return 0;
  800ebf:	b8 00 00 00 00       	mov    $0x0,%eax
  800ec4:	eb 13                	jmp    800ed9 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800ec6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800ecb:	eb 0c                	jmp    800ed9 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800ecd:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800ed2:	eb 05                	jmp    800ed9 <fd_lookup+0x55>
  800ed4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  800ed9:	5d                   	pop    %ebp
  800eda:	c3                   	ret    

00800edb <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  800edb:	55                   	push   %ebp
  800edc:	89 e5                	mov    %esp,%ebp
  800ede:	83 ec 08             	sub    $0x8,%esp
  800ee1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800ee4:	ba ac 23 80 00       	mov    $0x8023ac,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  800ee9:	eb 13                	jmp    800efe <dev_lookup+0x23>
  800eeb:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  800eee:	39 08                	cmp    %ecx,(%eax)
  800ef0:	75 0c                	jne    800efe <dev_lookup+0x23>
			*dev = devtab[i];
  800ef2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ef5:	89 01                	mov    %eax,(%ecx)
			return 0;
  800ef7:	b8 00 00 00 00       	mov    $0x0,%eax
  800efc:	eb 2e                	jmp    800f2c <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  800efe:	8b 02                	mov    (%edx),%eax
  800f00:	85 c0                	test   %eax,%eax
  800f02:	75 e7                	jne    800eeb <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  800f04:	a1 08 40 80 00       	mov    0x804008,%eax
  800f09:	8b 40 48             	mov    0x48(%eax),%eax
  800f0c:	83 ec 04             	sub    $0x4,%esp
  800f0f:	51                   	push   %ecx
  800f10:	50                   	push   %eax
  800f11:	68 2c 23 80 00       	push   $0x80232c
  800f16:	e8 9b f3 ff ff       	call   8002b6 <cprintf>
	*dev = 0;
  800f1b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800f1e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  800f24:	83 c4 10             	add    $0x10,%esp
  800f27:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  800f2c:	c9                   	leave  
  800f2d:	c3                   	ret    

00800f2e <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  800f2e:	55                   	push   %ebp
  800f2f:	89 e5                	mov    %esp,%ebp
  800f31:	56                   	push   %esi
  800f32:	53                   	push   %ebx
  800f33:	83 ec 10             	sub    $0x10,%esp
  800f36:	8b 75 08             	mov    0x8(%ebp),%esi
  800f39:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  800f3c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800f3f:	50                   	push   %eax
  800f40:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  800f46:	c1 e8 0c             	shr    $0xc,%eax
  800f49:	50                   	push   %eax
  800f4a:	e8 35 ff ff ff       	call   800e84 <fd_lookup>
  800f4f:	83 c4 08             	add    $0x8,%esp
  800f52:	85 c0                	test   %eax,%eax
  800f54:	78 05                	js     800f5b <fd_close+0x2d>
	    || fd != fd2)
  800f56:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  800f59:	74 06                	je     800f61 <fd_close+0x33>
		return (must_exist ? r : 0);
  800f5b:	84 db                	test   %bl,%bl
  800f5d:	74 47                	je     800fa6 <fd_close+0x78>
  800f5f:	eb 4a                	jmp    800fab <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  800f61:	83 ec 08             	sub    $0x8,%esp
  800f64:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800f67:	50                   	push   %eax
  800f68:	ff 36                	pushl  (%esi)
  800f6a:	e8 6c ff ff ff       	call   800edb <dev_lookup>
  800f6f:	89 c3                	mov    %eax,%ebx
  800f71:	83 c4 10             	add    $0x10,%esp
  800f74:	85 c0                	test   %eax,%eax
  800f76:	78 1c                	js     800f94 <fd_close+0x66>
		if (dev->dev_close)
  800f78:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800f7b:	8b 40 10             	mov    0x10(%eax),%eax
  800f7e:	85 c0                	test   %eax,%eax
  800f80:	74 0d                	je     800f8f <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  800f82:	83 ec 0c             	sub    $0xc,%esp
  800f85:	56                   	push   %esi
  800f86:	ff d0                	call   *%eax
  800f88:	89 c3                	mov    %eax,%ebx
  800f8a:	83 c4 10             	add    $0x10,%esp
  800f8d:	eb 05                	jmp    800f94 <fd_close+0x66>
		else
			r = 0;
  800f8f:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  800f94:	83 ec 08             	sub    $0x8,%esp
  800f97:	56                   	push   %esi
  800f98:	6a 00                	push   $0x0
  800f9a:	e8 e4 fc ff ff       	call   800c83 <sys_page_unmap>
	return r;
  800f9f:	83 c4 10             	add    $0x10,%esp
  800fa2:	89 d8                	mov    %ebx,%eax
  800fa4:	eb 05                	jmp    800fab <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  800fa6:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  800fab:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800fae:	5b                   	pop    %ebx
  800faf:	5e                   	pop    %esi
  800fb0:	5d                   	pop    %ebp
  800fb1:	c3                   	ret    

00800fb2 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  800fb2:	55                   	push   %ebp
  800fb3:	89 e5                	mov    %esp,%ebp
  800fb5:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800fb8:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800fbb:	50                   	push   %eax
  800fbc:	ff 75 08             	pushl  0x8(%ebp)
  800fbf:	e8 c0 fe ff ff       	call   800e84 <fd_lookup>
  800fc4:	83 c4 08             	add    $0x8,%esp
  800fc7:	85 c0                	test   %eax,%eax
  800fc9:	78 10                	js     800fdb <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  800fcb:	83 ec 08             	sub    $0x8,%esp
  800fce:	6a 01                	push   $0x1
  800fd0:	ff 75 f4             	pushl  -0xc(%ebp)
  800fd3:	e8 56 ff ff ff       	call   800f2e <fd_close>
  800fd8:	83 c4 10             	add    $0x10,%esp
}
  800fdb:	c9                   	leave  
  800fdc:	c3                   	ret    

00800fdd <close_all>:

void
close_all(void)
{
  800fdd:	55                   	push   %ebp
  800fde:	89 e5                	mov    %esp,%ebp
  800fe0:	53                   	push   %ebx
  800fe1:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  800fe4:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  800fe9:	83 ec 0c             	sub    $0xc,%esp
  800fec:	53                   	push   %ebx
  800fed:	e8 c0 ff ff ff       	call   800fb2 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  800ff2:	43                   	inc    %ebx
  800ff3:	83 c4 10             	add    $0x10,%esp
  800ff6:	83 fb 20             	cmp    $0x20,%ebx
  800ff9:	75 ee                	jne    800fe9 <close_all+0xc>
		close(i);
}
  800ffb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800ffe:	c9                   	leave  
  800fff:	c3                   	ret    

00801000 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801000:	55                   	push   %ebp
  801001:	89 e5                	mov    %esp,%ebp
  801003:	57                   	push   %edi
  801004:	56                   	push   %esi
  801005:	53                   	push   %ebx
  801006:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801009:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  80100c:	50                   	push   %eax
  80100d:	ff 75 08             	pushl  0x8(%ebp)
  801010:	e8 6f fe ff ff       	call   800e84 <fd_lookup>
  801015:	83 c4 08             	add    $0x8,%esp
  801018:	85 c0                	test   %eax,%eax
  80101a:	0f 88 c2 00 00 00    	js     8010e2 <dup+0xe2>
		return r;
	close(newfdnum);
  801020:	83 ec 0c             	sub    $0xc,%esp
  801023:	ff 75 0c             	pushl  0xc(%ebp)
  801026:	e8 87 ff ff ff       	call   800fb2 <close>

	newfd = INDEX2FD(newfdnum);
  80102b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80102e:	c1 e3 0c             	shl    $0xc,%ebx
  801031:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  801037:	83 c4 04             	add    $0x4,%esp
  80103a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80103d:	e8 dc fd ff ff       	call   800e1e <fd2data>
  801042:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  801044:	89 1c 24             	mov    %ebx,(%esp)
  801047:	e8 d2 fd ff ff       	call   800e1e <fd2data>
  80104c:	83 c4 10             	add    $0x10,%esp
  80104f:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801051:	89 f0                	mov    %esi,%eax
  801053:	c1 e8 16             	shr    $0x16,%eax
  801056:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  80105d:	a8 01                	test   $0x1,%al
  80105f:	74 35                	je     801096 <dup+0x96>
  801061:	89 f0                	mov    %esi,%eax
  801063:	c1 e8 0c             	shr    $0xc,%eax
  801066:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  80106d:	f6 c2 01             	test   $0x1,%dl
  801070:	74 24                	je     801096 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801072:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801079:	83 ec 0c             	sub    $0xc,%esp
  80107c:	25 07 0e 00 00       	and    $0xe07,%eax
  801081:	50                   	push   %eax
  801082:	57                   	push   %edi
  801083:	6a 00                	push   $0x0
  801085:	56                   	push   %esi
  801086:	6a 00                	push   $0x0
  801088:	e8 b4 fb ff ff       	call   800c41 <sys_page_map>
  80108d:	89 c6                	mov    %eax,%esi
  80108f:	83 c4 20             	add    $0x20,%esp
  801092:	85 c0                	test   %eax,%eax
  801094:	78 2c                	js     8010c2 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  801096:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801099:	89 d0                	mov    %edx,%eax
  80109b:	c1 e8 0c             	shr    $0xc,%eax
  80109e:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8010a5:	83 ec 0c             	sub    $0xc,%esp
  8010a8:	25 07 0e 00 00       	and    $0xe07,%eax
  8010ad:	50                   	push   %eax
  8010ae:	53                   	push   %ebx
  8010af:	6a 00                	push   $0x0
  8010b1:	52                   	push   %edx
  8010b2:	6a 00                	push   $0x0
  8010b4:	e8 88 fb ff ff       	call   800c41 <sys_page_map>
  8010b9:	89 c6                	mov    %eax,%esi
  8010bb:	83 c4 20             	add    $0x20,%esp
  8010be:	85 c0                	test   %eax,%eax
  8010c0:	79 1d                	jns    8010df <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8010c2:	83 ec 08             	sub    $0x8,%esp
  8010c5:	53                   	push   %ebx
  8010c6:	6a 00                	push   $0x0
  8010c8:	e8 b6 fb ff ff       	call   800c83 <sys_page_unmap>
	sys_page_unmap(0, nva);
  8010cd:	83 c4 08             	add    $0x8,%esp
  8010d0:	57                   	push   %edi
  8010d1:	6a 00                	push   $0x0
  8010d3:	e8 ab fb ff ff       	call   800c83 <sys_page_unmap>
	return r;
  8010d8:	83 c4 10             	add    $0x10,%esp
  8010db:	89 f0                	mov    %esi,%eax
  8010dd:	eb 03                	jmp    8010e2 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8010df:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8010e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010e5:	5b                   	pop    %ebx
  8010e6:	5e                   	pop    %esi
  8010e7:	5f                   	pop    %edi
  8010e8:	5d                   	pop    %ebp
  8010e9:	c3                   	ret    

008010ea <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8010ea:	55                   	push   %ebp
  8010eb:	89 e5                	mov    %esp,%ebp
  8010ed:	53                   	push   %ebx
  8010ee:	83 ec 14             	sub    $0x14,%esp
  8010f1:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8010f4:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8010f7:	50                   	push   %eax
  8010f8:	53                   	push   %ebx
  8010f9:	e8 86 fd ff ff       	call   800e84 <fd_lookup>
  8010fe:	83 c4 08             	add    $0x8,%esp
  801101:	85 c0                	test   %eax,%eax
  801103:	78 67                	js     80116c <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801105:	83 ec 08             	sub    $0x8,%esp
  801108:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80110b:	50                   	push   %eax
  80110c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80110f:	ff 30                	pushl  (%eax)
  801111:	e8 c5 fd ff ff       	call   800edb <dev_lookup>
  801116:	83 c4 10             	add    $0x10,%esp
  801119:	85 c0                	test   %eax,%eax
  80111b:	78 4f                	js     80116c <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  80111d:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801120:	8b 42 08             	mov    0x8(%edx),%eax
  801123:	83 e0 03             	and    $0x3,%eax
  801126:	83 f8 01             	cmp    $0x1,%eax
  801129:	75 21                	jne    80114c <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  80112b:	a1 08 40 80 00       	mov    0x804008,%eax
  801130:	8b 40 48             	mov    0x48(%eax),%eax
  801133:	83 ec 04             	sub    $0x4,%esp
  801136:	53                   	push   %ebx
  801137:	50                   	push   %eax
  801138:	68 70 23 80 00       	push   $0x802370
  80113d:	e8 74 f1 ff ff       	call   8002b6 <cprintf>
		return -E_INVAL;
  801142:	83 c4 10             	add    $0x10,%esp
  801145:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80114a:	eb 20                	jmp    80116c <read+0x82>
	}
	if (!dev->dev_read)
  80114c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80114f:	8b 40 08             	mov    0x8(%eax),%eax
  801152:	85 c0                	test   %eax,%eax
  801154:	74 11                	je     801167 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  801156:	83 ec 04             	sub    $0x4,%esp
  801159:	ff 75 10             	pushl  0x10(%ebp)
  80115c:	ff 75 0c             	pushl  0xc(%ebp)
  80115f:	52                   	push   %edx
  801160:	ff d0                	call   *%eax
  801162:	83 c4 10             	add    $0x10,%esp
  801165:	eb 05                	jmp    80116c <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801167:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  80116c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80116f:	c9                   	leave  
  801170:	c3                   	ret    

00801171 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801171:	55                   	push   %ebp
  801172:	89 e5                	mov    %esp,%ebp
  801174:	57                   	push   %edi
  801175:	56                   	push   %esi
  801176:	53                   	push   %ebx
  801177:	83 ec 0c             	sub    $0xc,%esp
  80117a:	8b 7d 08             	mov    0x8(%ebp),%edi
  80117d:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801180:	bb 00 00 00 00       	mov    $0x0,%ebx
  801185:	eb 21                	jmp    8011a8 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801187:	83 ec 04             	sub    $0x4,%esp
  80118a:	89 f0                	mov    %esi,%eax
  80118c:	29 d8                	sub    %ebx,%eax
  80118e:	50                   	push   %eax
  80118f:	89 d8                	mov    %ebx,%eax
  801191:	03 45 0c             	add    0xc(%ebp),%eax
  801194:	50                   	push   %eax
  801195:	57                   	push   %edi
  801196:	e8 4f ff ff ff       	call   8010ea <read>
		if (m < 0)
  80119b:	83 c4 10             	add    $0x10,%esp
  80119e:	85 c0                	test   %eax,%eax
  8011a0:	78 10                	js     8011b2 <readn+0x41>
			return m;
		if (m == 0)
  8011a2:	85 c0                	test   %eax,%eax
  8011a4:	74 0a                	je     8011b0 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8011a6:	01 c3                	add    %eax,%ebx
  8011a8:	39 f3                	cmp    %esi,%ebx
  8011aa:	72 db                	jb     801187 <readn+0x16>
  8011ac:	89 d8                	mov    %ebx,%eax
  8011ae:	eb 02                	jmp    8011b2 <readn+0x41>
  8011b0:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  8011b2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8011b5:	5b                   	pop    %ebx
  8011b6:	5e                   	pop    %esi
  8011b7:	5f                   	pop    %edi
  8011b8:	5d                   	pop    %ebp
  8011b9:	c3                   	ret    

008011ba <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  8011ba:	55                   	push   %ebp
  8011bb:	89 e5                	mov    %esp,%ebp
  8011bd:	53                   	push   %ebx
  8011be:	83 ec 14             	sub    $0x14,%esp
  8011c1:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8011c4:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8011c7:	50                   	push   %eax
  8011c8:	53                   	push   %ebx
  8011c9:	e8 b6 fc ff ff       	call   800e84 <fd_lookup>
  8011ce:	83 c4 08             	add    $0x8,%esp
  8011d1:	85 c0                	test   %eax,%eax
  8011d3:	78 62                	js     801237 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8011d5:	83 ec 08             	sub    $0x8,%esp
  8011d8:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8011db:	50                   	push   %eax
  8011dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011df:	ff 30                	pushl  (%eax)
  8011e1:	e8 f5 fc ff ff       	call   800edb <dev_lookup>
  8011e6:	83 c4 10             	add    $0x10,%esp
  8011e9:	85 c0                	test   %eax,%eax
  8011eb:	78 4a                	js     801237 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8011ed:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011f0:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8011f4:	75 21                	jne    801217 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  8011f6:	a1 08 40 80 00       	mov    0x804008,%eax
  8011fb:	8b 40 48             	mov    0x48(%eax),%eax
  8011fe:	83 ec 04             	sub    $0x4,%esp
  801201:	53                   	push   %ebx
  801202:	50                   	push   %eax
  801203:	68 8c 23 80 00       	push   $0x80238c
  801208:	e8 a9 f0 ff ff       	call   8002b6 <cprintf>
		return -E_INVAL;
  80120d:	83 c4 10             	add    $0x10,%esp
  801210:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801215:	eb 20                	jmp    801237 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  801217:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80121a:	8b 52 0c             	mov    0xc(%edx),%edx
  80121d:	85 d2                	test   %edx,%edx
  80121f:	74 11                	je     801232 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801221:	83 ec 04             	sub    $0x4,%esp
  801224:	ff 75 10             	pushl  0x10(%ebp)
  801227:	ff 75 0c             	pushl  0xc(%ebp)
  80122a:	50                   	push   %eax
  80122b:	ff d2                	call   *%edx
  80122d:	83 c4 10             	add    $0x10,%esp
  801230:	eb 05                	jmp    801237 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801232:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  801237:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80123a:	c9                   	leave  
  80123b:	c3                   	ret    

0080123c <seek>:

int
seek(int fdnum, off_t offset)
{
  80123c:	55                   	push   %ebp
  80123d:	89 e5                	mov    %esp,%ebp
  80123f:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801242:	8d 45 fc             	lea    -0x4(%ebp),%eax
  801245:	50                   	push   %eax
  801246:	ff 75 08             	pushl  0x8(%ebp)
  801249:	e8 36 fc ff ff       	call   800e84 <fd_lookup>
  80124e:	83 c4 08             	add    $0x8,%esp
  801251:	85 c0                	test   %eax,%eax
  801253:	78 0e                	js     801263 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801255:	8b 45 fc             	mov    -0x4(%ebp),%eax
  801258:	8b 55 0c             	mov    0xc(%ebp),%edx
  80125b:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  80125e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801263:	c9                   	leave  
  801264:	c3                   	ret    

00801265 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801265:	55                   	push   %ebp
  801266:	89 e5                	mov    %esp,%ebp
  801268:	53                   	push   %ebx
  801269:	83 ec 14             	sub    $0x14,%esp
  80126c:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  80126f:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801272:	50                   	push   %eax
  801273:	53                   	push   %ebx
  801274:	e8 0b fc ff ff       	call   800e84 <fd_lookup>
  801279:	83 c4 08             	add    $0x8,%esp
  80127c:	85 c0                	test   %eax,%eax
  80127e:	78 5f                	js     8012df <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801280:	83 ec 08             	sub    $0x8,%esp
  801283:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801286:	50                   	push   %eax
  801287:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80128a:	ff 30                	pushl  (%eax)
  80128c:	e8 4a fc ff ff       	call   800edb <dev_lookup>
  801291:	83 c4 10             	add    $0x10,%esp
  801294:	85 c0                	test   %eax,%eax
  801296:	78 47                	js     8012df <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801298:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80129b:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80129f:	75 21                	jne    8012c2 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  8012a1:	a1 08 40 80 00       	mov    0x804008,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  8012a6:	8b 40 48             	mov    0x48(%eax),%eax
  8012a9:	83 ec 04             	sub    $0x4,%esp
  8012ac:	53                   	push   %ebx
  8012ad:	50                   	push   %eax
  8012ae:	68 4c 23 80 00       	push   $0x80234c
  8012b3:	e8 fe ef ff ff       	call   8002b6 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  8012b8:	83 c4 10             	add    $0x10,%esp
  8012bb:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8012c0:	eb 1d                	jmp    8012df <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8012c2:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8012c5:	8b 52 18             	mov    0x18(%edx),%edx
  8012c8:	85 d2                	test   %edx,%edx
  8012ca:	74 0e                	je     8012da <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8012cc:	83 ec 08             	sub    $0x8,%esp
  8012cf:	ff 75 0c             	pushl  0xc(%ebp)
  8012d2:	50                   	push   %eax
  8012d3:	ff d2                	call   *%edx
  8012d5:	83 c4 10             	add    $0x10,%esp
  8012d8:	eb 05                	jmp    8012df <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8012da:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8012df:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8012e2:	c9                   	leave  
  8012e3:	c3                   	ret    

008012e4 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8012e4:	55                   	push   %ebp
  8012e5:	89 e5                	mov    %esp,%ebp
  8012e7:	53                   	push   %ebx
  8012e8:	83 ec 14             	sub    $0x14,%esp
  8012eb:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8012ee:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8012f1:	50                   	push   %eax
  8012f2:	ff 75 08             	pushl  0x8(%ebp)
  8012f5:	e8 8a fb ff ff       	call   800e84 <fd_lookup>
  8012fa:	83 c4 08             	add    $0x8,%esp
  8012fd:	85 c0                	test   %eax,%eax
  8012ff:	78 52                	js     801353 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801301:	83 ec 08             	sub    $0x8,%esp
  801304:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801307:	50                   	push   %eax
  801308:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80130b:	ff 30                	pushl  (%eax)
  80130d:	e8 c9 fb ff ff       	call   800edb <dev_lookup>
  801312:	83 c4 10             	add    $0x10,%esp
  801315:	85 c0                	test   %eax,%eax
  801317:	78 3a                	js     801353 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801319:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80131c:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  801320:	74 2c                	je     80134e <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  801322:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  801325:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  80132c:	00 00 00 
	stat->st_isdir = 0;
  80132f:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801336:	00 00 00 
	stat->st_dev = dev;
  801339:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  80133f:	83 ec 08             	sub    $0x8,%esp
  801342:	53                   	push   %ebx
  801343:	ff 75 f0             	pushl  -0x10(%ebp)
  801346:	ff 50 14             	call   *0x14(%eax)
  801349:	83 c4 10             	add    $0x10,%esp
  80134c:	eb 05                	jmp    801353 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  80134e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  801353:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801356:	c9                   	leave  
  801357:	c3                   	ret    

00801358 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  801358:	55                   	push   %ebp
  801359:	89 e5                	mov    %esp,%ebp
  80135b:	56                   	push   %esi
  80135c:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  80135d:	83 ec 08             	sub    $0x8,%esp
  801360:	6a 00                	push   $0x0
  801362:	ff 75 08             	pushl  0x8(%ebp)
  801365:	e8 e7 01 00 00       	call   801551 <open>
  80136a:	89 c3                	mov    %eax,%ebx
  80136c:	83 c4 10             	add    $0x10,%esp
  80136f:	85 c0                	test   %eax,%eax
  801371:	78 1d                	js     801390 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801373:	83 ec 08             	sub    $0x8,%esp
  801376:	ff 75 0c             	pushl  0xc(%ebp)
  801379:	50                   	push   %eax
  80137a:	e8 65 ff ff ff       	call   8012e4 <fstat>
  80137f:	89 c6                	mov    %eax,%esi
	close(fd);
  801381:	89 1c 24             	mov    %ebx,(%esp)
  801384:	e8 29 fc ff ff       	call   800fb2 <close>
	return r;
  801389:	83 c4 10             	add    $0x10,%esp
  80138c:	89 f0                	mov    %esi,%eax
  80138e:	eb 00                	jmp    801390 <stat+0x38>
}
  801390:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801393:	5b                   	pop    %ebx
  801394:	5e                   	pop    %esi
  801395:	5d                   	pop    %ebp
  801396:	c3                   	ret    

00801397 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  801397:	55                   	push   %ebp
  801398:	89 e5                	mov    %esp,%ebp
  80139a:	56                   	push   %esi
  80139b:	53                   	push   %ebx
  80139c:	89 c6                	mov    %eax,%esi
  80139e:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  8013a0:	83 3d 04 40 80 00 00 	cmpl   $0x0,0x804004
  8013a7:	75 12                	jne    8013bb <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  8013a9:	83 ec 0c             	sub    $0xc,%esp
  8013ac:	6a 01                	push   $0x1
  8013ae:	e8 c9 08 00 00       	call   801c7c <ipc_find_env>
  8013b3:	a3 04 40 80 00       	mov    %eax,0x804004
  8013b8:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8013bb:	6a 07                	push   $0x7
  8013bd:	68 00 50 80 00       	push   $0x805000
  8013c2:	56                   	push   %esi
  8013c3:	ff 35 04 40 80 00    	pushl  0x804004
  8013c9:	e8 59 08 00 00       	call   801c27 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8013ce:	83 c4 0c             	add    $0xc,%esp
  8013d1:	6a 00                	push   $0x0
  8013d3:	53                   	push   %ebx
  8013d4:	6a 00                	push   $0x0
  8013d6:	e8 e4 07 00 00       	call   801bbf <ipc_recv>
}
  8013db:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8013de:	5b                   	pop    %ebx
  8013df:	5e                   	pop    %esi
  8013e0:	5d                   	pop    %ebp
  8013e1:	c3                   	ret    

008013e2 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8013e2:	55                   	push   %ebp
  8013e3:	89 e5                	mov    %esp,%ebp
  8013e5:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8013e8:	8b 45 08             	mov    0x8(%ebp),%eax
  8013eb:	8b 40 0c             	mov    0xc(%eax),%eax
  8013ee:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  8013f3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8013f6:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  8013fb:	ba 00 00 00 00       	mov    $0x0,%edx
  801400:	b8 02 00 00 00       	mov    $0x2,%eax
  801405:	e8 8d ff ff ff       	call   801397 <fsipc>
}
  80140a:	c9                   	leave  
  80140b:	c3                   	ret    

0080140c <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  80140c:	55                   	push   %ebp
  80140d:	89 e5                	mov    %esp,%ebp
  80140f:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  801412:	8b 45 08             	mov    0x8(%ebp),%eax
  801415:	8b 40 0c             	mov    0xc(%eax),%eax
  801418:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  80141d:	ba 00 00 00 00       	mov    $0x0,%edx
  801422:	b8 06 00 00 00       	mov    $0x6,%eax
  801427:	e8 6b ff ff ff       	call   801397 <fsipc>
}
  80142c:	c9                   	leave  
  80142d:	c3                   	ret    

0080142e <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  80142e:	55                   	push   %ebp
  80142f:	89 e5                	mov    %esp,%ebp
  801431:	53                   	push   %ebx
  801432:	83 ec 04             	sub    $0x4,%esp
  801435:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  801438:	8b 45 08             	mov    0x8(%ebp),%eax
  80143b:	8b 40 0c             	mov    0xc(%eax),%eax
  80143e:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  801443:	ba 00 00 00 00       	mov    $0x0,%edx
  801448:	b8 05 00 00 00       	mov    $0x5,%eax
  80144d:	e8 45 ff ff ff       	call   801397 <fsipc>
  801452:	85 c0                	test   %eax,%eax
  801454:	78 2c                	js     801482 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  801456:	83 ec 08             	sub    $0x8,%esp
  801459:	68 00 50 80 00       	push   $0x805000
  80145e:	53                   	push   %ebx
  80145f:	e8 b6 f3 ff ff       	call   80081a <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801464:	a1 80 50 80 00       	mov    0x805080,%eax
  801469:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  80146f:	a1 84 50 80 00       	mov    0x805084,%eax
  801474:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  80147a:	83 c4 10             	add    $0x10,%esp
  80147d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801482:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801485:	c9                   	leave  
  801486:	c3                   	ret    

00801487 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801487:	55                   	push   %ebp
  801488:	89 e5                	mov    %esp,%ebp
  80148a:	83 ec 08             	sub    $0x8,%esp
  80148d:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801490:	8b 55 08             	mov    0x8(%ebp),%edx
  801493:	8b 52 0c             	mov    0xc(%edx),%edx
  801496:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  80149c:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  8014a1:	76 05                	jbe    8014a8 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  8014a3:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  8014a8:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  8014ad:	83 ec 04             	sub    $0x4,%esp
  8014b0:	50                   	push   %eax
  8014b1:	ff 75 0c             	pushl  0xc(%ebp)
  8014b4:	68 08 50 80 00       	push   $0x805008
  8014b9:	e8 d1 f4 ff ff       	call   80098f <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8014be:	ba 00 00 00 00       	mov    $0x0,%edx
  8014c3:	b8 04 00 00 00       	mov    $0x4,%eax
  8014c8:	e8 ca fe ff ff       	call   801397 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8014cd:	c9                   	leave  
  8014ce:	c3                   	ret    

008014cf <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8014cf:	55                   	push   %ebp
  8014d0:	89 e5                	mov    %esp,%ebp
  8014d2:	56                   	push   %esi
  8014d3:	53                   	push   %ebx
  8014d4:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8014d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8014da:	8b 40 0c             	mov    0xc(%eax),%eax
  8014dd:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  8014e2:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  8014e8:	ba 00 00 00 00       	mov    $0x0,%edx
  8014ed:	b8 03 00 00 00       	mov    $0x3,%eax
  8014f2:	e8 a0 fe ff ff       	call   801397 <fsipc>
  8014f7:	89 c3                	mov    %eax,%ebx
  8014f9:	85 c0                	test   %eax,%eax
  8014fb:	78 4b                	js     801548 <devfile_read+0x79>
		return r;
	assert(r <= n);
  8014fd:	39 c6                	cmp    %eax,%esi
  8014ff:	73 16                	jae    801517 <devfile_read+0x48>
  801501:	68 bc 23 80 00       	push   $0x8023bc
  801506:	68 c3 23 80 00       	push   $0x8023c3
  80150b:	6a 7c                	push   $0x7c
  80150d:	68 d8 23 80 00       	push   $0x8023d8
  801512:	e8 c7 ec ff ff       	call   8001de <_panic>
	assert(r <= PGSIZE);
  801517:	3d 00 10 00 00       	cmp    $0x1000,%eax
  80151c:	7e 16                	jle    801534 <devfile_read+0x65>
  80151e:	68 e3 23 80 00       	push   $0x8023e3
  801523:	68 c3 23 80 00       	push   $0x8023c3
  801528:	6a 7d                	push   $0x7d
  80152a:	68 d8 23 80 00       	push   $0x8023d8
  80152f:	e8 aa ec ff ff       	call   8001de <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801534:	83 ec 04             	sub    $0x4,%esp
  801537:	50                   	push   %eax
  801538:	68 00 50 80 00       	push   $0x805000
  80153d:	ff 75 0c             	pushl  0xc(%ebp)
  801540:	e8 4a f4 ff ff       	call   80098f <memmove>
	return r;
  801545:	83 c4 10             	add    $0x10,%esp
}
  801548:	89 d8                	mov    %ebx,%eax
  80154a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80154d:	5b                   	pop    %ebx
  80154e:	5e                   	pop    %esi
  80154f:	5d                   	pop    %ebp
  801550:	c3                   	ret    

00801551 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801551:	55                   	push   %ebp
  801552:	89 e5                	mov    %esp,%ebp
  801554:	53                   	push   %ebx
  801555:	83 ec 20             	sub    $0x20,%esp
  801558:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  80155b:	53                   	push   %ebx
  80155c:	e8 84 f2 ff ff       	call   8007e5 <strlen>
  801561:	83 c4 10             	add    $0x10,%esp
  801564:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801569:	7f 63                	jg     8015ce <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  80156b:	83 ec 0c             	sub    $0xc,%esp
  80156e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801571:	50                   	push   %eax
  801572:	e8 be f8 ff ff       	call   800e35 <fd_alloc>
  801577:	83 c4 10             	add    $0x10,%esp
  80157a:	85 c0                	test   %eax,%eax
  80157c:	78 55                	js     8015d3 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  80157e:	83 ec 08             	sub    $0x8,%esp
  801581:	53                   	push   %ebx
  801582:	68 00 50 80 00       	push   $0x805000
  801587:	e8 8e f2 ff ff       	call   80081a <strcpy>
	fsipcbuf.open.req_omode = mode;
  80158c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80158f:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801594:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801597:	b8 01 00 00 00       	mov    $0x1,%eax
  80159c:	e8 f6 fd ff ff       	call   801397 <fsipc>
  8015a1:	89 c3                	mov    %eax,%ebx
  8015a3:	83 c4 10             	add    $0x10,%esp
  8015a6:	85 c0                	test   %eax,%eax
  8015a8:	79 14                	jns    8015be <open+0x6d>
		fd_close(fd, 0);
  8015aa:	83 ec 08             	sub    $0x8,%esp
  8015ad:	6a 00                	push   $0x0
  8015af:	ff 75 f4             	pushl  -0xc(%ebp)
  8015b2:	e8 77 f9 ff ff       	call   800f2e <fd_close>
		return r;
  8015b7:	83 c4 10             	add    $0x10,%esp
  8015ba:	89 d8                	mov    %ebx,%eax
  8015bc:	eb 15                	jmp    8015d3 <open+0x82>
	}

	return fd2num(fd);
  8015be:	83 ec 0c             	sub    $0xc,%esp
  8015c1:	ff 75 f4             	pushl  -0xc(%ebp)
  8015c4:	e8 45 f8 ff ff       	call   800e0e <fd2num>
  8015c9:	83 c4 10             	add    $0x10,%esp
  8015cc:	eb 05                	jmp    8015d3 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  8015ce:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  8015d3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015d6:	c9                   	leave  
  8015d7:	c3                   	ret    

008015d8 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  8015d8:	55                   	push   %ebp
  8015d9:	89 e5                	mov    %esp,%ebp
  8015db:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  8015de:	ba 00 00 00 00       	mov    $0x0,%edx
  8015e3:	b8 08 00 00 00       	mov    $0x8,%eax
  8015e8:	e8 aa fd ff ff       	call   801397 <fsipc>
}
  8015ed:	c9                   	leave  
  8015ee:	c3                   	ret    

008015ef <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  8015ef:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  8015f3:	7e 38                	jle    80162d <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  8015f5:	55                   	push   %ebp
  8015f6:	89 e5                	mov    %esp,%ebp
  8015f8:	53                   	push   %ebx
  8015f9:	83 ec 08             	sub    $0x8,%esp
  8015fc:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  8015fe:	ff 70 04             	pushl  0x4(%eax)
  801601:	8d 40 10             	lea    0x10(%eax),%eax
  801604:	50                   	push   %eax
  801605:	ff 33                	pushl  (%ebx)
  801607:	e8 ae fb ff ff       	call   8011ba <write>
		if (result > 0)
  80160c:	83 c4 10             	add    $0x10,%esp
  80160f:	85 c0                	test   %eax,%eax
  801611:	7e 03                	jle    801616 <writebuf+0x27>
			b->result += result;
  801613:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  801616:	3b 43 04             	cmp    0x4(%ebx),%eax
  801619:	74 0e                	je     801629 <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  80161b:	89 c2                	mov    %eax,%edx
  80161d:	85 c0                	test   %eax,%eax
  80161f:	7e 05                	jle    801626 <writebuf+0x37>
  801621:	ba 00 00 00 00       	mov    $0x0,%edx
  801626:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  801629:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80162c:	c9                   	leave  
  80162d:	c3                   	ret    

0080162e <putch>:

static void
putch(int ch, void *thunk)
{
  80162e:	55                   	push   %ebp
  80162f:	89 e5                	mov    %esp,%ebp
  801631:	53                   	push   %ebx
  801632:	83 ec 04             	sub    $0x4,%esp
  801635:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  801638:	8b 53 04             	mov    0x4(%ebx),%edx
  80163b:	8d 42 01             	lea    0x1(%edx),%eax
  80163e:	89 43 04             	mov    %eax,0x4(%ebx)
  801641:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801644:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  801648:	3d 00 01 00 00       	cmp    $0x100,%eax
  80164d:	75 0e                	jne    80165d <putch+0x2f>
		writebuf(b);
  80164f:	89 d8                	mov    %ebx,%eax
  801651:	e8 99 ff ff ff       	call   8015ef <writebuf>
		b->idx = 0;
  801656:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  80165d:	83 c4 04             	add    $0x4,%esp
  801660:	5b                   	pop    %ebx
  801661:	5d                   	pop    %ebp
  801662:	c3                   	ret    

00801663 <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  801663:	55                   	push   %ebp
  801664:	89 e5                	mov    %esp,%ebp
  801666:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  80166c:	8b 45 08             	mov    0x8(%ebp),%eax
  80166f:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  801675:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  80167c:	00 00 00 
	b.result = 0;
  80167f:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  801686:	00 00 00 
	b.error = 1;
  801689:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  801690:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  801693:	ff 75 10             	pushl  0x10(%ebp)
  801696:	ff 75 0c             	pushl  0xc(%ebp)
  801699:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  80169f:	50                   	push   %eax
  8016a0:	68 2e 16 80 00       	push   $0x80162e
  8016a5:	e8 40 ed ff ff       	call   8003ea <vprintfmt>
	if (b.idx > 0)
  8016aa:	83 c4 10             	add    $0x10,%esp
  8016ad:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  8016b4:	7e 0b                	jle    8016c1 <vfprintf+0x5e>
		writebuf(&b);
  8016b6:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  8016bc:	e8 2e ff ff ff       	call   8015ef <writebuf>

	return (b.result ? b.result : b.error);
  8016c1:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8016c7:	85 c0                	test   %eax,%eax
  8016c9:	75 06                	jne    8016d1 <vfprintf+0x6e>
  8016cb:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8016d1:	c9                   	leave  
  8016d2:	c3                   	ret    

008016d3 <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  8016d3:	55                   	push   %ebp
  8016d4:	89 e5                	mov    %esp,%ebp
  8016d6:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8016d9:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  8016dc:	50                   	push   %eax
  8016dd:	ff 75 0c             	pushl  0xc(%ebp)
  8016e0:	ff 75 08             	pushl  0x8(%ebp)
  8016e3:	e8 7b ff ff ff       	call   801663 <vfprintf>
	va_end(ap);

	return cnt;
}
  8016e8:	c9                   	leave  
  8016e9:	c3                   	ret    

008016ea <printf>:

int
printf(const char *fmt, ...)
{
  8016ea:	55                   	push   %ebp
  8016eb:	89 e5                	mov    %esp,%ebp
  8016ed:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8016f0:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  8016f3:	50                   	push   %eax
  8016f4:	ff 75 08             	pushl  0x8(%ebp)
  8016f7:	6a 01                	push   $0x1
  8016f9:	e8 65 ff ff ff       	call   801663 <vfprintf>
	va_end(ap);

	return cnt;
}
  8016fe:	c9                   	leave  
  8016ff:	c3                   	ret    

00801700 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801700:	55                   	push   %ebp
  801701:	89 e5                	mov    %esp,%ebp
  801703:	56                   	push   %esi
  801704:	53                   	push   %ebx
  801705:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801708:	83 ec 0c             	sub    $0xc,%esp
  80170b:	ff 75 08             	pushl  0x8(%ebp)
  80170e:	e8 0b f7 ff ff       	call   800e1e <fd2data>
  801713:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801715:	83 c4 08             	add    $0x8,%esp
  801718:	68 ef 23 80 00       	push   $0x8023ef
  80171d:	53                   	push   %ebx
  80171e:	e8 f7 f0 ff ff       	call   80081a <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801723:	8b 46 04             	mov    0x4(%esi),%eax
  801726:	2b 06                	sub    (%esi),%eax
  801728:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  80172e:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801735:	00 00 00 
	stat->st_dev = &devpipe;
  801738:	c7 83 88 00 00 00 24 	movl   $0x803024,0x88(%ebx)
  80173f:	30 80 00 
	return 0;
}
  801742:	b8 00 00 00 00       	mov    $0x0,%eax
  801747:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80174a:	5b                   	pop    %ebx
  80174b:	5e                   	pop    %esi
  80174c:	5d                   	pop    %ebp
  80174d:	c3                   	ret    

0080174e <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  80174e:	55                   	push   %ebp
  80174f:	89 e5                	mov    %esp,%ebp
  801751:	53                   	push   %ebx
  801752:	83 ec 0c             	sub    $0xc,%esp
  801755:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801758:	53                   	push   %ebx
  801759:	6a 00                	push   $0x0
  80175b:	e8 23 f5 ff ff       	call   800c83 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801760:	89 1c 24             	mov    %ebx,(%esp)
  801763:	e8 b6 f6 ff ff       	call   800e1e <fd2data>
  801768:	83 c4 08             	add    $0x8,%esp
  80176b:	50                   	push   %eax
  80176c:	6a 00                	push   $0x0
  80176e:	e8 10 f5 ff ff       	call   800c83 <sys_page_unmap>
}
  801773:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801776:	c9                   	leave  
  801777:	c3                   	ret    

00801778 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801778:	55                   	push   %ebp
  801779:	89 e5                	mov    %esp,%ebp
  80177b:	57                   	push   %edi
  80177c:	56                   	push   %esi
  80177d:	53                   	push   %ebx
  80177e:	83 ec 1c             	sub    $0x1c,%esp
  801781:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801784:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801786:	a1 08 40 80 00       	mov    0x804008,%eax
  80178b:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  80178e:	83 ec 0c             	sub    $0xc,%esp
  801791:	ff 75 e0             	pushl  -0x20(%ebp)
  801794:	e8 27 05 00 00       	call   801cc0 <pageref>
  801799:	89 c3                	mov    %eax,%ebx
  80179b:	89 3c 24             	mov    %edi,(%esp)
  80179e:	e8 1d 05 00 00       	call   801cc0 <pageref>
  8017a3:	83 c4 10             	add    $0x10,%esp
  8017a6:	39 c3                	cmp    %eax,%ebx
  8017a8:	0f 94 c1             	sete   %cl
  8017ab:	0f b6 c9             	movzbl %cl,%ecx
  8017ae:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  8017b1:	8b 15 08 40 80 00    	mov    0x804008,%edx
  8017b7:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  8017ba:	39 ce                	cmp    %ecx,%esi
  8017bc:	74 1b                	je     8017d9 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  8017be:	39 c3                	cmp    %eax,%ebx
  8017c0:	75 c4                	jne    801786 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  8017c2:	8b 42 58             	mov    0x58(%edx),%eax
  8017c5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8017c8:	50                   	push   %eax
  8017c9:	56                   	push   %esi
  8017ca:	68 f6 23 80 00       	push   $0x8023f6
  8017cf:	e8 e2 ea ff ff       	call   8002b6 <cprintf>
  8017d4:	83 c4 10             	add    $0x10,%esp
  8017d7:	eb ad                	jmp    801786 <_pipeisclosed+0xe>
	}
}
  8017d9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8017dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8017df:	5b                   	pop    %ebx
  8017e0:	5e                   	pop    %esi
  8017e1:	5f                   	pop    %edi
  8017e2:	5d                   	pop    %ebp
  8017e3:	c3                   	ret    

008017e4 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  8017e4:	55                   	push   %ebp
  8017e5:	89 e5                	mov    %esp,%ebp
  8017e7:	57                   	push   %edi
  8017e8:	56                   	push   %esi
  8017e9:	53                   	push   %ebx
  8017ea:	83 ec 18             	sub    $0x18,%esp
  8017ed:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  8017f0:	56                   	push   %esi
  8017f1:	e8 28 f6 ff ff       	call   800e1e <fd2data>
  8017f6:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8017f8:	83 c4 10             	add    $0x10,%esp
  8017fb:	bf 00 00 00 00       	mov    $0x0,%edi
  801800:	eb 3b                	jmp    80183d <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801802:	89 da                	mov    %ebx,%edx
  801804:	89 f0                	mov    %esi,%eax
  801806:	e8 6d ff ff ff       	call   801778 <_pipeisclosed>
  80180b:	85 c0                	test   %eax,%eax
  80180d:	75 38                	jne    801847 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  80180f:	e8 cb f3 ff ff       	call   800bdf <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801814:	8b 53 04             	mov    0x4(%ebx),%edx
  801817:	8b 03                	mov    (%ebx),%eax
  801819:	83 c0 20             	add    $0x20,%eax
  80181c:	39 c2                	cmp    %eax,%edx
  80181e:	73 e2                	jae    801802 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801820:	8b 45 0c             	mov    0xc(%ebp),%eax
  801823:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801826:	89 d0                	mov    %edx,%eax
  801828:	25 1f 00 00 80       	and    $0x8000001f,%eax
  80182d:	79 05                	jns    801834 <devpipe_write+0x50>
  80182f:	48                   	dec    %eax
  801830:	83 c8 e0             	or     $0xffffffe0,%eax
  801833:	40                   	inc    %eax
  801834:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801838:	42                   	inc    %edx
  801839:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80183c:	47                   	inc    %edi
  80183d:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801840:	75 d2                	jne    801814 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801842:	8b 45 10             	mov    0x10(%ebp),%eax
  801845:	eb 05                	jmp    80184c <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801847:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  80184c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80184f:	5b                   	pop    %ebx
  801850:	5e                   	pop    %esi
  801851:	5f                   	pop    %edi
  801852:	5d                   	pop    %ebp
  801853:	c3                   	ret    

00801854 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801854:	55                   	push   %ebp
  801855:	89 e5                	mov    %esp,%ebp
  801857:	57                   	push   %edi
  801858:	56                   	push   %esi
  801859:	53                   	push   %ebx
  80185a:	83 ec 18             	sub    $0x18,%esp
  80185d:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801860:	57                   	push   %edi
  801861:	e8 b8 f5 ff ff       	call   800e1e <fd2data>
  801866:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801868:	83 c4 10             	add    $0x10,%esp
  80186b:	bb 00 00 00 00       	mov    $0x0,%ebx
  801870:	eb 3a                	jmp    8018ac <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801872:	85 db                	test   %ebx,%ebx
  801874:	74 04                	je     80187a <devpipe_read+0x26>
				return i;
  801876:	89 d8                	mov    %ebx,%eax
  801878:	eb 41                	jmp    8018bb <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  80187a:	89 f2                	mov    %esi,%edx
  80187c:	89 f8                	mov    %edi,%eax
  80187e:	e8 f5 fe ff ff       	call   801778 <_pipeisclosed>
  801883:	85 c0                	test   %eax,%eax
  801885:	75 2f                	jne    8018b6 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801887:	e8 53 f3 ff ff       	call   800bdf <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  80188c:	8b 06                	mov    (%esi),%eax
  80188e:	3b 46 04             	cmp    0x4(%esi),%eax
  801891:	74 df                	je     801872 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801893:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801898:	79 05                	jns    80189f <devpipe_read+0x4b>
  80189a:	48                   	dec    %eax
  80189b:	83 c8 e0             	or     $0xffffffe0,%eax
  80189e:	40                   	inc    %eax
  80189f:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  8018a3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8018a6:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  8018a9:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8018ab:	43                   	inc    %ebx
  8018ac:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  8018af:	75 db                	jne    80188c <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  8018b1:	8b 45 10             	mov    0x10(%ebp),%eax
  8018b4:	eb 05                	jmp    8018bb <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8018b6:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  8018bb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8018be:	5b                   	pop    %ebx
  8018bf:	5e                   	pop    %esi
  8018c0:	5f                   	pop    %edi
  8018c1:	5d                   	pop    %ebp
  8018c2:	c3                   	ret    

008018c3 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  8018c3:	55                   	push   %ebp
  8018c4:	89 e5                	mov    %esp,%ebp
  8018c6:	56                   	push   %esi
  8018c7:	53                   	push   %ebx
  8018c8:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  8018cb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8018ce:	50                   	push   %eax
  8018cf:	e8 61 f5 ff ff       	call   800e35 <fd_alloc>
  8018d4:	83 c4 10             	add    $0x10,%esp
  8018d7:	85 c0                	test   %eax,%eax
  8018d9:	0f 88 2a 01 00 00    	js     801a09 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8018df:	83 ec 04             	sub    $0x4,%esp
  8018e2:	68 07 04 00 00       	push   $0x407
  8018e7:	ff 75 f4             	pushl  -0xc(%ebp)
  8018ea:	6a 00                	push   $0x0
  8018ec:	e8 0d f3 ff ff       	call   800bfe <sys_page_alloc>
  8018f1:	83 c4 10             	add    $0x10,%esp
  8018f4:	85 c0                	test   %eax,%eax
  8018f6:	0f 88 0d 01 00 00    	js     801a09 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  8018fc:	83 ec 0c             	sub    $0xc,%esp
  8018ff:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801902:	50                   	push   %eax
  801903:	e8 2d f5 ff ff       	call   800e35 <fd_alloc>
  801908:	89 c3                	mov    %eax,%ebx
  80190a:	83 c4 10             	add    $0x10,%esp
  80190d:	85 c0                	test   %eax,%eax
  80190f:	0f 88 e2 00 00 00    	js     8019f7 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801915:	83 ec 04             	sub    $0x4,%esp
  801918:	68 07 04 00 00       	push   $0x407
  80191d:	ff 75 f0             	pushl  -0x10(%ebp)
  801920:	6a 00                	push   $0x0
  801922:	e8 d7 f2 ff ff       	call   800bfe <sys_page_alloc>
  801927:	89 c3                	mov    %eax,%ebx
  801929:	83 c4 10             	add    $0x10,%esp
  80192c:	85 c0                	test   %eax,%eax
  80192e:	0f 88 c3 00 00 00    	js     8019f7 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801934:	83 ec 0c             	sub    $0xc,%esp
  801937:	ff 75 f4             	pushl  -0xc(%ebp)
  80193a:	e8 df f4 ff ff       	call   800e1e <fd2data>
  80193f:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801941:	83 c4 0c             	add    $0xc,%esp
  801944:	68 07 04 00 00       	push   $0x407
  801949:	50                   	push   %eax
  80194a:	6a 00                	push   $0x0
  80194c:	e8 ad f2 ff ff       	call   800bfe <sys_page_alloc>
  801951:	89 c3                	mov    %eax,%ebx
  801953:	83 c4 10             	add    $0x10,%esp
  801956:	85 c0                	test   %eax,%eax
  801958:	0f 88 89 00 00 00    	js     8019e7 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  80195e:	83 ec 0c             	sub    $0xc,%esp
  801961:	ff 75 f0             	pushl  -0x10(%ebp)
  801964:	e8 b5 f4 ff ff       	call   800e1e <fd2data>
  801969:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801970:	50                   	push   %eax
  801971:	6a 00                	push   $0x0
  801973:	56                   	push   %esi
  801974:	6a 00                	push   $0x0
  801976:	e8 c6 f2 ff ff       	call   800c41 <sys_page_map>
  80197b:	89 c3                	mov    %eax,%ebx
  80197d:	83 c4 20             	add    $0x20,%esp
  801980:	85 c0                	test   %eax,%eax
  801982:	78 55                	js     8019d9 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801984:	8b 15 24 30 80 00    	mov    0x803024,%edx
  80198a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80198d:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  80198f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801992:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801999:	8b 15 24 30 80 00    	mov    0x803024,%edx
  80199f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8019a2:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  8019a4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8019a7:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  8019ae:	83 ec 0c             	sub    $0xc,%esp
  8019b1:	ff 75 f4             	pushl  -0xc(%ebp)
  8019b4:	e8 55 f4 ff ff       	call   800e0e <fd2num>
  8019b9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8019bc:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  8019be:	83 c4 04             	add    $0x4,%esp
  8019c1:	ff 75 f0             	pushl  -0x10(%ebp)
  8019c4:	e8 45 f4 ff ff       	call   800e0e <fd2num>
  8019c9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8019cc:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  8019cf:	83 c4 10             	add    $0x10,%esp
  8019d2:	b8 00 00 00 00       	mov    $0x0,%eax
  8019d7:	eb 30                	jmp    801a09 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  8019d9:	83 ec 08             	sub    $0x8,%esp
  8019dc:	56                   	push   %esi
  8019dd:	6a 00                	push   $0x0
  8019df:	e8 9f f2 ff ff       	call   800c83 <sys_page_unmap>
  8019e4:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  8019e7:	83 ec 08             	sub    $0x8,%esp
  8019ea:	ff 75 f0             	pushl  -0x10(%ebp)
  8019ed:	6a 00                	push   $0x0
  8019ef:	e8 8f f2 ff ff       	call   800c83 <sys_page_unmap>
  8019f4:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  8019f7:	83 ec 08             	sub    $0x8,%esp
  8019fa:	ff 75 f4             	pushl  -0xc(%ebp)
  8019fd:	6a 00                	push   $0x0
  8019ff:	e8 7f f2 ff ff       	call   800c83 <sys_page_unmap>
  801a04:	83 c4 10             	add    $0x10,%esp
  801a07:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801a09:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801a0c:	5b                   	pop    %ebx
  801a0d:	5e                   	pop    %esi
  801a0e:	5d                   	pop    %ebp
  801a0f:	c3                   	ret    

00801a10 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801a10:	55                   	push   %ebp
  801a11:	89 e5                	mov    %esp,%ebp
  801a13:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801a16:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801a19:	50                   	push   %eax
  801a1a:	ff 75 08             	pushl  0x8(%ebp)
  801a1d:	e8 62 f4 ff ff       	call   800e84 <fd_lookup>
  801a22:	83 c4 10             	add    $0x10,%esp
  801a25:	85 c0                	test   %eax,%eax
  801a27:	78 18                	js     801a41 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801a29:	83 ec 0c             	sub    $0xc,%esp
  801a2c:	ff 75 f4             	pushl  -0xc(%ebp)
  801a2f:	e8 ea f3 ff ff       	call   800e1e <fd2data>
	return _pipeisclosed(fd, p);
  801a34:	89 c2                	mov    %eax,%edx
  801a36:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801a39:	e8 3a fd ff ff       	call   801778 <_pipeisclosed>
  801a3e:	83 c4 10             	add    $0x10,%esp
}
  801a41:	c9                   	leave  
  801a42:	c3                   	ret    

00801a43 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801a43:	55                   	push   %ebp
  801a44:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801a46:	b8 00 00 00 00       	mov    $0x0,%eax
  801a4b:	5d                   	pop    %ebp
  801a4c:	c3                   	ret    

00801a4d <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801a4d:	55                   	push   %ebp
  801a4e:	89 e5                	mov    %esp,%ebp
  801a50:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801a53:	68 0e 24 80 00       	push   $0x80240e
  801a58:	ff 75 0c             	pushl  0xc(%ebp)
  801a5b:	e8 ba ed ff ff       	call   80081a <strcpy>
	return 0;
}
  801a60:	b8 00 00 00 00       	mov    $0x0,%eax
  801a65:	c9                   	leave  
  801a66:	c3                   	ret    

00801a67 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801a67:	55                   	push   %ebp
  801a68:	89 e5                	mov    %esp,%ebp
  801a6a:	57                   	push   %edi
  801a6b:	56                   	push   %esi
  801a6c:	53                   	push   %ebx
  801a6d:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801a73:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801a78:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801a7e:	eb 2c                	jmp    801aac <devcons_write+0x45>
		m = n - tot;
  801a80:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801a83:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801a85:	83 fb 7f             	cmp    $0x7f,%ebx
  801a88:	76 05                	jbe    801a8f <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801a8a:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801a8f:	83 ec 04             	sub    $0x4,%esp
  801a92:	53                   	push   %ebx
  801a93:	03 45 0c             	add    0xc(%ebp),%eax
  801a96:	50                   	push   %eax
  801a97:	57                   	push   %edi
  801a98:	e8 f2 ee ff ff       	call   80098f <memmove>
		sys_cputs(buf, m);
  801a9d:	83 c4 08             	add    $0x8,%esp
  801aa0:	53                   	push   %ebx
  801aa1:	57                   	push   %edi
  801aa2:	e8 9b f0 ff ff       	call   800b42 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801aa7:	01 de                	add    %ebx,%esi
  801aa9:	83 c4 10             	add    $0x10,%esp
  801aac:	89 f0                	mov    %esi,%eax
  801aae:	3b 75 10             	cmp    0x10(%ebp),%esi
  801ab1:	72 cd                	jb     801a80 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801ab3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801ab6:	5b                   	pop    %ebx
  801ab7:	5e                   	pop    %esi
  801ab8:	5f                   	pop    %edi
  801ab9:	5d                   	pop    %ebp
  801aba:	c3                   	ret    

00801abb <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801abb:	55                   	push   %ebp
  801abc:	89 e5                	mov    %esp,%ebp
  801abe:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801ac1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801ac5:	75 07                	jne    801ace <devcons_read+0x13>
  801ac7:	eb 23                	jmp    801aec <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801ac9:	e8 11 f1 ff ff       	call   800bdf <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801ace:	e8 8d f0 ff ff       	call   800b60 <sys_cgetc>
  801ad3:	85 c0                	test   %eax,%eax
  801ad5:	74 f2                	je     801ac9 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801ad7:	85 c0                	test   %eax,%eax
  801ad9:	78 1d                	js     801af8 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801adb:	83 f8 04             	cmp    $0x4,%eax
  801ade:	74 13                	je     801af3 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801ae0:	8b 55 0c             	mov    0xc(%ebp),%edx
  801ae3:	88 02                	mov    %al,(%edx)
	return 1;
  801ae5:	b8 01 00 00 00       	mov    $0x1,%eax
  801aea:	eb 0c                	jmp    801af8 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801aec:	b8 00 00 00 00       	mov    $0x0,%eax
  801af1:	eb 05                	jmp    801af8 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801af3:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801af8:	c9                   	leave  
  801af9:	c3                   	ret    

00801afa <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801afa:	55                   	push   %ebp
  801afb:	89 e5                	mov    %esp,%ebp
  801afd:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801b00:	8b 45 08             	mov    0x8(%ebp),%eax
  801b03:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801b06:	6a 01                	push   $0x1
  801b08:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801b0b:	50                   	push   %eax
  801b0c:	e8 31 f0 ff ff       	call   800b42 <sys_cputs>
}
  801b11:	83 c4 10             	add    $0x10,%esp
  801b14:	c9                   	leave  
  801b15:	c3                   	ret    

00801b16 <getchar>:

int
getchar(void)
{
  801b16:	55                   	push   %ebp
  801b17:	89 e5                	mov    %esp,%ebp
  801b19:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801b1c:	6a 01                	push   $0x1
  801b1e:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801b21:	50                   	push   %eax
  801b22:	6a 00                	push   $0x0
  801b24:	e8 c1 f5 ff ff       	call   8010ea <read>
	if (r < 0)
  801b29:	83 c4 10             	add    $0x10,%esp
  801b2c:	85 c0                	test   %eax,%eax
  801b2e:	78 0f                	js     801b3f <getchar+0x29>
		return r;
	if (r < 1)
  801b30:	85 c0                	test   %eax,%eax
  801b32:	7e 06                	jle    801b3a <getchar+0x24>
		return -E_EOF;
	return c;
  801b34:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801b38:	eb 05                	jmp    801b3f <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801b3a:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801b3f:	c9                   	leave  
  801b40:	c3                   	ret    

00801b41 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801b41:	55                   	push   %ebp
  801b42:	89 e5                	mov    %esp,%ebp
  801b44:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801b47:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b4a:	50                   	push   %eax
  801b4b:	ff 75 08             	pushl  0x8(%ebp)
  801b4e:	e8 31 f3 ff ff       	call   800e84 <fd_lookup>
  801b53:	83 c4 10             	add    $0x10,%esp
  801b56:	85 c0                	test   %eax,%eax
  801b58:	78 11                	js     801b6b <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801b5a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b5d:	8b 15 40 30 80 00    	mov    0x803040,%edx
  801b63:	39 10                	cmp    %edx,(%eax)
  801b65:	0f 94 c0             	sete   %al
  801b68:	0f b6 c0             	movzbl %al,%eax
}
  801b6b:	c9                   	leave  
  801b6c:	c3                   	ret    

00801b6d <opencons>:

int
opencons(void)
{
  801b6d:	55                   	push   %ebp
  801b6e:	89 e5                	mov    %esp,%ebp
  801b70:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801b73:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b76:	50                   	push   %eax
  801b77:	e8 b9 f2 ff ff       	call   800e35 <fd_alloc>
  801b7c:	83 c4 10             	add    $0x10,%esp
  801b7f:	85 c0                	test   %eax,%eax
  801b81:	78 3a                	js     801bbd <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801b83:	83 ec 04             	sub    $0x4,%esp
  801b86:	68 07 04 00 00       	push   $0x407
  801b8b:	ff 75 f4             	pushl  -0xc(%ebp)
  801b8e:	6a 00                	push   $0x0
  801b90:	e8 69 f0 ff ff       	call   800bfe <sys_page_alloc>
  801b95:	83 c4 10             	add    $0x10,%esp
  801b98:	85 c0                	test   %eax,%eax
  801b9a:	78 21                	js     801bbd <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801b9c:	8b 15 40 30 80 00    	mov    0x803040,%edx
  801ba2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ba5:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801ba7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801baa:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801bb1:	83 ec 0c             	sub    $0xc,%esp
  801bb4:	50                   	push   %eax
  801bb5:	e8 54 f2 ff ff       	call   800e0e <fd2num>
  801bba:	83 c4 10             	add    $0x10,%esp
}
  801bbd:	c9                   	leave  
  801bbe:	c3                   	ret    

00801bbf <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801bbf:	55                   	push   %ebp
  801bc0:	89 e5                	mov    %esp,%ebp
  801bc2:	56                   	push   %esi
  801bc3:	53                   	push   %ebx
  801bc4:	8b 75 08             	mov    0x8(%ebp),%esi
  801bc7:	8b 45 0c             	mov    0xc(%ebp),%eax
  801bca:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801bcd:	85 c0                	test   %eax,%eax
  801bcf:	75 05                	jne    801bd6 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801bd1:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801bd6:	83 ec 0c             	sub    $0xc,%esp
  801bd9:	50                   	push   %eax
  801bda:	e8 ee f1 ff ff       	call   800dcd <sys_ipc_recv>
  801bdf:	83 c4 10             	add    $0x10,%esp
  801be2:	85 c0                	test   %eax,%eax
  801be4:	74 16                	je     801bfc <ipc_recv+0x3d>
        if (from_env_store)
  801be6:	85 f6                	test   %esi,%esi
  801be8:	74 06                	je     801bf0 <ipc_recv+0x31>
            *from_env_store = 0;
  801bea:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801bf0:	85 db                	test   %ebx,%ebx
  801bf2:	74 2c                	je     801c20 <ipc_recv+0x61>
            *perm_store = 0;
  801bf4:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801bfa:	eb 24                	jmp    801c20 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801bfc:	85 f6                	test   %esi,%esi
  801bfe:	74 0a                	je     801c0a <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801c00:	a1 08 40 80 00       	mov    0x804008,%eax
  801c05:	8b 40 74             	mov    0x74(%eax),%eax
  801c08:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801c0a:	85 db                	test   %ebx,%ebx
  801c0c:	74 0a                	je     801c18 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801c0e:	a1 08 40 80 00       	mov    0x804008,%eax
  801c13:	8b 40 78             	mov    0x78(%eax),%eax
  801c16:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801c18:	a1 08 40 80 00       	mov    0x804008,%eax
  801c1d:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801c20:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c23:	5b                   	pop    %ebx
  801c24:	5e                   	pop    %esi
  801c25:	5d                   	pop    %ebp
  801c26:	c3                   	ret    

00801c27 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801c27:	55                   	push   %ebp
  801c28:	89 e5                	mov    %esp,%ebp
  801c2a:	57                   	push   %edi
  801c2b:	56                   	push   %esi
  801c2c:	53                   	push   %ebx
  801c2d:	83 ec 0c             	sub    $0xc,%esp
  801c30:	8b 75 0c             	mov    0xc(%ebp),%esi
  801c33:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801c36:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801c39:	85 db                	test   %ebx,%ebx
  801c3b:	75 0c                	jne    801c49 <ipc_send+0x22>
        pg = (void *)UTOP;
  801c3d:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801c42:	eb 05                	jmp    801c49 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801c44:	e8 96 ef ff ff       	call   800bdf <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801c49:	57                   	push   %edi
  801c4a:	53                   	push   %ebx
  801c4b:	56                   	push   %esi
  801c4c:	ff 75 08             	pushl  0x8(%ebp)
  801c4f:	e8 56 f1 ff ff       	call   800daa <sys_ipc_try_send>
  801c54:	83 c4 10             	add    $0x10,%esp
  801c57:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801c5a:	74 e8                	je     801c44 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801c5c:	85 c0                	test   %eax,%eax
  801c5e:	74 14                	je     801c74 <ipc_send+0x4d>
        panic("ipc_send: error");
  801c60:	83 ec 04             	sub    $0x4,%esp
  801c63:	68 1a 24 80 00       	push   $0x80241a
  801c68:	6a 6a                	push   $0x6a
  801c6a:	68 2a 24 80 00       	push   $0x80242a
  801c6f:	e8 6a e5 ff ff       	call   8001de <_panic>
    }
	//panic("ipc_send not implemented");
}
  801c74:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c77:	5b                   	pop    %ebx
  801c78:	5e                   	pop    %esi
  801c79:	5f                   	pop    %edi
  801c7a:	5d                   	pop    %ebp
  801c7b:	c3                   	ret    

00801c7c <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801c7c:	55                   	push   %ebp
  801c7d:	89 e5                	mov    %esp,%ebp
  801c7f:	53                   	push   %ebx
  801c80:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801c83:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801c88:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801c8f:	89 d0                	mov    %edx,%eax
  801c91:	c1 e0 07             	shl    $0x7,%eax
  801c94:	29 d8                	sub    %ebx,%eax
  801c96:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801c9b:	8b 40 50             	mov    0x50(%eax),%eax
  801c9e:	39 c8                	cmp    %ecx,%eax
  801ca0:	75 0d                	jne    801caf <ipc_find_env+0x33>
			return envs[i].env_id;
  801ca2:	c1 e2 07             	shl    $0x7,%edx
  801ca5:	29 da                	sub    %ebx,%edx
  801ca7:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801cad:	eb 0e                	jmp    801cbd <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801caf:	42                   	inc    %edx
  801cb0:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801cb6:	75 d0                	jne    801c88 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801cb8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801cbd:	5b                   	pop    %ebx
  801cbe:	5d                   	pop    %ebp
  801cbf:	c3                   	ret    

00801cc0 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801cc0:	55                   	push   %ebp
  801cc1:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801cc3:	8b 45 08             	mov    0x8(%ebp),%eax
  801cc6:	c1 e8 16             	shr    $0x16,%eax
  801cc9:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801cd0:	a8 01                	test   $0x1,%al
  801cd2:	74 21                	je     801cf5 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801cd4:	8b 45 08             	mov    0x8(%ebp),%eax
  801cd7:	c1 e8 0c             	shr    $0xc,%eax
  801cda:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801ce1:	a8 01                	test   $0x1,%al
  801ce3:	74 17                	je     801cfc <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801ce5:	c1 e8 0c             	shr    $0xc,%eax
  801ce8:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801cef:	ef 
  801cf0:	0f b7 c0             	movzwl %ax,%eax
  801cf3:	eb 0c                	jmp    801d01 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801cf5:	b8 00 00 00 00       	mov    $0x0,%eax
  801cfa:	eb 05                	jmp    801d01 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801cfc:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801d01:	5d                   	pop    %ebp
  801d02:	c3                   	ret    
  801d03:	90                   	nop

00801d04 <__udivdi3>:
  801d04:	55                   	push   %ebp
  801d05:	57                   	push   %edi
  801d06:	56                   	push   %esi
  801d07:	53                   	push   %ebx
  801d08:	83 ec 1c             	sub    $0x1c,%esp
  801d0b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801d0f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801d13:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801d17:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801d1b:	89 ca                	mov    %ecx,%edx
  801d1d:	89 f8                	mov    %edi,%eax
  801d1f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801d23:	85 f6                	test   %esi,%esi
  801d25:	75 2d                	jne    801d54 <__udivdi3+0x50>
  801d27:	39 cf                	cmp    %ecx,%edi
  801d29:	77 65                	ja     801d90 <__udivdi3+0x8c>
  801d2b:	89 fd                	mov    %edi,%ebp
  801d2d:	85 ff                	test   %edi,%edi
  801d2f:	75 0b                	jne    801d3c <__udivdi3+0x38>
  801d31:	b8 01 00 00 00       	mov    $0x1,%eax
  801d36:	31 d2                	xor    %edx,%edx
  801d38:	f7 f7                	div    %edi
  801d3a:	89 c5                	mov    %eax,%ebp
  801d3c:	31 d2                	xor    %edx,%edx
  801d3e:	89 c8                	mov    %ecx,%eax
  801d40:	f7 f5                	div    %ebp
  801d42:	89 c1                	mov    %eax,%ecx
  801d44:	89 d8                	mov    %ebx,%eax
  801d46:	f7 f5                	div    %ebp
  801d48:	89 cf                	mov    %ecx,%edi
  801d4a:	89 fa                	mov    %edi,%edx
  801d4c:	83 c4 1c             	add    $0x1c,%esp
  801d4f:	5b                   	pop    %ebx
  801d50:	5e                   	pop    %esi
  801d51:	5f                   	pop    %edi
  801d52:	5d                   	pop    %ebp
  801d53:	c3                   	ret    
  801d54:	39 ce                	cmp    %ecx,%esi
  801d56:	77 28                	ja     801d80 <__udivdi3+0x7c>
  801d58:	0f bd fe             	bsr    %esi,%edi
  801d5b:	83 f7 1f             	xor    $0x1f,%edi
  801d5e:	75 40                	jne    801da0 <__udivdi3+0x9c>
  801d60:	39 ce                	cmp    %ecx,%esi
  801d62:	72 0a                	jb     801d6e <__udivdi3+0x6a>
  801d64:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801d68:	0f 87 9e 00 00 00    	ja     801e0c <__udivdi3+0x108>
  801d6e:	b8 01 00 00 00       	mov    $0x1,%eax
  801d73:	89 fa                	mov    %edi,%edx
  801d75:	83 c4 1c             	add    $0x1c,%esp
  801d78:	5b                   	pop    %ebx
  801d79:	5e                   	pop    %esi
  801d7a:	5f                   	pop    %edi
  801d7b:	5d                   	pop    %ebp
  801d7c:	c3                   	ret    
  801d7d:	8d 76 00             	lea    0x0(%esi),%esi
  801d80:	31 ff                	xor    %edi,%edi
  801d82:	31 c0                	xor    %eax,%eax
  801d84:	89 fa                	mov    %edi,%edx
  801d86:	83 c4 1c             	add    $0x1c,%esp
  801d89:	5b                   	pop    %ebx
  801d8a:	5e                   	pop    %esi
  801d8b:	5f                   	pop    %edi
  801d8c:	5d                   	pop    %ebp
  801d8d:	c3                   	ret    
  801d8e:	66 90                	xchg   %ax,%ax
  801d90:	89 d8                	mov    %ebx,%eax
  801d92:	f7 f7                	div    %edi
  801d94:	31 ff                	xor    %edi,%edi
  801d96:	89 fa                	mov    %edi,%edx
  801d98:	83 c4 1c             	add    $0x1c,%esp
  801d9b:	5b                   	pop    %ebx
  801d9c:	5e                   	pop    %esi
  801d9d:	5f                   	pop    %edi
  801d9e:	5d                   	pop    %ebp
  801d9f:	c3                   	ret    
  801da0:	bd 20 00 00 00       	mov    $0x20,%ebp
  801da5:	89 eb                	mov    %ebp,%ebx
  801da7:	29 fb                	sub    %edi,%ebx
  801da9:	89 f9                	mov    %edi,%ecx
  801dab:	d3 e6                	shl    %cl,%esi
  801dad:	89 c5                	mov    %eax,%ebp
  801daf:	88 d9                	mov    %bl,%cl
  801db1:	d3 ed                	shr    %cl,%ebp
  801db3:	89 e9                	mov    %ebp,%ecx
  801db5:	09 f1                	or     %esi,%ecx
  801db7:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801dbb:	89 f9                	mov    %edi,%ecx
  801dbd:	d3 e0                	shl    %cl,%eax
  801dbf:	89 c5                	mov    %eax,%ebp
  801dc1:	89 d6                	mov    %edx,%esi
  801dc3:	88 d9                	mov    %bl,%cl
  801dc5:	d3 ee                	shr    %cl,%esi
  801dc7:	89 f9                	mov    %edi,%ecx
  801dc9:	d3 e2                	shl    %cl,%edx
  801dcb:	8b 44 24 08          	mov    0x8(%esp),%eax
  801dcf:	88 d9                	mov    %bl,%cl
  801dd1:	d3 e8                	shr    %cl,%eax
  801dd3:	09 c2                	or     %eax,%edx
  801dd5:	89 d0                	mov    %edx,%eax
  801dd7:	89 f2                	mov    %esi,%edx
  801dd9:	f7 74 24 0c          	divl   0xc(%esp)
  801ddd:	89 d6                	mov    %edx,%esi
  801ddf:	89 c3                	mov    %eax,%ebx
  801de1:	f7 e5                	mul    %ebp
  801de3:	39 d6                	cmp    %edx,%esi
  801de5:	72 19                	jb     801e00 <__udivdi3+0xfc>
  801de7:	74 0b                	je     801df4 <__udivdi3+0xf0>
  801de9:	89 d8                	mov    %ebx,%eax
  801deb:	31 ff                	xor    %edi,%edi
  801ded:	e9 58 ff ff ff       	jmp    801d4a <__udivdi3+0x46>
  801df2:	66 90                	xchg   %ax,%ax
  801df4:	8b 54 24 08          	mov    0x8(%esp),%edx
  801df8:	89 f9                	mov    %edi,%ecx
  801dfa:	d3 e2                	shl    %cl,%edx
  801dfc:	39 c2                	cmp    %eax,%edx
  801dfe:	73 e9                	jae    801de9 <__udivdi3+0xe5>
  801e00:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801e03:	31 ff                	xor    %edi,%edi
  801e05:	e9 40 ff ff ff       	jmp    801d4a <__udivdi3+0x46>
  801e0a:	66 90                	xchg   %ax,%ax
  801e0c:	31 c0                	xor    %eax,%eax
  801e0e:	e9 37 ff ff ff       	jmp    801d4a <__udivdi3+0x46>
  801e13:	90                   	nop

00801e14 <__umoddi3>:
  801e14:	55                   	push   %ebp
  801e15:	57                   	push   %edi
  801e16:	56                   	push   %esi
  801e17:	53                   	push   %ebx
  801e18:	83 ec 1c             	sub    $0x1c,%esp
  801e1b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  801e1f:	8b 74 24 34          	mov    0x34(%esp),%esi
  801e23:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801e27:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801e2b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  801e2f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801e33:	89 f3                	mov    %esi,%ebx
  801e35:	89 fa                	mov    %edi,%edx
  801e37:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801e3b:	89 34 24             	mov    %esi,(%esp)
  801e3e:	85 c0                	test   %eax,%eax
  801e40:	75 1a                	jne    801e5c <__umoddi3+0x48>
  801e42:	39 f7                	cmp    %esi,%edi
  801e44:	0f 86 a2 00 00 00    	jbe    801eec <__umoddi3+0xd8>
  801e4a:	89 c8                	mov    %ecx,%eax
  801e4c:	89 f2                	mov    %esi,%edx
  801e4e:	f7 f7                	div    %edi
  801e50:	89 d0                	mov    %edx,%eax
  801e52:	31 d2                	xor    %edx,%edx
  801e54:	83 c4 1c             	add    $0x1c,%esp
  801e57:	5b                   	pop    %ebx
  801e58:	5e                   	pop    %esi
  801e59:	5f                   	pop    %edi
  801e5a:	5d                   	pop    %ebp
  801e5b:	c3                   	ret    
  801e5c:	39 f0                	cmp    %esi,%eax
  801e5e:	0f 87 ac 00 00 00    	ja     801f10 <__umoddi3+0xfc>
  801e64:	0f bd e8             	bsr    %eax,%ebp
  801e67:	83 f5 1f             	xor    $0x1f,%ebp
  801e6a:	0f 84 ac 00 00 00    	je     801f1c <__umoddi3+0x108>
  801e70:	bf 20 00 00 00       	mov    $0x20,%edi
  801e75:	29 ef                	sub    %ebp,%edi
  801e77:	89 fe                	mov    %edi,%esi
  801e79:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801e7d:	89 e9                	mov    %ebp,%ecx
  801e7f:	d3 e0                	shl    %cl,%eax
  801e81:	89 d7                	mov    %edx,%edi
  801e83:	89 f1                	mov    %esi,%ecx
  801e85:	d3 ef                	shr    %cl,%edi
  801e87:	09 c7                	or     %eax,%edi
  801e89:	89 e9                	mov    %ebp,%ecx
  801e8b:	d3 e2                	shl    %cl,%edx
  801e8d:	89 14 24             	mov    %edx,(%esp)
  801e90:	89 d8                	mov    %ebx,%eax
  801e92:	d3 e0                	shl    %cl,%eax
  801e94:	89 c2                	mov    %eax,%edx
  801e96:	8b 44 24 08          	mov    0x8(%esp),%eax
  801e9a:	d3 e0                	shl    %cl,%eax
  801e9c:	89 44 24 04          	mov    %eax,0x4(%esp)
  801ea0:	8b 44 24 08          	mov    0x8(%esp),%eax
  801ea4:	89 f1                	mov    %esi,%ecx
  801ea6:	d3 e8                	shr    %cl,%eax
  801ea8:	09 d0                	or     %edx,%eax
  801eaa:	d3 eb                	shr    %cl,%ebx
  801eac:	89 da                	mov    %ebx,%edx
  801eae:	f7 f7                	div    %edi
  801eb0:	89 d3                	mov    %edx,%ebx
  801eb2:	f7 24 24             	mull   (%esp)
  801eb5:	89 c6                	mov    %eax,%esi
  801eb7:	89 d1                	mov    %edx,%ecx
  801eb9:	39 d3                	cmp    %edx,%ebx
  801ebb:	0f 82 87 00 00 00    	jb     801f48 <__umoddi3+0x134>
  801ec1:	0f 84 91 00 00 00    	je     801f58 <__umoddi3+0x144>
  801ec7:	8b 54 24 04          	mov    0x4(%esp),%edx
  801ecb:	29 f2                	sub    %esi,%edx
  801ecd:	19 cb                	sbb    %ecx,%ebx
  801ecf:	89 d8                	mov    %ebx,%eax
  801ed1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801ed5:	d3 e0                	shl    %cl,%eax
  801ed7:	89 e9                	mov    %ebp,%ecx
  801ed9:	d3 ea                	shr    %cl,%edx
  801edb:	09 d0                	or     %edx,%eax
  801edd:	89 e9                	mov    %ebp,%ecx
  801edf:	d3 eb                	shr    %cl,%ebx
  801ee1:	89 da                	mov    %ebx,%edx
  801ee3:	83 c4 1c             	add    $0x1c,%esp
  801ee6:	5b                   	pop    %ebx
  801ee7:	5e                   	pop    %esi
  801ee8:	5f                   	pop    %edi
  801ee9:	5d                   	pop    %ebp
  801eea:	c3                   	ret    
  801eeb:	90                   	nop
  801eec:	89 fd                	mov    %edi,%ebp
  801eee:	85 ff                	test   %edi,%edi
  801ef0:	75 0b                	jne    801efd <__umoddi3+0xe9>
  801ef2:	b8 01 00 00 00       	mov    $0x1,%eax
  801ef7:	31 d2                	xor    %edx,%edx
  801ef9:	f7 f7                	div    %edi
  801efb:	89 c5                	mov    %eax,%ebp
  801efd:	89 f0                	mov    %esi,%eax
  801eff:	31 d2                	xor    %edx,%edx
  801f01:	f7 f5                	div    %ebp
  801f03:	89 c8                	mov    %ecx,%eax
  801f05:	f7 f5                	div    %ebp
  801f07:	89 d0                	mov    %edx,%eax
  801f09:	e9 44 ff ff ff       	jmp    801e52 <__umoddi3+0x3e>
  801f0e:	66 90                	xchg   %ax,%ax
  801f10:	89 c8                	mov    %ecx,%eax
  801f12:	89 f2                	mov    %esi,%edx
  801f14:	83 c4 1c             	add    $0x1c,%esp
  801f17:	5b                   	pop    %ebx
  801f18:	5e                   	pop    %esi
  801f19:	5f                   	pop    %edi
  801f1a:	5d                   	pop    %ebp
  801f1b:	c3                   	ret    
  801f1c:	3b 04 24             	cmp    (%esp),%eax
  801f1f:	72 06                	jb     801f27 <__umoddi3+0x113>
  801f21:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801f25:	77 0f                	ja     801f36 <__umoddi3+0x122>
  801f27:	89 f2                	mov    %esi,%edx
  801f29:	29 f9                	sub    %edi,%ecx
  801f2b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  801f2f:	89 14 24             	mov    %edx,(%esp)
  801f32:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801f36:	8b 44 24 04          	mov    0x4(%esp),%eax
  801f3a:	8b 14 24             	mov    (%esp),%edx
  801f3d:	83 c4 1c             	add    $0x1c,%esp
  801f40:	5b                   	pop    %ebx
  801f41:	5e                   	pop    %esi
  801f42:	5f                   	pop    %edi
  801f43:	5d                   	pop    %ebp
  801f44:	c3                   	ret    
  801f45:	8d 76 00             	lea    0x0(%esi),%esi
  801f48:	2b 04 24             	sub    (%esp),%eax
  801f4b:	19 fa                	sbb    %edi,%edx
  801f4d:	89 d1                	mov    %edx,%ecx
  801f4f:	89 c6                	mov    %eax,%esi
  801f51:	e9 71 ff ff ff       	jmp    801ec7 <__umoddi3+0xb3>
  801f56:	66 90                	xchg   %ax,%ax
  801f58:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801f5c:	72 ea                	jb     801f48 <__umoddi3+0x134>
  801f5e:	89 d9                	mov    %ebx,%ecx
  801f60:	e9 62 ff ff ff       	jmp    801ec7 <__umoddi3+0xb3>
