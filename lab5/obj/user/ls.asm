
obj/user/ls.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 93 02 00 00       	call   8002c4 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <ls1>:
		panic("error reading directory %s: %e", path, n);
}

void
ls1(const char *prefix, bool isdir, off_t size, const char *name)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80003b:	8b 75 0c             	mov    0xc(%ebp),%esi
	const char *sep;

	if(flag['l'])
  80003e:	83 3d d0 41 80 00 00 	cmpl   $0x0,0x8041d0
  800045:	74 20                	je     800067 <ls1+0x34>
		printf("%11d %c ", size, isdir ? 'd' : '-');
  800047:	89 f0                	mov    %esi,%eax
  800049:	3c 01                	cmp    $0x1,%al
  80004b:	19 c0                	sbb    %eax,%eax
  80004d:	83 e0 c9             	and    $0xffffffc9,%eax
  800050:	83 c0 64             	add    $0x64,%eax
  800053:	83 ec 04             	sub    $0x4,%esp
  800056:	50                   	push   %eax
  800057:	ff 75 10             	pushl  0x10(%ebp)
  80005a:	68 22 22 80 00       	push   $0x802222
  80005f:	e8 24 19 00 00       	call   801988 <printf>
  800064:	83 c4 10             	add    $0x10,%esp
	if(prefix) {
  800067:	85 db                	test   %ebx,%ebx
  800069:	74 3d                	je     8000a8 <ls1+0x75>
		if (prefix[0] && prefix[strlen(prefix)-1] != '/')
  80006b:	80 3b 00             	cmpb   $0x0,(%ebx)
  80006e:	74 1a                	je     80008a <ls1+0x57>
  800070:	83 ec 0c             	sub    $0xc,%esp
  800073:	53                   	push   %ebx
  800074:	e8 b3 08 00 00       	call   80092c <strlen>
  800079:	83 c4 10             	add    $0x10,%esp
  80007c:	80 7c 03 ff 2f       	cmpb   $0x2f,-0x1(%ebx,%eax,1)
  800081:	75 0e                	jne    800091 <ls1+0x5e>
			sep = "/";
		else
			sep = "";
  800083:	b8 88 22 80 00       	mov    $0x802288,%eax
  800088:	eb 0c                	jmp    800096 <ls1+0x63>
  80008a:	b8 88 22 80 00       	mov    $0x802288,%eax
  80008f:	eb 05                	jmp    800096 <ls1+0x63>

	if(flag['l'])
		printf("%11d %c ", size, isdir ? 'd' : '-');
	if(prefix) {
		if (prefix[0] && prefix[strlen(prefix)-1] != '/')
			sep = "/";
  800091:	b8 20 22 80 00       	mov    $0x802220,%eax
		else
			sep = "";
		printf("%s%s", prefix, sep);
  800096:	83 ec 04             	sub    $0x4,%esp
  800099:	50                   	push   %eax
  80009a:	53                   	push   %ebx
  80009b:	68 2b 22 80 00       	push   $0x80222b
  8000a0:	e8 e3 18 00 00       	call   801988 <printf>
  8000a5:	83 c4 10             	add    $0x10,%esp
	}
	printf("%s", name);
  8000a8:	83 ec 08             	sub    $0x8,%esp
  8000ab:	ff 75 14             	pushl  0x14(%ebp)
  8000ae:	68 b5 26 80 00       	push   $0x8026b5
  8000b3:	e8 d0 18 00 00       	call   801988 <printf>
	if(flag['F'] && isdir)
  8000b8:	83 c4 10             	add    $0x10,%esp
  8000bb:	83 3d 38 41 80 00 00 	cmpl   $0x0,0x804138
  8000c2:	74 16                	je     8000da <ls1+0xa7>
  8000c4:	89 f0                	mov    %esi,%eax
  8000c6:	84 c0                	test   %al,%al
  8000c8:	74 10                	je     8000da <ls1+0xa7>
		printf("/");
  8000ca:	83 ec 0c             	sub    $0xc,%esp
  8000cd:	68 20 22 80 00       	push   $0x802220
  8000d2:	e8 b1 18 00 00       	call   801988 <printf>
  8000d7:	83 c4 10             	add    $0x10,%esp
	printf("\n");
  8000da:	83 ec 0c             	sub    $0xc,%esp
  8000dd:	68 87 22 80 00       	push   $0x802287
  8000e2:	e8 a1 18 00 00       	call   801988 <printf>
}
  8000e7:	83 c4 10             	add    $0x10,%esp
  8000ea:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000ed:	5b                   	pop    %ebx
  8000ee:	5e                   	pop    %esi
  8000ef:	5d                   	pop    %ebp
  8000f0:	c3                   	ret    

008000f1 <lsdir>:
		ls1(0, st.st_isdir, st.st_size, path);
}

void
lsdir(const char *path, const char *prefix)
{
  8000f1:	55                   	push   %ebp
  8000f2:	89 e5                	mov    %esp,%ebp
  8000f4:	57                   	push   %edi
  8000f5:	56                   	push   %esi
  8000f6:	53                   	push   %ebx
  8000f7:	81 ec 14 01 00 00    	sub    $0x114,%esp
  8000fd:	8b 7d 08             	mov    0x8(%ebp),%edi
	int fd, n;
	struct File f;

	if ((fd = open(path, O_RDONLY)) < 0)
  800100:	6a 00                	push   $0x0
  800102:	57                   	push   %edi
  800103:	e8 e7 16 00 00       	call   8017ef <open>
  800108:	89 c3                	mov    %eax,%ebx
  80010a:	83 c4 10             	add    $0x10,%esp
  80010d:	85 c0                	test   %eax,%eax
  80010f:	79 41                	jns    800152 <lsdir+0x61>
		panic("open %s: %e", path, fd);
  800111:	83 ec 0c             	sub    $0xc,%esp
  800114:	50                   	push   %eax
  800115:	57                   	push   %edi
  800116:	68 30 22 80 00       	push   $0x802230
  80011b:	6a 1d                	push   $0x1d
  80011d:	68 3c 22 80 00       	push   $0x80223c
  800122:	e8 fe 01 00 00       	call   800325 <_panic>
	while ((n = readn(fd, &f, sizeof f)) == sizeof f)
		if (f.f_name[0])
  800127:	80 bd e8 fe ff ff 00 	cmpb   $0x0,-0x118(%ebp)
  80012e:	74 28                	je     800158 <lsdir+0x67>
			ls1(prefix, f.f_type==FTYPE_DIR, f.f_size, f.f_name);
  800130:	56                   	push   %esi
  800131:	ff b5 68 ff ff ff    	pushl  -0x98(%ebp)
  800137:	83 bd 6c ff ff ff 01 	cmpl   $0x1,-0x94(%ebp)
  80013e:	0f 94 c0             	sete   %al
  800141:	0f b6 c0             	movzbl %al,%eax
  800144:	50                   	push   %eax
  800145:	ff 75 0c             	pushl  0xc(%ebp)
  800148:	e8 e6 fe ff ff       	call   800033 <ls1>
  80014d:	83 c4 10             	add    $0x10,%esp
  800150:	eb 06                	jmp    800158 <lsdir+0x67>
	int fd, n;
	struct File f;

	if ((fd = open(path, O_RDONLY)) < 0)
		panic("open %s: %e", path, fd);
	while ((n = readn(fd, &f, sizeof f)) == sizeof f)
  800152:	8d b5 e8 fe ff ff    	lea    -0x118(%ebp),%esi
  800158:	83 ec 04             	sub    $0x4,%esp
  80015b:	68 00 01 00 00       	push   $0x100
  800160:	56                   	push   %esi
  800161:	53                   	push   %ebx
  800162:	e8 a8 12 00 00       	call   80140f <readn>
  800167:	83 c4 10             	add    $0x10,%esp
  80016a:	3d 00 01 00 00       	cmp    $0x100,%eax
  80016f:	74 b6                	je     800127 <lsdir+0x36>
		if (f.f_name[0])
			ls1(prefix, f.f_type==FTYPE_DIR, f.f_size, f.f_name);
	if (n > 0)
  800171:	85 c0                	test   %eax,%eax
  800173:	7e 12                	jle    800187 <lsdir+0x96>
		panic("short read in directory %s", path);
  800175:	57                   	push   %edi
  800176:	68 46 22 80 00       	push   $0x802246
  80017b:	6a 22                	push   $0x22
  80017d:	68 3c 22 80 00       	push   $0x80223c
  800182:	e8 9e 01 00 00       	call   800325 <_panic>
	if (n < 0)
  800187:	85 c0                	test   %eax,%eax
  800189:	79 16                	jns    8001a1 <lsdir+0xb0>
		panic("error reading directory %s: %e", path, n);
  80018b:	83 ec 0c             	sub    $0xc,%esp
  80018e:	50                   	push   %eax
  80018f:	57                   	push   %edi
  800190:	68 8c 22 80 00       	push   $0x80228c
  800195:	6a 24                	push   $0x24
  800197:	68 3c 22 80 00       	push   $0x80223c
  80019c:	e8 84 01 00 00       	call   800325 <_panic>
}
  8001a1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001a4:	5b                   	pop    %ebx
  8001a5:	5e                   	pop    %esi
  8001a6:	5f                   	pop    %edi
  8001a7:	5d                   	pop    %ebp
  8001a8:	c3                   	ret    

008001a9 <ls>:
void lsdir(const char*, const char*);
void ls1(const char*, bool, off_t, const char*);

void
ls(const char *path, const char *prefix)
{
  8001a9:	55                   	push   %ebp
  8001aa:	89 e5                	mov    %esp,%ebp
  8001ac:	53                   	push   %ebx
  8001ad:	81 ec 9c 00 00 00    	sub    $0x9c,%esp
  8001b3:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Stat st;

	if ((r = stat(path, &st)) < 0)
  8001b6:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
  8001bc:	50                   	push   %eax
  8001bd:	53                   	push   %ebx
  8001be:	e8 33 14 00 00       	call   8015f6 <stat>
  8001c3:	83 c4 10             	add    $0x10,%esp
  8001c6:	85 c0                	test   %eax,%eax
  8001c8:	79 16                	jns    8001e0 <ls+0x37>
		panic("stat %s: %e", path, r);
  8001ca:	83 ec 0c             	sub    $0xc,%esp
  8001cd:	50                   	push   %eax
  8001ce:	53                   	push   %ebx
  8001cf:	68 61 22 80 00       	push   $0x802261
  8001d4:	6a 0f                	push   $0xf
  8001d6:	68 3c 22 80 00       	push   $0x80223c
  8001db:	e8 45 01 00 00       	call   800325 <_panic>
	if (st.st_isdir && !flag['d'])
  8001e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8001e3:	85 c0                	test   %eax,%eax
  8001e5:	74 1a                	je     800201 <ls+0x58>
  8001e7:	83 3d b0 41 80 00 00 	cmpl   $0x0,0x8041b0
  8001ee:	75 11                	jne    800201 <ls+0x58>
		lsdir(path, prefix);
  8001f0:	83 ec 08             	sub    $0x8,%esp
  8001f3:	ff 75 0c             	pushl  0xc(%ebp)
  8001f6:	53                   	push   %ebx
  8001f7:	e8 f5 fe ff ff       	call   8000f1 <lsdir>
  8001fc:	83 c4 10             	add    $0x10,%esp
  8001ff:	eb 17                	jmp    800218 <ls+0x6f>
	else
		ls1(0, st.st_isdir, st.st_size, path);
  800201:	53                   	push   %ebx
  800202:	ff 75 ec             	pushl  -0x14(%ebp)
  800205:	85 c0                	test   %eax,%eax
  800207:	0f 95 c0             	setne  %al
  80020a:	0f b6 c0             	movzbl %al,%eax
  80020d:	50                   	push   %eax
  80020e:	6a 00                	push   $0x0
  800210:	e8 1e fe ff ff       	call   800033 <ls1>
  800215:	83 c4 10             	add    $0x10,%esp
}
  800218:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80021b:	c9                   	leave  
  80021c:	c3                   	ret    

0080021d <usage>:
	printf("\n");
}

void
usage(void)
{
  80021d:	55                   	push   %ebp
  80021e:	89 e5                	mov    %esp,%ebp
  800220:	83 ec 14             	sub    $0x14,%esp
	printf("usage: ls [-dFl] [file...]\n");
  800223:	68 6d 22 80 00       	push   $0x80226d
  800228:	e8 5b 17 00 00       	call   801988 <printf>
	exit();
  80022d:	e8 e1 00 00 00       	call   800313 <exit>
}
  800232:	83 c4 10             	add    $0x10,%esp
  800235:	c9                   	leave  
  800236:	c3                   	ret    

00800237 <umain>:

void
umain(int argc, char **argv)
{
  800237:	55                   	push   %ebp
  800238:	89 e5                	mov    %esp,%ebp
  80023a:	56                   	push   %esi
  80023b:	53                   	push   %ebx
  80023c:	83 ec 14             	sub    $0x14,%esp
  80023f:	8b 75 0c             	mov    0xc(%ebp),%esi
	int i;
	struct Argstate args;

	argstart(&argc, argv, &args);
  800242:	8d 45 e8             	lea    -0x18(%ebp),%eax
  800245:	50                   	push   %eax
  800246:	56                   	push   %esi
  800247:	8d 45 08             	lea    0x8(%ebp),%eax
  80024a:	50                   	push   %eax
  80024b:	e8 05 0d 00 00       	call   800f55 <argstart>
	while ((i = argnext(&args)) >= 0)
  800250:	83 c4 10             	add    $0x10,%esp
  800253:	8d 5d e8             	lea    -0x18(%ebp),%ebx
  800256:	eb 1d                	jmp    800275 <umain+0x3e>
		switch (i) {
  800258:	83 f8 64             	cmp    $0x64,%eax
  80025b:	74 0a                	je     800267 <umain+0x30>
  80025d:	83 f8 6c             	cmp    $0x6c,%eax
  800260:	74 05                	je     800267 <umain+0x30>
  800262:	83 f8 46             	cmp    $0x46,%eax
  800265:	75 09                	jne    800270 <umain+0x39>
		case 'd':
		case 'F':
		case 'l':
			flag[i]++;
  800267:	ff 04 85 20 40 80 00 	incl   0x804020(,%eax,4)
			break;
  80026e:	eb 05                	jmp    800275 <umain+0x3e>
		default:
			usage();
  800270:	e8 a8 ff ff ff       	call   80021d <usage>
{
	int i;
	struct Argstate args;

	argstart(&argc, argv, &args);
	while ((i = argnext(&args)) >= 0)
  800275:	83 ec 0c             	sub    $0xc,%esp
  800278:	53                   	push   %ebx
  800279:	e8 10 0d 00 00       	call   800f8e <argnext>
  80027e:	83 c4 10             	add    $0x10,%esp
  800281:	85 c0                	test   %eax,%eax
  800283:	79 d3                	jns    800258 <umain+0x21>
  800285:	bb 01 00 00 00       	mov    $0x1,%ebx
			break;
		default:
			usage();
		}

	if (argc == 1)
  80028a:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  80028e:	75 28                	jne    8002b8 <umain+0x81>
		ls("/", "");
  800290:	83 ec 08             	sub    $0x8,%esp
  800293:	68 88 22 80 00       	push   $0x802288
  800298:	68 20 22 80 00       	push   $0x802220
  80029d:	e8 07 ff ff ff       	call   8001a9 <ls>
  8002a2:	83 c4 10             	add    $0x10,%esp
  8002a5:	eb 16                	jmp    8002bd <umain+0x86>
	else {
		for (i = 1; i < argc; i++)
			ls(argv[i], argv[i]);
  8002a7:	8b 04 9e             	mov    (%esi,%ebx,4),%eax
  8002aa:	83 ec 08             	sub    $0x8,%esp
  8002ad:	50                   	push   %eax
  8002ae:	50                   	push   %eax
  8002af:	e8 f5 fe ff ff       	call   8001a9 <ls>
		}

	if (argc == 1)
		ls("/", "");
	else {
		for (i = 1; i < argc; i++)
  8002b4:	43                   	inc    %ebx
  8002b5:	83 c4 10             	add    $0x10,%esp
  8002b8:	3b 5d 08             	cmp    0x8(%ebp),%ebx
  8002bb:	7c ea                	jl     8002a7 <umain+0x70>
			ls(argv[i], argv[i]);
	}
}
  8002bd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8002c0:	5b                   	pop    %ebx
  8002c1:	5e                   	pop    %esi
  8002c2:	5d                   	pop    %ebp
  8002c3:	c3                   	ret    

008002c4 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8002c4:	55                   	push   %ebp
  8002c5:	89 e5                	mov    %esp,%ebp
  8002c7:	56                   	push   %esi
  8002c8:	53                   	push   %ebx
  8002c9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8002cc:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8002cf:	e8 33 0a 00 00       	call   800d07 <sys_getenvid>
  8002d4:	25 ff 03 00 00       	and    $0x3ff,%eax
  8002d9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8002e0:	c1 e0 07             	shl    $0x7,%eax
  8002e3:	29 d0                	sub    %edx,%eax
  8002e5:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8002ea:	a3 20 44 80 00       	mov    %eax,0x804420
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8002ef:	85 db                	test   %ebx,%ebx
  8002f1:	7e 07                	jle    8002fa <libmain+0x36>
		binaryname = argv[0];
  8002f3:	8b 06                	mov    (%esi),%eax
  8002f5:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  8002fa:	83 ec 08             	sub    $0x8,%esp
  8002fd:	56                   	push   %esi
  8002fe:	53                   	push   %ebx
  8002ff:	e8 33 ff ff ff       	call   800237 <umain>

	// exit gracefully
	exit();
  800304:	e8 0a 00 00 00       	call   800313 <exit>
}
  800309:	83 c4 10             	add    $0x10,%esp
  80030c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80030f:	5b                   	pop    %ebx
  800310:	5e                   	pop    %esi
  800311:	5d                   	pop    %ebp
  800312:	c3                   	ret    

00800313 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800313:	55                   	push   %ebp
  800314:	89 e5                	mov    %esp,%ebp
  800316:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800319:	6a 00                	push   $0x0
  80031b:	e8 a6 09 00 00       	call   800cc6 <sys_env_destroy>
}
  800320:	83 c4 10             	add    $0x10,%esp
  800323:	c9                   	leave  
  800324:	c3                   	ret    

00800325 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800325:	55                   	push   %ebp
  800326:	89 e5                	mov    %esp,%ebp
  800328:	56                   	push   %esi
  800329:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80032a:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80032d:	8b 35 00 30 80 00    	mov    0x803000,%esi
  800333:	e8 cf 09 00 00       	call   800d07 <sys_getenvid>
  800338:	83 ec 0c             	sub    $0xc,%esp
  80033b:	ff 75 0c             	pushl  0xc(%ebp)
  80033e:	ff 75 08             	pushl  0x8(%ebp)
  800341:	56                   	push   %esi
  800342:	50                   	push   %eax
  800343:	68 b8 22 80 00       	push   $0x8022b8
  800348:	e8 b0 00 00 00       	call   8003fd <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80034d:	83 c4 18             	add    $0x18,%esp
  800350:	53                   	push   %ebx
  800351:	ff 75 10             	pushl  0x10(%ebp)
  800354:	e8 53 00 00 00       	call   8003ac <vcprintf>
	cprintf("\n");
  800359:	c7 04 24 87 22 80 00 	movl   $0x802287,(%esp)
  800360:	e8 98 00 00 00       	call   8003fd <cprintf>
  800365:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800368:	cc                   	int3   
  800369:	eb fd                	jmp    800368 <_panic+0x43>

0080036b <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80036b:	55                   	push   %ebp
  80036c:	89 e5                	mov    %esp,%ebp
  80036e:	53                   	push   %ebx
  80036f:	83 ec 04             	sub    $0x4,%esp
  800372:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800375:	8b 13                	mov    (%ebx),%edx
  800377:	8d 42 01             	lea    0x1(%edx),%eax
  80037a:	89 03                	mov    %eax,(%ebx)
  80037c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80037f:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800383:	3d ff 00 00 00       	cmp    $0xff,%eax
  800388:	75 1a                	jne    8003a4 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80038a:	83 ec 08             	sub    $0x8,%esp
  80038d:	68 ff 00 00 00       	push   $0xff
  800392:	8d 43 08             	lea    0x8(%ebx),%eax
  800395:	50                   	push   %eax
  800396:	e8 ee 08 00 00       	call   800c89 <sys_cputs>
		b->idx = 0;
  80039b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003a1:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003a4:	ff 43 04             	incl   0x4(%ebx)
}
  8003a7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003aa:	c9                   	leave  
  8003ab:	c3                   	ret    

008003ac <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003ac:	55                   	push   %ebp
  8003ad:	89 e5                	mov    %esp,%ebp
  8003af:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003b5:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003bc:	00 00 00 
	b.cnt = 0;
  8003bf:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8003c6:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8003c9:	ff 75 0c             	pushl  0xc(%ebp)
  8003cc:	ff 75 08             	pushl  0x8(%ebp)
  8003cf:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8003d5:	50                   	push   %eax
  8003d6:	68 6b 03 80 00       	push   $0x80036b
  8003db:	e8 51 01 00 00       	call   800531 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8003e0:	83 c4 08             	add    $0x8,%esp
  8003e3:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8003e9:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8003ef:	50                   	push   %eax
  8003f0:	e8 94 08 00 00       	call   800c89 <sys_cputs>

	return b.cnt;
}
  8003f5:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8003fb:	c9                   	leave  
  8003fc:	c3                   	ret    

008003fd <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8003fd:	55                   	push   %ebp
  8003fe:	89 e5                	mov    %esp,%ebp
  800400:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800403:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800406:	50                   	push   %eax
  800407:	ff 75 08             	pushl  0x8(%ebp)
  80040a:	e8 9d ff ff ff       	call   8003ac <vcprintf>
	va_end(ap);

	return cnt;
}
  80040f:	c9                   	leave  
  800410:	c3                   	ret    

00800411 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800411:	55                   	push   %ebp
  800412:	89 e5                	mov    %esp,%ebp
  800414:	57                   	push   %edi
  800415:	56                   	push   %esi
  800416:	53                   	push   %ebx
  800417:	83 ec 1c             	sub    $0x1c,%esp
  80041a:	89 c7                	mov    %eax,%edi
  80041c:	89 d6                	mov    %edx,%esi
  80041e:	8b 45 08             	mov    0x8(%ebp),%eax
  800421:	8b 55 0c             	mov    0xc(%ebp),%edx
  800424:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800427:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80042a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80042d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800432:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800435:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800438:	39 d3                	cmp    %edx,%ebx
  80043a:	72 05                	jb     800441 <printnum+0x30>
  80043c:	39 45 10             	cmp    %eax,0x10(%ebp)
  80043f:	77 45                	ja     800486 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800441:	83 ec 0c             	sub    $0xc,%esp
  800444:	ff 75 18             	pushl  0x18(%ebp)
  800447:	8b 45 14             	mov    0x14(%ebp),%eax
  80044a:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80044d:	53                   	push   %ebx
  80044e:	ff 75 10             	pushl  0x10(%ebp)
  800451:	83 ec 08             	sub    $0x8,%esp
  800454:	ff 75 e4             	pushl  -0x1c(%ebp)
  800457:	ff 75 e0             	pushl  -0x20(%ebp)
  80045a:	ff 75 dc             	pushl  -0x24(%ebp)
  80045d:	ff 75 d8             	pushl  -0x28(%ebp)
  800460:	e8 3f 1b 00 00       	call   801fa4 <__udivdi3>
  800465:	83 c4 18             	add    $0x18,%esp
  800468:	52                   	push   %edx
  800469:	50                   	push   %eax
  80046a:	89 f2                	mov    %esi,%edx
  80046c:	89 f8                	mov    %edi,%eax
  80046e:	e8 9e ff ff ff       	call   800411 <printnum>
  800473:	83 c4 20             	add    $0x20,%esp
  800476:	eb 16                	jmp    80048e <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800478:	83 ec 08             	sub    $0x8,%esp
  80047b:	56                   	push   %esi
  80047c:	ff 75 18             	pushl  0x18(%ebp)
  80047f:	ff d7                	call   *%edi
  800481:	83 c4 10             	add    $0x10,%esp
  800484:	eb 03                	jmp    800489 <printnum+0x78>
  800486:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800489:	4b                   	dec    %ebx
  80048a:	85 db                	test   %ebx,%ebx
  80048c:	7f ea                	jg     800478 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80048e:	83 ec 08             	sub    $0x8,%esp
  800491:	56                   	push   %esi
  800492:	83 ec 04             	sub    $0x4,%esp
  800495:	ff 75 e4             	pushl  -0x1c(%ebp)
  800498:	ff 75 e0             	pushl  -0x20(%ebp)
  80049b:	ff 75 dc             	pushl  -0x24(%ebp)
  80049e:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a1:	e8 0e 1c 00 00       	call   8020b4 <__umoddi3>
  8004a6:	83 c4 14             	add    $0x14,%esp
  8004a9:	0f be 80 db 22 80 00 	movsbl 0x8022db(%eax),%eax
  8004b0:	50                   	push   %eax
  8004b1:	ff d7                	call   *%edi
}
  8004b3:	83 c4 10             	add    $0x10,%esp
  8004b6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004b9:	5b                   	pop    %ebx
  8004ba:	5e                   	pop    %esi
  8004bb:	5f                   	pop    %edi
  8004bc:	5d                   	pop    %ebp
  8004bd:	c3                   	ret    

008004be <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004be:	55                   	push   %ebp
  8004bf:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8004c1:	83 fa 01             	cmp    $0x1,%edx
  8004c4:	7e 0e                	jle    8004d4 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8004c6:	8b 10                	mov    (%eax),%edx
  8004c8:	8d 4a 08             	lea    0x8(%edx),%ecx
  8004cb:	89 08                	mov    %ecx,(%eax)
  8004cd:	8b 02                	mov    (%edx),%eax
  8004cf:	8b 52 04             	mov    0x4(%edx),%edx
  8004d2:	eb 22                	jmp    8004f6 <getuint+0x38>
	else if (lflag)
  8004d4:	85 d2                	test   %edx,%edx
  8004d6:	74 10                	je     8004e8 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8004d8:	8b 10                	mov    (%eax),%edx
  8004da:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004dd:	89 08                	mov    %ecx,(%eax)
  8004df:	8b 02                	mov    (%edx),%eax
  8004e1:	ba 00 00 00 00       	mov    $0x0,%edx
  8004e6:	eb 0e                	jmp    8004f6 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8004e8:	8b 10                	mov    (%eax),%edx
  8004ea:	8d 4a 04             	lea    0x4(%edx),%ecx
  8004ed:	89 08                	mov    %ecx,(%eax)
  8004ef:	8b 02                	mov    (%edx),%eax
  8004f1:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8004f6:	5d                   	pop    %ebp
  8004f7:	c3                   	ret    

008004f8 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8004f8:	55                   	push   %ebp
  8004f9:	89 e5                	mov    %esp,%ebp
  8004fb:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8004fe:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800501:	8b 10                	mov    (%eax),%edx
  800503:	3b 50 04             	cmp    0x4(%eax),%edx
  800506:	73 0a                	jae    800512 <sprintputch+0x1a>
		*b->buf++ = ch;
  800508:	8d 4a 01             	lea    0x1(%edx),%ecx
  80050b:	89 08                	mov    %ecx,(%eax)
  80050d:	8b 45 08             	mov    0x8(%ebp),%eax
  800510:	88 02                	mov    %al,(%edx)
}
  800512:	5d                   	pop    %ebp
  800513:	c3                   	ret    

00800514 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800514:	55                   	push   %ebp
  800515:	89 e5                	mov    %esp,%ebp
  800517:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80051a:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80051d:	50                   	push   %eax
  80051e:	ff 75 10             	pushl  0x10(%ebp)
  800521:	ff 75 0c             	pushl  0xc(%ebp)
  800524:	ff 75 08             	pushl  0x8(%ebp)
  800527:	e8 05 00 00 00       	call   800531 <vprintfmt>
	va_end(ap);
}
  80052c:	83 c4 10             	add    $0x10,%esp
  80052f:	c9                   	leave  
  800530:	c3                   	ret    

00800531 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800531:	55                   	push   %ebp
  800532:	89 e5                	mov    %esp,%ebp
  800534:	57                   	push   %edi
  800535:	56                   	push   %esi
  800536:	53                   	push   %ebx
  800537:	83 ec 2c             	sub    $0x2c,%esp
  80053a:	8b 75 08             	mov    0x8(%ebp),%esi
  80053d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800540:	8b 7d 10             	mov    0x10(%ebp),%edi
  800543:	eb 12                	jmp    800557 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800545:	85 c0                	test   %eax,%eax
  800547:	0f 84 68 03 00 00    	je     8008b5 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80054d:	83 ec 08             	sub    $0x8,%esp
  800550:	53                   	push   %ebx
  800551:	50                   	push   %eax
  800552:	ff d6                	call   *%esi
  800554:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800557:	47                   	inc    %edi
  800558:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80055c:	83 f8 25             	cmp    $0x25,%eax
  80055f:	75 e4                	jne    800545 <vprintfmt+0x14>
  800561:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800565:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80056c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800573:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80057a:	ba 00 00 00 00       	mov    $0x0,%edx
  80057f:	eb 07                	jmp    800588 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800581:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800584:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800588:	8d 47 01             	lea    0x1(%edi),%eax
  80058b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80058e:	0f b6 0f             	movzbl (%edi),%ecx
  800591:	8a 07                	mov    (%edi),%al
  800593:	83 e8 23             	sub    $0x23,%eax
  800596:	3c 55                	cmp    $0x55,%al
  800598:	0f 87 fe 02 00 00    	ja     80089c <vprintfmt+0x36b>
  80059e:	0f b6 c0             	movzbl %al,%eax
  8005a1:	ff 24 85 20 24 80 00 	jmp    *0x802420(,%eax,4)
  8005a8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005ab:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005af:	eb d7                	jmp    800588 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005b1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005b4:	b8 00 00 00 00       	mov    $0x0,%eax
  8005b9:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005bc:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8005bf:	01 c0                	add    %eax,%eax
  8005c1:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8005c5:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8005c8:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8005cb:	83 fa 09             	cmp    $0x9,%edx
  8005ce:	77 34                	ja     800604 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8005d0:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8005d1:	eb e9                	jmp    8005bc <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8005d3:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d6:	8d 48 04             	lea    0x4(%eax),%ecx
  8005d9:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8005dc:	8b 00                	mov    (%eax),%eax
  8005de:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8005e4:	eb 24                	jmp    80060a <vprintfmt+0xd9>
  8005e6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005ea:	79 07                	jns    8005f3 <vprintfmt+0xc2>
  8005ec:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f6:	eb 90                	jmp    800588 <vprintfmt+0x57>
  8005f8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8005fb:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800602:	eb 84                	jmp    800588 <vprintfmt+0x57>
  800604:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800607:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80060a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80060e:	0f 89 74 ff ff ff    	jns    800588 <vprintfmt+0x57>
				width = precision, precision = -1;
  800614:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800617:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80061a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800621:	e9 62 ff ff ff       	jmp    800588 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800626:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800627:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80062a:	e9 59 ff ff ff       	jmp    800588 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80062f:	8b 45 14             	mov    0x14(%ebp),%eax
  800632:	8d 50 04             	lea    0x4(%eax),%edx
  800635:	89 55 14             	mov    %edx,0x14(%ebp)
  800638:	83 ec 08             	sub    $0x8,%esp
  80063b:	53                   	push   %ebx
  80063c:	ff 30                	pushl  (%eax)
  80063e:	ff d6                	call   *%esi
			break;
  800640:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800643:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800646:	e9 0c ff ff ff       	jmp    800557 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80064b:	8b 45 14             	mov    0x14(%ebp),%eax
  80064e:	8d 50 04             	lea    0x4(%eax),%edx
  800651:	89 55 14             	mov    %edx,0x14(%ebp)
  800654:	8b 00                	mov    (%eax),%eax
  800656:	85 c0                	test   %eax,%eax
  800658:	79 02                	jns    80065c <vprintfmt+0x12b>
  80065a:	f7 d8                	neg    %eax
  80065c:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80065e:	83 f8 0f             	cmp    $0xf,%eax
  800661:	7f 0b                	jg     80066e <vprintfmt+0x13d>
  800663:	8b 04 85 80 25 80 00 	mov    0x802580(,%eax,4),%eax
  80066a:	85 c0                	test   %eax,%eax
  80066c:	75 18                	jne    800686 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80066e:	52                   	push   %edx
  80066f:	68 f3 22 80 00       	push   $0x8022f3
  800674:	53                   	push   %ebx
  800675:	56                   	push   %esi
  800676:	e8 99 fe ff ff       	call   800514 <printfmt>
  80067b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80067e:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800681:	e9 d1 fe ff ff       	jmp    800557 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800686:	50                   	push   %eax
  800687:	68 b5 26 80 00       	push   $0x8026b5
  80068c:	53                   	push   %ebx
  80068d:	56                   	push   %esi
  80068e:	e8 81 fe ff ff       	call   800514 <printfmt>
  800693:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800696:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800699:	e9 b9 fe ff ff       	jmp    800557 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80069e:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a1:	8d 50 04             	lea    0x4(%eax),%edx
  8006a4:	89 55 14             	mov    %edx,0x14(%ebp)
  8006a7:	8b 38                	mov    (%eax),%edi
  8006a9:	85 ff                	test   %edi,%edi
  8006ab:	75 05                	jne    8006b2 <vprintfmt+0x181>
				p = "(null)";
  8006ad:	bf ec 22 80 00       	mov    $0x8022ec,%edi
			if (width > 0 && padc != '-')
  8006b2:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006b6:	0f 8e 90 00 00 00    	jle    80074c <vprintfmt+0x21b>
  8006bc:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8006c0:	0f 84 8e 00 00 00    	je     800754 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006c6:	83 ec 08             	sub    $0x8,%esp
  8006c9:	ff 75 d0             	pushl  -0x30(%ebp)
  8006cc:	57                   	push   %edi
  8006cd:	e8 70 02 00 00       	call   800942 <strnlen>
  8006d2:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8006d5:	29 c1                	sub    %eax,%ecx
  8006d7:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8006da:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8006dd:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8006e1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8006e4:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8006e7:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006e9:	eb 0d                	jmp    8006f8 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8006eb:	83 ec 08             	sub    $0x8,%esp
  8006ee:	53                   	push   %ebx
  8006ef:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006f2:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006f4:	4f                   	dec    %edi
  8006f5:	83 c4 10             	add    $0x10,%esp
  8006f8:	85 ff                	test   %edi,%edi
  8006fa:	7f ef                	jg     8006eb <vprintfmt+0x1ba>
  8006fc:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8006ff:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800702:	89 c8                	mov    %ecx,%eax
  800704:	85 c9                	test   %ecx,%ecx
  800706:	79 05                	jns    80070d <vprintfmt+0x1dc>
  800708:	b8 00 00 00 00       	mov    $0x0,%eax
  80070d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800710:	29 c1                	sub    %eax,%ecx
  800712:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800715:	89 75 08             	mov    %esi,0x8(%ebp)
  800718:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80071b:	eb 3d                	jmp    80075a <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80071d:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800721:	74 19                	je     80073c <vprintfmt+0x20b>
  800723:	0f be c0             	movsbl %al,%eax
  800726:	83 e8 20             	sub    $0x20,%eax
  800729:	83 f8 5e             	cmp    $0x5e,%eax
  80072c:	76 0e                	jbe    80073c <vprintfmt+0x20b>
					putch('?', putdat);
  80072e:	83 ec 08             	sub    $0x8,%esp
  800731:	53                   	push   %ebx
  800732:	6a 3f                	push   $0x3f
  800734:	ff 55 08             	call   *0x8(%ebp)
  800737:	83 c4 10             	add    $0x10,%esp
  80073a:	eb 0b                	jmp    800747 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80073c:	83 ec 08             	sub    $0x8,%esp
  80073f:	53                   	push   %ebx
  800740:	52                   	push   %edx
  800741:	ff 55 08             	call   *0x8(%ebp)
  800744:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800747:	ff 4d e4             	decl   -0x1c(%ebp)
  80074a:	eb 0e                	jmp    80075a <vprintfmt+0x229>
  80074c:	89 75 08             	mov    %esi,0x8(%ebp)
  80074f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800752:	eb 06                	jmp    80075a <vprintfmt+0x229>
  800754:	89 75 08             	mov    %esi,0x8(%ebp)
  800757:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075a:	47                   	inc    %edi
  80075b:	8a 47 ff             	mov    -0x1(%edi),%al
  80075e:	0f be d0             	movsbl %al,%edx
  800761:	85 d2                	test   %edx,%edx
  800763:	74 1d                	je     800782 <vprintfmt+0x251>
  800765:	85 f6                	test   %esi,%esi
  800767:	78 b4                	js     80071d <vprintfmt+0x1ec>
  800769:	4e                   	dec    %esi
  80076a:	79 b1                	jns    80071d <vprintfmt+0x1ec>
  80076c:	8b 75 08             	mov    0x8(%ebp),%esi
  80076f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800772:	eb 14                	jmp    800788 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800774:	83 ec 08             	sub    $0x8,%esp
  800777:	53                   	push   %ebx
  800778:	6a 20                	push   $0x20
  80077a:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80077c:	4f                   	dec    %edi
  80077d:	83 c4 10             	add    $0x10,%esp
  800780:	eb 06                	jmp    800788 <vprintfmt+0x257>
  800782:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800785:	8b 75 08             	mov    0x8(%ebp),%esi
  800788:	85 ff                	test   %edi,%edi
  80078a:	7f e8                	jg     800774 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80078c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80078f:	e9 c3 fd ff ff       	jmp    800557 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800794:	83 fa 01             	cmp    $0x1,%edx
  800797:	7e 16                	jle    8007af <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800799:	8b 45 14             	mov    0x14(%ebp),%eax
  80079c:	8d 50 08             	lea    0x8(%eax),%edx
  80079f:	89 55 14             	mov    %edx,0x14(%ebp)
  8007a2:	8b 50 04             	mov    0x4(%eax),%edx
  8007a5:	8b 00                	mov    (%eax),%eax
  8007a7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007aa:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ad:	eb 32                	jmp    8007e1 <vprintfmt+0x2b0>
	else if (lflag)
  8007af:	85 d2                	test   %edx,%edx
  8007b1:	74 18                	je     8007cb <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007b3:	8b 45 14             	mov    0x14(%ebp),%eax
  8007b6:	8d 50 04             	lea    0x4(%eax),%edx
  8007b9:	89 55 14             	mov    %edx,0x14(%ebp)
  8007bc:	8b 00                	mov    (%eax),%eax
  8007be:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007c1:	89 c1                	mov    %eax,%ecx
  8007c3:	c1 f9 1f             	sar    $0x1f,%ecx
  8007c6:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8007c9:	eb 16                	jmp    8007e1 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8007cb:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ce:	8d 50 04             	lea    0x4(%eax),%edx
  8007d1:	89 55 14             	mov    %edx,0x14(%ebp)
  8007d4:	8b 00                	mov    (%eax),%eax
  8007d6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007d9:	89 c1                	mov    %eax,%ecx
  8007db:	c1 f9 1f             	sar    $0x1f,%ecx
  8007de:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8007e1:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007e4:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8007e7:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8007eb:	79 76                	jns    800863 <vprintfmt+0x332>
				putch('-', putdat);
  8007ed:	83 ec 08             	sub    $0x8,%esp
  8007f0:	53                   	push   %ebx
  8007f1:	6a 2d                	push   $0x2d
  8007f3:	ff d6                	call   *%esi
				num = -(long long) num;
  8007f5:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8007f8:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8007fb:	f7 d8                	neg    %eax
  8007fd:	83 d2 00             	adc    $0x0,%edx
  800800:	f7 da                	neg    %edx
  800802:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800805:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80080a:	eb 5c                	jmp    800868 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80080c:	8d 45 14             	lea    0x14(%ebp),%eax
  80080f:	e8 aa fc ff ff       	call   8004be <getuint>
			base = 10;
  800814:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800819:	eb 4d                	jmp    800868 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80081b:	8d 45 14             	lea    0x14(%ebp),%eax
  80081e:	e8 9b fc ff ff       	call   8004be <getuint>
			base = 8;
  800823:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800828:	eb 3e                	jmp    800868 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80082a:	83 ec 08             	sub    $0x8,%esp
  80082d:	53                   	push   %ebx
  80082e:	6a 30                	push   $0x30
  800830:	ff d6                	call   *%esi
			putch('x', putdat);
  800832:	83 c4 08             	add    $0x8,%esp
  800835:	53                   	push   %ebx
  800836:	6a 78                	push   $0x78
  800838:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80083a:	8b 45 14             	mov    0x14(%ebp),%eax
  80083d:	8d 50 04             	lea    0x4(%eax),%edx
  800840:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800843:	8b 00                	mov    (%eax),%eax
  800845:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80084a:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80084d:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800852:	eb 14                	jmp    800868 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800854:	8d 45 14             	lea    0x14(%ebp),%eax
  800857:	e8 62 fc ff ff       	call   8004be <getuint>
			base = 16;
  80085c:	b9 10 00 00 00       	mov    $0x10,%ecx
  800861:	eb 05                	jmp    800868 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800863:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800868:	83 ec 0c             	sub    $0xc,%esp
  80086b:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80086f:	57                   	push   %edi
  800870:	ff 75 e4             	pushl  -0x1c(%ebp)
  800873:	51                   	push   %ecx
  800874:	52                   	push   %edx
  800875:	50                   	push   %eax
  800876:	89 da                	mov    %ebx,%edx
  800878:	89 f0                	mov    %esi,%eax
  80087a:	e8 92 fb ff ff       	call   800411 <printnum>
			break;
  80087f:	83 c4 20             	add    $0x20,%esp
  800882:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800885:	e9 cd fc ff ff       	jmp    800557 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80088a:	83 ec 08             	sub    $0x8,%esp
  80088d:	53                   	push   %ebx
  80088e:	51                   	push   %ecx
  80088f:	ff d6                	call   *%esi
			break;
  800891:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800894:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800897:	e9 bb fc ff ff       	jmp    800557 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80089c:	83 ec 08             	sub    $0x8,%esp
  80089f:	53                   	push   %ebx
  8008a0:	6a 25                	push   $0x25
  8008a2:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008a4:	83 c4 10             	add    $0x10,%esp
  8008a7:	eb 01                	jmp    8008aa <vprintfmt+0x379>
  8008a9:	4f                   	dec    %edi
  8008aa:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008ae:	75 f9                	jne    8008a9 <vprintfmt+0x378>
  8008b0:	e9 a2 fc ff ff       	jmp    800557 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008b5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008b8:	5b                   	pop    %ebx
  8008b9:	5e                   	pop    %esi
  8008ba:	5f                   	pop    %edi
  8008bb:	5d                   	pop    %ebp
  8008bc:	c3                   	ret    

008008bd <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008bd:	55                   	push   %ebp
  8008be:	89 e5                	mov    %esp,%ebp
  8008c0:	83 ec 18             	sub    $0x18,%esp
  8008c3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c6:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008cc:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8008d0:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8008d3:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8008da:	85 c0                	test   %eax,%eax
  8008dc:	74 26                	je     800904 <vsnprintf+0x47>
  8008de:	85 d2                	test   %edx,%edx
  8008e0:	7e 29                	jle    80090b <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8008e2:	ff 75 14             	pushl  0x14(%ebp)
  8008e5:	ff 75 10             	pushl  0x10(%ebp)
  8008e8:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8008eb:	50                   	push   %eax
  8008ec:	68 f8 04 80 00       	push   $0x8004f8
  8008f1:	e8 3b fc ff ff       	call   800531 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8008f6:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8008f9:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8008fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8008ff:	83 c4 10             	add    $0x10,%esp
  800902:	eb 0c                	jmp    800910 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800904:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800909:	eb 05                	jmp    800910 <vsnprintf+0x53>
  80090b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800910:	c9                   	leave  
  800911:	c3                   	ret    

00800912 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800912:	55                   	push   %ebp
  800913:	89 e5                	mov    %esp,%ebp
  800915:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800918:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80091b:	50                   	push   %eax
  80091c:	ff 75 10             	pushl  0x10(%ebp)
  80091f:	ff 75 0c             	pushl  0xc(%ebp)
  800922:	ff 75 08             	pushl  0x8(%ebp)
  800925:	e8 93 ff ff ff       	call   8008bd <vsnprintf>
	va_end(ap);

	return rc;
}
  80092a:	c9                   	leave  
  80092b:	c3                   	ret    

0080092c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80092c:	55                   	push   %ebp
  80092d:	89 e5                	mov    %esp,%ebp
  80092f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800932:	b8 00 00 00 00       	mov    $0x0,%eax
  800937:	eb 01                	jmp    80093a <strlen+0xe>
		n++;
  800939:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80093a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80093e:	75 f9                	jne    800939 <strlen+0xd>
		n++;
	return n;
}
  800940:	5d                   	pop    %ebp
  800941:	c3                   	ret    

00800942 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800942:	55                   	push   %ebp
  800943:	89 e5                	mov    %esp,%ebp
  800945:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800948:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80094b:	ba 00 00 00 00       	mov    $0x0,%edx
  800950:	eb 01                	jmp    800953 <strnlen+0x11>
		n++;
  800952:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800953:	39 c2                	cmp    %eax,%edx
  800955:	74 08                	je     80095f <strnlen+0x1d>
  800957:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80095b:	75 f5                	jne    800952 <strnlen+0x10>
  80095d:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80095f:	5d                   	pop    %ebp
  800960:	c3                   	ret    

00800961 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800961:	55                   	push   %ebp
  800962:	89 e5                	mov    %esp,%ebp
  800964:	53                   	push   %ebx
  800965:	8b 45 08             	mov    0x8(%ebp),%eax
  800968:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80096b:	89 c2                	mov    %eax,%edx
  80096d:	42                   	inc    %edx
  80096e:	41                   	inc    %ecx
  80096f:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800972:	88 5a ff             	mov    %bl,-0x1(%edx)
  800975:	84 db                	test   %bl,%bl
  800977:	75 f4                	jne    80096d <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800979:	5b                   	pop    %ebx
  80097a:	5d                   	pop    %ebp
  80097b:	c3                   	ret    

0080097c <strcat>:

char *
strcat(char *dst, const char *src)
{
  80097c:	55                   	push   %ebp
  80097d:	89 e5                	mov    %esp,%ebp
  80097f:	53                   	push   %ebx
  800980:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800983:	53                   	push   %ebx
  800984:	e8 a3 ff ff ff       	call   80092c <strlen>
  800989:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80098c:	ff 75 0c             	pushl  0xc(%ebp)
  80098f:	01 d8                	add    %ebx,%eax
  800991:	50                   	push   %eax
  800992:	e8 ca ff ff ff       	call   800961 <strcpy>
	return dst;
}
  800997:	89 d8                	mov    %ebx,%eax
  800999:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80099c:	c9                   	leave  
  80099d:	c3                   	ret    

0080099e <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80099e:	55                   	push   %ebp
  80099f:	89 e5                	mov    %esp,%ebp
  8009a1:	56                   	push   %esi
  8009a2:	53                   	push   %ebx
  8009a3:	8b 75 08             	mov    0x8(%ebp),%esi
  8009a6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009a9:	89 f3                	mov    %esi,%ebx
  8009ab:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ae:	89 f2                	mov    %esi,%edx
  8009b0:	eb 0c                	jmp    8009be <strncpy+0x20>
		*dst++ = *src;
  8009b2:	42                   	inc    %edx
  8009b3:	8a 01                	mov    (%ecx),%al
  8009b5:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009b8:	80 39 01             	cmpb   $0x1,(%ecx)
  8009bb:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009be:	39 da                	cmp    %ebx,%edx
  8009c0:	75 f0                	jne    8009b2 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8009c2:	89 f0                	mov    %esi,%eax
  8009c4:	5b                   	pop    %ebx
  8009c5:	5e                   	pop    %esi
  8009c6:	5d                   	pop    %ebp
  8009c7:	c3                   	ret    

008009c8 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8009c8:	55                   	push   %ebp
  8009c9:	89 e5                	mov    %esp,%ebp
  8009cb:	56                   	push   %esi
  8009cc:	53                   	push   %ebx
  8009cd:	8b 75 08             	mov    0x8(%ebp),%esi
  8009d0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009d3:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8009d6:	85 c0                	test   %eax,%eax
  8009d8:	74 1e                	je     8009f8 <strlcpy+0x30>
  8009da:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8009de:	89 f2                	mov    %esi,%edx
  8009e0:	eb 05                	jmp    8009e7 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8009e2:	42                   	inc    %edx
  8009e3:	41                   	inc    %ecx
  8009e4:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009e7:	39 c2                	cmp    %eax,%edx
  8009e9:	74 08                	je     8009f3 <strlcpy+0x2b>
  8009eb:	8a 19                	mov    (%ecx),%bl
  8009ed:	84 db                	test   %bl,%bl
  8009ef:	75 f1                	jne    8009e2 <strlcpy+0x1a>
  8009f1:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8009f3:	c6 00 00             	movb   $0x0,(%eax)
  8009f6:	eb 02                	jmp    8009fa <strlcpy+0x32>
  8009f8:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8009fa:	29 f0                	sub    %esi,%eax
}
  8009fc:	5b                   	pop    %ebx
  8009fd:	5e                   	pop    %esi
  8009fe:	5d                   	pop    %ebp
  8009ff:	c3                   	ret    

00800a00 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a00:	55                   	push   %ebp
  800a01:	89 e5                	mov    %esp,%ebp
  800a03:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a06:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a09:	eb 02                	jmp    800a0d <strcmp+0xd>
		p++, q++;
  800a0b:	41                   	inc    %ecx
  800a0c:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a0d:	8a 01                	mov    (%ecx),%al
  800a0f:	84 c0                	test   %al,%al
  800a11:	74 04                	je     800a17 <strcmp+0x17>
  800a13:	3a 02                	cmp    (%edx),%al
  800a15:	74 f4                	je     800a0b <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a17:	0f b6 c0             	movzbl %al,%eax
  800a1a:	0f b6 12             	movzbl (%edx),%edx
  800a1d:	29 d0                	sub    %edx,%eax
}
  800a1f:	5d                   	pop    %ebp
  800a20:	c3                   	ret    

00800a21 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a21:	55                   	push   %ebp
  800a22:	89 e5                	mov    %esp,%ebp
  800a24:	53                   	push   %ebx
  800a25:	8b 45 08             	mov    0x8(%ebp),%eax
  800a28:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a2b:	89 c3                	mov    %eax,%ebx
  800a2d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a30:	eb 02                	jmp    800a34 <strncmp+0x13>
		n--, p++, q++;
  800a32:	40                   	inc    %eax
  800a33:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a34:	39 d8                	cmp    %ebx,%eax
  800a36:	74 14                	je     800a4c <strncmp+0x2b>
  800a38:	8a 08                	mov    (%eax),%cl
  800a3a:	84 c9                	test   %cl,%cl
  800a3c:	74 04                	je     800a42 <strncmp+0x21>
  800a3e:	3a 0a                	cmp    (%edx),%cl
  800a40:	74 f0                	je     800a32 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a42:	0f b6 00             	movzbl (%eax),%eax
  800a45:	0f b6 12             	movzbl (%edx),%edx
  800a48:	29 d0                	sub    %edx,%eax
  800a4a:	eb 05                	jmp    800a51 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a4c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a51:	5b                   	pop    %ebx
  800a52:	5d                   	pop    %ebp
  800a53:	c3                   	ret    

00800a54 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a54:	55                   	push   %ebp
  800a55:	89 e5                	mov    %esp,%ebp
  800a57:	8b 45 08             	mov    0x8(%ebp),%eax
  800a5a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a5d:	eb 05                	jmp    800a64 <strchr+0x10>
		if (*s == c)
  800a5f:	38 ca                	cmp    %cl,%dl
  800a61:	74 0c                	je     800a6f <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a63:	40                   	inc    %eax
  800a64:	8a 10                	mov    (%eax),%dl
  800a66:	84 d2                	test   %dl,%dl
  800a68:	75 f5                	jne    800a5f <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800a6a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a6f:	5d                   	pop    %ebp
  800a70:	c3                   	ret    

00800a71 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a71:	55                   	push   %ebp
  800a72:	89 e5                	mov    %esp,%ebp
  800a74:	8b 45 08             	mov    0x8(%ebp),%eax
  800a77:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a7a:	eb 05                	jmp    800a81 <strfind+0x10>
		if (*s == c)
  800a7c:	38 ca                	cmp    %cl,%dl
  800a7e:	74 07                	je     800a87 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a80:	40                   	inc    %eax
  800a81:	8a 10                	mov    (%eax),%dl
  800a83:	84 d2                	test   %dl,%dl
  800a85:	75 f5                	jne    800a7c <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800a87:	5d                   	pop    %ebp
  800a88:	c3                   	ret    

00800a89 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a89:	55                   	push   %ebp
  800a8a:	89 e5                	mov    %esp,%ebp
  800a8c:	57                   	push   %edi
  800a8d:	56                   	push   %esi
  800a8e:	53                   	push   %ebx
  800a8f:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a92:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a95:	85 c9                	test   %ecx,%ecx
  800a97:	74 36                	je     800acf <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800a99:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800a9f:	75 28                	jne    800ac9 <memset+0x40>
  800aa1:	f6 c1 03             	test   $0x3,%cl
  800aa4:	75 23                	jne    800ac9 <memset+0x40>
		c &= 0xFF;
  800aa6:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aaa:	89 d3                	mov    %edx,%ebx
  800aac:	c1 e3 08             	shl    $0x8,%ebx
  800aaf:	89 d6                	mov    %edx,%esi
  800ab1:	c1 e6 18             	shl    $0x18,%esi
  800ab4:	89 d0                	mov    %edx,%eax
  800ab6:	c1 e0 10             	shl    $0x10,%eax
  800ab9:	09 f0                	or     %esi,%eax
  800abb:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800abd:	89 d8                	mov    %ebx,%eax
  800abf:	09 d0                	or     %edx,%eax
  800ac1:	c1 e9 02             	shr    $0x2,%ecx
  800ac4:	fc                   	cld    
  800ac5:	f3 ab                	rep stos %eax,%es:(%edi)
  800ac7:	eb 06                	jmp    800acf <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800ac9:	8b 45 0c             	mov    0xc(%ebp),%eax
  800acc:	fc                   	cld    
  800acd:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800acf:	89 f8                	mov    %edi,%eax
  800ad1:	5b                   	pop    %ebx
  800ad2:	5e                   	pop    %esi
  800ad3:	5f                   	pop    %edi
  800ad4:	5d                   	pop    %ebp
  800ad5:	c3                   	ret    

00800ad6 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800ad6:	55                   	push   %ebp
  800ad7:	89 e5                	mov    %esp,%ebp
  800ad9:	57                   	push   %edi
  800ada:	56                   	push   %esi
  800adb:	8b 45 08             	mov    0x8(%ebp),%eax
  800ade:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ae1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ae4:	39 c6                	cmp    %eax,%esi
  800ae6:	73 33                	jae    800b1b <memmove+0x45>
  800ae8:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800aeb:	39 d0                	cmp    %edx,%eax
  800aed:	73 2c                	jae    800b1b <memmove+0x45>
		s += n;
		d += n;
  800aef:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800af2:	89 d6                	mov    %edx,%esi
  800af4:	09 fe                	or     %edi,%esi
  800af6:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800afc:	75 13                	jne    800b11 <memmove+0x3b>
  800afe:	f6 c1 03             	test   $0x3,%cl
  800b01:	75 0e                	jne    800b11 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b03:	83 ef 04             	sub    $0x4,%edi
  800b06:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b09:	c1 e9 02             	shr    $0x2,%ecx
  800b0c:	fd                   	std    
  800b0d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b0f:	eb 07                	jmp    800b18 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b11:	4f                   	dec    %edi
  800b12:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b15:	fd                   	std    
  800b16:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b18:	fc                   	cld    
  800b19:	eb 1d                	jmp    800b38 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b1b:	89 f2                	mov    %esi,%edx
  800b1d:	09 c2                	or     %eax,%edx
  800b1f:	f6 c2 03             	test   $0x3,%dl
  800b22:	75 0f                	jne    800b33 <memmove+0x5d>
  800b24:	f6 c1 03             	test   $0x3,%cl
  800b27:	75 0a                	jne    800b33 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b29:	c1 e9 02             	shr    $0x2,%ecx
  800b2c:	89 c7                	mov    %eax,%edi
  800b2e:	fc                   	cld    
  800b2f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b31:	eb 05                	jmp    800b38 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b33:	89 c7                	mov    %eax,%edi
  800b35:	fc                   	cld    
  800b36:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b38:	5e                   	pop    %esi
  800b39:	5f                   	pop    %edi
  800b3a:	5d                   	pop    %ebp
  800b3b:	c3                   	ret    

00800b3c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b3c:	55                   	push   %ebp
  800b3d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b3f:	ff 75 10             	pushl  0x10(%ebp)
  800b42:	ff 75 0c             	pushl  0xc(%ebp)
  800b45:	ff 75 08             	pushl  0x8(%ebp)
  800b48:	e8 89 ff ff ff       	call   800ad6 <memmove>
}
  800b4d:	c9                   	leave  
  800b4e:	c3                   	ret    

00800b4f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b4f:	55                   	push   %ebp
  800b50:	89 e5                	mov    %esp,%ebp
  800b52:	56                   	push   %esi
  800b53:	53                   	push   %ebx
  800b54:	8b 45 08             	mov    0x8(%ebp),%eax
  800b57:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b5a:	89 c6                	mov    %eax,%esi
  800b5c:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b5f:	eb 14                	jmp    800b75 <memcmp+0x26>
		if (*s1 != *s2)
  800b61:	8a 08                	mov    (%eax),%cl
  800b63:	8a 1a                	mov    (%edx),%bl
  800b65:	38 d9                	cmp    %bl,%cl
  800b67:	74 0a                	je     800b73 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800b69:	0f b6 c1             	movzbl %cl,%eax
  800b6c:	0f b6 db             	movzbl %bl,%ebx
  800b6f:	29 d8                	sub    %ebx,%eax
  800b71:	eb 0b                	jmp    800b7e <memcmp+0x2f>
		s1++, s2++;
  800b73:	40                   	inc    %eax
  800b74:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800b75:	39 f0                	cmp    %esi,%eax
  800b77:	75 e8                	jne    800b61 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b79:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b7e:	5b                   	pop    %ebx
  800b7f:	5e                   	pop    %esi
  800b80:	5d                   	pop    %ebp
  800b81:	c3                   	ret    

00800b82 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800b82:	55                   	push   %ebp
  800b83:	89 e5                	mov    %esp,%ebp
  800b85:	53                   	push   %ebx
  800b86:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b89:	89 c1                	mov    %eax,%ecx
  800b8b:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b8e:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b92:	eb 08                	jmp    800b9c <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b94:	0f b6 10             	movzbl (%eax),%edx
  800b97:	39 da                	cmp    %ebx,%edx
  800b99:	74 05                	je     800ba0 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b9b:	40                   	inc    %eax
  800b9c:	39 c8                	cmp    %ecx,%eax
  800b9e:	72 f4                	jb     800b94 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800ba0:	5b                   	pop    %ebx
  800ba1:	5d                   	pop    %ebp
  800ba2:	c3                   	ret    

00800ba3 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800ba3:	55                   	push   %ebp
  800ba4:	89 e5                	mov    %esp,%ebp
  800ba6:	57                   	push   %edi
  800ba7:	56                   	push   %esi
  800ba8:	53                   	push   %ebx
  800ba9:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bac:	eb 01                	jmp    800baf <strtol+0xc>
		s++;
  800bae:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800baf:	8a 01                	mov    (%ecx),%al
  800bb1:	3c 20                	cmp    $0x20,%al
  800bb3:	74 f9                	je     800bae <strtol+0xb>
  800bb5:	3c 09                	cmp    $0x9,%al
  800bb7:	74 f5                	je     800bae <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bb9:	3c 2b                	cmp    $0x2b,%al
  800bbb:	75 08                	jne    800bc5 <strtol+0x22>
		s++;
  800bbd:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bbe:	bf 00 00 00 00       	mov    $0x0,%edi
  800bc3:	eb 11                	jmp    800bd6 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800bc5:	3c 2d                	cmp    $0x2d,%al
  800bc7:	75 08                	jne    800bd1 <strtol+0x2e>
		s++, neg = 1;
  800bc9:	41                   	inc    %ecx
  800bca:	bf 01 00 00 00       	mov    $0x1,%edi
  800bcf:	eb 05                	jmp    800bd6 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bd1:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bd6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bda:	0f 84 87 00 00 00    	je     800c67 <strtol+0xc4>
  800be0:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800be4:	75 27                	jne    800c0d <strtol+0x6a>
  800be6:	80 39 30             	cmpb   $0x30,(%ecx)
  800be9:	75 22                	jne    800c0d <strtol+0x6a>
  800beb:	e9 88 00 00 00       	jmp    800c78 <strtol+0xd5>
		s += 2, base = 16;
  800bf0:	83 c1 02             	add    $0x2,%ecx
  800bf3:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800bfa:	eb 11                	jmp    800c0d <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800bfc:	41                   	inc    %ecx
  800bfd:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c04:	eb 07                	jmp    800c0d <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c06:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c0d:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c12:	8a 11                	mov    (%ecx),%dl
  800c14:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c17:	80 fb 09             	cmp    $0x9,%bl
  800c1a:	77 08                	ja     800c24 <strtol+0x81>
			dig = *s - '0';
  800c1c:	0f be d2             	movsbl %dl,%edx
  800c1f:	83 ea 30             	sub    $0x30,%edx
  800c22:	eb 22                	jmp    800c46 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c24:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c27:	89 f3                	mov    %esi,%ebx
  800c29:	80 fb 19             	cmp    $0x19,%bl
  800c2c:	77 08                	ja     800c36 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c2e:	0f be d2             	movsbl %dl,%edx
  800c31:	83 ea 57             	sub    $0x57,%edx
  800c34:	eb 10                	jmp    800c46 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c36:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c39:	89 f3                	mov    %esi,%ebx
  800c3b:	80 fb 19             	cmp    $0x19,%bl
  800c3e:	77 14                	ja     800c54 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c40:	0f be d2             	movsbl %dl,%edx
  800c43:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c46:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c49:	7d 09                	jge    800c54 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c4b:	41                   	inc    %ecx
  800c4c:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c50:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c52:	eb be                	jmp    800c12 <strtol+0x6f>

	if (endptr)
  800c54:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c58:	74 05                	je     800c5f <strtol+0xbc>
		*endptr = (char *) s;
  800c5a:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c5d:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800c5f:	85 ff                	test   %edi,%edi
  800c61:	74 21                	je     800c84 <strtol+0xe1>
  800c63:	f7 d8                	neg    %eax
  800c65:	eb 1d                	jmp    800c84 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c67:	80 39 30             	cmpb   $0x30,(%ecx)
  800c6a:	75 9a                	jne    800c06 <strtol+0x63>
  800c6c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c70:	0f 84 7a ff ff ff    	je     800bf0 <strtol+0x4d>
  800c76:	eb 84                	jmp    800bfc <strtol+0x59>
  800c78:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800c7c:	0f 84 6e ff ff ff    	je     800bf0 <strtol+0x4d>
  800c82:	eb 89                	jmp    800c0d <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800c84:	5b                   	pop    %ebx
  800c85:	5e                   	pop    %esi
  800c86:	5f                   	pop    %edi
  800c87:	5d                   	pop    %ebp
  800c88:	c3                   	ret    

00800c89 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800c89:	55                   	push   %ebp
  800c8a:	89 e5                	mov    %esp,%ebp
  800c8c:	57                   	push   %edi
  800c8d:	56                   	push   %esi
  800c8e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c8f:	b8 00 00 00 00       	mov    $0x0,%eax
  800c94:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c97:	8b 55 08             	mov    0x8(%ebp),%edx
  800c9a:	89 c3                	mov    %eax,%ebx
  800c9c:	89 c7                	mov    %eax,%edi
  800c9e:	89 c6                	mov    %eax,%esi
  800ca0:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800ca2:	5b                   	pop    %ebx
  800ca3:	5e                   	pop    %esi
  800ca4:	5f                   	pop    %edi
  800ca5:	5d                   	pop    %ebp
  800ca6:	c3                   	ret    

00800ca7 <sys_cgetc>:

int
sys_cgetc(void)
{
  800ca7:	55                   	push   %ebp
  800ca8:	89 e5                	mov    %esp,%ebp
  800caa:	57                   	push   %edi
  800cab:	56                   	push   %esi
  800cac:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cad:	ba 00 00 00 00       	mov    $0x0,%edx
  800cb2:	b8 01 00 00 00       	mov    $0x1,%eax
  800cb7:	89 d1                	mov    %edx,%ecx
  800cb9:	89 d3                	mov    %edx,%ebx
  800cbb:	89 d7                	mov    %edx,%edi
  800cbd:	89 d6                	mov    %edx,%esi
  800cbf:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800cc1:	5b                   	pop    %ebx
  800cc2:	5e                   	pop    %esi
  800cc3:	5f                   	pop    %edi
  800cc4:	5d                   	pop    %ebp
  800cc5:	c3                   	ret    

00800cc6 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800cc6:	55                   	push   %ebp
  800cc7:	89 e5                	mov    %esp,%ebp
  800cc9:	57                   	push   %edi
  800cca:	56                   	push   %esi
  800ccb:	53                   	push   %ebx
  800ccc:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ccf:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cd4:	b8 03 00 00 00       	mov    $0x3,%eax
  800cd9:	8b 55 08             	mov    0x8(%ebp),%edx
  800cdc:	89 cb                	mov    %ecx,%ebx
  800cde:	89 cf                	mov    %ecx,%edi
  800ce0:	89 ce                	mov    %ecx,%esi
  800ce2:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ce4:	85 c0                	test   %eax,%eax
  800ce6:	7e 17                	jle    800cff <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ce8:	83 ec 0c             	sub    $0xc,%esp
  800ceb:	50                   	push   %eax
  800cec:	6a 03                	push   $0x3
  800cee:	68 df 25 80 00       	push   $0x8025df
  800cf3:	6a 23                	push   $0x23
  800cf5:	68 fc 25 80 00       	push   $0x8025fc
  800cfa:	e8 26 f6 ff ff       	call   800325 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800cff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d02:	5b                   	pop    %ebx
  800d03:	5e                   	pop    %esi
  800d04:	5f                   	pop    %edi
  800d05:	5d                   	pop    %ebp
  800d06:	c3                   	ret    

00800d07 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800d07:	55                   	push   %ebp
  800d08:	89 e5                	mov    %esp,%ebp
  800d0a:	57                   	push   %edi
  800d0b:	56                   	push   %esi
  800d0c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d0d:	ba 00 00 00 00       	mov    $0x0,%edx
  800d12:	b8 02 00 00 00       	mov    $0x2,%eax
  800d17:	89 d1                	mov    %edx,%ecx
  800d19:	89 d3                	mov    %edx,%ebx
  800d1b:	89 d7                	mov    %edx,%edi
  800d1d:	89 d6                	mov    %edx,%esi
  800d1f:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800d21:	5b                   	pop    %ebx
  800d22:	5e                   	pop    %esi
  800d23:	5f                   	pop    %edi
  800d24:	5d                   	pop    %ebp
  800d25:	c3                   	ret    

00800d26 <sys_yield>:

void
sys_yield(void)
{
  800d26:	55                   	push   %ebp
  800d27:	89 e5                	mov    %esp,%ebp
  800d29:	57                   	push   %edi
  800d2a:	56                   	push   %esi
  800d2b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d2c:	ba 00 00 00 00       	mov    $0x0,%edx
  800d31:	b8 0b 00 00 00       	mov    $0xb,%eax
  800d36:	89 d1                	mov    %edx,%ecx
  800d38:	89 d3                	mov    %edx,%ebx
  800d3a:	89 d7                	mov    %edx,%edi
  800d3c:	89 d6                	mov    %edx,%esi
  800d3e:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800d40:	5b                   	pop    %ebx
  800d41:	5e                   	pop    %esi
  800d42:	5f                   	pop    %edi
  800d43:	5d                   	pop    %ebp
  800d44:	c3                   	ret    

00800d45 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800d45:	55                   	push   %ebp
  800d46:	89 e5                	mov    %esp,%ebp
  800d48:	57                   	push   %edi
  800d49:	56                   	push   %esi
  800d4a:	53                   	push   %ebx
  800d4b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d4e:	be 00 00 00 00       	mov    $0x0,%esi
  800d53:	b8 04 00 00 00       	mov    $0x4,%eax
  800d58:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d5b:	8b 55 08             	mov    0x8(%ebp),%edx
  800d5e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d61:	89 f7                	mov    %esi,%edi
  800d63:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d65:	85 c0                	test   %eax,%eax
  800d67:	7e 17                	jle    800d80 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d69:	83 ec 0c             	sub    $0xc,%esp
  800d6c:	50                   	push   %eax
  800d6d:	6a 04                	push   $0x4
  800d6f:	68 df 25 80 00       	push   $0x8025df
  800d74:	6a 23                	push   $0x23
  800d76:	68 fc 25 80 00       	push   $0x8025fc
  800d7b:	e8 a5 f5 ff ff       	call   800325 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800d80:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d83:	5b                   	pop    %ebx
  800d84:	5e                   	pop    %esi
  800d85:	5f                   	pop    %edi
  800d86:	5d                   	pop    %ebp
  800d87:	c3                   	ret    

00800d88 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800d88:	55                   	push   %ebp
  800d89:	89 e5                	mov    %esp,%ebp
  800d8b:	57                   	push   %edi
  800d8c:	56                   	push   %esi
  800d8d:	53                   	push   %ebx
  800d8e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d91:	b8 05 00 00 00       	mov    $0x5,%eax
  800d96:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d99:	8b 55 08             	mov    0x8(%ebp),%edx
  800d9c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d9f:	8b 7d 14             	mov    0x14(%ebp),%edi
  800da2:	8b 75 18             	mov    0x18(%ebp),%esi
  800da5:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800da7:	85 c0                	test   %eax,%eax
  800da9:	7e 17                	jle    800dc2 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800dab:	83 ec 0c             	sub    $0xc,%esp
  800dae:	50                   	push   %eax
  800daf:	6a 05                	push   $0x5
  800db1:	68 df 25 80 00       	push   $0x8025df
  800db6:	6a 23                	push   $0x23
  800db8:	68 fc 25 80 00       	push   $0x8025fc
  800dbd:	e8 63 f5 ff ff       	call   800325 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800dc2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dc5:	5b                   	pop    %ebx
  800dc6:	5e                   	pop    %esi
  800dc7:	5f                   	pop    %edi
  800dc8:	5d                   	pop    %ebp
  800dc9:	c3                   	ret    

00800dca <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800dca:	55                   	push   %ebp
  800dcb:	89 e5                	mov    %esp,%ebp
  800dcd:	57                   	push   %edi
  800dce:	56                   	push   %esi
  800dcf:	53                   	push   %ebx
  800dd0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dd3:	bb 00 00 00 00       	mov    $0x0,%ebx
  800dd8:	b8 06 00 00 00       	mov    $0x6,%eax
  800ddd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800de0:	8b 55 08             	mov    0x8(%ebp),%edx
  800de3:	89 df                	mov    %ebx,%edi
  800de5:	89 de                	mov    %ebx,%esi
  800de7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800de9:	85 c0                	test   %eax,%eax
  800deb:	7e 17                	jle    800e04 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ded:	83 ec 0c             	sub    $0xc,%esp
  800df0:	50                   	push   %eax
  800df1:	6a 06                	push   $0x6
  800df3:	68 df 25 80 00       	push   $0x8025df
  800df8:	6a 23                	push   $0x23
  800dfa:	68 fc 25 80 00       	push   $0x8025fc
  800dff:	e8 21 f5 ff ff       	call   800325 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800e04:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e07:	5b                   	pop    %ebx
  800e08:	5e                   	pop    %esi
  800e09:	5f                   	pop    %edi
  800e0a:	5d                   	pop    %ebp
  800e0b:	c3                   	ret    

00800e0c <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800e0c:	55                   	push   %ebp
  800e0d:	89 e5                	mov    %esp,%ebp
  800e0f:	57                   	push   %edi
  800e10:	56                   	push   %esi
  800e11:	53                   	push   %ebx
  800e12:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e15:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e1a:	b8 08 00 00 00       	mov    $0x8,%eax
  800e1f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e22:	8b 55 08             	mov    0x8(%ebp),%edx
  800e25:	89 df                	mov    %ebx,%edi
  800e27:	89 de                	mov    %ebx,%esi
  800e29:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e2b:	85 c0                	test   %eax,%eax
  800e2d:	7e 17                	jle    800e46 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e2f:	83 ec 0c             	sub    $0xc,%esp
  800e32:	50                   	push   %eax
  800e33:	6a 08                	push   $0x8
  800e35:	68 df 25 80 00       	push   $0x8025df
  800e3a:	6a 23                	push   $0x23
  800e3c:	68 fc 25 80 00       	push   $0x8025fc
  800e41:	e8 df f4 ff ff       	call   800325 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800e46:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e49:	5b                   	pop    %ebx
  800e4a:	5e                   	pop    %esi
  800e4b:	5f                   	pop    %edi
  800e4c:	5d                   	pop    %ebp
  800e4d:	c3                   	ret    

00800e4e <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800e4e:	55                   	push   %ebp
  800e4f:	89 e5                	mov    %esp,%ebp
  800e51:	57                   	push   %edi
  800e52:	56                   	push   %esi
  800e53:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e54:	ba 00 00 00 00       	mov    $0x0,%edx
  800e59:	b8 0c 00 00 00       	mov    $0xc,%eax
  800e5e:	89 d1                	mov    %edx,%ecx
  800e60:	89 d3                	mov    %edx,%ebx
  800e62:	89 d7                	mov    %edx,%edi
  800e64:	89 d6                	mov    %edx,%esi
  800e66:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800e68:	5b                   	pop    %ebx
  800e69:	5e                   	pop    %esi
  800e6a:	5f                   	pop    %edi
  800e6b:	5d                   	pop    %ebp
  800e6c:	c3                   	ret    

00800e6d <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800e6d:	55                   	push   %ebp
  800e6e:	89 e5                	mov    %esp,%ebp
  800e70:	57                   	push   %edi
  800e71:	56                   	push   %esi
  800e72:	53                   	push   %ebx
  800e73:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e76:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e7b:	b8 09 00 00 00       	mov    $0x9,%eax
  800e80:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e83:	8b 55 08             	mov    0x8(%ebp),%edx
  800e86:	89 df                	mov    %ebx,%edi
  800e88:	89 de                	mov    %ebx,%esi
  800e8a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e8c:	85 c0                	test   %eax,%eax
  800e8e:	7e 17                	jle    800ea7 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e90:	83 ec 0c             	sub    $0xc,%esp
  800e93:	50                   	push   %eax
  800e94:	6a 09                	push   $0x9
  800e96:	68 df 25 80 00       	push   $0x8025df
  800e9b:	6a 23                	push   $0x23
  800e9d:	68 fc 25 80 00       	push   $0x8025fc
  800ea2:	e8 7e f4 ff ff       	call   800325 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800ea7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800eaa:	5b                   	pop    %ebx
  800eab:	5e                   	pop    %esi
  800eac:	5f                   	pop    %edi
  800ead:	5d                   	pop    %ebp
  800eae:	c3                   	ret    

00800eaf <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800eaf:	55                   	push   %ebp
  800eb0:	89 e5                	mov    %esp,%ebp
  800eb2:	57                   	push   %edi
  800eb3:	56                   	push   %esi
  800eb4:	53                   	push   %ebx
  800eb5:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800eb8:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ebd:	b8 0a 00 00 00       	mov    $0xa,%eax
  800ec2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ec5:	8b 55 08             	mov    0x8(%ebp),%edx
  800ec8:	89 df                	mov    %ebx,%edi
  800eca:	89 de                	mov    %ebx,%esi
  800ecc:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ece:	85 c0                	test   %eax,%eax
  800ed0:	7e 17                	jle    800ee9 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ed2:	83 ec 0c             	sub    $0xc,%esp
  800ed5:	50                   	push   %eax
  800ed6:	6a 0a                	push   $0xa
  800ed8:	68 df 25 80 00       	push   $0x8025df
  800edd:	6a 23                	push   $0x23
  800edf:	68 fc 25 80 00       	push   $0x8025fc
  800ee4:	e8 3c f4 ff ff       	call   800325 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800ee9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800eec:	5b                   	pop    %ebx
  800eed:	5e                   	pop    %esi
  800eee:	5f                   	pop    %edi
  800eef:	5d                   	pop    %ebp
  800ef0:	c3                   	ret    

00800ef1 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800ef1:	55                   	push   %ebp
  800ef2:	89 e5                	mov    %esp,%ebp
  800ef4:	57                   	push   %edi
  800ef5:	56                   	push   %esi
  800ef6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ef7:	be 00 00 00 00       	mov    $0x0,%esi
  800efc:	b8 0d 00 00 00       	mov    $0xd,%eax
  800f01:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f04:	8b 55 08             	mov    0x8(%ebp),%edx
  800f07:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800f0a:	8b 7d 14             	mov    0x14(%ebp),%edi
  800f0d:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800f0f:	5b                   	pop    %ebx
  800f10:	5e                   	pop    %esi
  800f11:	5f                   	pop    %edi
  800f12:	5d                   	pop    %ebp
  800f13:	c3                   	ret    

00800f14 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800f14:	55                   	push   %ebp
  800f15:	89 e5                	mov    %esp,%ebp
  800f17:	57                   	push   %edi
  800f18:	56                   	push   %esi
  800f19:	53                   	push   %ebx
  800f1a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f1d:	b9 00 00 00 00       	mov    $0x0,%ecx
  800f22:	b8 0e 00 00 00       	mov    $0xe,%eax
  800f27:	8b 55 08             	mov    0x8(%ebp),%edx
  800f2a:	89 cb                	mov    %ecx,%ebx
  800f2c:	89 cf                	mov    %ecx,%edi
  800f2e:	89 ce                	mov    %ecx,%esi
  800f30:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800f32:	85 c0                	test   %eax,%eax
  800f34:	7e 17                	jle    800f4d <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800f36:	83 ec 0c             	sub    $0xc,%esp
  800f39:	50                   	push   %eax
  800f3a:	6a 0e                	push   $0xe
  800f3c:	68 df 25 80 00       	push   $0x8025df
  800f41:	6a 23                	push   $0x23
  800f43:	68 fc 25 80 00       	push   $0x8025fc
  800f48:	e8 d8 f3 ff ff       	call   800325 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800f4d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800f50:	5b                   	pop    %ebx
  800f51:	5e                   	pop    %esi
  800f52:	5f                   	pop    %edi
  800f53:	5d                   	pop    %ebp
  800f54:	c3                   	ret    

00800f55 <argstart>:
#include <inc/args.h>
#include <inc/string.h>

void
argstart(int *argc, char **argv, struct Argstate *args)
{
  800f55:	55                   	push   %ebp
  800f56:	89 e5                	mov    %esp,%ebp
  800f58:	8b 55 08             	mov    0x8(%ebp),%edx
  800f5b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800f5e:	8b 45 10             	mov    0x10(%ebp),%eax
	args->argc = argc;
  800f61:	89 10                	mov    %edx,(%eax)
	args->argv = (const char **) argv;
  800f63:	89 48 04             	mov    %ecx,0x4(%eax)
	args->curarg = (*argc > 1 && argv ? "" : 0);
  800f66:	83 3a 01             	cmpl   $0x1,(%edx)
  800f69:	7e 0b                	jle    800f76 <argstart+0x21>
  800f6b:	85 c9                	test   %ecx,%ecx
  800f6d:	74 0e                	je     800f7d <argstart+0x28>
  800f6f:	ba 88 22 80 00       	mov    $0x802288,%edx
  800f74:	eb 0c                	jmp    800f82 <argstart+0x2d>
  800f76:	ba 00 00 00 00       	mov    $0x0,%edx
  800f7b:	eb 05                	jmp    800f82 <argstart+0x2d>
  800f7d:	ba 00 00 00 00       	mov    $0x0,%edx
  800f82:	89 50 08             	mov    %edx,0x8(%eax)
	args->argvalue = 0;
  800f85:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
}
  800f8c:	5d                   	pop    %ebp
  800f8d:	c3                   	ret    

00800f8e <argnext>:

int
argnext(struct Argstate *args)
{
  800f8e:	55                   	push   %ebp
  800f8f:	89 e5                	mov    %esp,%ebp
  800f91:	53                   	push   %ebx
  800f92:	83 ec 04             	sub    $0x4,%esp
  800f95:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int arg;

	args->argvalue = 0;
  800f98:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)

	// Done processing arguments if args->curarg == 0
	if (args->curarg == 0)
  800f9f:	8b 43 08             	mov    0x8(%ebx),%eax
  800fa2:	85 c0                	test   %eax,%eax
  800fa4:	74 6a                	je     801010 <argnext+0x82>
		return -1;

	if (!*args->curarg) {
  800fa6:	80 38 00             	cmpb   $0x0,(%eax)
  800fa9:	75 4b                	jne    800ff6 <argnext+0x68>
		// Need to process the next argument
		// Check for end of argument list
		if (*args->argc == 1
  800fab:	8b 0b                	mov    (%ebx),%ecx
  800fad:	83 39 01             	cmpl   $0x1,(%ecx)
  800fb0:	74 50                	je     801002 <argnext+0x74>
		    || args->argv[1][0] != '-'
  800fb2:	8b 53 04             	mov    0x4(%ebx),%edx
  800fb5:	8b 42 04             	mov    0x4(%edx),%eax
  800fb8:	80 38 2d             	cmpb   $0x2d,(%eax)
  800fbb:	75 45                	jne    801002 <argnext+0x74>
		    || args->argv[1][1] == '\0')
  800fbd:	80 78 01 00          	cmpb   $0x0,0x1(%eax)
  800fc1:	74 3f                	je     801002 <argnext+0x74>
			goto endofargs;
		// Shift arguments down one
		args->curarg = args->argv[1] + 1;
  800fc3:	40                   	inc    %eax
  800fc4:	89 43 08             	mov    %eax,0x8(%ebx)
		memmove(args->argv + 1, args->argv + 2, sizeof(const char *) * (*args->argc - 1));
  800fc7:	83 ec 04             	sub    $0x4,%esp
  800fca:	8b 01                	mov    (%ecx),%eax
  800fcc:	8d 04 85 fc ff ff ff 	lea    -0x4(,%eax,4),%eax
  800fd3:	50                   	push   %eax
  800fd4:	8d 42 08             	lea    0x8(%edx),%eax
  800fd7:	50                   	push   %eax
  800fd8:	83 c2 04             	add    $0x4,%edx
  800fdb:	52                   	push   %edx
  800fdc:	e8 f5 fa ff ff       	call   800ad6 <memmove>
		(*args->argc)--;
  800fe1:	8b 03                	mov    (%ebx),%eax
  800fe3:	ff 08                	decl   (%eax)
		// Check for "--": end of argument list
		if (args->curarg[0] == '-' && args->curarg[1] == '\0')
  800fe5:	8b 43 08             	mov    0x8(%ebx),%eax
  800fe8:	83 c4 10             	add    $0x10,%esp
  800feb:	80 38 2d             	cmpb   $0x2d,(%eax)
  800fee:	75 06                	jne    800ff6 <argnext+0x68>
  800ff0:	80 78 01 00          	cmpb   $0x0,0x1(%eax)
  800ff4:	74 0c                	je     801002 <argnext+0x74>
			goto endofargs;
	}

	arg = (unsigned char) *args->curarg;
  800ff6:	8b 53 08             	mov    0x8(%ebx),%edx
  800ff9:	0f b6 02             	movzbl (%edx),%eax
	args->curarg++;
  800ffc:	42                   	inc    %edx
  800ffd:	89 53 08             	mov    %edx,0x8(%ebx)
	return arg;
  801000:	eb 13                	jmp    801015 <argnext+0x87>

    endofargs:
	args->curarg = 0;
  801002:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
	return -1;
  801009:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  80100e:	eb 05                	jmp    801015 <argnext+0x87>

	args->argvalue = 0;

	// Done processing arguments if args->curarg == 0
	if (args->curarg == 0)
		return -1;
  801010:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return arg;

    endofargs:
	args->curarg = 0;
	return -1;
}
  801015:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801018:	c9                   	leave  
  801019:	c3                   	ret    

0080101a <argnextvalue>:
	return (char*) (args->argvalue ? args->argvalue : argnextvalue(args));
}

char *
argnextvalue(struct Argstate *args)
{
  80101a:	55                   	push   %ebp
  80101b:	89 e5                	mov    %esp,%ebp
  80101d:	53                   	push   %ebx
  80101e:	83 ec 04             	sub    $0x4,%esp
  801021:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (!args->curarg)
  801024:	8b 43 08             	mov    0x8(%ebx),%eax
  801027:	85 c0                	test   %eax,%eax
  801029:	74 57                	je     801082 <argnextvalue+0x68>
		return 0;
	if (*args->curarg) {
  80102b:	80 38 00             	cmpb   $0x0,(%eax)
  80102e:	74 0c                	je     80103c <argnextvalue+0x22>
		args->argvalue = args->curarg;
  801030:	89 43 0c             	mov    %eax,0xc(%ebx)
		args->curarg = "";
  801033:	c7 43 08 88 22 80 00 	movl   $0x802288,0x8(%ebx)
  80103a:	eb 41                	jmp    80107d <argnextvalue+0x63>
	} else if (*args->argc > 1) {
  80103c:	8b 13                	mov    (%ebx),%edx
  80103e:	83 3a 01             	cmpl   $0x1,(%edx)
  801041:	7e 2c                	jle    80106f <argnextvalue+0x55>
		args->argvalue = args->argv[1];
  801043:	8b 43 04             	mov    0x4(%ebx),%eax
  801046:	8b 48 04             	mov    0x4(%eax),%ecx
  801049:	89 4b 0c             	mov    %ecx,0xc(%ebx)
		memmove(args->argv + 1, args->argv + 2, sizeof(const char *) * (*args->argc - 1));
  80104c:	83 ec 04             	sub    $0x4,%esp
  80104f:	8b 12                	mov    (%edx),%edx
  801051:	8d 14 95 fc ff ff ff 	lea    -0x4(,%edx,4),%edx
  801058:	52                   	push   %edx
  801059:	8d 50 08             	lea    0x8(%eax),%edx
  80105c:	52                   	push   %edx
  80105d:	83 c0 04             	add    $0x4,%eax
  801060:	50                   	push   %eax
  801061:	e8 70 fa ff ff       	call   800ad6 <memmove>
		(*args->argc)--;
  801066:	8b 03                	mov    (%ebx),%eax
  801068:	ff 08                	decl   (%eax)
  80106a:	83 c4 10             	add    $0x10,%esp
  80106d:	eb 0e                	jmp    80107d <argnextvalue+0x63>
	} else {
		args->argvalue = 0;
  80106f:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
		args->curarg = 0;
  801076:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
	}
	return (char*) args->argvalue;
  80107d:	8b 43 0c             	mov    0xc(%ebx),%eax
  801080:	eb 05                	jmp    801087 <argnextvalue+0x6d>

char *
argnextvalue(struct Argstate *args)
{
	if (!args->curarg)
		return 0;
  801082:	b8 00 00 00 00       	mov    $0x0,%eax
	} else {
		args->argvalue = 0;
		args->curarg = 0;
	}
	return (char*) args->argvalue;
}
  801087:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80108a:	c9                   	leave  
  80108b:	c3                   	ret    

0080108c <argvalue>:
	return -1;
}

char *
argvalue(struct Argstate *args)
{
  80108c:	55                   	push   %ebp
  80108d:	89 e5                	mov    %esp,%ebp
  80108f:	83 ec 08             	sub    $0x8,%esp
  801092:	8b 4d 08             	mov    0x8(%ebp),%ecx
	return (char*) (args->argvalue ? args->argvalue : argnextvalue(args));
  801095:	8b 51 0c             	mov    0xc(%ecx),%edx
  801098:	89 d0                	mov    %edx,%eax
  80109a:	85 d2                	test   %edx,%edx
  80109c:	75 0c                	jne    8010aa <argvalue+0x1e>
  80109e:	83 ec 0c             	sub    $0xc,%esp
  8010a1:	51                   	push   %ecx
  8010a2:	e8 73 ff ff ff       	call   80101a <argnextvalue>
  8010a7:	83 c4 10             	add    $0x10,%esp
}
  8010aa:	c9                   	leave  
  8010ab:	c3                   	ret    

008010ac <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  8010ac:	55                   	push   %ebp
  8010ad:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  8010af:	8b 45 08             	mov    0x8(%ebp),%eax
  8010b2:	05 00 00 00 30       	add    $0x30000000,%eax
  8010b7:	c1 e8 0c             	shr    $0xc,%eax
}
  8010ba:	5d                   	pop    %ebp
  8010bb:	c3                   	ret    

008010bc <fd2data>:

char*
fd2data(struct Fd *fd)
{
  8010bc:	55                   	push   %ebp
  8010bd:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  8010bf:	8b 45 08             	mov    0x8(%ebp),%eax
  8010c2:	05 00 00 00 30       	add    $0x30000000,%eax
  8010c7:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  8010cc:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  8010d1:	5d                   	pop    %ebp
  8010d2:	c3                   	ret    

008010d3 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  8010d3:	55                   	push   %ebp
  8010d4:	89 e5                	mov    %esp,%ebp
  8010d6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8010d9:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  8010de:	89 c2                	mov    %eax,%edx
  8010e0:	c1 ea 16             	shr    $0x16,%edx
  8010e3:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8010ea:	f6 c2 01             	test   $0x1,%dl
  8010ed:	74 11                	je     801100 <fd_alloc+0x2d>
  8010ef:	89 c2                	mov    %eax,%edx
  8010f1:	c1 ea 0c             	shr    $0xc,%edx
  8010f4:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8010fb:	f6 c2 01             	test   $0x1,%dl
  8010fe:	75 09                	jne    801109 <fd_alloc+0x36>
			*fd_store = fd;
  801100:	89 01                	mov    %eax,(%ecx)
			return 0;
  801102:	b8 00 00 00 00       	mov    $0x0,%eax
  801107:	eb 17                	jmp    801120 <fd_alloc+0x4d>
  801109:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  80110e:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801113:	75 c9                	jne    8010de <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  801115:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  80111b:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801120:	5d                   	pop    %ebp
  801121:	c3                   	ret    

00801122 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  801122:	55                   	push   %ebp
  801123:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  801125:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801129:	77 39                	ja     801164 <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  80112b:	8b 45 08             	mov    0x8(%ebp),%eax
  80112e:	c1 e0 0c             	shl    $0xc,%eax
  801131:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  801136:	89 c2                	mov    %eax,%edx
  801138:	c1 ea 16             	shr    $0x16,%edx
  80113b:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801142:	f6 c2 01             	test   $0x1,%dl
  801145:	74 24                	je     80116b <fd_lookup+0x49>
  801147:	89 c2                	mov    %eax,%edx
  801149:	c1 ea 0c             	shr    $0xc,%edx
  80114c:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801153:	f6 c2 01             	test   $0x1,%dl
  801156:	74 1a                	je     801172 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  801158:	8b 55 0c             	mov    0xc(%ebp),%edx
  80115b:	89 02                	mov    %eax,(%edx)
	return 0;
  80115d:	b8 00 00 00 00       	mov    $0x0,%eax
  801162:	eb 13                	jmp    801177 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801164:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801169:	eb 0c                	jmp    801177 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  80116b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801170:	eb 05                	jmp    801177 <fd_lookup+0x55>
  801172:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  801177:	5d                   	pop    %ebp
  801178:	c3                   	ret    

00801179 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  801179:	55                   	push   %ebp
  80117a:	89 e5                	mov    %esp,%ebp
  80117c:	83 ec 08             	sub    $0x8,%esp
  80117f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801182:	ba 8c 26 80 00       	mov    $0x80268c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801187:	eb 13                	jmp    80119c <dev_lookup+0x23>
  801189:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  80118c:	39 08                	cmp    %ecx,(%eax)
  80118e:	75 0c                	jne    80119c <dev_lookup+0x23>
			*dev = devtab[i];
  801190:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801193:	89 01                	mov    %eax,(%ecx)
			return 0;
  801195:	b8 00 00 00 00       	mov    $0x0,%eax
  80119a:	eb 2e                	jmp    8011ca <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  80119c:	8b 02                	mov    (%edx),%eax
  80119e:	85 c0                	test   %eax,%eax
  8011a0:	75 e7                	jne    801189 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  8011a2:	a1 20 44 80 00       	mov    0x804420,%eax
  8011a7:	8b 40 48             	mov    0x48(%eax),%eax
  8011aa:	83 ec 04             	sub    $0x4,%esp
  8011ad:	51                   	push   %ecx
  8011ae:	50                   	push   %eax
  8011af:	68 0c 26 80 00       	push   $0x80260c
  8011b4:	e8 44 f2 ff ff       	call   8003fd <cprintf>
	*dev = 0;
  8011b9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8011bc:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  8011c2:	83 c4 10             	add    $0x10,%esp
  8011c5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  8011ca:	c9                   	leave  
  8011cb:	c3                   	ret    

008011cc <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  8011cc:	55                   	push   %ebp
  8011cd:	89 e5                	mov    %esp,%ebp
  8011cf:	56                   	push   %esi
  8011d0:	53                   	push   %ebx
  8011d1:	83 ec 10             	sub    $0x10,%esp
  8011d4:	8b 75 08             	mov    0x8(%ebp),%esi
  8011d7:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  8011da:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8011dd:	50                   	push   %eax
  8011de:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  8011e4:	c1 e8 0c             	shr    $0xc,%eax
  8011e7:	50                   	push   %eax
  8011e8:	e8 35 ff ff ff       	call   801122 <fd_lookup>
  8011ed:	83 c4 08             	add    $0x8,%esp
  8011f0:	85 c0                	test   %eax,%eax
  8011f2:	78 05                	js     8011f9 <fd_close+0x2d>
	    || fd != fd2)
  8011f4:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  8011f7:	74 06                	je     8011ff <fd_close+0x33>
		return (must_exist ? r : 0);
  8011f9:	84 db                	test   %bl,%bl
  8011fb:	74 47                	je     801244 <fd_close+0x78>
  8011fd:	eb 4a                	jmp    801249 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  8011ff:	83 ec 08             	sub    $0x8,%esp
  801202:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801205:	50                   	push   %eax
  801206:	ff 36                	pushl  (%esi)
  801208:	e8 6c ff ff ff       	call   801179 <dev_lookup>
  80120d:	89 c3                	mov    %eax,%ebx
  80120f:	83 c4 10             	add    $0x10,%esp
  801212:	85 c0                	test   %eax,%eax
  801214:	78 1c                	js     801232 <fd_close+0x66>
		if (dev->dev_close)
  801216:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801219:	8b 40 10             	mov    0x10(%eax),%eax
  80121c:	85 c0                	test   %eax,%eax
  80121e:	74 0d                	je     80122d <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801220:	83 ec 0c             	sub    $0xc,%esp
  801223:	56                   	push   %esi
  801224:	ff d0                	call   *%eax
  801226:	89 c3                	mov    %eax,%ebx
  801228:	83 c4 10             	add    $0x10,%esp
  80122b:	eb 05                	jmp    801232 <fd_close+0x66>
		else
			r = 0;
  80122d:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  801232:	83 ec 08             	sub    $0x8,%esp
  801235:	56                   	push   %esi
  801236:	6a 00                	push   $0x0
  801238:	e8 8d fb ff ff       	call   800dca <sys_page_unmap>
	return r;
  80123d:	83 c4 10             	add    $0x10,%esp
  801240:	89 d8                	mov    %ebx,%eax
  801242:	eb 05                	jmp    801249 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  801244:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801249:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80124c:	5b                   	pop    %ebx
  80124d:	5e                   	pop    %esi
  80124e:	5d                   	pop    %ebp
  80124f:	c3                   	ret    

00801250 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  801250:	55                   	push   %ebp
  801251:	89 e5                	mov    %esp,%ebp
  801253:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801256:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801259:	50                   	push   %eax
  80125a:	ff 75 08             	pushl  0x8(%ebp)
  80125d:	e8 c0 fe ff ff       	call   801122 <fd_lookup>
  801262:	83 c4 08             	add    $0x8,%esp
  801265:	85 c0                	test   %eax,%eax
  801267:	78 10                	js     801279 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  801269:	83 ec 08             	sub    $0x8,%esp
  80126c:	6a 01                	push   $0x1
  80126e:	ff 75 f4             	pushl  -0xc(%ebp)
  801271:	e8 56 ff ff ff       	call   8011cc <fd_close>
  801276:	83 c4 10             	add    $0x10,%esp
}
  801279:	c9                   	leave  
  80127a:	c3                   	ret    

0080127b <close_all>:

void
close_all(void)
{
  80127b:	55                   	push   %ebp
  80127c:	89 e5                	mov    %esp,%ebp
  80127e:	53                   	push   %ebx
  80127f:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  801282:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801287:	83 ec 0c             	sub    $0xc,%esp
  80128a:	53                   	push   %ebx
  80128b:	e8 c0 ff ff ff       	call   801250 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801290:	43                   	inc    %ebx
  801291:	83 c4 10             	add    $0x10,%esp
  801294:	83 fb 20             	cmp    $0x20,%ebx
  801297:	75 ee                	jne    801287 <close_all+0xc>
		close(i);
}
  801299:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80129c:	c9                   	leave  
  80129d:	c3                   	ret    

0080129e <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  80129e:	55                   	push   %ebp
  80129f:	89 e5                	mov    %esp,%ebp
  8012a1:	57                   	push   %edi
  8012a2:	56                   	push   %esi
  8012a3:	53                   	push   %ebx
  8012a4:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  8012a7:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8012aa:	50                   	push   %eax
  8012ab:	ff 75 08             	pushl  0x8(%ebp)
  8012ae:	e8 6f fe ff ff       	call   801122 <fd_lookup>
  8012b3:	83 c4 08             	add    $0x8,%esp
  8012b6:	85 c0                	test   %eax,%eax
  8012b8:	0f 88 c2 00 00 00    	js     801380 <dup+0xe2>
		return r;
	close(newfdnum);
  8012be:	83 ec 0c             	sub    $0xc,%esp
  8012c1:	ff 75 0c             	pushl  0xc(%ebp)
  8012c4:	e8 87 ff ff ff       	call   801250 <close>

	newfd = INDEX2FD(newfdnum);
  8012c9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8012cc:	c1 e3 0c             	shl    $0xc,%ebx
  8012cf:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  8012d5:	83 c4 04             	add    $0x4,%esp
  8012d8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8012db:	e8 dc fd ff ff       	call   8010bc <fd2data>
  8012e0:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  8012e2:	89 1c 24             	mov    %ebx,(%esp)
  8012e5:	e8 d2 fd ff ff       	call   8010bc <fd2data>
  8012ea:	83 c4 10             	add    $0x10,%esp
  8012ed:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8012ef:	89 f0                	mov    %esi,%eax
  8012f1:	c1 e8 16             	shr    $0x16,%eax
  8012f4:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8012fb:	a8 01                	test   $0x1,%al
  8012fd:	74 35                	je     801334 <dup+0x96>
  8012ff:	89 f0                	mov    %esi,%eax
  801301:	c1 e8 0c             	shr    $0xc,%eax
  801304:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  80130b:	f6 c2 01             	test   $0x1,%dl
  80130e:	74 24                	je     801334 <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801310:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801317:	83 ec 0c             	sub    $0xc,%esp
  80131a:	25 07 0e 00 00       	and    $0xe07,%eax
  80131f:	50                   	push   %eax
  801320:	57                   	push   %edi
  801321:	6a 00                	push   $0x0
  801323:	56                   	push   %esi
  801324:	6a 00                	push   $0x0
  801326:	e8 5d fa ff ff       	call   800d88 <sys_page_map>
  80132b:	89 c6                	mov    %eax,%esi
  80132d:	83 c4 20             	add    $0x20,%esp
  801330:	85 c0                	test   %eax,%eax
  801332:	78 2c                	js     801360 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  801334:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801337:	89 d0                	mov    %edx,%eax
  801339:	c1 e8 0c             	shr    $0xc,%eax
  80133c:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801343:	83 ec 0c             	sub    $0xc,%esp
  801346:	25 07 0e 00 00       	and    $0xe07,%eax
  80134b:	50                   	push   %eax
  80134c:	53                   	push   %ebx
  80134d:	6a 00                	push   $0x0
  80134f:	52                   	push   %edx
  801350:	6a 00                	push   $0x0
  801352:	e8 31 fa ff ff       	call   800d88 <sys_page_map>
  801357:	89 c6                	mov    %eax,%esi
  801359:	83 c4 20             	add    $0x20,%esp
  80135c:	85 c0                	test   %eax,%eax
  80135e:	79 1d                	jns    80137d <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801360:	83 ec 08             	sub    $0x8,%esp
  801363:	53                   	push   %ebx
  801364:	6a 00                	push   $0x0
  801366:	e8 5f fa ff ff       	call   800dca <sys_page_unmap>
	sys_page_unmap(0, nva);
  80136b:	83 c4 08             	add    $0x8,%esp
  80136e:	57                   	push   %edi
  80136f:	6a 00                	push   $0x0
  801371:	e8 54 fa ff ff       	call   800dca <sys_page_unmap>
	return r;
  801376:	83 c4 10             	add    $0x10,%esp
  801379:	89 f0                	mov    %esi,%eax
  80137b:	eb 03                	jmp    801380 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  80137d:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801380:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801383:	5b                   	pop    %ebx
  801384:	5e                   	pop    %esi
  801385:	5f                   	pop    %edi
  801386:	5d                   	pop    %ebp
  801387:	c3                   	ret    

00801388 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801388:	55                   	push   %ebp
  801389:	89 e5                	mov    %esp,%ebp
  80138b:	53                   	push   %ebx
  80138c:	83 ec 14             	sub    $0x14,%esp
  80138f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801392:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801395:	50                   	push   %eax
  801396:	53                   	push   %ebx
  801397:	e8 86 fd ff ff       	call   801122 <fd_lookup>
  80139c:	83 c4 08             	add    $0x8,%esp
  80139f:	85 c0                	test   %eax,%eax
  8013a1:	78 67                	js     80140a <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8013a3:	83 ec 08             	sub    $0x8,%esp
  8013a6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013a9:	50                   	push   %eax
  8013aa:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8013ad:	ff 30                	pushl  (%eax)
  8013af:	e8 c5 fd ff ff       	call   801179 <dev_lookup>
  8013b4:	83 c4 10             	add    $0x10,%esp
  8013b7:	85 c0                	test   %eax,%eax
  8013b9:	78 4f                	js     80140a <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8013bb:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8013be:	8b 42 08             	mov    0x8(%edx),%eax
  8013c1:	83 e0 03             	and    $0x3,%eax
  8013c4:	83 f8 01             	cmp    $0x1,%eax
  8013c7:	75 21                	jne    8013ea <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  8013c9:	a1 20 44 80 00       	mov    0x804420,%eax
  8013ce:	8b 40 48             	mov    0x48(%eax),%eax
  8013d1:	83 ec 04             	sub    $0x4,%esp
  8013d4:	53                   	push   %ebx
  8013d5:	50                   	push   %eax
  8013d6:	68 50 26 80 00       	push   $0x802650
  8013db:	e8 1d f0 ff ff       	call   8003fd <cprintf>
		return -E_INVAL;
  8013e0:	83 c4 10             	add    $0x10,%esp
  8013e3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8013e8:	eb 20                	jmp    80140a <read+0x82>
	}
	if (!dev->dev_read)
  8013ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8013ed:	8b 40 08             	mov    0x8(%eax),%eax
  8013f0:	85 c0                	test   %eax,%eax
  8013f2:	74 11                	je     801405 <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  8013f4:	83 ec 04             	sub    $0x4,%esp
  8013f7:	ff 75 10             	pushl  0x10(%ebp)
  8013fa:	ff 75 0c             	pushl  0xc(%ebp)
  8013fd:	52                   	push   %edx
  8013fe:	ff d0                	call   *%eax
  801400:	83 c4 10             	add    $0x10,%esp
  801403:	eb 05                	jmp    80140a <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  801405:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  80140a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80140d:	c9                   	leave  
  80140e:	c3                   	ret    

0080140f <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  80140f:	55                   	push   %ebp
  801410:	89 e5                	mov    %esp,%ebp
  801412:	57                   	push   %edi
  801413:	56                   	push   %esi
  801414:	53                   	push   %ebx
  801415:	83 ec 0c             	sub    $0xc,%esp
  801418:	8b 7d 08             	mov    0x8(%ebp),%edi
  80141b:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80141e:	bb 00 00 00 00       	mov    $0x0,%ebx
  801423:	eb 21                	jmp    801446 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  801425:	83 ec 04             	sub    $0x4,%esp
  801428:	89 f0                	mov    %esi,%eax
  80142a:	29 d8                	sub    %ebx,%eax
  80142c:	50                   	push   %eax
  80142d:	89 d8                	mov    %ebx,%eax
  80142f:	03 45 0c             	add    0xc(%ebp),%eax
  801432:	50                   	push   %eax
  801433:	57                   	push   %edi
  801434:	e8 4f ff ff ff       	call   801388 <read>
		if (m < 0)
  801439:	83 c4 10             	add    $0x10,%esp
  80143c:	85 c0                	test   %eax,%eax
  80143e:	78 10                	js     801450 <readn+0x41>
			return m;
		if (m == 0)
  801440:	85 c0                	test   %eax,%eax
  801442:	74 0a                	je     80144e <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801444:	01 c3                	add    %eax,%ebx
  801446:	39 f3                	cmp    %esi,%ebx
  801448:	72 db                	jb     801425 <readn+0x16>
  80144a:	89 d8                	mov    %ebx,%eax
  80144c:	eb 02                	jmp    801450 <readn+0x41>
  80144e:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801450:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801453:	5b                   	pop    %ebx
  801454:	5e                   	pop    %esi
  801455:	5f                   	pop    %edi
  801456:	5d                   	pop    %ebp
  801457:	c3                   	ret    

00801458 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801458:	55                   	push   %ebp
  801459:	89 e5                	mov    %esp,%ebp
  80145b:	53                   	push   %ebx
  80145c:	83 ec 14             	sub    $0x14,%esp
  80145f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801462:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801465:	50                   	push   %eax
  801466:	53                   	push   %ebx
  801467:	e8 b6 fc ff ff       	call   801122 <fd_lookup>
  80146c:	83 c4 08             	add    $0x8,%esp
  80146f:	85 c0                	test   %eax,%eax
  801471:	78 62                	js     8014d5 <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801473:	83 ec 08             	sub    $0x8,%esp
  801476:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801479:	50                   	push   %eax
  80147a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80147d:	ff 30                	pushl  (%eax)
  80147f:	e8 f5 fc ff ff       	call   801179 <dev_lookup>
  801484:	83 c4 10             	add    $0x10,%esp
  801487:	85 c0                	test   %eax,%eax
  801489:	78 4a                	js     8014d5 <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80148b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80148e:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801492:	75 21                	jne    8014b5 <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  801494:	a1 20 44 80 00       	mov    0x804420,%eax
  801499:	8b 40 48             	mov    0x48(%eax),%eax
  80149c:	83 ec 04             	sub    $0x4,%esp
  80149f:	53                   	push   %ebx
  8014a0:	50                   	push   %eax
  8014a1:	68 6c 26 80 00       	push   $0x80266c
  8014a6:	e8 52 ef ff ff       	call   8003fd <cprintf>
		return -E_INVAL;
  8014ab:	83 c4 10             	add    $0x10,%esp
  8014ae:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8014b3:	eb 20                	jmp    8014d5 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8014b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8014b8:	8b 52 0c             	mov    0xc(%edx),%edx
  8014bb:	85 d2                	test   %edx,%edx
  8014bd:	74 11                	je     8014d0 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8014bf:	83 ec 04             	sub    $0x4,%esp
  8014c2:	ff 75 10             	pushl  0x10(%ebp)
  8014c5:	ff 75 0c             	pushl  0xc(%ebp)
  8014c8:	50                   	push   %eax
  8014c9:	ff d2                	call   *%edx
  8014cb:	83 c4 10             	add    $0x10,%esp
  8014ce:	eb 05                	jmp    8014d5 <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8014d0:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8014d5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8014d8:	c9                   	leave  
  8014d9:	c3                   	ret    

008014da <seek>:

int
seek(int fdnum, off_t offset)
{
  8014da:	55                   	push   %ebp
  8014db:	89 e5                	mov    %esp,%ebp
  8014dd:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8014e0:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8014e3:	50                   	push   %eax
  8014e4:	ff 75 08             	pushl  0x8(%ebp)
  8014e7:	e8 36 fc ff ff       	call   801122 <fd_lookup>
  8014ec:	83 c4 08             	add    $0x8,%esp
  8014ef:	85 c0                	test   %eax,%eax
  8014f1:	78 0e                	js     801501 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  8014f3:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8014f6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8014f9:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  8014fc:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801501:	c9                   	leave  
  801502:	c3                   	ret    

00801503 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801503:	55                   	push   %ebp
  801504:	89 e5                	mov    %esp,%ebp
  801506:	53                   	push   %ebx
  801507:	83 ec 14             	sub    $0x14,%esp
  80150a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  80150d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801510:	50                   	push   %eax
  801511:	53                   	push   %ebx
  801512:	e8 0b fc ff ff       	call   801122 <fd_lookup>
  801517:	83 c4 08             	add    $0x8,%esp
  80151a:	85 c0                	test   %eax,%eax
  80151c:	78 5f                	js     80157d <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80151e:	83 ec 08             	sub    $0x8,%esp
  801521:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801524:	50                   	push   %eax
  801525:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801528:	ff 30                	pushl  (%eax)
  80152a:	e8 4a fc ff ff       	call   801179 <dev_lookup>
  80152f:	83 c4 10             	add    $0x10,%esp
  801532:	85 c0                	test   %eax,%eax
  801534:	78 47                	js     80157d <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801536:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801539:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80153d:	75 21                	jne    801560 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  80153f:	a1 20 44 80 00       	mov    0x804420,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  801544:	8b 40 48             	mov    0x48(%eax),%eax
  801547:	83 ec 04             	sub    $0x4,%esp
  80154a:	53                   	push   %ebx
  80154b:	50                   	push   %eax
  80154c:	68 2c 26 80 00       	push   $0x80262c
  801551:	e8 a7 ee ff ff       	call   8003fd <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  801556:	83 c4 10             	add    $0x10,%esp
  801559:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80155e:	eb 1d                	jmp    80157d <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  801560:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801563:	8b 52 18             	mov    0x18(%edx),%edx
  801566:	85 d2                	test   %edx,%edx
  801568:	74 0e                	je     801578 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  80156a:	83 ec 08             	sub    $0x8,%esp
  80156d:	ff 75 0c             	pushl  0xc(%ebp)
  801570:	50                   	push   %eax
  801571:	ff d2                	call   *%edx
  801573:	83 c4 10             	add    $0x10,%esp
  801576:	eb 05                	jmp    80157d <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  801578:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  80157d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801580:	c9                   	leave  
  801581:	c3                   	ret    

00801582 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  801582:	55                   	push   %ebp
  801583:	89 e5                	mov    %esp,%ebp
  801585:	53                   	push   %ebx
  801586:	83 ec 14             	sub    $0x14,%esp
  801589:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80158c:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80158f:	50                   	push   %eax
  801590:	ff 75 08             	pushl  0x8(%ebp)
  801593:	e8 8a fb ff ff       	call   801122 <fd_lookup>
  801598:	83 c4 08             	add    $0x8,%esp
  80159b:	85 c0                	test   %eax,%eax
  80159d:	78 52                	js     8015f1 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80159f:	83 ec 08             	sub    $0x8,%esp
  8015a2:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8015a5:	50                   	push   %eax
  8015a6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015a9:	ff 30                	pushl  (%eax)
  8015ab:	e8 c9 fb ff ff       	call   801179 <dev_lookup>
  8015b0:	83 c4 10             	add    $0x10,%esp
  8015b3:	85 c0                	test   %eax,%eax
  8015b5:	78 3a                	js     8015f1 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8015b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8015ba:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8015be:	74 2c                	je     8015ec <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8015c0:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8015c3:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8015ca:	00 00 00 
	stat->st_isdir = 0;
  8015cd:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8015d4:	00 00 00 
	stat->st_dev = dev;
  8015d7:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8015dd:	83 ec 08             	sub    $0x8,%esp
  8015e0:	53                   	push   %ebx
  8015e1:	ff 75 f0             	pushl  -0x10(%ebp)
  8015e4:	ff 50 14             	call   *0x14(%eax)
  8015e7:	83 c4 10             	add    $0x10,%esp
  8015ea:	eb 05                	jmp    8015f1 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8015ec:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8015f1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015f4:	c9                   	leave  
  8015f5:	c3                   	ret    

008015f6 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8015f6:	55                   	push   %ebp
  8015f7:	89 e5                	mov    %esp,%ebp
  8015f9:	56                   	push   %esi
  8015fa:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8015fb:	83 ec 08             	sub    $0x8,%esp
  8015fe:	6a 00                	push   $0x0
  801600:	ff 75 08             	pushl  0x8(%ebp)
  801603:	e8 e7 01 00 00       	call   8017ef <open>
  801608:	89 c3                	mov    %eax,%ebx
  80160a:	83 c4 10             	add    $0x10,%esp
  80160d:	85 c0                	test   %eax,%eax
  80160f:	78 1d                	js     80162e <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801611:	83 ec 08             	sub    $0x8,%esp
  801614:	ff 75 0c             	pushl  0xc(%ebp)
  801617:	50                   	push   %eax
  801618:	e8 65 ff ff ff       	call   801582 <fstat>
  80161d:	89 c6                	mov    %eax,%esi
	close(fd);
  80161f:	89 1c 24             	mov    %ebx,(%esp)
  801622:	e8 29 fc ff ff       	call   801250 <close>
	return r;
  801627:	83 c4 10             	add    $0x10,%esp
  80162a:	89 f0                	mov    %esi,%eax
  80162c:	eb 00                	jmp    80162e <stat+0x38>
}
  80162e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801631:	5b                   	pop    %ebx
  801632:	5e                   	pop    %esi
  801633:	5d                   	pop    %ebp
  801634:	c3                   	ret    

00801635 <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  801635:	55                   	push   %ebp
  801636:	89 e5                	mov    %esp,%ebp
  801638:	56                   	push   %esi
  801639:	53                   	push   %ebx
  80163a:	89 c6                	mov    %eax,%esi
  80163c:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  80163e:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  801645:	75 12                	jne    801659 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801647:	83 ec 0c             	sub    $0xc,%esp
  80164a:	6a 01                	push   $0x1
  80164c:	e8 c9 08 00 00       	call   801f1a <ipc_find_env>
  801651:	a3 00 40 80 00       	mov    %eax,0x804000
  801656:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  801659:	6a 07                	push   $0x7
  80165b:	68 00 50 80 00       	push   $0x805000
  801660:	56                   	push   %esi
  801661:	ff 35 00 40 80 00    	pushl  0x804000
  801667:	e8 59 08 00 00       	call   801ec5 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  80166c:	83 c4 0c             	add    $0xc,%esp
  80166f:	6a 00                	push   $0x0
  801671:	53                   	push   %ebx
  801672:	6a 00                	push   $0x0
  801674:	e8 e4 07 00 00       	call   801e5d <ipc_recv>
}
  801679:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80167c:	5b                   	pop    %ebx
  80167d:	5e                   	pop    %esi
  80167e:	5d                   	pop    %ebp
  80167f:	c3                   	ret    

00801680 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801680:	55                   	push   %ebp
  801681:	89 e5                	mov    %esp,%ebp
  801683:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801686:	8b 45 08             	mov    0x8(%ebp),%eax
  801689:	8b 40 0c             	mov    0xc(%eax),%eax
  80168c:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  801691:	8b 45 0c             	mov    0xc(%ebp),%eax
  801694:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801699:	ba 00 00 00 00       	mov    $0x0,%edx
  80169e:	b8 02 00 00 00       	mov    $0x2,%eax
  8016a3:	e8 8d ff ff ff       	call   801635 <fsipc>
}
  8016a8:	c9                   	leave  
  8016a9:	c3                   	ret    

008016aa <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8016aa:	55                   	push   %ebp
  8016ab:	89 e5                	mov    %esp,%ebp
  8016ad:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8016b0:	8b 45 08             	mov    0x8(%ebp),%eax
  8016b3:	8b 40 0c             	mov    0xc(%eax),%eax
  8016b6:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  8016bb:	ba 00 00 00 00       	mov    $0x0,%edx
  8016c0:	b8 06 00 00 00       	mov    $0x6,%eax
  8016c5:	e8 6b ff ff ff       	call   801635 <fsipc>
}
  8016ca:	c9                   	leave  
  8016cb:	c3                   	ret    

008016cc <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  8016cc:	55                   	push   %ebp
  8016cd:	89 e5                	mov    %esp,%ebp
  8016cf:	53                   	push   %ebx
  8016d0:	83 ec 04             	sub    $0x4,%esp
  8016d3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8016d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8016d9:	8b 40 0c             	mov    0xc(%eax),%eax
  8016dc:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8016e1:	ba 00 00 00 00       	mov    $0x0,%edx
  8016e6:	b8 05 00 00 00       	mov    $0x5,%eax
  8016eb:	e8 45 ff ff ff       	call   801635 <fsipc>
  8016f0:	85 c0                	test   %eax,%eax
  8016f2:	78 2c                	js     801720 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  8016f4:	83 ec 08             	sub    $0x8,%esp
  8016f7:	68 00 50 80 00       	push   $0x805000
  8016fc:	53                   	push   %ebx
  8016fd:	e8 5f f2 ff ff       	call   800961 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801702:	a1 80 50 80 00       	mov    0x805080,%eax
  801707:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  80170d:	a1 84 50 80 00       	mov    0x805084,%eax
  801712:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  801718:	83 c4 10             	add    $0x10,%esp
  80171b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801720:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801723:	c9                   	leave  
  801724:	c3                   	ret    

00801725 <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  801725:	55                   	push   %ebp
  801726:	89 e5                	mov    %esp,%ebp
  801728:	83 ec 08             	sub    $0x8,%esp
  80172b:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  80172e:	8b 55 08             	mov    0x8(%ebp),%edx
  801731:	8b 52 0c             	mov    0xc(%edx),%edx
  801734:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  80173a:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  80173f:	76 05                	jbe    801746 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801741:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801746:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  80174b:	83 ec 04             	sub    $0x4,%esp
  80174e:	50                   	push   %eax
  80174f:	ff 75 0c             	pushl  0xc(%ebp)
  801752:	68 08 50 80 00       	push   $0x805008
  801757:	e8 7a f3 ff ff       	call   800ad6 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  80175c:	ba 00 00 00 00       	mov    $0x0,%edx
  801761:	b8 04 00 00 00       	mov    $0x4,%eax
  801766:	e8 ca fe ff ff       	call   801635 <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  80176b:	c9                   	leave  
  80176c:	c3                   	ret    

0080176d <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  80176d:	55                   	push   %ebp
  80176e:	89 e5                	mov    %esp,%ebp
  801770:	56                   	push   %esi
  801771:	53                   	push   %ebx
  801772:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  801775:	8b 45 08             	mov    0x8(%ebp),%eax
  801778:	8b 40 0c             	mov    0xc(%eax),%eax
  80177b:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801780:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801786:	ba 00 00 00 00       	mov    $0x0,%edx
  80178b:	b8 03 00 00 00       	mov    $0x3,%eax
  801790:	e8 a0 fe ff ff       	call   801635 <fsipc>
  801795:	89 c3                	mov    %eax,%ebx
  801797:	85 c0                	test   %eax,%eax
  801799:	78 4b                	js     8017e6 <devfile_read+0x79>
		return r;
	assert(r <= n);
  80179b:	39 c6                	cmp    %eax,%esi
  80179d:	73 16                	jae    8017b5 <devfile_read+0x48>
  80179f:	68 9c 26 80 00       	push   $0x80269c
  8017a4:	68 a3 26 80 00       	push   $0x8026a3
  8017a9:	6a 7c                	push   $0x7c
  8017ab:	68 b8 26 80 00       	push   $0x8026b8
  8017b0:	e8 70 eb ff ff       	call   800325 <_panic>
	assert(r <= PGSIZE);
  8017b5:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8017ba:	7e 16                	jle    8017d2 <devfile_read+0x65>
  8017bc:	68 c3 26 80 00       	push   $0x8026c3
  8017c1:	68 a3 26 80 00       	push   $0x8026a3
  8017c6:	6a 7d                	push   $0x7d
  8017c8:	68 b8 26 80 00       	push   $0x8026b8
  8017cd:	e8 53 eb ff ff       	call   800325 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  8017d2:	83 ec 04             	sub    $0x4,%esp
  8017d5:	50                   	push   %eax
  8017d6:	68 00 50 80 00       	push   $0x805000
  8017db:	ff 75 0c             	pushl  0xc(%ebp)
  8017de:	e8 f3 f2 ff ff       	call   800ad6 <memmove>
	return r;
  8017e3:	83 c4 10             	add    $0x10,%esp
}
  8017e6:	89 d8                	mov    %ebx,%eax
  8017e8:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8017eb:	5b                   	pop    %ebx
  8017ec:	5e                   	pop    %esi
  8017ed:	5d                   	pop    %ebp
  8017ee:	c3                   	ret    

008017ef <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  8017ef:	55                   	push   %ebp
  8017f0:	89 e5                	mov    %esp,%ebp
  8017f2:	53                   	push   %ebx
  8017f3:	83 ec 20             	sub    $0x20,%esp
  8017f6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  8017f9:	53                   	push   %ebx
  8017fa:	e8 2d f1 ff ff       	call   80092c <strlen>
  8017ff:	83 c4 10             	add    $0x10,%esp
  801802:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801807:	7f 63                	jg     80186c <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801809:	83 ec 0c             	sub    $0xc,%esp
  80180c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80180f:	50                   	push   %eax
  801810:	e8 be f8 ff ff       	call   8010d3 <fd_alloc>
  801815:	83 c4 10             	add    $0x10,%esp
  801818:	85 c0                	test   %eax,%eax
  80181a:	78 55                	js     801871 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  80181c:	83 ec 08             	sub    $0x8,%esp
  80181f:	53                   	push   %ebx
  801820:	68 00 50 80 00       	push   $0x805000
  801825:	e8 37 f1 ff ff       	call   800961 <strcpy>
	fsipcbuf.open.req_omode = mode;
  80182a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80182d:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801832:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801835:	b8 01 00 00 00       	mov    $0x1,%eax
  80183a:	e8 f6 fd ff ff       	call   801635 <fsipc>
  80183f:	89 c3                	mov    %eax,%ebx
  801841:	83 c4 10             	add    $0x10,%esp
  801844:	85 c0                	test   %eax,%eax
  801846:	79 14                	jns    80185c <open+0x6d>
		fd_close(fd, 0);
  801848:	83 ec 08             	sub    $0x8,%esp
  80184b:	6a 00                	push   $0x0
  80184d:	ff 75 f4             	pushl  -0xc(%ebp)
  801850:	e8 77 f9 ff ff       	call   8011cc <fd_close>
		return r;
  801855:	83 c4 10             	add    $0x10,%esp
  801858:	89 d8                	mov    %ebx,%eax
  80185a:	eb 15                	jmp    801871 <open+0x82>
	}

	return fd2num(fd);
  80185c:	83 ec 0c             	sub    $0xc,%esp
  80185f:	ff 75 f4             	pushl  -0xc(%ebp)
  801862:	e8 45 f8 ff ff       	call   8010ac <fd2num>
  801867:	83 c4 10             	add    $0x10,%esp
  80186a:	eb 05                	jmp    801871 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  80186c:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801871:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801874:	c9                   	leave  
  801875:	c3                   	ret    

00801876 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801876:	55                   	push   %ebp
  801877:	89 e5                	mov    %esp,%ebp
  801879:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  80187c:	ba 00 00 00 00       	mov    $0x0,%edx
  801881:	b8 08 00 00 00       	mov    $0x8,%eax
  801886:	e8 aa fd ff ff       	call   801635 <fsipc>
}
  80188b:	c9                   	leave  
  80188c:	c3                   	ret    

0080188d <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  80188d:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  801891:	7e 38                	jle    8018cb <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  801893:	55                   	push   %ebp
  801894:	89 e5                	mov    %esp,%ebp
  801896:	53                   	push   %ebx
  801897:	83 ec 08             	sub    $0x8,%esp
  80189a:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  80189c:	ff 70 04             	pushl  0x4(%eax)
  80189f:	8d 40 10             	lea    0x10(%eax),%eax
  8018a2:	50                   	push   %eax
  8018a3:	ff 33                	pushl  (%ebx)
  8018a5:	e8 ae fb ff ff       	call   801458 <write>
		if (result > 0)
  8018aa:	83 c4 10             	add    $0x10,%esp
  8018ad:	85 c0                	test   %eax,%eax
  8018af:	7e 03                	jle    8018b4 <writebuf+0x27>
			b->result += result;
  8018b1:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  8018b4:	3b 43 04             	cmp    0x4(%ebx),%eax
  8018b7:	74 0e                	je     8018c7 <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  8018b9:	89 c2                	mov    %eax,%edx
  8018bb:	85 c0                	test   %eax,%eax
  8018bd:	7e 05                	jle    8018c4 <writebuf+0x37>
  8018bf:	ba 00 00 00 00       	mov    $0x0,%edx
  8018c4:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  8018c7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8018ca:	c9                   	leave  
  8018cb:	c3                   	ret    

008018cc <putch>:

static void
putch(int ch, void *thunk)
{
  8018cc:	55                   	push   %ebp
  8018cd:	89 e5                	mov    %esp,%ebp
  8018cf:	53                   	push   %ebx
  8018d0:	83 ec 04             	sub    $0x4,%esp
  8018d3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  8018d6:	8b 53 04             	mov    0x4(%ebx),%edx
  8018d9:	8d 42 01             	lea    0x1(%edx),%eax
  8018dc:	89 43 04             	mov    %eax,0x4(%ebx)
  8018df:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8018e2:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  8018e6:	3d 00 01 00 00       	cmp    $0x100,%eax
  8018eb:	75 0e                	jne    8018fb <putch+0x2f>
		writebuf(b);
  8018ed:	89 d8                	mov    %ebx,%eax
  8018ef:	e8 99 ff ff ff       	call   80188d <writebuf>
		b->idx = 0;
  8018f4:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  8018fb:	83 c4 04             	add    $0x4,%esp
  8018fe:	5b                   	pop    %ebx
  8018ff:	5d                   	pop    %ebp
  801900:	c3                   	ret    

00801901 <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  801901:	55                   	push   %ebp
  801902:	89 e5                	mov    %esp,%ebp
  801904:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  80190a:	8b 45 08             	mov    0x8(%ebp),%eax
  80190d:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  801913:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  80191a:	00 00 00 
	b.result = 0;
  80191d:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  801924:	00 00 00 
	b.error = 1;
  801927:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  80192e:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  801931:	ff 75 10             	pushl  0x10(%ebp)
  801934:	ff 75 0c             	pushl  0xc(%ebp)
  801937:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  80193d:	50                   	push   %eax
  80193e:	68 cc 18 80 00       	push   $0x8018cc
  801943:	e8 e9 eb ff ff       	call   800531 <vprintfmt>
	if (b.idx > 0)
  801948:	83 c4 10             	add    $0x10,%esp
  80194b:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  801952:	7e 0b                	jle    80195f <vfprintf+0x5e>
		writebuf(&b);
  801954:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  80195a:	e8 2e ff ff ff       	call   80188d <writebuf>

	return (b.result ? b.result : b.error);
  80195f:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  801965:	85 c0                	test   %eax,%eax
  801967:	75 06                	jne    80196f <vfprintf+0x6e>
  801969:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  80196f:	c9                   	leave  
  801970:	c3                   	ret    

00801971 <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  801971:	55                   	push   %ebp
  801972:	89 e5                	mov    %esp,%ebp
  801974:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  801977:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  80197a:	50                   	push   %eax
  80197b:	ff 75 0c             	pushl  0xc(%ebp)
  80197e:	ff 75 08             	pushl  0x8(%ebp)
  801981:	e8 7b ff ff ff       	call   801901 <vfprintf>
	va_end(ap);

	return cnt;
}
  801986:	c9                   	leave  
  801987:	c3                   	ret    

00801988 <printf>:

int
printf(const char *fmt, ...)
{
  801988:	55                   	push   %ebp
  801989:	89 e5                	mov    %esp,%ebp
  80198b:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80198e:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  801991:	50                   	push   %eax
  801992:	ff 75 08             	pushl  0x8(%ebp)
  801995:	6a 01                	push   $0x1
  801997:	e8 65 ff ff ff       	call   801901 <vfprintf>
	va_end(ap);

	return cnt;
}
  80199c:	c9                   	leave  
  80199d:	c3                   	ret    

0080199e <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  80199e:	55                   	push   %ebp
  80199f:	89 e5                	mov    %esp,%ebp
  8019a1:	56                   	push   %esi
  8019a2:	53                   	push   %ebx
  8019a3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  8019a6:	83 ec 0c             	sub    $0xc,%esp
  8019a9:	ff 75 08             	pushl  0x8(%ebp)
  8019ac:	e8 0b f7 ff ff       	call   8010bc <fd2data>
  8019b1:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  8019b3:	83 c4 08             	add    $0x8,%esp
  8019b6:	68 cf 26 80 00       	push   $0x8026cf
  8019bb:	53                   	push   %ebx
  8019bc:	e8 a0 ef ff ff       	call   800961 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  8019c1:	8b 46 04             	mov    0x4(%esi),%eax
  8019c4:	2b 06                	sub    (%esi),%eax
  8019c6:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  8019cc:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8019d3:	00 00 00 
	stat->st_dev = &devpipe;
  8019d6:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  8019dd:	30 80 00 
	return 0;
}
  8019e0:	b8 00 00 00 00       	mov    $0x0,%eax
  8019e5:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8019e8:	5b                   	pop    %ebx
  8019e9:	5e                   	pop    %esi
  8019ea:	5d                   	pop    %ebp
  8019eb:	c3                   	ret    

008019ec <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  8019ec:	55                   	push   %ebp
  8019ed:	89 e5                	mov    %esp,%ebp
  8019ef:	53                   	push   %ebx
  8019f0:	83 ec 0c             	sub    $0xc,%esp
  8019f3:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  8019f6:	53                   	push   %ebx
  8019f7:	6a 00                	push   $0x0
  8019f9:	e8 cc f3 ff ff       	call   800dca <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  8019fe:	89 1c 24             	mov    %ebx,(%esp)
  801a01:	e8 b6 f6 ff ff       	call   8010bc <fd2data>
  801a06:	83 c4 08             	add    $0x8,%esp
  801a09:	50                   	push   %eax
  801a0a:	6a 00                	push   $0x0
  801a0c:	e8 b9 f3 ff ff       	call   800dca <sys_page_unmap>
}
  801a11:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801a14:	c9                   	leave  
  801a15:	c3                   	ret    

00801a16 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801a16:	55                   	push   %ebp
  801a17:	89 e5                	mov    %esp,%ebp
  801a19:	57                   	push   %edi
  801a1a:	56                   	push   %esi
  801a1b:	53                   	push   %ebx
  801a1c:	83 ec 1c             	sub    $0x1c,%esp
  801a1f:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801a22:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801a24:	a1 20 44 80 00       	mov    0x804420,%eax
  801a29:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801a2c:	83 ec 0c             	sub    $0xc,%esp
  801a2f:	ff 75 e0             	pushl  -0x20(%ebp)
  801a32:	e8 27 05 00 00       	call   801f5e <pageref>
  801a37:	89 c3                	mov    %eax,%ebx
  801a39:	89 3c 24             	mov    %edi,(%esp)
  801a3c:	e8 1d 05 00 00       	call   801f5e <pageref>
  801a41:	83 c4 10             	add    $0x10,%esp
  801a44:	39 c3                	cmp    %eax,%ebx
  801a46:	0f 94 c1             	sete   %cl
  801a49:	0f b6 c9             	movzbl %cl,%ecx
  801a4c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801a4f:	8b 15 20 44 80 00    	mov    0x804420,%edx
  801a55:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801a58:	39 ce                	cmp    %ecx,%esi
  801a5a:	74 1b                	je     801a77 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801a5c:	39 c3                	cmp    %eax,%ebx
  801a5e:	75 c4                	jne    801a24 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801a60:	8b 42 58             	mov    0x58(%edx),%eax
  801a63:	ff 75 e4             	pushl  -0x1c(%ebp)
  801a66:	50                   	push   %eax
  801a67:	56                   	push   %esi
  801a68:	68 d6 26 80 00       	push   $0x8026d6
  801a6d:	e8 8b e9 ff ff       	call   8003fd <cprintf>
  801a72:	83 c4 10             	add    $0x10,%esp
  801a75:	eb ad                	jmp    801a24 <_pipeisclosed+0xe>
	}
}
  801a77:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801a7a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a7d:	5b                   	pop    %ebx
  801a7e:	5e                   	pop    %esi
  801a7f:	5f                   	pop    %edi
  801a80:	5d                   	pop    %ebp
  801a81:	c3                   	ret    

00801a82 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801a82:	55                   	push   %ebp
  801a83:	89 e5                	mov    %esp,%ebp
  801a85:	57                   	push   %edi
  801a86:	56                   	push   %esi
  801a87:	53                   	push   %ebx
  801a88:	83 ec 18             	sub    $0x18,%esp
  801a8b:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801a8e:	56                   	push   %esi
  801a8f:	e8 28 f6 ff ff       	call   8010bc <fd2data>
  801a94:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a96:	83 c4 10             	add    $0x10,%esp
  801a99:	bf 00 00 00 00       	mov    $0x0,%edi
  801a9e:	eb 3b                	jmp    801adb <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801aa0:	89 da                	mov    %ebx,%edx
  801aa2:	89 f0                	mov    %esi,%eax
  801aa4:	e8 6d ff ff ff       	call   801a16 <_pipeisclosed>
  801aa9:	85 c0                	test   %eax,%eax
  801aab:	75 38                	jne    801ae5 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801aad:	e8 74 f2 ff ff       	call   800d26 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801ab2:	8b 53 04             	mov    0x4(%ebx),%edx
  801ab5:	8b 03                	mov    (%ebx),%eax
  801ab7:	83 c0 20             	add    $0x20,%eax
  801aba:	39 c2                	cmp    %eax,%edx
  801abc:	73 e2                	jae    801aa0 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801abe:	8b 45 0c             	mov    0xc(%ebp),%eax
  801ac1:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801ac4:	89 d0                	mov    %edx,%eax
  801ac6:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801acb:	79 05                	jns    801ad2 <devpipe_write+0x50>
  801acd:	48                   	dec    %eax
  801ace:	83 c8 e0             	or     $0xffffffe0,%eax
  801ad1:	40                   	inc    %eax
  801ad2:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801ad6:	42                   	inc    %edx
  801ad7:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801ada:	47                   	inc    %edi
  801adb:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801ade:	75 d2                	jne    801ab2 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801ae0:	8b 45 10             	mov    0x10(%ebp),%eax
  801ae3:	eb 05                	jmp    801aea <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801ae5:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801aea:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801aed:	5b                   	pop    %ebx
  801aee:	5e                   	pop    %esi
  801aef:	5f                   	pop    %edi
  801af0:	5d                   	pop    %ebp
  801af1:	c3                   	ret    

00801af2 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801af2:	55                   	push   %ebp
  801af3:	89 e5                	mov    %esp,%ebp
  801af5:	57                   	push   %edi
  801af6:	56                   	push   %esi
  801af7:	53                   	push   %ebx
  801af8:	83 ec 18             	sub    $0x18,%esp
  801afb:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801afe:	57                   	push   %edi
  801aff:	e8 b8 f5 ff ff       	call   8010bc <fd2data>
  801b04:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801b06:	83 c4 10             	add    $0x10,%esp
  801b09:	bb 00 00 00 00       	mov    $0x0,%ebx
  801b0e:	eb 3a                	jmp    801b4a <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801b10:	85 db                	test   %ebx,%ebx
  801b12:	74 04                	je     801b18 <devpipe_read+0x26>
				return i;
  801b14:	89 d8                	mov    %ebx,%eax
  801b16:	eb 41                	jmp    801b59 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801b18:	89 f2                	mov    %esi,%edx
  801b1a:	89 f8                	mov    %edi,%eax
  801b1c:	e8 f5 fe ff ff       	call   801a16 <_pipeisclosed>
  801b21:	85 c0                	test   %eax,%eax
  801b23:	75 2f                	jne    801b54 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801b25:	e8 fc f1 ff ff       	call   800d26 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801b2a:	8b 06                	mov    (%esi),%eax
  801b2c:	3b 46 04             	cmp    0x4(%esi),%eax
  801b2f:	74 df                	je     801b10 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801b31:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801b36:	79 05                	jns    801b3d <devpipe_read+0x4b>
  801b38:	48                   	dec    %eax
  801b39:	83 c8 e0             	or     $0xffffffe0,%eax
  801b3c:	40                   	inc    %eax
  801b3d:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801b41:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801b44:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801b47:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801b49:	43                   	inc    %ebx
  801b4a:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801b4d:	75 db                	jne    801b2a <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801b4f:	8b 45 10             	mov    0x10(%ebp),%eax
  801b52:	eb 05                	jmp    801b59 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801b54:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801b59:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b5c:	5b                   	pop    %ebx
  801b5d:	5e                   	pop    %esi
  801b5e:	5f                   	pop    %edi
  801b5f:	5d                   	pop    %ebp
  801b60:	c3                   	ret    

00801b61 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801b61:	55                   	push   %ebp
  801b62:	89 e5                	mov    %esp,%ebp
  801b64:	56                   	push   %esi
  801b65:	53                   	push   %ebx
  801b66:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801b69:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b6c:	50                   	push   %eax
  801b6d:	e8 61 f5 ff ff       	call   8010d3 <fd_alloc>
  801b72:	83 c4 10             	add    $0x10,%esp
  801b75:	85 c0                	test   %eax,%eax
  801b77:	0f 88 2a 01 00 00    	js     801ca7 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b7d:	83 ec 04             	sub    $0x4,%esp
  801b80:	68 07 04 00 00       	push   $0x407
  801b85:	ff 75 f4             	pushl  -0xc(%ebp)
  801b88:	6a 00                	push   $0x0
  801b8a:	e8 b6 f1 ff ff       	call   800d45 <sys_page_alloc>
  801b8f:	83 c4 10             	add    $0x10,%esp
  801b92:	85 c0                	test   %eax,%eax
  801b94:	0f 88 0d 01 00 00    	js     801ca7 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801b9a:	83 ec 0c             	sub    $0xc,%esp
  801b9d:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801ba0:	50                   	push   %eax
  801ba1:	e8 2d f5 ff ff       	call   8010d3 <fd_alloc>
  801ba6:	89 c3                	mov    %eax,%ebx
  801ba8:	83 c4 10             	add    $0x10,%esp
  801bab:	85 c0                	test   %eax,%eax
  801bad:	0f 88 e2 00 00 00    	js     801c95 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801bb3:	83 ec 04             	sub    $0x4,%esp
  801bb6:	68 07 04 00 00       	push   $0x407
  801bbb:	ff 75 f0             	pushl  -0x10(%ebp)
  801bbe:	6a 00                	push   $0x0
  801bc0:	e8 80 f1 ff ff       	call   800d45 <sys_page_alloc>
  801bc5:	89 c3                	mov    %eax,%ebx
  801bc7:	83 c4 10             	add    $0x10,%esp
  801bca:	85 c0                	test   %eax,%eax
  801bcc:	0f 88 c3 00 00 00    	js     801c95 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801bd2:	83 ec 0c             	sub    $0xc,%esp
  801bd5:	ff 75 f4             	pushl  -0xc(%ebp)
  801bd8:	e8 df f4 ff ff       	call   8010bc <fd2data>
  801bdd:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801bdf:	83 c4 0c             	add    $0xc,%esp
  801be2:	68 07 04 00 00       	push   $0x407
  801be7:	50                   	push   %eax
  801be8:	6a 00                	push   $0x0
  801bea:	e8 56 f1 ff ff       	call   800d45 <sys_page_alloc>
  801bef:	89 c3                	mov    %eax,%ebx
  801bf1:	83 c4 10             	add    $0x10,%esp
  801bf4:	85 c0                	test   %eax,%eax
  801bf6:	0f 88 89 00 00 00    	js     801c85 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801bfc:	83 ec 0c             	sub    $0xc,%esp
  801bff:	ff 75 f0             	pushl  -0x10(%ebp)
  801c02:	e8 b5 f4 ff ff       	call   8010bc <fd2data>
  801c07:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801c0e:	50                   	push   %eax
  801c0f:	6a 00                	push   $0x0
  801c11:	56                   	push   %esi
  801c12:	6a 00                	push   $0x0
  801c14:	e8 6f f1 ff ff       	call   800d88 <sys_page_map>
  801c19:	89 c3                	mov    %eax,%ebx
  801c1b:	83 c4 20             	add    $0x20,%esp
  801c1e:	85 c0                	test   %eax,%eax
  801c20:	78 55                	js     801c77 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801c22:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801c28:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c2b:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801c2d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c30:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801c37:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801c3d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c40:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801c42:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c45:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801c4c:	83 ec 0c             	sub    $0xc,%esp
  801c4f:	ff 75 f4             	pushl  -0xc(%ebp)
  801c52:	e8 55 f4 ff ff       	call   8010ac <fd2num>
  801c57:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801c5a:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801c5c:	83 c4 04             	add    $0x4,%esp
  801c5f:	ff 75 f0             	pushl  -0x10(%ebp)
  801c62:	e8 45 f4 ff ff       	call   8010ac <fd2num>
  801c67:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801c6a:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801c6d:	83 c4 10             	add    $0x10,%esp
  801c70:	b8 00 00 00 00       	mov    $0x0,%eax
  801c75:	eb 30                	jmp    801ca7 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801c77:	83 ec 08             	sub    $0x8,%esp
  801c7a:	56                   	push   %esi
  801c7b:	6a 00                	push   $0x0
  801c7d:	e8 48 f1 ff ff       	call   800dca <sys_page_unmap>
  801c82:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801c85:	83 ec 08             	sub    $0x8,%esp
  801c88:	ff 75 f0             	pushl  -0x10(%ebp)
  801c8b:	6a 00                	push   $0x0
  801c8d:	e8 38 f1 ff ff       	call   800dca <sys_page_unmap>
  801c92:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801c95:	83 ec 08             	sub    $0x8,%esp
  801c98:	ff 75 f4             	pushl  -0xc(%ebp)
  801c9b:	6a 00                	push   $0x0
  801c9d:	e8 28 f1 ff ff       	call   800dca <sys_page_unmap>
  801ca2:	83 c4 10             	add    $0x10,%esp
  801ca5:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801ca7:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801caa:	5b                   	pop    %ebx
  801cab:	5e                   	pop    %esi
  801cac:	5d                   	pop    %ebp
  801cad:	c3                   	ret    

00801cae <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801cae:	55                   	push   %ebp
  801caf:	89 e5                	mov    %esp,%ebp
  801cb1:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801cb4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801cb7:	50                   	push   %eax
  801cb8:	ff 75 08             	pushl  0x8(%ebp)
  801cbb:	e8 62 f4 ff ff       	call   801122 <fd_lookup>
  801cc0:	83 c4 10             	add    $0x10,%esp
  801cc3:	85 c0                	test   %eax,%eax
  801cc5:	78 18                	js     801cdf <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801cc7:	83 ec 0c             	sub    $0xc,%esp
  801cca:	ff 75 f4             	pushl  -0xc(%ebp)
  801ccd:	e8 ea f3 ff ff       	call   8010bc <fd2data>
	return _pipeisclosed(fd, p);
  801cd2:	89 c2                	mov    %eax,%edx
  801cd4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801cd7:	e8 3a fd ff ff       	call   801a16 <_pipeisclosed>
  801cdc:	83 c4 10             	add    $0x10,%esp
}
  801cdf:	c9                   	leave  
  801ce0:	c3                   	ret    

00801ce1 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801ce1:	55                   	push   %ebp
  801ce2:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801ce4:	b8 00 00 00 00       	mov    $0x0,%eax
  801ce9:	5d                   	pop    %ebp
  801cea:	c3                   	ret    

00801ceb <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801ceb:	55                   	push   %ebp
  801cec:	89 e5                	mov    %esp,%ebp
  801cee:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801cf1:	68 ee 26 80 00       	push   $0x8026ee
  801cf6:	ff 75 0c             	pushl  0xc(%ebp)
  801cf9:	e8 63 ec ff ff       	call   800961 <strcpy>
	return 0;
}
  801cfe:	b8 00 00 00 00       	mov    $0x0,%eax
  801d03:	c9                   	leave  
  801d04:	c3                   	ret    

00801d05 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801d05:	55                   	push   %ebp
  801d06:	89 e5                	mov    %esp,%ebp
  801d08:	57                   	push   %edi
  801d09:	56                   	push   %esi
  801d0a:	53                   	push   %ebx
  801d0b:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801d11:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801d16:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801d1c:	eb 2c                	jmp    801d4a <devcons_write+0x45>
		m = n - tot;
  801d1e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801d21:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801d23:	83 fb 7f             	cmp    $0x7f,%ebx
  801d26:	76 05                	jbe    801d2d <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801d28:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801d2d:	83 ec 04             	sub    $0x4,%esp
  801d30:	53                   	push   %ebx
  801d31:	03 45 0c             	add    0xc(%ebp),%eax
  801d34:	50                   	push   %eax
  801d35:	57                   	push   %edi
  801d36:	e8 9b ed ff ff       	call   800ad6 <memmove>
		sys_cputs(buf, m);
  801d3b:	83 c4 08             	add    $0x8,%esp
  801d3e:	53                   	push   %ebx
  801d3f:	57                   	push   %edi
  801d40:	e8 44 ef ff ff       	call   800c89 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801d45:	01 de                	add    %ebx,%esi
  801d47:	83 c4 10             	add    $0x10,%esp
  801d4a:	89 f0                	mov    %esi,%eax
  801d4c:	3b 75 10             	cmp    0x10(%ebp),%esi
  801d4f:	72 cd                	jb     801d1e <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801d51:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d54:	5b                   	pop    %ebx
  801d55:	5e                   	pop    %esi
  801d56:	5f                   	pop    %edi
  801d57:	5d                   	pop    %ebp
  801d58:	c3                   	ret    

00801d59 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801d59:	55                   	push   %ebp
  801d5a:	89 e5                	mov    %esp,%ebp
  801d5c:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801d5f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801d63:	75 07                	jne    801d6c <devcons_read+0x13>
  801d65:	eb 23                	jmp    801d8a <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801d67:	e8 ba ef ff ff       	call   800d26 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801d6c:	e8 36 ef ff ff       	call   800ca7 <sys_cgetc>
  801d71:	85 c0                	test   %eax,%eax
  801d73:	74 f2                	je     801d67 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801d75:	85 c0                	test   %eax,%eax
  801d77:	78 1d                	js     801d96 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801d79:	83 f8 04             	cmp    $0x4,%eax
  801d7c:	74 13                	je     801d91 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801d7e:	8b 55 0c             	mov    0xc(%ebp),%edx
  801d81:	88 02                	mov    %al,(%edx)
	return 1;
  801d83:	b8 01 00 00 00       	mov    $0x1,%eax
  801d88:	eb 0c                	jmp    801d96 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801d8a:	b8 00 00 00 00       	mov    $0x0,%eax
  801d8f:	eb 05                	jmp    801d96 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801d91:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801d96:	c9                   	leave  
  801d97:	c3                   	ret    

00801d98 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801d98:	55                   	push   %ebp
  801d99:	89 e5                	mov    %esp,%ebp
  801d9b:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801d9e:	8b 45 08             	mov    0x8(%ebp),%eax
  801da1:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801da4:	6a 01                	push   $0x1
  801da6:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801da9:	50                   	push   %eax
  801daa:	e8 da ee ff ff       	call   800c89 <sys_cputs>
}
  801daf:	83 c4 10             	add    $0x10,%esp
  801db2:	c9                   	leave  
  801db3:	c3                   	ret    

00801db4 <getchar>:

int
getchar(void)
{
  801db4:	55                   	push   %ebp
  801db5:	89 e5                	mov    %esp,%ebp
  801db7:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801dba:	6a 01                	push   $0x1
  801dbc:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801dbf:	50                   	push   %eax
  801dc0:	6a 00                	push   $0x0
  801dc2:	e8 c1 f5 ff ff       	call   801388 <read>
	if (r < 0)
  801dc7:	83 c4 10             	add    $0x10,%esp
  801dca:	85 c0                	test   %eax,%eax
  801dcc:	78 0f                	js     801ddd <getchar+0x29>
		return r;
	if (r < 1)
  801dce:	85 c0                	test   %eax,%eax
  801dd0:	7e 06                	jle    801dd8 <getchar+0x24>
		return -E_EOF;
	return c;
  801dd2:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801dd6:	eb 05                	jmp    801ddd <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801dd8:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801ddd:	c9                   	leave  
  801dde:	c3                   	ret    

00801ddf <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801ddf:	55                   	push   %ebp
  801de0:	89 e5                	mov    %esp,%ebp
  801de2:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801de5:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801de8:	50                   	push   %eax
  801de9:	ff 75 08             	pushl  0x8(%ebp)
  801dec:	e8 31 f3 ff ff       	call   801122 <fd_lookup>
  801df1:	83 c4 10             	add    $0x10,%esp
  801df4:	85 c0                	test   %eax,%eax
  801df6:	78 11                	js     801e09 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801df8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801dfb:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801e01:	39 10                	cmp    %edx,(%eax)
  801e03:	0f 94 c0             	sete   %al
  801e06:	0f b6 c0             	movzbl %al,%eax
}
  801e09:	c9                   	leave  
  801e0a:	c3                   	ret    

00801e0b <opencons>:

int
opencons(void)
{
  801e0b:	55                   	push   %ebp
  801e0c:	89 e5                	mov    %esp,%ebp
  801e0e:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801e11:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801e14:	50                   	push   %eax
  801e15:	e8 b9 f2 ff ff       	call   8010d3 <fd_alloc>
  801e1a:	83 c4 10             	add    $0x10,%esp
  801e1d:	85 c0                	test   %eax,%eax
  801e1f:	78 3a                	js     801e5b <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801e21:	83 ec 04             	sub    $0x4,%esp
  801e24:	68 07 04 00 00       	push   $0x407
  801e29:	ff 75 f4             	pushl  -0xc(%ebp)
  801e2c:	6a 00                	push   $0x0
  801e2e:	e8 12 ef ff ff       	call   800d45 <sys_page_alloc>
  801e33:	83 c4 10             	add    $0x10,%esp
  801e36:	85 c0                	test   %eax,%eax
  801e38:	78 21                	js     801e5b <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801e3a:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801e40:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e43:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801e45:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e48:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801e4f:	83 ec 0c             	sub    $0xc,%esp
  801e52:	50                   	push   %eax
  801e53:	e8 54 f2 ff ff       	call   8010ac <fd2num>
  801e58:	83 c4 10             	add    $0x10,%esp
}
  801e5b:	c9                   	leave  
  801e5c:	c3                   	ret    

00801e5d <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801e5d:	55                   	push   %ebp
  801e5e:	89 e5                	mov    %esp,%ebp
  801e60:	56                   	push   %esi
  801e61:	53                   	push   %ebx
  801e62:	8b 75 08             	mov    0x8(%ebp),%esi
  801e65:	8b 45 0c             	mov    0xc(%ebp),%eax
  801e68:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801e6b:	85 c0                	test   %eax,%eax
  801e6d:	75 05                	jne    801e74 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801e6f:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801e74:	83 ec 0c             	sub    $0xc,%esp
  801e77:	50                   	push   %eax
  801e78:	e8 97 f0 ff ff       	call   800f14 <sys_ipc_recv>
  801e7d:	83 c4 10             	add    $0x10,%esp
  801e80:	85 c0                	test   %eax,%eax
  801e82:	74 16                	je     801e9a <ipc_recv+0x3d>
        if (from_env_store)
  801e84:	85 f6                	test   %esi,%esi
  801e86:	74 06                	je     801e8e <ipc_recv+0x31>
            *from_env_store = 0;
  801e88:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801e8e:	85 db                	test   %ebx,%ebx
  801e90:	74 2c                	je     801ebe <ipc_recv+0x61>
            *perm_store = 0;
  801e92:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801e98:	eb 24                	jmp    801ebe <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801e9a:	85 f6                	test   %esi,%esi
  801e9c:	74 0a                	je     801ea8 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801e9e:	a1 20 44 80 00       	mov    0x804420,%eax
  801ea3:	8b 40 74             	mov    0x74(%eax),%eax
  801ea6:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801ea8:	85 db                	test   %ebx,%ebx
  801eaa:	74 0a                	je     801eb6 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801eac:	a1 20 44 80 00       	mov    0x804420,%eax
  801eb1:	8b 40 78             	mov    0x78(%eax),%eax
  801eb4:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801eb6:	a1 20 44 80 00       	mov    0x804420,%eax
  801ebb:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801ebe:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801ec1:	5b                   	pop    %ebx
  801ec2:	5e                   	pop    %esi
  801ec3:	5d                   	pop    %ebp
  801ec4:	c3                   	ret    

00801ec5 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801ec5:	55                   	push   %ebp
  801ec6:	89 e5                	mov    %esp,%ebp
  801ec8:	57                   	push   %edi
  801ec9:	56                   	push   %esi
  801eca:	53                   	push   %ebx
  801ecb:	83 ec 0c             	sub    $0xc,%esp
  801ece:	8b 75 0c             	mov    0xc(%ebp),%esi
  801ed1:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801ed4:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801ed7:	85 db                	test   %ebx,%ebx
  801ed9:	75 0c                	jne    801ee7 <ipc_send+0x22>
        pg = (void *)UTOP;
  801edb:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801ee0:	eb 05                	jmp    801ee7 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801ee2:	e8 3f ee ff ff       	call   800d26 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801ee7:	57                   	push   %edi
  801ee8:	53                   	push   %ebx
  801ee9:	56                   	push   %esi
  801eea:	ff 75 08             	pushl  0x8(%ebp)
  801eed:	e8 ff ef ff ff       	call   800ef1 <sys_ipc_try_send>
  801ef2:	83 c4 10             	add    $0x10,%esp
  801ef5:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801ef8:	74 e8                	je     801ee2 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801efa:	85 c0                	test   %eax,%eax
  801efc:	74 14                	je     801f12 <ipc_send+0x4d>
        panic("ipc_send: error");
  801efe:	83 ec 04             	sub    $0x4,%esp
  801f01:	68 fa 26 80 00       	push   $0x8026fa
  801f06:	6a 6a                	push   $0x6a
  801f08:	68 0a 27 80 00       	push   $0x80270a
  801f0d:	e8 13 e4 ff ff       	call   800325 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801f12:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f15:	5b                   	pop    %ebx
  801f16:	5e                   	pop    %esi
  801f17:	5f                   	pop    %edi
  801f18:	5d                   	pop    %ebp
  801f19:	c3                   	ret    

00801f1a <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801f1a:	55                   	push   %ebp
  801f1b:	89 e5                	mov    %esp,%ebp
  801f1d:	53                   	push   %ebx
  801f1e:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801f21:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801f26:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801f2d:	89 d0                	mov    %edx,%eax
  801f2f:	c1 e0 07             	shl    $0x7,%eax
  801f32:	29 d8                	sub    %ebx,%eax
  801f34:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801f39:	8b 40 50             	mov    0x50(%eax),%eax
  801f3c:	39 c8                	cmp    %ecx,%eax
  801f3e:	75 0d                	jne    801f4d <ipc_find_env+0x33>
			return envs[i].env_id;
  801f40:	c1 e2 07             	shl    $0x7,%edx
  801f43:	29 da                	sub    %ebx,%edx
  801f45:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801f4b:	eb 0e                	jmp    801f5b <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801f4d:	42                   	inc    %edx
  801f4e:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801f54:	75 d0                	jne    801f26 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801f56:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801f5b:	5b                   	pop    %ebx
  801f5c:	5d                   	pop    %ebp
  801f5d:	c3                   	ret    

00801f5e <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801f5e:	55                   	push   %ebp
  801f5f:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801f61:	8b 45 08             	mov    0x8(%ebp),%eax
  801f64:	c1 e8 16             	shr    $0x16,%eax
  801f67:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801f6e:	a8 01                	test   $0x1,%al
  801f70:	74 21                	je     801f93 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801f72:	8b 45 08             	mov    0x8(%ebp),%eax
  801f75:	c1 e8 0c             	shr    $0xc,%eax
  801f78:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801f7f:	a8 01                	test   $0x1,%al
  801f81:	74 17                	je     801f9a <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801f83:	c1 e8 0c             	shr    $0xc,%eax
  801f86:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801f8d:	ef 
  801f8e:	0f b7 c0             	movzwl %ax,%eax
  801f91:	eb 0c                	jmp    801f9f <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801f93:	b8 00 00 00 00       	mov    $0x0,%eax
  801f98:	eb 05                	jmp    801f9f <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801f9a:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801f9f:	5d                   	pop    %ebp
  801fa0:	c3                   	ret    
  801fa1:	66 90                	xchg   %ax,%ax
  801fa3:	90                   	nop

00801fa4 <__udivdi3>:
  801fa4:	55                   	push   %ebp
  801fa5:	57                   	push   %edi
  801fa6:	56                   	push   %esi
  801fa7:	53                   	push   %ebx
  801fa8:	83 ec 1c             	sub    $0x1c,%esp
  801fab:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801faf:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801fb3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801fb7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801fbb:	89 ca                	mov    %ecx,%edx
  801fbd:	89 f8                	mov    %edi,%eax
  801fbf:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801fc3:	85 f6                	test   %esi,%esi
  801fc5:	75 2d                	jne    801ff4 <__udivdi3+0x50>
  801fc7:	39 cf                	cmp    %ecx,%edi
  801fc9:	77 65                	ja     802030 <__udivdi3+0x8c>
  801fcb:	89 fd                	mov    %edi,%ebp
  801fcd:	85 ff                	test   %edi,%edi
  801fcf:	75 0b                	jne    801fdc <__udivdi3+0x38>
  801fd1:	b8 01 00 00 00       	mov    $0x1,%eax
  801fd6:	31 d2                	xor    %edx,%edx
  801fd8:	f7 f7                	div    %edi
  801fda:	89 c5                	mov    %eax,%ebp
  801fdc:	31 d2                	xor    %edx,%edx
  801fde:	89 c8                	mov    %ecx,%eax
  801fe0:	f7 f5                	div    %ebp
  801fe2:	89 c1                	mov    %eax,%ecx
  801fe4:	89 d8                	mov    %ebx,%eax
  801fe6:	f7 f5                	div    %ebp
  801fe8:	89 cf                	mov    %ecx,%edi
  801fea:	89 fa                	mov    %edi,%edx
  801fec:	83 c4 1c             	add    $0x1c,%esp
  801fef:	5b                   	pop    %ebx
  801ff0:	5e                   	pop    %esi
  801ff1:	5f                   	pop    %edi
  801ff2:	5d                   	pop    %ebp
  801ff3:	c3                   	ret    
  801ff4:	39 ce                	cmp    %ecx,%esi
  801ff6:	77 28                	ja     802020 <__udivdi3+0x7c>
  801ff8:	0f bd fe             	bsr    %esi,%edi
  801ffb:	83 f7 1f             	xor    $0x1f,%edi
  801ffe:	75 40                	jne    802040 <__udivdi3+0x9c>
  802000:	39 ce                	cmp    %ecx,%esi
  802002:	72 0a                	jb     80200e <__udivdi3+0x6a>
  802004:	3b 44 24 08          	cmp    0x8(%esp),%eax
  802008:	0f 87 9e 00 00 00    	ja     8020ac <__udivdi3+0x108>
  80200e:	b8 01 00 00 00       	mov    $0x1,%eax
  802013:	89 fa                	mov    %edi,%edx
  802015:	83 c4 1c             	add    $0x1c,%esp
  802018:	5b                   	pop    %ebx
  802019:	5e                   	pop    %esi
  80201a:	5f                   	pop    %edi
  80201b:	5d                   	pop    %ebp
  80201c:	c3                   	ret    
  80201d:	8d 76 00             	lea    0x0(%esi),%esi
  802020:	31 ff                	xor    %edi,%edi
  802022:	31 c0                	xor    %eax,%eax
  802024:	89 fa                	mov    %edi,%edx
  802026:	83 c4 1c             	add    $0x1c,%esp
  802029:	5b                   	pop    %ebx
  80202a:	5e                   	pop    %esi
  80202b:	5f                   	pop    %edi
  80202c:	5d                   	pop    %ebp
  80202d:	c3                   	ret    
  80202e:	66 90                	xchg   %ax,%ax
  802030:	89 d8                	mov    %ebx,%eax
  802032:	f7 f7                	div    %edi
  802034:	31 ff                	xor    %edi,%edi
  802036:	89 fa                	mov    %edi,%edx
  802038:	83 c4 1c             	add    $0x1c,%esp
  80203b:	5b                   	pop    %ebx
  80203c:	5e                   	pop    %esi
  80203d:	5f                   	pop    %edi
  80203e:	5d                   	pop    %ebp
  80203f:	c3                   	ret    
  802040:	bd 20 00 00 00       	mov    $0x20,%ebp
  802045:	89 eb                	mov    %ebp,%ebx
  802047:	29 fb                	sub    %edi,%ebx
  802049:	89 f9                	mov    %edi,%ecx
  80204b:	d3 e6                	shl    %cl,%esi
  80204d:	89 c5                	mov    %eax,%ebp
  80204f:	88 d9                	mov    %bl,%cl
  802051:	d3 ed                	shr    %cl,%ebp
  802053:	89 e9                	mov    %ebp,%ecx
  802055:	09 f1                	or     %esi,%ecx
  802057:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80205b:	89 f9                	mov    %edi,%ecx
  80205d:	d3 e0                	shl    %cl,%eax
  80205f:	89 c5                	mov    %eax,%ebp
  802061:	89 d6                	mov    %edx,%esi
  802063:	88 d9                	mov    %bl,%cl
  802065:	d3 ee                	shr    %cl,%esi
  802067:	89 f9                	mov    %edi,%ecx
  802069:	d3 e2                	shl    %cl,%edx
  80206b:	8b 44 24 08          	mov    0x8(%esp),%eax
  80206f:	88 d9                	mov    %bl,%cl
  802071:	d3 e8                	shr    %cl,%eax
  802073:	09 c2                	or     %eax,%edx
  802075:	89 d0                	mov    %edx,%eax
  802077:	89 f2                	mov    %esi,%edx
  802079:	f7 74 24 0c          	divl   0xc(%esp)
  80207d:	89 d6                	mov    %edx,%esi
  80207f:	89 c3                	mov    %eax,%ebx
  802081:	f7 e5                	mul    %ebp
  802083:	39 d6                	cmp    %edx,%esi
  802085:	72 19                	jb     8020a0 <__udivdi3+0xfc>
  802087:	74 0b                	je     802094 <__udivdi3+0xf0>
  802089:	89 d8                	mov    %ebx,%eax
  80208b:	31 ff                	xor    %edi,%edi
  80208d:	e9 58 ff ff ff       	jmp    801fea <__udivdi3+0x46>
  802092:	66 90                	xchg   %ax,%ax
  802094:	8b 54 24 08          	mov    0x8(%esp),%edx
  802098:	89 f9                	mov    %edi,%ecx
  80209a:	d3 e2                	shl    %cl,%edx
  80209c:	39 c2                	cmp    %eax,%edx
  80209e:	73 e9                	jae    802089 <__udivdi3+0xe5>
  8020a0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8020a3:	31 ff                	xor    %edi,%edi
  8020a5:	e9 40 ff ff ff       	jmp    801fea <__udivdi3+0x46>
  8020aa:	66 90                	xchg   %ax,%ax
  8020ac:	31 c0                	xor    %eax,%eax
  8020ae:	e9 37 ff ff ff       	jmp    801fea <__udivdi3+0x46>
  8020b3:	90                   	nop

008020b4 <__umoddi3>:
  8020b4:	55                   	push   %ebp
  8020b5:	57                   	push   %edi
  8020b6:	56                   	push   %esi
  8020b7:	53                   	push   %ebx
  8020b8:	83 ec 1c             	sub    $0x1c,%esp
  8020bb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8020bf:	8b 74 24 34          	mov    0x34(%esp),%esi
  8020c3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8020c7:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8020cb:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8020cf:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8020d3:	89 f3                	mov    %esi,%ebx
  8020d5:	89 fa                	mov    %edi,%edx
  8020d7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8020db:	89 34 24             	mov    %esi,(%esp)
  8020de:	85 c0                	test   %eax,%eax
  8020e0:	75 1a                	jne    8020fc <__umoddi3+0x48>
  8020e2:	39 f7                	cmp    %esi,%edi
  8020e4:	0f 86 a2 00 00 00    	jbe    80218c <__umoddi3+0xd8>
  8020ea:	89 c8                	mov    %ecx,%eax
  8020ec:	89 f2                	mov    %esi,%edx
  8020ee:	f7 f7                	div    %edi
  8020f0:	89 d0                	mov    %edx,%eax
  8020f2:	31 d2                	xor    %edx,%edx
  8020f4:	83 c4 1c             	add    $0x1c,%esp
  8020f7:	5b                   	pop    %ebx
  8020f8:	5e                   	pop    %esi
  8020f9:	5f                   	pop    %edi
  8020fa:	5d                   	pop    %ebp
  8020fb:	c3                   	ret    
  8020fc:	39 f0                	cmp    %esi,%eax
  8020fe:	0f 87 ac 00 00 00    	ja     8021b0 <__umoddi3+0xfc>
  802104:	0f bd e8             	bsr    %eax,%ebp
  802107:	83 f5 1f             	xor    $0x1f,%ebp
  80210a:	0f 84 ac 00 00 00    	je     8021bc <__umoddi3+0x108>
  802110:	bf 20 00 00 00       	mov    $0x20,%edi
  802115:	29 ef                	sub    %ebp,%edi
  802117:	89 fe                	mov    %edi,%esi
  802119:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80211d:	89 e9                	mov    %ebp,%ecx
  80211f:	d3 e0                	shl    %cl,%eax
  802121:	89 d7                	mov    %edx,%edi
  802123:	89 f1                	mov    %esi,%ecx
  802125:	d3 ef                	shr    %cl,%edi
  802127:	09 c7                	or     %eax,%edi
  802129:	89 e9                	mov    %ebp,%ecx
  80212b:	d3 e2                	shl    %cl,%edx
  80212d:	89 14 24             	mov    %edx,(%esp)
  802130:	89 d8                	mov    %ebx,%eax
  802132:	d3 e0                	shl    %cl,%eax
  802134:	89 c2                	mov    %eax,%edx
  802136:	8b 44 24 08          	mov    0x8(%esp),%eax
  80213a:	d3 e0                	shl    %cl,%eax
  80213c:	89 44 24 04          	mov    %eax,0x4(%esp)
  802140:	8b 44 24 08          	mov    0x8(%esp),%eax
  802144:	89 f1                	mov    %esi,%ecx
  802146:	d3 e8                	shr    %cl,%eax
  802148:	09 d0                	or     %edx,%eax
  80214a:	d3 eb                	shr    %cl,%ebx
  80214c:	89 da                	mov    %ebx,%edx
  80214e:	f7 f7                	div    %edi
  802150:	89 d3                	mov    %edx,%ebx
  802152:	f7 24 24             	mull   (%esp)
  802155:	89 c6                	mov    %eax,%esi
  802157:	89 d1                	mov    %edx,%ecx
  802159:	39 d3                	cmp    %edx,%ebx
  80215b:	0f 82 87 00 00 00    	jb     8021e8 <__umoddi3+0x134>
  802161:	0f 84 91 00 00 00    	je     8021f8 <__umoddi3+0x144>
  802167:	8b 54 24 04          	mov    0x4(%esp),%edx
  80216b:	29 f2                	sub    %esi,%edx
  80216d:	19 cb                	sbb    %ecx,%ebx
  80216f:	89 d8                	mov    %ebx,%eax
  802171:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  802175:	d3 e0                	shl    %cl,%eax
  802177:	89 e9                	mov    %ebp,%ecx
  802179:	d3 ea                	shr    %cl,%edx
  80217b:	09 d0                	or     %edx,%eax
  80217d:	89 e9                	mov    %ebp,%ecx
  80217f:	d3 eb                	shr    %cl,%ebx
  802181:	89 da                	mov    %ebx,%edx
  802183:	83 c4 1c             	add    $0x1c,%esp
  802186:	5b                   	pop    %ebx
  802187:	5e                   	pop    %esi
  802188:	5f                   	pop    %edi
  802189:	5d                   	pop    %ebp
  80218a:	c3                   	ret    
  80218b:	90                   	nop
  80218c:	89 fd                	mov    %edi,%ebp
  80218e:	85 ff                	test   %edi,%edi
  802190:	75 0b                	jne    80219d <__umoddi3+0xe9>
  802192:	b8 01 00 00 00       	mov    $0x1,%eax
  802197:	31 d2                	xor    %edx,%edx
  802199:	f7 f7                	div    %edi
  80219b:	89 c5                	mov    %eax,%ebp
  80219d:	89 f0                	mov    %esi,%eax
  80219f:	31 d2                	xor    %edx,%edx
  8021a1:	f7 f5                	div    %ebp
  8021a3:	89 c8                	mov    %ecx,%eax
  8021a5:	f7 f5                	div    %ebp
  8021a7:	89 d0                	mov    %edx,%eax
  8021a9:	e9 44 ff ff ff       	jmp    8020f2 <__umoddi3+0x3e>
  8021ae:	66 90                	xchg   %ax,%ax
  8021b0:	89 c8                	mov    %ecx,%eax
  8021b2:	89 f2                	mov    %esi,%edx
  8021b4:	83 c4 1c             	add    $0x1c,%esp
  8021b7:	5b                   	pop    %ebx
  8021b8:	5e                   	pop    %esi
  8021b9:	5f                   	pop    %edi
  8021ba:	5d                   	pop    %ebp
  8021bb:	c3                   	ret    
  8021bc:	3b 04 24             	cmp    (%esp),%eax
  8021bf:	72 06                	jb     8021c7 <__umoddi3+0x113>
  8021c1:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8021c5:	77 0f                	ja     8021d6 <__umoddi3+0x122>
  8021c7:	89 f2                	mov    %esi,%edx
  8021c9:	29 f9                	sub    %edi,%ecx
  8021cb:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  8021cf:	89 14 24             	mov    %edx,(%esp)
  8021d2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  8021d6:	8b 44 24 04          	mov    0x4(%esp),%eax
  8021da:	8b 14 24             	mov    (%esp),%edx
  8021dd:	83 c4 1c             	add    $0x1c,%esp
  8021e0:	5b                   	pop    %ebx
  8021e1:	5e                   	pop    %esi
  8021e2:	5f                   	pop    %edi
  8021e3:	5d                   	pop    %ebp
  8021e4:	c3                   	ret    
  8021e5:	8d 76 00             	lea    0x0(%esi),%esi
  8021e8:	2b 04 24             	sub    (%esp),%eax
  8021eb:	19 fa                	sbb    %edi,%edx
  8021ed:	89 d1                	mov    %edx,%ecx
  8021ef:	89 c6                	mov    %eax,%esi
  8021f1:	e9 71 ff ff ff       	jmp    802167 <__umoddi3+0xb3>
  8021f6:	66 90                	xchg   %ax,%ax
  8021f8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  8021fc:	72 ea                	jb     8021e8 <__umoddi3+0x134>
  8021fe:	89 d9                	mov    %ebx,%ecx
  802200:	e9 62 ff ff ff       	jmp    802167 <__umoddi3+0xb3>
