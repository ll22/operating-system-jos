
obj/user/testbss.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 a5 00 00 00       	call   8000d6 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

uint32_t bigarray[ARRAYSIZE];

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 14             	sub    $0x14,%esp
	int i;

	cprintf("Making sure bss works right...\n");
  800039:	68 e0 0f 80 00       	push   $0x800fe0
  80003e:	e8 cc 01 00 00       	call   80020f <cprintf>
  800043:	83 c4 10             	add    $0x10,%esp
	for (i = 0; i < ARRAYSIZE; i++)
  800046:	b8 00 00 00 00       	mov    $0x0,%eax
		if (bigarray[i] != 0)
  80004b:	83 3c 85 20 20 80 00 	cmpl   $0x0,0x802020(,%eax,4)
  800052:	00 
  800053:	74 12                	je     800067 <umain+0x34>
			panic("bigarray[%d] isn't cleared!\n", i);
  800055:	50                   	push   %eax
  800056:	68 5b 10 80 00       	push   $0x80105b
  80005b:	6a 11                	push   $0x11
  80005d:	68 78 10 80 00       	push   $0x801078
  800062:	e8 d0 00 00 00       	call   800137 <_panic>
umain(int argc, char **argv)
{
	int i;

	cprintf("Making sure bss works right...\n");
	for (i = 0; i < ARRAYSIZE; i++)
  800067:	40                   	inc    %eax
  800068:	3d 00 00 10 00       	cmp    $0x100000,%eax
  80006d:	75 dc                	jne    80004b <umain+0x18>
  80006f:	b8 00 00 00 00       	mov    $0x0,%eax
		if (bigarray[i] != 0)
			panic("bigarray[%d] isn't cleared!\n", i);
	for (i = 0; i < ARRAYSIZE; i++)
		bigarray[i] = i;
  800074:	89 04 85 20 20 80 00 	mov    %eax,0x802020(,%eax,4)

	cprintf("Making sure bss works right...\n");
	for (i = 0; i < ARRAYSIZE; i++)
		if (bigarray[i] != 0)
			panic("bigarray[%d] isn't cleared!\n", i);
	for (i = 0; i < ARRAYSIZE; i++)
  80007b:	40                   	inc    %eax
  80007c:	3d 00 00 10 00       	cmp    $0x100000,%eax
  800081:	75 f1                	jne    800074 <umain+0x41>
  800083:	b8 00 00 00 00       	mov    $0x0,%eax
		bigarray[i] = i;
	for (i = 0; i < ARRAYSIZE; i++)
		if (bigarray[i] != i)
  800088:	3b 04 85 20 20 80 00 	cmp    0x802020(,%eax,4),%eax
  80008f:	74 12                	je     8000a3 <umain+0x70>
			panic("bigarray[%d] didn't hold its value!\n", i);
  800091:	50                   	push   %eax
  800092:	68 00 10 80 00       	push   $0x801000
  800097:	6a 16                	push   $0x16
  800099:	68 78 10 80 00       	push   $0x801078
  80009e:	e8 94 00 00 00       	call   800137 <_panic>
	for (i = 0; i < ARRAYSIZE; i++)
		if (bigarray[i] != 0)
			panic("bigarray[%d] isn't cleared!\n", i);
	for (i = 0; i < ARRAYSIZE; i++)
		bigarray[i] = i;
	for (i = 0; i < ARRAYSIZE; i++)
  8000a3:	40                   	inc    %eax
  8000a4:	3d 00 00 10 00       	cmp    $0x100000,%eax
  8000a9:	75 dd                	jne    800088 <umain+0x55>
		if (bigarray[i] != i)
			panic("bigarray[%d] didn't hold its value!\n", i);

	cprintf("Yes, good.  Now doing a wild write off the end...\n");
  8000ab:	83 ec 0c             	sub    $0xc,%esp
  8000ae:	68 28 10 80 00       	push   $0x801028
  8000b3:	e8 57 01 00 00       	call   80020f <cprintf>
	bigarray[ARRAYSIZE+1024] = 0;
  8000b8:	c7 05 20 30 c0 00 00 	movl   $0x0,0xc03020
  8000bf:	00 00 00 
	panic("SHOULD HAVE TRAPPED!!!");
  8000c2:	83 c4 0c             	add    $0xc,%esp
  8000c5:	68 87 10 80 00       	push   $0x801087
  8000ca:	6a 1a                	push   $0x1a
  8000cc:	68 78 10 80 00       	push   $0x801078
  8000d1:	e8 61 00 00 00       	call   800137 <_panic>

008000d6 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000d6:	55                   	push   %ebp
  8000d7:	89 e5                	mov    %esp,%ebp
  8000d9:	56                   	push   %esi
  8000da:	53                   	push   %ebx
  8000db:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000de:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  8000e1:	e8 33 0a 00 00       	call   800b19 <sys_getenvid>
  8000e6:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000eb:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000f2:	c1 e0 07             	shl    $0x7,%eax
  8000f5:	29 d0                	sub    %edx,%eax
  8000f7:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000fc:	a3 20 20 c0 00       	mov    %eax,0xc02020
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800101:	85 db                	test   %ebx,%ebx
  800103:	7e 07                	jle    80010c <libmain+0x36>
		binaryname = argv[0];
  800105:	8b 06                	mov    (%esi),%eax
  800107:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80010c:	83 ec 08             	sub    $0x8,%esp
  80010f:	56                   	push   %esi
  800110:	53                   	push   %ebx
  800111:	e8 1d ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800116:	e8 0a 00 00 00       	call   800125 <exit>
}
  80011b:	83 c4 10             	add    $0x10,%esp
  80011e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800121:	5b                   	pop    %ebx
  800122:	5e                   	pop    %esi
  800123:	5d                   	pop    %ebp
  800124:	c3                   	ret    

00800125 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800125:	55                   	push   %ebp
  800126:	89 e5                	mov    %esp,%ebp
  800128:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80012b:	6a 00                	push   $0x0
  80012d:	e8 a6 09 00 00       	call   800ad8 <sys_env_destroy>
}
  800132:	83 c4 10             	add    $0x10,%esp
  800135:	c9                   	leave  
  800136:	c3                   	ret    

00800137 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800137:	55                   	push   %ebp
  800138:	89 e5                	mov    %esp,%ebp
  80013a:	56                   	push   %esi
  80013b:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80013c:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80013f:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800145:	e8 cf 09 00 00       	call   800b19 <sys_getenvid>
  80014a:	83 ec 0c             	sub    $0xc,%esp
  80014d:	ff 75 0c             	pushl  0xc(%ebp)
  800150:	ff 75 08             	pushl  0x8(%ebp)
  800153:	56                   	push   %esi
  800154:	50                   	push   %eax
  800155:	68 a8 10 80 00       	push   $0x8010a8
  80015a:	e8 b0 00 00 00       	call   80020f <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80015f:	83 c4 18             	add    $0x18,%esp
  800162:	53                   	push   %ebx
  800163:	ff 75 10             	pushl  0x10(%ebp)
  800166:	e8 53 00 00 00       	call   8001be <vcprintf>
	cprintf("\n");
  80016b:	c7 04 24 76 10 80 00 	movl   $0x801076,(%esp)
  800172:	e8 98 00 00 00       	call   80020f <cprintf>
  800177:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80017a:	cc                   	int3   
  80017b:	eb fd                	jmp    80017a <_panic+0x43>

0080017d <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80017d:	55                   	push   %ebp
  80017e:	89 e5                	mov    %esp,%ebp
  800180:	53                   	push   %ebx
  800181:	83 ec 04             	sub    $0x4,%esp
  800184:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800187:	8b 13                	mov    (%ebx),%edx
  800189:	8d 42 01             	lea    0x1(%edx),%eax
  80018c:	89 03                	mov    %eax,(%ebx)
  80018e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800191:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800195:	3d ff 00 00 00       	cmp    $0xff,%eax
  80019a:	75 1a                	jne    8001b6 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80019c:	83 ec 08             	sub    $0x8,%esp
  80019f:	68 ff 00 00 00       	push   $0xff
  8001a4:	8d 43 08             	lea    0x8(%ebx),%eax
  8001a7:	50                   	push   %eax
  8001a8:	e8 ee 08 00 00       	call   800a9b <sys_cputs>
		b->idx = 0;
  8001ad:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001b3:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001b6:	ff 43 04             	incl   0x4(%ebx)
}
  8001b9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001bc:	c9                   	leave  
  8001bd:	c3                   	ret    

008001be <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001be:	55                   	push   %ebp
  8001bf:	89 e5                	mov    %esp,%ebp
  8001c1:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001c7:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001ce:	00 00 00 
	b.cnt = 0;
  8001d1:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001d8:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001db:	ff 75 0c             	pushl  0xc(%ebp)
  8001de:	ff 75 08             	pushl  0x8(%ebp)
  8001e1:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001e7:	50                   	push   %eax
  8001e8:	68 7d 01 80 00       	push   $0x80017d
  8001ed:	e8 51 01 00 00       	call   800343 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001f2:	83 c4 08             	add    $0x8,%esp
  8001f5:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001fb:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800201:	50                   	push   %eax
  800202:	e8 94 08 00 00       	call   800a9b <sys_cputs>

	return b.cnt;
}
  800207:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80020d:	c9                   	leave  
  80020e:	c3                   	ret    

0080020f <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80020f:	55                   	push   %ebp
  800210:	89 e5                	mov    %esp,%ebp
  800212:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800215:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800218:	50                   	push   %eax
  800219:	ff 75 08             	pushl  0x8(%ebp)
  80021c:	e8 9d ff ff ff       	call   8001be <vcprintf>
	va_end(ap);

	return cnt;
}
  800221:	c9                   	leave  
  800222:	c3                   	ret    

00800223 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800223:	55                   	push   %ebp
  800224:	89 e5                	mov    %esp,%ebp
  800226:	57                   	push   %edi
  800227:	56                   	push   %esi
  800228:	53                   	push   %ebx
  800229:	83 ec 1c             	sub    $0x1c,%esp
  80022c:	89 c7                	mov    %eax,%edi
  80022e:	89 d6                	mov    %edx,%esi
  800230:	8b 45 08             	mov    0x8(%ebp),%eax
  800233:	8b 55 0c             	mov    0xc(%ebp),%edx
  800236:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800239:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80023c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80023f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800244:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800247:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80024a:	39 d3                	cmp    %edx,%ebx
  80024c:	72 05                	jb     800253 <printnum+0x30>
  80024e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800251:	77 45                	ja     800298 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800253:	83 ec 0c             	sub    $0xc,%esp
  800256:	ff 75 18             	pushl  0x18(%ebp)
  800259:	8b 45 14             	mov    0x14(%ebp),%eax
  80025c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80025f:	53                   	push   %ebx
  800260:	ff 75 10             	pushl  0x10(%ebp)
  800263:	83 ec 08             	sub    $0x8,%esp
  800266:	ff 75 e4             	pushl  -0x1c(%ebp)
  800269:	ff 75 e0             	pushl  -0x20(%ebp)
  80026c:	ff 75 dc             	pushl  -0x24(%ebp)
  80026f:	ff 75 d8             	pushl  -0x28(%ebp)
  800272:	e8 f1 0a 00 00       	call   800d68 <__udivdi3>
  800277:	83 c4 18             	add    $0x18,%esp
  80027a:	52                   	push   %edx
  80027b:	50                   	push   %eax
  80027c:	89 f2                	mov    %esi,%edx
  80027e:	89 f8                	mov    %edi,%eax
  800280:	e8 9e ff ff ff       	call   800223 <printnum>
  800285:	83 c4 20             	add    $0x20,%esp
  800288:	eb 16                	jmp    8002a0 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80028a:	83 ec 08             	sub    $0x8,%esp
  80028d:	56                   	push   %esi
  80028e:	ff 75 18             	pushl  0x18(%ebp)
  800291:	ff d7                	call   *%edi
  800293:	83 c4 10             	add    $0x10,%esp
  800296:	eb 03                	jmp    80029b <printnum+0x78>
  800298:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80029b:	4b                   	dec    %ebx
  80029c:	85 db                	test   %ebx,%ebx
  80029e:	7f ea                	jg     80028a <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002a0:	83 ec 08             	sub    $0x8,%esp
  8002a3:	56                   	push   %esi
  8002a4:	83 ec 04             	sub    $0x4,%esp
  8002a7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002aa:	ff 75 e0             	pushl  -0x20(%ebp)
  8002ad:	ff 75 dc             	pushl  -0x24(%ebp)
  8002b0:	ff 75 d8             	pushl  -0x28(%ebp)
  8002b3:	e8 c0 0b 00 00       	call   800e78 <__umoddi3>
  8002b8:	83 c4 14             	add    $0x14,%esp
  8002bb:	0f be 80 cb 10 80 00 	movsbl 0x8010cb(%eax),%eax
  8002c2:	50                   	push   %eax
  8002c3:	ff d7                	call   *%edi
}
  8002c5:	83 c4 10             	add    $0x10,%esp
  8002c8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002cb:	5b                   	pop    %ebx
  8002cc:	5e                   	pop    %esi
  8002cd:	5f                   	pop    %edi
  8002ce:	5d                   	pop    %ebp
  8002cf:	c3                   	ret    

008002d0 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002d0:	55                   	push   %ebp
  8002d1:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002d3:	83 fa 01             	cmp    $0x1,%edx
  8002d6:	7e 0e                	jle    8002e6 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002d8:	8b 10                	mov    (%eax),%edx
  8002da:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002dd:	89 08                	mov    %ecx,(%eax)
  8002df:	8b 02                	mov    (%edx),%eax
  8002e1:	8b 52 04             	mov    0x4(%edx),%edx
  8002e4:	eb 22                	jmp    800308 <getuint+0x38>
	else if (lflag)
  8002e6:	85 d2                	test   %edx,%edx
  8002e8:	74 10                	je     8002fa <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002ea:	8b 10                	mov    (%eax),%edx
  8002ec:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002ef:	89 08                	mov    %ecx,(%eax)
  8002f1:	8b 02                	mov    (%edx),%eax
  8002f3:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f8:	eb 0e                	jmp    800308 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002fa:	8b 10                	mov    (%eax),%edx
  8002fc:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002ff:	89 08                	mov    %ecx,(%eax)
  800301:	8b 02                	mov    (%edx),%eax
  800303:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800308:	5d                   	pop    %ebp
  800309:	c3                   	ret    

0080030a <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80030a:	55                   	push   %ebp
  80030b:	89 e5                	mov    %esp,%ebp
  80030d:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800310:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800313:	8b 10                	mov    (%eax),%edx
  800315:	3b 50 04             	cmp    0x4(%eax),%edx
  800318:	73 0a                	jae    800324 <sprintputch+0x1a>
		*b->buf++ = ch;
  80031a:	8d 4a 01             	lea    0x1(%edx),%ecx
  80031d:	89 08                	mov    %ecx,(%eax)
  80031f:	8b 45 08             	mov    0x8(%ebp),%eax
  800322:	88 02                	mov    %al,(%edx)
}
  800324:	5d                   	pop    %ebp
  800325:	c3                   	ret    

00800326 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800326:	55                   	push   %ebp
  800327:	89 e5                	mov    %esp,%ebp
  800329:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80032c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80032f:	50                   	push   %eax
  800330:	ff 75 10             	pushl  0x10(%ebp)
  800333:	ff 75 0c             	pushl  0xc(%ebp)
  800336:	ff 75 08             	pushl  0x8(%ebp)
  800339:	e8 05 00 00 00       	call   800343 <vprintfmt>
	va_end(ap);
}
  80033e:	83 c4 10             	add    $0x10,%esp
  800341:	c9                   	leave  
  800342:	c3                   	ret    

00800343 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800343:	55                   	push   %ebp
  800344:	89 e5                	mov    %esp,%ebp
  800346:	57                   	push   %edi
  800347:	56                   	push   %esi
  800348:	53                   	push   %ebx
  800349:	83 ec 2c             	sub    $0x2c,%esp
  80034c:	8b 75 08             	mov    0x8(%ebp),%esi
  80034f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800352:	8b 7d 10             	mov    0x10(%ebp),%edi
  800355:	eb 12                	jmp    800369 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800357:	85 c0                	test   %eax,%eax
  800359:	0f 84 68 03 00 00    	je     8006c7 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80035f:	83 ec 08             	sub    $0x8,%esp
  800362:	53                   	push   %ebx
  800363:	50                   	push   %eax
  800364:	ff d6                	call   *%esi
  800366:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800369:	47                   	inc    %edi
  80036a:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80036e:	83 f8 25             	cmp    $0x25,%eax
  800371:	75 e4                	jne    800357 <vprintfmt+0x14>
  800373:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800377:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80037e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800385:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80038c:	ba 00 00 00 00       	mov    $0x0,%edx
  800391:	eb 07                	jmp    80039a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800393:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800396:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039a:	8d 47 01             	lea    0x1(%edi),%eax
  80039d:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003a0:	0f b6 0f             	movzbl (%edi),%ecx
  8003a3:	8a 07                	mov    (%edi),%al
  8003a5:	83 e8 23             	sub    $0x23,%eax
  8003a8:	3c 55                	cmp    $0x55,%al
  8003aa:	0f 87 fe 02 00 00    	ja     8006ae <vprintfmt+0x36b>
  8003b0:	0f b6 c0             	movzbl %al,%eax
  8003b3:	ff 24 85 20 12 80 00 	jmp    *0x801220(,%eax,4)
  8003ba:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003bd:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003c1:	eb d7                	jmp    80039a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003c6:	b8 00 00 00 00       	mov    $0x0,%eax
  8003cb:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003ce:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003d1:	01 c0                	add    %eax,%eax
  8003d3:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003d7:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003da:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003dd:	83 fa 09             	cmp    $0x9,%edx
  8003e0:	77 34                	ja     800416 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003e2:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003e3:	eb e9                	jmp    8003ce <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003e5:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e8:	8d 48 04             	lea    0x4(%eax),%ecx
  8003eb:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003ee:	8b 00                	mov    (%eax),%eax
  8003f0:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003f6:	eb 24                	jmp    80041c <vprintfmt+0xd9>
  8003f8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003fc:	79 07                	jns    800405 <vprintfmt+0xc2>
  8003fe:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800405:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800408:	eb 90                	jmp    80039a <vprintfmt+0x57>
  80040a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80040d:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800414:	eb 84                	jmp    80039a <vprintfmt+0x57>
  800416:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800419:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80041c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800420:	0f 89 74 ff ff ff    	jns    80039a <vprintfmt+0x57>
				width = precision, precision = -1;
  800426:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800429:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80042c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800433:	e9 62 ff ff ff       	jmp    80039a <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800438:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800439:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80043c:	e9 59 ff ff ff       	jmp    80039a <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800441:	8b 45 14             	mov    0x14(%ebp),%eax
  800444:	8d 50 04             	lea    0x4(%eax),%edx
  800447:	89 55 14             	mov    %edx,0x14(%ebp)
  80044a:	83 ec 08             	sub    $0x8,%esp
  80044d:	53                   	push   %ebx
  80044e:	ff 30                	pushl  (%eax)
  800450:	ff d6                	call   *%esi
			break;
  800452:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800455:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800458:	e9 0c ff ff ff       	jmp    800369 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80045d:	8b 45 14             	mov    0x14(%ebp),%eax
  800460:	8d 50 04             	lea    0x4(%eax),%edx
  800463:	89 55 14             	mov    %edx,0x14(%ebp)
  800466:	8b 00                	mov    (%eax),%eax
  800468:	85 c0                	test   %eax,%eax
  80046a:	79 02                	jns    80046e <vprintfmt+0x12b>
  80046c:	f7 d8                	neg    %eax
  80046e:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800470:	83 f8 0f             	cmp    $0xf,%eax
  800473:	7f 0b                	jg     800480 <vprintfmt+0x13d>
  800475:	8b 04 85 80 13 80 00 	mov    0x801380(,%eax,4),%eax
  80047c:	85 c0                	test   %eax,%eax
  80047e:	75 18                	jne    800498 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800480:	52                   	push   %edx
  800481:	68 e3 10 80 00       	push   $0x8010e3
  800486:	53                   	push   %ebx
  800487:	56                   	push   %esi
  800488:	e8 99 fe ff ff       	call   800326 <printfmt>
  80048d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800490:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800493:	e9 d1 fe ff ff       	jmp    800369 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800498:	50                   	push   %eax
  800499:	68 ec 10 80 00       	push   $0x8010ec
  80049e:	53                   	push   %ebx
  80049f:	56                   	push   %esi
  8004a0:	e8 81 fe ff ff       	call   800326 <printfmt>
  8004a5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004ab:	e9 b9 fe ff ff       	jmp    800369 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004b0:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b3:	8d 50 04             	lea    0x4(%eax),%edx
  8004b6:	89 55 14             	mov    %edx,0x14(%ebp)
  8004b9:	8b 38                	mov    (%eax),%edi
  8004bb:	85 ff                	test   %edi,%edi
  8004bd:	75 05                	jne    8004c4 <vprintfmt+0x181>
				p = "(null)";
  8004bf:	bf dc 10 80 00       	mov    $0x8010dc,%edi
			if (width > 0 && padc != '-')
  8004c4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004c8:	0f 8e 90 00 00 00    	jle    80055e <vprintfmt+0x21b>
  8004ce:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004d2:	0f 84 8e 00 00 00    	je     800566 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004d8:	83 ec 08             	sub    $0x8,%esp
  8004db:	ff 75 d0             	pushl  -0x30(%ebp)
  8004de:	57                   	push   %edi
  8004df:	e8 70 02 00 00       	call   800754 <strnlen>
  8004e4:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004e7:	29 c1                	sub    %eax,%ecx
  8004e9:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004ec:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004ef:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004f3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004f6:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004f9:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004fb:	eb 0d                	jmp    80050a <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004fd:	83 ec 08             	sub    $0x8,%esp
  800500:	53                   	push   %ebx
  800501:	ff 75 e4             	pushl  -0x1c(%ebp)
  800504:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800506:	4f                   	dec    %edi
  800507:	83 c4 10             	add    $0x10,%esp
  80050a:	85 ff                	test   %edi,%edi
  80050c:	7f ef                	jg     8004fd <vprintfmt+0x1ba>
  80050e:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800511:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800514:	89 c8                	mov    %ecx,%eax
  800516:	85 c9                	test   %ecx,%ecx
  800518:	79 05                	jns    80051f <vprintfmt+0x1dc>
  80051a:	b8 00 00 00 00       	mov    $0x0,%eax
  80051f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800522:	29 c1                	sub    %eax,%ecx
  800524:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800527:	89 75 08             	mov    %esi,0x8(%ebp)
  80052a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80052d:	eb 3d                	jmp    80056c <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80052f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800533:	74 19                	je     80054e <vprintfmt+0x20b>
  800535:	0f be c0             	movsbl %al,%eax
  800538:	83 e8 20             	sub    $0x20,%eax
  80053b:	83 f8 5e             	cmp    $0x5e,%eax
  80053e:	76 0e                	jbe    80054e <vprintfmt+0x20b>
					putch('?', putdat);
  800540:	83 ec 08             	sub    $0x8,%esp
  800543:	53                   	push   %ebx
  800544:	6a 3f                	push   $0x3f
  800546:	ff 55 08             	call   *0x8(%ebp)
  800549:	83 c4 10             	add    $0x10,%esp
  80054c:	eb 0b                	jmp    800559 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80054e:	83 ec 08             	sub    $0x8,%esp
  800551:	53                   	push   %ebx
  800552:	52                   	push   %edx
  800553:	ff 55 08             	call   *0x8(%ebp)
  800556:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800559:	ff 4d e4             	decl   -0x1c(%ebp)
  80055c:	eb 0e                	jmp    80056c <vprintfmt+0x229>
  80055e:	89 75 08             	mov    %esi,0x8(%ebp)
  800561:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800564:	eb 06                	jmp    80056c <vprintfmt+0x229>
  800566:	89 75 08             	mov    %esi,0x8(%ebp)
  800569:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80056c:	47                   	inc    %edi
  80056d:	8a 47 ff             	mov    -0x1(%edi),%al
  800570:	0f be d0             	movsbl %al,%edx
  800573:	85 d2                	test   %edx,%edx
  800575:	74 1d                	je     800594 <vprintfmt+0x251>
  800577:	85 f6                	test   %esi,%esi
  800579:	78 b4                	js     80052f <vprintfmt+0x1ec>
  80057b:	4e                   	dec    %esi
  80057c:	79 b1                	jns    80052f <vprintfmt+0x1ec>
  80057e:	8b 75 08             	mov    0x8(%ebp),%esi
  800581:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800584:	eb 14                	jmp    80059a <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800586:	83 ec 08             	sub    $0x8,%esp
  800589:	53                   	push   %ebx
  80058a:	6a 20                	push   $0x20
  80058c:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80058e:	4f                   	dec    %edi
  80058f:	83 c4 10             	add    $0x10,%esp
  800592:	eb 06                	jmp    80059a <vprintfmt+0x257>
  800594:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800597:	8b 75 08             	mov    0x8(%ebp),%esi
  80059a:	85 ff                	test   %edi,%edi
  80059c:	7f e8                	jg     800586 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005a1:	e9 c3 fd ff ff       	jmp    800369 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005a6:	83 fa 01             	cmp    $0x1,%edx
  8005a9:	7e 16                	jle    8005c1 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005ab:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ae:	8d 50 08             	lea    0x8(%eax),%edx
  8005b1:	89 55 14             	mov    %edx,0x14(%ebp)
  8005b4:	8b 50 04             	mov    0x4(%eax),%edx
  8005b7:	8b 00                	mov    (%eax),%eax
  8005b9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005bc:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005bf:	eb 32                	jmp    8005f3 <vprintfmt+0x2b0>
	else if (lflag)
  8005c1:	85 d2                	test   %edx,%edx
  8005c3:	74 18                	je     8005dd <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005c5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c8:	8d 50 04             	lea    0x4(%eax),%edx
  8005cb:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ce:	8b 00                	mov    (%eax),%eax
  8005d0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005d3:	89 c1                	mov    %eax,%ecx
  8005d5:	c1 f9 1f             	sar    $0x1f,%ecx
  8005d8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005db:	eb 16                	jmp    8005f3 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005dd:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e0:	8d 50 04             	lea    0x4(%eax),%edx
  8005e3:	89 55 14             	mov    %edx,0x14(%ebp)
  8005e6:	8b 00                	mov    (%eax),%eax
  8005e8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005eb:	89 c1                	mov    %eax,%ecx
  8005ed:	c1 f9 1f             	sar    $0x1f,%ecx
  8005f0:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005f3:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005f6:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005f9:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005fd:	79 76                	jns    800675 <vprintfmt+0x332>
				putch('-', putdat);
  8005ff:	83 ec 08             	sub    $0x8,%esp
  800602:	53                   	push   %ebx
  800603:	6a 2d                	push   $0x2d
  800605:	ff d6                	call   *%esi
				num = -(long long) num;
  800607:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80060a:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80060d:	f7 d8                	neg    %eax
  80060f:	83 d2 00             	adc    $0x0,%edx
  800612:	f7 da                	neg    %edx
  800614:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800617:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80061c:	eb 5c                	jmp    80067a <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80061e:	8d 45 14             	lea    0x14(%ebp),%eax
  800621:	e8 aa fc ff ff       	call   8002d0 <getuint>
			base = 10;
  800626:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80062b:	eb 4d                	jmp    80067a <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80062d:	8d 45 14             	lea    0x14(%ebp),%eax
  800630:	e8 9b fc ff ff       	call   8002d0 <getuint>
			base = 8;
  800635:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80063a:	eb 3e                	jmp    80067a <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80063c:	83 ec 08             	sub    $0x8,%esp
  80063f:	53                   	push   %ebx
  800640:	6a 30                	push   $0x30
  800642:	ff d6                	call   *%esi
			putch('x', putdat);
  800644:	83 c4 08             	add    $0x8,%esp
  800647:	53                   	push   %ebx
  800648:	6a 78                	push   $0x78
  80064a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80064c:	8b 45 14             	mov    0x14(%ebp),%eax
  80064f:	8d 50 04             	lea    0x4(%eax),%edx
  800652:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800655:	8b 00                	mov    (%eax),%eax
  800657:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80065c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80065f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800664:	eb 14                	jmp    80067a <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800666:	8d 45 14             	lea    0x14(%ebp),%eax
  800669:	e8 62 fc ff ff       	call   8002d0 <getuint>
			base = 16;
  80066e:	b9 10 00 00 00       	mov    $0x10,%ecx
  800673:	eb 05                	jmp    80067a <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800675:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80067a:	83 ec 0c             	sub    $0xc,%esp
  80067d:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800681:	57                   	push   %edi
  800682:	ff 75 e4             	pushl  -0x1c(%ebp)
  800685:	51                   	push   %ecx
  800686:	52                   	push   %edx
  800687:	50                   	push   %eax
  800688:	89 da                	mov    %ebx,%edx
  80068a:	89 f0                	mov    %esi,%eax
  80068c:	e8 92 fb ff ff       	call   800223 <printnum>
			break;
  800691:	83 c4 20             	add    $0x20,%esp
  800694:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800697:	e9 cd fc ff ff       	jmp    800369 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80069c:	83 ec 08             	sub    $0x8,%esp
  80069f:	53                   	push   %ebx
  8006a0:	51                   	push   %ecx
  8006a1:	ff d6                	call   *%esi
			break;
  8006a3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006a9:	e9 bb fc ff ff       	jmp    800369 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006ae:	83 ec 08             	sub    $0x8,%esp
  8006b1:	53                   	push   %ebx
  8006b2:	6a 25                	push   $0x25
  8006b4:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006b6:	83 c4 10             	add    $0x10,%esp
  8006b9:	eb 01                	jmp    8006bc <vprintfmt+0x379>
  8006bb:	4f                   	dec    %edi
  8006bc:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006c0:	75 f9                	jne    8006bb <vprintfmt+0x378>
  8006c2:	e9 a2 fc ff ff       	jmp    800369 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006c7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006ca:	5b                   	pop    %ebx
  8006cb:	5e                   	pop    %esi
  8006cc:	5f                   	pop    %edi
  8006cd:	5d                   	pop    %ebp
  8006ce:	c3                   	ret    

008006cf <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006cf:	55                   	push   %ebp
  8006d0:	89 e5                	mov    %esp,%ebp
  8006d2:	83 ec 18             	sub    $0x18,%esp
  8006d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d8:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006db:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006de:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006e2:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006e5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006ec:	85 c0                	test   %eax,%eax
  8006ee:	74 26                	je     800716 <vsnprintf+0x47>
  8006f0:	85 d2                	test   %edx,%edx
  8006f2:	7e 29                	jle    80071d <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006f4:	ff 75 14             	pushl  0x14(%ebp)
  8006f7:	ff 75 10             	pushl  0x10(%ebp)
  8006fa:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006fd:	50                   	push   %eax
  8006fe:	68 0a 03 80 00       	push   $0x80030a
  800703:	e8 3b fc ff ff       	call   800343 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800708:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80070b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80070e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800711:	83 c4 10             	add    $0x10,%esp
  800714:	eb 0c                	jmp    800722 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800716:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80071b:	eb 05                	jmp    800722 <vsnprintf+0x53>
  80071d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800722:	c9                   	leave  
  800723:	c3                   	ret    

00800724 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800724:	55                   	push   %ebp
  800725:	89 e5                	mov    %esp,%ebp
  800727:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80072a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80072d:	50                   	push   %eax
  80072e:	ff 75 10             	pushl  0x10(%ebp)
  800731:	ff 75 0c             	pushl  0xc(%ebp)
  800734:	ff 75 08             	pushl  0x8(%ebp)
  800737:	e8 93 ff ff ff       	call   8006cf <vsnprintf>
	va_end(ap);

	return rc;
}
  80073c:	c9                   	leave  
  80073d:	c3                   	ret    

0080073e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80073e:	55                   	push   %ebp
  80073f:	89 e5                	mov    %esp,%ebp
  800741:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800744:	b8 00 00 00 00       	mov    $0x0,%eax
  800749:	eb 01                	jmp    80074c <strlen+0xe>
		n++;
  80074b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80074c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800750:	75 f9                	jne    80074b <strlen+0xd>
		n++;
	return n;
}
  800752:	5d                   	pop    %ebp
  800753:	c3                   	ret    

00800754 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800754:	55                   	push   %ebp
  800755:	89 e5                	mov    %esp,%ebp
  800757:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80075a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80075d:	ba 00 00 00 00       	mov    $0x0,%edx
  800762:	eb 01                	jmp    800765 <strnlen+0x11>
		n++;
  800764:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800765:	39 c2                	cmp    %eax,%edx
  800767:	74 08                	je     800771 <strnlen+0x1d>
  800769:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80076d:	75 f5                	jne    800764 <strnlen+0x10>
  80076f:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800771:	5d                   	pop    %ebp
  800772:	c3                   	ret    

00800773 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800773:	55                   	push   %ebp
  800774:	89 e5                	mov    %esp,%ebp
  800776:	53                   	push   %ebx
  800777:	8b 45 08             	mov    0x8(%ebp),%eax
  80077a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80077d:	89 c2                	mov    %eax,%edx
  80077f:	42                   	inc    %edx
  800780:	41                   	inc    %ecx
  800781:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800784:	88 5a ff             	mov    %bl,-0x1(%edx)
  800787:	84 db                	test   %bl,%bl
  800789:	75 f4                	jne    80077f <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80078b:	5b                   	pop    %ebx
  80078c:	5d                   	pop    %ebp
  80078d:	c3                   	ret    

0080078e <strcat>:

char *
strcat(char *dst, const char *src)
{
  80078e:	55                   	push   %ebp
  80078f:	89 e5                	mov    %esp,%ebp
  800791:	53                   	push   %ebx
  800792:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800795:	53                   	push   %ebx
  800796:	e8 a3 ff ff ff       	call   80073e <strlen>
  80079b:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80079e:	ff 75 0c             	pushl  0xc(%ebp)
  8007a1:	01 d8                	add    %ebx,%eax
  8007a3:	50                   	push   %eax
  8007a4:	e8 ca ff ff ff       	call   800773 <strcpy>
	return dst;
}
  8007a9:	89 d8                	mov    %ebx,%eax
  8007ab:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007ae:	c9                   	leave  
  8007af:	c3                   	ret    

008007b0 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007b0:	55                   	push   %ebp
  8007b1:	89 e5                	mov    %esp,%ebp
  8007b3:	56                   	push   %esi
  8007b4:	53                   	push   %ebx
  8007b5:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007bb:	89 f3                	mov    %esi,%ebx
  8007bd:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007c0:	89 f2                	mov    %esi,%edx
  8007c2:	eb 0c                	jmp    8007d0 <strncpy+0x20>
		*dst++ = *src;
  8007c4:	42                   	inc    %edx
  8007c5:	8a 01                	mov    (%ecx),%al
  8007c7:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007ca:	80 39 01             	cmpb   $0x1,(%ecx)
  8007cd:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007d0:	39 da                	cmp    %ebx,%edx
  8007d2:	75 f0                	jne    8007c4 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007d4:	89 f0                	mov    %esi,%eax
  8007d6:	5b                   	pop    %ebx
  8007d7:	5e                   	pop    %esi
  8007d8:	5d                   	pop    %ebp
  8007d9:	c3                   	ret    

008007da <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007da:	55                   	push   %ebp
  8007db:	89 e5                	mov    %esp,%ebp
  8007dd:	56                   	push   %esi
  8007de:	53                   	push   %ebx
  8007df:	8b 75 08             	mov    0x8(%ebp),%esi
  8007e2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007e5:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007e8:	85 c0                	test   %eax,%eax
  8007ea:	74 1e                	je     80080a <strlcpy+0x30>
  8007ec:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007f0:	89 f2                	mov    %esi,%edx
  8007f2:	eb 05                	jmp    8007f9 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007f4:	42                   	inc    %edx
  8007f5:	41                   	inc    %ecx
  8007f6:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007f9:	39 c2                	cmp    %eax,%edx
  8007fb:	74 08                	je     800805 <strlcpy+0x2b>
  8007fd:	8a 19                	mov    (%ecx),%bl
  8007ff:	84 db                	test   %bl,%bl
  800801:	75 f1                	jne    8007f4 <strlcpy+0x1a>
  800803:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800805:	c6 00 00             	movb   $0x0,(%eax)
  800808:	eb 02                	jmp    80080c <strlcpy+0x32>
  80080a:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80080c:	29 f0                	sub    %esi,%eax
}
  80080e:	5b                   	pop    %ebx
  80080f:	5e                   	pop    %esi
  800810:	5d                   	pop    %ebp
  800811:	c3                   	ret    

00800812 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800812:	55                   	push   %ebp
  800813:	89 e5                	mov    %esp,%ebp
  800815:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800818:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80081b:	eb 02                	jmp    80081f <strcmp+0xd>
		p++, q++;
  80081d:	41                   	inc    %ecx
  80081e:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80081f:	8a 01                	mov    (%ecx),%al
  800821:	84 c0                	test   %al,%al
  800823:	74 04                	je     800829 <strcmp+0x17>
  800825:	3a 02                	cmp    (%edx),%al
  800827:	74 f4                	je     80081d <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800829:	0f b6 c0             	movzbl %al,%eax
  80082c:	0f b6 12             	movzbl (%edx),%edx
  80082f:	29 d0                	sub    %edx,%eax
}
  800831:	5d                   	pop    %ebp
  800832:	c3                   	ret    

00800833 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800833:	55                   	push   %ebp
  800834:	89 e5                	mov    %esp,%ebp
  800836:	53                   	push   %ebx
  800837:	8b 45 08             	mov    0x8(%ebp),%eax
  80083a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80083d:	89 c3                	mov    %eax,%ebx
  80083f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800842:	eb 02                	jmp    800846 <strncmp+0x13>
		n--, p++, q++;
  800844:	40                   	inc    %eax
  800845:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800846:	39 d8                	cmp    %ebx,%eax
  800848:	74 14                	je     80085e <strncmp+0x2b>
  80084a:	8a 08                	mov    (%eax),%cl
  80084c:	84 c9                	test   %cl,%cl
  80084e:	74 04                	je     800854 <strncmp+0x21>
  800850:	3a 0a                	cmp    (%edx),%cl
  800852:	74 f0                	je     800844 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800854:	0f b6 00             	movzbl (%eax),%eax
  800857:	0f b6 12             	movzbl (%edx),%edx
  80085a:	29 d0                	sub    %edx,%eax
  80085c:	eb 05                	jmp    800863 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80085e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800863:	5b                   	pop    %ebx
  800864:	5d                   	pop    %ebp
  800865:	c3                   	ret    

00800866 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800866:	55                   	push   %ebp
  800867:	89 e5                	mov    %esp,%ebp
  800869:	8b 45 08             	mov    0x8(%ebp),%eax
  80086c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80086f:	eb 05                	jmp    800876 <strchr+0x10>
		if (*s == c)
  800871:	38 ca                	cmp    %cl,%dl
  800873:	74 0c                	je     800881 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800875:	40                   	inc    %eax
  800876:	8a 10                	mov    (%eax),%dl
  800878:	84 d2                	test   %dl,%dl
  80087a:	75 f5                	jne    800871 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80087c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800881:	5d                   	pop    %ebp
  800882:	c3                   	ret    

00800883 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800883:	55                   	push   %ebp
  800884:	89 e5                	mov    %esp,%ebp
  800886:	8b 45 08             	mov    0x8(%ebp),%eax
  800889:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80088c:	eb 05                	jmp    800893 <strfind+0x10>
		if (*s == c)
  80088e:	38 ca                	cmp    %cl,%dl
  800890:	74 07                	je     800899 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800892:	40                   	inc    %eax
  800893:	8a 10                	mov    (%eax),%dl
  800895:	84 d2                	test   %dl,%dl
  800897:	75 f5                	jne    80088e <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800899:	5d                   	pop    %ebp
  80089a:	c3                   	ret    

0080089b <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80089b:	55                   	push   %ebp
  80089c:	89 e5                	mov    %esp,%ebp
  80089e:	57                   	push   %edi
  80089f:	56                   	push   %esi
  8008a0:	53                   	push   %ebx
  8008a1:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008a4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008a7:	85 c9                	test   %ecx,%ecx
  8008a9:	74 36                	je     8008e1 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008ab:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008b1:	75 28                	jne    8008db <memset+0x40>
  8008b3:	f6 c1 03             	test   $0x3,%cl
  8008b6:	75 23                	jne    8008db <memset+0x40>
		c &= 0xFF;
  8008b8:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008bc:	89 d3                	mov    %edx,%ebx
  8008be:	c1 e3 08             	shl    $0x8,%ebx
  8008c1:	89 d6                	mov    %edx,%esi
  8008c3:	c1 e6 18             	shl    $0x18,%esi
  8008c6:	89 d0                	mov    %edx,%eax
  8008c8:	c1 e0 10             	shl    $0x10,%eax
  8008cb:	09 f0                	or     %esi,%eax
  8008cd:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008cf:	89 d8                	mov    %ebx,%eax
  8008d1:	09 d0                	or     %edx,%eax
  8008d3:	c1 e9 02             	shr    $0x2,%ecx
  8008d6:	fc                   	cld    
  8008d7:	f3 ab                	rep stos %eax,%es:(%edi)
  8008d9:	eb 06                	jmp    8008e1 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008db:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008de:	fc                   	cld    
  8008df:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008e1:	89 f8                	mov    %edi,%eax
  8008e3:	5b                   	pop    %ebx
  8008e4:	5e                   	pop    %esi
  8008e5:	5f                   	pop    %edi
  8008e6:	5d                   	pop    %ebp
  8008e7:	c3                   	ret    

008008e8 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008e8:	55                   	push   %ebp
  8008e9:	89 e5                	mov    %esp,%ebp
  8008eb:	57                   	push   %edi
  8008ec:	56                   	push   %esi
  8008ed:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f0:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008f3:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008f6:	39 c6                	cmp    %eax,%esi
  8008f8:	73 33                	jae    80092d <memmove+0x45>
  8008fa:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008fd:	39 d0                	cmp    %edx,%eax
  8008ff:	73 2c                	jae    80092d <memmove+0x45>
		s += n;
		d += n;
  800901:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800904:	89 d6                	mov    %edx,%esi
  800906:	09 fe                	or     %edi,%esi
  800908:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80090e:	75 13                	jne    800923 <memmove+0x3b>
  800910:	f6 c1 03             	test   $0x3,%cl
  800913:	75 0e                	jne    800923 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800915:	83 ef 04             	sub    $0x4,%edi
  800918:	8d 72 fc             	lea    -0x4(%edx),%esi
  80091b:	c1 e9 02             	shr    $0x2,%ecx
  80091e:	fd                   	std    
  80091f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800921:	eb 07                	jmp    80092a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800923:	4f                   	dec    %edi
  800924:	8d 72 ff             	lea    -0x1(%edx),%esi
  800927:	fd                   	std    
  800928:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80092a:	fc                   	cld    
  80092b:	eb 1d                	jmp    80094a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80092d:	89 f2                	mov    %esi,%edx
  80092f:	09 c2                	or     %eax,%edx
  800931:	f6 c2 03             	test   $0x3,%dl
  800934:	75 0f                	jne    800945 <memmove+0x5d>
  800936:	f6 c1 03             	test   $0x3,%cl
  800939:	75 0a                	jne    800945 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80093b:	c1 e9 02             	shr    $0x2,%ecx
  80093e:	89 c7                	mov    %eax,%edi
  800940:	fc                   	cld    
  800941:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800943:	eb 05                	jmp    80094a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800945:	89 c7                	mov    %eax,%edi
  800947:	fc                   	cld    
  800948:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80094a:	5e                   	pop    %esi
  80094b:	5f                   	pop    %edi
  80094c:	5d                   	pop    %ebp
  80094d:	c3                   	ret    

0080094e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80094e:	55                   	push   %ebp
  80094f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800951:	ff 75 10             	pushl  0x10(%ebp)
  800954:	ff 75 0c             	pushl  0xc(%ebp)
  800957:	ff 75 08             	pushl  0x8(%ebp)
  80095a:	e8 89 ff ff ff       	call   8008e8 <memmove>
}
  80095f:	c9                   	leave  
  800960:	c3                   	ret    

00800961 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800961:	55                   	push   %ebp
  800962:	89 e5                	mov    %esp,%ebp
  800964:	56                   	push   %esi
  800965:	53                   	push   %ebx
  800966:	8b 45 08             	mov    0x8(%ebp),%eax
  800969:	8b 55 0c             	mov    0xc(%ebp),%edx
  80096c:	89 c6                	mov    %eax,%esi
  80096e:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800971:	eb 14                	jmp    800987 <memcmp+0x26>
		if (*s1 != *s2)
  800973:	8a 08                	mov    (%eax),%cl
  800975:	8a 1a                	mov    (%edx),%bl
  800977:	38 d9                	cmp    %bl,%cl
  800979:	74 0a                	je     800985 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80097b:	0f b6 c1             	movzbl %cl,%eax
  80097e:	0f b6 db             	movzbl %bl,%ebx
  800981:	29 d8                	sub    %ebx,%eax
  800983:	eb 0b                	jmp    800990 <memcmp+0x2f>
		s1++, s2++;
  800985:	40                   	inc    %eax
  800986:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800987:	39 f0                	cmp    %esi,%eax
  800989:	75 e8                	jne    800973 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80098b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800990:	5b                   	pop    %ebx
  800991:	5e                   	pop    %esi
  800992:	5d                   	pop    %ebp
  800993:	c3                   	ret    

00800994 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800994:	55                   	push   %ebp
  800995:	89 e5                	mov    %esp,%ebp
  800997:	53                   	push   %ebx
  800998:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80099b:	89 c1                	mov    %eax,%ecx
  80099d:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a0:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a4:	eb 08                	jmp    8009ae <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a6:	0f b6 10             	movzbl (%eax),%edx
  8009a9:	39 da                	cmp    %ebx,%edx
  8009ab:	74 05                	je     8009b2 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009ad:	40                   	inc    %eax
  8009ae:	39 c8                	cmp    %ecx,%eax
  8009b0:	72 f4                	jb     8009a6 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009b2:	5b                   	pop    %ebx
  8009b3:	5d                   	pop    %ebp
  8009b4:	c3                   	ret    

008009b5 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009b5:	55                   	push   %ebp
  8009b6:	89 e5                	mov    %esp,%ebp
  8009b8:	57                   	push   %edi
  8009b9:	56                   	push   %esi
  8009ba:	53                   	push   %ebx
  8009bb:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009be:	eb 01                	jmp    8009c1 <strtol+0xc>
		s++;
  8009c0:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009c1:	8a 01                	mov    (%ecx),%al
  8009c3:	3c 20                	cmp    $0x20,%al
  8009c5:	74 f9                	je     8009c0 <strtol+0xb>
  8009c7:	3c 09                	cmp    $0x9,%al
  8009c9:	74 f5                	je     8009c0 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009cb:	3c 2b                	cmp    $0x2b,%al
  8009cd:	75 08                	jne    8009d7 <strtol+0x22>
		s++;
  8009cf:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009d0:	bf 00 00 00 00       	mov    $0x0,%edi
  8009d5:	eb 11                	jmp    8009e8 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009d7:	3c 2d                	cmp    $0x2d,%al
  8009d9:	75 08                	jne    8009e3 <strtol+0x2e>
		s++, neg = 1;
  8009db:	41                   	inc    %ecx
  8009dc:	bf 01 00 00 00       	mov    $0x1,%edi
  8009e1:	eb 05                	jmp    8009e8 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009e3:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009e8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009ec:	0f 84 87 00 00 00    	je     800a79 <strtol+0xc4>
  8009f2:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009f6:	75 27                	jne    800a1f <strtol+0x6a>
  8009f8:	80 39 30             	cmpb   $0x30,(%ecx)
  8009fb:	75 22                	jne    800a1f <strtol+0x6a>
  8009fd:	e9 88 00 00 00       	jmp    800a8a <strtol+0xd5>
		s += 2, base = 16;
  800a02:	83 c1 02             	add    $0x2,%ecx
  800a05:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a0c:	eb 11                	jmp    800a1f <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a0e:	41                   	inc    %ecx
  800a0f:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a16:	eb 07                	jmp    800a1f <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a18:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a1f:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a24:	8a 11                	mov    (%ecx),%dl
  800a26:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a29:	80 fb 09             	cmp    $0x9,%bl
  800a2c:	77 08                	ja     800a36 <strtol+0x81>
			dig = *s - '0';
  800a2e:	0f be d2             	movsbl %dl,%edx
  800a31:	83 ea 30             	sub    $0x30,%edx
  800a34:	eb 22                	jmp    800a58 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a36:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a39:	89 f3                	mov    %esi,%ebx
  800a3b:	80 fb 19             	cmp    $0x19,%bl
  800a3e:	77 08                	ja     800a48 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a40:	0f be d2             	movsbl %dl,%edx
  800a43:	83 ea 57             	sub    $0x57,%edx
  800a46:	eb 10                	jmp    800a58 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a48:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a4b:	89 f3                	mov    %esi,%ebx
  800a4d:	80 fb 19             	cmp    $0x19,%bl
  800a50:	77 14                	ja     800a66 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a52:	0f be d2             	movsbl %dl,%edx
  800a55:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a58:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a5b:	7d 09                	jge    800a66 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a5d:	41                   	inc    %ecx
  800a5e:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a62:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a64:	eb be                	jmp    800a24 <strtol+0x6f>

	if (endptr)
  800a66:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a6a:	74 05                	je     800a71 <strtol+0xbc>
		*endptr = (char *) s;
  800a6c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a6f:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a71:	85 ff                	test   %edi,%edi
  800a73:	74 21                	je     800a96 <strtol+0xe1>
  800a75:	f7 d8                	neg    %eax
  800a77:	eb 1d                	jmp    800a96 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a79:	80 39 30             	cmpb   $0x30,(%ecx)
  800a7c:	75 9a                	jne    800a18 <strtol+0x63>
  800a7e:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a82:	0f 84 7a ff ff ff    	je     800a02 <strtol+0x4d>
  800a88:	eb 84                	jmp    800a0e <strtol+0x59>
  800a8a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a8e:	0f 84 6e ff ff ff    	je     800a02 <strtol+0x4d>
  800a94:	eb 89                	jmp    800a1f <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a96:	5b                   	pop    %ebx
  800a97:	5e                   	pop    %esi
  800a98:	5f                   	pop    %edi
  800a99:	5d                   	pop    %ebp
  800a9a:	c3                   	ret    

00800a9b <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a9b:	55                   	push   %ebp
  800a9c:	89 e5                	mov    %esp,%ebp
  800a9e:	57                   	push   %edi
  800a9f:	56                   	push   %esi
  800aa0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aa1:	b8 00 00 00 00       	mov    $0x0,%eax
  800aa6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800aa9:	8b 55 08             	mov    0x8(%ebp),%edx
  800aac:	89 c3                	mov    %eax,%ebx
  800aae:	89 c7                	mov    %eax,%edi
  800ab0:	89 c6                	mov    %eax,%esi
  800ab2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800ab4:	5b                   	pop    %ebx
  800ab5:	5e                   	pop    %esi
  800ab6:	5f                   	pop    %edi
  800ab7:	5d                   	pop    %ebp
  800ab8:	c3                   	ret    

00800ab9 <sys_cgetc>:

int
sys_cgetc(void)
{
  800ab9:	55                   	push   %ebp
  800aba:	89 e5                	mov    %esp,%ebp
  800abc:	57                   	push   %edi
  800abd:	56                   	push   %esi
  800abe:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800abf:	ba 00 00 00 00       	mov    $0x0,%edx
  800ac4:	b8 01 00 00 00       	mov    $0x1,%eax
  800ac9:	89 d1                	mov    %edx,%ecx
  800acb:	89 d3                	mov    %edx,%ebx
  800acd:	89 d7                	mov    %edx,%edi
  800acf:	89 d6                	mov    %edx,%esi
  800ad1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800ad3:	5b                   	pop    %ebx
  800ad4:	5e                   	pop    %esi
  800ad5:	5f                   	pop    %edi
  800ad6:	5d                   	pop    %ebp
  800ad7:	c3                   	ret    

00800ad8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800ad8:	55                   	push   %ebp
  800ad9:	89 e5                	mov    %esp,%ebp
  800adb:	57                   	push   %edi
  800adc:	56                   	push   %esi
  800add:	53                   	push   %ebx
  800ade:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae1:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ae6:	b8 03 00 00 00       	mov    $0x3,%eax
  800aeb:	8b 55 08             	mov    0x8(%ebp),%edx
  800aee:	89 cb                	mov    %ecx,%ebx
  800af0:	89 cf                	mov    %ecx,%edi
  800af2:	89 ce                	mov    %ecx,%esi
  800af4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800af6:	85 c0                	test   %eax,%eax
  800af8:	7e 17                	jle    800b11 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800afa:	83 ec 0c             	sub    $0xc,%esp
  800afd:	50                   	push   %eax
  800afe:	6a 03                	push   $0x3
  800b00:	68 e0 13 80 00       	push   $0x8013e0
  800b05:	6a 23                	push   $0x23
  800b07:	68 fd 13 80 00       	push   $0x8013fd
  800b0c:	e8 26 f6 ff ff       	call   800137 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b11:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b14:	5b                   	pop    %ebx
  800b15:	5e                   	pop    %esi
  800b16:	5f                   	pop    %edi
  800b17:	5d                   	pop    %ebp
  800b18:	c3                   	ret    

00800b19 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b19:	55                   	push   %ebp
  800b1a:	89 e5                	mov    %esp,%ebp
  800b1c:	57                   	push   %edi
  800b1d:	56                   	push   %esi
  800b1e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1f:	ba 00 00 00 00       	mov    $0x0,%edx
  800b24:	b8 02 00 00 00       	mov    $0x2,%eax
  800b29:	89 d1                	mov    %edx,%ecx
  800b2b:	89 d3                	mov    %edx,%ebx
  800b2d:	89 d7                	mov    %edx,%edi
  800b2f:	89 d6                	mov    %edx,%esi
  800b31:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b33:	5b                   	pop    %ebx
  800b34:	5e                   	pop    %esi
  800b35:	5f                   	pop    %edi
  800b36:	5d                   	pop    %ebp
  800b37:	c3                   	ret    

00800b38 <sys_yield>:

void
sys_yield(void)
{
  800b38:	55                   	push   %ebp
  800b39:	89 e5                	mov    %esp,%ebp
  800b3b:	57                   	push   %edi
  800b3c:	56                   	push   %esi
  800b3d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b3e:	ba 00 00 00 00       	mov    $0x0,%edx
  800b43:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b48:	89 d1                	mov    %edx,%ecx
  800b4a:	89 d3                	mov    %edx,%ebx
  800b4c:	89 d7                	mov    %edx,%edi
  800b4e:	89 d6                	mov    %edx,%esi
  800b50:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b52:	5b                   	pop    %ebx
  800b53:	5e                   	pop    %esi
  800b54:	5f                   	pop    %edi
  800b55:	5d                   	pop    %ebp
  800b56:	c3                   	ret    

00800b57 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b57:	55                   	push   %ebp
  800b58:	89 e5                	mov    %esp,%ebp
  800b5a:	57                   	push   %edi
  800b5b:	56                   	push   %esi
  800b5c:	53                   	push   %ebx
  800b5d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b60:	be 00 00 00 00       	mov    $0x0,%esi
  800b65:	b8 04 00 00 00       	mov    $0x4,%eax
  800b6a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b6d:	8b 55 08             	mov    0x8(%ebp),%edx
  800b70:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b73:	89 f7                	mov    %esi,%edi
  800b75:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b77:	85 c0                	test   %eax,%eax
  800b79:	7e 17                	jle    800b92 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b7b:	83 ec 0c             	sub    $0xc,%esp
  800b7e:	50                   	push   %eax
  800b7f:	6a 04                	push   $0x4
  800b81:	68 e0 13 80 00       	push   $0x8013e0
  800b86:	6a 23                	push   $0x23
  800b88:	68 fd 13 80 00       	push   $0x8013fd
  800b8d:	e8 a5 f5 ff ff       	call   800137 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b92:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b95:	5b                   	pop    %ebx
  800b96:	5e                   	pop    %esi
  800b97:	5f                   	pop    %edi
  800b98:	5d                   	pop    %ebp
  800b99:	c3                   	ret    

00800b9a <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b9a:	55                   	push   %ebp
  800b9b:	89 e5                	mov    %esp,%ebp
  800b9d:	57                   	push   %edi
  800b9e:	56                   	push   %esi
  800b9f:	53                   	push   %ebx
  800ba0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ba3:	b8 05 00 00 00       	mov    $0x5,%eax
  800ba8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bab:	8b 55 08             	mov    0x8(%ebp),%edx
  800bae:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800bb1:	8b 7d 14             	mov    0x14(%ebp),%edi
  800bb4:	8b 75 18             	mov    0x18(%ebp),%esi
  800bb7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bb9:	85 c0                	test   %eax,%eax
  800bbb:	7e 17                	jle    800bd4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bbd:	83 ec 0c             	sub    $0xc,%esp
  800bc0:	50                   	push   %eax
  800bc1:	6a 05                	push   $0x5
  800bc3:	68 e0 13 80 00       	push   $0x8013e0
  800bc8:	6a 23                	push   $0x23
  800bca:	68 fd 13 80 00       	push   $0x8013fd
  800bcf:	e8 63 f5 ff ff       	call   800137 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bd4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bd7:	5b                   	pop    %ebx
  800bd8:	5e                   	pop    %esi
  800bd9:	5f                   	pop    %edi
  800bda:	5d                   	pop    %ebp
  800bdb:	c3                   	ret    

00800bdc <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bdc:	55                   	push   %ebp
  800bdd:	89 e5                	mov    %esp,%ebp
  800bdf:	57                   	push   %edi
  800be0:	56                   	push   %esi
  800be1:	53                   	push   %ebx
  800be2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800be5:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bea:	b8 06 00 00 00       	mov    $0x6,%eax
  800bef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bf2:	8b 55 08             	mov    0x8(%ebp),%edx
  800bf5:	89 df                	mov    %ebx,%edi
  800bf7:	89 de                	mov    %ebx,%esi
  800bf9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bfb:	85 c0                	test   %eax,%eax
  800bfd:	7e 17                	jle    800c16 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bff:	83 ec 0c             	sub    $0xc,%esp
  800c02:	50                   	push   %eax
  800c03:	6a 06                	push   $0x6
  800c05:	68 e0 13 80 00       	push   $0x8013e0
  800c0a:	6a 23                	push   $0x23
  800c0c:	68 fd 13 80 00       	push   $0x8013fd
  800c11:	e8 21 f5 ff ff       	call   800137 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c16:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c19:	5b                   	pop    %ebx
  800c1a:	5e                   	pop    %esi
  800c1b:	5f                   	pop    %edi
  800c1c:	5d                   	pop    %ebp
  800c1d:	c3                   	ret    

00800c1e <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c1e:	55                   	push   %ebp
  800c1f:	89 e5                	mov    %esp,%ebp
  800c21:	57                   	push   %edi
  800c22:	56                   	push   %esi
  800c23:	53                   	push   %ebx
  800c24:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c27:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c2c:	b8 08 00 00 00       	mov    $0x8,%eax
  800c31:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c34:	8b 55 08             	mov    0x8(%ebp),%edx
  800c37:	89 df                	mov    %ebx,%edi
  800c39:	89 de                	mov    %ebx,%esi
  800c3b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c3d:	85 c0                	test   %eax,%eax
  800c3f:	7e 17                	jle    800c58 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c41:	83 ec 0c             	sub    $0xc,%esp
  800c44:	50                   	push   %eax
  800c45:	6a 08                	push   $0x8
  800c47:	68 e0 13 80 00       	push   $0x8013e0
  800c4c:	6a 23                	push   $0x23
  800c4e:	68 fd 13 80 00       	push   $0x8013fd
  800c53:	e8 df f4 ff ff       	call   800137 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c58:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c5b:	5b                   	pop    %ebx
  800c5c:	5e                   	pop    %esi
  800c5d:	5f                   	pop    %edi
  800c5e:	5d                   	pop    %ebp
  800c5f:	c3                   	ret    

00800c60 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c60:	55                   	push   %ebp
  800c61:	89 e5                	mov    %esp,%ebp
  800c63:	57                   	push   %edi
  800c64:	56                   	push   %esi
  800c65:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c66:	ba 00 00 00 00       	mov    $0x0,%edx
  800c6b:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c70:	89 d1                	mov    %edx,%ecx
  800c72:	89 d3                	mov    %edx,%ebx
  800c74:	89 d7                	mov    %edx,%edi
  800c76:	89 d6                	mov    %edx,%esi
  800c78:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c7a:	5b                   	pop    %ebx
  800c7b:	5e                   	pop    %esi
  800c7c:	5f                   	pop    %edi
  800c7d:	5d                   	pop    %ebp
  800c7e:	c3                   	ret    

00800c7f <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c7f:	55                   	push   %ebp
  800c80:	89 e5                	mov    %esp,%ebp
  800c82:	57                   	push   %edi
  800c83:	56                   	push   %esi
  800c84:	53                   	push   %ebx
  800c85:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c88:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c8d:	b8 09 00 00 00       	mov    $0x9,%eax
  800c92:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c95:	8b 55 08             	mov    0x8(%ebp),%edx
  800c98:	89 df                	mov    %ebx,%edi
  800c9a:	89 de                	mov    %ebx,%esi
  800c9c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c9e:	85 c0                	test   %eax,%eax
  800ca0:	7e 17                	jle    800cb9 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ca2:	83 ec 0c             	sub    $0xc,%esp
  800ca5:	50                   	push   %eax
  800ca6:	6a 09                	push   $0x9
  800ca8:	68 e0 13 80 00       	push   $0x8013e0
  800cad:	6a 23                	push   $0x23
  800caf:	68 fd 13 80 00       	push   $0x8013fd
  800cb4:	e8 7e f4 ff ff       	call   800137 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800cb9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cbc:	5b                   	pop    %ebx
  800cbd:	5e                   	pop    %esi
  800cbe:	5f                   	pop    %edi
  800cbf:	5d                   	pop    %ebp
  800cc0:	c3                   	ret    

00800cc1 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800cc1:	55                   	push   %ebp
  800cc2:	89 e5                	mov    %esp,%ebp
  800cc4:	57                   	push   %edi
  800cc5:	56                   	push   %esi
  800cc6:	53                   	push   %ebx
  800cc7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cca:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ccf:	b8 0a 00 00 00       	mov    $0xa,%eax
  800cd4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cd7:	8b 55 08             	mov    0x8(%ebp),%edx
  800cda:	89 df                	mov    %ebx,%edi
  800cdc:	89 de                	mov    %ebx,%esi
  800cde:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ce0:	85 c0                	test   %eax,%eax
  800ce2:	7e 17                	jle    800cfb <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ce4:	83 ec 0c             	sub    $0xc,%esp
  800ce7:	50                   	push   %eax
  800ce8:	6a 0a                	push   $0xa
  800cea:	68 e0 13 80 00       	push   $0x8013e0
  800cef:	6a 23                	push   $0x23
  800cf1:	68 fd 13 80 00       	push   $0x8013fd
  800cf6:	e8 3c f4 ff ff       	call   800137 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cfb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cfe:	5b                   	pop    %ebx
  800cff:	5e                   	pop    %esi
  800d00:	5f                   	pop    %edi
  800d01:	5d                   	pop    %ebp
  800d02:	c3                   	ret    

00800d03 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800d03:	55                   	push   %ebp
  800d04:	89 e5                	mov    %esp,%ebp
  800d06:	57                   	push   %edi
  800d07:	56                   	push   %esi
  800d08:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d09:	be 00 00 00 00       	mov    $0x0,%esi
  800d0e:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d13:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d16:	8b 55 08             	mov    0x8(%ebp),%edx
  800d19:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d1c:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d1f:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d21:	5b                   	pop    %ebx
  800d22:	5e                   	pop    %esi
  800d23:	5f                   	pop    %edi
  800d24:	5d                   	pop    %ebp
  800d25:	c3                   	ret    

00800d26 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d26:	55                   	push   %ebp
  800d27:	89 e5                	mov    %esp,%ebp
  800d29:	57                   	push   %edi
  800d2a:	56                   	push   %esi
  800d2b:	53                   	push   %ebx
  800d2c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d2f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d34:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d39:	8b 55 08             	mov    0x8(%ebp),%edx
  800d3c:	89 cb                	mov    %ecx,%ebx
  800d3e:	89 cf                	mov    %ecx,%edi
  800d40:	89 ce                	mov    %ecx,%esi
  800d42:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d44:	85 c0                	test   %eax,%eax
  800d46:	7e 17                	jle    800d5f <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d48:	83 ec 0c             	sub    $0xc,%esp
  800d4b:	50                   	push   %eax
  800d4c:	6a 0e                	push   $0xe
  800d4e:	68 e0 13 80 00       	push   $0x8013e0
  800d53:	6a 23                	push   $0x23
  800d55:	68 fd 13 80 00       	push   $0x8013fd
  800d5a:	e8 d8 f3 ff ff       	call   800137 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d5f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d62:	5b                   	pop    %ebx
  800d63:	5e                   	pop    %esi
  800d64:	5f                   	pop    %edi
  800d65:	5d                   	pop    %ebp
  800d66:	c3                   	ret    
  800d67:	90                   	nop

00800d68 <__udivdi3>:
  800d68:	55                   	push   %ebp
  800d69:	57                   	push   %edi
  800d6a:	56                   	push   %esi
  800d6b:	53                   	push   %ebx
  800d6c:	83 ec 1c             	sub    $0x1c,%esp
  800d6f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d73:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d77:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d7b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d7f:	89 ca                	mov    %ecx,%edx
  800d81:	89 f8                	mov    %edi,%eax
  800d83:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800d87:	85 f6                	test   %esi,%esi
  800d89:	75 2d                	jne    800db8 <__udivdi3+0x50>
  800d8b:	39 cf                	cmp    %ecx,%edi
  800d8d:	77 65                	ja     800df4 <__udivdi3+0x8c>
  800d8f:	89 fd                	mov    %edi,%ebp
  800d91:	85 ff                	test   %edi,%edi
  800d93:	75 0b                	jne    800da0 <__udivdi3+0x38>
  800d95:	b8 01 00 00 00       	mov    $0x1,%eax
  800d9a:	31 d2                	xor    %edx,%edx
  800d9c:	f7 f7                	div    %edi
  800d9e:	89 c5                	mov    %eax,%ebp
  800da0:	31 d2                	xor    %edx,%edx
  800da2:	89 c8                	mov    %ecx,%eax
  800da4:	f7 f5                	div    %ebp
  800da6:	89 c1                	mov    %eax,%ecx
  800da8:	89 d8                	mov    %ebx,%eax
  800daa:	f7 f5                	div    %ebp
  800dac:	89 cf                	mov    %ecx,%edi
  800dae:	89 fa                	mov    %edi,%edx
  800db0:	83 c4 1c             	add    $0x1c,%esp
  800db3:	5b                   	pop    %ebx
  800db4:	5e                   	pop    %esi
  800db5:	5f                   	pop    %edi
  800db6:	5d                   	pop    %ebp
  800db7:	c3                   	ret    
  800db8:	39 ce                	cmp    %ecx,%esi
  800dba:	77 28                	ja     800de4 <__udivdi3+0x7c>
  800dbc:	0f bd fe             	bsr    %esi,%edi
  800dbf:	83 f7 1f             	xor    $0x1f,%edi
  800dc2:	75 40                	jne    800e04 <__udivdi3+0x9c>
  800dc4:	39 ce                	cmp    %ecx,%esi
  800dc6:	72 0a                	jb     800dd2 <__udivdi3+0x6a>
  800dc8:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800dcc:	0f 87 9e 00 00 00    	ja     800e70 <__udivdi3+0x108>
  800dd2:	b8 01 00 00 00       	mov    $0x1,%eax
  800dd7:	89 fa                	mov    %edi,%edx
  800dd9:	83 c4 1c             	add    $0x1c,%esp
  800ddc:	5b                   	pop    %ebx
  800ddd:	5e                   	pop    %esi
  800dde:	5f                   	pop    %edi
  800ddf:	5d                   	pop    %ebp
  800de0:	c3                   	ret    
  800de1:	8d 76 00             	lea    0x0(%esi),%esi
  800de4:	31 ff                	xor    %edi,%edi
  800de6:	31 c0                	xor    %eax,%eax
  800de8:	89 fa                	mov    %edi,%edx
  800dea:	83 c4 1c             	add    $0x1c,%esp
  800ded:	5b                   	pop    %ebx
  800dee:	5e                   	pop    %esi
  800def:	5f                   	pop    %edi
  800df0:	5d                   	pop    %ebp
  800df1:	c3                   	ret    
  800df2:	66 90                	xchg   %ax,%ax
  800df4:	89 d8                	mov    %ebx,%eax
  800df6:	f7 f7                	div    %edi
  800df8:	31 ff                	xor    %edi,%edi
  800dfa:	89 fa                	mov    %edi,%edx
  800dfc:	83 c4 1c             	add    $0x1c,%esp
  800dff:	5b                   	pop    %ebx
  800e00:	5e                   	pop    %esi
  800e01:	5f                   	pop    %edi
  800e02:	5d                   	pop    %ebp
  800e03:	c3                   	ret    
  800e04:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e09:	89 eb                	mov    %ebp,%ebx
  800e0b:	29 fb                	sub    %edi,%ebx
  800e0d:	89 f9                	mov    %edi,%ecx
  800e0f:	d3 e6                	shl    %cl,%esi
  800e11:	89 c5                	mov    %eax,%ebp
  800e13:	88 d9                	mov    %bl,%cl
  800e15:	d3 ed                	shr    %cl,%ebp
  800e17:	89 e9                	mov    %ebp,%ecx
  800e19:	09 f1                	or     %esi,%ecx
  800e1b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800e1f:	89 f9                	mov    %edi,%ecx
  800e21:	d3 e0                	shl    %cl,%eax
  800e23:	89 c5                	mov    %eax,%ebp
  800e25:	89 d6                	mov    %edx,%esi
  800e27:	88 d9                	mov    %bl,%cl
  800e29:	d3 ee                	shr    %cl,%esi
  800e2b:	89 f9                	mov    %edi,%ecx
  800e2d:	d3 e2                	shl    %cl,%edx
  800e2f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e33:	88 d9                	mov    %bl,%cl
  800e35:	d3 e8                	shr    %cl,%eax
  800e37:	09 c2                	or     %eax,%edx
  800e39:	89 d0                	mov    %edx,%eax
  800e3b:	89 f2                	mov    %esi,%edx
  800e3d:	f7 74 24 0c          	divl   0xc(%esp)
  800e41:	89 d6                	mov    %edx,%esi
  800e43:	89 c3                	mov    %eax,%ebx
  800e45:	f7 e5                	mul    %ebp
  800e47:	39 d6                	cmp    %edx,%esi
  800e49:	72 19                	jb     800e64 <__udivdi3+0xfc>
  800e4b:	74 0b                	je     800e58 <__udivdi3+0xf0>
  800e4d:	89 d8                	mov    %ebx,%eax
  800e4f:	31 ff                	xor    %edi,%edi
  800e51:	e9 58 ff ff ff       	jmp    800dae <__udivdi3+0x46>
  800e56:	66 90                	xchg   %ax,%ax
  800e58:	8b 54 24 08          	mov    0x8(%esp),%edx
  800e5c:	89 f9                	mov    %edi,%ecx
  800e5e:	d3 e2                	shl    %cl,%edx
  800e60:	39 c2                	cmp    %eax,%edx
  800e62:	73 e9                	jae    800e4d <__udivdi3+0xe5>
  800e64:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800e67:	31 ff                	xor    %edi,%edi
  800e69:	e9 40 ff ff ff       	jmp    800dae <__udivdi3+0x46>
  800e6e:	66 90                	xchg   %ax,%ax
  800e70:	31 c0                	xor    %eax,%eax
  800e72:	e9 37 ff ff ff       	jmp    800dae <__udivdi3+0x46>
  800e77:	90                   	nop

00800e78 <__umoddi3>:
  800e78:	55                   	push   %ebp
  800e79:	57                   	push   %edi
  800e7a:	56                   	push   %esi
  800e7b:	53                   	push   %ebx
  800e7c:	83 ec 1c             	sub    $0x1c,%esp
  800e7f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800e83:	8b 74 24 34          	mov    0x34(%esp),%esi
  800e87:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800e8b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800e8f:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800e93:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800e97:	89 f3                	mov    %esi,%ebx
  800e99:	89 fa                	mov    %edi,%edx
  800e9b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e9f:	89 34 24             	mov    %esi,(%esp)
  800ea2:	85 c0                	test   %eax,%eax
  800ea4:	75 1a                	jne    800ec0 <__umoddi3+0x48>
  800ea6:	39 f7                	cmp    %esi,%edi
  800ea8:	0f 86 a2 00 00 00    	jbe    800f50 <__umoddi3+0xd8>
  800eae:	89 c8                	mov    %ecx,%eax
  800eb0:	89 f2                	mov    %esi,%edx
  800eb2:	f7 f7                	div    %edi
  800eb4:	89 d0                	mov    %edx,%eax
  800eb6:	31 d2                	xor    %edx,%edx
  800eb8:	83 c4 1c             	add    $0x1c,%esp
  800ebb:	5b                   	pop    %ebx
  800ebc:	5e                   	pop    %esi
  800ebd:	5f                   	pop    %edi
  800ebe:	5d                   	pop    %ebp
  800ebf:	c3                   	ret    
  800ec0:	39 f0                	cmp    %esi,%eax
  800ec2:	0f 87 ac 00 00 00    	ja     800f74 <__umoddi3+0xfc>
  800ec8:	0f bd e8             	bsr    %eax,%ebp
  800ecb:	83 f5 1f             	xor    $0x1f,%ebp
  800ece:	0f 84 ac 00 00 00    	je     800f80 <__umoddi3+0x108>
  800ed4:	bf 20 00 00 00       	mov    $0x20,%edi
  800ed9:	29 ef                	sub    %ebp,%edi
  800edb:	89 fe                	mov    %edi,%esi
  800edd:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800ee1:	89 e9                	mov    %ebp,%ecx
  800ee3:	d3 e0                	shl    %cl,%eax
  800ee5:	89 d7                	mov    %edx,%edi
  800ee7:	89 f1                	mov    %esi,%ecx
  800ee9:	d3 ef                	shr    %cl,%edi
  800eeb:	09 c7                	or     %eax,%edi
  800eed:	89 e9                	mov    %ebp,%ecx
  800eef:	d3 e2                	shl    %cl,%edx
  800ef1:	89 14 24             	mov    %edx,(%esp)
  800ef4:	89 d8                	mov    %ebx,%eax
  800ef6:	d3 e0                	shl    %cl,%eax
  800ef8:	89 c2                	mov    %eax,%edx
  800efa:	8b 44 24 08          	mov    0x8(%esp),%eax
  800efe:	d3 e0                	shl    %cl,%eax
  800f00:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f04:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f08:	89 f1                	mov    %esi,%ecx
  800f0a:	d3 e8                	shr    %cl,%eax
  800f0c:	09 d0                	or     %edx,%eax
  800f0e:	d3 eb                	shr    %cl,%ebx
  800f10:	89 da                	mov    %ebx,%edx
  800f12:	f7 f7                	div    %edi
  800f14:	89 d3                	mov    %edx,%ebx
  800f16:	f7 24 24             	mull   (%esp)
  800f19:	89 c6                	mov    %eax,%esi
  800f1b:	89 d1                	mov    %edx,%ecx
  800f1d:	39 d3                	cmp    %edx,%ebx
  800f1f:	0f 82 87 00 00 00    	jb     800fac <__umoddi3+0x134>
  800f25:	0f 84 91 00 00 00    	je     800fbc <__umoddi3+0x144>
  800f2b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800f2f:	29 f2                	sub    %esi,%edx
  800f31:	19 cb                	sbb    %ecx,%ebx
  800f33:	89 d8                	mov    %ebx,%eax
  800f35:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800f39:	d3 e0                	shl    %cl,%eax
  800f3b:	89 e9                	mov    %ebp,%ecx
  800f3d:	d3 ea                	shr    %cl,%edx
  800f3f:	09 d0                	or     %edx,%eax
  800f41:	89 e9                	mov    %ebp,%ecx
  800f43:	d3 eb                	shr    %cl,%ebx
  800f45:	89 da                	mov    %ebx,%edx
  800f47:	83 c4 1c             	add    $0x1c,%esp
  800f4a:	5b                   	pop    %ebx
  800f4b:	5e                   	pop    %esi
  800f4c:	5f                   	pop    %edi
  800f4d:	5d                   	pop    %ebp
  800f4e:	c3                   	ret    
  800f4f:	90                   	nop
  800f50:	89 fd                	mov    %edi,%ebp
  800f52:	85 ff                	test   %edi,%edi
  800f54:	75 0b                	jne    800f61 <__umoddi3+0xe9>
  800f56:	b8 01 00 00 00       	mov    $0x1,%eax
  800f5b:	31 d2                	xor    %edx,%edx
  800f5d:	f7 f7                	div    %edi
  800f5f:	89 c5                	mov    %eax,%ebp
  800f61:	89 f0                	mov    %esi,%eax
  800f63:	31 d2                	xor    %edx,%edx
  800f65:	f7 f5                	div    %ebp
  800f67:	89 c8                	mov    %ecx,%eax
  800f69:	f7 f5                	div    %ebp
  800f6b:	89 d0                	mov    %edx,%eax
  800f6d:	e9 44 ff ff ff       	jmp    800eb6 <__umoddi3+0x3e>
  800f72:	66 90                	xchg   %ax,%ax
  800f74:	89 c8                	mov    %ecx,%eax
  800f76:	89 f2                	mov    %esi,%edx
  800f78:	83 c4 1c             	add    $0x1c,%esp
  800f7b:	5b                   	pop    %ebx
  800f7c:	5e                   	pop    %esi
  800f7d:	5f                   	pop    %edi
  800f7e:	5d                   	pop    %ebp
  800f7f:	c3                   	ret    
  800f80:	3b 04 24             	cmp    (%esp),%eax
  800f83:	72 06                	jb     800f8b <__umoddi3+0x113>
  800f85:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800f89:	77 0f                	ja     800f9a <__umoddi3+0x122>
  800f8b:	89 f2                	mov    %esi,%edx
  800f8d:	29 f9                	sub    %edi,%ecx
  800f8f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800f93:	89 14 24             	mov    %edx,(%esp)
  800f96:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800f9a:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f9e:	8b 14 24             	mov    (%esp),%edx
  800fa1:	83 c4 1c             	add    $0x1c,%esp
  800fa4:	5b                   	pop    %ebx
  800fa5:	5e                   	pop    %esi
  800fa6:	5f                   	pop    %edi
  800fa7:	5d                   	pop    %ebp
  800fa8:	c3                   	ret    
  800fa9:	8d 76 00             	lea    0x0(%esi),%esi
  800fac:	2b 04 24             	sub    (%esp),%eax
  800faf:	19 fa                	sbb    %edi,%edx
  800fb1:	89 d1                	mov    %edx,%ecx
  800fb3:	89 c6                	mov    %eax,%esi
  800fb5:	e9 71 ff ff ff       	jmp    800f2b <__umoddi3+0xb3>
  800fba:	66 90                	xchg   %ax,%ax
  800fbc:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800fc0:	72 ea                	jb     800fac <__umoddi3+0x134>
  800fc2:	89 d9                	mov    %ebx,%ecx
  800fc4:	e9 62 ff ff ff       	jmp    800f2b <__umoddi3+0xb3>
