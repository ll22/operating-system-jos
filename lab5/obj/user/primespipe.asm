
obj/user/primespipe.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 0a 02 00 00       	call   80023b <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <primeproc>:

#include <inc/lib.h>

unsigned
primeproc(int fd)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 1c             	sub    $0x1c,%esp
  80003c:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int i, id, p, pfd[2], wfd, r;

	// fetch a prime from our left neighbor
top:
	if ((r = readn(fd, &p, 4)) != 4)
  80003f:	8d 75 e0             	lea    -0x20(%ebp),%esi
		panic("primeproc could not read initial prime: %d, %e", r, r >= 0 ? 0 : r);

	cprintf("%d\n", p);

	// fork a right neighbor to continue the chain
	if ((i=pipe(pfd)) < 0)
  800042:	8d 7d d8             	lea    -0x28(%ebp),%edi
{
	int i, id, p, pfd[2], wfd, r;

	// fetch a prime from our left neighbor
top:
	if ((r = readn(fd, &p, 4)) != 4)
  800045:	83 ec 04             	sub    $0x4,%esp
  800048:	6a 04                	push   $0x4
  80004a:	56                   	push   %esi
  80004b:	53                   	push   %ebx
  80004c:	e8 b4 14 00 00       	call   801505 <readn>
  800051:	83 c4 10             	add    $0x10,%esp
  800054:	83 f8 04             	cmp    $0x4,%eax
  800057:	74 21                	je     80007a <primeproc+0x47>
		panic("primeproc could not read initial prime: %d, %e", r, r >= 0 ? 0 : r);
  800059:	83 ec 0c             	sub    $0xc,%esp
  80005c:	89 c2                	mov    %eax,%edx
  80005e:	85 c0                	test   %eax,%eax
  800060:	7e 05                	jle    800067 <primeproc+0x34>
  800062:	ba 00 00 00 00       	mov    $0x0,%edx
  800067:	52                   	push   %edx
  800068:	50                   	push   %eax
  800069:	68 80 22 80 00       	push   $0x802280
  80006e:	6a 15                	push   $0x15
  800070:	68 af 22 80 00       	push   $0x8022af
  800075:	e8 22 02 00 00       	call   80029c <_panic>

	cprintf("%d\n", p);
  80007a:	83 ec 08             	sub    $0x8,%esp
  80007d:	ff 75 e0             	pushl  -0x20(%ebp)
  800080:	68 c1 22 80 00       	push   $0x8022c1
  800085:	e8 ea 02 00 00       	call   800374 <cprintf>

	// fork a right neighbor to continue the chain
	if ((i=pipe(pfd)) < 0)
  80008a:	89 3c 24             	mov    %edi,(%esp)
  80008d:	e8 b4 1a 00 00       	call   801b46 <pipe>
  800092:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800095:	83 c4 10             	add    $0x10,%esp
  800098:	85 c0                	test   %eax,%eax
  80009a:	79 12                	jns    8000ae <primeproc+0x7b>
		panic("pipe: %e", i);
  80009c:	50                   	push   %eax
  80009d:	68 c5 22 80 00       	push   $0x8022c5
  8000a2:	6a 1b                	push   $0x1b
  8000a4:	68 af 22 80 00       	push   $0x8022af
  8000a9:	e8 ee 01 00 00       	call   80029c <_panic>
	if ((id = fork()) < 0)
  8000ae:	e8 cb 0e 00 00       	call   800f7e <fork>
  8000b3:	85 c0                	test   %eax,%eax
  8000b5:	79 12                	jns    8000c9 <primeproc+0x96>
		panic("fork: %e", id);
  8000b7:	50                   	push   %eax
  8000b8:	68 ce 22 80 00       	push   $0x8022ce
  8000bd:	6a 1d                	push   $0x1d
  8000bf:	68 af 22 80 00       	push   $0x8022af
  8000c4:	e8 d3 01 00 00       	call   80029c <_panic>
	if (id == 0) {
  8000c9:	85 c0                	test   %eax,%eax
  8000cb:	75 1f                	jne    8000ec <primeproc+0xb9>
		close(fd);
  8000cd:	83 ec 0c             	sub    $0xc,%esp
  8000d0:	53                   	push   %ebx
  8000d1:	e8 70 12 00 00       	call   801346 <close>
		close(pfd[1]);
  8000d6:	83 c4 04             	add    $0x4,%esp
  8000d9:	ff 75 dc             	pushl  -0x24(%ebp)
  8000dc:	e8 65 12 00 00       	call   801346 <close>
		fd = pfd[0];
  8000e1:	8b 5d d8             	mov    -0x28(%ebp),%ebx
		goto top;
  8000e4:	83 c4 10             	add    $0x10,%esp
  8000e7:	e9 59 ff ff ff       	jmp    800045 <primeproc+0x12>
	}

	close(pfd[0]);
  8000ec:	83 ec 0c             	sub    $0xc,%esp
  8000ef:	ff 75 d8             	pushl  -0x28(%ebp)
  8000f2:	e8 4f 12 00 00       	call   801346 <close>
	wfd = pfd[1];
  8000f7:	8b 7d dc             	mov    -0x24(%ebp),%edi
  8000fa:	83 c4 10             	add    $0x10,%esp

	// filter out multiples of our prime
	for (;;) {
		if ((r=readn(fd, &i, 4)) != 4)
  8000fd:	8d 75 e4             	lea    -0x1c(%ebp),%esi
  800100:	83 ec 04             	sub    $0x4,%esp
  800103:	6a 04                	push   $0x4
  800105:	56                   	push   %esi
  800106:	53                   	push   %ebx
  800107:	e8 f9 13 00 00       	call   801505 <readn>
  80010c:	83 c4 10             	add    $0x10,%esp
  80010f:	83 f8 04             	cmp    $0x4,%eax
  800112:	74 25                	je     800139 <primeproc+0x106>
			panic("primeproc %d readn %d %d %e", p, fd, r, r >= 0 ? 0 : r);
  800114:	83 ec 04             	sub    $0x4,%esp
  800117:	89 c2                	mov    %eax,%edx
  800119:	85 c0                	test   %eax,%eax
  80011b:	7e 05                	jle    800122 <primeproc+0xef>
  80011d:	ba 00 00 00 00       	mov    $0x0,%edx
  800122:	52                   	push   %edx
  800123:	50                   	push   %eax
  800124:	53                   	push   %ebx
  800125:	ff 75 e0             	pushl  -0x20(%ebp)
  800128:	68 d7 22 80 00       	push   $0x8022d7
  80012d:	6a 2b                	push   $0x2b
  80012f:	68 af 22 80 00       	push   $0x8022af
  800134:	e8 63 01 00 00       	call   80029c <_panic>
		if (i%p)
  800139:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  80013c:	99                   	cltd   
  80013d:	f7 7d e0             	idivl  -0x20(%ebp)
  800140:	85 d2                	test   %edx,%edx
  800142:	74 bc                	je     800100 <primeproc+0xcd>
			if ((r=write(wfd, &i, 4)) != 4)
  800144:	83 ec 04             	sub    $0x4,%esp
  800147:	6a 04                	push   $0x4
  800149:	56                   	push   %esi
  80014a:	57                   	push   %edi
  80014b:	e8 fe 13 00 00       	call   80154e <write>
  800150:	83 c4 10             	add    $0x10,%esp
  800153:	83 f8 04             	cmp    $0x4,%eax
  800156:	74 a8                	je     800100 <primeproc+0xcd>
				panic("primeproc %d write: %d %e", p, r, r >= 0 ? 0 : r);
  800158:	83 ec 08             	sub    $0x8,%esp
  80015b:	89 c2                	mov    %eax,%edx
  80015d:	85 c0                	test   %eax,%eax
  80015f:	7e 05                	jle    800166 <primeproc+0x133>
  800161:	ba 00 00 00 00       	mov    $0x0,%edx
  800166:	52                   	push   %edx
  800167:	50                   	push   %eax
  800168:	ff 75 e0             	pushl  -0x20(%ebp)
  80016b:	68 f3 22 80 00       	push   $0x8022f3
  800170:	6a 2e                	push   $0x2e
  800172:	68 af 22 80 00       	push   $0x8022af
  800177:	e8 20 01 00 00       	call   80029c <_panic>

0080017c <umain>:
	}
}

void
umain(int argc, char **argv)
{
  80017c:	55                   	push   %ebp
  80017d:	89 e5                	mov    %esp,%ebp
  80017f:	53                   	push   %ebx
  800180:	83 ec 20             	sub    $0x20,%esp
	int i, id, p[2], r;

	binaryname = "primespipe";
  800183:	c7 05 00 30 80 00 0d 	movl   $0x80230d,0x803000
  80018a:	23 80 00 

	if ((i=pipe(p)) < 0)
  80018d:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800190:	50                   	push   %eax
  800191:	e8 b0 19 00 00       	call   801b46 <pipe>
  800196:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800199:	83 c4 10             	add    $0x10,%esp
  80019c:	85 c0                	test   %eax,%eax
  80019e:	79 12                	jns    8001b2 <umain+0x36>
		panic("pipe: %e", i);
  8001a0:	50                   	push   %eax
  8001a1:	68 c5 22 80 00       	push   $0x8022c5
  8001a6:	6a 3a                	push   $0x3a
  8001a8:	68 af 22 80 00       	push   $0x8022af
  8001ad:	e8 ea 00 00 00       	call   80029c <_panic>

	// fork the first prime process in the chain
	if ((id=fork()) < 0)
  8001b2:	e8 c7 0d 00 00       	call   800f7e <fork>
  8001b7:	85 c0                	test   %eax,%eax
  8001b9:	79 12                	jns    8001cd <umain+0x51>
		panic("fork: %e", id);
  8001bb:	50                   	push   %eax
  8001bc:	68 ce 22 80 00       	push   $0x8022ce
  8001c1:	6a 3e                	push   $0x3e
  8001c3:	68 af 22 80 00       	push   $0x8022af
  8001c8:	e8 cf 00 00 00       	call   80029c <_panic>

	if (id == 0) {
  8001cd:	85 c0                	test   %eax,%eax
  8001cf:	75 16                	jne    8001e7 <umain+0x6b>
		close(p[1]);
  8001d1:	83 ec 0c             	sub    $0xc,%esp
  8001d4:	ff 75 f0             	pushl  -0x10(%ebp)
  8001d7:	e8 6a 11 00 00       	call   801346 <close>
		primeproc(p[0]);
  8001dc:	83 c4 04             	add    $0x4,%esp
  8001df:	ff 75 ec             	pushl  -0x14(%ebp)
  8001e2:	e8 4c fe ff ff       	call   800033 <primeproc>
	}

	close(p[0]);
  8001e7:	83 ec 0c             	sub    $0xc,%esp
  8001ea:	ff 75 ec             	pushl  -0x14(%ebp)
  8001ed:	e8 54 11 00 00       	call   801346 <close>

	// feed all the integers through
	for (i=2;; i++)
  8001f2:	c7 45 f4 02 00 00 00 	movl   $0x2,-0xc(%ebp)
  8001f9:	83 c4 10             	add    $0x10,%esp
		if ((r=write(p[1], &i, 4)) != 4)
  8001fc:	8d 5d f4             	lea    -0xc(%ebp),%ebx
  8001ff:	83 ec 04             	sub    $0x4,%esp
  800202:	6a 04                	push   $0x4
  800204:	53                   	push   %ebx
  800205:	ff 75 f0             	pushl  -0x10(%ebp)
  800208:	e8 41 13 00 00       	call   80154e <write>
  80020d:	83 c4 10             	add    $0x10,%esp
  800210:	83 f8 04             	cmp    $0x4,%eax
  800213:	74 21                	je     800236 <umain+0xba>
			panic("generator write: %d, %e", r, r >= 0 ? 0 : r);
  800215:	83 ec 0c             	sub    $0xc,%esp
  800218:	89 c2                	mov    %eax,%edx
  80021a:	85 c0                	test   %eax,%eax
  80021c:	7e 05                	jle    800223 <umain+0xa7>
  80021e:	ba 00 00 00 00       	mov    $0x0,%edx
  800223:	52                   	push   %edx
  800224:	50                   	push   %eax
  800225:	68 18 23 80 00       	push   $0x802318
  80022a:	6a 4a                	push   $0x4a
  80022c:	68 af 22 80 00       	push   $0x8022af
  800231:	e8 66 00 00 00       	call   80029c <_panic>
	}

	close(p[0]);

	// feed all the integers through
	for (i=2;; i++)
  800236:	ff 45 f4             	incl   -0xc(%ebp)
		if ((r=write(p[1], &i, 4)) != 4)
			panic("generator write: %d, %e", r, r >= 0 ? 0 : r);
}
  800239:	eb c4                	jmp    8001ff <umain+0x83>

0080023b <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80023b:	55                   	push   %ebp
  80023c:	89 e5                	mov    %esp,%ebp
  80023e:	56                   	push   %esi
  80023f:	53                   	push   %ebx
  800240:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800243:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800246:	e8 33 0a 00 00       	call   800c7e <sys_getenvid>
  80024b:	25 ff 03 00 00       	and    $0x3ff,%eax
  800250:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800257:	c1 e0 07             	shl    $0x7,%eax
  80025a:	29 d0                	sub    %edx,%eax
  80025c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800261:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800266:	85 db                	test   %ebx,%ebx
  800268:	7e 07                	jle    800271 <libmain+0x36>
		binaryname = argv[0];
  80026a:	8b 06                	mov    (%esi),%eax
  80026c:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  800271:	83 ec 08             	sub    $0x8,%esp
  800274:	56                   	push   %esi
  800275:	53                   	push   %ebx
  800276:	e8 01 ff ff ff       	call   80017c <umain>

	// exit gracefully
	exit();
  80027b:	e8 0a 00 00 00       	call   80028a <exit>
}
  800280:	83 c4 10             	add    $0x10,%esp
  800283:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800286:	5b                   	pop    %ebx
  800287:	5e                   	pop    %esi
  800288:	5d                   	pop    %ebp
  800289:	c3                   	ret    

0080028a <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80028a:	55                   	push   %ebp
  80028b:	89 e5                	mov    %esp,%ebp
  80028d:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800290:	6a 00                	push   $0x0
  800292:	e8 a6 09 00 00       	call   800c3d <sys_env_destroy>
}
  800297:	83 c4 10             	add    $0x10,%esp
  80029a:	c9                   	leave  
  80029b:	c3                   	ret    

0080029c <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80029c:	55                   	push   %ebp
  80029d:	89 e5                	mov    %esp,%ebp
  80029f:	56                   	push   %esi
  8002a0:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8002a1:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8002a4:	8b 35 00 30 80 00    	mov    0x803000,%esi
  8002aa:	e8 cf 09 00 00       	call   800c7e <sys_getenvid>
  8002af:	83 ec 0c             	sub    $0xc,%esp
  8002b2:	ff 75 0c             	pushl  0xc(%ebp)
  8002b5:	ff 75 08             	pushl  0x8(%ebp)
  8002b8:	56                   	push   %esi
  8002b9:	50                   	push   %eax
  8002ba:	68 3c 23 80 00       	push   $0x80233c
  8002bf:	e8 b0 00 00 00       	call   800374 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8002c4:	83 c4 18             	add    $0x18,%esp
  8002c7:	53                   	push   %ebx
  8002c8:	ff 75 10             	pushl  0x10(%ebp)
  8002cb:	e8 53 00 00 00       	call   800323 <vcprintf>
	cprintf("\n");
  8002d0:	c7 04 24 c3 22 80 00 	movl   $0x8022c3,(%esp)
  8002d7:	e8 98 00 00 00       	call   800374 <cprintf>
  8002dc:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8002df:	cc                   	int3   
  8002e0:	eb fd                	jmp    8002df <_panic+0x43>

008002e2 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8002e2:	55                   	push   %ebp
  8002e3:	89 e5                	mov    %esp,%ebp
  8002e5:	53                   	push   %ebx
  8002e6:	83 ec 04             	sub    $0x4,%esp
  8002e9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8002ec:	8b 13                	mov    (%ebx),%edx
  8002ee:	8d 42 01             	lea    0x1(%edx),%eax
  8002f1:	89 03                	mov    %eax,(%ebx)
  8002f3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8002f6:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8002fa:	3d ff 00 00 00       	cmp    $0xff,%eax
  8002ff:	75 1a                	jne    80031b <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800301:	83 ec 08             	sub    $0x8,%esp
  800304:	68 ff 00 00 00       	push   $0xff
  800309:	8d 43 08             	lea    0x8(%ebx),%eax
  80030c:	50                   	push   %eax
  80030d:	e8 ee 08 00 00       	call   800c00 <sys_cputs>
		b->idx = 0;
  800312:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800318:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80031b:	ff 43 04             	incl   0x4(%ebx)
}
  80031e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800321:	c9                   	leave  
  800322:	c3                   	ret    

00800323 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800323:	55                   	push   %ebp
  800324:	89 e5                	mov    %esp,%ebp
  800326:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80032c:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800333:	00 00 00 
	b.cnt = 0;
  800336:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80033d:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800340:	ff 75 0c             	pushl  0xc(%ebp)
  800343:	ff 75 08             	pushl  0x8(%ebp)
  800346:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80034c:	50                   	push   %eax
  80034d:	68 e2 02 80 00       	push   $0x8002e2
  800352:	e8 51 01 00 00       	call   8004a8 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800357:	83 c4 08             	add    $0x8,%esp
  80035a:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800360:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800366:	50                   	push   %eax
  800367:	e8 94 08 00 00       	call   800c00 <sys_cputs>

	return b.cnt;
}
  80036c:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800372:	c9                   	leave  
  800373:	c3                   	ret    

00800374 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800374:	55                   	push   %ebp
  800375:	89 e5                	mov    %esp,%ebp
  800377:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80037a:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80037d:	50                   	push   %eax
  80037e:	ff 75 08             	pushl  0x8(%ebp)
  800381:	e8 9d ff ff ff       	call   800323 <vcprintf>
	va_end(ap);

	return cnt;
}
  800386:	c9                   	leave  
  800387:	c3                   	ret    

00800388 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800388:	55                   	push   %ebp
  800389:	89 e5                	mov    %esp,%ebp
  80038b:	57                   	push   %edi
  80038c:	56                   	push   %esi
  80038d:	53                   	push   %ebx
  80038e:	83 ec 1c             	sub    $0x1c,%esp
  800391:	89 c7                	mov    %eax,%edi
  800393:	89 d6                	mov    %edx,%esi
  800395:	8b 45 08             	mov    0x8(%ebp),%eax
  800398:	8b 55 0c             	mov    0xc(%ebp),%edx
  80039b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80039e:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8003a1:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8003a4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8003a9:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8003ac:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8003af:	39 d3                	cmp    %edx,%ebx
  8003b1:	72 05                	jb     8003b8 <printnum+0x30>
  8003b3:	39 45 10             	cmp    %eax,0x10(%ebp)
  8003b6:	77 45                	ja     8003fd <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8003b8:	83 ec 0c             	sub    $0xc,%esp
  8003bb:	ff 75 18             	pushl  0x18(%ebp)
  8003be:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c1:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8003c4:	53                   	push   %ebx
  8003c5:	ff 75 10             	pushl  0x10(%ebp)
  8003c8:	83 ec 08             	sub    $0x8,%esp
  8003cb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8003ce:	ff 75 e0             	pushl  -0x20(%ebp)
  8003d1:	ff 75 dc             	pushl  -0x24(%ebp)
  8003d4:	ff 75 d8             	pushl  -0x28(%ebp)
  8003d7:	e8 2c 1c 00 00       	call   802008 <__udivdi3>
  8003dc:	83 c4 18             	add    $0x18,%esp
  8003df:	52                   	push   %edx
  8003e0:	50                   	push   %eax
  8003e1:	89 f2                	mov    %esi,%edx
  8003e3:	89 f8                	mov    %edi,%eax
  8003e5:	e8 9e ff ff ff       	call   800388 <printnum>
  8003ea:	83 c4 20             	add    $0x20,%esp
  8003ed:	eb 16                	jmp    800405 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8003ef:	83 ec 08             	sub    $0x8,%esp
  8003f2:	56                   	push   %esi
  8003f3:	ff 75 18             	pushl  0x18(%ebp)
  8003f6:	ff d7                	call   *%edi
  8003f8:	83 c4 10             	add    $0x10,%esp
  8003fb:	eb 03                	jmp    800400 <printnum+0x78>
  8003fd:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800400:	4b                   	dec    %ebx
  800401:	85 db                	test   %ebx,%ebx
  800403:	7f ea                	jg     8003ef <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800405:	83 ec 08             	sub    $0x8,%esp
  800408:	56                   	push   %esi
  800409:	83 ec 04             	sub    $0x4,%esp
  80040c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80040f:	ff 75 e0             	pushl  -0x20(%ebp)
  800412:	ff 75 dc             	pushl  -0x24(%ebp)
  800415:	ff 75 d8             	pushl  -0x28(%ebp)
  800418:	e8 fb 1c 00 00       	call   802118 <__umoddi3>
  80041d:	83 c4 14             	add    $0x14,%esp
  800420:	0f be 80 5f 23 80 00 	movsbl 0x80235f(%eax),%eax
  800427:	50                   	push   %eax
  800428:	ff d7                	call   *%edi
}
  80042a:	83 c4 10             	add    $0x10,%esp
  80042d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800430:	5b                   	pop    %ebx
  800431:	5e                   	pop    %esi
  800432:	5f                   	pop    %edi
  800433:	5d                   	pop    %ebp
  800434:	c3                   	ret    

00800435 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800435:	55                   	push   %ebp
  800436:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800438:	83 fa 01             	cmp    $0x1,%edx
  80043b:	7e 0e                	jle    80044b <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80043d:	8b 10                	mov    (%eax),%edx
  80043f:	8d 4a 08             	lea    0x8(%edx),%ecx
  800442:	89 08                	mov    %ecx,(%eax)
  800444:	8b 02                	mov    (%edx),%eax
  800446:	8b 52 04             	mov    0x4(%edx),%edx
  800449:	eb 22                	jmp    80046d <getuint+0x38>
	else if (lflag)
  80044b:	85 d2                	test   %edx,%edx
  80044d:	74 10                	je     80045f <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80044f:	8b 10                	mov    (%eax),%edx
  800451:	8d 4a 04             	lea    0x4(%edx),%ecx
  800454:	89 08                	mov    %ecx,(%eax)
  800456:	8b 02                	mov    (%edx),%eax
  800458:	ba 00 00 00 00       	mov    $0x0,%edx
  80045d:	eb 0e                	jmp    80046d <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80045f:	8b 10                	mov    (%eax),%edx
  800461:	8d 4a 04             	lea    0x4(%edx),%ecx
  800464:	89 08                	mov    %ecx,(%eax)
  800466:	8b 02                	mov    (%edx),%eax
  800468:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80046d:	5d                   	pop    %ebp
  80046e:	c3                   	ret    

0080046f <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80046f:	55                   	push   %ebp
  800470:	89 e5                	mov    %esp,%ebp
  800472:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800475:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800478:	8b 10                	mov    (%eax),%edx
  80047a:	3b 50 04             	cmp    0x4(%eax),%edx
  80047d:	73 0a                	jae    800489 <sprintputch+0x1a>
		*b->buf++ = ch;
  80047f:	8d 4a 01             	lea    0x1(%edx),%ecx
  800482:	89 08                	mov    %ecx,(%eax)
  800484:	8b 45 08             	mov    0x8(%ebp),%eax
  800487:	88 02                	mov    %al,(%edx)
}
  800489:	5d                   	pop    %ebp
  80048a:	c3                   	ret    

0080048b <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80048b:	55                   	push   %ebp
  80048c:	89 e5                	mov    %esp,%ebp
  80048e:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800491:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800494:	50                   	push   %eax
  800495:	ff 75 10             	pushl  0x10(%ebp)
  800498:	ff 75 0c             	pushl  0xc(%ebp)
  80049b:	ff 75 08             	pushl  0x8(%ebp)
  80049e:	e8 05 00 00 00       	call   8004a8 <vprintfmt>
	va_end(ap);
}
  8004a3:	83 c4 10             	add    $0x10,%esp
  8004a6:	c9                   	leave  
  8004a7:	c3                   	ret    

008004a8 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8004a8:	55                   	push   %ebp
  8004a9:	89 e5                	mov    %esp,%ebp
  8004ab:	57                   	push   %edi
  8004ac:	56                   	push   %esi
  8004ad:	53                   	push   %ebx
  8004ae:	83 ec 2c             	sub    $0x2c,%esp
  8004b1:	8b 75 08             	mov    0x8(%ebp),%esi
  8004b4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8004b7:	8b 7d 10             	mov    0x10(%ebp),%edi
  8004ba:	eb 12                	jmp    8004ce <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8004bc:	85 c0                	test   %eax,%eax
  8004be:	0f 84 68 03 00 00    	je     80082c <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8004c4:	83 ec 08             	sub    $0x8,%esp
  8004c7:	53                   	push   %ebx
  8004c8:	50                   	push   %eax
  8004c9:	ff d6                	call   *%esi
  8004cb:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004ce:	47                   	inc    %edi
  8004cf:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8004d3:	83 f8 25             	cmp    $0x25,%eax
  8004d6:	75 e4                	jne    8004bc <vprintfmt+0x14>
  8004d8:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8004dc:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8004e3:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8004ea:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8004f1:	ba 00 00 00 00       	mov    $0x0,%edx
  8004f6:	eb 07                	jmp    8004ff <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004f8:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8004fb:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ff:	8d 47 01             	lea    0x1(%edi),%eax
  800502:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800505:	0f b6 0f             	movzbl (%edi),%ecx
  800508:	8a 07                	mov    (%edi),%al
  80050a:	83 e8 23             	sub    $0x23,%eax
  80050d:	3c 55                	cmp    $0x55,%al
  80050f:	0f 87 fe 02 00 00    	ja     800813 <vprintfmt+0x36b>
  800515:	0f b6 c0             	movzbl %al,%eax
  800518:	ff 24 85 a0 24 80 00 	jmp    *0x8024a0(,%eax,4)
  80051f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800522:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800526:	eb d7                	jmp    8004ff <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800528:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80052b:	b8 00 00 00 00       	mov    $0x0,%eax
  800530:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800533:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800536:	01 c0                	add    %eax,%eax
  800538:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80053c:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80053f:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800542:	83 fa 09             	cmp    $0x9,%edx
  800545:	77 34                	ja     80057b <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800547:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800548:	eb e9                	jmp    800533 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80054a:	8b 45 14             	mov    0x14(%ebp),%eax
  80054d:	8d 48 04             	lea    0x4(%eax),%ecx
  800550:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800553:	8b 00                	mov    (%eax),%eax
  800555:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800558:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80055b:	eb 24                	jmp    800581 <vprintfmt+0xd9>
  80055d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800561:	79 07                	jns    80056a <vprintfmt+0xc2>
  800563:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80056a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80056d:	eb 90                	jmp    8004ff <vprintfmt+0x57>
  80056f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800572:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800579:	eb 84                	jmp    8004ff <vprintfmt+0x57>
  80057b:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80057e:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800581:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800585:	0f 89 74 ff ff ff    	jns    8004ff <vprintfmt+0x57>
				width = precision, precision = -1;
  80058b:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80058e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800591:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800598:	e9 62 ff ff ff       	jmp    8004ff <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80059d:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8005a1:	e9 59 ff ff ff       	jmp    8004ff <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8005a6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a9:	8d 50 04             	lea    0x4(%eax),%edx
  8005ac:	89 55 14             	mov    %edx,0x14(%ebp)
  8005af:	83 ec 08             	sub    $0x8,%esp
  8005b2:	53                   	push   %ebx
  8005b3:	ff 30                	pushl  (%eax)
  8005b5:	ff d6                	call   *%esi
			break;
  8005b7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ba:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8005bd:	e9 0c ff ff ff       	jmp    8004ce <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8005c2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c5:	8d 50 04             	lea    0x4(%eax),%edx
  8005c8:	89 55 14             	mov    %edx,0x14(%ebp)
  8005cb:	8b 00                	mov    (%eax),%eax
  8005cd:	85 c0                	test   %eax,%eax
  8005cf:	79 02                	jns    8005d3 <vprintfmt+0x12b>
  8005d1:	f7 d8                	neg    %eax
  8005d3:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8005d5:	83 f8 0f             	cmp    $0xf,%eax
  8005d8:	7f 0b                	jg     8005e5 <vprintfmt+0x13d>
  8005da:	8b 04 85 00 26 80 00 	mov    0x802600(,%eax,4),%eax
  8005e1:	85 c0                	test   %eax,%eax
  8005e3:	75 18                	jne    8005fd <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8005e5:	52                   	push   %edx
  8005e6:	68 77 23 80 00       	push   $0x802377
  8005eb:	53                   	push   %ebx
  8005ec:	56                   	push   %esi
  8005ed:	e8 99 fe ff ff       	call   80048b <printfmt>
  8005f2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8005f8:	e9 d1 fe ff ff       	jmp    8004ce <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8005fd:	50                   	push   %eax
  8005fe:	68 65 28 80 00       	push   $0x802865
  800603:	53                   	push   %ebx
  800604:	56                   	push   %esi
  800605:	e8 81 fe ff ff       	call   80048b <printfmt>
  80060a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80060d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800610:	e9 b9 fe ff ff       	jmp    8004ce <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800615:	8b 45 14             	mov    0x14(%ebp),%eax
  800618:	8d 50 04             	lea    0x4(%eax),%edx
  80061b:	89 55 14             	mov    %edx,0x14(%ebp)
  80061e:	8b 38                	mov    (%eax),%edi
  800620:	85 ff                	test   %edi,%edi
  800622:	75 05                	jne    800629 <vprintfmt+0x181>
				p = "(null)";
  800624:	bf 70 23 80 00       	mov    $0x802370,%edi
			if (width > 0 && padc != '-')
  800629:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80062d:	0f 8e 90 00 00 00    	jle    8006c3 <vprintfmt+0x21b>
  800633:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800637:	0f 84 8e 00 00 00    	je     8006cb <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80063d:	83 ec 08             	sub    $0x8,%esp
  800640:	ff 75 d0             	pushl  -0x30(%ebp)
  800643:	57                   	push   %edi
  800644:	e8 70 02 00 00       	call   8008b9 <strnlen>
  800649:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80064c:	29 c1                	sub    %eax,%ecx
  80064e:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800651:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800654:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800658:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80065b:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80065e:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800660:	eb 0d                	jmp    80066f <vprintfmt+0x1c7>
					putch(padc, putdat);
  800662:	83 ec 08             	sub    $0x8,%esp
  800665:	53                   	push   %ebx
  800666:	ff 75 e4             	pushl  -0x1c(%ebp)
  800669:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80066b:	4f                   	dec    %edi
  80066c:	83 c4 10             	add    $0x10,%esp
  80066f:	85 ff                	test   %edi,%edi
  800671:	7f ef                	jg     800662 <vprintfmt+0x1ba>
  800673:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800676:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800679:	89 c8                	mov    %ecx,%eax
  80067b:	85 c9                	test   %ecx,%ecx
  80067d:	79 05                	jns    800684 <vprintfmt+0x1dc>
  80067f:	b8 00 00 00 00       	mov    $0x0,%eax
  800684:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800687:	29 c1                	sub    %eax,%ecx
  800689:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80068c:	89 75 08             	mov    %esi,0x8(%ebp)
  80068f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800692:	eb 3d                	jmp    8006d1 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800694:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800698:	74 19                	je     8006b3 <vprintfmt+0x20b>
  80069a:	0f be c0             	movsbl %al,%eax
  80069d:	83 e8 20             	sub    $0x20,%eax
  8006a0:	83 f8 5e             	cmp    $0x5e,%eax
  8006a3:	76 0e                	jbe    8006b3 <vprintfmt+0x20b>
					putch('?', putdat);
  8006a5:	83 ec 08             	sub    $0x8,%esp
  8006a8:	53                   	push   %ebx
  8006a9:	6a 3f                	push   $0x3f
  8006ab:	ff 55 08             	call   *0x8(%ebp)
  8006ae:	83 c4 10             	add    $0x10,%esp
  8006b1:	eb 0b                	jmp    8006be <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8006b3:	83 ec 08             	sub    $0x8,%esp
  8006b6:	53                   	push   %ebx
  8006b7:	52                   	push   %edx
  8006b8:	ff 55 08             	call   *0x8(%ebp)
  8006bb:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8006be:	ff 4d e4             	decl   -0x1c(%ebp)
  8006c1:	eb 0e                	jmp    8006d1 <vprintfmt+0x229>
  8006c3:	89 75 08             	mov    %esi,0x8(%ebp)
  8006c6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8006c9:	eb 06                	jmp    8006d1 <vprintfmt+0x229>
  8006cb:	89 75 08             	mov    %esi,0x8(%ebp)
  8006ce:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8006d1:	47                   	inc    %edi
  8006d2:	8a 47 ff             	mov    -0x1(%edi),%al
  8006d5:	0f be d0             	movsbl %al,%edx
  8006d8:	85 d2                	test   %edx,%edx
  8006da:	74 1d                	je     8006f9 <vprintfmt+0x251>
  8006dc:	85 f6                	test   %esi,%esi
  8006de:	78 b4                	js     800694 <vprintfmt+0x1ec>
  8006e0:	4e                   	dec    %esi
  8006e1:	79 b1                	jns    800694 <vprintfmt+0x1ec>
  8006e3:	8b 75 08             	mov    0x8(%ebp),%esi
  8006e6:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8006e9:	eb 14                	jmp    8006ff <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8006eb:	83 ec 08             	sub    $0x8,%esp
  8006ee:	53                   	push   %ebx
  8006ef:	6a 20                	push   $0x20
  8006f1:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8006f3:	4f                   	dec    %edi
  8006f4:	83 c4 10             	add    $0x10,%esp
  8006f7:	eb 06                	jmp    8006ff <vprintfmt+0x257>
  8006f9:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8006fc:	8b 75 08             	mov    0x8(%ebp),%esi
  8006ff:	85 ff                	test   %edi,%edi
  800701:	7f e8                	jg     8006eb <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800703:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800706:	e9 c3 fd ff ff       	jmp    8004ce <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80070b:	83 fa 01             	cmp    $0x1,%edx
  80070e:	7e 16                	jle    800726 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800710:	8b 45 14             	mov    0x14(%ebp),%eax
  800713:	8d 50 08             	lea    0x8(%eax),%edx
  800716:	89 55 14             	mov    %edx,0x14(%ebp)
  800719:	8b 50 04             	mov    0x4(%eax),%edx
  80071c:	8b 00                	mov    (%eax),%eax
  80071e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800721:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800724:	eb 32                	jmp    800758 <vprintfmt+0x2b0>
	else if (lflag)
  800726:	85 d2                	test   %edx,%edx
  800728:	74 18                	je     800742 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80072a:	8b 45 14             	mov    0x14(%ebp),%eax
  80072d:	8d 50 04             	lea    0x4(%eax),%edx
  800730:	89 55 14             	mov    %edx,0x14(%ebp)
  800733:	8b 00                	mov    (%eax),%eax
  800735:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800738:	89 c1                	mov    %eax,%ecx
  80073a:	c1 f9 1f             	sar    $0x1f,%ecx
  80073d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800740:	eb 16                	jmp    800758 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800742:	8b 45 14             	mov    0x14(%ebp),%eax
  800745:	8d 50 04             	lea    0x4(%eax),%edx
  800748:	89 55 14             	mov    %edx,0x14(%ebp)
  80074b:	8b 00                	mov    (%eax),%eax
  80074d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800750:	89 c1                	mov    %eax,%ecx
  800752:	c1 f9 1f             	sar    $0x1f,%ecx
  800755:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800758:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80075b:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80075e:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800762:	79 76                	jns    8007da <vprintfmt+0x332>
				putch('-', putdat);
  800764:	83 ec 08             	sub    $0x8,%esp
  800767:	53                   	push   %ebx
  800768:	6a 2d                	push   $0x2d
  80076a:	ff d6                	call   *%esi
				num = -(long long) num;
  80076c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80076f:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800772:	f7 d8                	neg    %eax
  800774:	83 d2 00             	adc    $0x0,%edx
  800777:	f7 da                	neg    %edx
  800779:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80077c:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800781:	eb 5c                	jmp    8007df <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800783:	8d 45 14             	lea    0x14(%ebp),%eax
  800786:	e8 aa fc ff ff       	call   800435 <getuint>
			base = 10;
  80078b:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800790:	eb 4d                	jmp    8007df <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800792:	8d 45 14             	lea    0x14(%ebp),%eax
  800795:	e8 9b fc ff ff       	call   800435 <getuint>
			base = 8;
  80079a:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80079f:	eb 3e                	jmp    8007df <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8007a1:	83 ec 08             	sub    $0x8,%esp
  8007a4:	53                   	push   %ebx
  8007a5:	6a 30                	push   $0x30
  8007a7:	ff d6                	call   *%esi
			putch('x', putdat);
  8007a9:	83 c4 08             	add    $0x8,%esp
  8007ac:	53                   	push   %ebx
  8007ad:	6a 78                	push   $0x78
  8007af:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8007b1:	8b 45 14             	mov    0x14(%ebp),%eax
  8007b4:	8d 50 04             	lea    0x4(%eax),%edx
  8007b7:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8007ba:	8b 00                	mov    (%eax),%eax
  8007bc:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8007c1:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8007c4:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8007c9:	eb 14                	jmp    8007df <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8007cb:	8d 45 14             	lea    0x14(%ebp),%eax
  8007ce:	e8 62 fc ff ff       	call   800435 <getuint>
			base = 16;
  8007d3:	b9 10 00 00 00       	mov    $0x10,%ecx
  8007d8:	eb 05                	jmp    8007df <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8007da:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8007df:	83 ec 0c             	sub    $0xc,%esp
  8007e2:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8007e6:	57                   	push   %edi
  8007e7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8007ea:	51                   	push   %ecx
  8007eb:	52                   	push   %edx
  8007ec:	50                   	push   %eax
  8007ed:	89 da                	mov    %ebx,%edx
  8007ef:	89 f0                	mov    %esi,%eax
  8007f1:	e8 92 fb ff ff       	call   800388 <printnum>
			break;
  8007f6:	83 c4 20             	add    $0x20,%esp
  8007f9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007fc:	e9 cd fc ff ff       	jmp    8004ce <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800801:	83 ec 08             	sub    $0x8,%esp
  800804:	53                   	push   %ebx
  800805:	51                   	push   %ecx
  800806:	ff d6                	call   *%esi
			break;
  800808:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80080b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80080e:	e9 bb fc ff ff       	jmp    8004ce <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800813:	83 ec 08             	sub    $0x8,%esp
  800816:	53                   	push   %ebx
  800817:	6a 25                	push   $0x25
  800819:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80081b:	83 c4 10             	add    $0x10,%esp
  80081e:	eb 01                	jmp    800821 <vprintfmt+0x379>
  800820:	4f                   	dec    %edi
  800821:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800825:	75 f9                	jne    800820 <vprintfmt+0x378>
  800827:	e9 a2 fc ff ff       	jmp    8004ce <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80082c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80082f:	5b                   	pop    %ebx
  800830:	5e                   	pop    %esi
  800831:	5f                   	pop    %edi
  800832:	5d                   	pop    %ebp
  800833:	c3                   	ret    

00800834 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800834:	55                   	push   %ebp
  800835:	89 e5                	mov    %esp,%ebp
  800837:	83 ec 18             	sub    $0x18,%esp
  80083a:	8b 45 08             	mov    0x8(%ebp),%eax
  80083d:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800840:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800843:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800847:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80084a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800851:	85 c0                	test   %eax,%eax
  800853:	74 26                	je     80087b <vsnprintf+0x47>
  800855:	85 d2                	test   %edx,%edx
  800857:	7e 29                	jle    800882 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800859:	ff 75 14             	pushl  0x14(%ebp)
  80085c:	ff 75 10             	pushl  0x10(%ebp)
  80085f:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800862:	50                   	push   %eax
  800863:	68 6f 04 80 00       	push   $0x80046f
  800868:	e8 3b fc ff ff       	call   8004a8 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80086d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800870:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800873:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800876:	83 c4 10             	add    $0x10,%esp
  800879:	eb 0c                	jmp    800887 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80087b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800880:	eb 05                	jmp    800887 <vsnprintf+0x53>
  800882:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800887:	c9                   	leave  
  800888:	c3                   	ret    

00800889 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800889:	55                   	push   %ebp
  80088a:	89 e5                	mov    %esp,%ebp
  80088c:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80088f:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800892:	50                   	push   %eax
  800893:	ff 75 10             	pushl  0x10(%ebp)
  800896:	ff 75 0c             	pushl  0xc(%ebp)
  800899:	ff 75 08             	pushl  0x8(%ebp)
  80089c:	e8 93 ff ff ff       	call   800834 <vsnprintf>
	va_end(ap);

	return rc;
}
  8008a1:	c9                   	leave  
  8008a2:	c3                   	ret    

008008a3 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8008a3:	55                   	push   %ebp
  8008a4:	89 e5                	mov    %esp,%ebp
  8008a6:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8008a9:	b8 00 00 00 00       	mov    $0x0,%eax
  8008ae:	eb 01                	jmp    8008b1 <strlen+0xe>
		n++;
  8008b0:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8008b1:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8008b5:	75 f9                	jne    8008b0 <strlen+0xd>
		n++;
	return n;
}
  8008b7:	5d                   	pop    %ebp
  8008b8:	c3                   	ret    

008008b9 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8008b9:	55                   	push   %ebp
  8008ba:	89 e5                	mov    %esp,%ebp
  8008bc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8008bf:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8008c2:	ba 00 00 00 00       	mov    $0x0,%edx
  8008c7:	eb 01                	jmp    8008ca <strnlen+0x11>
		n++;
  8008c9:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8008ca:	39 c2                	cmp    %eax,%edx
  8008cc:	74 08                	je     8008d6 <strnlen+0x1d>
  8008ce:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8008d2:	75 f5                	jne    8008c9 <strnlen+0x10>
  8008d4:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8008d6:	5d                   	pop    %ebp
  8008d7:	c3                   	ret    

008008d8 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8008d8:	55                   	push   %ebp
  8008d9:	89 e5                	mov    %esp,%ebp
  8008db:	53                   	push   %ebx
  8008dc:	8b 45 08             	mov    0x8(%ebp),%eax
  8008df:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8008e2:	89 c2                	mov    %eax,%edx
  8008e4:	42                   	inc    %edx
  8008e5:	41                   	inc    %ecx
  8008e6:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8008e9:	88 5a ff             	mov    %bl,-0x1(%edx)
  8008ec:	84 db                	test   %bl,%bl
  8008ee:	75 f4                	jne    8008e4 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8008f0:	5b                   	pop    %ebx
  8008f1:	5d                   	pop    %ebp
  8008f2:	c3                   	ret    

008008f3 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8008f3:	55                   	push   %ebp
  8008f4:	89 e5                	mov    %esp,%ebp
  8008f6:	53                   	push   %ebx
  8008f7:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8008fa:	53                   	push   %ebx
  8008fb:	e8 a3 ff ff ff       	call   8008a3 <strlen>
  800900:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800903:	ff 75 0c             	pushl  0xc(%ebp)
  800906:	01 d8                	add    %ebx,%eax
  800908:	50                   	push   %eax
  800909:	e8 ca ff ff ff       	call   8008d8 <strcpy>
	return dst;
}
  80090e:	89 d8                	mov    %ebx,%eax
  800910:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800913:	c9                   	leave  
  800914:	c3                   	ret    

00800915 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800915:	55                   	push   %ebp
  800916:	89 e5                	mov    %esp,%ebp
  800918:	56                   	push   %esi
  800919:	53                   	push   %ebx
  80091a:	8b 75 08             	mov    0x8(%ebp),%esi
  80091d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800920:	89 f3                	mov    %esi,%ebx
  800922:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800925:	89 f2                	mov    %esi,%edx
  800927:	eb 0c                	jmp    800935 <strncpy+0x20>
		*dst++ = *src;
  800929:	42                   	inc    %edx
  80092a:	8a 01                	mov    (%ecx),%al
  80092c:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80092f:	80 39 01             	cmpb   $0x1,(%ecx)
  800932:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800935:	39 da                	cmp    %ebx,%edx
  800937:	75 f0                	jne    800929 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800939:	89 f0                	mov    %esi,%eax
  80093b:	5b                   	pop    %ebx
  80093c:	5e                   	pop    %esi
  80093d:	5d                   	pop    %ebp
  80093e:	c3                   	ret    

0080093f <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80093f:	55                   	push   %ebp
  800940:	89 e5                	mov    %esp,%ebp
  800942:	56                   	push   %esi
  800943:	53                   	push   %ebx
  800944:	8b 75 08             	mov    0x8(%ebp),%esi
  800947:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80094a:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80094d:	85 c0                	test   %eax,%eax
  80094f:	74 1e                	je     80096f <strlcpy+0x30>
  800951:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800955:	89 f2                	mov    %esi,%edx
  800957:	eb 05                	jmp    80095e <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800959:	42                   	inc    %edx
  80095a:	41                   	inc    %ecx
  80095b:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80095e:	39 c2                	cmp    %eax,%edx
  800960:	74 08                	je     80096a <strlcpy+0x2b>
  800962:	8a 19                	mov    (%ecx),%bl
  800964:	84 db                	test   %bl,%bl
  800966:	75 f1                	jne    800959 <strlcpy+0x1a>
  800968:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80096a:	c6 00 00             	movb   $0x0,(%eax)
  80096d:	eb 02                	jmp    800971 <strlcpy+0x32>
  80096f:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800971:	29 f0                	sub    %esi,%eax
}
  800973:	5b                   	pop    %ebx
  800974:	5e                   	pop    %esi
  800975:	5d                   	pop    %ebp
  800976:	c3                   	ret    

00800977 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800977:	55                   	push   %ebp
  800978:	89 e5                	mov    %esp,%ebp
  80097a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80097d:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800980:	eb 02                	jmp    800984 <strcmp+0xd>
		p++, q++;
  800982:	41                   	inc    %ecx
  800983:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800984:	8a 01                	mov    (%ecx),%al
  800986:	84 c0                	test   %al,%al
  800988:	74 04                	je     80098e <strcmp+0x17>
  80098a:	3a 02                	cmp    (%edx),%al
  80098c:	74 f4                	je     800982 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80098e:	0f b6 c0             	movzbl %al,%eax
  800991:	0f b6 12             	movzbl (%edx),%edx
  800994:	29 d0                	sub    %edx,%eax
}
  800996:	5d                   	pop    %ebp
  800997:	c3                   	ret    

00800998 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800998:	55                   	push   %ebp
  800999:	89 e5                	mov    %esp,%ebp
  80099b:	53                   	push   %ebx
  80099c:	8b 45 08             	mov    0x8(%ebp),%eax
  80099f:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009a2:	89 c3                	mov    %eax,%ebx
  8009a4:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8009a7:	eb 02                	jmp    8009ab <strncmp+0x13>
		n--, p++, q++;
  8009a9:	40                   	inc    %eax
  8009aa:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8009ab:	39 d8                	cmp    %ebx,%eax
  8009ad:	74 14                	je     8009c3 <strncmp+0x2b>
  8009af:	8a 08                	mov    (%eax),%cl
  8009b1:	84 c9                	test   %cl,%cl
  8009b3:	74 04                	je     8009b9 <strncmp+0x21>
  8009b5:	3a 0a                	cmp    (%edx),%cl
  8009b7:	74 f0                	je     8009a9 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8009b9:	0f b6 00             	movzbl (%eax),%eax
  8009bc:	0f b6 12             	movzbl (%edx),%edx
  8009bf:	29 d0                	sub    %edx,%eax
  8009c1:	eb 05                	jmp    8009c8 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8009c3:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8009c8:	5b                   	pop    %ebx
  8009c9:	5d                   	pop    %ebp
  8009ca:	c3                   	ret    

008009cb <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8009cb:	55                   	push   %ebp
  8009cc:	89 e5                	mov    %esp,%ebp
  8009ce:	8b 45 08             	mov    0x8(%ebp),%eax
  8009d1:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8009d4:	eb 05                	jmp    8009db <strchr+0x10>
		if (*s == c)
  8009d6:	38 ca                	cmp    %cl,%dl
  8009d8:	74 0c                	je     8009e6 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8009da:	40                   	inc    %eax
  8009db:	8a 10                	mov    (%eax),%dl
  8009dd:	84 d2                	test   %dl,%dl
  8009df:	75 f5                	jne    8009d6 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8009e1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009e6:	5d                   	pop    %ebp
  8009e7:	c3                   	ret    

008009e8 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8009e8:	55                   	push   %ebp
  8009e9:	89 e5                	mov    %esp,%ebp
  8009eb:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ee:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8009f1:	eb 05                	jmp    8009f8 <strfind+0x10>
		if (*s == c)
  8009f3:	38 ca                	cmp    %cl,%dl
  8009f5:	74 07                	je     8009fe <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8009f7:	40                   	inc    %eax
  8009f8:	8a 10                	mov    (%eax),%dl
  8009fa:	84 d2                	test   %dl,%dl
  8009fc:	75 f5                	jne    8009f3 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8009fe:	5d                   	pop    %ebp
  8009ff:	c3                   	ret    

00800a00 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800a00:	55                   	push   %ebp
  800a01:	89 e5                	mov    %esp,%ebp
  800a03:	57                   	push   %edi
  800a04:	56                   	push   %esi
  800a05:	53                   	push   %ebx
  800a06:	8b 7d 08             	mov    0x8(%ebp),%edi
  800a09:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800a0c:	85 c9                	test   %ecx,%ecx
  800a0e:	74 36                	je     800a46 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800a10:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800a16:	75 28                	jne    800a40 <memset+0x40>
  800a18:	f6 c1 03             	test   $0x3,%cl
  800a1b:	75 23                	jne    800a40 <memset+0x40>
		c &= 0xFF;
  800a1d:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800a21:	89 d3                	mov    %edx,%ebx
  800a23:	c1 e3 08             	shl    $0x8,%ebx
  800a26:	89 d6                	mov    %edx,%esi
  800a28:	c1 e6 18             	shl    $0x18,%esi
  800a2b:	89 d0                	mov    %edx,%eax
  800a2d:	c1 e0 10             	shl    $0x10,%eax
  800a30:	09 f0                	or     %esi,%eax
  800a32:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800a34:	89 d8                	mov    %ebx,%eax
  800a36:	09 d0                	or     %edx,%eax
  800a38:	c1 e9 02             	shr    $0x2,%ecx
  800a3b:	fc                   	cld    
  800a3c:	f3 ab                	rep stos %eax,%es:(%edi)
  800a3e:	eb 06                	jmp    800a46 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800a40:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a43:	fc                   	cld    
  800a44:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800a46:	89 f8                	mov    %edi,%eax
  800a48:	5b                   	pop    %ebx
  800a49:	5e                   	pop    %esi
  800a4a:	5f                   	pop    %edi
  800a4b:	5d                   	pop    %ebp
  800a4c:	c3                   	ret    

00800a4d <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800a4d:	55                   	push   %ebp
  800a4e:	89 e5                	mov    %esp,%ebp
  800a50:	57                   	push   %edi
  800a51:	56                   	push   %esi
  800a52:	8b 45 08             	mov    0x8(%ebp),%eax
  800a55:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a58:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800a5b:	39 c6                	cmp    %eax,%esi
  800a5d:	73 33                	jae    800a92 <memmove+0x45>
  800a5f:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800a62:	39 d0                	cmp    %edx,%eax
  800a64:	73 2c                	jae    800a92 <memmove+0x45>
		s += n;
		d += n;
  800a66:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a69:	89 d6                	mov    %edx,%esi
  800a6b:	09 fe                	or     %edi,%esi
  800a6d:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800a73:	75 13                	jne    800a88 <memmove+0x3b>
  800a75:	f6 c1 03             	test   $0x3,%cl
  800a78:	75 0e                	jne    800a88 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800a7a:	83 ef 04             	sub    $0x4,%edi
  800a7d:	8d 72 fc             	lea    -0x4(%edx),%esi
  800a80:	c1 e9 02             	shr    $0x2,%ecx
  800a83:	fd                   	std    
  800a84:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a86:	eb 07                	jmp    800a8f <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800a88:	4f                   	dec    %edi
  800a89:	8d 72 ff             	lea    -0x1(%edx),%esi
  800a8c:	fd                   	std    
  800a8d:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800a8f:	fc                   	cld    
  800a90:	eb 1d                	jmp    800aaf <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a92:	89 f2                	mov    %esi,%edx
  800a94:	09 c2                	or     %eax,%edx
  800a96:	f6 c2 03             	test   $0x3,%dl
  800a99:	75 0f                	jne    800aaa <memmove+0x5d>
  800a9b:	f6 c1 03             	test   $0x3,%cl
  800a9e:	75 0a                	jne    800aaa <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800aa0:	c1 e9 02             	shr    $0x2,%ecx
  800aa3:	89 c7                	mov    %eax,%edi
  800aa5:	fc                   	cld    
  800aa6:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800aa8:	eb 05                	jmp    800aaf <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800aaa:	89 c7                	mov    %eax,%edi
  800aac:	fc                   	cld    
  800aad:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800aaf:	5e                   	pop    %esi
  800ab0:	5f                   	pop    %edi
  800ab1:	5d                   	pop    %ebp
  800ab2:	c3                   	ret    

00800ab3 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800ab3:	55                   	push   %ebp
  800ab4:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800ab6:	ff 75 10             	pushl  0x10(%ebp)
  800ab9:	ff 75 0c             	pushl  0xc(%ebp)
  800abc:	ff 75 08             	pushl  0x8(%ebp)
  800abf:	e8 89 ff ff ff       	call   800a4d <memmove>
}
  800ac4:	c9                   	leave  
  800ac5:	c3                   	ret    

00800ac6 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800ac6:	55                   	push   %ebp
  800ac7:	89 e5                	mov    %esp,%ebp
  800ac9:	56                   	push   %esi
  800aca:	53                   	push   %ebx
  800acb:	8b 45 08             	mov    0x8(%ebp),%eax
  800ace:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ad1:	89 c6                	mov    %eax,%esi
  800ad3:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ad6:	eb 14                	jmp    800aec <memcmp+0x26>
		if (*s1 != *s2)
  800ad8:	8a 08                	mov    (%eax),%cl
  800ada:	8a 1a                	mov    (%edx),%bl
  800adc:	38 d9                	cmp    %bl,%cl
  800ade:	74 0a                	je     800aea <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800ae0:	0f b6 c1             	movzbl %cl,%eax
  800ae3:	0f b6 db             	movzbl %bl,%ebx
  800ae6:	29 d8                	sub    %ebx,%eax
  800ae8:	eb 0b                	jmp    800af5 <memcmp+0x2f>
		s1++, s2++;
  800aea:	40                   	inc    %eax
  800aeb:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800aec:	39 f0                	cmp    %esi,%eax
  800aee:	75 e8                	jne    800ad8 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800af0:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800af5:	5b                   	pop    %ebx
  800af6:	5e                   	pop    %esi
  800af7:	5d                   	pop    %ebp
  800af8:	c3                   	ret    

00800af9 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800af9:	55                   	push   %ebp
  800afa:	89 e5                	mov    %esp,%ebp
  800afc:	53                   	push   %ebx
  800afd:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800b00:	89 c1                	mov    %eax,%ecx
  800b02:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800b05:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b09:	eb 08                	jmp    800b13 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b0b:	0f b6 10             	movzbl (%eax),%edx
  800b0e:	39 da                	cmp    %ebx,%edx
  800b10:	74 05                	je     800b17 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b12:	40                   	inc    %eax
  800b13:	39 c8                	cmp    %ecx,%eax
  800b15:	72 f4                	jb     800b0b <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800b17:	5b                   	pop    %ebx
  800b18:	5d                   	pop    %ebp
  800b19:	c3                   	ret    

00800b1a <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800b1a:	55                   	push   %ebp
  800b1b:	89 e5                	mov    %esp,%ebp
  800b1d:	57                   	push   %edi
  800b1e:	56                   	push   %esi
  800b1f:	53                   	push   %ebx
  800b20:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800b23:	eb 01                	jmp    800b26 <strtol+0xc>
		s++;
  800b25:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800b26:	8a 01                	mov    (%ecx),%al
  800b28:	3c 20                	cmp    $0x20,%al
  800b2a:	74 f9                	je     800b25 <strtol+0xb>
  800b2c:	3c 09                	cmp    $0x9,%al
  800b2e:	74 f5                	je     800b25 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800b30:	3c 2b                	cmp    $0x2b,%al
  800b32:	75 08                	jne    800b3c <strtol+0x22>
		s++;
  800b34:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800b35:	bf 00 00 00 00       	mov    $0x0,%edi
  800b3a:	eb 11                	jmp    800b4d <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800b3c:	3c 2d                	cmp    $0x2d,%al
  800b3e:	75 08                	jne    800b48 <strtol+0x2e>
		s++, neg = 1;
  800b40:	41                   	inc    %ecx
  800b41:	bf 01 00 00 00       	mov    $0x1,%edi
  800b46:	eb 05                	jmp    800b4d <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800b48:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b4d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b51:	0f 84 87 00 00 00    	je     800bde <strtol+0xc4>
  800b57:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800b5b:	75 27                	jne    800b84 <strtol+0x6a>
  800b5d:	80 39 30             	cmpb   $0x30,(%ecx)
  800b60:	75 22                	jne    800b84 <strtol+0x6a>
  800b62:	e9 88 00 00 00       	jmp    800bef <strtol+0xd5>
		s += 2, base = 16;
  800b67:	83 c1 02             	add    $0x2,%ecx
  800b6a:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800b71:	eb 11                	jmp    800b84 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800b73:	41                   	inc    %ecx
  800b74:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800b7b:	eb 07                	jmp    800b84 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800b7d:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800b84:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800b89:	8a 11                	mov    (%ecx),%dl
  800b8b:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800b8e:	80 fb 09             	cmp    $0x9,%bl
  800b91:	77 08                	ja     800b9b <strtol+0x81>
			dig = *s - '0';
  800b93:	0f be d2             	movsbl %dl,%edx
  800b96:	83 ea 30             	sub    $0x30,%edx
  800b99:	eb 22                	jmp    800bbd <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800b9b:	8d 72 9f             	lea    -0x61(%edx),%esi
  800b9e:	89 f3                	mov    %esi,%ebx
  800ba0:	80 fb 19             	cmp    $0x19,%bl
  800ba3:	77 08                	ja     800bad <strtol+0x93>
			dig = *s - 'a' + 10;
  800ba5:	0f be d2             	movsbl %dl,%edx
  800ba8:	83 ea 57             	sub    $0x57,%edx
  800bab:	eb 10                	jmp    800bbd <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800bad:	8d 72 bf             	lea    -0x41(%edx),%esi
  800bb0:	89 f3                	mov    %esi,%ebx
  800bb2:	80 fb 19             	cmp    $0x19,%bl
  800bb5:	77 14                	ja     800bcb <strtol+0xb1>
			dig = *s - 'A' + 10;
  800bb7:	0f be d2             	movsbl %dl,%edx
  800bba:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800bbd:	3b 55 10             	cmp    0x10(%ebp),%edx
  800bc0:	7d 09                	jge    800bcb <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800bc2:	41                   	inc    %ecx
  800bc3:	0f af 45 10          	imul   0x10(%ebp),%eax
  800bc7:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800bc9:	eb be                	jmp    800b89 <strtol+0x6f>

	if (endptr)
  800bcb:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800bcf:	74 05                	je     800bd6 <strtol+0xbc>
		*endptr = (char *) s;
  800bd1:	8b 75 0c             	mov    0xc(%ebp),%esi
  800bd4:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800bd6:	85 ff                	test   %edi,%edi
  800bd8:	74 21                	je     800bfb <strtol+0xe1>
  800bda:	f7 d8                	neg    %eax
  800bdc:	eb 1d                	jmp    800bfb <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bde:	80 39 30             	cmpb   $0x30,(%ecx)
  800be1:	75 9a                	jne    800b7d <strtol+0x63>
  800be3:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800be7:	0f 84 7a ff ff ff    	je     800b67 <strtol+0x4d>
  800bed:	eb 84                	jmp    800b73 <strtol+0x59>
  800bef:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800bf3:	0f 84 6e ff ff ff    	je     800b67 <strtol+0x4d>
  800bf9:	eb 89                	jmp    800b84 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800bfb:	5b                   	pop    %ebx
  800bfc:	5e                   	pop    %esi
  800bfd:	5f                   	pop    %edi
  800bfe:	5d                   	pop    %ebp
  800bff:	c3                   	ret    

00800c00 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800c00:	55                   	push   %ebp
  800c01:	89 e5                	mov    %esp,%ebp
  800c03:	57                   	push   %edi
  800c04:	56                   	push   %esi
  800c05:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c06:	b8 00 00 00 00       	mov    $0x0,%eax
  800c0b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c0e:	8b 55 08             	mov    0x8(%ebp),%edx
  800c11:	89 c3                	mov    %eax,%ebx
  800c13:	89 c7                	mov    %eax,%edi
  800c15:	89 c6                	mov    %eax,%esi
  800c17:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800c19:	5b                   	pop    %ebx
  800c1a:	5e                   	pop    %esi
  800c1b:	5f                   	pop    %edi
  800c1c:	5d                   	pop    %ebp
  800c1d:	c3                   	ret    

00800c1e <sys_cgetc>:

int
sys_cgetc(void)
{
  800c1e:	55                   	push   %ebp
  800c1f:	89 e5                	mov    %esp,%ebp
  800c21:	57                   	push   %edi
  800c22:	56                   	push   %esi
  800c23:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c24:	ba 00 00 00 00       	mov    $0x0,%edx
  800c29:	b8 01 00 00 00       	mov    $0x1,%eax
  800c2e:	89 d1                	mov    %edx,%ecx
  800c30:	89 d3                	mov    %edx,%ebx
  800c32:	89 d7                	mov    %edx,%edi
  800c34:	89 d6                	mov    %edx,%esi
  800c36:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800c38:	5b                   	pop    %ebx
  800c39:	5e                   	pop    %esi
  800c3a:	5f                   	pop    %edi
  800c3b:	5d                   	pop    %ebp
  800c3c:	c3                   	ret    

00800c3d <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800c3d:	55                   	push   %ebp
  800c3e:	89 e5                	mov    %esp,%ebp
  800c40:	57                   	push   %edi
  800c41:	56                   	push   %esi
  800c42:	53                   	push   %ebx
  800c43:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c46:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c4b:	b8 03 00 00 00       	mov    $0x3,%eax
  800c50:	8b 55 08             	mov    0x8(%ebp),%edx
  800c53:	89 cb                	mov    %ecx,%ebx
  800c55:	89 cf                	mov    %ecx,%edi
  800c57:	89 ce                	mov    %ecx,%esi
  800c59:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c5b:	85 c0                	test   %eax,%eax
  800c5d:	7e 17                	jle    800c76 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c5f:	83 ec 0c             	sub    $0xc,%esp
  800c62:	50                   	push   %eax
  800c63:	6a 03                	push   $0x3
  800c65:	68 5f 26 80 00       	push   $0x80265f
  800c6a:	6a 23                	push   $0x23
  800c6c:	68 7c 26 80 00       	push   $0x80267c
  800c71:	e8 26 f6 ff ff       	call   80029c <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800c76:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c79:	5b                   	pop    %ebx
  800c7a:	5e                   	pop    %esi
  800c7b:	5f                   	pop    %edi
  800c7c:	5d                   	pop    %ebp
  800c7d:	c3                   	ret    

00800c7e <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800c7e:	55                   	push   %ebp
  800c7f:	89 e5                	mov    %esp,%ebp
  800c81:	57                   	push   %edi
  800c82:	56                   	push   %esi
  800c83:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c84:	ba 00 00 00 00       	mov    $0x0,%edx
  800c89:	b8 02 00 00 00       	mov    $0x2,%eax
  800c8e:	89 d1                	mov    %edx,%ecx
  800c90:	89 d3                	mov    %edx,%ebx
  800c92:	89 d7                	mov    %edx,%edi
  800c94:	89 d6                	mov    %edx,%esi
  800c96:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800c98:	5b                   	pop    %ebx
  800c99:	5e                   	pop    %esi
  800c9a:	5f                   	pop    %edi
  800c9b:	5d                   	pop    %ebp
  800c9c:	c3                   	ret    

00800c9d <sys_yield>:

void
sys_yield(void)
{
  800c9d:	55                   	push   %ebp
  800c9e:	89 e5                	mov    %esp,%ebp
  800ca0:	57                   	push   %edi
  800ca1:	56                   	push   %esi
  800ca2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ca3:	ba 00 00 00 00       	mov    $0x0,%edx
  800ca8:	b8 0b 00 00 00       	mov    $0xb,%eax
  800cad:	89 d1                	mov    %edx,%ecx
  800caf:	89 d3                	mov    %edx,%ebx
  800cb1:	89 d7                	mov    %edx,%edi
  800cb3:	89 d6                	mov    %edx,%esi
  800cb5:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800cb7:	5b                   	pop    %ebx
  800cb8:	5e                   	pop    %esi
  800cb9:	5f                   	pop    %edi
  800cba:	5d                   	pop    %ebp
  800cbb:	c3                   	ret    

00800cbc <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800cbc:	55                   	push   %ebp
  800cbd:	89 e5                	mov    %esp,%ebp
  800cbf:	57                   	push   %edi
  800cc0:	56                   	push   %esi
  800cc1:	53                   	push   %ebx
  800cc2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cc5:	be 00 00 00 00       	mov    $0x0,%esi
  800cca:	b8 04 00 00 00       	mov    $0x4,%eax
  800ccf:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cd2:	8b 55 08             	mov    0x8(%ebp),%edx
  800cd5:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cd8:	89 f7                	mov    %esi,%edi
  800cda:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cdc:	85 c0                	test   %eax,%eax
  800cde:	7e 17                	jle    800cf7 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ce0:	83 ec 0c             	sub    $0xc,%esp
  800ce3:	50                   	push   %eax
  800ce4:	6a 04                	push   $0x4
  800ce6:	68 5f 26 80 00       	push   $0x80265f
  800ceb:	6a 23                	push   $0x23
  800ced:	68 7c 26 80 00       	push   $0x80267c
  800cf2:	e8 a5 f5 ff ff       	call   80029c <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800cf7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cfa:	5b                   	pop    %ebx
  800cfb:	5e                   	pop    %esi
  800cfc:	5f                   	pop    %edi
  800cfd:	5d                   	pop    %ebp
  800cfe:	c3                   	ret    

00800cff <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800cff:	55                   	push   %ebp
  800d00:	89 e5                	mov    %esp,%ebp
  800d02:	57                   	push   %edi
  800d03:	56                   	push   %esi
  800d04:	53                   	push   %ebx
  800d05:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d08:	b8 05 00 00 00       	mov    $0x5,%eax
  800d0d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d10:	8b 55 08             	mov    0x8(%ebp),%edx
  800d13:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d16:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d19:	8b 75 18             	mov    0x18(%ebp),%esi
  800d1c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d1e:	85 c0                	test   %eax,%eax
  800d20:	7e 17                	jle    800d39 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d22:	83 ec 0c             	sub    $0xc,%esp
  800d25:	50                   	push   %eax
  800d26:	6a 05                	push   $0x5
  800d28:	68 5f 26 80 00       	push   $0x80265f
  800d2d:	6a 23                	push   $0x23
  800d2f:	68 7c 26 80 00       	push   $0x80267c
  800d34:	e8 63 f5 ff ff       	call   80029c <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800d39:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d3c:	5b                   	pop    %ebx
  800d3d:	5e                   	pop    %esi
  800d3e:	5f                   	pop    %edi
  800d3f:	5d                   	pop    %ebp
  800d40:	c3                   	ret    

00800d41 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800d41:	55                   	push   %ebp
  800d42:	89 e5                	mov    %esp,%ebp
  800d44:	57                   	push   %edi
  800d45:	56                   	push   %esi
  800d46:	53                   	push   %ebx
  800d47:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d4a:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d4f:	b8 06 00 00 00       	mov    $0x6,%eax
  800d54:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d57:	8b 55 08             	mov    0x8(%ebp),%edx
  800d5a:	89 df                	mov    %ebx,%edi
  800d5c:	89 de                	mov    %ebx,%esi
  800d5e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d60:	85 c0                	test   %eax,%eax
  800d62:	7e 17                	jle    800d7b <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d64:	83 ec 0c             	sub    $0xc,%esp
  800d67:	50                   	push   %eax
  800d68:	6a 06                	push   $0x6
  800d6a:	68 5f 26 80 00       	push   $0x80265f
  800d6f:	6a 23                	push   $0x23
  800d71:	68 7c 26 80 00       	push   $0x80267c
  800d76:	e8 21 f5 ff ff       	call   80029c <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800d7b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d7e:	5b                   	pop    %ebx
  800d7f:	5e                   	pop    %esi
  800d80:	5f                   	pop    %edi
  800d81:	5d                   	pop    %ebp
  800d82:	c3                   	ret    

00800d83 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800d83:	55                   	push   %ebp
  800d84:	89 e5                	mov    %esp,%ebp
  800d86:	57                   	push   %edi
  800d87:	56                   	push   %esi
  800d88:	53                   	push   %ebx
  800d89:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d8c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d91:	b8 08 00 00 00       	mov    $0x8,%eax
  800d96:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d99:	8b 55 08             	mov    0x8(%ebp),%edx
  800d9c:	89 df                	mov    %ebx,%edi
  800d9e:	89 de                	mov    %ebx,%esi
  800da0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800da2:	85 c0                	test   %eax,%eax
  800da4:	7e 17                	jle    800dbd <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800da6:	83 ec 0c             	sub    $0xc,%esp
  800da9:	50                   	push   %eax
  800daa:	6a 08                	push   $0x8
  800dac:	68 5f 26 80 00       	push   $0x80265f
  800db1:	6a 23                	push   $0x23
  800db3:	68 7c 26 80 00       	push   $0x80267c
  800db8:	e8 df f4 ff ff       	call   80029c <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800dbd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dc0:	5b                   	pop    %ebx
  800dc1:	5e                   	pop    %esi
  800dc2:	5f                   	pop    %edi
  800dc3:	5d                   	pop    %ebp
  800dc4:	c3                   	ret    

00800dc5 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800dc5:	55                   	push   %ebp
  800dc6:	89 e5                	mov    %esp,%ebp
  800dc8:	57                   	push   %edi
  800dc9:	56                   	push   %esi
  800dca:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dcb:	ba 00 00 00 00       	mov    $0x0,%edx
  800dd0:	b8 0c 00 00 00       	mov    $0xc,%eax
  800dd5:	89 d1                	mov    %edx,%ecx
  800dd7:	89 d3                	mov    %edx,%ebx
  800dd9:	89 d7                	mov    %edx,%edi
  800ddb:	89 d6                	mov    %edx,%esi
  800ddd:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800ddf:	5b                   	pop    %ebx
  800de0:	5e                   	pop    %esi
  800de1:	5f                   	pop    %edi
  800de2:	5d                   	pop    %ebp
  800de3:	c3                   	ret    

00800de4 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800de4:	55                   	push   %ebp
  800de5:	89 e5                	mov    %esp,%ebp
  800de7:	57                   	push   %edi
  800de8:	56                   	push   %esi
  800de9:	53                   	push   %ebx
  800dea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ded:	bb 00 00 00 00       	mov    $0x0,%ebx
  800df2:	b8 09 00 00 00       	mov    $0x9,%eax
  800df7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800dfa:	8b 55 08             	mov    0x8(%ebp),%edx
  800dfd:	89 df                	mov    %ebx,%edi
  800dff:	89 de                	mov    %ebx,%esi
  800e01:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e03:	85 c0                	test   %eax,%eax
  800e05:	7e 17                	jle    800e1e <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e07:	83 ec 0c             	sub    $0xc,%esp
  800e0a:	50                   	push   %eax
  800e0b:	6a 09                	push   $0x9
  800e0d:	68 5f 26 80 00       	push   $0x80265f
  800e12:	6a 23                	push   $0x23
  800e14:	68 7c 26 80 00       	push   $0x80267c
  800e19:	e8 7e f4 ff ff       	call   80029c <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800e1e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e21:	5b                   	pop    %ebx
  800e22:	5e                   	pop    %esi
  800e23:	5f                   	pop    %edi
  800e24:	5d                   	pop    %ebp
  800e25:	c3                   	ret    

00800e26 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800e26:	55                   	push   %ebp
  800e27:	89 e5                	mov    %esp,%ebp
  800e29:	57                   	push   %edi
  800e2a:	56                   	push   %esi
  800e2b:	53                   	push   %ebx
  800e2c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e2f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e34:	b8 0a 00 00 00       	mov    $0xa,%eax
  800e39:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e3c:	8b 55 08             	mov    0x8(%ebp),%edx
  800e3f:	89 df                	mov    %ebx,%edi
  800e41:	89 de                	mov    %ebx,%esi
  800e43:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e45:	85 c0                	test   %eax,%eax
  800e47:	7e 17                	jle    800e60 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e49:	83 ec 0c             	sub    $0xc,%esp
  800e4c:	50                   	push   %eax
  800e4d:	6a 0a                	push   $0xa
  800e4f:	68 5f 26 80 00       	push   $0x80265f
  800e54:	6a 23                	push   $0x23
  800e56:	68 7c 26 80 00       	push   $0x80267c
  800e5b:	e8 3c f4 ff ff       	call   80029c <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800e60:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e63:	5b                   	pop    %ebx
  800e64:	5e                   	pop    %esi
  800e65:	5f                   	pop    %edi
  800e66:	5d                   	pop    %ebp
  800e67:	c3                   	ret    

00800e68 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800e68:	55                   	push   %ebp
  800e69:	89 e5                	mov    %esp,%ebp
  800e6b:	57                   	push   %edi
  800e6c:	56                   	push   %esi
  800e6d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e6e:	be 00 00 00 00       	mov    $0x0,%esi
  800e73:	b8 0d 00 00 00       	mov    $0xd,%eax
  800e78:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e7b:	8b 55 08             	mov    0x8(%ebp),%edx
  800e7e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e81:	8b 7d 14             	mov    0x14(%ebp),%edi
  800e84:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800e86:	5b                   	pop    %ebx
  800e87:	5e                   	pop    %esi
  800e88:	5f                   	pop    %edi
  800e89:	5d                   	pop    %ebp
  800e8a:	c3                   	ret    

00800e8b <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800e8b:	55                   	push   %ebp
  800e8c:	89 e5                	mov    %esp,%ebp
  800e8e:	57                   	push   %edi
  800e8f:	56                   	push   %esi
  800e90:	53                   	push   %ebx
  800e91:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e94:	b9 00 00 00 00       	mov    $0x0,%ecx
  800e99:	b8 0e 00 00 00       	mov    $0xe,%eax
  800e9e:	8b 55 08             	mov    0x8(%ebp),%edx
  800ea1:	89 cb                	mov    %ecx,%ebx
  800ea3:	89 cf                	mov    %ecx,%edi
  800ea5:	89 ce                	mov    %ecx,%esi
  800ea7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ea9:	85 c0                	test   %eax,%eax
  800eab:	7e 17                	jle    800ec4 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ead:	83 ec 0c             	sub    $0xc,%esp
  800eb0:	50                   	push   %eax
  800eb1:	6a 0e                	push   $0xe
  800eb3:	68 5f 26 80 00       	push   $0x80265f
  800eb8:	6a 23                	push   $0x23
  800eba:	68 7c 26 80 00       	push   $0x80267c
  800ebf:	e8 d8 f3 ff ff       	call   80029c <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800ec4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ec7:	5b                   	pop    %ebx
  800ec8:	5e                   	pop    %esi
  800ec9:	5f                   	pop    %edi
  800eca:	5d                   	pop    %ebp
  800ecb:	c3                   	ret    

00800ecc <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800ecc:	55                   	push   %ebp
  800ecd:	89 e5                	mov    %esp,%ebp
  800ecf:	53                   	push   %ebx
  800ed0:	83 ec 04             	sub    $0x4,%esp
  800ed3:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800ed6:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800ed8:	89 da                	mov    %ebx,%edx
  800eda:	c1 ea 0c             	shr    $0xc,%edx
  800edd:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800ee4:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800ee8:	74 05                	je     800eef <pgfault+0x23>
  800eea:	f6 c6 08             	test   $0x8,%dh
  800eed:	75 14                	jne    800f03 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800eef:	83 ec 04             	sub    $0x4,%esp
  800ef2:	68 8c 26 80 00       	push   $0x80268c
  800ef7:	6a 26                	push   $0x26
  800ef9:	68 f0 26 80 00       	push   $0x8026f0
  800efe:	e8 99 f3 ff ff       	call   80029c <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800f03:	83 ec 04             	sub    $0x4,%esp
  800f06:	6a 07                	push   $0x7
  800f08:	68 00 f0 7f 00       	push   $0x7ff000
  800f0d:	6a 00                	push   $0x0
  800f0f:	e8 a8 fd ff ff       	call   800cbc <sys_page_alloc>
  800f14:	83 c4 10             	add    $0x10,%esp
  800f17:	85 c0                	test   %eax,%eax
  800f19:	74 14                	je     800f2f <pgfault+0x63>
        panic("pgfault: no phys mem");
  800f1b:	83 ec 04             	sub    $0x4,%esp
  800f1e:	68 fb 26 80 00       	push   $0x8026fb
  800f23:	6a 32                	push   $0x32
  800f25:	68 f0 26 80 00       	push   $0x8026f0
  800f2a:	e8 6d f3 ff ff       	call   80029c <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800f2f:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800f35:	83 ec 04             	sub    $0x4,%esp
  800f38:	68 00 10 00 00       	push   $0x1000
  800f3d:	53                   	push   %ebx
  800f3e:	68 00 f0 7f 00       	push   $0x7ff000
  800f43:	e8 05 fb ff ff       	call   800a4d <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800f48:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800f4f:	53                   	push   %ebx
  800f50:	6a 00                	push   $0x0
  800f52:	68 00 f0 7f 00       	push   $0x7ff000
  800f57:	6a 00                	push   $0x0
  800f59:	e8 a1 fd ff ff       	call   800cff <sys_page_map>
  800f5e:	83 c4 20             	add    $0x20,%esp
  800f61:	85 c0                	test   %eax,%eax
  800f63:	74 14                	je     800f79 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800f65:	83 ec 04             	sub    $0x4,%esp
  800f68:	68 10 27 80 00       	push   $0x802710
  800f6d:	6a 3f                	push   $0x3f
  800f6f:	68 f0 26 80 00       	push   $0x8026f0
  800f74:	e8 23 f3 ff ff       	call   80029c <_panic>


	//panic("pgfault not implemented");
}
  800f79:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800f7c:	c9                   	leave  
  800f7d:	c3                   	ret    

00800f7e <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800f7e:	55                   	push   %ebp
  800f7f:	89 e5                	mov    %esp,%ebp
  800f81:	57                   	push   %edi
  800f82:	56                   	push   %esi
  800f83:	53                   	push   %ebx
  800f84:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800f87:	68 cc 0e 80 00       	push   $0x800ecc
  800f8c:	e8 b1 0e 00 00       	call   801e42 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800f91:	b8 07 00 00 00       	mov    $0x7,%eax
  800f96:	cd 30                	int    $0x30
  800f98:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800f9b:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800f9e:	83 c4 10             	add    $0x10,%esp
  800fa1:	85 c0                	test   %eax,%eax
  800fa3:	79 17                	jns    800fbc <fork+0x3e>
        panic("fork: cannot create child env");
  800fa5:	83 ec 04             	sub    $0x4,%esp
  800fa8:	68 23 27 80 00       	push   $0x802723
  800fad:	68 97 00 00 00       	push   $0x97
  800fb2:	68 f0 26 80 00       	push   $0x8026f0
  800fb7:	e8 e0 f2 ff ff       	call   80029c <_panic>
    } else if (envid == 0) {
  800fbc:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800fc0:	75 2a                	jne    800fec <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800fc2:	e8 b7 fc ff ff       	call   800c7e <sys_getenvid>
  800fc7:	25 ff 03 00 00       	and    $0x3ff,%eax
  800fcc:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800fd3:	c1 e0 07             	shl    $0x7,%eax
  800fd6:	29 d0                	sub    %edx,%eax
  800fd8:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800fdd:	a3 04 40 80 00       	mov    %eax,0x804004
        return 0;
  800fe2:	b8 00 00 00 00       	mov    $0x0,%eax
  800fe7:	e9 94 01 00 00       	jmp    801180 <fork+0x202>
  800fec:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800ff3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800ff6:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800ffd:	a8 01                	test   $0x1,%al
  800fff:	0f 84 0a 01 00 00    	je     80110f <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  801005:	c1 e7 0a             	shl    $0xa,%edi
  801008:	be 00 00 00 00       	mov    $0x0,%esi
  80100d:	89 fb                	mov    %edi,%ebx
  80100f:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  801011:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  801017:	75 34                	jne    80104d <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  801019:	83 ec 04             	sub    $0x4,%esp
  80101c:	6a 07                	push   $0x7
  80101e:	68 00 f0 bf ee       	push   $0xeebff000
  801023:	ff 75 e4             	pushl  -0x1c(%ebp)
  801026:	e8 91 fc ff ff       	call   800cbc <sys_page_alloc>
  80102b:	83 c4 10             	add    $0x10,%esp
  80102e:	85 c0                	test   %eax,%eax
  801030:	0f 84 cc 00 00 00    	je     801102 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  801036:	83 ec 04             	sub    $0x4,%esp
  801039:	68 41 27 80 00       	push   $0x802741
  80103e:	68 ad 00 00 00       	push   $0xad
  801043:	68 f0 26 80 00       	push   $0x8026f0
  801048:	e8 4f f2 ff ff       	call   80029c <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  80104d:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  801054:	a8 01                	test   $0x1,%al
  801056:	0f 84 a6 00 00 00    	je     801102 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  80105c:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  801063:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  801066:	a9 02 08 00 00       	test   $0x802,%eax
  80106b:	74 62                	je     8010cf <fork+0x151>
  80106d:	f6 c4 04             	test   $0x4,%ah
  801070:	75 78                	jne    8010ea <fork+0x16c>
        if (sys_page_map(0,
  801072:	83 ec 0c             	sub    $0xc,%esp
  801075:	68 05 08 00 00       	push   $0x805
  80107a:	53                   	push   %ebx
  80107b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80107e:	53                   	push   %ebx
  80107f:	6a 00                	push   $0x0
  801081:	e8 79 fc ff ff       	call   800cff <sys_page_map>
  801086:	83 c4 20             	add    $0x20,%esp
  801089:	85 c0                	test   %eax,%eax
  80108b:	74 14                	je     8010a1 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  80108d:	83 ec 04             	sub    $0x4,%esp
  801090:	68 5c 27 80 00       	push   $0x80275c
  801095:	6a 64                	push   $0x64
  801097:	68 f0 26 80 00       	push   $0x8026f0
  80109c:	e8 fb f1 ff ff       	call   80029c <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  8010a1:	83 ec 0c             	sub    $0xc,%esp
  8010a4:	68 05 08 00 00       	push   $0x805
  8010a9:	53                   	push   %ebx
  8010aa:	6a 00                	push   $0x0
  8010ac:	53                   	push   %ebx
  8010ad:	6a 00                	push   $0x0
  8010af:	e8 4b fc ff ff       	call   800cff <sys_page_map>
  8010b4:	83 c4 20             	add    $0x20,%esp
  8010b7:	85 c0                	test   %eax,%eax
  8010b9:	74 47                	je     801102 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  8010bb:	83 ec 04             	sub    $0x4,%esp
  8010be:	68 73 27 80 00       	push   $0x802773
  8010c3:	6a 6d                	push   $0x6d
  8010c5:	68 f0 26 80 00       	push   $0x8026f0
  8010ca:	e8 cd f1 ff ff       	call   80029c <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  8010cf:	f6 c4 04             	test   $0x4,%ah
  8010d2:	75 16                	jne    8010ea <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  8010d4:	83 ec 0c             	sub    $0xc,%esp
  8010d7:	6a 05                	push   $0x5
  8010d9:	53                   	push   %ebx
  8010da:	ff 75 e4             	pushl  -0x1c(%ebp)
  8010dd:	53                   	push   %ebx
  8010de:	6a 00                	push   $0x0
  8010e0:	e8 1a fc ff ff       	call   800cff <sys_page_map>
  8010e5:	83 c4 20             	add    $0x20,%esp
  8010e8:	eb 18                	jmp    801102 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  8010ea:	83 ec 0c             	sub    $0xc,%esp
  8010ed:	25 07 0e 00 00       	and    $0xe07,%eax
  8010f2:	50                   	push   %eax
  8010f3:	53                   	push   %ebx
  8010f4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8010f7:	53                   	push   %ebx
  8010f8:	6a 00                	push   $0x0
  8010fa:	e8 00 fc ff ff       	call   800cff <sys_page_map>
  8010ff:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801102:	46                   	inc    %esi
  801103:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  801109:	0f 85 fe fe ff ff    	jne    80100d <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80110f:	ff 45 e0             	incl   -0x20(%ebp)
  801112:	8b 45 e0             	mov    -0x20(%ebp),%eax
  801115:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  80111a:	0f 85 d3 fe ff ff    	jne    800ff3 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  801120:	a1 04 40 80 00       	mov    0x804004,%eax
  801125:	8b 40 64             	mov    0x64(%eax),%eax
  801128:	83 ec 08             	sub    $0x8,%esp
  80112b:	50                   	push   %eax
  80112c:	ff 75 dc             	pushl  -0x24(%ebp)
  80112f:	e8 f2 fc ff ff       	call   800e26 <sys_env_set_pgfault_upcall>
  801134:	83 c4 10             	add    $0x10,%esp
  801137:	85 c0                	test   %eax,%eax
  801139:	74 17                	je     801152 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  80113b:	83 ec 04             	sub    $0x4,%esp
  80113e:	68 d0 26 80 00       	push   $0x8026d0
  801143:	68 b9 00 00 00       	push   $0xb9
  801148:	68 f0 26 80 00       	push   $0x8026f0
  80114d:	e8 4a f1 ff ff       	call   80029c <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  801152:	83 ec 08             	sub    $0x8,%esp
  801155:	6a 02                	push   $0x2
  801157:	ff 75 dc             	pushl  -0x24(%ebp)
  80115a:	e8 24 fc ff ff       	call   800d83 <sys_env_set_status>
  80115f:	83 c4 10             	add    $0x10,%esp
  801162:	85 c0                	test   %eax,%eax
  801164:	74 17                	je     80117d <fork+0x1ff>
        panic("fork: cannot set env status");
  801166:	83 ec 04             	sub    $0x4,%esp
  801169:	68 8e 27 80 00       	push   $0x80278e
  80116e:	68 bd 00 00 00       	push   $0xbd
  801173:	68 f0 26 80 00       	push   $0x8026f0
  801178:	e8 1f f1 ff ff       	call   80029c <_panic>

    return envid;
  80117d:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  801180:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801183:	5b                   	pop    %ebx
  801184:	5e                   	pop    %esi
  801185:	5f                   	pop    %edi
  801186:	5d                   	pop    %ebp
  801187:	c3                   	ret    

00801188 <sfork>:

// Challenge!
int
sfork(void)
{
  801188:	55                   	push   %ebp
  801189:	89 e5                	mov    %esp,%ebp
  80118b:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  80118e:	68 aa 27 80 00       	push   $0x8027aa
  801193:	68 c8 00 00 00       	push   $0xc8
  801198:	68 f0 26 80 00       	push   $0x8026f0
  80119d:	e8 fa f0 ff ff       	call   80029c <_panic>

008011a2 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  8011a2:	55                   	push   %ebp
  8011a3:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  8011a5:	8b 45 08             	mov    0x8(%ebp),%eax
  8011a8:	05 00 00 00 30       	add    $0x30000000,%eax
  8011ad:	c1 e8 0c             	shr    $0xc,%eax
}
  8011b0:	5d                   	pop    %ebp
  8011b1:	c3                   	ret    

008011b2 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  8011b2:	55                   	push   %ebp
  8011b3:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  8011b5:	8b 45 08             	mov    0x8(%ebp),%eax
  8011b8:	05 00 00 00 30       	add    $0x30000000,%eax
  8011bd:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  8011c2:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  8011c7:	5d                   	pop    %ebp
  8011c8:	c3                   	ret    

008011c9 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  8011c9:	55                   	push   %ebp
  8011ca:	89 e5                	mov    %esp,%ebp
  8011cc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8011cf:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  8011d4:	89 c2                	mov    %eax,%edx
  8011d6:	c1 ea 16             	shr    $0x16,%edx
  8011d9:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8011e0:	f6 c2 01             	test   $0x1,%dl
  8011e3:	74 11                	je     8011f6 <fd_alloc+0x2d>
  8011e5:	89 c2                	mov    %eax,%edx
  8011e7:	c1 ea 0c             	shr    $0xc,%edx
  8011ea:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8011f1:	f6 c2 01             	test   $0x1,%dl
  8011f4:	75 09                	jne    8011ff <fd_alloc+0x36>
			*fd_store = fd;
  8011f6:	89 01                	mov    %eax,(%ecx)
			return 0;
  8011f8:	b8 00 00 00 00       	mov    $0x0,%eax
  8011fd:	eb 17                	jmp    801216 <fd_alloc+0x4d>
  8011ff:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801204:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  801209:	75 c9                	jne    8011d4 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  80120b:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801211:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801216:	5d                   	pop    %ebp
  801217:	c3                   	ret    

00801218 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  801218:	55                   	push   %ebp
  801219:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  80121b:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  80121f:	77 39                	ja     80125a <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801221:	8b 45 08             	mov    0x8(%ebp),%eax
  801224:	c1 e0 0c             	shl    $0xc,%eax
  801227:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  80122c:	89 c2                	mov    %eax,%edx
  80122e:	c1 ea 16             	shr    $0x16,%edx
  801231:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801238:	f6 c2 01             	test   $0x1,%dl
  80123b:	74 24                	je     801261 <fd_lookup+0x49>
  80123d:	89 c2                	mov    %eax,%edx
  80123f:	c1 ea 0c             	shr    $0xc,%edx
  801242:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801249:	f6 c2 01             	test   $0x1,%dl
  80124c:	74 1a                	je     801268 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  80124e:	8b 55 0c             	mov    0xc(%ebp),%edx
  801251:	89 02                	mov    %eax,(%edx)
	return 0;
  801253:	b8 00 00 00 00       	mov    $0x0,%eax
  801258:	eb 13                	jmp    80126d <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  80125a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80125f:	eb 0c                	jmp    80126d <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801261:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801266:	eb 05                	jmp    80126d <fd_lookup+0x55>
  801268:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  80126d:	5d                   	pop    %ebp
  80126e:	c3                   	ret    

0080126f <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  80126f:	55                   	push   %ebp
  801270:	89 e5                	mov    %esp,%ebp
  801272:	83 ec 08             	sub    $0x8,%esp
  801275:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801278:	ba 3c 28 80 00       	mov    $0x80283c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  80127d:	eb 13                	jmp    801292 <dev_lookup+0x23>
  80127f:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  801282:	39 08                	cmp    %ecx,(%eax)
  801284:	75 0c                	jne    801292 <dev_lookup+0x23>
			*dev = devtab[i];
  801286:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801289:	89 01                	mov    %eax,(%ecx)
			return 0;
  80128b:	b8 00 00 00 00       	mov    $0x0,%eax
  801290:	eb 2e                	jmp    8012c0 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801292:	8b 02                	mov    (%edx),%eax
  801294:	85 c0                	test   %eax,%eax
  801296:	75 e7                	jne    80127f <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  801298:	a1 04 40 80 00       	mov    0x804004,%eax
  80129d:	8b 40 48             	mov    0x48(%eax),%eax
  8012a0:	83 ec 04             	sub    $0x4,%esp
  8012a3:	51                   	push   %ecx
  8012a4:	50                   	push   %eax
  8012a5:	68 c0 27 80 00       	push   $0x8027c0
  8012aa:	e8 c5 f0 ff ff       	call   800374 <cprintf>
	*dev = 0;
  8012af:	8b 45 0c             	mov    0xc(%ebp),%eax
  8012b2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  8012b8:	83 c4 10             	add    $0x10,%esp
  8012bb:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  8012c0:	c9                   	leave  
  8012c1:	c3                   	ret    

008012c2 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  8012c2:	55                   	push   %ebp
  8012c3:	89 e5                	mov    %esp,%ebp
  8012c5:	56                   	push   %esi
  8012c6:	53                   	push   %ebx
  8012c7:	83 ec 10             	sub    $0x10,%esp
  8012ca:	8b 75 08             	mov    0x8(%ebp),%esi
  8012cd:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  8012d0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012d3:	50                   	push   %eax
  8012d4:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  8012da:	c1 e8 0c             	shr    $0xc,%eax
  8012dd:	50                   	push   %eax
  8012de:	e8 35 ff ff ff       	call   801218 <fd_lookup>
  8012e3:	83 c4 08             	add    $0x8,%esp
  8012e6:	85 c0                	test   %eax,%eax
  8012e8:	78 05                	js     8012ef <fd_close+0x2d>
	    || fd != fd2)
  8012ea:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  8012ed:	74 06                	je     8012f5 <fd_close+0x33>
		return (must_exist ? r : 0);
  8012ef:	84 db                	test   %bl,%bl
  8012f1:	74 47                	je     80133a <fd_close+0x78>
  8012f3:	eb 4a                	jmp    80133f <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  8012f5:	83 ec 08             	sub    $0x8,%esp
  8012f8:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8012fb:	50                   	push   %eax
  8012fc:	ff 36                	pushl  (%esi)
  8012fe:	e8 6c ff ff ff       	call   80126f <dev_lookup>
  801303:	89 c3                	mov    %eax,%ebx
  801305:	83 c4 10             	add    $0x10,%esp
  801308:	85 c0                	test   %eax,%eax
  80130a:	78 1c                	js     801328 <fd_close+0x66>
		if (dev->dev_close)
  80130c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80130f:	8b 40 10             	mov    0x10(%eax),%eax
  801312:	85 c0                	test   %eax,%eax
  801314:	74 0d                	je     801323 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801316:	83 ec 0c             	sub    $0xc,%esp
  801319:	56                   	push   %esi
  80131a:	ff d0                	call   *%eax
  80131c:	89 c3                	mov    %eax,%ebx
  80131e:	83 c4 10             	add    $0x10,%esp
  801321:	eb 05                	jmp    801328 <fd_close+0x66>
		else
			r = 0;
  801323:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  801328:	83 ec 08             	sub    $0x8,%esp
  80132b:	56                   	push   %esi
  80132c:	6a 00                	push   $0x0
  80132e:	e8 0e fa ff ff       	call   800d41 <sys_page_unmap>
	return r;
  801333:	83 c4 10             	add    $0x10,%esp
  801336:	89 d8                	mov    %ebx,%eax
  801338:	eb 05                	jmp    80133f <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  80133a:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  80133f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801342:	5b                   	pop    %ebx
  801343:	5e                   	pop    %esi
  801344:	5d                   	pop    %ebp
  801345:	c3                   	ret    

00801346 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  801346:	55                   	push   %ebp
  801347:	89 e5                	mov    %esp,%ebp
  801349:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80134c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80134f:	50                   	push   %eax
  801350:	ff 75 08             	pushl  0x8(%ebp)
  801353:	e8 c0 fe ff ff       	call   801218 <fd_lookup>
  801358:	83 c4 08             	add    $0x8,%esp
  80135b:	85 c0                	test   %eax,%eax
  80135d:	78 10                	js     80136f <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  80135f:	83 ec 08             	sub    $0x8,%esp
  801362:	6a 01                	push   $0x1
  801364:	ff 75 f4             	pushl  -0xc(%ebp)
  801367:	e8 56 ff ff ff       	call   8012c2 <fd_close>
  80136c:	83 c4 10             	add    $0x10,%esp
}
  80136f:	c9                   	leave  
  801370:	c3                   	ret    

00801371 <close_all>:

void
close_all(void)
{
  801371:	55                   	push   %ebp
  801372:	89 e5                	mov    %esp,%ebp
  801374:	53                   	push   %ebx
  801375:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  801378:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  80137d:	83 ec 0c             	sub    $0xc,%esp
  801380:	53                   	push   %ebx
  801381:	e8 c0 ff ff ff       	call   801346 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  801386:	43                   	inc    %ebx
  801387:	83 c4 10             	add    $0x10,%esp
  80138a:	83 fb 20             	cmp    $0x20,%ebx
  80138d:	75 ee                	jne    80137d <close_all+0xc>
		close(i);
}
  80138f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801392:	c9                   	leave  
  801393:	c3                   	ret    

00801394 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801394:	55                   	push   %ebp
  801395:	89 e5                	mov    %esp,%ebp
  801397:	57                   	push   %edi
  801398:	56                   	push   %esi
  801399:	53                   	push   %ebx
  80139a:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  80139d:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8013a0:	50                   	push   %eax
  8013a1:	ff 75 08             	pushl  0x8(%ebp)
  8013a4:	e8 6f fe ff ff       	call   801218 <fd_lookup>
  8013a9:	83 c4 08             	add    $0x8,%esp
  8013ac:	85 c0                	test   %eax,%eax
  8013ae:	0f 88 c2 00 00 00    	js     801476 <dup+0xe2>
		return r;
	close(newfdnum);
  8013b4:	83 ec 0c             	sub    $0xc,%esp
  8013b7:	ff 75 0c             	pushl  0xc(%ebp)
  8013ba:	e8 87 ff ff ff       	call   801346 <close>

	newfd = INDEX2FD(newfdnum);
  8013bf:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8013c2:	c1 e3 0c             	shl    $0xc,%ebx
  8013c5:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  8013cb:	83 c4 04             	add    $0x4,%esp
  8013ce:	ff 75 e4             	pushl  -0x1c(%ebp)
  8013d1:	e8 dc fd ff ff       	call   8011b2 <fd2data>
  8013d6:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  8013d8:	89 1c 24             	mov    %ebx,(%esp)
  8013db:	e8 d2 fd ff ff       	call   8011b2 <fd2data>
  8013e0:	83 c4 10             	add    $0x10,%esp
  8013e3:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8013e5:	89 f0                	mov    %esi,%eax
  8013e7:	c1 e8 16             	shr    $0x16,%eax
  8013ea:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8013f1:	a8 01                	test   $0x1,%al
  8013f3:	74 35                	je     80142a <dup+0x96>
  8013f5:	89 f0                	mov    %esi,%eax
  8013f7:	c1 e8 0c             	shr    $0xc,%eax
  8013fa:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801401:	f6 c2 01             	test   $0x1,%dl
  801404:	74 24                	je     80142a <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801406:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80140d:	83 ec 0c             	sub    $0xc,%esp
  801410:	25 07 0e 00 00       	and    $0xe07,%eax
  801415:	50                   	push   %eax
  801416:	57                   	push   %edi
  801417:	6a 00                	push   $0x0
  801419:	56                   	push   %esi
  80141a:	6a 00                	push   $0x0
  80141c:	e8 de f8 ff ff       	call   800cff <sys_page_map>
  801421:	89 c6                	mov    %eax,%esi
  801423:	83 c4 20             	add    $0x20,%esp
  801426:	85 c0                	test   %eax,%eax
  801428:	78 2c                	js     801456 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  80142a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  80142d:	89 d0                	mov    %edx,%eax
  80142f:	c1 e8 0c             	shr    $0xc,%eax
  801432:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801439:	83 ec 0c             	sub    $0xc,%esp
  80143c:	25 07 0e 00 00       	and    $0xe07,%eax
  801441:	50                   	push   %eax
  801442:	53                   	push   %ebx
  801443:	6a 00                	push   $0x0
  801445:	52                   	push   %edx
  801446:	6a 00                	push   $0x0
  801448:	e8 b2 f8 ff ff       	call   800cff <sys_page_map>
  80144d:	89 c6                	mov    %eax,%esi
  80144f:	83 c4 20             	add    $0x20,%esp
  801452:	85 c0                	test   %eax,%eax
  801454:	79 1d                	jns    801473 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801456:	83 ec 08             	sub    $0x8,%esp
  801459:	53                   	push   %ebx
  80145a:	6a 00                	push   $0x0
  80145c:	e8 e0 f8 ff ff       	call   800d41 <sys_page_unmap>
	sys_page_unmap(0, nva);
  801461:	83 c4 08             	add    $0x8,%esp
  801464:	57                   	push   %edi
  801465:	6a 00                	push   $0x0
  801467:	e8 d5 f8 ff ff       	call   800d41 <sys_page_unmap>
	return r;
  80146c:	83 c4 10             	add    $0x10,%esp
  80146f:	89 f0                	mov    %esi,%eax
  801471:	eb 03                	jmp    801476 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801473:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801476:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801479:	5b                   	pop    %ebx
  80147a:	5e                   	pop    %esi
  80147b:	5f                   	pop    %edi
  80147c:	5d                   	pop    %ebp
  80147d:	c3                   	ret    

0080147e <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  80147e:	55                   	push   %ebp
  80147f:	89 e5                	mov    %esp,%ebp
  801481:	53                   	push   %ebx
  801482:	83 ec 14             	sub    $0x14,%esp
  801485:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801488:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80148b:	50                   	push   %eax
  80148c:	53                   	push   %ebx
  80148d:	e8 86 fd ff ff       	call   801218 <fd_lookup>
  801492:	83 c4 08             	add    $0x8,%esp
  801495:	85 c0                	test   %eax,%eax
  801497:	78 67                	js     801500 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801499:	83 ec 08             	sub    $0x8,%esp
  80149c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80149f:	50                   	push   %eax
  8014a0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8014a3:	ff 30                	pushl  (%eax)
  8014a5:	e8 c5 fd ff ff       	call   80126f <dev_lookup>
  8014aa:	83 c4 10             	add    $0x10,%esp
  8014ad:	85 c0                	test   %eax,%eax
  8014af:	78 4f                	js     801500 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8014b1:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8014b4:	8b 42 08             	mov    0x8(%edx),%eax
  8014b7:	83 e0 03             	and    $0x3,%eax
  8014ba:	83 f8 01             	cmp    $0x1,%eax
  8014bd:	75 21                	jne    8014e0 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  8014bf:	a1 04 40 80 00       	mov    0x804004,%eax
  8014c4:	8b 40 48             	mov    0x48(%eax),%eax
  8014c7:	83 ec 04             	sub    $0x4,%esp
  8014ca:	53                   	push   %ebx
  8014cb:	50                   	push   %eax
  8014cc:	68 01 28 80 00       	push   $0x802801
  8014d1:	e8 9e ee ff ff       	call   800374 <cprintf>
		return -E_INVAL;
  8014d6:	83 c4 10             	add    $0x10,%esp
  8014d9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8014de:	eb 20                	jmp    801500 <read+0x82>
	}
	if (!dev->dev_read)
  8014e0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8014e3:	8b 40 08             	mov    0x8(%eax),%eax
  8014e6:	85 c0                	test   %eax,%eax
  8014e8:	74 11                	je     8014fb <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  8014ea:	83 ec 04             	sub    $0x4,%esp
  8014ed:	ff 75 10             	pushl  0x10(%ebp)
  8014f0:	ff 75 0c             	pushl  0xc(%ebp)
  8014f3:	52                   	push   %edx
  8014f4:	ff d0                	call   *%eax
  8014f6:	83 c4 10             	add    $0x10,%esp
  8014f9:	eb 05                	jmp    801500 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  8014fb:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801500:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801503:	c9                   	leave  
  801504:	c3                   	ret    

00801505 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801505:	55                   	push   %ebp
  801506:	89 e5                	mov    %esp,%ebp
  801508:	57                   	push   %edi
  801509:	56                   	push   %esi
  80150a:	53                   	push   %ebx
  80150b:	83 ec 0c             	sub    $0xc,%esp
  80150e:	8b 7d 08             	mov    0x8(%ebp),%edi
  801511:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801514:	bb 00 00 00 00       	mov    $0x0,%ebx
  801519:	eb 21                	jmp    80153c <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  80151b:	83 ec 04             	sub    $0x4,%esp
  80151e:	89 f0                	mov    %esi,%eax
  801520:	29 d8                	sub    %ebx,%eax
  801522:	50                   	push   %eax
  801523:	89 d8                	mov    %ebx,%eax
  801525:	03 45 0c             	add    0xc(%ebp),%eax
  801528:	50                   	push   %eax
  801529:	57                   	push   %edi
  80152a:	e8 4f ff ff ff       	call   80147e <read>
		if (m < 0)
  80152f:	83 c4 10             	add    $0x10,%esp
  801532:	85 c0                	test   %eax,%eax
  801534:	78 10                	js     801546 <readn+0x41>
			return m;
		if (m == 0)
  801536:	85 c0                	test   %eax,%eax
  801538:	74 0a                	je     801544 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80153a:	01 c3                	add    %eax,%ebx
  80153c:	39 f3                	cmp    %esi,%ebx
  80153e:	72 db                	jb     80151b <readn+0x16>
  801540:	89 d8                	mov    %ebx,%eax
  801542:	eb 02                	jmp    801546 <readn+0x41>
  801544:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801546:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801549:	5b                   	pop    %ebx
  80154a:	5e                   	pop    %esi
  80154b:	5f                   	pop    %edi
  80154c:	5d                   	pop    %ebp
  80154d:	c3                   	ret    

0080154e <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  80154e:	55                   	push   %ebp
  80154f:	89 e5                	mov    %esp,%ebp
  801551:	53                   	push   %ebx
  801552:	83 ec 14             	sub    $0x14,%esp
  801555:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801558:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80155b:	50                   	push   %eax
  80155c:	53                   	push   %ebx
  80155d:	e8 b6 fc ff ff       	call   801218 <fd_lookup>
  801562:	83 c4 08             	add    $0x8,%esp
  801565:	85 c0                	test   %eax,%eax
  801567:	78 62                	js     8015cb <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801569:	83 ec 08             	sub    $0x8,%esp
  80156c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80156f:	50                   	push   %eax
  801570:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801573:	ff 30                	pushl  (%eax)
  801575:	e8 f5 fc ff ff       	call   80126f <dev_lookup>
  80157a:	83 c4 10             	add    $0x10,%esp
  80157d:	85 c0                	test   %eax,%eax
  80157f:	78 4a                	js     8015cb <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801581:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801584:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801588:	75 21                	jne    8015ab <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  80158a:	a1 04 40 80 00       	mov    0x804004,%eax
  80158f:	8b 40 48             	mov    0x48(%eax),%eax
  801592:	83 ec 04             	sub    $0x4,%esp
  801595:	53                   	push   %ebx
  801596:	50                   	push   %eax
  801597:	68 1d 28 80 00       	push   $0x80281d
  80159c:	e8 d3 ed ff ff       	call   800374 <cprintf>
		return -E_INVAL;
  8015a1:	83 c4 10             	add    $0x10,%esp
  8015a4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8015a9:	eb 20                	jmp    8015cb <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8015ab:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8015ae:	8b 52 0c             	mov    0xc(%edx),%edx
  8015b1:	85 d2                	test   %edx,%edx
  8015b3:	74 11                	je     8015c6 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8015b5:	83 ec 04             	sub    $0x4,%esp
  8015b8:	ff 75 10             	pushl  0x10(%ebp)
  8015bb:	ff 75 0c             	pushl  0xc(%ebp)
  8015be:	50                   	push   %eax
  8015bf:	ff d2                	call   *%edx
  8015c1:	83 c4 10             	add    $0x10,%esp
  8015c4:	eb 05                	jmp    8015cb <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8015c6:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8015cb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015ce:	c9                   	leave  
  8015cf:	c3                   	ret    

008015d0 <seek>:

int
seek(int fdnum, off_t offset)
{
  8015d0:	55                   	push   %ebp
  8015d1:	89 e5                	mov    %esp,%ebp
  8015d3:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8015d6:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8015d9:	50                   	push   %eax
  8015da:	ff 75 08             	pushl  0x8(%ebp)
  8015dd:	e8 36 fc ff ff       	call   801218 <fd_lookup>
  8015e2:	83 c4 08             	add    $0x8,%esp
  8015e5:	85 c0                	test   %eax,%eax
  8015e7:	78 0e                	js     8015f7 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  8015e9:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8015ec:	8b 55 0c             	mov    0xc(%ebp),%edx
  8015ef:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  8015f2:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8015f7:	c9                   	leave  
  8015f8:	c3                   	ret    

008015f9 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  8015f9:	55                   	push   %ebp
  8015fa:	89 e5                	mov    %esp,%ebp
  8015fc:	53                   	push   %ebx
  8015fd:	83 ec 14             	sub    $0x14,%esp
  801600:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801603:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801606:	50                   	push   %eax
  801607:	53                   	push   %ebx
  801608:	e8 0b fc ff ff       	call   801218 <fd_lookup>
  80160d:	83 c4 08             	add    $0x8,%esp
  801610:	85 c0                	test   %eax,%eax
  801612:	78 5f                	js     801673 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801614:	83 ec 08             	sub    $0x8,%esp
  801617:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80161a:	50                   	push   %eax
  80161b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80161e:	ff 30                	pushl  (%eax)
  801620:	e8 4a fc ff ff       	call   80126f <dev_lookup>
  801625:	83 c4 10             	add    $0x10,%esp
  801628:	85 c0                	test   %eax,%eax
  80162a:	78 47                	js     801673 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80162c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80162f:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801633:	75 21                	jne    801656 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801635:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80163a:	8b 40 48             	mov    0x48(%eax),%eax
  80163d:	83 ec 04             	sub    $0x4,%esp
  801640:	53                   	push   %ebx
  801641:	50                   	push   %eax
  801642:	68 e0 27 80 00       	push   $0x8027e0
  801647:	e8 28 ed ff ff       	call   800374 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  80164c:	83 c4 10             	add    $0x10,%esp
  80164f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801654:	eb 1d                	jmp    801673 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  801656:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801659:	8b 52 18             	mov    0x18(%edx),%edx
  80165c:	85 d2                	test   %edx,%edx
  80165e:	74 0e                	je     80166e <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  801660:	83 ec 08             	sub    $0x8,%esp
  801663:	ff 75 0c             	pushl  0xc(%ebp)
  801666:	50                   	push   %eax
  801667:	ff d2                	call   *%edx
  801669:	83 c4 10             	add    $0x10,%esp
  80166c:	eb 05                	jmp    801673 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  80166e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  801673:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801676:	c9                   	leave  
  801677:	c3                   	ret    

00801678 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  801678:	55                   	push   %ebp
  801679:	89 e5                	mov    %esp,%ebp
  80167b:	53                   	push   %ebx
  80167c:	83 ec 14             	sub    $0x14,%esp
  80167f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801682:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801685:	50                   	push   %eax
  801686:	ff 75 08             	pushl  0x8(%ebp)
  801689:	e8 8a fb ff ff       	call   801218 <fd_lookup>
  80168e:	83 c4 08             	add    $0x8,%esp
  801691:	85 c0                	test   %eax,%eax
  801693:	78 52                	js     8016e7 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801695:	83 ec 08             	sub    $0x8,%esp
  801698:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80169b:	50                   	push   %eax
  80169c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80169f:	ff 30                	pushl  (%eax)
  8016a1:	e8 c9 fb ff ff       	call   80126f <dev_lookup>
  8016a6:	83 c4 10             	add    $0x10,%esp
  8016a9:	85 c0                	test   %eax,%eax
  8016ab:	78 3a                	js     8016e7 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8016ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8016b0:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8016b4:	74 2c                	je     8016e2 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8016b6:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8016b9:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8016c0:	00 00 00 
	stat->st_isdir = 0;
  8016c3:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8016ca:	00 00 00 
	stat->st_dev = dev;
  8016cd:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8016d3:	83 ec 08             	sub    $0x8,%esp
  8016d6:	53                   	push   %ebx
  8016d7:	ff 75 f0             	pushl  -0x10(%ebp)
  8016da:	ff 50 14             	call   *0x14(%eax)
  8016dd:	83 c4 10             	add    $0x10,%esp
  8016e0:	eb 05                	jmp    8016e7 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8016e2:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8016e7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8016ea:	c9                   	leave  
  8016eb:	c3                   	ret    

008016ec <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8016ec:	55                   	push   %ebp
  8016ed:	89 e5                	mov    %esp,%ebp
  8016ef:	56                   	push   %esi
  8016f0:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8016f1:	83 ec 08             	sub    $0x8,%esp
  8016f4:	6a 00                	push   $0x0
  8016f6:	ff 75 08             	pushl  0x8(%ebp)
  8016f9:	e8 e7 01 00 00       	call   8018e5 <open>
  8016fe:	89 c3                	mov    %eax,%ebx
  801700:	83 c4 10             	add    $0x10,%esp
  801703:	85 c0                	test   %eax,%eax
  801705:	78 1d                	js     801724 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801707:	83 ec 08             	sub    $0x8,%esp
  80170a:	ff 75 0c             	pushl  0xc(%ebp)
  80170d:	50                   	push   %eax
  80170e:	e8 65 ff ff ff       	call   801678 <fstat>
  801713:	89 c6                	mov    %eax,%esi
	close(fd);
  801715:	89 1c 24             	mov    %ebx,(%esp)
  801718:	e8 29 fc ff ff       	call   801346 <close>
	return r;
  80171d:	83 c4 10             	add    $0x10,%esp
  801720:	89 f0                	mov    %esi,%eax
  801722:	eb 00                	jmp    801724 <stat+0x38>
}
  801724:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801727:	5b                   	pop    %ebx
  801728:	5e                   	pop    %esi
  801729:	5d                   	pop    %ebp
  80172a:	c3                   	ret    

0080172b <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  80172b:	55                   	push   %ebp
  80172c:	89 e5                	mov    %esp,%ebp
  80172e:	56                   	push   %esi
  80172f:	53                   	push   %ebx
  801730:	89 c6                	mov    %eax,%esi
  801732:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801734:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  80173b:	75 12                	jne    80174f <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  80173d:	83 ec 0c             	sub    $0xc,%esp
  801740:	6a 01                	push   $0x1
  801742:	e8 37 08 00 00       	call   801f7e <ipc_find_env>
  801747:	a3 00 40 80 00       	mov    %eax,0x804000
  80174c:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  80174f:	6a 07                	push   $0x7
  801751:	68 00 50 80 00       	push   $0x805000
  801756:	56                   	push   %esi
  801757:	ff 35 00 40 80 00    	pushl  0x804000
  80175d:	e8 c7 07 00 00       	call   801f29 <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  801762:	83 c4 0c             	add    $0xc,%esp
  801765:	6a 00                	push   $0x0
  801767:	53                   	push   %ebx
  801768:	6a 00                	push   $0x0
  80176a:	e8 52 07 00 00       	call   801ec1 <ipc_recv>
}
  80176f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801772:	5b                   	pop    %ebx
  801773:	5e                   	pop    %esi
  801774:	5d                   	pop    %ebp
  801775:	c3                   	ret    

00801776 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801776:	55                   	push   %ebp
  801777:	89 e5                	mov    %esp,%ebp
  801779:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  80177c:	8b 45 08             	mov    0x8(%ebp),%eax
  80177f:	8b 40 0c             	mov    0xc(%eax),%eax
  801782:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  801787:	8b 45 0c             	mov    0xc(%ebp),%eax
  80178a:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  80178f:	ba 00 00 00 00       	mov    $0x0,%edx
  801794:	b8 02 00 00 00       	mov    $0x2,%eax
  801799:	e8 8d ff ff ff       	call   80172b <fsipc>
}
  80179e:	c9                   	leave  
  80179f:	c3                   	ret    

008017a0 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8017a0:	55                   	push   %ebp
  8017a1:	89 e5                	mov    %esp,%ebp
  8017a3:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8017a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8017a9:	8b 40 0c             	mov    0xc(%eax),%eax
  8017ac:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  8017b1:	ba 00 00 00 00       	mov    $0x0,%edx
  8017b6:	b8 06 00 00 00       	mov    $0x6,%eax
  8017bb:	e8 6b ff ff ff       	call   80172b <fsipc>
}
  8017c0:	c9                   	leave  
  8017c1:	c3                   	ret    

008017c2 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  8017c2:	55                   	push   %ebp
  8017c3:	89 e5                	mov    %esp,%ebp
  8017c5:	53                   	push   %ebx
  8017c6:	83 ec 04             	sub    $0x4,%esp
  8017c9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8017cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8017cf:	8b 40 0c             	mov    0xc(%eax),%eax
  8017d2:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8017d7:	ba 00 00 00 00       	mov    $0x0,%edx
  8017dc:	b8 05 00 00 00       	mov    $0x5,%eax
  8017e1:	e8 45 ff ff ff       	call   80172b <fsipc>
  8017e6:	85 c0                	test   %eax,%eax
  8017e8:	78 2c                	js     801816 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  8017ea:	83 ec 08             	sub    $0x8,%esp
  8017ed:	68 00 50 80 00       	push   $0x805000
  8017f2:	53                   	push   %ebx
  8017f3:	e8 e0 f0 ff ff       	call   8008d8 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  8017f8:	a1 80 50 80 00       	mov    0x805080,%eax
  8017fd:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801803:	a1 84 50 80 00       	mov    0x805084,%eax
  801808:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  80180e:	83 c4 10             	add    $0x10,%esp
  801811:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801816:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801819:	c9                   	leave  
  80181a:	c3                   	ret    

0080181b <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  80181b:	55                   	push   %ebp
  80181c:	89 e5                	mov    %esp,%ebp
  80181e:	83 ec 08             	sub    $0x8,%esp
  801821:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801824:	8b 55 08             	mov    0x8(%ebp),%edx
  801827:	8b 52 0c             	mov    0xc(%edx),%edx
  80182a:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801830:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801835:	76 05                	jbe    80183c <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801837:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  80183c:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801841:	83 ec 04             	sub    $0x4,%esp
  801844:	50                   	push   %eax
  801845:	ff 75 0c             	pushl  0xc(%ebp)
  801848:	68 08 50 80 00       	push   $0x805008
  80184d:	e8 fb f1 ff ff       	call   800a4d <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801852:	ba 00 00 00 00       	mov    $0x0,%edx
  801857:	b8 04 00 00 00       	mov    $0x4,%eax
  80185c:	e8 ca fe ff ff       	call   80172b <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801861:	c9                   	leave  
  801862:	c3                   	ret    

00801863 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801863:	55                   	push   %ebp
  801864:	89 e5                	mov    %esp,%ebp
  801866:	56                   	push   %esi
  801867:	53                   	push   %ebx
  801868:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  80186b:	8b 45 08             	mov    0x8(%ebp),%eax
  80186e:	8b 40 0c             	mov    0xc(%eax),%eax
  801871:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  801876:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  80187c:	ba 00 00 00 00       	mov    $0x0,%edx
  801881:	b8 03 00 00 00       	mov    $0x3,%eax
  801886:	e8 a0 fe ff ff       	call   80172b <fsipc>
  80188b:	89 c3                	mov    %eax,%ebx
  80188d:	85 c0                	test   %eax,%eax
  80188f:	78 4b                	js     8018dc <devfile_read+0x79>
		return r;
	assert(r <= n);
  801891:	39 c6                	cmp    %eax,%esi
  801893:	73 16                	jae    8018ab <devfile_read+0x48>
  801895:	68 4c 28 80 00       	push   $0x80284c
  80189a:	68 53 28 80 00       	push   $0x802853
  80189f:	6a 7c                	push   $0x7c
  8018a1:	68 68 28 80 00       	push   $0x802868
  8018a6:	e8 f1 e9 ff ff       	call   80029c <_panic>
	assert(r <= PGSIZE);
  8018ab:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8018b0:	7e 16                	jle    8018c8 <devfile_read+0x65>
  8018b2:	68 73 28 80 00       	push   $0x802873
  8018b7:	68 53 28 80 00       	push   $0x802853
  8018bc:	6a 7d                	push   $0x7d
  8018be:	68 68 28 80 00       	push   $0x802868
  8018c3:	e8 d4 e9 ff ff       	call   80029c <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  8018c8:	83 ec 04             	sub    $0x4,%esp
  8018cb:	50                   	push   %eax
  8018cc:	68 00 50 80 00       	push   $0x805000
  8018d1:	ff 75 0c             	pushl  0xc(%ebp)
  8018d4:	e8 74 f1 ff ff       	call   800a4d <memmove>
	return r;
  8018d9:	83 c4 10             	add    $0x10,%esp
}
  8018dc:	89 d8                	mov    %ebx,%eax
  8018de:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8018e1:	5b                   	pop    %ebx
  8018e2:	5e                   	pop    %esi
  8018e3:	5d                   	pop    %ebp
  8018e4:	c3                   	ret    

008018e5 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  8018e5:	55                   	push   %ebp
  8018e6:	89 e5                	mov    %esp,%ebp
  8018e8:	53                   	push   %ebx
  8018e9:	83 ec 20             	sub    $0x20,%esp
  8018ec:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  8018ef:	53                   	push   %ebx
  8018f0:	e8 ae ef ff ff       	call   8008a3 <strlen>
  8018f5:	83 c4 10             	add    $0x10,%esp
  8018f8:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  8018fd:	7f 63                	jg     801962 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  8018ff:	83 ec 0c             	sub    $0xc,%esp
  801902:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801905:	50                   	push   %eax
  801906:	e8 be f8 ff ff       	call   8011c9 <fd_alloc>
  80190b:	83 c4 10             	add    $0x10,%esp
  80190e:	85 c0                	test   %eax,%eax
  801910:	78 55                	js     801967 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801912:	83 ec 08             	sub    $0x8,%esp
  801915:	53                   	push   %ebx
  801916:	68 00 50 80 00       	push   $0x805000
  80191b:	e8 b8 ef ff ff       	call   8008d8 <strcpy>
	fsipcbuf.open.req_omode = mode;
  801920:	8b 45 0c             	mov    0xc(%ebp),%eax
  801923:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801928:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80192b:	b8 01 00 00 00       	mov    $0x1,%eax
  801930:	e8 f6 fd ff ff       	call   80172b <fsipc>
  801935:	89 c3                	mov    %eax,%ebx
  801937:	83 c4 10             	add    $0x10,%esp
  80193a:	85 c0                	test   %eax,%eax
  80193c:	79 14                	jns    801952 <open+0x6d>
		fd_close(fd, 0);
  80193e:	83 ec 08             	sub    $0x8,%esp
  801941:	6a 00                	push   $0x0
  801943:	ff 75 f4             	pushl  -0xc(%ebp)
  801946:	e8 77 f9 ff ff       	call   8012c2 <fd_close>
		return r;
  80194b:	83 c4 10             	add    $0x10,%esp
  80194e:	89 d8                	mov    %ebx,%eax
  801950:	eb 15                	jmp    801967 <open+0x82>
	}

	return fd2num(fd);
  801952:	83 ec 0c             	sub    $0xc,%esp
  801955:	ff 75 f4             	pushl  -0xc(%ebp)
  801958:	e8 45 f8 ff ff       	call   8011a2 <fd2num>
  80195d:	83 c4 10             	add    $0x10,%esp
  801960:	eb 05                	jmp    801967 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801962:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801967:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80196a:	c9                   	leave  
  80196b:	c3                   	ret    

0080196c <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  80196c:	55                   	push   %ebp
  80196d:	89 e5                	mov    %esp,%ebp
  80196f:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801972:	ba 00 00 00 00       	mov    $0x0,%edx
  801977:	b8 08 00 00 00       	mov    $0x8,%eax
  80197c:	e8 aa fd ff ff       	call   80172b <fsipc>
}
  801981:	c9                   	leave  
  801982:	c3                   	ret    

00801983 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801983:	55                   	push   %ebp
  801984:	89 e5                	mov    %esp,%ebp
  801986:	56                   	push   %esi
  801987:	53                   	push   %ebx
  801988:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  80198b:	83 ec 0c             	sub    $0xc,%esp
  80198e:	ff 75 08             	pushl  0x8(%ebp)
  801991:	e8 1c f8 ff ff       	call   8011b2 <fd2data>
  801996:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801998:	83 c4 08             	add    $0x8,%esp
  80199b:	68 7f 28 80 00       	push   $0x80287f
  8019a0:	53                   	push   %ebx
  8019a1:	e8 32 ef ff ff       	call   8008d8 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  8019a6:	8b 46 04             	mov    0x4(%esi),%eax
  8019a9:	2b 06                	sub    (%esi),%eax
  8019ab:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  8019b1:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8019b8:	00 00 00 
	stat->st_dev = &devpipe;
  8019bb:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  8019c2:	30 80 00 
	return 0;
}
  8019c5:	b8 00 00 00 00       	mov    $0x0,%eax
  8019ca:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8019cd:	5b                   	pop    %ebx
  8019ce:	5e                   	pop    %esi
  8019cf:	5d                   	pop    %ebp
  8019d0:	c3                   	ret    

008019d1 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  8019d1:	55                   	push   %ebp
  8019d2:	89 e5                	mov    %esp,%ebp
  8019d4:	53                   	push   %ebx
  8019d5:	83 ec 0c             	sub    $0xc,%esp
  8019d8:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  8019db:	53                   	push   %ebx
  8019dc:	6a 00                	push   $0x0
  8019de:	e8 5e f3 ff ff       	call   800d41 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  8019e3:	89 1c 24             	mov    %ebx,(%esp)
  8019e6:	e8 c7 f7 ff ff       	call   8011b2 <fd2data>
  8019eb:	83 c4 08             	add    $0x8,%esp
  8019ee:	50                   	push   %eax
  8019ef:	6a 00                	push   $0x0
  8019f1:	e8 4b f3 ff ff       	call   800d41 <sys_page_unmap>
}
  8019f6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8019f9:	c9                   	leave  
  8019fa:	c3                   	ret    

008019fb <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  8019fb:	55                   	push   %ebp
  8019fc:	89 e5                	mov    %esp,%ebp
  8019fe:	57                   	push   %edi
  8019ff:	56                   	push   %esi
  801a00:	53                   	push   %ebx
  801a01:	83 ec 1c             	sub    $0x1c,%esp
  801a04:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801a07:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801a09:	a1 04 40 80 00       	mov    0x804004,%eax
  801a0e:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801a11:	83 ec 0c             	sub    $0xc,%esp
  801a14:	ff 75 e0             	pushl  -0x20(%ebp)
  801a17:	e8 a6 05 00 00       	call   801fc2 <pageref>
  801a1c:	89 c3                	mov    %eax,%ebx
  801a1e:	89 3c 24             	mov    %edi,(%esp)
  801a21:	e8 9c 05 00 00       	call   801fc2 <pageref>
  801a26:	83 c4 10             	add    $0x10,%esp
  801a29:	39 c3                	cmp    %eax,%ebx
  801a2b:	0f 94 c1             	sete   %cl
  801a2e:	0f b6 c9             	movzbl %cl,%ecx
  801a31:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801a34:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801a3a:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801a3d:	39 ce                	cmp    %ecx,%esi
  801a3f:	74 1b                	je     801a5c <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801a41:	39 c3                	cmp    %eax,%ebx
  801a43:	75 c4                	jne    801a09 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801a45:	8b 42 58             	mov    0x58(%edx),%eax
  801a48:	ff 75 e4             	pushl  -0x1c(%ebp)
  801a4b:	50                   	push   %eax
  801a4c:	56                   	push   %esi
  801a4d:	68 86 28 80 00       	push   $0x802886
  801a52:	e8 1d e9 ff ff       	call   800374 <cprintf>
  801a57:	83 c4 10             	add    $0x10,%esp
  801a5a:	eb ad                	jmp    801a09 <_pipeisclosed+0xe>
	}
}
  801a5c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801a5f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a62:	5b                   	pop    %ebx
  801a63:	5e                   	pop    %esi
  801a64:	5f                   	pop    %edi
  801a65:	5d                   	pop    %ebp
  801a66:	c3                   	ret    

00801a67 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801a67:	55                   	push   %ebp
  801a68:	89 e5                	mov    %esp,%ebp
  801a6a:	57                   	push   %edi
  801a6b:	56                   	push   %esi
  801a6c:	53                   	push   %ebx
  801a6d:	83 ec 18             	sub    $0x18,%esp
  801a70:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801a73:	56                   	push   %esi
  801a74:	e8 39 f7 ff ff       	call   8011b2 <fd2data>
  801a79:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801a7b:	83 c4 10             	add    $0x10,%esp
  801a7e:	bf 00 00 00 00       	mov    $0x0,%edi
  801a83:	eb 3b                	jmp    801ac0 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801a85:	89 da                	mov    %ebx,%edx
  801a87:	89 f0                	mov    %esi,%eax
  801a89:	e8 6d ff ff ff       	call   8019fb <_pipeisclosed>
  801a8e:	85 c0                	test   %eax,%eax
  801a90:	75 38                	jne    801aca <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801a92:	e8 06 f2 ff ff       	call   800c9d <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801a97:	8b 53 04             	mov    0x4(%ebx),%edx
  801a9a:	8b 03                	mov    (%ebx),%eax
  801a9c:	83 c0 20             	add    $0x20,%eax
  801a9f:	39 c2                	cmp    %eax,%edx
  801aa1:	73 e2                	jae    801a85 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801aa3:	8b 45 0c             	mov    0xc(%ebp),%eax
  801aa6:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801aa9:	89 d0                	mov    %edx,%eax
  801aab:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801ab0:	79 05                	jns    801ab7 <devpipe_write+0x50>
  801ab2:	48                   	dec    %eax
  801ab3:	83 c8 e0             	or     $0xffffffe0,%eax
  801ab6:	40                   	inc    %eax
  801ab7:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801abb:	42                   	inc    %edx
  801abc:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801abf:	47                   	inc    %edi
  801ac0:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801ac3:	75 d2                	jne    801a97 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801ac5:	8b 45 10             	mov    0x10(%ebp),%eax
  801ac8:	eb 05                	jmp    801acf <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801aca:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801acf:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801ad2:	5b                   	pop    %ebx
  801ad3:	5e                   	pop    %esi
  801ad4:	5f                   	pop    %edi
  801ad5:	5d                   	pop    %ebp
  801ad6:	c3                   	ret    

00801ad7 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801ad7:	55                   	push   %ebp
  801ad8:	89 e5                	mov    %esp,%ebp
  801ada:	57                   	push   %edi
  801adb:	56                   	push   %esi
  801adc:	53                   	push   %ebx
  801add:	83 ec 18             	sub    $0x18,%esp
  801ae0:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801ae3:	57                   	push   %edi
  801ae4:	e8 c9 f6 ff ff       	call   8011b2 <fd2data>
  801ae9:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801aeb:	83 c4 10             	add    $0x10,%esp
  801aee:	bb 00 00 00 00       	mov    $0x0,%ebx
  801af3:	eb 3a                	jmp    801b2f <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801af5:	85 db                	test   %ebx,%ebx
  801af7:	74 04                	je     801afd <devpipe_read+0x26>
				return i;
  801af9:	89 d8                	mov    %ebx,%eax
  801afb:	eb 41                	jmp    801b3e <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801afd:	89 f2                	mov    %esi,%edx
  801aff:	89 f8                	mov    %edi,%eax
  801b01:	e8 f5 fe ff ff       	call   8019fb <_pipeisclosed>
  801b06:	85 c0                	test   %eax,%eax
  801b08:	75 2f                	jne    801b39 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801b0a:	e8 8e f1 ff ff       	call   800c9d <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801b0f:	8b 06                	mov    (%esi),%eax
  801b11:	3b 46 04             	cmp    0x4(%esi),%eax
  801b14:	74 df                	je     801af5 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801b16:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801b1b:	79 05                	jns    801b22 <devpipe_read+0x4b>
  801b1d:	48                   	dec    %eax
  801b1e:	83 c8 e0             	or     $0xffffffe0,%eax
  801b21:	40                   	inc    %eax
  801b22:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801b26:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801b29:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801b2c:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801b2e:	43                   	inc    %ebx
  801b2f:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801b32:	75 db                	jne    801b0f <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801b34:	8b 45 10             	mov    0x10(%ebp),%eax
  801b37:	eb 05                	jmp    801b3e <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801b39:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801b3e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b41:	5b                   	pop    %ebx
  801b42:	5e                   	pop    %esi
  801b43:	5f                   	pop    %edi
  801b44:	5d                   	pop    %ebp
  801b45:	c3                   	ret    

00801b46 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801b46:	55                   	push   %ebp
  801b47:	89 e5                	mov    %esp,%ebp
  801b49:	56                   	push   %esi
  801b4a:	53                   	push   %ebx
  801b4b:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801b4e:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b51:	50                   	push   %eax
  801b52:	e8 72 f6 ff ff       	call   8011c9 <fd_alloc>
  801b57:	83 c4 10             	add    $0x10,%esp
  801b5a:	85 c0                	test   %eax,%eax
  801b5c:	0f 88 2a 01 00 00    	js     801c8c <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b62:	83 ec 04             	sub    $0x4,%esp
  801b65:	68 07 04 00 00       	push   $0x407
  801b6a:	ff 75 f4             	pushl  -0xc(%ebp)
  801b6d:	6a 00                	push   $0x0
  801b6f:	e8 48 f1 ff ff       	call   800cbc <sys_page_alloc>
  801b74:	83 c4 10             	add    $0x10,%esp
  801b77:	85 c0                	test   %eax,%eax
  801b79:	0f 88 0d 01 00 00    	js     801c8c <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801b7f:	83 ec 0c             	sub    $0xc,%esp
  801b82:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801b85:	50                   	push   %eax
  801b86:	e8 3e f6 ff ff       	call   8011c9 <fd_alloc>
  801b8b:	89 c3                	mov    %eax,%ebx
  801b8d:	83 c4 10             	add    $0x10,%esp
  801b90:	85 c0                	test   %eax,%eax
  801b92:	0f 88 e2 00 00 00    	js     801c7a <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801b98:	83 ec 04             	sub    $0x4,%esp
  801b9b:	68 07 04 00 00       	push   $0x407
  801ba0:	ff 75 f0             	pushl  -0x10(%ebp)
  801ba3:	6a 00                	push   $0x0
  801ba5:	e8 12 f1 ff ff       	call   800cbc <sys_page_alloc>
  801baa:	89 c3                	mov    %eax,%ebx
  801bac:	83 c4 10             	add    $0x10,%esp
  801baf:	85 c0                	test   %eax,%eax
  801bb1:	0f 88 c3 00 00 00    	js     801c7a <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801bb7:	83 ec 0c             	sub    $0xc,%esp
  801bba:	ff 75 f4             	pushl  -0xc(%ebp)
  801bbd:	e8 f0 f5 ff ff       	call   8011b2 <fd2data>
  801bc2:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801bc4:	83 c4 0c             	add    $0xc,%esp
  801bc7:	68 07 04 00 00       	push   $0x407
  801bcc:	50                   	push   %eax
  801bcd:	6a 00                	push   $0x0
  801bcf:	e8 e8 f0 ff ff       	call   800cbc <sys_page_alloc>
  801bd4:	89 c3                	mov    %eax,%ebx
  801bd6:	83 c4 10             	add    $0x10,%esp
  801bd9:	85 c0                	test   %eax,%eax
  801bdb:	0f 88 89 00 00 00    	js     801c6a <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801be1:	83 ec 0c             	sub    $0xc,%esp
  801be4:	ff 75 f0             	pushl  -0x10(%ebp)
  801be7:	e8 c6 f5 ff ff       	call   8011b2 <fd2data>
  801bec:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801bf3:	50                   	push   %eax
  801bf4:	6a 00                	push   $0x0
  801bf6:	56                   	push   %esi
  801bf7:	6a 00                	push   $0x0
  801bf9:	e8 01 f1 ff ff       	call   800cff <sys_page_map>
  801bfe:	89 c3                	mov    %eax,%ebx
  801c00:	83 c4 20             	add    $0x20,%esp
  801c03:	85 c0                	test   %eax,%eax
  801c05:	78 55                	js     801c5c <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801c07:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801c0d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c10:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801c12:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801c15:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801c1c:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801c22:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c25:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801c27:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801c2a:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801c31:	83 ec 0c             	sub    $0xc,%esp
  801c34:	ff 75 f4             	pushl  -0xc(%ebp)
  801c37:	e8 66 f5 ff ff       	call   8011a2 <fd2num>
  801c3c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801c3f:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801c41:	83 c4 04             	add    $0x4,%esp
  801c44:	ff 75 f0             	pushl  -0x10(%ebp)
  801c47:	e8 56 f5 ff ff       	call   8011a2 <fd2num>
  801c4c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801c4f:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801c52:	83 c4 10             	add    $0x10,%esp
  801c55:	b8 00 00 00 00       	mov    $0x0,%eax
  801c5a:	eb 30                	jmp    801c8c <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801c5c:	83 ec 08             	sub    $0x8,%esp
  801c5f:	56                   	push   %esi
  801c60:	6a 00                	push   $0x0
  801c62:	e8 da f0 ff ff       	call   800d41 <sys_page_unmap>
  801c67:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801c6a:	83 ec 08             	sub    $0x8,%esp
  801c6d:	ff 75 f0             	pushl  -0x10(%ebp)
  801c70:	6a 00                	push   $0x0
  801c72:	e8 ca f0 ff ff       	call   800d41 <sys_page_unmap>
  801c77:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801c7a:	83 ec 08             	sub    $0x8,%esp
  801c7d:	ff 75 f4             	pushl  -0xc(%ebp)
  801c80:	6a 00                	push   $0x0
  801c82:	e8 ba f0 ff ff       	call   800d41 <sys_page_unmap>
  801c87:	83 c4 10             	add    $0x10,%esp
  801c8a:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801c8c:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801c8f:	5b                   	pop    %ebx
  801c90:	5e                   	pop    %esi
  801c91:	5d                   	pop    %ebp
  801c92:	c3                   	ret    

00801c93 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801c93:	55                   	push   %ebp
  801c94:	89 e5                	mov    %esp,%ebp
  801c96:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801c99:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c9c:	50                   	push   %eax
  801c9d:	ff 75 08             	pushl  0x8(%ebp)
  801ca0:	e8 73 f5 ff ff       	call   801218 <fd_lookup>
  801ca5:	83 c4 10             	add    $0x10,%esp
  801ca8:	85 c0                	test   %eax,%eax
  801caa:	78 18                	js     801cc4 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801cac:	83 ec 0c             	sub    $0xc,%esp
  801caf:	ff 75 f4             	pushl  -0xc(%ebp)
  801cb2:	e8 fb f4 ff ff       	call   8011b2 <fd2data>
	return _pipeisclosed(fd, p);
  801cb7:	89 c2                	mov    %eax,%edx
  801cb9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801cbc:	e8 3a fd ff ff       	call   8019fb <_pipeisclosed>
  801cc1:	83 c4 10             	add    $0x10,%esp
}
  801cc4:	c9                   	leave  
  801cc5:	c3                   	ret    

00801cc6 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801cc6:	55                   	push   %ebp
  801cc7:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801cc9:	b8 00 00 00 00       	mov    $0x0,%eax
  801cce:	5d                   	pop    %ebp
  801ccf:	c3                   	ret    

00801cd0 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801cd0:	55                   	push   %ebp
  801cd1:	89 e5                	mov    %esp,%ebp
  801cd3:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801cd6:	68 99 28 80 00       	push   $0x802899
  801cdb:	ff 75 0c             	pushl  0xc(%ebp)
  801cde:	e8 f5 eb ff ff       	call   8008d8 <strcpy>
	return 0;
}
  801ce3:	b8 00 00 00 00       	mov    $0x0,%eax
  801ce8:	c9                   	leave  
  801ce9:	c3                   	ret    

00801cea <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801cea:	55                   	push   %ebp
  801ceb:	89 e5                	mov    %esp,%ebp
  801ced:	57                   	push   %edi
  801cee:	56                   	push   %esi
  801cef:	53                   	push   %ebx
  801cf0:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801cf6:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801cfb:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801d01:	eb 2c                	jmp    801d2f <devcons_write+0x45>
		m = n - tot;
  801d03:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801d06:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801d08:	83 fb 7f             	cmp    $0x7f,%ebx
  801d0b:	76 05                	jbe    801d12 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801d0d:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801d12:	83 ec 04             	sub    $0x4,%esp
  801d15:	53                   	push   %ebx
  801d16:	03 45 0c             	add    0xc(%ebp),%eax
  801d19:	50                   	push   %eax
  801d1a:	57                   	push   %edi
  801d1b:	e8 2d ed ff ff       	call   800a4d <memmove>
		sys_cputs(buf, m);
  801d20:	83 c4 08             	add    $0x8,%esp
  801d23:	53                   	push   %ebx
  801d24:	57                   	push   %edi
  801d25:	e8 d6 ee ff ff       	call   800c00 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801d2a:	01 de                	add    %ebx,%esi
  801d2c:	83 c4 10             	add    $0x10,%esp
  801d2f:	89 f0                	mov    %esi,%eax
  801d31:	3b 75 10             	cmp    0x10(%ebp),%esi
  801d34:	72 cd                	jb     801d03 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801d36:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801d39:	5b                   	pop    %ebx
  801d3a:	5e                   	pop    %esi
  801d3b:	5f                   	pop    %edi
  801d3c:	5d                   	pop    %ebp
  801d3d:	c3                   	ret    

00801d3e <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801d3e:	55                   	push   %ebp
  801d3f:	89 e5                	mov    %esp,%ebp
  801d41:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801d44:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801d48:	75 07                	jne    801d51 <devcons_read+0x13>
  801d4a:	eb 23                	jmp    801d6f <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801d4c:	e8 4c ef ff ff       	call   800c9d <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801d51:	e8 c8 ee ff ff       	call   800c1e <sys_cgetc>
  801d56:	85 c0                	test   %eax,%eax
  801d58:	74 f2                	je     801d4c <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801d5a:	85 c0                	test   %eax,%eax
  801d5c:	78 1d                	js     801d7b <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801d5e:	83 f8 04             	cmp    $0x4,%eax
  801d61:	74 13                	je     801d76 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801d63:	8b 55 0c             	mov    0xc(%ebp),%edx
  801d66:	88 02                	mov    %al,(%edx)
	return 1;
  801d68:	b8 01 00 00 00       	mov    $0x1,%eax
  801d6d:	eb 0c                	jmp    801d7b <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801d6f:	b8 00 00 00 00       	mov    $0x0,%eax
  801d74:	eb 05                	jmp    801d7b <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801d76:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801d7b:	c9                   	leave  
  801d7c:	c3                   	ret    

00801d7d <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801d7d:	55                   	push   %ebp
  801d7e:	89 e5                	mov    %esp,%ebp
  801d80:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801d83:	8b 45 08             	mov    0x8(%ebp),%eax
  801d86:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801d89:	6a 01                	push   $0x1
  801d8b:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801d8e:	50                   	push   %eax
  801d8f:	e8 6c ee ff ff       	call   800c00 <sys_cputs>
}
  801d94:	83 c4 10             	add    $0x10,%esp
  801d97:	c9                   	leave  
  801d98:	c3                   	ret    

00801d99 <getchar>:

int
getchar(void)
{
  801d99:	55                   	push   %ebp
  801d9a:	89 e5                	mov    %esp,%ebp
  801d9c:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801d9f:	6a 01                	push   $0x1
  801da1:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801da4:	50                   	push   %eax
  801da5:	6a 00                	push   $0x0
  801da7:	e8 d2 f6 ff ff       	call   80147e <read>
	if (r < 0)
  801dac:	83 c4 10             	add    $0x10,%esp
  801daf:	85 c0                	test   %eax,%eax
  801db1:	78 0f                	js     801dc2 <getchar+0x29>
		return r;
	if (r < 1)
  801db3:	85 c0                	test   %eax,%eax
  801db5:	7e 06                	jle    801dbd <getchar+0x24>
		return -E_EOF;
	return c;
  801db7:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801dbb:	eb 05                	jmp    801dc2 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801dbd:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801dc2:	c9                   	leave  
  801dc3:	c3                   	ret    

00801dc4 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801dc4:	55                   	push   %ebp
  801dc5:	89 e5                	mov    %esp,%ebp
  801dc7:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801dca:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801dcd:	50                   	push   %eax
  801dce:	ff 75 08             	pushl  0x8(%ebp)
  801dd1:	e8 42 f4 ff ff       	call   801218 <fd_lookup>
  801dd6:	83 c4 10             	add    $0x10,%esp
  801dd9:	85 c0                	test   %eax,%eax
  801ddb:	78 11                	js     801dee <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801ddd:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801de0:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801de6:	39 10                	cmp    %edx,(%eax)
  801de8:	0f 94 c0             	sete   %al
  801deb:	0f b6 c0             	movzbl %al,%eax
}
  801dee:	c9                   	leave  
  801def:	c3                   	ret    

00801df0 <opencons>:

int
opencons(void)
{
  801df0:	55                   	push   %ebp
  801df1:	89 e5                	mov    %esp,%ebp
  801df3:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801df6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801df9:	50                   	push   %eax
  801dfa:	e8 ca f3 ff ff       	call   8011c9 <fd_alloc>
  801dff:	83 c4 10             	add    $0x10,%esp
  801e02:	85 c0                	test   %eax,%eax
  801e04:	78 3a                	js     801e40 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801e06:	83 ec 04             	sub    $0x4,%esp
  801e09:	68 07 04 00 00       	push   $0x407
  801e0e:	ff 75 f4             	pushl  -0xc(%ebp)
  801e11:	6a 00                	push   $0x0
  801e13:	e8 a4 ee ff ff       	call   800cbc <sys_page_alloc>
  801e18:	83 c4 10             	add    $0x10,%esp
  801e1b:	85 c0                	test   %eax,%eax
  801e1d:	78 21                	js     801e40 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801e1f:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801e25:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e28:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801e2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e2d:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801e34:	83 ec 0c             	sub    $0xc,%esp
  801e37:	50                   	push   %eax
  801e38:	e8 65 f3 ff ff       	call   8011a2 <fd2num>
  801e3d:	83 c4 10             	add    $0x10,%esp
}
  801e40:	c9                   	leave  
  801e41:	c3                   	ret    

00801e42 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801e42:	55                   	push   %ebp
  801e43:	89 e5                	mov    %esp,%ebp
  801e45:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801e48:	83 3d 00 60 80 00 00 	cmpl   $0x0,0x806000
  801e4f:	75 3e                	jne    801e8f <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801e51:	83 ec 04             	sub    $0x4,%esp
  801e54:	6a 07                	push   $0x7
  801e56:	68 00 f0 bf ee       	push   $0xeebff000
  801e5b:	6a 00                	push   $0x0
  801e5d:	e8 5a ee ff ff       	call   800cbc <sys_page_alloc>
  801e62:	83 c4 10             	add    $0x10,%esp
  801e65:	85 c0                	test   %eax,%eax
  801e67:	74 14                	je     801e7d <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801e69:	83 ec 04             	sub    $0x4,%esp
  801e6c:	68 a8 28 80 00       	push   $0x8028a8
  801e71:	6a 23                	push   $0x23
  801e73:	68 cc 28 80 00       	push   $0x8028cc
  801e78:	e8 1f e4 ff ff       	call   80029c <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801e7d:	83 ec 08             	sub    $0x8,%esp
  801e80:	68 99 1e 80 00       	push   $0x801e99
  801e85:	6a 00                	push   $0x0
  801e87:	e8 9a ef ff ff       	call   800e26 <sys_env_set_pgfault_upcall>
  801e8c:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801e8f:	8b 45 08             	mov    0x8(%ebp),%eax
  801e92:	a3 00 60 80 00       	mov    %eax,0x806000
}
  801e97:	c9                   	leave  
  801e98:	c3                   	ret    

00801e99 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801e99:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801e9a:	a1 00 60 80 00       	mov    0x806000,%eax
	call *%eax
  801e9f:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801ea1:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801ea4:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801ea6:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801eaa:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801eae:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801eaf:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801eb2:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801eb4:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801eb7:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801eba:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801ebb:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  801ebe:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801ebf:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801ec0:	c3                   	ret    

00801ec1 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801ec1:	55                   	push   %ebp
  801ec2:	89 e5                	mov    %esp,%ebp
  801ec4:	56                   	push   %esi
  801ec5:	53                   	push   %ebx
  801ec6:	8b 75 08             	mov    0x8(%ebp),%esi
  801ec9:	8b 45 0c             	mov    0xc(%ebp),%eax
  801ecc:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801ecf:	85 c0                	test   %eax,%eax
  801ed1:	75 05                	jne    801ed8 <ipc_recv+0x17>
        pg = (void *)UTOP;
  801ed3:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801ed8:	83 ec 0c             	sub    $0xc,%esp
  801edb:	50                   	push   %eax
  801edc:	e8 aa ef ff ff       	call   800e8b <sys_ipc_recv>
  801ee1:	83 c4 10             	add    $0x10,%esp
  801ee4:	85 c0                	test   %eax,%eax
  801ee6:	74 16                	je     801efe <ipc_recv+0x3d>
        if (from_env_store)
  801ee8:	85 f6                	test   %esi,%esi
  801eea:	74 06                	je     801ef2 <ipc_recv+0x31>
            *from_env_store = 0;
  801eec:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801ef2:	85 db                	test   %ebx,%ebx
  801ef4:	74 2c                	je     801f22 <ipc_recv+0x61>
            *perm_store = 0;
  801ef6:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801efc:	eb 24                	jmp    801f22 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801efe:	85 f6                	test   %esi,%esi
  801f00:	74 0a                	je     801f0c <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801f02:	a1 04 40 80 00       	mov    0x804004,%eax
  801f07:	8b 40 74             	mov    0x74(%eax),%eax
  801f0a:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801f0c:	85 db                	test   %ebx,%ebx
  801f0e:	74 0a                	je     801f1a <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801f10:	a1 04 40 80 00       	mov    0x804004,%eax
  801f15:	8b 40 78             	mov    0x78(%eax),%eax
  801f18:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801f1a:	a1 04 40 80 00       	mov    0x804004,%eax
  801f1f:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801f22:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801f25:	5b                   	pop    %ebx
  801f26:	5e                   	pop    %esi
  801f27:	5d                   	pop    %ebp
  801f28:	c3                   	ret    

00801f29 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801f29:	55                   	push   %ebp
  801f2a:	89 e5                	mov    %esp,%ebp
  801f2c:	57                   	push   %edi
  801f2d:	56                   	push   %esi
  801f2e:	53                   	push   %ebx
  801f2f:	83 ec 0c             	sub    $0xc,%esp
  801f32:	8b 75 0c             	mov    0xc(%ebp),%esi
  801f35:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801f38:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801f3b:	85 db                	test   %ebx,%ebx
  801f3d:	75 0c                	jne    801f4b <ipc_send+0x22>
        pg = (void *)UTOP;
  801f3f:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801f44:	eb 05                	jmp    801f4b <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801f46:	e8 52 ed ff ff       	call   800c9d <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801f4b:	57                   	push   %edi
  801f4c:	53                   	push   %ebx
  801f4d:	56                   	push   %esi
  801f4e:	ff 75 08             	pushl  0x8(%ebp)
  801f51:	e8 12 ef ff ff       	call   800e68 <sys_ipc_try_send>
  801f56:	83 c4 10             	add    $0x10,%esp
  801f59:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801f5c:	74 e8                	je     801f46 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801f5e:	85 c0                	test   %eax,%eax
  801f60:	74 14                	je     801f76 <ipc_send+0x4d>
        panic("ipc_send: error");
  801f62:	83 ec 04             	sub    $0x4,%esp
  801f65:	68 da 28 80 00       	push   $0x8028da
  801f6a:	6a 6a                	push   $0x6a
  801f6c:	68 ea 28 80 00       	push   $0x8028ea
  801f71:	e8 26 e3 ff ff       	call   80029c <_panic>
    }
	//panic("ipc_send not implemented");
}
  801f76:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801f79:	5b                   	pop    %ebx
  801f7a:	5e                   	pop    %esi
  801f7b:	5f                   	pop    %edi
  801f7c:	5d                   	pop    %ebp
  801f7d:	c3                   	ret    

00801f7e <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801f7e:	55                   	push   %ebp
  801f7f:	89 e5                	mov    %esp,%ebp
  801f81:	53                   	push   %ebx
  801f82:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801f85:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801f8a:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801f91:	89 d0                	mov    %edx,%eax
  801f93:	c1 e0 07             	shl    $0x7,%eax
  801f96:	29 d8                	sub    %ebx,%eax
  801f98:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801f9d:	8b 40 50             	mov    0x50(%eax),%eax
  801fa0:	39 c8                	cmp    %ecx,%eax
  801fa2:	75 0d                	jne    801fb1 <ipc_find_env+0x33>
			return envs[i].env_id;
  801fa4:	c1 e2 07             	shl    $0x7,%edx
  801fa7:	29 da                	sub    %ebx,%edx
  801fa9:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801faf:	eb 0e                	jmp    801fbf <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801fb1:	42                   	inc    %edx
  801fb2:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801fb8:	75 d0                	jne    801f8a <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801fba:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801fbf:	5b                   	pop    %ebx
  801fc0:	5d                   	pop    %ebp
  801fc1:	c3                   	ret    

00801fc2 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801fc2:	55                   	push   %ebp
  801fc3:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801fc5:	8b 45 08             	mov    0x8(%ebp),%eax
  801fc8:	c1 e8 16             	shr    $0x16,%eax
  801fcb:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801fd2:	a8 01                	test   $0x1,%al
  801fd4:	74 21                	je     801ff7 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801fd6:	8b 45 08             	mov    0x8(%ebp),%eax
  801fd9:	c1 e8 0c             	shr    $0xc,%eax
  801fdc:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801fe3:	a8 01                	test   $0x1,%al
  801fe5:	74 17                	je     801ffe <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801fe7:	c1 e8 0c             	shr    $0xc,%eax
  801fea:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801ff1:	ef 
  801ff2:	0f b7 c0             	movzwl %ax,%eax
  801ff5:	eb 0c                	jmp    802003 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801ff7:	b8 00 00 00 00       	mov    $0x0,%eax
  801ffc:	eb 05                	jmp    802003 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801ffe:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  802003:	5d                   	pop    %ebp
  802004:	c3                   	ret    
  802005:	66 90                	xchg   %ax,%ax
  802007:	90                   	nop

00802008 <__udivdi3>:
  802008:	55                   	push   %ebp
  802009:	57                   	push   %edi
  80200a:	56                   	push   %esi
  80200b:	53                   	push   %ebx
  80200c:	83 ec 1c             	sub    $0x1c,%esp
  80200f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  802013:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  802017:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80201b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  80201f:	89 ca                	mov    %ecx,%edx
  802021:	89 f8                	mov    %edi,%eax
  802023:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  802027:	85 f6                	test   %esi,%esi
  802029:	75 2d                	jne    802058 <__udivdi3+0x50>
  80202b:	39 cf                	cmp    %ecx,%edi
  80202d:	77 65                	ja     802094 <__udivdi3+0x8c>
  80202f:	89 fd                	mov    %edi,%ebp
  802031:	85 ff                	test   %edi,%edi
  802033:	75 0b                	jne    802040 <__udivdi3+0x38>
  802035:	b8 01 00 00 00       	mov    $0x1,%eax
  80203a:	31 d2                	xor    %edx,%edx
  80203c:	f7 f7                	div    %edi
  80203e:	89 c5                	mov    %eax,%ebp
  802040:	31 d2                	xor    %edx,%edx
  802042:	89 c8                	mov    %ecx,%eax
  802044:	f7 f5                	div    %ebp
  802046:	89 c1                	mov    %eax,%ecx
  802048:	89 d8                	mov    %ebx,%eax
  80204a:	f7 f5                	div    %ebp
  80204c:	89 cf                	mov    %ecx,%edi
  80204e:	89 fa                	mov    %edi,%edx
  802050:	83 c4 1c             	add    $0x1c,%esp
  802053:	5b                   	pop    %ebx
  802054:	5e                   	pop    %esi
  802055:	5f                   	pop    %edi
  802056:	5d                   	pop    %ebp
  802057:	c3                   	ret    
  802058:	39 ce                	cmp    %ecx,%esi
  80205a:	77 28                	ja     802084 <__udivdi3+0x7c>
  80205c:	0f bd fe             	bsr    %esi,%edi
  80205f:	83 f7 1f             	xor    $0x1f,%edi
  802062:	75 40                	jne    8020a4 <__udivdi3+0x9c>
  802064:	39 ce                	cmp    %ecx,%esi
  802066:	72 0a                	jb     802072 <__udivdi3+0x6a>
  802068:	3b 44 24 08          	cmp    0x8(%esp),%eax
  80206c:	0f 87 9e 00 00 00    	ja     802110 <__udivdi3+0x108>
  802072:	b8 01 00 00 00       	mov    $0x1,%eax
  802077:	89 fa                	mov    %edi,%edx
  802079:	83 c4 1c             	add    $0x1c,%esp
  80207c:	5b                   	pop    %ebx
  80207d:	5e                   	pop    %esi
  80207e:	5f                   	pop    %edi
  80207f:	5d                   	pop    %ebp
  802080:	c3                   	ret    
  802081:	8d 76 00             	lea    0x0(%esi),%esi
  802084:	31 ff                	xor    %edi,%edi
  802086:	31 c0                	xor    %eax,%eax
  802088:	89 fa                	mov    %edi,%edx
  80208a:	83 c4 1c             	add    $0x1c,%esp
  80208d:	5b                   	pop    %ebx
  80208e:	5e                   	pop    %esi
  80208f:	5f                   	pop    %edi
  802090:	5d                   	pop    %ebp
  802091:	c3                   	ret    
  802092:	66 90                	xchg   %ax,%ax
  802094:	89 d8                	mov    %ebx,%eax
  802096:	f7 f7                	div    %edi
  802098:	31 ff                	xor    %edi,%edi
  80209a:	89 fa                	mov    %edi,%edx
  80209c:	83 c4 1c             	add    $0x1c,%esp
  80209f:	5b                   	pop    %ebx
  8020a0:	5e                   	pop    %esi
  8020a1:	5f                   	pop    %edi
  8020a2:	5d                   	pop    %ebp
  8020a3:	c3                   	ret    
  8020a4:	bd 20 00 00 00       	mov    $0x20,%ebp
  8020a9:	89 eb                	mov    %ebp,%ebx
  8020ab:	29 fb                	sub    %edi,%ebx
  8020ad:	89 f9                	mov    %edi,%ecx
  8020af:	d3 e6                	shl    %cl,%esi
  8020b1:	89 c5                	mov    %eax,%ebp
  8020b3:	88 d9                	mov    %bl,%cl
  8020b5:	d3 ed                	shr    %cl,%ebp
  8020b7:	89 e9                	mov    %ebp,%ecx
  8020b9:	09 f1                	or     %esi,%ecx
  8020bb:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8020bf:	89 f9                	mov    %edi,%ecx
  8020c1:	d3 e0                	shl    %cl,%eax
  8020c3:	89 c5                	mov    %eax,%ebp
  8020c5:	89 d6                	mov    %edx,%esi
  8020c7:	88 d9                	mov    %bl,%cl
  8020c9:	d3 ee                	shr    %cl,%esi
  8020cb:	89 f9                	mov    %edi,%ecx
  8020cd:	d3 e2                	shl    %cl,%edx
  8020cf:	8b 44 24 08          	mov    0x8(%esp),%eax
  8020d3:	88 d9                	mov    %bl,%cl
  8020d5:	d3 e8                	shr    %cl,%eax
  8020d7:	09 c2                	or     %eax,%edx
  8020d9:	89 d0                	mov    %edx,%eax
  8020db:	89 f2                	mov    %esi,%edx
  8020dd:	f7 74 24 0c          	divl   0xc(%esp)
  8020e1:	89 d6                	mov    %edx,%esi
  8020e3:	89 c3                	mov    %eax,%ebx
  8020e5:	f7 e5                	mul    %ebp
  8020e7:	39 d6                	cmp    %edx,%esi
  8020e9:	72 19                	jb     802104 <__udivdi3+0xfc>
  8020eb:	74 0b                	je     8020f8 <__udivdi3+0xf0>
  8020ed:	89 d8                	mov    %ebx,%eax
  8020ef:	31 ff                	xor    %edi,%edi
  8020f1:	e9 58 ff ff ff       	jmp    80204e <__udivdi3+0x46>
  8020f6:	66 90                	xchg   %ax,%ax
  8020f8:	8b 54 24 08          	mov    0x8(%esp),%edx
  8020fc:	89 f9                	mov    %edi,%ecx
  8020fe:	d3 e2                	shl    %cl,%edx
  802100:	39 c2                	cmp    %eax,%edx
  802102:	73 e9                	jae    8020ed <__udivdi3+0xe5>
  802104:	8d 43 ff             	lea    -0x1(%ebx),%eax
  802107:	31 ff                	xor    %edi,%edi
  802109:	e9 40 ff ff ff       	jmp    80204e <__udivdi3+0x46>
  80210e:	66 90                	xchg   %ax,%ax
  802110:	31 c0                	xor    %eax,%eax
  802112:	e9 37 ff ff ff       	jmp    80204e <__udivdi3+0x46>
  802117:	90                   	nop

00802118 <__umoddi3>:
  802118:	55                   	push   %ebp
  802119:	57                   	push   %edi
  80211a:	56                   	push   %esi
  80211b:	53                   	push   %ebx
  80211c:	83 ec 1c             	sub    $0x1c,%esp
  80211f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  802123:	8b 74 24 34          	mov    0x34(%esp),%esi
  802127:	8b 7c 24 38          	mov    0x38(%esp),%edi
  80212b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  80212f:	89 44 24 0c          	mov    %eax,0xc(%esp)
  802133:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  802137:	89 f3                	mov    %esi,%ebx
  802139:	89 fa                	mov    %edi,%edx
  80213b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80213f:	89 34 24             	mov    %esi,(%esp)
  802142:	85 c0                	test   %eax,%eax
  802144:	75 1a                	jne    802160 <__umoddi3+0x48>
  802146:	39 f7                	cmp    %esi,%edi
  802148:	0f 86 a2 00 00 00    	jbe    8021f0 <__umoddi3+0xd8>
  80214e:	89 c8                	mov    %ecx,%eax
  802150:	89 f2                	mov    %esi,%edx
  802152:	f7 f7                	div    %edi
  802154:	89 d0                	mov    %edx,%eax
  802156:	31 d2                	xor    %edx,%edx
  802158:	83 c4 1c             	add    $0x1c,%esp
  80215b:	5b                   	pop    %ebx
  80215c:	5e                   	pop    %esi
  80215d:	5f                   	pop    %edi
  80215e:	5d                   	pop    %ebp
  80215f:	c3                   	ret    
  802160:	39 f0                	cmp    %esi,%eax
  802162:	0f 87 ac 00 00 00    	ja     802214 <__umoddi3+0xfc>
  802168:	0f bd e8             	bsr    %eax,%ebp
  80216b:	83 f5 1f             	xor    $0x1f,%ebp
  80216e:	0f 84 ac 00 00 00    	je     802220 <__umoddi3+0x108>
  802174:	bf 20 00 00 00       	mov    $0x20,%edi
  802179:	29 ef                	sub    %ebp,%edi
  80217b:	89 fe                	mov    %edi,%esi
  80217d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802181:	89 e9                	mov    %ebp,%ecx
  802183:	d3 e0                	shl    %cl,%eax
  802185:	89 d7                	mov    %edx,%edi
  802187:	89 f1                	mov    %esi,%ecx
  802189:	d3 ef                	shr    %cl,%edi
  80218b:	09 c7                	or     %eax,%edi
  80218d:	89 e9                	mov    %ebp,%ecx
  80218f:	d3 e2                	shl    %cl,%edx
  802191:	89 14 24             	mov    %edx,(%esp)
  802194:	89 d8                	mov    %ebx,%eax
  802196:	d3 e0                	shl    %cl,%eax
  802198:	89 c2                	mov    %eax,%edx
  80219a:	8b 44 24 08          	mov    0x8(%esp),%eax
  80219e:	d3 e0                	shl    %cl,%eax
  8021a0:	89 44 24 04          	mov    %eax,0x4(%esp)
  8021a4:	8b 44 24 08          	mov    0x8(%esp),%eax
  8021a8:	89 f1                	mov    %esi,%ecx
  8021aa:	d3 e8                	shr    %cl,%eax
  8021ac:	09 d0                	or     %edx,%eax
  8021ae:	d3 eb                	shr    %cl,%ebx
  8021b0:	89 da                	mov    %ebx,%edx
  8021b2:	f7 f7                	div    %edi
  8021b4:	89 d3                	mov    %edx,%ebx
  8021b6:	f7 24 24             	mull   (%esp)
  8021b9:	89 c6                	mov    %eax,%esi
  8021bb:	89 d1                	mov    %edx,%ecx
  8021bd:	39 d3                	cmp    %edx,%ebx
  8021bf:	0f 82 87 00 00 00    	jb     80224c <__umoddi3+0x134>
  8021c5:	0f 84 91 00 00 00    	je     80225c <__umoddi3+0x144>
  8021cb:	8b 54 24 04          	mov    0x4(%esp),%edx
  8021cf:	29 f2                	sub    %esi,%edx
  8021d1:	19 cb                	sbb    %ecx,%ebx
  8021d3:	89 d8                	mov    %ebx,%eax
  8021d5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8021d9:	d3 e0                	shl    %cl,%eax
  8021db:	89 e9                	mov    %ebp,%ecx
  8021dd:	d3 ea                	shr    %cl,%edx
  8021df:	09 d0                	or     %edx,%eax
  8021e1:	89 e9                	mov    %ebp,%ecx
  8021e3:	d3 eb                	shr    %cl,%ebx
  8021e5:	89 da                	mov    %ebx,%edx
  8021e7:	83 c4 1c             	add    $0x1c,%esp
  8021ea:	5b                   	pop    %ebx
  8021eb:	5e                   	pop    %esi
  8021ec:	5f                   	pop    %edi
  8021ed:	5d                   	pop    %ebp
  8021ee:	c3                   	ret    
  8021ef:	90                   	nop
  8021f0:	89 fd                	mov    %edi,%ebp
  8021f2:	85 ff                	test   %edi,%edi
  8021f4:	75 0b                	jne    802201 <__umoddi3+0xe9>
  8021f6:	b8 01 00 00 00       	mov    $0x1,%eax
  8021fb:	31 d2                	xor    %edx,%edx
  8021fd:	f7 f7                	div    %edi
  8021ff:	89 c5                	mov    %eax,%ebp
  802201:	89 f0                	mov    %esi,%eax
  802203:	31 d2                	xor    %edx,%edx
  802205:	f7 f5                	div    %ebp
  802207:	89 c8                	mov    %ecx,%eax
  802209:	f7 f5                	div    %ebp
  80220b:	89 d0                	mov    %edx,%eax
  80220d:	e9 44 ff ff ff       	jmp    802156 <__umoddi3+0x3e>
  802212:	66 90                	xchg   %ax,%ax
  802214:	89 c8                	mov    %ecx,%eax
  802216:	89 f2                	mov    %esi,%edx
  802218:	83 c4 1c             	add    $0x1c,%esp
  80221b:	5b                   	pop    %ebx
  80221c:	5e                   	pop    %esi
  80221d:	5f                   	pop    %edi
  80221e:	5d                   	pop    %ebp
  80221f:	c3                   	ret    
  802220:	3b 04 24             	cmp    (%esp),%eax
  802223:	72 06                	jb     80222b <__umoddi3+0x113>
  802225:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  802229:	77 0f                	ja     80223a <__umoddi3+0x122>
  80222b:	89 f2                	mov    %esi,%edx
  80222d:	29 f9                	sub    %edi,%ecx
  80222f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  802233:	89 14 24             	mov    %edx,(%esp)
  802236:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80223a:	8b 44 24 04          	mov    0x4(%esp),%eax
  80223e:	8b 14 24             	mov    (%esp),%edx
  802241:	83 c4 1c             	add    $0x1c,%esp
  802244:	5b                   	pop    %ebx
  802245:	5e                   	pop    %esi
  802246:	5f                   	pop    %edi
  802247:	5d                   	pop    %ebp
  802248:	c3                   	ret    
  802249:	8d 76 00             	lea    0x0(%esi),%esi
  80224c:	2b 04 24             	sub    (%esp),%eax
  80224f:	19 fa                	sbb    %edi,%edx
  802251:	89 d1                	mov    %edx,%ecx
  802253:	89 c6                	mov    %eax,%esi
  802255:	e9 71 ff ff ff       	jmp    8021cb <__umoddi3+0xb3>
  80225a:	66 90                	xchg   %ax,%ax
  80225c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802260:	72 ea                	jb     80224c <__umoddi3+0x134>
  802262:	89 d9                	mov    %ebx,%ecx
  802264:	e9 62 ff ff ff       	jmp    8021cb <__umoddi3+0xb3>
