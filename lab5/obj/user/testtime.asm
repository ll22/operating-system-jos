
obj/user/testtime.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 c5 00 00 00       	call   8000f6 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <sleep>:
#include <inc/lib.h>
#include <inc/x86.h>

void
sleep(int sec)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	53                   	push   %ebx
  800037:	83 ec 04             	sub    $0x4,%esp
  80003a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	unsigned now = sys_time_msec();
  80003d:	e8 3e 0c 00 00       	call   800c80 <sys_time_msec>
	unsigned end = now + sec * 1000;
  800042:	8d 14 9b             	lea    (%ebx,%ebx,4),%edx
  800045:	8d 14 92             	lea    (%edx,%edx,4),%edx
  800048:	8d 14 92             	lea    (%edx,%edx,4),%edx
  80004b:	8d 1c d0             	lea    (%eax,%edx,8),%ebx

	if ((int)now < 0 && (int)now > -MAXERROR)
  80004e:	85 c0                	test   %eax,%eax
  800050:	79 17                	jns    800069 <sleep+0x36>
  800052:	83 f8 f1             	cmp    $0xfffffff1,%eax
  800055:	7c 12                	jl     800069 <sleep+0x36>
		panic("sys_time_msec: %e", (int)now);
  800057:	50                   	push   %eax
  800058:	68 00 10 80 00       	push   $0x801000
  80005d:	6a 0b                	push   $0xb
  80005f:	68 12 10 80 00       	push   $0x801012
  800064:	e8 ee 00 00 00       	call   800157 <_panic>
	if (end < now)
  800069:	39 d8                	cmp    %ebx,%eax
  80006b:	76 19                	jbe    800086 <sleep+0x53>
		panic("sleep: wrap");
  80006d:	83 ec 04             	sub    $0x4,%esp
  800070:	68 22 10 80 00       	push   $0x801022
  800075:	6a 0d                	push   $0xd
  800077:	68 12 10 80 00       	push   $0x801012
  80007c:	e8 d6 00 00 00       	call   800157 <_panic>

	while (sys_time_msec() < end)
		sys_yield();
  800081:	e8 d2 0a 00 00       	call   800b58 <sys_yield>
	if ((int)now < 0 && (int)now > -MAXERROR)
		panic("sys_time_msec: %e", (int)now);
	if (end < now)
		panic("sleep: wrap");

	while (sys_time_msec() < end)
  800086:	e8 f5 0b 00 00       	call   800c80 <sys_time_msec>
  80008b:	39 c3                	cmp    %eax,%ebx
  80008d:	77 f2                	ja     800081 <sleep+0x4e>
		sys_yield();
}
  80008f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800092:	c9                   	leave  
  800093:	c3                   	ret    

00800094 <umain>:

void
umain(int argc, char **argv)
{
  800094:	55                   	push   %ebp
  800095:	89 e5                	mov    %esp,%ebp
  800097:	53                   	push   %ebx
  800098:	83 ec 04             	sub    $0x4,%esp
  80009b:	bb 32 00 00 00       	mov    $0x32,%ebx
	int i;

	// Wait for the console to calm down
	for (i = 0; i < 50; i++)
		sys_yield();
  8000a0:	e8 b3 0a 00 00       	call   800b58 <sys_yield>
umain(int argc, char **argv)
{
	int i;

	// Wait for the console to calm down
	for (i = 0; i < 50; i++)
  8000a5:	4b                   	dec    %ebx
  8000a6:	75 f8                	jne    8000a0 <umain+0xc>
		sys_yield();

	cprintf("starting count down: ");
  8000a8:	83 ec 0c             	sub    $0xc,%esp
  8000ab:	68 2e 10 80 00       	push   $0x80102e
  8000b0:	e8 7a 01 00 00       	call   80022f <cprintf>
  8000b5:	83 c4 10             	add    $0x10,%esp
	for (i = 5; i >= 0; i--) {
  8000b8:	bb 05 00 00 00       	mov    $0x5,%ebx
		cprintf("%d ", i);
  8000bd:	83 ec 08             	sub    $0x8,%esp
  8000c0:	53                   	push   %ebx
  8000c1:	68 44 10 80 00       	push   $0x801044
  8000c6:	e8 64 01 00 00       	call   80022f <cprintf>
		sleep(1);
  8000cb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  8000d2:	e8 5c ff ff ff       	call   800033 <sleep>
	// Wait for the console to calm down
	for (i = 0; i < 50; i++)
		sys_yield();

	cprintf("starting count down: ");
	for (i = 5; i >= 0; i--) {
  8000d7:	4b                   	dec    %ebx
  8000d8:	83 c4 10             	add    $0x10,%esp
  8000db:	83 fb ff             	cmp    $0xffffffff,%ebx
  8000de:	75 dd                	jne    8000bd <umain+0x29>
		cprintf("%d ", i);
		sleep(1);
	}
	cprintf("\n");
  8000e0:	83 ec 0c             	sub    $0xc,%esp
  8000e3:	68 48 10 80 00       	push   $0x801048
  8000e8:	e8 42 01 00 00       	call   80022f <cprintf>
#include <inc/types.h>

static inline void
breakpoint(void)
{
	asm volatile("int3");
  8000ed:	cc                   	int3   
	breakpoint();
}
  8000ee:	83 c4 10             	add    $0x10,%esp
  8000f1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000f4:	c9                   	leave  
  8000f5:	c3                   	ret    

008000f6 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000f6:	55                   	push   %ebp
  8000f7:	89 e5                	mov    %esp,%ebp
  8000f9:	56                   	push   %esi
  8000fa:	53                   	push   %ebx
  8000fb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000fe:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800101:	e8 33 0a 00 00       	call   800b39 <sys_getenvid>
  800106:	25 ff 03 00 00       	and    $0x3ff,%eax
  80010b:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800112:	c1 e0 07             	shl    $0x7,%eax
  800115:	29 d0                	sub    %edx,%eax
  800117:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80011c:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800121:	85 db                	test   %ebx,%ebx
  800123:	7e 07                	jle    80012c <libmain+0x36>
		binaryname = argv[0];
  800125:	8b 06                	mov    (%esi),%eax
  800127:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80012c:	83 ec 08             	sub    $0x8,%esp
  80012f:	56                   	push   %esi
  800130:	53                   	push   %ebx
  800131:	e8 5e ff ff ff       	call   800094 <umain>

	// exit gracefully
	exit();
  800136:	e8 0a 00 00 00       	call   800145 <exit>
}
  80013b:	83 c4 10             	add    $0x10,%esp
  80013e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800141:	5b                   	pop    %ebx
  800142:	5e                   	pop    %esi
  800143:	5d                   	pop    %ebp
  800144:	c3                   	ret    

00800145 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800145:	55                   	push   %ebp
  800146:	89 e5                	mov    %esp,%ebp
  800148:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80014b:	6a 00                	push   $0x0
  80014d:	e8 a6 09 00 00       	call   800af8 <sys_env_destroy>
}
  800152:	83 c4 10             	add    $0x10,%esp
  800155:	c9                   	leave  
  800156:	c3                   	ret    

00800157 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800157:	55                   	push   %ebp
  800158:	89 e5                	mov    %esp,%ebp
  80015a:	56                   	push   %esi
  80015b:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80015c:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80015f:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800165:	e8 cf 09 00 00       	call   800b39 <sys_getenvid>
  80016a:	83 ec 0c             	sub    $0xc,%esp
  80016d:	ff 75 0c             	pushl  0xc(%ebp)
  800170:	ff 75 08             	pushl  0x8(%ebp)
  800173:	56                   	push   %esi
  800174:	50                   	push   %eax
  800175:	68 54 10 80 00       	push   $0x801054
  80017a:	e8 b0 00 00 00       	call   80022f <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80017f:	83 c4 18             	add    $0x18,%esp
  800182:	53                   	push   %ebx
  800183:	ff 75 10             	pushl  0x10(%ebp)
  800186:	e8 53 00 00 00       	call   8001de <vcprintf>
	cprintf("\n");
  80018b:	c7 04 24 48 10 80 00 	movl   $0x801048,(%esp)
  800192:	e8 98 00 00 00       	call   80022f <cprintf>
  800197:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80019a:	cc                   	int3   
  80019b:	eb fd                	jmp    80019a <_panic+0x43>

0080019d <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80019d:	55                   	push   %ebp
  80019e:	89 e5                	mov    %esp,%ebp
  8001a0:	53                   	push   %ebx
  8001a1:	83 ec 04             	sub    $0x4,%esp
  8001a4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8001a7:	8b 13                	mov    (%ebx),%edx
  8001a9:	8d 42 01             	lea    0x1(%edx),%eax
  8001ac:	89 03                	mov    %eax,(%ebx)
  8001ae:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8001b1:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001b5:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001ba:	75 1a                	jne    8001d6 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001bc:	83 ec 08             	sub    $0x8,%esp
  8001bf:	68 ff 00 00 00       	push   $0xff
  8001c4:	8d 43 08             	lea    0x8(%ebx),%eax
  8001c7:	50                   	push   %eax
  8001c8:	e8 ee 08 00 00       	call   800abb <sys_cputs>
		b->idx = 0;
  8001cd:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001d3:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001d6:	ff 43 04             	incl   0x4(%ebx)
}
  8001d9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001dc:	c9                   	leave  
  8001dd:	c3                   	ret    

008001de <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001de:	55                   	push   %ebp
  8001df:	89 e5                	mov    %esp,%ebp
  8001e1:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001e7:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001ee:	00 00 00 
	b.cnt = 0;
  8001f1:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001f8:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001fb:	ff 75 0c             	pushl  0xc(%ebp)
  8001fe:	ff 75 08             	pushl  0x8(%ebp)
  800201:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800207:	50                   	push   %eax
  800208:	68 9d 01 80 00       	push   $0x80019d
  80020d:	e8 51 01 00 00       	call   800363 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800212:	83 c4 08             	add    $0x8,%esp
  800215:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80021b:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800221:	50                   	push   %eax
  800222:	e8 94 08 00 00       	call   800abb <sys_cputs>

	return b.cnt;
}
  800227:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80022d:	c9                   	leave  
  80022e:	c3                   	ret    

0080022f <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80022f:	55                   	push   %ebp
  800230:	89 e5                	mov    %esp,%ebp
  800232:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800235:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800238:	50                   	push   %eax
  800239:	ff 75 08             	pushl  0x8(%ebp)
  80023c:	e8 9d ff ff ff       	call   8001de <vcprintf>
	va_end(ap);

	return cnt;
}
  800241:	c9                   	leave  
  800242:	c3                   	ret    

00800243 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800243:	55                   	push   %ebp
  800244:	89 e5                	mov    %esp,%ebp
  800246:	57                   	push   %edi
  800247:	56                   	push   %esi
  800248:	53                   	push   %ebx
  800249:	83 ec 1c             	sub    $0x1c,%esp
  80024c:	89 c7                	mov    %eax,%edi
  80024e:	89 d6                	mov    %edx,%esi
  800250:	8b 45 08             	mov    0x8(%ebp),%eax
  800253:	8b 55 0c             	mov    0xc(%ebp),%edx
  800256:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800259:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80025c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80025f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800264:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800267:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80026a:	39 d3                	cmp    %edx,%ebx
  80026c:	72 05                	jb     800273 <printnum+0x30>
  80026e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800271:	77 45                	ja     8002b8 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800273:	83 ec 0c             	sub    $0xc,%esp
  800276:	ff 75 18             	pushl  0x18(%ebp)
  800279:	8b 45 14             	mov    0x14(%ebp),%eax
  80027c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80027f:	53                   	push   %ebx
  800280:	ff 75 10             	pushl  0x10(%ebp)
  800283:	83 ec 08             	sub    $0x8,%esp
  800286:	ff 75 e4             	pushl  -0x1c(%ebp)
  800289:	ff 75 e0             	pushl  -0x20(%ebp)
  80028c:	ff 75 dc             	pushl  -0x24(%ebp)
  80028f:	ff 75 d8             	pushl  -0x28(%ebp)
  800292:	e8 f1 0a 00 00       	call   800d88 <__udivdi3>
  800297:	83 c4 18             	add    $0x18,%esp
  80029a:	52                   	push   %edx
  80029b:	50                   	push   %eax
  80029c:	89 f2                	mov    %esi,%edx
  80029e:	89 f8                	mov    %edi,%eax
  8002a0:	e8 9e ff ff ff       	call   800243 <printnum>
  8002a5:	83 c4 20             	add    $0x20,%esp
  8002a8:	eb 16                	jmp    8002c0 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8002aa:	83 ec 08             	sub    $0x8,%esp
  8002ad:	56                   	push   %esi
  8002ae:	ff 75 18             	pushl  0x18(%ebp)
  8002b1:	ff d7                	call   *%edi
  8002b3:	83 c4 10             	add    $0x10,%esp
  8002b6:	eb 03                	jmp    8002bb <printnum+0x78>
  8002b8:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002bb:	4b                   	dec    %ebx
  8002bc:	85 db                	test   %ebx,%ebx
  8002be:	7f ea                	jg     8002aa <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002c0:	83 ec 08             	sub    $0x8,%esp
  8002c3:	56                   	push   %esi
  8002c4:	83 ec 04             	sub    $0x4,%esp
  8002c7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002ca:	ff 75 e0             	pushl  -0x20(%ebp)
  8002cd:	ff 75 dc             	pushl  -0x24(%ebp)
  8002d0:	ff 75 d8             	pushl  -0x28(%ebp)
  8002d3:	e8 c0 0b 00 00       	call   800e98 <__umoddi3>
  8002d8:	83 c4 14             	add    $0x14,%esp
  8002db:	0f be 80 77 10 80 00 	movsbl 0x801077(%eax),%eax
  8002e2:	50                   	push   %eax
  8002e3:	ff d7                	call   *%edi
}
  8002e5:	83 c4 10             	add    $0x10,%esp
  8002e8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002eb:	5b                   	pop    %ebx
  8002ec:	5e                   	pop    %esi
  8002ed:	5f                   	pop    %edi
  8002ee:	5d                   	pop    %ebp
  8002ef:	c3                   	ret    

008002f0 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002f0:	55                   	push   %ebp
  8002f1:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002f3:	83 fa 01             	cmp    $0x1,%edx
  8002f6:	7e 0e                	jle    800306 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002f8:	8b 10                	mov    (%eax),%edx
  8002fa:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002fd:	89 08                	mov    %ecx,(%eax)
  8002ff:	8b 02                	mov    (%edx),%eax
  800301:	8b 52 04             	mov    0x4(%edx),%edx
  800304:	eb 22                	jmp    800328 <getuint+0x38>
	else if (lflag)
  800306:	85 d2                	test   %edx,%edx
  800308:	74 10                	je     80031a <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80030a:	8b 10                	mov    (%eax),%edx
  80030c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80030f:	89 08                	mov    %ecx,(%eax)
  800311:	8b 02                	mov    (%edx),%eax
  800313:	ba 00 00 00 00       	mov    $0x0,%edx
  800318:	eb 0e                	jmp    800328 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80031a:	8b 10                	mov    (%eax),%edx
  80031c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80031f:	89 08                	mov    %ecx,(%eax)
  800321:	8b 02                	mov    (%edx),%eax
  800323:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800328:	5d                   	pop    %ebp
  800329:	c3                   	ret    

0080032a <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80032a:	55                   	push   %ebp
  80032b:	89 e5                	mov    %esp,%ebp
  80032d:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800330:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800333:	8b 10                	mov    (%eax),%edx
  800335:	3b 50 04             	cmp    0x4(%eax),%edx
  800338:	73 0a                	jae    800344 <sprintputch+0x1a>
		*b->buf++ = ch;
  80033a:	8d 4a 01             	lea    0x1(%edx),%ecx
  80033d:	89 08                	mov    %ecx,(%eax)
  80033f:	8b 45 08             	mov    0x8(%ebp),%eax
  800342:	88 02                	mov    %al,(%edx)
}
  800344:	5d                   	pop    %ebp
  800345:	c3                   	ret    

00800346 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800346:	55                   	push   %ebp
  800347:	89 e5                	mov    %esp,%ebp
  800349:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80034c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80034f:	50                   	push   %eax
  800350:	ff 75 10             	pushl  0x10(%ebp)
  800353:	ff 75 0c             	pushl  0xc(%ebp)
  800356:	ff 75 08             	pushl  0x8(%ebp)
  800359:	e8 05 00 00 00       	call   800363 <vprintfmt>
	va_end(ap);
}
  80035e:	83 c4 10             	add    $0x10,%esp
  800361:	c9                   	leave  
  800362:	c3                   	ret    

00800363 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800363:	55                   	push   %ebp
  800364:	89 e5                	mov    %esp,%ebp
  800366:	57                   	push   %edi
  800367:	56                   	push   %esi
  800368:	53                   	push   %ebx
  800369:	83 ec 2c             	sub    $0x2c,%esp
  80036c:	8b 75 08             	mov    0x8(%ebp),%esi
  80036f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800372:	8b 7d 10             	mov    0x10(%ebp),%edi
  800375:	eb 12                	jmp    800389 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800377:	85 c0                	test   %eax,%eax
  800379:	0f 84 68 03 00 00    	je     8006e7 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80037f:	83 ec 08             	sub    $0x8,%esp
  800382:	53                   	push   %ebx
  800383:	50                   	push   %eax
  800384:	ff d6                	call   *%esi
  800386:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800389:	47                   	inc    %edi
  80038a:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80038e:	83 f8 25             	cmp    $0x25,%eax
  800391:	75 e4                	jne    800377 <vprintfmt+0x14>
  800393:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800397:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80039e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003a5:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8003ac:	ba 00 00 00 00       	mov    $0x0,%edx
  8003b1:	eb 07                	jmp    8003ba <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b3:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003b6:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ba:	8d 47 01             	lea    0x1(%edi),%eax
  8003bd:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003c0:	0f b6 0f             	movzbl (%edi),%ecx
  8003c3:	8a 07                	mov    (%edi),%al
  8003c5:	83 e8 23             	sub    $0x23,%eax
  8003c8:	3c 55                	cmp    $0x55,%al
  8003ca:	0f 87 fe 02 00 00    	ja     8006ce <vprintfmt+0x36b>
  8003d0:	0f b6 c0             	movzbl %al,%eax
  8003d3:	ff 24 85 c0 11 80 00 	jmp    *0x8011c0(,%eax,4)
  8003da:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003dd:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003e1:	eb d7                	jmp    8003ba <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003e6:	b8 00 00 00 00       	mov    $0x0,%eax
  8003eb:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003ee:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003f1:	01 c0                	add    %eax,%eax
  8003f3:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003f7:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003fa:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003fd:	83 fa 09             	cmp    $0x9,%edx
  800400:	77 34                	ja     800436 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800402:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800403:	eb e9                	jmp    8003ee <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800405:	8b 45 14             	mov    0x14(%ebp),%eax
  800408:	8d 48 04             	lea    0x4(%eax),%ecx
  80040b:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80040e:	8b 00                	mov    (%eax),%eax
  800410:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800413:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800416:	eb 24                	jmp    80043c <vprintfmt+0xd9>
  800418:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80041c:	79 07                	jns    800425 <vprintfmt+0xc2>
  80041e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800425:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800428:	eb 90                	jmp    8003ba <vprintfmt+0x57>
  80042a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80042d:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800434:	eb 84                	jmp    8003ba <vprintfmt+0x57>
  800436:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800439:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80043c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800440:	0f 89 74 ff ff ff    	jns    8003ba <vprintfmt+0x57>
				width = precision, precision = -1;
  800446:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800449:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80044c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800453:	e9 62 ff ff ff       	jmp    8003ba <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800458:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800459:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80045c:	e9 59 ff ff ff       	jmp    8003ba <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800461:	8b 45 14             	mov    0x14(%ebp),%eax
  800464:	8d 50 04             	lea    0x4(%eax),%edx
  800467:	89 55 14             	mov    %edx,0x14(%ebp)
  80046a:	83 ec 08             	sub    $0x8,%esp
  80046d:	53                   	push   %ebx
  80046e:	ff 30                	pushl  (%eax)
  800470:	ff d6                	call   *%esi
			break;
  800472:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800475:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800478:	e9 0c ff ff ff       	jmp    800389 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80047d:	8b 45 14             	mov    0x14(%ebp),%eax
  800480:	8d 50 04             	lea    0x4(%eax),%edx
  800483:	89 55 14             	mov    %edx,0x14(%ebp)
  800486:	8b 00                	mov    (%eax),%eax
  800488:	85 c0                	test   %eax,%eax
  80048a:	79 02                	jns    80048e <vprintfmt+0x12b>
  80048c:	f7 d8                	neg    %eax
  80048e:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800490:	83 f8 0f             	cmp    $0xf,%eax
  800493:	7f 0b                	jg     8004a0 <vprintfmt+0x13d>
  800495:	8b 04 85 20 13 80 00 	mov    0x801320(,%eax,4),%eax
  80049c:	85 c0                	test   %eax,%eax
  80049e:	75 18                	jne    8004b8 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8004a0:	52                   	push   %edx
  8004a1:	68 8f 10 80 00       	push   $0x80108f
  8004a6:	53                   	push   %ebx
  8004a7:	56                   	push   %esi
  8004a8:	e8 99 fe ff ff       	call   800346 <printfmt>
  8004ad:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b0:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004b3:	e9 d1 fe ff ff       	jmp    800389 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004b8:	50                   	push   %eax
  8004b9:	68 98 10 80 00       	push   $0x801098
  8004be:	53                   	push   %ebx
  8004bf:	56                   	push   %esi
  8004c0:	e8 81 fe ff ff       	call   800346 <printfmt>
  8004c5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004c8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004cb:	e9 b9 fe ff ff       	jmp    800389 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004d0:	8b 45 14             	mov    0x14(%ebp),%eax
  8004d3:	8d 50 04             	lea    0x4(%eax),%edx
  8004d6:	89 55 14             	mov    %edx,0x14(%ebp)
  8004d9:	8b 38                	mov    (%eax),%edi
  8004db:	85 ff                	test   %edi,%edi
  8004dd:	75 05                	jne    8004e4 <vprintfmt+0x181>
				p = "(null)";
  8004df:	bf 88 10 80 00       	mov    $0x801088,%edi
			if (width > 0 && padc != '-')
  8004e4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004e8:	0f 8e 90 00 00 00    	jle    80057e <vprintfmt+0x21b>
  8004ee:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004f2:	0f 84 8e 00 00 00    	je     800586 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004f8:	83 ec 08             	sub    $0x8,%esp
  8004fb:	ff 75 d0             	pushl  -0x30(%ebp)
  8004fe:	57                   	push   %edi
  8004ff:	e8 70 02 00 00       	call   800774 <strnlen>
  800504:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800507:	29 c1                	sub    %eax,%ecx
  800509:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80050c:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80050f:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800513:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800516:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800519:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80051b:	eb 0d                	jmp    80052a <vprintfmt+0x1c7>
					putch(padc, putdat);
  80051d:	83 ec 08             	sub    $0x8,%esp
  800520:	53                   	push   %ebx
  800521:	ff 75 e4             	pushl  -0x1c(%ebp)
  800524:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800526:	4f                   	dec    %edi
  800527:	83 c4 10             	add    $0x10,%esp
  80052a:	85 ff                	test   %edi,%edi
  80052c:	7f ef                	jg     80051d <vprintfmt+0x1ba>
  80052e:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800531:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800534:	89 c8                	mov    %ecx,%eax
  800536:	85 c9                	test   %ecx,%ecx
  800538:	79 05                	jns    80053f <vprintfmt+0x1dc>
  80053a:	b8 00 00 00 00       	mov    $0x0,%eax
  80053f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800542:	29 c1                	sub    %eax,%ecx
  800544:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800547:	89 75 08             	mov    %esi,0x8(%ebp)
  80054a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80054d:	eb 3d                	jmp    80058c <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80054f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800553:	74 19                	je     80056e <vprintfmt+0x20b>
  800555:	0f be c0             	movsbl %al,%eax
  800558:	83 e8 20             	sub    $0x20,%eax
  80055b:	83 f8 5e             	cmp    $0x5e,%eax
  80055e:	76 0e                	jbe    80056e <vprintfmt+0x20b>
					putch('?', putdat);
  800560:	83 ec 08             	sub    $0x8,%esp
  800563:	53                   	push   %ebx
  800564:	6a 3f                	push   $0x3f
  800566:	ff 55 08             	call   *0x8(%ebp)
  800569:	83 c4 10             	add    $0x10,%esp
  80056c:	eb 0b                	jmp    800579 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80056e:	83 ec 08             	sub    $0x8,%esp
  800571:	53                   	push   %ebx
  800572:	52                   	push   %edx
  800573:	ff 55 08             	call   *0x8(%ebp)
  800576:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800579:	ff 4d e4             	decl   -0x1c(%ebp)
  80057c:	eb 0e                	jmp    80058c <vprintfmt+0x229>
  80057e:	89 75 08             	mov    %esi,0x8(%ebp)
  800581:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800584:	eb 06                	jmp    80058c <vprintfmt+0x229>
  800586:	89 75 08             	mov    %esi,0x8(%ebp)
  800589:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80058c:	47                   	inc    %edi
  80058d:	8a 47 ff             	mov    -0x1(%edi),%al
  800590:	0f be d0             	movsbl %al,%edx
  800593:	85 d2                	test   %edx,%edx
  800595:	74 1d                	je     8005b4 <vprintfmt+0x251>
  800597:	85 f6                	test   %esi,%esi
  800599:	78 b4                	js     80054f <vprintfmt+0x1ec>
  80059b:	4e                   	dec    %esi
  80059c:	79 b1                	jns    80054f <vprintfmt+0x1ec>
  80059e:	8b 75 08             	mov    0x8(%ebp),%esi
  8005a1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005a4:	eb 14                	jmp    8005ba <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8005a6:	83 ec 08             	sub    $0x8,%esp
  8005a9:	53                   	push   %ebx
  8005aa:	6a 20                	push   $0x20
  8005ac:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005ae:	4f                   	dec    %edi
  8005af:	83 c4 10             	add    $0x10,%esp
  8005b2:	eb 06                	jmp    8005ba <vprintfmt+0x257>
  8005b4:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005b7:	8b 75 08             	mov    0x8(%ebp),%esi
  8005ba:	85 ff                	test   %edi,%edi
  8005bc:	7f e8                	jg     8005a6 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005be:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005c1:	e9 c3 fd ff ff       	jmp    800389 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005c6:	83 fa 01             	cmp    $0x1,%edx
  8005c9:	7e 16                	jle    8005e1 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005cb:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ce:	8d 50 08             	lea    0x8(%eax),%edx
  8005d1:	89 55 14             	mov    %edx,0x14(%ebp)
  8005d4:	8b 50 04             	mov    0x4(%eax),%edx
  8005d7:	8b 00                	mov    (%eax),%eax
  8005d9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005dc:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005df:	eb 32                	jmp    800613 <vprintfmt+0x2b0>
	else if (lflag)
  8005e1:	85 d2                	test   %edx,%edx
  8005e3:	74 18                	je     8005fd <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005e5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e8:	8d 50 04             	lea    0x4(%eax),%edx
  8005eb:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ee:	8b 00                	mov    (%eax),%eax
  8005f0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005f3:	89 c1                	mov    %eax,%ecx
  8005f5:	c1 f9 1f             	sar    $0x1f,%ecx
  8005f8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005fb:	eb 16                	jmp    800613 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005fd:	8b 45 14             	mov    0x14(%ebp),%eax
  800600:	8d 50 04             	lea    0x4(%eax),%edx
  800603:	89 55 14             	mov    %edx,0x14(%ebp)
  800606:	8b 00                	mov    (%eax),%eax
  800608:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80060b:	89 c1                	mov    %eax,%ecx
  80060d:	c1 f9 1f             	sar    $0x1f,%ecx
  800610:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800613:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800616:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800619:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80061d:	79 76                	jns    800695 <vprintfmt+0x332>
				putch('-', putdat);
  80061f:	83 ec 08             	sub    $0x8,%esp
  800622:	53                   	push   %ebx
  800623:	6a 2d                	push   $0x2d
  800625:	ff d6                	call   *%esi
				num = -(long long) num;
  800627:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80062a:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80062d:	f7 d8                	neg    %eax
  80062f:	83 d2 00             	adc    $0x0,%edx
  800632:	f7 da                	neg    %edx
  800634:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800637:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80063c:	eb 5c                	jmp    80069a <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80063e:	8d 45 14             	lea    0x14(%ebp),%eax
  800641:	e8 aa fc ff ff       	call   8002f0 <getuint>
			base = 10;
  800646:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80064b:	eb 4d                	jmp    80069a <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80064d:	8d 45 14             	lea    0x14(%ebp),%eax
  800650:	e8 9b fc ff ff       	call   8002f0 <getuint>
			base = 8;
  800655:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80065a:	eb 3e                	jmp    80069a <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80065c:	83 ec 08             	sub    $0x8,%esp
  80065f:	53                   	push   %ebx
  800660:	6a 30                	push   $0x30
  800662:	ff d6                	call   *%esi
			putch('x', putdat);
  800664:	83 c4 08             	add    $0x8,%esp
  800667:	53                   	push   %ebx
  800668:	6a 78                	push   $0x78
  80066a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80066c:	8b 45 14             	mov    0x14(%ebp),%eax
  80066f:	8d 50 04             	lea    0x4(%eax),%edx
  800672:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800675:	8b 00                	mov    (%eax),%eax
  800677:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80067c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80067f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800684:	eb 14                	jmp    80069a <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800686:	8d 45 14             	lea    0x14(%ebp),%eax
  800689:	e8 62 fc ff ff       	call   8002f0 <getuint>
			base = 16;
  80068e:	b9 10 00 00 00       	mov    $0x10,%ecx
  800693:	eb 05                	jmp    80069a <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800695:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80069a:	83 ec 0c             	sub    $0xc,%esp
  80069d:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8006a1:	57                   	push   %edi
  8006a2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006a5:	51                   	push   %ecx
  8006a6:	52                   	push   %edx
  8006a7:	50                   	push   %eax
  8006a8:	89 da                	mov    %ebx,%edx
  8006aa:	89 f0                	mov    %esi,%eax
  8006ac:	e8 92 fb ff ff       	call   800243 <printnum>
			break;
  8006b1:	83 c4 20             	add    $0x20,%esp
  8006b4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006b7:	e9 cd fc ff ff       	jmp    800389 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006bc:	83 ec 08             	sub    $0x8,%esp
  8006bf:	53                   	push   %ebx
  8006c0:	51                   	push   %ecx
  8006c1:	ff d6                	call   *%esi
			break;
  8006c3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006c6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006c9:	e9 bb fc ff ff       	jmp    800389 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006ce:	83 ec 08             	sub    $0x8,%esp
  8006d1:	53                   	push   %ebx
  8006d2:	6a 25                	push   $0x25
  8006d4:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006d6:	83 c4 10             	add    $0x10,%esp
  8006d9:	eb 01                	jmp    8006dc <vprintfmt+0x379>
  8006db:	4f                   	dec    %edi
  8006dc:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006e0:	75 f9                	jne    8006db <vprintfmt+0x378>
  8006e2:	e9 a2 fc ff ff       	jmp    800389 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006e7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006ea:	5b                   	pop    %ebx
  8006eb:	5e                   	pop    %esi
  8006ec:	5f                   	pop    %edi
  8006ed:	5d                   	pop    %ebp
  8006ee:	c3                   	ret    

008006ef <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006ef:	55                   	push   %ebp
  8006f0:	89 e5                	mov    %esp,%ebp
  8006f2:	83 ec 18             	sub    $0x18,%esp
  8006f5:	8b 45 08             	mov    0x8(%ebp),%eax
  8006f8:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006fb:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006fe:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800702:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800705:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80070c:	85 c0                	test   %eax,%eax
  80070e:	74 26                	je     800736 <vsnprintf+0x47>
  800710:	85 d2                	test   %edx,%edx
  800712:	7e 29                	jle    80073d <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800714:	ff 75 14             	pushl  0x14(%ebp)
  800717:	ff 75 10             	pushl  0x10(%ebp)
  80071a:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80071d:	50                   	push   %eax
  80071e:	68 2a 03 80 00       	push   $0x80032a
  800723:	e8 3b fc ff ff       	call   800363 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800728:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80072b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80072e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800731:	83 c4 10             	add    $0x10,%esp
  800734:	eb 0c                	jmp    800742 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800736:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80073b:	eb 05                	jmp    800742 <vsnprintf+0x53>
  80073d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800742:	c9                   	leave  
  800743:	c3                   	ret    

00800744 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800744:	55                   	push   %ebp
  800745:	89 e5                	mov    %esp,%ebp
  800747:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80074a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80074d:	50                   	push   %eax
  80074e:	ff 75 10             	pushl  0x10(%ebp)
  800751:	ff 75 0c             	pushl  0xc(%ebp)
  800754:	ff 75 08             	pushl  0x8(%ebp)
  800757:	e8 93 ff ff ff       	call   8006ef <vsnprintf>
	va_end(ap);

	return rc;
}
  80075c:	c9                   	leave  
  80075d:	c3                   	ret    

0080075e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80075e:	55                   	push   %ebp
  80075f:	89 e5                	mov    %esp,%ebp
  800761:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800764:	b8 00 00 00 00       	mov    $0x0,%eax
  800769:	eb 01                	jmp    80076c <strlen+0xe>
		n++;
  80076b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80076c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800770:	75 f9                	jne    80076b <strlen+0xd>
		n++;
	return n;
}
  800772:	5d                   	pop    %ebp
  800773:	c3                   	ret    

00800774 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800774:	55                   	push   %ebp
  800775:	89 e5                	mov    %esp,%ebp
  800777:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80077a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80077d:	ba 00 00 00 00       	mov    $0x0,%edx
  800782:	eb 01                	jmp    800785 <strnlen+0x11>
		n++;
  800784:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800785:	39 c2                	cmp    %eax,%edx
  800787:	74 08                	je     800791 <strnlen+0x1d>
  800789:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80078d:	75 f5                	jne    800784 <strnlen+0x10>
  80078f:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800791:	5d                   	pop    %ebp
  800792:	c3                   	ret    

00800793 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800793:	55                   	push   %ebp
  800794:	89 e5                	mov    %esp,%ebp
  800796:	53                   	push   %ebx
  800797:	8b 45 08             	mov    0x8(%ebp),%eax
  80079a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80079d:	89 c2                	mov    %eax,%edx
  80079f:	42                   	inc    %edx
  8007a0:	41                   	inc    %ecx
  8007a1:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8007a4:	88 5a ff             	mov    %bl,-0x1(%edx)
  8007a7:	84 db                	test   %bl,%bl
  8007a9:	75 f4                	jne    80079f <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8007ab:	5b                   	pop    %ebx
  8007ac:	5d                   	pop    %ebp
  8007ad:	c3                   	ret    

008007ae <strcat>:

char *
strcat(char *dst, const char *src)
{
  8007ae:	55                   	push   %ebp
  8007af:	89 e5                	mov    %esp,%ebp
  8007b1:	53                   	push   %ebx
  8007b2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007b5:	53                   	push   %ebx
  8007b6:	e8 a3 ff ff ff       	call   80075e <strlen>
  8007bb:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007be:	ff 75 0c             	pushl  0xc(%ebp)
  8007c1:	01 d8                	add    %ebx,%eax
  8007c3:	50                   	push   %eax
  8007c4:	e8 ca ff ff ff       	call   800793 <strcpy>
	return dst;
}
  8007c9:	89 d8                	mov    %ebx,%eax
  8007cb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007ce:	c9                   	leave  
  8007cf:	c3                   	ret    

008007d0 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007d0:	55                   	push   %ebp
  8007d1:	89 e5                	mov    %esp,%ebp
  8007d3:	56                   	push   %esi
  8007d4:	53                   	push   %ebx
  8007d5:	8b 75 08             	mov    0x8(%ebp),%esi
  8007d8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007db:	89 f3                	mov    %esi,%ebx
  8007dd:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007e0:	89 f2                	mov    %esi,%edx
  8007e2:	eb 0c                	jmp    8007f0 <strncpy+0x20>
		*dst++ = *src;
  8007e4:	42                   	inc    %edx
  8007e5:	8a 01                	mov    (%ecx),%al
  8007e7:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007ea:	80 39 01             	cmpb   $0x1,(%ecx)
  8007ed:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007f0:	39 da                	cmp    %ebx,%edx
  8007f2:	75 f0                	jne    8007e4 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007f4:	89 f0                	mov    %esi,%eax
  8007f6:	5b                   	pop    %ebx
  8007f7:	5e                   	pop    %esi
  8007f8:	5d                   	pop    %ebp
  8007f9:	c3                   	ret    

008007fa <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007fa:	55                   	push   %ebp
  8007fb:	89 e5                	mov    %esp,%ebp
  8007fd:	56                   	push   %esi
  8007fe:	53                   	push   %ebx
  8007ff:	8b 75 08             	mov    0x8(%ebp),%esi
  800802:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800805:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800808:	85 c0                	test   %eax,%eax
  80080a:	74 1e                	je     80082a <strlcpy+0x30>
  80080c:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800810:	89 f2                	mov    %esi,%edx
  800812:	eb 05                	jmp    800819 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800814:	42                   	inc    %edx
  800815:	41                   	inc    %ecx
  800816:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800819:	39 c2                	cmp    %eax,%edx
  80081b:	74 08                	je     800825 <strlcpy+0x2b>
  80081d:	8a 19                	mov    (%ecx),%bl
  80081f:	84 db                	test   %bl,%bl
  800821:	75 f1                	jne    800814 <strlcpy+0x1a>
  800823:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800825:	c6 00 00             	movb   $0x0,(%eax)
  800828:	eb 02                	jmp    80082c <strlcpy+0x32>
  80082a:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80082c:	29 f0                	sub    %esi,%eax
}
  80082e:	5b                   	pop    %ebx
  80082f:	5e                   	pop    %esi
  800830:	5d                   	pop    %ebp
  800831:	c3                   	ret    

00800832 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800832:	55                   	push   %ebp
  800833:	89 e5                	mov    %esp,%ebp
  800835:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800838:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80083b:	eb 02                	jmp    80083f <strcmp+0xd>
		p++, q++;
  80083d:	41                   	inc    %ecx
  80083e:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80083f:	8a 01                	mov    (%ecx),%al
  800841:	84 c0                	test   %al,%al
  800843:	74 04                	je     800849 <strcmp+0x17>
  800845:	3a 02                	cmp    (%edx),%al
  800847:	74 f4                	je     80083d <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800849:	0f b6 c0             	movzbl %al,%eax
  80084c:	0f b6 12             	movzbl (%edx),%edx
  80084f:	29 d0                	sub    %edx,%eax
}
  800851:	5d                   	pop    %ebp
  800852:	c3                   	ret    

00800853 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800853:	55                   	push   %ebp
  800854:	89 e5                	mov    %esp,%ebp
  800856:	53                   	push   %ebx
  800857:	8b 45 08             	mov    0x8(%ebp),%eax
  80085a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80085d:	89 c3                	mov    %eax,%ebx
  80085f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800862:	eb 02                	jmp    800866 <strncmp+0x13>
		n--, p++, q++;
  800864:	40                   	inc    %eax
  800865:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800866:	39 d8                	cmp    %ebx,%eax
  800868:	74 14                	je     80087e <strncmp+0x2b>
  80086a:	8a 08                	mov    (%eax),%cl
  80086c:	84 c9                	test   %cl,%cl
  80086e:	74 04                	je     800874 <strncmp+0x21>
  800870:	3a 0a                	cmp    (%edx),%cl
  800872:	74 f0                	je     800864 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800874:	0f b6 00             	movzbl (%eax),%eax
  800877:	0f b6 12             	movzbl (%edx),%edx
  80087a:	29 d0                	sub    %edx,%eax
  80087c:	eb 05                	jmp    800883 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80087e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800883:	5b                   	pop    %ebx
  800884:	5d                   	pop    %ebp
  800885:	c3                   	ret    

00800886 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800886:	55                   	push   %ebp
  800887:	89 e5                	mov    %esp,%ebp
  800889:	8b 45 08             	mov    0x8(%ebp),%eax
  80088c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80088f:	eb 05                	jmp    800896 <strchr+0x10>
		if (*s == c)
  800891:	38 ca                	cmp    %cl,%dl
  800893:	74 0c                	je     8008a1 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800895:	40                   	inc    %eax
  800896:	8a 10                	mov    (%eax),%dl
  800898:	84 d2                	test   %dl,%dl
  80089a:	75 f5                	jne    800891 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80089c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008a1:	5d                   	pop    %ebp
  8008a2:	c3                   	ret    

008008a3 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8008a3:	55                   	push   %ebp
  8008a4:	89 e5                	mov    %esp,%ebp
  8008a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008ac:	eb 05                	jmp    8008b3 <strfind+0x10>
		if (*s == c)
  8008ae:	38 ca                	cmp    %cl,%dl
  8008b0:	74 07                	je     8008b9 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8008b2:	40                   	inc    %eax
  8008b3:	8a 10                	mov    (%eax),%dl
  8008b5:	84 d2                	test   %dl,%dl
  8008b7:	75 f5                	jne    8008ae <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008b9:	5d                   	pop    %ebp
  8008ba:	c3                   	ret    

008008bb <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008bb:	55                   	push   %ebp
  8008bc:	89 e5                	mov    %esp,%ebp
  8008be:	57                   	push   %edi
  8008bf:	56                   	push   %esi
  8008c0:	53                   	push   %ebx
  8008c1:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008c4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008c7:	85 c9                	test   %ecx,%ecx
  8008c9:	74 36                	je     800901 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008cb:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008d1:	75 28                	jne    8008fb <memset+0x40>
  8008d3:	f6 c1 03             	test   $0x3,%cl
  8008d6:	75 23                	jne    8008fb <memset+0x40>
		c &= 0xFF;
  8008d8:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008dc:	89 d3                	mov    %edx,%ebx
  8008de:	c1 e3 08             	shl    $0x8,%ebx
  8008e1:	89 d6                	mov    %edx,%esi
  8008e3:	c1 e6 18             	shl    $0x18,%esi
  8008e6:	89 d0                	mov    %edx,%eax
  8008e8:	c1 e0 10             	shl    $0x10,%eax
  8008eb:	09 f0                	or     %esi,%eax
  8008ed:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008ef:	89 d8                	mov    %ebx,%eax
  8008f1:	09 d0                	or     %edx,%eax
  8008f3:	c1 e9 02             	shr    $0x2,%ecx
  8008f6:	fc                   	cld    
  8008f7:	f3 ab                	rep stos %eax,%es:(%edi)
  8008f9:	eb 06                	jmp    800901 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008fb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008fe:	fc                   	cld    
  8008ff:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800901:	89 f8                	mov    %edi,%eax
  800903:	5b                   	pop    %ebx
  800904:	5e                   	pop    %esi
  800905:	5f                   	pop    %edi
  800906:	5d                   	pop    %ebp
  800907:	c3                   	ret    

00800908 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800908:	55                   	push   %ebp
  800909:	89 e5                	mov    %esp,%ebp
  80090b:	57                   	push   %edi
  80090c:	56                   	push   %esi
  80090d:	8b 45 08             	mov    0x8(%ebp),%eax
  800910:	8b 75 0c             	mov    0xc(%ebp),%esi
  800913:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800916:	39 c6                	cmp    %eax,%esi
  800918:	73 33                	jae    80094d <memmove+0x45>
  80091a:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80091d:	39 d0                	cmp    %edx,%eax
  80091f:	73 2c                	jae    80094d <memmove+0x45>
		s += n;
		d += n;
  800921:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800924:	89 d6                	mov    %edx,%esi
  800926:	09 fe                	or     %edi,%esi
  800928:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80092e:	75 13                	jne    800943 <memmove+0x3b>
  800930:	f6 c1 03             	test   $0x3,%cl
  800933:	75 0e                	jne    800943 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800935:	83 ef 04             	sub    $0x4,%edi
  800938:	8d 72 fc             	lea    -0x4(%edx),%esi
  80093b:	c1 e9 02             	shr    $0x2,%ecx
  80093e:	fd                   	std    
  80093f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800941:	eb 07                	jmp    80094a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800943:	4f                   	dec    %edi
  800944:	8d 72 ff             	lea    -0x1(%edx),%esi
  800947:	fd                   	std    
  800948:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80094a:	fc                   	cld    
  80094b:	eb 1d                	jmp    80096a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80094d:	89 f2                	mov    %esi,%edx
  80094f:	09 c2                	or     %eax,%edx
  800951:	f6 c2 03             	test   $0x3,%dl
  800954:	75 0f                	jne    800965 <memmove+0x5d>
  800956:	f6 c1 03             	test   $0x3,%cl
  800959:	75 0a                	jne    800965 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80095b:	c1 e9 02             	shr    $0x2,%ecx
  80095e:	89 c7                	mov    %eax,%edi
  800960:	fc                   	cld    
  800961:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800963:	eb 05                	jmp    80096a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800965:	89 c7                	mov    %eax,%edi
  800967:	fc                   	cld    
  800968:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80096a:	5e                   	pop    %esi
  80096b:	5f                   	pop    %edi
  80096c:	5d                   	pop    %ebp
  80096d:	c3                   	ret    

0080096e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80096e:	55                   	push   %ebp
  80096f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800971:	ff 75 10             	pushl  0x10(%ebp)
  800974:	ff 75 0c             	pushl  0xc(%ebp)
  800977:	ff 75 08             	pushl  0x8(%ebp)
  80097a:	e8 89 ff ff ff       	call   800908 <memmove>
}
  80097f:	c9                   	leave  
  800980:	c3                   	ret    

00800981 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800981:	55                   	push   %ebp
  800982:	89 e5                	mov    %esp,%ebp
  800984:	56                   	push   %esi
  800985:	53                   	push   %ebx
  800986:	8b 45 08             	mov    0x8(%ebp),%eax
  800989:	8b 55 0c             	mov    0xc(%ebp),%edx
  80098c:	89 c6                	mov    %eax,%esi
  80098e:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800991:	eb 14                	jmp    8009a7 <memcmp+0x26>
		if (*s1 != *s2)
  800993:	8a 08                	mov    (%eax),%cl
  800995:	8a 1a                	mov    (%edx),%bl
  800997:	38 d9                	cmp    %bl,%cl
  800999:	74 0a                	je     8009a5 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80099b:	0f b6 c1             	movzbl %cl,%eax
  80099e:	0f b6 db             	movzbl %bl,%ebx
  8009a1:	29 d8                	sub    %ebx,%eax
  8009a3:	eb 0b                	jmp    8009b0 <memcmp+0x2f>
		s1++, s2++;
  8009a5:	40                   	inc    %eax
  8009a6:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009a7:	39 f0                	cmp    %esi,%eax
  8009a9:	75 e8                	jne    800993 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8009ab:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009b0:	5b                   	pop    %ebx
  8009b1:	5e                   	pop    %esi
  8009b2:	5d                   	pop    %ebp
  8009b3:	c3                   	ret    

008009b4 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009b4:	55                   	push   %ebp
  8009b5:	89 e5                	mov    %esp,%ebp
  8009b7:	53                   	push   %ebx
  8009b8:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009bb:	89 c1                	mov    %eax,%ecx
  8009bd:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009c0:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009c4:	eb 08                	jmp    8009ce <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009c6:	0f b6 10             	movzbl (%eax),%edx
  8009c9:	39 da                	cmp    %ebx,%edx
  8009cb:	74 05                	je     8009d2 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009cd:	40                   	inc    %eax
  8009ce:	39 c8                	cmp    %ecx,%eax
  8009d0:	72 f4                	jb     8009c6 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009d2:	5b                   	pop    %ebx
  8009d3:	5d                   	pop    %ebp
  8009d4:	c3                   	ret    

008009d5 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009d5:	55                   	push   %ebp
  8009d6:	89 e5                	mov    %esp,%ebp
  8009d8:	57                   	push   %edi
  8009d9:	56                   	push   %esi
  8009da:	53                   	push   %ebx
  8009db:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009de:	eb 01                	jmp    8009e1 <strtol+0xc>
		s++;
  8009e0:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009e1:	8a 01                	mov    (%ecx),%al
  8009e3:	3c 20                	cmp    $0x20,%al
  8009e5:	74 f9                	je     8009e0 <strtol+0xb>
  8009e7:	3c 09                	cmp    $0x9,%al
  8009e9:	74 f5                	je     8009e0 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009eb:	3c 2b                	cmp    $0x2b,%al
  8009ed:	75 08                	jne    8009f7 <strtol+0x22>
		s++;
  8009ef:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009f0:	bf 00 00 00 00       	mov    $0x0,%edi
  8009f5:	eb 11                	jmp    800a08 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009f7:	3c 2d                	cmp    $0x2d,%al
  8009f9:	75 08                	jne    800a03 <strtol+0x2e>
		s++, neg = 1;
  8009fb:	41                   	inc    %ecx
  8009fc:	bf 01 00 00 00       	mov    $0x1,%edi
  800a01:	eb 05                	jmp    800a08 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a03:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a08:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a0c:	0f 84 87 00 00 00    	je     800a99 <strtol+0xc4>
  800a12:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a16:	75 27                	jne    800a3f <strtol+0x6a>
  800a18:	80 39 30             	cmpb   $0x30,(%ecx)
  800a1b:	75 22                	jne    800a3f <strtol+0x6a>
  800a1d:	e9 88 00 00 00       	jmp    800aaa <strtol+0xd5>
		s += 2, base = 16;
  800a22:	83 c1 02             	add    $0x2,%ecx
  800a25:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a2c:	eb 11                	jmp    800a3f <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a2e:	41                   	inc    %ecx
  800a2f:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a36:	eb 07                	jmp    800a3f <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a38:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a3f:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a44:	8a 11                	mov    (%ecx),%dl
  800a46:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a49:	80 fb 09             	cmp    $0x9,%bl
  800a4c:	77 08                	ja     800a56 <strtol+0x81>
			dig = *s - '0';
  800a4e:	0f be d2             	movsbl %dl,%edx
  800a51:	83 ea 30             	sub    $0x30,%edx
  800a54:	eb 22                	jmp    800a78 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a56:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a59:	89 f3                	mov    %esi,%ebx
  800a5b:	80 fb 19             	cmp    $0x19,%bl
  800a5e:	77 08                	ja     800a68 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a60:	0f be d2             	movsbl %dl,%edx
  800a63:	83 ea 57             	sub    $0x57,%edx
  800a66:	eb 10                	jmp    800a78 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a68:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a6b:	89 f3                	mov    %esi,%ebx
  800a6d:	80 fb 19             	cmp    $0x19,%bl
  800a70:	77 14                	ja     800a86 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a72:	0f be d2             	movsbl %dl,%edx
  800a75:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a78:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a7b:	7d 09                	jge    800a86 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a7d:	41                   	inc    %ecx
  800a7e:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a82:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a84:	eb be                	jmp    800a44 <strtol+0x6f>

	if (endptr)
  800a86:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a8a:	74 05                	je     800a91 <strtol+0xbc>
		*endptr = (char *) s;
  800a8c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a8f:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a91:	85 ff                	test   %edi,%edi
  800a93:	74 21                	je     800ab6 <strtol+0xe1>
  800a95:	f7 d8                	neg    %eax
  800a97:	eb 1d                	jmp    800ab6 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a99:	80 39 30             	cmpb   $0x30,(%ecx)
  800a9c:	75 9a                	jne    800a38 <strtol+0x63>
  800a9e:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800aa2:	0f 84 7a ff ff ff    	je     800a22 <strtol+0x4d>
  800aa8:	eb 84                	jmp    800a2e <strtol+0x59>
  800aaa:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800aae:	0f 84 6e ff ff ff    	je     800a22 <strtol+0x4d>
  800ab4:	eb 89                	jmp    800a3f <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800ab6:	5b                   	pop    %ebx
  800ab7:	5e                   	pop    %esi
  800ab8:	5f                   	pop    %edi
  800ab9:	5d                   	pop    %ebp
  800aba:	c3                   	ret    

00800abb <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800abb:	55                   	push   %ebp
  800abc:	89 e5                	mov    %esp,%ebp
  800abe:	57                   	push   %edi
  800abf:	56                   	push   %esi
  800ac0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac1:	b8 00 00 00 00       	mov    $0x0,%eax
  800ac6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ac9:	8b 55 08             	mov    0x8(%ebp),%edx
  800acc:	89 c3                	mov    %eax,%ebx
  800ace:	89 c7                	mov    %eax,%edi
  800ad0:	89 c6                	mov    %eax,%esi
  800ad2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800ad4:	5b                   	pop    %ebx
  800ad5:	5e                   	pop    %esi
  800ad6:	5f                   	pop    %edi
  800ad7:	5d                   	pop    %ebp
  800ad8:	c3                   	ret    

00800ad9 <sys_cgetc>:

int
sys_cgetc(void)
{
  800ad9:	55                   	push   %ebp
  800ada:	89 e5                	mov    %esp,%ebp
  800adc:	57                   	push   %edi
  800add:	56                   	push   %esi
  800ade:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800adf:	ba 00 00 00 00       	mov    $0x0,%edx
  800ae4:	b8 01 00 00 00       	mov    $0x1,%eax
  800ae9:	89 d1                	mov    %edx,%ecx
  800aeb:	89 d3                	mov    %edx,%ebx
  800aed:	89 d7                	mov    %edx,%edi
  800aef:	89 d6                	mov    %edx,%esi
  800af1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800af3:	5b                   	pop    %ebx
  800af4:	5e                   	pop    %esi
  800af5:	5f                   	pop    %edi
  800af6:	5d                   	pop    %ebp
  800af7:	c3                   	ret    

00800af8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800af8:	55                   	push   %ebp
  800af9:	89 e5                	mov    %esp,%ebp
  800afb:	57                   	push   %edi
  800afc:	56                   	push   %esi
  800afd:	53                   	push   %ebx
  800afe:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b01:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b06:	b8 03 00 00 00       	mov    $0x3,%eax
  800b0b:	8b 55 08             	mov    0x8(%ebp),%edx
  800b0e:	89 cb                	mov    %ecx,%ebx
  800b10:	89 cf                	mov    %ecx,%edi
  800b12:	89 ce                	mov    %ecx,%esi
  800b14:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b16:	85 c0                	test   %eax,%eax
  800b18:	7e 17                	jle    800b31 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b1a:	83 ec 0c             	sub    $0xc,%esp
  800b1d:	50                   	push   %eax
  800b1e:	6a 03                	push   $0x3
  800b20:	68 80 13 80 00       	push   $0x801380
  800b25:	6a 23                	push   $0x23
  800b27:	68 9d 13 80 00       	push   $0x80139d
  800b2c:	e8 26 f6 ff ff       	call   800157 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b31:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b34:	5b                   	pop    %ebx
  800b35:	5e                   	pop    %esi
  800b36:	5f                   	pop    %edi
  800b37:	5d                   	pop    %ebp
  800b38:	c3                   	ret    

00800b39 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b39:	55                   	push   %ebp
  800b3a:	89 e5                	mov    %esp,%ebp
  800b3c:	57                   	push   %edi
  800b3d:	56                   	push   %esi
  800b3e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b3f:	ba 00 00 00 00       	mov    $0x0,%edx
  800b44:	b8 02 00 00 00       	mov    $0x2,%eax
  800b49:	89 d1                	mov    %edx,%ecx
  800b4b:	89 d3                	mov    %edx,%ebx
  800b4d:	89 d7                	mov    %edx,%edi
  800b4f:	89 d6                	mov    %edx,%esi
  800b51:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b53:	5b                   	pop    %ebx
  800b54:	5e                   	pop    %esi
  800b55:	5f                   	pop    %edi
  800b56:	5d                   	pop    %ebp
  800b57:	c3                   	ret    

00800b58 <sys_yield>:

void
sys_yield(void)
{
  800b58:	55                   	push   %ebp
  800b59:	89 e5                	mov    %esp,%ebp
  800b5b:	57                   	push   %edi
  800b5c:	56                   	push   %esi
  800b5d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b5e:	ba 00 00 00 00       	mov    $0x0,%edx
  800b63:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b68:	89 d1                	mov    %edx,%ecx
  800b6a:	89 d3                	mov    %edx,%ebx
  800b6c:	89 d7                	mov    %edx,%edi
  800b6e:	89 d6                	mov    %edx,%esi
  800b70:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b72:	5b                   	pop    %ebx
  800b73:	5e                   	pop    %esi
  800b74:	5f                   	pop    %edi
  800b75:	5d                   	pop    %ebp
  800b76:	c3                   	ret    

00800b77 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b77:	55                   	push   %ebp
  800b78:	89 e5                	mov    %esp,%ebp
  800b7a:	57                   	push   %edi
  800b7b:	56                   	push   %esi
  800b7c:	53                   	push   %ebx
  800b7d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b80:	be 00 00 00 00       	mov    $0x0,%esi
  800b85:	b8 04 00 00 00       	mov    $0x4,%eax
  800b8a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b8d:	8b 55 08             	mov    0x8(%ebp),%edx
  800b90:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b93:	89 f7                	mov    %esi,%edi
  800b95:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b97:	85 c0                	test   %eax,%eax
  800b99:	7e 17                	jle    800bb2 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b9b:	83 ec 0c             	sub    $0xc,%esp
  800b9e:	50                   	push   %eax
  800b9f:	6a 04                	push   $0x4
  800ba1:	68 80 13 80 00       	push   $0x801380
  800ba6:	6a 23                	push   $0x23
  800ba8:	68 9d 13 80 00       	push   $0x80139d
  800bad:	e8 a5 f5 ff ff       	call   800157 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800bb2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bb5:	5b                   	pop    %ebx
  800bb6:	5e                   	pop    %esi
  800bb7:	5f                   	pop    %edi
  800bb8:	5d                   	pop    %ebp
  800bb9:	c3                   	ret    

00800bba <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800bba:	55                   	push   %ebp
  800bbb:	89 e5                	mov    %esp,%ebp
  800bbd:	57                   	push   %edi
  800bbe:	56                   	push   %esi
  800bbf:	53                   	push   %ebx
  800bc0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc3:	b8 05 00 00 00       	mov    $0x5,%eax
  800bc8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bcb:	8b 55 08             	mov    0x8(%ebp),%edx
  800bce:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800bd1:	8b 7d 14             	mov    0x14(%ebp),%edi
  800bd4:	8b 75 18             	mov    0x18(%ebp),%esi
  800bd7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bd9:	85 c0                	test   %eax,%eax
  800bdb:	7e 17                	jle    800bf4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bdd:	83 ec 0c             	sub    $0xc,%esp
  800be0:	50                   	push   %eax
  800be1:	6a 05                	push   $0x5
  800be3:	68 80 13 80 00       	push   $0x801380
  800be8:	6a 23                	push   $0x23
  800bea:	68 9d 13 80 00       	push   $0x80139d
  800bef:	e8 63 f5 ff ff       	call   800157 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bf4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf7:	5b                   	pop    %ebx
  800bf8:	5e                   	pop    %esi
  800bf9:	5f                   	pop    %edi
  800bfa:	5d                   	pop    %ebp
  800bfb:	c3                   	ret    

00800bfc <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bfc:	55                   	push   %ebp
  800bfd:	89 e5                	mov    %esp,%ebp
  800bff:	57                   	push   %edi
  800c00:	56                   	push   %esi
  800c01:	53                   	push   %ebx
  800c02:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c05:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c0a:	b8 06 00 00 00       	mov    $0x6,%eax
  800c0f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c12:	8b 55 08             	mov    0x8(%ebp),%edx
  800c15:	89 df                	mov    %ebx,%edi
  800c17:	89 de                	mov    %ebx,%esi
  800c19:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c1b:	85 c0                	test   %eax,%eax
  800c1d:	7e 17                	jle    800c36 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c1f:	83 ec 0c             	sub    $0xc,%esp
  800c22:	50                   	push   %eax
  800c23:	6a 06                	push   $0x6
  800c25:	68 80 13 80 00       	push   $0x801380
  800c2a:	6a 23                	push   $0x23
  800c2c:	68 9d 13 80 00       	push   $0x80139d
  800c31:	e8 21 f5 ff ff       	call   800157 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c36:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c39:	5b                   	pop    %ebx
  800c3a:	5e                   	pop    %esi
  800c3b:	5f                   	pop    %edi
  800c3c:	5d                   	pop    %ebp
  800c3d:	c3                   	ret    

00800c3e <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c3e:	55                   	push   %ebp
  800c3f:	89 e5                	mov    %esp,%ebp
  800c41:	57                   	push   %edi
  800c42:	56                   	push   %esi
  800c43:	53                   	push   %ebx
  800c44:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c47:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c4c:	b8 08 00 00 00       	mov    $0x8,%eax
  800c51:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c54:	8b 55 08             	mov    0x8(%ebp),%edx
  800c57:	89 df                	mov    %ebx,%edi
  800c59:	89 de                	mov    %ebx,%esi
  800c5b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c5d:	85 c0                	test   %eax,%eax
  800c5f:	7e 17                	jle    800c78 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c61:	83 ec 0c             	sub    $0xc,%esp
  800c64:	50                   	push   %eax
  800c65:	6a 08                	push   $0x8
  800c67:	68 80 13 80 00       	push   $0x801380
  800c6c:	6a 23                	push   $0x23
  800c6e:	68 9d 13 80 00       	push   $0x80139d
  800c73:	e8 df f4 ff ff       	call   800157 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c78:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c7b:	5b                   	pop    %ebx
  800c7c:	5e                   	pop    %esi
  800c7d:	5f                   	pop    %edi
  800c7e:	5d                   	pop    %ebp
  800c7f:	c3                   	ret    

00800c80 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c80:	55                   	push   %ebp
  800c81:	89 e5                	mov    %esp,%ebp
  800c83:	57                   	push   %edi
  800c84:	56                   	push   %esi
  800c85:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c86:	ba 00 00 00 00       	mov    $0x0,%edx
  800c8b:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c90:	89 d1                	mov    %edx,%ecx
  800c92:	89 d3                	mov    %edx,%ebx
  800c94:	89 d7                	mov    %edx,%edi
  800c96:	89 d6                	mov    %edx,%esi
  800c98:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c9a:	5b                   	pop    %ebx
  800c9b:	5e                   	pop    %esi
  800c9c:	5f                   	pop    %edi
  800c9d:	5d                   	pop    %ebp
  800c9e:	c3                   	ret    

00800c9f <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c9f:	55                   	push   %ebp
  800ca0:	89 e5                	mov    %esp,%ebp
  800ca2:	57                   	push   %edi
  800ca3:	56                   	push   %esi
  800ca4:	53                   	push   %ebx
  800ca5:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ca8:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cad:	b8 09 00 00 00       	mov    $0x9,%eax
  800cb2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cb5:	8b 55 08             	mov    0x8(%ebp),%edx
  800cb8:	89 df                	mov    %ebx,%edi
  800cba:	89 de                	mov    %ebx,%esi
  800cbc:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cbe:	85 c0                	test   %eax,%eax
  800cc0:	7e 17                	jle    800cd9 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cc2:	83 ec 0c             	sub    $0xc,%esp
  800cc5:	50                   	push   %eax
  800cc6:	6a 09                	push   $0x9
  800cc8:	68 80 13 80 00       	push   $0x801380
  800ccd:	6a 23                	push   $0x23
  800ccf:	68 9d 13 80 00       	push   $0x80139d
  800cd4:	e8 7e f4 ff ff       	call   800157 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800cd9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cdc:	5b                   	pop    %ebx
  800cdd:	5e                   	pop    %esi
  800cde:	5f                   	pop    %edi
  800cdf:	5d                   	pop    %ebp
  800ce0:	c3                   	ret    

00800ce1 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800ce1:	55                   	push   %ebp
  800ce2:	89 e5                	mov    %esp,%ebp
  800ce4:	57                   	push   %edi
  800ce5:	56                   	push   %esi
  800ce6:	53                   	push   %ebx
  800ce7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cea:	bb 00 00 00 00       	mov    $0x0,%ebx
  800cef:	b8 0a 00 00 00       	mov    $0xa,%eax
  800cf4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cf7:	8b 55 08             	mov    0x8(%ebp),%edx
  800cfa:	89 df                	mov    %ebx,%edi
  800cfc:	89 de                	mov    %ebx,%esi
  800cfe:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d00:	85 c0                	test   %eax,%eax
  800d02:	7e 17                	jle    800d1b <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d04:	83 ec 0c             	sub    $0xc,%esp
  800d07:	50                   	push   %eax
  800d08:	6a 0a                	push   $0xa
  800d0a:	68 80 13 80 00       	push   $0x801380
  800d0f:	6a 23                	push   $0x23
  800d11:	68 9d 13 80 00       	push   $0x80139d
  800d16:	e8 3c f4 ff ff       	call   800157 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800d1b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d1e:	5b                   	pop    %ebx
  800d1f:	5e                   	pop    %esi
  800d20:	5f                   	pop    %edi
  800d21:	5d                   	pop    %ebp
  800d22:	c3                   	ret    

00800d23 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800d23:	55                   	push   %ebp
  800d24:	89 e5                	mov    %esp,%ebp
  800d26:	57                   	push   %edi
  800d27:	56                   	push   %esi
  800d28:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d29:	be 00 00 00 00       	mov    $0x0,%esi
  800d2e:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d33:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d36:	8b 55 08             	mov    0x8(%ebp),%edx
  800d39:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d3c:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d3f:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d41:	5b                   	pop    %ebx
  800d42:	5e                   	pop    %esi
  800d43:	5f                   	pop    %edi
  800d44:	5d                   	pop    %ebp
  800d45:	c3                   	ret    

00800d46 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d46:	55                   	push   %ebp
  800d47:	89 e5                	mov    %esp,%ebp
  800d49:	57                   	push   %edi
  800d4a:	56                   	push   %esi
  800d4b:	53                   	push   %ebx
  800d4c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d4f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d54:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d59:	8b 55 08             	mov    0x8(%ebp),%edx
  800d5c:	89 cb                	mov    %ecx,%ebx
  800d5e:	89 cf                	mov    %ecx,%edi
  800d60:	89 ce                	mov    %ecx,%esi
  800d62:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d64:	85 c0                	test   %eax,%eax
  800d66:	7e 17                	jle    800d7f <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d68:	83 ec 0c             	sub    $0xc,%esp
  800d6b:	50                   	push   %eax
  800d6c:	6a 0e                	push   $0xe
  800d6e:	68 80 13 80 00       	push   $0x801380
  800d73:	6a 23                	push   $0x23
  800d75:	68 9d 13 80 00       	push   $0x80139d
  800d7a:	e8 d8 f3 ff ff       	call   800157 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d7f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d82:	5b                   	pop    %ebx
  800d83:	5e                   	pop    %esi
  800d84:	5f                   	pop    %edi
  800d85:	5d                   	pop    %ebp
  800d86:	c3                   	ret    
  800d87:	90                   	nop

00800d88 <__udivdi3>:
  800d88:	55                   	push   %ebp
  800d89:	57                   	push   %edi
  800d8a:	56                   	push   %esi
  800d8b:	53                   	push   %ebx
  800d8c:	83 ec 1c             	sub    $0x1c,%esp
  800d8f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800d93:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800d97:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800d9b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800d9f:	89 ca                	mov    %ecx,%edx
  800da1:	89 f8                	mov    %edi,%eax
  800da3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800da7:	85 f6                	test   %esi,%esi
  800da9:	75 2d                	jne    800dd8 <__udivdi3+0x50>
  800dab:	39 cf                	cmp    %ecx,%edi
  800dad:	77 65                	ja     800e14 <__udivdi3+0x8c>
  800daf:	89 fd                	mov    %edi,%ebp
  800db1:	85 ff                	test   %edi,%edi
  800db3:	75 0b                	jne    800dc0 <__udivdi3+0x38>
  800db5:	b8 01 00 00 00       	mov    $0x1,%eax
  800dba:	31 d2                	xor    %edx,%edx
  800dbc:	f7 f7                	div    %edi
  800dbe:	89 c5                	mov    %eax,%ebp
  800dc0:	31 d2                	xor    %edx,%edx
  800dc2:	89 c8                	mov    %ecx,%eax
  800dc4:	f7 f5                	div    %ebp
  800dc6:	89 c1                	mov    %eax,%ecx
  800dc8:	89 d8                	mov    %ebx,%eax
  800dca:	f7 f5                	div    %ebp
  800dcc:	89 cf                	mov    %ecx,%edi
  800dce:	89 fa                	mov    %edi,%edx
  800dd0:	83 c4 1c             	add    $0x1c,%esp
  800dd3:	5b                   	pop    %ebx
  800dd4:	5e                   	pop    %esi
  800dd5:	5f                   	pop    %edi
  800dd6:	5d                   	pop    %ebp
  800dd7:	c3                   	ret    
  800dd8:	39 ce                	cmp    %ecx,%esi
  800dda:	77 28                	ja     800e04 <__udivdi3+0x7c>
  800ddc:	0f bd fe             	bsr    %esi,%edi
  800ddf:	83 f7 1f             	xor    $0x1f,%edi
  800de2:	75 40                	jne    800e24 <__udivdi3+0x9c>
  800de4:	39 ce                	cmp    %ecx,%esi
  800de6:	72 0a                	jb     800df2 <__udivdi3+0x6a>
  800de8:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800dec:	0f 87 9e 00 00 00    	ja     800e90 <__udivdi3+0x108>
  800df2:	b8 01 00 00 00       	mov    $0x1,%eax
  800df7:	89 fa                	mov    %edi,%edx
  800df9:	83 c4 1c             	add    $0x1c,%esp
  800dfc:	5b                   	pop    %ebx
  800dfd:	5e                   	pop    %esi
  800dfe:	5f                   	pop    %edi
  800dff:	5d                   	pop    %ebp
  800e00:	c3                   	ret    
  800e01:	8d 76 00             	lea    0x0(%esi),%esi
  800e04:	31 ff                	xor    %edi,%edi
  800e06:	31 c0                	xor    %eax,%eax
  800e08:	89 fa                	mov    %edi,%edx
  800e0a:	83 c4 1c             	add    $0x1c,%esp
  800e0d:	5b                   	pop    %ebx
  800e0e:	5e                   	pop    %esi
  800e0f:	5f                   	pop    %edi
  800e10:	5d                   	pop    %ebp
  800e11:	c3                   	ret    
  800e12:	66 90                	xchg   %ax,%ax
  800e14:	89 d8                	mov    %ebx,%eax
  800e16:	f7 f7                	div    %edi
  800e18:	31 ff                	xor    %edi,%edi
  800e1a:	89 fa                	mov    %edi,%edx
  800e1c:	83 c4 1c             	add    $0x1c,%esp
  800e1f:	5b                   	pop    %ebx
  800e20:	5e                   	pop    %esi
  800e21:	5f                   	pop    %edi
  800e22:	5d                   	pop    %ebp
  800e23:	c3                   	ret    
  800e24:	bd 20 00 00 00       	mov    $0x20,%ebp
  800e29:	89 eb                	mov    %ebp,%ebx
  800e2b:	29 fb                	sub    %edi,%ebx
  800e2d:	89 f9                	mov    %edi,%ecx
  800e2f:	d3 e6                	shl    %cl,%esi
  800e31:	89 c5                	mov    %eax,%ebp
  800e33:	88 d9                	mov    %bl,%cl
  800e35:	d3 ed                	shr    %cl,%ebp
  800e37:	89 e9                	mov    %ebp,%ecx
  800e39:	09 f1                	or     %esi,%ecx
  800e3b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800e3f:	89 f9                	mov    %edi,%ecx
  800e41:	d3 e0                	shl    %cl,%eax
  800e43:	89 c5                	mov    %eax,%ebp
  800e45:	89 d6                	mov    %edx,%esi
  800e47:	88 d9                	mov    %bl,%cl
  800e49:	d3 ee                	shr    %cl,%esi
  800e4b:	89 f9                	mov    %edi,%ecx
  800e4d:	d3 e2                	shl    %cl,%edx
  800e4f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e53:	88 d9                	mov    %bl,%cl
  800e55:	d3 e8                	shr    %cl,%eax
  800e57:	09 c2                	or     %eax,%edx
  800e59:	89 d0                	mov    %edx,%eax
  800e5b:	89 f2                	mov    %esi,%edx
  800e5d:	f7 74 24 0c          	divl   0xc(%esp)
  800e61:	89 d6                	mov    %edx,%esi
  800e63:	89 c3                	mov    %eax,%ebx
  800e65:	f7 e5                	mul    %ebp
  800e67:	39 d6                	cmp    %edx,%esi
  800e69:	72 19                	jb     800e84 <__udivdi3+0xfc>
  800e6b:	74 0b                	je     800e78 <__udivdi3+0xf0>
  800e6d:	89 d8                	mov    %ebx,%eax
  800e6f:	31 ff                	xor    %edi,%edi
  800e71:	e9 58 ff ff ff       	jmp    800dce <__udivdi3+0x46>
  800e76:	66 90                	xchg   %ax,%ax
  800e78:	8b 54 24 08          	mov    0x8(%esp),%edx
  800e7c:	89 f9                	mov    %edi,%ecx
  800e7e:	d3 e2                	shl    %cl,%edx
  800e80:	39 c2                	cmp    %eax,%edx
  800e82:	73 e9                	jae    800e6d <__udivdi3+0xe5>
  800e84:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800e87:	31 ff                	xor    %edi,%edi
  800e89:	e9 40 ff ff ff       	jmp    800dce <__udivdi3+0x46>
  800e8e:	66 90                	xchg   %ax,%ax
  800e90:	31 c0                	xor    %eax,%eax
  800e92:	e9 37 ff ff ff       	jmp    800dce <__udivdi3+0x46>
  800e97:	90                   	nop

00800e98 <__umoddi3>:
  800e98:	55                   	push   %ebp
  800e99:	57                   	push   %edi
  800e9a:	56                   	push   %esi
  800e9b:	53                   	push   %ebx
  800e9c:	83 ec 1c             	sub    $0x1c,%esp
  800e9f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800ea3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800ea7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800eab:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800eaf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800eb3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800eb7:	89 f3                	mov    %esi,%ebx
  800eb9:	89 fa                	mov    %edi,%edx
  800ebb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800ebf:	89 34 24             	mov    %esi,(%esp)
  800ec2:	85 c0                	test   %eax,%eax
  800ec4:	75 1a                	jne    800ee0 <__umoddi3+0x48>
  800ec6:	39 f7                	cmp    %esi,%edi
  800ec8:	0f 86 a2 00 00 00    	jbe    800f70 <__umoddi3+0xd8>
  800ece:	89 c8                	mov    %ecx,%eax
  800ed0:	89 f2                	mov    %esi,%edx
  800ed2:	f7 f7                	div    %edi
  800ed4:	89 d0                	mov    %edx,%eax
  800ed6:	31 d2                	xor    %edx,%edx
  800ed8:	83 c4 1c             	add    $0x1c,%esp
  800edb:	5b                   	pop    %ebx
  800edc:	5e                   	pop    %esi
  800edd:	5f                   	pop    %edi
  800ede:	5d                   	pop    %ebp
  800edf:	c3                   	ret    
  800ee0:	39 f0                	cmp    %esi,%eax
  800ee2:	0f 87 ac 00 00 00    	ja     800f94 <__umoddi3+0xfc>
  800ee8:	0f bd e8             	bsr    %eax,%ebp
  800eeb:	83 f5 1f             	xor    $0x1f,%ebp
  800eee:	0f 84 ac 00 00 00    	je     800fa0 <__umoddi3+0x108>
  800ef4:	bf 20 00 00 00       	mov    $0x20,%edi
  800ef9:	29 ef                	sub    %ebp,%edi
  800efb:	89 fe                	mov    %edi,%esi
  800efd:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800f01:	89 e9                	mov    %ebp,%ecx
  800f03:	d3 e0                	shl    %cl,%eax
  800f05:	89 d7                	mov    %edx,%edi
  800f07:	89 f1                	mov    %esi,%ecx
  800f09:	d3 ef                	shr    %cl,%edi
  800f0b:	09 c7                	or     %eax,%edi
  800f0d:	89 e9                	mov    %ebp,%ecx
  800f0f:	d3 e2                	shl    %cl,%edx
  800f11:	89 14 24             	mov    %edx,(%esp)
  800f14:	89 d8                	mov    %ebx,%eax
  800f16:	d3 e0                	shl    %cl,%eax
  800f18:	89 c2                	mov    %eax,%edx
  800f1a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f1e:	d3 e0                	shl    %cl,%eax
  800f20:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f24:	8b 44 24 08          	mov    0x8(%esp),%eax
  800f28:	89 f1                	mov    %esi,%ecx
  800f2a:	d3 e8                	shr    %cl,%eax
  800f2c:	09 d0                	or     %edx,%eax
  800f2e:	d3 eb                	shr    %cl,%ebx
  800f30:	89 da                	mov    %ebx,%edx
  800f32:	f7 f7                	div    %edi
  800f34:	89 d3                	mov    %edx,%ebx
  800f36:	f7 24 24             	mull   (%esp)
  800f39:	89 c6                	mov    %eax,%esi
  800f3b:	89 d1                	mov    %edx,%ecx
  800f3d:	39 d3                	cmp    %edx,%ebx
  800f3f:	0f 82 87 00 00 00    	jb     800fcc <__umoddi3+0x134>
  800f45:	0f 84 91 00 00 00    	je     800fdc <__umoddi3+0x144>
  800f4b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800f4f:	29 f2                	sub    %esi,%edx
  800f51:	19 cb                	sbb    %ecx,%ebx
  800f53:	89 d8                	mov    %ebx,%eax
  800f55:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800f59:	d3 e0                	shl    %cl,%eax
  800f5b:	89 e9                	mov    %ebp,%ecx
  800f5d:	d3 ea                	shr    %cl,%edx
  800f5f:	09 d0                	or     %edx,%eax
  800f61:	89 e9                	mov    %ebp,%ecx
  800f63:	d3 eb                	shr    %cl,%ebx
  800f65:	89 da                	mov    %ebx,%edx
  800f67:	83 c4 1c             	add    $0x1c,%esp
  800f6a:	5b                   	pop    %ebx
  800f6b:	5e                   	pop    %esi
  800f6c:	5f                   	pop    %edi
  800f6d:	5d                   	pop    %ebp
  800f6e:	c3                   	ret    
  800f6f:	90                   	nop
  800f70:	89 fd                	mov    %edi,%ebp
  800f72:	85 ff                	test   %edi,%edi
  800f74:	75 0b                	jne    800f81 <__umoddi3+0xe9>
  800f76:	b8 01 00 00 00       	mov    $0x1,%eax
  800f7b:	31 d2                	xor    %edx,%edx
  800f7d:	f7 f7                	div    %edi
  800f7f:	89 c5                	mov    %eax,%ebp
  800f81:	89 f0                	mov    %esi,%eax
  800f83:	31 d2                	xor    %edx,%edx
  800f85:	f7 f5                	div    %ebp
  800f87:	89 c8                	mov    %ecx,%eax
  800f89:	f7 f5                	div    %ebp
  800f8b:	89 d0                	mov    %edx,%eax
  800f8d:	e9 44 ff ff ff       	jmp    800ed6 <__umoddi3+0x3e>
  800f92:	66 90                	xchg   %ax,%ax
  800f94:	89 c8                	mov    %ecx,%eax
  800f96:	89 f2                	mov    %esi,%edx
  800f98:	83 c4 1c             	add    $0x1c,%esp
  800f9b:	5b                   	pop    %ebx
  800f9c:	5e                   	pop    %esi
  800f9d:	5f                   	pop    %edi
  800f9e:	5d                   	pop    %ebp
  800f9f:	c3                   	ret    
  800fa0:	3b 04 24             	cmp    (%esp),%eax
  800fa3:	72 06                	jb     800fab <__umoddi3+0x113>
  800fa5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800fa9:	77 0f                	ja     800fba <__umoddi3+0x122>
  800fab:	89 f2                	mov    %esi,%edx
  800fad:	29 f9                	sub    %edi,%ecx
  800faf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800fb3:	89 14 24             	mov    %edx,(%esp)
  800fb6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800fba:	8b 44 24 04          	mov    0x4(%esp),%eax
  800fbe:	8b 14 24             	mov    (%esp),%edx
  800fc1:	83 c4 1c             	add    $0x1c,%esp
  800fc4:	5b                   	pop    %ebx
  800fc5:	5e                   	pop    %esi
  800fc6:	5f                   	pop    %edi
  800fc7:	5d                   	pop    %ebp
  800fc8:	c3                   	ret    
  800fc9:	8d 76 00             	lea    0x0(%esi),%esi
  800fcc:	2b 04 24             	sub    (%esp),%eax
  800fcf:	19 fa                	sbb    %edi,%edx
  800fd1:	89 d1                	mov    %edx,%ecx
  800fd3:	89 c6                	mov    %eax,%esi
  800fd5:	e9 71 ff ff ff       	jmp    800f4b <__umoddi3+0xb3>
  800fda:	66 90                	xchg   %ax,%ax
  800fdc:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800fe0:	72 ea                	jb     800fcc <__umoddi3+0x134>
  800fe2:	89 d9                	mov    %ebx,%ecx
  800fe4:	e9 62 ff ff ff       	jmp    800f4b <__umoddi3+0xb3>
