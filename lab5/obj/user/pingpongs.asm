
obj/user/pingpongs.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 cb 00 00 00       	call   8000fc <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

uint32_t val;

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	57                   	push   %edi
  800037:	56                   	push   %esi
  800038:	53                   	push   %ebx
  800039:	83 ec 2c             	sub    $0x2c,%esp
	envid_t who;
	uint32_t i;

	i = 0;
	if ((who = sfork()) != 0) {
  80003c:	e8 c2 0f 00 00       	call   801003 <sfork>
  800041:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800044:	85 c0                	test   %eax,%eax
  800046:	74 42                	je     80008a <umain+0x57>
		cprintf("i am %08x; thisenv is %p\n", sys_getenvid(), thisenv);
  800048:	8b 1d 08 20 80 00    	mov    0x802008,%ebx
  80004e:	e8 a6 0a 00 00       	call   800af9 <sys_getenvid>
  800053:	83 ec 04             	sub    $0x4,%esp
  800056:	53                   	push   %ebx
  800057:	50                   	push   %eax
  800058:	68 60 14 80 00       	push   $0x801460
  80005d:	e8 8d 01 00 00       	call   8001ef <cprintf>
		// get the ball rolling
		cprintf("send 0 from %x to %x\n", sys_getenvid(), who);
  800062:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
  800065:	e8 8f 0a 00 00       	call   800af9 <sys_getenvid>
  80006a:	83 c4 0c             	add    $0xc,%esp
  80006d:	53                   	push   %ebx
  80006e:	50                   	push   %eax
  80006f:	68 7a 14 80 00       	push   $0x80147a
  800074:	e8 76 01 00 00       	call   8001ef <cprintf>
		ipc_send(who, 0, 0, 0);
  800079:	6a 00                	push   $0x0
  80007b:	6a 00                	push   $0x0
  80007d:	6a 00                	push   $0x0
  80007f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800082:	e8 fe 0f 00 00       	call   801085 <ipc_send>
  800087:	83 c4 20             	add    $0x20,%esp
	}

	while (1) {
		ipc_recv(&who, 0, 0);
  80008a:	83 ec 04             	sub    $0x4,%esp
  80008d:	6a 00                	push   $0x0
  80008f:	6a 00                	push   $0x0
  800091:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  800094:	50                   	push   %eax
  800095:	e8 83 0f 00 00       	call   80101d <ipc_recv>
		cprintf("%x got %d from %x (thisenv is %p %x)\n", sys_getenvid(), val, who, thisenv, thisenv->env_id);
  80009a:	8b 1d 08 20 80 00    	mov    0x802008,%ebx
  8000a0:	8b 7b 48             	mov    0x48(%ebx),%edi
  8000a3:	8b 75 e4             	mov    -0x1c(%ebp),%esi
  8000a6:	a1 04 20 80 00       	mov    0x802004,%eax
  8000ab:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  8000ae:	e8 46 0a 00 00       	call   800af9 <sys_getenvid>
  8000b3:	83 c4 08             	add    $0x8,%esp
  8000b6:	57                   	push   %edi
  8000b7:	53                   	push   %ebx
  8000b8:	56                   	push   %esi
  8000b9:	ff 75 d4             	pushl  -0x2c(%ebp)
  8000bc:	50                   	push   %eax
  8000bd:	68 90 14 80 00       	push   $0x801490
  8000c2:	e8 28 01 00 00       	call   8001ef <cprintf>
		if (val == 10)
  8000c7:	a1 04 20 80 00       	mov    0x802004,%eax
  8000cc:	83 c4 20             	add    $0x20,%esp
  8000cf:	83 f8 0a             	cmp    $0xa,%eax
  8000d2:	74 20                	je     8000f4 <umain+0xc1>
			return;
		++val;
  8000d4:	40                   	inc    %eax
  8000d5:	a3 04 20 80 00       	mov    %eax,0x802004
		ipc_send(who, 0, 0, 0);
  8000da:	6a 00                	push   $0x0
  8000dc:	6a 00                	push   $0x0
  8000de:	6a 00                	push   $0x0
  8000e0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8000e3:	e8 9d 0f 00 00       	call   801085 <ipc_send>
		if (val == 10)
  8000e8:	83 c4 10             	add    $0x10,%esp
  8000eb:	83 3d 04 20 80 00 0a 	cmpl   $0xa,0x802004
  8000f2:	75 96                	jne    80008a <umain+0x57>
			return;
	}

}
  8000f4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000f7:	5b                   	pop    %ebx
  8000f8:	5e                   	pop    %esi
  8000f9:	5f                   	pop    %edi
  8000fa:	5d                   	pop    %ebp
  8000fb:	c3                   	ret    

008000fc <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000fc:	55                   	push   %ebp
  8000fd:	89 e5                	mov    %esp,%ebp
  8000ff:	56                   	push   %esi
  800100:	53                   	push   %ebx
  800101:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800104:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800107:	e8 ed 09 00 00       	call   800af9 <sys_getenvid>
  80010c:	25 ff 03 00 00       	and    $0x3ff,%eax
  800111:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800118:	c1 e0 07             	shl    $0x7,%eax
  80011b:	29 d0                	sub    %edx,%eax
  80011d:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800122:	a3 08 20 80 00       	mov    %eax,0x802008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800127:	85 db                	test   %ebx,%ebx
  800129:	7e 07                	jle    800132 <libmain+0x36>
		binaryname = argv[0];
  80012b:	8b 06                	mov    (%esi),%eax
  80012d:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800132:	83 ec 08             	sub    $0x8,%esp
  800135:	56                   	push   %esi
  800136:	53                   	push   %ebx
  800137:	e8 f7 fe ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80013c:	e8 0a 00 00 00       	call   80014b <exit>
}
  800141:	83 c4 10             	add    $0x10,%esp
  800144:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800147:	5b                   	pop    %ebx
  800148:	5e                   	pop    %esi
  800149:	5d                   	pop    %ebp
  80014a:	c3                   	ret    

0080014b <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80014b:	55                   	push   %ebp
  80014c:	89 e5                	mov    %esp,%ebp
  80014e:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800151:	6a 00                	push   $0x0
  800153:	e8 60 09 00 00       	call   800ab8 <sys_env_destroy>
}
  800158:	83 c4 10             	add    $0x10,%esp
  80015b:	c9                   	leave  
  80015c:	c3                   	ret    

0080015d <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80015d:	55                   	push   %ebp
  80015e:	89 e5                	mov    %esp,%ebp
  800160:	53                   	push   %ebx
  800161:	83 ec 04             	sub    $0x4,%esp
  800164:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800167:	8b 13                	mov    (%ebx),%edx
  800169:	8d 42 01             	lea    0x1(%edx),%eax
  80016c:	89 03                	mov    %eax,(%ebx)
  80016e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800171:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800175:	3d ff 00 00 00       	cmp    $0xff,%eax
  80017a:	75 1a                	jne    800196 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80017c:	83 ec 08             	sub    $0x8,%esp
  80017f:	68 ff 00 00 00       	push   $0xff
  800184:	8d 43 08             	lea    0x8(%ebx),%eax
  800187:	50                   	push   %eax
  800188:	e8 ee 08 00 00       	call   800a7b <sys_cputs>
		b->idx = 0;
  80018d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800193:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800196:	ff 43 04             	incl   0x4(%ebx)
}
  800199:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80019c:	c9                   	leave  
  80019d:	c3                   	ret    

0080019e <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80019e:	55                   	push   %ebp
  80019f:	89 e5                	mov    %esp,%ebp
  8001a1:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001a7:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001ae:	00 00 00 
	b.cnt = 0;
  8001b1:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001b8:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001bb:	ff 75 0c             	pushl  0xc(%ebp)
  8001be:	ff 75 08             	pushl  0x8(%ebp)
  8001c1:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001c7:	50                   	push   %eax
  8001c8:	68 5d 01 80 00       	push   $0x80015d
  8001cd:	e8 51 01 00 00       	call   800323 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001d2:	83 c4 08             	add    $0x8,%esp
  8001d5:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001db:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001e1:	50                   	push   %eax
  8001e2:	e8 94 08 00 00       	call   800a7b <sys_cputs>

	return b.cnt;
}
  8001e7:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001ed:	c9                   	leave  
  8001ee:	c3                   	ret    

008001ef <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001ef:	55                   	push   %ebp
  8001f0:	89 e5                	mov    %esp,%ebp
  8001f2:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001f5:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8001f8:	50                   	push   %eax
  8001f9:	ff 75 08             	pushl  0x8(%ebp)
  8001fc:	e8 9d ff ff ff       	call   80019e <vcprintf>
	va_end(ap);

	return cnt;
}
  800201:	c9                   	leave  
  800202:	c3                   	ret    

00800203 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800203:	55                   	push   %ebp
  800204:	89 e5                	mov    %esp,%ebp
  800206:	57                   	push   %edi
  800207:	56                   	push   %esi
  800208:	53                   	push   %ebx
  800209:	83 ec 1c             	sub    $0x1c,%esp
  80020c:	89 c7                	mov    %eax,%edi
  80020e:	89 d6                	mov    %edx,%esi
  800210:	8b 45 08             	mov    0x8(%ebp),%eax
  800213:	8b 55 0c             	mov    0xc(%ebp),%edx
  800216:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800219:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80021c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80021f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800224:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800227:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80022a:	39 d3                	cmp    %edx,%ebx
  80022c:	72 05                	jb     800233 <printnum+0x30>
  80022e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800231:	77 45                	ja     800278 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800233:	83 ec 0c             	sub    $0xc,%esp
  800236:	ff 75 18             	pushl  0x18(%ebp)
  800239:	8b 45 14             	mov    0x14(%ebp),%eax
  80023c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80023f:	53                   	push   %ebx
  800240:	ff 75 10             	pushl  0x10(%ebp)
  800243:	83 ec 08             	sub    $0x8,%esp
  800246:	ff 75 e4             	pushl  -0x1c(%ebp)
  800249:	ff 75 e0             	pushl  -0x20(%ebp)
  80024c:	ff 75 dc             	pushl  -0x24(%ebp)
  80024f:	ff 75 d8             	pushl  -0x28(%ebp)
  800252:	e8 8d 0f 00 00       	call   8011e4 <__udivdi3>
  800257:	83 c4 18             	add    $0x18,%esp
  80025a:	52                   	push   %edx
  80025b:	50                   	push   %eax
  80025c:	89 f2                	mov    %esi,%edx
  80025e:	89 f8                	mov    %edi,%eax
  800260:	e8 9e ff ff ff       	call   800203 <printnum>
  800265:	83 c4 20             	add    $0x20,%esp
  800268:	eb 16                	jmp    800280 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80026a:	83 ec 08             	sub    $0x8,%esp
  80026d:	56                   	push   %esi
  80026e:	ff 75 18             	pushl  0x18(%ebp)
  800271:	ff d7                	call   *%edi
  800273:	83 c4 10             	add    $0x10,%esp
  800276:	eb 03                	jmp    80027b <printnum+0x78>
  800278:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80027b:	4b                   	dec    %ebx
  80027c:	85 db                	test   %ebx,%ebx
  80027e:	7f ea                	jg     80026a <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800280:	83 ec 08             	sub    $0x8,%esp
  800283:	56                   	push   %esi
  800284:	83 ec 04             	sub    $0x4,%esp
  800287:	ff 75 e4             	pushl  -0x1c(%ebp)
  80028a:	ff 75 e0             	pushl  -0x20(%ebp)
  80028d:	ff 75 dc             	pushl  -0x24(%ebp)
  800290:	ff 75 d8             	pushl  -0x28(%ebp)
  800293:	e8 5c 10 00 00       	call   8012f4 <__umoddi3>
  800298:	83 c4 14             	add    $0x14,%esp
  80029b:	0f be 80 c0 14 80 00 	movsbl 0x8014c0(%eax),%eax
  8002a2:	50                   	push   %eax
  8002a3:	ff d7                	call   *%edi
}
  8002a5:	83 c4 10             	add    $0x10,%esp
  8002a8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002ab:	5b                   	pop    %ebx
  8002ac:	5e                   	pop    %esi
  8002ad:	5f                   	pop    %edi
  8002ae:	5d                   	pop    %ebp
  8002af:	c3                   	ret    

008002b0 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002b0:	55                   	push   %ebp
  8002b1:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002b3:	83 fa 01             	cmp    $0x1,%edx
  8002b6:	7e 0e                	jle    8002c6 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002b8:	8b 10                	mov    (%eax),%edx
  8002ba:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002bd:	89 08                	mov    %ecx,(%eax)
  8002bf:	8b 02                	mov    (%edx),%eax
  8002c1:	8b 52 04             	mov    0x4(%edx),%edx
  8002c4:	eb 22                	jmp    8002e8 <getuint+0x38>
	else if (lflag)
  8002c6:	85 d2                	test   %edx,%edx
  8002c8:	74 10                	je     8002da <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002ca:	8b 10                	mov    (%eax),%edx
  8002cc:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002cf:	89 08                	mov    %ecx,(%eax)
  8002d1:	8b 02                	mov    (%edx),%eax
  8002d3:	ba 00 00 00 00       	mov    $0x0,%edx
  8002d8:	eb 0e                	jmp    8002e8 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002da:	8b 10                	mov    (%eax),%edx
  8002dc:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002df:	89 08                	mov    %ecx,(%eax)
  8002e1:	8b 02                	mov    (%edx),%eax
  8002e3:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002e8:	5d                   	pop    %ebp
  8002e9:	c3                   	ret    

008002ea <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002ea:	55                   	push   %ebp
  8002eb:	89 e5                	mov    %esp,%ebp
  8002ed:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002f0:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8002f3:	8b 10                	mov    (%eax),%edx
  8002f5:	3b 50 04             	cmp    0x4(%eax),%edx
  8002f8:	73 0a                	jae    800304 <sprintputch+0x1a>
		*b->buf++ = ch;
  8002fa:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002fd:	89 08                	mov    %ecx,(%eax)
  8002ff:	8b 45 08             	mov    0x8(%ebp),%eax
  800302:	88 02                	mov    %al,(%edx)
}
  800304:	5d                   	pop    %ebp
  800305:	c3                   	ret    

00800306 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800306:	55                   	push   %ebp
  800307:	89 e5                	mov    %esp,%ebp
  800309:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80030c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80030f:	50                   	push   %eax
  800310:	ff 75 10             	pushl  0x10(%ebp)
  800313:	ff 75 0c             	pushl  0xc(%ebp)
  800316:	ff 75 08             	pushl  0x8(%ebp)
  800319:	e8 05 00 00 00       	call   800323 <vprintfmt>
	va_end(ap);
}
  80031e:	83 c4 10             	add    $0x10,%esp
  800321:	c9                   	leave  
  800322:	c3                   	ret    

00800323 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800323:	55                   	push   %ebp
  800324:	89 e5                	mov    %esp,%ebp
  800326:	57                   	push   %edi
  800327:	56                   	push   %esi
  800328:	53                   	push   %ebx
  800329:	83 ec 2c             	sub    $0x2c,%esp
  80032c:	8b 75 08             	mov    0x8(%ebp),%esi
  80032f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800332:	8b 7d 10             	mov    0x10(%ebp),%edi
  800335:	eb 12                	jmp    800349 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800337:	85 c0                	test   %eax,%eax
  800339:	0f 84 68 03 00 00    	je     8006a7 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80033f:	83 ec 08             	sub    $0x8,%esp
  800342:	53                   	push   %ebx
  800343:	50                   	push   %eax
  800344:	ff d6                	call   *%esi
  800346:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800349:	47                   	inc    %edi
  80034a:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80034e:	83 f8 25             	cmp    $0x25,%eax
  800351:	75 e4                	jne    800337 <vprintfmt+0x14>
  800353:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800357:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80035e:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800365:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80036c:	ba 00 00 00 00       	mov    $0x0,%edx
  800371:	eb 07                	jmp    80037a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800373:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800376:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80037a:	8d 47 01             	lea    0x1(%edi),%eax
  80037d:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800380:	0f b6 0f             	movzbl (%edi),%ecx
  800383:	8a 07                	mov    (%edi),%al
  800385:	83 e8 23             	sub    $0x23,%eax
  800388:	3c 55                	cmp    $0x55,%al
  80038a:	0f 87 fe 02 00 00    	ja     80068e <vprintfmt+0x36b>
  800390:	0f b6 c0             	movzbl %al,%eax
  800393:	ff 24 85 00 16 80 00 	jmp    *0x801600(,%eax,4)
  80039a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80039d:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003a1:	eb d7                	jmp    80037a <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003a6:	b8 00 00 00 00       	mov    $0x0,%eax
  8003ab:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003ae:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003b1:	01 c0                	add    %eax,%eax
  8003b3:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003b7:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003ba:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003bd:	83 fa 09             	cmp    $0x9,%edx
  8003c0:	77 34                	ja     8003f6 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003c2:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003c3:	eb e9                	jmp    8003ae <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003c5:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c8:	8d 48 04             	lea    0x4(%eax),%ecx
  8003cb:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003ce:	8b 00                	mov    (%eax),%eax
  8003d0:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003d6:	eb 24                	jmp    8003fc <vprintfmt+0xd9>
  8003d8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003dc:	79 07                	jns    8003e5 <vprintfmt+0xc2>
  8003de:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003e8:	eb 90                	jmp    80037a <vprintfmt+0x57>
  8003ea:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003ed:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8003f4:	eb 84                	jmp    80037a <vprintfmt+0x57>
  8003f6:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003f9:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003fc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800400:	0f 89 74 ff ff ff    	jns    80037a <vprintfmt+0x57>
				width = precision, precision = -1;
  800406:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800409:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80040c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800413:	e9 62 ff ff ff       	jmp    80037a <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800418:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800419:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80041c:	e9 59 ff ff ff       	jmp    80037a <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800421:	8b 45 14             	mov    0x14(%ebp),%eax
  800424:	8d 50 04             	lea    0x4(%eax),%edx
  800427:	89 55 14             	mov    %edx,0x14(%ebp)
  80042a:	83 ec 08             	sub    $0x8,%esp
  80042d:	53                   	push   %ebx
  80042e:	ff 30                	pushl  (%eax)
  800430:	ff d6                	call   *%esi
			break;
  800432:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800435:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800438:	e9 0c ff ff ff       	jmp    800349 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80043d:	8b 45 14             	mov    0x14(%ebp),%eax
  800440:	8d 50 04             	lea    0x4(%eax),%edx
  800443:	89 55 14             	mov    %edx,0x14(%ebp)
  800446:	8b 00                	mov    (%eax),%eax
  800448:	85 c0                	test   %eax,%eax
  80044a:	79 02                	jns    80044e <vprintfmt+0x12b>
  80044c:	f7 d8                	neg    %eax
  80044e:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800450:	83 f8 0f             	cmp    $0xf,%eax
  800453:	7f 0b                	jg     800460 <vprintfmt+0x13d>
  800455:	8b 04 85 60 17 80 00 	mov    0x801760(,%eax,4),%eax
  80045c:	85 c0                	test   %eax,%eax
  80045e:	75 18                	jne    800478 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800460:	52                   	push   %edx
  800461:	68 d8 14 80 00       	push   $0x8014d8
  800466:	53                   	push   %ebx
  800467:	56                   	push   %esi
  800468:	e8 99 fe ff ff       	call   800306 <printfmt>
  80046d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800470:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800473:	e9 d1 fe ff ff       	jmp    800349 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800478:	50                   	push   %eax
  800479:	68 e1 14 80 00       	push   $0x8014e1
  80047e:	53                   	push   %ebx
  80047f:	56                   	push   %esi
  800480:	e8 81 fe ff ff       	call   800306 <printfmt>
  800485:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800488:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80048b:	e9 b9 fe ff ff       	jmp    800349 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800490:	8b 45 14             	mov    0x14(%ebp),%eax
  800493:	8d 50 04             	lea    0x4(%eax),%edx
  800496:	89 55 14             	mov    %edx,0x14(%ebp)
  800499:	8b 38                	mov    (%eax),%edi
  80049b:	85 ff                	test   %edi,%edi
  80049d:	75 05                	jne    8004a4 <vprintfmt+0x181>
				p = "(null)";
  80049f:	bf d1 14 80 00       	mov    $0x8014d1,%edi
			if (width > 0 && padc != '-')
  8004a4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004a8:	0f 8e 90 00 00 00    	jle    80053e <vprintfmt+0x21b>
  8004ae:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004b2:	0f 84 8e 00 00 00    	je     800546 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004b8:	83 ec 08             	sub    $0x8,%esp
  8004bb:	ff 75 d0             	pushl  -0x30(%ebp)
  8004be:	57                   	push   %edi
  8004bf:	e8 70 02 00 00       	call   800734 <strnlen>
  8004c4:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004c7:	29 c1                	sub    %eax,%ecx
  8004c9:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004cc:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004cf:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004d3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004d6:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004d9:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004db:	eb 0d                	jmp    8004ea <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004dd:	83 ec 08             	sub    $0x8,%esp
  8004e0:	53                   	push   %ebx
  8004e1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004e4:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004e6:	4f                   	dec    %edi
  8004e7:	83 c4 10             	add    $0x10,%esp
  8004ea:	85 ff                	test   %edi,%edi
  8004ec:	7f ef                	jg     8004dd <vprintfmt+0x1ba>
  8004ee:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8004f1:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004f4:	89 c8                	mov    %ecx,%eax
  8004f6:	85 c9                	test   %ecx,%ecx
  8004f8:	79 05                	jns    8004ff <vprintfmt+0x1dc>
  8004fa:	b8 00 00 00 00       	mov    $0x0,%eax
  8004ff:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800502:	29 c1                	sub    %eax,%ecx
  800504:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800507:	89 75 08             	mov    %esi,0x8(%ebp)
  80050a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80050d:	eb 3d                	jmp    80054c <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80050f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800513:	74 19                	je     80052e <vprintfmt+0x20b>
  800515:	0f be c0             	movsbl %al,%eax
  800518:	83 e8 20             	sub    $0x20,%eax
  80051b:	83 f8 5e             	cmp    $0x5e,%eax
  80051e:	76 0e                	jbe    80052e <vprintfmt+0x20b>
					putch('?', putdat);
  800520:	83 ec 08             	sub    $0x8,%esp
  800523:	53                   	push   %ebx
  800524:	6a 3f                	push   $0x3f
  800526:	ff 55 08             	call   *0x8(%ebp)
  800529:	83 c4 10             	add    $0x10,%esp
  80052c:	eb 0b                	jmp    800539 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80052e:	83 ec 08             	sub    $0x8,%esp
  800531:	53                   	push   %ebx
  800532:	52                   	push   %edx
  800533:	ff 55 08             	call   *0x8(%ebp)
  800536:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800539:	ff 4d e4             	decl   -0x1c(%ebp)
  80053c:	eb 0e                	jmp    80054c <vprintfmt+0x229>
  80053e:	89 75 08             	mov    %esi,0x8(%ebp)
  800541:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800544:	eb 06                	jmp    80054c <vprintfmt+0x229>
  800546:	89 75 08             	mov    %esi,0x8(%ebp)
  800549:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80054c:	47                   	inc    %edi
  80054d:	8a 47 ff             	mov    -0x1(%edi),%al
  800550:	0f be d0             	movsbl %al,%edx
  800553:	85 d2                	test   %edx,%edx
  800555:	74 1d                	je     800574 <vprintfmt+0x251>
  800557:	85 f6                	test   %esi,%esi
  800559:	78 b4                	js     80050f <vprintfmt+0x1ec>
  80055b:	4e                   	dec    %esi
  80055c:	79 b1                	jns    80050f <vprintfmt+0x1ec>
  80055e:	8b 75 08             	mov    0x8(%ebp),%esi
  800561:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800564:	eb 14                	jmp    80057a <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800566:	83 ec 08             	sub    $0x8,%esp
  800569:	53                   	push   %ebx
  80056a:	6a 20                	push   $0x20
  80056c:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80056e:	4f                   	dec    %edi
  80056f:	83 c4 10             	add    $0x10,%esp
  800572:	eb 06                	jmp    80057a <vprintfmt+0x257>
  800574:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800577:	8b 75 08             	mov    0x8(%ebp),%esi
  80057a:	85 ff                	test   %edi,%edi
  80057c:	7f e8                	jg     800566 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80057e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800581:	e9 c3 fd ff ff       	jmp    800349 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800586:	83 fa 01             	cmp    $0x1,%edx
  800589:	7e 16                	jle    8005a1 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80058b:	8b 45 14             	mov    0x14(%ebp),%eax
  80058e:	8d 50 08             	lea    0x8(%eax),%edx
  800591:	89 55 14             	mov    %edx,0x14(%ebp)
  800594:	8b 50 04             	mov    0x4(%eax),%edx
  800597:	8b 00                	mov    (%eax),%eax
  800599:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80059c:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80059f:	eb 32                	jmp    8005d3 <vprintfmt+0x2b0>
	else if (lflag)
  8005a1:	85 d2                	test   %edx,%edx
  8005a3:	74 18                	je     8005bd <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005a5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a8:	8d 50 04             	lea    0x4(%eax),%edx
  8005ab:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ae:	8b 00                	mov    (%eax),%eax
  8005b0:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005b3:	89 c1                	mov    %eax,%ecx
  8005b5:	c1 f9 1f             	sar    $0x1f,%ecx
  8005b8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005bb:	eb 16                	jmp    8005d3 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005bd:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c0:	8d 50 04             	lea    0x4(%eax),%edx
  8005c3:	89 55 14             	mov    %edx,0x14(%ebp)
  8005c6:	8b 00                	mov    (%eax),%eax
  8005c8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005cb:	89 c1                	mov    %eax,%ecx
  8005cd:	c1 f9 1f             	sar    $0x1f,%ecx
  8005d0:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005d3:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005d6:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005d9:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005dd:	79 76                	jns    800655 <vprintfmt+0x332>
				putch('-', putdat);
  8005df:	83 ec 08             	sub    $0x8,%esp
  8005e2:	53                   	push   %ebx
  8005e3:	6a 2d                	push   $0x2d
  8005e5:	ff d6                	call   *%esi
				num = -(long long) num;
  8005e7:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005ea:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005ed:	f7 d8                	neg    %eax
  8005ef:	83 d2 00             	adc    $0x0,%edx
  8005f2:	f7 da                	neg    %edx
  8005f4:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8005f7:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005fc:	eb 5c                	jmp    80065a <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005fe:	8d 45 14             	lea    0x14(%ebp),%eax
  800601:	e8 aa fc ff ff       	call   8002b0 <getuint>
			base = 10;
  800606:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80060b:	eb 4d                	jmp    80065a <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80060d:	8d 45 14             	lea    0x14(%ebp),%eax
  800610:	e8 9b fc ff ff       	call   8002b0 <getuint>
			base = 8;
  800615:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80061a:	eb 3e                	jmp    80065a <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80061c:	83 ec 08             	sub    $0x8,%esp
  80061f:	53                   	push   %ebx
  800620:	6a 30                	push   $0x30
  800622:	ff d6                	call   *%esi
			putch('x', putdat);
  800624:	83 c4 08             	add    $0x8,%esp
  800627:	53                   	push   %ebx
  800628:	6a 78                	push   $0x78
  80062a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80062c:	8b 45 14             	mov    0x14(%ebp),%eax
  80062f:	8d 50 04             	lea    0x4(%eax),%edx
  800632:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800635:	8b 00                	mov    (%eax),%eax
  800637:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80063c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80063f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800644:	eb 14                	jmp    80065a <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800646:	8d 45 14             	lea    0x14(%ebp),%eax
  800649:	e8 62 fc ff ff       	call   8002b0 <getuint>
			base = 16;
  80064e:	b9 10 00 00 00       	mov    $0x10,%ecx
  800653:	eb 05                	jmp    80065a <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800655:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80065a:	83 ec 0c             	sub    $0xc,%esp
  80065d:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800661:	57                   	push   %edi
  800662:	ff 75 e4             	pushl  -0x1c(%ebp)
  800665:	51                   	push   %ecx
  800666:	52                   	push   %edx
  800667:	50                   	push   %eax
  800668:	89 da                	mov    %ebx,%edx
  80066a:	89 f0                	mov    %esi,%eax
  80066c:	e8 92 fb ff ff       	call   800203 <printnum>
			break;
  800671:	83 c4 20             	add    $0x20,%esp
  800674:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800677:	e9 cd fc ff ff       	jmp    800349 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80067c:	83 ec 08             	sub    $0x8,%esp
  80067f:	53                   	push   %ebx
  800680:	51                   	push   %ecx
  800681:	ff d6                	call   *%esi
			break;
  800683:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800686:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800689:	e9 bb fc ff ff       	jmp    800349 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80068e:	83 ec 08             	sub    $0x8,%esp
  800691:	53                   	push   %ebx
  800692:	6a 25                	push   $0x25
  800694:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800696:	83 c4 10             	add    $0x10,%esp
  800699:	eb 01                	jmp    80069c <vprintfmt+0x379>
  80069b:	4f                   	dec    %edi
  80069c:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006a0:	75 f9                	jne    80069b <vprintfmt+0x378>
  8006a2:	e9 a2 fc ff ff       	jmp    800349 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006a7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006aa:	5b                   	pop    %ebx
  8006ab:	5e                   	pop    %esi
  8006ac:	5f                   	pop    %edi
  8006ad:	5d                   	pop    %ebp
  8006ae:	c3                   	ret    

008006af <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006af:	55                   	push   %ebp
  8006b0:	89 e5                	mov    %esp,%ebp
  8006b2:	83 ec 18             	sub    $0x18,%esp
  8006b5:	8b 45 08             	mov    0x8(%ebp),%eax
  8006b8:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006bb:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006be:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006c2:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006c5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006cc:	85 c0                	test   %eax,%eax
  8006ce:	74 26                	je     8006f6 <vsnprintf+0x47>
  8006d0:	85 d2                	test   %edx,%edx
  8006d2:	7e 29                	jle    8006fd <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006d4:	ff 75 14             	pushl  0x14(%ebp)
  8006d7:	ff 75 10             	pushl  0x10(%ebp)
  8006da:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006dd:	50                   	push   %eax
  8006de:	68 ea 02 80 00       	push   $0x8002ea
  8006e3:	e8 3b fc ff ff       	call   800323 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006e8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006eb:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006ee:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8006f1:	83 c4 10             	add    $0x10,%esp
  8006f4:	eb 0c                	jmp    800702 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8006f6:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006fb:	eb 05                	jmp    800702 <vsnprintf+0x53>
  8006fd:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800702:	c9                   	leave  
  800703:	c3                   	ret    

00800704 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800704:	55                   	push   %ebp
  800705:	89 e5                	mov    %esp,%ebp
  800707:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80070a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80070d:	50                   	push   %eax
  80070e:	ff 75 10             	pushl  0x10(%ebp)
  800711:	ff 75 0c             	pushl  0xc(%ebp)
  800714:	ff 75 08             	pushl  0x8(%ebp)
  800717:	e8 93 ff ff ff       	call   8006af <vsnprintf>
	va_end(ap);

	return rc;
}
  80071c:	c9                   	leave  
  80071d:	c3                   	ret    

0080071e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80071e:	55                   	push   %ebp
  80071f:	89 e5                	mov    %esp,%ebp
  800721:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800724:	b8 00 00 00 00       	mov    $0x0,%eax
  800729:	eb 01                	jmp    80072c <strlen+0xe>
		n++;
  80072b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80072c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800730:	75 f9                	jne    80072b <strlen+0xd>
		n++;
	return n;
}
  800732:	5d                   	pop    %ebp
  800733:	c3                   	ret    

00800734 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800734:	55                   	push   %ebp
  800735:	89 e5                	mov    %esp,%ebp
  800737:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80073a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80073d:	ba 00 00 00 00       	mov    $0x0,%edx
  800742:	eb 01                	jmp    800745 <strnlen+0x11>
		n++;
  800744:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800745:	39 c2                	cmp    %eax,%edx
  800747:	74 08                	je     800751 <strnlen+0x1d>
  800749:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80074d:	75 f5                	jne    800744 <strnlen+0x10>
  80074f:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800751:	5d                   	pop    %ebp
  800752:	c3                   	ret    

00800753 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800753:	55                   	push   %ebp
  800754:	89 e5                	mov    %esp,%ebp
  800756:	53                   	push   %ebx
  800757:	8b 45 08             	mov    0x8(%ebp),%eax
  80075a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80075d:	89 c2                	mov    %eax,%edx
  80075f:	42                   	inc    %edx
  800760:	41                   	inc    %ecx
  800761:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800764:	88 5a ff             	mov    %bl,-0x1(%edx)
  800767:	84 db                	test   %bl,%bl
  800769:	75 f4                	jne    80075f <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80076b:	5b                   	pop    %ebx
  80076c:	5d                   	pop    %ebp
  80076d:	c3                   	ret    

0080076e <strcat>:

char *
strcat(char *dst, const char *src)
{
  80076e:	55                   	push   %ebp
  80076f:	89 e5                	mov    %esp,%ebp
  800771:	53                   	push   %ebx
  800772:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800775:	53                   	push   %ebx
  800776:	e8 a3 ff ff ff       	call   80071e <strlen>
  80077b:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80077e:	ff 75 0c             	pushl  0xc(%ebp)
  800781:	01 d8                	add    %ebx,%eax
  800783:	50                   	push   %eax
  800784:	e8 ca ff ff ff       	call   800753 <strcpy>
	return dst;
}
  800789:	89 d8                	mov    %ebx,%eax
  80078b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80078e:	c9                   	leave  
  80078f:	c3                   	ret    

00800790 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800790:	55                   	push   %ebp
  800791:	89 e5                	mov    %esp,%ebp
  800793:	56                   	push   %esi
  800794:	53                   	push   %ebx
  800795:	8b 75 08             	mov    0x8(%ebp),%esi
  800798:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80079b:	89 f3                	mov    %esi,%ebx
  80079d:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007a0:	89 f2                	mov    %esi,%edx
  8007a2:	eb 0c                	jmp    8007b0 <strncpy+0x20>
		*dst++ = *src;
  8007a4:	42                   	inc    %edx
  8007a5:	8a 01                	mov    (%ecx),%al
  8007a7:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007aa:	80 39 01             	cmpb   $0x1,(%ecx)
  8007ad:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007b0:	39 da                	cmp    %ebx,%edx
  8007b2:	75 f0                	jne    8007a4 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007b4:	89 f0                	mov    %esi,%eax
  8007b6:	5b                   	pop    %ebx
  8007b7:	5e                   	pop    %esi
  8007b8:	5d                   	pop    %ebp
  8007b9:	c3                   	ret    

008007ba <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007ba:	55                   	push   %ebp
  8007bb:	89 e5                	mov    %esp,%ebp
  8007bd:	56                   	push   %esi
  8007be:	53                   	push   %ebx
  8007bf:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007c5:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007c8:	85 c0                	test   %eax,%eax
  8007ca:	74 1e                	je     8007ea <strlcpy+0x30>
  8007cc:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007d0:	89 f2                	mov    %esi,%edx
  8007d2:	eb 05                	jmp    8007d9 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007d4:	42                   	inc    %edx
  8007d5:	41                   	inc    %ecx
  8007d6:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007d9:	39 c2                	cmp    %eax,%edx
  8007db:	74 08                	je     8007e5 <strlcpy+0x2b>
  8007dd:	8a 19                	mov    (%ecx),%bl
  8007df:	84 db                	test   %bl,%bl
  8007e1:	75 f1                	jne    8007d4 <strlcpy+0x1a>
  8007e3:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007e5:	c6 00 00             	movb   $0x0,(%eax)
  8007e8:	eb 02                	jmp    8007ec <strlcpy+0x32>
  8007ea:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007ec:	29 f0                	sub    %esi,%eax
}
  8007ee:	5b                   	pop    %ebx
  8007ef:	5e                   	pop    %esi
  8007f0:	5d                   	pop    %ebp
  8007f1:	c3                   	ret    

008007f2 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8007f2:	55                   	push   %ebp
  8007f3:	89 e5                	mov    %esp,%ebp
  8007f5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007f8:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007fb:	eb 02                	jmp    8007ff <strcmp+0xd>
		p++, q++;
  8007fd:	41                   	inc    %ecx
  8007fe:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007ff:	8a 01                	mov    (%ecx),%al
  800801:	84 c0                	test   %al,%al
  800803:	74 04                	je     800809 <strcmp+0x17>
  800805:	3a 02                	cmp    (%edx),%al
  800807:	74 f4                	je     8007fd <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800809:	0f b6 c0             	movzbl %al,%eax
  80080c:	0f b6 12             	movzbl (%edx),%edx
  80080f:	29 d0                	sub    %edx,%eax
}
  800811:	5d                   	pop    %ebp
  800812:	c3                   	ret    

00800813 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800813:	55                   	push   %ebp
  800814:	89 e5                	mov    %esp,%ebp
  800816:	53                   	push   %ebx
  800817:	8b 45 08             	mov    0x8(%ebp),%eax
  80081a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80081d:	89 c3                	mov    %eax,%ebx
  80081f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800822:	eb 02                	jmp    800826 <strncmp+0x13>
		n--, p++, q++;
  800824:	40                   	inc    %eax
  800825:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800826:	39 d8                	cmp    %ebx,%eax
  800828:	74 14                	je     80083e <strncmp+0x2b>
  80082a:	8a 08                	mov    (%eax),%cl
  80082c:	84 c9                	test   %cl,%cl
  80082e:	74 04                	je     800834 <strncmp+0x21>
  800830:	3a 0a                	cmp    (%edx),%cl
  800832:	74 f0                	je     800824 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800834:	0f b6 00             	movzbl (%eax),%eax
  800837:	0f b6 12             	movzbl (%edx),%edx
  80083a:	29 d0                	sub    %edx,%eax
  80083c:	eb 05                	jmp    800843 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80083e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800843:	5b                   	pop    %ebx
  800844:	5d                   	pop    %ebp
  800845:	c3                   	ret    

00800846 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800846:	55                   	push   %ebp
  800847:	89 e5                	mov    %esp,%ebp
  800849:	8b 45 08             	mov    0x8(%ebp),%eax
  80084c:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80084f:	eb 05                	jmp    800856 <strchr+0x10>
		if (*s == c)
  800851:	38 ca                	cmp    %cl,%dl
  800853:	74 0c                	je     800861 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800855:	40                   	inc    %eax
  800856:	8a 10                	mov    (%eax),%dl
  800858:	84 d2                	test   %dl,%dl
  80085a:	75 f5                	jne    800851 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80085c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800861:	5d                   	pop    %ebp
  800862:	c3                   	ret    

00800863 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800863:	55                   	push   %ebp
  800864:	89 e5                	mov    %esp,%ebp
  800866:	8b 45 08             	mov    0x8(%ebp),%eax
  800869:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80086c:	eb 05                	jmp    800873 <strfind+0x10>
		if (*s == c)
  80086e:	38 ca                	cmp    %cl,%dl
  800870:	74 07                	je     800879 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800872:	40                   	inc    %eax
  800873:	8a 10                	mov    (%eax),%dl
  800875:	84 d2                	test   %dl,%dl
  800877:	75 f5                	jne    80086e <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800879:	5d                   	pop    %ebp
  80087a:	c3                   	ret    

0080087b <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80087b:	55                   	push   %ebp
  80087c:	89 e5                	mov    %esp,%ebp
  80087e:	57                   	push   %edi
  80087f:	56                   	push   %esi
  800880:	53                   	push   %ebx
  800881:	8b 7d 08             	mov    0x8(%ebp),%edi
  800884:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800887:	85 c9                	test   %ecx,%ecx
  800889:	74 36                	je     8008c1 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80088b:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800891:	75 28                	jne    8008bb <memset+0x40>
  800893:	f6 c1 03             	test   $0x3,%cl
  800896:	75 23                	jne    8008bb <memset+0x40>
		c &= 0xFF;
  800898:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80089c:	89 d3                	mov    %edx,%ebx
  80089e:	c1 e3 08             	shl    $0x8,%ebx
  8008a1:	89 d6                	mov    %edx,%esi
  8008a3:	c1 e6 18             	shl    $0x18,%esi
  8008a6:	89 d0                	mov    %edx,%eax
  8008a8:	c1 e0 10             	shl    $0x10,%eax
  8008ab:	09 f0                	or     %esi,%eax
  8008ad:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008af:	89 d8                	mov    %ebx,%eax
  8008b1:	09 d0                	or     %edx,%eax
  8008b3:	c1 e9 02             	shr    $0x2,%ecx
  8008b6:	fc                   	cld    
  8008b7:	f3 ab                	rep stos %eax,%es:(%edi)
  8008b9:	eb 06                	jmp    8008c1 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008bb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008be:	fc                   	cld    
  8008bf:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008c1:	89 f8                	mov    %edi,%eax
  8008c3:	5b                   	pop    %ebx
  8008c4:	5e                   	pop    %esi
  8008c5:	5f                   	pop    %edi
  8008c6:	5d                   	pop    %ebp
  8008c7:	c3                   	ret    

008008c8 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008c8:	55                   	push   %ebp
  8008c9:	89 e5                	mov    %esp,%ebp
  8008cb:	57                   	push   %edi
  8008cc:	56                   	push   %esi
  8008cd:	8b 45 08             	mov    0x8(%ebp),%eax
  8008d0:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008d3:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008d6:	39 c6                	cmp    %eax,%esi
  8008d8:	73 33                	jae    80090d <memmove+0x45>
  8008da:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008dd:	39 d0                	cmp    %edx,%eax
  8008df:	73 2c                	jae    80090d <memmove+0x45>
		s += n;
		d += n;
  8008e1:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008e4:	89 d6                	mov    %edx,%esi
  8008e6:	09 fe                	or     %edi,%esi
  8008e8:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008ee:	75 13                	jne    800903 <memmove+0x3b>
  8008f0:	f6 c1 03             	test   $0x3,%cl
  8008f3:	75 0e                	jne    800903 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8008f5:	83 ef 04             	sub    $0x4,%edi
  8008f8:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008fb:	c1 e9 02             	shr    $0x2,%ecx
  8008fe:	fd                   	std    
  8008ff:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800901:	eb 07                	jmp    80090a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800903:	4f                   	dec    %edi
  800904:	8d 72 ff             	lea    -0x1(%edx),%esi
  800907:	fd                   	std    
  800908:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80090a:	fc                   	cld    
  80090b:	eb 1d                	jmp    80092a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80090d:	89 f2                	mov    %esi,%edx
  80090f:	09 c2                	or     %eax,%edx
  800911:	f6 c2 03             	test   $0x3,%dl
  800914:	75 0f                	jne    800925 <memmove+0x5d>
  800916:	f6 c1 03             	test   $0x3,%cl
  800919:	75 0a                	jne    800925 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80091b:	c1 e9 02             	shr    $0x2,%ecx
  80091e:	89 c7                	mov    %eax,%edi
  800920:	fc                   	cld    
  800921:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800923:	eb 05                	jmp    80092a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800925:	89 c7                	mov    %eax,%edi
  800927:	fc                   	cld    
  800928:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80092a:	5e                   	pop    %esi
  80092b:	5f                   	pop    %edi
  80092c:	5d                   	pop    %ebp
  80092d:	c3                   	ret    

0080092e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80092e:	55                   	push   %ebp
  80092f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800931:	ff 75 10             	pushl  0x10(%ebp)
  800934:	ff 75 0c             	pushl  0xc(%ebp)
  800937:	ff 75 08             	pushl  0x8(%ebp)
  80093a:	e8 89 ff ff ff       	call   8008c8 <memmove>
}
  80093f:	c9                   	leave  
  800940:	c3                   	ret    

00800941 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800941:	55                   	push   %ebp
  800942:	89 e5                	mov    %esp,%ebp
  800944:	56                   	push   %esi
  800945:	53                   	push   %ebx
  800946:	8b 45 08             	mov    0x8(%ebp),%eax
  800949:	8b 55 0c             	mov    0xc(%ebp),%edx
  80094c:	89 c6                	mov    %eax,%esi
  80094e:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800951:	eb 14                	jmp    800967 <memcmp+0x26>
		if (*s1 != *s2)
  800953:	8a 08                	mov    (%eax),%cl
  800955:	8a 1a                	mov    (%edx),%bl
  800957:	38 d9                	cmp    %bl,%cl
  800959:	74 0a                	je     800965 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80095b:	0f b6 c1             	movzbl %cl,%eax
  80095e:	0f b6 db             	movzbl %bl,%ebx
  800961:	29 d8                	sub    %ebx,%eax
  800963:	eb 0b                	jmp    800970 <memcmp+0x2f>
		s1++, s2++;
  800965:	40                   	inc    %eax
  800966:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800967:	39 f0                	cmp    %esi,%eax
  800969:	75 e8                	jne    800953 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80096b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800970:	5b                   	pop    %ebx
  800971:	5e                   	pop    %esi
  800972:	5d                   	pop    %ebp
  800973:	c3                   	ret    

00800974 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800974:	55                   	push   %ebp
  800975:	89 e5                	mov    %esp,%ebp
  800977:	53                   	push   %ebx
  800978:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80097b:	89 c1                	mov    %eax,%ecx
  80097d:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800980:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800984:	eb 08                	jmp    80098e <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800986:	0f b6 10             	movzbl (%eax),%edx
  800989:	39 da                	cmp    %ebx,%edx
  80098b:	74 05                	je     800992 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80098d:	40                   	inc    %eax
  80098e:	39 c8                	cmp    %ecx,%eax
  800990:	72 f4                	jb     800986 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800992:	5b                   	pop    %ebx
  800993:	5d                   	pop    %ebp
  800994:	c3                   	ret    

00800995 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800995:	55                   	push   %ebp
  800996:	89 e5                	mov    %esp,%ebp
  800998:	57                   	push   %edi
  800999:	56                   	push   %esi
  80099a:	53                   	push   %ebx
  80099b:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80099e:	eb 01                	jmp    8009a1 <strtol+0xc>
		s++;
  8009a0:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009a1:	8a 01                	mov    (%ecx),%al
  8009a3:	3c 20                	cmp    $0x20,%al
  8009a5:	74 f9                	je     8009a0 <strtol+0xb>
  8009a7:	3c 09                	cmp    $0x9,%al
  8009a9:	74 f5                	je     8009a0 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009ab:	3c 2b                	cmp    $0x2b,%al
  8009ad:	75 08                	jne    8009b7 <strtol+0x22>
		s++;
  8009af:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009b0:	bf 00 00 00 00       	mov    $0x0,%edi
  8009b5:	eb 11                	jmp    8009c8 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009b7:	3c 2d                	cmp    $0x2d,%al
  8009b9:	75 08                	jne    8009c3 <strtol+0x2e>
		s++, neg = 1;
  8009bb:	41                   	inc    %ecx
  8009bc:	bf 01 00 00 00       	mov    $0x1,%edi
  8009c1:	eb 05                	jmp    8009c8 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009c3:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009c8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009cc:	0f 84 87 00 00 00    	je     800a59 <strtol+0xc4>
  8009d2:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009d6:	75 27                	jne    8009ff <strtol+0x6a>
  8009d8:	80 39 30             	cmpb   $0x30,(%ecx)
  8009db:	75 22                	jne    8009ff <strtol+0x6a>
  8009dd:	e9 88 00 00 00       	jmp    800a6a <strtol+0xd5>
		s += 2, base = 16;
  8009e2:	83 c1 02             	add    $0x2,%ecx
  8009e5:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009ec:	eb 11                	jmp    8009ff <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009ee:	41                   	inc    %ecx
  8009ef:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8009f6:	eb 07                	jmp    8009ff <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8009f8:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009ff:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a04:	8a 11                	mov    (%ecx),%dl
  800a06:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a09:	80 fb 09             	cmp    $0x9,%bl
  800a0c:	77 08                	ja     800a16 <strtol+0x81>
			dig = *s - '0';
  800a0e:	0f be d2             	movsbl %dl,%edx
  800a11:	83 ea 30             	sub    $0x30,%edx
  800a14:	eb 22                	jmp    800a38 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a16:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a19:	89 f3                	mov    %esi,%ebx
  800a1b:	80 fb 19             	cmp    $0x19,%bl
  800a1e:	77 08                	ja     800a28 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a20:	0f be d2             	movsbl %dl,%edx
  800a23:	83 ea 57             	sub    $0x57,%edx
  800a26:	eb 10                	jmp    800a38 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a28:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a2b:	89 f3                	mov    %esi,%ebx
  800a2d:	80 fb 19             	cmp    $0x19,%bl
  800a30:	77 14                	ja     800a46 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a32:	0f be d2             	movsbl %dl,%edx
  800a35:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a38:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a3b:	7d 09                	jge    800a46 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a3d:	41                   	inc    %ecx
  800a3e:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a42:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a44:	eb be                	jmp    800a04 <strtol+0x6f>

	if (endptr)
  800a46:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a4a:	74 05                	je     800a51 <strtol+0xbc>
		*endptr = (char *) s;
  800a4c:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a4f:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a51:	85 ff                	test   %edi,%edi
  800a53:	74 21                	je     800a76 <strtol+0xe1>
  800a55:	f7 d8                	neg    %eax
  800a57:	eb 1d                	jmp    800a76 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a59:	80 39 30             	cmpb   $0x30,(%ecx)
  800a5c:	75 9a                	jne    8009f8 <strtol+0x63>
  800a5e:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a62:	0f 84 7a ff ff ff    	je     8009e2 <strtol+0x4d>
  800a68:	eb 84                	jmp    8009ee <strtol+0x59>
  800a6a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a6e:	0f 84 6e ff ff ff    	je     8009e2 <strtol+0x4d>
  800a74:	eb 89                	jmp    8009ff <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a76:	5b                   	pop    %ebx
  800a77:	5e                   	pop    %esi
  800a78:	5f                   	pop    %edi
  800a79:	5d                   	pop    %ebp
  800a7a:	c3                   	ret    

00800a7b <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a7b:	55                   	push   %ebp
  800a7c:	89 e5                	mov    %esp,%ebp
  800a7e:	57                   	push   %edi
  800a7f:	56                   	push   %esi
  800a80:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a81:	b8 00 00 00 00       	mov    $0x0,%eax
  800a86:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a89:	8b 55 08             	mov    0x8(%ebp),%edx
  800a8c:	89 c3                	mov    %eax,%ebx
  800a8e:	89 c7                	mov    %eax,%edi
  800a90:	89 c6                	mov    %eax,%esi
  800a92:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a94:	5b                   	pop    %ebx
  800a95:	5e                   	pop    %esi
  800a96:	5f                   	pop    %edi
  800a97:	5d                   	pop    %ebp
  800a98:	c3                   	ret    

00800a99 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a99:	55                   	push   %ebp
  800a9a:	89 e5                	mov    %esp,%ebp
  800a9c:	57                   	push   %edi
  800a9d:	56                   	push   %esi
  800a9e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a9f:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa4:	b8 01 00 00 00       	mov    $0x1,%eax
  800aa9:	89 d1                	mov    %edx,%ecx
  800aab:	89 d3                	mov    %edx,%ebx
  800aad:	89 d7                	mov    %edx,%edi
  800aaf:	89 d6                	mov    %edx,%esi
  800ab1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800ab3:	5b                   	pop    %ebx
  800ab4:	5e                   	pop    %esi
  800ab5:	5f                   	pop    %edi
  800ab6:	5d                   	pop    %ebp
  800ab7:	c3                   	ret    

00800ab8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800ab8:	55                   	push   %ebp
  800ab9:	89 e5                	mov    %esp,%ebp
  800abb:	57                   	push   %edi
  800abc:	56                   	push   %esi
  800abd:	53                   	push   %ebx
  800abe:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac1:	b9 00 00 00 00       	mov    $0x0,%ecx
  800ac6:	b8 03 00 00 00       	mov    $0x3,%eax
  800acb:	8b 55 08             	mov    0x8(%ebp),%edx
  800ace:	89 cb                	mov    %ecx,%ebx
  800ad0:	89 cf                	mov    %ecx,%edi
  800ad2:	89 ce                	mov    %ecx,%esi
  800ad4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ad6:	85 c0                	test   %eax,%eax
  800ad8:	7e 17                	jle    800af1 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ada:	83 ec 0c             	sub    $0xc,%esp
  800add:	50                   	push   %eax
  800ade:	6a 03                	push   $0x3
  800ae0:	68 bf 17 80 00       	push   $0x8017bf
  800ae5:	6a 23                	push   $0x23
  800ae7:	68 dc 17 80 00       	push   $0x8017dc
  800aec:	e8 2d 06 00 00       	call   80111e <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800af1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800af4:	5b                   	pop    %ebx
  800af5:	5e                   	pop    %esi
  800af6:	5f                   	pop    %edi
  800af7:	5d                   	pop    %ebp
  800af8:	c3                   	ret    

00800af9 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800af9:	55                   	push   %ebp
  800afa:	89 e5                	mov    %esp,%ebp
  800afc:	57                   	push   %edi
  800afd:	56                   	push   %esi
  800afe:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800aff:	ba 00 00 00 00       	mov    $0x0,%edx
  800b04:	b8 02 00 00 00       	mov    $0x2,%eax
  800b09:	89 d1                	mov    %edx,%ecx
  800b0b:	89 d3                	mov    %edx,%ebx
  800b0d:	89 d7                	mov    %edx,%edi
  800b0f:	89 d6                	mov    %edx,%esi
  800b11:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b13:	5b                   	pop    %ebx
  800b14:	5e                   	pop    %esi
  800b15:	5f                   	pop    %edi
  800b16:	5d                   	pop    %ebp
  800b17:	c3                   	ret    

00800b18 <sys_yield>:

void
sys_yield(void)
{
  800b18:	55                   	push   %ebp
  800b19:	89 e5                	mov    %esp,%ebp
  800b1b:	57                   	push   %edi
  800b1c:	56                   	push   %esi
  800b1d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1e:	ba 00 00 00 00       	mov    $0x0,%edx
  800b23:	b8 0b 00 00 00       	mov    $0xb,%eax
  800b28:	89 d1                	mov    %edx,%ecx
  800b2a:	89 d3                	mov    %edx,%ebx
  800b2c:	89 d7                	mov    %edx,%edi
  800b2e:	89 d6                	mov    %edx,%esi
  800b30:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800b32:	5b                   	pop    %ebx
  800b33:	5e                   	pop    %esi
  800b34:	5f                   	pop    %edi
  800b35:	5d                   	pop    %ebp
  800b36:	c3                   	ret    

00800b37 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800b37:	55                   	push   %ebp
  800b38:	89 e5                	mov    %esp,%ebp
  800b3a:	57                   	push   %edi
  800b3b:	56                   	push   %esi
  800b3c:	53                   	push   %ebx
  800b3d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b40:	be 00 00 00 00       	mov    $0x0,%esi
  800b45:	b8 04 00 00 00       	mov    $0x4,%eax
  800b4a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b4d:	8b 55 08             	mov    0x8(%ebp),%edx
  800b50:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b53:	89 f7                	mov    %esi,%edi
  800b55:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b57:	85 c0                	test   %eax,%eax
  800b59:	7e 17                	jle    800b72 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b5b:	83 ec 0c             	sub    $0xc,%esp
  800b5e:	50                   	push   %eax
  800b5f:	6a 04                	push   $0x4
  800b61:	68 bf 17 80 00       	push   $0x8017bf
  800b66:	6a 23                	push   $0x23
  800b68:	68 dc 17 80 00       	push   $0x8017dc
  800b6d:	e8 ac 05 00 00       	call   80111e <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b72:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b75:	5b                   	pop    %ebx
  800b76:	5e                   	pop    %esi
  800b77:	5f                   	pop    %edi
  800b78:	5d                   	pop    %ebp
  800b79:	c3                   	ret    

00800b7a <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b7a:	55                   	push   %ebp
  800b7b:	89 e5                	mov    %esp,%ebp
  800b7d:	57                   	push   %edi
  800b7e:	56                   	push   %esi
  800b7f:	53                   	push   %ebx
  800b80:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b83:	b8 05 00 00 00       	mov    $0x5,%eax
  800b88:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b8b:	8b 55 08             	mov    0x8(%ebp),%edx
  800b8e:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b91:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b94:	8b 75 18             	mov    0x18(%ebp),%esi
  800b97:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b99:	85 c0                	test   %eax,%eax
  800b9b:	7e 17                	jle    800bb4 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b9d:	83 ec 0c             	sub    $0xc,%esp
  800ba0:	50                   	push   %eax
  800ba1:	6a 05                	push   $0x5
  800ba3:	68 bf 17 80 00       	push   $0x8017bf
  800ba8:	6a 23                	push   $0x23
  800baa:	68 dc 17 80 00       	push   $0x8017dc
  800baf:	e8 6a 05 00 00       	call   80111e <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800bb4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bb7:	5b                   	pop    %ebx
  800bb8:	5e                   	pop    %esi
  800bb9:	5f                   	pop    %edi
  800bba:	5d                   	pop    %ebp
  800bbb:	c3                   	ret    

00800bbc <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800bbc:	55                   	push   %ebp
  800bbd:	89 e5                	mov    %esp,%ebp
  800bbf:	57                   	push   %edi
  800bc0:	56                   	push   %esi
  800bc1:	53                   	push   %ebx
  800bc2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bc5:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bca:	b8 06 00 00 00       	mov    $0x6,%eax
  800bcf:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bd2:	8b 55 08             	mov    0x8(%ebp),%edx
  800bd5:	89 df                	mov    %ebx,%edi
  800bd7:	89 de                	mov    %ebx,%esi
  800bd9:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bdb:	85 c0                	test   %eax,%eax
  800bdd:	7e 17                	jle    800bf6 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bdf:	83 ec 0c             	sub    $0xc,%esp
  800be2:	50                   	push   %eax
  800be3:	6a 06                	push   $0x6
  800be5:	68 bf 17 80 00       	push   $0x8017bf
  800bea:	6a 23                	push   $0x23
  800bec:	68 dc 17 80 00       	push   $0x8017dc
  800bf1:	e8 28 05 00 00       	call   80111e <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800bf6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf9:	5b                   	pop    %ebx
  800bfa:	5e                   	pop    %esi
  800bfb:	5f                   	pop    %edi
  800bfc:	5d                   	pop    %ebp
  800bfd:	c3                   	ret    

00800bfe <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800bfe:	55                   	push   %ebp
  800bff:	89 e5                	mov    %esp,%ebp
  800c01:	57                   	push   %edi
  800c02:	56                   	push   %esi
  800c03:	53                   	push   %ebx
  800c04:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c07:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c0c:	b8 08 00 00 00       	mov    $0x8,%eax
  800c11:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c14:	8b 55 08             	mov    0x8(%ebp),%edx
  800c17:	89 df                	mov    %ebx,%edi
  800c19:	89 de                	mov    %ebx,%esi
  800c1b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c1d:	85 c0                	test   %eax,%eax
  800c1f:	7e 17                	jle    800c38 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c21:	83 ec 0c             	sub    $0xc,%esp
  800c24:	50                   	push   %eax
  800c25:	6a 08                	push   $0x8
  800c27:	68 bf 17 80 00       	push   $0x8017bf
  800c2c:	6a 23                	push   $0x23
  800c2e:	68 dc 17 80 00       	push   $0x8017dc
  800c33:	e8 e6 04 00 00       	call   80111e <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800c38:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c3b:	5b                   	pop    %ebx
  800c3c:	5e                   	pop    %esi
  800c3d:	5f                   	pop    %edi
  800c3e:	5d                   	pop    %ebp
  800c3f:	c3                   	ret    

00800c40 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c40:	55                   	push   %ebp
  800c41:	89 e5                	mov    %esp,%ebp
  800c43:	57                   	push   %edi
  800c44:	56                   	push   %esi
  800c45:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c46:	ba 00 00 00 00       	mov    $0x0,%edx
  800c4b:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c50:	89 d1                	mov    %edx,%ecx
  800c52:	89 d3                	mov    %edx,%ebx
  800c54:	89 d7                	mov    %edx,%edi
  800c56:	89 d6                	mov    %edx,%esi
  800c58:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c5a:	5b                   	pop    %ebx
  800c5b:	5e                   	pop    %esi
  800c5c:	5f                   	pop    %edi
  800c5d:	5d                   	pop    %ebp
  800c5e:	c3                   	ret    

00800c5f <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c5f:	55                   	push   %ebp
  800c60:	89 e5                	mov    %esp,%ebp
  800c62:	57                   	push   %edi
  800c63:	56                   	push   %esi
  800c64:	53                   	push   %ebx
  800c65:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c68:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c6d:	b8 09 00 00 00       	mov    $0x9,%eax
  800c72:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c75:	8b 55 08             	mov    0x8(%ebp),%edx
  800c78:	89 df                	mov    %ebx,%edi
  800c7a:	89 de                	mov    %ebx,%esi
  800c7c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c7e:	85 c0                	test   %eax,%eax
  800c80:	7e 17                	jle    800c99 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c82:	83 ec 0c             	sub    $0xc,%esp
  800c85:	50                   	push   %eax
  800c86:	6a 09                	push   $0x9
  800c88:	68 bf 17 80 00       	push   $0x8017bf
  800c8d:	6a 23                	push   $0x23
  800c8f:	68 dc 17 80 00       	push   $0x8017dc
  800c94:	e8 85 04 00 00       	call   80111e <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c99:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c9c:	5b                   	pop    %ebx
  800c9d:	5e                   	pop    %esi
  800c9e:	5f                   	pop    %edi
  800c9f:	5d                   	pop    %ebp
  800ca0:	c3                   	ret    

00800ca1 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800ca1:	55                   	push   %ebp
  800ca2:	89 e5                	mov    %esp,%ebp
  800ca4:	57                   	push   %edi
  800ca5:	56                   	push   %esi
  800ca6:	53                   	push   %ebx
  800ca7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800caa:	bb 00 00 00 00       	mov    $0x0,%ebx
  800caf:	b8 0a 00 00 00       	mov    $0xa,%eax
  800cb4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cb7:	8b 55 08             	mov    0x8(%ebp),%edx
  800cba:	89 df                	mov    %ebx,%edi
  800cbc:	89 de                	mov    %ebx,%esi
  800cbe:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cc0:	85 c0                	test   %eax,%eax
  800cc2:	7e 17                	jle    800cdb <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cc4:	83 ec 0c             	sub    $0xc,%esp
  800cc7:	50                   	push   %eax
  800cc8:	6a 0a                	push   $0xa
  800cca:	68 bf 17 80 00       	push   $0x8017bf
  800ccf:	6a 23                	push   $0x23
  800cd1:	68 dc 17 80 00       	push   $0x8017dc
  800cd6:	e8 43 04 00 00       	call   80111e <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800cdb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cde:	5b                   	pop    %ebx
  800cdf:	5e                   	pop    %esi
  800ce0:	5f                   	pop    %edi
  800ce1:	5d                   	pop    %ebp
  800ce2:	c3                   	ret    

00800ce3 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800ce3:	55                   	push   %ebp
  800ce4:	89 e5                	mov    %esp,%ebp
  800ce6:	57                   	push   %edi
  800ce7:	56                   	push   %esi
  800ce8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ce9:	be 00 00 00 00       	mov    $0x0,%esi
  800cee:	b8 0d 00 00 00       	mov    $0xd,%eax
  800cf3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cf6:	8b 55 08             	mov    0x8(%ebp),%edx
  800cf9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cfc:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cff:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d01:	5b                   	pop    %ebx
  800d02:	5e                   	pop    %esi
  800d03:	5f                   	pop    %edi
  800d04:	5d                   	pop    %ebp
  800d05:	c3                   	ret    

00800d06 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d06:	55                   	push   %ebp
  800d07:	89 e5                	mov    %esp,%ebp
  800d09:	57                   	push   %edi
  800d0a:	56                   	push   %esi
  800d0b:	53                   	push   %ebx
  800d0c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d0f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d14:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d19:	8b 55 08             	mov    0x8(%ebp),%edx
  800d1c:	89 cb                	mov    %ecx,%ebx
  800d1e:	89 cf                	mov    %ecx,%edi
  800d20:	89 ce                	mov    %ecx,%esi
  800d22:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d24:	85 c0                	test   %eax,%eax
  800d26:	7e 17                	jle    800d3f <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d28:	83 ec 0c             	sub    $0xc,%esp
  800d2b:	50                   	push   %eax
  800d2c:	6a 0e                	push   $0xe
  800d2e:	68 bf 17 80 00       	push   $0x8017bf
  800d33:	6a 23                	push   $0x23
  800d35:	68 dc 17 80 00       	push   $0x8017dc
  800d3a:	e8 df 03 00 00       	call   80111e <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d3f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d42:	5b                   	pop    %ebx
  800d43:	5e                   	pop    %esi
  800d44:	5f                   	pop    %edi
  800d45:	5d                   	pop    %ebp
  800d46:	c3                   	ret    

00800d47 <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800d47:	55                   	push   %ebp
  800d48:	89 e5                	mov    %esp,%ebp
  800d4a:	53                   	push   %ebx
  800d4b:	83 ec 04             	sub    $0x4,%esp
  800d4e:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800d51:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800d53:	89 da                	mov    %ebx,%edx
  800d55:	c1 ea 0c             	shr    $0xc,%edx
  800d58:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800d5f:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800d63:	74 05                	je     800d6a <pgfault+0x23>
  800d65:	f6 c6 08             	test   $0x8,%dh
  800d68:	75 14                	jne    800d7e <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800d6a:	83 ec 04             	sub    $0x4,%esp
  800d6d:	68 ec 17 80 00       	push   $0x8017ec
  800d72:	6a 26                	push   $0x26
  800d74:	68 50 18 80 00       	push   $0x801850
  800d79:	e8 a0 03 00 00       	call   80111e <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800d7e:	83 ec 04             	sub    $0x4,%esp
  800d81:	6a 07                	push   $0x7
  800d83:	68 00 f0 7f 00       	push   $0x7ff000
  800d88:	6a 00                	push   $0x0
  800d8a:	e8 a8 fd ff ff       	call   800b37 <sys_page_alloc>
  800d8f:	83 c4 10             	add    $0x10,%esp
  800d92:	85 c0                	test   %eax,%eax
  800d94:	74 14                	je     800daa <pgfault+0x63>
        panic("pgfault: no phys mem");
  800d96:	83 ec 04             	sub    $0x4,%esp
  800d99:	68 5b 18 80 00       	push   $0x80185b
  800d9e:	6a 32                	push   $0x32
  800da0:	68 50 18 80 00       	push   $0x801850
  800da5:	e8 74 03 00 00       	call   80111e <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800daa:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800db0:	83 ec 04             	sub    $0x4,%esp
  800db3:	68 00 10 00 00       	push   $0x1000
  800db8:	53                   	push   %ebx
  800db9:	68 00 f0 7f 00       	push   $0x7ff000
  800dbe:	e8 05 fb ff ff       	call   8008c8 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800dc3:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800dca:	53                   	push   %ebx
  800dcb:	6a 00                	push   $0x0
  800dcd:	68 00 f0 7f 00       	push   $0x7ff000
  800dd2:	6a 00                	push   $0x0
  800dd4:	e8 a1 fd ff ff       	call   800b7a <sys_page_map>
  800dd9:	83 c4 20             	add    $0x20,%esp
  800ddc:	85 c0                	test   %eax,%eax
  800dde:	74 14                	je     800df4 <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800de0:	83 ec 04             	sub    $0x4,%esp
  800de3:	68 70 18 80 00       	push   $0x801870
  800de8:	6a 3f                	push   $0x3f
  800dea:	68 50 18 80 00       	push   $0x801850
  800def:	e8 2a 03 00 00       	call   80111e <_panic>


	//panic("pgfault not implemented");
}
  800df4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800df7:	c9                   	leave  
  800df8:	c3                   	ret    

00800df9 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800df9:	55                   	push   %ebp
  800dfa:	89 e5                	mov    %esp,%ebp
  800dfc:	57                   	push   %edi
  800dfd:	56                   	push   %esi
  800dfe:	53                   	push   %ebx
  800dff:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800e02:	68 47 0d 80 00       	push   $0x800d47
  800e07:	e8 58 03 00 00       	call   801164 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800e0c:	b8 07 00 00 00       	mov    $0x7,%eax
  800e11:	cd 30                	int    $0x30
  800e13:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800e16:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800e19:	83 c4 10             	add    $0x10,%esp
  800e1c:	85 c0                	test   %eax,%eax
  800e1e:	79 17                	jns    800e37 <fork+0x3e>
        panic("fork: cannot create child env");
  800e20:	83 ec 04             	sub    $0x4,%esp
  800e23:	68 83 18 80 00       	push   $0x801883
  800e28:	68 97 00 00 00       	push   $0x97
  800e2d:	68 50 18 80 00       	push   $0x801850
  800e32:	e8 e7 02 00 00       	call   80111e <_panic>
    } else if (envid == 0) {
  800e37:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800e3b:	75 2a                	jne    800e67 <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800e3d:	e8 b7 fc ff ff       	call   800af9 <sys_getenvid>
  800e42:	25 ff 03 00 00       	and    $0x3ff,%eax
  800e47:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e4e:	c1 e0 07             	shl    $0x7,%eax
  800e51:	29 d0                	sub    %edx,%eax
  800e53:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800e58:	a3 08 20 80 00       	mov    %eax,0x802008
        return 0;
  800e5d:	b8 00 00 00 00       	mov    $0x0,%eax
  800e62:	e9 94 01 00 00       	jmp    800ffb <fork+0x202>
  800e67:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800e6e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800e71:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800e78:	a8 01                	test   $0x1,%al
  800e7a:	0f 84 0a 01 00 00    	je     800f8a <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800e80:	c1 e7 0a             	shl    $0xa,%edi
  800e83:	be 00 00 00 00       	mov    $0x0,%esi
  800e88:	89 fb                	mov    %edi,%ebx
  800e8a:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800e8c:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800e92:	75 34                	jne    800ec8 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800e94:	83 ec 04             	sub    $0x4,%esp
  800e97:	6a 07                	push   $0x7
  800e99:	68 00 f0 bf ee       	push   $0xeebff000
  800e9e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ea1:	e8 91 fc ff ff       	call   800b37 <sys_page_alloc>
  800ea6:	83 c4 10             	add    $0x10,%esp
  800ea9:	85 c0                	test   %eax,%eax
  800eab:	0f 84 cc 00 00 00    	je     800f7d <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  800eb1:	83 ec 04             	sub    $0x4,%esp
  800eb4:	68 a1 18 80 00       	push   $0x8018a1
  800eb9:	68 ad 00 00 00       	push   $0xad
  800ebe:	68 50 18 80 00       	push   $0x801850
  800ec3:	e8 56 02 00 00       	call   80111e <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  800ec8:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  800ecf:	a8 01                	test   $0x1,%al
  800ed1:	0f 84 a6 00 00 00    	je     800f7d <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  800ed7:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  800ede:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  800ee1:	a9 02 08 00 00       	test   $0x802,%eax
  800ee6:	74 62                	je     800f4a <fork+0x151>
  800ee8:	f6 c4 04             	test   $0x4,%ah
  800eeb:	75 78                	jne    800f65 <fork+0x16c>
        if (sys_page_map(0,
  800eed:	83 ec 0c             	sub    $0xc,%esp
  800ef0:	68 05 08 00 00       	push   $0x805
  800ef5:	53                   	push   %ebx
  800ef6:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ef9:	53                   	push   %ebx
  800efa:	6a 00                	push   $0x0
  800efc:	e8 79 fc ff ff       	call   800b7a <sys_page_map>
  800f01:	83 c4 20             	add    $0x20,%esp
  800f04:	85 c0                	test   %eax,%eax
  800f06:	74 14                	je     800f1c <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  800f08:	83 ec 04             	sub    $0x4,%esp
  800f0b:	68 bc 18 80 00       	push   $0x8018bc
  800f10:	6a 64                	push   $0x64
  800f12:	68 50 18 80 00       	push   $0x801850
  800f17:	e8 02 02 00 00       	call   80111e <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  800f1c:	83 ec 0c             	sub    $0xc,%esp
  800f1f:	68 05 08 00 00       	push   $0x805
  800f24:	53                   	push   %ebx
  800f25:	6a 00                	push   $0x0
  800f27:	53                   	push   %ebx
  800f28:	6a 00                	push   $0x0
  800f2a:	e8 4b fc ff ff       	call   800b7a <sys_page_map>
  800f2f:	83 c4 20             	add    $0x20,%esp
  800f32:	85 c0                	test   %eax,%eax
  800f34:	74 47                	je     800f7d <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  800f36:	83 ec 04             	sub    $0x4,%esp
  800f39:	68 d3 18 80 00       	push   $0x8018d3
  800f3e:	6a 6d                	push   $0x6d
  800f40:	68 50 18 80 00       	push   $0x801850
  800f45:	e8 d4 01 00 00       	call   80111e <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  800f4a:	f6 c4 04             	test   $0x4,%ah
  800f4d:	75 16                	jne    800f65 <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  800f4f:	83 ec 0c             	sub    $0xc,%esp
  800f52:	6a 05                	push   $0x5
  800f54:	53                   	push   %ebx
  800f55:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f58:	53                   	push   %ebx
  800f59:	6a 00                	push   $0x0
  800f5b:	e8 1a fc ff ff       	call   800b7a <sys_page_map>
  800f60:	83 c4 20             	add    $0x20,%esp
  800f63:	eb 18                	jmp    800f7d <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  800f65:	83 ec 0c             	sub    $0xc,%esp
  800f68:	25 07 0e 00 00       	and    $0xe07,%eax
  800f6d:	50                   	push   %eax
  800f6e:	53                   	push   %ebx
  800f6f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800f72:	53                   	push   %ebx
  800f73:	6a 00                	push   $0x0
  800f75:	e8 00 fc ff ff       	call   800b7a <sys_page_map>
  800f7a:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  800f7d:	46                   	inc    %esi
  800f7e:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  800f84:	0f 85 fe fe ff ff    	jne    800e88 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  800f8a:	ff 45 e0             	incl   -0x20(%ebp)
  800f8d:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800f90:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  800f95:	0f 85 d3 fe ff ff    	jne    800e6e <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  800f9b:	a1 08 20 80 00       	mov    0x802008,%eax
  800fa0:	8b 40 64             	mov    0x64(%eax),%eax
  800fa3:	83 ec 08             	sub    $0x8,%esp
  800fa6:	50                   	push   %eax
  800fa7:	ff 75 dc             	pushl  -0x24(%ebp)
  800faa:	e8 f2 fc ff ff       	call   800ca1 <sys_env_set_pgfault_upcall>
  800faf:	83 c4 10             	add    $0x10,%esp
  800fb2:	85 c0                	test   %eax,%eax
  800fb4:	74 17                	je     800fcd <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  800fb6:	83 ec 04             	sub    $0x4,%esp
  800fb9:	68 30 18 80 00       	push   $0x801830
  800fbe:	68 b9 00 00 00       	push   $0xb9
  800fc3:	68 50 18 80 00       	push   $0x801850
  800fc8:	e8 51 01 00 00       	call   80111e <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  800fcd:	83 ec 08             	sub    $0x8,%esp
  800fd0:	6a 02                	push   $0x2
  800fd2:	ff 75 dc             	pushl  -0x24(%ebp)
  800fd5:	e8 24 fc ff ff       	call   800bfe <sys_env_set_status>
  800fda:	83 c4 10             	add    $0x10,%esp
  800fdd:	85 c0                	test   %eax,%eax
  800fdf:	74 17                	je     800ff8 <fork+0x1ff>
        panic("fork: cannot set env status");
  800fe1:	83 ec 04             	sub    $0x4,%esp
  800fe4:	68 ee 18 80 00       	push   $0x8018ee
  800fe9:	68 bd 00 00 00       	push   $0xbd
  800fee:	68 50 18 80 00       	push   $0x801850
  800ff3:	e8 26 01 00 00       	call   80111e <_panic>

    return envid;
  800ff8:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  800ffb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ffe:	5b                   	pop    %ebx
  800fff:	5e                   	pop    %esi
  801000:	5f                   	pop    %edi
  801001:	5d                   	pop    %ebp
  801002:	c3                   	ret    

00801003 <sfork>:

// Challenge!
int
sfork(void)
{
  801003:	55                   	push   %ebp
  801004:	89 e5                	mov    %esp,%ebp
  801006:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  801009:	68 0a 19 80 00       	push   $0x80190a
  80100e:	68 c8 00 00 00       	push   $0xc8
  801013:	68 50 18 80 00       	push   $0x801850
  801018:	e8 01 01 00 00       	call   80111e <_panic>

0080101d <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  80101d:	55                   	push   %ebp
  80101e:	89 e5                	mov    %esp,%ebp
  801020:	56                   	push   %esi
  801021:	53                   	push   %ebx
  801022:	8b 75 08             	mov    0x8(%ebp),%esi
  801025:	8b 45 0c             	mov    0xc(%ebp),%eax
  801028:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  80102b:	85 c0                	test   %eax,%eax
  80102d:	75 05                	jne    801034 <ipc_recv+0x17>
        pg = (void *)UTOP;
  80102f:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801034:	83 ec 0c             	sub    $0xc,%esp
  801037:	50                   	push   %eax
  801038:	e8 c9 fc ff ff       	call   800d06 <sys_ipc_recv>
  80103d:	83 c4 10             	add    $0x10,%esp
  801040:	85 c0                	test   %eax,%eax
  801042:	74 16                	je     80105a <ipc_recv+0x3d>
        if (from_env_store)
  801044:	85 f6                	test   %esi,%esi
  801046:	74 06                	je     80104e <ipc_recv+0x31>
            *from_env_store = 0;
  801048:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  80104e:	85 db                	test   %ebx,%ebx
  801050:	74 2c                	je     80107e <ipc_recv+0x61>
            *perm_store = 0;
  801052:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801058:	eb 24                	jmp    80107e <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  80105a:	85 f6                	test   %esi,%esi
  80105c:	74 0a                	je     801068 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  80105e:	a1 08 20 80 00       	mov    0x802008,%eax
  801063:	8b 40 74             	mov    0x74(%eax),%eax
  801066:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801068:	85 db                	test   %ebx,%ebx
  80106a:	74 0a                	je     801076 <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  80106c:	a1 08 20 80 00       	mov    0x802008,%eax
  801071:	8b 40 78             	mov    0x78(%eax),%eax
  801074:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801076:	a1 08 20 80 00       	mov    0x802008,%eax
  80107b:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  80107e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801081:	5b                   	pop    %ebx
  801082:	5e                   	pop    %esi
  801083:	5d                   	pop    %ebp
  801084:	c3                   	ret    

00801085 <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801085:	55                   	push   %ebp
  801086:	89 e5                	mov    %esp,%ebp
  801088:	57                   	push   %edi
  801089:	56                   	push   %esi
  80108a:	53                   	push   %ebx
  80108b:	83 ec 0c             	sub    $0xc,%esp
  80108e:	8b 75 0c             	mov    0xc(%ebp),%esi
  801091:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801094:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801097:	85 db                	test   %ebx,%ebx
  801099:	75 0c                	jne    8010a7 <ipc_send+0x22>
        pg = (void *)UTOP;
  80109b:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  8010a0:	eb 05                	jmp    8010a7 <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  8010a2:	e8 71 fa ff ff       	call   800b18 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  8010a7:	57                   	push   %edi
  8010a8:	53                   	push   %ebx
  8010a9:	56                   	push   %esi
  8010aa:	ff 75 08             	pushl  0x8(%ebp)
  8010ad:	e8 31 fc ff ff       	call   800ce3 <sys_ipc_try_send>
  8010b2:	83 c4 10             	add    $0x10,%esp
  8010b5:	83 f8 f9             	cmp    $0xfffffff9,%eax
  8010b8:	74 e8                	je     8010a2 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  8010ba:	85 c0                	test   %eax,%eax
  8010bc:	74 14                	je     8010d2 <ipc_send+0x4d>
        panic("ipc_send: error");
  8010be:	83 ec 04             	sub    $0x4,%esp
  8010c1:	68 20 19 80 00       	push   $0x801920
  8010c6:	6a 6a                	push   $0x6a
  8010c8:	68 30 19 80 00       	push   $0x801930
  8010cd:	e8 4c 00 00 00       	call   80111e <_panic>
    }
	//panic("ipc_send not implemented");
}
  8010d2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8010d5:	5b                   	pop    %ebx
  8010d6:	5e                   	pop    %esi
  8010d7:	5f                   	pop    %edi
  8010d8:	5d                   	pop    %ebp
  8010d9:	c3                   	ret    

008010da <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  8010da:	55                   	push   %ebp
  8010db:	89 e5                	mov    %esp,%ebp
  8010dd:	53                   	push   %ebx
  8010de:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  8010e1:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  8010e6:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  8010ed:	89 d0                	mov    %edx,%eax
  8010ef:	c1 e0 07             	shl    $0x7,%eax
  8010f2:	29 d8                	sub    %ebx,%eax
  8010f4:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8010f9:	8b 40 50             	mov    0x50(%eax),%eax
  8010fc:	39 c8                	cmp    %ecx,%eax
  8010fe:	75 0d                	jne    80110d <ipc_find_env+0x33>
			return envs[i].env_id;
  801100:	c1 e2 07             	shl    $0x7,%edx
  801103:	29 da                	sub    %ebx,%edx
  801105:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  80110b:	eb 0e                	jmp    80111b <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  80110d:	42                   	inc    %edx
  80110e:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801114:	75 d0                	jne    8010e6 <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801116:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80111b:	5b                   	pop    %ebx
  80111c:	5d                   	pop    %ebp
  80111d:	c3                   	ret    

0080111e <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80111e:	55                   	push   %ebp
  80111f:	89 e5                	mov    %esp,%ebp
  801121:	56                   	push   %esi
  801122:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  801123:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  801126:	8b 35 00 20 80 00    	mov    0x802000,%esi
  80112c:	e8 c8 f9 ff ff       	call   800af9 <sys_getenvid>
  801131:	83 ec 0c             	sub    $0xc,%esp
  801134:	ff 75 0c             	pushl  0xc(%ebp)
  801137:	ff 75 08             	pushl  0x8(%ebp)
  80113a:	56                   	push   %esi
  80113b:	50                   	push   %eax
  80113c:	68 3c 19 80 00       	push   $0x80193c
  801141:	e8 a9 f0 ff ff       	call   8001ef <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  801146:	83 c4 18             	add    $0x18,%esp
  801149:	53                   	push   %ebx
  80114a:	ff 75 10             	pushl  0x10(%ebp)
  80114d:	e8 4c f0 ff ff       	call   80019e <vcprintf>
	cprintf("\n");
  801152:	c7 04 24 78 14 80 00 	movl   $0x801478,(%esp)
  801159:	e8 91 f0 ff ff       	call   8001ef <cprintf>
  80115e:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801161:	cc                   	int3   
  801162:	eb fd                	jmp    801161 <_panic+0x43>

00801164 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801164:	55                   	push   %ebp
  801165:	89 e5                	mov    %esp,%ebp
  801167:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  80116a:	83 3d 0c 20 80 00 00 	cmpl   $0x0,0x80200c
  801171:	75 3e                	jne    8011b1 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801173:	83 ec 04             	sub    $0x4,%esp
  801176:	6a 07                	push   $0x7
  801178:	68 00 f0 bf ee       	push   $0xeebff000
  80117d:	6a 00                	push   $0x0
  80117f:	e8 b3 f9 ff ff       	call   800b37 <sys_page_alloc>
  801184:	83 c4 10             	add    $0x10,%esp
  801187:	85 c0                	test   %eax,%eax
  801189:	74 14                	je     80119f <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  80118b:	83 ec 04             	sub    $0x4,%esp
  80118e:	68 60 19 80 00       	push   $0x801960
  801193:	6a 23                	push   $0x23
  801195:	68 84 19 80 00       	push   $0x801984
  80119a:	e8 7f ff ff ff       	call   80111e <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  80119f:	83 ec 08             	sub    $0x8,%esp
  8011a2:	68 bb 11 80 00       	push   $0x8011bb
  8011a7:	6a 00                	push   $0x0
  8011a9:	e8 f3 fa ff ff       	call   800ca1 <sys_env_set_pgfault_upcall>
  8011ae:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  8011b1:	8b 45 08             	mov    0x8(%ebp),%eax
  8011b4:	a3 0c 20 80 00       	mov    %eax,0x80200c
}
  8011b9:	c9                   	leave  
  8011ba:	c3                   	ret    

008011bb <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  8011bb:	54                   	push   %esp
	movl _pgfault_handler, %eax
  8011bc:	a1 0c 20 80 00       	mov    0x80200c,%eax
	call *%eax
  8011c1:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  8011c3:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  8011c6:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  8011c8:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  8011cc:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  8011d0:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  8011d1:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  8011d4:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  8011d6:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  8011d9:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  8011dc:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  8011dd:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  8011e0:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  8011e1:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  8011e2:	c3                   	ret    
  8011e3:	90                   	nop

008011e4 <__udivdi3>:
  8011e4:	55                   	push   %ebp
  8011e5:	57                   	push   %edi
  8011e6:	56                   	push   %esi
  8011e7:	53                   	push   %ebx
  8011e8:	83 ec 1c             	sub    $0x1c,%esp
  8011eb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8011ef:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8011f3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8011f7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8011fb:	89 ca                	mov    %ecx,%edx
  8011fd:	89 f8                	mov    %edi,%eax
  8011ff:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801203:	85 f6                	test   %esi,%esi
  801205:	75 2d                	jne    801234 <__udivdi3+0x50>
  801207:	39 cf                	cmp    %ecx,%edi
  801209:	77 65                	ja     801270 <__udivdi3+0x8c>
  80120b:	89 fd                	mov    %edi,%ebp
  80120d:	85 ff                	test   %edi,%edi
  80120f:	75 0b                	jne    80121c <__udivdi3+0x38>
  801211:	b8 01 00 00 00       	mov    $0x1,%eax
  801216:	31 d2                	xor    %edx,%edx
  801218:	f7 f7                	div    %edi
  80121a:	89 c5                	mov    %eax,%ebp
  80121c:	31 d2                	xor    %edx,%edx
  80121e:	89 c8                	mov    %ecx,%eax
  801220:	f7 f5                	div    %ebp
  801222:	89 c1                	mov    %eax,%ecx
  801224:	89 d8                	mov    %ebx,%eax
  801226:	f7 f5                	div    %ebp
  801228:	89 cf                	mov    %ecx,%edi
  80122a:	89 fa                	mov    %edi,%edx
  80122c:	83 c4 1c             	add    $0x1c,%esp
  80122f:	5b                   	pop    %ebx
  801230:	5e                   	pop    %esi
  801231:	5f                   	pop    %edi
  801232:	5d                   	pop    %ebp
  801233:	c3                   	ret    
  801234:	39 ce                	cmp    %ecx,%esi
  801236:	77 28                	ja     801260 <__udivdi3+0x7c>
  801238:	0f bd fe             	bsr    %esi,%edi
  80123b:	83 f7 1f             	xor    $0x1f,%edi
  80123e:	75 40                	jne    801280 <__udivdi3+0x9c>
  801240:	39 ce                	cmp    %ecx,%esi
  801242:	72 0a                	jb     80124e <__udivdi3+0x6a>
  801244:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801248:	0f 87 9e 00 00 00    	ja     8012ec <__udivdi3+0x108>
  80124e:	b8 01 00 00 00       	mov    $0x1,%eax
  801253:	89 fa                	mov    %edi,%edx
  801255:	83 c4 1c             	add    $0x1c,%esp
  801258:	5b                   	pop    %ebx
  801259:	5e                   	pop    %esi
  80125a:	5f                   	pop    %edi
  80125b:	5d                   	pop    %ebp
  80125c:	c3                   	ret    
  80125d:	8d 76 00             	lea    0x0(%esi),%esi
  801260:	31 ff                	xor    %edi,%edi
  801262:	31 c0                	xor    %eax,%eax
  801264:	89 fa                	mov    %edi,%edx
  801266:	83 c4 1c             	add    $0x1c,%esp
  801269:	5b                   	pop    %ebx
  80126a:	5e                   	pop    %esi
  80126b:	5f                   	pop    %edi
  80126c:	5d                   	pop    %ebp
  80126d:	c3                   	ret    
  80126e:	66 90                	xchg   %ax,%ax
  801270:	89 d8                	mov    %ebx,%eax
  801272:	f7 f7                	div    %edi
  801274:	31 ff                	xor    %edi,%edi
  801276:	89 fa                	mov    %edi,%edx
  801278:	83 c4 1c             	add    $0x1c,%esp
  80127b:	5b                   	pop    %ebx
  80127c:	5e                   	pop    %esi
  80127d:	5f                   	pop    %edi
  80127e:	5d                   	pop    %ebp
  80127f:	c3                   	ret    
  801280:	bd 20 00 00 00       	mov    $0x20,%ebp
  801285:	89 eb                	mov    %ebp,%ebx
  801287:	29 fb                	sub    %edi,%ebx
  801289:	89 f9                	mov    %edi,%ecx
  80128b:	d3 e6                	shl    %cl,%esi
  80128d:	89 c5                	mov    %eax,%ebp
  80128f:	88 d9                	mov    %bl,%cl
  801291:	d3 ed                	shr    %cl,%ebp
  801293:	89 e9                	mov    %ebp,%ecx
  801295:	09 f1                	or     %esi,%ecx
  801297:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80129b:	89 f9                	mov    %edi,%ecx
  80129d:	d3 e0                	shl    %cl,%eax
  80129f:	89 c5                	mov    %eax,%ebp
  8012a1:	89 d6                	mov    %edx,%esi
  8012a3:	88 d9                	mov    %bl,%cl
  8012a5:	d3 ee                	shr    %cl,%esi
  8012a7:	89 f9                	mov    %edi,%ecx
  8012a9:	d3 e2                	shl    %cl,%edx
  8012ab:	8b 44 24 08          	mov    0x8(%esp),%eax
  8012af:	88 d9                	mov    %bl,%cl
  8012b1:	d3 e8                	shr    %cl,%eax
  8012b3:	09 c2                	or     %eax,%edx
  8012b5:	89 d0                	mov    %edx,%eax
  8012b7:	89 f2                	mov    %esi,%edx
  8012b9:	f7 74 24 0c          	divl   0xc(%esp)
  8012bd:	89 d6                	mov    %edx,%esi
  8012bf:	89 c3                	mov    %eax,%ebx
  8012c1:	f7 e5                	mul    %ebp
  8012c3:	39 d6                	cmp    %edx,%esi
  8012c5:	72 19                	jb     8012e0 <__udivdi3+0xfc>
  8012c7:	74 0b                	je     8012d4 <__udivdi3+0xf0>
  8012c9:	89 d8                	mov    %ebx,%eax
  8012cb:	31 ff                	xor    %edi,%edi
  8012cd:	e9 58 ff ff ff       	jmp    80122a <__udivdi3+0x46>
  8012d2:	66 90                	xchg   %ax,%ax
  8012d4:	8b 54 24 08          	mov    0x8(%esp),%edx
  8012d8:	89 f9                	mov    %edi,%ecx
  8012da:	d3 e2                	shl    %cl,%edx
  8012dc:	39 c2                	cmp    %eax,%edx
  8012de:	73 e9                	jae    8012c9 <__udivdi3+0xe5>
  8012e0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8012e3:	31 ff                	xor    %edi,%edi
  8012e5:	e9 40 ff ff ff       	jmp    80122a <__udivdi3+0x46>
  8012ea:	66 90                	xchg   %ax,%ax
  8012ec:	31 c0                	xor    %eax,%eax
  8012ee:	e9 37 ff ff ff       	jmp    80122a <__udivdi3+0x46>
  8012f3:	90                   	nop

008012f4 <__umoddi3>:
  8012f4:	55                   	push   %ebp
  8012f5:	57                   	push   %edi
  8012f6:	56                   	push   %esi
  8012f7:	53                   	push   %ebx
  8012f8:	83 ec 1c             	sub    $0x1c,%esp
  8012fb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8012ff:	8b 74 24 34          	mov    0x34(%esp),%esi
  801303:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801307:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  80130b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80130f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801313:	89 f3                	mov    %esi,%ebx
  801315:	89 fa                	mov    %edi,%edx
  801317:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80131b:	89 34 24             	mov    %esi,(%esp)
  80131e:	85 c0                	test   %eax,%eax
  801320:	75 1a                	jne    80133c <__umoddi3+0x48>
  801322:	39 f7                	cmp    %esi,%edi
  801324:	0f 86 a2 00 00 00    	jbe    8013cc <__umoddi3+0xd8>
  80132a:	89 c8                	mov    %ecx,%eax
  80132c:	89 f2                	mov    %esi,%edx
  80132e:	f7 f7                	div    %edi
  801330:	89 d0                	mov    %edx,%eax
  801332:	31 d2                	xor    %edx,%edx
  801334:	83 c4 1c             	add    $0x1c,%esp
  801337:	5b                   	pop    %ebx
  801338:	5e                   	pop    %esi
  801339:	5f                   	pop    %edi
  80133a:	5d                   	pop    %ebp
  80133b:	c3                   	ret    
  80133c:	39 f0                	cmp    %esi,%eax
  80133e:	0f 87 ac 00 00 00    	ja     8013f0 <__umoddi3+0xfc>
  801344:	0f bd e8             	bsr    %eax,%ebp
  801347:	83 f5 1f             	xor    $0x1f,%ebp
  80134a:	0f 84 ac 00 00 00    	je     8013fc <__umoddi3+0x108>
  801350:	bf 20 00 00 00       	mov    $0x20,%edi
  801355:	29 ef                	sub    %ebp,%edi
  801357:	89 fe                	mov    %edi,%esi
  801359:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80135d:	89 e9                	mov    %ebp,%ecx
  80135f:	d3 e0                	shl    %cl,%eax
  801361:	89 d7                	mov    %edx,%edi
  801363:	89 f1                	mov    %esi,%ecx
  801365:	d3 ef                	shr    %cl,%edi
  801367:	09 c7                	or     %eax,%edi
  801369:	89 e9                	mov    %ebp,%ecx
  80136b:	d3 e2                	shl    %cl,%edx
  80136d:	89 14 24             	mov    %edx,(%esp)
  801370:	89 d8                	mov    %ebx,%eax
  801372:	d3 e0                	shl    %cl,%eax
  801374:	89 c2                	mov    %eax,%edx
  801376:	8b 44 24 08          	mov    0x8(%esp),%eax
  80137a:	d3 e0                	shl    %cl,%eax
  80137c:	89 44 24 04          	mov    %eax,0x4(%esp)
  801380:	8b 44 24 08          	mov    0x8(%esp),%eax
  801384:	89 f1                	mov    %esi,%ecx
  801386:	d3 e8                	shr    %cl,%eax
  801388:	09 d0                	or     %edx,%eax
  80138a:	d3 eb                	shr    %cl,%ebx
  80138c:	89 da                	mov    %ebx,%edx
  80138e:	f7 f7                	div    %edi
  801390:	89 d3                	mov    %edx,%ebx
  801392:	f7 24 24             	mull   (%esp)
  801395:	89 c6                	mov    %eax,%esi
  801397:	89 d1                	mov    %edx,%ecx
  801399:	39 d3                	cmp    %edx,%ebx
  80139b:	0f 82 87 00 00 00    	jb     801428 <__umoddi3+0x134>
  8013a1:	0f 84 91 00 00 00    	je     801438 <__umoddi3+0x144>
  8013a7:	8b 54 24 04          	mov    0x4(%esp),%edx
  8013ab:	29 f2                	sub    %esi,%edx
  8013ad:	19 cb                	sbb    %ecx,%ebx
  8013af:	89 d8                	mov    %ebx,%eax
  8013b1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8013b5:	d3 e0                	shl    %cl,%eax
  8013b7:	89 e9                	mov    %ebp,%ecx
  8013b9:	d3 ea                	shr    %cl,%edx
  8013bb:	09 d0                	or     %edx,%eax
  8013bd:	89 e9                	mov    %ebp,%ecx
  8013bf:	d3 eb                	shr    %cl,%ebx
  8013c1:	89 da                	mov    %ebx,%edx
  8013c3:	83 c4 1c             	add    $0x1c,%esp
  8013c6:	5b                   	pop    %ebx
  8013c7:	5e                   	pop    %esi
  8013c8:	5f                   	pop    %edi
  8013c9:	5d                   	pop    %ebp
  8013ca:	c3                   	ret    
  8013cb:	90                   	nop
  8013cc:	89 fd                	mov    %edi,%ebp
  8013ce:	85 ff                	test   %edi,%edi
  8013d0:	75 0b                	jne    8013dd <__umoddi3+0xe9>
  8013d2:	b8 01 00 00 00       	mov    $0x1,%eax
  8013d7:	31 d2                	xor    %edx,%edx
  8013d9:	f7 f7                	div    %edi
  8013db:	89 c5                	mov    %eax,%ebp
  8013dd:	89 f0                	mov    %esi,%eax
  8013df:	31 d2                	xor    %edx,%edx
  8013e1:	f7 f5                	div    %ebp
  8013e3:	89 c8                	mov    %ecx,%eax
  8013e5:	f7 f5                	div    %ebp
  8013e7:	89 d0                	mov    %edx,%eax
  8013e9:	e9 44 ff ff ff       	jmp    801332 <__umoddi3+0x3e>
  8013ee:	66 90                	xchg   %ax,%ax
  8013f0:	89 c8                	mov    %ecx,%eax
  8013f2:	89 f2                	mov    %esi,%edx
  8013f4:	83 c4 1c             	add    $0x1c,%esp
  8013f7:	5b                   	pop    %ebx
  8013f8:	5e                   	pop    %esi
  8013f9:	5f                   	pop    %edi
  8013fa:	5d                   	pop    %ebp
  8013fb:	c3                   	ret    
  8013fc:	3b 04 24             	cmp    (%esp),%eax
  8013ff:	72 06                	jb     801407 <__umoddi3+0x113>
  801401:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801405:	77 0f                	ja     801416 <__umoddi3+0x122>
  801407:	89 f2                	mov    %esi,%edx
  801409:	29 f9                	sub    %edi,%ecx
  80140b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  80140f:	89 14 24             	mov    %edx,(%esp)
  801412:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801416:	8b 44 24 04          	mov    0x4(%esp),%eax
  80141a:	8b 14 24             	mov    (%esp),%edx
  80141d:	83 c4 1c             	add    $0x1c,%esp
  801420:	5b                   	pop    %ebx
  801421:	5e                   	pop    %esi
  801422:	5f                   	pop    %edi
  801423:	5d                   	pop    %ebp
  801424:	c3                   	ret    
  801425:	8d 76 00             	lea    0x0(%esi),%esi
  801428:	2b 04 24             	sub    (%esp),%eax
  80142b:	19 fa                	sbb    %edi,%edx
  80142d:	89 d1                	mov    %edx,%ecx
  80142f:	89 c6                	mov    %eax,%esi
  801431:	e9 71 ff ff ff       	jmp    8013a7 <__umoddi3+0xb3>
  801436:	66 90                	xchg   %ax,%ax
  801438:	39 44 24 04          	cmp    %eax,0x4(%esp)
  80143c:	72 ea                	jb     801428 <__umoddi3+0x134>
  80143e:	89 d9                	mov    %ebx,%ecx
  801440:	e9 62 ff ff ff       	jmp    8013a7 <__umoddi3+0xb3>
