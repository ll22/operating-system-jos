
obj/user/testpiperace.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 dd 01 00 00       	call   80020e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	83 ec 1c             	sub    $0x1c,%esp
	int p[2], r, pid, i, max;
	void *va;
	struct Fd *fd;
	const volatile struct Env *kid;

	cprintf("testing for dup race...\n");
  80003b:	68 40 22 80 00       	push   $0x802240
  800040:	e8 02 03 00 00       	call   800347 <cprintf>
	if ((r = pipe(p)) < 0)
  800045:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800048:	89 04 24             	mov    %eax,(%esp)
  80004b:	e8 0d 1c 00 00       	call   801c5d <pipe>
  800050:	83 c4 10             	add    $0x10,%esp
  800053:	85 c0                	test   %eax,%eax
  800055:	79 12                	jns    800069 <umain+0x36>
		panic("pipe: %e", r);
  800057:	50                   	push   %eax
  800058:	68 59 22 80 00       	push   $0x802259
  80005d:	6a 0d                	push   $0xd
  80005f:	68 62 22 80 00       	push   $0x802262
  800064:	e8 06 02 00 00       	call   80026f <_panic>
	max = 200;
	if ((r = fork()) < 0)
  800069:	e8 e3 0e 00 00       	call   800f51 <fork>
  80006e:	89 c6                	mov    %eax,%esi
  800070:	85 c0                	test   %eax,%eax
  800072:	79 12                	jns    800086 <umain+0x53>
		panic("fork: %e", r);
  800074:	50                   	push   %eax
  800075:	68 76 22 80 00       	push   $0x802276
  80007a:	6a 10                	push   $0x10
  80007c:	68 62 22 80 00       	push   $0x802262
  800081:	e8 e9 01 00 00       	call   80026f <_panic>
	if (r == 0) {
  800086:	85 c0                	test   %eax,%eax
  800088:	75 53                	jne    8000dd <umain+0xaa>
		close(p[1]);
  80008a:	83 ec 0c             	sub    $0xc,%esp
  80008d:	ff 75 f4             	pushl  -0xc(%ebp)
  800090:	e8 85 13 00 00       	call   80141a <close>
  800095:	83 c4 10             	add    $0x10,%esp
  800098:	bb c8 00 00 00       	mov    $0xc8,%ebx
		// If a clock interrupt catches dup between mapping the
		// fd and mapping the pipe structure, we'll have the same
		// ref counts, still a no-no.
		//
		for (i=0; i<max; i++) {
			if(pipeisclosed(p[0])){
  80009d:	83 ec 0c             	sub    $0xc,%esp
  8000a0:	ff 75 f0             	pushl  -0x10(%ebp)
  8000a3:	e8 02 1d 00 00       	call   801daa <pipeisclosed>
  8000a8:	83 c4 10             	add    $0x10,%esp
  8000ab:	85 c0                	test   %eax,%eax
  8000ad:	74 15                	je     8000c4 <umain+0x91>
				cprintf("RACE: pipe appears closed\n");
  8000af:	83 ec 0c             	sub    $0xc,%esp
  8000b2:	68 7f 22 80 00       	push   $0x80227f
  8000b7:	e8 8b 02 00 00       	call   800347 <cprintf>
				exit();
  8000bc:	e8 9c 01 00 00       	call   80025d <exit>
  8000c1:	83 c4 10             	add    $0x10,%esp
			}
			sys_yield();
  8000c4:	e8 a7 0b 00 00       	call   800c70 <sys_yield>
		//
		// If a clock interrupt catches dup between mapping the
		// fd and mapping the pipe structure, we'll have the same
		// ref counts, still a no-no.
		//
		for (i=0; i<max; i++) {
  8000c9:	4b                   	dec    %ebx
  8000ca:	75 d1                	jne    80009d <umain+0x6a>
				exit();
			}
			sys_yield();
		}
		// do something to be not runnable besides exiting
		ipc_recv(0,0,0);
  8000cc:	83 ec 04             	sub    $0x4,%esp
  8000cf:	6a 00                	push   $0x0
  8000d1:	6a 00                	push   $0x0
  8000d3:	6a 00                	push   $0x0
  8000d5:	e8 9b 10 00 00       	call   801175 <ipc_recv>
  8000da:	83 c4 10             	add    $0x10,%esp
	}
	pid = r;
	cprintf("pid is %d\n", pid);
  8000dd:	83 ec 08             	sub    $0x8,%esp
  8000e0:	56                   	push   %esi
  8000e1:	68 9a 22 80 00       	push   $0x80229a
  8000e6:	e8 5c 02 00 00       	call   800347 <cprintf>
	va = 0;
	kid = &envs[ENVX(pid)];
  8000eb:	81 e6 ff 03 00 00    	and    $0x3ff,%esi
  8000f1:	89 f3                	mov    %esi,%ebx
	cprintf("kid is %d\n", kid-envs);
  8000f3:	83 c4 08             	add    $0x8,%esp
		ipc_recv(0,0,0);
	}
	pid = r;
	cprintf("pid is %d\n", pid);
	va = 0;
	kid = &envs[ENVX(pid)];
  8000f6:	8d 14 b5 00 00 00 00 	lea    0x0(,%esi,4),%edx
  8000fd:	89 f0                	mov    %esi,%eax
  8000ff:	c1 e0 07             	shl    $0x7,%eax
	cprintf("kid is %d\n", kid-envs);
  800102:	29 d0                	sub    %edx,%eax
  800104:	c1 f8 02             	sar    $0x2,%eax
  800107:	89 c1                	mov    %eax,%ecx
  800109:	c1 e1 05             	shl    $0x5,%ecx
  80010c:	89 c2                	mov    %eax,%edx
  80010e:	c1 e2 0a             	shl    $0xa,%edx
  800111:	01 ca                	add    %ecx,%edx
  800113:	01 c2                	add    %eax,%edx
  800115:	89 d1                	mov    %edx,%ecx
  800117:	c1 e1 0f             	shl    $0xf,%ecx
  80011a:	01 ca                	add    %ecx,%edx
  80011c:	c1 e2 05             	shl    $0x5,%edx
  80011f:	01 d0                	add    %edx,%eax
  800121:	f7 d8                	neg    %eax
  800123:	50                   	push   %eax
  800124:	68 a5 22 80 00       	push   $0x8022a5
  800129:	e8 19 02 00 00       	call   800347 <cprintf>
	dup(p[0], 10);
  80012e:	83 c4 08             	add    $0x8,%esp
  800131:	6a 0a                	push   $0xa
  800133:	ff 75 f0             	pushl  -0x10(%ebp)
  800136:	e8 2d 13 00 00       	call   801468 <dup>
	while (kid->env_status == ENV_RUNNABLE)
  80013b:	83 c4 10             	add    $0x10,%esp
  80013e:	8d 04 b5 00 00 00 00 	lea    0x0(,%esi,4),%eax
  800145:	c1 e3 07             	shl    $0x7,%ebx
  800148:	29 c3                	sub    %eax,%ebx
  80014a:	81 c3 00 00 c0 ee    	add    $0xeec00000,%ebx
  800150:	eb 10                	jmp    800162 <umain+0x12f>
		dup(p[0], 10);
  800152:	83 ec 08             	sub    $0x8,%esp
  800155:	6a 0a                	push   $0xa
  800157:	ff 75 f0             	pushl  -0x10(%ebp)
  80015a:	e8 09 13 00 00       	call   801468 <dup>
  80015f:	83 c4 10             	add    $0x10,%esp
	cprintf("pid is %d\n", pid);
	va = 0;
	kid = &envs[ENVX(pid)];
	cprintf("kid is %d\n", kid-envs);
	dup(p[0], 10);
	while (kid->env_status == ENV_RUNNABLE)
  800162:	8b 53 54             	mov    0x54(%ebx),%edx
  800165:	83 fa 02             	cmp    $0x2,%edx
  800168:	74 e8                	je     800152 <umain+0x11f>
		dup(p[0], 10);

	cprintf("child done with loop\n");
  80016a:	83 ec 0c             	sub    $0xc,%esp
  80016d:	68 b0 22 80 00       	push   $0x8022b0
  800172:	e8 d0 01 00 00       	call   800347 <cprintf>
	if (pipeisclosed(p[0]))
  800177:	83 c4 04             	add    $0x4,%esp
  80017a:	ff 75 f0             	pushl  -0x10(%ebp)
  80017d:	e8 28 1c 00 00       	call   801daa <pipeisclosed>
  800182:	83 c4 10             	add    $0x10,%esp
  800185:	85 c0                	test   %eax,%eax
  800187:	74 14                	je     80019d <umain+0x16a>
		panic("somehow the other end of p[0] got closed!");
  800189:	83 ec 04             	sub    $0x4,%esp
  80018c:	68 0c 23 80 00       	push   $0x80230c
  800191:	6a 3a                	push   $0x3a
  800193:	68 62 22 80 00       	push   $0x802262
  800198:	e8 d2 00 00 00       	call   80026f <_panic>
	if ((r = fd_lookup(p[0], &fd)) < 0)
  80019d:	83 ec 08             	sub    $0x8,%esp
  8001a0:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8001a3:	50                   	push   %eax
  8001a4:	ff 75 f0             	pushl  -0x10(%ebp)
  8001a7:	e8 40 11 00 00       	call   8012ec <fd_lookup>
  8001ac:	83 c4 10             	add    $0x10,%esp
  8001af:	85 c0                	test   %eax,%eax
  8001b1:	79 12                	jns    8001c5 <umain+0x192>
		panic("cannot look up p[0]: %e", r);
  8001b3:	50                   	push   %eax
  8001b4:	68 c6 22 80 00       	push   $0x8022c6
  8001b9:	6a 3c                	push   $0x3c
  8001bb:	68 62 22 80 00       	push   $0x802262
  8001c0:	e8 aa 00 00 00       	call   80026f <_panic>
	va = fd2data(fd);
  8001c5:	83 ec 0c             	sub    $0xc,%esp
  8001c8:	ff 75 ec             	pushl  -0x14(%ebp)
  8001cb:	e8 b6 10 00 00       	call   801286 <fd2data>
	if (pageref(va) != 3+1)
  8001d0:	89 04 24             	mov    %eax,(%esp)
  8001d3:	e8 7f 18 00 00       	call   801a57 <pageref>
  8001d8:	83 c4 10             	add    $0x10,%esp
  8001db:	83 f8 04             	cmp    $0x4,%eax
  8001de:	74 12                	je     8001f2 <umain+0x1bf>
		cprintf("\nchild detected race\n");
  8001e0:	83 ec 0c             	sub    $0xc,%esp
  8001e3:	68 de 22 80 00       	push   $0x8022de
  8001e8:	e8 5a 01 00 00       	call   800347 <cprintf>
  8001ed:	83 c4 10             	add    $0x10,%esp
  8001f0:	eb 15                	jmp    800207 <umain+0x1d4>
	else
		cprintf("\nrace didn't happen\n", max);
  8001f2:	83 ec 08             	sub    $0x8,%esp
  8001f5:	68 c8 00 00 00       	push   $0xc8
  8001fa:	68 f4 22 80 00       	push   $0x8022f4
  8001ff:	e8 43 01 00 00       	call   800347 <cprintf>
  800204:	83 c4 10             	add    $0x10,%esp
}
  800207:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80020a:	5b                   	pop    %ebx
  80020b:	5e                   	pop    %esi
  80020c:	5d                   	pop    %ebp
  80020d:	c3                   	ret    

0080020e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80020e:	55                   	push   %ebp
  80020f:	89 e5                	mov    %esp,%ebp
  800211:	56                   	push   %esi
  800212:	53                   	push   %ebx
  800213:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800216:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800219:	e8 33 0a 00 00       	call   800c51 <sys_getenvid>
  80021e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800223:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80022a:	c1 e0 07             	shl    $0x7,%eax
  80022d:	29 d0                	sub    %edx,%eax
  80022f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800234:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800239:	85 db                	test   %ebx,%ebx
  80023b:	7e 07                	jle    800244 <libmain+0x36>
		binaryname = argv[0];
  80023d:	8b 06                	mov    (%esi),%eax
  80023f:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  800244:	83 ec 08             	sub    $0x8,%esp
  800247:	56                   	push   %esi
  800248:	53                   	push   %ebx
  800249:	e8 e5 fd ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80024e:	e8 0a 00 00 00       	call   80025d <exit>
}
  800253:	83 c4 10             	add    $0x10,%esp
  800256:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800259:	5b                   	pop    %ebx
  80025a:	5e                   	pop    %esi
  80025b:	5d                   	pop    %ebp
  80025c:	c3                   	ret    

0080025d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80025d:	55                   	push   %ebp
  80025e:	89 e5                	mov    %esp,%ebp
  800260:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800263:	6a 00                	push   $0x0
  800265:	e8 a6 09 00 00       	call   800c10 <sys_env_destroy>
}
  80026a:	83 c4 10             	add    $0x10,%esp
  80026d:	c9                   	leave  
  80026e:	c3                   	ret    

0080026f <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80026f:	55                   	push   %ebp
  800270:	89 e5                	mov    %esp,%ebp
  800272:	56                   	push   %esi
  800273:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800274:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800277:	8b 35 00 30 80 00    	mov    0x803000,%esi
  80027d:	e8 cf 09 00 00       	call   800c51 <sys_getenvid>
  800282:	83 ec 0c             	sub    $0xc,%esp
  800285:	ff 75 0c             	pushl  0xc(%ebp)
  800288:	ff 75 08             	pushl  0x8(%ebp)
  80028b:	56                   	push   %esi
  80028c:	50                   	push   %eax
  80028d:	68 40 23 80 00       	push   $0x802340
  800292:	e8 b0 00 00 00       	call   800347 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800297:	83 c4 18             	add    $0x18,%esp
  80029a:	53                   	push   %ebx
  80029b:	ff 75 10             	pushl  0x10(%ebp)
  80029e:	e8 53 00 00 00       	call   8002f6 <vcprintf>
	cprintf("\n");
  8002a3:	c7 04 24 57 22 80 00 	movl   $0x802257,(%esp)
  8002aa:	e8 98 00 00 00       	call   800347 <cprintf>
  8002af:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8002b2:	cc                   	int3   
  8002b3:	eb fd                	jmp    8002b2 <_panic+0x43>

008002b5 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8002b5:	55                   	push   %ebp
  8002b6:	89 e5                	mov    %esp,%ebp
  8002b8:	53                   	push   %ebx
  8002b9:	83 ec 04             	sub    $0x4,%esp
  8002bc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8002bf:	8b 13                	mov    (%ebx),%edx
  8002c1:	8d 42 01             	lea    0x1(%edx),%eax
  8002c4:	89 03                	mov    %eax,(%ebx)
  8002c6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8002c9:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8002cd:	3d ff 00 00 00       	cmp    $0xff,%eax
  8002d2:	75 1a                	jne    8002ee <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8002d4:	83 ec 08             	sub    $0x8,%esp
  8002d7:	68 ff 00 00 00       	push   $0xff
  8002dc:	8d 43 08             	lea    0x8(%ebx),%eax
  8002df:	50                   	push   %eax
  8002e0:	e8 ee 08 00 00       	call   800bd3 <sys_cputs>
		b->idx = 0;
  8002e5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8002eb:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8002ee:	ff 43 04             	incl   0x4(%ebx)
}
  8002f1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8002f4:	c9                   	leave  
  8002f5:	c3                   	ret    

008002f6 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8002f6:	55                   	push   %ebp
  8002f7:	89 e5                	mov    %esp,%ebp
  8002f9:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8002ff:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800306:	00 00 00 
	b.cnt = 0;
  800309:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800310:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800313:	ff 75 0c             	pushl  0xc(%ebp)
  800316:	ff 75 08             	pushl  0x8(%ebp)
  800319:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80031f:	50                   	push   %eax
  800320:	68 b5 02 80 00       	push   $0x8002b5
  800325:	e8 51 01 00 00       	call   80047b <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80032a:	83 c4 08             	add    $0x8,%esp
  80032d:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800333:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800339:	50                   	push   %eax
  80033a:	e8 94 08 00 00       	call   800bd3 <sys_cputs>

	return b.cnt;
}
  80033f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800345:	c9                   	leave  
  800346:	c3                   	ret    

00800347 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800347:	55                   	push   %ebp
  800348:	89 e5                	mov    %esp,%ebp
  80034a:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80034d:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800350:	50                   	push   %eax
  800351:	ff 75 08             	pushl  0x8(%ebp)
  800354:	e8 9d ff ff ff       	call   8002f6 <vcprintf>
	va_end(ap);

	return cnt;
}
  800359:	c9                   	leave  
  80035a:	c3                   	ret    

0080035b <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80035b:	55                   	push   %ebp
  80035c:	89 e5                	mov    %esp,%ebp
  80035e:	57                   	push   %edi
  80035f:	56                   	push   %esi
  800360:	53                   	push   %ebx
  800361:	83 ec 1c             	sub    $0x1c,%esp
  800364:	89 c7                	mov    %eax,%edi
  800366:	89 d6                	mov    %edx,%esi
  800368:	8b 45 08             	mov    0x8(%ebp),%eax
  80036b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80036e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800371:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800374:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800377:	bb 00 00 00 00       	mov    $0x0,%ebx
  80037c:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80037f:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800382:	39 d3                	cmp    %edx,%ebx
  800384:	72 05                	jb     80038b <printnum+0x30>
  800386:	39 45 10             	cmp    %eax,0x10(%ebp)
  800389:	77 45                	ja     8003d0 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80038b:	83 ec 0c             	sub    $0xc,%esp
  80038e:	ff 75 18             	pushl  0x18(%ebp)
  800391:	8b 45 14             	mov    0x14(%ebp),%eax
  800394:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800397:	53                   	push   %ebx
  800398:	ff 75 10             	pushl  0x10(%ebp)
  80039b:	83 ec 08             	sub    $0x8,%esp
  80039e:	ff 75 e4             	pushl  -0x1c(%ebp)
  8003a1:	ff 75 e0             	pushl  -0x20(%ebp)
  8003a4:	ff 75 dc             	pushl  -0x24(%ebp)
  8003a7:	ff 75 d8             	pushl  -0x28(%ebp)
  8003aa:	e8 29 1c 00 00       	call   801fd8 <__udivdi3>
  8003af:	83 c4 18             	add    $0x18,%esp
  8003b2:	52                   	push   %edx
  8003b3:	50                   	push   %eax
  8003b4:	89 f2                	mov    %esi,%edx
  8003b6:	89 f8                	mov    %edi,%eax
  8003b8:	e8 9e ff ff ff       	call   80035b <printnum>
  8003bd:	83 c4 20             	add    $0x20,%esp
  8003c0:	eb 16                	jmp    8003d8 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8003c2:	83 ec 08             	sub    $0x8,%esp
  8003c5:	56                   	push   %esi
  8003c6:	ff 75 18             	pushl  0x18(%ebp)
  8003c9:	ff d7                	call   *%edi
  8003cb:	83 c4 10             	add    $0x10,%esp
  8003ce:	eb 03                	jmp    8003d3 <printnum+0x78>
  8003d0:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8003d3:	4b                   	dec    %ebx
  8003d4:	85 db                	test   %ebx,%ebx
  8003d6:	7f ea                	jg     8003c2 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8003d8:	83 ec 08             	sub    $0x8,%esp
  8003db:	56                   	push   %esi
  8003dc:	83 ec 04             	sub    $0x4,%esp
  8003df:	ff 75 e4             	pushl  -0x1c(%ebp)
  8003e2:	ff 75 e0             	pushl  -0x20(%ebp)
  8003e5:	ff 75 dc             	pushl  -0x24(%ebp)
  8003e8:	ff 75 d8             	pushl  -0x28(%ebp)
  8003eb:	e8 f8 1c 00 00       	call   8020e8 <__umoddi3>
  8003f0:	83 c4 14             	add    $0x14,%esp
  8003f3:	0f be 80 63 23 80 00 	movsbl 0x802363(%eax),%eax
  8003fa:	50                   	push   %eax
  8003fb:	ff d7                	call   *%edi
}
  8003fd:	83 c4 10             	add    $0x10,%esp
  800400:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800403:	5b                   	pop    %ebx
  800404:	5e                   	pop    %esi
  800405:	5f                   	pop    %edi
  800406:	5d                   	pop    %ebp
  800407:	c3                   	ret    

00800408 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800408:	55                   	push   %ebp
  800409:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80040b:	83 fa 01             	cmp    $0x1,%edx
  80040e:	7e 0e                	jle    80041e <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800410:	8b 10                	mov    (%eax),%edx
  800412:	8d 4a 08             	lea    0x8(%edx),%ecx
  800415:	89 08                	mov    %ecx,(%eax)
  800417:	8b 02                	mov    (%edx),%eax
  800419:	8b 52 04             	mov    0x4(%edx),%edx
  80041c:	eb 22                	jmp    800440 <getuint+0x38>
	else if (lflag)
  80041e:	85 d2                	test   %edx,%edx
  800420:	74 10                	je     800432 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800422:	8b 10                	mov    (%eax),%edx
  800424:	8d 4a 04             	lea    0x4(%edx),%ecx
  800427:	89 08                	mov    %ecx,(%eax)
  800429:	8b 02                	mov    (%edx),%eax
  80042b:	ba 00 00 00 00       	mov    $0x0,%edx
  800430:	eb 0e                	jmp    800440 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800432:	8b 10                	mov    (%eax),%edx
  800434:	8d 4a 04             	lea    0x4(%edx),%ecx
  800437:	89 08                	mov    %ecx,(%eax)
  800439:	8b 02                	mov    (%edx),%eax
  80043b:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800440:	5d                   	pop    %ebp
  800441:	c3                   	ret    

00800442 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800442:	55                   	push   %ebp
  800443:	89 e5                	mov    %esp,%ebp
  800445:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800448:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80044b:	8b 10                	mov    (%eax),%edx
  80044d:	3b 50 04             	cmp    0x4(%eax),%edx
  800450:	73 0a                	jae    80045c <sprintputch+0x1a>
		*b->buf++ = ch;
  800452:	8d 4a 01             	lea    0x1(%edx),%ecx
  800455:	89 08                	mov    %ecx,(%eax)
  800457:	8b 45 08             	mov    0x8(%ebp),%eax
  80045a:	88 02                	mov    %al,(%edx)
}
  80045c:	5d                   	pop    %ebp
  80045d:	c3                   	ret    

0080045e <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80045e:	55                   	push   %ebp
  80045f:	89 e5                	mov    %esp,%ebp
  800461:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800464:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800467:	50                   	push   %eax
  800468:	ff 75 10             	pushl  0x10(%ebp)
  80046b:	ff 75 0c             	pushl  0xc(%ebp)
  80046e:	ff 75 08             	pushl  0x8(%ebp)
  800471:	e8 05 00 00 00       	call   80047b <vprintfmt>
	va_end(ap);
}
  800476:	83 c4 10             	add    $0x10,%esp
  800479:	c9                   	leave  
  80047a:	c3                   	ret    

0080047b <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80047b:	55                   	push   %ebp
  80047c:	89 e5                	mov    %esp,%ebp
  80047e:	57                   	push   %edi
  80047f:	56                   	push   %esi
  800480:	53                   	push   %ebx
  800481:	83 ec 2c             	sub    $0x2c,%esp
  800484:	8b 75 08             	mov    0x8(%ebp),%esi
  800487:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80048a:	8b 7d 10             	mov    0x10(%ebp),%edi
  80048d:	eb 12                	jmp    8004a1 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80048f:	85 c0                	test   %eax,%eax
  800491:	0f 84 68 03 00 00    	je     8007ff <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800497:	83 ec 08             	sub    $0x8,%esp
  80049a:	53                   	push   %ebx
  80049b:	50                   	push   %eax
  80049c:	ff d6                	call   *%esi
  80049e:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004a1:	47                   	inc    %edi
  8004a2:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8004a6:	83 f8 25             	cmp    $0x25,%eax
  8004a9:	75 e4                	jne    80048f <vprintfmt+0x14>
  8004ab:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8004af:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8004b6:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8004bd:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8004c4:	ba 00 00 00 00       	mov    $0x0,%edx
  8004c9:	eb 07                	jmp    8004d2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004cb:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8004ce:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004d2:	8d 47 01             	lea    0x1(%edi),%eax
  8004d5:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8004d8:	0f b6 0f             	movzbl (%edi),%ecx
  8004db:	8a 07                	mov    (%edi),%al
  8004dd:	83 e8 23             	sub    $0x23,%eax
  8004e0:	3c 55                	cmp    $0x55,%al
  8004e2:	0f 87 fe 02 00 00    	ja     8007e6 <vprintfmt+0x36b>
  8004e8:	0f b6 c0             	movzbl %al,%eax
  8004eb:	ff 24 85 a0 24 80 00 	jmp    *0x8024a0(,%eax,4)
  8004f2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8004f5:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8004f9:	eb d7                	jmp    8004d2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004fb:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004fe:	b8 00 00 00 00       	mov    $0x0,%eax
  800503:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800506:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800509:	01 c0                	add    %eax,%eax
  80050b:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80050f:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800512:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800515:	83 fa 09             	cmp    $0x9,%edx
  800518:	77 34                	ja     80054e <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80051a:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80051b:	eb e9                	jmp    800506 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80051d:	8b 45 14             	mov    0x14(%ebp),%eax
  800520:	8d 48 04             	lea    0x4(%eax),%ecx
  800523:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800526:	8b 00                	mov    (%eax),%eax
  800528:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80052b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80052e:	eb 24                	jmp    800554 <vprintfmt+0xd9>
  800530:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800534:	79 07                	jns    80053d <vprintfmt+0xc2>
  800536:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80053d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800540:	eb 90                	jmp    8004d2 <vprintfmt+0x57>
  800542:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800545:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80054c:	eb 84                	jmp    8004d2 <vprintfmt+0x57>
  80054e:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800551:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800554:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800558:	0f 89 74 ff ff ff    	jns    8004d2 <vprintfmt+0x57>
				width = precision, precision = -1;
  80055e:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800561:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800564:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80056b:	e9 62 ff ff ff       	jmp    8004d2 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800570:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800571:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800574:	e9 59 ff ff ff       	jmp    8004d2 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800579:	8b 45 14             	mov    0x14(%ebp),%eax
  80057c:	8d 50 04             	lea    0x4(%eax),%edx
  80057f:	89 55 14             	mov    %edx,0x14(%ebp)
  800582:	83 ec 08             	sub    $0x8,%esp
  800585:	53                   	push   %ebx
  800586:	ff 30                	pushl  (%eax)
  800588:	ff d6                	call   *%esi
			break;
  80058a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80058d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800590:	e9 0c ff ff ff       	jmp    8004a1 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800595:	8b 45 14             	mov    0x14(%ebp),%eax
  800598:	8d 50 04             	lea    0x4(%eax),%edx
  80059b:	89 55 14             	mov    %edx,0x14(%ebp)
  80059e:	8b 00                	mov    (%eax),%eax
  8005a0:	85 c0                	test   %eax,%eax
  8005a2:	79 02                	jns    8005a6 <vprintfmt+0x12b>
  8005a4:	f7 d8                	neg    %eax
  8005a6:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8005a8:	83 f8 0f             	cmp    $0xf,%eax
  8005ab:	7f 0b                	jg     8005b8 <vprintfmt+0x13d>
  8005ad:	8b 04 85 00 26 80 00 	mov    0x802600(,%eax,4),%eax
  8005b4:	85 c0                	test   %eax,%eax
  8005b6:	75 18                	jne    8005d0 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8005b8:	52                   	push   %edx
  8005b9:	68 7b 23 80 00       	push   $0x80237b
  8005be:	53                   	push   %ebx
  8005bf:	56                   	push   %esi
  8005c0:	e8 99 fe ff ff       	call   80045e <printfmt>
  8005c5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c8:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8005cb:	e9 d1 fe ff ff       	jmp    8004a1 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8005d0:	50                   	push   %eax
  8005d1:	68 81 28 80 00       	push   $0x802881
  8005d6:	53                   	push   %ebx
  8005d7:	56                   	push   %esi
  8005d8:	e8 81 fe ff ff       	call   80045e <printfmt>
  8005dd:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005e3:	e9 b9 fe ff ff       	jmp    8004a1 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8005e8:	8b 45 14             	mov    0x14(%ebp),%eax
  8005eb:	8d 50 04             	lea    0x4(%eax),%edx
  8005ee:	89 55 14             	mov    %edx,0x14(%ebp)
  8005f1:	8b 38                	mov    (%eax),%edi
  8005f3:	85 ff                	test   %edi,%edi
  8005f5:	75 05                	jne    8005fc <vprintfmt+0x181>
				p = "(null)";
  8005f7:	bf 74 23 80 00       	mov    $0x802374,%edi
			if (width > 0 && padc != '-')
  8005fc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800600:	0f 8e 90 00 00 00    	jle    800696 <vprintfmt+0x21b>
  800606:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80060a:	0f 84 8e 00 00 00    	je     80069e <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800610:	83 ec 08             	sub    $0x8,%esp
  800613:	ff 75 d0             	pushl  -0x30(%ebp)
  800616:	57                   	push   %edi
  800617:	e8 70 02 00 00       	call   80088c <strnlen>
  80061c:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80061f:	29 c1                	sub    %eax,%ecx
  800621:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800624:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800627:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80062b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80062e:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800631:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800633:	eb 0d                	jmp    800642 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800635:	83 ec 08             	sub    $0x8,%esp
  800638:	53                   	push   %ebx
  800639:	ff 75 e4             	pushl  -0x1c(%ebp)
  80063c:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80063e:	4f                   	dec    %edi
  80063f:	83 c4 10             	add    $0x10,%esp
  800642:	85 ff                	test   %edi,%edi
  800644:	7f ef                	jg     800635 <vprintfmt+0x1ba>
  800646:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800649:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80064c:	89 c8                	mov    %ecx,%eax
  80064e:	85 c9                	test   %ecx,%ecx
  800650:	79 05                	jns    800657 <vprintfmt+0x1dc>
  800652:	b8 00 00 00 00       	mov    $0x0,%eax
  800657:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80065a:	29 c1                	sub    %eax,%ecx
  80065c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80065f:	89 75 08             	mov    %esi,0x8(%ebp)
  800662:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800665:	eb 3d                	jmp    8006a4 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800667:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80066b:	74 19                	je     800686 <vprintfmt+0x20b>
  80066d:	0f be c0             	movsbl %al,%eax
  800670:	83 e8 20             	sub    $0x20,%eax
  800673:	83 f8 5e             	cmp    $0x5e,%eax
  800676:	76 0e                	jbe    800686 <vprintfmt+0x20b>
					putch('?', putdat);
  800678:	83 ec 08             	sub    $0x8,%esp
  80067b:	53                   	push   %ebx
  80067c:	6a 3f                	push   $0x3f
  80067e:	ff 55 08             	call   *0x8(%ebp)
  800681:	83 c4 10             	add    $0x10,%esp
  800684:	eb 0b                	jmp    800691 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800686:	83 ec 08             	sub    $0x8,%esp
  800689:	53                   	push   %ebx
  80068a:	52                   	push   %edx
  80068b:	ff 55 08             	call   *0x8(%ebp)
  80068e:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800691:	ff 4d e4             	decl   -0x1c(%ebp)
  800694:	eb 0e                	jmp    8006a4 <vprintfmt+0x229>
  800696:	89 75 08             	mov    %esi,0x8(%ebp)
  800699:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80069c:	eb 06                	jmp    8006a4 <vprintfmt+0x229>
  80069e:	89 75 08             	mov    %esi,0x8(%ebp)
  8006a1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8006a4:	47                   	inc    %edi
  8006a5:	8a 47 ff             	mov    -0x1(%edi),%al
  8006a8:	0f be d0             	movsbl %al,%edx
  8006ab:	85 d2                	test   %edx,%edx
  8006ad:	74 1d                	je     8006cc <vprintfmt+0x251>
  8006af:	85 f6                	test   %esi,%esi
  8006b1:	78 b4                	js     800667 <vprintfmt+0x1ec>
  8006b3:	4e                   	dec    %esi
  8006b4:	79 b1                	jns    800667 <vprintfmt+0x1ec>
  8006b6:	8b 75 08             	mov    0x8(%ebp),%esi
  8006b9:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8006bc:	eb 14                	jmp    8006d2 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8006be:	83 ec 08             	sub    $0x8,%esp
  8006c1:	53                   	push   %ebx
  8006c2:	6a 20                	push   $0x20
  8006c4:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8006c6:	4f                   	dec    %edi
  8006c7:	83 c4 10             	add    $0x10,%esp
  8006ca:	eb 06                	jmp    8006d2 <vprintfmt+0x257>
  8006cc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8006cf:	8b 75 08             	mov    0x8(%ebp),%esi
  8006d2:	85 ff                	test   %edi,%edi
  8006d4:	7f e8                	jg     8006be <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006d6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006d9:	e9 c3 fd ff ff       	jmp    8004a1 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8006de:	83 fa 01             	cmp    $0x1,%edx
  8006e1:	7e 16                	jle    8006f9 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8006e3:	8b 45 14             	mov    0x14(%ebp),%eax
  8006e6:	8d 50 08             	lea    0x8(%eax),%edx
  8006e9:	89 55 14             	mov    %edx,0x14(%ebp)
  8006ec:	8b 50 04             	mov    0x4(%eax),%edx
  8006ef:	8b 00                	mov    (%eax),%eax
  8006f1:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8006f4:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8006f7:	eb 32                	jmp    80072b <vprintfmt+0x2b0>
	else if (lflag)
  8006f9:	85 d2                	test   %edx,%edx
  8006fb:	74 18                	je     800715 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8006fd:	8b 45 14             	mov    0x14(%ebp),%eax
  800700:	8d 50 04             	lea    0x4(%eax),%edx
  800703:	89 55 14             	mov    %edx,0x14(%ebp)
  800706:	8b 00                	mov    (%eax),%eax
  800708:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80070b:	89 c1                	mov    %eax,%ecx
  80070d:	c1 f9 1f             	sar    $0x1f,%ecx
  800710:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800713:	eb 16                	jmp    80072b <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800715:	8b 45 14             	mov    0x14(%ebp),%eax
  800718:	8d 50 04             	lea    0x4(%eax),%edx
  80071b:	89 55 14             	mov    %edx,0x14(%ebp)
  80071e:	8b 00                	mov    (%eax),%eax
  800720:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800723:	89 c1                	mov    %eax,%ecx
  800725:	c1 f9 1f             	sar    $0x1f,%ecx
  800728:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80072b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80072e:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800731:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800735:	79 76                	jns    8007ad <vprintfmt+0x332>
				putch('-', putdat);
  800737:	83 ec 08             	sub    $0x8,%esp
  80073a:	53                   	push   %ebx
  80073b:	6a 2d                	push   $0x2d
  80073d:	ff d6                	call   *%esi
				num = -(long long) num;
  80073f:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800742:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800745:	f7 d8                	neg    %eax
  800747:	83 d2 00             	adc    $0x0,%edx
  80074a:	f7 da                	neg    %edx
  80074c:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80074f:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800754:	eb 5c                	jmp    8007b2 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800756:	8d 45 14             	lea    0x14(%ebp),%eax
  800759:	e8 aa fc ff ff       	call   800408 <getuint>
			base = 10;
  80075e:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800763:	eb 4d                	jmp    8007b2 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800765:	8d 45 14             	lea    0x14(%ebp),%eax
  800768:	e8 9b fc ff ff       	call   800408 <getuint>
			base = 8;
  80076d:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800772:	eb 3e                	jmp    8007b2 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800774:	83 ec 08             	sub    $0x8,%esp
  800777:	53                   	push   %ebx
  800778:	6a 30                	push   $0x30
  80077a:	ff d6                	call   *%esi
			putch('x', putdat);
  80077c:	83 c4 08             	add    $0x8,%esp
  80077f:	53                   	push   %ebx
  800780:	6a 78                	push   $0x78
  800782:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800784:	8b 45 14             	mov    0x14(%ebp),%eax
  800787:	8d 50 04             	lea    0x4(%eax),%edx
  80078a:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80078d:	8b 00                	mov    (%eax),%eax
  80078f:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800794:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800797:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80079c:	eb 14                	jmp    8007b2 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80079e:	8d 45 14             	lea    0x14(%ebp),%eax
  8007a1:	e8 62 fc ff ff       	call   800408 <getuint>
			base = 16;
  8007a6:	b9 10 00 00 00       	mov    $0x10,%ecx
  8007ab:	eb 05                	jmp    8007b2 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8007ad:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8007b2:	83 ec 0c             	sub    $0xc,%esp
  8007b5:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8007b9:	57                   	push   %edi
  8007ba:	ff 75 e4             	pushl  -0x1c(%ebp)
  8007bd:	51                   	push   %ecx
  8007be:	52                   	push   %edx
  8007bf:	50                   	push   %eax
  8007c0:	89 da                	mov    %ebx,%edx
  8007c2:	89 f0                	mov    %esi,%eax
  8007c4:	e8 92 fb ff ff       	call   80035b <printnum>
			break;
  8007c9:	83 c4 20             	add    $0x20,%esp
  8007cc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007cf:	e9 cd fc ff ff       	jmp    8004a1 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8007d4:	83 ec 08             	sub    $0x8,%esp
  8007d7:	53                   	push   %ebx
  8007d8:	51                   	push   %ecx
  8007d9:	ff d6                	call   *%esi
			break;
  8007db:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007de:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8007e1:	e9 bb fc ff ff       	jmp    8004a1 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8007e6:	83 ec 08             	sub    $0x8,%esp
  8007e9:	53                   	push   %ebx
  8007ea:	6a 25                	push   $0x25
  8007ec:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8007ee:	83 c4 10             	add    $0x10,%esp
  8007f1:	eb 01                	jmp    8007f4 <vprintfmt+0x379>
  8007f3:	4f                   	dec    %edi
  8007f4:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8007f8:	75 f9                	jne    8007f3 <vprintfmt+0x378>
  8007fa:	e9 a2 fc ff ff       	jmp    8004a1 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8007ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800802:	5b                   	pop    %ebx
  800803:	5e                   	pop    %esi
  800804:	5f                   	pop    %edi
  800805:	5d                   	pop    %ebp
  800806:	c3                   	ret    

00800807 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800807:	55                   	push   %ebp
  800808:	89 e5                	mov    %esp,%ebp
  80080a:	83 ec 18             	sub    $0x18,%esp
  80080d:	8b 45 08             	mov    0x8(%ebp),%eax
  800810:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800813:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800816:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80081a:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80081d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800824:	85 c0                	test   %eax,%eax
  800826:	74 26                	je     80084e <vsnprintf+0x47>
  800828:	85 d2                	test   %edx,%edx
  80082a:	7e 29                	jle    800855 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80082c:	ff 75 14             	pushl  0x14(%ebp)
  80082f:	ff 75 10             	pushl  0x10(%ebp)
  800832:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800835:	50                   	push   %eax
  800836:	68 42 04 80 00       	push   $0x800442
  80083b:	e8 3b fc ff ff       	call   80047b <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800840:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800843:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800846:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800849:	83 c4 10             	add    $0x10,%esp
  80084c:	eb 0c                	jmp    80085a <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80084e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800853:	eb 05                	jmp    80085a <vsnprintf+0x53>
  800855:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80085a:	c9                   	leave  
  80085b:	c3                   	ret    

0080085c <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80085c:	55                   	push   %ebp
  80085d:	89 e5                	mov    %esp,%ebp
  80085f:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800862:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800865:	50                   	push   %eax
  800866:	ff 75 10             	pushl  0x10(%ebp)
  800869:	ff 75 0c             	pushl  0xc(%ebp)
  80086c:	ff 75 08             	pushl  0x8(%ebp)
  80086f:	e8 93 ff ff ff       	call   800807 <vsnprintf>
	va_end(ap);

	return rc;
}
  800874:	c9                   	leave  
  800875:	c3                   	ret    

00800876 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800876:	55                   	push   %ebp
  800877:	89 e5                	mov    %esp,%ebp
  800879:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80087c:	b8 00 00 00 00       	mov    $0x0,%eax
  800881:	eb 01                	jmp    800884 <strlen+0xe>
		n++;
  800883:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800884:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800888:	75 f9                	jne    800883 <strlen+0xd>
		n++;
	return n;
}
  80088a:	5d                   	pop    %ebp
  80088b:	c3                   	ret    

0080088c <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80088c:	55                   	push   %ebp
  80088d:	89 e5                	mov    %esp,%ebp
  80088f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800892:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800895:	ba 00 00 00 00       	mov    $0x0,%edx
  80089a:	eb 01                	jmp    80089d <strnlen+0x11>
		n++;
  80089c:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80089d:	39 c2                	cmp    %eax,%edx
  80089f:	74 08                	je     8008a9 <strnlen+0x1d>
  8008a1:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8008a5:	75 f5                	jne    80089c <strnlen+0x10>
  8008a7:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8008a9:	5d                   	pop    %ebp
  8008aa:	c3                   	ret    

008008ab <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8008ab:	55                   	push   %ebp
  8008ac:	89 e5                	mov    %esp,%ebp
  8008ae:	53                   	push   %ebx
  8008af:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8008b5:	89 c2                	mov    %eax,%edx
  8008b7:	42                   	inc    %edx
  8008b8:	41                   	inc    %ecx
  8008b9:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8008bc:	88 5a ff             	mov    %bl,-0x1(%edx)
  8008bf:	84 db                	test   %bl,%bl
  8008c1:	75 f4                	jne    8008b7 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8008c3:	5b                   	pop    %ebx
  8008c4:	5d                   	pop    %ebp
  8008c5:	c3                   	ret    

008008c6 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8008c6:	55                   	push   %ebp
  8008c7:	89 e5                	mov    %esp,%ebp
  8008c9:	53                   	push   %ebx
  8008ca:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8008cd:	53                   	push   %ebx
  8008ce:	e8 a3 ff ff ff       	call   800876 <strlen>
  8008d3:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8008d6:	ff 75 0c             	pushl  0xc(%ebp)
  8008d9:	01 d8                	add    %ebx,%eax
  8008db:	50                   	push   %eax
  8008dc:	e8 ca ff ff ff       	call   8008ab <strcpy>
	return dst;
}
  8008e1:	89 d8                	mov    %ebx,%eax
  8008e3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8008e6:	c9                   	leave  
  8008e7:	c3                   	ret    

008008e8 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8008e8:	55                   	push   %ebp
  8008e9:	89 e5                	mov    %esp,%ebp
  8008eb:	56                   	push   %esi
  8008ec:	53                   	push   %ebx
  8008ed:	8b 75 08             	mov    0x8(%ebp),%esi
  8008f0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8008f3:	89 f3                	mov    %esi,%ebx
  8008f5:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008f8:	89 f2                	mov    %esi,%edx
  8008fa:	eb 0c                	jmp    800908 <strncpy+0x20>
		*dst++ = *src;
  8008fc:	42                   	inc    %edx
  8008fd:	8a 01                	mov    (%ecx),%al
  8008ff:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800902:	80 39 01             	cmpb   $0x1,(%ecx)
  800905:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800908:	39 da                	cmp    %ebx,%edx
  80090a:	75 f0                	jne    8008fc <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80090c:	89 f0                	mov    %esi,%eax
  80090e:	5b                   	pop    %ebx
  80090f:	5e                   	pop    %esi
  800910:	5d                   	pop    %ebp
  800911:	c3                   	ret    

00800912 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800912:	55                   	push   %ebp
  800913:	89 e5                	mov    %esp,%ebp
  800915:	56                   	push   %esi
  800916:	53                   	push   %ebx
  800917:	8b 75 08             	mov    0x8(%ebp),%esi
  80091a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80091d:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800920:	85 c0                	test   %eax,%eax
  800922:	74 1e                	je     800942 <strlcpy+0x30>
  800924:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800928:	89 f2                	mov    %esi,%edx
  80092a:	eb 05                	jmp    800931 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80092c:	42                   	inc    %edx
  80092d:	41                   	inc    %ecx
  80092e:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800931:	39 c2                	cmp    %eax,%edx
  800933:	74 08                	je     80093d <strlcpy+0x2b>
  800935:	8a 19                	mov    (%ecx),%bl
  800937:	84 db                	test   %bl,%bl
  800939:	75 f1                	jne    80092c <strlcpy+0x1a>
  80093b:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80093d:	c6 00 00             	movb   $0x0,(%eax)
  800940:	eb 02                	jmp    800944 <strlcpy+0x32>
  800942:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800944:	29 f0                	sub    %esi,%eax
}
  800946:	5b                   	pop    %ebx
  800947:	5e                   	pop    %esi
  800948:	5d                   	pop    %ebp
  800949:	c3                   	ret    

0080094a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80094a:	55                   	push   %ebp
  80094b:	89 e5                	mov    %esp,%ebp
  80094d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800950:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800953:	eb 02                	jmp    800957 <strcmp+0xd>
		p++, q++;
  800955:	41                   	inc    %ecx
  800956:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800957:	8a 01                	mov    (%ecx),%al
  800959:	84 c0                	test   %al,%al
  80095b:	74 04                	je     800961 <strcmp+0x17>
  80095d:	3a 02                	cmp    (%edx),%al
  80095f:	74 f4                	je     800955 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800961:	0f b6 c0             	movzbl %al,%eax
  800964:	0f b6 12             	movzbl (%edx),%edx
  800967:	29 d0                	sub    %edx,%eax
}
  800969:	5d                   	pop    %ebp
  80096a:	c3                   	ret    

0080096b <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80096b:	55                   	push   %ebp
  80096c:	89 e5                	mov    %esp,%ebp
  80096e:	53                   	push   %ebx
  80096f:	8b 45 08             	mov    0x8(%ebp),%eax
  800972:	8b 55 0c             	mov    0xc(%ebp),%edx
  800975:	89 c3                	mov    %eax,%ebx
  800977:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80097a:	eb 02                	jmp    80097e <strncmp+0x13>
		n--, p++, q++;
  80097c:	40                   	inc    %eax
  80097d:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80097e:	39 d8                	cmp    %ebx,%eax
  800980:	74 14                	je     800996 <strncmp+0x2b>
  800982:	8a 08                	mov    (%eax),%cl
  800984:	84 c9                	test   %cl,%cl
  800986:	74 04                	je     80098c <strncmp+0x21>
  800988:	3a 0a                	cmp    (%edx),%cl
  80098a:	74 f0                	je     80097c <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  80098c:	0f b6 00             	movzbl (%eax),%eax
  80098f:	0f b6 12             	movzbl (%edx),%edx
  800992:	29 d0                	sub    %edx,%eax
  800994:	eb 05                	jmp    80099b <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800996:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80099b:	5b                   	pop    %ebx
  80099c:	5d                   	pop    %ebp
  80099d:	c3                   	ret    

0080099e <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80099e:	55                   	push   %ebp
  80099f:	89 e5                	mov    %esp,%ebp
  8009a1:	8b 45 08             	mov    0x8(%ebp),%eax
  8009a4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8009a7:	eb 05                	jmp    8009ae <strchr+0x10>
		if (*s == c)
  8009a9:	38 ca                	cmp    %cl,%dl
  8009ab:	74 0c                	je     8009b9 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8009ad:	40                   	inc    %eax
  8009ae:	8a 10                	mov    (%eax),%dl
  8009b0:	84 d2                	test   %dl,%dl
  8009b2:	75 f5                	jne    8009a9 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8009b4:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009b9:	5d                   	pop    %ebp
  8009ba:	c3                   	ret    

008009bb <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8009bb:	55                   	push   %ebp
  8009bc:	89 e5                	mov    %esp,%ebp
  8009be:	8b 45 08             	mov    0x8(%ebp),%eax
  8009c1:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8009c4:	eb 05                	jmp    8009cb <strfind+0x10>
		if (*s == c)
  8009c6:	38 ca                	cmp    %cl,%dl
  8009c8:	74 07                	je     8009d1 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8009ca:	40                   	inc    %eax
  8009cb:	8a 10                	mov    (%eax),%dl
  8009cd:	84 d2                	test   %dl,%dl
  8009cf:	75 f5                	jne    8009c6 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8009d1:	5d                   	pop    %ebp
  8009d2:	c3                   	ret    

008009d3 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8009d3:	55                   	push   %ebp
  8009d4:	89 e5                	mov    %esp,%ebp
  8009d6:	57                   	push   %edi
  8009d7:	56                   	push   %esi
  8009d8:	53                   	push   %ebx
  8009d9:	8b 7d 08             	mov    0x8(%ebp),%edi
  8009dc:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8009df:	85 c9                	test   %ecx,%ecx
  8009e1:	74 36                	je     800a19 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8009e3:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8009e9:	75 28                	jne    800a13 <memset+0x40>
  8009eb:	f6 c1 03             	test   $0x3,%cl
  8009ee:	75 23                	jne    800a13 <memset+0x40>
		c &= 0xFF;
  8009f0:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8009f4:	89 d3                	mov    %edx,%ebx
  8009f6:	c1 e3 08             	shl    $0x8,%ebx
  8009f9:	89 d6                	mov    %edx,%esi
  8009fb:	c1 e6 18             	shl    $0x18,%esi
  8009fe:	89 d0                	mov    %edx,%eax
  800a00:	c1 e0 10             	shl    $0x10,%eax
  800a03:	09 f0                	or     %esi,%eax
  800a05:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800a07:	89 d8                	mov    %ebx,%eax
  800a09:	09 d0                	or     %edx,%eax
  800a0b:	c1 e9 02             	shr    $0x2,%ecx
  800a0e:	fc                   	cld    
  800a0f:	f3 ab                	rep stos %eax,%es:(%edi)
  800a11:	eb 06                	jmp    800a19 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800a13:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a16:	fc                   	cld    
  800a17:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800a19:	89 f8                	mov    %edi,%eax
  800a1b:	5b                   	pop    %ebx
  800a1c:	5e                   	pop    %esi
  800a1d:	5f                   	pop    %edi
  800a1e:	5d                   	pop    %ebp
  800a1f:	c3                   	ret    

00800a20 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800a20:	55                   	push   %ebp
  800a21:	89 e5                	mov    %esp,%ebp
  800a23:	57                   	push   %edi
  800a24:	56                   	push   %esi
  800a25:	8b 45 08             	mov    0x8(%ebp),%eax
  800a28:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a2b:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800a2e:	39 c6                	cmp    %eax,%esi
  800a30:	73 33                	jae    800a65 <memmove+0x45>
  800a32:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800a35:	39 d0                	cmp    %edx,%eax
  800a37:	73 2c                	jae    800a65 <memmove+0x45>
		s += n;
		d += n;
  800a39:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a3c:	89 d6                	mov    %edx,%esi
  800a3e:	09 fe                	or     %edi,%esi
  800a40:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800a46:	75 13                	jne    800a5b <memmove+0x3b>
  800a48:	f6 c1 03             	test   $0x3,%cl
  800a4b:	75 0e                	jne    800a5b <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800a4d:	83 ef 04             	sub    $0x4,%edi
  800a50:	8d 72 fc             	lea    -0x4(%edx),%esi
  800a53:	c1 e9 02             	shr    $0x2,%ecx
  800a56:	fd                   	std    
  800a57:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a59:	eb 07                	jmp    800a62 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800a5b:	4f                   	dec    %edi
  800a5c:	8d 72 ff             	lea    -0x1(%edx),%esi
  800a5f:	fd                   	std    
  800a60:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800a62:	fc                   	cld    
  800a63:	eb 1d                	jmp    800a82 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800a65:	89 f2                	mov    %esi,%edx
  800a67:	09 c2                	or     %eax,%edx
  800a69:	f6 c2 03             	test   $0x3,%dl
  800a6c:	75 0f                	jne    800a7d <memmove+0x5d>
  800a6e:	f6 c1 03             	test   $0x3,%cl
  800a71:	75 0a                	jne    800a7d <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800a73:	c1 e9 02             	shr    $0x2,%ecx
  800a76:	89 c7                	mov    %eax,%edi
  800a78:	fc                   	cld    
  800a79:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800a7b:	eb 05                	jmp    800a82 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800a7d:	89 c7                	mov    %eax,%edi
  800a7f:	fc                   	cld    
  800a80:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800a82:	5e                   	pop    %esi
  800a83:	5f                   	pop    %edi
  800a84:	5d                   	pop    %ebp
  800a85:	c3                   	ret    

00800a86 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800a86:	55                   	push   %ebp
  800a87:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800a89:	ff 75 10             	pushl  0x10(%ebp)
  800a8c:	ff 75 0c             	pushl  0xc(%ebp)
  800a8f:	ff 75 08             	pushl  0x8(%ebp)
  800a92:	e8 89 ff ff ff       	call   800a20 <memmove>
}
  800a97:	c9                   	leave  
  800a98:	c3                   	ret    

00800a99 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800a99:	55                   	push   %ebp
  800a9a:	89 e5                	mov    %esp,%ebp
  800a9c:	56                   	push   %esi
  800a9d:	53                   	push   %ebx
  800a9e:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa1:	8b 55 0c             	mov    0xc(%ebp),%edx
  800aa4:	89 c6                	mov    %eax,%esi
  800aa6:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800aa9:	eb 14                	jmp    800abf <memcmp+0x26>
		if (*s1 != *s2)
  800aab:	8a 08                	mov    (%eax),%cl
  800aad:	8a 1a                	mov    (%edx),%bl
  800aaf:	38 d9                	cmp    %bl,%cl
  800ab1:	74 0a                	je     800abd <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800ab3:	0f b6 c1             	movzbl %cl,%eax
  800ab6:	0f b6 db             	movzbl %bl,%ebx
  800ab9:	29 d8                	sub    %ebx,%eax
  800abb:	eb 0b                	jmp    800ac8 <memcmp+0x2f>
		s1++, s2++;
  800abd:	40                   	inc    %eax
  800abe:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800abf:	39 f0                	cmp    %esi,%eax
  800ac1:	75 e8                	jne    800aab <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800ac3:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ac8:	5b                   	pop    %ebx
  800ac9:	5e                   	pop    %esi
  800aca:	5d                   	pop    %ebp
  800acb:	c3                   	ret    

00800acc <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800acc:	55                   	push   %ebp
  800acd:	89 e5                	mov    %esp,%ebp
  800acf:	53                   	push   %ebx
  800ad0:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800ad3:	89 c1                	mov    %eax,%ecx
  800ad5:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800ad8:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800adc:	eb 08                	jmp    800ae6 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800ade:	0f b6 10             	movzbl (%eax),%edx
  800ae1:	39 da                	cmp    %ebx,%edx
  800ae3:	74 05                	je     800aea <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ae5:	40                   	inc    %eax
  800ae6:	39 c8                	cmp    %ecx,%eax
  800ae8:	72 f4                	jb     800ade <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800aea:	5b                   	pop    %ebx
  800aeb:	5d                   	pop    %ebp
  800aec:	c3                   	ret    

00800aed <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800aed:	55                   	push   %ebp
  800aee:	89 e5                	mov    %esp,%ebp
  800af0:	57                   	push   %edi
  800af1:	56                   	push   %esi
  800af2:	53                   	push   %ebx
  800af3:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800af6:	eb 01                	jmp    800af9 <strtol+0xc>
		s++;
  800af8:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800af9:	8a 01                	mov    (%ecx),%al
  800afb:	3c 20                	cmp    $0x20,%al
  800afd:	74 f9                	je     800af8 <strtol+0xb>
  800aff:	3c 09                	cmp    $0x9,%al
  800b01:	74 f5                	je     800af8 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800b03:	3c 2b                	cmp    $0x2b,%al
  800b05:	75 08                	jne    800b0f <strtol+0x22>
		s++;
  800b07:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800b08:	bf 00 00 00 00       	mov    $0x0,%edi
  800b0d:	eb 11                	jmp    800b20 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800b0f:	3c 2d                	cmp    $0x2d,%al
  800b11:	75 08                	jne    800b1b <strtol+0x2e>
		s++, neg = 1;
  800b13:	41                   	inc    %ecx
  800b14:	bf 01 00 00 00       	mov    $0x1,%edi
  800b19:	eb 05                	jmp    800b20 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800b1b:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800b20:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b24:	0f 84 87 00 00 00    	je     800bb1 <strtol+0xc4>
  800b2a:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800b2e:	75 27                	jne    800b57 <strtol+0x6a>
  800b30:	80 39 30             	cmpb   $0x30,(%ecx)
  800b33:	75 22                	jne    800b57 <strtol+0x6a>
  800b35:	e9 88 00 00 00       	jmp    800bc2 <strtol+0xd5>
		s += 2, base = 16;
  800b3a:	83 c1 02             	add    $0x2,%ecx
  800b3d:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800b44:	eb 11                	jmp    800b57 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800b46:	41                   	inc    %ecx
  800b47:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800b4e:	eb 07                	jmp    800b57 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800b50:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800b57:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800b5c:	8a 11                	mov    (%ecx),%dl
  800b5e:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800b61:	80 fb 09             	cmp    $0x9,%bl
  800b64:	77 08                	ja     800b6e <strtol+0x81>
			dig = *s - '0';
  800b66:	0f be d2             	movsbl %dl,%edx
  800b69:	83 ea 30             	sub    $0x30,%edx
  800b6c:	eb 22                	jmp    800b90 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800b6e:	8d 72 9f             	lea    -0x61(%edx),%esi
  800b71:	89 f3                	mov    %esi,%ebx
  800b73:	80 fb 19             	cmp    $0x19,%bl
  800b76:	77 08                	ja     800b80 <strtol+0x93>
			dig = *s - 'a' + 10;
  800b78:	0f be d2             	movsbl %dl,%edx
  800b7b:	83 ea 57             	sub    $0x57,%edx
  800b7e:	eb 10                	jmp    800b90 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800b80:	8d 72 bf             	lea    -0x41(%edx),%esi
  800b83:	89 f3                	mov    %esi,%ebx
  800b85:	80 fb 19             	cmp    $0x19,%bl
  800b88:	77 14                	ja     800b9e <strtol+0xb1>
			dig = *s - 'A' + 10;
  800b8a:	0f be d2             	movsbl %dl,%edx
  800b8d:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800b90:	3b 55 10             	cmp    0x10(%ebp),%edx
  800b93:	7d 09                	jge    800b9e <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800b95:	41                   	inc    %ecx
  800b96:	0f af 45 10          	imul   0x10(%ebp),%eax
  800b9a:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800b9c:	eb be                	jmp    800b5c <strtol+0x6f>

	if (endptr)
  800b9e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ba2:	74 05                	je     800ba9 <strtol+0xbc>
		*endptr = (char *) s;
  800ba4:	8b 75 0c             	mov    0xc(%ebp),%esi
  800ba7:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ba9:	85 ff                	test   %edi,%edi
  800bab:	74 21                	je     800bce <strtol+0xe1>
  800bad:	f7 d8                	neg    %eax
  800baf:	eb 1d                	jmp    800bce <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bb1:	80 39 30             	cmpb   $0x30,(%ecx)
  800bb4:	75 9a                	jne    800b50 <strtol+0x63>
  800bb6:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800bba:	0f 84 7a ff ff ff    	je     800b3a <strtol+0x4d>
  800bc0:	eb 84                	jmp    800b46 <strtol+0x59>
  800bc2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800bc6:	0f 84 6e ff ff ff    	je     800b3a <strtol+0x4d>
  800bcc:	eb 89                	jmp    800b57 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800bce:	5b                   	pop    %ebx
  800bcf:	5e                   	pop    %esi
  800bd0:	5f                   	pop    %edi
  800bd1:	5d                   	pop    %ebp
  800bd2:	c3                   	ret    

00800bd3 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800bd3:	55                   	push   %ebp
  800bd4:	89 e5                	mov    %esp,%ebp
  800bd6:	57                   	push   %edi
  800bd7:	56                   	push   %esi
  800bd8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bd9:	b8 00 00 00 00       	mov    $0x0,%eax
  800bde:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800be1:	8b 55 08             	mov    0x8(%ebp),%edx
  800be4:	89 c3                	mov    %eax,%ebx
  800be6:	89 c7                	mov    %eax,%edi
  800be8:	89 c6                	mov    %eax,%esi
  800bea:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800bec:	5b                   	pop    %ebx
  800bed:	5e                   	pop    %esi
  800bee:	5f                   	pop    %edi
  800bef:	5d                   	pop    %ebp
  800bf0:	c3                   	ret    

00800bf1 <sys_cgetc>:

int
sys_cgetc(void)
{
  800bf1:	55                   	push   %ebp
  800bf2:	89 e5                	mov    %esp,%ebp
  800bf4:	57                   	push   %edi
  800bf5:	56                   	push   %esi
  800bf6:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bf7:	ba 00 00 00 00       	mov    $0x0,%edx
  800bfc:	b8 01 00 00 00       	mov    $0x1,%eax
  800c01:	89 d1                	mov    %edx,%ecx
  800c03:	89 d3                	mov    %edx,%ebx
  800c05:	89 d7                	mov    %edx,%edi
  800c07:	89 d6                	mov    %edx,%esi
  800c09:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800c0b:	5b                   	pop    %ebx
  800c0c:	5e                   	pop    %esi
  800c0d:	5f                   	pop    %edi
  800c0e:	5d                   	pop    %ebp
  800c0f:	c3                   	ret    

00800c10 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800c10:	55                   	push   %ebp
  800c11:	89 e5                	mov    %esp,%ebp
  800c13:	57                   	push   %edi
  800c14:	56                   	push   %esi
  800c15:	53                   	push   %ebx
  800c16:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c19:	b9 00 00 00 00       	mov    $0x0,%ecx
  800c1e:	b8 03 00 00 00       	mov    $0x3,%eax
  800c23:	8b 55 08             	mov    0x8(%ebp),%edx
  800c26:	89 cb                	mov    %ecx,%ebx
  800c28:	89 cf                	mov    %ecx,%edi
  800c2a:	89 ce                	mov    %ecx,%esi
  800c2c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c2e:	85 c0                	test   %eax,%eax
  800c30:	7e 17                	jle    800c49 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c32:	83 ec 0c             	sub    $0xc,%esp
  800c35:	50                   	push   %eax
  800c36:	6a 03                	push   $0x3
  800c38:	68 5f 26 80 00       	push   $0x80265f
  800c3d:	6a 23                	push   $0x23
  800c3f:	68 7c 26 80 00       	push   $0x80267c
  800c44:	e8 26 f6 ff ff       	call   80026f <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800c49:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c4c:	5b                   	pop    %ebx
  800c4d:	5e                   	pop    %esi
  800c4e:	5f                   	pop    %edi
  800c4f:	5d                   	pop    %ebp
  800c50:	c3                   	ret    

00800c51 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800c51:	55                   	push   %ebp
  800c52:	89 e5                	mov    %esp,%ebp
  800c54:	57                   	push   %edi
  800c55:	56                   	push   %esi
  800c56:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c57:	ba 00 00 00 00       	mov    $0x0,%edx
  800c5c:	b8 02 00 00 00       	mov    $0x2,%eax
  800c61:	89 d1                	mov    %edx,%ecx
  800c63:	89 d3                	mov    %edx,%ebx
  800c65:	89 d7                	mov    %edx,%edi
  800c67:	89 d6                	mov    %edx,%esi
  800c69:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800c6b:	5b                   	pop    %ebx
  800c6c:	5e                   	pop    %esi
  800c6d:	5f                   	pop    %edi
  800c6e:	5d                   	pop    %ebp
  800c6f:	c3                   	ret    

00800c70 <sys_yield>:

void
sys_yield(void)
{
  800c70:	55                   	push   %ebp
  800c71:	89 e5                	mov    %esp,%ebp
  800c73:	57                   	push   %edi
  800c74:	56                   	push   %esi
  800c75:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c76:	ba 00 00 00 00       	mov    $0x0,%edx
  800c7b:	b8 0b 00 00 00       	mov    $0xb,%eax
  800c80:	89 d1                	mov    %edx,%ecx
  800c82:	89 d3                	mov    %edx,%ebx
  800c84:	89 d7                	mov    %edx,%edi
  800c86:	89 d6                	mov    %edx,%esi
  800c88:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800c8a:	5b                   	pop    %ebx
  800c8b:	5e                   	pop    %esi
  800c8c:	5f                   	pop    %edi
  800c8d:	5d                   	pop    %ebp
  800c8e:	c3                   	ret    

00800c8f <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800c8f:	55                   	push   %ebp
  800c90:	89 e5                	mov    %esp,%ebp
  800c92:	57                   	push   %edi
  800c93:	56                   	push   %esi
  800c94:	53                   	push   %ebx
  800c95:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c98:	be 00 00 00 00       	mov    $0x0,%esi
  800c9d:	b8 04 00 00 00       	mov    $0x4,%eax
  800ca2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ca5:	8b 55 08             	mov    0x8(%ebp),%edx
  800ca8:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cab:	89 f7                	mov    %esi,%edi
  800cad:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800caf:	85 c0                	test   %eax,%eax
  800cb1:	7e 17                	jle    800cca <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cb3:	83 ec 0c             	sub    $0xc,%esp
  800cb6:	50                   	push   %eax
  800cb7:	6a 04                	push   $0x4
  800cb9:	68 5f 26 80 00       	push   $0x80265f
  800cbe:	6a 23                	push   $0x23
  800cc0:	68 7c 26 80 00       	push   $0x80267c
  800cc5:	e8 a5 f5 ff ff       	call   80026f <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800cca:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ccd:	5b                   	pop    %ebx
  800cce:	5e                   	pop    %esi
  800ccf:	5f                   	pop    %edi
  800cd0:	5d                   	pop    %ebp
  800cd1:	c3                   	ret    

00800cd2 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800cd2:	55                   	push   %ebp
  800cd3:	89 e5                	mov    %esp,%ebp
  800cd5:	57                   	push   %edi
  800cd6:	56                   	push   %esi
  800cd7:	53                   	push   %ebx
  800cd8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cdb:	b8 05 00 00 00       	mov    $0x5,%eax
  800ce0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ce3:	8b 55 08             	mov    0x8(%ebp),%edx
  800ce6:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800ce9:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cec:	8b 75 18             	mov    0x18(%ebp),%esi
  800cef:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cf1:	85 c0                	test   %eax,%eax
  800cf3:	7e 17                	jle    800d0c <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cf5:	83 ec 0c             	sub    $0xc,%esp
  800cf8:	50                   	push   %eax
  800cf9:	6a 05                	push   $0x5
  800cfb:	68 5f 26 80 00       	push   $0x80265f
  800d00:	6a 23                	push   $0x23
  800d02:	68 7c 26 80 00       	push   $0x80267c
  800d07:	e8 63 f5 ff ff       	call   80026f <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800d0c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d0f:	5b                   	pop    %ebx
  800d10:	5e                   	pop    %esi
  800d11:	5f                   	pop    %edi
  800d12:	5d                   	pop    %ebp
  800d13:	c3                   	ret    

00800d14 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800d14:	55                   	push   %ebp
  800d15:	89 e5                	mov    %esp,%ebp
  800d17:	57                   	push   %edi
  800d18:	56                   	push   %esi
  800d19:	53                   	push   %ebx
  800d1a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d1d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d22:	b8 06 00 00 00       	mov    $0x6,%eax
  800d27:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d2a:	8b 55 08             	mov    0x8(%ebp),%edx
  800d2d:	89 df                	mov    %ebx,%edi
  800d2f:	89 de                	mov    %ebx,%esi
  800d31:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d33:	85 c0                	test   %eax,%eax
  800d35:	7e 17                	jle    800d4e <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d37:	83 ec 0c             	sub    $0xc,%esp
  800d3a:	50                   	push   %eax
  800d3b:	6a 06                	push   $0x6
  800d3d:	68 5f 26 80 00       	push   $0x80265f
  800d42:	6a 23                	push   $0x23
  800d44:	68 7c 26 80 00       	push   $0x80267c
  800d49:	e8 21 f5 ff ff       	call   80026f <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800d4e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d51:	5b                   	pop    %ebx
  800d52:	5e                   	pop    %esi
  800d53:	5f                   	pop    %edi
  800d54:	5d                   	pop    %ebp
  800d55:	c3                   	ret    

00800d56 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800d56:	55                   	push   %ebp
  800d57:	89 e5                	mov    %esp,%ebp
  800d59:	57                   	push   %edi
  800d5a:	56                   	push   %esi
  800d5b:	53                   	push   %ebx
  800d5c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d5f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d64:	b8 08 00 00 00       	mov    $0x8,%eax
  800d69:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d6c:	8b 55 08             	mov    0x8(%ebp),%edx
  800d6f:	89 df                	mov    %ebx,%edi
  800d71:	89 de                	mov    %ebx,%esi
  800d73:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d75:	85 c0                	test   %eax,%eax
  800d77:	7e 17                	jle    800d90 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d79:	83 ec 0c             	sub    $0xc,%esp
  800d7c:	50                   	push   %eax
  800d7d:	6a 08                	push   $0x8
  800d7f:	68 5f 26 80 00       	push   $0x80265f
  800d84:	6a 23                	push   $0x23
  800d86:	68 7c 26 80 00       	push   $0x80267c
  800d8b:	e8 df f4 ff ff       	call   80026f <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800d90:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d93:	5b                   	pop    %ebx
  800d94:	5e                   	pop    %esi
  800d95:	5f                   	pop    %edi
  800d96:	5d                   	pop    %ebp
  800d97:	c3                   	ret    

00800d98 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800d98:	55                   	push   %ebp
  800d99:	89 e5                	mov    %esp,%ebp
  800d9b:	57                   	push   %edi
  800d9c:	56                   	push   %esi
  800d9d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d9e:	ba 00 00 00 00       	mov    $0x0,%edx
  800da3:	b8 0c 00 00 00       	mov    $0xc,%eax
  800da8:	89 d1                	mov    %edx,%ecx
  800daa:	89 d3                	mov    %edx,%ebx
  800dac:	89 d7                	mov    %edx,%edi
  800dae:	89 d6                	mov    %edx,%esi
  800db0:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800db2:	5b                   	pop    %ebx
  800db3:	5e                   	pop    %esi
  800db4:	5f                   	pop    %edi
  800db5:	5d                   	pop    %ebp
  800db6:	c3                   	ret    

00800db7 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800db7:	55                   	push   %ebp
  800db8:	89 e5                	mov    %esp,%ebp
  800dba:	57                   	push   %edi
  800dbb:	56                   	push   %esi
  800dbc:	53                   	push   %ebx
  800dbd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dc0:	bb 00 00 00 00       	mov    $0x0,%ebx
  800dc5:	b8 09 00 00 00       	mov    $0x9,%eax
  800dca:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800dcd:	8b 55 08             	mov    0x8(%ebp),%edx
  800dd0:	89 df                	mov    %ebx,%edi
  800dd2:	89 de                	mov    %ebx,%esi
  800dd4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800dd6:	85 c0                	test   %eax,%eax
  800dd8:	7e 17                	jle    800df1 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800dda:	83 ec 0c             	sub    $0xc,%esp
  800ddd:	50                   	push   %eax
  800dde:	6a 09                	push   $0x9
  800de0:	68 5f 26 80 00       	push   $0x80265f
  800de5:	6a 23                	push   $0x23
  800de7:	68 7c 26 80 00       	push   $0x80267c
  800dec:	e8 7e f4 ff ff       	call   80026f <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800df1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800df4:	5b                   	pop    %ebx
  800df5:	5e                   	pop    %esi
  800df6:	5f                   	pop    %edi
  800df7:	5d                   	pop    %ebp
  800df8:	c3                   	ret    

00800df9 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800df9:	55                   	push   %ebp
  800dfa:	89 e5                	mov    %esp,%ebp
  800dfc:	57                   	push   %edi
  800dfd:	56                   	push   %esi
  800dfe:	53                   	push   %ebx
  800dff:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e02:	bb 00 00 00 00       	mov    $0x0,%ebx
  800e07:	b8 0a 00 00 00       	mov    $0xa,%eax
  800e0c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e0f:	8b 55 08             	mov    0x8(%ebp),%edx
  800e12:	89 df                	mov    %ebx,%edi
  800e14:	89 de                	mov    %ebx,%esi
  800e16:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e18:	85 c0                	test   %eax,%eax
  800e1a:	7e 17                	jle    800e33 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e1c:	83 ec 0c             	sub    $0xc,%esp
  800e1f:	50                   	push   %eax
  800e20:	6a 0a                	push   $0xa
  800e22:	68 5f 26 80 00       	push   $0x80265f
  800e27:	6a 23                	push   $0x23
  800e29:	68 7c 26 80 00       	push   $0x80267c
  800e2e:	e8 3c f4 ff ff       	call   80026f <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800e33:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e36:	5b                   	pop    %ebx
  800e37:	5e                   	pop    %esi
  800e38:	5f                   	pop    %edi
  800e39:	5d                   	pop    %ebp
  800e3a:	c3                   	ret    

00800e3b <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800e3b:	55                   	push   %ebp
  800e3c:	89 e5                	mov    %esp,%ebp
  800e3e:	57                   	push   %edi
  800e3f:	56                   	push   %esi
  800e40:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e41:	be 00 00 00 00       	mov    $0x0,%esi
  800e46:	b8 0d 00 00 00       	mov    $0xd,%eax
  800e4b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800e4e:	8b 55 08             	mov    0x8(%ebp),%edx
  800e51:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800e54:	8b 7d 14             	mov    0x14(%ebp),%edi
  800e57:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800e59:	5b                   	pop    %ebx
  800e5a:	5e                   	pop    %esi
  800e5b:	5f                   	pop    %edi
  800e5c:	5d                   	pop    %ebp
  800e5d:	c3                   	ret    

00800e5e <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800e5e:	55                   	push   %ebp
  800e5f:	89 e5                	mov    %esp,%ebp
  800e61:	57                   	push   %edi
  800e62:	56                   	push   %esi
  800e63:	53                   	push   %ebx
  800e64:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e67:	b9 00 00 00 00       	mov    $0x0,%ecx
  800e6c:	b8 0e 00 00 00       	mov    $0xe,%eax
  800e71:	8b 55 08             	mov    0x8(%ebp),%edx
  800e74:	89 cb                	mov    %ecx,%ebx
  800e76:	89 cf                	mov    %ecx,%edi
  800e78:	89 ce                	mov    %ecx,%esi
  800e7a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800e7c:	85 c0                	test   %eax,%eax
  800e7e:	7e 17                	jle    800e97 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800e80:	83 ec 0c             	sub    $0xc,%esp
  800e83:	50                   	push   %eax
  800e84:	6a 0e                	push   $0xe
  800e86:	68 5f 26 80 00       	push   $0x80265f
  800e8b:	6a 23                	push   $0x23
  800e8d:	68 7c 26 80 00       	push   $0x80267c
  800e92:	e8 d8 f3 ff ff       	call   80026f <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800e97:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800e9a:	5b                   	pop    %ebx
  800e9b:	5e                   	pop    %esi
  800e9c:	5f                   	pop    %edi
  800e9d:	5d                   	pop    %ebp
  800e9e:	c3                   	ret    

00800e9f <pgfault>:
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
  800e9f:	55                   	push   %ebp
  800ea0:	89 e5                	mov    %esp,%ebp
  800ea2:	53                   	push   %ebx
  800ea3:	83 ec 04             	sub    $0x4,%esp
  800ea6:	8b 45 08             	mov    0x8(%ebp),%eax
	void *addr = (void *) utf->utf_fault_va;
  800ea9:	8b 18                	mov    (%eax),%ebx
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

    // check if access is write and to a copy-on-write page.
    pte_t pte = uvpt[PGNUM(addr)];
  800eab:	89 da                	mov    %ebx,%edx
  800ead:	c1 ea 0c             	shr    $0xc,%edx
  800eb0:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
    if (!(err & FEC_WR) || !(pte & PTE_COW))
  800eb7:	f6 40 04 02          	testb  $0x2,0x4(%eax)
  800ebb:	74 05                	je     800ec2 <pgfault+0x23>
  800ebd:	f6 c6 08             	test   $0x8,%dh
  800ec0:	75 14                	jne    800ed6 <pgfault+0x37>
        panic("pgfault: faulting access not write or not to a copy-on-write page");
  800ec2:	83 ec 04             	sub    $0x4,%esp
  800ec5:	68 8c 26 80 00       	push   $0x80268c
  800eca:	6a 26                	push   $0x26
  800ecc:	68 f0 26 80 00       	push   $0x8026f0
  800ed1:	e8 99 f3 ff ff       	call   80026f <_panic>
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	//sys_page_alloc(envid_t envid, void *va, int perm)
    if (sys_page_alloc(0, PFTEMP, PTE_W | PTE_U | PTE_P))
  800ed6:	83 ec 04             	sub    $0x4,%esp
  800ed9:	6a 07                	push   $0x7
  800edb:	68 00 f0 7f 00       	push   $0x7ff000
  800ee0:	6a 00                	push   $0x0
  800ee2:	e8 a8 fd ff ff       	call   800c8f <sys_page_alloc>
  800ee7:	83 c4 10             	add    $0x10,%esp
  800eea:	85 c0                	test   %eax,%eax
  800eec:	74 14                	je     800f02 <pgfault+0x63>
        panic("pgfault: no phys mem");
  800eee:	83 ec 04             	sub    $0x4,%esp
  800ef1:	68 fb 26 80 00       	push   $0x8026fb
  800ef6:	6a 32                	push   $0x32
  800ef8:	68 f0 26 80 00       	push   $0x8026f0
  800efd:	e8 6d f3 ff ff       	call   80026f <_panic>

    // copy data to the new page from the source page.
    void *fltpg_addr = (void *)ROUNDDOWN(addr, PGSIZE);
  800f02:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    memmove(PFTEMP, fltpg_addr, PGSIZE);
  800f08:	83 ec 04             	sub    $0x4,%esp
  800f0b:	68 00 10 00 00       	push   $0x1000
  800f10:	53                   	push   %ebx
  800f11:	68 00 f0 7f 00       	push   $0x7ff000
  800f16:	e8 05 fb ff ff       	call   800a20 <memmove>

    // change mapping for the faulting page.
    //sys_page_map(envid_t srcenvid, void *srcva, envid_t dstenvid, void *dstva, int perm)
    if (sys_page_map(0,
  800f1b:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  800f22:	53                   	push   %ebx
  800f23:	6a 00                	push   $0x0
  800f25:	68 00 f0 7f 00       	push   $0x7ff000
  800f2a:	6a 00                	push   $0x0
  800f2c:	e8 a1 fd ff ff       	call   800cd2 <sys_page_map>
  800f31:	83 c4 20             	add    $0x20,%esp
  800f34:	85 c0                	test   %eax,%eax
  800f36:	74 14                	je     800f4c <pgfault+0xad>
                     PFTEMP,
                     0,
                     fltpg_addr,
                     PTE_W | PTE_U | PTE_P))
        panic("pgfault: map error");
  800f38:	83 ec 04             	sub    $0x4,%esp
  800f3b:	68 10 27 80 00       	push   $0x802710
  800f40:	6a 3f                	push   $0x3f
  800f42:	68 f0 26 80 00       	push   $0x8026f0
  800f47:	e8 23 f3 ff ff       	call   80026f <_panic>


	//panic("pgfault not implemented");
}
  800f4c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800f4f:	c9                   	leave  
  800f50:	c3                   	ret    

00800f51 <fork>:
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
  800f51:	55                   	push   %ebp
  800f52:	89 e5                	mov    %esp,%ebp
  800f54:	57                   	push   %edi
  800f55:	56                   	push   %esi
  800f56:	53                   	push   %ebx
  800f57:	83 ec 28             	sub    $0x28,%esp
	// LAB 4: Your code here.
    // Step 1: install user mode pgfault handler.
    set_pgfault_handler(pgfault);
  800f5a:	68 9f 0e 80 00       	push   $0x800e9f
  800f5f:	e8 f5 0f 00 00       	call   801f59 <set_pgfault_handler>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800f64:	b8 07 00 00 00       	mov    $0x7,%eax
  800f69:	cd 30                	int    $0x30
  800f6b:	89 45 dc             	mov    %eax,-0x24(%ebp)
  800f6e:	89 45 e4             	mov    %eax,-0x1c(%ebp)

    // Step 2: create child environment.
    envid_t envid = sys_exofork();
    if (envid < 0) {
  800f71:	83 c4 10             	add    $0x10,%esp
  800f74:	85 c0                	test   %eax,%eax
  800f76:	79 17                	jns    800f8f <fork+0x3e>
        panic("fork: cannot create child env");
  800f78:	83 ec 04             	sub    $0x4,%esp
  800f7b:	68 23 27 80 00       	push   $0x802723
  800f80:	68 97 00 00 00       	push   $0x97
  800f85:	68 f0 26 80 00       	push   $0x8026f0
  800f8a:	e8 e0 f2 ff ff       	call   80026f <_panic>
    } else if (envid == 0) {
  800f8f:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800f93:	75 2a                	jne    800fbf <fork+0x6e>
        // child environment.
        thisenv = &envs[ENVX(sys_getenvid())];
  800f95:	e8 b7 fc ff ff       	call   800c51 <sys_getenvid>
  800f9a:	25 ff 03 00 00       	and    $0x3ff,%eax
  800f9f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800fa6:	c1 e0 07             	shl    $0x7,%eax
  800fa9:	29 d0                	sub    %edx,%eax
  800fab:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800fb0:	a3 04 40 80 00       	mov    %eax,0x804004
        return 0;
  800fb5:	b8 00 00 00 00       	mov    $0x0,%eax
  800fba:	e9 94 01 00 00       	jmp    801153 <fork+0x202>
  800fbf:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  800fc6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800fc9:	8b 04 bd 00 d0 7b ef 	mov    -0x10843000(,%edi,4),%eax
  800fd0:	a8 01                	test   $0x1,%al
  800fd2:	0f 84 0a 01 00 00    	je     8010e2 <fork+0x191>
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
            unsigned pn = (ipd << 10) | ipt;
  800fd8:	c1 e7 0a             	shl    $0xa,%edi
  800fdb:	be 00 00 00 00       	mov    $0x0,%esi
  800fe0:	89 fb                	mov    %edi,%ebx
  800fe2:	09 f3                	or     %esi,%ebx
            if (pn == PGNUM(UXSTACKTOP - PGSIZE)) {
  800fe4:	81 fb ff eb 0e 00    	cmp    $0xeebff,%ebx
  800fea:	75 34                	jne    801020 <fork+0xcf>
                // allocate a new page for child to hold the exception stack.
                if (sys_page_alloc(envid,
  800fec:	83 ec 04             	sub    $0x4,%esp
  800fef:	6a 07                	push   $0x7
  800ff1:	68 00 f0 bf ee       	push   $0xeebff000
  800ff6:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ff9:	e8 91 fc ff ff       	call   800c8f <sys_page_alloc>
  800ffe:	83 c4 10             	add    $0x10,%esp
  801001:	85 c0                	test   %eax,%eax
  801003:	0f 84 cc 00 00 00    	je     8010d5 <fork+0x184>
                                   (void *)(UXSTACKTOP - PGSIZE), 
                                   PTE_W | PTE_U | PTE_P))
                    panic("fork: no phys mem for xstk");
  801009:	83 ec 04             	sub    $0x4,%esp
  80100c:	68 41 27 80 00       	push   $0x802741
  801011:	68 ad 00 00 00       	push   $0xad
  801016:	68 f0 26 80 00       	push   $0x8026f0
  80101b:	e8 4f f2 ff ff       	call   80026f <_panic>

                continue;
            }

            if (uvpt[pn] & PTE_P)
  801020:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
  801027:	a8 01                	test   $0x1,%al
  801029:	0f 84 a6 00 00 00    	je     8010d5 <fork+0x184>
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
    pte_t pte = uvpt[pn];
  80102f:	8b 04 9d 00 00 40 ef 	mov    -0x10c00000(,%ebx,4),%eax
    void *va = (void *)(pn << PGSHIFT);
  801036:	c1 e3 0c             	shl    $0xc,%ebx
    // If the page is writable or copy-on-write,
    // the mapping must be copy-on-write ,
    // otherwise the new environment could change this page.
    //sys_page_map(envid_t srcenvid, void *srcva,
	//     envid_t dstenvid, void *dstva, int perm)
    if (((pte & PTE_W) || (pte & PTE_COW)) && !(perm & PTE_SHARE) ) {
  801039:	a9 02 08 00 00       	test   $0x802,%eax
  80103e:	74 62                	je     8010a2 <fork+0x151>
  801040:	f6 c4 04             	test   $0x4,%ah
  801043:	75 78                	jne    8010bd <fork+0x16c>
        if (sys_page_map(0,
  801045:	83 ec 0c             	sub    $0xc,%esp
  801048:	68 05 08 00 00       	push   $0x805
  80104d:	53                   	push   %ebx
  80104e:	ff 75 e4             	pushl  -0x1c(%ebp)
  801051:	53                   	push   %ebx
  801052:	6a 00                	push   $0x0
  801054:	e8 79 fc ff ff       	call   800cd2 <sys_page_map>
  801059:	83 c4 20             	add    $0x20,%esp
  80105c:	85 c0                	test   %eax,%eax
  80105e:	74 14                	je     801074 <fork+0x123>
                         va,
                         envid,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: map cow error");
  801060:	83 ec 04             	sub    $0x4,%esp
  801063:	68 5c 27 80 00       	push   $0x80275c
  801068:	6a 64                	push   $0x64
  80106a:	68 f0 26 80 00       	push   $0x8026f0
  80106f:	e8 fb f1 ff ff       	call   80026f <_panic>
        
        // Change permission of the page in this environment to copy-on-write.
        // Otherwise the new environment would see the change in this environment.
        if (sys_page_map(0,
  801074:	83 ec 0c             	sub    $0xc,%esp
  801077:	68 05 08 00 00       	push   $0x805
  80107c:	53                   	push   %ebx
  80107d:	6a 00                	push   $0x0
  80107f:	53                   	push   %ebx
  801080:	6a 00                	push   $0x0
  801082:	e8 4b fc ff ff       	call   800cd2 <sys_page_map>
  801087:	83 c4 20             	add    $0x20,%esp
  80108a:	85 c0                	test   %eax,%eax
  80108c:	74 47                	je     8010d5 <fork+0x184>
                         va,
                         0,
                         va,
                         PTE_COW | PTE_U | PTE_P))
            panic("duppage: change perm error");
  80108e:	83 ec 04             	sub    $0x4,%esp
  801091:	68 73 27 80 00       	push   $0x802773
  801096:	6a 6d                	push   $0x6d
  801098:	68 f0 26 80 00       	push   $0x8026f0
  80109d:	e8 cd f1 ff ff       	call   80026f <_panic>

        return 0;
    } else if (!(perm & PTE_SHARE) ){ //read only
  8010a2:	f6 c4 04             	test   $0x4,%ah
  8010a5:	75 16                	jne    8010bd <fork+0x16c>
        //if(sys_page_map(0, va, envid, va, PTE_U | PTE_P)){
        //    panic("duppage: map ro error");
        //}
        return sys_page_map(0, va, envid, va, PTE_U | PTE_P);
  8010a7:	83 ec 0c             	sub    $0xc,%esp
  8010aa:	6a 05                	push   $0x5
  8010ac:	53                   	push   %ebx
  8010ad:	ff 75 e4             	pushl  -0x1c(%ebp)
  8010b0:	53                   	push   %ebx
  8010b1:	6a 00                	push   $0x0
  8010b3:	e8 1a fc ff ff       	call   800cd2 <sys_page_map>
  8010b8:	83 c4 20             	add    $0x20,%esp
  8010bb:	eb 18                	jmp    8010d5 <fork+0x184>
    }else{ //shared page
        return sys_page_map(0, va, envid, va, perm);
  8010bd:	83 ec 0c             	sub    $0xc,%esp
  8010c0:	25 07 0e 00 00       	and    $0xe07,%eax
  8010c5:	50                   	push   %eax
  8010c6:	53                   	push   %ebx
  8010c7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8010ca:	53                   	push   %ebx
  8010cb:	6a 00                	push   $0x0
  8010cd:	e8 00 fc ff ff       	call   800cd2 <sys_page_map>
  8010d2:	83 c4 20             	add    $0x20,%esp
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  8010d5:	46                   	inc    %esi
  8010d6:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  8010dc:	0f 85 fe fe ff ff    	jne    800fe0 <fork+0x8f>
        return 0;
    }

    // Step 3: duplicate pages.
    int ipd;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  8010e2:	ff 45 e0             	incl   -0x20(%ebp)
  8010e5:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8010e8:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  8010ed:	0f 85 d3 fe ff ff    	jne    800fc6 <fork+0x75>
                duppage(envid, pn);
        }
    }

    // Step 4: set user page fault entry for child.
    if (sys_env_set_pgfault_upcall(envid, thisenv->env_pgfault_upcall))
  8010f3:	a1 04 40 80 00       	mov    0x804004,%eax
  8010f8:	8b 40 64             	mov    0x64(%eax),%eax
  8010fb:	83 ec 08             	sub    $0x8,%esp
  8010fe:	50                   	push   %eax
  8010ff:	ff 75 dc             	pushl  -0x24(%ebp)
  801102:	e8 f2 fc ff ff       	call   800df9 <sys_env_set_pgfault_upcall>
  801107:	83 c4 10             	add    $0x10,%esp
  80110a:	85 c0                	test   %eax,%eax
  80110c:	74 17                	je     801125 <fork+0x1d4>
        panic("fork: cannot set pgfault upcall");
  80110e:	83 ec 04             	sub    $0x4,%esp
  801111:	68 d0 26 80 00       	push   $0x8026d0
  801116:	68 b9 00 00 00       	push   $0xb9
  80111b:	68 f0 26 80 00       	push   $0x8026f0
  801120:	e8 4a f1 ff ff       	call   80026f <_panic>

    // Step 5: set child status to ENV_RUNNABLE.
    if (sys_env_set_status(envid, ENV_RUNNABLE))
  801125:	83 ec 08             	sub    $0x8,%esp
  801128:	6a 02                	push   $0x2
  80112a:	ff 75 dc             	pushl  -0x24(%ebp)
  80112d:	e8 24 fc ff ff       	call   800d56 <sys_env_set_status>
  801132:	83 c4 10             	add    $0x10,%esp
  801135:	85 c0                	test   %eax,%eax
  801137:	74 17                	je     801150 <fork+0x1ff>
        panic("fork: cannot set env status");
  801139:	83 ec 04             	sub    $0x4,%esp
  80113c:	68 8e 27 80 00       	push   $0x80278e
  801141:	68 bd 00 00 00       	push   $0xbd
  801146:	68 f0 26 80 00       	push   $0x8026f0
  80114b:	e8 1f f1 ff ff       	call   80026f <_panic>

    return envid;
  801150:	8b 45 dc             	mov    -0x24(%ebp),%eax
	
	//panic("fork not implemented");
}
  801153:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801156:	5b                   	pop    %ebx
  801157:	5e                   	pop    %esi
  801158:	5f                   	pop    %edi
  801159:	5d                   	pop    %ebp
  80115a:	c3                   	ret    

0080115b <sfork>:

// Challenge!
int
sfork(void)
{
  80115b:	55                   	push   %ebp
  80115c:	89 e5                	mov    %esp,%ebp
  80115e:	83 ec 0c             	sub    $0xc,%esp
	panic("sfork not implemented");
  801161:	68 aa 27 80 00       	push   $0x8027aa
  801166:	68 c8 00 00 00       	push   $0xc8
  80116b:	68 f0 26 80 00       	push   $0x8026f0
  801170:	e8 fa f0 ff ff       	call   80026f <_panic>

00801175 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801175:	55                   	push   %ebp
  801176:	89 e5                	mov    %esp,%ebp
  801178:	56                   	push   %esi
  801179:	53                   	push   %ebx
  80117a:	8b 75 08             	mov    0x8(%ebp),%esi
  80117d:	8b 45 0c             	mov    0xc(%ebp),%eax
  801180:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801183:	85 c0                	test   %eax,%eax
  801185:	75 05                	jne    80118c <ipc_recv+0x17>
        pg = (void *)UTOP;
  801187:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  80118c:	83 ec 0c             	sub    $0xc,%esp
  80118f:	50                   	push   %eax
  801190:	e8 c9 fc ff ff       	call   800e5e <sys_ipc_recv>
  801195:	83 c4 10             	add    $0x10,%esp
  801198:	85 c0                	test   %eax,%eax
  80119a:	74 16                	je     8011b2 <ipc_recv+0x3d>
        if (from_env_store)
  80119c:	85 f6                	test   %esi,%esi
  80119e:	74 06                	je     8011a6 <ipc_recv+0x31>
            *from_env_store = 0;
  8011a0:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  8011a6:	85 db                	test   %ebx,%ebx
  8011a8:	74 2c                	je     8011d6 <ipc_recv+0x61>
            *perm_store = 0;
  8011aa:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8011b0:	eb 24                	jmp    8011d6 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  8011b2:	85 f6                	test   %esi,%esi
  8011b4:	74 0a                	je     8011c0 <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  8011b6:	a1 04 40 80 00       	mov    0x804004,%eax
  8011bb:	8b 40 74             	mov    0x74(%eax),%eax
  8011be:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  8011c0:	85 db                	test   %ebx,%ebx
  8011c2:	74 0a                	je     8011ce <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  8011c4:	a1 04 40 80 00       	mov    0x804004,%eax
  8011c9:	8b 40 78             	mov    0x78(%eax),%eax
  8011cc:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  8011ce:	a1 04 40 80 00       	mov    0x804004,%eax
  8011d3:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  8011d6:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8011d9:	5b                   	pop    %ebx
  8011da:	5e                   	pop    %esi
  8011db:	5d                   	pop    %ebp
  8011dc:	c3                   	ret    

008011dd <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  8011dd:	55                   	push   %ebp
  8011de:	89 e5                	mov    %esp,%ebp
  8011e0:	57                   	push   %edi
  8011e1:	56                   	push   %esi
  8011e2:	53                   	push   %ebx
  8011e3:	83 ec 0c             	sub    $0xc,%esp
  8011e6:	8b 75 0c             	mov    0xc(%ebp),%esi
  8011e9:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8011ec:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  8011ef:	85 db                	test   %ebx,%ebx
  8011f1:	75 0c                	jne    8011ff <ipc_send+0x22>
        pg = (void *)UTOP;
  8011f3:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  8011f8:	eb 05                	jmp    8011ff <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  8011fa:	e8 71 fa ff ff       	call   800c70 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  8011ff:	57                   	push   %edi
  801200:	53                   	push   %ebx
  801201:	56                   	push   %esi
  801202:	ff 75 08             	pushl  0x8(%ebp)
  801205:	e8 31 fc ff ff       	call   800e3b <sys_ipc_try_send>
  80120a:	83 c4 10             	add    $0x10,%esp
  80120d:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801210:	74 e8                	je     8011fa <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801212:	85 c0                	test   %eax,%eax
  801214:	74 14                	je     80122a <ipc_send+0x4d>
        panic("ipc_send: error");
  801216:	83 ec 04             	sub    $0x4,%esp
  801219:	68 c0 27 80 00       	push   $0x8027c0
  80121e:	6a 6a                	push   $0x6a
  801220:	68 d0 27 80 00       	push   $0x8027d0
  801225:	e8 45 f0 ff ff       	call   80026f <_panic>
    }
	//panic("ipc_send not implemented");
}
  80122a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80122d:	5b                   	pop    %ebx
  80122e:	5e                   	pop    %esi
  80122f:	5f                   	pop    %edi
  801230:	5d                   	pop    %ebp
  801231:	c3                   	ret    

00801232 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801232:	55                   	push   %ebp
  801233:	89 e5                	mov    %esp,%ebp
  801235:	53                   	push   %ebx
  801236:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801239:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  80123e:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801245:	89 d0                	mov    %edx,%eax
  801247:	c1 e0 07             	shl    $0x7,%eax
  80124a:	29 d8                	sub    %ebx,%eax
  80124c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801251:	8b 40 50             	mov    0x50(%eax),%eax
  801254:	39 c8                	cmp    %ecx,%eax
  801256:	75 0d                	jne    801265 <ipc_find_env+0x33>
			return envs[i].env_id;
  801258:	c1 e2 07             	shl    $0x7,%edx
  80125b:	29 da                	sub    %ebx,%edx
  80125d:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801263:	eb 0e                	jmp    801273 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801265:	42                   	inc    %edx
  801266:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  80126c:	75 d0                	jne    80123e <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  80126e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801273:	5b                   	pop    %ebx
  801274:	5d                   	pop    %ebp
  801275:	c3                   	ret    

00801276 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  801276:	55                   	push   %ebp
  801277:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  801279:	8b 45 08             	mov    0x8(%ebp),%eax
  80127c:	05 00 00 00 30       	add    $0x30000000,%eax
  801281:	c1 e8 0c             	shr    $0xc,%eax
}
  801284:	5d                   	pop    %ebp
  801285:	c3                   	ret    

00801286 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  801286:	55                   	push   %ebp
  801287:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  801289:	8b 45 08             	mov    0x8(%ebp),%eax
  80128c:	05 00 00 00 30       	add    $0x30000000,%eax
  801291:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  801296:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  80129b:	5d                   	pop    %ebp
  80129c:	c3                   	ret    

0080129d <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  80129d:	55                   	push   %ebp
  80129e:	89 e5                	mov    %esp,%ebp
  8012a0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8012a3:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  8012a8:	89 c2                	mov    %eax,%edx
  8012aa:	c1 ea 16             	shr    $0x16,%edx
  8012ad:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  8012b4:	f6 c2 01             	test   $0x1,%dl
  8012b7:	74 11                	je     8012ca <fd_alloc+0x2d>
  8012b9:	89 c2                	mov    %eax,%edx
  8012bb:	c1 ea 0c             	shr    $0xc,%edx
  8012be:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  8012c5:	f6 c2 01             	test   $0x1,%dl
  8012c8:	75 09                	jne    8012d3 <fd_alloc+0x36>
			*fd_store = fd;
  8012ca:	89 01                	mov    %eax,(%ecx)
			return 0;
  8012cc:	b8 00 00 00 00       	mov    $0x0,%eax
  8012d1:	eb 17                	jmp    8012ea <fd_alloc+0x4d>
  8012d3:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  8012d8:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  8012dd:	75 c9                	jne    8012a8 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  8012df:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  8012e5:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  8012ea:	5d                   	pop    %ebp
  8012eb:	c3                   	ret    

008012ec <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  8012ec:	55                   	push   %ebp
  8012ed:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  8012ef:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  8012f3:	77 39                	ja     80132e <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  8012f5:	8b 45 08             	mov    0x8(%ebp),%eax
  8012f8:	c1 e0 0c             	shl    $0xc,%eax
  8012fb:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  801300:	89 c2                	mov    %eax,%edx
  801302:	c1 ea 16             	shr    $0x16,%edx
  801305:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  80130c:	f6 c2 01             	test   $0x1,%dl
  80130f:	74 24                	je     801335 <fd_lookup+0x49>
  801311:	89 c2                	mov    %eax,%edx
  801313:	c1 ea 0c             	shr    $0xc,%edx
  801316:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80131d:	f6 c2 01             	test   $0x1,%dl
  801320:	74 1a                	je     80133c <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  801322:	8b 55 0c             	mov    0xc(%ebp),%edx
  801325:	89 02                	mov    %eax,(%edx)
	return 0;
  801327:	b8 00 00 00 00       	mov    $0x0,%eax
  80132c:	eb 13                	jmp    801341 <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  80132e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801333:	eb 0c                	jmp    801341 <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  801335:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80133a:	eb 05                	jmp    801341 <fd_lookup+0x55>
  80133c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  801341:	5d                   	pop    %ebp
  801342:	c3                   	ret    

00801343 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  801343:	55                   	push   %ebp
  801344:	89 e5                	mov    %esp,%ebp
  801346:	83 ec 08             	sub    $0x8,%esp
  801349:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80134c:	ba 58 28 80 00       	mov    $0x802858,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  801351:	eb 13                	jmp    801366 <dev_lookup+0x23>
  801353:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  801356:	39 08                	cmp    %ecx,(%eax)
  801358:	75 0c                	jne    801366 <dev_lookup+0x23>
			*dev = devtab[i];
  80135a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80135d:	89 01                	mov    %eax,(%ecx)
			return 0;
  80135f:	b8 00 00 00 00       	mov    $0x0,%eax
  801364:	eb 2e                	jmp    801394 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  801366:	8b 02                	mov    (%edx),%eax
  801368:	85 c0                	test   %eax,%eax
  80136a:	75 e7                	jne    801353 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  80136c:	a1 04 40 80 00       	mov    0x804004,%eax
  801371:	8b 40 48             	mov    0x48(%eax),%eax
  801374:	83 ec 04             	sub    $0x4,%esp
  801377:	51                   	push   %ecx
  801378:	50                   	push   %eax
  801379:	68 dc 27 80 00       	push   $0x8027dc
  80137e:	e8 c4 ef ff ff       	call   800347 <cprintf>
	*dev = 0;
  801383:	8b 45 0c             	mov    0xc(%ebp),%eax
  801386:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  80138c:	83 c4 10             	add    $0x10,%esp
  80138f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801394:	c9                   	leave  
  801395:	c3                   	ret    

00801396 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  801396:	55                   	push   %ebp
  801397:	89 e5                	mov    %esp,%ebp
  801399:	56                   	push   %esi
  80139a:	53                   	push   %ebx
  80139b:	83 ec 10             	sub    $0x10,%esp
  80139e:	8b 75 08             	mov    0x8(%ebp),%esi
  8013a1:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  8013a4:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8013a7:	50                   	push   %eax
  8013a8:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  8013ae:	c1 e8 0c             	shr    $0xc,%eax
  8013b1:	50                   	push   %eax
  8013b2:	e8 35 ff ff ff       	call   8012ec <fd_lookup>
  8013b7:	83 c4 08             	add    $0x8,%esp
  8013ba:	85 c0                	test   %eax,%eax
  8013bc:	78 05                	js     8013c3 <fd_close+0x2d>
	    || fd != fd2)
  8013be:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  8013c1:	74 06                	je     8013c9 <fd_close+0x33>
		return (must_exist ? r : 0);
  8013c3:	84 db                	test   %bl,%bl
  8013c5:	74 47                	je     80140e <fd_close+0x78>
  8013c7:	eb 4a                	jmp    801413 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  8013c9:	83 ec 08             	sub    $0x8,%esp
  8013cc:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8013cf:	50                   	push   %eax
  8013d0:	ff 36                	pushl  (%esi)
  8013d2:	e8 6c ff ff ff       	call   801343 <dev_lookup>
  8013d7:	89 c3                	mov    %eax,%ebx
  8013d9:	83 c4 10             	add    $0x10,%esp
  8013dc:	85 c0                	test   %eax,%eax
  8013de:	78 1c                	js     8013fc <fd_close+0x66>
		if (dev->dev_close)
  8013e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8013e3:	8b 40 10             	mov    0x10(%eax),%eax
  8013e6:	85 c0                	test   %eax,%eax
  8013e8:	74 0d                	je     8013f7 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  8013ea:	83 ec 0c             	sub    $0xc,%esp
  8013ed:	56                   	push   %esi
  8013ee:	ff d0                	call   *%eax
  8013f0:	89 c3                	mov    %eax,%ebx
  8013f2:	83 c4 10             	add    $0x10,%esp
  8013f5:	eb 05                	jmp    8013fc <fd_close+0x66>
		else
			r = 0;
  8013f7:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  8013fc:	83 ec 08             	sub    $0x8,%esp
  8013ff:	56                   	push   %esi
  801400:	6a 00                	push   $0x0
  801402:	e8 0d f9 ff ff       	call   800d14 <sys_page_unmap>
	return r;
  801407:	83 c4 10             	add    $0x10,%esp
  80140a:	89 d8                	mov    %ebx,%eax
  80140c:	eb 05                	jmp    801413 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  80140e:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801413:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801416:	5b                   	pop    %ebx
  801417:	5e                   	pop    %esi
  801418:	5d                   	pop    %ebp
  801419:	c3                   	ret    

0080141a <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  80141a:	55                   	push   %ebp
  80141b:	89 e5                	mov    %esp,%ebp
  80141d:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801420:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801423:	50                   	push   %eax
  801424:	ff 75 08             	pushl  0x8(%ebp)
  801427:	e8 c0 fe ff ff       	call   8012ec <fd_lookup>
  80142c:	83 c4 08             	add    $0x8,%esp
  80142f:	85 c0                	test   %eax,%eax
  801431:	78 10                	js     801443 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  801433:	83 ec 08             	sub    $0x8,%esp
  801436:	6a 01                	push   $0x1
  801438:	ff 75 f4             	pushl  -0xc(%ebp)
  80143b:	e8 56 ff ff ff       	call   801396 <fd_close>
  801440:	83 c4 10             	add    $0x10,%esp
}
  801443:	c9                   	leave  
  801444:	c3                   	ret    

00801445 <close_all>:

void
close_all(void)
{
  801445:	55                   	push   %ebp
  801446:	89 e5                	mov    %esp,%ebp
  801448:	53                   	push   %ebx
  801449:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  80144c:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  801451:	83 ec 0c             	sub    $0xc,%esp
  801454:	53                   	push   %ebx
  801455:	e8 c0 ff ff ff       	call   80141a <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  80145a:	43                   	inc    %ebx
  80145b:	83 c4 10             	add    $0x10,%esp
  80145e:	83 fb 20             	cmp    $0x20,%ebx
  801461:	75 ee                	jne    801451 <close_all+0xc>
		close(i);
}
  801463:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801466:	c9                   	leave  
  801467:	c3                   	ret    

00801468 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  801468:	55                   	push   %ebp
  801469:	89 e5                	mov    %esp,%ebp
  80146b:	57                   	push   %edi
  80146c:	56                   	push   %esi
  80146d:	53                   	push   %ebx
  80146e:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  801471:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  801474:	50                   	push   %eax
  801475:	ff 75 08             	pushl  0x8(%ebp)
  801478:	e8 6f fe ff ff       	call   8012ec <fd_lookup>
  80147d:	83 c4 08             	add    $0x8,%esp
  801480:	85 c0                	test   %eax,%eax
  801482:	0f 88 c2 00 00 00    	js     80154a <dup+0xe2>
		return r;
	close(newfdnum);
  801488:	83 ec 0c             	sub    $0xc,%esp
  80148b:	ff 75 0c             	pushl  0xc(%ebp)
  80148e:	e8 87 ff ff ff       	call   80141a <close>

	newfd = INDEX2FD(newfdnum);
  801493:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801496:	c1 e3 0c             	shl    $0xc,%ebx
  801499:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  80149f:	83 c4 04             	add    $0x4,%esp
  8014a2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8014a5:	e8 dc fd ff ff       	call   801286 <fd2data>
  8014aa:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  8014ac:	89 1c 24             	mov    %ebx,(%esp)
  8014af:	e8 d2 fd ff ff       	call   801286 <fd2data>
  8014b4:	83 c4 10             	add    $0x10,%esp
  8014b7:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  8014b9:	89 f0                	mov    %esi,%eax
  8014bb:	c1 e8 16             	shr    $0x16,%eax
  8014be:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8014c5:	a8 01                	test   $0x1,%al
  8014c7:	74 35                	je     8014fe <dup+0x96>
  8014c9:	89 f0                	mov    %esi,%eax
  8014cb:	c1 e8 0c             	shr    $0xc,%eax
  8014ce:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  8014d5:	f6 c2 01             	test   $0x1,%dl
  8014d8:	74 24                	je     8014fe <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  8014da:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  8014e1:	83 ec 0c             	sub    $0xc,%esp
  8014e4:	25 07 0e 00 00       	and    $0xe07,%eax
  8014e9:	50                   	push   %eax
  8014ea:	57                   	push   %edi
  8014eb:	6a 00                	push   $0x0
  8014ed:	56                   	push   %esi
  8014ee:	6a 00                	push   $0x0
  8014f0:	e8 dd f7 ff ff       	call   800cd2 <sys_page_map>
  8014f5:	89 c6                	mov    %eax,%esi
  8014f7:	83 c4 20             	add    $0x20,%esp
  8014fa:	85 c0                	test   %eax,%eax
  8014fc:	78 2c                	js     80152a <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  8014fe:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  801501:	89 d0                	mov    %edx,%eax
  801503:	c1 e8 0c             	shr    $0xc,%eax
  801506:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80150d:	83 ec 0c             	sub    $0xc,%esp
  801510:	25 07 0e 00 00       	and    $0xe07,%eax
  801515:	50                   	push   %eax
  801516:	53                   	push   %ebx
  801517:	6a 00                	push   $0x0
  801519:	52                   	push   %edx
  80151a:	6a 00                	push   $0x0
  80151c:	e8 b1 f7 ff ff       	call   800cd2 <sys_page_map>
  801521:	89 c6                	mov    %eax,%esi
  801523:	83 c4 20             	add    $0x20,%esp
  801526:	85 c0                	test   %eax,%eax
  801528:	79 1d                	jns    801547 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  80152a:	83 ec 08             	sub    $0x8,%esp
  80152d:	53                   	push   %ebx
  80152e:	6a 00                	push   $0x0
  801530:	e8 df f7 ff ff       	call   800d14 <sys_page_unmap>
	sys_page_unmap(0, nva);
  801535:	83 c4 08             	add    $0x8,%esp
  801538:	57                   	push   %edi
  801539:	6a 00                	push   $0x0
  80153b:	e8 d4 f7 ff ff       	call   800d14 <sys_page_unmap>
	return r;
  801540:	83 c4 10             	add    $0x10,%esp
  801543:	89 f0                	mov    %esi,%eax
  801545:	eb 03                	jmp    80154a <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801547:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  80154a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80154d:	5b                   	pop    %ebx
  80154e:	5e                   	pop    %esi
  80154f:	5f                   	pop    %edi
  801550:	5d                   	pop    %ebp
  801551:	c3                   	ret    

00801552 <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  801552:	55                   	push   %ebp
  801553:	89 e5                	mov    %esp,%ebp
  801555:	53                   	push   %ebx
  801556:	83 ec 14             	sub    $0x14,%esp
  801559:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80155c:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80155f:	50                   	push   %eax
  801560:	53                   	push   %ebx
  801561:	e8 86 fd ff ff       	call   8012ec <fd_lookup>
  801566:	83 c4 08             	add    $0x8,%esp
  801569:	85 c0                	test   %eax,%eax
  80156b:	78 67                	js     8015d4 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80156d:	83 ec 08             	sub    $0x8,%esp
  801570:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801573:	50                   	push   %eax
  801574:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801577:	ff 30                	pushl  (%eax)
  801579:	e8 c5 fd ff ff       	call   801343 <dev_lookup>
  80157e:	83 c4 10             	add    $0x10,%esp
  801581:	85 c0                	test   %eax,%eax
  801583:	78 4f                	js     8015d4 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801585:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801588:	8b 42 08             	mov    0x8(%edx),%eax
  80158b:	83 e0 03             	and    $0x3,%eax
  80158e:	83 f8 01             	cmp    $0x1,%eax
  801591:	75 21                	jne    8015b4 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801593:	a1 04 40 80 00       	mov    0x804004,%eax
  801598:	8b 40 48             	mov    0x48(%eax),%eax
  80159b:	83 ec 04             	sub    $0x4,%esp
  80159e:	53                   	push   %ebx
  80159f:	50                   	push   %eax
  8015a0:	68 1d 28 80 00       	push   $0x80281d
  8015a5:	e8 9d ed ff ff       	call   800347 <cprintf>
		return -E_INVAL;
  8015aa:	83 c4 10             	add    $0x10,%esp
  8015ad:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8015b2:	eb 20                	jmp    8015d4 <read+0x82>
	}
	if (!dev->dev_read)
  8015b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8015b7:	8b 40 08             	mov    0x8(%eax),%eax
  8015ba:	85 c0                	test   %eax,%eax
  8015bc:	74 11                	je     8015cf <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  8015be:	83 ec 04             	sub    $0x4,%esp
  8015c1:	ff 75 10             	pushl  0x10(%ebp)
  8015c4:	ff 75 0c             	pushl  0xc(%ebp)
  8015c7:	52                   	push   %edx
  8015c8:	ff d0                	call   *%eax
  8015ca:	83 c4 10             	add    $0x10,%esp
  8015cd:	eb 05                	jmp    8015d4 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  8015cf:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  8015d4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015d7:	c9                   	leave  
  8015d8:	c3                   	ret    

008015d9 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  8015d9:	55                   	push   %ebp
  8015da:	89 e5                	mov    %esp,%ebp
  8015dc:	57                   	push   %edi
  8015dd:	56                   	push   %esi
  8015de:	53                   	push   %ebx
  8015df:	83 ec 0c             	sub    $0xc,%esp
  8015e2:	8b 7d 08             	mov    0x8(%ebp),%edi
  8015e5:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  8015e8:	bb 00 00 00 00       	mov    $0x0,%ebx
  8015ed:	eb 21                	jmp    801610 <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  8015ef:	83 ec 04             	sub    $0x4,%esp
  8015f2:	89 f0                	mov    %esi,%eax
  8015f4:	29 d8                	sub    %ebx,%eax
  8015f6:	50                   	push   %eax
  8015f7:	89 d8                	mov    %ebx,%eax
  8015f9:	03 45 0c             	add    0xc(%ebp),%eax
  8015fc:	50                   	push   %eax
  8015fd:	57                   	push   %edi
  8015fe:	e8 4f ff ff ff       	call   801552 <read>
		if (m < 0)
  801603:	83 c4 10             	add    $0x10,%esp
  801606:	85 c0                	test   %eax,%eax
  801608:	78 10                	js     80161a <readn+0x41>
			return m;
		if (m == 0)
  80160a:	85 c0                	test   %eax,%eax
  80160c:	74 0a                	je     801618 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80160e:	01 c3                	add    %eax,%ebx
  801610:	39 f3                	cmp    %esi,%ebx
  801612:	72 db                	jb     8015ef <readn+0x16>
  801614:	89 d8                	mov    %ebx,%eax
  801616:	eb 02                	jmp    80161a <readn+0x41>
  801618:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  80161a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80161d:	5b                   	pop    %ebx
  80161e:	5e                   	pop    %esi
  80161f:	5f                   	pop    %edi
  801620:	5d                   	pop    %ebp
  801621:	c3                   	ret    

00801622 <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  801622:	55                   	push   %ebp
  801623:	89 e5                	mov    %esp,%ebp
  801625:	53                   	push   %ebx
  801626:	83 ec 14             	sub    $0x14,%esp
  801629:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  80162c:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80162f:	50                   	push   %eax
  801630:	53                   	push   %ebx
  801631:	e8 b6 fc ff ff       	call   8012ec <fd_lookup>
  801636:	83 c4 08             	add    $0x8,%esp
  801639:	85 c0                	test   %eax,%eax
  80163b:	78 62                	js     80169f <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  80163d:	83 ec 08             	sub    $0x8,%esp
  801640:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801643:	50                   	push   %eax
  801644:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801647:	ff 30                	pushl  (%eax)
  801649:	e8 f5 fc ff ff       	call   801343 <dev_lookup>
  80164e:	83 c4 10             	add    $0x10,%esp
  801651:	85 c0                	test   %eax,%eax
  801653:	78 4a                	js     80169f <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801655:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801658:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  80165c:	75 21                	jne    80167f <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  80165e:	a1 04 40 80 00       	mov    0x804004,%eax
  801663:	8b 40 48             	mov    0x48(%eax),%eax
  801666:	83 ec 04             	sub    $0x4,%esp
  801669:	53                   	push   %ebx
  80166a:	50                   	push   %eax
  80166b:	68 39 28 80 00       	push   $0x802839
  801670:	e8 d2 ec ff ff       	call   800347 <cprintf>
		return -E_INVAL;
  801675:	83 c4 10             	add    $0x10,%esp
  801678:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80167d:	eb 20                	jmp    80169f <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  80167f:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801682:	8b 52 0c             	mov    0xc(%edx),%edx
  801685:	85 d2                	test   %edx,%edx
  801687:	74 11                	je     80169a <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801689:	83 ec 04             	sub    $0x4,%esp
  80168c:	ff 75 10             	pushl  0x10(%ebp)
  80168f:	ff 75 0c             	pushl  0xc(%ebp)
  801692:	50                   	push   %eax
  801693:	ff d2                	call   *%edx
  801695:	83 c4 10             	add    $0x10,%esp
  801698:	eb 05                	jmp    80169f <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  80169a:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  80169f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8016a2:	c9                   	leave  
  8016a3:	c3                   	ret    

008016a4 <seek>:

int
seek(int fdnum, off_t offset)
{
  8016a4:	55                   	push   %ebp
  8016a5:	89 e5                	mov    %esp,%ebp
  8016a7:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8016aa:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8016ad:	50                   	push   %eax
  8016ae:	ff 75 08             	pushl  0x8(%ebp)
  8016b1:	e8 36 fc ff ff       	call   8012ec <fd_lookup>
  8016b6:	83 c4 08             	add    $0x8,%esp
  8016b9:	85 c0                	test   %eax,%eax
  8016bb:	78 0e                	js     8016cb <seek+0x27>
		return r;
	fd->fd_offset = offset;
  8016bd:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8016c0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8016c3:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  8016c6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8016cb:	c9                   	leave  
  8016cc:	c3                   	ret    

008016cd <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  8016cd:	55                   	push   %ebp
  8016ce:	89 e5                	mov    %esp,%ebp
  8016d0:	53                   	push   %ebx
  8016d1:	83 ec 14             	sub    $0x14,%esp
  8016d4:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  8016d7:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8016da:	50                   	push   %eax
  8016db:	53                   	push   %ebx
  8016dc:	e8 0b fc ff ff       	call   8012ec <fd_lookup>
  8016e1:	83 c4 08             	add    $0x8,%esp
  8016e4:	85 c0                	test   %eax,%eax
  8016e6:	78 5f                	js     801747 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8016e8:	83 ec 08             	sub    $0x8,%esp
  8016eb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8016ee:	50                   	push   %eax
  8016ef:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8016f2:	ff 30                	pushl  (%eax)
  8016f4:	e8 4a fc ff ff       	call   801343 <dev_lookup>
  8016f9:	83 c4 10             	add    $0x10,%esp
  8016fc:	85 c0                	test   %eax,%eax
  8016fe:	78 47                	js     801747 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  801700:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801703:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801707:	75 21                	jne    80172a <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801709:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80170e:	8b 40 48             	mov    0x48(%eax),%eax
  801711:	83 ec 04             	sub    $0x4,%esp
  801714:	53                   	push   %ebx
  801715:	50                   	push   %eax
  801716:	68 fc 27 80 00       	push   $0x8027fc
  80171b:	e8 27 ec ff ff       	call   800347 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  801720:	83 c4 10             	add    $0x10,%esp
  801723:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801728:	eb 1d                	jmp    801747 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  80172a:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80172d:	8b 52 18             	mov    0x18(%edx),%edx
  801730:	85 d2                	test   %edx,%edx
  801732:	74 0e                	je     801742 <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  801734:	83 ec 08             	sub    $0x8,%esp
  801737:	ff 75 0c             	pushl  0xc(%ebp)
  80173a:	50                   	push   %eax
  80173b:	ff d2                	call   *%edx
  80173d:	83 c4 10             	add    $0x10,%esp
  801740:	eb 05                	jmp    801747 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  801742:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  801747:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80174a:	c9                   	leave  
  80174b:	c3                   	ret    

0080174c <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  80174c:	55                   	push   %ebp
  80174d:	89 e5                	mov    %esp,%ebp
  80174f:	53                   	push   %ebx
  801750:	83 ec 14             	sub    $0x14,%esp
  801753:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801756:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801759:	50                   	push   %eax
  80175a:	ff 75 08             	pushl  0x8(%ebp)
  80175d:	e8 8a fb ff ff       	call   8012ec <fd_lookup>
  801762:	83 c4 08             	add    $0x8,%esp
  801765:	85 c0                	test   %eax,%eax
  801767:	78 52                	js     8017bb <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801769:	83 ec 08             	sub    $0x8,%esp
  80176c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80176f:	50                   	push   %eax
  801770:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801773:	ff 30                	pushl  (%eax)
  801775:	e8 c9 fb ff ff       	call   801343 <dev_lookup>
  80177a:	83 c4 10             	add    $0x10,%esp
  80177d:	85 c0                	test   %eax,%eax
  80177f:	78 3a                	js     8017bb <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  801781:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801784:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  801788:	74 2c                	je     8017b6 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  80178a:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  80178d:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801794:	00 00 00 
	stat->st_isdir = 0;
  801797:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  80179e:	00 00 00 
	stat->st_dev = dev;
  8017a1:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8017a7:	83 ec 08             	sub    $0x8,%esp
  8017aa:	53                   	push   %ebx
  8017ab:	ff 75 f0             	pushl  -0x10(%ebp)
  8017ae:	ff 50 14             	call   *0x14(%eax)
  8017b1:	83 c4 10             	add    $0x10,%esp
  8017b4:	eb 05                	jmp    8017bb <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  8017b6:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  8017bb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017be:	c9                   	leave  
  8017bf:	c3                   	ret    

008017c0 <stat>:

int
stat(const char *path, struct Stat *stat)
{
  8017c0:	55                   	push   %ebp
  8017c1:	89 e5                	mov    %esp,%ebp
  8017c3:	56                   	push   %esi
  8017c4:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  8017c5:	83 ec 08             	sub    $0x8,%esp
  8017c8:	6a 00                	push   $0x0
  8017ca:	ff 75 08             	pushl  0x8(%ebp)
  8017cd:	e8 e7 01 00 00       	call   8019b9 <open>
  8017d2:	89 c3                	mov    %eax,%ebx
  8017d4:	83 c4 10             	add    $0x10,%esp
  8017d7:	85 c0                	test   %eax,%eax
  8017d9:	78 1d                	js     8017f8 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  8017db:	83 ec 08             	sub    $0x8,%esp
  8017de:	ff 75 0c             	pushl  0xc(%ebp)
  8017e1:	50                   	push   %eax
  8017e2:	e8 65 ff ff ff       	call   80174c <fstat>
  8017e7:	89 c6                	mov    %eax,%esi
	close(fd);
  8017e9:	89 1c 24             	mov    %ebx,(%esp)
  8017ec:	e8 29 fc ff ff       	call   80141a <close>
	return r;
  8017f1:	83 c4 10             	add    $0x10,%esp
  8017f4:	89 f0                	mov    %esi,%eax
  8017f6:	eb 00                	jmp    8017f8 <stat+0x38>
}
  8017f8:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8017fb:	5b                   	pop    %ebx
  8017fc:	5e                   	pop    %esi
  8017fd:	5d                   	pop    %ebp
  8017fe:	c3                   	ret    

008017ff <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  8017ff:	55                   	push   %ebp
  801800:	89 e5                	mov    %esp,%ebp
  801802:	56                   	push   %esi
  801803:	53                   	push   %ebx
  801804:	89 c6                	mov    %eax,%esi
  801806:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801808:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  80180f:	75 12                	jne    801823 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  801811:	83 ec 0c             	sub    $0xc,%esp
  801814:	6a 01                	push   $0x1
  801816:	e8 17 fa ff ff       	call   801232 <ipc_find_env>
  80181b:	a3 00 40 80 00       	mov    %eax,0x804000
  801820:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  801823:	6a 07                	push   $0x7
  801825:	68 00 50 80 00       	push   $0x805000
  80182a:	56                   	push   %esi
  80182b:	ff 35 00 40 80 00    	pushl  0x804000
  801831:	e8 a7 f9 ff ff       	call   8011dd <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  801836:	83 c4 0c             	add    $0xc,%esp
  801839:	6a 00                	push   $0x0
  80183b:	53                   	push   %ebx
  80183c:	6a 00                	push   $0x0
  80183e:	e8 32 f9 ff ff       	call   801175 <ipc_recv>
}
  801843:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801846:	5b                   	pop    %ebx
  801847:	5e                   	pop    %esi
  801848:	5d                   	pop    %ebp
  801849:	c3                   	ret    

0080184a <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  80184a:	55                   	push   %ebp
  80184b:	89 e5                	mov    %esp,%ebp
  80184d:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  801850:	8b 45 08             	mov    0x8(%ebp),%eax
  801853:	8b 40 0c             	mov    0xc(%eax),%eax
  801856:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  80185b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80185e:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  801863:	ba 00 00 00 00       	mov    $0x0,%edx
  801868:	b8 02 00 00 00       	mov    $0x2,%eax
  80186d:	e8 8d ff ff ff       	call   8017ff <fsipc>
}
  801872:	c9                   	leave  
  801873:	c3                   	ret    

00801874 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  801874:	55                   	push   %ebp
  801875:	89 e5                	mov    %esp,%ebp
  801877:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  80187a:	8b 45 08             	mov    0x8(%ebp),%eax
  80187d:	8b 40 0c             	mov    0xc(%eax),%eax
  801880:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  801885:	ba 00 00 00 00       	mov    $0x0,%edx
  80188a:	b8 06 00 00 00       	mov    $0x6,%eax
  80188f:	e8 6b ff ff ff       	call   8017ff <fsipc>
}
  801894:	c9                   	leave  
  801895:	c3                   	ret    

00801896 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801896:	55                   	push   %ebp
  801897:	89 e5                	mov    %esp,%ebp
  801899:	53                   	push   %ebx
  80189a:	83 ec 04             	sub    $0x4,%esp
  80189d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8018a0:	8b 45 08             	mov    0x8(%ebp),%eax
  8018a3:	8b 40 0c             	mov    0xc(%eax),%eax
  8018a6:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8018ab:	ba 00 00 00 00       	mov    $0x0,%edx
  8018b0:	b8 05 00 00 00       	mov    $0x5,%eax
  8018b5:	e8 45 ff ff ff       	call   8017ff <fsipc>
  8018ba:	85 c0                	test   %eax,%eax
  8018bc:	78 2c                	js     8018ea <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  8018be:	83 ec 08             	sub    $0x8,%esp
  8018c1:	68 00 50 80 00       	push   $0x805000
  8018c6:	53                   	push   %ebx
  8018c7:	e8 df ef ff ff       	call   8008ab <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  8018cc:	a1 80 50 80 00       	mov    0x805080,%eax
  8018d1:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  8018d7:	a1 84 50 80 00       	mov    0x805084,%eax
  8018dc:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  8018e2:	83 c4 10             	add    $0x10,%esp
  8018e5:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8018ea:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8018ed:	c9                   	leave  
  8018ee:	c3                   	ret    

008018ef <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  8018ef:	55                   	push   %ebp
  8018f0:	89 e5                	mov    %esp,%ebp
  8018f2:	83 ec 08             	sub    $0x8,%esp
  8018f5:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  8018f8:	8b 55 08             	mov    0x8(%ebp),%edx
  8018fb:	8b 52 0c             	mov    0xc(%edx),%edx
  8018fe:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801904:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801909:	76 05                	jbe    801910 <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  80190b:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  801910:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801915:	83 ec 04             	sub    $0x4,%esp
  801918:	50                   	push   %eax
  801919:	ff 75 0c             	pushl  0xc(%ebp)
  80191c:	68 08 50 80 00       	push   $0x805008
  801921:	e8 fa f0 ff ff       	call   800a20 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801926:	ba 00 00 00 00       	mov    $0x0,%edx
  80192b:	b8 04 00 00 00       	mov    $0x4,%eax
  801930:	e8 ca fe ff ff       	call   8017ff <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801935:	c9                   	leave  
  801936:	c3                   	ret    

00801937 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801937:	55                   	push   %ebp
  801938:	89 e5                	mov    %esp,%ebp
  80193a:	56                   	push   %esi
  80193b:	53                   	push   %ebx
  80193c:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  80193f:	8b 45 08             	mov    0x8(%ebp),%eax
  801942:	8b 40 0c             	mov    0xc(%eax),%eax
  801945:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  80194a:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  801950:	ba 00 00 00 00       	mov    $0x0,%edx
  801955:	b8 03 00 00 00       	mov    $0x3,%eax
  80195a:	e8 a0 fe ff ff       	call   8017ff <fsipc>
  80195f:	89 c3                	mov    %eax,%ebx
  801961:	85 c0                	test   %eax,%eax
  801963:	78 4b                	js     8019b0 <devfile_read+0x79>
		return r;
	assert(r <= n);
  801965:	39 c6                	cmp    %eax,%esi
  801967:	73 16                	jae    80197f <devfile_read+0x48>
  801969:	68 68 28 80 00       	push   $0x802868
  80196e:	68 6f 28 80 00       	push   $0x80286f
  801973:	6a 7c                	push   $0x7c
  801975:	68 84 28 80 00       	push   $0x802884
  80197a:	e8 f0 e8 ff ff       	call   80026f <_panic>
	assert(r <= PGSIZE);
  80197f:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801984:	7e 16                	jle    80199c <devfile_read+0x65>
  801986:	68 8f 28 80 00       	push   $0x80288f
  80198b:	68 6f 28 80 00       	push   $0x80286f
  801990:	6a 7d                	push   $0x7d
  801992:	68 84 28 80 00       	push   $0x802884
  801997:	e8 d3 e8 ff ff       	call   80026f <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  80199c:	83 ec 04             	sub    $0x4,%esp
  80199f:	50                   	push   %eax
  8019a0:	68 00 50 80 00       	push   $0x805000
  8019a5:	ff 75 0c             	pushl  0xc(%ebp)
  8019a8:	e8 73 f0 ff ff       	call   800a20 <memmove>
	return r;
  8019ad:	83 c4 10             	add    $0x10,%esp
}
  8019b0:	89 d8                	mov    %ebx,%eax
  8019b2:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8019b5:	5b                   	pop    %ebx
  8019b6:	5e                   	pop    %esi
  8019b7:	5d                   	pop    %ebp
  8019b8:	c3                   	ret    

008019b9 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  8019b9:	55                   	push   %ebp
  8019ba:	89 e5                	mov    %esp,%ebp
  8019bc:	53                   	push   %ebx
  8019bd:	83 ec 20             	sub    $0x20,%esp
  8019c0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  8019c3:	53                   	push   %ebx
  8019c4:	e8 ad ee ff ff       	call   800876 <strlen>
  8019c9:	83 c4 10             	add    $0x10,%esp
  8019cc:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  8019d1:	7f 63                	jg     801a36 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  8019d3:	83 ec 0c             	sub    $0xc,%esp
  8019d6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8019d9:	50                   	push   %eax
  8019da:	e8 be f8 ff ff       	call   80129d <fd_alloc>
  8019df:	83 c4 10             	add    $0x10,%esp
  8019e2:	85 c0                	test   %eax,%eax
  8019e4:	78 55                	js     801a3b <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  8019e6:	83 ec 08             	sub    $0x8,%esp
  8019e9:	53                   	push   %ebx
  8019ea:	68 00 50 80 00       	push   $0x805000
  8019ef:	e8 b7 ee ff ff       	call   8008ab <strcpy>
	fsipcbuf.open.req_omode = mode;
  8019f4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8019f7:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  8019fc:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8019ff:	b8 01 00 00 00       	mov    $0x1,%eax
  801a04:	e8 f6 fd ff ff       	call   8017ff <fsipc>
  801a09:	89 c3                	mov    %eax,%ebx
  801a0b:	83 c4 10             	add    $0x10,%esp
  801a0e:	85 c0                	test   %eax,%eax
  801a10:	79 14                	jns    801a26 <open+0x6d>
		fd_close(fd, 0);
  801a12:	83 ec 08             	sub    $0x8,%esp
  801a15:	6a 00                	push   $0x0
  801a17:	ff 75 f4             	pushl  -0xc(%ebp)
  801a1a:	e8 77 f9 ff ff       	call   801396 <fd_close>
		return r;
  801a1f:	83 c4 10             	add    $0x10,%esp
  801a22:	89 d8                	mov    %ebx,%eax
  801a24:	eb 15                	jmp    801a3b <open+0x82>
	}

	return fd2num(fd);
  801a26:	83 ec 0c             	sub    $0xc,%esp
  801a29:	ff 75 f4             	pushl  -0xc(%ebp)
  801a2c:	e8 45 f8 ff ff       	call   801276 <fd2num>
  801a31:	83 c4 10             	add    $0x10,%esp
  801a34:	eb 05                	jmp    801a3b <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801a36:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801a3b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801a3e:	c9                   	leave  
  801a3f:	c3                   	ret    

00801a40 <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801a40:	55                   	push   %ebp
  801a41:	89 e5                	mov    %esp,%ebp
  801a43:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801a46:	ba 00 00 00 00       	mov    $0x0,%edx
  801a4b:	b8 08 00 00 00       	mov    $0x8,%eax
  801a50:	e8 aa fd ff ff       	call   8017ff <fsipc>
}
  801a55:	c9                   	leave  
  801a56:	c3                   	ret    

00801a57 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801a57:	55                   	push   %ebp
  801a58:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801a5a:	8b 45 08             	mov    0x8(%ebp),%eax
  801a5d:	c1 e8 16             	shr    $0x16,%eax
  801a60:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801a67:	a8 01                	test   $0x1,%al
  801a69:	74 21                	je     801a8c <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801a6b:	8b 45 08             	mov    0x8(%ebp),%eax
  801a6e:	c1 e8 0c             	shr    $0xc,%eax
  801a71:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801a78:	a8 01                	test   $0x1,%al
  801a7a:	74 17                	je     801a93 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801a7c:	c1 e8 0c             	shr    $0xc,%eax
  801a7f:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801a86:	ef 
  801a87:	0f b7 c0             	movzwl %ax,%eax
  801a8a:	eb 0c                	jmp    801a98 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801a8c:	b8 00 00 00 00       	mov    $0x0,%eax
  801a91:	eb 05                	jmp    801a98 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801a93:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801a98:	5d                   	pop    %ebp
  801a99:	c3                   	ret    

00801a9a <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801a9a:	55                   	push   %ebp
  801a9b:	89 e5                	mov    %esp,%ebp
  801a9d:	56                   	push   %esi
  801a9e:	53                   	push   %ebx
  801a9f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801aa2:	83 ec 0c             	sub    $0xc,%esp
  801aa5:	ff 75 08             	pushl  0x8(%ebp)
  801aa8:	e8 d9 f7 ff ff       	call   801286 <fd2data>
  801aad:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801aaf:	83 c4 08             	add    $0x8,%esp
  801ab2:	68 9b 28 80 00       	push   $0x80289b
  801ab7:	53                   	push   %ebx
  801ab8:	e8 ee ed ff ff       	call   8008ab <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801abd:	8b 46 04             	mov    0x4(%esi),%eax
  801ac0:	2b 06                	sub    (%esi),%eax
  801ac2:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801ac8:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801acf:	00 00 00 
	stat->st_dev = &devpipe;
  801ad2:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  801ad9:	30 80 00 
	return 0;
}
  801adc:	b8 00 00 00 00       	mov    $0x0,%eax
  801ae1:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801ae4:	5b                   	pop    %ebx
  801ae5:	5e                   	pop    %esi
  801ae6:	5d                   	pop    %ebp
  801ae7:	c3                   	ret    

00801ae8 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801ae8:	55                   	push   %ebp
  801ae9:	89 e5                	mov    %esp,%ebp
  801aeb:	53                   	push   %ebx
  801aec:	83 ec 0c             	sub    $0xc,%esp
  801aef:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801af2:	53                   	push   %ebx
  801af3:	6a 00                	push   $0x0
  801af5:	e8 1a f2 ff ff       	call   800d14 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801afa:	89 1c 24             	mov    %ebx,(%esp)
  801afd:	e8 84 f7 ff ff       	call   801286 <fd2data>
  801b02:	83 c4 08             	add    $0x8,%esp
  801b05:	50                   	push   %eax
  801b06:	6a 00                	push   $0x0
  801b08:	e8 07 f2 ff ff       	call   800d14 <sys_page_unmap>
}
  801b0d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801b10:	c9                   	leave  
  801b11:	c3                   	ret    

00801b12 <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801b12:	55                   	push   %ebp
  801b13:	89 e5                	mov    %esp,%ebp
  801b15:	57                   	push   %edi
  801b16:	56                   	push   %esi
  801b17:	53                   	push   %ebx
  801b18:	83 ec 1c             	sub    $0x1c,%esp
  801b1b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801b1e:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801b20:	a1 04 40 80 00       	mov    0x804004,%eax
  801b25:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801b28:	83 ec 0c             	sub    $0xc,%esp
  801b2b:	ff 75 e0             	pushl  -0x20(%ebp)
  801b2e:	e8 24 ff ff ff       	call   801a57 <pageref>
  801b33:	89 c3                	mov    %eax,%ebx
  801b35:	89 3c 24             	mov    %edi,(%esp)
  801b38:	e8 1a ff ff ff       	call   801a57 <pageref>
  801b3d:	83 c4 10             	add    $0x10,%esp
  801b40:	39 c3                	cmp    %eax,%ebx
  801b42:	0f 94 c1             	sete   %cl
  801b45:	0f b6 c9             	movzbl %cl,%ecx
  801b48:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801b4b:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801b51:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801b54:	39 ce                	cmp    %ecx,%esi
  801b56:	74 1b                	je     801b73 <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801b58:	39 c3                	cmp    %eax,%ebx
  801b5a:	75 c4                	jne    801b20 <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801b5c:	8b 42 58             	mov    0x58(%edx),%eax
  801b5f:	ff 75 e4             	pushl  -0x1c(%ebp)
  801b62:	50                   	push   %eax
  801b63:	56                   	push   %esi
  801b64:	68 a2 28 80 00       	push   $0x8028a2
  801b69:	e8 d9 e7 ff ff       	call   800347 <cprintf>
  801b6e:	83 c4 10             	add    $0x10,%esp
  801b71:	eb ad                	jmp    801b20 <_pipeisclosed+0xe>
	}
}
  801b73:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801b76:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801b79:	5b                   	pop    %ebx
  801b7a:	5e                   	pop    %esi
  801b7b:	5f                   	pop    %edi
  801b7c:	5d                   	pop    %ebp
  801b7d:	c3                   	ret    

00801b7e <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801b7e:	55                   	push   %ebp
  801b7f:	89 e5                	mov    %esp,%ebp
  801b81:	57                   	push   %edi
  801b82:	56                   	push   %esi
  801b83:	53                   	push   %ebx
  801b84:	83 ec 18             	sub    $0x18,%esp
  801b87:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801b8a:	56                   	push   %esi
  801b8b:	e8 f6 f6 ff ff       	call   801286 <fd2data>
  801b90:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801b92:	83 c4 10             	add    $0x10,%esp
  801b95:	bf 00 00 00 00       	mov    $0x0,%edi
  801b9a:	eb 3b                	jmp    801bd7 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801b9c:	89 da                	mov    %ebx,%edx
  801b9e:	89 f0                	mov    %esi,%eax
  801ba0:	e8 6d ff ff ff       	call   801b12 <_pipeisclosed>
  801ba5:	85 c0                	test   %eax,%eax
  801ba7:	75 38                	jne    801be1 <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801ba9:	e8 c2 f0 ff ff       	call   800c70 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801bae:	8b 53 04             	mov    0x4(%ebx),%edx
  801bb1:	8b 03                	mov    (%ebx),%eax
  801bb3:	83 c0 20             	add    $0x20,%eax
  801bb6:	39 c2                	cmp    %eax,%edx
  801bb8:	73 e2                	jae    801b9c <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801bba:	8b 45 0c             	mov    0xc(%ebp),%eax
  801bbd:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801bc0:	89 d0                	mov    %edx,%eax
  801bc2:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801bc7:	79 05                	jns    801bce <devpipe_write+0x50>
  801bc9:	48                   	dec    %eax
  801bca:	83 c8 e0             	or     $0xffffffe0,%eax
  801bcd:	40                   	inc    %eax
  801bce:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801bd2:	42                   	inc    %edx
  801bd3:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801bd6:	47                   	inc    %edi
  801bd7:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801bda:	75 d2                	jne    801bae <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801bdc:	8b 45 10             	mov    0x10(%ebp),%eax
  801bdf:	eb 05                	jmp    801be6 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801be1:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801be6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801be9:	5b                   	pop    %ebx
  801bea:	5e                   	pop    %esi
  801beb:	5f                   	pop    %edi
  801bec:	5d                   	pop    %ebp
  801bed:	c3                   	ret    

00801bee <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801bee:	55                   	push   %ebp
  801bef:	89 e5                	mov    %esp,%ebp
  801bf1:	57                   	push   %edi
  801bf2:	56                   	push   %esi
  801bf3:	53                   	push   %ebx
  801bf4:	83 ec 18             	sub    $0x18,%esp
  801bf7:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801bfa:	57                   	push   %edi
  801bfb:	e8 86 f6 ff ff       	call   801286 <fd2data>
  801c00:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c02:	83 c4 10             	add    $0x10,%esp
  801c05:	bb 00 00 00 00       	mov    $0x0,%ebx
  801c0a:	eb 3a                	jmp    801c46 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801c0c:	85 db                	test   %ebx,%ebx
  801c0e:	74 04                	je     801c14 <devpipe_read+0x26>
				return i;
  801c10:	89 d8                	mov    %ebx,%eax
  801c12:	eb 41                	jmp    801c55 <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801c14:	89 f2                	mov    %esi,%edx
  801c16:	89 f8                	mov    %edi,%eax
  801c18:	e8 f5 fe ff ff       	call   801b12 <_pipeisclosed>
  801c1d:	85 c0                	test   %eax,%eax
  801c1f:	75 2f                	jne    801c50 <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801c21:	e8 4a f0 ff ff       	call   800c70 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801c26:	8b 06                	mov    (%esi),%eax
  801c28:	3b 46 04             	cmp    0x4(%esi),%eax
  801c2b:	74 df                	je     801c0c <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801c2d:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801c32:	79 05                	jns    801c39 <devpipe_read+0x4b>
  801c34:	48                   	dec    %eax
  801c35:	83 c8 e0             	or     $0xffffffe0,%eax
  801c38:	40                   	inc    %eax
  801c39:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801c3d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801c40:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801c43:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c45:	43                   	inc    %ebx
  801c46:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801c49:	75 db                	jne    801c26 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801c4b:	8b 45 10             	mov    0x10(%ebp),%eax
  801c4e:	eb 05                	jmp    801c55 <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801c50:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801c55:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c58:	5b                   	pop    %ebx
  801c59:	5e                   	pop    %esi
  801c5a:	5f                   	pop    %edi
  801c5b:	5d                   	pop    %ebp
  801c5c:	c3                   	ret    

00801c5d <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801c5d:	55                   	push   %ebp
  801c5e:	89 e5                	mov    %esp,%ebp
  801c60:	56                   	push   %esi
  801c61:	53                   	push   %ebx
  801c62:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801c65:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801c68:	50                   	push   %eax
  801c69:	e8 2f f6 ff ff       	call   80129d <fd_alloc>
  801c6e:	83 c4 10             	add    $0x10,%esp
  801c71:	85 c0                	test   %eax,%eax
  801c73:	0f 88 2a 01 00 00    	js     801da3 <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801c79:	83 ec 04             	sub    $0x4,%esp
  801c7c:	68 07 04 00 00       	push   $0x407
  801c81:	ff 75 f4             	pushl  -0xc(%ebp)
  801c84:	6a 00                	push   $0x0
  801c86:	e8 04 f0 ff ff       	call   800c8f <sys_page_alloc>
  801c8b:	83 c4 10             	add    $0x10,%esp
  801c8e:	85 c0                	test   %eax,%eax
  801c90:	0f 88 0d 01 00 00    	js     801da3 <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801c96:	83 ec 0c             	sub    $0xc,%esp
  801c99:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801c9c:	50                   	push   %eax
  801c9d:	e8 fb f5 ff ff       	call   80129d <fd_alloc>
  801ca2:	89 c3                	mov    %eax,%ebx
  801ca4:	83 c4 10             	add    $0x10,%esp
  801ca7:	85 c0                	test   %eax,%eax
  801ca9:	0f 88 e2 00 00 00    	js     801d91 <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801caf:	83 ec 04             	sub    $0x4,%esp
  801cb2:	68 07 04 00 00       	push   $0x407
  801cb7:	ff 75 f0             	pushl  -0x10(%ebp)
  801cba:	6a 00                	push   $0x0
  801cbc:	e8 ce ef ff ff       	call   800c8f <sys_page_alloc>
  801cc1:	89 c3                	mov    %eax,%ebx
  801cc3:	83 c4 10             	add    $0x10,%esp
  801cc6:	85 c0                	test   %eax,%eax
  801cc8:	0f 88 c3 00 00 00    	js     801d91 <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801cce:	83 ec 0c             	sub    $0xc,%esp
  801cd1:	ff 75 f4             	pushl  -0xc(%ebp)
  801cd4:	e8 ad f5 ff ff       	call   801286 <fd2data>
  801cd9:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801cdb:	83 c4 0c             	add    $0xc,%esp
  801cde:	68 07 04 00 00       	push   $0x407
  801ce3:	50                   	push   %eax
  801ce4:	6a 00                	push   $0x0
  801ce6:	e8 a4 ef ff ff       	call   800c8f <sys_page_alloc>
  801ceb:	89 c3                	mov    %eax,%ebx
  801ced:	83 c4 10             	add    $0x10,%esp
  801cf0:	85 c0                	test   %eax,%eax
  801cf2:	0f 88 89 00 00 00    	js     801d81 <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801cf8:	83 ec 0c             	sub    $0xc,%esp
  801cfb:	ff 75 f0             	pushl  -0x10(%ebp)
  801cfe:	e8 83 f5 ff ff       	call   801286 <fd2data>
  801d03:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801d0a:	50                   	push   %eax
  801d0b:	6a 00                	push   $0x0
  801d0d:	56                   	push   %esi
  801d0e:	6a 00                	push   $0x0
  801d10:	e8 bd ef ff ff       	call   800cd2 <sys_page_map>
  801d15:	89 c3                	mov    %eax,%ebx
  801d17:	83 c4 20             	add    $0x20,%esp
  801d1a:	85 c0                	test   %eax,%eax
  801d1c:	78 55                	js     801d73 <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801d1e:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801d24:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801d27:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801d29:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801d2c:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801d33:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801d39:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801d3c:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801d3e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801d41:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801d48:	83 ec 0c             	sub    $0xc,%esp
  801d4b:	ff 75 f4             	pushl  -0xc(%ebp)
  801d4e:	e8 23 f5 ff ff       	call   801276 <fd2num>
  801d53:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801d56:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801d58:	83 c4 04             	add    $0x4,%esp
  801d5b:	ff 75 f0             	pushl  -0x10(%ebp)
  801d5e:	e8 13 f5 ff ff       	call   801276 <fd2num>
  801d63:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801d66:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801d69:	83 c4 10             	add    $0x10,%esp
  801d6c:	b8 00 00 00 00       	mov    $0x0,%eax
  801d71:	eb 30                	jmp    801da3 <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801d73:	83 ec 08             	sub    $0x8,%esp
  801d76:	56                   	push   %esi
  801d77:	6a 00                	push   $0x0
  801d79:	e8 96 ef ff ff       	call   800d14 <sys_page_unmap>
  801d7e:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801d81:	83 ec 08             	sub    $0x8,%esp
  801d84:	ff 75 f0             	pushl  -0x10(%ebp)
  801d87:	6a 00                	push   $0x0
  801d89:	e8 86 ef ff ff       	call   800d14 <sys_page_unmap>
  801d8e:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801d91:	83 ec 08             	sub    $0x8,%esp
  801d94:	ff 75 f4             	pushl  -0xc(%ebp)
  801d97:	6a 00                	push   $0x0
  801d99:	e8 76 ef ff ff       	call   800d14 <sys_page_unmap>
  801d9e:	83 c4 10             	add    $0x10,%esp
  801da1:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801da3:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801da6:	5b                   	pop    %ebx
  801da7:	5e                   	pop    %esi
  801da8:	5d                   	pop    %ebp
  801da9:	c3                   	ret    

00801daa <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801daa:	55                   	push   %ebp
  801dab:	89 e5                	mov    %esp,%ebp
  801dad:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801db0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801db3:	50                   	push   %eax
  801db4:	ff 75 08             	pushl  0x8(%ebp)
  801db7:	e8 30 f5 ff ff       	call   8012ec <fd_lookup>
  801dbc:	83 c4 10             	add    $0x10,%esp
  801dbf:	85 c0                	test   %eax,%eax
  801dc1:	78 18                	js     801ddb <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801dc3:	83 ec 0c             	sub    $0xc,%esp
  801dc6:	ff 75 f4             	pushl  -0xc(%ebp)
  801dc9:	e8 b8 f4 ff ff       	call   801286 <fd2data>
	return _pipeisclosed(fd, p);
  801dce:	89 c2                	mov    %eax,%edx
  801dd0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801dd3:	e8 3a fd ff ff       	call   801b12 <_pipeisclosed>
  801dd8:	83 c4 10             	add    $0x10,%esp
}
  801ddb:	c9                   	leave  
  801ddc:	c3                   	ret    

00801ddd <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801ddd:	55                   	push   %ebp
  801dde:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801de0:	b8 00 00 00 00       	mov    $0x0,%eax
  801de5:	5d                   	pop    %ebp
  801de6:	c3                   	ret    

00801de7 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801de7:	55                   	push   %ebp
  801de8:	89 e5                	mov    %esp,%ebp
  801dea:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801ded:	68 ba 28 80 00       	push   $0x8028ba
  801df2:	ff 75 0c             	pushl  0xc(%ebp)
  801df5:	e8 b1 ea ff ff       	call   8008ab <strcpy>
	return 0;
}
  801dfa:	b8 00 00 00 00       	mov    $0x0,%eax
  801dff:	c9                   	leave  
  801e00:	c3                   	ret    

00801e01 <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801e01:	55                   	push   %ebp
  801e02:	89 e5                	mov    %esp,%ebp
  801e04:	57                   	push   %edi
  801e05:	56                   	push   %esi
  801e06:	53                   	push   %ebx
  801e07:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801e0d:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801e12:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801e18:	eb 2c                	jmp    801e46 <devcons_write+0x45>
		m = n - tot;
  801e1a:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801e1d:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801e1f:	83 fb 7f             	cmp    $0x7f,%ebx
  801e22:	76 05                	jbe    801e29 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801e24:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801e29:	83 ec 04             	sub    $0x4,%esp
  801e2c:	53                   	push   %ebx
  801e2d:	03 45 0c             	add    0xc(%ebp),%eax
  801e30:	50                   	push   %eax
  801e31:	57                   	push   %edi
  801e32:	e8 e9 eb ff ff       	call   800a20 <memmove>
		sys_cputs(buf, m);
  801e37:	83 c4 08             	add    $0x8,%esp
  801e3a:	53                   	push   %ebx
  801e3b:	57                   	push   %edi
  801e3c:	e8 92 ed ff ff       	call   800bd3 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801e41:	01 de                	add    %ebx,%esi
  801e43:	83 c4 10             	add    $0x10,%esp
  801e46:	89 f0                	mov    %esi,%eax
  801e48:	3b 75 10             	cmp    0x10(%ebp),%esi
  801e4b:	72 cd                	jb     801e1a <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801e4d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801e50:	5b                   	pop    %ebx
  801e51:	5e                   	pop    %esi
  801e52:	5f                   	pop    %edi
  801e53:	5d                   	pop    %ebp
  801e54:	c3                   	ret    

00801e55 <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801e55:	55                   	push   %ebp
  801e56:	89 e5                	mov    %esp,%ebp
  801e58:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801e5b:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801e5f:	75 07                	jne    801e68 <devcons_read+0x13>
  801e61:	eb 23                	jmp    801e86 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801e63:	e8 08 ee ff ff       	call   800c70 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801e68:	e8 84 ed ff ff       	call   800bf1 <sys_cgetc>
  801e6d:	85 c0                	test   %eax,%eax
  801e6f:	74 f2                	je     801e63 <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801e71:	85 c0                	test   %eax,%eax
  801e73:	78 1d                	js     801e92 <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801e75:	83 f8 04             	cmp    $0x4,%eax
  801e78:	74 13                	je     801e8d <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801e7a:	8b 55 0c             	mov    0xc(%ebp),%edx
  801e7d:	88 02                	mov    %al,(%edx)
	return 1;
  801e7f:	b8 01 00 00 00       	mov    $0x1,%eax
  801e84:	eb 0c                	jmp    801e92 <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801e86:	b8 00 00 00 00       	mov    $0x0,%eax
  801e8b:	eb 05                	jmp    801e92 <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801e8d:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801e92:	c9                   	leave  
  801e93:	c3                   	ret    

00801e94 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801e94:	55                   	push   %ebp
  801e95:	89 e5                	mov    %esp,%ebp
  801e97:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801e9a:	8b 45 08             	mov    0x8(%ebp),%eax
  801e9d:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801ea0:	6a 01                	push   $0x1
  801ea2:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801ea5:	50                   	push   %eax
  801ea6:	e8 28 ed ff ff       	call   800bd3 <sys_cputs>
}
  801eab:	83 c4 10             	add    $0x10,%esp
  801eae:	c9                   	leave  
  801eaf:	c3                   	ret    

00801eb0 <getchar>:

int
getchar(void)
{
  801eb0:	55                   	push   %ebp
  801eb1:	89 e5                	mov    %esp,%ebp
  801eb3:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801eb6:	6a 01                	push   $0x1
  801eb8:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801ebb:	50                   	push   %eax
  801ebc:	6a 00                	push   $0x0
  801ebe:	e8 8f f6 ff ff       	call   801552 <read>
	if (r < 0)
  801ec3:	83 c4 10             	add    $0x10,%esp
  801ec6:	85 c0                	test   %eax,%eax
  801ec8:	78 0f                	js     801ed9 <getchar+0x29>
		return r;
	if (r < 1)
  801eca:	85 c0                	test   %eax,%eax
  801ecc:	7e 06                	jle    801ed4 <getchar+0x24>
		return -E_EOF;
	return c;
  801ece:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801ed2:	eb 05                	jmp    801ed9 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801ed4:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801ed9:	c9                   	leave  
  801eda:	c3                   	ret    

00801edb <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801edb:	55                   	push   %ebp
  801edc:	89 e5                	mov    %esp,%ebp
  801ede:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801ee1:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ee4:	50                   	push   %eax
  801ee5:	ff 75 08             	pushl  0x8(%ebp)
  801ee8:	e8 ff f3 ff ff       	call   8012ec <fd_lookup>
  801eed:	83 c4 10             	add    $0x10,%esp
  801ef0:	85 c0                	test   %eax,%eax
  801ef2:	78 11                	js     801f05 <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801ef4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801ef7:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801efd:	39 10                	cmp    %edx,(%eax)
  801eff:	0f 94 c0             	sete   %al
  801f02:	0f b6 c0             	movzbl %al,%eax
}
  801f05:	c9                   	leave  
  801f06:	c3                   	ret    

00801f07 <opencons>:

int
opencons(void)
{
  801f07:	55                   	push   %ebp
  801f08:	89 e5                	mov    %esp,%ebp
  801f0a:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801f0d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801f10:	50                   	push   %eax
  801f11:	e8 87 f3 ff ff       	call   80129d <fd_alloc>
  801f16:	83 c4 10             	add    $0x10,%esp
  801f19:	85 c0                	test   %eax,%eax
  801f1b:	78 3a                	js     801f57 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801f1d:	83 ec 04             	sub    $0x4,%esp
  801f20:	68 07 04 00 00       	push   $0x407
  801f25:	ff 75 f4             	pushl  -0xc(%ebp)
  801f28:	6a 00                	push   $0x0
  801f2a:	e8 60 ed ff ff       	call   800c8f <sys_page_alloc>
  801f2f:	83 c4 10             	add    $0x10,%esp
  801f32:	85 c0                	test   %eax,%eax
  801f34:	78 21                	js     801f57 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801f36:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801f3c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801f3f:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801f41:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801f44:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801f4b:	83 ec 0c             	sub    $0xc,%esp
  801f4e:	50                   	push   %eax
  801f4f:	e8 22 f3 ff ff       	call   801276 <fd2num>
  801f54:	83 c4 10             	add    $0x10,%esp
}
  801f57:	c9                   	leave  
  801f58:	c3                   	ret    

00801f59 <set_pgfault_handler>:
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
  801f59:	55                   	push   %ebp
  801f5a:	89 e5                	mov    %esp,%ebp
  801f5c:	83 ec 08             	sub    $0x8,%esp
	int r;

	if (_pgfault_handler == 0) {
  801f5f:	83 3d 00 60 80 00 00 	cmpl   $0x0,0x806000
  801f66:	75 3e                	jne    801fa6 <set_pgfault_handler+0x4d>
		// First time through!
		// LAB 4: Your code here.
		if (sys_page_alloc(0,
  801f68:	83 ec 04             	sub    $0x4,%esp
  801f6b:	6a 07                	push   $0x7
  801f6d:	68 00 f0 bf ee       	push   $0xeebff000
  801f72:	6a 00                	push   $0x0
  801f74:	e8 16 ed ff ff       	call   800c8f <sys_page_alloc>
  801f79:	83 c4 10             	add    $0x10,%esp
  801f7c:	85 c0                	test   %eax,%eax
  801f7e:	74 14                	je     801f94 <set_pgfault_handler+0x3b>
                           (void *)(UXSTACKTOP - PGSIZE),
                           PTE_W | PTE_U | PTE_P/* must be present */))
			panic("set_pgfault_handler: no phys mem");
  801f80:	83 ec 04             	sub    $0x4,%esp
  801f83:	68 c8 28 80 00       	push   $0x8028c8
  801f88:	6a 23                	push   $0x23
  801f8a:	68 ec 28 80 00       	push   $0x8028ec
  801f8f:	e8 db e2 ff ff       	call   80026f <_panic>

		sys_env_set_pgfault_upcall(0, _pgfault_upcall);
  801f94:	83 ec 08             	sub    $0x8,%esp
  801f97:	68 b0 1f 80 00       	push   $0x801fb0
  801f9c:	6a 00                	push   $0x0
  801f9e:	e8 56 ee ff ff       	call   800df9 <sys_env_set_pgfault_upcall>
  801fa3:	83 c4 10             	add    $0x10,%esp
		//panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
  801fa6:	8b 45 08             	mov    0x8(%ebp),%eax
  801fa9:	a3 00 60 80 00       	mov    %eax,0x806000
}
  801fae:	c9                   	leave  
  801faf:	c3                   	ret    

00801fb0 <_pgfault_upcall>:

.text
.globl _pgfault_upcall
_pgfault_upcall:
	// Call the C page fault handler.
	pushl %esp			// function argument: pointer to UTF
  801fb0:	54                   	push   %esp
	movl _pgfault_handler, %eax
  801fb1:	a1 00 60 80 00       	mov    0x806000,%eax
	call *%eax
  801fb6:	ff d0                	call   *%eax
	addl $4, %esp			// pop function argument
  801fb8:	83 c4 04             	add    $0x4,%esp
	// registers are available for intermediate calculations.  You
	// may find that you have to rearrange your code in non-obvious
	// ways as registers become unavailable as scratch space.
	//
	// LAB 4: Your code here.
	movl %esp, %eax /* temporarily save exception stack esp */
  801fbb:	89 e0                	mov    %esp,%eax
	movl 40(%esp), %ebx /* return addr -> ebx */
  801fbd:	8b 5c 24 28          	mov    0x28(%esp),%ebx
	movl 48(%esp), %esp /* now trap-time stack  */
  801fc1:	8b 64 24 30          	mov    0x30(%esp),%esp
	pushl %ebx /* push onto trap-time stack */
  801fc5:	53                   	push   %ebx
	movl %esp, 48(%eax) /* esp in frame is no longer its original position,
  801fc6:	89 60 30             	mov    %esp,0x30(%eax)
	                     * we just pushed the return address */

	// Restore the trap-time registers.  After you do this, you
	// can no longer modify any general-purpose registers.
	// LAB 4: Your code here.
	movl %eax, %esp /* now exception stack */
  801fc9:	89 c4                	mov    %eax,%esp
	addl $4, %esp /* skip utf_fault_va */
  801fcb:	83 c4 04             	add    $0x4,%esp
	addl $4, %esp /* skip utf_err */
  801fce:	83 c4 04             	add    $0x4,%esp
	popal /* restore from utf_regs  */
  801fd1:	61                   	popa   
	addl $4, %esp /* skip utf_eip (already on trap-time stack) */
  801fd2:	83 c4 04             	add    $0x4,%esp

	// Restore eflags from the stack.  After you do this, you can
	// no longer use arithmetic operations or anything else that
	// modifies eflags.
	// LAB 4: Your code here.
	popfl /* restore from utf_eflags */
  801fd5:	9d                   	popf   

	// Switch back to the adjusted trap-time stack.
	// LAB 4: Your code here.
	popl %esp /* restore from utf_esp */
  801fd6:	5c                   	pop    %esp

	// Return to re-execute the instruction that faulted.
	// LAB 4: Your code here.
  801fd7:	c3                   	ret    

00801fd8 <__udivdi3>:
  801fd8:	55                   	push   %ebp
  801fd9:	57                   	push   %edi
  801fda:	56                   	push   %esi
  801fdb:	53                   	push   %ebx
  801fdc:	83 ec 1c             	sub    $0x1c,%esp
  801fdf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801fe3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801fe7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801feb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801fef:	89 ca                	mov    %ecx,%edx
  801ff1:	89 f8                	mov    %edi,%eax
  801ff3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801ff7:	85 f6                	test   %esi,%esi
  801ff9:	75 2d                	jne    802028 <__udivdi3+0x50>
  801ffb:	39 cf                	cmp    %ecx,%edi
  801ffd:	77 65                	ja     802064 <__udivdi3+0x8c>
  801fff:	89 fd                	mov    %edi,%ebp
  802001:	85 ff                	test   %edi,%edi
  802003:	75 0b                	jne    802010 <__udivdi3+0x38>
  802005:	b8 01 00 00 00       	mov    $0x1,%eax
  80200a:	31 d2                	xor    %edx,%edx
  80200c:	f7 f7                	div    %edi
  80200e:	89 c5                	mov    %eax,%ebp
  802010:	31 d2                	xor    %edx,%edx
  802012:	89 c8                	mov    %ecx,%eax
  802014:	f7 f5                	div    %ebp
  802016:	89 c1                	mov    %eax,%ecx
  802018:	89 d8                	mov    %ebx,%eax
  80201a:	f7 f5                	div    %ebp
  80201c:	89 cf                	mov    %ecx,%edi
  80201e:	89 fa                	mov    %edi,%edx
  802020:	83 c4 1c             	add    $0x1c,%esp
  802023:	5b                   	pop    %ebx
  802024:	5e                   	pop    %esi
  802025:	5f                   	pop    %edi
  802026:	5d                   	pop    %ebp
  802027:	c3                   	ret    
  802028:	39 ce                	cmp    %ecx,%esi
  80202a:	77 28                	ja     802054 <__udivdi3+0x7c>
  80202c:	0f bd fe             	bsr    %esi,%edi
  80202f:	83 f7 1f             	xor    $0x1f,%edi
  802032:	75 40                	jne    802074 <__udivdi3+0x9c>
  802034:	39 ce                	cmp    %ecx,%esi
  802036:	72 0a                	jb     802042 <__udivdi3+0x6a>
  802038:	3b 44 24 08          	cmp    0x8(%esp),%eax
  80203c:	0f 87 9e 00 00 00    	ja     8020e0 <__udivdi3+0x108>
  802042:	b8 01 00 00 00       	mov    $0x1,%eax
  802047:	89 fa                	mov    %edi,%edx
  802049:	83 c4 1c             	add    $0x1c,%esp
  80204c:	5b                   	pop    %ebx
  80204d:	5e                   	pop    %esi
  80204e:	5f                   	pop    %edi
  80204f:	5d                   	pop    %ebp
  802050:	c3                   	ret    
  802051:	8d 76 00             	lea    0x0(%esi),%esi
  802054:	31 ff                	xor    %edi,%edi
  802056:	31 c0                	xor    %eax,%eax
  802058:	89 fa                	mov    %edi,%edx
  80205a:	83 c4 1c             	add    $0x1c,%esp
  80205d:	5b                   	pop    %ebx
  80205e:	5e                   	pop    %esi
  80205f:	5f                   	pop    %edi
  802060:	5d                   	pop    %ebp
  802061:	c3                   	ret    
  802062:	66 90                	xchg   %ax,%ax
  802064:	89 d8                	mov    %ebx,%eax
  802066:	f7 f7                	div    %edi
  802068:	31 ff                	xor    %edi,%edi
  80206a:	89 fa                	mov    %edi,%edx
  80206c:	83 c4 1c             	add    $0x1c,%esp
  80206f:	5b                   	pop    %ebx
  802070:	5e                   	pop    %esi
  802071:	5f                   	pop    %edi
  802072:	5d                   	pop    %ebp
  802073:	c3                   	ret    
  802074:	bd 20 00 00 00       	mov    $0x20,%ebp
  802079:	89 eb                	mov    %ebp,%ebx
  80207b:	29 fb                	sub    %edi,%ebx
  80207d:	89 f9                	mov    %edi,%ecx
  80207f:	d3 e6                	shl    %cl,%esi
  802081:	89 c5                	mov    %eax,%ebp
  802083:	88 d9                	mov    %bl,%cl
  802085:	d3 ed                	shr    %cl,%ebp
  802087:	89 e9                	mov    %ebp,%ecx
  802089:	09 f1                	or     %esi,%ecx
  80208b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80208f:	89 f9                	mov    %edi,%ecx
  802091:	d3 e0                	shl    %cl,%eax
  802093:	89 c5                	mov    %eax,%ebp
  802095:	89 d6                	mov    %edx,%esi
  802097:	88 d9                	mov    %bl,%cl
  802099:	d3 ee                	shr    %cl,%esi
  80209b:	89 f9                	mov    %edi,%ecx
  80209d:	d3 e2                	shl    %cl,%edx
  80209f:	8b 44 24 08          	mov    0x8(%esp),%eax
  8020a3:	88 d9                	mov    %bl,%cl
  8020a5:	d3 e8                	shr    %cl,%eax
  8020a7:	09 c2                	or     %eax,%edx
  8020a9:	89 d0                	mov    %edx,%eax
  8020ab:	89 f2                	mov    %esi,%edx
  8020ad:	f7 74 24 0c          	divl   0xc(%esp)
  8020b1:	89 d6                	mov    %edx,%esi
  8020b3:	89 c3                	mov    %eax,%ebx
  8020b5:	f7 e5                	mul    %ebp
  8020b7:	39 d6                	cmp    %edx,%esi
  8020b9:	72 19                	jb     8020d4 <__udivdi3+0xfc>
  8020bb:	74 0b                	je     8020c8 <__udivdi3+0xf0>
  8020bd:	89 d8                	mov    %ebx,%eax
  8020bf:	31 ff                	xor    %edi,%edi
  8020c1:	e9 58 ff ff ff       	jmp    80201e <__udivdi3+0x46>
  8020c6:	66 90                	xchg   %ax,%ax
  8020c8:	8b 54 24 08          	mov    0x8(%esp),%edx
  8020cc:	89 f9                	mov    %edi,%ecx
  8020ce:	d3 e2                	shl    %cl,%edx
  8020d0:	39 c2                	cmp    %eax,%edx
  8020d2:	73 e9                	jae    8020bd <__udivdi3+0xe5>
  8020d4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8020d7:	31 ff                	xor    %edi,%edi
  8020d9:	e9 40 ff ff ff       	jmp    80201e <__udivdi3+0x46>
  8020de:	66 90                	xchg   %ax,%ax
  8020e0:	31 c0                	xor    %eax,%eax
  8020e2:	e9 37 ff ff ff       	jmp    80201e <__udivdi3+0x46>
  8020e7:	90                   	nop

008020e8 <__umoddi3>:
  8020e8:	55                   	push   %ebp
  8020e9:	57                   	push   %edi
  8020ea:	56                   	push   %esi
  8020eb:	53                   	push   %ebx
  8020ec:	83 ec 1c             	sub    $0x1c,%esp
  8020ef:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8020f3:	8b 74 24 34          	mov    0x34(%esp),%esi
  8020f7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8020fb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8020ff:	89 44 24 0c          	mov    %eax,0xc(%esp)
  802103:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  802107:	89 f3                	mov    %esi,%ebx
  802109:	89 fa                	mov    %edi,%edx
  80210b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80210f:	89 34 24             	mov    %esi,(%esp)
  802112:	85 c0                	test   %eax,%eax
  802114:	75 1a                	jne    802130 <__umoddi3+0x48>
  802116:	39 f7                	cmp    %esi,%edi
  802118:	0f 86 a2 00 00 00    	jbe    8021c0 <__umoddi3+0xd8>
  80211e:	89 c8                	mov    %ecx,%eax
  802120:	89 f2                	mov    %esi,%edx
  802122:	f7 f7                	div    %edi
  802124:	89 d0                	mov    %edx,%eax
  802126:	31 d2                	xor    %edx,%edx
  802128:	83 c4 1c             	add    $0x1c,%esp
  80212b:	5b                   	pop    %ebx
  80212c:	5e                   	pop    %esi
  80212d:	5f                   	pop    %edi
  80212e:	5d                   	pop    %ebp
  80212f:	c3                   	ret    
  802130:	39 f0                	cmp    %esi,%eax
  802132:	0f 87 ac 00 00 00    	ja     8021e4 <__umoddi3+0xfc>
  802138:	0f bd e8             	bsr    %eax,%ebp
  80213b:	83 f5 1f             	xor    $0x1f,%ebp
  80213e:	0f 84 ac 00 00 00    	je     8021f0 <__umoddi3+0x108>
  802144:	bf 20 00 00 00       	mov    $0x20,%edi
  802149:	29 ef                	sub    %ebp,%edi
  80214b:	89 fe                	mov    %edi,%esi
  80214d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802151:	89 e9                	mov    %ebp,%ecx
  802153:	d3 e0                	shl    %cl,%eax
  802155:	89 d7                	mov    %edx,%edi
  802157:	89 f1                	mov    %esi,%ecx
  802159:	d3 ef                	shr    %cl,%edi
  80215b:	09 c7                	or     %eax,%edi
  80215d:	89 e9                	mov    %ebp,%ecx
  80215f:	d3 e2                	shl    %cl,%edx
  802161:	89 14 24             	mov    %edx,(%esp)
  802164:	89 d8                	mov    %ebx,%eax
  802166:	d3 e0                	shl    %cl,%eax
  802168:	89 c2                	mov    %eax,%edx
  80216a:	8b 44 24 08          	mov    0x8(%esp),%eax
  80216e:	d3 e0                	shl    %cl,%eax
  802170:	89 44 24 04          	mov    %eax,0x4(%esp)
  802174:	8b 44 24 08          	mov    0x8(%esp),%eax
  802178:	89 f1                	mov    %esi,%ecx
  80217a:	d3 e8                	shr    %cl,%eax
  80217c:	09 d0                	or     %edx,%eax
  80217e:	d3 eb                	shr    %cl,%ebx
  802180:	89 da                	mov    %ebx,%edx
  802182:	f7 f7                	div    %edi
  802184:	89 d3                	mov    %edx,%ebx
  802186:	f7 24 24             	mull   (%esp)
  802189:	89 c6                	mov    %eax,%esi
  80218b:	89 d1                	mov    %edx,%ecx
  80218d:	39 d3                	cmp    %edx,%ebx
  80218f:	0f 82 87 00 00 00    	jb     80221c <__umoddi3+0x134>
  802195:	0f 84 91 00 00 00    	je     80222c <__umoddi3+0x144>
  80219b:	8b 54 24 04          	mov    0x4(%esp),%edx
  80219f:	29 f2                	sub    %esi,%edx
  8021a1:	19 cb                	sbb    %ecx,%ebx
  8021a3:	89 d8                	mov    %ebx,%eax
  8021a5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8021a9:	d3 e0                	shl    %cl,%eax
  8021ab:	89 e9                	mov    %ebp,%ecx
  8021ad:	d3 ea                	shr    %cl,%edx
  8021af:	09 d0                	or     %edx,%eax
  8021b1:	89 e9                	mov    %ebp,%ecx
  8021b3:	d3 eb                	shr    %cl,%ebx
  8021b5:	89 da                	mov    %ebx,%edx
  8021b7:	83 c4 1c             	add    $0x1c,%esp
  8021ba:	5b                   	pop    %ebx
  8021bb:	5e                   	pop    %esi
  8021bc:	5f                   	pop    %edi
  8021bd:	5d                   	pop    %ebp
  8021be:	c3                   	ret    
  8021bf:	90                   	nop
  8021c0:	89 fd                	mov    %edi,%ebp
  8021c2:	85 ff                	test   %edi,%edi
  8021c4:	75 0b                	jne    8021d1 <__umoddi3+0xe9>
  8021c6:	b8 01 00 00 00       	mov    $0x1,%eax
  8021cb:	31 d2                	xor    %edx,%edx
  8021cd:	f7 f7                	div    %edi
  8021cf:	89 c5                	mov    %eax,%ebp
  8021d1:	89 f0                	mov    %esi,%eax
  8021d3:	31 d2                	xor    %edx,%edx
  8021d5:	f7 f5                	div    %ebp
  8021d7:	89 c8                	mov    %ecx,%eax
  8021d9:	f7 f5                	div    %ebp
  8021db:	89 d0                	mov    %edx,%eax
  8021dd:	e9 44 ff ff ff       	jmp    802126 <__umoddi3+0x3e>
  8021e2:	66 90                	xchg   %ax,%ax
  8021e4:	89 c8                	mov    %ecx,%eax
  8021e6:	89 f2                	mov    %esi,%edx
  8021e8:	83 c4 1c             	add    $0x1c,%esp
  8021eb:	5b                   	pop    %ebx
  8021ec:	5e                   	pop    %esi
  8021ed:	5f                   	pop    %edi
  8021ee:	5d                   	pop    %ebp
  8021ef:	c3                   	ret    
  8021f0:	3b 04 24             	cmp    (%esp),%eax
  8021f3:	72 06                	jb     8021fb <__umoddi3+0x113>
  8021f5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8021f9:	77 0f                	ja     80220a <__umoddi3+0x122>
  8021fb:	89 f2                	mov    %esi,%edx
  8021fd:	29 f9                	sub    %edi,%ecx
  8021ff:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  802203:	89 14 24             	mov    %edx,(%esp)
  802206:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80220a:	8b 44 24 04          	mov    0x4(%esp),%eax
  80220e:	8b 14 24             	mov    (%esp),%edx
  802211:	83 c4 1c             	add    $0x1c,%esp
  802214:	5b                   	pop    %ebx
  802215:	5e                   	pop    %esi
  802216:	5f                   	pop    %edi
  802217:	5d                   	pop    %ebp
  802218:	c3                   	ret    
  802219:	8d 76 00             	lea    0x0(%esi),%esi
  80221c:	2b 04 24             	sub    (%esp),%eax
  80221f:	19 fa                	sbb    %edi,%edx
  802221:	89 d1                	mov    %edx,%ecx
  802223:	89 c6                	mov    %eax,%esi
  802225:	e9 71 ff ff ff       	jmp    80219b <__umoddi3+0xb3>
  80222a:	66 90                	xchg   %ax,%ax
  80222c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802230:	72 ea                	jb     80221c <__umoddi3+0x134>
  802232:	89 d9                	mov    %ebx,%ecx
  802234:	e9 62 ff ff ff       	jmp    80219b <__umoddi3+0xb3>
