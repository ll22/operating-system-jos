
obj/user/cat.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 00 01 00 00       	call   800131 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <cat>:

char buf[8192];

void
cat(int f, char *s)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	56                   	push   %esi
  800037:	53                   	push   %ebx
  800038:	8b 75 08             	mov    0x8(%ebp),%esi
	long n;
	int r;

	while ((n = read(f, buf, (long)sizeof(buf))) > 0)
  80003b:	eb 2f                	jmp    80006c <cat+0x39>
		if ((r = write(1, buf, n)) != n)
  80003d:	83 ec 04             	sub    $0x4,%esp
  800040:	53                   	push   %ebx
  800041:	68 20 40 80 00       	push   $0x804020
  800046:	6a 01                	push   $0x1
  800048:	e8 21 11 00 00       	call   80116e <write>
  80004d:	83 c4 10             	add    $0x10,%esp
  800050:	39 c3                	cmp    %eax,%ebx
  800052:	74 18                	je     80006c <cat+0x39>
			panic("write error copying %s: %e", s, r);
  800054:	83 ec 0c             	sub    $0xc,%esp
  800057:	50                   	push   %eax
  800058:	ff 75 0c             	pushl  0xc(%ebp)
  80005b:	68 20 1f 80 00       	push   $0x801f20
  800060:	6a 0d                	push   $0xd
  800062:	68 3b 1f 80 00       	push   $0x801f3b
  800067:	e8 26 01 00 00       	call   800192 <_panic>
cat(int f, char *s)
{
	long n;
	int r;

	while ((n = read(f, buf, (long)sizeof(buf))) > 0)
  80006c:	83 ec 04             	sub    $0x4,%esp
  80006f:	68 00 20 00 00       	push   $0x2000
  800074:	68 20 40 80 00       	push   $0x804020
  800079:	56                   	push   %esi
  80007a:	e8 1f 10 00 00       	call   80109e <read>
  80007f:	89 c3                	mov    %eax,%ebx
  800081:	83 c4 10             	add    $0x10,%esp
  800084:	85 c0                	test   %eax,%eax
  800086:	7f b5                	jg     80003d <cat+0xa>
		if ((r = write(1, buf, n)) != n)
			panic("write error copying %s: %e", s, r);
	if (n < 0)
  800088:	85 c0                	test   %eax,%eax
  80008a:	79 18                	jns    8000a4 <cat+0x71>
		panic("error reading %s: %e", s, n);
  80008c:	83 ec 0c             	sub    $0xc,%esp
  80008f:	50                   	push   %eax
  800090:	ff 75 0c             	pushl  0xc(%ebp)
  800093:	68 46 1f 80 00       	push   $0x801f46
  800098:	6a 0f                	push   $0xf
  80009a:	68 3b 1f 80 00       	push   $0x801f3b
  80009f:	e8 ee 00 00 00       	call   800192 <_panic>
}
  8000a4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000a7:	5b                   	pop    %ebx
  8000a8:	5e                   	pop    %esi
  8000a9:	5d                   	pop    %ebp
  8000aa:	c3                   	ret    

008000ab <umain>:

void
umain(int argc, char **argv)
{
  8000ab:	55                   	push   %ebp
  8000ac:	89 e5                	mov    %esp,%ebp
  8000ae:	57                   	push   %edi
  8000af:	56                   	push   %esi
  8000b0:	53                   	push   %ebx
  8000b1:	83 ec 0c             	sub    $0xc,%esp
  8000b4:	8b 7d 0c             	mov    0xc(%ebp),%edi
	int f, i;

	binaryname = "cat";
  8000b7:	c7 05 00 30 80 00 5b 	movl   $0x801f5b,0x803000
  8000be:	1f 80 00 
  8000c1:	bb 01 00 00 00       	mov    $0x1,%ebx
	if (argc == 1)
  8000c6:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  8000ca:	75 58                	jne    800124 <umain+0x79>
		cat(0, "<stdin>");
  8000cc:	83 ec 08             	sub    $0x8,%esp
  8000cf:	68 5f 1f 80 00       	push   $0x801f5f
  8000d4:	6a 00                	push   $0x0
  8000d6:	e8 58 ff ff ff       	call   800033 <cat>
  8000db:	83 c4 10             	add    $0x10,%esp
  8000de:	eb 49                	jmp    800129 <umain+0x7e>
	else
		for (i = 1; i < argc; i++) {
			f = open(argv[i], O_RDONLY);
  8000e0:	83 ec 08             	sub    $0x8,%esp
  8000e3:	6a 00                	push   $0x0
  8000e5:	ff 34 9f             	pushl  (%edi,%ebx,4)
  8000e8:	e8 18 14 00 00       	call   801505 <open>
  8000ed:	89 c6                	mov    %eax,%esi
			if (f < 0)
  8000ef:	83 c4 10             	add    $0x10,%esp
  8000f2:	85 c0                	test   %eax,%eax
  8000f4:	79 16                	jns    80010c <umain+0x61>
				printf("can't open %s: %e\n", argv[i], f);
  8000f6:	83 ec 04             	sub    $0x4,%esp
  8000f9:	50                   	push   %eax
  8000fa:	ff 34 9f             	pushl  (%edi,%ebx,4)
  8000fd:	68 67 1f 80 00       	push   $0x801f67
  800102:	e8 97 15 00 00       	call   80169e <printf>
  800107:	83 c4 10             	add    $0x10,%esp
  80010a:	eb 17                	jmp    800123 <umain+0x78>
			else {
				cat(f, argv[i]);
  80010c:	83 ec 08             	sub    $0x8,%esp
  80010f:	ff 34 9f             	pushl  (%edi,%ebx,4)
  800112:	50                   	push   %eax
  800113:	e8 1b ff ff ff       	call   800033 <cat>
				close(f);
  800118:	89 34 24             	mov    %esi,(%esp)
  80011b:	e8 46 0e 00 00       	call   800f66 <close>
  800120:	83 c4 10             	add    $0x10,%esp

	binaryname = "cat";
	if (argc == 1)
		cat(0, "<stdin>");
	else
		for (i = 1; i < argc; i++) {
  800123:	43                   	inc    %ebx
  800124:	3b 5d 08             	cmp    0x8(%ebp),%ebx
  800127:	7c b7                	jl     8000e0 <umain+0x35>
			else {
				cat(f, argv[i]);
				close(f);
			}
		}
}
  800129:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80012c:	5b                   	pop    %ebx
  80012d:	5e                   	pop    %esi
  80012e:	5f                   	pop    %edi
  80012f:	5d                   	pop    %ebp
  800130:	c3                   	ret    

00800131 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800131:	55                   	push   %ebp
  800132:	89 e5                	mov    %esp,%ebp
  800134:	56                   	push   %esi
  800135:	53                   	push   %ebx
  800136:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800139:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80013c:	e8 33 0a 00 00       	call   800b74 <sys_getenvid>
  800141:	25 ff 03 00 00       	and    $0x3ff,%eax
  800146:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80014d:	c1 e0 07             	shl    $0x7,%eax
  800150:	29 d0                	sub    %edx,%eax
  800152:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800157:	a3 20 60 80 00       	mov    %eax,0x806020
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80015c:	85 db                	test   %ebx,%ebx
  80015e:	7e 07                	jle    800167 <libmain+0x36>
		binaryname = argv[0];
  800160:	8b 06                	mov    (%esi),%eax
  800162:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  800167:	83 ec 08             	sub    $0x8,%esp
  80016a:	56                   	push   %esi
  80016b:	53                   	push   %ebx
  80016c:	e8 3a ff ff ff       	call   8000ab <umain>

	// exit gracefully
	exit();
  800171:	e8 0a 00 00 00       	call   800180 <exit>
}
  800176:	83 c4 10             	add    $0x10,%esp
  800179:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80017c:	5b                   	pop    %ebx
  80017d:	5e                   	pop    %esi
  80017e:	5d                   	pop    %ebp
  80017f:	c3                   	ret    

00800180 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800180:	55                   	push   %ebp
  800181:	89 e5                	mov    %esp,%ebp
  800183:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  800186:	6a 00                	push   $0x0
  800188:	e8 a6 09 00 00       	call   800b33 <sys_env_destroy>
}
  80018d:	83 c4 10             	add    $0x10,%esp
  800190:	c9                   	leave  
  800191:	c3                   	ret    

00800192 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800192:	55                   	push   %ebp
  800193:	89 e5                	mov    %esp,%ebp
  800195:	56                   	push   %esi
  800196:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800197:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80019a:	8b 35 00 30 80 00    	mov    0x803000,%esi
  8001a0:	e8 cf 09 00 00       	call   800b74 <sys_getenvid>
  8001a5:	83 ec 0c             	sub    $0xc,%esp
  8001a8:	ff 75 0c             	pushl  0xc(%ebp)
  8001ab:	ff 75 08             	pushl  0x8(%ebp)
  8001ae:	56                   	push   %esi
  8001af:	50                   	push   %eax
  8001b0:	68 84 1f 80 00       	push   $0x801f84
  8001b5:	e8 b0 00 00 00       	call   80026a <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  8001ba:	83 c4 18             	add    $0x18,%esp
  8001bd:	53                   	push   %ebx
  8001be:	ff 75 10             	pushl  0x10(%ebp)
  8001c1:	e8 53 00 00 00       	call   800219 <vcprintf>
	cprintf("\n");
  8001c6:	c7 04 24 a7 23 80 00 	movl   $0x8023a7,(%esp)
  8001cd:	e8 98 00 00 00       	call   80026a <cprintf>
  8001d2:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8001d5:	cc                   	int3   
  8001d6:	eb fd                	jmp    8001d5 <_panic+0x43>

008001d8 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8001d8:	55                   	push   %ebp
  8001d9:	89 e5                	mov    %esp,%ebp
  8001db:	53                   	push   %ebx
  8001dc:	83 ec 04             	sub    $0x4,%esp
  8001df:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8001e2:	8b 13                	mov    (%ebx),%edx
  8001e4:	8d 42 01             	lea    0x1(%edx),%eax
  8001e7:	89 03                	mov    %eax,(%ebx)
  8001e9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8001ec:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001f0:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001f5:	75 1a                	jne    800211 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001f7:	83 ec 08             	sub    $0x8,%esp
  8001fa:	68 ff 00 00 00       	push   $0xff
  8001ff:	8d 43 08             	lea    0x8(%ebx),%eax
  800202:	50                   	push   %eax
  800203:	e8 ee 08 00 00       	call   800af6 <sys_cputs>
		b->idx = 0;
  800208:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80020e:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800211:	ff 43 04             	incl   0x4(%ebx)
}
  800214:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800217:	c9                   	leave  
  800218:	c3                   	ret    

00800219 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800219:	55                   	push   %ebp
  80021a:	89 e5                	mov    %esp,%ebp
  80021c:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800222:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800229:	00 00 00 
	b.cnt = 0;
  80022c:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800233:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800236:	ff 75 0c             	pushl  0xc(%ebp)
  800239:	ff 75 08             	pushl  0x8(%ebp)
  80023c:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800242:	50                   	push   %eax
  800243:	68 d8 01 80 00       	push   $0x8001d8
  800248:	e8 51 01 00 00       	call   80039e <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80024d:	83 c4 08             	add    $0x8,%esp
  800250:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800256:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80025c:	50                   	push   %eax
  80025d:	e8 94 08 00 00       	call   800af6 <sys_cputs>

	return b.cnt;
}
  800262:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800268:	c9                   	leave  
  800269:	c3                   	ret    

0080026a <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80026a:	55                   	push   %ebp
  80026b:	89 e5                	mov    %esp,%ebp
  80026d:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800270:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800273:	50                   	push   %eax
  800274:	ff 75 08             	pushl  0x8(%ebp)
  800277:	e8 9d ff ff ff       	call   800219 <vcprintf>
	va_end(ap);

	return cnt;
}
  80027c:	c9                   	leave  
  80027d:	c3                   	ret    

0080027e <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80027e:	55                   	push   %ebp
  80027f:	89 e5                	mov    %esp,%ebp
  800281:	57                   	push   %edi
  800282:	56                   	push   %esi
  800283:	53                   	push   %ebx
  800284:	83 ec 1c             	sub    $0x1c,%esp
  800287:	89 c7                	mov    %eax,%edi
  800289:	89 d6                	mov    %edx,%esi
  80028b:	8b 45 08             	mov    0x8(%ebp),%eax
  80028e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800291:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800294:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800297:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80029a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80029f:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8002a2:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8002a5:	39 d3                	cmp    %edx,%ebx
  8002a7:	72 05                	jb     8002ae <printnum+0x30>
  8002a9:	39 45 10             	cmp    %eax,0x10(%ebp)
  8002ac:	77 45                	ja     8002f3 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8002ae:	83 ec 0c             	sub    $0xc,%esp
  8002b1:	ff 75 18             	pushl  0x18(%ebp)
  8002b4:	8b 45 14             	mov    0x14(%ebp),%eax
  8002b7:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8002ba:	53                   	push   %ebx
  8002bb:	ff 75 10             	pushl  0x10(%ebp)
  8002be:	83 ec 08             	sub    $0x8,%esp
  8002c1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002c4:	ff 75 e0             	pushl  -0x20(%ebp)
  8002c7:	ff 75 dc             	pushl  -0x24(%ebp)
  8002ca:	ff 75 d8             	pushl  -0x28(%ebp)
  8002cd:	e8 e6 19 00 00       	call   801cb8 <__udivdi3>
  8002d2:	83 c4 18             	add    $0x18,%esp
  8002d5:	52                   	push   %edx
  8002d6:	50                   	push   %eax
  8002d7:	89 f2                	mov    %esi,%edx
  8002d9:	89 f8                	mov    %edi,%eax
  8002db:	e8 9e ff ff ff       	call   80027e <printnum>
  8002e0:	83 c4 20             	add    $0x20,%esp
  8002e3:	eb 16                	jmp    8002fb <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8002e5:	83 ec 08             	sub    $0x8,%esp
  8002e8:	56                   	push   %esi
  8002e9:	ff 75 18             	pushl  0x18(%ebp)
  8002ec:	ff d7                	call   *%edi
  8002ee:	83 c4 10             	add    $0x10,%esp
  8002f1:	eb 03                	jmp    8002f6 <printnum+0x78>
  8002f3:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002f6:	4b                   	dec    %ebx
  8002f7:	85 db                	test   %ebx,%ebx
  8002f9:	7f ea                	jg     8002e5 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002fb:	83 ec 08             	sub    $0x8,%esp
  8002fe:	56                   	push   %esi
  8002ff:	83 ec 04             	sub    $0x4,%esp
  800302:	ff 75 e4             	pushl  -0x1c(%ebp)
  800305:	ff 75 e0             	pushl  -0x20(%ebp)
  800308:	ff 75 dc             	pushl  -0x24(%ebp)
  80030b:	ff 75 d8             	pushl  -0x28(%ebp)
  80030e:	e8 b5 1a 00 00       	call   801dc8 <__umoddi3>
  800313:	83 c4 14             	add    $0x14,%esp
  800316:	0f be 80 a7 1f 80 00 	movsbl 0x801fa7(%eax),%eax
  80031d:	50                   	push   %eax
  80031e:	ff d7                	call   *%edi
}
  800320:	83 c4 10             	add    $0x10,%esp
  800323:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800326:	5b                   	pop    %ebx
  800327:	5e                   	pop    %esi
  800328:	5f                   	pop    %edi
  800329:	5d                   	pop    %ebp
  80032a:	c3                   	ret    

0080032b <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80032b:	55                   	push   %ebp
  80032c:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80032e:	83 fa 01             	cmp    $0x1,%edx
  800331:	7e 0e                	jle    800341 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800333:	8b 10                	mov    (%eax),%edx
  800335:	8d 4a 08             	lea    0x8(%edx),%ecx
  800338:	89 08                	mov    %ecx,(%eax)
  80033a:	8b 02                	mov    (%edx),%eax
  80033c:	8b 52 04             	mov    0x4(%edx),%edx
  80033f:	eb 22                	jmp    800363 <getuint+0x38>
	else if (lflag)
  800341:	85 d2                	test   %edx,%edx
  800343:	74 10                	je     800355 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800345:	8b 10                	mov    (%eax),%edx
  800347:	8d 4a 04             	lea    0x4(%edx),%ecx
  80034a:	89 08                	mov    %ecx,(%eax)
  80034c:	8b 02                	mov    (%edx),%eax
  80034e:	ba 00 00 00 00       	mov    $0x0,%edx
  800353:	eb 0e                	jmp    800363 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800355:	8b 10                	mov    (%eax),%edx
  800357:	8d 4a 04             	lea    0x4(%edx),%ecx
  80035a:	89 08                	mov    %ecx,(%eax)
  80035c:	8b 02                	mov    (%edx),%eax
  80035e:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800363:	5d                   	pop    %ebp
  800364:	c3                   	ret    

00800365 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800365:	55                   	push   %ebp
  800366:	89 e5                	mov    %esp,%ebp
  800368:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80036b:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80036e:	8b 10                	mov    (%eax),%edx
  800370:	3b 50 04             	cmp    0x4(%eax),%edx
  800373:	73 0a                	jae    80037f <sprintputch+0x1a>
		*b->buf++ = ch;
  800375:	8d 4a 01             	lea    0x1(%edx),%ecx
  800378:	89 08                	mov    %ecx,(%eax)
  80037a:	8b 45 08             	mov    0x8(%ebp),%eax
  80037d:	88 02                	mov    %al,(%edx)
}
  80037f:	5d                   	pop    %ebp
  800380:	c3                   	ret    

00800381 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800381:	55                   	push   %ebp
  800382:	89 e5                	mov    %esp,%ebp
  800384:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800387:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80038a:	50                   	push   %eax
  80038b:	ff 75 10             	pushl  0x10(%ebp)
  80038e:	ff 75 0c             	pushl  0xc(%ebp)
  800391:	ff 75 08             	pushl  0x8(%ebp)
  800394:	e8 05 00 00 00       	call   80039e <vprintfmt>
	va_end(ap);
}
  800399:	83 c4 10             	add    $0x10,%esp
  80039c:	c9                   	leave  
  80039d:	c3                   	ret    

0080039e <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80039e:	55                   	push   %ebp
  80039f:	89 e5                	mov    %esp,%ebp
  8003a1:	57                   	push   %edi
  8003a2:	56                   	push   %esi
  8003a3:	53                   	push   %ebx
  8003a4:	83 ec 2c             	sub    $0x2c,%esp
  8003a7:	8b 75 08             	mov    0x8(%ebp),%esi
  8003aa:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8003ad:	8b 7d 10             	mov    0x10(%ebp),%edi
  8003b0:	eb 12                	jmp    8003c4 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8003b2:	85 c0                	test   %eax,%eax
  8003b4:	0f 84 68 03 00 00    	je     800722 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8003ba:	83 ec 08             	sub    $0x8,%esp
  8003bd:	53                   	push   %ebx
  8003be:	50                   	push   %eax
  8003bf:	ff d6                	call   *%esi
  8003c1:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003c4:	47                   	inc    %edi
  8003c5:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8003c9:	83 f8 25             	cmp    $0x25,%eax
  8003cc:	75 e4                	jne    8003b2 <vprintfmt+0x14>
  8003ce:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8003d2:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8003d9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003e0:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8003e7:	ba 00 00 00 00       	mov    $0x0,%edx
  8003ec:	eb 07                	jmp    8003f5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ee:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003f1:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f5:	8d 47 01             	lea    0x1(%edi),%eax
  8003f8:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003fb:	0f b6 0f             	movzbl (%edi),%ecx
  8003fe:	8a 07                	mov    (%edi),%al
  800400:	83 e8 23             	sub    $0x23,%eax
  800403:	3c 55                	cmp    $0x55,%al
  800405:	0f 87 fe 02 00 00    	ja     800709 <vprintfmt+0x36b>
  80040b:	0f b6 c0             	movzbl %al,%eax
  80040e:	ff 24 85 e0 20 80 00 	jmp    *0x8020e0(,%eax,4)
  800415:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800418:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80041c:	eb d7                	jmp    8003f5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80041e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800421:	b8 00 00 00 00       	mov    $0x0,%eax
  800426:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800429:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80042c:	01 c0                	add    %eax,%eax
  80042e:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800432:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800435:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800438:	83 fa 09             	cmp    $0x9,%edx
  80043b:	77 34                	ja     800471 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80043d:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80043e:	eb e9                	jmp    800429 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800440:	8b 45 14             	mov    0x14(%ebp),%eax
  800443:	8d 48 04             	lea    0x4(%eax),%ecx
  800446:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800449:	8b 00                	mov    (%eax),%eax
  80044b:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80044e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800451:	eb 24                	jmp    800477 <vprintfmt+0xd9>
  800453:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800457:	79 07                	jns    800460 <vprintfmt+0xc2>
  800459:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800460:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800463:	eb 90                	jmp    8003f5 <vprintfmt+0x57>
  800465:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800468:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80046f:	eb 84                	jmp    8003f5 <vprintfmt+0x57>
  800471:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800474:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800477:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80047b:	0f 89 74 ff ff ff    	jns    8003f5 <vprintfmt+0x57>
				width = precision, precision = -1;
  800481:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800484:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800487:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80048e:	e9 62 ff ff ff       	jmp    8003f5 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800493:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800494:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800497:	e9 59 ff ff ff       	jmp    8003f5 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80049c:	8b 45 14             	mov    0x14(%ebp),%eax
  80049f:	8d 50 04             	lea    0x4(%eax),%edx
  8004a2:	89 55 14             	mov    %edx,0x14(%ebp)
  8004a5:	83 ec 08             	sub    $0x8,%esp
  8004a8:	53                   	push   %ebx
  8004a9:	ff 30                	pushl  (%eax)
  8004ab:	ff d6                	call   *%esi
			break;
  8004ad:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8004b3:	e9 0c ff ff ff       	jmp    8003c4 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004b8:	8b 45 14             	mov    0x14(%ebp),%eax
  8004bb:	8d 50 04             	lea    0x4(%eax),%edx
  8004be:	89 55 14             	mov    %edx,0x14(%ebp)
  8004c1:	8b 00                	mov    (%eax),%eax
  8004c3:	85 c0                	test   %eax,%eax
  8004c5:	79 02                	jns    8004c9 <vprintfmt+0x12b>
  8004c7:	f7 d8                	neg    %eax
  8004c9:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8004cb:	83 f8 0f             	cmp    $0xf,%eax
  8004ce:	7f 0b                	jg     8004db <vprintfmt+0x13d>
  8004d0:	8b 04 85 40 22 80 00 	mov    0x802240(,%eax,4),%eax
  8004d7:	85 c0                	test   %eax,%eax
  8004d9:	75 18                	jne    8004f3 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8004db:	52                   	push   %edx
  8004dc:	68 bf 1f 80 00       	push   $0x801fbf
  8004e1:	53                   	push   %ebx
  8004e2:	56                   	push   %esi
  8004e3:	e8 99 fe ff ff       	call   800381 <printfmt>
  8004e8:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004eb:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004ee:	e9 d1 fe ff ff       	jmp    8003c4 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004f3:	50                   	push   %eax
  8004f4:	68 75 23 80 00       	push   $0x802375
  8004f9:	53                   	push   %ebx
  8004fa:	56                   	push   %esi
  8004fb:	e8 81 fe ff ff       	call   800381 <printfmt>
  800500:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800503:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800506:	e9 b9 fe ff ff       	jmp    8003c4 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80050b:	8b 45 14             	mov    0x14(%ebp),%eax
  80050e:	8d 50 04             	lea    0x4(%eax),%edx
  800511:	89 55 14             	mov    %edx,0x14(%ebp)
  800514:	8b 38                	mov    (%eax),%edi
  800516:	85 ff                	test   %edi,%edi
  800518:	75 05                	jne    80051f <vprintfmt+0x181>
				p = "(null)";
  80051a:	bf b8 1f 80 00       	mov    $0x801fb8,%edi
			if (width > 0 && padc != '-')
  80051f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800523:	0f 8e 90 00 00 00    	jle    8005b9 <vprintfmt+0x21b>
  800529:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80052d:	0f 84 8e 00 00 00    	je     8005c1 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800533:	83 ec 08             	sub    $0x8,%esp
  800536:	ff 75 d0             	pushl  -0x30(%ebp)
  800539:	57                   	push   %edi
  80053a:	e8 70 02 00 00       	call   8007af <strnlen>
  80053f:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800542:	29 c1                	sub    %eax,%ecx
  800544:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800547:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80054a:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80054e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800551:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800554:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800556:	eb 0d                	jmp    800565 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800558:	83 ec 08             	sub    $0x8,%esp
  80055b:	53                   	push   %ebx
  80055c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80055f:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800561:	4f                   	dec    %edi
  800562:	83 c4 10             	add    $0x10,%esp
  800565:	85 ff                	test   %edi,%edi
  800567:	7f ef                	jg     800558 <vprintfmt+0x1ba>
  800569:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80056c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80056f:	89 c8                	mov    %ecx,%eax
  800571:	85 c9                	test   %ecx,%ecx
  800573:	79 05                	jns    80057a <vprintfmt+0x1dc>
  800575:	b8 00 00 00 00       	mov    $0x0,%eax
  80057a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80057d:	29 c1                	sub    %eax,%ecx
  80057f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800582:	89 75 08             	mov    %esi,0x8(%ebp)
  800585:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800588:	eb 3d                	jmp    8005c7 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80058a:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80058e:	74 19                	je     8005a9 <vprintfmt+0x20b>
  800590:	0f be c0             	movsbl %al,%eax
  800593:	83 e8 20             	sub    $0x20,%eax
  800596:	83 f8 5e             	cmp    $0x5e,%eax
  800599:	76 0e                	jbe    8005a9 <vprintfmt+0x20b>
					putch('?', putdat);
  80059b:	83 ec 08             	sub    $0x8,%esp
  80059e:	53                   	push   %ebx
  80059f:	6a 3f                	push   $0x3f
  8005a1:	ff 55 08             	call   *0x8(%ebp)
  8005a4:	83 c4 10             	add    $0x10,%esp
  8005a7:	eb 0b                	jmp    8005b4 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8005a9:	83 ec 08             	sub    $0x8,%esp
  8005ac:	53                   	push   %ebx
  8005ad:	52                   	push   %edx
  8005ae:	ff 55 08             	call   *0x8(%ebp)
  8005b1:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005b4:	ff 4d e4             	decl   -0x1c(%ebp)
  8005b7:	eb 0e                	jmp    8005c7 <vprintfmt+0x229>
  8005b9:	89 75 08             	mov    %esi,0x8(%ebp)
  8005bc:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005bf:	eb 06                	jmp    8005c7 <vprintfmt+0x229>
  8005c1:	89 75 08             	mov    %esi,0x8(%ebp)
  8005c4:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005c7:	47                   	inc    %edi
  8005c8:	8a 47 ff             	mov    -0x1(%edi),%al
  8005cb:	0f be d0             	movsbl %al,%edx
  8005ce:	85 d2                	test   %edx,%edx
  8005d0:	74 1d                	je     8005ef <vprintfmt+0x251>
  8005d2:	85 f6                	test   %esi,%esi
  8005d4:	78 b4                	js     80058a <vprintfmt+0x1ec>
  8005d6:	4e                   	dec    %esi
  8005d7:	79 b1                	jns    80058a <vprintfmt+0x1ec>
  8005d9:	8b 75 08             	mov    0x8(%ebp),%esi
  8005dc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005df:	eb 14                	jmp    8005f5 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8005e1:	83 ec 08             	sub    $0x8,%esp
  8005e4:	53                   	push   %ebx
  8005e5:	6a 20                	push   $0x20
  8005e7:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005e9:	4f                   	dec    %edi
  8005ea:	83 c4 10             	add    $0x10,%esp
  8005ed:	eb 06                	jmp    8005f5 <vprintfmt+0x257>
  8005ef:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005f2:	8b 75 08             	mov    0x8(%ebp),%esi
  8005f5:	85 ff                	test   %edi,%edi
  8005f7:	7f e8                	jg     8005e1 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fc:	e9 c3 fd ff ff       	jmp    8003c4 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800601:	83 fa 01             	cmp    $0x1,%edx
  800604:	7e 16                	jle    80061c <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800606:	8b 45 14             	mov    0x14(%ebp),%eax
  800609:	8d 50 08             	lea    0x8(%eax),%edx
  80060c:	89 55 14             	mov    %edx,0x14(%ebp)
  80060f:	8b 50 04             	mov    0x4(%eax),%edx
  800612:	8b 00                	mov    (%eax),%eax
  800614:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800617:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80061a:	eb 32                	jmp    80064e <vprintfmt+0x2b0>
	else if (lflag)
  80061c:	85 d2                	test   %edx,%edx
  80061e:	74 18                	je     800638 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800620:	8b 45 14             	mov    0x14(%ebp),%eax
  800623:	8d 50 04             	lea    0x4(%eax),%edx
  800626:	89 55 14             	mov    %edx,0x14(%ebp)
  800629:	8b 00                	mov    (%eax),%eax
  80062b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80062e:	89 c1                	mov    %eax,%ecx
  800630:	c1 f9 1f             	sar    $0x1f,%ecx
  800633:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800636:	eb 16                	jmp    80064e <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800638:	8b 45 14             	mov    0x14(%ebp),%eax
  80063b:	8d 50 04             	lea    0x4(%eax),%edx
  80063e:	89 55 14             	mov    %edx,0x14(%ebp)
  800641:	8b 00                	mov    (%eax),%eax
  800643:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800646:	89 c1                	mov    %eax,%ecx
  800648:	c1 f9 1f             	sar    $0x1f,%ecx
  80064b:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80064e:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800651:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800654:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800658:	79 76                	jns    8006d0 <vprintfmt+0x332>
				putch('-', putdat);
  80065a:	83 ec 08             	sub    $0x8,%esp
  80065d:	53                   	push   %ebx
  80065e:	6a 2d                	push   $0x2d
  800660:	ff d6                	call   *%esi
				num = -(long long) num;
  800662:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800665:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800668:	f7 d8                	neg    %eax
  80066a:	83 d2 00             	adc    $0x0,%edx
  80066d:	f7 da                	neg    %edx
  80066f:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800672:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800677:	eb 5c                	jmp    8006d5 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800679:	8d 45 14             	lea    0x14(%ebp),%eax
  80067c:	e8 aa fc ff ff       	call   80032b <getuint>
			base = 10;
  800681:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800686:	eb 4d                	jmp    8006d5 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800688:	8d 45 14             	lea    0x14(%ebp),%eax
  80068b:	e8 9b fc ff ff       	call   80032b <getuint>
			base = 8;
  800690:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800695:	eb 3e                	jmp    8006d5 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800697:	83 ec 08             	sub    $0x8,%esp
  80069a:	53                   	push   %ebx
  80069b:	6a 30                	push   $0x30
  80069d:	ff d6                	call   *%esi
			putch('x', putdat);
  80069f:	83 c4 08             	add    $0x8,%esp
  8006a2:	53                   	push   %ebx
  8006a3:	6a 78                	push   $0x78
  8006a5:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8006a7:	8b 45 14             	mov    0x14(%ebp),%eax
  8006aa:	8d 50 04             	lea    0x4(%eax),%edx
  8006ad:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006b0:	8b 00                	mov    (%eax),%eax
  8006b2:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8006b7:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8006ba:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8006bf:	eb 14                	jmp    8006d5 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006c1:	8d 45 14             	lea    0x14(%ebp),%eax
  8006c4:	e8 62 fc ff ff       	call   80032b <getuint>
			base = 16;
  8006c9:	b9 10 00 00 00       	mov    $0x10,%ecx
  8006ce:	eb 05                	jmp    8006d5 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8006d0:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006d5:	83 ec 0c             	sub    $0xc,%esp
  8006d8:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8006dc:	57                   	push   %edi
  8006dd:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006e0:	51                   	push   %ecx
  8006e1:	52                   	push   %edx
  8006e2:	50                   	push   %eax
  8006e3:	89 da                	mov    %ebx,%edx
  8006e5:	89 f0                	mov    %esi,%eax
  8006e7:	e8 92 fb ff ff       	call   80027e <printnum>
			break;
  8006ec:	83 c4 20             	add    $0x20,%esp
  8006ef:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006f2:	e9 cd fc ff ff       	jmp    8003c4 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006f7:	83 ec 08             	sub    $0x8,%esp
  8006fa:	53                   	push   %ebx
  8006fb:	51                   	push   %ecx
  8006fc:	ff d6                	call   *%esi
			break;
  8006fe:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800701:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800704:	e9 bb fc ff ff       	jmp    8003c4 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800709:	83 ec 08             	sub    $0x8,%esp
  80070c:	53                   	push   %ebx
  80070d:	6a 25                	push   $0x25
  80070f:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800711:	83 c4 10             	add    $0x10,%esp
  800714:	eb 01                	jmp    800717 <vprintfmt+0x379>
  800716:	4f                   	dec    %edi
  800717:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80071b:	75 f9                	jne    800716 <vprintfmt+0x378>
  80071d:	e9 a2 fc ff ff       	jmp    8003c4 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800722:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800725:	5b                   	pop    %ebx
  800726:	5e                   	pop    %esi
  800727:	5f                   	pop    %edi
  800728:	5d                   	pop    %ebp
  800729:	c3                   	ret    

0080072a <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80072a:	55                   	push   %ebp
  80072b:	89 e5                	mov    %esp,%ebp
  80072d:	83 ec 18             	sub    $0x18,%esp
  800730:	8b 45 08             	mov    0x8(%ebp),%eax
  800733:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800736:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800739:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80073d:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800740:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800747:	85 c0                	test   %eax,%eax
  800749:	74 26                	je     800771 <vsnprintf+0x47>
  80074b:	85 d2                	test   %edx,%edx
  80074d:	7e 29                	jle    800778 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80074f:	ff 75 14             	pushl  0x14(%ebp)
  800752:	ff 75 10             	pushl  0x10(%ebp)
  800755:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800758:	50                   	push   %eax
  800759:	68 65 03 80 00       	push   $0x800365
  80075e:	e8 3b fc ff ff       	call   80039e <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800763:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800766:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800769:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80076c:	83 c4 10             	add    $0x10,%esp
  80076f:	eb 0c                	jmp    80077d <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800771:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800776:	eb 05                	jmp    80077d <vsnprintf+0x53>
  800778:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80077d:	c9                   	leave  
  80077e:	c3                   	ret    

0080077f <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80077f:	55                   	push   %ebp
  800780:	89 e5                	mov    %esp,%ebp
  800782:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800785:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800788:	50                   	push   %eax
  800789:	ff 75 10             	pushl  0x10(%ebp)
  80078c:	ff 75 0c             	pushl  0xc(%ebp)
  80078f:	ff 75 08             	pushl  0x8(%ebp)
  800792:	e8 93 ff ff ff       	call   80072a <vsnprintf>
	va_end(ap);

	return rc;
}
  800797:	c9                   	leave  
  800798:	c3                   	ret    

00800799 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800799:	55                   	push   %ebp
  80079a:	89 e5                	mov    %esp,%ebp
  80079c:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80079f:	b8 00 00 00 00       	mov    $0x0,%eax
  8007a4:	eb 01                	jmp    8007a7 <strlen+0xe>
		n++;
  8007a6:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8007a7:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8007ab:	75 f9                	jne    8007a6 <strlen+0xd>
		n++;
	return n;
}
  8007ad:	5d                   	pop    %ebp
  8007ae:	c3                   	ret    

008007af <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8007af:	55                   	push   %ebp
  8007b0:	89 e5                	mov    %esp,%ebp
  8007b2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007b5:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007b8:	ba 00 00 00 00       	mov    $0x0,%edx
  8007bd:	eb 01                	jmp    8007c0 <strnlen+0x11>
		n++;
  8007bf:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007c0:	39 c2                	cmp    %eax,%edx
  8007c2:	74 08                	je     8007cc <strnlen+0x1d>
  8007c4:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8007c8:	75 f5                	jne    8007bf <strnlen+0x10>
  8007ca:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8007cc:	5d                   	pop    %ebp
  8007cd:	c3                   	ret    

008007ce <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8007ce:	55                   	push   %ebp
  8007cf:	89 e5                	mov    %esp,%ebp
  8007d1:	53                   	push   %ebx
  8007d2:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8007d8:	89 c2                	mov    %eax,%edx
  8007da:	42                   	inc    %edx
  8007db:	41                   	inc    %ecx
  8007dc:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8007df:	88 5a ff             	mov    %bl,-0x1(%edx)
  8007e2:	84 db                	test   %bl,%bl
  8007e4:	75 f4                	jne    8007da <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8007e6:	5b                   	pop    %ebx
  8007e7:	5d                   	pop    %ebp
  8007e8:	c3                   	ret    

008007e9 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8007e9:	55                   	push   %ebp
  8007ea:	89 e5                	mov    %esp,%ebp
  8007ec:	53                   	push   %ebx
  8007ed:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007f0:	53                   	push   %ebx
  8007f1:	e8 a3 ff ff ff       	call   800799 <strlen>
  8007f6:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007f9:	ff 75 0c             	pushl  0xc(%ebp)
  8007fc:	01 d8                	add    %ebx,%eax
  8007fe:	50                   	push   %eax
  8007ff:	e8 ca ff ff ff       	call   8007ce <strcpy>
	return dst;
}
  800804:	89 d8                	mov    %ebx,%eax
  800806:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800809:	c9                   	leave  
  80080a:	c3                   	ret    

0080080b <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80080b:	55                   	push   %ebp
  80080c:	89 e5                	mov    %esp,%ebp
  80080e:	56                   	push   %esi
  80080f:	53                   	push   %ebx
  800810:	8b 75 08             	mov    0x8(%ebp),%esi
  800813:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800816:	89 f3                	mov    %esi,%ebx
  800818:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80081b:	89 f2                	mov    %esi,%edx
  80081d:	eb 0c                	jmp    80082b <strncpy+0x20>
		*dst++ = *src;
  80081f:	42                   	inc    %edx
  800820:	8a 01                	mov    (%ecx),%al
  800822:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800825:	80 39 01             	cmpb   $0x1,(%ecx)
  800828:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80082b:	39 da                	cmp    %ebx,%edx
  80082d:	75 f0                	jne    80081f <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80082f:	89 f0                	mov    %esi,%eax
  800831:	5b                   	pop    %ebx
  800832:	5e                   	pop    %esi
  800833:	5d                   	pop    %ebp
  800834:	c3                   	ret    

00800835 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800835:	55                   	push   %ebp
  800836:	89 e5                	mov    %esp,%ebp
  800838:	56                   	push   %esi
  800839:	53                   	push   %ebx
  80083a:	8b 75 08             	mov    0x8(%ebp),%esi
  80083d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800840:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800843:	85 c0                	test   %eax,%eax
  800845:	74 1e                	je     800865 <strlcpy+0x30>
  800847:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80084b:	89 f2                	mov    %esi,%edx
  80084d:	eb 05                	jmp    800854 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80084f:	42                   	inc    %edx
  800850:	41                   	inc    %ecx
  800851:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800854:	39 c2                	cmp    %eax,%edx
  800856:	74 08                	je     800860 <strlcpy+0x2b>
  800858:	8a 19                	mov    (%ecx),%bl
  80085a:	84 db                	test   %bl,%bl
  80085c:	75 f1                	jne    80084f <strlcpy+0x1a>
  80085e:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800860:	c6 00 00             	movb   $0x0,(%eax)
  800863:	eb 02                	jmp    800867 <strlcpy+0x32>
  800865:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800867:	29 f0                	sub    %esi,%eax
}
  800869:	5b                   	pop    %ebx
  80086a:	5e                   	pop    %esi
  80086b:	5d                   	pop    %ebp
  80086c:	c3                   	ret    

0080086d <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80086d:	55                   	push   %ebp
  80086e:	89 e5                	mov    %esp,%ebp
  800870:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800873:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800876:	eb 02                	jmp    80087a <strcmp+0xd>
		p++, q++;
  800878:	41                   	inc    %ecx
  800879:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80087a:	8a 01                	mov    (%ecx),%al
  80087c:	84 c0                	test   %al,%al
  80087e:	74 04                	je     800884 <strcmp+0x17>
  800880:	3a 02                	cmp    (%edx),%al
  800882:	74 f4                	je     800878 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800884:	0f b6 c0             	movzbl %al,%eax
  800887:	0f b6 12             	movzbl (%edx),%edx
  80088a:	29 d0                	sub    %edx,%eax
}
  80088c:	5d                   	pop    %ebp
  80088d:	c3                   	ret    

0080088e <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80088e:	55                   	push   %ebp
  80088f:	89 e5                	mov    %esp,%ebp
  800891:	53                   	push   %ebx
  800892:	8b 45 08             	mov    0x8(%ebp),%eax
  800895:	8b 55 0c             	mov    0xc(%ebp),%edx
  800898:	89 c3                	mov    %eax,%ebx
  80089a:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80089d:	eb 02                	jmp    8008a1 <strncmp+0x13>
		n--, p++, q++;
  80089f:	40                   	inc    %eax
  8008a0:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8008a1:	39 d8                	cmp    %ebx,%eax
  8008a3:	74 14                	je     8008b9 <strncmp+0x2b>
  8008a5:	8a 08                	mov    (%eax),%cl
  8008a7:	84 c9                	test   %cl,%cl
  8008a9:	74 04                	je     8008af <strncmp+0x21>
  8008ab:	3a 0a                	cmp    (%edx),%cl
  8008ad:	74 f0                	je     80089f <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8008af:	0f b6 00             	movzbl (%eax),%eax
  8008b2:	0f b6 12             	movzbl (%edx),%edx
  8008b5:	29 d0                	sub    %edx,%eax
  8008b7:	eb 05                	jmp    8008be <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8008b9:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8008be:	5b                   	pop    %ebx
  8008bf:	5d                   	pop    %ebp
  8008c0:	c3                   	ret    

008008c1 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8008c1:	55                   	push   %ebp
  8008c2:	89 e5                	mov    %esp,%ebp
  8008c4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c7:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008ca:	eb 05                	jmp    8008d1 <strchr+0x10>
		if (*s == c)
  8008cc:	38 ca                	cmp    %cl,%dl
  8008ce:	74 0c                	je     8008dc <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8008d0:	40                   	inc    %eax
  8008d1:	8a 10                	mov    (%eax),%dl
  8008d3:	84 d2                	test   %dl,%dl
  8008d5:	75 f5                	jne    8008cc <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8008d7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008dc:	5d                   	pop    %ebp
  8008dd:	c3                   	ret    

008008de <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8008de:	55                   	push   %ebp
  8008df:	89 e5                	mov    %esp,%ebp
  8008e1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008e7:	eb 05                	jmp    8008ee <strfind+0x10>
		if (*s == c)
  8008e9:	38 ca                	cmp    %cl,%dl
  8008eb:	74 07                	je     8008f4 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8008ed:	40                   	inc    %eax
  8008ee:	8a 10                	mov    (%eax),%dl
  8008f0:	84 d2                	test   %dl,%dl
  8008f2:	75 f5                	jne    8008e9 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008f4:	5d                   	pop    %ebp
  8008f5:	c3                   	ret    

008008f6 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008f6:	55                   	push   %ebp
  8008f7:	89 e5                	mov    %esp,%ebp
  8008f9:	57                   	push   %edi
  8008fa:	56                   	push   %esi
  8008fb:	53                   	push   %ebx
  8008fc:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008ff:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800902:	85 c9                	test   %ecx,%ecx
  800904:	74 36                	je     80093c <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800906:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80090c:	75 28                	jne    800936 <memset+0x40>
  80090e:	f6 c1 03             	test   $0x3,%cl
  800911:	75 23                	jne    800936 <memset+0x40>
		c &= 0xFF;
  800913:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800917:	89 d3                	mov    %edx,%ebx
  800919:	c1 e3 08             	shl    $0x8,%ebx
  80091c:	89 d6                	mov    %edx,%esi
  80091e:	c1 e6 18             	shl    $0x18,%esi
  800921:	89 d0                	mov    %edx,%eax
  800923:	c1 e0 10             	shl    $0x10,%eax
  800926:	09 f0                	or     %esi,%eax
  800928:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80092a:	89 d8                	mov    %ebx,%eax
  80092c:	09 d0                	or     %edx,%eax
  80092e:	c1 e9 02             	shr    $0x2,%ecx
  800931:	fc                   	cld    
  800932:	f3 ab                	rep stos %eax,%es:(%edi)
  800934:	eb 06                	jmp    80093c <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800936:	8b 45 0c             	mov    0xc(%ebp),%eax
  800939:	fc                   	cld    
  80093a:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80093c:	89 f8                	mov    %edi,%eax
  80093e:	5b                   	pop    %ebx
  80093f:	5e                   	pop    %esi
  800940:	5f                   	pop    %edi
  800941:	5d                   	pop    %ebp
  800942:	c3                   	ret    

00800943 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800943:	55                   	push   %ebp
  800944:	89 e5                	mov    %esp,%ebp
  800946:	57                   	push   %edi
  800947:	56                   	push   %esi
  800948:	8b 45 08             	mov    0x8(%ebp),%eax
  80094b:	8b 75 0c             	mov    0xc(%ebp),%esi
  80094e:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800951:	39 c6                	cmp    %eax,%esi
  800953:	73 33                	jae    800988 <memmove+0x45>
  800955:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800958:	39 d0                	cmp    %edx,%eax
  80095a:	73 2c                	jae    800988 <memmove+0x45>
		s += n;
		d += n;
  80095c:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80095f:	89 d6                	mov    %edx,%esi
  800961:	09 fe                	or     %edi,%esi
  800963:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800969:	75 13                	jne    80097e <memmove+0x3b>
  80096b:	f6 c1 03             	test   $0x3,%cl
  80096e:	75 0e                	jne    80097e <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800970:	83 ef 04             	sub    $0x4,%edi
  800973:	8d 72 fc             	lea    -0x4(%edx),%esi
  800976:	c1 e9 02             	shr    $0x2,%ecx
  800979:	fd                   	std    
  80097a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80097c:	eb 07                	jmp    800985 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80097e:	4f                   	dec    %edi
  80097f:	8d 72 ff             	lea    -0x1(%edx),%esi
  800982:	fd                   	std    
  800983:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800985:	fc                   	cld    
  800986:	eb 1d                	jmp    8009a5 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800988:	89 f2                	mov    %esi,%edx
  80098a:	09 c2                	or     %eax,%edx
  80098c:	f6 c2 03             	test   $0x3,%dl
  80098f:	75 0f                	jne    8009a0 <memmove+0x5d>
  800991:	f6 c1 03             	test   $0x3,%cl
  800994:	75 0a                	jne    8009a0 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800996:	c1 e9 02             	shr    $0x2,%ecx
  800999:	89 c7                	mov    %eax,%edi
  80099b:	fc                   	cld    
  80099c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80099e:	eb 05                	jmp    8009a5 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8009a0:	89 c7                	mov    %eax,%edi
  8009a2:	fc                   	cld    
  8009a3:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8009a5:	5e                   	pop    %esi
  8009a6:	5f                   	pop    %edi
  8009a7:	5d                   	pop    %ebp
  8009a8:	c3                   	ret    

008009a9 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8009a9:	55                   	push   %ebp
  8009aa:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8009ac:	ff 75 10             	pushl  0x10(%ebp)
  8009af:	ff 75 0c             	pushl  0xc(%ebp)
  8009b2:	ff 75 08             	pushl  0x8(%ebp)
  8009b5:	e8 89 ff ff ff       	call   800943 <memmove>
}
  8009ba:	c9                   	leave  
  8009bb:	c3                   	ret    

008009bc <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8009bc:	55                   	push   %ebp
  8009bd:	89 e5                	mov    %esp,%ebp
  8009bf:	56                   	push   %esi
  8009c0:	53                   	push   %ebx
  8009c1:	8b 45 08             	mov    0x8(%ebp),%eax
  8009c4:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009c7:	89 c6                	mov    %eax,%esi
  8009c9:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009cc:	eb 14                	jmp    8009e2 <memcmp+0x26>
		if (*s1 != *s2)
  8009ce:	8a 08                	mov    (%eax),%cl
  8009d0:	8a 1a                	mov    (%edx),%bl
  8009d2:	38 d9                	cmp    %bl,%cl
  8009d4:	74 0a                	je     8009e0 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8009d6:	0f b6 c1             	movzbl %cl,%eax
  8009d9:	0f b6 db             	movzbl %bl,%ebx
  8009dc:	29 d8                	sub    %ebx,%eax
  8009de:	eb 0b                	jmp    8009eb <memcmp+0x2f>
		s1++, s2++;
  8009e0:	40                   	inc    %eax
  8009e1:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009e2:	39 f0                	cmp    %esi,%eax
  8009e4:	75 e8                	jne    8009ce <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8009e6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009eb:	5b                   	pop    %ebx
  8009ec:	5e                   	pop    %esi
  8009ed:	5d                   	pop    %ebp
  8009ee:	c3                   	ret    

008009ef <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009ef:	55                   	push   %ebp
  8009f0:	89 e5                	mov    %esp,%ebp
  8009f2:	53                   	push   %ebx
  8009f3:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009f6:	89 c1                	mov    %eax,%ecx
  8009f8:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009fb:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009ff:	eb 08                	jmp    800a09 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800a01:	0f b6 10             	movzbl (%eax),%edx
  800a04:	39 da                	cmp    %ebx,%edx
  800a06:	74 05                	je     800a0d <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800a08:	40                   	inc    %eax
  800a09:	39 c8                	cmp    %ecx,%eax
  800a0b:	72 f4                	jb     800a01 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800a0d:	5b                   	pop    %ebx
  800a0e:	5d                   	pop    %ebp
  800a0f:	c3                   	ret    

00800a10 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800a10:	55                   	push   %ebp
  800a11:	89 e5                	mov    %esp,%ebp
  800a13:	57                   	push   %edi
  800a14:	56                   	push   %esi
  800a15:	53                   	push   %ebx
  800a16:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a19:	eb 01                	jmp    800a1c <strtol+0xc>
		s++;
  800a1b:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800a1c:	8a 01                	mov    (%ecx),%al
  800a1e:	3c 20                	cmp    $0x20,%al
  800a20:	74 f9                	je     800a1b <strtol+0xb>
  800a22:	3c 09                	cmp    $0x9,%al
  800a24:	74 f5                	je     800a1b <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800a26:	3c 2b                	cmp    $0x2b,%al
  800a28:	75 08                	jne    800a32 <strtol+0x22>
		s++;
  800a2a:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a2b:	bf 00 00 00 00       	mov    $0x0,%edi
  800a30:	eb 11                	jmp    800a43 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800a32:	3c 2d                	cmp    $0x2d,%al
  800a34:	75 08                	jne    800a3e <strtol+0x2e>
		s++, neg = 1;
  800a36:	41                   	inc    %ecx
  800a37:	bf 01 00 00 00       	mov    $0x1,%edi
  800a3c:	eb 05                	jmp    800a43 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a3e:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a43:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a47:	0f 84 87 00 00 00    	je     800ad4 <strtol+0xc4>
  800a4d:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a51:	75 27                	jne    800a7a <strtol+0x6a>
  800a53:	80 39 30             	cmpb   $0x30,(%ecx)
  800a56:	75 22                	jne    800a7a <strtol+0x6a>
  800a58:	e9 88 00 00 00       	jmp    800ae5 <strtol+0xd5>
		s += 2, base = 16;
  800a5d:	83 c1 02             	add    $0x2,%ecx
  800a60:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a67:	eb 11                	jmp    800a7a <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a69:	41                   	inc    %ecx
  800a6a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a71:	eb 07                	jmp    800a7a <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a73:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a7a:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a7f:	8a 11                	mov    (%ecx),%dl
  800a81:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a84:	80 fb 09             	cmp    $0x9,%bl
  800a87:	77 08                	ja     800a91 <strtol+0x81>
			dig = *s - '0';
  800a89:	0f be d2             	movsbl %dl,%edx
  800a8c:	83 ea 30             	sub    $0x30,%edx
  800a8f:	eb 22                	jmp    800ab3 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a91:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a94:	89 f3                	mov    %esi,%ebx
  800a96:	80 fb 19             	cmp    $0x19,%bl
  800a99:	77 08                	ja     800aa3 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a9b:	0f be d2             	movsbl %dl,%edx
  800a9e:	83 ea 57             	sub    $0x57,%edx
  800aa1:	eb 10                	jmp    800ab3 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800aa3:	8d 72 bf             	lea    -0x41(%edx),%esi
  800aa6:	89 f3                	mov    %esi,%ebx
  800aa8:	80 fb 19             	cmp    $0x19,%bl
  800aab:	77 14                	ja     800ac1 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800aad:	0f be d2             	movsbl %dl,%edx
  800ab0:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800ab3:	3b 55 10             	cmp    0x10(%ebp),%edx
  800ab6:	7d 09                	jge    800ac1 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800ab8:	41                   	inc    %ecx
  800ab9:	0f af 45 10          	imul   0x10(%ebp),%eax
  800abd:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800abf:	eb be                	jmp    800a7f <strtol+0x6f>

	if (endptr)
  800ac1:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ac5:	74 05                	je     800acc <strtol+0xbc>
		*endptr = (char *) s;
  800ac7:	8b 75 0c             	mov    0xc(%ebp),%esi
  800aca:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800acc:	85 ff                	test   %edi,%edi
  800ace:	74 21                	je     800af1 <strtol+0xe1>
  800ad0:	f7 d8                	neg    %eax
  800ad2:	eb 1d                	jmp    800af1 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ad4:	80 39 30             	cmpb   $0x30,(%ecx)
  800ad7:	75 9a                	jne    800a73 <strtol+0x63>
  800ad9:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800add:	0f 84 7a ff ff ff    	je     800a5d <strtol+0x4d>
  800ae3:	eb 84                	jmp    800a69 <strtol+0x59>
  800ae5:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800ae9:	0f 84 6e ff ff ff    	je     800a5d <strtol+0x4d>
  800aef:	eb 89                	jmp    800a7a <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800af1:	5b                   	pop    %ebx
  800af2:	5e                   	pop    %esi
  800af3:	5f                   	pop    %edi
  800af4:	5d                   	pop    %ebp
  800af5:	c3                   	ret    

00800af6 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800af6:	55                   	push   %ebp
  800af7:	89 e5                	mov    %esp,%ebp
  800af9:	57                   	push   %edi
  800afa:	56                   	push   %esi
  800afb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800afc:	b8 00 00 00 00       	mov    $0x0,%eax
  800b01:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b04:	8b 55 08             	mov    0x8(%ebp),%edx
  800b07:	89 c3                	mov    %eax,%ebx
  800b09:	89 c7                	mov    %eax,%edi
  800b0b:	89 c6                	mov    %eax,%esi
  800b0d:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800b0f:	5b                   	pop    %ebx
  800b10:	5e                   	pop    %esi
  800b11:	5f                   	pop    %edi
  800b12:	5d                   	pop    %ebp
  800b13:	c3                   	ret    

00800b14 <sys_cgetc>:

int
sys_cgetc(void)
{
  800b14:	55                   	push   %ebp
  800b15:	89 e5                	mov    %esp,%ebp
  800b17:	57                   	push   %edi
  800b18:	56                   	push   %esi
  800b19:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1a:	ba 00 00 00 00       	mov    $0x0,%edx
  800b1f:	b8 01 00 00 00       	mov    $0x1,%eax
  800b24:	89 d1                	mov    %edx,%ecx
  800b26:	89 d3                	mov    %edx,%ebx
  800b28:	89 d7                	mov    %edx,%edi
  800b2a:	89 d6                	mov    %edx,%esi
  800b2c:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800b2e:	5b                   	pop    %ebx
  800b2f:	5e                   	pop    %esi
  800b30:	5f                   	pop    %edi
  800b31:	5d                   	pop    %ebp
  800b32:	c3                   	ret    

00800b33 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800b33:	55                   	push   %ebp
  800b34:	89 e5                	mov    %esp,%ebp
  800b36:	57                   	push   %edi
  800b37:	56                   	push   %esi
  800b38:	53                   	push   %ebx
  800b39:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b3c:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b41:	b8 03 00 00 00       	mov    $0x3,%eax
  800b46:	8b 55 08             	mov    0x8(%ebp),%edx
  800b49:	89 cb                	mov    %ecx,%ebx
  800b4b:	89 cf                	mov    %ecx,%edi
  800b4d:	89 ce                	mov    %ecx,%esi
  800b4f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b51:	85 c0                	test   %eax,%eax
  800b53:	7e 17                	jle    800b6c <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b55:	83 ec 0c             	sub    $0xc,%esp
  800b58:	50                   	push   %eax
  800b59:	6a 03                	push   $0x3
  800b5b:	68 9f 22 80 00       	push   $0x80229f
  800b60:	6a 23                	push   $0x23
  800b62:	68 bc 22 80 00       	push   $0x8022bc
  800b67:	e8 26 f6 ff ff       	call   800192 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b6c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b6f:	5b                   	pop    %ebx
  800b70:	5e                   	pop    %esi
  800b71:	5f                   	pop    %edi
  800b72:	5d                   	pop    %ebp
  800b73:	c3                   	ret    

00800b74 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b74:	55                   	push   %ebp
  800b75:	89 e5                	mov    %esp,%ebp
  800b77:	57                   	push   %edi
  800b78:	56                   	push   %esi
  800b79:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b7a:	ba 00 00 00 00       	mov    $0x0,%edx
  800b7f:	b8 02 00 00 00       	mov    $0x2,%eax
  800b84:	89 d1                	mov    %edx,%ecx
  800b86:	89 d3                	mov    %edx,%ebx
  800b88:	89 d7                	mov    %edx,%edi
  800b8a:	89 d6                	mov    %edx,%esi
  800b8c:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b8e:	5b                   	pop    %ebx
  800b8f:	5e                   	pop    %esi
  800b90:	5f                   	pop    %edi
  800b91:	5d                   	pop    %ebp
  800b92:	c3                   	ret    

00800b93 <sys_yield>:

void
sys_yield(void)
{
  800b93:	55                   	push   %ebp
  800b94:	89 e5                	mov    %esp,%ebp
  800b96:	57                   	push   %edi
  800b97:	56                   	push   %esi
  800b98:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b99:	ba 00 00 00 00       	mov    $0x0,%edx
  800b9e:	b8 0b 00 00 00       	mov    $0xb,%eax
  800ba3:	89 d1                	mov    %edx,%ecx
  800ba5:	89 d3                	mov    %edx,%ebx
  800ba7:	89 d7                	mov    %edx,%edi
  800ba9:	89 d6                	mov    %edx,%esi
  800bab:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800bad:	5b                   	pop    %ebx
  800bae:	5e                   	pop    %esi
  800baf:	5f                   	pop    %edi
  800bb0:	5d                   	pop    %ebp
  800bb1:	c3                   	ret    

00800bb2 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800bb2:	55                   	push   %ebp
  800bb3:	89 e5                	mov    %esp,%ebp
  800bb5:	57                   	push   %edi
  800bb6:	56                   	push   %esi
  800bb7:	53                   	push   %ebx
  800bb8:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bbb:	be 00 00 00 00       	mov    $0x0,%esi
  800bc0:	b8 04 00 00 00       	mov    $0x4,%eax
  800bc5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bc8:	8b 55 08             	mov    0x8(%ebp),%edx
  800bcb:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800bce:	89 f7                	mov    %esi,%edi
  800bd0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800bd2:	85 c0                	test   %eax,%eax
  800bd4:	7e 17                	jle    800bed <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800bd6:	83 ec 0c             	sub    $0xc,%esp
  800bd9:	50                   	push   %eax
  800bda:	6a 04                	push   $0x4
  800bdc:	68 9f 22 80 00       	push   $0x80229f
  800be1:	6a 23                	push   $0x23
  800be3:	68 bc 22 80 00       	push   $0x8022bc
  800be8:	e8 a5 f5 ff ff       	call   800192 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800bed:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bf0:	5b                   	pop    %ebx
  800bf1:	5e                   	pop    %esi
  800bf2:	5f                   	pop    %edi
  800bf3:	5d                   	pop    %ebp
  800bf4:	c3                   	ret    

00800bf5 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800bf5:	55                   	push   %ebp
  800bf6:	89 e5                	mov    %esp,%ebp
  800bf8:	57                   	push   %edi
  800bf9:	56                   	push   %esi
  800bfa:	53                   	push   %ebx
  800bfb:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bfe:	b8 05 00 00 00       	mov    $0x5,%eax
  800c03:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c06:	8b 55 08             	mov    0x8(%ebp),%edx
  800c09:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800c0c:	8b 7d 14             	mov    0x14(%ebp),%edi
  800c0f:	8b 75 18             	mov    0x18(%ebp),%esi
  800c12:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c14:	85 c0                	test   %eax,%eax
  800c16:	7e 17                	jle    800c2f <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c18:	83 ec 0c             	sub    $0xc,%esp
  800c1b:	50                   	push   %eax
  800c1c:	6a 05                	push   $0x5
  800c1e:	68 9f 22 80 00       	push   $0x80229f
  800c23:	6a 23                	push   $0x23
  800c25:	68 bc 22 80 00       	push   $0x8022bc
  800c2a:	e8 63 f5 ff ff       	call   800192 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800c2f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c32:	5b                   	pop    %ebx
  800c33:	5e                   	pop    %esi
  800c34:	5f                   	pop    %edi
  800c35:	5d                   	pop    %ebp
  800c36:	c3                   	ret    

00800c37 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800c37:	55                   	push   %ebp
  800c38:	89 e5                	mov    %esp,%ebp
  800c3a:	57                   	push   %edi
  800c3b:	56                   	push   %esi
  800c3c:	53                   	push   %ebx
  800c3d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c40:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c45:	b8 06 00 00 00       	mov    $0x6,%eax
  800c4a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c4d:	8b 55 08             	mov    0x8(%ebp),%edx
  800c50:	89 df                	mov    %ebx,%edi
  800c52:	89 de                	mov    %ebx,%esi
  800c54:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c56:	85 c0                	test   %eax,%eax
  800c58:	7e 17                	jle    800c71 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c5a:	83 ec 0c             	sub    $0xc,%esp
  800c5d:	50                   	push   %eax
  800c5e:	6a 06                	push   $0x6
  800c60:	68 9f 22 80 00       	push   $0x80229f
  800c65:	6a 23                	push   $0x23
  800c67:	68 bc 22 80 00       	push   $0x8022bc
  800c6c:	e8 21 f5 ff ff       	call   800192 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800c71:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c74:	5b                   	pop    %ebx
  800c75:	5e                   	pop    %esi
  800c76:	5f                   	pop    %edi
  800c77:	5d                   	pop    %ebp
  800c78:	c3                   	ret    

00800c79 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800c79:	55                   	push   %ebp
  800c7a:	89 e5                	mov    %esp,%ebp
  800c7c:	57                   	push   %edi
  800c7d:	56                   	push   %esi
  800c7e:	53                   	push   %ebx
  800c7f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c82:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c87:	b8 08 00 00 00       	mov    $0x8,%eax
  800c8c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c8f:	8b 55 08             	mov    0x8(%ebp),%edx
  800c92:	89 df                	mov    %ebx,%edi
  800c94:	89 de                	mov    %ebx,%esi
  800c96:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c98:	85 c0                	test   %eax,%eax
  800c9a:	7e 17                	jle    800cb3 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c9c:	83 ec 0c             	sub    $0xc,%esp
  800c9f:	50                   	push   %eax
  800ca0:	6a 08                	push   $0x8
  800ca2:	68 9f 22 80 00       	push   $0x80229f
  800ca7:	6a 23                	push   $0x23
  800ca9:	68 bc 22 80 00       	push   $0x8022bc
  800cae:	e8 df f4 ff ff       	call   800192 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800cb3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800cb6:	5b                   	pop    %ebx
  800cb7:	5e                   	pop    %esi
  800cb8:	5f                   	pop    %edi
  800cb9:	5d                   	pop    %ebp
  800cba:	c3                   	ret    

00800cbb <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800cbb:	55                   	push   %ebp
  800cbc:	89 e5                	mov    %esp,%ebp
  800cbe:	57                   	push   %edi
  800cbf:	56                   	push   %esi
  800cc0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cc1:	ba 00 00 00 00       	mov    $0x0,%edx
  800cc6:	b8 0c 00 00 00       	mov    $0xc,%eax
  800ccb:	89 d1                	mov    %edx,%ecx
  800ccd:	89 d3                	mov    %edx,%ebx
  800ccf:	89 d7                	mov    %edx,%edi
  800cd1:	89 d6                	mov    %edx,%esi
  800cd3:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800cd5:	5b                   	pop    %ebx
  800cd6:	5e                   	pop    %esi
  800cd7:	5f                   	pop    %edi
  800cd8:	5d                   	pop    %ebp
  800cd9:	c3                   	ret    

00800cda <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800cda:	55                   	push   %ebp
  800cdb:	89 e5                	mov    %esp,%ebp
  800cdd:	57                   	push   %edi
  800cde:	56                   	push   %esi
  800cdf:	53                   	push   %ebx
  800ce0:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ce3:	bb 00 00 00 00       	mov    $0x0,%ebx
  800ce8:	b8 09 00 00 00       	mov    $0x9,%eax
  800ced:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cf0:	8b 55 08             	mov    0x8(%ebp),%edx
  800cf3:	89 df                	mov    %ebx,%edi
  800cf5:	89 de                	mov    %ebx,%esi
  800cf7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800cf9:	85 c0                	test   %eax,%eax
  800cfb:	7e 17                	jle    800d14 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800cfd:	83 ec 0c             	sub    $0xc,%esp
  800d00:	50                   	push   %eax
  800d01:	6a 09                	push   $0x9
  800d03:	68 9f 22 80 00       	push   $0x80229f
  800d08:	6a 23                	push   $0x23
  800d0a:	68 bc 22 80 00       	push   $0x8022bc
  800d0f:	e8 7e f4 ff ff       	call   800192 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800d14:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d17:	5b                   	pop    %ebx
  800d18:	5e                   	pop    %esi
  800d19:	5f                   	pop    %edi
  800d1a:	5d                   	pop    %ebp
  800d1b:	c3                   	ret    

00800d1c <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800d1c:	55                   	push   %ebp
  800d1d:	89 e5                	mov    %esp,%ebp
  800d1f:	57                   	push   %edi
  800d20:	56                   	push   %esi
  800d21:	53                   	push   %ebx
  800d22:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d25:	bb 00 00 00 00       	mov    $0x0,%ebx
  800d2a:	b8 0a 00 00 00       	mov    $0xa,%eax
  800d2f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d32:	8b 55 08             	mov    0x8(%ebp),%edx
  800d35:	89 df                	mov    %ebx,%edi
  800d37:	89 de                	mov    %ebx,%esi
  800d39:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d3b:	85 c0                	test   %eax,%eax
  800d3d:	7e 17                	jle    800d56 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800d3f:	83 ec 0c             	sub    $0xc,%esp
  800d42:	50                   	push   %eax
  800d43:	6a 0a                	push   $0xa
  800d45:	68 9f 22 80 00       	push   $0x80229f
  800d4a:	6a 23                	push   $0x23
  800d4c:	68 bc 22 80 00       	push   $0x8022bc
  800d51:	e8 3c f4 ff ff       	call   800192 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800d56:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d59:	5b                   	pop    %ebx
  800d5a:	5e                   	pop    %esi
  800d5b:	5f                   	pop    %edi
  800d5c:	5d                   	pop    %ebp
  800d5d:	c3                   	ret    

00800d5e <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800d5e:	55                   	push   %ebp
  800d5f:	89 e5                	mov    %esp,%ebp
  800d61:	57                   	push   %edi
  800d62:	56                   	push   %esi
  800d63:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d64:	be 00 00 00 00       	mov    $0x0,%esi
  800d69:	b8 0d 00 00 00       	mov    $0xd,%eax
  800d6e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800d71:	8b 55 08             	mov    0x8(%ebp),%edx
  800d74:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800d77:	8b 7d 14             	mov    0x14(%ebp),%edi
  800d7a:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800d7c:	5b                   	pop    %ebx
  800d7d:	5e                   	pop    %esi
  800d7e:	5f                   	pop    %edi
  800d7f:	5d                   	pop    %ebp
  800d80:	c3                   	ret    

00800d81 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800d81:	55                   	push   %ebp
  800d82:	89 e5                	mov    %esp,%ebp
  800d84:	57                   	push   %edi
  800d85:	56                   	push   %esi
  800d86:	53                   	push   %ebx
  800d87:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d8a:	b9 00 00 00 00       	mov    $0x0,%ecx
  800d8f:	b8 0e 00 00 00       	mov    $0xe,%eax
  800d94:	8b 55 08             	mov    0x8(%ebp),%edx
  800d97:	89 cb                	mov    %ecx,%ebx
  800d99:	89 cf                	mov    %ecx,%edi
  800d9b:	89 ce                	mov    %ecx,%esi
  800d9d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800d9f:	85 c0                	test   %eax,%eax
  800da1:	7e 17                	jle    800dba <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800da3:	83 ec 0c             	sub    $0xc,%esp
  800da6:	50                   	push   %eax
  800da7:	6a 0e                	push   $0xe
  800da9:	68 9f 22 80 00       	push   $0x80229f
  800dae:	6a 23                	push   $0x23
  800db0:	68 bc 22 80 00       	push   $0x8022bc
  800db5:	e8 d8 f3 ff ff       	call   800192 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800dba:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800dbd:	5b                   	pop    %ebx
  800dbe:	5e                   	pop    %esi
  800dbf:	5f                   	pop    %edi
  800dc0:	5d                   	pop    %ebp
  800dc1:	c3                   	ret    

00800dc2 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  800dc2:	55                   	push   %ebp
  800dc3:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  800dc5:	8b 45 08             	mov    0x8(%ebp),%eax
  800dc8:	05 00 00 00 30       	add    $0x30000000,%eax
  800dcd:	c1 e8 0c             	shr    $0xc,%eax
}
  800dd0:	5d                   	pop    %ebp
  800dd1:	c3                   	ret    

00800dd2 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  800dd2:	55                   	push   %ebp
  800dd3:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  800dd5:	8b 45 08             	mov    0x8(%ebp),%eax
  800dd8:	05 00 00 00 30       	add    $0x30000000,%eax
  800ddd:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  800de2:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  800de7:	5d                   	pop    %ebp
  800de8:	c3                   	ret    

00800de9 <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  800de9:	55                   	push   %ebp
  800dea:	89 e5                	mov    %esp,%ebp
  800dec:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800def:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  800df4:	89 c2                	mov    %eax,%edx
  800df6:	c1 ea 16             	shr    $0x16,%edx
  800df9:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800e00:	f6 c2 01             	test   $0x1,%dl
  800e03:	74 11                	je     800e16 <fd_alloc+0x2d>
  800e05:	89 c2                	mov    %eax,%edx
  800e07:	c1 ea 0c             	shr    $0xc,%edx
  800e0a:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800e11:	f6 c2 01             	test   $0x1,%dl
  800e14:	75 09                	jne    800e1f <fd_alloc+0x36>
			*fd_store = fd;
  800e16:	89 01                	mov    %eax,(%ecx)
			return 0;
  800e18:	b8 00 00 00 00       	mov    $0x0,%eax
  800e1d:	eb 17                	jmp    800e36 <fd_alloc+0x4d>
  800e1f:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  800e24:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  800e29:	75 c9                	jne    800df4 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  800e2b:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  800e31:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  800e36:	5d                   	pop    %ebp
  800e37:	c3                   	ret    

00800e38 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  800e38:	55                   	push   %ebp
  800e39:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  800e3b:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  800e3f:	77 39                	ja     800e7a <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  800e41:	8b 45 08             	mov    0x8(%ebp),%eax
  800e44:	c1 e0 0c             	shl    $0xc,%eax
  800e47:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  800e4c:	89 c2                	mov    %eax,%edx
  800e4e:	c1 ea 16             	shr    $0x16,%edx
  800e51:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  800e58:	f6 c2 01             	test   $0x1,%dl
  800e5b:	74 24                	je     800e81 <fd_lookup+0x49>
  800e5d:	89 c2                	mov    %eax,%edx
  800e5f:	c1 ea 0c             	shr    $0xc,%edx
  800e62:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  800e69:	f6 c2 01             	test   $0x1,%dl
  800e6c:	74 1a                	je     800e88 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  800e6e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e71:	89 02                	mov    %eax,(%edx)
	return 0;
  800e73:	b8 00 00 00 00       	mov    $0x0,%eax
  800e78:	eb 13                	jmp    800e8d <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800e7a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800e7f:	eb 0c                	jmp    800e8d <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  800e81:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800e86:	eb 05                	jmp    800e8d <fd_lookup+0x55>
  800e88:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  800e8d:	5d                   	pop    %ebp
  800e8e:	c3                   	ret    

00800e8f <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  800e8f:	55                   	push   %ebp
  800e90:	89 e5                	mov    %esp,%ebp
  800e92:	83 ec 08             	sub    $0x8,%esp
  800e95:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800e98:	ba 4c 23 80 00       	mov    $0x80234c,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  800e9d:	eb 13                	jmp    800eb2 <dev_lookup+0x23>
  800e9f:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  800ea2:	39 08                	cmp    %ecx,(%eax)
  800ea4:	75 0c                	jne    800eb2 <dev_lookup+0x23>
			*dev = devtab[i];
  800ea6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ea9:	89 01                	mov    %eax,(%ecx)
			return 0;
  800eab:	b8 00 00 00 00       	mov    $0x0,%eax
  800eb0:	eb 2e                	jmp    800ee0 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  800eb2:	8b 02                	mov    (%edx),%eax
  800eb4:	85 c0                	test   %eax,%eax
  800eb6:	75 e7                	jne    800e9f <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  800eb8:	a1 20 60 80 00       	mov    0x806020,%eax
  800ebd:	8b 40 48             	mov    0x48(%eax),%eax
  800ec0:	83 ec 04             	sub    $0x4,%esp
  800ec3:	51                   	push   %ecx
  800ec4:	50                   	push   %eax
  800ec5:	68 cc 22 80 00       	push   $0x8022cc
  800eca:	e8 9b f3 ff ff       	call   80026a <cprintf>
	*dev = 0;
  800ecf:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ed2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  800ed8:	83 c4 10             	add    $0x10,%esp
  800edb:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  800ee0:	c9                   	leave  
  800ee1:	c3                   	ret    

00800ee2 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  800ee2:	55                   	push   %ebp
  800ee3:	89 e5                	mov    %esp,%ebp
  800ee5:	56                   	push   %esi
  800ee6:	53                   	push   %ebx
  800ee7:	83 ec 10             	sub    $0x10,%esp
  800eea:	8b 75 08             	mov    0x8(%ebp),%esi
  800eed:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  800ef0:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800ef3:	50                   	push   %eax
  800ef4:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  800efa:	c1 e8 0c             	shr    $0xc,%eax
  800efd:	50                   	push   %eax
  800efe:	e8 35 ff ff ff       	call   800e38 <fd_lookup>
  800f03:	83 c4 08             	add    $0x8,%esp
  800f06:	85 c0                	test   %eax,%eax
  800f08:	78 05                	js     800f0f <fd_close+0x2d>
	    || fd != fd2)
  800f0a:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  800f0d:	74 06                	je     800f15 <fd_close+0x33>
		return (must_exist ? r : 0);
  800f0f:	84 db                	test   %bl,%bl
  800f11:	74 47                	je     800f5a <fd_close+0x78>
  800f13:	eb 4a                	jmp    800f5f <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  800f15:	83 ec 08             	sub    $0x8,%esp
  800f18:	8d 45 f0             	lea    -0x10(%ebp),%eax
  800f1b:	50                   	push   %eax
  800f1c:	ff 36                	pushl  (%esi)
  800f1e:	e8 6c ff ff ff       	call   800e8f <dev_lookup>
  800f23:	89 c3                	mov    %eax,%ebx
  800f25:	83 c4 10             	add    $0x10,%esp
  800f28:	85 c0                	test   %eax,%eax
  800f2a:	78 1c                	js     800f48 <fd_close+0x66>
		if (dev->dev_close)
  800f2c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800f2f:	8b 40 10             	mov    0x10(%eax),%eax
  800f32:	85 c0                	test   %eax,%eax
  800f34:	74 0d                	je     800f43 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  800f36:	83 ec 0c             	sub    $0xc,%esp
  800f39:	56                   	push   %esi
  800f3a:	ff d0                	call   *%eax
  800f3c:	89 c3                	mov    %eax,%ebx
  800f3e:	83 c4 10             	add    $0x10,%esp
  800f41:	eb 05                	jmp    800f48 <fd_close+0x66>
		else
			r = 0;
  800f43:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  800f48:	83 ec 08             	sub    $0x8,%esp
  800f4b:	56                   	push   %esi
  800f4c:	6a 00                	push   $0x0
  800f4e:	e8 e4 fc ff ff       	call   800c37 <sys_page_unmap>
	return r;
  800f53:	83 c4 10             	add    $0x10,%esp
  800f56:	89 d8                	mov    %ebx,%eax
  800f58:	eb 05                	jmp    800f5f <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  800f5a:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  800f5f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800f62:	5b                   	pop    %ebx
  800f63:	5e                   	pop    %esi
  800f64:	5d                   	pop    %ebp
  800f65:	c3                   	ret    

00800f66 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  800f66:	55                   	push   %ebp
  800f67:	89 e5                	mov    %esp,%ebp
  800f69:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  800f6c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  800f6f:	50                   	push   %eax
  800f70:	ff 75 08             	pushl  0x8(%ebp)
  800f73:	e8 c0 fe ff ff       	call   800e38 <fd_lookup>
  800f78:	83 c4 08             	add    $0x8,%esp
  800f7b:	85 c0                	test   %eax,%eax
  800f7d:	78 10                	js     800f8f <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  800f7f:	83 ec 08             	sub    $0x8,%esp
  800f82:	6a 01                	push   $0x1
  800f84:	ff 75 f4             	pushl  -0xc(%ebp)
  800f87:	e8 56 ff ff ff       	call   800ee2 <fd_close>
  800f8c:	83 c4 10             	add    $0x10,%esp
}
  800f8f:	c9                   	leave  
  800f90:	c3                   	ret    

00800f91 <close_all>:

void
close_all(void)
{
  800f91:	55                   	push   %ebp
  800f92:	89 e5                	mov    %esp,%ebp
  800f94:	53                   	push   %ebx
  800f95:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  800f98:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  800f9d:	83 ec 0c             	sub    $0xc,%esp
  800fa0:	53                   	push   %ebx
  800fa1:	e8 c0 ff ff ff       	call   800f66 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  800fa6:	43                   	inc    %ebx
  800fa7:	83 c4 10             	add    $0x10,%esp
  800faa:	83 fb 20             	cmp    $0x20,%ebx
  800fad:	75 ee                	jne    800f9d <close_all+0xc>
		close(i);
}
  800faf:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800fb2:	c9                   	leave  
  800fb3:	c3                   	ret    

00800fb4 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  800fb4:	55                   	push   %ebp
  800fb5:	89 e5                	mov    %esp,%ebp
  800fb7:	57                   	push   %edi
  800fb8:	56                   	push   %esi
  800fb9:	53                   	push   %ebx
  800fba:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  800fbd:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  800fc0:	50                   	push   %eax
  800fc1:	ff 75 08             	pushl  0x8(%ebp)
  800fc4:	e8 6f fe ff ff       	call   800e38 <fd_lookup>
  800fc9:	83 c4 08             	add    $0x8,%esp
  800fcc:	85 c0                	test   %eax,%eax
  800fce:	0f 88 c2 00 00 00    	js     801096 <dup+0xe2>
		return r;
	close(newfdnum);
  800fd4:	83 ec 0c             	sub    $0xc,%esp
  800fd7:	ff 75 0c             	pushl  0xc(%ebp)
  800fda:	e8 87 ff ff ff       	call   800f66 <close>

	newfd = INDEX2FD(newfdnum);
  800fdf:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800fe2:	c1 e3 0c             	shl    $0xc,%ebx
  800fe5:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  800feb:	83 c4 04             	add    $0x4,%esp
  800fee:	ff 75 e4             	pushl  -0x1c(%ebp)
  800ff1:	e8 dc fd ff ff       	call   800dd2 <fd2data>
  800ff6:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  800ff8:	89 1c 24             	mov    %ebx,(%esp)
  800ffb:	e8 d2 fd ff ff       	call   800dd2 <fd2data>
  801000:	83 c4 10             	add    $0x10,%esp
  801003:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801005:	89 f0                	mov    %esi,%eax
  801007:	c1 e8 16             	shr    $0x16,%eax
  80100a:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801011:	a8 01                	test   $0x1,%al
  801013:	74 35                	je     80104a <dup+0x96>
  801015:	89 f0                	mov    %esi,%eax
  801017:	c1 e8 0c             	shr    $0xc,%eax
  80101a:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801021:	f6 c2 01             	test   $0x1,%dl
  801024:	74 24                	je     80104a <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801026:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80102d:	83 ec 0c             	sub    $0xc,%esp
  801030:	25 07 0e 00 00       	and    $0xe07,%eax
  801035:	50                   	push   %eax
  801036:	57                   	push   %edi
  801037:	6a 00                	push   $0x0
  801039:	56                   	push   %esi
  80103a:	6a 00                	push   $0x0
  80103c:	e8 b4 fb ff ff       	call   800bf5 <sys_page_map>
  801041:	89 c6                	mov    %eax,%esi
  801043:	83 c4 20             	add    $0x20,%esp
  801046:	85 c0                	test   %eax,%eax
  801048:	78 2c                	js     801076 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  80104a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  80104d:	89 d0                	mov    %edx,%eax
  80104f:	c1 e8 0c             	shr    $0xc,%eax
  801052:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  801059:	83 ec 0c             	sub    $0xc,%esp
  80105c:	25 07 0e 00 00       	and    $0xe07,%eax
  801061:	50                   	push   %eax
  801062:	53                   	push   %ebx
  801063:	6a 00                	push   $0x0
  801065:	52                   	push   %edx
  801066:	6a 00                	push   $0x0
  801068:	e8 88 fb ff ff       	call   800bf5 <sys_page_map>
  80106d:	89 c6                	mov    %eax,%esi
  80106f:	83 c4 20             	add    $0x20,%esp
  801072:	85 c0                	test   %eax,%eax
  801074:	79 1d                	jns    801093 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  801076:	83 ec 08             	sub    $0x8,%esp
  801079:	53                   	push   %ebx
  80107a:	6a 00                	push   $0x0
  80107c:	e8 b6 fb ff ff       	call   800c37 <sys_page_unmap>
	sys_page_unmap(0, nva);
  801081:	83 c4 08             	add    $0x8,%esp
  801084:	57                   	push   %edi
  801085:	6a 00                	push   $0x0
  801087:	e8 ab fb ff ff       	call   800c37 <sys_page_unmap>
	return r;
  80108c:	83 c4 10             	add    $0x10,%esp
  80108f:	89 f0                	mov    %esi,%eax
  801091:	eb 03                	jmp    801096 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  801093:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  801096:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801099:	5b                   	pop    %ebx
  80109a:	5e                   	pop    %esi
  80109b:	5f                   	pop    %edi
  80109c:	5d                   	pop    %ebp
  80109d:	c3                   	ret    

0080109e <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  80109e:	55                   	push   %ebp
  80109f:	89 e5                	mov    %esp,%ebp
  8010a1:	53                   	push   %ebx
  8010a2:	83 ec 14             	sub    $0x14,%esp
  8010a5:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8010a8:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8010ab:	50                   	push   %eax
  8010ac:	53                   	push   %ebx
  8010ad:	e8 86 fd ff ff       	call   800e38 <fd_lookup>
  8010b2:	83 c4 08             	add    $0x8,%esp
  8010b5:	85 c0                	test   %eax,%eax
  8010b7:	78 67                	js     801120 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8010b9:	83 ec 08             	sub    $0x8,%esp
  8010bc:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8010bf:	50                   	push   %eax
  8010c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8010c3:	ff 30                	pushl  (%eax)
  8010c5:	e8 c5 fd ff ff       	call   800e8f <dev_lookup>
  8010ca:	83 c4 10             	add    $0x10,%esp
  8010cd:	85 c0                	test   %eax,%eax
  8010cf:	78 4f                	js     801120 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  8010d1:	8b 55 f0             	mov    -0x10(%ebp),%edx
  8010d4:	8b 42 08             	mov    0x8(%edx),%eax
  8010d7:	83 e0 03             	and    $0x3,%eax
  8010da:	83 f8 01             	cmp    $0x1,%eax
  8010dd:	75 21                	jne    801100 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  8010df:	a1 20 60 80 00       	mov    0x806020,%eax
  8010e4:	8b 40 48             	mov    0x48(%eax),%eax
  8010e7:	83 ec 04             	sub    $0x4,%esp
  8010ea:	53                   	push   %ebx
  8010eb:	50                   	push   %eax
  8010ec:	68 10 23 80 00       	push   $0x802310
  8010f1:	e8 74 f1 ff ff       	call   80026a <cprintf>
		return -E_INVAL;
  8010f6:	83 c4 10             	add    $0x10,%esp
  8010f9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8010fe:	eb 20                	jmp    801120 <read+0x82>
	}
	if (!dev->dev_read)
  801100:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801103:	8b 40 08             	mov    0x8(%eax),%eax
  801106:	85 c0                	test   %eax,%eax
  801108:	74 11                	je     80111b <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  80110a:	83 ec 04             	sub    $0x4,%esp
  80110d:	ff 75 10             	pushl  0x10(%ebp)
  801110:	ff 75 0c             	pushl  0xc(%ebp)
  801113:	52                   	push   %edx
  801114:	ff d0                	call   *%eax
  801116:	83 c4 10             	add    $0x10,%esp
  801119:	eb 05                	jmp    801120 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  80111b:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801120:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801123:	c9                   	leave  
  801124:	c3                   	ret    

00801125 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801125:	55                   	push   %ebp
  801126:	89 e5                	mov    %esp,%ebp
  801128:	57                   	push   %edi
  801129:	56                   	push   %esi
  80112a:	53                   	push   %ebx
  80112b:	83 ec 0c             	sub    $0xc,%esp
  80112e:	8b 7d 08             	mov    0x8(%ebp),%edi
  801131:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801134:	bb 00 00 00 00       	mov    $0x0,%ebx
  801139:	eb 21                	jmp    80115c <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  80113b:	83 ec 04             	sub    $0x4,%esp
  80113e:	89 f0                	mov    %esi,%eax
  801140:	29 d8                	sub    %ebx,%eax
  801142:	50                   	push   %eax
  801143:	89 d8                	mov    %ebx,%eax
  801145:	03 45 0c             	add    0xc(%ebp),%eax
  801148:	50                   	push   %eax
  801149:	57                   	push   %edi
  80114a:	e8 4f ff ff ff       	call   80109e <read>
		if (m < 0)
  80114f:	83 c4 10             	add    $0x10,%esp
  801152:	85 c0                	test   %eax,%eax
  801154:	78 10                	js     801166 <readn+0x41>
			return m;
		if (m == 0)
  801156:	85 c0                	test   %eax,%eax
  801158:	74 0a                	je     801164 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80115a:	01 c3                	add    %eax,%ebx
  80115c:	39 f3                	cmp    %esi,%ebx
  80115e:	72 db                	jb     80113b <readn+0x16>
  801160:	89 d8                	mov    %ebx,%eax
  801162:	eb 02                	jmp    801166 <readn+0x41>
  801164:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801166:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801169:	5b                   	pop    %ebx
  80116a:	5e                   	pop    %esi
  80116b:	5f                   	pop    %edi
  80116c:	5d                   	pop    %ebp
  80116d:	c3                   	ret    

0080116e <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  80116e:	55                   	push   %ebp
  80116f:	89 e5                	mov    %esp,%ebp
  801171:	53                   	push   %ebx
  801172:	83 ec 14             	sub    $0x14,%esp
  801175:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  801178:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80117b:	50                   	push   %eax
  80117c:	53                   	push   %ebx
  80117d:	e8 b6 fc ff ff       	call   800e38 <fd_lookup>
  801182:	83 c4 08             	add    $0x8,%esp
  801185:	85 c0                	test   %eax,%eax
  801187:	78 62                	js     8011eb <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801189:	83 ec 08             	sub    $0x8,%esp
  80118c:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80118f:	50                   	push   %eax
  801190:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801193:	ff 30                	pushl  (%eax)
  801195:	e8 f5 fc ff ff       	call   800e8f <dev_lookup>
  80119a:	83 c4 10             	add    $0x10,%esp
  80119d:	85 c0                	test   %eax,%eax
  80119f:	78 4a                	js     8011eb <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8011a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8011a4:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8011a8:	75 21                	jne    8011cb <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  8011aa:	a1 20 60 80 00       	mov    0x806020,%eax
  8011af:	8b 40 48             	mov    0x48(%eax),%eax
  8011b2:	83 ec 04             	sub    $0x4,%esp
  8011b5:	53                   	push   %ebx
  8011b6:	50                   	push   %eax
  8011b7:	68 2c 23 80 00       	push   $0x80232c
  8011bc:	e8 a9 f0 ff ff       	call   80026a <cprintf>
		return -E_INVAL;
  8011c1:	83 c4 10             	add    $0x10,%esp
  8011c4:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8011c9:	eb 20                	jmp    8011eb <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8011cb:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8011ce:	8b 52 0c             	mov    0xc(%edx),%edx
  8011d1:	85 d2                	test   %edx,%edx
  8011d3:	74 11                	je     8011e6 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  8011d5:	83 ec 04             	sub    $0x4,%esp
  8011d8:	ff 75 10             	pushl  0x10(%ebp)
  8011db:	ff 75 0c             	pushl  0xc(%ebp)
  8011de:	50                   	push   %eax
  8011df:	ff d2                	call   *%edx
  8011e1:	83 c4 10             	add    $0x10,%esp
  8011e4:	eb 05                	jmp    8011eb <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  8011e6:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  8011eb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8011ee:	c9                   	leave  
  8011ef:	c3                   	ret    

008011f0 <seek>:

int
seek(int fdnum, off_t offset)
{
  8011f0:	55                   	push   %ebp
  8011f1:	89 e5                	mov    %esp,%ebp
  8011f3:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8011f6:	8d 45 fc             	lea    -0x4(%ebp),%eax
  8011f9:	50                   	push   %eax
  8011fa:	ff 75 08             	pushl  0x8(%ebp)
  8011fd:	e8 36 fc ff ff       	call   800e38 <fd_lookup>
  801202:	83 c4 08             	add    $0x8,%esp
  801205:	85 c0                	test   %eax,%eax
  801207:	78 0e                	js     801217 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  801209:	8b 45 fc             	mov    -0x4(%ebp),%eax
  80120c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80120f:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801212:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801217:	c9                   	leave  
  801218:	c3                   	ret    

00801219 <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  801219:	55                   	push   %ebp
  80121a:	89 e5                	mov    %esp,%ebp
  80121c:	53                   	push   %ebx
  80121d:	83 ec 14             	sub    $0x14,%esp
  801220:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801223:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801226:	50                   	push   %eax
  801227:	53                   	push   %ebx
  801228:	e8 0b fc ff ff       	call   800e38 <fd_lookup>
  80122d:	83 c4 08             	add    $0x8,%esp
  801230:	85 c0                	test   %eax,%eax
  801232:	78 5f                	js     801293 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801234:	83 ec 08             	sub    $0x8,%esp
  801237:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80123a:	50                   	push   %eax
  80123b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80123e:	ff 30                	pushl  (%eax)
  801240:	e8 4a fc ff ff       	call   800e8f <dev_lookup>
  801245:	83 c4 10             	add    $0x10,%esp
  801248:	85 c0                	test   %eax,%eax
  80124a:	78 47                	js     801293 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80124c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80124f:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801253:	75 21                	jne    801276 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801255:	a1 20 60 80 00       	mov    0x806020,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80125a:	8b 40 48             	mov    0x48(%eax),%eax
  80125d:	83 ec 04             	sub    $0x4,%esp
  801260:	53                   	push   %ebx
  801261:	50                   	push   %eax
  801262:	68 ec 22 80 00       	push   $0x8022ec
  801267:	e8 fe ef ff ff       	call   80026a <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  80126c:	83 c4 10             	add    $0x10,%esp
  80126f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  801274:	eb 1d                	jmp    801293 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  801276:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801279:	8b 52 18             	mov    0x18(%edx),%edx
  80127c:	85 d2                	test   %edx,%edx
  80127e:	74 0e                	je     80128e <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  801280:	83 ec 08             	sub    $0x8,%esp
  801283:	ff 75 0c             	pushl  0xc(%ebp)
  801286:	50                   	push   %eax
  801287:	ff d2                	call   *%edx
  801289:	83 c4 10             	add    $0x10,%esp
  80128c:	eb 05                	jmp    801293 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  80128e:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  801293:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801296:	c9                   	leave  
  801297:	c3                   	ret    

00801298 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  801298:	55                   	push   %ebp
  801299:	89 e5                	mov    %esp,%ebp
  80129b:	53                   	push   %ebx
  80129c:	83 ec 14             	sub    $0x14,%esp
  80129f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8012a2:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8012a5:	50                   	push   %eax
  8012a6:	ff 75 08             	pushl  0x8(%ebp)
  8012a9:	e8 8a fb ff ff       	call   800e38 <fd_lookup>
  8012ae:	83 c4 08             	add    $0x8,%esp
  8012b1:	85 c0                	test   %eax,%eax
  8012b3:	78 52                	js     801307 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8012b5:	83 ec 08             	sub    $0x8,%esp
  8012b8:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8012bb:	50                   	push   %eax
  8012bc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8012bf:	ff 30                	pushl  (%eax)
  8012c1:	e8 c9 fb ff ff       	call   800e8f <dev_lookup>
  8012c6:	83 c4 10             	add    $0x10,%esp
  8012c9:	85 c0                	test   %eax,%eax
  8012cb:	78 3a                	js     801307 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8012cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8012d0:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  8012d4:	74 2c                	je     801302 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  8012d6:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  8012d9:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  8012e0:	00 00 00 
	stat->st_isdir = 0;
  8012e3:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8012ea:	00 00 00 
	stat->st_dev = dev;
  8012ed:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  8012f3:	83 ec 08             	sub    $0x8,%esp
  8012f6:	53                   	push   %ebx
  8012f7:	ff 75 f0             	pushl  -0x10(%ebp)
  8012fa:	ff 50 14             	call   *0x14(%eax)
  8012fd:	83 c4 10             	add    $0x10,%esp
  801300:	eb 05                	jmp    801307 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801302:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  801307:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80130a:	c9                   	leave  
  80130b:	c3                   	ret    

0080130c <stat>:

int
stat(const char *path, struct Stat *stat)
{
  80130c:	55                   	push   %ebp
  80130d:	89 e5                	mov    %esp,%ebp
  80130f:	56                   	push   %esi
  801310:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801311:	83 ec 08             	sub    $0x8,%esp
  801314:	6a 00                	push   $0x0
  801316:	ff 75 08             	pushl  0x8(%ebp)
  801319:	e8 e7 01 00 00       	call   801505 <open>
  80131e:	89 c3                	mov    %eax,%ebx
  801320:	83 c4 10             	add    $0x10,%esp
  801323:	85 c0                	test   %eax,%eax
  801325:	78 1d                	js     801344 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801327:	83 ec 08             	sub    $0x8,%esp
  80132a:	ff 75 0c             	pushl  0xc(%ebp)
  80132d:	50                   	push   %eax
  80132e:	e8 65 ff ff ff       	call   801298 <fstat>
  801333:	89 c6                	mov    %eax,%esi
	close(fd);
  801335:	89 1c 24             	mov    %ebx,(%esp)
  801338:	e8 29 fc ff ff       	call   800f66 <close>
	return r;
  80133d:	83 c4 10             	add    $0x10,%esp
  801340:	89 f0                	mov    %esi,%eax
  801342:	eb 00                	jmp    801344 <stat+0x38>
}
  801344:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801347:	5b                   	pop    %ebx
  801348:	5e                   	pop    %esi
  801349:	5d                   	pop    %ebp
  80134a:	c3                   	ret    

0080134b <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  80134b:	55                   	push   %ebp
  80134c:	89 e5                	mov    %esp,%ebp
  80134e:	56                   	push   %esi
  80134f:	53                   	push   %ebx
  801350:	89 c6                	mov    %eax,%esi
  801352:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801354:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  80135b:	75 12                	jne    80136f <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  80135d:	83 ec 0c             	sub    $0xc,%esp
  801360:	6a 01                	push   $0x1
  801362:	e8 c9 08 00 00       	call   801c30 <ipc_find_env>
  801367:	a3 00 40 80 00       	mov    %eax,0x804000
  80136c:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  80136f:	6a 07                	push   $0x7
  801371:	68 00 70 80 00       	push   $0x807000
  801376:	56                   	push   %esi
  801377:	ff 35 00 40 80 00    	pushl  0x804000
  80137d:	e8 59 08 00 00       	call   801bdb <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  801382:	83 c4 0c             	add    $0xc,%esp
  801385:	6a 00                	push   $0x0
  801387:	53                   	push   %ebx
  801388:	6a 00                	push   $0x0
  80138a:	e8 e4 07 00 00       	call   801b73 <ipc_recv>
}
  80138f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801392:	5b                   	pop    %ebx
  801393:	5e                   	pop    %esi
  801394:	5d                   	pop    %ebp
  801395:	c3                   	ret    

00801396 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  801396:	55                   	push   %ebp
  801397:	89 e5                	mov    %esp,%ebp
  801399:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  80139c:	8b 45 08             	mov    0x8(%ebp),%eax
  80139f:	8b 40 0c             	mov    0xc(%eax),%eax
  8013a2:	a3 00 70 80 00       	mov    %eax,0x807000
	fsipcbuf.set_size.req_size = newsize;
  8013a7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8013aa:	a3 04 70 80 00       	mov    %eax,0x807004
	return fsipc(FSREQ_SET_SIZE, NULL);
  8013af:	ba 00 00 00 00       	mov    $0x0,%edx
  8013b4:	b8 02 00 00 00       	mov    $0x2,%eax
  8013b9:	e8 8d ff ff ff       	call   80134b <fsipc>
}
  8013be:	c9                   	leave  
  8013bf:	c3                   	ret    

008013c0 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8013c0:	55                   	push   %ebp
  8013c1:	89 e5                	mov    %esp,%ebp
  8013c3:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8013c6:	8b 45 08             	mov    0x8(%ebp),%eax
  8013c9:	8b 40 0c             	mov    0xc(%eax),%eax
  8013cc:	a3 00 70 80 00       	mov    %eax,0x807000
	return fsipc(FSREQ_FLUSH, NULL);
  8013d1:	ba 00 00 00 00       	mov    $0x0,%edx
  8013d6:	b8 06 00 00 00       	mov    $0x6,%eax
  8013db:	e8 6b ff ff ff       	call   80134b <fsipc>
}
  8013e0:	c9                   	leave  
  8013e1:	c3                   	ret    

008013e2 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  8013e2:	55                   	push   %ebp
  8013e3:	89 e5                	mov    %esp,%ebp
  8013e5:	53                   	push   %ebx
  8013e6:	83 ec 04             	sub    $0x4,%esp
  8013e9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  8013ec:	8b 45 08             	mov    0x8(%ebp),%eax
  8013ef:	8b 40 0c             	mov    0xc(%eax),%eax
  8013f2:	a3 00 70 80 00       	mov    %eax,0x807000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  8013f7:	ba 00 00 00 00       	mov    $0x0,%edx
  8013fc:	b8 05 00 00 00       	mov    $0x5,%eax
  801401:	e8 45 ff ff ff       	call   80134b <fsipc>
  801406:	85 c0                	test   %eax,%eax
  801408:	78 2c                	js     801436 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  80140a:	83 ec 08             	sub    $0x8,%esp
  80140d:	68 00 70 80 00       	push   $0x807000
  801412:	53                   	push   %ebx
  801413:	e8 b6 f3 ff ff       	call   8007ce <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801418:	a1 80 70 80 00       	mov    0x807080,%eax
  80141d:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801423:	a1 84 70 80 00       	mov    0x807084,%eax
  801428:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  80142e:	83 c4 10             	add    $0x10,%esp
  801431:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801436:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801439:	c9                   	leave  
  80143a:	c3                   	ret    

0080143b <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  80143b:	55                   	push   %ebp
  80143c:	89 e5                	mov    %esp,%ebp
  80143e:	83 ec 08             	sub    $0x8,%esp
  801441:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801444:	8b 55 08             	mov    0x8(%ebp),%edx
  801447:	8b 52 0c             	mov    0xc(%edx),%edx
  80144a:	89 15 00 70 80 00    	mov    %edx,0x807000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801450:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801455:	76 05                	jbe    80145c <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801457:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  80145c:	a3 04 70 80 00       	mov    %eax,0x807004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801461:	83 ec 04             	sub    $0x4,%esp
  801464:	50                   	push   %eax
  801465:	ff 75 0c             	pushl  0xc(%ebp)
  801468:	68 08 70 80 00       	push   $0x807008
  80146d:	e8 d1 f4 ff ff       	call   800943 <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  801472:	ba 00 00 00 00       	mov    $0x0,%edx
  801477:	b8 04 00 00 00       	mov    $0x4,%eax
  80147c:	e8 ca fe ff ff       	call   80134b <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  801481:	c9                   	leave  
  801482:	c3                   	ret    

00801483 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  801483:	55                   	push   %ebp
  801484:	89 e5                	mov    %esp,%ebp
  801486:	56                   	push   %esi
  801487:	53                   	push   %ebx
  801488:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  80148b:	8b 45 08             	mov    0x8(%ebp),%eax
  80148e:	8b 40 0c             	mov    0xc(%eax),%eax
  801491:	a3 00 70 80 00       	mov    %eax,0x807000
	fsipcbuf.read.req_n = n;
  801496:	89 35 04 70 80 00    	mov    %esi,0x807004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  80149c:	ba 00 00 00 00       	mov    $0x0,%edx
  8014a1:	b8 03 00 00 00       	mov    $0x3,%eax
  8014a6:	e8 a0 fe ff ff       	call   80134b <fsipc>
  8014ab:	89 c3                	mov    %eax,%ebx
  8014ad:	85 c0                	test   %eax,%eax
  8014af:	78 4b                	js     8014fc <devfile_read+0x79>
		return r;
	assert(r <= n);
  8014b1:	39 c6                	cmp    %eax,%esi
  8014b3:	73 16                	jae    8014cb <devfile_read+0x48>
  8014b5:	68 5c 23 80 00       	push   $0x80235c
  8014ba:	68 63 23 80 00       	push   $0x802363
  8014bf:	6a 7c                	push   $0x7c
  8014c1:	68 78 23 80 00       	push   $0x802378
  8014c6:	e8 c7 ec ff ff       	call   800192 <_panic>
	assert(r <= PGSIZE);
  8014cb:	3d 00 10 00 00       	cmp    $0x1000,%eax
  8014d0:	7e 16                	jle    8014e8 <devfile_read+0x65>
  8014d2:	68 83 23 80 00       	push   $0x802383
  8014d7:	68 63 23 80 00       	push   $0x802363
  8014dc:	6a 7d                	push   $0x7d
  8014de:	68 78 23 80 00       	push   $0x802378
  8014e3:	e8 aa ec ff ff       	call   800192 <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  8014e8:	83 ec 04             	sub    $0x4,%esp
  8014eb:	50                   	push   %eax
  8014ec:	68 00 70 80 00       	push   $0x807000
  8014f1:	ff 75 0c             	pushl  0xc(%ebp)
  8014f4:	e8 4a f4 ff ff       	call   800943 <memmove>
	return r;
  8014f9:	83 c4 10             	add    $0x10,%esp
}
  8014fc:	89 d8                	mov    %ebx,%eax
  8014fe:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801501:	5b                   	pop    %ebx
  801502:	5e                   	pop    %esi
  801503:	5d                   	pop    %ebp
  801504:	c3                   	ret    

00801505 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801505:	55                   	push   %ebp
  801506:	89 e5                	mov    %esp,%ebp
  801508:	53                   	push   %ebx
  801509:	83 ec 20             	sub    $0x20,%esp
  80150c:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  80150f:	53                   	push   %ebx
  801510:	e8 84 f2 ff ff       	call   800799 <strlen>
  801515:	83 c4 10             	add    $0x10,%esp
  801518:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  80151d:	7f 63                	jg     801582 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  80151f:	83 ec 0c             	sub    $0xc,%esp
  801522:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801525:	50                   	push   %eax
  801526:	e8 be f8 ff ff       	call   800de9 <fd_alloc>
  80152b:	83 c4 10             	add    $0x10,%esp
  80152e:	85 c0                	test   %eax,%eax
  801530:	78 55                	js     801587 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801532:	83 ec 08             	sub    $0x8,%esp
  801535:	53                   	push   %ebx
  801536:	68 00 70 80 00       	push   $0x807000
  80153b:	e8 8e f2 ff ff       	call   8007ce <strcpy>
	fsipcbuf.open.req_omode = mode;
  801540:	8b 45 0c             	mov    0xc(%ebp),%eax
  801543:	a3 00 74 80 00       	mov    %eax,0x807400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801548:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80154b:	b8 01 00 00 00       	mov    $0x1,%eax
  801550:	e8 f6 fd ff ff       	call   80134b <fsipc>
  801555:	89 c3                	mov    %eax,%ebx
  801557:	83 c4 10             	add    $0x10,%esp
  80155a:	85 c0                	test   %eax,%eax
  80155c:	79 14                	jns    801572 <open+0x6d>
		fd_close(fd, 0);
  80155e:	83 ec 08             	sub    $0x8,%esp
  801561:	6a 00                	push   $0x0
  801563:	ff 75 f4             	pushl  -0xc(%ebp)
  801566:	e8 77 f9 ff ff       	call   800ee2 <fd_close>
		return r;
  80156b:	83 c4 10             	add    $0x10,%esp
  80156e:	89 d8                	mov    %ebx,%eax
  801570:	eb 15                	jmp    801587 <open+0x82>
	}

	return fd2num(fd);
  801572:	83 ec 0c             	sub    $0xc,%esp
  801575:	ff 75 f4             	pushl  -0xc(%ebp)
  801578:	e8 45 f8 ff ff       	call   800dc2 <fd2num>
  80157d:	83 c4 10             	add    $0x10,%esp
  801580:	eb 05                	jmp    801587 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801582:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801587:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80158a:	c9                   	leave  
  80158b:	c3                   	ret    

0080158c <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  80158c:	55                   	push   %ebp
  80158d:	89 e5                	mov    %esp,%ebp
  80158f:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801592:	ba 00 00 00 00       	mov    $0x0,%edx
  801597:	b8 08 00 00 00       	mov    $0x8,%eax
  80159c:	e8 aa fd ff ff       	call   80134b <fsipc>
}
  8015a1:	c9                   	leave  
  8015a2:	c3                   	ret    

008015a3 <writebuf>:


static void
writebuf(struct printbuf *b)
{
	if (b->error > 0) {
  8015a3:	83 78 0c 00          	cmpl   $0x0,0xc(%eax)
  8015a7:	7e 38                	jle    8015e1 <writebuf+0x3e>
};


static void
writebuf(struct printbuf *b)
{
  8015a9:	55                   	push   %ebp
  8015aa:	89 e5                	mov    %esp,%ebp
  8015ac:	53                   	push   %ebx
  8015ad:	83 ec 08             	sub    $0x8,%esp
  8015b0:	89 c3                	mov    %eax,%ebx
	if (b->error > 0) {
		ssize_t result = write(b->fd, b->buf, b->idx);
  8015b2:	ff 70 04             	pushl  0x4(%eax)
  8015b5:	8d 40 10             	lea    0x10(%eax),%eax
  8015b8:	50                   	push   %eax
  8015b9:	ff 33                	pushl  (%ebx)
  8015bb:	e8 ae fb ff ff       	call   80116e <write>
		if (result > 0)
  8015c0:	83 c4 10             	add    $0x10,%esp
  8015c3:	85 c0                	test   %eax,%eax
  8015c5:	7e 03                	jle    8015ca <writebuf+0x27>
			b->result += result;
  8015c7:	01 43 08             	add    %eax,0x8(%ebx)
		if (result != b->idx) // error, or wrote less than supplied
  8015ca:	3b 43 04             	cmp    0x4(%ebx),%eax
  8015cd:	74 0e                	je     8015dd <writebuf+0x3a>
			b->error = (result < 0 ? result : 0);
  8015cf:	89 c2                	mov    %eax,%edx
  8015d1:	85 c0                	test   %eax,%eax
  8015d3:	7e 05                	jle    8015da <writebuf+0x37>
  8015d5:	ba 00 00 00 00       	mov    $0x0,%edx
  8015da:	89 53 0c             	mov    %edx,0xc(%ebx)
	}
}
  8015dd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8015e0:	c9                   	leave  
  8015e1:	c3                   	ret    

008015e2 <putch>:

static void
putch(int ch, void *thunk)
{
  8015e2:	55                   	push   %ebp
  8015e3:	89 e5                	mov    %esp,%ebp
  8015e5:	53                   	push   %ebx
  8015e6:	83 ec 04             	sub    $0x4,%esp
  8015e9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct printbuf *b = (struct printbuf *) thunk;
	b->buf[b->idx++] = ch;
  8015ec:	8b 53 04             	mov    0x4(%ebx),%edx
  8015ef:	8d 42 01             	lea    0x1(%edx),%eax
  8015f2:	89 43 04             	mov    %eax,0x4(%ebx)
  8015f5:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8015f8:	88 4c 13 10          	mov    %cl,0x10(%ebx,%edx,1)
	if (b->idx == 256) {
  8015fc:	3d 00 01 00 00       	cmp    $0x100,%eax
  801601:	75 0e                	jne    801611 <putch+0x2f>
		writebuf(b);
  801603:	89 d8                	mov    %ebx,%eax
  801605:	e8 99 ff ff ff       	call   8015a3 <writebuf>
		b->idx = 0;
  80160a:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	}
}
  801611:	83 c4 04             	add    $0x4,%esp
  801614:	5b                   	pop    %ebx
  801615:	5d                   	pop    %ebp
  801616:	c3                   	ret    

00801617 <vfprintf>:

int
vfprintf(int fd, const char *fmt, va_list ap)
{
  801617:	55                   	push   %ebp
  801618:	89 e5                	mov    %esp,%ebp
  80161a:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.fd = fd;
  801620:	8b 45 08             	mov    0x8(%ebp),%eax
  801623:	89 85 e8 fe ff ff    	mov    %eax,-0x118(%ebp)
	b.idx = 0;
  801629:	c7 85 ec fe ff ff 00 	movl   $0x0,-0x114(%ebp)
  801630:	00 00 00 
	b.result = 0;
  801633:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80163a:	00 00 00 
	b.error = 1;
  80163d:	c7 85 f4 fe ff ff 01 	movl   $0x1,-0x10c(%ebp)
  801644:	00 00 00 
	vprintfmt(putch, &b, fmt, ap);
  801647:	ff 75 10             	pushl  0x10(%ebp)
  80164a:	ff 75 0c             	pushl  0xc(%ebp)
  80164d:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  801653:	50                   	push   %eax
  801654:	68 e2 15 80 00       	push   $0x8015e2
  801659:	e8 40 ed ff ff       	call   80039e <vprintfmt>
	if (b.idx > 0)
  80165e:	83 c4 10             	add    $0x10,%esp
  801661:	83 bd ec fe ff ff 00 	cmpl   $0x0,-0x114(%ebp)
  801668:	7e 0b                	jle    801675 <vfprintf+0x5e>
		writebuf(&b);
  80166a:	8d 85 e8 fe ff ff    	lea    -0x118(%ebp),%eax
  801670:	e8 2e ff ff ff       	call   8015a3 <writebuf>

	return (b.result ? b.result : b.error);
  801675:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  80167b:	85 c0                	test   %eax,%eax
  80167d:	75 06                	jne    801685 <vfprintf+0x6e>
  80167f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  801685:	c9                   	leave  
  801686:	c3                   	ret    

00801687 <fprintf>:

int
fprintf(int fd, const char *fmt, ...)
{
  801687:	55                   	push   %ebp
  801688:	89 e5                	mov    %esp,%ebp
  80168a:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80168d:	8d 45 10             	lea    0x10(%ebp),%eax
	cnt = vfprintf(fd, fmt, ap);
  801690:	50                   	push   %eax
  801691:	ff 75 0c             	pushl  0xc(%ebp)
  801694:	ff 75 08             	pushl  0x8(%ebp)
  801697:	e8 7b ff ff ff       	call   801617 <vfprintf>
	va_end(ap);

	return cnt;
}
  80169c:	c9                   	leave  
  80169d:	c3                   	ret    

0080169e <printf>:

int
printf(const char *fmt, ...)
{
  80169e:	55                   	push   %ebp
  80169f:	89 e5                	mov    %esp,%ebp
  8016a1:	83 ec 0c             	sub    $0xc,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8016a4:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vfprintf(1, fmt, ap);
  8016a7:	50                   	push   %eax
  8016a8:	ff 75 08             	pushl  0x8(%ebp)
  8016ab:	6a 01                	push   $0x1
  8016ad:	e8 65 ff ff ff       	call   801617 <vfprintf>
	va_end(ap);

	return cnt;
}
  8016b2:	c9                   	leave  
  8016b3:	c3                   	ret    

008016b4 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  8016b4:	55                   	push   %ebp
  8016b5:	89 e5                	mov    %esp,%ebp
  8016b7:	56                   	push   %esi
  8016b8:	53                   	push   %ebx
  8016b9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  8016bc:	83 ec 0c             	sub    $0xc,%esp
  8016bf:	ff 75 08             	pushl  0x8(%ebp)
  8016c2:	e8 0b f7 ff ff       	call   800dd2 <fd2data>
  8016c7:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  8016c9:	83 c4 08             	add    $0x8,%esp
  8016cc:	68 8f 23 80 00       	push   $0x80238f
  8016d1:	53                   	push   %ebx
  8016d2:	e8 f7 f0 ff ff       	call   8007ce <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  8016d7:	8b 46 04             	mov    0x4(%esi),%eax
  8016da:	2b 06                	sub    (%esi),%eax
  8016dc:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  8016e2:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  8016e9:	00 00 00 
	stat->st_dev = &devpipe;
  8016ec:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  8016f3:	30 80 00 
	return 0;
}
  8016f6:	b8 00 00 00 00       	mov    $0x0,%eax
  8016fb:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8016fe:	5b                   	pop    %ebx
  8016ff:	5e                   	pop    %esi
  801700:	5d                   	pop    %ebp
  801701:	c3                   	ret    

00801702 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801702:	55                   	push   %ebp
  801703:	89 e5                	mov    %esp,%ebp
  801705:	53                   	push   %ebx
  801706:	83 ec 0c             	sub    $0xc,%esp
  801709:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  80170c:	53                   	push   %ebx
  80170d:	6a 00                	push   $0x0
  80170f:	e8 23 f5 ff ff       	call   800c37 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801714:	89 1c 24             	mov    %ebx,(%esp)
  801717:	e8 b6 f6 ff ff       	call   800dd2 <fd2data>
  80171c:	83 c4 08             	add    $0x8,%esp
  80171f:	50                   	push   %eax
  801720:	6a 00                	push   $0x0
  801722:	e8 10 f5 ff ff       	call   800c37 <sys_page_unmap>
}
  801727:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80172a:	c9                   	leave  
  80172b:	c3                   	ret    

0080172c <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  80172c:	55                   	push   %ebp
  80172d:	89 e5                	mov    %esp,%ebp
  80172f:	57                   	push   %edi
  801730:	56                   	push   %esi
  801731:	53                   	push   %ebx
  801732:	83 ec 1c             	sub    $0x1c,%esp
  801735:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801738:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  80173a:	a1 20 60 80 00       	mov    0x806020,%eax
  80173f:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801742:	83 ec 0c             	sub    $0xc,%esp
  801745:	ff 75 e0             	pushl  -0x20(%ebp)
  801748:	e8 27 05 00 00       	call   801c74 <pageref>
  80174d:	89 c3                	mov    %eax,%ebx
  80174f:	89 3c 24             	mov    %edi,(%esp)
  801752:	e8 1d 05 00 00       	call   801c74 <pageref>
  801757:	83 c4 10             	add    $0x10,%esp
  80175a:	39 c3                	cmp    %eax,%ebx
  80175c:	0f 94 c1             	sete   %cl
  80175f:	0f b6 c9             	movzbl %cl,%ecx
  801762:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801765:	8b 15 20 60 80 00    	mov    0x806020,%edx
  80176b:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  80176e:	39 ce                	cmp    %ecx,%esi
  801770:	74 1b                	je     80178d <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801772:	39 c3                	cmp    %eax,%ebx
  801774:	75 c4                	jne    80173a <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801776:	8b 42 58             	mov    0x58(%edx),%eax
  801779:	ff 75 e4             	pushl  -0x1c(%ebp)
  80177c:	50                   	push   %eax
  80177d:	56                   	push   %esi
  80177e:	68 96 23 80 00       	push   $0x802396
  801783:	e8 e2 ea ff ff       	call   80026a <cprintf>
  801788:	83 c4 10             	add    $0x10,%esp
  80178b:	eb ad                	jmp    80173a <_pipeisclosed+0xe>
	}
}
  80178d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801790:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801793:	5b                   	pop    %ebx
  801794:	5e                   	pop    %esi
  801795:	5f                   	pop    %edi
  801796:	5d                   	pop    %ebp
  801797:	c3                   	ret    

00801798 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801798:	55                   	push   %ebp
  801799:	89 e5                	mov    %esp,%ebp
  80179b:	57                   	push   %edi
  80179c:	56                   	push   %esi
  80179d:	53                   	push   %ebx
  80179e:	83 ec 18             	sub    $0x18,%esp
  8017a1:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  8017a4:	56                   	push   %esi
  8017a5:	e8 28 f6 ff ff       	call   800dd2 <fd2data>
  8017aa:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8017ac:	83 c4 10             	add    $0x10,%esp
  8017af:	bf 00 00 00 00       	mov    $0x0,%edi
  8017b4:	eb 3b                	jmp    8017f1 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  8017b6:	89 da                	mov    %ebx,%edx
  8017b8:	89 f0                	mov    %esi,%eax
  8017ba:	e8 6d ff ff ff       	call   80172c <_pipeisclosed>
  8017bf:	85 c0                	test   %eax,%eax
  8017c1:	75 38                	jne    8017fb <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  8017c3:	e8 cb f3 ff ff       	call   800b93 <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  8017c8:	8b 53 04             	mov    0x4(%ebx),%edx
  8017cb:	8b 03                	mov    (%ebx),%eax
  8017cd:	83 c0 20             	add    $0x20,%eax
  8017d0:	39 c2                	cmp    %eax,%edx
  8017d2:	73 e2                	jae    8017b6 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  8017d4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8017d7:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  8017da:	89 d0                	mov    %edx,%eax
  8017dc:	25 1f 00 00 80       	and    $0x8000001f,%eax
  8017e1:	79 05                	jns    8017e8 <devpipe_write+0x50>
  8017e3:	48                   	dec    %eax
  8017e4:	83 c8 e0             	or     $0xffffffe0,%eax
  8017e7:	40                   	inc    %eax
  8017e8:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  8017ec:	42                   	inc    %edx
  8017ed:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  8017f0:	47                   	inc    %edi
  8017f1:	3b 7d 10             	cmp    0x10(%ebp),%edi
  8017f4:	75 d2                	jne    8017c8 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  8017f6:	8b 45 10             	mov    0x10(%ebp),%eax
  8017f9:	eb 05                	jmp    801800 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  8017fb:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801800:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801803:	5b                   	pop    %ebx
  801804:	5e                   	pop    %esi
  801805:	5f                   	pop    %edi
  801806:	5d                   	pop    %ebp
  801807:	c3                   	ret    

00801808 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801808:	55                   	push   %ebp
  801809:	89 e5                	mov    %esp,%ebp
  80180b:	57                   	push   %edi
  80180c:	56                   	push   %esi
  80180d:	53                   	push   %ebx
  80180e:	83 ec 18             	sub    $0x18,%esp
  801811:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801814:	57                   	push   %edi
  801815:	e8 b8 f5 ff ff       	call   800dd2 <fd2data>
  80181a:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80181c:	83 c4 10             	add    $0x10,%esp
  80181f:	bb 00 00 00 00       	mov    $0x0,%ebx
  801824:	eb 3a                	jmp    801860 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801826:	85 db                	test   %ebx,%ebx
  801828:	74 04                	je     80182e <devpipe_read+0x26>
				return i;
  80182a:	89 d8                	mov    %ebx,%eax
  80182c:	eb 41                	jmp    80186f <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  80182e:	89 f2                	mov    %esi,%edx
  801830:	89 f8                	mov    %edi,%eax
  801832:	e8 f5 fe ff ff       	call   80172c <_pipeisclosed>
  801837:	85 c0                	test   %eax,%eax
  801839:	75 2f                	jne    80186a <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  80183b:	e8 53 f3 ff ff       	call   800b93 <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801840:	8b 06                	mov    (%esi),%eax
  801842:	3b 46 04             	cmp    0x4(%esi),%eax
  801845:	74 df                	je     801826 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801847:	25 1f 00 00 80       	and    $0x8000001f,%eax
  80184c:	79 05                	jns    801853 <devpipe_read+0x4b>
  80184e:	48                   	dec    %eax
  80184f:	83 c8 e0             	or     $0xffffffe0,%eax
  801852:	40                   	inc    %eax
  801853:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801857:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80185a:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  80185d:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  80185f:	43                   	inc    %ebx
  801860:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801863:	75 db                	jne    801840 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801865:	8b 45 10             	mov    0x10(%ebp),%eax
  801868:	eb 05                	jmp    80186f <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  80186a:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  80186f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801872:	5b                   	pop    %ebx
  801873:	5e                   	pop    %esi
  801874:	5f                   	pop    %edi
  801875:	5d                   	pop    %ebp
  801876:	c3                   	ret    

00801877 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801877:	55                   	push   %ebp
  801878:	89 e5                	mov    %esp,%ebp
  80187a:	56                   	push   %esi
  80187b:	53                   	push   %ebx
  80187c:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  80187f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801882:	50                   	push   %eax
  801883:	e8 61 f5 ff ff       	call   800de9 <fd_alloc>
  801888:	83 c4 10             	add    $0x10,%esp
  80188b:	85 c0                	test   %eax,%eax
  80188d:	0f 88 2a 01 00 00    	js     8019bd <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801893:	83 ec 04             	sub    $0x4,%esp
  801896:	68 07 04 00 00       	push   $0x407
  80189b:	ff 75 f4             	pushl  -0xc(%ebp)
  80189e:	6a 00                	push   $0x0
  8018a0:	e8 0d f3 ff ff       	call   800bb2 <sys_page_alloc>
  8018a5:	83 c4 10             	add    $0x10,%esp
  8018a8:	85 c0                	test   %eax,%eax
  8018aa:	0f 88 0d 01 00 00    	js     8019bd <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  8018b0:	83 ec 0c             	sub    $0xc,%esp
  8018b3:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8018b6:	50                   	push   %eax
  8018b7:	e8 2d f5 ff ff       	call   800de9 <fd_alloc>
  8018bc:	89 c3                	mov    %eax,%ebx
  8018be:	83 c4 10             	add    $0x10,%esp
  8018c1:	85 c0                	test   %eax,%eax
  8018c3:	0f 88 e2 00 00 00    	js     8019ab <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8018c9:	83 ec 04             	sub    $0x4,%esp
  8018cc:	68 07 04 00 00       	push   $0x407
  8018d1:	ff 75 f0             	pushl  -0x10(%ebp)
  8018d4:	6a 00                	push   $0x0
  8018d6:	e8 d7 f2 ff ff       	call   800bb2 <sys_page_alloc>
  8018db:	89 c3                	mov    %eax,%ebx
  8018dd:	83 c4 10             	add    $0x10,%esp
  8018e0:	85 c0                	test   %eax,%eax
  8018e2:	0f 88 c3 00 00 00    	js     8019ab <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  8018e8:	83 ec 0c             	sub    $0xc,%esp
  8018eb:	ff 75 f4             	pushl  -0xc(%ebp)
  8018ee:	e8 df f4 ff ff       	call   800dd2 <fd2data>
  8018f3:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  8018f5:	83 c4 0c             	add    $0xc,%esp
  8018f8:	68 07 04 00 00       	push   $0x407
  8018fd:	50                   	push   %eax
  8018fe:	6a 00                	push   $0x0
  801900:	e8 ad f2 ff ff       	call   800bb2 <sys_page_alloc>
  801905:	89 c3                	mov    %eax,%ebx
  801907:	83 c4 10             	add    $0x10,%esp
  80190a:	85 c0                	test   %eax,%eax
  80190c:	0f 88 89 00 00 00    	js     80199b <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801912:	83 ec 0c             	sub    $0xc,%esp
  801915:	ff 75 f0             	pushl  -0x10(%ebp)
  801918:	e8 b5 f4 ff ff       	call   800dd2 <fd2data>
  80191d:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801924:	50                   	push   %eax
  801925:	6a 00                	push   $0x0
  801927:	56                   	push   %esi
  801928:	6a 00                	push   $0x0
  80192a:	e8 c6 f2 ff ff       	call   800bf5 <sys_page_map>
  80192f:	89 c3                	mov    %eax,%ebx
  801931:	83 c4 20             	add    $0x20,%esp
  801934:	85 c0                	test   %eax,%eax
  801936:	78 55                	js     80198d <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801938:	8b 15 20 30 80 00    	mov    0x803020,%edx
  80193e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801941:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801943:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801946:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  80194d:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801953:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801956:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801958:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80195b:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801962:	83 ec 0c             	sub    $0xc,%esp
  801965:	ff 75 f4             	pushl  -0xc(%ebp)
  801968:	e8 55 f4 ff ff       	call   800dc2 <fd2num>
  80196d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801970:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801972:	83 c4 04             	add    $0x4,%esp
  801975:	ff 75 f0             	pushl  -0x10(%ebp)
  801978:	e8 45 f4 ff ff       	call   800dc2 <fd2num>
  80197d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801980:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801983:	83 c4 10             	add    $0x10,%esp
  801986:	b8 00 00 00 00       	mov    $0x0,%eax
  80198b:	eb 30                	jmp    8019bd <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  80198d:	83 ec 08             	sub    $0x8,%esp
  801990:	56                   	push   %esi
  801991:	6a 00                	push   $0x0
  801993:	e8 9f f2 ff ff       	call   800c37 <sys_page_unmap>
  801998:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  80199b:	83 ec 08             	sub    $0x8,%esp
  80199e:	ff 75 f0             	pushl  -0x10(%ebp)
  8019a1:	6a 00                	push   $0x0
  8019a3:	e8 8f f2 ff ff       	call   800c37 <sys_page_unmap>
  8019a8:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  8019ab:	83 ec 08             	sub    $0x8,%esp
  8019ae:	ff 75 f4             	pushl  -0xc(%ebp)
  8019b1:	6a 00                	push   $0x0
  8019b3:	e8 7f f2 ff ff       	call   800c37 <sys_page_unmap>
  8019b8:	83 c4 10             	add    $0x10,%esp
  8019bb:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  8019bd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8019c0:	5b                   	pop    %ebx
  8019c1:	5e                   	pop    %esi
  8019c2:	5d                   	pop    %ebp
  8019c3:	c3                   	ret    

008019c4 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  8019c4:	55                   	push   %ebp
  8019c5:	89 e5                	mov    %esp,%ebp
  8019c7:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  8019ca:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8019cd:	50                   	push   %eax
  8019ce:	ff 75 08             	pushl  0x8(%ebp)
  8019d1:	e8 62 f4 ff ff       	call   800e38 <fd_lookup>
  8019d6:	83 c4 10             	add    $0x10,%esp
  8019d9:	85 c0                	test   %eax,%eax
  8019db:	78 18                	js     8019f5 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  8019dd:	83 ec 0c             	sub    $0xc,%esp
  8019e0:	ff 75 f4             	pushl  -0xc(%ebp)
  8019e3:	e8 ea f3 ff ff       	call   800dd2 <fd2data>
	return _pipeisclosed(fd, p);
  8019e8:	89 c2                	mov    %eax,%edx
  8019ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8019ed:	e8 3a fd ff ff       	call   80172c <_pipeisclosed>
  8019f2:	83 c4 10             	add    $0x10,%esp
}
  8019f5:	c9                   	leave  
  8019f6:	c3                   	ret    

008019f7 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  8019f7:	55                   	push   %ebp
  8019f8:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  8019fa:	b8 00 00 00 00       	mov    $0x0,%eax
  8019ff:	5d                   	pop    %ebp
  801a00:	c3                   	ret    

00801a01 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801a01:	55                   	push   %ebp
  801a02:	89 e5                	mov    %esp,%ebp
  801a04:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801a07:	68 ae 23 80 00       	push   $0x8023ae
  801a0c:	ff 75 0c             	pushl  0xc(%ebp)
  801a0f:	e8 ba ed ff ff       	call   8007ce <strcpy>
	return 0;
}
  801a14:	b8 00 00 00 00       	mov    $0x0,%eax
  801a19:	c9                   	leave  
  801a1a:	c3                   	ret    

00801a1b <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801a1b:	55                   	push   %ebp
  801a1c:	89 e5                	mov    %esp,%ebp
  801a1e:	57                   	push   %edi
  801a1f:	56                   	push   %esi
  801a20:	53                   	push   %ebx
  801a21:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801a27:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801a2c:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801a32:	eb 2c                	jmp    801a60 <devcons_write+0x45>
		m = n - tot;
  801a34:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801a37:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801a39:	83 fb 7f             	cmp    $0x7f,%ebx
  801a3c:	76 05                	jbe    801a43 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801a3e:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801a43:	83 ec 04             	sub    $0x4,%esp
  801a46:	53                   	push   %ebx
  801a47:	03 45 0c             	add    0xc(%ebp),%eax
  801a4a:	50                   	push   %eax
  801a4b:	57                   	push   %edi
  801a4c:	e8 f2 ee ff ff       	call   800943 <memmove>
		sys_cputs(buf, m);
  801a51:	83 c4 08             	add    $0x8,%esp
  801a54:	53                   	push   %ebx
  801a55:	57                   	push   %edi
  801a56:	e8 9b f0 ff ff       	call   800af6 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801a5b:	01 de                	add    %ebx,%esi
  801a5d:	83 c4 10             	add    $0x10,%esp
  801a60:	89 f0                	mov    %esi,%eax
  801a62:	3b 75 10             	cmp    0x10(%ebp),%esi
  801a65:	72 cd                	jb     801a34 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801a67:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801a6a:	5b                   	pop    %ebx
  801a6b:	5e                   	pop    %esi
  801a6c:	5f                   	pop    %edi
  801a6d:	5d                   	pop    %ebp
  801a6e:	c3                   	ret    

00801a6f <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801a6f:	55                   	push   %ebp
  801a70:	89 e5                	mov    %esp,%ebp
  801a72:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801a75:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801a79:	75 07                	jne    801a82 <devcons_read+0x13>
  801a7b:	eb 23                	jmp    801aa0 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801a7d:	e8 11 f1 ff ff       	call   800b93 <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801a82:	e8 8d f0 ff ff       	call   800b14 <sys_cgetc>
  801a87:	85 c0                	test   %eax,%eax
  801a89:	74 f2                	je     801a7d <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801a8b:	85 c0                	test   %eax,%eax
  801a8d:	78 1d                	js     801aac <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801a8f:	83 f8 04             	cmp    $0x4,%eax
  801a92:	74 13                	je     801aa7 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801a94:	8b 55 0c             	mov    0xc(%ebp),%edx
  801a97:	88 02                	mov    %al,(%edx)
	return 1;
  801a99:	b8 01 00 00 00       	mov    $0x1,%eax
  801a9e:	eb 0c                	jmp    801aac <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801aa0:	b8 00 00 00 00       	mov    $0x0,%eax
  801aa5:	eb 05                	jmp    801aac <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801aa7:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801aac:	c9                   	leave  
  801aad:	c3                   	ret    

00801aae <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801aae:	55                   	push   %ebp
  801aaf:	89 e5                	mov    %esp,%ebp
  801ab1:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801ab4:	8b 45 08             	mov    0x8(%ebp),%eax
  801ab7:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801aba:	6a 01                	push   $0x1
  801abc:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801abf:	50                   	push   %eax
  801ac0:	e8 31 f0 ff ff       	call   800af6 <sys_cputs>
}
  801ac5:	83 c4 10             	add    $0x10,%esp
  801ac8:	c9                   	leave  
  801ac9:	c3                   	ret    

00801aca <getchar>:

int
getchar(void)
{
  801aca:	55                   	push   %ebp
  801acb:	89 e5                	mov    %esp,%ebp
  801acd:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801ad0:	6a 01                	push   $0x1
  801ad2:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801ad5:	50                   	push   %eax
  801ad6:	6a 00                	push   $0x0
  801ad8:	e8 c1 f5 ff ff       	call   80109e <read>
	if (r < 0)
  801add:	83 c4 10             	add    $0x10,%esp
  801ae0:	85 c0                	test   %eax,%eax
  801ae2:	78 0f                	js     801af3 <getchar+0x29>
		return r;
	if (r < 1)
  801ae4:	85 c0                	test   %eax,%eax
  801ae6:	7e 06                	jle    801aee <getchar+0x24>
		return -E_EOF;
	return c;
  801ae8:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801aec:	eb 05                	jmp    801af3 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801aee:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801af3:	c9                   	leave  
  801af4:	c3                   	ret    

00801af5 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801af5:	55                   	push   %ebp
  801af6:	89 e5                	mov    %esp,%ebp
  801af8:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801afb:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801afe:	50                   	push   %eax
  801aff:	ff 75 08             	pushl  0x8(%ebp)
  801b02:	e8 31 f3 ff ff       	call   800e38 <fd_lookup>
  801b07:	83 c4 10             	add    $0x10,%esp
  801b0a:	85 c0                	test   %eax,%eax
  801b0c:	78 11                	js     801b1f <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801b0e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b11:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801b17:	39 10                	cmp    %edx,(%eax)
  801b19:	0f 94 c0             	sete   %al
  801b1c:	0f b6 c0             	movzbl %al,%eax
}
  801b1f:	c9                   	leave  
  801b20:	c3                   	ret    

00801b21 <opencons>:

int
opencons(void)
{
  801b21:	55                   	push   %ebp
  801b22:	89 e5                	mov    %esp,%ebp
  801b24:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801b27:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801b2a:	50                   	push   %eax
  801b2b:	e8 b9 f2 ff ff       	call   800de9 <fd_alloc>
  801b30:	83 c4 10             	add    $0x10,%esp
  801b33:	85 c0                	test   %eax,%eax
  801b35:	78 3a                	js     801b71 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801b37:	83 ec 04             	sub    $0x4,%esp
  801b3a:	68 07 04 00 00       	push   $0x407
  801b3f:	ff 75 f4             	pushl  -0xc(%ebp)
  801b42:	6a 00                	push   $0x0
  801b44:	e8 69 f0 ff ff       	call   800bb2 <sys_page_alloc>
  801b49:	83 c4 10             	add    $0x10,%esp
  801b4c:	85 c0                	test   %eax,%eax
  801b4e:	78 21                	js     801b71 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801b50:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801b56:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b59:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801b5b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801b5e:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801b65:	83 ec 0c             	sub    $0xc,%esp
  801b68:	50                   	push   %eax
  801b69:	e8 54 f2 ff ff       	call   800dc2 <fd2num>
  801b6e:	83 c4 10             	add    $0x10,%esp
}
  801b71:	c9                   	leave  
  801b72:	c3                   	ret    

00801b73 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801b73:	55                   	push   %ebp
  801b74:	89 e5                	mov    %esp,%ebp
  801b76:	56                   	push   %esi
  801b77:	53                   	push   %ebx
  801b78:	8b 75 08             	mov    0x8(%ebp),%esi
  801b7b:	8b 45 0c             	mov    0xc(%ebp),%eax
  801b7e:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801b81:	85 c0                	test   %eax,%eax
  801b83:	75 05                	jne    801b8a <ipc_recv+0x17>
        pg = (void *)UTOP;
  801b85:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801b8a:	83 ec 0c             	sub    $0xc,%esp
  801b8d:	50                   	push   %eax
  801b8e:	e8 ee f1 ff ff       	call   800d81 <sys_ipc_recv>
  801b93:	83 c4 10             	add    $0x10,%esp
  801b96:	85 c0                	test   %eax,%eax
  801b98:	74 16                	je     801bb0 <ipc_recv+0x3d>
        if (from_env_store)
  801b9a:	85 f6                	test   %esi,%esi
  801b9c:	74 06                	je     801ba4 <ipc_recv+0x31>
            *from_env_store = 0;
  801b9e:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801ba4:	85 db                	test   %ebx,%ebx
  801ba6:	74 2c                	je     801bd4 <ipc_recv+0x61>
            *perm_store = 0;
  801ba8:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801bae:	eb 24                	jmp    801bd4 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801bb0:	85 f6                	test   %esi,%esi
  801bb2:	74 0a                	je     801bbe <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801bb4:	a1 20 60 80 00       	mov    0x806020,%eax
  801bb9:	8b 40 74             	mov    0x74(%eax),%eax
  801bbc:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801bbe:	85 db                	test   %ebx,%ebx
  801bc0:	74 0a                	je     801bcc <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801bc2:	a1 20 60 80 00       	mov    0x806020,%eax
  801bc7:	8b 40 78             	mov    0x78(%eax),%eax
  801bca:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801bcc:	a1 20 60 80 00       	mov    0x806020,%eax
  801bd1:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801bd4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801bd7:	5b                   	pop    %ebx
  801bd8:	5e                   	pop    %esi
  801bd9:	5d                   	pop    %ebp
  801bda:	c3                   	ret    

00801bdb <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801bdb:	55                   	push   %ebp
  801bdc:	89 e5                	mov    %esp,%ebp
  801bde:	57                   	push   %edi
  801bdf:	56                   	push   %esi
  801be0:	53                   	push   %ebx
  801be1:	83 ec 0c             	sub    $0xc,%esp
  801be4:	8b 75 0c             	mov    0xc(%ebp),%esi
  801be7:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801bea:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  801bed:	85 db                	test   %ebx,%ebx
  801bef:	75 0c                	jne    801bfd <ipc_send+0x22>
        pg = (void *)UTOP;
  801bf1:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  801bf6:	eb 05                	jmp    801bfd <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  801bf8:	e8 96 ef ff ff       	call   800b93 <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  801bfd:	57                   	push   %edi
  801bfe:	53                   	push   %ebx
  801bff:	56                   	push   %esi
  801c00:	ff 75 08             	pushl  0x8(%ebp)
  801c03:	e8 56 f1 ff ff       	call   800d5e <sys_ipc_try_send>
  801c08:	83 c4 10             	add    $0x10,%esp
  801c0b:	83 f8 f9             	cmp    $0xfffffff9,%eax
  801c0e:	74 e8                	je     801bf8 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  801c10:	85 c0                	test   %eax,%eax
  801c12:	74 14                	je     801c28 <ipc_send+0x4d>
        panic("ipc_send: error");
  801c14:	83 ec 04             	sub    $0x4,%esp
  801c17:	68 ba 23 80 00       	push   $0x8023ba
  801c1c:	6a 6a                	push   $0x6a
  801c1e:	68 ca 23 80 00       	push   $0x8023ca
  801c23:	e8 6a e5 ff ff       	call   800192 <_panic>
    }
	//panic("ipc_send not implemented");
}
  801c28:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c2b:	5b                   	pop    %ebx
  801c2c:	5e                   	pop    %esi
  801c2d:	5f                   	pop    %edi
  801c2e:	5d                   	pop    %ebp
  801c2f:	c3                   	ret    

00801c30 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  801c30:	55                   	push   %ebp
  801c31:	89 e5                	mov    %esp,%ebp
  801c33:	53                   	push   %ebx
  801c34:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  801c37:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  801c3c:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  801c43:	89 d0                	mov    %edx,%eax
  801c45:	c1 e0 07             	shl    $0x7,%eax
  801c48:	29 d8                	sub    %ebx,%eax
  801c4a:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  801c4f:	8b 40 50             	mov    0x50(%eax),%eax
  801c52:	39 c8                	cmp    %ecx,%eax
  801c54:	75 0d                	jne    801c63 <ipc_find_env+0x33>
			return envs[i].env_id;
  801c56:	c1 e2 07             	shl    $0x7,%edx
  801c59:	29 da                	sub    %ebx,%edx
  801c5b:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  801c61:	eb 0e                	jmp    801c71 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  801c63:	42                   	inc    %edx
  801c64:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  801c6a:	75 d0                	jne    801c3c <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  801c6c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801c71:	5b                   	pop    %ebx
  801c72:	5d                   	pop    %ebp
  801c73:	c3                   	ret    

00801c74 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  801c74:	55                   	push   %ebp
  801c75:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  801c77:	8b 45 08             	mov    0x8(%ebp),%eax
  801c7a:	c1 e8 16             	shr    $0x16,%eax
  801c7d:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801c84:	a8 01                	test   $0x1,%al
  801c86:	74 21                	je     801ca9 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  801c88:	8b 45 08             	mov    0x8(%ebp),%eax
  801c8b:	c1 e8 0c             	shr    $0xc,%eax
  801c8e:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  801c95:	a8 01                	test   $0x1,%al
  801c97:	74 17                	je     801cb0 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  801c99:	c1 e8 0c             	shr    $0xc,%eax
  801c9c:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  801ca3:	ef 
  801ca4:	0f b7 c0             	movzwl %ax,%eax
  801ca7:	eb 0c                	jmp    801cb5 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  801ca9:	b8 00 00 00 00       	mov    $0x0,%eax
  801cae:	eb 05                	jmp    801cb5 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  801cb0:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  801cb5:	5d                   	pop    %ebp
  801cb6:	c3                   	ret    
  801cb7:	90                   	nop

00801cb8 <__udivdi3>:
  801cb8:	55                   	push   %ebp
  801cb9:	57                   	push   %edi
  801cba:	56                   	push   %esi
  801cbb:	53                   	push   %ebx
  801cbc:	83 ec 1c             	sub    $0x1c,%esp
  801cbf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  801cc3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801cc7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801ccb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801ccf:	89 ca                	mov    %ecx,%edx
  801cd1:	89 f8                	mov    %edi,%eax
  801cd3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  801cd7:	85 f6                	test   %esi,%esi
  801cd9:	75 2d                	jne    801d08 <__udivdi3+0x50>
  801cdb:	39 cf                	cmp    %ecx,%edi
  801cdd:	77 65                	ja     801d44 <__udivdi3+0x8c>
  801cdf:	89 fd                	mov    %edi,%ebp
  801ce1:	85 ff                	test   %edi,%edi
  801ce3:	75 0b                	jne    801cf0 <__udivdi3+0x38>
  801ce5:	b8 01 00 00 00       	mov    $0x1,%eax
  801cea:	31 d2                	xor    %edx,%edx
  801cec:	f7 f7                	div    %edi
  801cee:	89 c5                	mov    %eax,%ebp
  801cf0:	31 d2                	xor    %edx,%edx
  801cf2:	89 c8                	mov    %ecx,%eax
  801cf4:	f7 f5                	div    %ebp
  801cf6:	89 c1                	mov    %eax,%ecx
  801cf8:	89 d8                	mov    %ebx,%eax
  801cfa:	f7 f5                	div    %ebp
  801cfc:	89 cf                	mov    %ecx,%edi
  801cfe:	89 fa                	mov    %edi,%edx
  801d00:	83 c4 1c             	add    $0x1c,%esp
  801d03:	5b                   	pop    %ebx
  801d04:	5e                   	pop    %esi
  801d05:	5f                   	pop    %edi
  801d06:	5d                   	pop    %ebp
  801d07:	c3                   	ret    
  801d08:	39 ce                	cmp    %ecx,%esi
  801d0a:	77 28                	ja     801d34 <__udivdi3+0x7c>
  801d0c:	0f bd fe             	bsr    %esi,%edi
  801d0f:	83 f7 1f             	xor    $0x1f,%edi
  801d12:	75 40                	jne    801d54 <__udivdi3+0x9c>
  801d14:	39 ce                	cmp    %ecx,%esi
  801d16:	72 0a                	jb     801d22 <__udivdi3+0x6a>
  801d18:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801d1c:	0f 87 9e 00 00 00    	ja     801dc0 <__udivdi3+0x108>
  801d22:	b8 01 00 00 00       	mov    $0x1,%eax
  801d27:	89 fa                	mov    %edi,%edx
  801d29:	83 c4 1c             	add    $0x1c,%esp
  801d2c:	5b                   	pop    %ebx
  801d2d:	5e                   	pop    %esi
  801d2e:	5f                   	pop    %edi
  801d2f:	5d                   	pop    %ebp
  801d30:	c3                   	ret    
  801d31:	8d 76 00             	lea    0x0(%esi),%esi
  801d34:	31 ff                	xor    %edi,%edi
  801d36:	31 c0                	xor    %eax,%eax
  801d38:	89 fa                	mov    %edi,%edx
  801d3a:	83 c4 1c             	add    $0x1c,%esp
  801d3d:	5b                   	pop    %ebx
  801d3e:	5e                   	pop    %esi
  801d3f:	5f                   	pop    %edi
  801d40:	5d                   	pop    %ebp
  801d41:	c3                   	ret    
  801d42:	66 90                	xchg   %ax,%ax
  801d44:	89 d8                	mov    %ebx,%eax
  801d46:	f7 f7                	div    %edi
  801d48:	31 ff                	xor    %edi,%edi
  801d4a:	89 fa                	mov    %edi,%edx
  801d4c:	83 c4 1c             	add    $0x1c,%esp
  801d4f:	5b                   	pop    %ebx
  801d50:	5e                   	pop    %esi
  801d51:	5f                   	pop    %edi
  801d52:	5d                   	pop    %ebp
  801d53:	c3                   	ret    
  801d54:	bd 20 00 00 00       	mov    $0x20,%ebp
  801d59:	89 eb                	mov    %ebp,%ebx
  801d5b:	29 fb                	sub    %edi,%ebx
  801d5d:	89 f9                	mov    %edi,%ecx
  801d5f:	d3 e6                	shl    %cl,%esi
  801d61:	89 c5                	mov    %eax,%ebp
  801d63:	88 d9                	mov    %bl,%cl
  801d65:	d3 ed                	shr    %cl,%ebp
  801d67:	89 e9                	mov    %ebp,%ecx
  801d69:	09 f1                	or     %esi,%ecx
  801d6b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801d6f:	89 f9                	mov    %edi,%ecx
  801d71:	d3 e0                	shl    %cl,%eax
  801d73:	89 c5                	mov    %eax,%ebp
  801d75:	89 d6                	mov    %edx,%esi
  801d77:	88 d9                	mov    %bl,%cl
  801d79:	d3 ee                	shr    %cl,%esi
  801d7b:	89 f9                	mov    %edi,%ecx
  801d7d:	d3 e2                	shl    %cl,%edx
  801d7f:	8b 44 24 08          	mov    0x8(%esp),%eax
  801d83:	88 d9                	mov    %bl,%cl
  801d85:	d3 e8                	shr    %cl,%eax
  801d87:	09 c2                	or     %eax,%edx
  801d89:	89 d0                	mov    %edx,%eax
  801d8b:	89 f2                	mov    %esi,%edx
  801d8d:	f7 74 24 0c          	divl   0xc(%esp)
  801d91:	89 d6                	mov    %edx,%esi
  801d93:	89 c3                	mov    %eax,%ebx
  801d95:	f7 e5                	mul    %ebp
  801d97:	39 d6                	cmp    %edx,%esi
  801d99:	72 19                	jb     801db4 <__udivdi3+0xfc>
  801d9b:	74 0b                	je     801da8 <__udivdi3+0xf0>
  801d9d:	89 d8                	mov    %ebx,%eax
  801d9f:	31 ff                	xor    %edi,%edi
  801da1:	e9 58 ff ff ff       	jmp    801cfe <__udivdi3+0x46>
  801da6:	66 90                	xchg   %ax,%ax
  801da8:	8b 54 24 08          	mov    0x8(%esp),%edx
  801dac:	89 f9                	mov    %edi,%ecx
  801dae:	d3 e2                	shl    %cl,%edx
  801db0:	39 c2                	cmp    %eax,%edx
  801db2:	73 e9                	jae    801d9d <__udivdi3+0xe5>
  801db4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  801db7:	31 ff                	xor    %edi,%edi
  801db9:	e9 40 ff ff ff       	jmp    801cfe <__udivdi3+0x46>
  801dbe:	66 90                	xchg   %ax,%ax
  801dc0:	31 c0                	xor    %eax,%eax
  801dc2:	e9 37 ff ff ff       	jmp    801cfe <__udivdi3+0x46>
  801dc7:	90                   	nop

00801dc8 <__umoddi3>:
  801dc8:	55                   	push   %ebp
  801dc9:	57                   	push   %edi
  801dca:	56                   	push   %esi
  801dcb:	53                   	push   %ebx
  801dcc:	83 ec 1c             	sub    $0x1c,%esp
  801dcf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  801dd3:	8b 74 24 34          	mov    0x34(%esp),%esi
  801dd7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801ddb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  801ddf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  801de3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801de7:	89 f3                	mov    %esi,%ebx
  801de9:	89 fa                	mov    %edi,%edx
  801deb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801def:	89 34 24             	mov    %esi,(%esp)
  801df2:	85 c0                	test   %eax,%eax
  801df4:	75 1a                	jne    801e10 <__umoddi3+0x48>
  801df6:	39 f7                	cmp    %esi,%edi
  801df8:	0f 86 a2 00 00 00    	jbe    801ea0 <__umoddi3+0xd8>
  801dfe:	89 c8                	mov    %ecx,%eax
  801e00:	89 f2                	mov    %esi,%edx
  801e02:	f7 f7                	div    %edi
  801e04:	89 d0                	mov    %edx,%eax
  801e06:	31 d2                	xor    %edx,%edx
  801e08:	83 c4 1c             	add    $0x1c,%esp
  801e0b:	5b                   	pop    %ebx
  801e0c:	5e                   	pop    %esi
  801e0d:	5f                   	pop    %edi
  801e0e:	5d                   	pop    %ebp
  801e0f:	c3                   	ret    
  801e10:	39 f0                	cmp    %esi,%eax
  801e12:	0f 87 ac 00 00 00    	ja     801ec4 <__umoddi3+0xfc>
  801e18:	0f bd e8             	bsr    %eax,%ebp
  801e1b:	83 f5 1f             	xor    $0x1f,%ebp
  801e1e:	0f 84 ac 00 00 00    	je     801ed0 <__umoddi3+0x108>
  801e24:	bf 20 00 00 00       	mov    $0x20,%edi
  801e29:	29 ef                	sub    %ebp,%edi
  801e2b:	89 fe                	mov    %edi,%esi
  801e2d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801e31:	89 e9                	mov    %ebp,%ecx
  801e33:	d3 e0                	shl    %cl,%eax
  801e35:	89 d7                	mov    %edx,%edi
  801e37:	89 f1                	mov    %esi,%ecx
  801e39:	d3 ef                	shr    %cl,%edi
  801e3b:	09 c7                	or     %eax,%edi
  801e3d:	89 e9                	mov    %ebp,%ecx
  801e3f:	d3 e2                	shl    %cl,%edx
  801e41:	89 14 24             	mov    %edx,(%esp)
  801e44:	89 d8                	mov    %ebx,%eax
  801e46:	d3 e0                	shl    %cl,%eax
  801e48:	89 c2                	mov    %eax,%edx
  801e4a:	8b 44 24 08          	mov    0x8(%esp),%eax
  801e4e:	d3 e0                	shl    %cl,%eax
  801e50:	89 44 24 04          	mov    %eax,0x4(%esp)
  801e54:	8b 44 24 08          	mov    0x8(%esp),%eax
  801e58:	89 f1                	mov    %esi,%ecx
  801e5a:	d3 e8                	shr    %cl,%eax
  801e5c:	09 d0                	or     %edx,%eax
  801e5e:	d3 eb                	shr    %cl,%ebx
  801e60:	89 da                	mov    %ebx,%edx
  801e62:	f7 f7                	div    %edi
  801e64:	89 d3                	mov    %edx,%ebx
  801e66:	f7 24 24             	mull   (%esp)
  801e69:	89 c6                	mov    %eax,%esi
  801e6b:	89 d1                	mov    %edx,%ecx
  801e6d:	39 d3                	cmp    %edx,%ebx
  801e6f:	0f 82 87 00 00 00    	jb     801efc <__umoddi3+0x134>
  801e75:	0f 84 91 00 00 00    	je     801f0c <__umoddi3+0x144>
  801e7b:	8b 54 24 04          	mov    0x4(%esp),%edx
  801e7f:	29 f2                	sub    %esi,%edx
  801e81:	19 cb                	sbb    %ecx,%ebx
  801e83:	89 d8                	mov    %ebx,%eax
  801e85:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  801e89:	d3 e0                	shl    %cl,%eax
  801e8b:	89 e9                	mov    %ebp,%ecx
  801e8d:	d3 ea                	shr    %cl,%edx
  801e8f:	09 d0                	or     %edx,%eax
  801e91:	89 e9                	mov    %ebp,%ecx
  801e93:	d3 eb                	shr    %cl,%ebx
  801e95:	89 da                	mov    %ebx,%edx
  801e97:	83 c4 1c             	add    $0x1c,%esp
  801e9a:	5b                   	pop    %ebx
  801e9b:	5e                   	pop    %esi
  801e9c:	5f                   	pop    %edi
  801e9d:	5d                   	pop    %ebp
  801e9e:	c3                   	ret    
  801e9f:	90                   	nop
  801ea0:	89 fd                	mov    %edi,%ebp
  801ea2:	85 ff                	test   %edi,%edi
  801ea4:	75 0b                	jne    801eb1 <__umoddi3+0xe9>
  801ea6:	b8 01 00 00 00       	mov    $0x1,%eax
  801eab:	31 d2                	xor    %edx,%edx
  801ead:	f7 f7                	div    %edi
  801eaf:	89 c5                	mov    %eax,%ebp
  801eb1:	89 f0                	mov    %esi,%eax
  801eb3:	31 d2                	xor    %edx,%edx
  801eb5:	f7 f5                	div    %ebp
  801eb7:	89 c8                	mov    %ecx,%eax
  801eb9:	f7 f5                	div    %ebp
  801ebb:	89 d0                	mov    %edx,%eax
  801ebd:	e9 44 ff ff ff       	jmp    801e06 <__umoddi3+0x3e>
  801ec2:	66 90                	xchg   %ax,%ax
  801ec4:	89 c8                	mov    %ecx,%eax
  801ec6:	89 f2                	mov    %esi,%edx
  801ec8:	83 c4 1c             	add    $0x1c,%esp
  801ecb:	5b                   	pop    %ebx
  801ecc:	5e                   	pop    %esi
  801ecd:	5f                   	pop    %edi
  801ece:	5d                   	pop    %ebp
  801ecf:	c3                   	ret    
  801ed0:	3b 04 24             	cmp    (%esp),%eax
  801ed3:	72 06                	jb     801edb <__umoddi3+0x113>
  801ed5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  801ed9:	77 0f                	ja     801eea <__umoddi3+0x122>
  801edb:	89 f2                	mov    %esi,%edx
  801edd:	29 f9                	sub    %edi,%ecx
  801edf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  801ee3:	89 14 24             	mov    %edx,(%esp)
  801ee6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  801eea:	8b 44 24 04          	mov    0x4(%esp),%eax
  801eee:	8b 14 24             	mov    (%esp),%edx
  801ef1:	83 c4 1c             	add    $0x1c,%esp
  801ef4:	5b                   	pop    %ebx
  801ef5:	5e                   	pop    %esi
  801ef6:	5f                   	pop    %edi
  801ef7:	5d                   	pop    %ebp
  801ef8:	c3                   	ret    
  801ef9:	8d 76 00             	lea    0x0(%esi),%esi
  801efc:	2b 04 24             	sub    (%esp),%eax
  801eff:	19 fa                	sbb    %edi,%edx
  801f01:	89 d1                	mov    %edx,%ecx
  801f03:	89 c6                	mov    %eax,%esi
  801f05:	e9 71 ff ff ff       	jmp    801e7b <__umoddi3+0xb3>
  801f0a:	66 90                	xchg   %ax,%ax
  801f0c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  801f10:	72 ea                	jb     801efc <__umoddi3+0x134>
  801f12:	89 d9                	mov    %ebx,%ecx
  801f14:	e9 62 ff ff ff       	jmp    801e7b <__umoddi3+0xb3>
