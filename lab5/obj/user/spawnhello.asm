
obj/user/spawnhello.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 4a 00 00 00       	call   80007b <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	int r;
	cprintf("i am parent environment %08x\n", thisenv->env_id);
  800039:	a1 04 40 80 00       	mov    0x804004,%eax
  80003e:	8b 40 48             	mov    0x48(%eax),%eax
  800041:	50                   	push   %eax
  800042:	68 40 23 80 00       	push   $0x802340
  800047:	e8 68 01 00 00       	call   8001b4 <cprintf>
	if ((r = spawnl("hello", "hello", 0)) < 0)
  80004c:	83 c4 0c             	add    $0xc,%esp
  80004f:	6a 00                	push   $0x0
  800051:	68 5e 23 80 00       	push   $0x80235e
  800056:	68 5e 23 80 00       	push   $0x80235e
  80005b:	e8 24 12 00 00       	call   801284 <spawnl>
  800060:	83 c4 10             	add    $0x10,%esp
  800063:	85 c0                	test   %eax,%eax
  800065:	79 12                	jns    800079 <umain+0x46>
		panic("spawn(hello) failed: %e", r);
  800067:	50                   	push   %eax
  800068:	68 64 23 80 00       	push   $0x802364
  80006d:	6a 09                	push   $0x9
  80006f:	68 7c 23 80 00       	push   $0x80237c
  800074:	e8 63 00 00 00       	call   8000dc <_panic>
}
  800079:	c9                   	leave  
  80007a:	c3                   	ret    

0080007b <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80007b:	55                   	push   %ebp
  80007c:	89 e5                	mov    %esp,%ebp
  80007e:	56                   	push   %esi
  80007f:	53                   	push   %ebx
  800080:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800083:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800086:	e8 33 0a 00 00       	call   800abe <sys_getenvid>
  80008b:	25 ff 03 00 00       	and    $0x3ff,%eax
  800090:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800097:	c1 e0 07             	shl    $0x7,%eax
  80009a:	29 d0                	sub    %edx,%eax
  80009c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000a1:	a3 04 40 80 00       	mov    %eax,0x804004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000a6:	85 db                	test   %ebx,%ebx
  8000a8:	7e 07                	jle    8000b1 <libmain+0x36>
		binaryname = argv[0];
  8000aa:	8b 06                	mov    (%esi),%eax
  8000ac:	a3 00 30 80 00       	mov    %eax,0x803000

	// call user main routine
	umain(argc, argv);
  8000b1:	83 ec 08             	sub    $0x8,%esp
  8000b4:	56                   	push   %esi
  8000b5:	53                   	push   %ebx
  8000b6:	e8 78 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000bb:	e8 0a 00 00 00       	call   8000ca <exit>
}
  8000c0:	83 c4 10             	add    $0x10,%esp
  8000c3:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000c6:	5b                   	pop    %ebx
  8000c7:	5e                   	pop    %esi
  8000c8:	5d                   	pop    %ebp
  8000c9:	c3                   	ret    

008000ca <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000ca:	55                   	push   %ebp
  8000cb:	89 e5                	mov    %esp,%ebp
  8000cd:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  8000d0:	6a 00                	push   $0x0
  8000d2:	e8 a6 09 00 00       	call   800a7d <sys_env_destroy>
}
  8000d7:	83 c4 10             	add    $0x10,%esp
  8000da:	c9                   	leave  
  8000db:	c3                   	ret    

008000dc <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  8000dc:	55                   	push   %ebp
  8000dd:	89 e5                	mov    %esp,%ebp
  8000df:	56                   	push   %esi
  8000e0:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  8000e1:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  8000e4:	8b 35 00 30 80 00    	mov    0x803000,%esi
  8000ea:	e8 cf 09 00 00       	call   800abe <sys_getenvid>
  8000ef:	83 ec 0c             	sub    $0xc,%esp
  8000f2:	ff 75 0c             	pushl  0xc(%ebp)
  8000f5:	ff 75 08             	pushl  0x8(%ebp)
  8000f8:	56                   	push   %esi
  8000f9:	50                   	push   %eax
  8000fa:	68 98 23 80 00       	push   $0x802398
  8000ff:	e8 b0 00 00 00       	call   8001b4 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800104:	83 c4 18             	add    $0x18,%esp
  800107:	53                   	push   %ebx
  800108:	ff 75 10             	pushl  0x10(%ebp)
  80010b:	e8 53 00 00 00       	call   800163 <vcprintf>
	cprintf("\n");
  800110:	c7 04 24 7a 28 80 00 	movl   $0x80287a,(%esp)
  800117:	e8 98 00 00 00       	call   8001b4 <cprintf>
  80011c:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80011f:	cc                   	int3   
  800120:	eb fd                	jmp    80011f <_panic+0x43>

00800122 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800122:	55                   	push   %ebp
  800123:	89 e5                	mov    %esp,%ebp
  800125:	53                   	push   %ebx
  800126:	83 ec 04             	sub    $0x4,%esp
  800129:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80012c:	8b 13                	mov    (%ebx),%edx
  80012e:	8d 42 01             	lea    0x1(%edx),%eax
  800131:	89 03                	mov    %eax,(%ebx)
  800133:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800136:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80013a:	3d ff 00 00 00       	cmp    $0xff,%eax
  80013f:	75 1a                	jne    80015b <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800141:	83 ec 08             	sub    $0x8,%esp
  800144:	68 ff 00 00 00       	push   $0xff
  800149:	8d 43 08             	lea    0x8(%ebx),%eax
  80014c:	50                   	push   %eax
  80014d:	e8 ee 08 00 00       	call   800a40 <sys_cputs>
		b->idx = 0;
  800152:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800158:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80015b:	ff 43 04             	incl   0x4(%ebx)
}
  80015e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800161:	c9                   	leave  
  800162:	c3                   	ret    

00800163 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800163:	55                   	push   %ebp
  800164:	89 e5                	mov    %esp,%ebp
  800166:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80016c:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800173:	00 00 00 
	b.cnt = 0;
  800176:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80017d:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800180:	ff 75 0c             	pushl  0xc(%ebp)
  800183:	ff 75 08             	pushl  0x8(%ebp)
  800186:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80018c:	50                   	push   %eax
  80018d:	68 22 01 80 00       	push   $0x800122
  800192:	e8 51 01 00 00       	call   8002e8 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800197:	83 c4 08             	add    $0x8,%esp
  80019a:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001a0:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001a6:	50                   	push   %eax
  8001a7:	e8 94 08 00 00       	call   800a40 <sys_cputs>

	return b.cnt;
}
  8001ac:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  8001b2:	c9                   	leave  
  8001b3:	c3                   	ret    

008001b4 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001b4:	55                   	push   %ebp
  8001b5:	89 e5                	mov    %esp,%ebp
  8001b7:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001ba:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  8001bd:	50                   	push   %eax
  8001be:	ff 75 08             	pushl  0x8(%ebp)
  8001c1:	e8 9d ff ff ff       	call   800163 <vcprintf>
	va_end(ap);

	return cnt;
}
  8001c6:	c9                   	leave  
  8001c7:	c3                   	ret    

008001c8 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001c8:	55                   	push   %ebp
  8001c9:	89 e5                	mov    %esp,%ebp
  8001cb:	57                   	push   %edi
  8001cc:	56                   	push   %esi
  8001cd:	53                   	push   %ebx
  8001ce:	83 ec 1c             	sub    $0x1c,%esp
  8001d1:	89 c7                	mov    %eax,%edi
  8001d3:	89 d6                	mov    %edx,%esi
  8001d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8001d8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001db:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001de:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001e1:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001e4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001e9:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001ec:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001ef:	39 d3                	cmp    %edx,%ebx
  8001f1:	72 05                	jb     8001f8 <printnum+0x30>
  8001f3:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001f6:	77 45                	ja     80023d <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001f8:	83 ec 0c             	sub    $0xc,%esp
  8001fb:	ff 75 18             	pushl  0x18(%ebp)
  8001fe:	8b 45 14             	mov    0x14(%ebp),%eax
  800201:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800204:	53                   	push   %ebx
  800205:	ff 75 10             	pushl  0x10(%ebp)
  800208:	83 ec 08             	sub    $0x8,%esp
  80020b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80020e:	ff 75 e0             	pushl  -0x20(%ebp)
  800211:	ff 75 dc             	pushl  -0x24(%ebp)
  800214:	ff 75 d8             	pushl  -0x28(%ebp)
  800217:	e8 bc 1e 00 00       	call   8020d8 <__udivdi3>
  80021c:	83 c4 18             	add    $0x18,%esp
  80021f:	52                   	push   %edx
  800220:	50                   	push   %eax
  800221:	89 f2                	mov    %esi,%edx
  800223:	89 f8                	mov    %edi,%eax
  800225:	e8 9e ff ff ff       	call   8001c8 <printnum>
  80022a:	83 c4 20             	add    $0x20,%esp
  80022d:	eb 16                	jmp    800245 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80022f:	83 ec 08             	sub    $0x8,%esp
  800232:	56                   	push   %esi
  800233:	ff 75 18             	pushl  0x18(%ebp)
  800236:	ff d7                	call   *%edi
  800238:	83 c4 10             	add    $0x10,%esp
  80023b:	eb 03                	jmp    800240 <printnum+0x78>
  80023d:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800240:	4b                   	dec    %ebx
  800241:	85 db                	test   %ebx,%ebx
  800243:	7f ea                	jg     80022f <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800245:	83 ec 08             	sub    $0x8,%esp
  800248:	56                   	push   %esi
  800249:	83 ec 04             	sub    $0x4,%esp
  80024c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80024f:	ff 75 e0             	pushl  -0x20(%ebp)
  800252:	ff 75 dc             	pushl  -0x24(%ebp)
  800255:	ff 75 d8             	pushl  -0x28(%ebp)
  800258:	e8 8b 1f 00 00       	call   8021e8 <__umoddi3>
  80025d:	83 c4 14             	add    $0x14,%esp
  800260:	0f be 80 bb 23 80 00 	movsbl 0x8023bb(%eax),%eax
  800267:	50                   	push   %eax
  800268:	ff d7                	call   *%edi
}
  80026a:	83 c4 10             	add    $0x10,%esp
  80026d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800270:	5b                   	pop    %ebx
  800271:	5e                   	pop    %esi
  800272:	5f                   	pop    %edi
  800273:	5d                   	pop    %ebp
  800274:	c3                   	ret    

00800275 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800275:	55                   	push   %ebp
  800276:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800278:	83 fa 01             	cmp    $0x1,%edx
  80027b:	7e 0e                	jle    80028b <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80027d:	8b 10                	mov    (%eax),%edx
  80027f:	8d 4a 08             	lea    0x8(%edx),%ecx
  800282:	89 08                	mov    %ecx,(%eax)
  800284:	8b 02                	mov    (%edx),%eax
  800286:	8b 52 04             	mov    0x4(%edx),%edx
  800289:	eb 22                	jmp    8002ad <getuint+0x38>
	else if (lflag)
  80028b:	85 d2                	test   %edx,%edx
  80028d:	74 10                	je     80029f <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80028f:	8b 10                	mov    (%eax),%edx
  800291:	8d 4a 04             	lea    0x4(%edx),%ecx
  800294:	89 08                	mov    %ecx,(%eax)
  800296:	8b 02                	mov    (%edx),%eax
  800298:	ba 00 00 00 00       	mov    $0x0,%edx
  80029d:	eb 0e                	jmp    8002ad <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80029f:	8b 10                	mov    (%eax),%edx
  8002a1:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002a4:	89 08                	mov    %ecx,(%eax)
  8002a6:	8b 02                	mov    (%edx),%eax
  8002a8:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002ad:	5d                   	pop    %ebp
  8002ae:	c3                   	ret    

008002af <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8002af:	55                   	push   %ebp
  8002b0:	89 e5                	mov    %esp,%ebp
  8002b2:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  8002b5:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  8002b8:	8b 10                	mov    (%eax),%edx
  8002ba:	3b 50 04             	cmp    0x4(%eax),%edx
  8002bd:	73 0a                	jae    8002c9 <sprintputch+0x1a>
		*b->buf++ = ch;
  8002bf:	8d 4a 01             	lea    0x1(%edx),%ecx
  8002c2:	89 08                	mov    %ecx,(%eax)
  8002c4:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c7:	88 02                	mov    %al,(%edx)
}
  8002c9:	5d                   	pop    %ebp
  8002ca:	c3                   	ret    

008002cb <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002cb:	55                   	push   %ebp
  8002cc:	89 e5                	mov    %esp,%ebp
  8002ce:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002d1:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002d4:	50                   	push   %eax
  8002d5:	ff 75 10             	pushl  0x10(%ebp)
  8002d8:	ff 75 0c             	pushl  0xc(%ebp)
  8002db:	ff 75 08             	pushl  0x8(%ebp)
  8002de:	e8 05 00 00 00       	call   8002e8 <vprintfmt>
	va_end(ap);
}
  8002e3:	83 c4 10             	add    $0x10,%esp
  8002e6:	c9                   	leave  
  8002e7:	c3                   	ret    

008002e8 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002e8:	55                   	push   %ebp
  8002e9:	89 e5                	mov    %esp,%ebp
  8002eb:	57                   	push   %edi
  8002ec:	56                   	push   %esi
  8002ed:	53                   	push   %ebx
  8002ee:	83 ec 2c             	sub    $0x2c,%esp
  8002f1:	8b 75 08             	mov    0x8(%ebp),%esi
  8002f4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002f7:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002fa:	eb 12                	jmp    80030e <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002fc:	85 c0                	test   %eax,%eax
  8002fe:	0f 84 68 03 00 00    	je     80066c <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800304:	83 ec 08             	sub    $0x8,%esp
  800307:	53                   	push   %ebx
  800308:	50                   	push   %eax
  800309:	ff d6                	call   *%esi
  80030b:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80030e:	47                   	inc    %edi
  80030f:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800313:	83 f8 25             	cmp    $0x25,%eax
  800316:	75 e4                	jne    8002fc <vprintfmt+0x14>
  800318:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80031c:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800323:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80032a:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800331:	ba 00 00 00 00       	mov    $0x0,%edx
  800336:	eb 07                	jmp    80033f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800338:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80033b:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80033f:	8d 47 01             	lea    0x1(%edi),%eax
  800342:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800345:	0f b6 0f             	movzbl (%edi),%ecx
  800348:	8a 07                	mov    (%edi),%al
  80034a:	83 e8 23             	sub    $0x23,%eax
  80034d:	3c 55                	cmp    $0x55,%al
  80034f:	0f 87 fe 02 00 00    	ja     800653 <vprintfmt+0x36b>
  800355:	0f b6 c0             	movzbl %al,%eax
  800358:	ff 24 85 00 25 80 00 	jmp    *0x802500(,%eax,4)
  80035f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800362:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800366:	eb d7                	jmp    80033f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800368:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80036b:	b8 00 00 00 00       	mov    $0x0,%eax
  800370:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800373:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800376:	01 c0                	add    %eax,%eax
  800378:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80037c:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80037f:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800382:	83 fa 09             	cmp    $0x9,%edx
  800385:	77 34                	ja     8003bb <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800387:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800388:	eb e9                	jmp    800373 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80038a:	8b 45 14             	mov    0x14(%ebp),%eax
  80038d:	8d 48 04             	lea    0x4(%eax),%ecx
  800390:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800393:	8b 00                	mov    (%eax),%eax
  800395:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800398:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80039b:	eb 24                	jmp    8003c1 <vprintfmt+0xd9>
  80039d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003a1:	79 07                	jns    8003aa <vprintfmt+0xc2>
  8003a3:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003aa:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003ad:	eb 90                	jmp    80033f <vprintfmt+0x57>
  8003af:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  8003b2:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  8003b9:	eb 84                	jmp    80033f <vprintfmt+0x57>
  8003bb:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003be:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  8003c1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003c5:	0f 89 74 ff ff ff    	jns    80033f <vprintfmt+0x57>
				width = precision, precision = -1;
  8003cb:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003ce:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003d1:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003d8:	e9 62 ff ff ff       	jmp    80033f <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003dd:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003de:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003e1:	e9 59 ff ff ff       	jmp    80033f <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e9:	8d 50 04             	lea    0x4(%eax),%edx
  8003ec:	89 55 14             	mov    %edx,0x14(%ebp)
  8003ef:	83 ec 08             	sub    $0x8,%esp
  8003f2:	53                   	push   %ebx
  8003f3:	ff 30                	pushl  (%eax)
  8003f5:	ff d6                	call   *%esi
			break;
  8003f7:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003fa:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003fd:	e9 0c ff ff ff       	jmp    80030e <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800402:	8b 45 14             	mov    0x14(%ebp),%eax
  800405:	8d 50 04             	lea    0x4(%eax),%edx
  800408:	89 55 14             	mov    %edx,0x14(%ebp)
  80040b:	8b 00                	mov    (%eax),%eax
  80040d:	85 c0                	test   %eax,%eax
  80040f:	79 02                	jns    800413 <vprintfmt+0x12b>
  800411:	f7 d8                	neg    %eax
  800413:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800415:	83 f8 0f             	cmp    $0xf,%eax
  800418:	7f 0b                	jg     800425 <vprintfmt+0x13d>
  80041a:	8b 04 85 60 26 80 00 	mov    0x802660(,%eax,4),%eax
  800421:	85 c0                	test   %eax,%eax
  800423:	75 18                	jne    80043d <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800425:	52                   	push   %edx
  800426:	68 d3 23 80 00       	push   $0x8023d3
  80042b:	53                   	push   %ebx
  80042c:	56                   	push   %esi
  80042d:	e8 99 fe ff ff       	call   8002cb <printfmt>
  800432:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800435:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800438:	e9 d1 fe ff ff       	jmp    80030e <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80043d:	50                   	push   %eax
  80043e:	68 16 27 80 00       	push   $0x802716
  800443:	53                   	push   %ebx
  800444:	56                   	push   %esi
  800445:	e8 81 fe ff ff       	call   8002cb <printfmt>
  80044a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80044d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800450:	e9 b9 fe ff ff       	jmp    80030e <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800455:	8b 45 14             	mov    0x14(%ebp),%eax
  800458:	8d 50 04             	lea    0x4(%eax),%edx
  80045b:	89 55 14             	mov    %edx,0x14(%ebp)
  80045e:	8b 38                	mov    (%eax),%edi
  800460:	85 ff                	test   %edi,%edi
  800462:	75 05                	jne    800469 <vprintfmt+0x181>
				p = "(null)";
  800464:	bf cc 23 80 00       	mov    $0x8023cc,%edi
			if (width > 0 && padc != '-')
  800469:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80046d:	0f 8e 90 00 00 00    	jle    800503 <vprintfmt+0x21b>
  800473:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800477:	0f 84 8e 00 00 00    	je     80050b <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80047d:	83 ec 08             	sub    $0x8,%esp
  800480:	ff 75 d0             	pushl  -0x30(%ebp)
  800483:	57                   	push   %edi
  800484:	e8 70 02 00 00       	call   8006f9 <strnlen>
  800489:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80048c:	29 c1                	sub    %eax,%ecx
  80048e:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800491:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800494:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800498:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80049b:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80049e:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004a0:	eb 0d                	jmp    8004af <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004a2:	83 ec 08             	sub    $0x8,%esp
  8004a5:	53                   	push   %ebx
  8004a6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004a9:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004ab:	4f                   	dec    %edi
  8004ac:	83 c4 10             	add    $0x10,%esp
  8004af:	85 ff                	test   %edi,%edi
  8004b1:	7f ef                	jg     8004a2 <vprintfmt+0x1ba>
  8004b3:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  8004b6:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004b9:	89 c8                	mov    %ecx,%eax
  8004bb:	85 c9                	test   %ecx,%ecx
  8004bd:	79 05                	jns    8004c4 <vprintfmt+0x1dc>
  8004bf:	b8 00 00 00 00       	mov    $0x0,%eax
  8004c4:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004c7:	29 c1                	sub    %eax,%ecx
  8004c9:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004cc:	89 75 08             	mov    %esi,0x8(%ebp)
  8004cf:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d2:	eb 3d                	jmp    800511 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004d4:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004d8:	74 19                	je     8004f3 <vprintfmt+0x20b>
  8004da:	0f be c0             	movsbl %al,%eax
  8004dd:	83 e8 20             	sub    $0x20,%eax
  8004e0:	83 f8 5e             	cmp    $0x5e,%eax
  8004e3:	76 0e                	jbe    8004f3 <vprintfmt+0x20b>
					putch('?', putdat);
  8004e5:	83 ec 08             	sub    $0x8,%esp
  8004e8:	53                   	push   %ebx
  8004e9:	6a 3f                	push   $0x3f
  8004eb:	ff 55 08             	call   *0x8(%ebp)
  8004ee:	83 c4 10             	add    $0x10,%esp
  8004f1:	eb 0b                	jmp    8004fe <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004f3:	83 ec 08             	sub    $0x8,%esp
  8004f6:	53                   	push   %ebx
  8004f7:	52                   	push   %edx
  8004f8:	ff 55 08             	call   *0x8(%ebp)
  8004fb:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004fe:	ff 4d e4             	decl   -0x1c(%ebp)
  800501:	eb 0e                	jmp    800511 <vprintfmt+0x229>
  800503:	89 75 08             	mov    %esi,0x8(%ebp)
  800506:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800509:	eb 06                	jmp    800511 <vprintfmt+0x229>
  80050b:	89 75 08             	mov    %esi,0x8(%ebp)
  80050e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800511:	47                   	inc    %edi
  800512:	8a 47 ff             	mov    -0x1(%edi),%al
  800515:	0f be d0             	movsbl %al,%edx
  800518:	85 d2                	test   %edx,%edx
  80051a:	74 1d                	je     800539 <vprintfmt+0x251>
  80051c:	85 f6                	test   %esi,%esi
  80051e:	78 b4                	js     8004d4 <vprintfmt+0x1ec>
  800520:	4e                   	dec    %esi
  800521:	79 b1                	jns    8004d4 <vprintfmt+0x1ec>
  800523:	8b 75 08             	mov    0x8(%ebp),%esi
  800526:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800529:	eb 14                	jmp    80053f <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80052b:	83 ec 08             	sub    $0x8,%esp
  80052e:	53                   	push   %ebx
  80052f:	6a 20                	push   $0x20
  800531:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800533:	4f                   	dec    %edi
  800534:	83 c4 10             	add    $0x10,%esp
  800537:	eb 06                	jmp    80053f <vprintfmt+0x257>
  800539:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80053c:	8b 75 08             	mov    0x8(%ebp),%esi
  80053f:	85 ff                	test   %edi,%edi
  800541:	7f e8                	jg     80052b <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800543:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800546:	e9 c3 fd ff ff       	jmp    80030e <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80054b:	83 fa 01             	cmp    $0x1,%edx
  80054e:	7e 16                	jle    800566 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800550:	8b 45 14             	mov    0x14(%ebp),%eax
  800553:	8d 50 08             	lea    0x8(%eax),%edx
  800556:	89 55 14             	mov    %edx,0x14(%ebp)
  800559:	8b 50 04             	mov    0x4(%eax),%edx
  80055c:	8b 00                	mov    (%eax),%eax
  80055e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800561:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800564:	eb 32                	jmp    800598 <vprintfmt+0x2b0>
	else if (lflag)
  800566:	85 d2                	test   %edx,%edx
  800568:	74 18                	je     800582 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80056a:	8b 45 14             	mov    0x14(%ebp),%eax
  80056d:	8d 50 04             	lea    0x4(%eax),%edx
  800570:	89 55 14             	mov    %edx,0x14(%ebp)
  800573:	8b 00                	mov    (%eax),%eax
  800575:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800578:	89 c1                	mov    %eax,%ecx
  80057a:	c1 f9 1f             	sar    $0x1f,%ecx
  80057d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800580:	eb 16                	jmp    800598 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800582:	8b 45 14             	mov    0x14(%ebp),%eax
  800585:	8d 50 04             	lea    0x4(%eax),%edx
  800588:	89 55 14             	mov    %edx,0x14(%ebp)
  80058b:	8b 00                	mov    (%eax),%eax
  80058d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800590:	89 c1                	mov    %eax,%ecx
  800592:	c1 f9 1f             	sar    $0x1f,%ecx
  800595:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800598:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80059b:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80059e:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005a2:	79 76                	jns    80061a <vprintfmt+0x332>
				putch('-', putdat);
  8005a4:	83 ec 08             	sub    $0x8,%esp
  8005a7:	53                   	push   %ebx
  8005a8:	6a 2d                	push   $0x2d
  8005aa:	ff d6                	call   *%esi
				num = -(long long) num;
  8005ac:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005af:	8b 55 dc             	mov    -0x24(%ebp),%edx
  8005b2:	f7 d8                	neg    %eax
  8005b4:	83 d2 00             	adc    $0x0,%edx
  8005b7:	f7 da                	neg    %edx
  8005b9:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  8005bc:	b9 0a 00 00 00       	mov    $0xa,%ecx
  8005c1:	eb 5c                	jmp    80061f <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005c3:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c6:	e8 aa fc ff ff       	call   800275 <getuint>
			base = 10;
  8005cb:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005d0:	eb 4d                	jmp    80061f <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005d2:	8d 45 14             	lea    0x14(%ebp),%eax
  8005d5:	e8 9b fc ff ff       	call   800275 <getuint>
			base = 8;
  8005da:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005df:	eb 3e                	jmp    80061f <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005e1:	83 ec 08             	sub    $0x8,%esp
  8005e4:	53                   	push   %ebx
  8005e5:	6a 30                	push   $0x30
  8005e7:	ff d6                	call   *%esi
			putch('x', putdat);
  8005e9:	83 c4 08             	add    $0x8,%esp
  8005ec:	53                   	push   %ebx
  8005ed:	6a 78                	push   $0x78
  8005ef:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005f1:	8b 45 14             	mov    0x14(%ebp),%eax
  8005f4:	8d 50 04             	lea    0x4(%eax),%edx
  8005f7:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005fa:	8b 00                	mov    (%eax),%eax
  8005fc:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800601:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800604:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800609:	eb 14                	jmp    80061f <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80060b:	8d 45 14             	lea    0x14(%ebp),%eax
  80060e:	e8 62 fc ff ff       	call   800275 <getuint>
			base = 16;
  800613:	b9 10 00 00 00       	mov    $0x10,%ecx
  800618:	eb 05                	jmp    80061f <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80061a:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80061f:	83 ec 0c             	sub    $0xc,%esp
  800622:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800626:	57                   	push   %edi
  800627:	ff 75 e4             	pushl  -0x1c(%ebp)
  80062a:	51                   	push   %ecx
  80062b:	52                   	push   %edx
  80062c:	50                   	push   %eax
  80062d:	89 da                	mov    %ebx,%edx
  80062f:	89 f0                	mov    %esi,%eax
  800631:	e8 92 fb ff ff       	call   8001c8 <printnum>
			break;
  800636:	83 c4 20             	add    $0x20,%esp
  800639:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80063c:	e9 cd fc ff ff       	jmp    80030e <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800641:	83 ec 08             	sub    $0x8,%esp
  800644:	53                   	push   %ebx
  800645:	51                   	push   %ecx
  800646:	ff d6                	call   *%esi
			break;
  800648:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80064b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80064e:	e9 bb fc ff ff       	jmp    80030e <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800653:	83 ec 08             	sub    $0x8,%esp
  800656:	53                   	push   %ebx
  800657:	6a 25                	push   $0x25
  800659:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80065b:	83 c4 10             	add    $0x10,%esp
  80065e:	eb 01                	jmp    800661 <vprintfmt+0x379>
  800660:	4f                   	dec    %edi
  800661:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800665:	75 f9                	jne    800660 <vprintfmt+0x378>
  800667:	e9 a2 fc ff ff       	jmp    80030e <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80066c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80066f:	5b                   	pop    %ebx
  800670:	5e                   	pop    %esi
  800671:	5f                   	pop    %edi
  800672:	5d                   	pop    %ebp
  800673:	c3                   	ret    

00800674 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800674:	55                   	push   %ebp
  800675:	89 e5                	mov    %esp,%ebp
  800677:	83 ec 18             	sub    $0x18,%esp
  80067a:	8b 45 08             	mov    0x8(%ebp),%eax
  80067d:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800680:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800683:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800687:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80068a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800691:	85 c0                	test   %eax,%eax
  800693:	74 26                	je     8006bb <vsnprintf+0x47>
  800695:	85 d2                	test   %edx,%edx
  800697:	7e 29                	jle    8006c2 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800699:	ff 75 14             	pushl  0x14(%ebp)
  80069c:	ff 75 10             	pushl  0x10(%ebp)
  80069f:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006a2:	50                   	push   %eax
  8006a3:	68 af 02 80 00       	push   $0x8002af
  8006a8:	e8 3b fc ff ff       	call   8002e8 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8006ad:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006b0:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8006b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8006b6:	83 c4 10             	add    $0x10,%esp
  8006b9:	eb 0c                	jmp    8006c7 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  8006bb:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8006c0:	eb 05                	jmp    8006c7 <vsnprintf+0x53>
  8006c2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8006c7:	c9                   	leave  
  8006c8:	c3                   	ret    

008006c9 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006c9:	55                   	push   %ebp
  8006ca:	89 e5                	mov    %esp,%ebp
  8006cc:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006cf:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006d2:	50                   	push   %eax
  8006d3:	ff 75 10             	pushl  0x10(%ebp)
  8006d6:	ff 75 0c             	pushl  0xc(%ebp)
  8006d9:	ff 75 08             	pushl  0x8(%ebp)
  8006dc:	e8 93 ff ff ff       	call   800674 <vsnprintf>
	va_end(ap);

	return rc;
}
  8006e1:	c9                   	leave  
  8006e2:	c3                   	ret    

008006e3 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006e3:	55                   	push   %ebp
  8006e4:	89 e5                	mov    %esp,%ebp
  8006e6:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006e9:	b8 00 00 00 00       	mov    $0x0,%eax
  8006ee:	eb 01                	jmp    8006f1 <strlen+0xe>
		n++;
  8006f0:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006f1:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006f5:	75 f9                	jne    8006f0 <strlen+0xd>
		n++;
	return n;
}
  8006f7:	5d                   	pop    %ebp
  8006f8:	c3                   	ret    

008006f9 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006f9:	55                   	push   %ebp
  8006fa:	89 e5                	mov    %esp,%ebp
  8006fc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006ff:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800702:	ba 00 00 00 00       	mov    $0x0,%edx
  800707:	eb 01                	jmp    80070a <strnlen+0x11>
		n++;
  800709:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80070a:	39 c2                	cmp    %eax,%edx
  80070c:	74 08                	je     800716 <strnlen+0x1d>
  80070e:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800712:	75 f5                	jne    800709 <strnlen+0x10>
  800714:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800716:	5d                   	pop    %ebp
  800717:	c3                   	ret    

00800718 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800718:	55                   	push   %ebp
  800719:	89 e5                	mov    %esp,%ebp
  80071b:	53                   	push   %ebx
  80071c:	8b 45 08             	mov    0x8(%ebp),%eax
  80071f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800722:	89 c2                	mov    %eax,%edx
  800724:	42                   	inc    %edx
  800725:	41                   	inc    %ecx
  800726:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800729:	88 5a ff             	mov    %bl,-0x1(%edx)
  80072c:	84 db                	test   %bl,%bl
  80072e:	75 f4                	jne    800724 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800730:	5b                   	pop    %ebx
  800731:	5d                   	pop    %ebp
  800732:	c3                   	ret    

00800733 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800733:	55                   	push   %ebp
  800734:	89 e5                	mov    %esp,%ebp
  800736:	53                   	push   %ebx
  800737:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80073a:	53                   	push   %ebx
  80073b:	e8 a3 ff ff ff       	call   8006e3 <strlen>
  800740:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800743:	ff 75 0c             	pushl  0xc(%ebp)
  800746:	01 d8                	add    %ebx,%eax
  800748:	50                   	push   %eax
  800749:	e8 ca ff ff ff       	call   800718 <strcpy>
	return dst;
}
  80074e:	89 d8                	mov    %ebx,%eax
  800750:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800753:	c9                   	leave  
  800754:	c3                   	ret    

00800755 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800755:	55                   	push   %ebp
  800756:	89 e5                	mov    %esp,%ebp
  800758:	56                   	push   %esi
  800759:	53                   	push   %ebx
  80075a:	8b 75 08             	mov    0x8(%ebp),%esi
  80075d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800760:	89 f3                	mov    %esi,%ebx
  800762:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800765:	89 f2                	mov    %esi,%edx
  800767:	eb 0c                	jmp    800775 <strncpy+0x20>
		*dst++ = *src;
  800769:	42                   	inc    %edx
  80076a:	8a 01                	mov    (%ecx),%al
  80076c:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80076f:	80 39 01             	cmpb   $0x1,(%ecx)
  800772:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800775:	39 da                	cmp    %ebx,%edx
  800777:	75 f0                	jne    800769 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800779:	89 f0                	mov    %esi,%eax
  80077b:	5b                   	pop    %ebx
  80077c:	5e                   	pop    %esi
  80077d:	5d                   	pop    %ebp
  80077e:	c3                   	ret    

0080077f <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80077f:	55                   	push   %ebp
  800780:	89 e5                	mov    %esp,%ebp
  800782:	56                   	push   %esi
  800783:	53                   	push   %ebx
  800784:	8b 75 08             	mov    0x8(%ebp),%esi
  800787:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80078a:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80078d:	85 c0                	test   %eax,%eax
  80078f:	74 1e                	je     8007af <strlcpy+0x30>
  800791:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800795:	89 f2                	mov    %esi,%edx
  800797:	eb 05                	jmp    80079e <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800799:	42                   	inc    %edx
  80079a:	41                   	inc    %ecx
  80079b:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80079e:	39 c2                	cmp    %eax,%edx
  8007a0:	74 08                	je     8007aa <strlcpy+0x2b>
  8007a2:	8a 19                	mov    (%ecx),%bl
  8007a4:	84 db                	test   %bl,%bl
  8007a6:	75 f1                	jne    800799 <strlcpy+0x1a>
  8007a8:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  8007aa:	c6 00 00             	movb   $0x0,(%eax)
  8007ad:	eb 02                	jmp    8007b1 <strlcpy+0x32>
  8007af:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  8007b1:	29 f0                	sub    %esi,%eax
}
  8007b3:	5b                   	pop    %ebx
  8007b4:	5e                   	pop    %esi
  8007b5:	5d                   	pop    %ebp
  8007b6:	c3                   	ret    

008007b7 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8007b7:	55                   	push   %ebp
  8007b8:	89 e5                	mov    %esp,%ebp
  8007ba:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8007bd:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  8007c0:	eb 02                	jmp    8007c4 <strcmp+0xd>
		p++, q++;
  8007c2:	41                   	inc    %ecx
  8007c3:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8007c4:	8a 01                	mov    (%ecx),%al
  8007c6:	84 c0                	test   %al,%al
  8007c8:	74 04                	je     8007ce <strcmp+0x17>
  8007ca:	3a 02                	cmp    (%edx),%al
  8007cc:	74 f4                	je     8007c2 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007ce:	0f b6 c0             	movzbl %al,%eax
  8007d1:	0f b6 12             	movzbl (%edx),%edx
  8007d4:	29 d0                	sub    %edx,%eax
}
  8007d6:	5d                   	pop    %ebp
  8007d7:	c3                   	ret    

008007d8 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007d8:	55                   	push   %ebp
  8007d9:	89 e5                	mov    %esp,%ebp
  8007db:	53                   	push   %ebx
  8007dc:	8b 45 08             	mov    0x8(%ebp),%eax
  8007df:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007e2:	89 c3                	mov    %eax,%ebx
  8007e4:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007e7:	eb 02                	jmp    8007eb <strncmp+0x13>
		n--, p++, q++;
  8007e9:	40                   	inc    %eax
  8007ea:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007eb:	39 d8                	cmp    %ebx,%eax
  8007ed:	74 14                	je     800803 <strncmp+0x2b>
  8007ef:	8a 08                	mov    (%eax),%cl
  8007f1:	84 c9                	test   %cl,%cl
  8007f3:	74 04                	je     8007f9 <strncmp+0x21>
  8007f5:	3a 0a                	cmp    (%edx),%cl
  8007f7:	74 f0                	je     8007e9 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007f9:	0f b6 00             	movzbl (%eax),%eax
  8007fc:	0f b6 12             	movzbl (%edx),%edx
  8007ff:	29 d0                	sub    %edx,%eax
  800801:	eb 05                	jmp    800808 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800803:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800808:	5b                   	pop    %ebx
  800809:	5d                   	pop    %ebp
  80080a:	c3                   	ret    

0080080b <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80080b:	55                   	push   %ebp
  80080c:	89 e5                	mov    %esp,%ebp
  80080e:	8b 45 08             	mov    0x8(%ebp),%eax
  800811:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800814:	eb 05                	jmp    80081b <strchr+0x10>
		if (*s == c)
  800816:	38 ca                	cmp    %cl,%dl
  800818:	74 0c                	je     800826 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80081a:	40                   	inc    %eax
  80081b:	8a 10                	mov    (%eax),%dl
  80081d:	84 d2                	test   %dl,%dl
  80081f:	75 f5                	jne    800816 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800821:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800826:	5d                   	pop    %ebp
  800827:	c3                   	ret    

00800828 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800828:	55                   	push   %ebp
  800829:	89 e5                	mov    %esp,%ebp
  80082b:	8b 45 08             	mov    0x8(%ebp),%eax
  80082e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800831:	eb 05                	jmp    800838 <strfind+0x10>
		if (*s == c)
  800833:	38 ca                	cmp    %cl,%dl
  800835:	74 07                	je     80083e <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800837:	40                   	inc    %eax
  800838:	8a 10                	mov    (%eax),%dl
  80083a:	84 d2                	test   %dl,%dl
  80083c:	75 f5                	jne    800833 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80083e:	5d                   	pop    %ebp
  80083f:	c3                   	ret    

00800840 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800840:	55                   	push   %ebp
  800841:	89 e5                	mov    %esp,%ebp
  800843:	57                   	push   %edi
  800844:	56                   	push   %esi
  800845:	53                   	push   %ebx
  800846:	8b 7d 08             	mov    0x8(%ebp),%edi
  800849:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80084c:	85 c9                	test   %ecx,%ecx
  80084e:	74 36                	je     800886 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800850:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800856:	75 28                	jne    800880 <memset+0x40>
  800858:	f6 c1 03             	test   $0x3,%cl
  80085b:	75 23                	jne    800880 <memset+0x40>
		c &= 0xFF;
  80085d:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800861:	89 d3                	mov    %edx,%ebx
  800863:	c1 e3 08             	shl    $0x8,%ebx
  800866:	89 d6                	mov    %edx,%esi
  800868:	c1 e6 18             	shl    $0x18,%esi
  80086b:	89 d0                	mov    %edx,%eax
  80086d:	c1 e0 10             	shl    $0x10,%eax
  800870:	09 f0                	or     %esi,%eax
  800872:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800874:	89 d8                	mov    %ebx,%eax
  800876:	09 d0                	or     %edx,%eax
  800878:	c1 e9 02             	shr    $0x2,%ecx
  80087b:	fc                   	cld    
  80087c:	f3 ab                	rep stos %eax,%es:(%edi)
  80087e:	eb 06                	jmp    800886 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800880:	8b 45 0c             	mov    0xc(%ebp),%eax
  800883:	fc                   	cld    
  800884:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800886:	89 f8                	mov    %edi,%eax
  800888:	5b                   	pop    %ebx
  800889:	5e                   	pop    %esi
  80088a:	5f                   	pop    %edi
  80088b:	5d                   	pop    %ebp
  80088c:	c3                   	ret    

0080088d <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80088d:	55                   	push   %ebp
  80088e:	89 e5                	mov    %esp,%ebp
  800890:	57                   	push   %edi
  800891:	56                   	push   %esi
  800892:	8b 45 08             	mov    0x8(%ebp),%eax
  800895:	8b 75 0c             	mov    0xc(%ebp),%esi
  800898:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80089b:	39 c6                	cmp    %eax,%esi
  80089d:	73 33                	jae    8008d2 <memmove+0x45>
  80089f:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008a2:	39 d0                	cmp    %edx,%eax
  8008a4:	73 2c                	jae    8008d2 <memmove+0x45>
		s += n;
		d += n;
  8008a6:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008a9:	89 d6                	mov    %edx,%esi
  8008ab:	09 fe                	or     %edi,%esi
  8008ad:	f7 c6 03 00 00 00    	test   $0x3,%esi
  8008b3:	75 13                	jne    8008c8 <memmove+0x3b>
  8008b5:	f6 c1 03             	test   $0x3,%cl
  8008b8:	75 0e                	jne    8008c8 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  8008ba:	83 ef 04             	sub    $0x4,%edi
  8008bd:	8d 72 fc             	lea    -0x4(%edx),%esi
  8008c0:	c1 e9 02             	shr    $0x2,%ecx
  8008c3:	fd                   	std    
  8008c4:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008c6:	eb 07                	jmp    8008cf <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008c8:	4f                   	dec    %edi
  8008c9:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008cc:	fd                   	std    
  8008cd:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008cf:	fc                   	cld    
  8008d0:	eb 1d                	jmp    8008ef <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008d2:	89 f2                	mov    %esi,%edx
  8008d4:	09 c2                	or     %eax,%edx
  8008d6:	f6 c2 03             	test   $0x3,%dl
  8008d9:	75 0f                	jne    8008ea <memmove+0x5d>
  8008db:	f6 c1 03             	test   $0x3,%cl
  8008de:	75 0a                	jne    8008ea <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008e0:	c1 e9 02             	shr    $0x2,%ecx
  8008e3:	89 c7                	mov    %eax,%edi
  8008e5:	fc                   	cld    
  8008e6:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008e8:	eb 05                	jmp    8008ef <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008ea:	89 c7                	mov    %eax,%edi
  8008ec:	fc                   	cld    
  8008ed:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008ef:	5e                   	pop    %esi
  8008f0:	5f                   	pop    %edi
  8008f1:	5d                   	pop    %ebp
  8008f2:	c3                   	ret    

008008f3 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008f3:	55                   	push   %ebp
  8008f4:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008f6:	ff 75 10             	pushl  0x10(%ebp)
  8008f9:	ff 75 0c             	pushl  0xc(%ebp)
  8008fc:	ff 75 08             	pushl  0x8(%ebp)
  8008ff:	e8 89 ff ff ff       	call   80088d <memmove>
}
  800904:	c9                   	leave  
  800905:	c3                   	ret    

00800906 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800906:	55                   	push   %ebp
  800907:	89 e5                	mov    %esp,%ebp
  800909:	56                   	push   %esi
  80090a:	53                   	push   %ebx
  80090b:	8b 45 08             	mov    0x8(%ebp),%eax
  80090e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800911:	89 c6                	mov    %eax,%esi
  800913:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800916:	eb 14                	jmp    80092c <memcmp+0x26>
		if (*s1 != *s2)
  800918:	8a 08                	mov    (%eax),%cl
  80091a:	8a 1a                	mov    (%edx),%bl
  80091c:	38 d9                	cmp    %bl,%cl
  80091e:	74 0a                	je     80092a <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800920:	0f b6 c1             	movzbl %cl,%eax
  800923:	0f b6 db             	movzbl %bl,%ebx
  800926:	29 d8                	sub    %ebx,%eax
  800928:	eb 0b                	jmp    800935 <memcmp+0x2f>
		s1++, s2++;
  80092a:	40                   	inc    %eax
  80092b:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80092c:	39 f0                	cmp    %esi,%eax
  80092e:	75 e8                	jne    800918 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800930:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800935:	5b                   	pop    %ebx
  800936:	5e                   	pop    %esi
  800937:	5d                   	pop    %ebp
  800938:	c3                   	ret    

00800939 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800939:	55                   	push   %ebp
  80093a:	89 e5                	mov    %esp,%ebp
  80093c:	53                   	push   %ebx
  80093d:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800940:	89 c1                	mov    %eax,%ecx
  800942:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800945:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800949:	eb 08                	jmp    800953 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80094b:	0f b6 10             	movzbl (%eax),%edx
  80094e:	39 da                	cmp    %ebx,%edx
  800950:	74 05                	je     800957 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800952:	40                   	inc    %eax
  800953:	39 c8                	cmp    %ecx,%eax
  800955:	72 f4                	jb     80094b <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800957:	5b                   	pop    %ebx
  800958:	5d                   	pop    %ebp
  800959:	c3                   	ret    

0080095a <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80095a:	55                   	push   %ebp
  80095b:	89 e5                	mov    %esp,%ebp
  80095d:	57                   	push   %edi
  80095e:	56                   	push   %esi
  80095f:	53                   	push   %ebx
  800960:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800963:	eb 01                	jmp    800966 <strtol+0xc>
		s++;
  800965:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800966:	8a 01                	mov    (%ecx),%al
  800968:	3c 20                	cmp    $0x20,%al
  80096a:	74 f9                	je     800965 <strtol+0xb>
  80096c:	3c 09                	cmp    $0x9,%al
  80096e:	74 f5                	je     800965 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800970:	3c 2b                	cmp    $0x2b,%al
  800972:	75 08                	jne    80097c <strtol+0x22>
		s++;
  800974:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800975:	bf 00 00 00 00       	mov    $0x0,%edi
  80097a:	eb 11                	jmp    80098d <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80097c:	3c 2d                	cmp    $0x2d,%al
  80097e:	75 08                	jne    800988 <strtol+0x2e>
		s++, neg = 1;
  800980:	41                   	inc    %ecx
  800981:	bf 01 00 00 00       	mov    $0x1,%edi
  800986:	eb 05                	jmp    80098d <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800988:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80098d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800991:	0f 84 87 00 00 00    	je     800a1e <strtol+0xc4>
  800997:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80099b:	75 27                	jne    8009c4 <strtol+0x6a>
  80099d:	80 39 30             	cmpb   $0x30,(%ecx)
  8009a0:	75 22                	jne    8009c4 <strtol+0x6a>
  8009a2:	e9 88 00 00 00       	jmp    800a2f <strtol+0xd5>
		s += 2, base = 16;
  8009a7:	83 c1 02             	add    $0x2,%ecx
  8009aa:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  8009b1:	eb 11                	jmp    8009c4 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  8009b3:	41                   	inc    %ecx
  8009b4:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  8009bb:	eb 07                	jmp    8009c4 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  8009bd:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  8009c4:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009c9:	8a 11                	mov    (%ecx),%dl
  8009cb:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009ce:	80 fb 09             	cmp    $0x9,%bl
  8009d1:	77 08                	ja     8009db <strtol+0x81>
			dig = *s - '0';
  8009d3:	0f be d2             	movsbl %dl,%edx
  8009d6:	83 ea 30             	sub    $0x30,%edx
  8009d9:	eb 22                	jmp    8009fd <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009db:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009de:	89 f3                	mov    %esi,%ebx
  8009e0:	80 fb 19             	cmp    $0x19,%bl
  8009e3:	77 08                	ja     8009ed <strtol+0x93>
			dig = *s - 'a' + 10;
  8009e5:	0f be d2             	movsbl %dl,%edx
  8009e8:	83 ea 57             	sub    $0x57,%edx
  8009eb:	eb 10                	jmp    8009fd <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009ed:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009f0:	89 f3                	mov    %esi,%ebx
  8009f2:	80 fb 19             	cmp    $0x19,%bl
  8009f5:	77 14                	ja     800a0b <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009f7:	0f be d2             	movsbl %dl,%edx
  8009fa:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009fd:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a00:	7d 09                	jge    800a0b <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a02:	41                   	inc    %ecx
  800a03:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a07:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a09:	eb be                	jmp    8009c9 <strtol+0x6f>

	if (endptr)
  800a0b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a0f:	74 05                	je     800a16 <strtol+0xbc>
		*endptr = (char *) s;
  800a11:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a14:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a16:	85 ff                	test   %edi,%edi
  800a18:	74 21                	je     800a3b <strtol+0xe1>
  800a1a:	f7 d8                	neg    %eax
  800a1c:	eb 1d                	jmp    800a3b <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a1e:	80 39 30             	cmpb   $0x30,(%ecx)
  800a21:	75 9a                	jne    8009bd <strtol+0x63>
  800a23:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a27:	0f 84 7a ff ff ff    	je     8009a7 <strtol+0x4d>
  800a2d:	eb 84                	jmp    8009b3 <strtol+0x59>
  800a2f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a33:	0f 84 6e ff ff ff    	je     8009a7 <strtol+0x4d>
  800a39:	eb 89                	jmp    8009c4 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a3b:	5b                   	pop    %ebx
  800a3c:	5e                   	pop    %esi
  800a3d:	5f                   	pop    %edi
  800a3e:	5d                   	pop    %ebp
  800a3f:	c3                   	ret    

00800a40 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a40:	55                   	push   %ebp
  800a41:	89 e5                	mov    %esp,%ebp
  800a43:	57                   	push   %edi
  800a44:	56                   	push   %esi
  800a45:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a46:	b8 00 00 00 00       	mov    $0x0,%eax
  800a4b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a4e:	8b 55 08             	mov    0x8(%ebp),%edx
  800a51:	89 c3                	mov    %eax,%ebx
  800a53:	89 c7                	mov    %eax,%edi
  800a55:	89 c6                	mov    %eax,%esi
  800a57:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a59:	5b                   	pop    %ebx
  800a5a:	5e                   	pop    %esi
  800a5b:	5f                   	pop    %edi
  800a5c:	5d                   	pop    %ebp
  800a5d:	c3                   	ret    

00800a5e <sys_cgetc>:

int
sys_cgetc(void)
{
  800a5e:	55                   	push   %ebp
  800a5f:	89 e5                	mov    %esp,%ebp
  800a61:	57                   	push   %edi
  800a62:	56                   	push   %esi
  800a63:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a64:	ba 00 00 00 00       	mov    $0x0,%edx
  800a69:	b8 01 00 00 00       	mov    $0x1,%eax
  800a6e:	89 d1                	mov    %edx,%ecx
  800a70:	89 d3                	mov    %edx,%ebx
  800a72:	89 d7                	mov    %edx,%edi
  800a74:	89 d6                	mov    %edx,%esi
  800a76:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a78:	5b                   	pop    %ebx
  800a79:	5e                   	pop    %esi
  800a7a:	5f                   	pop    %edi
  800a7b:	5d                   	pop    %ebp
  800a7c:	c3                   	ret    

00800a7d <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a7d:	55                   	push   %ebp
  800a7e:	89 e5                	mov    %esp,%ebp
  800a80:	57                   	push   %edi
  800a81:	56                   	push   %esi
  800a82:	53                   	push   %ebx
  800a83:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a86:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a8b:	b8 03 00 00 00       	mov    $0x3,%eax
  800a90:	8b 55 08             	mov    0x8(%ebp),%edx
  800a93:	89 cb                	mov    %ecx,%ebx
  800a95:	89 cf                	mov    %ecx,%edi
  800a97:	89 ce                	mov    %ecx,%esi
  800a99:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a9b:	85 c0                	test   %eax,%eax
  800a9d:	7e 17                	jle    800ab6 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a9f:	83 ec 0c             	sub    $0xc,%esp
  800aa2:	50                   	push   %eax
  800aa3:	6a 03                	push   $0x3
  800aa5:	68 bf 26 80 00       	push   $0x8026bf
  800aaa:	6a 23                	push   $0x23
  800aac:	68 dc 26 80 00       	push   $0x8026dc
  800ab1:	e8 26 f6 ff ff       	call   8000dc <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800ab6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ab9:	5b                   	pop    %ebx
  800aba:	5e                   	pop    %esi
  800abb:	5f                   	pop    %edi
  800abc:	5d                   	pop    %ebp
  800abd:	c3                   	ret    

00800abe <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800abe:	55                   	push   %ebp
  800abf:	89 e5                	mov    %esp,%ebp
  800ac1:	57                   	push   %edi
  800ac2:	56                   	push   %esi
  800ac3:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ac4:	ba 00 00 00 00       	mov    $0x0,%edx
  800ac9:	b8 02 00 00 00       	mov    $0x2,%eax
  800ace:	89 d1                	mov    %edx,%ecx
  800ad0:	89 d3                	mov    %edx,%ebx
  800ad2:	89 d7                	mov    %edx,%edi
  800ad4:	89 d6                	mov    %edx,%esi
  800ad6:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800ad8:	5b                   	pop    %ebx
  800ad9:	5e                   	pop    %esi
  800ada:	5f                   	pop    %edi
  800adb:	5d                   	pop    %ebp
  800adc:	c3                   	ret    

00800add <sys_yield>:

void
sys_yield(void)
{
  800add:	55                   	push   %ebp
  800ade:	89 e5                	mov    %esp,%ebp
  800ae0:	57                   	push   %edi
  800ae1:	56                   	push   %esi
  800ae2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ae3:	ba 00 00 00 00       	mov    $0x0,%edx
  800ae8:	b8 0b 00 00 00       	mov    $0xb,%eax
  800aed:	89 d1                	mov    %edx,%ecx
  800aef:	89 d3                	mov    %edx,%ebx
  800af1:	89 d7                	mov    %edx,%edi
  800af3:	89 d6                	mov    %edx,%esi
  800af5:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800af7:	5b                   	pop    %ebx
  800af8:	5e                   	pop    %esi
  800af9:	5f                   	pop    %edi
  800afa:	5d                   	pop    %ebp
  800afb:	c3                   	ret    

00800afc <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800afc:	55                   	push   %ebp
  800afd:	89 e5                	mov    %esp,%ebp
  800aff:	57                   	push   %edi
  800b00:	56                   	push   %esi
  800b01:	53                   	push   %ebx
  800b02:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b05:	be 00 00 00 00       	mov    $0x0,%esi
  800b0a:	b8 04 00 00 00       	mov    $0x4,%eax
  800b0f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b12:	8b 55 08             	mov    0x8(%ebp),%edx
  800b15:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b18:	89 f7                	mov    %esi,%edi
  800b1a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b1c:	85 c0                	test   %eax,%eax
  800b1e:	7e 17                	jle    800b37 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b20:	83 ec 0c             	sub    $0xc,%esp
  800b23:	50                   	push   %eax
  800b24:	6a 04                	push   $0x4
  800b26:	68 bf 26 80 00       	push   $0x8026bf
  800b2b:	6a 23                	push   $0x23
  800b2d:	68 dc 26 80 00       	push   $0x8026dc
  800b32:	e8 a5 f5 ff ff       	call   8000dc <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800b37:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b3a:	5b                   	pop    %ebx
  800b3b:	5e                   	pop    %esi
  800b3c:	5f                   	pop    %edi
  800b3d:	5d                   	pop    %ebp
  800b3e:	c3                   	ret    

00800b3f <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800b3f:	55                   	push   %ebp
  800b40:	89 e5                	mov    %esp,%ebp
  800b42:	57                   	push   %edi
  800b43:	56                   	push   %esi
  800b44:	53                   	push   %ebx
  800b45:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b48:	b8 05 00 00 00       	mov    $0x5,%eax
  800b4d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b50:	8b 55 08             	mov    0x8(%ebp),%edx
  800b53:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800b56:	8b 7d 14             	mov    0x14(%ebp),%edi
  800b59:	8b 75 18             	mov    0x18(%ebp),%esi
  800b5c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b5e:	85 c0                	test   %eax,%eax
  800b60:	7e 17                	jle    800b79 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b62:	83 ec 0c             	sub    $0xc,%esp
  800b65:	50                   	push   %eax
  800b66:	6a 05                	push   $0x5
  800b68:	68 bf 26 80 00       	push   $0x8026bf
  800b6d:	6a 23                	push   $0x23
  800b6f:	68 dc 26 80 00       	push   $0x8026dc
  800b74:	e8 63 f5 ff ff       	call   8000dc <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  800b79:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b7c:	5b                   	pop    %ebx
  800b7d:	5e                   	pop    %esi
  800b7e:	5f                   	pop    %edi
  800b7f:	5d                   	pop    %ebp
  800b80:	c3                   	ret    

00800b81 <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  800b81:	55                   	push   %ebp
  800b82:	89 e5                	mov    %esp,%ebp
  800b84:	57                   	push   %edi
  800b85:	56                   	push   %esi
  800b86:	53                   	push   %ebx
  800b87:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b8a:	bb 00 00 00 00       	mov    $0x0,%ebx
  800b8f:	b8 06 00 00 00       	mov    $0x6,%eax
  800b94:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800b97:	8b 55 08             	mov    0x8(%ebp),%edx
  800b9a:	89 df                	mov    %ebx,%edi
  800b9c:	89 de                	mov    %ebx,%esi
  800b9e:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ba0:	85 c0                	test   %eax,%eax
  800ba2:	7e 17                	jle    800bbb <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ba4:	83 ec 0c             	sub    $0xc,%esp
  800ba7:	50                   	push   %eax
  800ba8:	6a 06                	push   $0x6
  800baa:	68 bf 26 80 00       	push   $0x8026bf
  800baf:	6a 23                	push   $0x23
  800bb1:	68 dc 26 80 00       	push   $0x8026dc
  800bb6:	e8 21 f5 ff ff       	call   8000dc <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800bbb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800bbe:	5b                   	pop    %ebx
  800bbf:	5e                   	pop    %esi
  800bc0:	5f                   	pop    %edi
  800bc1:	5d                   	pop    %ebp
  800bc2:	c3                   	ret    

00800bc3 <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  800bc3:	55                   	push   %ebp
  800bc4:	89 e5                	mov    %esp,%ebp
  800bc6:	57                   	push   %edi
  800bc7:	56                   	push   %esi
  800bc8:	53                   	push   %ebx
  800bc9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800bcc:	bb 00 00 00 00       	mov    $0x0,%ebx
  800bd1:	b8 08 00 00 00       	mov    $0x8,%eax
  800bd6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800bd9:	8b 55 08             	mov    0x8(%ebp),%edx
  800bdc:	89 df                	mov    %ebx,%edi
  800bde:	89 de                	mov    %ebx,%esi
  800be0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800be2:	85 c0                	test   %eax,%eax
  800be4:	7e 17                	jle    800bfd <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800be6:	83 ec 0c             	sub    $0xc,%esp
  800be9:	50                   	push   %eax
  800bea:	6a 08                	push   $0x8
  800bec:	68 bf 26 80 00       	push   $0x8026bf
  800bf1:	6a 23                	push   $0x23
  800bf3:	68 dc 26 80 00       	push   $0x8026dc
  800bf8:	e8 df f4 ff ff       	call   8000dc <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800bfd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c00:	5b                   	pop    %ebx
  800c01:	5e                   	pop    %esi
  800c02:	5f                   	pop    %edi
  800c03:	5d                   	pop    %ebp
  800c04:	c3                   	ret    

00800c05 <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  800c05:	55                   	push   %ebp
  800c06:	89 e5                	mov    %esp,%ebp
  800c08:	57                   	push   %edi
  800c09:	56                   	push   %esi
  800c0a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c0b:	ba 00 00 00 00       	mov    $0x0,%edx
  800c10:	b8 0c 00 00 00       	mov    $0xc,%eax
  800c15:	89 d1                	mov    %edx,%ecx
  800c17:	89 d3                	mov    %edx,%ebx
  800c19:	89 d7                	mov    %edx,%edi
  800c1b:	89 d6                	mov    %edx,%esi
  800c1d:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800c1f:	5b                   	pop    %ebx
  800c20:	5e                   	pop    %esi
  800c21:	5f                   	pop    %edi
  800c22:	5d                   	pop    %ebp
  800c23:	c3                   	ret    

00800c24 <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  800c24:	55                   	push   %ebp
  800c25:	89 e5                	mov    %esp,%ebp
  800c27:	57                   	push   %edi
  800c28:	56                   	push   %esi
  800c29:	53                   	push   %ebx
  800c2a:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c2d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c32:	b8 09 00 00 00       	mov    $0x9,%eax
  800c37:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c3a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c3d:	89 df                	mov    %ebx,%edi
  800c3f:	89 de                	mov    %ebx,%esi
  800c41:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c43:	85 c0                	test   %eax,%eax
  800c45:	7e 17                	jle    800c5e <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c47:	83 ec 0c             	sub    $0xc,%esp
  800c4a:	50                   	push   %eax
  800c4b:	6a 09                	push   $0x9
  800c4d:	68 bf 26 80 00       	push   $0x8026bf
  800c52:	6a 23                	push   $0x23
  800c54:	68 dc 26 80 00       	push   $0x8026dc
  800c59:	e8 7e f4 ff ff       	call   8000dc <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  800c5e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800c61:	5b                   	pop    %ebx
  800c62:	5e                   	pop    %esi
  800c63:	5f                   	pop    %edi
  800c64:	5d                   	pop    %ebp
  800c65:	c3                   	ret    

00800c66 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  800c66:	55                   	push   %ebp
  800c67:	89 e5                	mov    %esp,%ebp
  800c69:	57                   	push   %edi
  800c6a:	56                   	push   %esi
  800c6b:	53                   	push   %ebx
  800c6c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800c6f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800c74:	b8 0a 00 00 00       	mov    $0xa,%eax
  800c79:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800c7c:	8b 55 08             	mov    0x8(%ebp),%edx
  800c7f:	89 df                	mov    %ebx,%edi
  800c81:	89 de                	mov    %ebx,%esi
  800c83:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800c85:	85 c0                	test   %eax,%eax
  800c87:	7e 17                	jle    800ca0 <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800c89:	83 ec 0c             	sub    $0xc,%esp
  800c8c:	50                   	push   %eax
  800c8d:	6a 0a                	push   $0xa
  800c8f:	68 bf 26 80 00       	push   $0x8026bf
  800c94:	6a 23                	push   $0x23
  800c96:	68 dc 26 80 00       	push   $0x8026dc
  800c9b:	e8 3c f4 ff ff       	call   8000dc <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  800ca0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800ca3:	5b                   	pop    %ebx
  800ca4:	5e                   	pop    %esi
  800ca5:	5f                   	pop    %edi
  800ca6:	5d                   	pop    %ebp
  800ca7:	c3                   	ret    

00800ca8 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800ca8:	55                   	push   %ebp
  800ca9:	89 e5                	mov    %esp,%ebp
  800cab:	57                   	push   %edi
  800cac:	56                   	push   %esi
  800cad:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cae:	be 00 00 00 00       	mov    $0x0,%esi
  800cb3:	b8 0d 00 00 00       	mov    $0xd,%eax
  800cb8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800cbb:	8b 55 08             	mov    0x8(%ebp),%edx
  800cbe:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800cc1:	8b 7d 14             	mov    0x14(%ebp),%edi
  800cc4:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800cc6:	5b                   	pop    %ebx
  800cc7:	5e                   	pop    %esi
  800cc8:	5f                   	pop    %edi
  800cc9:	5d                   	pop    %ebp
  800cca:	c3                   	ret    

00800ccb <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800ccb:	55                   	push   %ebp
  800ccc:	89 e5                	mov    %esp,%ebp
  800cce:	57                   	push   %edi
  800ccf:	56                   	push   %esi
  800cd0:	53                   	push   %ebx
  800cd1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800cd4:	b9 00 00 00 00       	mov    $0x0,%ecx
  800cd9:	b8 0e 00 00 00       	mov    $0xe,%eax
  800cde:	8b 55 08             	mov    0x8(%ebp),%edx
  800ce1:	89 cb                	mov    %ecx,%ebx
  800ce3:	89 cf                	mov    %ecx,%edi
  800ce5:	89 ce                	mov    %ecx,%esi
  800ce7:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800ce9:	85 c0                	test   %eax,%eax
  800ceb:	7e 17                	jle    800d04 <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800ced:	83 ec 0c             	sub    $0xc,%esp
  800cf0:	50                   	push   %eax
  800cf1:	6a 0e                	push   $0xe
  800cf3:	68 bf 26 80 00       	push   $0x8026bf
  800cf8:	6a 23                	push   $0x23
  800cfa:	68 dc 26 80 00       	push   $0x8026dc
  800cff:	e8 d8 f3 ff ff       	call   8000dc <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  800d04:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800d07:	5b                   	pop    %ebx
  800d08:	5e                   	pop    %esi
  800d09:	5f                   	pop    %edi
  800d0a:	5d                   	pop    %ebp
  800d0b:	c3                   	ret    

00800d0c <spawn>:
// argv: pointer to null-terminated array of pointers to strings,
// 	 which will be passed to the child as its command-line arguments.
// Returns child envid on success, < 0 on failure.
int
spawn(const char *prog, const char **argv)
{
  800d0c:	55                   	push   %ebp
  800d0d:	89 e5                	mov    %esp,%ebp
  800d0f:	57                   	push   %edi
  800d10:	56                   	push   %esi
  800d11:	53                   	push   %ebx
  800d12:	81 ec 94 02 00 00    	sub    $0x294,%esp
  800d18:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	//   - Call sys_env_set_trapframe(child, &child_tf) to set up the
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
  800d1b:	6a 00                	push   $0x0
  800d1d:	ff 75 08             	pushl  0x8(%ebp)
  800d20:	e8 11 0d 00 00       	call   801a36 <open>
  800d25:	89 c1                	mov    %eax,%ecx
  800d27:	89 85 8c fd ff ff    	mov    %eax,-0x274(%ebp)
  800d2d:	83 c4 10             	add    $0x10,%esp
  800d30:	85 c0                	test   %eax,%eax
  800d32:	0f 88 e2 04 00 00    	js     80121a <spawn+0x50e>
		return r;
	fd = r;

	// Read elf header
	elf = (struct Elf*) elf_buf;
	if (readn(fd, elf_buf, sizeof(elf_buf)) != sizeof(elf_buf)
  800d38:	83 ec 04             	sub    $0x4,%esp
  800d3b:	68 00 02 00 00       	push   $0x200
  800d40:	8d 85 e8 fd ff ff    	lea    -0x218(%ebp),%eax
  800d46:	50                   	push   %eax
  800d47:	51                   	push   %ecx
  800d48:	e8 09 09 00 00       	call   801656 <readn>
  800d4d:	83 c4 10             	add    $0x10,%esp
  800d50:	3d 00 02 00 00       	cmp    $0x200,%eax
  800d55:	75 0c                	jne    800d63 <spawn+0x57>
	    || elf->e_magic != ELF_MAGIC) {
  800d57:	81 bd e8 fd ff ff 7f 	cmpl   $0x464c457f,-0x218(%ebp)
  800d5e:	45 4c 46 
  800d61:	74 33                	je     800d96 <spawn+0x8a>
		close(fd);
  800d63:	83 ec 0c             	sub    $0xc,%esp
  800d66:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  800d6c:	e8 26 07 00 00       	call   801497 <close>
		cprintf("elf magic %08x want %08x\n", elf->e_magic, ELF_MAGIC);
  800d71:	83 c4 0c             	add    $0xc,%esp
  800d74:	68 7f 45 4c 46       	push   $0x464c457f
  800d79:	ff b5 e8 fd ff ff    	pushl  -0x218(%ebp)
  800d7f:	68 ea 26 80 00       	push   $0x8026ea
  800d84:	e8 2b f4 ff ff       	call   8001b4 <cprintf>
		return -E_NOT_EXEC;
  800d89:	83 c4 10             	add    $0x10,%esp
  800d8c:	bb f2 ff ff ff       	mov    $0xfffffff2,%ebx
  800d91:	e9 e4 04 00 00       	jmp    80127a <spawn+0x56e>
// This must be inlined.  Exercise for reader: why?
static inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	asm volatile("int %2"
  800d96:	b8 07 00 00 00       	mov    $0x7,%eax
  800d9b:	cd 30                	int    $0x30
  800d9d:	89 85 74 fd ff ff    	mov    %eax,-0x28c(%ebp)
  800da3:	89 85 84 fd ff ff    	mov    %eax,-0x27c(%ebp)
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
  800da9:	85 c0                	test   %eax,%eax
  800dab:	0f 88 71 04 00 00    	js     801222 <spawn+0x516>
		return r;
	child = r;

	// Set up trap frame, including initial stack.
	child_tf = envs[ENVX(child)].env_tf;
  800db1:	25 ff 03 00 00       	and    $0x3ff,%eax
  800db6:	89 c6                	mov    %eax,%esi
  800db8:	8d 04 85 00 00 00 00 	lea    0x0(,%eax,4),%eax
  800dbf:	c1 e6 07             	shl    $0x7,%esi
  800dc2:	29 c6                	sub    %eax,%esi
  800dc4:	81 c6 00 00 c0 ee    	add    $0xeec00000,%esi
  800dca:	8d bd a4 fd ff ff    	lea    -0x25c(%ebp),%edi
  800dd0:	b9 11 00 00 00       	mov    $0x11,%ecx
  800dd5:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
	child_tf.tf_eip = elf->e_entry;
  800dd7:	8b 85 00 fe ff ff    	mov    -0x200(%ebp),%eax
  800ddd:	89 85 d4 fd ff ff    	mov    %eax,-0x22c(%ebp)
  800de3:	ba 00 00 00 00       	mov    $0x0,%edx
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  800de8:	b8 00 00 00 00       	mov    $0x0,%eax
	char *string_store;
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
  800ded:	bf 00 00 00 00       	mov    $0x0,%edi
  800df2:	89 5d 0c             	mov    %ebx,0xc(%ebp)
  800df5:	89 c3                	mov    %eax,%ebx
  800df7:	eb 13                	jmp    800e0c <spawn+0x100>
	for (argc = 0; argv[argc] != 0; argc++)
		string_size += strlen(argv[argc]) + 1;
  800df9:	83 ec 0c             	sub    $0xc,%esp
  800dfc:	50                   	push   %eax
  800dfd:	e8 e1 f8 ff ff       	call   8006e3 <strlen>
  800e02:	8d 7c 38 01          	lea    0x1(%eax,%edi,1),%edi
	uintptr_t *argv_store;

	// Count the number of arguments (argc)
	// and the total amount of space needed for strings (string_size).
	string_size = 0;
	for (argc = 0; argv[argc] != 0; argc++)
  800e06:	43                   	inc    %ebx
  800e07:	89 f2                	mov    %esi,%edx
  800e09:	83 c4 10             	add    $0x10,%esp
  800e0c:	89 d9                	mov    %ebx,%ecx
  800e0e:	8d 72 04             	lea    0x4(%edx),%esi
  800e11:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e14:	8b 44 30 fc          	mov    -0x4(%eax,%esi,1),%eax
  800e18:	85 c0                	test   %eax,%eax
  800e1a:	75 dd                	jne    800df9 <spawn+0xed>
  800e1c:	89 9d 90 fd ff ff    	mov    %ebx,-0x270(%ebp)
  800e22:	89 9d 88 fd ff ff    	mov    %ebx,-0x278(%ebp)
  800e28:	89 95 80 fd ff ff    	mov    %edx,-0x280(%ebp)
  800e2e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Determine where to place the strings and the argv array.
	// Set up pointers into the temporary page 'UTEMP'; we'll map a page
	// there later, then remap that page into the child environment
	// at (USTACKTOP - PGSIZE).
	// strings is the topmost thing on the stack.
	string_store = (char*) UTEMP + PGSIZE - string_size;
  800e31:	b8 00 10 40 00       	mov    $0x401000,%eax
  800e36:	29 f8                	sub    %edi,%eax
  800e38:	89 c7                	mov    %eax,%edi
	// argv is below that.  There's one argument pointer per argument, plus
	// a null pointer.
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));
  800e3a:	89 c2                	mov    %eax,%edx
  800e3c:	83 e2 fc             	and    $0xfffffffc,%edx
  800e3f:	8d 04 8d 04 00 00 00 	lea    0x4(,%ecx,4),%eax
  800e46:	29 c2                	sub    %eax,%edx
  800e48:	89 95 94 fd ff ff    	mov    %edx,-0x26c(%ebp)

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
  800e4e:	8d 42 f8             	lea    -0x8(%edx),%eax
  800e51:	3d ff ff 3f 00       	cmp    $0x3fffff,%eax
  800e56:	0f 86 d6 03 00 00    	jbe    801232 <spawn+0x526>
		return -E_NO_MEM;

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  800e5c:	83 ec 04             	sub    $0x4,%esp
  800e5f:	6a 07                	push   $0x7
  800e61:	68 00 00 40 00       	push   $0x400000
  800e66:	6a 00                	push   $0x0
  800e68:	e8 8f fc ff ff       	call   800afc <sys_page_alloc>
  800e6d:	83 c4 10             	add    $0x10,%esp
  800e70:	85 c0                	test   %eax,%eax
  800e72:	0f 88 c1 03 00 00    	js     801239 <spawn+0x52d>
  800e78:	be 00 00 00 00       	mov    $0x0,%esi
  800e7d:	eb 2e                	jmp    800ead <spawn+0x1a1>
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
		argv_store[i] = UTEMP2USTACK(string_store);
  800e7f:	8d 87 00 d0 7f ee    	lea    -0x11803000(%edi),%eax
  800e85:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  800e8b:	89 04 b1             	mov    %eax,(%ecx,%esi,4)
		strcpy(string_store, argv[i]);
  800e8e:	83 ec 08             	sub    $0x8,%esp
  800e91:	ff 34 b3             	pushl  (%ebx,%esi,4)
  800e94:	57                   	push   %edi
  800e95:	e8 7e f8 ff ff       	call   800718 <strcpy>
		string_store += strlen(argv[i]) + 1;
  800e9a:	83 c4 04             	add    $0x4,%esp
  800e9d:	ff 34 b3             	pushl  (%ebx,%esi,4)
  800ea0:	e8 3e f8 ff ff       	call   8006e3 <strlen>
  800ea5:	8d 7c 07 01          	lea    0x1(%edi,%eax,1),%edi
	//	  (Again, argv should use an address valid in the child's
	//	  environment.)
	//
	//	* Set *init_esp to the initial stack pointer for the child,
	//	  (Again, use an address valid in the child's environment.)
	for (i = 0; i < argc; i++) {
  800ea9:	46                   	inc    %esi
  800eaa:	83 c4 10             	add    $0x10,%esp
  800ead:	39 b5 90 fd ff ff    	cmp    %esi,-0x270(%ebp)
  800eb3:	7f ca                	jg     800e7f <spawn+0x173>
		argv_store[i] = UTEMP2USTACK(string_store);
		strcpy(string_store, argv[i]);
		string_store += strlen(argv[i]) + 1;
	}
	argv_store[argc] = 0;
  800eb5:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  800ebb:	8b 8d 80 fd ff ff    	mov    -0x280(%ebp),%ecx
  800ec1:	c7 04 08 00 00 00 00 	movl   $0x0,(%eax,%ecx,1)
	assert(string_store == (char*)UTEMP + PGSIZE);
  800ec8:	81 ff 00 10 40 00    	cmp    $0x401000,%edi
  800ece:	74 19                	je     800ee9 <spawn+0x1dd>
  800ed0:	68 8c 27 80 00       	push   $0x80278c
  800ed5:	68 04 27 80 00       	push   $0x802704
  800eda:	68 f1 00 00 00       	push   $0xf1
  800edf:	68 19 27 80 00       	push   $0x802719
  800ee4:	e8 f3 f1 ff ff       	call   8000dc <_panic>

	argv_store[-1] = UTEMP2USTACK(argv_store);
  800ee9:	8b 8d 94 fd ff ff    	mov    -0x26c(%ebp),%ecx
  800eef:	89 c8                	mov    %ecx,%eax
  800ef1:	2d 00 30 80 11       	sub    $0x11803000,%eax
  800ef6:	89 41 fc             	mov    %eax,-0x4(%ecx)
	argv_store[-2] = argc;
  800ef9:	89 c8                	mov    %ecx,%eax
  800efb:	8b 8d 88 fd ff ff    	mov    -0x278(%ebp),%ecx
  800f01:	89 48 f8             	mov    %ecx,-0x8(%eax)

	*init_esp = UTEMP2USTACK(&argv_store[-2]);
  800f04:	2d 08 30 80 11       	sub    $0x11803008,%eax
  800f09:	89 85 e0 fd ff ff    	mov    %eax,-0x220(%ebp)

	// After completing the stack, map it into the child's address space
	// and unmap it from ours!
	if ((r = sys_page_map(0, UTEMP, child, (void*) (USTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W)) < 0)
  800f0f:	83 ec 0c             	sub    $0xc,%esp
  800f12:	6a 07                	push   $0x7
  800f14:	68 00 d0 bf ee       	push   $0xeebfd000
  800f19:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  800f1f:	68 00 00 40 00       	push   $0x400000
  800f24:	6a 00                	push   $0x0
  800f26:	e8 14 fc ff ff       	call   800b3f <sys_page_map>
  800f2b:	89 c3                	mov    %eax,%ebx
  800f2d:	83 c4 20             	add    $0x20,%esp
  800f30:	85 c0                	test   %eax,%eax
  800f32:	0f 88 30 03 00 00    	js     801268 <spawn+0x55c>
		goto error;
	if ((r = sys_page_unmap(0, UTEMP)) < 0)
  800f38:	83 ec 08             	sub    $0x8,%esp
  800f3b:	68 00 00 40 00       	push   $0x400000
  800f40:	6a 00                	push   $0x0
  800f42:	e8 3a fc ff ff       	call   800b81 <sys_page_unmap>
  800f47:	89 c3                	mov    %eax,%ebx
  800f49:	83 c4 10             	add    $0x10,%esp
  800f4c:	85 c0                	test   %eax,%eax
  800f4e:	0f 88 14 03 00 00    	js     801268 <spawn+0x55c>

	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
  800f54:	8b 85 04 fe ff ff    	mov    -0x1fc(%ebp),%eax
  800f5a:	8d 84 05 e8 fd ff ff 	lea    -0x218(%ebp,%eax,1),%eax
  800f61:	89 85 7c fd ff ff    	mov    %eax,-0x284(%ebp)
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  800f67:	c7 85 78 fd ff ff 00 	movl   $0x0,-0x288(%ebp)
  800f6e:	00 00 00 
  800f71:	e9 88 01 00 00       	jmp    8010fe <spawn+0x3f2>
		if (ph->p_type != ELF_PROG_LOAD)
  800f76:	8b 85 7c fd ff ff    	mov    -0x284(%ebp),%eax
  800f7c:	83 38 01             	cmpl   $0x1,(%eax)
  800f7f:	0f 85 6c 01 00 00    	jne    8010f1 <spawn+0x3e5>
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
  800f85:	89 c1                	mov    %eax,%ecx
  800f87:	8b 40 18             	mov    0x18(%eax),%eax
  800f8a:	83 e0 02             	and    $0x2,%eax
			perm |= PTE_W;
  800f8d:	83 f8 01             	cmp    $0x1,%eax
  800f90:	19 c0                	sbb    %eax,%eax
  800f92:	83 e0 fe             	and    $0xfffffffe,%eax
  800f95:	83 c0 07             	add    $0x7,%eax
  800f98:	89 85 88 fd ff ff    	mov    %eax,-0x278(%ebp)
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
  800f9e:	89 c8                	mov    %ecx,%eax
  800fa0:	8b 49 04             	mov    0x4(%ecx),%ecx
  800fa3:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
  800fa9:	8b 78 10             	mov    0x10(%eax),%edi
  800fac:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  800fb2:	8b 70 14             	mov    0x14(%eax),%esi
  800fb5:	89 f2                	mov    %esi,%edx
  800fb7:	89 b5 90 fd ff ff    	mov    %esi,-0x270(%ebp)
  800fbd:	8b 70 08             	mov    0x8(%eax),%esi
	int i, r;
	void *blk;

	//cprintf("map_segment %x+%x\n", va, memsz);

	if ((i = PGOFF(va))) {
  800fc0:	89 f0                	mov    %esi,%eax
  800fc2:	25 ff 0f 00 00       	and    $0xfff,%eax
  800fc7:	74 1a                	je     800fe3 <spawn+0x2d7>
		va -= i;
  800fc9:	29 c6                	sub    %eax,%esi
		memsz += i;
  800fcb:	01 c2                	add    %eax,%edx
  800fcd:	89 95 90 fd ff ff    	mov    %edx,-0x270(%ebp)
		filesz += i;
  800fd3:	01 c7                	add    %eax,%edi
  800fd5:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
		fileoffset -= i;
  800fdb:	29 c1                	sub    %eax,%ecx
  800fdd:	89 8d 80 fd ff ff    	mov    %ecx,-0x280(%ebp)
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  800fe3:	bb 00 00 00 00       	mov    $0x0,%ebx
  800fe8:	e9 f6 00 00 00       	jmp    8010e3 <spawn+0x3d7>
		if (i >= filesz) {
  800fed:	39 9d 94 fd ff ff    	cmp    %ebx,-0x26c(%ebp)
  800ff3:	77 27                	ja     80101c <spawn+0x310>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
  800ff5:	83 ec 04             	sub    $0x4,%esp
  800ff8:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  800ffe:	56                   	push   %esi
  800fff:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  801005:	e8 f2 fa ff ff       	call   800afc <sys_page_alloc>
  80100a:	83 c4 10             	add    $0x10,%esp
  80100d:	85 c0                	test   %eax,%eax
  80100f:	0f 89 c2 00 00 00    	jns    8010d7 <spawn+0x3cb>
  801015:	89 c3                	mov    %eax,%ebx
  801017:	e9 2b 02 00 00       	jmp    801247 <spawn+0x53b>
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  80101c:	83 ec 04             	sub    $0x4,%esp
  80101f:	6a 07                	push   $0x7
  801021:	68 00 00 40 00       	push   $0x400000
  801026:	6a 00                	push   $0x0
  801028:	e8 cf fa ff ff       	call   800afc <sys_page_alloc>
  80102d:	83 c4 10             	add    $0x10,%esp
  801030:	85 c0                	test   %eax,%eax
  801032:	0f 88 05 02 00 00    	js     80123d <spawn+0x531>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801038:	83 ec 08             	sub    $0x8,%esp
  80103b:	8b 85 80 fd ff ff    	mov    -0x280(%ebp),%eax
  801041:	01 f8                	add    %edi,%eax
  801043:	50                   	push   %eax
  801044:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  80104a:	e8 d2 06 00 00       	call   801721 <seek>
  80104f:	83 c4 10             	add    $0x10,%esp
  801052:	85 c0                	test   %eax,%eax
  801054:	0f 88 e7 01 00 00    	js     801241 <spawn+0x535>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  80105a:	83 ec 04             	sub    $0x4,%esp
  80105d:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  801063:	29 f8                	sub    %edi,%eax
  801065:	3d 00 10 00 00       	cmp    $0x1000,%eax
  80106a:	76 05                	jbe    801071 <spawn+0x365>
  80106c:	b8 00 10 00 00       	mov    $0x1000,%eax
  801071:	50                   	push   %eax
  801072:	68 00 00 40 00       	push   $0x400000
  801077:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  80107d:	e8 d4 05 00 00       	call   801656 <readn>
  801082:	83 c4 10             	add    $0x10,%esp
  801085:	85 c0                	test   %eax,%eax
  801087:	0f 88 b8 01 00 00    	js     801245 <spawn+0x539>
				return r;
			if ((r = sys_page_map(0, UTEMP, child, (void*) (va + i), perm)) < 0)
  80108d:	83 ec 0c             	sub    $0xc,%esp
  801090:	ff b5 88 fd ff ff    	pushl  -0x278(%ebp)
  801096:	56                   	push   %esi
  801097:	ff b5 84 fd ff ff    	pushl  -0x27c(%ebp)
  80109d:	68 00 00 40 00       	push   $0x400000
  8010a2:	6a 00                	push   $0x0
  8010a4:	e8 96 fa ff ff       	call   800b3f <sys_page_map>
  8010a9:	83 c4 20             	add    $0x20,%esp
  8010ac:	85 c0                	test   %eax,%eax
  8010ae:	79 15                	jns    8010c5 <spawn+0x3b9>
				panic("spawn: sys_page_map data: %e", r);
  8010b0:	50                   	push   %eax
  8010b1:	68 25 27 80 00       	push   $0x802725
  8010b6:	68 24 01 00 00       	push   $0x124
  8010bb:	68 19 27 80 00       	push   $0x802719
  8010c0:	e8 17 f0 ff ff       	call   8000dc <_panic>
			sys_page_unmap(0, UTEMP);
  8010c5:	83 ec 08             	sub    $0x8,%esp
  8010c8:	68 00 00 40 00       	push   $0x400000
  8010cd:	6a 00                	push   $0x0
  8010cf:	e8 ad fa ff ff       	call   800b81 <sys_page_unmap>
  8010d4:	83 c4 10             	add    $0x10,%esp
		memsz += i;
		filesz += i;
		fileoffset -= i;
	}

	for (i = 0; i < memsz; i += PGSIZE) {
  8010d7:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  8010dd:	81 c6 00 10 00 00    	add    $0x1000,%esi
  8010e3:	89 df                	mov    %ebx,%edi
  8010e5:	39 9d 90 fd ff ff    	cmp    %ebx,-0x270(%ebp)
  8010eb:	0f 87 fc fe ff ff    	ja     800fed <spawn+0x2e1>
	if ((r = init_stack(child, argv, &child_tf.tf_esp)) < 0)
		return r;

	// Set up program segments as defined in ELF header.
	ph = (struct Proghdr*) (elf_buf + elf->e_phoff);
	for (i = 0; i < elf->e_phnum; i++, ph++) {
  8010f1:	ff 85 78 fd ff ff    	incl   -0x288(%ebp)
  8010f7:	83 85 7c fd ff ff 20 	addl   $0x20,-0x284(%ebp)
  8010fe:	0f b7 85 14 fe ff ff 	movzwl -0x1ec(%ebp),%eax
  801105:	39 85 78 fd ff ff    	cmp    %eax,-0x288(%ebp)
  80110b:	0f 8c 65 fe ff ff    	jl     800f76 <spawn+0x26a>
			perm |= PTE_W;
		if ((r = map_segment(child, ph->p_va, ph->p_memsz,
				     fd, ph->p_filesz, ph->p_offset, perm)) < 0)
			goto error;
	}
	close(fd);
  801111:	83 ec 0c             	sub    $0xc,%esp
  801114:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  80111a:	e8 78 03 00 00       	call   801497 <close>
  80111f:	83 c4 10             	add    $0x10,%esp
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  801122:	bf 00 00 00 00       	mov    $0x0,%edi
  801127:	89 bd 94 fd ff ff    	mov    %edi,-0x26c(%ebp)
  80112d:	8b bd 84 fd ff ff    	mov    -0x27c(%ebp),%edi
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
  801133:	8b b5 94 fd ff ff    	mov    -0x26c(%ebp),%esi
  801139:	8b 04 b5 00 d0 7b ef 	mov    -0x10843000(,%esi,4),%eax
  801140:	a8 01                	test   $0x1,%al
  801142:	74 4b                	je     80118f <spawn+0x483>
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
        	/* LAB 4: Your code here.
		    pte_t pte = uvpt[pn];
		    void *va = (void *)(pn << PGSHIFT);
		    int perm = pte & PTE_SYSCALL;*/
		    unsigned pn = (ipd << 10) | ipt;
  801144:	c1 e6 0a             	shl    $0xa,%esi
  801147:	bb 00 00 00 00       	mov    $0x0,%ebx
  80114c:	89 f0                	mov    %esi,%eax
  80114e:	09 d8                	or     %ebx,%eax
        	if(!(uvpt[pn] & PTE_P)){ //doesn't exist
  801150:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801157:	f6 c2 01             	test   $0x1,%dl
  80115a:	74 2a                	je     801186 <spawn+0x47a>
        		continue;
        	}
	        int perm = uvpt[pn] & PTE_SYSCALL;
  80115c:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
	        if(perm & PTE_SHARE){
  801163:	f6 c6 04             	test   $0x4,%dh
  801166:	74 1e                	je     801186 <spawn+0x47a>
			    void *va = (void *)(pn << PGSHIFT);
  801168:	c1 e0 0c             	shl    $0xc,%eax
			    r = sys_page_map(0, va, child, va, perm);
  80116b:	83 ec 0c             	sub    $0xc,%esp
  80116e:	81 e2 07 0e 00 00    	and    $0xe07,%edx
  801174:	52                   	push   %edx
  801175:	50                   	push   %eax
  801176:	57                   	push   %edi
  801177:	50                   	push   %eax
  801178:	6a 00                	push   $0x0
  80117a:	e8 c0 f9 ff ff       	call   800b3f <sys_page_map>
			    if (r){
  80117f:	83 c4 20             	add    $0x20,%esp
  801182:	85 c0                	test   %eax,%eax
  801184:	75 1e                	jne    8011a4 <spawn+0x498>
        // No page table yet.
        if (!(uvpd[ipd] & PTE_P))
            continue;
        //if the PT does exist
        int ipt;
        for (ipt = 0; ipt != NPTENTRIES; ++ipt) {
  801186:	43                   	inc    %ebx
  801187:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
  80118d:	75 bd                	jne    80114c <spawn+0x440>
{
	// LAB 5: Your code here.
	    // Step 3: duplicate pages.
    int ipd;
    int r;
    for (ipd = 0; ipd != PDX(UTOP); ++ipd) {
  80118f:	ff 85 94 fd ff ff    	incl   -0x26c(%ebp)
  801195:	8b 85 94 fd ff ff    	mov    -0x26c(%ebp),%eax
  80119b:	3d bb 03 00 00       	cmp    $0x3bb,%eax
  8011a0:	75 91                	jne    801133 <spawn+0x427>
  8011a2:	eb 19                	jmp    8011bd <spawn+0x4b1>
	}
	close(fd);
	fd = -1;

	// Copy shared library state.
	if ((r = copy_shared_pages(child)) < 0)
  8011a4:	85 c0                	test   %eax,%eax
  8011a6:	79 15                	jns    8011bd <spawn+0x4b1>
		panic("copy_shared_pages: %e", r);
  8011a8:	50                   	push   %eax
  8011a9:	68 42 27 80 00       	push   $0x802742
  8011ae:	68 82 00 00 00       	push   $0x82
  8011b3:	68 19 27 80 00       	push   $0x802719
  8011b8:	e8 1f ef ff ff       	call   8000dc <_panic>

	if ((r = sys_env_set_trapframe(child, &child_tf)) < 0)
  8011bd:	83 ec 08             	sub    $0x8,%esp
  8011c0:	8d 85 a4 fd ff ff    	lea    -0x25c(%ebp),%eax
  8011c6:	50                   	push   %eax
  8011c7:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8011cd:	e8 52 fa ff ff       	call   800c24 <sys_env_set_trapframe>
  8011d2:	83 c4 10             	add    $0x10,%esp
  8011d5:	85 c0                	test   %eax,%eax
  8011d7:	79 15                	jns    8011ee <spawn+0x4e2>
		panic("sys_env_set_trapframe: %e", r);
  8011d9:	50                   	push   %eax
  8011da:	68 58 27 80 00       	push   $0x802758
  8011df:	68 85 00 00 00       	push   $0x85
  8011e4:	68 19 27 80 00       	push   $0x802719
  8011e9:	e8 ee ee ff ff       	call   8000dc <_panic>

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
  8011ee:	83 ec 08             	sub    $0x8,%esp
  8011f1:	6a 02                	push   $0x2
  8011f3:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  8011f9:	e8 c5 f9 ff ff       	call   800bc3 <sys_env_set_status>
  8011fe:	83 c4 10             	add    $0x10,%esp
  801201:	85 c0                	test   %eax,%eax
  801203:	79 25                	jns    80122a <spawn+0x51e>
		panic("sys_env_set_status: %e", r);
  801205:	50                   	push   %eax
  801206:	68 72 27 80 00       	push   $0x802772
  80120b:	68 88 00 00 00       	push   $0x88
  801210:	68 19 27 80 00       	push   $0x802719
  801215:	e8 c2 ee ff ff       	call   8000dc <_panic>
	//     correct initial eip and esp values in the child.
	//
	//   - Start the child process running with sys_env_set_status().

	if ((r = open(prog, O_RDONLY)) < 0)
		return r;
  80121a:	8b 9d 8c fd ff ff    	mov    -0x274(%ebp),%ebx
  801220:	eb 58                	jmp    80127a <spawn+0x56e>
		return -E_NOT_EXEC;
	}

	// Create new child environment
	if ((r = sys_exofork()) < 0)
		return r;
  801222:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801228:	eb 50                	jmp    80127a <spawn+0x56e>
		panic("sys_env_set_trapframe: %e", r);

	if ((r = sys_env_set_status(child, ENV_RUNNABLE)) < 0)
		panic("sys_env_set_status: %e", r);

	return child;
  80122a:	8b 9d 74 fd ff ff    	mov    -0x28c(%ebp),%ebx
  801230:	eb 48                	jmp    80127a <spawn+0x56e>
	argv_store = (uintptr_t*) (ROUNDDOWN(string_store, 4) - 4 * (argc + 1));

	// Make sure that argv, strings, and the 2 words that hold 'argc'
	// and 'argv' themselves will all fit in a single stack page.
	if ((void*) (argv_store - 2) < (void*) UTEMP)
		return -E_NO_MEM;
  801232:	bb fc ff ff ff       	mov    $0xfffffffc,%ebx
  801237:	eb 41                	jmp    80127a <spawn+0x56e>

	// Allocate the single stack page at UTEMP.
	if ((r = sys_page_alloc(0, (void*) UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
		return r;
  801239:	89 c3                	mov    %eax,%ebx
  80123b:	eb 3d                	jmp    80127a <spawn+0x56e>
			// allocate a blank page
			if ((r = sys_page_alloc(child, (void*) (va + i), perm)) < 0)
				return r;
		} else {
			// from file
			if ((r = sys_page_alloc(0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
  80123d:	89 c3                	mov    %eax,%ebx
  80123f:	eb 06                	jmp    801247 <spawn+0x53b>
				return r;
			if ((r = seek(fd, fileoffset + i)) < 0)
  801241:	89 c3                	mov    %eax,%ebx
  801243:	eb 02                	jmp    801247 <spawn+0x53b>
				return r;
			if ((r = readn(fd, UTEMP, MIN(PGSIZE, filesz-i))) < 0)
  801245:	89 c3                	mov    %eax,%ebx
		panic("sys_env_set_status: %e", r);

	return child;

error:
	sys_env_destroy(child);
  801247:	83 ec 0c             	sub    $0xc,%esp
  80124a:	ff b5 74 fd ff ff    	pushl  -0x28c(%ebp)
  801250:	e8 28 f8 ff ff       	call   800a7d <sys_env_destroy>
	close(fd);
  801255:	83 c4 04             	add    $0x4,%esp
  801258:	ff b5 8c fd ff ff    	pushl  -0x274(%ebp)
  80125e:	e8 34 02 00 00       	call   801497 <close>
	return r;
  801263:	83 c4 10             	add    $0x10,%esp
  801266:	eb 12                	jmp    80127a <spawn+0x56e>
		goto error;

	return 0;

error:
	sys_page_unmap(0, UTEMP);
  801268:	83 ec 08             	sub    $0x8,%esp
  80126b:	68 00 00 40 00       	push   $0x400000
  801270:	6a 00                	push   $0x0
  801272:	e8 0a f9 ff ff       	call   800b81 <sys_page_unmap>
  801277:	83 c4 10             	add    $0x10,%esp

error:
	sys_env_destroy(child);
	close(fd);
	return r;
}
  80127a:	89 d8                	mov    %ebx,%eax
  80127c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80127f:	5b                   	pop    %ebx
  801280:	5e                   	pop    %esi
  801281:	5f                   	pop    %edi
  801282:	5d                   	pop    %ebp
  801283:	c3                   	ret    

00801284 <spawnl>:
// Spawn, taking command-line arguments array directly on the stack.
// NOTE: Must have a sentinal of NULL at the end of the args
// (none of the args may be NULL).
int
spawnl(const char *prog, const char *arg0, ...)
{
  801284:	55                   	push   %ebp
  801285:	89 e5                	mov    %esp,%ebp
  801287:	56                   	push   %esi
  801288:	53                   	push   %ebx
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801289:	8d 55 10             	lea    0x10(%ebp),%edx
{
	// We calculate argc by advancing the args until we hit NULL.
	// The contract of the function guarantees that the last
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
  80128c:	b8 00 00 00 00       	mov    $0x0,%eax
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801291:	eb 01                	jmp    801294 <spawnl+0x10>
		argc++;
  801293:	40                   	inc    %eax
	// argument will always be NULL, and that none of the other
	// arguments will be NULL.
	int argc=0;
	va_list vl;
	va_start(vl, arg0);
	while(va_arg(vl, void *) != NULL)
  801294:	83 c2 04             	add    $0x4,%edx
  801297:	83 7a fc 00          	cmpl   $0x0,-0x4(%edx)
  80129b:	75 f6                	jne    801293 <spawnl+0xf>
		argc++;
	va_end(vl);

	// Now that we have the size of the args, do a second pass
	// and store the values in a VLA, which has the format of argv
	const char *argv[argc+2];
  80129d:	8d 14 85 1a 00 00 00 	lea    0x1a(,%eax,4),%edx
  8012a4:	83 e2 f0             	and    $0xfffffff0,%edx
  8012a7:	29 d4                	sub    %edx,%esp
  8012a9:	8d 54 24 03          	lea    0x3(%esp),%edx
  8012ad:	c1 ea 02             	shr    $0x2,%edx
  8012b0:	8d 34 95 00 00 00 00 	lea    0x0(,%edx,4),%esi
  8012b7:	89 f3                	mov    %esi,%ebx
	argv[0] = arg0;
  8012b9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8012bc:	89 0c 95 00 00 00 00 	mov    %ecx,0x0(,%edx,4)
	argv[argc+1] = NULL;
  8012c3:	c7 44 86 04 00 00 00 	movl   $0x0,0x4(%esi,%eax,4)
  8012ca:	00 
  8012cb:	89 c2                	mov    %eax,%edx

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  8012cd:	b8 00 00 00 00       	mov    $0x0,%eax
  8012d2:	eb 08                	jmp    8012dc <spawnl+0x58>
		argv[i+1] = va_arg(vl, const char *);
  8012d4:	40                   	inc    %eax
  8012d5:	8b 4c 85 0c          	mov    0xc(%ebp,%eax,4),%ecx
  8012d9:	89 0c 83             	mov    %ecx,(%ebx,%eax,4)
	argv[0] = arg0;
	argv[argc+1] = NULL;

	va_start(vl, arg0);
	unsigned i;
	for(i=0;i<argc;i++)
  8012dc:	39 d0                	cmp    %edx,%eax
  8012de:	75 f4                	jne    8012d4 <spawnl+0x50>
		argv[i+1] = va_arg(vl, const char *);
	va_end(vl);
	return spawn(prog, argv);
  8012e0:	83 ec 08             	sub    $0x8,%esp
  8012e3:	56                   	push   %esi
  8012e4:	ff 75 08             	pushl  0x8(%ebp)
  8012e7:	e8 20 fa ff ff       	call   800d0c <spawn>
}
  8012ec:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8012ef:	5b                   	pop    %ebx
  8012f0:	5e                   	pop    %esi
  8012f1:	5d                   	pop    %ebp
  8012f2:	c3                   	ret    

008012f3 <fd2num>:
// File descriptor manipulators
// --------------------------------------------------------------

int
fd2num(struct Fd *fd)
{
  8012f3:	55                   	push   %ebp
  8012f4:	89 e5                	mov    %esp,%ebp
	return ((uintptr_t) fd - FDTABLE) / PGSIZE;
  8012f6:	8b 45 08             	mov    0x8(%ebp),%eax
  8012f9:	05 00 00 00 30       	add    $0x30000000,%eax
  8012fe:	c1 e8 0c             	shr    $0xc,%eax
}
  801301:	5d                   	pop    %ebp
  801302:	c3                   	ret    

00801303 <fd2data>:

char*
fd2data(struct Fd *fd)
{
  801303:	55                   	push   %ebp
  801304:	89 e5                	mov    %esp,%ebp
	return INDEX2DATA(fd2num(fd));
  801306:	8b 45 08             	mov    0x8(%ebp),%eax
  801309:	05 00 00 00 30       	add    $0x30000000,%eax
  80130e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  801313:	2d 00 00 fe 2f       	sub    $0x2ffe0000,%eax
}
  801318:	5d                   	pop    %ebp
  801319:	c3                   	ret    

0080131a <fd_alloc>:
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_MAX_FD: no more file descriptors
// On error, *fd_store is set to 0.
int
fd_alloc(struct Fd **fd_store)
{
  80131a:	55                   	push   %ebp
  80131b:	89 e5                	mov    %esp,%ebp
  80131d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801320:	b8 00 00 00 d0       	mov    $0xd0000000,%eax
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
		fd = INDEX2FD(i);
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
  801325:	89 c2                	mov    %eax,%edx
  801327:	c1 ea 16             	shr    $0x16,%edx
  80132a:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801331:	f6 c2 01             	test   $0x1,%dl
  801334:	74 11                	je     801347 <fd_alloc+0x2d>
  801336:	89 c2                	mov    %eax,%edx
  801338:	c1 ea 0c             	shr    $0xc,%edx
  80133b:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  801342:	f6 c2 01             	test   $0x1,%dl
  801345:	75 09                	jne    801350 <fd_alloc+0x36>
			*fd_store = fd;
  801347:	89 01                	mov    %eax,(%ecx)
			return 0;
  801349:	b8 00 00 00 00       	mov    $0x0,%eax
  80134e:	eb 17                	jmp    801367 <fd_alloc+0x4d>
  801350:	05 00 10 00 00       	add    $0x1000,%eax
fd_alloc(struct Fd **fd_store)
{
	int i;
	struct Fd *fd;

	for (i = 0; i < MAXFD; i++) {
  801355:	3d 00 00 02 d0       	cmp    $0xd0020000,%eax
  80135a:	75 c9                	jne    801325 <fd_alloc+0xb>
		if ((uvpd[PDX(fd)] & PTE_P) == 0 || (uvpt[PGNUM(fd)] & PTE_P) == 0) {
			*fd_store = fd;
			return 0;
		}
	}
	*fd_store = 0;
  80135c:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	return -E_MAX_OPEN;
  801362:	b8 f6 ff ff ff       	mov    $0xfffffff6,%eax
}
  801367:	5d                   	pop    %ebp
  801368:	c3                   	ret    

00801369 <fd_lookup>:
// Returns 0 on success (the page is in range and mapped), < 0 on error.
// Errors are:
//	-E_INVAL: fdnum was either not in range or not mapped.
int
fd_lookup(int fdnum, struct Fd **fd_store)
{
  801369:	55                   	push   %ebp
  80136a:	89 e5                	mov    %esp,%ebp
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
  80136c:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  801370:	77 39                	ja     8013ab <fd_lookup+0x42>
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	fd = INDEX2FD(fdnum);
  801372:	8b 45 08             	mov    0x8(%ebp),%eax
  801375:	c1 e0 0c             	shl    $0xc,%eax
  801378:	2d 00 00 00 30       	sub    $0x30000000,%eax
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
  80137d:	89 c2                	mov    %eax,%edx
  80137f:	c1 ea 16             	shr    $0x16,%edx
  801382:	8b 14 95 00 d0 7b ef 	mov    -0x10843000(,%edx,4),%edx
  801389:	f6 c2 01             	test   $0x1,%dl
  80138c:	74 24                	je     8013b2 <fd_lookup+0x49>
  80138e:	89 c2                	mov    %eax,%edx
  801390:	c1 ea 0c             	shr    $0xc,%edx
  801393:	8b 14 95 00 00 40 ef 	mov    -0x10c00000(,%edx,4),%edx
  80139a:	f6 c2 01             	test   $0x1,%dl
  80139d:	74 1a                	je     8013b9 <fd_lookup+0x50>
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	*fd_store = fd;
  80139f:	8b 55 0c             	mov    0xc(%ebp),%edx
  8013a2:	89 02                	mov    %eax,(%edx)
	return 0;
  8013a4:	b8 00 00 00 00       	mov    $0x0,%eax
  8013a9:	eb 13                	jmp    8013be <fd_lookup+0x55>
	struct Fd *fd;

	if (fdnum < 0 || fdnum >= MAXFD) {
		if (debug)
			cprintf("[%08x] bad fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8013ab:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8013b0:	eb 0c                	jmp    8013be <fd_lookup+0x55>
	}
	fd = INDEX2FD(fdnum);
	if (!(uvpd[PDX(fd)] & PTE_P) || !(uvpt[PGNUM(fd)] & PTE_P)) {
		if (debug)
			cprintf("[%08x] closed fd %d\n", thisenv->env_id, fdnum);
		return -E_INVAL;
  8013b2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8013b7:	eb 05                	jmp    8013be <fd_lookup+0x55>
  8013b9:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
	*fd_store = fd;
	return 0;
}
  8013be:	5d                   	pop    %ebp
  8013bf:	c3                   	ret    

008013c0 <dev_lookup>:
	0
};

int
dev_lookup(int dev_id, struct Dev **dev)
{
  8013c0:	55                   	push   %ebp
  8013c1:	89 e5                	mov    %esp,%ebp
  8013c3:	83 ec 08             	sub    $0x8,%esp
  8013c6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8013c9:	ba 34 28 80 00       	mov    $0x802834,%edx
	int i;
	for (i = 0; devtab[i]; i++)
  8013ce:	eb 13                	jmp    8013e3 <dev_lookup+0x23>
  8013d0:	83 c2 04             	add    $0x4,%edx
		if (devtab[i]->dev_id == dev_id) {
  8013d3:	39 08                	cmp    %ecx,(%eax)
  8013d5:	75 0c                	jne    8013e3 <dev_lookup+0x23>
			*dev = devtab[i];
  8013d7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8013da:	89 01                	mov    %eax,(%ecx)
			return 0;
  8013dc:	b8 00 00 00 00       	mov    $0x0,%eax
  8013e1:	eb 2e                	jmp    801411 <dev_lookup+0x51>

int
dev_lookup(int dev_id, struct Dev **dev)
{
	int i;
	for (i = 0; devtab[i]; i++)
  8013e3:	8b 02                	mov    (%edx),%eax
  8013e5:	85 c0                	test   %eax,%eax
  8013e7:	75 e7                	jne    8013d0 <dev_lookup+0x10>
		if (devtab[i]->dev_id == dev_id) {
			*dev = devtab[i];
			return 0;
		}
	cprintf("[%08x] unknown device type %d\n", thisenv->env_id, dev_id);
  8013e9:	a1 04 40 80 00       	mov    0x804004,%eax
  8013ee:	8b 40 48             	mov    0x48(%eax),%eax
  8013f1:	83 ec 04             	sub    $0x4,%esp
  8013f4:	51                   	push   %ecx
  8013f5:	50                   	push   %eax
  8013f6:	68 b4 27 80 00       	push   $0x8027b4
  8013fb:	e8 b4 ed ff ff       	call   8001b4 <cprintf>
	*dev = 0;
  801400:	8b 45 0c             	mov    0xc(%ebp),%eax
  801403:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return -E_INVAL;
  801409:	83 c4 10             	add    $0x10,%esp
  80140c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
}
  801411:	c9                   	leave  
  801412:	c3                   	ret    

00801413 <fd_close>:
// If 'must_exist' is 1, then fd_close returns -E_INVAL when passed a
// closed or nonexistent file descriptor.
// Returns 0 on success, < 0 on error.
int
fd_close(struct Fd *fd, bool must_exist)
{
  801413:	55                   	push   %ebp
  801414:	89 e5                	mov    %esp,%ebp
  801416:	56                   	push   %esi
  801417:	53                   	push   %ebx
  801418:	83 ec 10             	sub    $0x10,%esp
  80141b:	8b 75 08             	mov    0x8(%ebp),%esi
  80141e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
  801421:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801424:	50                   	push   %eax
  801425:	8d 86 00 00 00 30    	lea    0x30000000(%esi),%eax
  80142b:	c1 e8 0c             	shr    $0xc,%eax
  80142e:	50                   	push   %eax
  80142f:	e8 35 ff ff ff       	call   801369 <fd_lookup>
  801434:	83 c4 08             	add    $0x8,%esp
  801437:	85 c0                	test   %eax,%eax
  801439:	78 05                	js     801440 <fd_close+0x2d>
	    || fd != fd2)
  80143b:	3b 75 f4             	cmp    -0xc(%ebp),%esi
  80143e:	74 06                	je     801446 <fd_close+0x33>
		return (must_exist ? r : 0);
  801440:	84 db                	test   %bl,%bl
  801442:	74 47                	je     80148b <fd_close+0x78>
  801444:	eb 4a                	jmp    801490 <fd_close+0x7d>
	if ((r = dev_lookup(fd->fd_dev_id, &dev)) >= 0) {
  801446:	83 ec 08             	sub    $0x8,%esp
  801449:	8d 45 f0             	lea    -0x10(%ebp),%eax
  80144c:	50                   	push   %eax
  80144d:	ff 36                	pushl  (%esi)
  80144f:	e8 6c ff ff ff       	call   8013c0 <dev_lookup>
  801454:	89 c3                	mov    %eax,%ebx
  801456:	83 c4 10             	add    $0x10,%esp
  801459:	85 c0                	test   %eax,%eax
  80145b:	78 1c                	js     801479 <fd_close+0x66>
		if (dev->dev_close)
  80145d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801460:	8b 40 10             	mov    0x10(%eax),%eax
  801463:	85 c0                	test   %eax,%eax
  801465:	74 0d                	je     801474 <fd_close+0x61>
			r = (*dev->dev_close)(fd);
  801467:	83 ec 0c             	sub    $0xc,%esp
  80146a:	56                   	push   %esi
  80146b:	ff d0                	call   *%eax
  80146d:	89 c3                	mov    %eax,%ebx
  80146f:	83 c4 10             	add    $0x10,%esp
  801472:	eb 05                	jmp    801479 <fd_close+0x66>
		else
			r = 0;
  801474:	bb 00 00 00 00       	mov    $0x0,%ebx
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
  801479:	83 ec 08             	sub    $0x8,%esp
  80147c:	56                   	push   %esi
  80147d:	6a 00                	push   $0x0
  80147f:	e8 fd f6 ff ff       	call   800b81 <sys_page_unmap>
	return r;
  801484:	83 c4 10             	add    $0x10,%esp
  801487:	89 d8                	mov    %ebx,%eax
  801489:	eb 05                	jmp    801490 <fd_close+0x7d>
	struct Fd *fd2;
	struct Dev *dev;
	int r;
	if ((r = fd_lookup(fd2num(fd), &fd2)) < 0
	    || fd != fd2)
		return (must_exist ? r : 0);
  80148b:	b8 00 00 00 00       	mov    $0x0,%eax
	}
	// Make sure fd is unmapped.  Might be a no-op if
	// (*dev->dev_close)(fd) already unmapped it.
	(void) sys_page_unmap(0, fd);
	return r;
}
  801490:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801493:	5b                   	pop    %ebx
  801494:	5e                   	pop    %esi
  801495:	5d                   	pop    %ebp
  801496:	c3                   	ret    

00801497 <close>:
	return -E_INVAL;
}

int
close(int fdnum)
{
  801497:	55                   	push   %ebp
  801498:	89 e5                	mov    %esp,%ebp
  80149a:	83 ec 18             	sub    $0x18,%esp
	struct Fd *fd;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  80149d:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8014a0:	50                   	push   %eax
  8014a1:	ff 75 08             	pushl  0x8(%ebp)
  8014a4:	e8 c0 fe ff ff       	call   801369 <fd_lookup>
  8014a9:	83 c4 08             	add    $0x8,%esp
  8014ac:	85 c0                	test   %eax,%eax
  8014ae:	78 10                	js     8014c0 <close+0x29>
		return r;
	else
		return fd_close(fd, 1);
  8014b0:	83 ec 08             	sub    $0x8,%esp
  8014b3:	6a 01                	push   $0x1
  8014b5:	ff 75 f4             	pushl  -0xc(%ebp)
  8014b8:	e8 56 ff ff ff       	call   801413 <fd_close>
  8014bd:	83 c4 10             	add    $0x10,%esp
}
  8014c0:	c9                   	leave  
  8014c1:	c3                   	ret    

008014c2 <close_all>:

void
close_all(void)
{
  8014c2:	55                   	push   %ebp
  8014c3:	89 e5                	mov    %esp,%ebp
  8014c5:	53                   	push   %ebx
  8014c6:	83 ec 04             	sub    $0x4,%esp
	int i;
	for (i = 0; i < MAXFD; i++)
  8014c9:	bb 00 00 00 00       	mov    $0x0,%ebx
		close(i);
  8014ce:	83 ec 0c             	sub    $0xc,%esp
  8014d1:	53                   	push   %ebx
  8014d2:	e8 c0 ff ff ff       	call   801497 <close>

void
close_all(void)
{
	int i;
	for (i = 0; i < MAXFD; i++)
  8014d7:	43                   	inc    %ebx
  8014d8:	83 c4 10             	add    $0x10,%esp
  8014db:	83 fb 20             	cmp    $0x20,%ebx
  8014de:	75 ee                	jne    8014ce <close_all+0xc>
		close(i);
}
  8014e0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8014e3:	c9                   	leave  
  8014e4:	c3                   	ret    

008014e5 <dup>:
// file and the file offset of the other.
// Closes any previously open file descriptor at 'newfdnum'.
// This is implemented using virtual memory tricks (of course!).
int
dup(int oldfdnum, int newfdnum)
{
  8014e5:	55                   	push   %ebp
  8014e6:	89 e5                	mov    %esp,%ebp
  8014e8:	57                   	push   %edi
  8014e9:	56                   	push   %esi
  8014ea:	53                   	push   %ebx
  8014eb:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	char *ova, *nva;
	pte_t pte;
	struct Fd *oldfd, *newfd;

	if ((r = fd_lookup(oldfdnum, &oldfd)) < 0)
  8014ee:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  8014f1:	50                   	push   %eax
  8014f2:	ff 75 08             	pushl  0x8(%ebp)
  8014f5:	e8 6f fe ff ff       	call   801369 <fd_lookup>
  8014fa:	83 c4 08             	add    $0x8,%esp
  8014fd:	85 c0                	test   %eax,%eax
  8014ff:	0f 88 c2 00 00 00    	js     8015c7 <dup+0xe2>
		return r;
	close(newfdnum);
  801505:	83 ec 0c             	sub    $0xc,%esp
  801508:	ff 75 0c             	pushl  0xc(%ebp)
  80150b:	e8 87 ff ff ff       	call   801497 <close>

	newfd = INDEX2FD(newfdnum);
  801510:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  801513:	c1 e3 0c             	shl    $0xc,%ebx
  801516:	81 eb 00 00 00 30    	sub    $0x30000000,%ebx
	ova = fd2data(oldfd);
  80151c:	83 c4 04             	add    $0x4,%esp
  80151f:	ff 75 e4             	pushl  -0x1c(%ebp)
  801522:	e8 dc fd ff ff       	call   801303 <fd2data>
  801527:	89 c6                	mov    %eax,%esi
	nva = fd2data(newfd);
  801529:	89 1c 24             	mov    %ebx,(%esp)
  80152c:	e8 d2 fd ff ff       	call   801303 <fd2data>
  801531:	83 c4 10             	add    $0x10,%esp
  801534:	89 c7                	mov    %eax,%edi

	if ((uvpd[PDX(ova)] & PTE_P) && (uvpt[PGNUM(ova)] & PTE_P))
  801536:	89 f0                	mov    %esi,%eax
  801538:	c1 e8 16             	shr    $0x16,%eax
  80153b:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  801542:	a8 01                	test   $0x1,%al
  801544:	74 35                	je     80157b <dup+0x96>
  801546:	89 f0                	mov    %esi,%eax
  801548:	c1 e8 0c             	shr    $0xc,%eax
  80154b:	8b 14 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%edx
  801552:	f6 c2 01             	test   $0x1,%dl
  801555:	74 24                	je     80157b <dup+0x96>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
  801557:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80155e:	83 ec 0c             	sub    $0xc,%esp
  801561:	25 07 0e 00 00       	and    $0xe07,%eax
  801566:	50                   	push   %eax
  801567:	57                   	push   %edi
  801568:	6a 00                	push   $0x0
  80156a:	56                   	push   %esi
  80156b:	6a 00                	push   $0x0
  80156d:	e8 cd f5 ff ff       	call   800b3f <sys_page_map>
  801572:	89 c6                	mov    %eax,%esi
  801574:	83 c4 20             	add    $0x20,%esp
  801577:	85 c0                	test   %eax,%eax
  801579:	78 2c                	js     8015a7 <dup+0xc2>
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
  80157b:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  80157e:	89 d0                	mov    %edx,%eax
  801580:	c1 e8 0c             	shr    $0xc,%eax
  801583:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
  80158a:	83 ec 0c             	sub    $0xc,%esp
  80158d:	25 07 0e 00 00       	and    $0xe07,%eax
  801592:	50                   	push   %eax
  801593:	53                   	push   %ebx
  801594:	6a 00                	push   $0x0
  801596:	52                   	push   %edx
  801597:	6a 00                	push   $0x0
  801599:	e8 a1 f5 ff ff       	call   800b3f <sys_page_map>
  80159e:	89 c6                	mov    %eax,%esi
  8015a0:	83 c4 20             	add    $0x20,%esp
  8015a3:	85 c0                	test   %eax,%eax
  8015a5:	79 1d                	jns    8015c4 <dup+0xdf>
		goto err;

	return newfdnum;

err:
	sys_page_unmap(0, newfd);
  8015a7:	83 ec 08             	sub    $0x8,%esp
  8015aa:	53                   	push   %ebx
  8015ab:	6a 00                	push   $0x0
  8015ad:	e8 cf f5 ff ff       	call   800b81 <sys_page_unmap>
	sys_page_unmap(0, nva);
  8015b2:	83 c4 08             	add    $0x8,%esp
  8015b5:	57                   	push   %edi
  8015b6:	6a 00                	push   $0x0
  8015b8:	e8 c4 f5 ff ff       	call   800b81 <sys_page_unmap>
	return r;
  8015bd:	83 c4 10             	add    $0x10,%esp
  8015c0:	89 f0                	mov    %esi,%eax
  8015c2:	eb 03                	jmp    8015c7 <dup+0xe2>
		if ((r = sys_page_map(0, ova, 0, nva, uvpt[PGNUM(ova)] & PTE_SYSCALL)) < 0)
			goto err;
	if ((r = sys_page_map(0, oldfd, 0, newfd, uvpt[PGNUM(oldfd)] & PTE_SYSCALL)) < 0)
		goto err;

	return newfdnum;
  8015c4:	8b 45 0c             	mov    0xc(%ebp),%eax

err:
	sys_page_unmap(0, newfd);
	sys_page_unmap(0, nva);
	return r;
}
  8015c7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8015ca:	5b                   	pop    %ebx
  8015cb:	5e                   	pop    %esi
  8015cc:	5f                   	pop    %edi
  8015cd:	5d                   	pop    %ebp
  8015ce:	c3                   	ret    

008015cf <read>:

ssize_t
read(int fdnum, void *buf, size_t n)
{
  8015cf:	55                   	push   %ebp
  8015d0:	89 e5                	mov    %esp,%ebp
  8015d2:	53                   	push   %ebx
  8015d3:	83 ec 14             	sub    $0x14,%esp
  8015d6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8015d9:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8015dc:	50                   	push   %eax
  8015dd:	53                   	push   %ebx
  8015de:	e8 86 fd ff ff       	call   801369 <fd_lookup>
  8015e3:	83 c4 08             	add    $0x8,%esp
  8015e6:	85 c0                	test   %eax,%eax
  8015e8:	78 67                	js     801651 <read+0x82>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8015ea:	83 ec 08             	sub    $0x8,%esp
  8015ed:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8015f0:	50                   	push   %eax
  8015f1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8015f4:	ff 30                	pushl  (%eax)
  8015f6:	e8 c5 fd ff ff       	call   8013c0 <dev_lookup>
  8015fb:	83 c4 10             	add    $0x10,%esp
  8015fe:	85 c0                	test   %eax,%eax
  801600:	78 4f                	js     801651 <read+0x82>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
  801602:	8b 55 f0             	mov    -0x10(%ebp),%edx
  801605:	8b 42 08             	mov    0x8(%edx),%eax
  801608:	83 e0 03             	and    $0x3,%eax
  80160b:	83 f8 01             	cmp    $0x1,%eax
  80160e:	75 21                	jne    801631 <read+0x62>
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
  801610:	a1 04 40 80 00       	mov    0x804004,%eax
  801615:	8b 40 48             	mov    0x48(%eax),%eax
  801618:	83 ec 04             	sub    $0x4,%esp
  80161b:	53                   	push   %ebx
  80161c:	50                   	push   %eax
  80161d:	68 f8 27 80 00       	push   $0x8027f8
  801622:	e8 8d eb ff ff       	call   8001b4 <cprintf>
		return -E_INVAL;
  801627:	83 c4 10             	add    $0x10,%esp
  80162a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80162f:	eb 20                	jmp    801651 <read+0x82>
	}
	if (!dev->dev_read)
  801631:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801634:	8b 40 08             	mov    0x8(%eax),%eax
  801637:	85 c0                	test   %eax,%eax
  801639:	74 11                	je     80164c <read+0x7d>
		return -E_NOT_SUPP;
	return (*dev->dev_read)(fd, buf, n);
  80163b:	83 ec 04             	sub    $0x4,%esp
  80163e:	ff 75 10             	pushl  0x10(%ebp)
  801641:	ff 75 0c             	pushl  0xc(%ebp)
  801644:	52                   	push   %edx
  801645:	ff d0                	call   *%eax
  801647:	83 c4 10             	add    $0x10,%esp
  80164a:	eb 05                	jmp    801651 <read+0x82>
	if ((fd->fd_omode & O_ACCMODE) == O_WRONLY) {
		cprintf("[%08x] read %d -- bad mode\n", thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_read)
		return -E_NOT_SUPP;
  80164c:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_read)(fd, buf, n);
}
  801651:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801654:	c9                   	leave  
  801655:	c3                   	ret    

00801656 <readn>:

ssize_t
readn(int fdnum, void *buf, size_t n)
{
  801656:	55                   	push   %ebp
  801657:	89 e5                	mov    %esp,%ebp
  801659:	57                   	push   %edi
  80165a:	56                   	push   %esi
  80165b:	53                   	push   %ebx
  80165c:	83 ec 0c             	sub    $0xc,%esp
  80165f:	8b 7d 08             	mov    0x8(%ebp),%edi
  801662:	8b 75 10             	mov    0x10(%ebp),%esi
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  801665:	bb 00 00 00 00       	mov    $0x0,%ebx
  80166a:	eb 21                	jmp    80168d <readn+0x37>
		m = read(fdnum, (char*)buf + tot, n - tot);
  80166c:	83 ec 04             	sub    $0x4,%esp
  80166f:	89 f0                	mov    %esi,%eax
  801671:	29 d8                	sub    %ebx,%eax
  801673:	50                   	push   %eax
  801674:	89 d8                	mov    %ebx,%eax
  801676:	03 45 0c             	add    0xc(%ebp),%eax
  801679:	50                   	push   %eax
  80167a:	57                   	push   %edi
  80167b:	e8 4f ff ff ff       	call   8015cf <read>
		if (m < 0)
  801680:	83 c4 10             	add    $0x10,%esp
  801683:	85 c0                	test   %eax,%eax
  801685:	78 10                	js     801697 <readn+0x41>
			return m;
		if (m == 0)
  801687:	85 c0                	test   %eax,%eax
  801689:	74 0a                	je     801695 <readn+0x3f>
ssize_t
readn(int fdnum, void *buf, size_t n)
{
	int m, tot;

	for (tot = 0; tot < n; tot += m) {
  80168b:	01 c3                	add    %eax,%ebx
  80168d:	39 f3                	cmp    %esi,%ebx
  80168f:	72 db                	jb     80166c <readn+0x16>
  801691:	89 d8                	mov    %ebx,%eax
  801693:	eb 02                	jmp    801697 <readn+0x41>
  801695:	89 d8                	mov    %ebx,%eax
			return m;
		if (m == 0)
			break;
	}
	return tot;
}
  801697:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80169a:	5b                   	pop    %ebx
  80169b:	5e                   	pop    %esi
  80169c:	5f                   	pop    %edi
  80169d:	5d                   	pop    %ebp
  80169e:	c3                   	ret    

0080169f <write>:

ssize_t
write(int fdnum, const void *buf, size_t n)
{
  80169f:	55                   	push   %ebp
  8016a0:	89 e5                	mov    %esp,%ebp
  8016a2:	53                   	push   %ebx
  8016a3:	83 ec 14             	sub    $0x14,%esp
  8016a6:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8016a9:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8016ac:	50                   	push   %eax
  8016ad:	53                   	push   %ebx
  8016ae:	e8 b6 fc ff ff       	call   801369 <fd_lookup>
  8016b3:	83 c4 08             	add    $0x8,%esp
  8016b6:	85 c0                	test   %eax,%eax
  8016b8:	78 62                	js     80171c <write+0x7d>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8016ba:	83 ec 08             	sub    $0x8,%esp
  8016bd:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8016c0:	50                   	push   %eax
  8016c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8016c4:	ff 30                	pushl  (%eax)
  8016c6:	e8 f5 fc ff ff       	call   8013c0 <dev_lookup>
  8016cb:	83 c4 10             	add    $0x10,%esp
  8016ce:	85 c0                	test   %eax,%eax
  8016d0:	78 4a                	js     80171c <write+0x7d>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  8016d2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8016d5:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  8016d9:	75 21                	jne    8016fc <write+0x5d>
		cprintf("[%08x] write %d -- bad mode\n", thisenv->env_id, fdnum);
  8016db:	a1 04 40 80 00       	mov    0x804004,%eax
  8016e0:	8b 40 48             	mov    0x48(%eax),%eax
  8016e3:	83 ec 04             	sub    $0x4,%esp
  8016e6:	53                   	push   %ebx
  8016e7:	50                   	push   %eax
  8016e8:	68 14 28 80 00       	push   $0x802814
  8016ed:	e8 c2 ea ff ff       	call   8001b4 <cprintf>
		return -E_INVAL;
  8016f2:	83 c4 10             	add    $0x10,%esp
  8016f5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8016fa:	eb 20                	jmp    80171c <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
  8016fc:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8016ff:	8b 52 0c             	mov    0xc(%edx),%edx
  801702:	85 d2                	test   %edx,%edx
  801704:	74 11                	je     801717 <write+0x78>
		return -E_NOT_SUPP;
	return (*dev->dev_write)(fd, buf, n);
  801706:	83 ec 04             	sub    $0x4,%esp
  801709:	ff 75 10             	pushl  0x10(%ebp)
  80170c:	ff 75 0c             	pushl  0xc(%ebp)
  80170f:	50                   	push   %eax
  801710:	ff d2                	call   *%edx
  801712:	83 c4 10             	add    $0x10,%esp
  801715:	eb 05                	jmp    80171c <write+0x7d>
	}
	if (debug)
		cprintf("write %d %p %d via dev %s\n",
			fdnum, buf, n, dev->dev_name);
	if (!dev->dev_write)
		return -E_NOT_SUPP;
  801717:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_write)(fd, buf, n);
}
  80171c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80171f:	c9                   	leave  
  801720:	c3                   	ret    

00801721 <seek>:

int
seek(int fdnum, off_t offset)
{
  801721:	55                   	push   %ebp
  801722:	89 e5                	mov    %esp,%ebp
  801724:	83 ec 10             	sub    $0x10,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801727:	8d 45 fc             	lea    -0x4(%ebp),%eax
  80172a:	50                   	push   %eax
  80172b:	ff 75 08             	pushl  0x8(%ebp)
  80172e:	e8 36 fc ff ff       	call   801369 <fd_lookup>
  801733:	83 c4 08             	add    $0x8,%esp
  801736:	85 c0                	test   %eax,%eax
  801738:	78 0e                	js     801748 <seek+0x27>
		return r;
	fd->fd_offset = offset;
  80173a:	8b 45 fc             	mov    -0x4(%ebp),%eax
  80173d:	8b 55 0c             	mov    0xc(%ebp),%edx
  801740:	89 50 04             	mov    %edx,0x4(%eax)
	return 0;
  801743:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801748:	c9                   	leave  
  801749:	c3                   	ret    

0080174a <ftruncate>:

int
ftruncate(int fdnum, off_t newsize)
{
  80174a:	55                   	push   %ebp
  80174b:	89 e5                	mov    %esp,%ebp
  80174d:	53                   	push   %ebx
  80174e:	83 ec 14             	sub    $0x14,%esp
  801751:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
  801754:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801757:	50                   	push   %eax
  801758:	53                   	push   %ebx
  801759:	e8 0b fc ff ff       	call   801369 <fd_lookup>
  80175e:	83 c4 08             	add    $0x8,%esp
  801761:	85 c0                	test   %eax,%eax
  801763:	78 5f                	js     8017c4 <ftruncate+0x7a>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  801765:	83 ec 08             	sub    $0x8,%esp
  801768:	8d 45 f4             	lea    -0xc(%ebp),%eax
  80176b:	50                   	push   %eax
  80176c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80176f:	ff 30                	pushl  (%eax)
  801771:	e8 4a fc ff ff       	call   8013c0 <dev_lookup>
  801776:	83 c4 10             	add    $0x10,%esp
  801779:	85 c0                	test   %eax,%eax
  80177b:	78 47                	js     8017c4 <ftruncate+0x7a>
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
  80177d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801780:	f6 40 08 03          	testb  $0x3,0x8(%eax)
  801784:	75 21                	jne    8017a7 <ftruncate+0x5d>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
  801786:	a1 04 40 80 00       	mov    0x804004,%eax
	struct Fd *fd;
	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if ((fd->fd_omode & O_ACCMODE) == O_RDONLY) {
		cprintf("[%08x] ftruncate %d -- bad mode\n",
  80178b:	8b 40 48             	mov    0x48(%eax),%eax
  80178e:	83 ec 04             	sub    $0x4,%esp
  801791:	53                   	push   %ebx
  801792:	50                   	push   %eax
  801793:	68 d4 27 80 00       	push   $0x8027d4
  801798:	e8 17 ea ff ff       	call   8001b4 <cprintf>
			thisenv->env_id, fdnum);
		return -E_INVAL;
  80179d:	83 c4 10             	add    $0x10,%esp
  8017a0:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  8017a5:	eb 1d                	jmp    8017c4 <ftruncate+0x7a>
	}
	if (!dev->dev_trunc)
  8017a7:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8017aa:	8b 52 18             	mov    0x18(%edx),%edx
  8017ad:	85 d2                	test   %edx,%edx
  8017af:	74 0e                	je     8017bf <ftruncate+0x75>
		return -E_NOT_SUPP;
	return (*dev->dev_trunc)(fd, newsize);
  8017b1:	83 ec 08             	sub    $0x8,%esp
  8017b4:	ff 75 0c             	pushl  0xc(%ebp)
  8017b7:	50                   	push   %eax
  8017b8:	ff d2                	call   *%edx
  8017ba:	83 c4 10             	add    $0x10,%esp
  8017bd:	eb 05                	jmp    8017c4 <ftruncate+0x7a>
		cprintf("[%08x] ftruncate %d -- bad mode\n",
			thisenv->env_id, fdnum);
		return -E_INVAL;
	}
	if (!dev->dev_trunc)
		return -E_NOT_SUPP;
  8017bf:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	return (*dev->dev_trunc)(fd, newsize);
}
  8017c4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8017c7:	c9                   	leave  
  8017c8:	c3                   	ret    

008017c9 <fstat>:

int
fstat(int fdnum, struct Stat *stat)
{
  8017c9:	55                   	push   %ebp
  8017ca:	89 e5                	mov    %esp,%ebp
  8017cc:	53                   	push   %ebx
  8017cd:	83 ec 14             	sub    $0x14,%esp
  8017d0:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;
	struct Dev *dev;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0
  8017d3:	8d 45 f0             	lea    -0x10(%ebp),%eax
  8017d6:	50                   	push   %eax
  8017d7:	ff 75 08             	pushl  0x8(%ebp)
  8017da:	e8 8a fb ff ff       	call   801369 <fd_lookup>
  8017df:	83 c4 08             	add    $0x8,%esp
  8017e2:	85 c0                	test   %eax,%eax
  8017e4:	78 52                	js     801838 <fstat+0x6f>
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
  8017e6:	83 ec 08             	sub    $0x8,%esp
  8017e9:	8d 45 f4             	lea    -0xc(%ebp),%eax
  8017ec:	50                   	push   %eax
  8017ed:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8017f0:	ff 30                	pushl  (%eax)
  8017f2:	e8 c9 fb ff ff       	call   8013c0 <dev_lookup>
  8017f7:	83 c4 10             	add    $0x10,%esp
  8017fa:	85 c0                	test   %eax,%eax
  8017fc:	78 3a                	js     801838 <fstat+0x6f>
		return r;
	if (!dev->dev_stat)
  8017fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801801:	83 78 14 00          	cmpl   $0x0,0x14(%eax)
  801805:	74 2c                	je     801833 <fstat+0x6a>
		return -E_NOT_SUPP;
	stat->st_name[0] = 0;
  801807:	c6 03 00             	movb   $0x0,(%ebx)
	stat->st_size = 0;
  80180a:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
  801811:	00 00 00 
	stat->st_isdir = 0;
  801814:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  80181b:	00 00 00 
	stat->st_dev = dev;
  80181e:	89 83 88 00 00 00    	mov    %eax,0x88(%ebx)
	return (*dev->dev_stat)(fd, stat);
  801824:	83 ec 08             	sub    $0x8,%esp
  801827:	53                   	push   %ebx
  801828:	ff 75 f0             	pushl  -0x10(%ebp)
  80182b:	ff 50 14             	call   *0x14(%eax)
  80182e:	83 c4 10             	add    $0x10,%esp
  801831:	eb 05                	jmp    801838 <fstat+0x6f>

	if ((r = fd_lookup(fdnum, &fd)) < 0
	    || (r = dev_lookup(fd->fd_dev_id, &dev)) < 0)
		return r;
	if (!dev->dev_stat)
		return -E_NOT_SUPP;
  801833:	b8 f1 ff ff ff       	mov    $0xfffffff1,%eax
	stat->st_name[0] = 0;
	stat->st_size = 0;
	stat->st_isdir = 0;
	stat->st_dev = dev;
	return (*dev->dev_stat)(fd, stat);
}
  801838:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80183b:	c9                   	leave  
  80183c:	c3                   	ret    

0080183d <stat>:

int
stat(const char *path, struct Stat *stat)
{
  80183d:	55                   	push   %ebp
  80183e:	89 e5                	mov    %esp,%ebp
  801840:	56                   	push   %esi
  801841:	53                   	push   %ebx
	int fd, r;

	if ((fd = open(path, O_RDONLY)) < 0)
  801842:	83 ec 08             	sub    $0x8,%esp
  801845:	6a 00                	push   $0x0
  801847:	ff 75 08             	pushl  0x8(%ebp)
  80184a:	e8 e7 01 00 00       	call   801a36 <open>
  80184f:	89 c3                	mov    %eax,%ebx
  801851:	83 c4 10             	add    $0x10,%esp
  801854:	85 c0                	test   %eax,%eax
  801856:	78 1d                	js     801875 <stat+0x38>
		return fd;
	r = fstat(fd, stat);
  801858:	83 ec 08             	sub    $0x8,%esp
  80185b:	ff 75 0c             	pushl  0xc(%ebp)
  80185e:	50                   	push   %eax
  80185f:	e8 65 ff ff ff       	call   8017c9 <fstat>
  801864:	89 c6                	mov    %eax,%esi
	close(fd);
  801866:	89 1c 24             	mov    %ebx,(%esp)
  801869:	e8 29 fc ff ff       	call   801497 <close>
	return r;
  80186e:	83 c4 10             	add    $0x10,%esp
  801871:	89 f0                	mov    %esi,%eax
  801873:	eb 00                	jmp    801875 <stat+0x38>
}
  801875:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801878:	5b                   	pop    %ebx
  801879:	5e                   	pop    %esi
  80187a:	5d                   	pop    %ebp
  80187b:	c3                   	ret    

0080187c <fsipc>:
// type: request code, passed as the simple integer IPC value.
// dstva: virtual address at which to receive reply page, 0 if none.
// Returns result from the file server.
static int
fsipc(unsigned type, void *dstva)
{
  80187c:	55                   	push   %ebp
  80187d:	89 e5                	mov    %esp,%ebp
  80187f:	56                   	push   %esi
  801880:	53                   	push   %ebx
  801881:	89 c6                	mov    %eax,%esi
  801883:	89 d3                	mov    %edx,%ebx
	static envid_t fsenv;
	if (fsenv == 0)
  801885:	83 3d 00 40 80 00 00 	cmpl   $0x0,0x804000
  80188c:	75 12                	jne    8018a0 <fsipc+0x24>
		fsenv = ipc_find_env(ENV_TYPE_FS);
  80188e:	83 ec 0c             	sub    $0xc,%esp
  801891:	6a 01                	push   $0x1
  801893:	e8 b8 07 00 00       	call   802050 <ipc_find_env>
  801898:	a3 00 40 80 00       	mov    %eax,0x804000
  80189d:	83 c4 10             	add    $0x10,%esp
	static_assert(sizeof(fsipcbuf) == PGSIZE);

	if (debug)
		cprintf("[%08x] fsipc %d %08x\n", thisenv->env_id, type, *(uint32_t *)&fsipcbuf);

	ipc_send(fsenv, type, &fsipcbuf, PTE_P | PTE_W | PTE_U);
  8018a0:	6a 07                	push   $0x7
  8018a2:	68 00 50 80 00       	push   $0x805000
  8018a7:	56                   	push   %esi
  8018a8:	ff 35 00 40 80 00    	pushl  0x804000
  8018ae:	e8 48 07 00 00       	call   801ffb <ipc_send>
	return ipc_recv(NULL, dstva, NULL);
  8018b3:	83 c4 0c             	add    $0xc,%esp
  8018b6:	6a 00                	push   $0x0
  8018b8:	53                   	push   %ebx
  8018b9:	6a 00                	push   $0x0
  8018bb:	e8 d3 06 00 00       	call   801f93 <ipc_recv>
}
  8018c0:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8018c3:	5b                   	pop    %ebx
  8018c4:	5e                   	pop    %esi
  8018c5:	5d                   	pop    %ebp
  8018c6:	c3                   	ret    

008018c7 <devfile_trunc>:
}

// Truncate or extend an open file to 'size' bytes
static int
devfile_trunc(struct Fd *fd, off_t newsize)
{
  8018c7:	55                   	push   %ebp
  8018c8:	89 e5                	mov    %esp,%ebp
  8018ca:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.set_size.req_fileid = fd->fd_file.id;
  8018cd:	8b 45 08             	mov    0x8(%ebp),%eax
  8018d0:	8b 40 0c             	mov    0xc(%eax),%eax
  8018d3:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.set_size.req_size = newsize;
  8018d8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8018db:	a3 04 50 80 00       	mov    %eax,0x805004
	return fsipc(FSREQ_SET_SIZE, NULL);
  8018e0:	ba 00 00 00 00       	mov    $0x0,%edx
  8018e5:	b8 02 00 00 00       	mov    $0x2,%eax
  8018ea:	e8 8d ff ff ff       	call   80187c <fsipc>
}
  8018ef:	c9                   	leave  
  8018f0:	c3                   	ret    

008018f1 <devfile_flush>:
// open, unmapping it is enough to free up server-side resources.
// Other than that, we just have to make sure our changes are flushed
// to disk.
static int
devfile_flush(struct Fd *fd)
{
  8018f1:	55                   	push   %ebp
  8018f2:	89 e5                	mov    %esp,%ebp
  8018f4:	83 ec 08             	sub    $0x8,%esp
	fsipcbuf.flush.req_fileid = fd->fd_file.id;
  8018f7:	8b 45 08             	mov    0x8(%ebp),%eax
  8018fa:	8b 40 0c             	mov    0xc(%eax),%eax
  8018fd:	a3 00 50 80 00       	mov    %eax,0x805000
	return fsipc(FSREQ_FLUSH, NULL);
  801902:	ba 00 00 00 00       	mov    $0x0,%edx
  801907:	b8 06 00 00 00       	mov    $0x6,%eax
  80190c:	e8 6b ff ff ff       	call   80187c <fsipc>
}
  801911:	c9                   	leave  
  801912:	c3                   	ret    

00801913 <devfile_stat>:
	//panic("devfile_write not implemented");
}

static int
devfile_stat(struct Fd *fd, struct Stat *st)
{
  801913:	55                   	push   %ebp
  801914:	89 e5                	mov    %esp,%ebp
  801916:	53                   	push   %ebx
  801917:	83 ec 04             	sub    $0x4,%esp
  80191a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	int r;

	fsipcbuf.stat.req_fileid = fd->fd_file.id;
  80191d:	8b 45 08             	mov    0x8(%ebp),%eax
  801920:	8b 40 0c             	mov    0xc(%eax),%eax
  801923:	a3 00 50 80 00       	mov    %eax,0x805000
	if ((r = fsipc(FSREQ_STAT, NULL)) < 0)
  801928:	ba 00 00 00 00       	mov    $0x0,%edx
  80192d:	b8 05 00 00 00       	mov    $0x5,%eax
  801932:	e8 45 ff ff ff       	call   80187c <fsipc>
  801937:	85 c0                	test   %eax,%eax
  801939:	78 2c                	js     801967 <devfile_stat+0x54>
		return r;
	strcpy(st->st_name, fsipcbuf.statRet.ret_name);
  80193b:	83 ec 08             	sub    $0x8,%esp
  80193e:	68 00 50 80 00       	push   $0x805000
  801943:	53                   	push   %ebx
  801944:	e8 cf ed ff ff       	call   800718 <strcpy>
	st->st_size = fsipcbuf.statRet.ret_size;
  801949:	a1 80 50 80 00       	mov    0x805080,%eax
  80194e:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	st->st_isdir = fsipcbuf.statRet.ret_isdir;
  801954:	a1 84 50 80 00       	mov    0x805084,%eax
  801959:	89 83 84 00 00 00    	mov    %eax,0x84(%ebx)
	return 0;
  80195f:	83 c4 10             	add    $0x10,%esp
  801962:	b8 00 00 00 00       	mov    $0x0,%eax
}
  801967:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80196a:	c9                   	leave  
  80196b:	c3                   	ret    

0080196c <devfile_write>:
// Returns:
//	 The number of bytes successfully written.
//	 < 0 on error.
static ssize_t
devfile_write(struct Fd *fd, const void *buf, size_t n)
{
  80196c:	55                   	push   %ebp
  80196d:	89 e5                	mov    %esp,%ebp
  80196f:	83 ec 08             	sub    $0x8,%esp
  801972:	8b 45 10             	mov    0x10(%ebp),%eax
	// remember that write is always allowed to write *fewer*
	// bytes than requested.
	// LAB 5: Your code here
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;
  801975:	8b 55 08             	mov    0x8(%ebp),%edx
  801978:	8b 52 0c             	mov    0xc(%edx),%edx
  80197b:	89 15 00 50 80 00    	mov    %edx,0x805000

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
    if (n < mvsz)
  801981:	3d f7 0f 00 00       	cmp    $0xff7,%eax
  801986:	76 05                	jbe    80198d <devfile_write+0x21>
    // Make request.
    struct Fsreq_write *req = &fsipcbuf.write;
    req->req_fileid = fd->fd_file.id;

    // Make sure not exceed size of req_buf.
    size_t mvsz = sizeof(req->req_buf);
  801988:	b8 f8 0f 00 00       	mov    $0xff8,%eax
    if (n < mvsz)
        mvsz = n;
    req->req_n = mvsz;
  80198d:	a3 04 50 80 00       	mov    %eax,0x805004
    
    // Copy the bytes to write.
    memmove(req->req_buf, buf, mvsz);
  801992:	83 ec 04             	sub    $0x4,%esp
  801995:	50                   	push   %eax
  801996:	ff 75 0c             	pushl  0xc(%ebp)
  801999:	68 08 50 80 00       	push   $0x805008
  80199e:	e8 ea ee ff ff       	call   80088d <memmove>

    // Send request.
    ssize_t wrnsz = fsipc(FSREQ_WRITE, NULL);
  8019a3:	ba 00 00 00 00       	mov    $0x0,%edx
  8019a8:	b8 04 00 00 00       	mov    $0x4,%eax
  8019ad:	e8 ca fe ff ff       	call   80187c <fsipc>

    return wrnsz;
	//panic("devfile_write not implemented");
}
  8019b2:	c9                   	leave  
  8019b3:	c3                   	ret    

008019b4 <devfile_read>:
// Returns:
// 	The number of bytes successfully read.
// 	< 0 on error.
static ssize_t
devfile_read(struct Fd *fd, void *buf, size_t n)
{
  8019b4:	55                   	push   %ebp
  8019b5:	89 e5                	mov    %esp,%ebp
  8019b7:	56                   	push   %esi
  8019b8:	53                   	push   %ebx
  8019b9:	8b 75 10             	mov    0x10(%ebp),%esi
	// filling fsipcbuf.read with the request arguments.  The
	// bytes read will be written back to fsipcbuf by the file
	// system server.
	int r;

	fsipcbuf.read.req_fileid = fd->fd_file.id;
  8019bc:	8b 45 08             	mov    0x8(%ebp),%eax
  8019bf:	8b 40 0c             	mov    0xc(%eax),%eax
  8019c2:	a3 00 50 80 00       	mov    %eax,0x805000
	fsipcbuf.read.req_n = n;
  8019c7:	89 35 04 50 80 00    	mov    %esi,0x805004
	if ((r = fsipc(FSREQ_READ, NULL)) < 0)
  8019cd:	ba 00 00 00 00       	mov    $0x0,%edx
  8019d2:	b8 03 00 00 00       	mov    $0x3,%eax
  8019d7:	e8 a0 fe ff ff       	call   80187c <fsipc>
  8019dc:	89 c3                	mov    %eax,%ebx
  8019de:	85 c0                	test   %eax,%eax
  8019e0:	78 4b                	js     801a2d <devfile_read+0x79>
		return r;
	assert(r <= n);
  8019e2:	39 c6                	cmp    %eax,%esi
  8019e4:	73 16                	jae    8019fc <devfile_read+0x48>
  8019e6:	68 44 28 80 00       	push   $0x802844
  8019eb:	68 04 27 80 00       	push   $0x802704
  8019f0:	6a 7c                	push   $0x7c
  8019f2:	68 4b 28 80 00       	push   $0x80284b
  8019f7:	e8 e0 e6 ff ff       	call   8000dc <_panic>
	assert(r <= PGSIZE);
  8019fc:	3d 00 10 00 00       	cmp    $0x1000,%eax
  801a01:	7e 16                	jle    801a19 <devfile_read+0x65>
  801a03:	68 56 28 80 00       	push   $0x802856
  801a08:	68 04 27 80 00       	push   $0x802704
  801a0d:	6a 7d                	push   $0x7d
  801a0f:	68 4b 28 80 00       	push   $0x80284b
  801a14:	e8 c3 e6 ff ff       	call   8000dc <_panic>
	memmove(buf, fsipcbuf.readRet.ret_buf, r);
  801a19:	83 ec 04             	sub    $0x4,%esp
  801a1c:	50                   	push   %eax
  801a1d:	68 00 50 80 00       	push   $0x805000
  801a22:	ff 75 0c             	pushl  0xc(%ebp)
  801a25:	e8 63 ee ff ff       	call   80088d <memmove>
	return r;
  801a2a:	83 c4 10             	add    $0x10,%esp
}
  801a2d:	89 d8                	mov    %ebx,%eax
  801a2f:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801a32:	5b                   	pop    %ebx
  801a33:	5e                   	pop    %esi
  801a34:	5d                   	pop    %ebp
  801a35:	c3                   	ret    

00801a36 <open>:
// 	The file descriptor index on success
// 	-E_BAD_PATH if the path is too long (>= MAXPATHLEN)
// 	< 0 for other errors.
int
open(const char *path, int mode)
{
  801a36:	55                   	push   %ebp
  801a37:	89 e5                	mov    %esp,%ebp
  801a39:	53                   	push   %ebx
  801a3a:	83 ec 20             	sub    $0x20,%esp
  801a3d:	8b 5d 08             	mov    0x8(%ebp),%ebx
	// file descriptor.

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
  801a40:	53                   	push   %ebx
  801a41:	e8 9d ec ff ff       	call   8006e3 <strlen>
  801a46:	83 c4 10             	add    $0x10,%esp
  801a49:	3d ff 03 00 00       	cmp    $0x3ff,%eax
  801a4e:	7f 63                	jg     801ab3 <open+0x7d>
		return -E_BAD_PATH;

	if ((r = fd_alloc(&fd)) < 0)
  801a50:	83 ec 0c             	sub    $0xc,%esp
  801a53:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801a56:	50                   	push   %eax
  801a57:	e8 be f8 ff ff       	call   80131a <fd_alloc>
  801a5c:	83 c4 10             	add    $0x10,%esp
  801a5f:	85 c0                	test   %eax,%eax
  801a61:	78 55                	js     801ab8 <open+0x82>
		return r;

	strcpy(fsipcbuf.open.req_path, path);
  801a63:	83 ec 08             	sub    $0x8,%esp
  801a66:	53                   	push   %ebx
  801a67:	68 00 50 80 00       	push   $0x805000
  801a6c:	e8 a7 ec ff ff       	call   800718 <strcpy>
	fsipcbuf.open.req_omode = mode;
  801a71:	8b 45 0c             	mov    0xc(%ebp),%eax
  801a74:	a3 00 54 80 00       	mov    %eax,0x805400

	if ((r = fsipc(FSREQ_OPEN, fd)) < 0) {
  801a79:	8b 55 f4             	mov    -0xc(%ebp),%edx
  801a7c:	b8 01 00 00 00       	mov    $0x1,%eax
  801a81:	e8 f6 fd ff ff       	call   80187c <fsipc>
  801a86:	89 c3                	mov    %eax,%ebx
  801a88:	83 c4 10             	add    $0x10,%esp
  801a8b:	85 c0                	test   %eax,%eax
  801a8d:	79 14                	jns    801aa3 <open+0x6d>
		fd_close(fd, 0);
  801a8f:	83 ec 08             	sub    $0x8,%esp
  801a92:	6a 00                	push   $0x0
  801a94:	ff 75 f4             	pushl  -0xc(%ebp)
  801a97:	e8 77 f9 ff ff       	call   801413 <fd_close>
		return r;
  801a9c:	83 c4 10             	add    $0x10,%esp
  801a9f:	89 d8                	mov    %ebx,%eax
  801aa1:	eb 15                	jmp    801ab8 <open+0x82>
	}

	return fd2num(fd);
  801aa3:	83 ec 0c             	sub    $0xc,%esp
  801aa6:	ff 75 f4             	pushl  -0xc(%ebp)
  801aa9:	e8 45 f8 ff ff       	call   8012f3 <fd2num>
  801aae:	83 c4 10             	add    $0x10,%esp
  801ab1:	eb 05                	jmp    801ab8 <open+0x82>

	int r;
	struct Fd *fd;

	if (strlen(path) >= MAXPATHLEN)
		return -E_BAD_PATH;
  801ab3:	b8 f4 ff ff ff       	mov    $0xfffffff4,%eax
		fd_close(fd, 0);
		return r;
	}

	return fd2num(fd);
}
  801ab8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801abb:	c9                   	leave  
  801abc:	c3                   	ret    

00801abd <sync>:


// Synchronize disk with buffer cache
int
sync(void)
{
  801abd:	55                   	push   %ebp
  801abe:	89 e5                	mov    %esp,%ebp
  801ac0:	83 ec 08             	sub    $0x8,%esp
	// Ask the file server to update the disk
	// by writing any dirty blocks in the buffer cache.

	return fsipc(FSREQ_SYNC, NULL);
  801ac3:	ba 00 00 00 00       	mov    $0x0,%edx
  801ac8:	b8 08 00 00 00       	mov    $0x8,%eax
  801acd:	e8 aa fd ff ff       	call   80187c <fsipc>
}
  801ad2:	c9                   	leave  
  801ad3:	c3                   	ret    

00801ad4 <devpipe_stat>:
	return i;
}

static int
devpipe_stat(struct Fd *fd, struct Stat *stat)
{
  801ad4:	55                   	push   %ebp
  801ad5:	89 e5                	mov    %esp,%ebp
  801ad7:	56                   	push   %esi
  801ad8:	53                   	push   %ebx
  801ad9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	struct Pipe *p = (struct Pipe*) fd2data(fd);
  801adc:	83 ec 0c             	sub    $0xc,%esp
  801adf:	ff 75 08             	pushl  0x8(%ebp)
  801ae2:	e8 1c f8 ff ff       	call   801303 <fd2data>
  801ae7:	89 c6                	mov    %eax,%esi
	strcpy(stat->st_name, "<pipe>");
  801ae9:	83 c4 08             	add    $0x8,%esp
  801aec:	68 62 28 80 00       	push   $0x802862
  801af1:	53                   	push   %ebx
  801af2:	e8 21 ec ff ff       	call   800718 <strcpy>
	stat->st_size = p->p_wpos - p->p_rpos;
  801af7:	8b 46 04             	mov    0x4(%esi),%eax
  801afa:	2b 06                	sub    (%esi),%eax
  801afc:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
	stat->st_isdir = 0;
  801b02:	c7 83 84 00 00 00 00 	movl   $0x0,0x84(%ebx)
  801b09:	00 00 00 
	stat->st_dev = &devpipe;
  801b0c:	c7 83 88 00 00 00 20 	movl   $0x803020,0x88(%ebx)
  801b13:	30 80 00 
	return 0;
}
  801b16:	b8 00 00 00 00       	mov    $0x0,%eax
  801b1b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801b1e:	5b                   	pop    %ebx
  801b1f:	5e                   	pop    %esi
  801b20:	5d                   	pop    %ebp
  801b21:	c3                   	ret    

00801b22 <devpipe_close>:

static int
devpipe_close(struct Fd *fd)
{
  801b22:	55                   	push   %ebp
  801b23:	89 e5                	mov    %esp,%ebp
  801b25:	53                   	push   %ebx
  801b26:	83 ec 0c             	sub    $0xc,%esp
  801b29:	8b 5d 08             	mov    0x8(%ebp),%ebx
	(void) sys_page_unmap(0, fd);
  801b2c:	53                   	push   %ebx
  801b2d:	6a 00                	push   $0x0
  801b2f:	e8 4d f0 ff ff       	call   800b81 <sys_page_unmap>
	return sys_page_unmap(0, fd2data(fd));
  801b34:	89 1c 24             	mov    %ebx,(%esp)
  801b37:	e8 c7 f7 ff ff       	call   801303 <fd2data>
  801b3c:	83 c4 08             	add    $0x8,%esp
  801b3f:	50                   	push   %eax
  801b40:	6a 00                	push   $0x0
  801b42:	e8 3a f0 ff ff       	call   800b81 <sys_page_unmap>
}
  801b47:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  801b4a:	c9                   	leave  
  801b4b:	c3                   	ret    

00801b4c <_pipeisclosed>:
	return r;
}

static int
_pipeisclosed(struct Fd *fd, struct Pipe *p)
{
  801b4c:	55                   	push   %ebp
  801b4d:	89 e5                	mov    %esp,%ebp
  801b4f:	57                   	push   %edi
  801b50:	56                   	push   %esi
  801b51:	53                   	push   %ebx
  801b52:	83 ec 1c             	sub    $0x1c,%esp
  801b55:	89 45 e0             	mov    %eax,-0x20(%ebp)
  801b58:	89 d7                	mov    %edx,%edi
	int n, nn, ret;

	while (1) {
		n = thisenv->env_runs;
  801b5a:	a1 04 40 80 00       	mov    0x804004,%eax
  801b5f:	8b 70 58             	mov    0x58(%eax),%esi
		ret = pageref(fd) == pageref(p);
  801b62:	83 ec 0c             	sub    $0xc,%esp
  801b65:	ff 75 e0             	pushl  -0x20(%ebp)
  801b68:	e8 27 05 00 00       	call   802094 <pageref>
  801b6d:	89 c3                	mov    %eax,%ebx
  801b6f:	89 3c 24             	mov    %edi,(%esp)
  801b72:	e8 1d 05 00 00       	call   802094 <pageref>
  801b77:	83 c4 10             	add    $0x10,%esp
  801b7a:	39 c3                	cmp    %eax,%ebx
  801b7c:	0f 94 c1             	sete   %cl
  801b7f:	0f b6 c9             	movzbl %cl,%ecx
  801b82:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
		nn = thisenv->env_runs;
  801b85:	8b 15 04 40 80 00    	mov    0x804004,%edx
  801b8b:	8b 4a 58             	mov    0x58(%edx),%ecx
		if (n == nn)
  801b8e:	39 ce                	cmp    %ecx,%esi
  801b90:	74 1b                	je     801bad <_pipeisclosed+0x61>
			return ret;
		if (n != nn && ret == 1)
  801b92:	39 c3                	cmp    %eax,%ebx
  801b94:	75 c4                	jne    801b5a <_pipeisclosed+0xe>
			cprintf("pipe race avoided\n", n, thisenv->env_runs, ret);
  801b96:	8b 42 58             	mov    0x58(%edx),%eax
  801b99:	ff 75 e4             	pushl  -0x1c(%ebp)
  801b9c:	50                   	push   %eax
  801b9d:	56                   	push   %esi
  801b9e:	68 69 28 80 00       	push   $0x802869
  801ba3:	e8 0c e6 ff ff       	call   8001b4 <cprintf>
  801ba8:	83 c4 10             	add    $0x10,%esp
  801bab:	eb ad                	jmp    801b5a <_pipeisclosed+0xe>
	}
}
  801bad:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  801bb0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801bb3:	5b                   	pop    %ebx
  801bb4:	5e                   	pop    %esi
  801bb5:	5f                   	pop    %edi
  801bb6:	5d                   	pop    %ebp
  801bb7:	c3                   	ret    

00801bb8 <devpipe_write>:
	return i;
}

static ssize_t
devpipe_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801bb8:	55                   	push   %ebp
  801bb9:	89 e5                	mov    %esp,%ebp
  801bbb:	57                   	push   %edi
  801bbc:	56                   	push   %esi
  801bbd:	53                   	push   %ebx
  801bbe:	83 ec 18             	sub    $0x18,%esp
  801bc1:	8b 75 08             	mov    0x8(%ebp),%esi
	const uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*) fd2data(fd);
  801bc4:	56                   	push   %esi
  801bc5:	e8 39 f7 ff ff       	call   801303 <fd2data>
  801bca:	89 c3                	mov    %eax,%ebx
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801bcc:	83 c4 10             	add    $0x10,%esp
  801bcf:	bf 00 00 00 00       	mov    $0x0,%edi
  801bd4:	eb 3b                	jmp    801c11 <devpipe_write+0x59>
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
  801bd6:	89 da                	mov    %ebx,%edx
  801bd8:	89 f0                	mov    %esi,%eax
  801bda:	e8 6d ff ff ff       	call   801b4c <_pipeisclosed>
  801bdf:	85 c0                	test   %eax,%eax
  801be1:	75 38                	jne    801c1b <devpipe_write+0x63>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_write yield\n");
			sys_yield();
  801be3:	e8 f5 ee ff ff       	call   800add <sys_yield>
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_wpos >= p->p_rpos + sizeof(p->p_buf)) {
  801be8:	8b 53 04             	mov    0x4(%ebx),%edx
  801beb:	8b 03                	mov    (%ebx),%eax
  801bed:	83 c0 20             	add    $0x20,%eax
  801bf0:	39 c2                	cmp    %eax,%edx
  801bf2:	73 e2                	jae    801bd6 <devpipe_write+0x1e>
				cprintf("devpipe_write yield\n");
			sys_yield();
		}
		// there's room for a byte.  store it.
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
  801bf4:	8b 45 0c             	mov    0xc(%ebp),%eax
  801bf7:	8a 0c 38             	mov    (%eax,%edi,1),%cl
  801bfa:	89 d0                	mov    %edx,%eax
  801bfc:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801c01:	79 05                	jns    801c08 <devpipe_write+0x50>
  801c03:	48                   	dec    %eax
  801c04:	83 c8 e0             	or     $0xffffffe0,%eax
  801c07:	40                   	inc    %eax
  801c08:	88 4c 03 08          	mov    %cl,0x8(%ebx,%eax,1)
		p->p_wpos++;
  801c0c:	42                   	inc    %edx
  801c0d:	89 53 04             	mov    %edx,0x4(%ebx)
	if (debug)
		cprintf("[%08x] devpipe_write %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c10:	47                   	inc    %edi
  801c11:	3b 7d 10             	cmp    0x10(%ebp),%edi
  801c14:	75 d2                	jne    801be8 <devpipe_write+0x30>
		// wait to increment wpos until the byte is stored!
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
  801c16:	8b 45 10             	mov    0x10(%ebp),%eax
  801c19:	eb 05                	jmp    801c20 <devpipe_write+0x68>
			// pipe is full
			// if all the readers are gone
			// (it's only writers like us now),
			// note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801c1b:	b8 00 00 00 00       	mov    $0x0,%eax
		p->p_buf[p->p_wpos % PIPEBUFSIZ] = buf[i];
		p->p_wpos++;
	}

	return i;
}
  801c20:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c23:	5b                   	pop    %ebx
  801c24:	5e                   	pop    %esi
  801c25:	5f                   	pop    %edi
  801c26:	5d                   	pop    %ebp
  801c27:	c3                   	ret    

00801c28 <devpipe_read>:
	return _pipeisclosed(fd, p);
}

static ssize_t
devpipe_read(struct Fd *fd, void *vbuf, size_t n)
{
  801c28:	55                   	push   %ebp
  801c29:	89 e5                	mov    %esp,%ebp
  801c2b:	57                   	push   %edi
  801c2c:	56                   	push   %esi
  801c2d:	53                   	push   %ebx
  801c2e:	83 ec 18             	sub    $0x18,%esp
  801c31:	8b 7d 08             	mov    0x8(%ebp),%edi
	uint8_t *buf;
	size_t i;
	struct Pipe *p;

	p = (struct Pipe*)fd2data(fd);
  801c34:	57                   	push   %edi
  801c35:	e8 c9 f6 ff ff       	call   801303 <fd2data>
  801c3a:	89 c6                	mov    %eax,%esi
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c3c:	83 c4 10             	add    $0x10,%esp
  801c3f:	bb 00 00 00 00       	mov    $0x0,%ebx
  801c44:	eb 3a                	jmp    801c80 <devpipe_read+0x58>
		while (p->p_rpos == p->p_wpos) {
			// pipe is empty
			// if we got any data, return it
			if (i > 0)
  801c46:	85 db                	test   %ebx,%ebx
  801c48:	74 04                	je     801c4e <devpipe_read+0x26>
				return i;
  801c4a:	89 d8                	mov    %ebx,%eax
  801c4c:	eb 41                	jmp    801c8f <devpipe_read+0x67>
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
  801c4e:	89 f2                	mov    %esi,%edx
  801c50:	89 f8                	mov    %edi,%eax
  801c52:	e8 f5 fe ff ff       	call   801b4c <_pipeisclosed>
  801c57:	85 c0                	test   %eax,%eax
  801c59:	75 2f                	jne    801c8a <devpipe_read+0x62>
				return 0;
			// yield and see what happens
			if (debug)
				cprintf("devpipe_read yield\n");
			sys_yield();
  801c5b:	e8 7d ee ff ff       	call   800add <sys_yield>
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
		while (p->p_rpos == p->p_wpos) {
  801c60:	8b 06                	mov    (%esi),%eax
  801c62:	3b 46 04             	cmp    0x4(%esi),%eax
  801c65:	74 df                	je     801c46 <devpipe_read+0x1e>
				cprintf("devpipe_read yield\n");
			sys_yield();
		}
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
  801c67:	25 1f 00 00 80       	and    $0x8000001f,%eax
  801c6c:	79 05                	jns    801c73 <devpipe_read+0x4b>
  801c6e:	48                   	dec    %eax
  801c6f:	83 c8 e0             	or     $0xffffffe0,%eax
  801c72:	40                   	inc    %eax
  801c73:	8a 44 06 08          	mov    0x8(%esi,%eax,1),%al
  801c77:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  801c7a:	88 04 19             	mov    %al,(%ecx,%ebx,1)
		p->p_rpos++;
  801c7d:	ff 06                	incl   (%esi)
	if (debug)
		cprintf("[%08x] devpipe_read %08x %d rpos %d wpos %d\n",
			thisenv->env_id, uvpt[PGNUM(p)], n, p->p_rpos, p->p_wpos);

	buf = vbuf;
	for (i = 0; i < n; i++) {
  801c7f:	43                   	inc    %ebx
  801c80:	3b 5d 10             	cmp    0x10(%ebp),%ebx
  801c83:	75 db                	jne    801c60 <devpipe_read+0x38>
		// there's a byte.  take it.
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
  801c85:	8b 45 10             	mov    0x10(%ebp),%eax
  801c88:	eb 05                	jmp    801c8f <devpipe_read+0x67>
			// if we got any data, return it
			if (i > 0)
				return i;
			// if all the writers are gone, note eof
			if (_pipeisclosed(fd, p))
				return 0;
  801c8a:	b8 00 00 00 00       	mov    $0x0,%eax
		// wait to increment rpos until the byte is taken!
		buf[i] = p->p_buf[p->p_rpos % PIPEBUFSIZ];
		p->p_rpos++;
	}
	return i;
}
  801c8f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801c92:	5b                   	pop    %ebx
  801c93:	5e                   	pop    %esi
  801c94:	5f                   	pop    %edi
  801c95:	5d                   	pop    %ebp
  801c96:	c3                   	ret    

00801c97 <pipe>:
	uint8_t p_buf[PIPEBUFSIZ];	// data buffer
};

int
pipe(int pfd[2])
{
  801c97:	55                   	push   %ebp
  801c98:	89 e5                	mov    %esp,%ebp
  801c9a:	56                   	push   %esi
  801c9b:	53                   	push   %ebx
  801c9c:	83 ec 1c             	sub    $0x1c,%esp
	int r;
	struct Fd *fd0, *fd1;
	void *va;

	// allocate the file descriptor table entries
	if ((r = fd_alloc(&fd0)) < 0
  801c9f:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ca2:	50                   	push   %eax
  801ca3:	e8 72 f6 ff ff       	call   80131a <fd_alloc>
  801ca8:	83 c4 10             	add    $0x10,%esp
  801cab:	85 c0                	test   %eax,%eax
  801cad:	0f 88 2a 01 00 00    	js     801ddd <pipe+0x146>
	    || (r = sys_page_alloc(0, fd0, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801cb3:	83 ec 04             	sub    $0x4,%esp
  801cb6:	68 07 04 00 00       	push   $0x407
  801cbb:	ff 75 f4             	pushl  -0xc(%ebp)
  801cbe:	6a 00                	push   $0x0
  801cc0:	e8 37 ee ff ff       	call   800afc <sys_page_alloc>
  801cc5:	83 c4 10             	add    $0x10,%esp
  801cc8:	85 c0                	test   %eax,%eax
  801cca:	0f 88 0d 01 00 00    	js     801ddd <pipe+0x146>
		goto err;

	if ((r = fd_alloc(&fd1)) < 0
  801cd0:	83 ec 0c             	sub    $0xc,%esp
  801cd3:	8d 45 f0             	lea    -0x10(%ebp),%eax
  801cd6:	50                   	push   %eax
  801cd7:	e8 3e f6 ff ff       	call   80131a <fd_alloc>
  801cdc:	89 c3                	mov    %eax,%ebx
  801cde:	83 c4 10             	add    $0x10,%esp
  801ce1:	85 c0                	test   %eax,%eax
  801ce3:	0f 88 e2 00 00 00    	js     801dcb <pipe+0x134>
	    || (r = sys_page_alloc(0, fd1, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801ce9:	83 ec 04             	sub    $0x4,%esp
  801cec:	68 07 04 00 00       	push   $0x407
  801cf1:	ff 75 f0             	pushl  -0x10(%ebp)
  801cf4:	6a 00                	push   $0x0
  801cf6:	e8 01 ee ff ff       	call   800afc <sys_page_alloc>
  801cfb:	89 c3                	mov    %eax,%ebx
  801cfd:	83 c4 10             	add    $0x10,%esp
  801d00:	85 c0                	test   %eax,%eax
  801d02:	0f 88 c3 00 00 00    	js     801dcb <pipe+0x134>
		goto err1;

	// allocate the pipe structure as first data page in both
	va = fd2data(fd0);
  801d08:	83 ec 0c             	sub    $0xc,%esp
  801d0b:	ff 75 f4             	pushl  -0xc(%ebp)
  801d0e:	e8 f0 f5 ff ff       	call   801303 <fd2data>
  801d13:	89 c6                	mov    %eax,%esi
	if ((r = sys_page_alloc(0, va, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801d15:	83 c4 0c             	add    $0xc,%esp
  801d18:	68 07 04 00 00       	push   $0x407
  801d1d:	50                   	push   %eax
  801d1e:	6a 00                	push   $0x0
  801d20:	e8 d7 ed ff ff       	call   800afc <sys_page_alloc>
  801d25:	89 c3                	mov    %eax,%ebx
  801d27:	83 c4 10             	add    $0x10,%esp
  801d2a:	85 c0                	test   %eax,%eax
  801d2c:	0f 88 89 00 00 00    	js     801dbb <pipe+0x124>
		goto err2;
	if ((r = sys_page_map(0, va, 0, fd2data(fd1), PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0)
  801d32:	83 ec 0c             	sub    $0xc,%esp
  801d35:	ff 75 f0             	pushl  -0x10(%ebp)
  801d38:	e8 c6 f5 ff ff       	call   801303 <fd2data>
  801d3d:	c7 04 24 07 04 00 00 	movl   $0x407,(%esp)
  801d44:	50                   	push   %eax
  801d45:	6a 00                	push   $0x0
  801d47:	56                   	push   %esi
  801d48:	6a 00                	push   $0x0
  801d4a:	e8 f0 ed ff ff       	call   800b3f <sys_page_map>
  801d4f:	89 c3                	mov    %eax,%ebx
  801d51:	83 c4 20             	add    $0x20,%esp
  801d54:	85 c0                	test   %eax,%eax
  801d56:	78 55                	js     801dad <pipe+0x116>
		goto err3;

	// set up fd structures
	fd0->fd_dev_id = devpipe.dev_id;
  801d58:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801d5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801d61:	89 10                	mov    %edx,(%eax)
	fd0->fd_omode = O_RDONLY;
  801d63:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801d66:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)

	fd1->fd_dev_id = devpipe.dev_id;
  801d6d:	8b 15 20 30 80 00    	mov    0x803020,%edx
  801d73:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801d76:	89 10                	mov    %edx,(%eax)
	fd1->fd_omode = O_WRONLY;
  801d78:	8b 45 f0             	mov    -0x10(%ebp),%eax
  801d7b:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)

	if (debug)
		cprintf("[%08x] pipecreate %08x\n", thisenv->env_id, uvpt[PGNUM(va)]);

	pfd[0] = fd2num(fd0);
  801d82:	83 ec 0c             	sub    $0xc,%esp
  801d85:	ff 75 f4             	pushl  -0xc(%ebp)
  801d88:	e8 66 f5 ff ff       	call   8012f3 <fd2num>
  801d8d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801d90:	89 01                	mov    %eax,(%ecx)
	pfd[1] = fd2num(fd1);
  801d92:	83 c4 04             	add    $0x4,%esp
  801d95:	ff 75 f0             	pushl  -0x10(%ebp)
  801d98:	e8 56 f5 ff ff       	call   8012f3 <fd2num>
  801d9d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  801da0:	89 41 04             	mov    %eax,0x4(%ecx)
	return 0;
  801da3:	83 c4 10             	add    $0x10,%esp
  801da6:	b8 00 00 00 00       	mov    $0x0,%eax
  801dab:	eb 30                	jmp    801ddd <pipe+0x146>

    err3:
	sys_page_unmap(0, va);
  801dad:	83 ec 08             	sub    $0x8,%esp
  801db0:	56                   	push   %esi
  801db1:	6a 00                	push   $0x0
  801db3:	e8 c9 ed ff ff       	call   800b81 <sys_page_unmap>
  801db8:	83 c4 10             	add    $0x10,%esp
    err2:
	sys_page_unmap(0, fd1);
  801dbb:	83 ec 08             	sub    $0x8,%esp
  801dbe:	ff 75 f0             	pushl  -0x10(%ebp)
  801dc1:	6a 00                	push   $0x0
  801dc3:	e8 b9 ed ff ff       	call   800b81 <sys_page_unmap>
  801dc8:	83 c4 10             	add    $0x10,%esp
    err1:
	sys_page_unmap(0, fd0);
  801dcb:	83 ec 08             	sub    $0x8,%esp
  801dce:	ff 75 f4             	pushl  -0xc(%ebp)
  801dd1:	6a 00                	push   $0x0
  801dd3:	e8 a9 ed ff ff       	call   800b81 <sys_page_unmap>
  801dd8:	83 c4 10             	add    $0x10,%esp
  801ddb:	89 d8                	mov    %ebx,%eax
    err:
	return r;
}
  801ddd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801de0:	5b                   	pop    %ebx
  801de1:	5e                   	pop    %esi
  801de2:	5d                   	pop    %ebp
  801de3:	c3                   	ret    

00801de4 <pipeisclosed>:
	}
}

int
pipeisclosed(int fdnum)
{
  801de4:	55                   	push   %ebp
  801de5:	89 e5                	mov    %esp,%ebp
  801de7:	83 ec 20             	sub    $0x20,%esp
	struct Fd *fd;
	struct Pipe *p;
	int r;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801dea:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801ded:	50                   	push   %eax
  801dee:	ff 75 08             	pushl  0x8(%ebp)
  801df1:	e8 73 f5 ff ff       	call   801369 <fd_lookup>
  801df6:	83 c4 10             	add    $0x10,%esp
  801df9:	85 c0                	test   %eax,%eax
  801dfb:	78 18                	js     801e15 <pipeisclosed+0x31>
		return r;
	p = (struct Pipe*) fd2data(fd);
  801dfd:	83 ec 0c             	sub    $0xc,%esp
  801e00:	ff 75 f4             	pushl  -0xc(%ebp)
  801e03:	e8 fb f4 ff ff       	call   801303 <fd2data>
	return _pipeisclosed(fd, p);
  801e08:	89 c2                	mov    %eax,%edx
  801e0a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801e0d:	e8 3a fd ff ff       	call   801b4c <_pipeisclosed>
  801e12:	83 c4 10             	add    $0x10,%esp
}
  801e15:	c9                   	leave  
  801e16:	c3                   	ret    

00801e17 <devcons_close>:
	return tot;
}

static int
devcons_close(struct Fd *fd)
{
  801e17:	55                   	push   %ebp
  801e18:	89 e5                	mov    %esp,%ebp
	USED(fd);

	return 0;
}
  801e1a:	b8 00 00 00 00       	mov    $0x0,%eax
  801e1f:	5d                   	pop    %ebp
  801e20:	c3                   	ret    

00801e21 <devcons_stat>:

static int
devcons_stat(struct Fd *fd, struct Stat *stat)
{
  801e21:	55                   	push   %ebp
  801e22:	89 e5                	mov    %esp,%ebp
  801e24:	83 ec 10             	sub    $0x10,%esp
	strcpy(stat->st_name, "<cons>");
  801e27:	68 81 28 80 00       	push   $0x802881
  801e2c:	ff 75 0c             	pushl  0xc(%ebp)
  801e2f:	e8 e4 e8 ff ff       	call   800718 <strcpy>
	return 0;
}
  801e34:	b8 00 00 00 00       	mov    $0x0,%eax
  801e39:	c9                   	leave  
  801e3a:	c3                   	ret    

00801e3b <devcons_write>:
	return 1;
}

static ssize_t
devcons_write(struct Fd *fd, const void *vbuf, size_t n)
{
  801e3b:	55                   	push   %ebp
  801e3c:	89 e5                	mov    %esp,%ebp
  801e3e:	57                   	push   %edi
  801e3f:	56                   	push   %esi
  801e40:	53                   	push   %ebx
  801e41:	81 ec 8c 00 00 00    	sub    $0x8c,%esp
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801e47:	be 00 00 00 00       	mov    $0x0,%esi
		m = n - tot;
		if (m > sizeof(buf) - 1)
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
  801e4c:	8d bd 68 ff ff ff    	lea    -0x98(%ebp),%edi
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801e52:	eb 2c                	jmp    801e80 <devcons_write+0x45>
		m = n - tot;
  801e54:	8b 5d 10             	mov    0x10(%ebp),%ebx
  801e57:	29 f3                	sub    %esi,%ebx
		if (m > sizeof(buf) - 1)
  801e59:	83 fb 7f             	cmp    $0x7f,%ebx
  801e5c:	76 05                	jbe    801e63 <devcons_write+0x28>
			m = sizeof(buf) - 1;
  801e5e:	bb 7f 00 00 00       	mov    $0x7f,%ebx
		memmove(buf, (char*)vbuf + tot, m);
  801e63:	83 ec 04             	sub    $0x4,%esp
  801e66:	53                   	push   %ebx
  801e67:	03 45 0c             	add    0xc(%ebp),%eax
  801e6a:	50                   	push   %eax
  801e6b:	57                   	push   %edi
  801e6c:	e8 1c ea ff ff       	call   80088d <memmove>
		sys_cputs(buf, m);
  801e71:	83 c4 08             	add    $0x8,%esp
  801e74:	53                   	push   %ebx
  801e75:	57                   	push   %edi
  801e76:	e8 c5 eb ff ff       	call   800a40 <sys_cputs>
	int tot, m;
	char buf[128];

	// mistake: have to nul-terminate arg to sys_cputs,
	// so we have to copy vbuf into buf in chunks and nul-terminate.
	for (tot = 0; tot < n; tot += m) {
  801e7b:	01 de                	add    %ebx,%esi
  801e7d:	83 c4 10             	add    $0x10,%esp
  801e80:	89 f0                	mov    %esi,%eax
  801e82:	3b 75 10             	cmp    0x10(%ebp),%esi
  801e85:	72 cd                	jb     801e54 <devcons_write+0x19>
			m = sizeof(buf) - 1;
		memmove(buf, (char*)vbuf + tot, m);
		sys_cputs(buf, m);
	}
	return tot;
}
  801e87:	8d 65 f4             	lea    -0xc(%ebp),%esp
  801e8a:	5b                   	pop    %ebx
  801e8b:	5e                   	pop    %esi
  801e8c:	5f                   	pop    %edi
  801e8d:	5d                   	pop    %ebp
  801e8e:	c3                   	ret    

00801e8f <devcons_read>:
	return fd2num(fd);
}

static ssize_t
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
  801e8f:	55                   	push   %ebp
  801e90:	89 e5                	mov    %esp,%ebp
  801e92:	83 ec 08             	sub    $0x8,%esp
	int c;

	if (n == 0)
  801e95:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  801e99:	75 07                	jne    801ea2 <devcons_read+0x13>
  801e9b:	eb 23                	jmp    801ec0 <devcons_read+0x31>
		return 0;

	while ((c = sys_cgetc()) == 0)
		sys_yield();
  801e9d:	e8 3b ec ff ff       	call   800add <sys_yield>
	int c;

	if (n == 0)
		return 0;

	while ((c = sys_cgetc()) == 0)
  801ea2:	e8 b7 eb ff ff       	call   800a5e <sys_cgetc>
  801ea7:	85 c0                	test   %eax,%eax
  801ea9:	74 f2                	je     801e9d <devcons_read+0xe>
		sys_yield();
	if (c < 0)
  801eab:	85 c0                	test   %eax,%eax
  801ead:	78 1d                	js     801ecc <devcons_read+0x3d>
		return c;
	if (c == 0x04)	// ctl-d is eof
  801eaf:	83 f8 04             	cmp    $0x4,%eax
  801eb2:	74 13                	je     801ec7 <devcons_read+0x38>
		return 0;
	*(char*)vbuf = c;
  801eb4:	8b 55 0c             	mov    0xc(%ebp),%edx
  801eb7:	88 02                	mov    %al,(%edx)
	return 1;
  801eb9:	b8 01 00 00 00       	mov    $0x1,%eax
  801ebe:	eb 0c                	jmp    801ecc <devcons_read+0x3d>
devcons_read(struct Fd *fd, void *vbuf, size_t n)
{
	int c;

	if (n == 0)
		return 0;
  801ec0:	b8 00 00 00 00       	mov    $0x0,%eax
  801ec5:	eb 05                	jmp    801ecc <devcons_read+0x3d>
	while ((c = sys_cgetc()) == 0)
		sys_yield();
	if (c < 0)
		return c;
	if (c == 0x04)	// ctl-d is eof
		return 0;
  801ec7:	b8 00 00 00 00       	mov    $0x0,%eax
	*(char*)vbuf = c;
	return 1;
}
  801ecc:	c9                   	leave  
  801ecd:	c3                   	ret    

00801ece <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801ece:	55                   	push   %ebp
  801ecf:	89 e5                	mov    %esp,%ebp
  801ed1:	83 ec 20             	sub    $0x20,%esp
	char c = ch;
  801ed4:	8b 45 08             	mov    0x8(%ebp),%eax
  801ed7:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801eda:	6a 01                	push   $0x1
  801edc:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801edf:	50                   	push   %eax
  801ee0:	e8 5b eb ff ff       	call   800a40 <sys_cputs>
}
  801ee5:	83 c4 10             	add    $0x10,%esp
  801ee8:	c9                   	leave  
  801ee9:	c3                   	ret    

00801eea <getchar>:

int
getchar(void)
{
  801eea:	55                   	push   %ebp
  801eeb:	89 e5                	mov    %esp,%ebp
  801eed:	83 ec 1c             	sub    $0x1c,%esp
	int r;

	// JOS does, however, support standard _input_ redirection,
	// allowing the user to redirect script files to the shell and such.
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
  801ef0:	6a 01                	push   $0x1
  801ef2:	8d 45 f7             	lea    -0x9(%ebp),%eax
  801ef5:	50                   	push   %eax
  801ef6:	6a 00                	push   $0x0
  801ef8:	e8 d2 f6 ff ff       	call   8015cf <read>
	if (r < 0)
  801efd:	83 c4 10             	add    $0x10,%esp
  801f00:	85 c0                	test   %eax,%eax
  801f02:	78 0f                	js     801f13 <getchar+0x29>
		return r;
	if (r < 1)
  801f04:	85 c0                	test   %eax,%eax
  801f06:	7e 06                	jle    801f0e <getchar+0x24>
		return -E_EOF;
	return c;
  801f08:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  801f0c:	eb 05                	jmp    801f13 <getchar+0x29>
	// getchar() reads a character from file descriptor 0.
	r = read(0, &c, 1);
	if (r < 0)
		return r;
	if (r < 1)
		return -E_EOF;
  801f0e:	b8 f8 ff ff ff       	mov    $0xfffffff8,%eax
	return c;
}
  801f13:	c9                   	leave  
  801f14:	c3                   	ret    

00801f15 <iscons>:
	.dev_stat =	devcons_stat
};

int
iscons(int fdnum)
{
  801f15:	55                   	push   %ebp
  801f16:	89 e5                	mov    %esp,%ebp
  801f18:	83 ec 20             	sub    $0x20,%esp
	int r;
	struct Fd *fd;

	if ((r = fd_lookup(fdnum, &fd)) < 0)
  801f1b:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801f1e:	50                   	push   %eax
  801f1f:	ff 75 08             	pushl  0x8(%ebp)
  801f22:	e8 42 f4 ff ff       	call   801369 <fd_lookup>
  801f27:	83 c4 10             	add    $0x10,%esp
  801f2a:	85 c0                	test   %eax,%eax
  801f2c:	78 11                	js     801f3f <iscons+0x2a>
		return r;
	return fd->fd_dev_id == devcons.dev_id;
  801f2e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801f31:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801f37:	39 10                	cmp    %edx,(%eax)
  801f39:	0f 94 c0             	sete   %al
  801f3c:	0f b6 c0             	movzbl %al,%eax
}
  801f3f:	c9                   	leave  
  801f40:	c3                   	ret    

00801f41 <opencons>:

int
opencons(void)
{
  801f41:	55                   	push   %ebp
  801f42:	89 e5                	mov    %esp,%ebp
  801f44:	83 ec 24             	sub    $0x24,%esp
	int r;
	struct Fd* fd;

	if ((r = fd_alloc(&fd)) < 0)
  801f47:	8d 45 f4             	lea    -0xc(%ebp),%eax
  801f4a:	50                   	push   %eax
  801f4b:	e8 ca f3 ff ff       	call   80131a <fd_alloc>
  801f50:	83 c4 10             	add    $0x10,%esp
  801f53:	85 c0                	test   %eax,%eax
  801f55:	78 3a                	js     801f91 <opencons+0x50>
		return r;
	if ((r = sys_page_alloc(0, fd, PTE_P|PTE_U|PTE_W|PTE_SHARE)) < 0)
  801f57:	83 ec 04             	sub    $0x4,%esp
  801f5a:	68 07 04 00 00       	push   $0x407
  801f5f:	ff 75 f4             	pushl  -0xc(%ebp)
  801f62:	6a 00                	push   $0x0
  801f64:	e8 93 eb ff ff       	call   800afc <sys_page_alloc>
  801f69:	83 c4 10             	add    $0x10,%esp
  801f6c:	85 c0                	test   %eax,%eax
  801f6e:	78 21                	js     801f91 <opencons+0x50>
		return r;
	fd->fd_dev_id = devcons.dev_id;
  801f70:	8b 15 3c 30 80 00    	mov    0x80303c,%edx
  801f76:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801f79:	89 10                	mov    %edx,(%eax)
	fd->fd_omode = O_RDWR;
  801f7b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  801f7e:	c7 40 08 02 00 00 00 	movl   $0x2,0x8(%eax)
	return fd2num(fd);
  801f85:	83 ec 0c             	sub    $0xc,%esp
  801f88:	50                   	push   %eax
  801f89:	e8 65 f3 ff ff       	call   8012f3 <fd2num>
  801f8e:	83 c4 10             	add    $0x10,%esp
}
  801f91:	c9                   	leave  
  801f92:	c3                   	ret    

00801f93 <ipc_recv>:
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
  801f93:	55                   	push   %ebp
  801f94:	89 e5                	mov    %esp,%ebp
  801f96:	56                   	push   %esi
  801f97:	53                   	push   %ebx
  801f98:	8b 75 08             	mov    0x8(%ebp),%esi
  801f9b:	8b 45 0c             	mov    0xc(%ebp),%eax
  801f9e:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// LAB 4: Your code here.
	
    if (!pg)
  801fa1:	85 c0                	test   %eax,%eax
  801fa3:	75 05                	jne    801faa <ipc_recv+0x17>
        pg = (void *)UTOP;
  801fa5:	b8 00 00 c0 ee       	mov    $0xeec00000,%eax

    int result;
    if ((result = sys_ipc_recv(pg))) {
  801faa:	83 ec 0c             	sub    $0xc,%esp
  801fad:	50                   	push   %eax
  801fae:	e8 18 ed ff ff       	call   800ccb <sys_ipc_recv>
  801fb3:	83 c4 10             	add    $0x10,%esp
  801fb6:	85 c0                	test   %eax,%eax
  801fb8:	74 16                	je     801fd0 <ipc_recv+0x3d>
        if (from_env_store)
  801fba:	85 f6                	test   %esi,%esi
  801fbc:	74 06                	je     801fc4 <ipc_recv+0x31>
            *from_env_store = 0;
  801fbe:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
        if (perm_store)
  801fc4:	85 db                	test   %ebx,%ebx
  801fc6:	74 2c                	je     801ff4 <ipc_recv+0x61>
            *perm_store = 0;
  801fc8:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  801fce:	eb 24                	jmp    801ff4 <ipc_recv+0x61>
            
        return result;
    }

    if (from_env_store){
  801fd0:	85 f6                	test   %esi,%esi
  801fd2:	74 0a                	je     801fde <ipc_recv+0x4b>
        *from_env_store = thisenv->env_ipc_from;
  801fd4:	a1 04 40 80 00       	mov    0x804004,%eax
  801fd9:	8b 40 74             	mov    0x74(%eax),%eax
  801fdc:	89 06                	mov    %eax,(%esi)

    }
    if (perm_store)
  801fde:	85 db                	test   %ebx,%ebx
  801fe0:	74 0a                	je     801fec <ipc_recv+0x59>
        *perm_store = thisenv->env_ipc_perm;
  801fe2:	a1 04 40 80 00       	mov    0x804004,%eax
  801fe7:	8b 40 78             	mov    0x78(%eax),%eax
  801fea:	89 03                	mov    %eax,(%ebx)
		cprintf("env not runable\n");
	}else{
		cprintf("env runable\n");
	}*/

	return thisenv->env_ipc_value;
  801fec:	a1 04 40 80 00       	mov    0x804004,%eax
  801ff1:	8b 40 70             	mov    0x70(%eax),%eax
	//panic("ipc_recv not implemented");
	//return 0;
}
  801ff4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801ff7:	5b                   	pop    %ebx
  801ff8:	5e                   	pop    %esi
  801ff9:	5d                   	pop    %ebp
  801ffa:	c3                   	ret    

00801ffb <ipc_send>:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
  801ffb:	55                   	push   %ebp
  801ffc:	89 e5                	mov    %esp,%ebp
  801ffe:	57                   	push   %edi
  801fff:	56                   	push   %esi
  802000:	53                   	push   %ebx
  802001:	83 ec 0c             	sub    $0xc,%esp
  802004:	8b 75 0c             	mov    0xc(%ebp),%esi
  802007:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80200a:	8b 7d 14             	mov    0x14(%ebp),%edi
	// LAB 4: Your code here.
	if (!pg){
  80200d:	85 db                	test   %ebx,%ebx
  80200f:	75 0c                	jne    80201d <ipc_send+0x22>
        pg = (void *)UTOP;
  802011:	bb 00 00 c0 ee       	mov    $0xeec00000,%ebx
  802016:	eb 05                	jmp    80201d <ipc_send+0x22>
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
        sys_yield();
  802018:	e8 c0 ea ff ff       	call   800add <sys_yield>
        pg = (void *)UTOP;
    }

    int result;
    //cprintf("ipc_send() val is %d\n", val);
    while (-E_IPC_NOT_RECV == (result = sys_ipc_try_send(to_env, val, pg, perm))){
  80201d:	57                   	push   %edi
  80201e:	53                   	push   %ebx
  80201f:	56                   	push   %esi
  802020:	ff 75 08             	pushl  0x8(%ebp)
  802023:	e8 80 ec ff ff       	call   800ca8 <sys_ipc_try_send>
  802028:	83 c4 10             	add    $0x10,%esp
  80202b:	83 f8 f9             	cmp    $0xfffffff9,%eax
  80202e:	74 e8                	je     802018 <ipc_send+0x1d>
        sys_yield();
    }

    if (result){
  802030:	85 c0                	test   %eax,%eax
  802032:	74 14                	je     802048 <ipc_send+0x4d>
        panic("ipc_send: error");
  802034:	83 ec 04             	sub    $0x4,%esp
  802037:	68 8d 28 80 00       	push   $0x80288d
  80203c:	6a 6a                	push   $0x6a
  80203e:	68 9d 28 80 00       	push   $0x80289d
  802043:	e8 94 e0 ff ff       	call   8000dc <_panic>
    }
	//panic("ipc_send not implemented");
}
  802048:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80204b:	5b                   	pop    %ebx
  80204c:	5e                   	pop    %esi
  80204d:	5f                   	pop    %edi
  80204e:	5d                   	pop    %ebp
  80204f:	c3                   	ret    

00802050 <ipc_find_env>:
// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
  802050:	55                   	push   %ebp
  802051:	89 e5                	mov    %esp,%ebp
  802053:	53                   	push   %ebx
  802054:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int i;
	for (i = 0; i < NENV; i++)
  802057:	ba 00 00 00 00       	mov    $0x0,%edx
		if (envs[i].env_type == type)
  80205c:	8d 1c 95 00 00 00 00 	lea    0x0(,%edx,4),%ebx
  802063:	89 d0                	mov    %edx,%eax
  802065:	c1 e0 07             	shl    $0x7,%eax
  802068:	29 d8                	sub    %ebx,%eax
  80206a:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80206f:	8b 40 50             	mov    0x50(%eax),%eax
  802072:	39 c8                	cmp    %ecx,%eax
  802074:	75 0d                	jne    802083 <ipc_find_env+0x33>
			return envs[i].env_id;
  802076:	c1 e2 07             	shl    $0x7,%edx
  802079:	29 da                	sub    %ebx,%edx
  80207b:	8b 82 48 00 c0 ee    	mov    -0x113fffb8(%edx),%eax
  802081:	eb 0e                	jmp    802091 <ipc_find_env+0x41>
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
  802083:	42                   	inc    %edx
  802084:	81 fa 00 04 00 00    	cmp    $0x400,%edx
  80208a:	75 d0                	jne    80205c <ipc_find_env+0xc>
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
  80208c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  802091:	5b                   	pop    %ebx
  802092:	5d                   	pop    %ebp
  802093:	c3                   	ret    

00802094 <pageref>:
#include <inc/lib.h>

int
pageref(void *v)
{
  802094:	55                   	push   %ebp
  802095:	89 e5                	mov    %esp,%ebp
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
  802097:	8b 45 08             	mov    0x8(%ebp),%eax
  80209a:	c1 e8 16             	shr    $0x16,%eax
  80209d:	8b 04 85 00 d0 7b ef 	mov    -0x10843000(,%eax,4),%eax
  8020a4:	a8 01                	test   $0x1,%al
  8020a6:	74 21                	je     8020c9 <pageref+0x35>
		return 0;
	pte = uvpt[PGNUM(v)];
  8020a8:	8b 45 08             	mov    0x8(%ebp),%eax
  8020ab:	c1 e8 0c             	shr    $0xc,%eax
  8020ae:	8b 04 85 00 00 40 ef 	mov    -0x10c00000(,%eax,4),%eax
	if (!(pte & PTE_P))
  8020b5:	a8 01                	test   $0x1,%al
  8020b7:	74 17                	je     8020d0 <pageref+0x3c>
		return 0;
	return pages[PGNUM(pte)].pp_ref;
  8020b9:	c1 e8 0c             	shr    $0xc,%eax
  8020bc:	66 8b 04 c5 04 00 00 	mov    -0x10fffffc(,%eax,8),%ax
  8020c3:	ef 
  8020c4:	0f b7 c0             	movzwl %ax,%eax
  8020c7:	eb 0c                	jmp    8020d5 <pageref+0x41>
pageref(void *v)
{
	pte_t pte;

	if (!(uvpd[PDX(v)] & PTE_P))
		return 0;
  8020c9:	b8 00 00 00 00       	mov    $0x0,%eax
  8020ce:	eb 05                	jmp    8020d5 <pageref+0x41>
	pte = uvpt[PGNUM(v)];
	if (!(pte & PTE_P))
		return 0;
  8020d0:	b8 00 00 00 00       	mov    $0x0,%eax
	return pages[PGNUM(pte)].pp_ref;
}
  8020d5:	5d                   	pop    %ebp
  8020d6:	c3                   	ret    
  8020d7:	90                   	nop

008020d8 <__udivdi3>:
  8020d8:	55                   	push   %ebp
  8020d9:	57                   	push   %edi
  8020da:	56                   	push   %esi
  8020db:	53                   	push   %ebx
  8020dc:	83 ec 1c             	sub    $0x1c,%esp
  8020df:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8020e3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8020e7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8020eb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8020ef:	89 ca                	mov    %ecx,%edx
  8020f1:	89 f8                	mov    %edi,%eax
  8020f3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8020f7:	85 f6                	test   %esi,%esi
  8020f9:	75 2d                	jne    802128 <__udivdi3+0x50>
  8020fb:	39 cf                	cmp    %ecx,%edi
  8020fd:	77 65                	ja     802164 <__udivdi3+0x8c>
  8020ff:	89 fd                	mov    %edi,%ebp
  802101:	85 ff                	test   %edi,%edi
  802103:	75 0b                	jne    802110 <__udivdi3+0x38>
  802105:	b8 01 00 00 00       	mov    $0x1,%eax
  80210a:	31 d2                	xor    %edx,%edx
  80210c:	f7 f7                	div    %edi
  80210e:	89 c5                	mov    %eax,%ebp
  802110:	31 d2                	xor    %edx,%edx
  802112:	89 c8                	mov    %ecx,%eax
  802114:	f7 f5                	div    %ebp
  802116:	89 c1                	mov    %eax,%ecx
  802118:	89 d8                	mov    %ebx,%eax
  80211a:	f7 f5                	div    %ebp
  80211c:	89 cf                	mov    %ecx,%edi
  80211e:	89 fa                	mov    %edi,%edx
  802120:	83 c4 1c             	add    $0x1c,%esp
  802123:	5b                   	pop    %ebx
  802124:	5e                   	pop    %esi
  802125:	5f                   	pop    %edi
  802126:	5d                   	pop    %ebp
  802127:	c3                   	ret    
  802128:	39 ce                	cmp    %ecx,%esi
  80212a:	77 28                	ja     802154 <__udivdi3+0x7c>
  80212c:	0f bd fe             	bsr    %esi,%edi
  80212f:	83 f7 1f             	xor    $0x1f,%edi
  802132:	75 40                	jne    802174 <__udivdi3+0x9c>
  802134:	39 ce                	cmp    %ecx,%esi
  802136:	72 0a                	jb     802142 <__udivdi3+0x6a>
  802138:	3b 44 24 08          	cmp    0x8(%esp),%eax
  80213c:	0f 87 9e 00 00 00    	ja     8021e0 <__udivdi3+0x108>
  802142:	b8 01 00 00 00       	mov    $0x1,%eax
  802147:	89 fa                	mov    %edi,%edx
  802149:	83 c4 1c             	add    $0x1c,%esp
  80214c:	5b                   	pop    %ebx
  80214d:	5e                   	pop    %esi
  80214e:	5f                   	pop    %edi
  80214f:	5d                   	pop    %ebp
  802150:	c3                   	ret    
  802151:	8d 76 00             	lea    0x0(%esi),%esi
  802154:	31 ff                	xor    %edi,%edi
  802156:	31 c0                	xor    %eax,%eax
  802158:	89 fa                	mov    %edi,%edx
  80215a:	83 c4 1c             	add    $0x1c,%esp
  80215d:	5b                   	pop    %ebx
  80215e:	5e                   	pop    %esi
  80215f:	5f                   	pop    %edi
  802160:	5d                   	pop    %ebp
  802161:	c3                   	ret    
  802162:	66 90                	xchg   %ax,%ax
  802164:	89 d8                	mov    %ebx,%eax
  802166:	f7 f7                	div    %edi
  802168:	31 ff                	xor    %edi,%edi
  80216a:	89 fa                	mov    %edi,%edx
  80216c:	83 c4 1c             	add    $0x1c,%esp
  80216f:	5b                   	pop    %ebx
  802170:	5e                   	pop    %esi
  802171:	5f                   	pop    %edi
  802172:	5d                   	pop    %ebp
  802173:	c3                   	ret    
  802174:	bd 20 00 00 00       	mov    $0x20,%ebp
  802179:	89 eb                	mov    %ebp,%ebx
  80217b:	29 fb                	sub    %edi,%ebx
  80217d:	89 f9                	mov    %edi,%ecx
  80217f:	d3 e6                	shl    %cl,%esi
  802181:	89 c5                	mov    %eax,%ebp
  802183:	88 d9                	mov    %bl,%cl
  802185:	d3 ed                	shr    %cl,%ebp
  802187:	89 e9                	mov    %ebp,%ecx
  802189:	09 f1                	or     %esi,%ecx
  80218b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80218f:	89 f9                	mov    %edi,%ecx
  802191:	d3 e0                	shl    %cl,%eax
  802193:	89 c5                	mov    %eax,%ebp
  802195:	89 d6                	mov    %edx,%esi
  802197:	88 d9                	mov    %bl,%cl
  802199:	d3 ee                	shr    %cl,%esi
  80219b:	89 f9                	mov    %edi,%ecx
  80219d:	d3 e2                	shl    %cl,%edx
  80219f:	8b 44 24 08          	mov    0x8(%esp),%eax
  8021a3:	88 d9                	mov    %bl,%cl
  8021a5:	d3 e8                	shr    %cl,%eax
  8021a7:	09 c2                	or     %eax,%edx
  8021a9:	89 d0                	mov    %edx,%eax
  8021ab:	89 f2                	mov    %esi,%edx
  8021ad:	f7 74 24 0c          	divl   0xc(%esp)
  8021b1:	89 d6                	mov    %edx,%esi
  8021b3:	89 c3                	mov    %eax,%ebx
  8021b5:	f7 e5                	mul    %ebp
  8021b7:	39 d6                	cmp    %edx,%esi
  8021b9:	72 19                	jb     8021d4 <__udivdi3+0xfc>
  8021bb:	74 0b                	je     8021c8 <__udivdi3+0xf0>
  8021bd:	89 d8                	mov    %ebx,%eax
  8021bf:	31 ff                	xor    %edi,%edi
  8021c1:	e9 58 ff ff ff       	jmp    80211e <__udivdi3+0x46>
  8021c6:	66 90                	xchg   %ax,%ax
  8021c8:	8b 54 24 08          	mov    0x8(%esp),%edx
  8021cc:	89 f9                	mov    %edi,%ecx
  8021ce:	d3 e2                	shl    %cl,%edx
  8021d0:	39 c2                	cmp    %eax,%edx
  8021d2:	73 e9                	jae    8021bd <__udivdi3+0xe5>
  8021d4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  8021d7:	31 ff                	xor    %edi,%edi
  8021d9:	e9 40 ff ff ff       	jmp    80211e <__udivdi3+0x46>
  8021de:	66 90                	xchg   %ax,%ax
  8021e0:	31 c0                	xor    %eax,%eax
  8021e2:	e9 37 ff ff ff       	jmp    80211e <__udivdi3+0x46>
  8021e7:	90                   	nop

008021e8 <__umoddi3>:
  8021e8:	55                   	push   %ebp
  8021e9:	57                   	push   %edi
  8021ea:	56                   	push   %esi
  8021eb:	53                   	push   %ebx
  8021ec:	83 ec 1c             	sub    $0x1c,%esp
  8021ef:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8021f3:	8b 74 24 34          	mov    0x34(%esp),%esi
  8021f7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8021fb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  8021ff:	89 44 24 0c          	mov    %eax,0xc(%esp)
  802203:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  802207:	89 f3                	mov    %esi,%ebx
  802209:	89 fa                	mov    %edi,%edx
  80220b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80220f:	89 34 24             	mov    %esi,(%esp)
  802212:	85 c0                	test   %eax,%eax
  802214:	75 1a                	jne    802230 <__umoddi3+0x48>
  802216:	39 f7                	cmp    %esi,%edi
  802218:	0f 86 a2 00 00 00    	jbe    8022c0 <__umoddi3+0xd8>
  80221e:	89 c8                	mov    %ecx,%eax
  802220:	89 f2                	mov    %esi,%edx
  802222:	f7 f7                	div    %edi
  802224:	89 d0                	mov    %edx,%eax
  802226:	31 d2                	xor    %edx,%edx
  802228:	83 c4 1c             	add    $0x1c,%esp
  80222b:	5b                   	pop    %ebx
  80222c:	5e                   	pop    %esi
  80222d:	5f                   	pop    %edi
  80222e:	5d                   	pop    %ebp
  80222f:	c3                   	ret    
  802230:	39 f0                	cmp    %esi,%eax
  802232:	0f 87 ac 00 00 00    	ja     8022e4 <__umoddi3+0xfc>
  802238:	0f bd e8             	bsr    %eax,%ebp
  80223b:	83 f5 1f             	xor    $0x1f,%ebp
  80223e:	0f 84 ac 00 00 00    	je     8022f0 <__umoddi3+0x108>
  802244:	bf 20 00 00 00       	mov    $0x20,%edi
  802249:	29 ef                	sub    %ebp,%edi
  80224b:	89 fe                	mov    %edi,%esi
  80224d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  802251:	89 e9                	mov    %ebp,%ecx
  802253:	d3 e0                	shl    %cl,%eax
  802255:	89 d7                	mov    %edx,%edi
  802257:	89 f1                	mov    %esi,%ecx
  802259:	d3 ef                	shr    %cl,%edi
  80225b:	09 c7                	or     %eax,%edi
  80225d:	89 e9                	mov    %ebp,%ecx
  80225f:	d3 e2                	shl    %cl,%edx
  802261:	89 14 24             	mov    %edx,(%esp)
  802264:	89 d8                	mov    %ebx,%eax
  802266:	d3 e0                	shl    %cl,%eax
  802268:	89 c2                	mov    %eax,%edx
  80226a:	8b 44 24 08          	mov    0x8(%esp),%eax
  80226e:	d3 e0                	shl    %cl,%eax
  802270:	89 44 24 04          	mov    %eax,0x4(%esp)
  802274:	8b 44 24 08          	mov    0x8(%esp),%eax
  802278:	89 f1                	mov    %esi,%ecx
  80227a:	d3 e8                	shr    %cl,%eax
  80227c:	09 d0                	or     %edx,%eax
  80227e:	d3 eb                	shr    %cl,%ebx
  802280:	89 da                	mov    %ebx,%edx
  802282:	f7 f7                	div    %edi
  802284:	89 d3                	mov    %edx,%ebx
  802286:	f7 24 24             	mull   (%esp)
  802289:	89 c6                	mov    %eax,%esi
  80228b:	89 d1                	mov    %edx,%ecx
  80228d:	39 d3                	cmp    %edx,%ebx
  80228f:	0f 82 87 00 00 00    	jb     80231c <__umoddi3+0x134>
  802295:	0f 84 91 00 00 00    	je     80232c <__umoddi3+0x144>
  80229b:	8b 54 24 04          	mov    0x4(%esp),%edx
  80229f:	29 f2                	sub    %esi,%edx
  8022a1:	19 cb                	sbb    %ecx,%ebx
  8022a3:	89 d8                	mov    %ebx,%eax
  8022a5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  8022a9:	d3 e0                	shl    %cl,%eax
  8022ab:	89 e9                	mov    %ebp,%ecx
  8022ad:	d3 ea                	shr    %cl,%edx
  8022af:	09 d0                	or     %edx,%eax
  8022b1:	89 e9                	mov    %ebp,%ecx
  8022b3:	d3 eb                	shr    %cl,%ebx
  8022b5:	89 da                	mov    %ebx,%edx
  8022b7:	83 c4 1c             	add    $0x1c,%esp
  8022ba:	5b                   	pop    %ebx
  8022bb:	5e                   	pop    %esi
  8022bc:	5f                   	pop    %edi
  8022bd:	5d                   	pop    %ebp
  8022be:	c3                   	ret    
  8022bf:	90                   	nop
  8022c0:	89 fd                	mov    %edi,%ebp
  8022c2:	85 ff                	test   %edi,%edi
  8022c4:	75 0b                	jne    8022d1 <__umoddi3+0xe9>
  8022c6:	b8 01 00 00 00       	mov    $0x1,%eax
  8022cb:	31 d2                	xor    %edx,%edx
  8022cd:	f7 f7                	div    %edi
  8022cf:	89 c5                	mov    %eax,%ebp
  8022d1:	89 f0                	mov    %esi,%eax
  8022d3:	31 d2                	xor    %edx,%edx
  8022d5:	f7 f5                	div    %ebp
  8022d7:	89 c8                	mov    %ecx,%eax
  8022d9:	f7 f5                	div    %ebp
  8022db:	89 d0                	mov    %edx,%eax
  8022dd:	e9 44 ff ff ff       	jmp    802226 <__umoddi3+0x3e>
  8022e2:	66 90                	xchg   %ax,%ax
  8022e4:	89 c8                	mov    %ecx,%eax
  8022e6:	89 f2                	mov    %esi,%edx
  8022e8:	83 c4 1c             	add    $0x1c,%esp
  8022eb:	5b                   	pop    %ebx
  8022ec:	5e                   	pop    %esi
  8022ed:	5f                   	pop    %edi
  8022ee:	5d                   	pop    %ebp
  8022ef:	c3                   	ret    
  8022f0:	3b 04 24             	cmp    (%esp),%eax
  8022f3:	72 06                	jb     8022fb <__umoddi3+0x113>
  8022f5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  8022f9:	77 0f                	ja     80230a <__umoddi3+0x122>
  8022fb:	89 f2                	mov    %esi,%edx
  8022fd:	29 f9                	sub    %edi,%ecx
  8022ff:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  802303:	89 14 24             	mov    %edx,(%esp)
  802306:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  80230a:	8b 44 24 04          	mov    0x4(%esp),%eax
  80230e:	8b 14 24             	mov    (%esp),%edx
  802311:	83 c4 1c             	add    $0x1c,%esp
  802314:	5b                   	pop    %ebx
  802315:	5e                   	pop    %esi
  802316:	5f                   	pop    %edi
  802317:	5d                   	pop    %ebp
  802318:	c3                   	ret    
  802319:	8d 76 00             	lea    0x0(%esi),%esi
  80231c:	2b 04 24             	sub    (%esp),%eax
  80231f:	19 fa                	sbb    %edi,%edx
  802321:	89 d1                	mov    %edx,%ecx
  802323:	89 c6                	mov    %eax,%esi
  802325:	e9 71 ff ff ff       	jmp    80229b <__umoddi3+0xb3>
  80232a:	66 90                	xchg   %ax,%ax
  80232c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  802330:	72 ea                	jb     80231c <__umoddi3+0x134>
  802332:	89 d9                	mov    %ebx,%ecx
  802334:	e9 62 ff ff ff       	jmp    80229b <__umoddi3+0xb3>
