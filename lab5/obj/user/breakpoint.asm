
obj/user/breakpoint.debug:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 08 00 00 00       	call   800039 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $3"); //break point
  800036:	cc                   	int3   
}
  800037:	5d                   	pop    %ebp
  800038:	c3                   	ret    

00800039 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800039:	55                   	push   %ebp
  80003a:	89 e5                	mov    %esp,%ebp
  80003c:	56                   	push   %esi
  80003d:	53                   	push   %ebx
  80003e:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800041:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800044:	e8 cf 00 00 00       	call   800118 <sys_getenvid>
  800049:	25 ff 03 00 00       	and    $0x3ff,%eax
  80004e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800055:	c1 e0 07             	shl    $0x7,%eax
  800058:	29 d0                	sub    %edx,%eax
  80005a:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80005f:	a3 04 20 80 00       	mov    %eax,0x802004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800064:	85 db                	test   %ebx,%ebx
  800066:	7e 07                	jle    80006f <libmain+0x36>
		binaryname = argv[0];
  800068:	8b 06                	mov    (%esi),%eax
  80006a:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  80006f:	83 ec 08             	sub    $0x8,%esp
  800072:	56                   	push   %esi
  800073:	53                   	push   %ebx
  800074:	e8 ba ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800079:	e8 0a 00 00 00       	call   800088 <exit>
}
  80007e:	83 c4 10             	add    $0x10,%esp
  800081:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800084:	5b                   	pop    %ebx
  800085:	5e                   	pop    %esi
  800086:	5d                   	pop    %ebp
  800087:	c3                   	ret    

00800088 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800088:	55                   	push   %ebp
  800089:	89 e5                	mov    %esp,%ebp
  80008b:	83 ec 14             	sub    $0x14,%esp
	//close_all();
	sys_env_destroy(0);
  80008e:	6a 00                	push   $0x0
  800090:	e8 42 00 00 00       	call   8000d7 <sys_env_destroy>
}
  800095:	83 c4 10             	add    $0x10,%esp
  800098:	c9                   	leave  
  800099:	c3                   	ret    

0080009a <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009a:	55                   	push   %ebp
  80009b:	89 e5                	mov    %esp,%ebp
  80009d:	57                   	push   %edi
  80009e:	56                   	push   %esi
  80009f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a0:	b8 00 00 00 00       	mov    $0x0,%eax
  8000a5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000a8:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ab:	89 c3                	mov    %eax,%ebx
  8000ad:	89 c7                	mov    %eax,%edi
  8000af:	89 c6                	mov    %eax,%esi
  8000b1:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b3:	5b                   	pop    %ebx
  8000b4:	5e                   	pop    %esi
  8000b5:	5f                   	pop    %edi
  8000b6:	5d                   	pop    %ebp
  8000b7:	c3                   	ret    

008000b8 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000b8:	55                   	push   %ebp
  8000b9:	89 e5                	mov    %esp,%ebp
  8000bb:	57                   	push   %edi
  8000bc:	56                   	push   %esi
  8000bd:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000be:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c3:	b8 01 00 00 00       	mov    $0x1,%eax
  8000c8:	89 d1                	mov    %edx,%ecx
  8000ca:	89 d3                	mov    %edx,%ebx
  8000cc:	89 d7                	mov    %edx,%edi
  8000ce:	89 d6                	mov    %edx,%esi
  8000d0:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d2:	5b                   	pop    %ebx
  8000d3:	5e                   	pop    %esi
  8000d4:	5f                   	pop    %edi
  8000d5:	5d                   	pop    %ebp
  8000d6:	c3                   	ret    

008000d7 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000d7:	55                   	push   %ebp
  8000d8:	89 e5                	mov    %esp,%ebp
  8000da:	57                   	push   %edi
  8000db:	56                   	push   %esi
  8000dc:	53                   	push   %ebx
  8000dd:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e0:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000e5:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ea:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ed:	89 cb                	mov    %ecx,%ebx
  8000ef:	89 cf                	mov    %ecx,%edi
  8000f1:	89 ce                	mov    %ecx,%esi
  8000f3:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000f5:	85 c0                	test   %eax,%eax
  8000f7:	7e 17                	jle    800110 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000f9:	83 ec 0c             	sub    $0xc,%esp
  8000fc:	50                   	push   %eax
  8000fd:	6a 03                	push   $0x3
  8000ff:	68 4a 0f 80 00       	push   $0x800f4a
  800104:	6a 23                	push   $0x23
  800106:	68 67 0f 80 00       	push   $0x800f67
  80010b:	e8 56 02 00 00       	call   800366 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800110:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800113:	5b                   	pop    %ebx
  800114:	5e                   	pop    %esi
  800115:	5f                   	pop    %edi
  800116:	5d                   	pop    %ebp
  800117:	c3                   	ret    

00800118 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800118:	55                   	push   %ebp
  800119:	89 e5                	mov    %esp,%ebp
  80011b:	57                   	push   %edi
  80011c:	56                   	push   %esi
  80011d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80011e:	ba 00 00 00 00       	mov    $0x0,%edx
  800123:	b8 02 00 00 00       	mov    $0x2,%eax
  800128:	89 d1                	mov    %edx,%ecx
  80012a:	89 d3                	mov    %edx,%ebx
  80012c:	89 d7                	mov    %edx,%edi
  80012e:	89 d6                	mov    %edx,%esi
  800130:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800132:	5b                   	pop    %ebx
  800133:	5e                   	pop    %esi
  800134:	5f                   	pop    %edi
  800135:	5d                   	pop    %ebp
  800136:	c3                   	ret    

00800137 <sys_yield>:

void
sys_yield(void)
{
  800137:	55                   	push   %ebp
  800138:	89 e5                	mov    %esp,%ebp
  80013a:	57                   	push   %edi
  80013b:	56                   	push   %esi
  80013c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80013d:	ba 00 00 00 00       	mov    $0x0,%edx
  800142:	b8 0b 00 00 00       	mov    $0xb,%eax
  800147:	89 d1                	mov    %edx,%ecx
  800149:	89 d3                	mov    %edx,%ebx
  80014b:	89 d7                	mov    %edx,%edi
  80014d:	89 d6                	mov    %edx,%esi
  80014f:	cd 30                	int    $0x30

void
sys_yield(void)
{
	syscall(SYS_yield, 0, 0, 0, 0, 0, 0);
}
  800151:	5b                   	pop    %ebx
  800152:	5e                   	pop    %esi
  800153:	5f                   	pop    %edi
  800154:	5d                   	pop    %ebp
  800155:	c3                   	ret    

00800156 <sys_page_alloc>:

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
  800156:	55                   	push   %ebp
  800157:	89 e5                	mov    %esp,%ebp
  800159:	57                   	push   %edi
  80015a:	56                   	push   %esi
  80015b:	53                   	push   %ebx
  80015c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80015f:	be 00 00 00 00       	mov    $0x0,%esi
  800164:	b8 04 00 00 00       	mov    $0x4,%eax
  800169:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80016c:	8b 55 08             	mov    0x8(%ebp),%edx
  80016f:	8b 5d 10             	mov    0x10(%ebp),%ebx
  800172:	89 f7                	mov    %esi,%edi
  800174:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800176:	85 c0                	test   %eax,%eax
  800178:	7e 17                	jle    800191 <sys_page_alloc+0x3b>
		panic("syscall %d returned %d (> 0)", num, ret);
  80017a:	83 ec 0c             	sub    $0xc,%esp
  80017d:	50                   	push   %eax
  80017e:	6a 04                	push   $0x4
  800180:	68 4a 0f 80 00       	push   $0x800f4a
  800185:	6a 23                	push   $0x23
  800187:	68 67 0f 80 00       	push   $0x800f67
  80018c:	e8 d5 01 00 00       	call   800366 <_panic>

int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	return syscall(SYS_page_alloc, 1, envid, (uint32_t) va, perm, 0, 0);
}
  800191:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800194:	5b                   	pop    %ebx
  800195:	5e                   	pop    %esi
  800196:	5f                   	pop    %edi
  800197:	5d                   	pop    %ebp
  800198:	c3                   	ret    

00800199 <sys_page_map>:

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
  800199:	55                   	push   %ebp
  80019a:	89 e5                	mov    %esp,%ebp
  80019c:	57                   	push   %edi
  80019d:	56                   	push   %esi
  80019e:	53                   	push   %ebx
  80019f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001a2:	b8 05 00 00 00       	mov    $0x5,%eax
  8001a7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001aa:	8b 55 08             	mov    0x8(%ebp),%edx
  8001ad:	8b 5d 10             	mov    0x10(%ebp),%ebx
  8001b0:	8b 7d 14             	mov    0x14(%ebp),%edi
  8001b3:	8b 75 18             	mov    0x18(%ebp),%esi
  8001b6:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001b8:	85 c0                	test   %eax,%eax
  8001ba:	7e 17                	jle    8001d3 <sys_page_map+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001bc:	83 ec 0c             	sub    $0xc,%esp
  8001bf:	50                   	push   %eax
  8001c0:	6a 05                	push   $0x5
  8001c2:	68 4a 0f 80 00       	push   $0x800f4a
  8001c7:	6a 23                	push   $0x23
  8001c9:	68 67 0f 80 00       	push   $0x800f67
  8001ce:	e8 93 01 00 00       	call   800366 <_panic>

int
sys_page_map(envid_t srcenv, void *srcva, envid_t dstenv, void *dstva, int perm)
{
	return syscall(SYS_page_map, 1, srcenv, (uint32_t) srcva, dstenv, (uint32_t) dstva, perm);
}
  8001d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001d6:	5b                   	pop    %ebx
  8001d7:	5e                   	pop    %esi
  8001d8:	5f                   	pop    %edi
  8001d9:	5d                   	pop    %ebp
  8001da:	c3                   	ret    

008001db <sys_page_unmap>:

int
sys_page_unmap(envid_t envid, void *va)
{
  8001db:	55                   	push   %ebp
  8001dc:	89 e5                	mov    %esp,%ebp
  8001de:	57                   	push   %edi
  8001df:	56                   	push   %esi
  8001e0:	53                   	push   %ebx
  8001e1:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8001e4:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001e9:	b8 06 00 00 00       	mov    $0x6,%eax
  8001ee:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8001f1:	8b 55 08             	mov    0x8(%ebp),%edx
  8001f4:	89 df                	mov    %ebx,%edi
  8001f6:	89 de                	mov    %ebx,%esi
  8001f8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8001fa:	85 c0                	test   %eax,%eax
  8001fc:	7e 17                	jle    800215 <sys_page_unmap+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8001fe:	83 ec 0c             	sub    $0xc,%esp
  800201:	50                   	push   %eax
  800202:	6a 06                	push   $0x6
  800204:	68 4a 0f 80 00       	push   $0x800f4a
  800209:	6a 23                	push   $0x23
  80020b:	68 67 0f 80 00       	push   $0x800f67
  800210:	e8 51 01 00 00       	call   800366 <_panic>

int
sys_page_unmap(envid_t envid, void *va)
{
	return syscall(SYS_page_unmap, 1, envid, (uint32_t) va, 0, 0, 0);
}
  800215:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800218:	5b                   	pop    %ebx
  800219:	5e                   	pop    %esi
  80021a:	5f                   	pop    %edi
  80021b:	5d                   	pop    %ebp
  80021c:	c3                   	ret    

0080021d <sys_env_set_status>:

// sys_exofork is inlined in lib.h

int
sys_env_set_status(envid_t envid, int status)
{
  80021d:	55                   	push   %ebp
  80021e:	89 e5                	mov    %esp,%ebp
  800220:	57                   	push   %edi
  800221:	56                   	push   %esi
  800222:	53                   	push   %ebx
  800223:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800226:	bb 00 00 00 00       	mov    $0x0,%ebx
  80022b:	b8 08 00 00 00       	mov    $0x8,%eax
  800230:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800233:	8b 55 08             	mov    0x8(%ebp),%edx
  800236:	89 df                	mov    %ebx,%edi
  800238:	89 de                	mov    %ebx,%esi
  80023a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80023c:	85 c0                	test   %eax,%eax
  80023e:	7e 17                	jle    800257 <sys_env_set_status+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  800240:	83 ec 0c             	sub    $0xc,%esp
  800243:	50                   	push   %eax
  800244:	6a 08                	push   $0x8
  800246:	68 4a 0f 80 00       	push   $0x800f4a
  80024b:	6a 23                	push   $0x23
  80024d:	68 67 0f 80 00       	push   $0x800f67
  800252:	e8 0f 01 00 00       	call   800366 <_panic>

int
sys_env_set_status(envid_t envid, int status)
{
	return syscall(SYS_env_set_status, 1, envid, status, 0, 0, 0);
}
  800257:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80025a:	5b                   	pop    %ebx
  80025b:	5e                   	pop    %esi
  80025c:	5f                   	pop    %edi
  80025d:	5d                   	pop    %ebp
  80025e:	c3                   	ret    

0080025f <sys_time_msec>:

unsigned int
sys_time_msec(void)
{
  80025f:	55                   	push   %ebp
  800260:	89 e5                	mov    %esp,%ebp
  800262:	57                   	push   %edi
  800263:	56                   	push   %esi
  800264:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800265:	ba 00 00 00 00       	mov    $0x0,%edx
  80026a:	b8 0c 00 00 00       	mov    $0xc,%eax
  80026f:	89 d1                	mov    %edx,%ecx
  800271:	89 d3                	mov    %edx,%ebx
  800273:	89 d7                	mov    %edx,%edi
  800275:	89 d6                	mov    %edx,%esi
  800277:	cd 30                	int    $0x30

unsigned int
sys_time_msec(void)
{
	return (unsigned int) syscall(SYS_time_msec, 0, 0, 0, 0, 0, 0);
}
  800279:	5b                   	pop    %ebx
  80027a:	5e                   	pop    %esi
  80027b:	5f                   	pop    %edi
  80027c:	5d                   	pop    %ebp
  80027d:	c3                   	ret    

0080027e <sys_env_set_trapframe>:

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
  80027e:	55                   	push   %ebp
  80027f:	89 e5                	mov    %esp,%ebp
  800281:	57                   	push   %edi
  800282:	56                   	push   %esi
  800283:	53                   	push   %ebx
  800284:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800287:	bb 00 00 00 00       	mov    $0x0,%ebx
  80028c:	b8 09 00 00 00       	mov    $0x9,%eax
  800291:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800294:	8b 55 08             	mov    0x8(%ebp),%edx
  800297:	89 df                	mov    %ebx,%edi
  800299:	89 de                	mov    %ebx,%esi
  80029b:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  80029d:	85 c0                	test   %eax,%eax
  80029f:	7e 17                	jle    8002b8 <sys_env_set_trapframe+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002a1:	83 ec 0c             	sub    $0xc,%esp
  8002a4:	50                   	push   %eax
  8002a5:	6a 09                	push   $0x9
  8002a7:	68 4a 0f 80 00       	push   $0x800f4a
  8002ac:	6a 23                	push   $0x23
  8002ae:	68 67 0f 80 00       	push   $0x800f67
  8002b3:	e8 ae 00 00 00       	call   800366 <_panic>

int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	return syscall(SYS_env_set_trapframe, 1, envid, (uint32_t) tf, 0, 0, 0);
}
  8002b8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002bb:	5b                   	pop    %ebx
  8002bc:	5e                   	pop    %esi
  8002bd:	5f                   	pop    %edi
  8002be:	5d                   	pop    %ebp
  8002bf:	c3                   	ret    

008002c0 <sys_env_set_pgfault_upcall>:

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
  8002c0:	55                   	push   %ebp
  8002c1:	89 e5                	mov    %esp,%ebp
  8002c3:	57                   	push   %edi
  8002c4:	56                   	push   %esi
  8002c5:	53                   	push   %ebx
  8002c6:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8002c9:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002ce:	b8 0a 00 00 00       	mov    $0xa,%eax
  8002d3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8002d6:	8b 55 08             	mov    0x8(%ebp),%edx
  8002d9:	89 df                	mov    %ebx,%edi
  8002db:	89 de                	mov    %ebx,%esi
  8002dd:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8002df:	85 c0                	test   %eax,%eax
  8002e1:	7e 17                	jle    8002fa <sys_env_set_pgfault_upcall+0x3a>
		panic("syscall %d returned %d (> 0)", num, ret);
  8002e3:	83 ec 0c             	sub    $0xc,%esp
  8002e6:	50                   	push   %eax
  8002e7:	6a 0a                	push   $0xa
  8002e9:	68 4a 0f 80 00       	push   $0x800f4a
  8002ee:	6a 23                	push   $0x23
  8002f0:	68 67 0f 80 00       	push   $0x800f67
  8002f5:	e8 6c 00 00 00       	call   800366 <_panic>

int
sys_env_set_pgfault_upcall(envid_t envid, void *upcall)
{
	return syscall(SYS_env_set_pgfault_upcall, 1, envid, (uint32_t) upcall, 0, 0, 0);
}
  8002fa:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002fd:	5b                   	pop    %ebx
  8002fe:	5e                   	pop    %esi
  8002ff:	5f                   	pop    %edi
  800300:	5d                   	pop    %ebp
  800301:	c3                   	ret    

00800302 <sys_ipc_try_send>:

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
  800302:	55                   	push   %ebp
  800303:	89 e5                	mov    %esp,%ebp
  800305:	57                   	push   %edi
  800306:	56                   	push   %esi
  800307:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800308:	be 00 00 00 00       	mov    $0x0,%esi
  80030d:	b8 0d 00 00 00       	mov    $0xd,%eax
  800312:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800315:	8b 55 08             	mov    0x8(%ebp),%edx
  800318:	8b 5d 10             	mov    0x10(%ebp),%ebx
  80031b:	8b 7d 14             	mov    0x14(%ebp),%edi
  80031e:	cd 30                	int    $0x30

int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, int perm)
{
	return syscall(SYS_ipc_try_send, 0, envid, value, (uint32_t) srcva, perm, 0);
}
  800320:	5b                   	pop    %ebx
  800321:	5e                   	pop    %esi
  800322:	5f                   	pop    %edi
  800323:	5d                   	pop    %ebp
  800324:	c3                   	ret    

00800325 <sys_ipc_recv>:

int
sys_ipc_recv(void *dstva)
{
  800325:	55                   	push   %ebp
  800326:	89 e5                	mov    %esp,%ebp
  800328:	57                   	push   %edi
  800329:	56                   	push   %esi
  80032a:	53                   	push   %ebx
  80032b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80032e:	b9 00 00 00 00       	mov    $0x0,%ecx
  800333:	b8 0e 00 00 00       	mov    $0xe,%eax
  800338:	8b 55 08             	mov    0x8(%ebp),%edx
  80033b:	89 cb                	mov    %ecx,%ebx
  80033d:	89 cf                	mov    %ecx,%edi
  80033f:	89 ce                	mov    %ecx,%esi
  800341:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800343:	85 c0                	test   %eax,%eax
  800345:	7e 17                	jle    80035e <sys_ipc_recv+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800347:	83 ec 0c             	sub    $0xc,%esp
  80034a:	50                   	push   %eax
  80034b:	6a 0e                	push   $0xe
  80034d:	68 4a 0f 80 00       	push   $0x800f4a
  800352:	6a 23                	push   $0x23
  800354:	68 67 0f 80 00       	push   $0x800f67
  800359:	e8 08 00 00 00       	call   800366 <_panic>

int
sys_ipc_recv(void *dstva)
{
	return syscall(SYS_ipc_recv, 1, (uint32_t)dstva, 0, 0, 0, 0);
}
  80035e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800361:	5b                   	pop    %ebx
  800362:	5e                   	pop    %esi
  800363:	5f                   	pop    %edi
  800364:	5d                   	pop    %ebp
  800365:	c3                   	ret    

00800366 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800366:	55                   	push   %ebp
  800367:	89 e5                	mov    %esp,%ebp
  800369:	56                   	push   %esi
  80036a:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80036b:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80036e:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800374:	e8 9f fd ff ff       	call   800118 <sys_getenvid>
  800379:	83 ec 0c             	sub    $0xc,%esp
  80037c:	ff 75 0c             	pushl  0xc(%ebp)
  80037f:	ff 75 08             	pushl  0x8(%ebp)
  800382:	56                   	push   %esi
  800383:	50                   	push   %eax
  800384:	68 78 0f 80 00       	push   $0x800f78
  800389:	e8 b0 00 00 00       	call   80043e <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80038e:	83 c4 18             	add    $0x18,%esp
  800391:	53                   	push   %ebx
  800392:	ff 75 10             	pushl  0x10(%ebp)
  800395:	e8 53 00 00 00       	call   8003ed <vcprintf>
	cprintf("\n");
  80039a:	c7 04 24 9b 0f 80 00 	movl   $0x800f9b,(%esp)
  8003a1:	e8 98 00 00 00       	call   80043e <cprintf>
  8003a6:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8003a9:	cc                   	int3   
  8003aa:	eb fd                	jmp    8003a9 <_panic+0x43>

008003ac <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8003ac:	55                   	push   %ebp
  8003ad:	89 e5                	mov    %esp,%ebp
  8003af:	53                   	push   %ebx
  8003b0:	83 ec 04             	sub    $0x4,%esp
  8003b3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8003b6:	8b 13                	mov    (%ebx),%edx
  8003b8:	8d 42 01             	lea    0x1(%edx),%eax
  8003bb:	89 03                	mov    %eax,(%ebx)
  8003bd:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8003c0:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8003c4:	3d ff 00 00 00       	cmp    $0xff,%eax
  8003c9:	75 1a                	jne    8003e5 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8003cb:	83 ec 08             	sub    $0x8,%esp
  8003ce:	68 ff 00 00 00       	push   $0xff
  8003d3:	8d 43 08             	lea    0x8(%ebx),%eax
  8003d6:	50                   	push   %eax
  8003d7:	e8 be fc ff ff       	call   80009a <sys_cputs>
		b->idx = 0;
  8003dc:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8003e2:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8003e5:	ff 43 04             	incl   0x4(%ebx)
}
  8003e8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003eb:	c9                   	leave  
  8003ec:	c3                   	ret    

008003ed <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8003ed:	55                   	push   %ebp
  8003ee:	89 e5                	mov    %esp,%ebp
  8003f0:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8003f6:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8003fd:	00 00 00 
	b.cnt = 0;
  800400:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800407:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80040a:	ff 75 0c             	pushl  0xc(%ebp)
  80040d:	ff 75 08             	pushl  0x8(%ebp)
  800410:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800416:	50                   	push   %eax
  800417:	68 ac 03 80 00       	push   $0x8003ac
  80041c:	e8 51 01 00 00       	call   800572 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800421:	83 c4 08             	add    $0x8,%esp
  800424:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80042a:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800430:	50                   	push   %eax
  800431:	e8 64 fc ff ff       	call   80009a <sys_cputs>

	return b.cnt;
}
  800436:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80043c:	c9                   	leave  
  80043d:	c3                   	ret    

0080043e <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80043e:	55                   	push   %ebp
  80043f:	89 e5                	mov    %esp,%ebp
  800441:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800444:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800447:	50                   	push   %eax
  800448:	ff 75 08             	pushl  0x8(%ebp)
  80044b:	e8 9d ff ff ff       	call   8003ed <vcprintf>
	va_end(ap);

	return cnt;
}
  800450:	c9                   	leave  
  800451:	c3                   	ret    

00800452 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800452:	55                   	push   %ebp
  800453:	89 e5                	mov    %esp,%ebp
  800455:	57                   	push   %edi
  800456:	56                   	push   %esi
  800457:	53                   	push   %ebx
  800458:	83 ec 1c             	sub    $0x1c,%esp
  80045b:	89 c7                	mov    %eax,%edi
  80045d:	89 d6                	mov    %edx,%esi
  80045f:	8b 45 08             	mov    0x8(%ebp),%eax
  800462:	8b 55 0c             	mov    0xc(%ebp),%edx
  800465:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800468:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80046b:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80046e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800473:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800476:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800479:	39 d3                	cmp    %edx,%ebx
  80047b:	72 05                	jb     800482 <printnum+0x30>
  80047d:	39 45 10             	cmp    %eax,0x10(%ebp)
  800480:	77 45                	ja     8004c7 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800482:	83 ec 0c             	sub    $0xc,%esp
  800485:	ff 75 18             	pushl  0x18(%ebp)
  800488:	8b 45 14             	mov    0x14(%ebp),%eax
  80048b:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80048e:	53                   	push   %ebx
  80048f:	ff 75 10             	pushl  0x10(%ebp)
  800492:	83 ec 08             	sub    $0x8,%esp
  800495:	ff 75 e4             	pushl  -0x1c(%ebp)
  800498:	ff 75 e0             	pushl  -0x20(%ebp)
  80049b:	ff 75 dc             	pushl  -0x24(%ebp)
  80049e:	ff 75 d8             	pushl  -0x28(%ebp)
  8004a1:	e8 26 08 00 00       	call   800ccc <__udivdi3>
  8004a6:	83 c4 18             	add    $0x18,%esp
  8004a9:	52                   	push   %edx
  8004aa:	50                   	push   %eax
  8004ab:	89 f2                	mov    %esi,%edx
  8004ad:	89 f8                	mov    %edi,%eax
  8004af:	e8 9e ff ff ff       	call   800452 <printnum>
  8004b4:	83 c4 20             	add    $0x20,%esp
  8004b7:	eb 16                	jmp    8004cf <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8004b9:	83 ec 08             	sub    $0x8,%esp
  8004bc:	56                   	push   %esi
  8004bd:	ff 75 18             	pushl  0x18(%ebp)
  8004c0:	ff d7                	call   *%edi
  8004c2:	83 c4 10             	add    $0x10,%esp
  8004c5:	eb 03                	jmp    8004ca <printnum+0x78>
  8004c7:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8004ca:	4b                   	dec    %ebx
  8004cb:	85 db                	test   %ebx,%ebx
  8004cd:	7f ea                	jg     8004b9 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8004cf:	83 ec 08             	sub    $0x8,%esp
  8004d2:	56                   	push   %esi
  8004d3:	83 ec 04             	sub    $0x4,%esp
  8004d6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8004d9:	ff 75 e0             	pushl  -0x20(%ebp)
  8004dc:	ff 75 dc             	pushl  -0x24(%ebp)
  8004df:	ff 75 d8             	pushl  -0x28(%ebp)
  8004e2:	e8 f5 08 00 00       	call   800ddc <__umoddi3>
  8004e7:	83 c4 14             	add    $0x14,%esp
  8004ea:	0f be 80 9d 0f 80 00 	movsbl 0x800f9d(%eax),%eax
  8004f1:	50                   	push   %eax
  8004f2:	ff d7                	call   *%edi
}
  8004f4:	83 c4 10             	add    $0x10,%esp
  8004f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8004fa:	5b                   	pop    %ebx
  8004fb:	5e                   	pop    %esi
  8004fc:	5f                   	pop    %edi
  8004fd:	5d                   	pop    %ebp
  8004fe:	c3                   	ret    

008004ff <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8004ff:	55                   	push   %ebp
  800500:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800502:	83 fa 01             	cmp    $0x1,%edx
  800505:	7e 0e                	jle    800515 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800507:	8b 10                	mov    (%eax),%edx
  800509:	8d 4a 08             	lea    0x8(%edx),%ecx
  80050c:	89 08                	mov    %ecx,(%eax)
  80050e:	8b 02                	mov    (%edx),%eax
  800510:	8b 52 04             	mov    0x4(%edx),%edx
  800513:	eb 22                	jmp    800537 <getuint+0x38>
	else if (lflag)
  800515:	85 d2                	test   %edx,%edx
  800517:	74 10                	je     800529 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800519:	8b 10                	mov    (%eax),%edx
  80051b:	8d 4a 04             	lea    0x4(%edx),%ecx
  80051e:	89 08                	mov    %ecx,(%eax)
  800520:	8b 02                	mov    (%edx),%eax
  800522:	ba 00 00 00 00       	mov    $0x0,%edx
  800527:	eb 0e                	jmp    800537 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800529:	8b 10                	mov    (%eax),%edx
  80052b:	8d 4a 04             	lea    0x4(%edx),%ecx
  80052e:	89 08                	mov    %ecx,(%eax)
  800530:	8b 02                	mov    (%edx),%eax
  800532:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800537:	5d                   	pop    %ebp
  800538:	c3                   	ret    

00800539 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800539:	55                   	push   %ebp
  80053a:	89 e5                	mov    %esp,%ebp
  80053c:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80053f:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800542:	8b 10                	mov    (%eax),%edx
  800544:	3b 50 04             	cmp    0x4(%eax),%edx
  800547:	73 0a                	jae    800553 <sprintputch+0x1a>
		*b->buf++ = ch;
  800549:	8d 4a 01             	lea    0x1(%edx),%ecx
  80054c:	89 08                	mov    %ecx,(%eax)
  80054e:	8b 45 08             	mov    0x8(%ebp),%eax
  800551:	88 02                	mov    %al,(%edx)
}
  800553:	5d                   	pop    %ebp
  800554:	c3                   	ret    

00800555 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800555:	55                   	push   %ebp
  800556:	89 e5                	mov    %esp,%ebp
  800558:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80055b:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80055e:	50                   	push   %eax
  80055f:	ff 75 10             	pushl  0x10(%ebp)
  800562:	ff 75 0c             	pushl  0xc(%ebp)
  800565:	ff 75 08             	pushl  0x8(%ebp)
  800568:	e8 05 00 00 00       	call   800572 <vprintfmt>
	va_end(ap);
}
  80056d:	83 c4 10             	add    $0x10,%esp
  800570:	c9                   	leave  
  800571:	c3                   	ret    

00800572 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800572:	55                   	push   %ebp
  800573:	89 e5                	mov    %esp,%ebp
  800575:	57                   	push   %edi
  800576:	56                   	push   %esi
  800577:	53                   	push   %ebx
  800578:	83 ec 2c             	sub    $0x2c,%esp
  80057b:	8b 75 08             	mov    0x8(%ebp),%esi
  80057e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800581:	8b 7d 10             	mov    0x10(%ebp),%edi
  800584:	eb 12                	jmp    800598 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800586:	85 c0                	test   %eax,%eax
  800588:	0f 84 68 03 00 00    	je     8008f6 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80058e:	83 ec 08             	sub    $0x8,%esp
  800591:	53                   	push   %ebx
  800592:	50                   	push   %eax
  800593:	ff d6                	call   *%esi
  800595:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800598:	47                   	inc    %edi
  800599:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80059d:	83 f8 25             	cmp    $0x25,%eax
  8005a0:	75 e4                	jne    800586 <vprintfmt+0x14>
  8005a2:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8005a6:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8005ad:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8005b4:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8005bb:	ba 00 00 00 00       	mov    $0x0,%edx
  8005c0:	eb 07                	jmp    8005c9 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c2:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8005c5:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005c9:	8d 47 01             	lea    0x1(%edi),%eax
  8005cc:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8005cf:	0f b6 0f             	movzbl (%edi),%ecx
  8005d2:	8a 07                	mov    (%edi),%al
  8005d4:	83 e8 23             	sub    $0x23,%eax
  8005d7:	3c 55                	cmp    $0x55,%al
  8005d9:	0f 87 fe 02 00 00    	ja     8008dd <vprintfmt+0x36b>
  8005df:	0f b6 c0             	movzbl %al,%eax
  8005e2:	ff 24 85 e0 10 80 00 	jmp    *0x8010e0(,%eax,4)
  8005e9:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8005ec:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8005f0:	eb d7                	jmp    8005c9 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005f2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f5:	b8 00 00 00 00       	mov    $0x0,%eax
  8005fa:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8005fd:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800600:	01 c0                	add    %eax,%eax
  800602:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800606:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800609:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80060c:	83 fa 09             	cmp    $0x9,%edx
  80060f:	77 34                	ja     800645 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800611:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800612:	eb e9                	jmp    8005fd <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800614:	8b 45 14             	mov    0x14(%ebp),%eax
  800617:	8d 48 04             	lea    0x4(%eax),%ecx
  80061a:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80061d:	8b 00                	mov    (%eax),%eax
  80061f:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800622:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800625:	eb 24                	jmp    80064b <vprintfmt+0xd9>
  800627:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80062b:	79 07                	jns    800634 <vprintfmt+0xc2>
  80062d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800634:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800637:	eb 90                	jmp    8005c9 <vprintfmt+0x57>
  800639:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80063c:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800643:	eb 84                	jmp    8005c9 <vprintfmt+0x57>
  800645:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800648:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80064b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80064f:	0f 89 74 ff ff ff    	jns    8005c9 <vprintfmt+0x57>
				width = precision, precision = -1;
  800655:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800658:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80065b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800662:	e9 62 ff ff ff       	jmp    8005c9 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800667:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800668:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80066b:	e9 59 ff ff ff       	jmp    8005c9 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800670:	8b 45 14             	mov    0x14(%ebp),%eax
  800673:	8d 50 04             	lea    0x4(%eax),%edx
  800676:	89 55 14             	mov    %edx,0x14(%ebp)
  800679:	83 ec 08             	sub    $0x8,%esp
  80067c:	53                   	push   %ebx
  80067d:	ff 30                	pushl  (%eax)
  80067f:	ff d6                	call   *%esi
			break;
  800681:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800684:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800687:	e9 0c ff ff ff       	jmp    800598 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80068c:	8b 45 14             	mov    0x14(%ebp),%eax
  80068f:	8d 50 04             	lea    0x4(%eax),%edx
  800692:	89 55 14             	mov    %edx,0x14(%ebp)
  800695:	8b 00                	mov    (%eax),%eax
  800697:	85 c0                	test   %eax,%eax
  800699:	79 02                	jns    80069d <vprintfmt+0x12b>
  80069b:	f7 d8                	neg    %eax
  80069d:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80069f:	83 f8 0f             	cmp    $0xf,%eax
  8006a2:	7f 0b                	jg     8006af <vprintfmt+0x13d>
  8006a4:	8b 04 85 40 12 80 00 	mov    0x801240(,%eax,4),%eax
  8006ab:	85 c0                	test   %eax,%eax
  8006ad:	75 18                	jne    8006c7 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8006af:	52                   	push   %edx
  8006b0:	68 b5 0f 80 00       	push   $0x800fb5
  8006b5:	53                   	push   %ebx
  8006b6:	56                   	push   %esi
  8006b7:	e8 99 fe ff ff       	call   800555 <printfmt>
  8006bc:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006bf:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8006c2:	e9 d1 fe ff ff       	jmp    800598 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8006c7:	50                   	push   %eax
  8006c8:	68 be 0f 80 00       	push   $0x800fbe
  8006cd:	53                   	push   %ebx
  8006ce:	56                   	push   %esi
  8006cf:	e8 81 fe ff ff       	call   800555 <printfmt>
  8006d4:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006d7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006da:	e9 b9 fe ff ff       	jmp    800598 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8006df:	8b 45 14             	mov    0x14(%ebp),%eax
  8006e2:	8d 50 04             	lea    0x4(%eax),%edx
  8006e5:	89 55 14             	mov    %edx,0x14(%ebp)
  8006e8:	8b 38                	mov    (%eax),%edi
  8006ea:	85 ff                	test   %edi,%edi
  8006ec:	75 05                	jne    8006f3 <vprintfmt+0x181>
				p = "(null)";
  8006ee:	bf ae 0f 80 00       	mov    $0x800fae,%edi
			if (width > 0 && padc != '-')
  8006f3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006f7:	0f 8e 90 00 00 00    	jle    80078d <vprintfmt+0x21b>
  8006fd:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800701:	0f 84 8e 00 00 00    	je     800795 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800707:	83 ec 08             	sub    $0x8,%esp
  80070a:	ff 75 d0             	pushl  -0x30(%ebp)
  80070d:	57                   	push   %edi
  80070e:	e8 70 02 00 00       	call   800983 <strnlen>
  800713:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800716:	29 c1                	sub    %eax,%ecx
  800718:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80071b:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80071e:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800722:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800725:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800728:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80072a:	eb 0d                	jmp    800739 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80072c:	83 ec 08             	sub    $0x8,%esp
  80072f:	53                   	push   %ebx
  800730:	ff 75 e4             	pushl  -0x1c(%ebp)
  800733:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800735:	4f                   	dec    %edi
  800736:	83 c4 10             	add    $0x10,%esp
  800739:	85 ff                	test   %edi,%edi
  80073b:	7f ef                	jg     80072c <vprintfmt+0x1ba>
  80073d:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800740:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800743:	89 c8                	mov    %ecx,%eax
  800745:	85 c9                	test   %ecx,%ecx
  800747:	79 05                	jns    80074e <vprintfmt+0x1dc>
  800749:	b8 00 00 00 00       	mov    $0x0,%eax
  80074e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800751:	29 c1                	sub    %eax,%ecx
  800753:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800756:	89 75 08             	mov    %esi,0x8(%ebp)
  800759:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80075c:	eb 3d                	jmp    80079b <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80075e:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800762:	74 19                	je     80077d <vprintfmt+0x20b>
  800764:	0f be c0             	movsbl %al,%eax
  800767:	83 e8 20             	sub    $0x20,%eax
  80076a:	83 f8 5e             	cmp    $0x5e,%eax
  80076d:	76 0e                	jbe    80077d <vprintfmt+0x20b>
					putch('?', putdat);
  80076f:	83 ec 08             	sub    $0x8,%esp
  800772:	53                   	push   %ebx
  800773:	6a 3f                	push   $0x3f
  800775:	ff 55 08             	call   *0x8(%ebp)
  800778:	83 c4 10             	add    $0x10,%esp
  80077b:	eb 0b                	jmp    800788 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80077d:	83 ec 08             	sub    $0x8,%esp
  800780:	53                   	push   %ebx
  800781:	52                   	push   %edx
  800782:	ff 55 08             	call   *0x8(%ebp)
  800785:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800788:	ff 4d e4             	decl   -0x1c(%ebp)
  80078b:	eb 0e                	jmp    80079b <vprintfmt+0x229>
  80078d:	89 75 08             	mov    %esi,0x8(%ebp)
  800790:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800793:	eb 06                	jmp    80079b <vprintfmt+0x229>
  800795:	89 75 08             	mov    %esi,0x8(%ebp)
  800798:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80079b:	47                   	inc    %edi
  80079c:	8a 47 ff             	mov    -0x1(%edi),%al
  80079f:	0f be d0             	movsbl %al,%edx
  8007a2:	85 d2                	test   %edx,%edx
  8007a4:	74 1d                	je     8007c3 <vprintfmt+0x251>
  8007a6:	85 f6                	test   %esi,%esi
  8007a8:	78 b4                	js     80075e <vprintfmt+0x1ec>
  8007aa:	4e                   	dec    %esi
  8007ab:	79 b1                	jns    80075e <vprintfmt+0x1ec>
  8007ad:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b0:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007b3:	eb 14                	jmp    8007c9 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8007b5:	83 ec 08             	sub    $0x8,%esp
  8007b8:	53                   	push   %ebx
  8007b9:	6a 20                	push   $0x20
  8007bb:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8007bd:	4f                   	dec    %edi
  8007be:	83 c4 10             	add    $0x10,%esp
  8007c1:	eb 06                	jmp    8007c9 <vprintfmt+0x257>
  8007c3:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8007c6:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c9:	85 ff                	test   %edi,%edi
  8007cb:	7f e8                	jg     8007b5 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8007cd:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8007d0:	e9 c3 fd ff ff       	jmp    800598 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8007d5:	83 fa 01             	cmp    $0x1,%edx
  8007d8:	7e 16                	jle    8007f0 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8007da:	8b 45 14             	mov    0x14(%ebp),%eax
  8007dd:	8d 50 08             	lea    0x8(%eax),%edx
  8007e0:	89 55 14             	mov    %edx,0x14(%ebp)
  8007e3:	8b 50 04             	mov    0x4(%eax),%edx
  8007e6:	8b 00                	mov    (%eax),%eax
  8007e8:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8007eb:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8007ee:	eb 32                	jmp    800822 <vprintfmt+0x2b0>
	else if (lflag)
  8007f0:	85 d2                	test   %edx,%edx
  8007f2:	74 18                	je     80080c <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8007f4:	8b 45 14             	mov    0x14(%ebp),%eax
  8007f7:	8d 50 04             	lea    0x4(%eax),%edx
  8007fa:	89 55 14             	mov    %edx,0x14(%ebp)
  8007fd:	8b 00                	mov    (%eax),%eax
  8007ff:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800802:	89 c1                	mov    %eax,%ecx
  800804:	c1 f9 1f             	sar    $0x1f,%ecx
  800807:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80080a:	eb 16                	jmp    800822 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80080c:	8b 45 14             	mov    0x14(%ebp),%eax
  80080f:	8d 50 04             	lea    0x4(%eax),%edx
  800812:	89 55 14             	mov    %edx,0x14(%ebp)
  800815:	8b 00                	mov    (%eax),%eax
  800817:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80081a:	89 c1                	mov    %eax,%ecx
  80081c:	c1 f9 1f             	sar    $0x1f,%ecx
  80081f:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800822:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800825:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800828:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80082c:	79 76                	jns    8008a4 <vprintfmt+0x332>
				putch('-', putdat);
  80082e:	83 ec 08             	sub    $0x8,%esp
  800831:	53                   	push   %ebx
  800832:	6a 2d                	push   $0x2d
  800834:	ff d6                	call   *%esi
				num = -(long long) num;
  800836:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800839:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80083c:	f7 d8                	neg    %eax
  80083e:	83 d2 00             	adc    $0x0,%edx
  800841:	f7 da                	neg    %edx
  800843:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800846:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80084b:	eb 5c                	jmp    8008a9 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80084d:	8d 45 14             	lea    0x14(%ebp),%eax
  800850:	e8 aa fc ff ff       	call   8004ff <getuint>
			base = 10;
  800855:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80085a:	eb 4d                	jmp    8008a9 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80085c:	8d 45 14             	lea    0x14(%ebp),%eax
  80085f:	e8 9b fc ff ff       	call   8004ff <getuint>
			base = 8;
  800864:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800869:	eb 3e                	jmp    8008a9 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80086b:	83 ec 08             	sub    $0x8,%esp
  80086e:	53                   	push   %ebx
  80086f:	6a 30                	push   $0x30
  800871:	ff d6                	call   *%esi
			putch('x', putdat);
  800873:	83 c4 08             	add    $0x8,%esp
  800876:	53                   	push   %ebx
  800877:	6a 78                	push   $0x78
  800879:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80087b:	8b 45 14             	mov    0x14(%ebp),%eax
  80087e:	8d 50 04             	lea    0x4(%eax),%edx
  800881:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800884:	8b 00                	mov    (%eax),%eax
  800886:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80088b:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80088e:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800893:	eb 14                	jmp    8008a9 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800895:	8d 45 14             	lea    0x14(%ebp),%eax
  800898:	e8 62 fc ff ff       	call   8004ff <getuint>
			base = 16;
  80089d:	b9 10 00 00 00       	mov    $0x10,%ecx
  8008a2:	eb 05                	jmp    8008a9 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8008a4:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8008a9:	83 ec 0c             	sub    $0xc,%esp
  8008ac:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8008b0:	57                   	push   %edi
  8008b1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8008b4:	51                   	push   %ecx
  8008b5:	52                   	push   %edx
  8008b6:	50                   	push   %eax
  8008b7:	89 da                	mov    %ebx,%edx
  8008b9:	89 f0                	mov    %esi,%eax
  8008bb:	e8 92 fb ff ff       	call   800452 <printnum>
			break;
  8008c0:	83 c4 20             	add    $0x20,%esp
  8008c3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8008c6:	e9 cd fc ff ff       	jmp    800598 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008cb:	83 ec 08             	sub    $0x8,%esp
  8008ce:	53                   	push   %ebx
  8008cf:	51                   	push   %ecx
  8008d0:	ff d6                	call   *%esi
			break;
  8008d2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8008d5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8008d8:	e9 bb fc ff ff       	jmp    800598 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008dd:	83 ec 08             	sub    $0x8,%esp
  8008e0:	53                   	push   %ebx
  8008e1:	6a 25                	push   $0x25
  8008e3:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008e5:	83 c4 10             	add    $0x10,%esp
  8008e8:	eb 01                	jmp    8008eb <vprintfmt+0x379>
  8008ea:	4f                   	dec    %edi
  8008eb:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8008ef:	75 f9                	jne    8008ea <vprintfmt+0x378>
  8008f1:	e9 a2 fc ff ff       	jmp    800598 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8008f6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8008f9:	5b                   	pop    %ebx
  8008fa:	5e                   	pop    %esi
  8008fb:	5f                   	pop    %edi
  8008fc:	5d                   	pop    %ebp
  8008fd:	c3                   	ret    

008008fe <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008fe:	55                   	push   %ebp
  8008ff:	89 e5                	mov    %esp,%ebp
  800901:	83 ec 18             	sub    $0x18,%esp
  800904:	8b 45 08             	mov    0x8(%ebp),%eax
  800907:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80090a:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80090d:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800911:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800914:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80091b:	85 c0                	test   %eax,%eax
  80091d:	74 26                	je     800945 <vsnprintf+0x47>
  80091f:	85 d2                	test   %edx,%edx
  800921:	7e 29                	jle    80094c <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800923:	ff 75 14             	pushl  0x14(%ebp)
  800926:	ff 75 10             	pushl  0x10(%ebp)
  800929:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80092c:	50                   	push   %eax
  80092d:	68 39 05 80 00       	push   $0x800539
  800932:	e8 3b fc ff ff       	call   800572 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800937:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80093a:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80093d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800940:	83 c4 10             	add    $0x10,%esp
  800943:	eb 0c                	jmp    800951 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800945:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80094a:	eb 05                	jmp    800951 <vsnprintf+0x53>
  80094c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800951:	c9                   	leave  
  800952:	c3                   	ret    

00800953 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800953:	55                   	push   %ebp
  800954:	89 e5                	mov    %esp,%ebp
  800956:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800959:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80095c:	50                   	push   %eax
  80095d:	ff 75 10             	pushl  0x10(%ebp)
  800960:	ff 75 0c             	pushl  0xc(%ebp)
  800963:	ff 75 08             	pushl  0x8(%ebp)
  800966:	e8 93 ff ff ff       	call   8008fe <vsnprintf>
	va_end(ap);

	return rc;
}
  80096b:	c9                   	leave  
  80096c:	c3                   	ret    

0080096d <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80096d:	55                   	push   %ebp
  80096e:	89 e5                	mov    %esp,%ebp
  800970:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800973:	b8 00 00 00 00       	mov    $0x0,%eax
  800978:	eb 01                	jmp    80097b <strlen+0xe>
		n++;
  80097a:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80097b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80097f:	75 f9                	jne    80097a <strlen+0xd>
		n++;
	return n;
}
  800981:	5d                   	pop    %ebp
  800982:	c3                   	ret    

00800983 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800983:	55                   	push   %ebp
  800984:	89 e5                	mov    %esp,%ebp
  800986:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800989:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80098c:	ba 00 00 00 00       	mov    $0x0,%edx
  800991:	eb 01                	jmp    800994 <strnlen+0x11>
		n++;
  800993:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800994:	39 c2                	cmp    %eax,%edx
  800996:	74 08                	je     8009a0 <strnlen+0x1d>
  800998:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80099c:	75 f5                	jne    800993 <strnlen+0x10>
  80099e:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8009a0:	5d                   	pop    %ebp
  8009a1:	c3                   	ret    

008009a2 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009a2:	55                   	push   %ebp
  8009a3:	89 e5                	mov    %esp,%ebp
  8009a5:	53                   	push   %ebx
  8009a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8009a9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8009ac:	89 c2                	mov    %eax,%edx
  8009ae:	42                   	inc    %edx
  8009af:	41                   	inc    %ecx
  8009b0:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8009b3:	88 5a ff             	mov    %bl,-0x1(%edx)
  8009b6:	84 db                	test   %bl,%bl
  8009b8:	75 f4                	jne    8009ae <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8009ba:	5b                   	pop    %ebx
  8009bb:	5d                   	pop    %ebp
  8009bc:	c3                   	ret    

008009bd <strcat>:

char *
strcat(char *dst, const char *src)
{
  8009bd:	55                   	push   %ebp
  8009be:	89 e5                	mov    %esp,%ebp
  8009c0:	53                   	push   %ebx
  8009c1:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8009c4:	53                   	push   %ebx
  8009c5:	e8 a3 ff ff ff       	call   80096d <strlen>
  8009ca:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8009cd:	ff 75 0c             	pushl  0xc(%ebp)
  8009d0:	01 d8                	add    %ebx,%eax
  8009d2:	50                   	push   %eax
  8009d3:	e8 ca ff ff ff       	call   8009a2 <strcpy>
	return dst;
}
  8009d8:	89 d8                	mov    %ebx,%eax
  8009da:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8009dd:	c9                   	leave  
  8009de:	c3                   	ret    

008009df <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8009df:	55                   	push   %ebp
  8009e0:	89 e5                	mov    %esp,%ebp
  8009e2:	56                   	push   %esi
  8009e3:	53                   	push   %ebx
  8009e4:	8b 75 08             	mov    0x8(%ebp),%esi
  8009e7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009ea:	89 f3                	mov    %esi,%ebx
  8009ec:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ef:	89 f2                	mov    %esi,%edx
  8009f1:	eb 0c                	jmp    8009ff <strncpy+0x20>
		*dst++ = *src;
  8009f3:	42                   	inc    %edx
  8009f4:	8a 01                	mov    (%ecx),%al
  8009f6:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8009f9:	80 39 01             	cmpb   $0x1,(%ecx)
  8009fc:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009ff:	39 da                	cmp    %ebx,%edx
  800a01:	75 f0                	jne    8009f3 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800a03:	89 f0                	mov    %esi,%eax
  800a05:	5b                   	pop    %ebx
  800a06:	5e                   	pop    %esi
  800a07:	5d                   	pop    %ebp
  800a08:	c3                   	ret    

00800a09 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800a09:	55                   	push   %ebp
  800a0a:	89 e5                	mov    %esp,%ebp
  800a0c:	56                   	push   %esi
  800a0d:	53                   	push   %ebx
  800a0e:	8b 75 08             	mov    0x8(%ebp),%esi
  800a11:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a14:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800a17:	85 c0                	test   %eax,%eax
  800a19:	74 1e                	je     800a39 <strlcpy+0x30>
  800a1b:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800a1f:	89 f2                	mov    %esi,%edx
  800a21:	eb 05                	jmp    800a28 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800a23:	42                   	inc    %edx
  800a24:	41                   	inc    %ecx
  800a25:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a28:	39 c2                	cmp    %eax,%edx
  800a2a:	74 08                	je     800a34 <strlcpy+0x2b>
  800a2c:	8a 19                	mov    (%ecx),%bl
  800a2e:	84 db                	test   %bl,%bl
  800a30:	75 f1                	jne    800a23 <strlcpy+0x1a>
  800a32:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800a34:	c6 00 00             	movb   $0x0,(%eax)
  800a37:	eb 02                	jmp    800a3b <strlcpy+0x32>
  800a39:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800a3b:	29 f0                	sub    %esi,%eax
}
  800a3d:	5b                   	pop    %ebx
  800a3e:	5e                   	pop    %esi
  800a3f:	5d                   	pop    %ebp
  800a40:	c3                   	ret    

00800a41 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a41:	55                   	push   %ebp
  800a42:	89 e5                	mov    %esp,%ebp
  800a44:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800a47:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800a4a:	eb 02                	jmp    800a4e <strcmp+0xd>
		p++, q++;
  800a4c:	41                   	inc    %ecx
  800a4d:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a4e:	8a 01                	mov    (%ecx),%al
  800a50:	84 c0                	test   %al,%al
  800a52:	74 04                	je     800a58 <strcmp+0x17>
  800a54:	3a 02                	cmp    (%edx),%al
  800a56:	74 f4                	je     800a4c <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a58:	0f b6 c0             	movzbl %al,%eax
  800a5b:	0f b6 12             	movzbl (%edx),%edx
  800a5e:	29 d0                	sub    %edx,%eax
}
  800a60:	5d                   	pop    %ebp
  800a61:	c3                   	ret    

00800a62 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800a62:	55                   	push   %ebp
  800a63:	89 e5                	mov    %esp,%ebp
  800a65:	53                   	push   %ebx
  800a66:	8b 45 08             	mov    0x8(%ebp),%eax
  800a69:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a6c:	89 c3                	mov    %eax,%ebx
  800a6e:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800a71:	eb 02                	jmp    800a75 <strncmp+0x13>
		n--, p++, q++;
  800a73:	40                   	inc    %eax
  800a74:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800a75:	39 d8                	cmp    %ebx,%eax
  800a77:	74 14                	je     800a8d <strncmp+0x2b>
  800a79:	8a 08                	mov    (%eax),%cl
  800a7b:	84 c9                	test   %cl,%cl
  800a7d:	74 04                	je     800a83 <strncmp+0x21>
  800a7f:	3a 0a                	cmp    (%edx),%cl
  800a81:	74 f0                	je     800a73 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a83:	0f b6 00             	movzbl (%eax),%eax
  800a86:	0f b6 12             	movzbl (%edx),%edx
  800a89:	29 d0                	sub    %edx,%eax
  800a8b:	eb 05                	jmp    800a92 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800a8d:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800a92:	5b                   	pop    %ebx
  800a93:	5d                   	pop    %ebp
  800a94:	c3                   	ret    

00800a95 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a95:	55                   	push   %ebp
  800a96:	89 e5                	mov    %esp,%ebp
  800a98:	8b 45 08             	mov    0x8(%ebp),%eax
  800a9b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800a9e:	eb 05                	jmp    800aa5 <strchr+0x10>
		if (*s == c)
  800aa0:	38 ca                	cmp    %cl,%dl
  800aa2:	74 0c                	je     800ab0 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800aa4:	40                   	inc    %eax
  800aa5:	8a 10                	mov    (%eax),%dl
  800aa7:	84 d2                	test   %dl,%dl
  800aa9:	75 f5                	jne    800aa0 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800aab:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ab0:	5d                   	pop    %ebp
  800ab1:	c3                   	ret    

00800ab2 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800ab2:	55                   	push   %ebp
  800ab3:	89 e5                	mov    %esp,%ebp
  800ab5:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab8:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800abb:	eb 05                	jmp    800ac2 <strfind+0x10>
		if (*s == c)
  800abd:	38 ca                	cmp    %cl,%dl
  800abf:	74 07                	je     800ac8 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800ac1:	40                   	inc    %eax
  800ac2:	8a 10                	mov    (%eax),%dl
  800ac4:	84 d2                	test   %dl,%dl
  800ac6:	75 f5                	jne    800abd <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800ac8:	5d                   	pop    %ebp
  800ac9:	c3                   	ret    

00800aca <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800aca:	55                   	push   %ebp
  800acb:	89 e5                	mov    %esp,%ebp
  800acd:	57                   	push   %edi
  800ace:	56                   	push   %esi
  800acf:	53                   	push   %ebx
  800ad0:	8b 7d 08             	mov    0x8(%ebp),%edi
  800ad3:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800ad6:	85 c9                	test   %ecx,%ecx
  800ad8:	74 36                	je     800b10 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800ada:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800ae0:	75 28                	jne    800b0a <memset+0x40>
  800ae2:	f6 c1 03             	test   $0x3,%cl
  800ae5:	75 23                	jne    800b0a <memset+0x40>
		c &= 0xFF;
  800ae7:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800aeb:	89 d3                	mov    %edx,%ebx
  800aed:	c1 e3 08             	shl    $0x8,%ebx
  800af0:	89 d6                	mov    %edx,%esi
  800af2:	c1 e6 18             	shl    $0x18,%esi
  800af5:	89 d0                	mov    %edx,%eax
  800af7:	c1 e0 10             	shl    $0x10,%eax
  800afa:	09 f0                	or     %esi,%eax
  800afc:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800afe:	89 d8                	mov    %ebx,%eax
  800b00:	09 d0                	or     %edx,%eax
  800b02:	c1 e9 02             	shr    $0x2,%ecx
  800b05:	fc                   	cld    
  800b06:	f3 ab                	rep stos %eax,%es:(%edi)
  800b08:	eb 06                	jmp    800b10 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800b0a:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b0d:	fc                   	cld    
  800b0e:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800b10:	89 f8                	mov    %edi,%eax
  800b12:	5b                   	pop    %ebx
  800b13:	5e                   	pop    %esi
  800b14:	5f                   	pop    %edi
  800b15:	5d                   	pop    %ebp
  800b16:	c3                   	ret    

00800b17 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800b17:	55                   	push   %ebp
  800b18:	89 e5                	mov    %esp,%ebp
  800b1a:	57                   	push   %edi
  800b1b:	56                   	push   %esi
  800b1c:	8b 45 08             	mov    0x8(%ebp),%eax
  800b1f:	8b 75 0c             	mov    0xc(%ebp),%esi
  800b22:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b25:	39 c6                	cmp    %eax,%esi
  800b27:	73 33                	jae    800b5c <memmove+0x45>
  800b29:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800b2c:	39 d0                	cmp    %edx,%eax
  800b2e:	73 2c                	jae    800b5c <memmove+0x45>
		s += n;
		d += n;
  800b30:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b33:	89 d6                	mov    %edx,%esi
  800b35:	09 fe                	or     %edi,%esi
  800b37:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800b3d:	75 13                	jne    800b52 <memmove+0x3b>
  800b3f:	f6 c1 03             	test   $0x3,%cl
  800b42:	75 0e                	jne    800b52 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800b44:	83 ef 04             	sub    $0x4,%edi
  800b47:	8d 72 fc             	lea    -0x4(%edx),%esi
  800b4a:	c1 e9 02             	shr    $0x2,%ecx
  800b4d:	fd                   	std    
  800b4e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b50:	eb 07                	jmp    800b59 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800b52:	4f                   	dec    %edi
  800b53:	8d 72 ff             	lea    -0x1(%edx),%esi
  800b56:	fd                   	std    
  800b57:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800b59:	fc                   	cld    
  800b5a:	eb 1d                	jmp    800b79 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800b5c:	89 f2                	mov    %esi,%edx
  800b5e:	09 c2                	or     %eax,%edx
  800b60:	f6 c2 03             	test   $0x3,%dl
  800b63:	75 0f                	jne    800b74 <memmove+0x5d>
  800b65:	f6 c1 03             	test   $0x3,%cl
  800b68:	75 0a                	jne    800b74 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800b6a:	c1 e9 02             	shr    $0x2,%ecx
  800b6d:	89 c7                	mov    %eax,%edi
  800b6f:	fc                   	cld    
  800b70:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800b72:	eb 05                	jmp    800b79 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800b74:	89 c7                	mov    %eax,%edi
  800b76:	fc                   	cld    
  800b77:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800b79:	5e                   	pop    %esi
  800b7a:	5f                   	pop    %edi
  800b7b:	5d                   	pop    %ebp
  800b7c:	c3                   	ret    

00800b7d <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800b7d:	55                   	push   %ebp
  800b7e:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800b80:	ff 75 10             	pushl  0x10(%ebp)
  800b83:	ff 75 0c             	pushl  0xc(%ebp)
  800b86:	ff 75 08             	pushl  0x8(%ebp)
  800b89:	e8 89 ff ff ff       	call   800b17 <memmove>
}
  800b8e:	c9                   	leave  
  800b8f:	c3                   	ret    

00800b90 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800b90:	55                   	push   %ebp
  800b91:	89 e5                	mov    %esp,%ebp
  800b93:	56                   	push   %esi
  800b94:	53                   	push   %ebx
  800b95:	8b 45 08             	mov    0x8(%ebp),%eax
  800b98:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b9b:	89 c6                	mov    %eax,%esi
  800b9d:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800ba0:	eb 14                	jmp    800bb6 <memcmp+0x26>
		if (*s1 != *s2)
  800ba2:	8a 08                	mov    (%eax),%cl
  800ba4:	8a 1a                	mov    (%edx),%bl
  800ba6:	38 d9                	cmp    %bl,%cl
  800ba8:	74 0a                	je     800bb4 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800baa:	0f b6 c1             	movzbl %cl,%eax
  800bad:	0f b6 db             	movzbl %bl,%ebx
  800bb0:	29 d8                	sub    %ebx,%eax
  800bb2:	eb 0b                	jmp    800bbf <memcmp+0x2f>
		s1++, s2++;
  800bb4:	40                   	inc    %eax
  800bb5:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800bb6:	39 f0                	cmp    %esi,%eax
  800bb8:	75 e8                	jne    800ba2 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bba:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bbf:	5b                   	pop    %ebx
  800bc0:	5e                   	pop    %esi
  800bc1:	5d                   	pop    %ebp
  800bc2:	c3                   	ret    

00800bc3 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800bc3:	55                   	push   %ebp
  800bc4:	89 e5                	mov    %esp,%ebp
  800bc6:	53                   	push   %ebx
  800bc7:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800bca:	89 c1                	mov    %eax,%ecx
  800bcc:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800bcf:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bd3:	eb 08                	jmp    800bdd <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bd5:	0f b6 10             	movzbl (%eax),%edx
  800bd8:	39 da                	cmp    %ebx,%edx
  800bda:	74 05                	je     800be1 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bdc:	40                   	inc    %eax
  800bdd:	39 c8                	cmp    %ecx,%eax
  800bdf:	72 f4                	jb     800bd5 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800be1:	5b                   	pop    %ebx
  800be2:	5d                   	pop    %ebp
  800be3:	c3                   	ret    

00800be4 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800be4:	55                   	push   %ebp
  800be5:	89 e5                	mov    %esp,%ebp
  800be7:	57                   	push   %edi
  800be8:	56                   	push   %esi
  800be9:	53                   	push   %ebx
  800bea:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bed:	eb 01                	jmp    800bf0 <strtol+0xc>
		s++;
  800bef:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf0:	8a 01                	mov    (%ecx),%al
  800bf2:	3c 20                	cmp    $0x20,%al
  800bf4:	74 f9                	je     800bef <strtol+0xb>
  800bf6:	3c 09                	cmp    $0x9,%al
  800bf8:	74 f5                	je     800bef <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bfa:	3c 2b                	cmp    $0x2b,%al
  800bfc:	75 08                	jne    800c06 <strtol+0x22>
		s++;
  800bfe:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800bff:	bf 00 00 00 00       	mov    $0x0,%edi
  800c04:	eb 11                	jmp    800c17 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800c06:	3c 2d                	cmp    $0x2d,%al
  800c08:	75 08                	jne    800c12 <strtol+0x2e>
		s++, neg = 1;
  800c0a:	41                   	inc    %ecx
  800c0b:	bf 01 00 00 00       	mov    $0x1,%edi
  800c10:	eb 05                	jmp    800c17 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800c12:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c17:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c1b:	0f 84 87 00 00 00    	je     800ca8 <strtol+0xc4>
  800c21:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c25:	75 27                	jne    800c4e <strtol+0x6a>
  800c27:	80 39 30             	cmpb   $0x30,(%ecx)
  800c2a:	75 22                	jne    800c4e <strtol+0x6a>
  800c2c:	e9 88 00 00 00       	jmp    800cb9 <strtol+0xd5>
		s += 2, base = 16;
  800c31:	83 c1 02             	add    $0x2,%ecx
  800c34:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c3b:	eb 11                	jmp    800c4e <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800c3d:	41                   	inc    %ecx
  800c3e:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c45:	eb 07                	jmp    800c4e <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800c47:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800c4e:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c53:	8a 11                	mov    (%ecx),%dl
  800c55:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800c58:	80 fb 09             	cmp    $0x9,%bl
  800c5b:	77 08                	ja     800c65 <strtol+0x81>
			dig = *s - '0';
  800c5d:	0f be d2             	movsbl %dl,%edx
  800c60:	83 ea 30             	sub    $0x30,%edx
  800c63:	eb 22                	jmp    800c87 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800c65:	8d 72 9f             	lea    -0x61(%edx),%esi
  800c68:	89 f3                	mov    %esi,%ebx
  800c6a:	80 fb 19             	cmp    $0x19,%bl
  800c6d:	77 08                	ja     800c77 <strtol+0x93>
			dig = *s - 'a' + 10;
  800c6f:	0f be d2             	movsbl %dl,%edx
  800c72:	83 ea 57             	sub    $0x57,%edx
  800c75:	eb 10                	jmp    800c87 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800c77:	8d 72 bf             	lea    -0x41(%edx),%esi
  800c7a:	89 f3                	mov    %esi,%ebx
  800c7c:	80 fb 19             	cmp    $0x19,%bl
  800c7f:	77 14                	ja     800c95 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800c81:	0f be d2             	movsbl %dl,%edx
  800c84:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800c87:	3b 55 10             	cmp    0x10(%ebp),%edx
  800c8a:	7d 09                	jge    800c95 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800c8c:	41                   	inc    %ecx
  800c8d:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c91:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800c93:	eb be                	jmp    800c53 <strtol+0x6f>

	if (endptr)
  800c95:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c99:	74 05                	je     800ca0 <strtol+0xbc>
		*endptr = (char *) s;
  800c9b:	8b 75 0c             	mov    0xc(%ebp),%esi
  800c9e:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800ca0:	85 ff                	test   %edi,%edi
  800ca2:	74 21                	je     800cc5 <strtol+0xe1>
  800ca4:	f7 d8                	neg    %eax
  800ca6:	eb 1d                	jmp    800cc5 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ca8:	80 39 30             	cmpb   $0x30,(%ecx)
  800cab:	75 9a                	jne    800c47 <strtol+0x63>
  800cad:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cb1:	0f 84 7a ff ff ff    	je     800c31 <strtol+0x4d>
  800cb7:	eb 84                	jmp    800c3d <strtol+0x59>
  800cb9:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800cbd:	0f 84 6e ff ff ff    	je     800c31 <strtol+0x4d>
  800cc3:	eb 89                	jmp    800c4e <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800cc5:	5b                   	pop    %ebx
  800cc6:	5e                   	pop    %esi
  800cc7:	5f                   	pop    %edi
  800cc8:	5d                   	pop    %ebp
  800cc9:	c3                   	ret    
  800cca:	66 90                	xchg   %ax,%ax

00800ccc <__udivdi3>:
  800ccc:	55                   	push   %ebp
  800ccd:	57                   	push   %edi
  800cce:	56                   	push   %esi
  800ccf:	53                   	push   %ebx
  800cd0:	83 ec 1c             	sub    $0x1c,%esp
  800cd3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800cd7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800cdb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800cdf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ce3:	89 ca                	mov    %ecx,%edx
  800ce5:	89 f8                	mov    %edi,%eax
  800ce7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ceb:	85 f6                	test   %esi,%esi
  800ced:	75 2d                	jne    800d1c <__udivdi3+0x50>
  800cef:	39 cf                	cmp    %ecx,%edi
  800cf1:	77 65                	ja     800d58 <__udivdi3+0x8c>
  800cf3:	89 fd                	mov    %edi,%ebp
  800cf5:	85 ff                	test   %edi,%edi
  800cf7:	75 0b                	jne    800d04 <__udivdi3+0x38>
  800cf9:	b8 01 00 00 00       	mov    $0x1,%eax
  800cfe:	31 d2                	xor    %edx,%edx
  800d00:	f7 f7                	div    %edi
  800d02:	89 c5                	mov    %eax,%ebp
  800d04:	31 d2                	xor    %edx,%edx
  800d06:	89 c8                	mov    %ecx,%eax
  800d08:	f7 f5                	div    %ebp
  800d0a:	89 c1                	mov    %eax,%ecx
  800d0c:	89 d8                	mov    %ebx,%eax
  800d0e:	f7 f5                	div    %ebp
  800d10:	89 cf                	mov    %ecx,%edi
  800d12:	89 fa                	mov    %edi,%edx
  800d14:	83 c4 1c             	add    $0x1c,%esp
  800d17:	5b                   	pop    %ebx
  800d18:	5e                   	pop    %esi
  800d19:	5f                   	pop    %edi
  800d1a:	5d                   	pop    %ebp
  800d1b:	c3                   	ret    
  800d1c:	39 ce                	cmp    %ecx,%esi
  800d1e:	77 28                	ja     800d48 <__udivdi3+0x7c>
  800d20:	0f bd fe             	bsr    %esi,%edi
  800d23:	83 f7 1f             	xor    $0x1f,%edi
  800d26:	75 40                	jne    800d68 <__udivdi3+0x9c>
  800d28:	39 ce                	cmp    %ecx,%esi
  800d2a:	72 0a                	jb     800d36 <__udivdi3+0x6a>
  800d2c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800d30:	0f 87 9e 00 00 00    	ja     800dd4 <__udivdi3+0x108>
  800d36:	b8 01 00 00 00       	mov    $0x1,%eax
  800d3b:	89 fa                	mov    %edi,%edx
  800d3d:	83 c4 1c             	add    $0x1c,%esp
  800d40:	5b                   	pop    %ebx
  800d41:	5e                   	pop    %esi
  800d42:	5f                   	pop    %edi
  800d43:	5d                   	pop    %ebp
  800d44:	c3                   	ret    
  800d45:	8d 76 00             	lea    0x0(%esi),%esi
  800d48:	31 ff                	xor    %edi,%edi
  800d4a:	31 c0                	xor    %eax,%eax
  800d4c:	89 fa                	mov    %edi,%edx
  800d4e:	83 c4 1c             	add    $0x1c,%esp
  800d51:	5b                   	pop    %ebx
  800d52:	5e                   	pop    %esi
  800d53:	5f                   	pop    %edi
  800d54:	5d                   	pop    %ebp
  800d55:	c3                   	ret    
  800d56:	66 90                	xchg   %ax,%ax
  800d58:	89 d8                	mov    %ebx,%eax
  800d5a:	f7 f7                	div    %edi
  800d5c:	31 ff                	xor    %edi,%edi
  800d5e:	89 fa                	mov    %edi,%edx
  800d60:	83 c4 1c             	add    $0x1c,%esp
  800d63:	5b                   	pop    %ebx
  800d64:	5e                   	pop    %esi
  800d65:	5f                   	pop    %edi
  800d66:	5d                   	pop    %ebp
  800d67:	c3                   	ret    
  800d68:	bd 20 00 00 00       	mov    $0x20,%ebp
  800d6d:	89 eb                	mov    %ebp,%ebx
  800d6f:	29 fb                	sub    %edi,%ebx
  800d71:	89 f9                	mov    %edi,%ecx
  800d73:	d3 e6                	shl    %cl,%esi
  800d75:	89 c5                	mov    %eax,%ebp
  800d77:	88 d9                	mov    %bl,%cl
  800d79:	d3 ed                	shr    %cl,%ebp
  800d7b:	89 e9                	mov    %ebp,%ecx
  800d7d:	09 f1                	or     %esi,%ecx
  800d7f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800d83:	89 f9                	mov    %edi,%ecx
  800d85:	d3 e0                	shl    %cl,%eax
  800d87:	89 c5                	mov    %eax,%ebp
  800d89:	89 d6                	mov    %edx,%esi
  800d8b:	88 d9                	mov    %bl,%cl
  800d8d:	d3 ee                	shr    %cl,%esi
  800d8f:	89 f9                	mov    %edi,%ecx
  800d91:	d3 e2                	shl    %cl,%edx
  800d93:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d97:	88 d9                	mov    %bl,%cl
  800d99:	d3 e8                	shr    %cl,%eax
  800d9b:	09 c2                	or     %eax,%edx
  800d9d:	89 d0                	mov    %edx,%eax
  800d9f:	89 f2                	mov    %esi,%edx
  800da1:	f7 74 24 0c          	divl   0xc(%esp)
  800da5:	89 d6                	mov    %edx,%esi
  800da7:	89 c3                	mov    %eax,%ebx
  800da9:	f7 e5                	mul    %ebp
  800dab:	39 d6                	cmp    %edx,%esi
  800dad:	72 19                	jb     800dc8 <__udivdi3+0xfc>
  800daf:	74 0b                	je     800dbc <__udivdi3+0xf0>
  800db1:	89 d8                	mov    %ebx,%eax
  800db3:	31 ff                	xor    %edi,%edi
  800db5:	e9 58 ff ff ff       	jmp    800d12 <__udivdi3+0x46>
  800dba:	66 90                	xchg   %ax,%ax
  800dbc:	8b 54 24 08          	mov    0x8(%esp),%edx
  800dc0:	89 f9                	mov    %edi,%ecx
  800dc2:	d3 e2                	shl    %cl,%edx
  800dc4:	39 c2                	cmp    %eax,%edx
  800dc6:	73 e9                	jae    800db1 <__udivdi3+0xe5>
  800dc8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800dcb:	31 ff                	xor    %edi,%edi
  800dcd:	e9 40 ff ff ff       	jmp    800d12 <__udivdi3+0x46>
  800dd2:	66 90                	xchg   %ax,%ax
  800dd4:	31 c0                	xor    %eax,%eax
  800dd6:	e9 37 ff ff ff       	jmp    800d12 <__udivdi3+0x46>
  800ddb:	90                   	nop

00800ddc <__umoddi3>:
  800ddc:	55                   	push   %ebp
  800ddd:	57                   	push   %edi
  800dde:	56                   	push   %esi
  800ddf:	53                   	push   %ebx
  800de0:	83 ec 1c             	sub    $0x1c,%esp
  800de3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800de7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800deb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800def:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800df3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800df7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800dfb:	89 f3                	mov    %esi,%ebx
  800dfd:	89 fa                	mov    %edi,%edx
  800dff:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800e03:	89 34 24             	mov    %esi,(%esp)
  800e06:	85 c0                	test   %eax,%eax
  800e08:	75 1a                	jne    800e24 <__umoddi3+0x48>
  800e0a:	39 f7                	cmp    %esi,%edi
  800e0c:	0f 86 a2 00 00 00    	jbe    800eb4 <__umoddi3+0xd8>
  800e12:	89 c8                	mov    %ecx,%eax
  800e14:	89 f2                	mov    %esi,%edx
  800e16:	f7 f7                	div    %edi
  800e18:	89 d0                	mov    %edx,%eax
  800e1a:	31 d2                	xor    %edx,%edx
  800e1c:	83 c4 1c             	add    $0x1c,%esp
  800e1f:	5b                   	pop    %ebx
  800e20:	5e                   	pop    %esi
  800e21:	5f                   	pop    %edi
  800e22:	5d                   	pop    %ebp
  800e23:	c3                   	ret    
  800e24:	39 f0                	cmp    %esi,%eax
  800e26:	0f 87 ac 00 00 00    	ja     800ed8 <__umoddi3+0xfc>
  800e2c:	0f bd e8             	bsr    %eax,%ebp
  800e2f:	83 f5 1f             	xor    $0x1f,%ebp
  800e32:	0f 84 ac 00 00 00    	je     800ee4 <__umoddi3+0x108>
  800e38:	bf 20 00 00 00       	mov    $0x20,%edi
  800e3d:	29 ef                	sub    %ebp,%edi
  800e3f:	89 fe                	mov    %edi,%esi
  800e41:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800e45:	89 e9                	mov    %ebp,%ecx
  800e47:	d3 e0                	shl    %cl,%eax
  800e49:	89 d7                	mov    %edx,%edi
  800e4b:	89 f1                	mov    %esi,%ecx
  800e4d:	d3 ef                	shr    %cl,%edi
  800e4f:	09 c7                	or     %eax,%edi
  800e51:	89 e9                	mov    %ebp,%ecx
  800e53:	d3 e2                	shl    %cl,%edx
  800e55:	89 14 24             	mov    %edx,(%esp)
  800e58:	89 d8                	mov    %ebx,%eax
  800e5a:	d3 e0                	shl    %cl,%eax
  800e5c:	89 c2                	mov    %eax,%edx
  800e5e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e62:	d3 e0                	shl    %cl,%eax
  800e64:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e68:	8b 44 24 08          	mov    0x8(%esp),%eax
  800e6c:	89 f1                	mov    %esi,%ecx
  800e6e:	d3 e8                	shr    %cl,%eax
  800e70:	09 d0                	or     %edx,%eax
  800e72:	d3 eb                	shr    %cl,%ebx
  800e74:	89 da                	mov    %ebx,%edx
  800e76:	f7 f7                	div    %edi
  800e78:	89 d3                	mov    %edx,%ebx
  800e7a:	f7 24 24             	mull   (%esp)
  800e7d:	89 c6                	mov    %eax,%esi
  800e7f:	89 d1                	mov    %edx,%ecx
  800e81:	39 d3                	cmp    %edx,%ebx
  800e83:	0f 82 87 00 00 00    	jb     800f10 <__umoddi3+0x134>
  800e89:	0f 84 91 00 00 00    	je     800f20 <__umoddi3+0x144>
  800e8f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800e93:	29 f2                	sub    %esi,%edx
  800e95:	19 cb                	sbb    %ecx,%ebx
  800e97:	89 d8                	mov    %ebx,%eax
  800e99:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800e9d:	d3 e0                	shl    %cl,%eax
  800e9f:	89 e9                	mov    %ebp,%ecx
  800ea1:	d3 ea                	shr    %cl,%edx
  800ea3:	09 d0                	or     %edx,%eax
  800ea5:	89 e9                	mov    %ebp,%ecx
  800ea7:	d3 eb                	shr    %cl,%ebx
  800ea9:	89 da                	mov    %ebx,%edx
  800eab:	83 c4 1c             	add    $0x1c,%esp
  800eae:	5b                   	pop    %ebx
  800eaf:	5e                   	pop    %esi
  800eb0:	5f                   	pop    %edi
  800eb1:	5d                   	pop    %ebp
  800eb2:	c3                   	ret    
  800eb3:	90                   	nop
  800eb4:	89 fd                	mov    %edi,%ebp
  800eb6:	85 ff                	test   %edi,%edi
  800eb8:	75 0b                	jne    800ec5 <__umoddi3+0xe9>
  800eba:	b8 01 00 00 00       	mov    $0x1,%eax
  800ebf:	31 d2                	xor    %edx,%edx
  800ec1:	f7 f7                	div    %edi
  800ec3:	89 c5                	mov    %eax,%ebp
  800ec5:	89 f0                	mov    %esi,%eax
  800ec7:	31 d2                	xor    %edx,%edx
  800ec9:	f7 f5                	div    %ebp
  800ecb:	89 c8                	mov    %ecx,%eax
  800ecd:	f7 f5                	div    %ebp
  800ecf:	89 d0                	mov    %edx,%eax
  800ed1:	e9 44 ff ff ff       	jmp    800e1a <__umoddi3+0x3e>
  800ed6:	66 90                	xchg   %ax,%ax
  800ed8:	89 c8                	mov    %ecx,%eax
  800eda:	89 f2                	mov    %esi,%edx
  800edc:	83 c4 1c             	add    $0x1c,%esp
  800edf:	5b                   	pop    %ebx
  800ee0:	5e                   	pop    %esi
  800ee1:	5f                   	pop    %edi
  800ee2:	5d                   	pop    %ebp
  800ee3:	c3                   	ret    
  800ee4:	3b 04 24             	cmp    (%esp),%eax
  800ee7:	72 06                	jb     800eef <__umoddi3+0x113>
  800ee9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800eed:	77 0f                	ja     800efe <__umoddi3+0x122>
  800eef:	89 f2                	mov    %esi,%edx
  800ef1:	29 f9                	sub    %edi,%ecx
  800ef3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ef7:	89 14 24             	mov    %edx,(%esp)
  800efa:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800efe:	8b 44 24 04          	mov    0x4(%esp),%eax
  800f02:	8b 14 24             	mov    (%esp),%edx
  800f05:	83 c4 1c             	add    $0x1c,%esp
  800f08:	5b                   	pop    %ebx
  800f09:	5e                   	pop    %esi
  800f0a:	5f                   	pop    %edi
  800f0b:	5d                   	pop    %ebp
  800f0c:	c3                   	ret    
  800f0d:	8d 76 00             	lea    0x0(%esi),%esi
  800f10:	2b 04 24             	sub    (%esp),%eax
  800f13:	19 fa                	sbb    %edi,%edx
  800f15:	89 d1                	mov    %edx,%ecx
  800f17:	89 c6                	mov    %eax,%esi
  800f19:	e9 71 ff ff ff       	jmp    800e8f <__umoddi3+0xb3>
  800f1e:	66 90                	xchg   %ax,%ax
  800f20:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800f24:	72 ea                	jb     800f10 <__umoddi3+0x134>
  800f26:	89 d9                	mov    %ebx,%ecx
  800f28:	e9 62 ff ff ff       	jmp    800e8f <__umoddi3+0xb3>
