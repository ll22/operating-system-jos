
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start+0xeffffff4>:
.globl _start
_start = RELOC(entry)

.globl entry
entry:
	movw	$0x1234, 0x472			# warm boot
f0100000:	02 b0 ad 1b 02 00    	add    0x21bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fc                   	cld    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <entry>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 
	# sufficient until we set up our real page table in mem_init
	# in lab 2.

	# Load the physical address of entry_pgdir into cr3.  entry_pgdir
	# is defined in entrypgdir.c.
	movl	$(RELOC(entry_pgdir)), %ecx
f0100015:	b9 00 20 11 00       	mov    $0x112000,%ecx
	movl	%ecx, %cr3
f010001a:	0f 22 d9             	mov    %ecx,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %ecx
f010001d:	0f 20 e1             	mov    %cr4,%ecx
	orl	$(CR4_PSE), %ecx
f0100020:	83 c9 10             	or     $0x10,%ecx
	movl	%ecx, %cr4
f0100023:	0f 22 e1             	mov    %ecx,%cr4
	# Turn on paging.
	movl	%cr0, %ecx
f0100026:	0f 20 c1             	mov    %cr0,%ecx
	orl	$(CR0_PE|CR0_PG|CR0_WP), %ecx
f0100029:	81 c9 01 00 01 80    	or     $0x80010001,%ecx
	movl	%ecx, %cr0
f010002f:	0f 22 c1             	mov    %ecx,%cr0

	# Now paging is enabled, but we're still running at a low EIP
	# (why is this okay?).  Jump up above KERNBASE before entering
	# C code.
	mov	$relocated, %ecx
f0100032:	b9 39 00 10 f0       	mov    $0xf0100039,%ecx
	jmp	*%ecx
f0100037:	ff e1                	jmp    *%ecx

f0100039 <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0, %ebp			# nuke frame pointer
f0100039:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f010003e:	bc 00 20 11 f0       	mov    $0xf0112000,%esp

	# pointer to struct multiboot_info
	pushl	%ebx
f0100043:	53                   	push   %ebx
	# saved magic value
	pushl	%eax
f0100044:	50                   	push   %eax

	# now to C code
	call	i386_init
f0100045:	e8 9a 00 00 00       	call   f01000e4 <i386_init>

f010004a <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010004a:	eb fe                	jmp    f010004a <spin>

f010004c <test_backtrace>:
#include <kern/console.h>

// Test the stack backtrace function (lab 1 only)
void
test_backtrace(int x)
{
f010004c:	55                   	push   %ebp
f010004d:	89 e5                	mov    %esp,%ebp
f010004f:	53                   	push   %ebx
f0100050:	83 ec 0c             	sub    $0xc,%esp
f0100053:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("test_backtrace %d\n", x);
f0100056:	53                   	push   %ebx
f0100057:	68 c0 1b 10 f0       	push   $0xf0101bc0
f010005c:	e8 f0 0a 00 00       	call   f0100b51 <cprintf>
	if (x > 0)
f0100061:	83 c4 10             	add    $0x10,%esp
f0100064:	85 db                	test   %ebx,%ebx
f0100066:	7e 0f                	jle    f0100077 <test_backtrace+0x2b>
		test_backtrace(x-1);
f0100068:	83 ec 0c             	sub    $0xc,%esp
f010006b:	4b                   	dec    %ebx
f010006c:	53                   	push   %ebx
f010006d:	e8 da ff ff ff       	call   f010004c <test_backtrace>
f0100072:	83 c4 10             	add    $0x10,%esp
f0100075:	eb 11                	jmp    f0100088 <test_backtrace+0x3c>
	else
		mon_backtrace(0, 0, 0);
f0100077:	83 ec 04             	sub    $0x4,%esp
f010007a:	6a 00                	push   $0x0
f010007c:	6a 00                	push   $0x0
f010007e:	6a 00                	push   $0x0
f0100080:	e8 de 07 00 00       	call   f0100863 <mon_backtrace>
f0100085:	83 c4 10             	add    $0x10,%esp
}
f0100088:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010008b:	c9                   	leave  
f010008c:	c3                   	ret    

f010008d <_panic>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
f010008d:	55                   	push   %ebp
f010008e:	89 e5                	mov    %esp,%ebp
f0100090:	56                   	push   %esi
f0100091:	53                   	push   %ebx
f0100092:	8b 75 10             	mov    0x10(%ebp),%esi
	va_list ap;

	if (panicstr)
f0100095:	83 3d 40 49 11 f0 00 	cmpl   $0x0,0xf0114940
f010009c:	75 37                	jne    f01000d5 <_panic+0x48>
		goto dead;
	panicstr = fmt;
f010009e:	89 35 40 49 11 f0    	mov    %esi,0xf0114940

	// Be extra sure that the machine is in as reasonable state
	asm volatile("cli; cld");
f01000a4:	fa                   	cli    
f01000a5:	fc                   	cld    

	va_start(ap, fmt);
f01000a6:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel panic at %s:%d: ", file, line);
f01000a9:	83 ec 04             	sub    $0x4,%esp
f01000ac:	ff 75 0c             	pushl  0xc(%ebp)
f01000af:	ff 75 08             	pushl  0x8(%ebp)
f01000b2:	68 d3 1b 10 f0       	push   $0xf0101bd3
f01000b7:	e8 95 0a 00 00       	call   f0100b51 <cprintf>
	vcprintf(fmt, ap);
f01000bc:	83 c4 08             	add    $0x8,%esp
f01000bf:	53                   	push   %ebx
f01000c0:	56                   	push   %esi
f01000c1:	e8 65 0a 00 00       	call   f0100b2b <vcprintf>
	cprintf("\n");
f01000c6:	c7 04 24 6e 1c 10 f0 	movl   $0xf0101c6e,(%esp)
f01000cd:	e8 7f 0a 00 00       	call   f0100b51 <cprintf>
	va_end(ap);
f01000d2:	83 c4 10             	add    $0x10,%esp

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f01000d5:	83 ec 0c             	sub    $0xc,%esp
f01000d8:	6a 00                	push   $0x0
f01000da:	e8 db 07 00 00       	call   f01008ba <monitor>
f01000df:	83 c4 10             	add    $0x10,%esp
f01000e2:	eb f1                	jmp    f01000d5 <_panic+0x48>

f01000e4 <i386_init>:
		mon_backtrace(0, 0, 0);
}

void
i386_init(uint32_t magic, uint32_t addr)
{
f01000e4:	55                   	push   %ebp
f01000e5:	89 e5                	mov    %esp,%ebp
f01000e7:	83 ec 0c             	sub    $0xc,%esp
	extern char edata[], end[];

	// Before doing anything else, complete the ELF loading process.
	// Clear the uninitialized global data (BSS) section of our program.
	// This ensures that all static/global variables start out zero.
	memset(edata, 0, end - edata);
f01000ea:	b8 64 4e 11 f0       	mov    $0xf0114e64,%eax
f01000ef:	2d 00 33 11 f0       	sub    $0xf0113300,%eax
f01000f4:	50                   	push   %eax
f01000f5:	6a 00                	push   $0x0
f01000f7:	68 00 33 11 f0       	push   $0xf0113300
f01000fc:	e8 4c 16 00 00       	call   f010174d <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f0100101:	e8 4e 04 00 00       	call   f0100554 <cons_init>

	// Must boot from Multiboot.
	assert(magic == MULTIBOOT_BOOTLOADER_MAGIC);
f0100106:	83 c4 10             	add    $0x10,%esp
f0100109:	81 7d 08 02 b0 ad 2b 	cmpl   $0x2badb002,0x8(%ebp)
f0100110:	74 16                	je     f0100128 <i386_init+0x44>
f0100112:	68 40 1c 10 f0       	push   $0xf0101c40
f0100117:	68 eb 1b 10 f0       	push   $0xf0101beb
f010011c:	6a 27                	push   $0x27
f010011e:	68 00 1c 10 f0       	push   $0xf0101c00
f0100123:	e8 65 ff ff ff       	call   f010008d <_panic>

	cprintf("451 decimal is %o octal!\n", 451);
f0100128:	83 ec 08             	sub    $0x8,%esp
f010012b:	68 c3 01 00 00       	push   $0x1c3
f0100130:	68 0c 1c 10 f0       	push   $0xf0101c0c
f0100135:	e8 17 0a 00 00       	call   f0100b51 <cprintf>

	// Print CPU information.
	cpuid_print();
f010013a:	e8 3f 0d 00 00       	call   f0100e7e <cpuid_print>

	// Initialize e820 memory map.
	e820_init(addr);
f010013f:	83 c4 04             	add    $0x4,%esp
f0100142:	ff 75 0c             	pushl  0xc(%ebp)
f0100145:	e8 a0 08 00 00       	call   f01009ea <e820_init>

	// Test the stack backtrace function (lab 1 only)
	test_backtrace(5);
f010014a:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
f0100151:	e8 f6 fe ff ff       	call   f010004c <test_backtrace>
f0100156:	83 c4 10             	add    $0x10,%esp

	// Drop into the kernel monitor.
	while (1)
		monitor(NULL);
f0100159:	83 ec 0c             	sub    $0xc,%esp
f010015c:	6a 00                	push   $0x0
f010015e:	e8 57 07 00 00       	call   f01008ba <monitor>
f0100163:	83 c4 10             	add    $0x10,%esp
f0100166:	eb f1                	jmp    f0100159 <i386_init+0x75>

f0100168 <_warn>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
f0100168:	55                   	push   %ebp
f0100169:	89 e5                	mov    %esp,%ebp
f010016b:	53                   	push   %ebx
f010016c:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f010016f:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f0100172:	ff 75 0c             	pushl  0xc(%ebp)
f0100175:	ff 75 08             	pushl  0x8(%ebp)
f0100178:	68 26 1c 10 f0       	push   $0xf0101c26
f010017d:	e8 cf 09 00 00       	call   f0100b51 <cprintf>
	vcprintf(fmt, ap);
f0100182:	83 c4 08             	add    $0x8,%esp
f0100185:	53                   	push   %ebx
f0100186:	ff 75 10             	pushl  0x10(%ebp)
f0100189:	e8 9d 09 00 00       	call   f0100b2b <vcprintf>
	cprintf("\n");
f010018e:	c7 04 24 6e 1c 10 f0 	movl   $0xf0101c6e,(%esp)
f0100195:	e8 b7 09 00 00       	call   f0100b51 <cprintf>
	va_end(ap);
}
f010019a:	83 c4 10             	add    $0x10,%esp
f010019d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01001a0:	c9                   	leave  
f01001a1:	c3                   	ret    

f01001a2 <serial_proc_data>:

static bool serial_exists;

static int
serial_proc_data(void)
{
f01001a2:	55                   	push   %ebp
f01001a3:	89 e5                	mov    %esp,%ebp

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01001a5:	ba fd 03 00 00       	mov    $0x3fd,%edx
f01001aa:	ec                   	in     (%dx),%al
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f01001ab:	a8 01                	test   $0x1,%al
f01001ad:	74 0b                	je     f01001ba <serial_proc_data+0x18>
f01001af:	ba f8 03 00 00       	mov    $0x3f8,%edx
f01001b4:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f01001b5:	0f b6 c0             	movzbl %al,%eax
f01001b8:	eb 05                	jmp    f01001bf <serial_proc_data+0x1d>

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f01001ba:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return inb(COM1+COM_RX);
}
f01001bf:	5d                   	pop    %ebp
f01001c0:	c3                   	ret    

f01001c1 <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f01001c1:	55                   	push   %ebp
f01001c2:	89 e5                	mov    %esp,%ebp
f01001c4:	53                   	push   %ebx
f01001c5:	83 ec 04             	sub    $0x4,%esp
f01001c8:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f01001ca:	eb 2b                	jmp    f01001f7 <cons_intr+0x36>
		if (c == 0)
f01001cc:	85 c0                	test   %eax,%eax
f01001ce:	74 27                	je     f01001f7 <cons_intr+0x36>
			continue;
		cons.buf[cons.wpos++] = c;
f01001d0:	8b 0d 24 35 11 f0    	mov    0xf0113524,%ecx
f01001d6:	8d 51 01             	lea    0x1(%ecx),%edx
f01001d9:	89 15 24 35 11 f0    	mov    %edx,0xf0113524
f01001df:	88 81 20 33 11 f0    	mov    %al,-0xfeecce0(%ecx)
		if (cons.wpos == CONSBUFSIZE)
f01001e5:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f01001eb:	75 0a                	jne    f01001f7 <cons_intr+0x36>
			cons.wpos = 0;
f01001ed:	c7 05 24 35 11 f0 00 	movl   $0x0,0xf0113524
f01001f4:	00 00 00 
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f01001f7:	ff d3                	call   *%ebx
f01001f9:	83 f8 ff             	cmp    $0xffffffff,%eax
f01001fc:	75 ce                	jne    f01001cc <cons_intr+0xb>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01001fe:	83 c4 04             	add    $0x4,%esp
f0100201:	5b                   	pop    %ebx
f0100202:	5d                   	pop    %ebp
f0100203:	c3                   	ret    

f0100204 <kbd_proc_data>:
f0100204:	ba 64 00 00 00       	mov    $0x64,%edx
f0100209:	ec                   	in     (%dx),%al
	int c;
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
f010020a:	a8 01                	test   $0x1,%al
f010020c:	0f 84 ee 00 00 00    	je     f0100300 <kbd_proc_data+0xfc>
		return -1;
	// Ignore data from mouse.
	if (stat & KBS_TERR)
f0100212:	a8 20                	test   $0x20,%al
f0100214:	0f 85 ec 00 00 00    	jne    f0100306 <kbd_proc_data+0x102>
f010021a:	ba 60 00 00 00       	mov    $0x60,%edx
f010021f:	ec                   	in     (%dx),%al
f0100220:	88 c2                	mov    %al,%dl
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f0100222:	3c e0                	cmp    $0xe0,%al
f0100224:	75 0d                	jne    f0100233 <kbd_proc_data+0x2f>
		// E0 escape character
		shift |= E0ESC;
f0100226:	83 0d 00 33 11 f0 40 	orl    $0x40,0xf0113300
		return 0;
f010022d:	b8 00 00 00 00       	mov    $0x0,%eax
f0100232:	c3                   	ret    
	} else if (data & 0x80) {
f0100233:	84 c0                	test   %al,%al
f0100235:	79 2e                	jns    f0100265 <kbd_proc_data+0x61>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f0100237:	8b 0d 00 33 11 f0    	mov    0xf0113300,%ecx
f010023d:	f6 c1 40             	test   $0x40,%cl
f0100240:	75 05                	jne    f0100247 <kbd_proc_data+0x43>
f0100242:	83 e0 7f             	and    $0x7f,%eax
f0100245:	88 c2                	mov    %al,%dl
		shift &= ~(shiftcode[data] | E0ESC);
f0100247:	0f b6 c2             	movzbl %dl,%eax
f010024a:	8a 80 c0 1d 10 f0    	mov    -0xfefe240(%eax),%al
f0100250:	83 c8 40             	or     $0x40,%eax
f0100253:	0f b6 c0             	movzbl %al,%eax
f0100256:	f7 d0                	not    %eax
f0100258:	21 c8                	and    %ecx,%eax
f010025a:	a3 00 33 11 f0       	mov    %eax,0xf0113300
		return 0;
f010025f:	b8 00 00 00 00       	mov    $0x0,%eax
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f0100264:	c3                   	ret    
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f0100265:	55                   	push   %ebp
f0100266:	89 e5                	mov    %esp,%ebp
f0100268:	53                   	push   %ebx
f0100269:	83 ec 04             	sub    $0x4,%esp
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
f010026c:	8b 0d 00 33 11 f0    	mov    0xf0113300,%ecx
f0100272:	f6 c1 40             	test   $0x40,%cl
f0100275:	74 0e                	je     f0100285 <kbd_proc_data+0x81>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f0100277:	83 c8 80             	or     $0xffffff80,%eax
f010027a:	88 c2                	mov    %al,%dl
		shift &= ~E0ESC;
f010027c:	83 e1 bf             	and    $0xffffffbf,%ecx
f010027f:	89 0d 00 33 11 f0    	mov    %ecx,0xf0113300
	}

	shift |= shiftcode[data];
f0100285:	0f b6 c2             	movzbl %dl,%eax
	shift ^= togglecode[data];
f0100288:	0f b6 90 c0 1d 10 f0 	movzbl -0xfefe240(%eax),%edx
f010028f:	0b 15 00 33 11 f0    	or     0xf0113300,%edx
f0100295:	0f b6 88 c0 1c 10 f0 	movzbl -0xfefe340(%eax),%ecx
f010029c:	31 ca                	xor    %ecx,%edx
f010029e:	89 15 00 33 11 f0    	mov    %edx,0xf0113300

	c = charcode[shift & (CTL | SHIFT)][data];
f01002a4:	89 d1                	mov    %edx,%ecx
f01002a6:	83 e1 03             	and    $0x3,%ecx
f01002a9:	8b 0c 8d a0 1c 10 f0 	mov    -0xfefe360(,%ecx,4),%ecx
f01002b0:	8a 04 01             	mov    (%ecx,%eax,1),%al
f01002b3:	0f b6 d8             	movzbl %al,%ebx
	if (shift & CAPSLOCK) {
f01002b6:	f6 c2 08             	test   $0x8,%dl
f01002b9:	74 1a                	je     f01002d5 <kbd_proc_data+0xd1>
		if ('a' <= c && c <= 'z')
f01002bb:	89 d8                	mov    %ebx,%eax
f01002bd:	8d 4b 9f             	lea    -0x61(%ebx),%ecx
f01002c0:	83 f9 19             	cmp    $0x19,%ecx
f01002c3:	77 05                	ja     f01002ca <kbd_proc_data+0xc6>
			c += 'A' - 'a';
f01002c5:	83 eb 20             	sub    $0x20,%ebx
f01002c8:	eb 0b                	jmp    f01002d5 <kbd_proc_data+0xd1>
		else if ('A' <= c && c <= 'Z')
f01002ca:	83 e8 41             	sub    $0x41,%eax
f01002cd:	83 f8 19             	cmp    $0x19,%eax
f01002d0:	77 03                	ja     f01002d5 <kbd_proc_data+0xd1>
			c += 'a' - 'A';
f01002d2:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f01002d5:	f7 d2                	not    %edx
f01002d7:	f6 c2 06             	test   $0x6,%dl
f01002da:	75 30                	jne    f010030c <kbd_proc_data+0x108>
f01002dc:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f01002e2:	75 2c                	jne    f0100310 <kbd_proc_data+0x10c>
		cprintf("Rebooting!\n");
f01002e4:	83 ec 0c             	sub    $0xc,%esp
f01002e7:	68 64 1c 10 f0       	push   $0xf0101c64
f01002ec:	e8 60 08 00 00       	call   f0100b51 <cprintf>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01002f1:	ba 92 00 00 00       	mov    $0x92,%edx
f01002f6:	b0 03                	mov    $0x3,%al
f01002f8:	ee                   	out    %al,(%dx)
f01002f9:	83 c4 10             	add    $0x10,%esp
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002fc:	89 d8                	mov    %ebx,%eax
f01002fe:	eb 12                	jmp    f0100312 <kbd_proc_data+0x10e>
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
		return -1;
f0100300:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100305:	c3                   	ret    
	// Ignore data from mouse.
	if (stat & KBS_TERR)
		return -1;
f0100306:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f010030b:	c3                   	ret    
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f010030c:	89 d8                	mov    %ebx,%eax
f010030e:	eb 02                	jmp    f0100312 <kbd_proc_data+0x10e>
f0100310:	89 d8                	mov    %ebx,%eax
}
f0100312:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100315:	c9                   	leave  
f0100316:	c3                   	ret    

f0100317 <cons_putc>:
}

// output a character to the console
static void
cons_putc(int c)
{
f0100317:	55                   	push   %ebp
f0100318:	89 e5                	mov    %esp,%ebp
f010031a:	57                   	push   %edi
f010031b:	56                   	push   %esi
f010031c:	53                   	push   %ebx
f010031d:	83 ec 1c             	sub    $0x1c,%esp
f0100320:	89 c7                	mov    %eax,%edi
f0100322:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100327:	be fd 03 00 00       	mov    $0x3fd,%esi
f010032c:	b9 84 00 00 00       	mov    $0x84,%ecx
f0100331:	eb 06                	jmp    f0100339 <cons_putc+0x22>
f0100333:	89 ca                	mov    %ecx,%edx
f0100335:	ec                   	in     (%dx),%al
f0100336:	ec                   	in     (%dx),%al
f0100337:	ec                   	in     (%dx),%al
f0100338:	ec                   	in     (%dx),%al
f0100339:	89 f2                	mov    %esi,%edx
f010033b:	ec                   	in     (%dx),%al
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f010033c:	a8 20                	test   $0x20,%al
f010033e:	75 03                	jne    f0100343 <cons_putc+0x2c>
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
f0100340:	4b                   	dec    %ebx
f0100341:	75 f0                	jne    f0100333 <cons_putc+0x1c>
f0100343:	89 f8                	mov    %edi,%eax
f0100345:	88 45 e7             	mov    %al,-0x19(%ebp)
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100348:	ba f8 03 00 00       	mov    $0x3f8,%edx
f010034d:	ee                   	out    %al,(%dx)
f010034e:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100353:	be 79 03 00 00       	mov    $0x379,%esi
f0100358:	b9 84 00 00 00       	mov    $0x84,%ecx
f010035d:	eb 06                	jmp    f0100365 <cons_putc+0x4e>
f010035f:	89 ca                	mov    %ecx,%edx
f0100361:	ec                   	in     (%dx),%al
f0100362:	ec                   	in     (%dx),%al
f0100363:	ec                   	in     (%dx),%al
f0100364:	ec                   	in     (%dx),%al
f0100365:	89 f2                	mov    %esi,%edx
f0100367:	ec                   	in     (%dx),%al
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f0100368:	84 c0                	test   %al,%al
f010036a:	78 03                	js     f010036f <cons_putc+0x58>
f010036c:	4b                   	dec    %ebx
f010036d:	75 f0                	jne    f010035f <cons_putc+0x48>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010036f:	ba 78 03 00 00       	mov    $0x378,%edx
f0100374:	8a 45 e7             	mov    -0x19(%ebp),%al
f0100377:	ee                   	out    %al,(%dx)
f0100378:	ba 7a 03 00 00       	mov    $0x37a,%edx
f010037d:	b0 0d                	mov    $0xd,%al
f010037f:	ee                   	out    %al,(%dx)
f0100380:	b0 08                	mov    $0x8,%al
f0100382:	ee                   	out    %al,(%dx)

static void
cga_putc(int c)
{
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f0100383:	f7 c7 00 ff ff ff    	test   $0xffffff00,%edi
f0100389:	75 06                	jne    f0100391 <cons_putc+0x7a>
		c |= 0x0500;
f010038b:	81 cf 00 05 00 00    	or     $0x500,%edi

	switch (c & 0xff) {
f0100391:	89 f8                	mov    %edi,%eax
f0100393:	0f b6 c0             	movzbl %al,%eax
f0100396:	83 f8 09             	cmp    $0x9,%eax
f0100399:	74 75                	je     f0100410 <cons_putc+0xf9>
f010039b:	83 f8 09             	cmp    $0x9,%eax
f010039e:	7f 0a                	jg     f01003aa <cons_putc+0x93>
f01003a0:	83 f8 08             	cmp    $0x8,%eax
f01003a3:	74 14                	je     f01003b9 <cons_putc+0xa2>
f01003a5:	e9 9a 00 00 00       	jmp    f0100444 <cons_putc+0x12d>
f01003aa:	83 f8 0a             	cmp    $0xa,%eax
f01003ad:	74 38                	je     f01003e7 <cons_putc+0xd0>
f01003af:	83 f8 0d             	cmp    $0xd,%eax
f01003b2:	74 3b                	je     f01003ef <cons_putc+0xd8>
f01003b4:	e9 8b 00 00 00       	jmp    f0100444 <cons_putc+0x12d>
	case '\b':
		if (crt_pos > 0) {
f01003b9:	66 a1 28 35 11 f0    	mov    0xf0113528,%ax
f01003bf:	66 85 c0             	test   %ax,%ax
f01003c2:	0f 84 e7 00 00 00    	je     f01004af <cons_putc+0x198>
			crt_pos--;
f01003c8:	48                   	dec    %eax
f01003c9:	66 a3 28 35 11 f0    	mov    %ax,0xf0113528
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f01003cf:	0f b7 c0             	movzwl %ax,%eax
f01003d2:	81 e7 00 ff ff ff    	and    $0xffffff00,%edi
f01003d8:	83 cf 20             	or     $0x20,%edi
f01003db:	8b 15 2c 35 11 f0    	mov    0xf011352c,%edx
f01003e1:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
f01003e5:	eb 7a                	jmp    f0100461 <cons_putc+0x14a>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f01003e7:	66 83 05 28 35 11 f0 	addw   $0x50,0xf0113528
f01003ee:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f01003ef:	66 8b 0d 28 35 11 f0 	mov    0xf0113528,%cx
f01003f6:	bb 50 00 00 00       	mov    $0x50,%ebx
f01003fb:	89 c8                	mov    %ecx,%eax
f01003fd:	ba 00 00 00 00       	mov    $0x0,%edx
f0100402:	66 f7 f3             	div    %bx
f0100405:	29 d1                	sub    %edx,%ecx
f0100407:	66 89 0d 28 35 11 f0 	mov    %cx,0xf0113528
f010040e:	eb 51                	jmp    f0100461 <cons_putc+0x14a>
		break;
	case '\t':
		cons_putc(' ');
f0100410:	b8 20 00 00 00       	mov    $0x20,%eax
f0100415:	e8 fd fe ff ff       	call   f0100317 <cons_putc>
		cons_putc(' ');
f010041a:	b8 20 00 00 00       	mov    $0x20,%eax
f010041f:	e8 f3 fe ff ff       	call   f0100317 <cons_putc>
		cons_putc(' ');
f0100424:	b8 20 00 00 00       	mov    $0x20,%eax
f0100429:	e8 e9 fe ff ff       	call   f0100317 <cons_putc>
		cons_putc(' ');
f010042e:	b8 20 00 00 00       	mov    $0x20,%eax
f0100433:	e8 df fe ff ff       	call   f0100317 <cons_putc>
		cons_putc(' ');
f0100438:	b8 20 00 00 00       	mov    $0x20,%eax
f010043d:	e8 d5 fe ff ff       	call   f0100317 <cons_putc>
f0100442:	eb 1d                	jmp    f0100461 <cons_putc+0x14a>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f0100444:	66 a1 28 35 11 f0    	mov    0xf0113528,%ax
f010044a:	8d 50 01             	lea    0x1(%eax),%edx
f010044d:	66 89 15 28 35 11 f0 	mov    %dx,0xf0113528
f0100454:	0f b7 c0             	movzwl %ax,%eax
f0100457:	8b 15 2c 35 11 f0    	mov    0xf011352c,%edx
f010045d:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f0100461:	66 81 3d 28 35 11 f0 	cmpw   $0x7cf,0xf0113528
f0100468:	cf 07 
f010046a:	76 43                	jbe    f01004af <cons_putc+0x198>
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f010046c:	a1 2c 35 11 f0       	mov    0xf011352c,%eax
f0100471:	83 ec 04             	sub    $0x4,%esp
f0100474:	68 00 0f 00 00       	push   $0xf00
f0100479:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f010047f:	52                   	push   %edx
f0100480:	50                   	push   %eax
f0100481:	e8 14 13 00 00       	call   f010179a <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f0100486:	8b 15 2c 35 11 f0    	mov    0xf011352c,%edx
f010048c:	8d 82 00 0f 00 00    	lea    0xf00(%edx),%eax
f0100492:	81 c2 a0 0f 00 00    	add    $0xfa0,%edx
f0100498:	83 c4 10             	add    $0x10,%esp
f010049b:	66 c7 00 20 07       	movw   $0x720,(%eax)
f01004a0:	83 c0 02             	add    $0x2,%eax
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f01004a3:	39 d0                	cmp    %edx,%eax
f01004a5:	75 f4                	jne    f010049b <cons_putc+0x184>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f01004a7:	66 83 2d 28 35 11 f0 	subw   $0x50,0xf0113528
f01004ae:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f01004af:	8b 0d 30 35 11 f0    	mov    0xf0113530,%ecx
f01004b5:	b0 0e                	mov    $0xe,%al
f01004b7:	89 ca                	mov    %ecx,%edx
f01004b9:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f01004ba:	8d 59 01             	lea    0x1(%ecx),%ebx
f01004bd:	66 a1 28 35 11 f0    	mov    0xf0113528,%ax
f01004c3:	66 c1 e8 08          	shr    $0x8,%ax
f01004c7:	89 da                	mov    %ebx,%edx
f01004c9:	ee                   	out    %al,(%dx)
f01004ca:	b0 0f                	mov    $0xf,%al
f01004cc:	89 ca                	mov    %ecx,%edx
f01004ce:	ee                   	out    %al,(%dx)
f01004cf:	a0 28 35 11 f0       	mov    0xf0113528,%al
f01004d4:	89 da                	mov    %ebx,%edx
f01004d6:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f01004d7:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01004da:	5b                   	pop    %ebx
f01004db:	5e                   	pop    %esi
f01004dc:	5f                   	pop    %edi
f01004dd:	5d                   	pop    %ebp
f01004de:	c3                   	ret    

f01004df <serial_intr>:
}

void
serial_intr(void)
{
	if (serial_exists)
f01004df:	80 3d 34 35 11 f0 00 	cmpb   $0x0,0xf0113534
f01004e6:	74 11                	je     f01004f9 <serial_intr+0x1a>
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f01004e8:	55                   	push   %ebp
f01004e9:	89 e5                	mov    %esp,%ebp
f01004eb:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
		cons_intr(serial_proc_data);
f01004ee:	b8 a2 01 10 f0       	mov    $0xf01001a2,%eax
f01004f3:	e8 c9 fc ff ff       	call   f01001c1 <cons_intr>
}
f01004f8:	c9                   	leave  
f01004f9:	c3                   	ret    

f01004fa <kbd_intr>:
	return c;
}

void
kbd_intr(void)
{
f01004fa:	55                   	push   %ebp
f01004fb:	89 e5                	mov    %esp,%ebp
f01004fd:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f0100500:	b8 04 02 10 f0       	mov    $0xf0100204,%eax
f0100505:	e8 b7 fc ff ff       	call   f01001c1 <cons_intr>
}
f010050a:	c9                   	leave  
f010050b:	c3                   	ret    

f010050c <cons_getc>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f010050c:	55                   	push   %ebp
f010050d:	89 e5                	mov    %esp,%ebp
f010050f:	83 ec 08             	sub    $0x8,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f0100512:	e8 c8 ff ff ff       	call   f01004df <serial_intr>
	kbd_intr();
f0100517:	e8 de ff ff ff       	call   f01004fa <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f010051c:	a1 20 35 11 f0       	mov    0xf0113520,%eax
f0100521:	3b 05 24 35 11 f0    	cmp    0xf0113524,%eax
f0100527:	74 24                	je     f010054d <cons_getc+0x41>
		c = cons.buf[cons.rpos++];
f0100529:	8d 50 01             	lea    0x1(%eax),%edx
f010052c:	89 15 20 35 11 f0    	mov    %edx,0xf0113520
f0100532:	0f b6 80 20 33 11 f0 	movzbl -0xfeecce0(%eax),%eax
		if (cons.rpos == CONSBUFSIZE)
f0100539:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f010053f:	75 11                	jne    f0100552 <cons_getc+0x46>
			cons.rpos = 0;
f0100541:	c7 05 20 35 11 f0 00 	movl   $0x0,0xf0113520
f0100548:	00 00 00 
f010054b:	eb 05                	jmp    f0100552 <cons_getc+0x46>
		return c;
	}
	return 0;
f010054d:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100552:	c9                   	leave  
f0100553:	c3                   	ret    

f0100554 <cons_init>:
}

// initialize the console devices
void
cons_init(void)
{
f0100554:	55                   	push   %ebp
f0100555:	89 e5                	mov    %esp,%ebp
f0100557:	56                   	push   %esi
f0100558:	53                   	push   %ebx
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f0100559:	66 8b 15 00 80 0b f0 	mov    0xf00b8000,%dx
	*cp = (uint16_t) 0xA55A;
f0100560:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f0100567:	5a a5 
	if (*cp != 0xA55A) {
f0100569:	66 a1 00 80 0b f0    	mov    0xf00b8000,%ax
f010056f:	66 3d 5a a5          	cmp    $0xa55a,%ax
f0100573:	74 11                	je     f0100586 <cons_init+0x32>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f0100575:	c7 05 30 35 11 f0 b4 	movl   $0x3b4,0xf0113530
f010057c:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f010057f:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f0100584:	eb 16                	jmp    f010059c <cons_init+0x48>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f0100586:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f010058d:	c7 05 30 35 11 f0 d4 	movl   $0x3d4,0xf0113530
f0100594:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f0100597:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
f010059c:	b0 0e                	mov    $0xe,%al
f010059e:	8b 15 30 35 11 f0    	mov    0xf0113530,%edx
f01005a4:	ee                   	out    %al,(%dx)
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
f01005a5:	8d 5a 01             	lea    0x1(%edx),%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01005a8:	89 da                	mov    %ebx,%edx
f01005aa:	ec                   	in     (%dx),%al
f01005ab:	0f b6 c8             	movzbl %al,%ecx
f01005ae:	c1 e1 08             	shl    $0x8,%ecx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01005b1:	b0 0f                	mov    $0xf,%al
f01005b3:	8b 15 30 35 11 f0    	mov    0xf0113530,%edx
f01005b9:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01005ba:	89 da                	mov    %ebx,%edx
f01005bc:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f01005bd:	89 35 2c 35 11 f0    	mov    %esi,0xf011352c
	crt_pos = pos;
f01005c3:	0f b6 c0             	movzbl %al,%eax
f01005c6:	09 c8                	or     %ecx,%eax
f01005c8:	66 a3 28 35 11 f0    	mov    %ax,0xf0113528
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01005ce:	be fa 03 00 00       	mov    $0x3fa,%esi
f01005d3:	b0 00                	mov    $0x0,%al
f01005d5:	89 f2                	mov    %esi,%edx
f01005d7:	ee                   	out    %al,(%dx)
f01005d8:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005dd:	b0 80                	mov    $0x80,%al
f01005df:	ee                   	out    %al,(%dx)
f01005e0:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f01005e5:	b0 0c                	mov    $0xc,%al
f01005e7:	89 da                	mov    %ebx,%edx
f01005e9:	ee                   	out    %al,(%dx)
f01005ea:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005ef:	b0 00                	mov    $0x0,%al
f01005f1:	ee                   	out    %al,(%dx)
f01005f2:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005f7:	b0 03                	mov    $0x3,%al
f01005f9:	ee                   	out    %al,(%dx)
f01005fa:	ba fc 03 00 00       	mov    $0x3fc,%edx
f01005ff:	b0 00                	mov    $0x0,%al
f0100601:	ee                   	out    %al,(%dx)
f0100602:	ba f9 03 00 00       	mov    $0x3f9,%edx
f0100607:	b0 01                	mov    $0x1,%al
f0100609:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010060a:	ba fd 03 00 00       	mov    $0x3fd,%edx
f010060f:	ec                   	in     (%dx),%al
f0100610:	88 c1                	mov    %al,%cl
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f0100612:	3c ff                	cmp    $0xff,%al
f0100614:	0f 95 05 34 35 11 f0 	setne  0xf0113534
f010061b:	89 f2                	mov    %esi,%edx
f010061d:	ec                   	in     (%dx),%al
f010061e:	89 da                	mov    %ebx,%edx
f0100620:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f0100621:	80 f9 ff             	cmp    $0xff,%cl
f0100624:	75 10                	jne    f0100636 <cons_init+0xe2>
		cprintf("Serial port does not exist!\n");
f0100626:	83 ec 0c             	sub    $0xc,%esp
f0100629:	68 70 1c 10 f0       	push   $0xf0101c70
f010062e:	e8 1e 05 00 00       	call   f0100b51 <cprintf>
f0100633:	83 c4 10             	add    $0x10,%esp
}
f0100636:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0100639:	5b                   	pop    %ebx
f010063a:	5e                   	pop    %esi
f010063b:	5d                   	pop    %ebp
f010063c:	c3                   	ret    

f010063d <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f010063d:	55                   	push   %ebp
f010063e:	89 e5                	mov    %esp,%ebp
f0100640:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f0100643:	8b 45 08             	mov    0x8(%ebp),%eax
f0100646:	e8 cc fc ff ff       	call   f0100317 <cons_putc>
}
f010064b:	c9                   	leave  
f010064c:	c3                   	ret    

f010064d <getchar>:

int
getchar(void)
{
f010064d:	55                   	push   %ebp
f010064e:	89 e5                	mov    %esp,%ebp
f0100650:	83 ec 08             	sub    $0x8,%esp
	int c;

	while ((c = cons_getc()) == 0)
f0100653:	e8 b4 fe ff ff       	call   f010050c <cons_getc>
f0100658:	85 c0                	test   %eax,%eax
f010065a:	74 f7                	je     f0100653 <getchar+0x6>
		/* do nothing */;
	return c;
}
f010065c:	c9                   	leave  
f010065d:	c3                   	ret    

f010065e <iscons>:

int
iscons(int fdnum)
{
f010065e:	55                   	push   %ebp
f010065f:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
}
f0100661:	b8 01 00 00 00       	mov    $0x1,%eax
f0100666:	5d                   	pop    %ebp
f0100667:	c3                   	ret    

f0100668 <mon_help>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f0100668:	55                   	push   %ebp
f0100669:	89 e5                	mov    %esp,%ebp
f010066b:	83 ec 0c             	sub    $0xc,%esp
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f010066e:	68 c0 1e 10 f0       	push   $0xf0101ec0
f0100673:	68 de 1e 10 f0       	push   $0xf0101ede
f0100678:	68 e3 1e 10 f0       	push   $0xf0101ee3
f010067d:	e8 cf 04 00 00       	call   f0100b51 <cprintf>
f0100682:	83 c4 0c             	add    $0xc,%esp
f0100685:	68 a4 1f 10 f0       	push   $0xf0101fa4
f010068a:	68 ec 1e 10 f0       	push   $0xf0101eec
f010068f:	68 e3 1e 10 f0       	push   $0xf0101ee3
f0100694:	e8 b8 04 00 00       	call   f0100b51 <cprintf>
f0100699:	83 c4 0c             	add    $0xc,%esp
f010069c:	68 6f 1c 10 f0       	push   $0xf0101c6f
f01006a1:	68 f5 1e 10 f0       	push   $0xf0101ef5
f01006a6:	68 e3 1e 10 f0       	push   $0xf0101ee3
f01006ab:	e8 a1 04 00 00       	call   f0100b51 <cprintf>
	return 0;
}
f01006b0:	b8 00 00 00 00       	mov    $0x0,%eax
f01006b5:	c9                   	leave  
f01006b6:	c3                   	ret    

f01006b7 <mon_kerninfo>:

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f01006b7:	55                   	push   %ebp
f01006b8:	89 e5                	mov    %esp,%ebp
f01006ba:	83 ec 14             	sub    $0x14,%esp
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f01006bd:	68 ff 1e 10 f0       	push   $0xf0101eff
f01006c2:	e8 8a 04 00 00       	call   f0100b51 <cprintf>
	cprintf("  _start                  %08x (phys)\n", _start);
f01006c7:	83 c4 08             	add    $0x8,%esp
f01006ca:	68 0c 00 10 00       	push   $0x10000c
f01006cf:	68 cc 1f 10 f0       	push   $0xf0101fcc
f01006d4:	e8 78 04 00 00       	call   f0100b51 <cprintf>
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
f01006d9:	83 c4 0c             	add    $0xc,%esp
f01006dc:	68 0c 00 10 00       	push   $0x10000c
f01006e1:	68 0c 00 10 f0       	push   $0xf010000c
f01006e6:	68 f4 1f 10 f0       	push   $0xf0101ff4
f01006eb:	e8 61 04 00 00       	call   f0100b51 <cprintf>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f01006f0:	83 c4 0c             	add    $0xc,%esp
f01006f3:	68 b1 1b 10 00       	push   $0x101bb1
f01006f8:	68 b1 1b 10 f0       	push   $0xf0101bb1
f01006fd:	68 18 20 10 f0       	push   $0xf0102018
f0100702:	e8 4a 04 00 00       	call   f0100b51 <cprintf>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f0100707:	83 c4 0c             	add    $0xc,%esp
f010070a:	68 00 33 11 00       	push   $0x113300
f010070f:	68 00 33 11 f0       	push   $0xf0113300
f0100714:	68 3c 20 10 f0       	push   $0xf010203c
f0100719:	e8 33 04 00 00       	call   f0100b51 <cprintf>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f010071e:	83 c4 0c             	add    $0xc,%esp
f0100721:	68 64 4e 11 00       	push   $0x114e64
f0100726:	68 64 4e 11 f0       	push   $0xf0114e64
f010072b:	68 60 20 10 f0       	push   $0xf0102060
f0100730:	e8 1c 04 00 00       	call   f0100b51 <cprintf>
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
f0100735:	b8 63 52 11 f0       	mov    $0xf0115263,%eax
f010073a:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
f010073f:	83 c4 08             	add    $0x8,%esp
f0100742:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f0100747:	89 c2                	mov    %eax,%edx
f0100749:	85 c0                	test   %eax,%eax
f010074b:	79 06                	jns    f0100753 <mon_kerninfo+0x9c>
f010074d:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f0100753:	c1 fa 0a             	sar    $0xa,%edx
f0100756:	52                   	push   %edx
f0100757:	68 84 20 10 f0       	push   $0xf0102084
f010075c:	e8 f0 03 00 00       	call   f0100b51 <cprintf>
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
f0100761:	b8 00 00 00 00       	mov    $0x0,%eax
f0100766:	c9                   	leave  
f0100767:	c3                   	ret    

f0100768 <print_stack_Info>:
ebp f010ff78  eip f01008ae  args 00000001 f010ff8c 00000000 f0110580 00000000
       kern/monitor.c:143: monitor+106
       (filename, line#, function name+offset)
*/
void print_stack_Info(uint32_t esp, uint32_t* return_Addr)
{
f0100768:	55                   	push   %ebp
f0100769:	89 e5                	mov    %esp,%ebp
f010076b:	57                   	push   %edi
f010076c:	56                   	push   %esi
f010076d:	53                   	push   %ebx
f010076e:	83 ec 30             	sub    $0x30,%esp
f0100771:	8b 75 0c             	mov    0xc(%ebp),%esi

	//if(0==debuginfo_eip((uintptr_t)(*return_Addr), info)){

	//}

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
f0100774:	ff 36                	pushl  (%esi)
f0100776:	ff 75 08             	pushl  0x8(%ebp)
f0100779:	68 18 1f 10 f0       	push   $0xf0101f18
f010077e:	e8 ce 03 00 00       	call   f0100b51 <cprintf>
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
f0100783:	c7 04 24 2b 1f 10 f0 	movl   $0xf0101f2b,(%esp)
f010078a:	e8 c2 03 00 00       	call   f0100b51 <cprintf>
f010078f:	8d 5e 04             	lea    0x4(%esi),%ebx
f0100792:	8d 7e 18             	lea    0x18(%esi),%edi
f0100795:	83 c4 10             	add    $0x10,%esp
	for(int i=1; i<6; i++){
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
f0100798:	83 ec 08             	sub    $0x8,%esp
f010079b:	ff 33                	pushl  (%ebx)
f010079d:	68 25 1f 10 f0       	push   $0xf0101f25
f01007a2:	e8 aa 03 00 00       	call   f0100b51 <cprintf>
f01007a7:	83 c3 04             	add    $0x4,%ebx

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
	for(int i=1; i<6; i++){
f01007aa:	83 c4 10             	add    $0x10,%esp
f01007ad:	39 fb                	cmp    %edi,%ebx
f01007af:	75 e7                	jne    f0100798 <print_stack_Info+0x30>
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
	}
	cprintf("\n");
f01007b1:	83 ec 0c             	sub    $0xc,%esp
f01007b4:	68 6e 1c 10 f0       	push   $0xf0101c6e
f01007b9:	e8 93 03 00 00       	call   f0100b51 <cprintf>

	//print more readable info
	//struct Eipdebuginfo* info_Ptr;
	debuginfo_eip((uintptr_t)(*return_Addr), info_Ptr); //get the info anyway
f01007be:	83 c4 08             	add    $0x8,%esp
f01007c1:	8d 45 d0             	lea    -0x30(%ebp),%eax
f01007c4:	50                   	push   %eax
f01007c5:	ff 36                	pushl  (%esi)
f01007c7:	e8 8c 04 00 00       	call   f0100c58 <debuginfo_eip>
	//print file name
	cprintf("%s", (info_Ptr->eip_file) );
f01007cc:	83 c4 08             	add    $0x8,%esp
f01007cf:	ff 75 d0             	pushl  -0x30(%ebp)
f01007d2:	68 fd 1b 10 f0       	push   $0xf0101bfd
f01007d7:	e8 75 03 00 00       	call   f0100b51 <cprintf>
	cprintf(":");
f01007dc:	c7 04 24 32 1f 10 f0 	movl   $0xf0101f32,(%esp)
f01007e3:	e8 69 03 00 00       	call   f0100b51 <cprintf>

	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
f01007e8:	83 c4 08             	add    $0x8,%esp
f01007eb:	ff 75 d4             	pushl  -0x2c(%ebp)
f01007ee:	68 5e 27 10 f0       	push   $0xf010275e
f01007f3:	e8 59 03 00 00       	call   f0100b51 <cprintf>
	cprintf(": ");
f01007f8:	c7 04 24 e8 1b 10 f0 	movl   $0xf0101be8,(%esp)
f01007ff:	e8 4d 03 00 00       	call   f0100b51 <cprintf>
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f0100804:	83 c4 10             	add    $0x10,%esp
f0100807:	bb 00 00 00 00       	mov    $0x0,%ebx
f010080c:	eb 19                	jmp    f0100827 <print_stack_Info+0xbf>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
f010080e:	83 ec 08             	sub    $0x8,%esp
f0100811:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100814:	0f be 04 18          	movsbl (%eax,%ebx,1),%eax
f0100818:	50                   	push   %eax
f0100819:	68 34 1f 10 f0       	push   $0xf0101f34
f010081e:	e8 2e 03 00 00       	call   f0100b51 <cprintf>
	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
	cprintf(": ");
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f0100823:	43                   	inc    %ebx
f0100824:	83 c4 10             	add    $0x10,%esp
f0100827:	3b 5d dc             	cmp    -0x24(%ebp),%ebx
f010082a:	7c e2                	jl     f010080e <print_stack_Info+0xa6>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
	}
	//print offset number
	cprintf("+");
f010082c:	83 ec 0c             	sub    $0xc,%esp
f010082f:	68 37 1f 10 f0       	push   $0xf0101f37
f0100834:	e8 18 03 00 00       	call   f0100b51 <cprintf>
	cprintf("%d", ( (int)(*return_Addr) - (int)(info_Ptr->eip_fn_addr) ) );
f0100839:	83 c4 08             	add    $0x8,%esp
f010083c:	8b 06                	mov    (%esi),%eax
f010083e:	2b 45 e0             	sub    -0x20(%ebp),%eax
f0100841:	50                   	push   %eax
f0100842:	68 5e 27 10 f0       	push   $0xf010275e
f0100847:	e8 05 03 00 00       	call   f0100b51 <cprintf>
	cprintf("\n");
f010084c:	c7 04 24 6e 1c 10 f0 	movl   $0xf0101c6e,(%esp)
f0100853:	e8 f9 02 00 00       	call   f0100b51 <cprintf>
}
f0100858:	83 c4 10             	add    $0x10,%esp
f010085b:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010085e:	5b                   	pop    %ebx
f010085f:	5e                   	pop    %esi
f0100860:	5f                   	pop    %edi
f0100861:	5d                   	pop    %ebp
f0100862:	c3                   	ret    

f0100863 <mon_backtrace>:


*/
int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
f0100863:	55                   	push   %ebp
f0100864:	89 e5                	mov    %esp,%ebp
f0100866:	56                   	push   %esi
f0100867:	53                   	push   %ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f0100868:	89 ee                	mov    %ebp,%esi
	//function prologue
	//f0100751:	55                   	push   %ebp
	//f0100752:	89 e5                	mov    %esp,%ebp
	// Your code here. -.-
	uint32_t esp = read_ebp(); //since mov %esp,%ebp
	uint32_t ebp = *( (uint32_t*)esp );
f010086a:	8b 1e                	mov    (%esi),%ebx
	uint32_t* return_Addr = (uint32_t*)(esp+4);
	//start print information
	cprintf("Stack backtrace:\n");
f010086c:	83 ec 0c             	sub    $0xc,%esp
f010086f:	68 39 1f 10 f0       	push   $0xf0101f39
f0100874:	e8 d8 02 00 00       	call   f0100b51 <cprintf>
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
f0100879:	83 c4 08             	add    $0x8,%esp
f010087c:	8d 46 04             	lea    0x4(%esi),%eax
f010087f:	50                   	push   %eax
f0100880:	56                   	push   %esi
f0100881:	e8 e2 fe ff ff       	call   f0100768 <print_stack_Info>
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100886:	83 c4 10             	add    $0x10,%esp
f0100889:	eb 12                	jmp    f010089d <mon_backtrace+0x3a>
		return_Addr = ((uint32_t*)ebp+1);
		//ebp = *((uint32_t*)ebp); //trace back to the last ebp
		//cprintf("ebp %08x eip %08x\n", ebp, return_Addr);  //the current ebp == esp
		print_stack_Info(ebp, return_Addr);
f010088b:	83 ec 08             	sub    $0x8,%esp
f010088e:	8d 43 04             	lea    0x4(%ebx),%eax
f0100891:	50                   	push   %eax
f0100892:	53                   	push   %ebx
f0100893:	e8 d0 fe ff ff       	call   f0100768 <print_stack_Info>
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
f0100898:	8b 1b                	mov    (%ebx),%ebx
f010089a:	83 c4 10             	add    $0x10,%esp
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f010089d:	85 db                	test   %ebx,%ebx
f010089f:	75 ea                	jne    f010088b <mon_backtrace+0x28>
		print_stack_Info(ebp, return_Addr);
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
	}

	//Not print the 1st stack address since it is the kernel allocated the beginning of the stack
	cprintf("Stoppp Stack backtrace:\n ");
f01008a1:	83 ec 0c             	sub    $0xc,%esp
f01008a4:	68 4b 1f 10 f0       	push   $0xf0101f4b
f01008a9:	e8 a3 02 00 00       	call   f0100b51 <cprintf>

	//cprintf("argc is %08x\n", argc);
	return 0;
}
f01008ae:	b8 00 00 00 00       	mov    $0x0,%eax
f01008b3:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01008b6:	5b                   	pop    %ebx
f01008b7:	5e                   	pop    %esi
f01008b8:	5d                   	pop    %ebp
f01008b9:	c3                   	ret    

f01008ba <monitor>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f01008ba:	55                   	push   %ebp
f01008bb:	89 e5                	mov    %esp,%ebp
f01008bd:	57                   	push   %edi
f01008be:	56                   	push   %esi
f01008bf:	53                   	push   %ebx
f01008c0:	83 ec 58             	sub    $0x58,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f01008c3:	68 b0 20 10 f0       	push   $0xf01020b0
f01008c8:	e8 84 02 00 00       	call   f0100b51 <cprintf>
	cprintf("Type 'help' for a list of commands.\n");
f01008cd:	c7 04 24 d4 20 10 f0 	movl   $0xf01020d4,(%esp)
f01008d4:	e8 78 02 00 00       	call   f0100b51 <cprintf>
f01008d9:	83 c4 10             	add    $0x10,%esp


	while (1) {
		buf = readline("K> ");
f01008dc:	83 ec 0c             	sub    $0xc,%esp
f01008df:	68 65 1f 10 f0       	push   $0xf0101f65
f01008e4:	e8 17 0c 00 00       	call   f0101500 <readline>
f01008e9:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f01008eb:	83 c4 10             	add    $0x10,%esp
f01008ee:	85 c0                	test   %eax,%eax
f01008f0:	74 ea                	je     f01008dc <monitor+0x22>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f01008f2:	c7 45 a8 00 00 00 00 	movl   $0x0,-0x58(%ebp)
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f01008f9:	be 00 00 00 00       	mov    $0x0,%esi
f01008fe:	eb 0a                	jmp    f010090a <monitor+0x50>
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f0100900:	c6 03 00             	movb   $0x0,(%ebx)
f0100903:	89 f7                	mov    %esi,%edi
f0100905:	8d 5b 01             	lea    0x1(%ebx),%ebx
f0100908:	89 fe                	mov    %edi,%esi
	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f010090a:	8a 03                	mov    (%ebx),%al
f010090c:	84 c0                	test   %al,%al
f010090e:	74 60                	je     f0100970 <monitor+0xb6>
f0100910:	83 ec 08             	sub    $0x8,%esp
f0100913:	0f be c0             	movsbl %al,%eax
f0100916:	50                   	push   %eax
f0100917:	68 69 1f 10 f0       	push   $0xf0101f69
f010091c:	e8 f7 0d 00 00       	call   f0101718 <strchr>
f0100921:	83 c4 10             	add    $0x10,%esp
f0100924:	85 c0                	test   %eax,%eax
f0100926:	75 d8                	jne    f0100900 <monitor+0x46>
			*buf++ = 0;
		if (*buf == 0)
f0100928:	80 3b 00             	cmpb   $0x0,(%ebx)
f010092b:	74 43                	je     f0100970 <monitor+0xb6>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f010092d:	83 fe 0f             	cmp    $0xf,%esi
f0100930:	75 14                	jne    f0100946 <monitor+0x8c>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f0100932:	83 ec 08             	sub    $0x8,%esp
f0100935:	6a 10                	push   $0x10
f0100937:	68 6e 1f 10 f0       	push   $0xf0101f6e
f010093c:	e8 10 02 00 00       	call   f0100b51 <cprintf>
f0100941:	83 c4 10             	add    $0x10,%esp
f0100944:	eb 96                	jmp    f01008dc <monitor+0x22>
			return 0;
		}
		argv[argc++] = buf;
f0100946:	8d 7e 01             	lea    0x1(%esi),%edi
f0100949:	89 5c b5 a8          	mov    %ebx,-0x58(%ebp,%esi,4)
f010094d:	eb 01                	jmp    f0100950 <monitor+0x96>
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f010094f:	43                   	inc    %ebx
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
f0100950:	8a 03                	mov    (%ebx),%al
f0100952:	84 c0                	test   %al,%al
f0100954:	74 b2                	je     f0100908 <monitor+0x4e>
f0100956:	83 ec 08             	sub    $0x8,%esp
f0100959:	0f be c0             	movsbl %al,%eax
f010095c:	50                   	push   %eax
f010095d:	68 69 1f 10 f0       	push   $0xf0101f69
f0100962:	e8 b1 0d 00 00       	call   f0101718 <strchr>
f0100967:	83 c4 10             	add    $0x10,%esp
f010096a:	85 c0                	test   %eax,%eax
f010096c:	74 e1                	je     f010094f <monitor+0x95>
f010096e:	eb 98                	jmp    f0100908 <monitor+0x4e>
			buf++;
	}
	argv[argc] = 0;
f0100970:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
f0100977:	00 

	// Lookup and invoke the command
	if (argc == 0)
f0100978:	85 f6                	test   %esi,%esi
f010097a:	0f 84 5c ff ff ff    	je     f01008dc <monitor+0x22>
f0100980:	bf 00 21 10 f0       	mov    $0xf0102100,%edi
f0100985:	bb 00 00 00 00       	mov    $0x0,%ebx
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f010098a:	83 ec 08             	sub    $0x8,%esp
f010098d:	ff 37                	pushl  (%edi)
f010098f:	ff 75 a8             	pushl  -0x58(%ebp)
f0100992:	e8 2d 0d 00 00       	call   f01016c4 <strcmp>
f0100997:	83 c4 10             	add    $0x10,%esp
f010099a:	85 c0                	test   %eax,%eax
f010099c:	75 23                	jne    f01009c1 <monitor+0x107>
			return commands[i].func(argc, argv, tf);
f010099e:	83 ec 04             	sub    $0x4,%esp
f01009a1:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
f01009a4:	01 c3                	add    %eax,%ebx
f01009a6:	ff 75 08             	pushl  0x8(%ebp)
f01009a9:	8d 45 a8             	lea    -0x58(%ebp),%eax
f01009ac:	50                   	push   %eax
f01009ad:	56                   	push   %esi
f01009ae:	ff 14 9d 08 21 10 f0 	call   *-0xfefdef8(,%ebx,4)


	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f01009b5:	83 c4 10             	add    $0x10,%esp
f01009b8:	85 c0                	test   %eax,%eax
f01009ba:	78 26                	js     f01009e2 <monitor+0x128>
f01009bc:	e9 1b ff ff ff       	jmp    f01008dc <monitor+0x22>
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
f01009c1:	43                   	inc    %ebx
f01009c2:	83 c7 0c             	add    $0xc,%edi
f01009c5:	83 fb 03             	cmp    $0x3,%ebx
f01009c8:	75 c0                	jne    f010098a <monitor+0xd0>
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f01009ca:	83 ec 08             	sub    $0x8,%esp
f01009cd:	ff 75 a8             	pushl  -0x58(%ebp)
f01009d0:	68 8b 1f 10 f0       	push   $0xf0101f8b
f01009d5:	e8 77 01 00 00       	call   f0100b51 <cprintf>
f01009da:	83 c4 10             	add    $0x10,%esp
f01009dd:	e9 fa fe ff ff       	jmp    f01008dc <monitor+0x22>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f01009e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01009e5:	5b                   	pop    %ebx
f01009e6:	5e                   	pop    %esi
f01009e7:	5f                   	pop    %edi
f01009e8:	5d                   	pop    %ebp
f01009e9:	c3                   	ret    

f01009ea <e820_init>:

// This function may ONLY be used during initialization,
// before page_init().
void
e820_init(physaddr_t mbi_pa)
{
f01009ea:	55                   	push   %ebp
f01009eb:	89 e5                	mov    %esp,%ebp
f01009ed:	57                   	push   %edi
f01009ee:	56                   	push   %esi
f01009ef:	53                   	push   %ebx
f01009f0:	83 ec 1c             	sub    $0x1c,%esp
f01009f3:	8b 75 08             	mov    0x8(%ebp),%esi
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
f01009f6:	f6 06 40             	testb  $0x40,(%esi)
f01009f9:	75 16                	jne    f0100a11 <e820_init+0x27>
f01009fb:	68 24 21 10 f0       	push   $0xf0102124
f0100a00:	68 eb 1b 10 f0       	push   $0xf0101beb
f0100a05:	6a 26                	push   $0x26
f0100a07:	68 77 21 10 f0       	push   $0xf0102177
f0100a0c:	e8 7c f6 ff ff       	call   f010008d <_panic>
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);
f0100a11:	8b 56 30             	mov    0x30(%esi),%edx
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
f0100a14:	83 ec 04             	sub    $0x4,%esp
f0100a17:	89 d0                	mov    %edx,%eax
f0100a19:	03 46 2c             	add    0x2c(%esi),%eax
f0100a1c:	48                   	dec    %eax
f0100a1d:	50                   	push   %eax
f0100a1e:	52                   	push   %edx
f0100a1f:	68 48 21 10 f0       	push   $0xf0102148
f0100a24:	e8 28 01 00 00       	call   f0100b51 <cprintf>
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
f0100a29:	8b 5e 30             	mov    0x30(%esi),%ebx
	addr_end = mbi->mmap_addr + mbi->mmap_length;
f0100a2c:	89 d8                	mov    %ebx,%eax
f0100a2e:	03 46 2c             	add    0x2c(%esi),%eax
f0100a31:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0100a34:	c7 45 dc 64 49 11 f0 	movl   $0xf0114964,-0x24(%ebp)
	for (i = 0; addr < addr_end; ++i) {
f0100a3b:	83 c4 10             	add    $0x10,%esp
f0100a3e:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f0100a45:	e9 b5 00 00 00       	jmp    f0100aff <e820_init+0x115>
		struct multiboot_mmap_entry *e;

		// Print memory mapping.
		assert(addr_end - addr >= sizeof(*e));
f0100a4a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100a4d:	29 d8                	sub    %ebx,%eax
f0100a4f:	83 f8 17             	cmp    $0x17,%eax
f0100a52:	77 16                	ja     f0100a6a <e820_init+0x80>
f0100a54:	68 83 21 10 f0       	push   $0xf0102183
f0100a59:	68 eb 1b 10 f0       	push   $0xf0101beb
f0100a5e:	6a 30                	push   $0x30
f0100a60:	68 77 21 10 f0       	push   $0xf0102177
f0100a65:	e8 23 f6 ff ff       	call   f010008d <_panic>
		e = (struct multiboot_mmap_entry *)addr;
f0100a6a:	89 de                	mov    %ebx,%esi
		cprintf("  [mem %08p-%08p] ",
f0100a6c:	8b 53 04             	mov    0x4(%ebx),%edx
f0100a6f:	83 ec 04             	sub    $0x4,%esp
f0100a72:	89 d0                	mov    %edx,%eax
f0100a74:	03 43 0c             	add    0xc(%ebx),%eax
f0100a77:	48                   	dec    %eax
f0100a78:	50                   	push   %eax
f0100a79:	52                   	push   %edx
f0100a7a:	68 a1 21 10 f0       	push   $0xf01021a1
f0100a7f:	e8 cd 00 00 00       	call   f0100b51 <cprintf>
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
f0100a84:	8b 43 14             	mov    0x14(%ebx),%eax
	"unusable",
};

static void
print_e820_map_type(uint32_t type) {
	switch (type) {
f0100a87:	83 c4 10             	add    $0x10,%esp
f0100a8a:	8d 50 ff             	lea    -0x1(%eax),%edx
f0100a8d:	83 fa 04             	cmp    $0x4,%edx
f0100a90:	77 14                	ja     f0100aa6 <e820_init+0xbc>
	case 1 ... 5:
		cprintf(e820_map_types[type - 1]);
f0100a92:	83 ec 0c             	sub    $0xc,%esp
f0100a95:	ff 34 85 00 22 10 f0 	pushl  -0xfefde00(,%eax,4)
f0100a9c:	e8 b0 00 00 00       	call   f0100b51 <cprintf>
f0100aa1:	83 c4 10             	add    $0x10,%esp
f0100aa4:	eb 11                	jmp    f0100ab7 <e820_init+0xcd>
		break;
	default:
		cprintf("type %u", type);
f0100aa6:	83 ec 08             	sub    $0x8,%esp
f0100aa9:	50                   	push   %eax
f0100aaa:	68 b4 21 10 f0       	push   $0xf01021b4
f0100aaf:	e8 9d 00 00 00       	call   f0100b51 <cprintf>
f0100ab4:	83 c4 10             	add    $0x10,%esp
		e = (struct multiboot_mmap_entry *)addr;
		cprintf("  [mem %08p-%08p] ",
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
		cprintf("\n");
f0100ab7:	83 ec 0c             	sub    $0xc,%esp
f0100aba:	68 6e 1c 10 f0       	push   $0xf0101c6e
f0100abf:	e8 8d 00 00 00       	call   f0100b51 <cprintf>

		// Save a copy.
		assert(i < E820_NR_MAX);
f0100ac4:	83 c4 10             	add    $0x10,%esp
f0100ac7:	83 7d e0 40          	cmpl   $0x40,-0x20(%ebp)
f0100acb:	75 16                	jne    f0100ae3 <e820_init+0xf9>
f0100acd:	68 bc 21 10 f0       	push   $0xf01021bc
f0100ad2:	68 eb 1b 10 f0       	push   $0xf0101beb
f0100ad7:	6a 39                	push   $0x39
f0100ad9:	68 77 21 10 f0       	push   $0xf0102177
f0100ade:	e8 aa f5 ff ff       	call   f010008d <_panic>
		e820_map.entries[i] = e->e820;
f0100ae3:	89 f0                	mov    %esi,%eax
f0100ae5:	8d 76 04             	lea    0x4(%esi),%esi
f0100ae8:	b9 05 00 00 00       	mov    $0x5,%ecx
f0100aed:	8b 7d dc             	mov    -0x24(%ebp),%edi
f0100af0:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		addr += (e->size + 4);
f0100af2:	8b 00                	mov    (%eax),%eax
f0100af4:	8d 5c 03 04          	lea    0x4(%ebx,%eax,1),%ebx
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
	addr_end = mbi->mmap_addr + mbi->mmap_length;
	for (i = 0; addr < addr_end; ++i) {
f0100af8:	ff 45 e0             	incl   -0x20(%ebp)
f0100afb:	83 45 dc 14          	addl   $0x14,-0x24(%ebp)
f0100aff:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f0100b02:	0f 82 42 ff ff ff    	jb     f0100a4a <e820_init+0x60>
		// Save a copy.
		assert(i < E820_NR_MAX);
		e820_map.entries[i] = e->e820;
		addr += (e->size + 4);
	}
	e820_map.nr = i;
f0100b08:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100b0b:	a3 60 49 11 f0       	mov    %eax,0xf0114960
}
f0100b10:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100b13:	5b                   	pop    %ebx
f0100b14:	5e                   	pop    %esi
f0100b15:	5f                   	pop    %edi
f0100b16:	5d                   	pop    %ebp
f0100b17:	c3                   	ret    

f0100b18 <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0100b18:	55                   	push   %ebp
f0100b19:	89 e5                	mov    %esp,%ebp
f0100b1b:	83 ec 14             	sub    $0x14,%esp
	cputchar(ch);
f0100b1e:	ff 75 08             	pushl  0x8(%ebp)
f0100b21:	e8 17 fb ff ff       	call   f010063d <cputchar>
	*cnt++;
}
f0100b26:	83 c4 10             	add    $0x10,%esp
f0100b29:	c9                   	leave  
f0100b2a:	c3                   	ret    

f0100b2b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f0100b2b:	55                   	push   %ebp
f0100b2c:	89 e5                	mov    %esp,%ebp
f0100b2e:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f0100b31:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0100b38:	ff 75 0c             	pushl  0xc(%ebp)
f0100b3b:	ff 75 08             	pushl  0x8(%ebp)
f0100b3e:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0100b41:	50                   	push   %eax
f0100b42:	68 18 0b 10 f0       	push   $0xf0100b18
f0100b47:	e8 b9 05 00 00       	call   f0101105 <vprintfmt>
	return cnt;
}
f0100b4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100b4f:	c9                   	leave  
f0100b50:	c3                   	ret    

f0100b51 <cprintf>:

int
cprintf(const char *fmt, ...)
{
f0100b51:	55                   	push   %ebp
f0100b52:	89 e5                	mov    %esp,%ebp
f0100b54:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0100b57:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
f0100b5a:	50                   	push   %eax
f0100b5b:	ff 75 08             	pushl  0x8(%ebp)
f0100b5e:	e8 c8 ff ff ff       	call   f0100b2b <vcprintf>
	va_end(ap);

	return cnt;
}
f0100b63:	c9                   	leave  
f0100b64:	c3                   	ret    

f0100b65 <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f0100b65:	55                   	push   %ebp
f0100b66:	89 e5                	mov    %esp,%ebp
f0100b68:	57                   	push   %edi
f0100b69:	56                   	push   %esi
f0100b6a:	53                   	push   %ebx
f0100b6b:	83 ec 14             	sub    $0x14,%esp
f0100b6e:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0100b71:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0100b74:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f0100b77:	8b 7d 08             	mov    0x8(%ebp),%edi
	int l = *region_left, r = *region_right, any_matches = 0;
f0100b7a:	8b 1a                	mov    (%edx),%ebx
f0100b7c:	8b 01                	mov    (%ecx),%eax
f0100b7e:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100b81:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	while (l <= r) {
f0100b88:	eb 7e                	jmp    f0100c08 <stab_binsearch+0xa3>
		int true_m = (l + r) / 2, m = true_m;
f0100b8a:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100b8d:	01 d8                	add    %ebx,%eax
f0100b8f:	89 c6                	mov    %eax,%esi
f0100b91:	c1 ee 1f             	shr    $0x1f,%esi
f0100b94:	01 c6                	add    %eax,%esi
f0100b96:	d1 fe                	sar    %esi
f0100b98:	8d 04 36             	lea    (%esi,%esi,1),%eax
f0100b9b:	01 f0                	add    %esi,%eax
f0100b9d:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f0100ba0:	8d 54 81 04          	lea    0x4(%ecx,%eax,4),%edx
f0100ba4:	89 f0                	mov    %esi,%eax

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0100ba6:	eb 01                	jmp    f0100ba9 <stab_binsearch+0x44>
			m--;
f0100ba8:	48                   	dec    %eax

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0100ba9:	39 c3                	cmp    %eax,%ebx
f0100bab:	7f 0c                	jg     f0100bb9 <stab_binsearch+0x54>
f0100bad:	0f b6 0a             	movzbl (%edx),%ecx
f0100bb0:	83 ea 0c             	sub    $0xc,%edx
f0100bb3:	39 f9                	cmp    %edi,%ecx
f0100bb5:	75 f1                	jne    f0100ba8 <stab_binsearch+0x43>
f0100bb7:	eb 05                	jmp    f0100bbe <stab_binsearch+0x59>
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f0100bb9:	8d 5e 01             	lea    0x1(%esi),%ebx
			continue;
f0100bbc:	eb 4a                	jmp    f0100c08 <stab_binsearch+0xa3>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f0100bbe:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0100bc1:	01 c2                	add    %eax,%edx
f0100bc3:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f0100bc6:	8b 54 91 08          	mov    0x8(%ecx,%edx,4),%edx
f0100bca:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0100bcd:	76 11                	jbe    f0100be0 <stab_binsearch+0x7b>
			*region_left = m;
f0100bcf:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f0100bd2:	89 03                	mov    %eax,(%ebx)
			l = true_m + 1;
f0100bd4:	8d 5e 01             	lea    0x1(%esi),%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100bd7:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0100bde:	eb 28                	jmp    f0100c08 <stab_binsearch+0xa3>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f0100be0:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0100be3:	73 12                	jae    f0100bf7 <stab_binsearch+0x92>
			*region_right = m - 1;
f0100be5:	48                   	dec    %eax
f0100be6:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100be9:	8b 75 e0             	mov    -0x20(%ebp),%esi
f0100bec:	89 06                	mov    %eax,(%esi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100bee:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0100bf5:	eb 11                	jmp    f0100c08 <stab_binsearch+0xa3>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f0100bf7:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100bfa:	89 06                	mov    %eax,(%esi)
			l = m;
			addr++;
f0100bfc:	ff 45 0c             	incl   0xc(%ebp)
f0100bff:	89 c3                	mov    %eax,%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100c01:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f0100c08:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
f0100c0b:	0f 8e 79 ff ff ff    	jle    f0100b8a <stab_binsearch+0x25>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0100c11:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
f0100c15:	75 0d                	jne    f0100c24 <stab_binsearch+0xbf>
		*region_right = *region_left - 1;
f0100c17:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100c1a:	8b 00                	mov    (%eax),%eax
f0100c1c:	48                   	dec    %eax
f0100c1d:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0100c20:	89 07                	mov    %eax,(%edi)
f0100c22:	eb 2c                	jmp    f0100c50 <stab_binsearch+0xeb>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100c24:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100c27:	8b 00                	mov    (%eax),%eax
		     l > *region_left && stabs[l].n_type != type;
f0100c29:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100c2c:	8b 0e                	mov    (%esi),%ecx
f0100c2e:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0100c31:	01 c2                	add    %eax,%edx
f0100c33:	8b 75 ec             	mov    -0x14(%ebp),%esi
f0100c36:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100c3a:	eb 01                	jmp    f0100c3d <stab_binsearch+0xd8>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0100c3c:	48                   	dec    %eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100c3d:	39 c8                	cmp    %ecx,%eax
f0100c3f:	7e 0a                	jle    f0100c4b <stab_binsearch+0xe6>
		     l > *region_left && stabs[l].n_type != type;
f0100c41:	0f b6 1a             	movzbl (%edx),%ebx
f0100c44:	83 ea 0c             	sub    $0xc,%edx
f0100c47:	39 df                	cmp    %ebx,%edi
f0100c49:	75 f1                	jne    f0100c3c <stab_binsearch+0xd7>
		     l--)
			/* do nothing */;
		*region_left = l;
f0100c4b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0100c4e:	89 07                	mov    %eax,(%edi)
	}
}
f0100c50:	83 c4 14             	add    $0x14,%esp
f0100c53:	5b                   	pop    %ebx
f0100c54:	5e                   	pop    %esi
f0100c55:	5f                   	pop    %edi
f0100c56:	5d                   	pop    %ebp
f0100c57:	c3                   	ret    

f0100c58 <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f0100c58:	55                   	push   %ebp
f0100c59:	89 e5                	mov    %esp,%ebp
f0100c5b:	57                   	push   %edi
f0100c5c:	56                   	push   %esi
f0100c5d:	53                   	push   %ebx
f0100c5e:	83 ec 3c             	sub    $0x3c,%esp
f0100c61:	8b 75 08             	mov    0x8(%ebp),%esi
f0100c64:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0100c67:	c7 03 18 22 10 f0    	movl   $0xf0102218,(%ebx)
	info->eip_line = 0;
f0100c6d:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	info->eip_fn_name = "<unknown>";
f0100c74:	c7 43 08 18 22 10 f0 	movl   $0xf0102218,0x8(%ebx)
	info->eip_fn_namelen = 9;
f0100c7b:	c7 43 0c 09 00 00 00 	movl   $0x9,0xc(%ebx)
	info->eip_fn_addr = addr;
f0100c82:	89 73 10             	mov    %esi,0x10(%ebx)
	info->eip_fn_narg = 0;
f0100c85:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f0100c8c:	81 fe ff ff 7f ef    	cmp    $0xef7fffff,%esi
f0100c92:	76 11                	jbe    f0100ca5 <debuginfo_eip+0x4d>
		// Can't search for user-level addresses yet!
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0100c94:	b8 05 90 10 f0       	mov    $0xf0109005,%eax
f0100c99:	3d 61 6a 10 f0       	cmp    $0xf0106a61,%eax
f0100c9e:	77 19                	ja     f0100cb9 <debuginfo_eip+0x61>
f0100ca0:	e9 b0 01 00 00       	jmp    f0100e55 <debuginfo_eip+0x1fd>
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
	} else {
		// Can't search for user-level addresses yet!
  	        panic("User address");
f0100ca5:	83 ec 04             	sub    $0x4,%esp
f0100ca8:	68 22 22 10 f0       	push   $0xf0102222
f0100cad:	6a 7f                	push   $0x7f
f0100caf:	68 2f 22 10 f0       	push   $0xf010222f
f0100cb4:	e8 d4 f3 ff ff       	call   f010008d <_panic>
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0100cb9:	80 3d 04 90 10 f0 00 	cmpb   $0x0,0xf0109004
f0100cc0:	0f 85 96 01 00 00    	jne    f0100e5c <debuginfo_eip+0x204>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0100cc6:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	rfile = (stab_end - stabs) - 1;
f0100ccd:	b8 60 6a 10 f0       	mov    $0xf0106a60,%eax
f0100cd2:	2d 50 29 10 f0       	sub    $0xf0102950,%eax
f0100cd7:	c1 f8 02             	sar    $0x2,%eax
f0100cda:	8d 14 80             	lea    (%eax,%eax,4),%edx
f0100cdd:	8d 14 90             	lea    (%eax,%edx,4),%edx
f0100ce0:	8d 0c 90             	lea    (%eax,%edx,4),%ecx
f0100ce3:	89 ca                	mov    %ecx,%edx
f0100ce5:	c1 e2 08             	shl    $0x8,%edx
f0100ce8:	01 d1                	add    %edx,%ecx
f0100cea:	89 ca                	mov    %ecx,%edx
f0100cec:	c1 e2 10             	shl    $0x10,%edx
f0100cef:	01 ca                	add    %ecx,%edx
f0100cf1:	01 d2                	add    %edx,%edx
f0100cf3:	8d 44 10 ff          	lea    -0x1(%eax,%edx,1),%eax
f0100cf7:	89 45 e0             	mov    %eax,-0x20(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f0100cfa:	83 ec 08             	sub    $0x8,%esp
f0100cfd:	56                   	push   %esi
f0100cfe:	6a 64                	push   $0x64
f0100d00:	8d 4d e0             	lea    -0x20(%ebp),%ecx
f0100d03:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f0100d06:	b8 50 29 10 f0       	mov    $0xf0102950,%eax
f0100d0b:	e8 55 fe ff ff       	call   f0100b65 <stab_binsearch>
	if (lfile == 0)
f0100d10:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100d13:	83 c4 10             	add    $0x10,%esp
f0100d16:	85 c0                	test   %eax,%eax
f0100d18:	0f 84 45 01 00 00    	je     f0100e63 <debuginfo_eip+0x20b>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f0100d1e:	89 45 dc             	mov    %eax,-0x24(%ebp)
	rfun = rfile;
f0100d21:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100d24:	89 45 d8             	mov    %eax,-0x28(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f0100d27:	83 ec 08             	sub    $0x8,%esp
f0100d2a:	56                   	push   %esi
f0100d2b:	6a 24                	push   $0x24
f0100d2d:	8d 4d d8             	lea    -0x28(%ebp),%ecx
f0100d30:	8d 55 dc             	lea    -0x24(%ebp),%edx
f0100d33:	b8 50 29 10 f0       	mov    $0xf0102950,%eax
f0100d38:	e8 28 fe ff ff       	call   f0100b65 <stab_binsearch>

	if (lfun <= rfun) {
f0100d3d:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0100d40:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0100d43:	83 c4 10             	add    $0x10,%esp
f0100d46:	39 d0                	cmp    %edx,%eax
f0100d48:	7f 42                	jg     f0100d8c <debuginfo_eip+0x134>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0100d4a:	8d 0c 00             	lea    (%eax,%eax,1),%ecx
f0100d4d:	01 c1                	add    %eax,%ecx
f0100d4f:	c1 e1 02             	shl    $0x2,%ecx
f0100d52:	8d b9 50 29 10 f0    	lea    -0xfefd6b0(%ecx),%edi
f0100d58:	89 7d c4             	mov    %edi,-0x3c(%ebp)
f0100d5b:	8b 89 50 29 10 f0    	mov    -0xfefd6b0(%ecx),%ecx
f0100d61:	bf 05 90 10 f0       	mov    $0xf0109005,%edi
f0100d66:	81 ef 61 6a 10 f0    	sub    $0xf0106a61,%edi
f0100d6c:	39 f9                	cmp    %edi,%ecx
f0100d6e:	73 09                	jae    f0100d79 <debuginfo_eip+0x121>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0100d70:	81 c1 61 6a 10 f0    	add    $0xf0106a61,%ecx
f0100d76:	89 4b 08             	mov    %ecx,0x8(%ebx)
		info->eip_fn_addr = stabs[lfun].n_value;
f0100d79:	8b 7d c4             	mov    -0x3c(%ebp),%edi
f0100d7c:	8b 4f 08             	mov    0x8(%edi),%ecx
f0100d7f:	89 4b 10             	mov    %ecx,0x10(%ebx)
		addr -= info->eip_fn_addr;
f0100d82:	29 ce                	sub    %ecx,%esi
		// Search within the function definition for the line number.
		lline = lfun;
f0100d84:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfun;
f0100d87:	89 55 d0             	mov    %edx,-0x30(%ebp)
f0100d8a:	eb 0f                	jmp    f0100d9b <debuginfo_eip+0x143>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f0100d8c:	89 73 10             	mov    %esi,0x10(%ebx)
		lline = lfile;
f0100d8f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100d92:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfile;
f0100d95:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100d98:	89 45 d0             	mov    %eax,-0x30(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f0100d9b:	83 ec 08             	sub    $0x8,%esp
f0100d9e:	6a 3a                	push   $0x3a
f0100da0:	ff 73 08             	pushl  0x8(%ebx)
f0100da3:	e8 8d 09 00 00       	call   f0101735 <strfind>
f0100da8:	2b 43 08             	sub    0x8(%ebx),%eax
f0100dab:	89 43 0c             	mov    %eax,0xc(%ebx)
	// Hint:
	//	There's a particular stabs type used for line numbers.
	//	Look at the STABS documentation and <inc/stab.h> to find
	//	which one.
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
f0100dae:	83 c4 08             	add    $0x8,%esp
f0100db1:	56                   	push   %esi
f0100db2:	6a 44                	push   $0x44
f0100db4:	8d 4d d0             	lea    -0x30(%ebp),%ecx
f0100db7:	8d 55 d4             	lea    -0x2c(%ebp),%edx
f0100dba:	b8 50 29 10 f0       	mov    $0xf0102950,%eax
f0100dbf:	e8 a1 fd ff ff       	call   f0100b65 <stab_binsearch>
	info->eip_line = stabs[lline].n_value;
f0100dc4:	8b 55 d4             	mov    -0x2c(%ebp),%edx
f0100dc7:	8d 04 12             	lea    (%edx,%edx,1),%eax
f0100dca:	01 d0                	add    %edx,%eax
f0100dcc:	c1 e0 02             	shl    $0x2,%eax
f0100dcf:	8b 88 58 29 10 f0    	mov    -0xfefd6a8(%eax),%ecx
f0100dd5:	89 4b 04             	mov    %ecx,0x4(%ebx)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0100dd8:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100ddb:	05 58 29 10 f0       	add    $0xf0102958,%eax
f0100de0:	83 c4 10             	add    $0x10,%esp
f0100de3:	eb 04                	jmp    f0100de9 <debuginfo_eip+0x191>
f0100de5:	4a                   	dec    %edx
f0100de6:	83 e8 0c             	sub    $0xc,%eax
f0100de9:	39 d6                	cmp    %edx,%esi
f0100deb:	7f 34                	jg     f0100e21 <debuginfo_eip+0x1c9>
	       && stabs[lline].n_type != N_SOL
f0100ded:	8a 48 fc             	mov    -0x4(%eax),%cl
f0100df0:	80 f9 84             	cmp    $0x84,%cl
f0100df3:	74 0a                	je     f0100dff <debuginfo_eip+0x1a7>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f0100df5:	80 f9 64             	cmp    $0x64,%cl
f0100df8:	75 eb                	jne    f0100de5 <debuginfo_eip+0x18d>
f0100dfa:	83 38 00             	cmpl   $0x0,(%eax)
f0100dfd:	74 e6                	je     f0100de5 <debuginfo_eip+0x18d>
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f0100dff:	8d 04 12             	lea    (%edx,%edx,1),%eax
f0100e02:	01 c2                	add    %eax,%edx
f0100e04:	8b 14 95 50 29 10 f0 	mov    -0xfefd6b0(,%edx,4),%edx
f0100e0b:	b8 05 90 10 f0       	mov    $0xf0109005,%eax
f0100e10:	2d 61 6a 10 f0       	sub    $0xf0106a61,%eax
f0100e15:	39 c2                	cmp    %eax,%edx
f0100e17:	73 08                	jae    f0100e21 <debuginfo_eip+0x1c9>
		info->eip_file = stabstr + stabs[lline].n_strx;
f0100e19:	81 c2 61 6a 10 f0    	add    $0xf0106a61,%edx
f0100e1f:	89 13                	mov    %edx,(%ebx)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f0100e21:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100e24:	8b 75 d8             	mov    -0x28(%ebp),%esi
f0100e27:	39 f2                	cmp    %esi,%edx
f0100e29:	7d 3f                	jge    f0100e6a <debuginfo_eip+0x212>
		for (lline = lfun + 1;
f0100e2b:	42                   	inc    %edx
f0100e2c:	89 d0                	mov    %edx,%eax
f0100e2e:	8d 0c 12             	lea    (%edx,%edx,1),%ecx
f0100e31:	01 ca                	add    %ecx,%edx
f0100e33:	8d 14 95 54 29 10 f0 	lea    -0xfefd6ac(,%edx,4),%edx
f0100e3a:	eb 03                	jmp    f0100e3f <debuginfo_eip+0x1e7>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;
f0100e3c:	ff 43 14             	incl   0x14(%ebx)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
		for (lline = lfun + 1;
f0100e3f:	39 c6                	cmp    %eax,%esi
f0100e41:	7e 2e                	jle    f0100e71 <debuginfo_eip+0x219>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
f0100e43:	8a 0a                	mov    (%edx),%cl
f0100e45:	40                   	inc    %eax
f0100e46:	83 c2 0c             	add    $0xc,%edx
f0100e49:	80 f9 a0             	cmp    $0xa0,%cl
f0100e4c:	74 ee                	je     f0100e3c <debuginfo_eip+0x1e4>
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0100e4e:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e53:	eb 21                	jmp    f0100e76 <debuginfo_eip+0x21e>
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f0100e55:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100e5a:	eb 1a                	jmp    f0100e76 <debuginfo_eip+0x21e>
f0100e5c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100e61:	eb 13                	jmp    f0100e76 <debuginfo_eip+0x21e>
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
		return -1;
f0100e63:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100e68:	eb 0c                	jmp    f0100e76 <debuginfo_eip+0x21e>
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0100e6a:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e6f:	eb 05                	jmp    f0100e76 <debuginfo_eip+0x21e>
f0100e71:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100e76:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100e79:	5b                   	pop    %ebx
f0100e7a:	5e                   	pop    %esi
f0100e7b:	5f                   	pop    %edi
f0100e7c:	5d                   	pop    %ebp
f0100e7d:	c3                   	ret    

f0100e7e <cpuid_print>:
	return feature[bit / 32] & BIT(bit % 32);
}

void
cpuid_print(void)
{
f0100e7e:	55                   	push   %ebp
f0100e7f:	89 e5                	mov    %esp,%ebp
f0100e81:	57                   	push   %edi
f0100e82:	56                   	push   %esi
f0100e83:	53                   	push   %ebx
f0100e84:	83 ec 6c             	sub    $0x6c,%esp
	uint32_t eax, brand[12], feature[CPUID_NR_FLAGS] = {0};
f0100e87:	8d 7d a4             	lea    -0x5c(%ebp),%edi
f0100e8a:	b9 05 00 00 00       	mov    $0x5,%ecx
f0100e8f:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e94:	f3 ab                	rep stos %eax,%es:(%edi)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0100e96:	b8 00 00 00 80       	mov    $0x80000000,%eax
f0100e9b:	0f a2                	cpuid  

	cpuid(0x80000000, &eax, NULL, NULL, NULL);
	if (eax < 0x80000004)
f0100e9d:	3d 03 00 00 80       	cmp    $0x80000003,%eax
f0100ea2:	77 17                	ja     f0100ebb <cpuid_print+0x3d>
		panic("CPU too old!");
f0100ea4:	83 ec 04             	sub    $0x4,%esp
f0100ea7:	68 3d 22 10 f0       	push   $0xf010223d
f0100eac:	68 8d 00 00 00       	push   $0x8d
f0100eb1:	68 4a 22 10 f0       	push   $0xf010224a
f0100eb6:	e8 d2 f1 ff ff       	call   f010008d <_panic>
f0100ebb:	b8 02 00 00 80       	mov    $0x80000002,%eax
f0100ec0:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f0100ec2:	89 45 b8             	mov    %eax,-0x48(%ebp)
	if (ebxp)
		*ebxp = ebx;
f0100ec5:	89 5d bc             	mov    %ebx,-0x44(%ebp)
	if (ecxp)
		*ecxp = ecx;
f0100ec8:	89 4d c0             	mov    %ecx,-0x40(%ebp)
	if (edxp)
		*edxp = edx;
f0100ecb:	89 55 c4             	mov    %edx,-0x3c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0100ece:	b8 03 00 00 80       	mov    $0x80000003,%eax
f0100ed3:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f0100ed5:	89 45 c8             	mov    %eax,-0x38(%ebp)
	if (ebxp)
		*ebxp = ebx;
f0100ed8:	89 5d cc             	mov    %ebx,-0x34(%ebp)
	if (ecxp)
		*ecxp = ecx;
f0100edb:	89 4d d0             	mov    %ecx,-0x30(%ebp)
	if (edxp)
		*edxp = edx;
f0100ede:	89 55 d4             	mov    %edx,-0x2c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0100ee1:	b8 04 00 00 80       	mov    $0x80000004,%eax
f0100ee6:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f0100ee8:	89 45 d8             	mov    %eax,-0x28(%ebp)
	if (ebxp)
		*ebxp = ebx;
f0100eeb:	89 5d dc             	mov    %ebx,-0x24(%ebp)
	if (ecxp)
		*ecxp = ecx;
f0100eee:	89 4d e0             	mov    %ecx,-0x20(%ebp)
	if (edxp)
		*edxp = edx;
f0100ef1:	89 55 e4             	mov    %edx,-0x1c(%ebp)

	cpuid(0x80000002, &brand[0], &brand[1], &brand[2], &brand[3]);
	cpuid(0x80000003, &brand[4], &brand[5], &brand[6], &brand[7]);
	cpuid(0x80000004, &brand[8], &brand[9], &brand[10], &brand[11]);
	cprintf("CPU: %.48s\n", brand);
f0100ef4:	83 ec 08             	sub    $0x8,%esp
f0100ef7:	8d 45 b8             	lea    -0x48(%ebp),%eax
f0100efa:	50                   	push   %eax
f0100efb:	68 56 22 10 f0       	push   $0xf0102256
f0100f00:	e8 4c fc ff ff       	call   f0100b51 <cprintf>

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0100f05:	b8 01 00 00 00       	mov    $0x1,%eax
f0100f0a:	0f a2                	cpuid  
f0100f0c:	89 55 90             	mov    %edx,-0x70(%ebp)
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f0100f0f:	89 4d a8             	mov    %ecx,-0x58(%ebp)
	if (edxp)
		*edxp = edx;
f0100f12:	89 55 a4             	mov    %edx,-0x5c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0100f15:	b8 01 00 00 80       	mov    $0x80000001,%eax
f0100f1a:	0f a2                	cpuid  
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f0100f1c:	89 4d b4             	mov    %ecx,-0x4c(%ebp)
	if (edxp)
		*edxp = edx;
f0100f1f:	89 55 b0             	mov    %edx,-0x50(%ebp)
f0100f22:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f0100f25:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
		if (!feature[i])
f0100f2c:	8b 45 94             	mov    -0x6c(%ebp),%eax
f0100f2f:	8b 74 85 a4          	mov    -0x5c(%ebp,%eax,4),%esi
f0100f33:	85 f6                	test   %esi,%esi
f0100f35:	74 5a                	je     f0100f91 <cpuid_print+0x113>
			continue;
		cprintf(" ");
f0100f37:	83 ec 0c             	sub    $0xc,%esp
f0100f3a:	68 6c 1f 10 f0       	push   $0xf0101f6c
f0100f3f:	e8 0d fc ff ff       	call   f0100b51 <cprintf>
f0100f44:	8b 7d 94             	mov    -0x6c(%ebp),%edi
f0100f47:	c1 e7 05             	shl    $0x5,%edi
f0100f4a:	83 c4 10             	add    $0x10,%esp
		for (j = 0; j < 32; ++j) {
f0100f4d:	bb 00 00 00 00       	mov    $0x0,%ebx
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
f0100f52:	89 f0                	mov    %esi,%eax
f0100f54:	88 d9                	mov    %bl,%cl
f0100f56:	d3 e8                	shr    %cl,%eax
f0100f58:	a8 01                	test   $0x1,%al
f0100f5a:	74 1f                	je     f0100f7b <cpuid_print+0xfd>
	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
			const char *name = names[CPUID_BIT(i, j)];
f0100f5c:	8d 04 3b             	lea    (%ebx,%edi,1),%eax
f0100f5f:	8b 04 85 c0 24 10 f0 	mov    -0xfefdb40(,%eax,4),%eax

			if ((feature[i] & BIT(j)) && name)
f0100f66:	85 c0                	test   %eax,%eax
f0100f68:	74 11                	je     f0100f7b <cpuid_print+0xfd>
				cprintf(" %s", name);
f0100f6a:	83 ec 08             	sub    $0x8,%esp
f0100f6d:	50                   	push   %eax
f0100f6e:	68 fc 1b 10 f0       	push   $0xf0101bfc
f0100f73:	e8 d9 fb ff ff       	call   f0100b51 <cprintf>
f0100f78:	83 c4 10             	add    $0x10,%esp

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
f0100f7b:	43                   	inc    %ebx
f0100f7c:	83 fb 20             	cmp    $0x20,%ebx
f0100f7f:	75 d1                	jne    f0100f52 <cpuid_print+0xd4>
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
				cprintf(" %s", name);
		}
		cprintf("\n");
f0100f81:	83 ec 0c             	sub    $0xc,%esp
f0100f84:	68 6e 1c 10 f0       	push   $0xf0101c6e
f0100f89:	e8 c3 fb ff ff       	call   f0100b51 <cprintf>
f0100f8e:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f0100f91:	ff 45 94             	incl   -0x6c(%ebp)
f0100f94:	8b 45 94             	mov    -0x6c(%ebp),%eax
f0100f97:	83 f8 05             	cmp    $0x5,%eax
f0100f9a:	75 90                	jne    f0100f2c <cpuid_print+0xae>
	      &feature[CPUID_1_ECX], &feature[CPUID_1_EDX]);
	cpuid(0x80000001, NULL, NULL,
	      &feature[CPUID_80000001_ECX], &feature[CPUID_80000001_EDX]);
	print_feature(feature);
	// Check feature bits.
	assert(cpuid_has(feature, CPUID_FEATURE_PSE));
f0100f9c:	f6 45 90 08          	testb  $0x8,-0x70(%ebp)
f0100fa0:	75 19                	jne    f0100fbb <cpuid_print+0x13d>
f0100fa2:	68 58 24 10 f0       	push   $0xf0102458
f0100fa7:	68 eb 1b 10 f0       	push   $0xf0101beb
f0100fac:	68 9a 00 00 00       	push   $0x9a
f0100fb1:	68 4a 22 10 f0       	push   $0xf010224a
f0100fb6:	e8 d2 f0 ff ff       	call   f010008d <_panic>
	assert(cpuid_has(feature, CPUID_FEATURE_APIC));
f0100fbb:	f7 45 90 00 02 00 00 	testl  $0x200,-0x70(%ebp)
f0100fc2:	75 19                	jne    f0100fdd <cpuid_print+0x15f>
f0100fc4:	68 80 24 10 f0       	push   $0xf0102480
f0100fc9:	68 eb 1b 10 f0       	push   $0xf0101beb
f0100fce:	68 9b 00 00 00       	push   $0x9b
f0100fd3:	68 4a 22 10 f0       	push   $0xf010224a
f0100fd8:	e8 b0 f0 ff ff       	call   f010008d <_panic>
}
f0100fdd:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100fe0:	5b                   	pop    %ebx
f0100fe1:	5e                   	pop    %esi
f0100fe2:	5f                   	pop    %edi
f0100fe3:	5d                   	pop    %ebp
f0100fe4:	c3                   	ret    

f0100fe5 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f0100fe5:	55                   	push   %ebp
f0100fe6:	89 e5                	mov    %esp,%ebp
f0100fe8:	57                   	push   %edi
f0100fe9:	56                   	push   %esi
f0100fea:	53                   	push   %ebx
f0100feb:	83 ec 1c             	sub    $0x1c,%esp
f0100fee:	89 c7                	mov    %eax,%edi
f0100ff0:	89 d6                	mov    %edx,%esi
f0100ff2:	8b 45 08             	mov    0x8(%ebp),%eax
f0100ff5:	8b 55 0c             	mov    0xc(%ebp),%edx
f0100ff8:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0100ffb:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f0100ffe:	8b 4d 10             	mov    0x10(%ebp),%ecx
f0101001:	bb 00 00 00 00       	mov    $0x0,%ebx
f0101006:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f0101009:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
f010100c:	39 d3                	cmp    %edx,%ebx
f010100e:	72 05                	jb     f0101015 <printnum+0x30>
f0101010:	39 45 10             	cmp    %eax,0x10(%ebp)
f0101013:	77 45                	ja     f010105a <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f0101015:	83 ec 0c             	sub    $0xc,%esp
f0101018:	ff 75 18             	pushl  0x18(%ebp)
f010101b:	8b 45 14             	mov    0x14(%ebp),%eax
f010101e:	8d 58 ff             	lea    -0x1(%eax),%ebx
f0101021:	53                   	push   %ebx
f0101022:	ff 75 10             	pushl  0x10(%ebp)
f0101025:	83 ec 08             	sub    $0x8,%esp
f0101028:	ff 75 e4             	pushl  -0x1c(%ebp)
f010102b:	ff 75 e0             	pushl  -0x20(%ebp)
f010102e:	ff 75 dc             	pushl  -0x24(%ebp)
f0101031:	ff 75 d8             	pushl  -0x28(%ebp)
f0101034:	e8 17 09 00 00       	call   f0101950 <__udivdi3>
f0101039:	83 c4 18             	add    $0x18,%esp
f010103c:	52                   	push   %edx
f010103d:	50                   	push   %eax
f010103e:	89 f2                	mov    %esi,%edx
f0101040:	89 f8                	mov    %edi,%eax
f0101042:	e8 9e ff ff ff       	call   f0100fe5 <printnum>
f0101047:	83 c4 20             	add    $0x20,%esp
f010104a:	eb 16                	jmp    f0101062 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f010104c:	83 ec 08             	sub    $0x8,%esp
f010104f:	56                   	push   %esi
f0101050:	ff 75 18             	pushl  0x18(%ebp)
f0101053:	ff d7                	call   *%edi
f0101055:	83 c4 10             	add    $0x10,%esp
f0101058:	eb 03                	jmp    f010105d <printnum+0x78>
f010105a:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f010105d:	4b                   	dec    %ebx
f010105e:	85 db                	test   %ebx,%ebx
f0101060:	7f ea                	jg     f010104c <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0101062:	83 ec 08             	sub    $0x8,%esp
f0101065:	56                   	push   %esi
f0101066:	83 ec 04             	sub    $0x4,%esp
f0101069:	ff 75 e4             	pushl  -0x1c(%ebp)
f010106c:	ff 75 e0             	pushl  -0x20(%ebp)
f010106f:	ff 75 dc             	pushl  -0x24(%ebp)
f0101072:	ff 75 d8             	pushl  -0x28(%ebp)
f0101075:	e8 e6 09 00 00       	call   f0101a60 <__umoddi3>
f010107a:	83 c4 14             	add    $0x14,%esp
f010107d:	0f be 80 40 27 10 f0 	movsbl -0xfefd8c0(%eax),%eax
f0101084:	50                   	push   %eax
f0101085:	ff d7                	call   *%edi
}
f0101087:	83 c4 10             	add    $0x10,%esp
f010108a:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010108d:	5b                   	pop    %ebx
f010108e:	5e                   	pop    %esi
f010108f:	5f                   	pop    %edi
f0101090:	5d                   	pop    %ebp
f0101091:	c3                   	ret    

f0101092 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0101092:	55                   	push   %ebp
f0101093:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0101095:	83 fa 01             	cmp    $0x1,%edx
f0101098:	7e 0e                	jle    f01010a8 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
f010109a:	8b 10                	mov    (%eax),%edx
f010109c:	8d 4a 08             	lea    0x8(%edx),%ecx
f010109f:	89 08                	mov    %ecx,(%eax)
f01010a1:	8b 02                	mov    (%edx),%eax
f01010a3:	8b 52 04             	mov    0x4(%edx),%edx
f01010a6:	eb 22                	jmp    f01010ca <getuint+0x38>
	else if (lflag)
f01010a8:	85 d2                	test   %edx,%edx
f01010aa:	74 10                	je     f01010bc <getuint+0x2a>
		return va_arg(*ap, unsigned long);
f01010ac:	8b 10                	mov    (%eax),%edx
f01010ae:	8d 4a 04             	lea    0x4(%edx),%ecx
f01010b1:	89 08                	mov    %ecx,(%eax)
f01010b3:	8b 02                	mov    (%edx),%eax
f01010b5:	ba 00 00 00 00       	mov    $0x0,%edx
f01010ba:	eb 0e                	jmp    f01010ca <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
f01010bc:	8b 10                	mov    (%eax),%edx
f01010be:	8d 4a 04             	lea    0x4(%edx),%ecx
f01010c1:	89 08                	mov    %ecx,(%eax)
f01010c3:	8b 02                	mov    (%edx),%eax
f01010c5:	ba 00 00 00 00       	mov    $0x0,%edx
}
f01010ca:	5d                   	pop    %ebp
f01010cb:	c3                   	ret    

f01010cc <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f01010cc:	55                   	push   %ebp
f01010cd:	89 e5                	mov    %esp,%ebp
f01010cf:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
f01010d2:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
f01010d5:	8b 10                	mov    (%eax),%edx
f01010d7:	3b 50 04             	cmp    0x4(%eax),%edx
f01010da:	73 0a                	jae    f01010e6 <sprintputch+0x1a>
		*b->buf++ = ch;
f01010dc:	8d 4a 01             	lea    0x1(%edx),%ecx
f01010df:	89 08                	mov    %ecx,(%eax)
f01010e1:	8b 45 08             	mov    0x8(%ebp),%eax
f01010e4:	88 02                	mov    %al,(%edx)
}
f01010e6:	5d                   	pop    %ebp
f01010e7:	c3                   	ret    

f01010e8 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f01010e8:	55                   	push   %ebp
f01010e9:	89 e5                	mov    %esp,%ebp
f01010eb:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f01010ee:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f01010f1:	50                   	push   %eax
f01010f2:	ff 75 10             	pushl  0x10(%ebp)
f01010f5:	ff 75 0c             	pushl  0xc(%ebp)
f01010f8:	ff 75 08             	pushl  0x8(%ebp)
f01010fb:	e8 05 00 00 00       	call   f0101105 <vprintfmt>
	va_end(ap);
}
f0101100:	83 c4 10             	add    $0x10,%esp
f0101103:	c9                   	leave  
f0101104:	c3                   	ret    

f0101105 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f0101105:	55                   	push   %ebp
f0101106:	89 e5                	mov    %esp,%ebp
f0101108:	57                   	push   %edi
f0101109:	56                   	push   %esi
f010110a:	53                   	push   %ebx
f010110b:	83 ec 2c             	sub    $0x2c,%esp
f010110e:	8b 75 08             	mov    0x8(%ebp),%esi
f0101111:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0101114:	8b 7d 10             	mov    0x10(%ebp),%edi
f0101117:	eb 12                	jmp    f010112b <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
f0101119:	85 c0                	test   %eax,%eax
f010111b:	0f 84 68 03 00 00    	je     f0101489 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
f0101121:	83 ec 08             	sub    $0x8,%esp
f0101124:	53                   	push   %ebx
f0101125:	50                   	push   %eax
f0101126:	ff d6                	call   *%esi
f0101128:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f010112b:	47                   	inc    %edi
f010112c:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f0101130:	83 f8 25             	cmp    $0x25,%eax
f0101133:	75 e4                	jne    f0101119 <vprintfmt+0x14>
f0101135:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
f0101139:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f0101140:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0101147:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
f010114e:	ba 00 00 00 00       	mov    $0x0,%edx
f0101153:	eb 07                	jmp    f010115c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101155:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
f0101158:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010115c:	8d 47 01             	lea    0x1(%edi),%eax
f010115f:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0101162:	0f b6 0f             	movzbl (%edi),%ecx
f0101165:	8a 07                	mov    (%edi),%al
f0101167:	83 e8 23             	sub    $0x23,%eax
f010116a:	3c 55                	cmp    $0x55,%al
f010116c:	0f 87 fe 02 00 00    	ja     f0101470 <vprintfmt+0x36b>
f0101172:	0f b6 c0             	movzbl %al,%eax
f0101175:	ff 24 85 cc 27 10 f0 	jmp    *-0xfefd834(,%eax,4)
f010117c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f010117f:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
f0101183:	eb d7                	jmp    f010115c <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101185:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0101188:	b8 00 00 00 00       	mov    $0x0,%eax
f010118d:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f0101190:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0101193:	01 c0                	add    %eax,%eax
f0101195:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
f0101199:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
f010119c:	8d 51 d0             	lea    -0x30(%ecx),%edx
f010119f:	83 fa 09             	cmp    $0x9,%edx
f01011a2:	77 34                	ja     f01011d8 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f01011a4:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f01011a5:	eb e9                	jmp    f0101190 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f01011a7:	8b 45 14             	mov    0x14(%ebp),%eax
f01011aa:	8d 48 04             	lea    0x4(%eax),%ecx
f01011ad:	89 4d 14             	mov    %ecx,0x14(%ebp)
f01011b0:	8b 00                	mov    (%eax),%eax
f01011b2:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01011b5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f01011b8:	eb 24                	jmp    f01011de <vprintfmt+0xd9>
f01011ba:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01011be:	79 07                	jns    f01011c7 <vprintfmt+0xc2>
f01011c0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01011c7:	8b 7d e0             	mov    -0x20(%ebp),%edi
f01011ca:	eb 90                	jmp    f010115c <vprintfmt+0x57>
f01011cc:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f01011cf:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
f01011d6:	eb 84                	jmp    f010115c <vprintfmt+0x57>
f01011d8:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01011db:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
f01011de:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01011e2:	0f 89 74 ff ff ff    	jns    f010115c <vprintfmt+0x57>
				width = precision, precision = -1;
f01011e8:	8b 45 d0             	mov    -0x30(%ebp),%eax
f01011eb:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01011ee:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f01011f5:	e9 62 ff ff ff       	jmp    f010115c <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f01011fa:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01011fb:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f01011fe:	e9 59 ff ff ff       	jmp    f010115c <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f0101203:	8b 45 14             	mov    0x14(%ebp),%eax
f0101206:	8d 50 04             	lea    0x4(%eax),%edx
f0101209:	89 55 14             	mov    %edx,0x14(%ebp)
f010120c:	83 ec 08             	sub    $0x8,%esp
f010120f:	53                   	push   %ebx
f0101210:	ff 30                	pushl  (%eax)
f0101212:	ff d6                	call   *%esi
			break;
f0101214:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101217:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
f010121a:	e9 0c ff ff ff       	jmp    f010112b <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
f010121f:	8b 45 14             	mov    0x14(%ebp),%eax
f0101222:	8d 50 04             	lea    0x4(%eax),%edx
f0101225:	89 55 14             	mov    %edx,0x14(%ebp)
f0101228:	8b 00                	mov    (%eax),%eax
f010122a:	85 c0                	test   %eax,%eax
f010122c:	79 02                	jns    f0101230 <vprintfmt+0x12b>
f010122e:	f7 d8                	neg    %eax
f0101230:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
f0101232:	83 f8 06             	cmp    $0x6,%eax
f0101235:	7f 0b                	jg     f0101242 <vprintfmt+0x13d>
f0101237:	8b 04 85 24 29 10 f0 	mov    -0xfefd6dc(,%eax,4),%eax
f010123e:	85 c0                	test   %eax,%eax
f0101240:	75 18                	jne    f010125a <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
f0101242:	52                   	push   %edx
f0101243:	68 58 27 10 f0       	push   $0xf0102758
f0101248:	53                   	push   %ebx
f0101249:	56                   	push   %esi
f010124a:	e8 99 fe ff ff       	call   f01010e8 <printfmt>
f010124f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101252:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
f0101255:	e9 d1 fe ff ff       	jmp    f010112b <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
f010125a:	50                   	push   %eax
f010125b:	68 fd 1b 10 f0       	push   $0xf0101bfd
f0101260:	53                   	push   %ebx
f0101261:	56                   	push   %esi
f0101262:	e8 81 fe ff ff       	call   f01010e8 <printfmt>
f0101267:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010126a:	8b 7d e0             	mov    -0x20(%ebp),%edi
f010126d:	e9 b9 fe ff ff       	jmp    f010112b <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0101272:	8b 45 14             	mov    0x14(%ebp),%eax
f0101275:	8d 50 04             	lea    0x4(%eax),%edx
f0101278:	89 55 14             	mov    %edx,0x14(%ebp)
f010127b:	8b 38                	mov    (%eax),%edi
f010127d:	85 ff                	test   %edi,%edi
f010127f:	75 05                	jne    f0101286 <vprintfmt+0x181>
				p = "(null)";
f0101281:	bf 51 27 10 f0       	mov    $0xf0102751,%edi
			if (width > 0 && padc != '-')
f0101286:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f010128a:	0f 8e 90 00 00 00    	jle    f0101320 <vprintfmt+0x21b>
f0101290:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
f0101294:	0f 84 8e 00 00 00    	je     f0101328 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
f010129a:	83 ec 08             	sub    $0x8,%esp
f010129d:	ff 75 d0             	pushl  -0x30(%ebp)
f01012a0:	57                   	push   %edi
f01012a1:	e8 60 03 00 00       	call   f0101606 <strnlen>
f01012a6:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f01012a9:	29 c1                	sub    %eax,%ecx
f01012ab:	89 4d cc             	mov    %ecx,-0x34(%ebp)
f01012ae:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
f01012b1:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
f01012b5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01012b8:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f01012bb:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f01012bd:	eb 0d                	jmp    f01012cc <vprintfmt+0x1c7>
					putch(padc, putdat);
f01012bf:	83 ec 08             	sub    $0x8,%esp
f01012c2:	53                   	push   %ebx
f01012c3:	ff 75 e4             	pushl  -0x1c(%ebp)
f01012c6:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f01012c8:	4f                   	dec    %edi
f01012c9:	83 c4 10             	add    $0x10,%esp
f01012cc:	85 ff                	test   %edi,%edi
f01012ce:	7f ef                	jg     f01012bf <vprintfmt+0x1ba>
f01012d0:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f01012d3:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f01012d6:	89 c8                	mov    %ecx,%eax
f01012d8:	85 c9                	test   %ecx,%ecx
f01012da:	79 05                	jns    f01012e1 <vprintfmt+0x1dc>
f01012dc:	b8 00 00 00 00       	mov    $0x0,%eax
f01012e1:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f01012e4:	29 c1                	sub    %eax,%ecx
f01012e6:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f01012e9:	89 75 08             	mov    %esi,0x8(%ebp)
f01012ec:	8b 75 d0             	mov    -0x30(%ebp),%esi
f01012ef:	eb 3d                	jmp    f010132e <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f01012f1:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
f01012f5:	74 19                	je     f0101310 <vprintfmt+0x20b>
f01012f7:	0f be c0             	movsbl %al,%eax
f01012fa:	83 e8 20             	sub    $0x20,%eax
f01012fd:	83 f8 5e             	cmp    $0x5e,%eax
f0101300:	76 0e                	jbe    f0101310 <vprintfmt+0x20b>
					putch('?', putdat);
f0101302:	83 ec 08             	sub    $0x8,%esp
f0101305:	53                   	push   %ebx
f0101306:	6a 3f                	push   $0x3f
f0101308:	ff 55 08             	call   *0x8(%ebp)
f010130b:	83 c4 10             	add    $0x10,%esp
f010130e:	eb 0b                	jmp    f010131b <vprintfmt+0x216>
				else
					putch(ch, putdat);
f0101310:	83 ec 08             	sub    $0x8,%esp
f0101313:	53                   	push   %ebx
f0101314:	52                   	push   %edx
f0101315:	ff 55 08             	call   *0x8(%ebp)
f0101318:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f010131b:	ff 4d e4             	decl   -0x1c(%ebp)
f010131e:	eb 0e                	jmp    f010132e <vprintfmt+0x229>
f0101320:	89 75 08             	mov    %esi,0x8(%ebp)
f0101323:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0101326:	eb 06                	jmp    f010132e <vprintfmt+0x229>
f0101328:	89 75 08             	mov    %esi,0x8(%ebp)
f010132b:	8b 75 d0             	mov    -0x30(%ebp),%esi
f010132e:	47                   	inc    %edi
f010132f:	8a 47 ff             	mov    -0x1(%edi),%al
f0101332:	0f be d0             	movsbl %al,%edx
f0101335:	85 d2                	test   %edx,%edx
f0101337:	74 1d                	je     f0101356 <vprintfmt+0x251>
f0101339:	85 f6                	test   %esi,%esi
f010133b:	78 b4                	js     f01012f1 <vprintfmt+0x1ec>
f010133d:	4e                   	dec    %esi
f010133e:	79 b1                	jns    f01012f1 <vprintfmt+0x1ec>
f0101340:	8b 75 08             	mov    0x8(%ebp),%esi
f0101343:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0101346:	eb 14                	jmp    f010135c <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
f0101348:	83 ec 08             	sub    $0x8,%esp
f010134b:	53                   	push   %ebx
f010134c:	6a 20                	push   $0x20
f010134e:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f0101350:	4f                   	dec    %edi
f0101351:	83 c4 10             	add    $0x10,%esp
f0101354:	eb 06                	jmp    f010135c <vprintfmt+0x257>
f0101356:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0101359:	8b 75 08             	mov    0x8(%ebp),%esi
f010135c:	85 ff                	test   %edi,%edi
f010135e:	7f e8                	jg     f0101348 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101360:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0101363:	e9 c3 fd ff ff       	jmp    f010112b <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0101368:	83 fa 01             	cmp    $0x1,%edx
f010136b:	7e 16                	jle    f0101383 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
f010136d:	8b 45 14             	mov    0x14(%ebp),%eax
f0101370:	8d 50 08             	lea    0x8(%eax),%edx
f0101373:	89 55 14             	mov    %edx,0x14(%ebp)
f0101376:	8b 50 04             	mov    0x4(%eax),%edx
f0101379:	8b 00                	mov    (%eax),%eax
f010137b:	89 45 d8             	mov    %eax,-0x28(%ebp)
f010137e:	89 55 dc             	mov    %edx,-0x24(%ebp)
f0101381:	eb 32                	jmp    f01013b5 <vprintfmt+0x2b0>
	else if (lflag)
f0101383:	85 d2                	test   %edx,%edx
f0101385:	74 18                	je     f010139f <vprintfmt+0x29a>
		return va_arg(*ap, long);
f0101387:	8b 45 14             	mov    0x14(%ebp),%eax
f010138a:	8d 50 04             	lea    0x4(%eax),%edx
f010138d:	89 55 14             	mov    %edx,0x14(%ebp)
f0101390:	8b 00                	mov    (%eax),%eax
f0101392:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0101395:	89 c1                	mov    %eax,%ecx
f0101397:	c1 f9 1f             	sar    $0x1f,%ecx
f010139a:	89 4d dc             	mov    %ecx,-0x24(%ebp)
f010139d:	eb 16                	jmp    f01013b5 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
f010139f:	8b 45 14             	mov    0x14(%ebp),%eax
f01013a2:	8d 50 04             	lea    0x4(%eax),%edx
f01013a5:	89 55 14             	mov    %edx,0x14(%ebp)
f01013a8:	8b 00                	mov    (%eax),%eax
f01013aa:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01013ad:	89 c1                	mov    %eax,%ecx
f01013af:	c1 f9 1f             	sar    $0x1f,%ecx
f01013b2:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f01013b5:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01013b8:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
f01013bb:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f01013bf:	79 76                	jns    f0101437 <vprintfmt+0x332>
				putch('-', putdat);
f01013c1:	83 ec 08             	sub    $0x8,%esp
f01013c4:	53                   	push   %ebx
f01013c5:	6a 2d                	push   $0x2d
f01013c7:	ff d6                	call   *%esi
				num = -(long long) num;
f01013c9:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01013cc:	8b 55 dc             	mov    -0x24(%ebp),%edx
f01013cf:	f7 d8                	neg    %eax
f01013d1:	83 d2 00             	adc    $0x0,%edx
f01013d4:	f7 da                	neg    %edx
f01013d6:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
f01013d9:	b9 0a 00 00 00       	mov    $0xa,%ecx
f01013de:	eb 5c                	jmp    f010143c <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f01013e0:	8d 45 14             	lea    0x14(%ebp),%eax
f01013e3:	e8 aa fc ff ff       	call   f0101092 <getuint>
			base = 10;
f01013e8:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
f01013ed:	eb 4d                	jmp    f010143c <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
f01013ef:	8d 45 14             	lea    0x14(%ebp),%eax
f01013f2:	e8 9b fc ff ff       	call   f0101092 <getuint>
			base = 8;
f01013f7:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
f01013fc:	eb 3e                	jmp    f010143c <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
f01013fe:	83 ec 08             	sub    $0x8,%esp
f0101401:	53                   	push   %ebx
f0101402:	6a 30                	push   $0x30
f0101404:	ff d6                	call   *%esi
			putch('x', putdat);
f0101406:	83 c4 08             	add    $0x8,%esp
f0101409:	53                   	push   %ebx
f010140a:	6a 78                	push   $0x78
f010140c:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f010140e:	8b 45 14             	mov    0x14(%ebp),%eax
f0101411:	8d 50 04             	lea    0x4(%eax),%edx
f0101414:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f0101417:	8b 00                	mov    (%eax),%eax
f0101419:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
f010141e:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
f0101421:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
f0101426:	eb 14                	jmp    f010143c <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f0101428:	8d 45 14             	lea    0x14(%ebp),%eax
f010142b:	e8 62 fc ff ff       	call   f0101092 <getuint>
			base = 16;
f0101430:	b9 10 00 00 00       	mov    $0x10,%ecx
f0101435:	eb 05                	jmp    f010143c <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f0101437:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
f010143c:	83 ec 0c             	sub    $0xc,%esp
f010143f:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
f0101443:	57                   	push   %edi
f0101444:	ff 75 e4             	pushl  -0x1c(%ebp)
f0101447:	51                   	push   %ecx
f0101448:	52                   	push   %edx
f0101449:	50                   	push   %eax
f010144a:	89 da                	mov    %ebx,%edx
f010144c:	89 f0                	mov    %esi,%eax
f010144e:	e8 92 fb ff ff       	call   f0100fe5 <printnum>
			break;
f0101453:	83 c4 20             	add    $0x20,%esp
f0101456:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0101459:	e9 cd fc ff ff       	jmp    f010112b <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f010145e:	83 ec 08             	sub    $0x8,%esp
f0101461:	53                   	push   %ebx
f0101462:	51                   	push   %ecx
f0101463:	ff d6                	call   *%esi
			break;
f0101465:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101468:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
f010146b:	e9 bb fc ff ff       	jmp    f010112b <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0101470:	83 ec 08             	sub    $0x8,%esp
f0101473:	53                   	push   %ebx
f0101474:	6a 25                	push   $0x25
f0101476:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
f0101478:	83 c4 10             	add    $0x10,%esp
f010147b:	eb 01                	jmp    f010147e <vprintfmt+0x379>
f010147d:	4f                   	dec    %edi
f010147e:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
f0101482:	75 f9                	jne    f010147d <vprintfmt+0x378>
f0101484:	e9 a2 fc ff ff       	jmp    f010112b <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
f0101489:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010148c:	5b                   	pop    %ebx
f010148d:	5e                   	pop    %esi
f010148e:	5f                   	pop    %edi
f010148f:	5d                   	pop    %ebp
f0101490:	c3                   	ret    

f0101491 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f0101491:	55                   	push   %ebp
f0101492:	89 e5                	mov    %esp,%ebp
f0101494:	83 ec 18             	sub    $0x18,%esp
f0101497:	8b 45 08             	mov    0x8(%ebp),%eax
f010149a:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f010149d:	89 45 ec             	mov    %eax,-0x14(%ebp)
f01014a0:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f01014a4:	89 4d f0             	mov    %ecx,-0x10(%ebp)
f01014a7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f01014ae:	85 c0                	test   %eax,%eax
f01014b0:	74 26                	je     f01014d8 <vsnprintf+0x47>
f01014b2:	85 d2                	test   %edx,%edx
f01014b4:	7e 29                	jle    f01014df <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f01014b6:	ff 75 14             	pushl  0x14(%ebp)
f01014b9:	ff 75 10             	pushl  0x10(%ebp)
f01014bc:	8d 45 ec             	lea    -0x14(%ebp),%eax
f01014bf:	50                   	push   %eax
f01014c0:	68 cc 10 10 f0       	push   $0xf01010cc
f01014c5:	e8 3b fc ff ff       	call   f0101105 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f01014ca:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01014cd:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f01014d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01014d3:	83 c4 10             	add    $0x10,%esp
f01014d6:	eb 0c                	jmp    f01014e4 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f01014d8:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f01014dd:	eb 05                	jmp    f01014e4 <vsnprintf+0x53>
f01014df:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f01014e4:	c9                   	leave  
f01014e5:	c3                   	ret    

f01014e6 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f01014e6:	55                   	push   %ebp
f01014e7:	89 e5                	mov    %esp,%ebp
f01014e9:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f01014ec:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f01014ef:	50                   	push   %eax
f01014f0:	ff 75 10             	pushl  0x10(%ebp)
f01014f3:	ff 75 0c             	pushl  0xc(%ebp)
f01014f6:	ff 75 08             	pushl  0x8(%ebp)
f01014f9:	e8 93 ff ff ff       	call   f0101491 <vsnprintf>
	va_end(ap);

	return rc;
}
f01014fe:	c9                   	leave  
f01014ff:	c3                   	ret    

f0101500 <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f0101500:	55                   	push   %ebp
f0101501:	89 e5                	mov    %esp,%ebp
f0101503:	57                   	push   %edi
f0101504:	56                   	push   %esi
f0101505:	53                   	push   %ebx
f0101506:	83 ec 0c             	sub    $0xc,%esp
f0101509:	8b 45 08             	mov    0x8(%ebp),%eax
	int i, c, echoing;

	if (prompt != NULL)
f010150c:	85 c0                	test   %eax,%eax
f010150e:	74 11                	je     f0101521 <readline+0x21>
		cprintf("%s", prompt);
f0101510:	83 ec 08             	sub    $0x8,%esp
f0101513:	50                   	push   %eax
f0101514:	68 fd 1b 10 f0       	push   $0xf0101bfd
f0101519:	e8 33 f6 ff ff       	call   f0100b51 <cprintf>
f010151e:	83 c4 10             	add    $0x10,%esp

	i = 0;
	echoing = iscons(0);
f0101521:	83 ec 0c             	sub    $0xc,%esp
f0101524:	6a 00                	push   $0x0
f0101526:	e8 33 f1 ff ff       	call   f010065e <iscons>
f010152b:	89 c7                	mov    %eax,%edi
f010152d:	83 c4 10             	add    $0x10,%esp
	int i, c, echoing;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
f0101530:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
f0101535:	e8 13 f1 ff ff       	call   f010064d <getchar>
f010153a:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f010153c:	85 c0                	test   %eax,%eax
f010153e:	79 1b                	jns    f010155b <readline+0x5b>
			cprintf("read error: %e\n", c);
f0101540:	83 ec 08             	sub    $0x8,%esp
f0101543:	50                   	push   %eax
f0101544:	68 40 29 10 f0       	push   $0xf0102940
f0101549:	e8 03 f6 ff ff       	call   f0100b51 <cprintf>
			return NULL;
f010154e:	83 c4 10             	add    $0x10,%esp
f0101551:	b8 00 00 00 00       	mov    $0x0,%eax
f0101556:	e9 8d 00 00 00       	jmp    f01015e8 <readline+0xe8>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f010155b:	83 f8 08             	cmp    $0x8,%eax
f010155e:	74 72                	je     f01015d2 <readline+0xd2>
f0101560:	83 f8 7f             	cmp    $0x7f,%eax
f0101563:	75 16                	jne    f010157b <readline+0x7b>
f0101565:	eb 65                	jmp    f01015cc <readline+0xcc>
			if (echoing)
f0101567:	85 ff                	test   %edi,%edi
f0101569:	74 0d                	je     f0101578 <readline+0x78>
				cputchar('\b');
f010156b:	83 ec 0c             	sub    $0xc,%esp
f010156e:	6a 08                	push   $0x8
f0101570:	e8 c8 f0 ff ff       	call   f010063d <cputchar>
f0101575:	83 c4 10             	add    $0x10,%esp
			i--;
f0101578:	4e                   	dec    %esi
f0101579:	eb ba                	jmp    f0101535 <readline+0x35>
		} else if (c >= ' ' && i < BUFLEN-1) {
f010157b:	83 f8 1f             	cmp    $0x1f,%eax
f010157e:	7e 23                	jle    f01015a3 <readline+0xa3>
f0101580:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0101586:	7f 1b                	jg     f01015a3 <readline+0xa3>
			if (echoing)
f0101588:	85 ff                	test   %edi,%edi
f010158a:	74 0c                	je     f0101598 <readline+0x98>
				cputchar(c);
f010158c:	83 ec 0c             	sub    $0xc,%esp
f010158f:	53                   	push   %ebx
f0101590:	e8 a8 f0 ff ff       	call   f010063d <cputchar>
f0101595:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f0101598:	88 9e 40 35 11 f0    	mov    %bl,-0xfeecac0(%esi)
f010159e:	8d 76 01             	lea    0x1(%esi),%esi
f01015a1:	eb 92                	jmp    f0101535 <readline+0x35>
		} else if (c == '\n' || c == '\r') {
f01015a3:	83 fb 0a             	cmp    $0xa,%ebx
f01015a6:	74 05                	je     f01015ad <readline+0xad>
f01015a8:	83 fb 0d             	cmp    $0xd,%ebx
f01015ab:	75 88                	jne    f0101535 <readline+0x35>
			if (echoing)
f01015ad:	85 ff                	test   %edi,%edi
f01015af:	74 0d                	je     f01015be <readline+0xbe>
				cputchar('\n');
f01015b1:	83 ec 0c             	sub    $0xc,%esp
f01015b4:	6a 0a                	push   $0xa
f01015b6:	e8 82 f0 ff ff       	call   f010063d <cputchar>
f01015bb:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
f01015be:	c6 86 40 35 11 f0 00 	movb   $0x0,-0xfeecac0(%esi)
			return buf;
f01015c5:	b8 40 35 11 f0       	mov    $0xf0113540,%eax
f01015ca:	eb 1c                	jmp    f01015e8 <readline+0xe8>
	while (1) {
		c = getchar();
		if (c < 0) {
			cprintf("read error: %e\n", c);
			return NULL;
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f01015cc:	85 f6                	test   %esi,%esi
f01015ce:	7f 97                	jg     f0101567 <readline+0x67>
f01015d0:	eb 09                	jmp    f01015db <readline+0xdb>
f01015d2:	85 f6                	test   %esi,%esi
f01015d4:	7f 91                	jg     f0101567 <readline+0x67>
f01015d6:	e9 5a ff ff ff       	jmp    f0101535 <readline+0x35>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
f01015db:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f01015e1:	7e a5                	jle    f0101588 <readline+0x88>
f01015e3:	e9 4d ff ff ff       	jmp    f0101535 <readline+0x35>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
f01015e8:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01015eb:	5b                   	pop    %ebx
f01015ec:	5e                   	pop    %esi
f01015ed:	5f                   	pop    %edi
f01015ee:	5d                   	pop    %ebp
f01015ef:	c3                   	ret    

f01015f0 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f01015f0:	55                   	push   %ebp
f01015f1:	89 e5                	mov    %esp,%ebp
f01015f3:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f01015f6:	b8 00 00 00 00       	mov    $0x0,%eax
f01015fb:	eb 01                	jmp    f01015fe <strlen+0xe>
		n++;
f01015fd:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f01015fe:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f0101602:	75 f9                	jne    f01015fd <strlen+0xd>
		n++;
	return n;
}
f0101604:	5d                   	pop    %ebp
f0101605:	c3                   	ret    

f0101606 <strnlen>:

int
strnlen(const char *s, size_t size)
{
f0101606:	55                   	push   %ebp
f0101607:	89 e5                	mov    %esp,%ebp
f0101609:	8b 4d 08             	mov    0x8(%ebp),%ecx
f010160c:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f010160f:	ba 00 00 00 00       	mov    $0x0,%edx
f0101614:	eb 01                	jmp    f0101617 <strnlen+0x11>
		n++;
f0101616:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0101617:	39 c2                	cmp    %eax,%edx
f0101619:	74 08                	je     f0101623 <strnlen+0x1d>
f010161b:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
f010161f:	75 f5                	jne    f0101616 <strnlen+0x10>
f0101621:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
f0101623:	5d                   	pop    %ebp
f0101624:	c3                   	ret    

f0101625 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f0101625:	55                   	push   %ebp
f0101626:	89 e5                	mov    %esp,%ebp
f0101628:	53                   	push   %ebx
f0101629:	8b 45 08             	mov    0x8(%ebp),%eax
f010162c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
f010162f:	89 c2                	mov    %eax,%edx
f0101631:	42                   	inc    %edx
f0101632:	41                   	inc    %ecx
f0101633:	8a 59 ff             	mov    -0x1(%ecx),%bl
f0101636:	88 5a ff             	mov    %bl,-0x1(%edx)
f0101639:	84 db                	test   %bl,%bl
f010163b:	75 f4                	jne    f0101631 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
f010163d:	5b                   	pop    %ebx
f010163e:	5d                   	pop    %ebp
f010163f:	c3                   	ret    

f0101640 <strcat>:

char *
strcat(char *dst, const char *src)
{
f0101640:	55                   	push   %ebp
f0101641:	89 e5                	mov    %esp,%ebp
f0101643:	53                   	push   %ebx
f0101644:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
f0101647:	53                   	push   %ebx
f0101648:	e8 a3 ff ff ff       	call   f01015f0 <strlen>
f010164d:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
f0101650:	ff 75 0c             	pushl  0xc(%ebp)
f0101653:	01 d8                	add    %ebx,%eax
f0101655:	50                   	push   %eax
f0101656:	e8 ca ff ff ff       	call   f0101625 <strcpy>
	return dst;
}
f010165b:	89 d8                	mov    %ebx,%eax
f010165d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101660:	c9                   	leave  
f0101661:	c3                   	ret    

f0101662 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
f0101662:	55                   	push   %ebp
f0101663:	89 e5                	mov    %esp,%ebp
f0101665:	56                   	push   %esi
f0101666:	53                   	push   %ebx
f0101667:	8b 75 08             	mov    0x8(%ebp),%esi
f010166a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f010166d:	89 f3                	mov    %esi,%ebx
f010166f:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0101672:	89 f2                	mov    %esi,%edx
f0101674:	eb 0c                	jmp    f0101682 <strncpy+0x20>
		*dst++ = *src;
f0101676:	42                   	inc    %edx
f0101677:	8a 01                	mov    (%ecx),%al
f0101679:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f010167c:	80 39 01             	cmpb   $0x1,(%ecx)
f010167f:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0101682:	39 da                	cmp    %ebx,%edx
f0101684:	75 f0                	jne    f0101676 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f0101686:	89 f0                	mov    %esi,%eax
f0101688:	5b                   	pop    %ebx
f0101689:	5e                   	pop    %esi
f010168a:	5d                   	pop    %ebp
f010168b:	c3                   	ret    

f010168c <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f010168c:	55                   	push   %ebp
f010168d:	89 e5                	mov    %esp,%ebp
f010168f:	56                   	push   %esi
f0101690:	53                   	push   %ebx
f0101691:	8b 75 08             	mov    0x8(%ebp),%esi
f0101694:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0101697:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
f010169a:	85 c0                	test   %eax,%eax
f010169c:	74 1e                	je     f01016bc <strlcpy+0x30>
f010169e:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f01016a2:	89 f2                	mov    %esi,%edx
f01016a4:	eb 05                	jmp    f01016ab <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
f01016a6:	42                   	inc    %edx
f01016a7:	41                   	inc    %ecx
f01016a8:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f01016ab:	39 c2                	cmp    %eax,%edx
f01016ad:	74 08                	je     f01016b7 <strlcpy+0x2b>
f01016af:	8a 19                	mov    (%ecx),%bl
f01016b1:	84 db                	test   %bl,%bl
f01016b3:	75 f1                	jne    f01016a6 <strlcpy+0x1a>
f01016b5:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
f01016b7:	c6 00 00             	movb   $0x0,(%eax)
f01016ba:	eb 02                	jmp    f01016be <strlcpy+0x32>
f01016bc:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
f01016be:	29 f0                	sub    %esi,%eax
}
f01016c0:	5b                   	pop    %ebx
f01016c1:	5e                   	pop    %esi
f01016c2:	5d                   	pop    %ebp
f01016c3:	c3                   	ret    

f01016c4 <strcmp>:

int
strcmp(const char *p, const char *q)
{
f01016c4:	55                   	push   %ebp
f01016c5:	89 e5                	mov    %esp,%ebp
f01016c7:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01016ca:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
f01016cd:	eb 02                	jmp    f01016d1 <strcmp+0xd>
		p++, q++;
f01016cf:	41                   	inc    %ecx
f01016d0:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f01016d1:	8a 01                	mov    (%ecx),%al
f01016d3:	84 c0                	test   %al,%al
f01016d5:	74 04                	je     f01016db <strcmp+0x17>
f01016d7:	3a 02                	cmp    (%edx),%al
f01016d9:	74 f4                	je     f01016cf <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f01016db:	0f b6 c0             	movzbl %al,%eax
f01016de:	0f b6 12             	movzbl (%edx),%edx
f01016e1:	29 d0                	sub    %edx,%eax
}
f01016e3:	5d                   	pop    %ebp
f01016e4:	c3                   	ret    

f01016e5 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
f01016e5:	55                   	push   %ebp
f01016e6:	89 e5                	mov    %esp,%ebp
f01016e8:	53                   	push   %ebx
f01016e9:	8b 45 08             	mov    0x8(%ebp),%eax
f01016ec:	8b 55 0c             	mov    0xc(%ebp),%edx
f01016ef:	89 c3                	mov    %eax,%ebx
f01016f1:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
f01016f4:	eb 02                	jmp    f01016f8 <strncmp+0x13>
		n--, p++, q++;
f01016f6:	40                   	inc    %eax
f01016f7:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
f01016f8:	39 d8                	cmp    %ebx,%eax
f01016fa:	74 14                	je     f0101710 <strncmp+0x2b>
f01016fc:	8a 08                	mov    (%eax),%cl
f01016fe:	84 c9                	test   %cl,%cl
f0101700:	74 04                	je     f0101706 <strncmp+0x21>
f0101702:	3a 0a                	cmp    (%edx),%cl
f0101704:	74 f0                	je     f01016f6 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0101706:	0f b6 00             	movzbl (%eax),%eax
f0101709:	0f b6 12             	movzbl (%edx),%edx
f010170c:	29 d0                	sub    %edx,%eax
f010170e:	eb 05                	jmp    f0101715 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
f0101710:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
f0101715:	5b                   	pop    %ebx
f0101716:	5d                   	pop    %ebp
f0101717:	c3                   	ret    

f0101718 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f0101718:	55                   	push   %ebp
f0101719:	89 e5                	mov    %esp,%ebp
f010171b:	8b 45 08             	mov    0x8(%ebp),%eax
f010171e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0101721:	eb 05                	jmp    f0101728 <strchr+0x10>
		if (*s == c)
f0101723:	38 ca                	cmp    %cl,%dl
f0101725:	74 0c                	je     f0101733 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f0101727:	40                   	inc    %eax
f0101728:	8a 10                	mov    (%eax),%dl
f010172a:	84 d2                	test   %dl,%dl
f010172c:	75 f5                	jne    f0101723 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
f010172e:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0101733:	5d                   	pop    %ebp
f0101734:	c3                   	ret    

f0101735 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f0101735:	55                   	push   %ebp
f0101736:	89 e5                	mov    %esp,%ebp
f0101738:	8b 45 08             	mov    0x8(%ebp),%eax
f010173b:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f010173e:	eb 05                	jmp    f0101745 <strfind+0x10>
		if (*s == c)
f0101740:	38 ca                	cmp    %cl,%dl
f0101742:	74 07                	je     f010174b <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f0101744:	40                   	inc    %eax
f0101745:	8a 10                	mov    (%eax),%dl
f0101747:	84 d2                	test   %dl,%dl
f0101749:	75 f5                	jne    f0101740 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
f010174b:	5d                   	pop    %ebp
f010174c:	c3                   	ret    

f010174d <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
f010174d:	55                   	push   %ebp
f010174e:	89 e5                	mov    %esp,%ebp
f0101750:	57                   	push   %edi
f0101751:	56                   	push   %esi
f0101752:	53                   	push   %ebx
f0101753:	8b 7d 08             	mov    0x8(%ebp),%edi
f0101756:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
f0101759:	85 c9                	test   %ecx,%ecx
f010175b:	74 36                	je     f0101793 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
f010175d:	f7 c7 03 00 00 00    	test   $0x3,%edi
f0101763:	75 28                	jne    f010178d <memset+0x40>
f0101765:	f6 c1 03             	test   $0x3,%cl
f0101768:	75 23                	jne    f010178d <memset+0x40>
		c &= 0xFF;
f010176a:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f010176e:	89 d3                	mov    %edx,%ebx
f0101770:	c1 e3 08             	shl    $0x8,%ebx
f0101773:	89 d6                	mov    %edx,%esi
f0101775:	c1 e6 18             	shl    $0x18,%esi
f0101778:	89 d0                	mov    %edx,%eax
f010177a:	c1 e0 10             	shl    $0x10,%eax
f010177d:	09 f0                	or     %esi,%eax
f010177f:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
f0101781:	89 d8                	mov    %ebx,%eax
f0101783:	09 d0                	or     %edx,%eax
f0101785:	c1 e9 02             	shr    $0x2,%ecx
f0101788:	fc                   	cld    
f0101789:	f3 ab                	rep stos %eax,%es:(%edi)
f010178b:	eb 06                	jmp    f0101793 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
f010178d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101790:	fc                   	cld    
f0101791:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
f0101793:	89 f8                	mov    %edi,%eax
f0101795:	5b                   	pop    %ebx
f0101796:	5e                   	pop    %esi
f0101797:	5f                   	pop    %edi
f0101798:	5d                   	pop    %ebp
f0101799:	c3                   	ret    

f010179a <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
f010179a:	55                   	push   %ebp
f010179b:	89 e5                	mov    %esp,%ebp
f010179d:	57                   	push   %edi
f010179e:	56                   	push   %esi
f010179f:	8b 45 08             	mov    0x8(%ebp),%eax
f01017a2:	8b 75 0c             	mov    0xc(%ebp),%esi
f01017a5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
f01017a8:	39 c6                	cmp    %eax,%esi
f01017aa:	73 33                	jae    f01017df <memmove+0x45>
f01017ac:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f01017af:	39 d0                	cmp    %edx,%eax
f01017b1:	73 2c                	jae    f01017df <memmove+0x45>
		s += n;
		d += n;
f01017b3:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f01017b6:	89 d6                	mov    %edx,%esi
f01017b8:	09 fe                	or     %edi,%esi
f01017ba:	f7 c6 03 00 00 00    	test   $0x3,%esi
f01017c0:	75 13                	jne    f01017d5 <memmove+0x3b>
f01017c2:	f6 c1 03             	test   $0x3,%cl
f01017c5:	75 0e                	jne    f01017d5 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
f01017c7:	83 ef 04             	sub    $0x4,%edi
f01017ca:	8d 72 fc             	lea    -0x4(%edx),%esi
f01017cd:	c1 e9 02             	shr    $0x2,%ecx
f01017d0:	fd                   	std    
f01017d1:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01017d3:	eb 07                	jmp    f01017dc <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
f01017d5:	4f                   	dec    %edi
f01017d6:	8d 72 ff             	lea    -0x1(%edx),%esi
f01017d9:	fd                   	std    
f01017da:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f01017dc:	fc                   	cld    
f01017dd:	eb 1d                	jmp    f01017fc <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f01017df:	89 f2                	mov    %esi,%edx
f01017e1:	09 c2                	or     %eax,%edx
f01017e3:	f6 c2 03             	test   $0x3,%dl
f01017e6:	75 0f                	jne    f01017f7 <memmove+0x5d>
f01017e8:	f6 c1 03             	test   $0x3,%cl
f01017eb:	75 0a                	jne    f01017f7 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
f01017ed:	c1 e9 02             	shr    $0x2,%ecx
f01017f0:	89 c7                	mov    %eax,%edi
f01017f2:	fc                   	cld    
f01017f3:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01017f5:	eb 05                	jmp    f01017fc <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
f01017f7:	89 c7                	mov    %eax,%edi
f01017f9:	fc                   	cld    
f01017fa:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
f01017fc:	5e                   	pop    %esi
f01017fd:	5f                   	pop    %edi
f01017fe:	5d                   	pop    %ebp
f01017ff:	c3                   	ret    

f0101800 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
f0101800:	55                   	push   %ebp
f0101801:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
f0101803:	ff 75 10             	pushl  0x10(%ebp)
f0101806:	ff 75 0c             	pushl  0xc(%ebp)
f0101809:	ff 75 08             	pushl  0x8(%ebp)
f010180c:	e8 89 ff ff ff       	call   f010179a <memmove>
}
f0101811:	c9                   	leave  
f0101812:	c3                   	ret    

f0101813 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
f0101813:	55                   	push   %ebp
f0101814:	89 e5                	mov    %esp,%ebp
f0101816:	56                   	push   %esi
f0101817:	53                   	push   %ebx
f0101818:	8b 45 08             	mov    0x8(%ebp),%eax
f010181b:	8b 55 0c             	mov    0xc(%ebp),%edx
f010181e:	89 c6                	mov    %eax,%esi
f0101820:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0101823:	eb 14                	jmp    f0101839 <memcmp+0x26>
		if (*s1 != *s2)
f0101825:	8a 08                	mov    (%eax),%cl
f0101827:	8a 1a                	mov    (%edx),%bl
f0101829:	38 d9                	cmp    %bl,%cl
f010182b:	74 0a                	je     f0101837 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
f010182d:	0f b6 c1             	movzbl %cl,%eax
f0101830:	0f b6 db             	movzbl %bl,%ebx
f0101833:	29 d8                	sub    %ebx,%eax
f0101835:	eb 0b                	jmp    f0101842 <memcmp+0x2f>
		s1++, s2++;
f0101837:	40                   	inc    %eax
f0101838:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0101839:	39 f0                	cmp    %esi,%eax
f010183b:	75 e8                	jne    f0101825 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f010183d:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0101842:	5b                   	pop    %ebx
f0101843:	5e                   	pop    %esi
f0101844:	5d                   	pop    %ebp
f0101845:	c3                   	ret    

f0101846 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
f0101846:	55                   	push   %ebp
f0101847:	89 e5                	mov    %esp,%ebp
f0101849:	53                   	push   %ebx
f010184a:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
f010184d:	89 c1                	mov    %eax,%ecx
f010184f:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
f0101852:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0101856:	eb 08                	jmp    f0101860 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
f0101858:	0f b6 10             	movzbl (%eax),%edx
f010185b:	39 da                	cmp    %ebx,%edx
f010185d:	74 05                	je     f0101864 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f010185f:	40                   	inc    %eax
f0101860:	39 c8                	cmp    %ecx,%eax
f0101862:	72 f4                	jb     f0101858 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
f0101864:	5b                   	pop    %ebx
f0101865:	5d                   	pop    %ebp
f0101866:	c3                   	ret    

f0101867 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f0101867:	55                   	push   %ebp
f0101868:	89 e5                	mov    %esp,%ebp
f010186a:	57                   	push   %edi
f010186b:	56                   	push   %esi
f010186c:	53                   	push   %ebx
f010186d:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0101870:	eb 01                	jmp    f0101873 <strtol+0xc>
		s++;
f0101872:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0101873:	8a 01                	mov    (%ecx),%al
f0101875:	3c 20                	cmp    $0x20,%al
f0101877:	74 f9                	je     f0101872 <strtol+0xb>
f0101879:	3c 09                	cmp    $0x9,%al
f010187b:	74 f5                	je     f0101872 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
f010187d:	3c 2b                	cmp    $0x2b,%al
f010187f:	75 08                	jne    f0101889 <strtol+0x22>
		s++;
f0101881:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0101882:	bf 00 00 00 00       	mov    $0x0,%edi
f0101887:	eb 11                	jmp    f010189a <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f0101889:	3c 2d                	cmp    $0x2d,%al
f010188b:	75 08                	jne    f0101895 <strtol+0x2e>
		s++, neg = 1;
f010188d:	41                   	inc    %ecx
f010188e:	bf 01 00 00 00       	mov    $0x1,%edi
f0101893:	eb 05                	jmp    f010189a <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0101895:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f010189a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f010189e:	0f 84 87 00 00 00    	je     f010192b <strtol+0xc4>
f01018a4:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f01018a8:	75 27                	jne    f01018d1 <strtol+0x6a>
f01018aa:	80 39 30             	cmpb   $0x30,(%ecx)
f01018ad:	75 22                	jne    f01018d1 <strtol+0x6a>
f01018af:	e9 88 00 00 00       	jmp    f010193c <strtol+0xd5>
		s += 2, base = 16;
f01018b4:	83 c1 02             	add    $0x2,%ecx
f01018b7:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f01018be:	eb 11                	jmp    f01018d1 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
f01018c0:	41                   	inc    %ecx
f01018c1:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f01018c8:	eb 07                	jmp    f01018d1 <strtol+0x6a>
	else if (base == 0)
		base = 10;
f01018ca:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
f01018d1:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f01018d6:	8a 11                	mov    (%ecx),%dl
f01018d8:	8d 5a d0             	lea    -0x30(%edx),%ebx
f01018db:	80 fb 09             	cmp    $0x9,%bl
f01018de:	77 08                	ja     f01018e8 <strtol+0x81>
			dig = *s - '0';
f01018e0:	0f be d2             	movsbl %dl,%edx
f01018e3:	83 ea 30             	sub    $0x30,%edx
f01018e6:	eb 22                	jmp    f010190a <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
f01018e8:	8d 72 9f             	lea    -0x61(%edx),%esi
f01018eb:	89 f3                	mov    %esi,%ebx
f01018ed:	80 fb 19             	cmp    $0x19,%bl
f01018f0:	77 08                	ja     f01018fa <strtol+0x93>
			dig = *s - 'a' + 10;
f01018f2:	0f be d2             	movsbl %dl,%edx
f01018f5:	83 ea 57             	sub    $0x57,%edx
f01018f8:	eb 10                	jmp    f010190a <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
f01018fa:	8d 72 bf             	lea    -0x41(%edx),%esi
f01018fd:	89 f3                	mov    %esi,%ebx
f01018ff:	80 fb 19             	cmp    $0x19,%bl
f0101902:	77 14                	ja     f0101918 <strtol+0xb1>
			dig = *s - 'A' + 10;
f0101904:	0f be d2             	movsbl %dl,%edx
f0101907:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
f010190a:	3b 55 10             	cmp    0x10(%ebp),%edx
f010190d:	7d 09                	jge    f0101918 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
f010190f:	41                   	inc    %ecx
f0101910:	0f af 45 10          	imul   0x10(%ebp),%eax
f0101914:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
f0101916:	eb be                	jmp    f01018d6 <strtol+0x6f>

	if (endptr)
f0101918:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f010191c:	74 05                	je     f0101923 <strtol+0xbc>
		*endptr = (char *) s;
f010191e:	8b 75 0c             	mov    0xc(%ebp),%esi
f0101921:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
f0101923:	85 ff                	test   %edi,%edi
f0101925:	74 21                	je     f0101948 <strtol+0xe1>
f0101927:	f7 d8                	neg    %eax
f0101929:	eb 1d                	jmp    f0101948 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f010192b:	80 39 30             	cmpb   $0x30,(%ecx)
f010192e:	75 9a                	jne    f01018ca <strtol+0x63>
f0101930:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f0101934:	0f 84 7a ff ff ff    	je     f01018b4 <strtol+0x4d>
f010193a:	eb 84                	jmp    f01018c0 <strtol+0x59>
f010193c:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f0101940:	0f 84 6e ff ff ff    	je     f01018b4 <strtol+0x4d>
f0101946:	eb 89                	jmp    f01018d1 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
f0101948:	5b                   	pop    %ebx
f0101949:	5e                   	pop    %esi
f010194a:	5f                   	pop    %edi
f010194b:	5d                   	pop    %ebp
f010194c:	c3                   	ret    
f010194d:	66 90                	xchg   %ax,%ax
f010194f:	90                   	nop

f0101950 <__udivdi3>:
f0101950:	55                   	push   %ebp
f0101951:	57                   	push   %edi
f0101952:	56                   	push   %esi
f0101953:	53                   	push   %ebx
f0101954:	83 ec 1c             	sub    $0x1c,%esp
f0101957:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f010195b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f010195f:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0101963:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0101967:	89 ca                	mov    %ecx,%edx
f0101969:	89 f8                	mov    %edi,%eax
f010196b:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f010196f:	85 f6                	test   %esi,%esi
f0101971:	75 2d                	jne    f01019a0 <__udivdi3+0x50>
f0101973:	39 cf                	cmp    %ecx,%edi
f0101975:	77 65                	ja     f01019dc <__udivdi3+0x8c>
f0101977:	89 fd                	mov    %edi,%ebp
f0101979:	85 ff                	test   %edi,%edi
f010197b:	75 0b                	jne    f0101988 <__udivdi3+0x38>
f010197d:	b8 01 00 00 00       	mov    $0x1,%eax
f0101982:	31 d2                	xor    %edx,%edx
f0101984:	f7 f7                	div    %edi
f0101986:	89 c5                	mov    %eax,%ebp
f0101988:	31 d2                	xor    %edx,%edx
f010198a:	89 c8                	mov    %ecx,%eax
f010198c:	f7 f5                	div    %ebp
f010198e:	89 c1                	mov    %eax,%ecx
f0101990:	89 d8                	mov    %ebx,%eax
f0101992:	f7 f5                	div    %ebp
f0101994:	89 cf                	mov    %ecx,%edi
f0101996:	89 fa                	mov    %edi,%edx
f0101998:	83 c4 1c             	add    $0x1c,%esp
f010199b:	5b                   	pop    %ebx
f010199c:	5e                   	pop    %esi
f010199d:	5f                   	pop    %edi
f010199e:	5d                   	pop    %ebp
f010199f:	c3                   	ret    
f01019a0:	39 ce                	cmp    %ecx,%esi
f01019a2:	77 28                	ja     f01019cc <__udivdi3+0x7c>
f01019a4:	0f bd fe             	bsr    %esi,%edi
f01019a7:	83 f7 1f             	xor    $0x1f,%edi
f01019aa:	75 40                	jne    f01019ec <__udivdi3+0x9c>
f01019ac:	39 ce                	cmp    %ecx,%esi
f01019ae:	72 0a                	jb     f01019ba <__udivdi3+0x6a>
f01019b0:	3b 44 24 08          	cmp    0x8(%esp),%eax
f01019b4:	0f 87 9e 00 00 00    	ja     f0101a58 <__udivdi3+0x108>
f01019ba:	b8 01 00 00 00       	mov    $0x1,%eax
f01019bf:	89 fa                	mov    %edi,%edx
f01019c1:	83 c4 1c             	add    $0x1c,%esp
f01019c4:	5b                   	pop    %ebx
f01019c5:	5e                   	pop    %esi
f01019c6:	5f                   	pop    %edi
f01019c7:	5d                   	pop    %ebp
f01019c8:	c3                   	ret    
f01019c9:	8d 76 00             	lea    0x0(%esi),%esi
f01019cc:	31 ff                	xor    %edi,%edi
f01019ce:	31 c0                	xor    %eax,%eax
f01019d0:	89 fa                	mov    %edi,%edx
f01019d2:	83 c4 1c             	add    $0x1c,%esp
f01019d5:	5b                   	pop    %ebx
f01019d6:	5e                   	pop    %esi
f01019d7:	5f                   	pop    %edi
f01019d8:	5d                   	pop    %ebp
f01019d9:	c3                   	ret    
f01019da:	66 90                	xchg   %ax,%ax
f01019dc:	89 d8                	mov    %ebx,%eax
f01019de:	f7 f7                	div    %edi
f01019e0:	31 ff                	xor    %edi,%edi
f01019e2:	89 fa                	mov    %edi,%edx
f01019e4:	83 c4 1c             	add    $0x1c,%esp
f01019e7:	5b                   	pop    %ebx
f01019e8:	5e                   	pop    %esi
f01019e9:	5f                   	pop    %edi
f01019ea:	5d                   	pop    %ebp
f01019eb:	c3                   	ret    
f01019ec:	bd 20 00 00 00       	mov    $0x20,%ebp
f01019f1:	89 eb                	mov    %ebp,%ebx
f01019f3:	29 fb                	sub    %edi,%ebx
f01019f5:	89 f9                	mov    %edi,%ecx
f01019f7:	d3 e6                	shl    %cl,%esi
f01019f9:	89 c5                	mov    %eax,%ebp
f01019fb:	88 d9                	mov    %bl,%cl
f01019fd:	d3 ed                	shr    %cl,%ebp
f01019ff:	89 e9                	mov    %ebp,%ecx
f0101a01:	09 f1                	or     %esi,%ecx
f0101a03:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f0101a07:	89 f9                	mov    %edi,%ecx
f0101a09:	d3 e0                	shl    %cl,%eax
f0101a0b:	89 c5                	mov    %eax,%ebp
f0101a0d:	89 d6                	mov    %edx,%esi
f0101a0f:	88 d9                	mov    %bl,%cl
f0101a11:	d3 ee                	shr    %cl,%esi
f0101a13:	89 f9                	mov    %edi,%ecx
f0101a15:	d3 e2                	shl    %cl,%edx
f0101a17:	8b 44 24 08          	mov    0x8(%esp),%eax
f0101a1b:	88 d9                	mov    %bl,%cl
f0101a1d:	d3 e8                	shr    %cl,%eax
f0101a1f:	09 c2                	or     %eax,%edx
f0101a21:	89 d0                	mov    %edx,%eax
f0101a23:	89 f2                	mov    %esi,%edx
f0101a25:	f7 74 24 0c          	divl   0xc(%esp)
f0101a29:	89 d6                	mov    %edx,%esi
f0101a2b:	89 c3                	mov    %eax,%ebx
f0101a2d:	f7 e5                	mul    %ebp
f0101a2f:	39 d6                	cmp    %edx,%esi
f0101a31:	72 19                	jb     f0101a4c <__udivdi3+0xfc>
f0101a33:	74 0b                	je     f0101a40 <__udivdi3+0xf0>
f0101a35:	89 d8                	mov    %ebx,%eax
f0101a37:	31 ff                	xor    %edi,%edi
f0101a39:	e9 58 ff ff ff       	jmp    f0101996 <__udivdi3+0x46>
f0101a3e:	66 90                	xchg   %ax,%ax
f0101a40:	8b 54 24 08          	mov    0x8(%esp),%edx
f0101a44:	89 f9                	mov    %edi,%ecx
f0101a46:	d3 e2                	shl    %cl,%edx
f0101a48:	39 c2                	cmp    %eax,%edx
f0101a4a:	73 e9                	jae    f0101a35 <__udivdi3+0xe5>
f0101a4c:	8d 43 ff             	lea    -0x1(%ebx),%eax
f0101a4f:	31 ff                	xor    %edi,%edi
f0101a51:	e9 40 ff ff ff       	jmp    f0101996 <__udivdi3+0x46>
f0101a56:	66 90                	xchg   %ax,%ax
f0101a58:	31 c0                	xor    %eax,%eax
f0101a5a:	e9 37 ff ff ff       	jmp    f0101996 <__udivdi3+0x46>
f0101a5f:	90                   	nop

f0101a60 <__umoddi3>:
f0101a60:	55                   	push   %ebp
f0101a61:	57                   	push   %edi
f0101a62:	56                   	push   %esi
f0101a63:	53                   	push   %ebx
f0101a64:	83 ec 1c             	sub    $0x1c,%esp
f0101a67:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f0101a6b:	8b 74 24 34          	mov    0x34(%esp),%esi
f0101a6f:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0101a73:	8b 44 24 3c          	mov    0x3c(%esp),%eax
f0101a77:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101a7b:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0101a7f:	89 f3                	mov    %esi,%ebx
f0101a81:	89 fa                	mov    %edi,%edx
f0101a83:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0101a87:	89 34 24             	mov    %esi,(%esp)
f0101a8a:	85 c0                	test   %eax,%eax
f0101a8c:	75 1a                	jne    f0101aa8 <__umoddi3+0x48>
f0101a8e:	39 f7                	cmp    %esi,%edi
f0101a90:	0f 86 a2 00 00 00    	jbe    f0101b38 <__umoddi3+0xd8>
f0101a96:	89 c8                	mov    %ecx,%eax
f0101a98:	89 f2                	mov    %esi,%edx
f0101a9a:	f7 f7                	div    %edi
f0101a9c:	89 d0                	mov    %edx,%eax
f0101a9e:	31 d2                	xor    %edx,%edx
f0101aa0:	83 c4 1c             	add    $0x1c,%esp
f0101aa3:	5b                   	pop    %ebx
f0101aa4:	5e                   	pop    %esi
f0101aa5:	5f                   	pop    %edi
f0101aa6:	5d                   	pop    %ebp
f0101aa7:	c3                   	ret    
f0101aa8:	39 f0                	cmp    %esi,%eax
f0101aaa:	0f 87 ac 00 00 00    	ja     f0101b5c <__umoddi3+0xfc>
f0101ab0:	0f bd e8             	bsr    %eax,%ebp
f0101ab3:	83 f5 1f             	xor    $0x1f,%ebp
f0101ab6:	0f 84 ac 00 00 00    	je     f0101b68 <__umoddi3+0x108>
f0101abc:	bf 20 00 00 00       	mov    $0x20,%edi
f0101ac1:	29 ef                	sub    %ebp,%edi
f0101ac3:	89 fe                	mov    %edi,%esi
f0101ac5:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0101ac9:	89 e9                	mov    %ebp,%ecx
f0101acb:	d3 e0                	shl    %cl,%eax
f0101acd:	89 d7                	mov    %edx,%edi
f0101acf:	89 f1                	mov    %esi,%ecx
f0101ad1:	d3 ef                	shr    %cl,%edi
f0101ad3:	09 c7                	or     %eax,%edi
f0101ad5:	89 e9                	mov    %ebp,%ecx
f0101ad7:	d3 e2                	shl    %cl,%edx
f0101ad9:	89 14 24             	mov    %edx,(%esp)
f0101adc:	89 d8                	mov    %ebx,%eax
f0101ade:	d3 e0                	shl    %cl,%eax
f0101ae0:	89 c2                	mov    %eax,%edx
f0101ae2:	8b 44 24 08          	mov    0x8(%esp),%eax
f0101ae6:	d3 e0                	shl    %cl,%eax
f0101ae8:	89 44 24 04          	mov    %eax,0x4(%esp)
f0101aec:	8b 44 24 08          	mov    0x8(%esp),%eax
f0101af0:	89 f1                	mov    %esi,%ecx
f0101af2:	d3 e8                	shr    %cl,%eax
f0101af4:	09 d0                	or     %edx,%eax
f0101af6:	d3 eb                	shr    %cl,%ebx
f0101af8:	89 da                	mov    %ebx,%edx
f0101afa:	f7 f7                	div    %edi
f0101afc:	89 d3                	mov    %edx,%ebx
f0101afe:	f7 24 24             	mull   (%esp)
f0101b01:	89 c6                	mov    %eax,%esi
f0101b03:	89 d1                	mov    %edx,%ecx
f0101b05:	39 d3                	cmp    %edx,%ebx
f0101b07:	0f 82 87 00 00 00    	jb     f0101b94 <__umoddi3+0x134>
f0101b0d:	0f 84 91 00 00 00    	je     f0101ba4 <__umoddi3+0x144>
f0101b13:	8b 54 24 04          	mov    0x4(%esp),%edx
f0101b17:	29 f2                	sub    %esi,%edx
f0101b19:	19 cb                	sbb    %ecx,%ebx
f0101b1b:	89 d8                	mov    %ebx,%eax
f0101b1d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
f0101b21:	d3 e0                	shl    %cl,%eax
f0101b23:	89 e9                	mov    %ebp,%ecx
f0101b25:	d3 ea                	shr    %cl,%edx
f0101b27:	09 d0                	or     %edx,%eax
f0101b29:	89 e9                	mov    %ebp,%ecx
f0101b2b:	d3 eb                	shr    %cl,%ebx
f0101b2d:	89 da                	mov    %ebx,%edx
f0101b2f:	83 c4 1c             	add    $0x1c,%esp
f0101b32:	5b                   	pop    %ebx
f0101b33:	5e                   	pop    %esi
f0101b34:	5f                   	pop    %edi
f0101b35:	5d                   	pop    %ebp
f0101b36:	c3                   	ret    
f0101b37:	90                   	nop
f0101b38:	89 fd                	mov    %edi,%ebp
f0101b3a:	85 ff                	test   %edi,%edi
f0101b3c:	75 0b                	jne    f0101b49 <__umoddi3+0xe9>
f0101b3e:	b8 01 00 00 00       	mov    $0x1,%eax
f0101b43:	31 d2                	xor    %edx,%edx
f0101b45:	f7 f7                	div    %edi
f0101b47:	89 c5                	mov    %eax,%ebp
f0101b49:	89 f0                	mov    %esi,%eax
f0101b4b:	31 d2                	xor    %edx,%edx
f0101b4d:	f7 f5                	div    %ebp
f0101b4f:	89 c8                	mov    %ecx,%eax
f0101b51:	f7 f5                	div    %ebp
f0101b53:	89 d0                	mov    %edx,%eax
f0101b55:	e9 44 ff ff ff       	jmp    f0101a9e <__umoddi3+0x3e>
f0101b5a:	66 90                	xchg   %ax,%ax
f0101b5c:	89 c8                	mov    %ecx,%eax
f0101b5e:	89 f2                	mov    %esi,%edx
f0101b60:	83 c4 1c             	add    $0x1c,%esp
f0101b63:	5b                   	pop    %ebx
f0101b64:	5e                   	pop    %esi
f0101b65:	5f                   	pop    %edi
f0101b66:	5d                   	pop    %ebp
f0101b67:	c3                   	ret    
f0101b68:	3b 04 24             	cmp    (%esp),%eax
f0101b6b:	72 06                	jb     f0101b73 <__umoddi3+0x113>
f0101b6d:	3b 7c 24 04          	cmp    0x4(%esp),%edi
f0101b71:	77 0f                	ja     f0101b82 <__umoddi3+0x122>
f0101b73:	89 f2                	mov    %esi,%edx
f0101b75:	29 f9                	sub    %edi,%ecx
f0101b77:	1b 54 24 0c          	sbb    0xc(%esp),%edx
f0101b7b:	89 14 24             	mov    %edx,(%esp)
f0101b7e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0101b82:	8b 44 24 04          	mov    0x4(%esp),%eax
f0101b86:	8b 14 24             	mov    (%esp),%edx
f0101b89:	83 c4 1c             	add    $0x1c,%esp
f0101b8c:	5b                   	pop    %ebx
f0101b8d:	5e                   	pop    %esi
f0101b8e:	5f                   	pop    %edi
f0101b8f:	5d                   	pop    %ebp
f0101b90:	c3                   	ret    
f0101b91:	8d 76 00             	lea    0x0(%esi),%esi
f0101b94:	2b 04 24             	sub    (%esp),%eax
f0101b97:	19 fa                	sbb    %edi,%edx
f0101b99:	89 d1                	mov    %edx,%ecx
f0101b9b:	89 c6                	mov    %eax,%esi
f0101b9d:	e9 71 ff ff ff       	jmp    f0101b13 <__umoddi3+0xb3>
f0101ba2:	66 90                	xchg   %ax,%ax
f0101ba4:	39 44 24 04          	cmp    %eax,0x4(%esp)
f0101ba8:	72 ea                	jb     f0101b94 <__umoddi3+0x134>
f0101baa:	89 d9                	mov    %ebx,%ecx
f0101bac:	e9 62 ff ff ff       	jmp    f0101b13 <__umoddi3+0xb3>
